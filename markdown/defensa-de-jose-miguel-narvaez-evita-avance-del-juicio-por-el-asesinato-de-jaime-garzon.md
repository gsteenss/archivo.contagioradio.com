Title: Defensa de José Miguel Narváez evita avance del juicio por el asesinato de Jaime Garzón
Date: 2015-02-12 19:50
Author: CtgAdm
Category: Judicial, Nacional
Tags: Jaime Garzon, José Miguel Narváez, Opercaciones ilegales del DAS, Paramilitarismo
Slug: defensa-de-jose-miguel-narvaez-evita-avance-del-juicio-por-el-asesinato-de-jaime-garzon
Status: published

###### Foto: contagioradio.com 

El pasado 10 de febrero debía realizarse una de las audiencias dentro del proceso, sin embargo no se llevó a cabo porque la defensa de **José Miguel Narváez citó a Ever Velosa García y Rodrigo Tovar Pupo** quienes **se han negado a participar como testigos en el caso**. Aunque el juzgado no puede obligarlos a comparecer y la representación de las víctimas exige que se retire esta prueba, la **defensa insiste, retrasando todas las diligencias.**

Para Sebastián Escobar, abogado de la Comisión Colombiana de Juristas y representante de las víctimas, la **defensa de Narváez actúa de esa manera para evitar el avance del proceso de juicio en contra del Ex Sub director del DAS**, pues se ha solicitado, en cinco ocasiones, el testimonio de los ex jefes paramilitares, extraditados por el gobierno de Álvaro Uribe.

Otra de las razones que han retrasado la continuidad del juicio es que **se ha negado el reconocimiento de la parte civil a los representantes de las víctimas**. Escobar explica que aunque ya se habían constituido como tal en el proceso, primero en el juicio contra Carlos Castaño, el juzgado consideró que debían hacer la solicitud de nuevo aunque el trámite se había cumplido y es el mismo proceso investigado. El proceso ya se surtió y nuevamente se reconocieron las partes el pasado 9 de febrero.

Así las cosas, la próxima audiencia podría ser aplazada nuevamente si la defensa insiste en la práctica de los testimonios de los paramilitares y, aunque no hay premura en el tiempo en cuanto a la posibilidad de la prescripción del caso, si hay necesidad de esclarecer las responsabilidades de los autores y garantizar** los derechos de las víctimas a verdad, justicia y reparación integral.**
