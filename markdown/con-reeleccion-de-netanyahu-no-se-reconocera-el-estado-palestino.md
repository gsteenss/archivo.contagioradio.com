Title: Con reelección de Netanyahu Israel no reconocerá el Estado Palestino
Date: 2015-03-18 18:12
Author: CtgAdm
Category: El mundo, Política
Tags: Agencia de medios alternativos Israel, Netanyahu campaña del miedo por Israel, Netanyahu gana elecciones en Israel
Slug: con-reeleccion-de-netanyahu-no-se-reconocera-el-estado-palestino
Status: published

###### Foto:Notihoy.com 

##### **Entrevista con[ Sergio Yani]:** 

<iframe src="http://www.ivoox.com/player_ek_4232117_2_1.html?data=lZeglJaVe46ZmKiakpqJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRksbowtPmw83Zb8jVz8aYzsbXb8bgxsjQy9TSqdSfxtOYq9jWpcbgjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Con una participación de **más del 71,8% de los electores** el partido del actual presidente Benjamín **Netanyahu, el Likud, ha ganado los comicios** celebrados este martes en **Israel**. En total Netanyahu logra **30 escaños de 120**, dejando en segunda posición ha la coalición liberal de centro izquierda, **Unión Sionista, con 24**.

A pesar de que las encuestas vaticinaban la derrota total del líder laborista, la **agresiva campaña nacional e internacional** emprendida por el primer ministro en la recta final de la campaña electoral lo ha **situado en su mejor resultado**.

Si el Likud pacta con otras fuerzas de ultraderecha podría, en un futuro**,** ser el **primer ministro que más veces ha gobernado después de Ben Gurion.**

Pero el panorama político se oscurece para la creación de un futuro **Estado Palestino**, ya que así ha declarado reiteradas veces **Netanyahu, negando su existencia**, pero también ataca al programa nuclear de Irán y las negociaciones con EEUU.

Según **Sergio Yani, miembro de la Agencia Alternativa de Información de Israel**, el gobierno futuro va a **continuar con la línea actual** en referencia al Estado Palestino y su no reconocimiento, igual que la continuidad con las **invasiones militares y la creación de colonias ilegales judías en territorio palestino**.

Por tanto en un futuro habrán mas contiendas bélicas con países árabes. Sergio Yani explica que Netanyahu, presionado por el centro izquierda que ha conseguido consolidarse, ha realizado una **campaña basada en el miedo a los árabes** y en concreto a Irán. También la **oposición** ha basado su campaña publicitaria en el **miedo al propio primer ministro.**

Un ejemplo es la** visita del líder del Likud al congreso de EEUU** para hablar de la **amenaza que supone Irán** sin consultar con Barack Obama, provocando fuertes críticas de congresistas demócratas y del propio presidente de EEUU.

En un contexto donde Israel vive una profunda crisis social, **Palestina va a continuar** igual **a la espera de futuras agresiones** por parte de un gobierno ultraconservador como el que se formará en las siguientes semanas en **Israel**.
