Title: La salud de la democracia: una mirada desde la bioética
Date: 2018-02-22 15:24
Category: Nacional, Paz
Tags: Cátedra de Bioética, contagio radio, Paz y reconciliación, Plataforma Alto, Salud de la Democracia
Slug: catedra_abierta_bioetica_universidad_bosque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/WhatsApp-Image-2018-02-22-at-8.21.34-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cátedra de Bioética] 

###### [22 Feb 2018] 

Este 23 de febrero se realizará el segundo foro de la Cátedra Abierta de Bioética que tendrá como tema fundamental la salud de la democracia, en un momento propicio para hablar de las **violencias estructurales de las que es víctima el sistema democrático en Colombia** y que condiciona las libertades de las personas que quieren aspirar a cargos públicos, pero también las que buscan ejercer su derecho al voto.

### **El diagnóstico de la Democracia** 

De acuerdo con Mauricio Sánchez, coordinador de la Cátedra, se espera que en el foro de se analicen las realidades de la participación democrática en el país, de tal manera que se pueda conocer “qué es lo que está pasando. La democracia es esa posibilidad de elegir o ser elegido o será que desde su misma estructura **está viciado el tema del poder y mantiene unas hegemonías inamovibles**”, manifiesta Sánchez.

Además, el profesor afirmó que en este momento de tránsito para el país con los Acuerdos de paz y la implementación de los mismos, es importante analizar si se pueden cambiar los mecanismos de la democracia para que la misma esta sea garantizada, o **si por el contrario continuará como un sistema enfermo.** (Le puede interesar: ["MOE alerta sobre porcentajes atípicos de inscripciones de cédulas en 28 municipios"](https://archivo.contagioradio.com/inscripcion_cedulas_elecciones_marzo_moe/))

El producto de este proceso será un documento que pueda servir como insumo para la construcción de políticas públicas, de tal manera que sea un aporte también para el gobierno nacional, en el marco de la construcción de una agenda de participación mucho más incluyente que la actual.

**Los ponentes**

En esta sesión se contará con la participación de Ariel Ávila, subdirector de la Fundación Paz y Reconciliación; Gustavo Quirola, profesor investigador biopolítio del Departamento de Bioética; Alejandra Barrios, directora de la Misión Observatorio Electoral y el profesor e investigador Francisco Barbosa de la Universidad Externado de Colombia.

La entrada a este evento es libre e iniciará a partir de las 8:30 de la mañana, en el auditorio central de la Universidad del Bosque, en la Av Carrera novena con calle 131 \#02.

Las personas que lo deseen pueden hacer la inscripción previamente en el fanpage del departamento de Bioética en [Facebook](https://www.facebook.com/bioetica.uelbosque?hc_ref=ARQ9lhDAFJrQyoFqQF3kTilKbQ1R3ritnU1Ol3ZMt0xkV8xLxSyfstajBOlUVNhY7vc). Además la iniciativa cuenta con el respaldo de las organizaciones Iniciativa Unión por la Paz, Fundación Franz Weber, el Departamento de Bioética de la Universidad del Bosque, la Fundación Paz y Reconciliación, la Plataforma ALTO y Contagio Radio.

<iframe id="audio_23980583" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23980583_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
