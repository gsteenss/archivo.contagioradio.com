Title: Plan Colombia no ha significado “mejoría” para las comunidades: CONPAZ al presidente Obama
Date: 2016-01-29 16:31
Category: DDHH, Nacional
Tags: Conpaz, Conversaciones de paz en Colombia, Obamma
Slug: plan-colombia-no-ha-significado-mejoria-para-las-comunidades-conpaz-al-presidente-obama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/conpaz_4_contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: comunidadesconpaz.wordpress.] 

###### [29 Ene 2016.]

A pocos días de que se haga la evaluación de los 15 años de aplicación del Plan Colombia, las Comunidades Construyendo Paz en los Territorios han enviado una carta al presidente Barack Obama en la que insisten en que **lejos de los resultados positivos del Plan hay una serie de violaciones a los Derechos Humanos a los que la financiación por parte de Estados Unidos ha sido favorable.**

En la carta CONPAZ resalta que la ley de víctimas y restitución de tierras ha sido un “total fracaso” porque no ha dado garantías para el acceso a las tierras de manera digna incluso sobre decisiones de la propia Corte Constitucional. *“Evidentemente Colombia ha cambiado con el Plan Colombia. Dichos cambios, no necesariamente ha significado mejoría en la calidad de la mayoría de los colombianos*” señala.

La comunicación también señala que incluso las políticas extractivas propiciadas y facilitadas en Colombia son muy diferentes a las que se aplican en Estados Unidos, donde mediante resoluciones comunitarias **se impide la práctica del Fracking entre otras que son altamente destructivas de los ecosistemas**.

CONPAZ resalta los mecanismos jurídicos que impiden el acceso a las tierras y burlan los derechos a la consulta previa, la “tributación desigual que beneficia a los más ricos” y la cantidad de posibilidades que apoyadas por los**[tratados de libre comercio](https://archivo.contagioradio.com/se-debatira-sobre-incumplimiento-de-plan-de-accion-laboral-suscrito-con-eeuu/)** se abren para desconocer los derechos laborales de los trabajadores o la construcción de obras de infraestructura de alto impacto nocivo.

Por esas razones las comunidades **hacen un llamado para que en la evaluación de los 15 años del Plan Colombia se tengan en cuenta las voces de las comunidades negras, campesinas e indígenas** que son las víctimas directas de las acciones que a través del Plan se han visto afectadas.

[Carta al presidente Obama](https://es.scribd.com/doc/297155068/Carta-al-presidente-Obama "View Carta al presidente Obama on Scribd")

<iframe id="doc_44118" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/297155068/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
