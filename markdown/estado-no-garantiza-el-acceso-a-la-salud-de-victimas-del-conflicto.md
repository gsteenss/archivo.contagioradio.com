Title: Estado no garantiza acceso a salud y atención psicosocial a víctimas del conflicto
Date: 2017-04-04 16:32
Category: DDHH, Nacional
Tags: Acceso a Salud, El Salado, Pueblo Bello, víctimas
Slug: estado-no-garantiza-el-acceso-a-la-salud-de-victimas-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Fotografía-Pueblo-Bello-CCJ-5-e1452894635733.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [04 Abri. 2017] 

Son varias las sentencias que instancias nacionales como la Corte Constitucional, e internacionales como la Corte Interamericana (CorteIDH), han proferido para que el Estado colombiano le garantice el acceso a la salud y la atención psicosocial a las víctimas.

Sin embargo, a **la fecha los casos de muertes, las interminables filas, la falta de medicamentos y en general toda la negligencia de las EPS** ha cobrado la vida de más de 30 personas, víctimas diversos hechos que dichas sentencias habían priorizado para su atención integral en salud.

El hecho más reciente sucedió con las víctimas de la masacre de "El Salado", en donde **Juliana Torres, una niña de seis meses falleció al verse enfrentada a la ausencia de condiciones adecuadas para su tratamiento en el puesto de salud,** por la falta de suministros médicos y disponibilidad de una ambulancia para su traslado.

Además, se suma el fallecimiento de Enrique Garrido y Jaime Velazco, muertes que, según la denuncia “guardan estrecha relación con **la ausencia de convenios vigentes para la atención psicosocial de la comunidad de El Salado**”.  Le puede interesar: [La violencia sexual contra la mujer documentada en "Retorno al Salado"](https://archivo.contagioradio.com/la-violencia-sexual-contra-la-mujer-documentada-en-retorno-al-salado/)

Las víctimas de El Salado se encuentran cobijadas por la sentencia **T-045 de la Corte Constitucional** en la que ordenan al Ministerio de Salud de una parte **garantizar el derecho a la salud desde un enfoque diferencial y la prestación de atención psicosocial** y de otra, crear e implementar protocolos, programas y políticas para la garantía de este derecho a todas las víctimas.

Ninguna de las anteriores ordenes han sido cumplidas por el Ministerio a cabalidad, si bien han existido periodos de tiempo en los que la institución cumple con la sentencia, en otros momentos como el más reciente que cobro la vida de la niña Juliana, no lo ha hecho y **las víctimas se ven enfrentadas a las demoras en la prestación de los servicios de salud. ** Le puede interesar: [Gobierno no responde por derecho a la salud de los niños y las niñas](https://archivo.contagioradio.com/36779/)

Para la Comisión Colombiana de Juristas, organización que acompaña a las víctimas, esta falta de continuidad en el acompañamiento psicosocial ordenado por la Corte “**vulnera el derecho a la salud, sitúa a la comunidad en una condición de vulnerabilidad,** debilita la confianza en el Ministerio de Salud, esencial en el proceso de reparación, y trunca los procesos individuales, familiares y comunitarios de recuperación emocional que se venían desarrollando con las víctimas.

Además de este caso, se suma el de las **víctimas de Pueblo Bello**, para quienes la CorteIDH ordenó al Estado, entre otras medidas, **dar “tratamiento médico y psicológico adecuado a los familiares”. **Le puede interesar: [Víctimas en Pueblo Bello completan 26 años exigiendo verdad y justicia](https://archivo.contagioradio.com/victimas-pueblo-bello-26-anos/)

En Pueblo Bello, la falta de acceso a medicamentos, atención médica y el servicio de ambulancia terminó por **cobrar la vida de María de la Cruz Ramos, una de las madres de los 43 desaparecidos por paramilitares y miembros de la fuerza pública** en ese corregimiento. Le puede interesar: [Se agudiza la precaria situación del sistema de salud indígena](https://archivo.contagioradio.com/se-agudiza-la-precaria-situacion-del-sistema-de-salud-indigena/)

Así como los casos anteriormente mencionados, se encuentran otros 25 casos que tienen un estado de prioritario y por los que el Estado no ha dado respuesta con respecto al acceso a atención integral, **poniendo en riesgo la vida de las víctimas dentro de las que se encuentran niños y adultos mayores.**

La Comisión Colombiana de Juristas envío una comunicación a la CorteIDH para mantenerla al tanto de la grave situación por la que atraviesan las víctimas en cuanto a la garantía del derecho a la salud y la vida, razón por la cual han instado al organismo internacional **citar una audiencia de seguimiento para revisar el cumplimiento** de reparación en atención médica y psicológica de estas víctimas por parte del Estado.

<iframe id="audio_17986006" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17986006_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
