Title: "Es primordial llegar al fin del conflicto para gozar de los DDHH" Comunidad internacional
Date: 2015-11-19 17:32
Category: DDHH, Paz
Tags: Comunidad Internacional en Colombia, Diálogos de La Habana, dialogos de paz, FARC, gobierno colombiano, paramilitarismo en Colombia
Slug: primordial-llegar-al-fin-del-conflicto-para-gozar-de-ddhh-sociedad-civil-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/farc-dialogos-de-paz-cuba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9455931_2_1.html?data=mpmil56XdY6ZmKiak5WJd6KpmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptiY0tfNsdDmxc7OzpDQsMbbwteYw9GPqsrijMnSzpDHs8%2Fazc7Q1tSPtMLmwpDU0d%2FFtozYjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sara Vuorensola] 

###### [19 Nov 2015 ] 

[En el marco de los 3 años del proceso de paz, mil organizaciones de la sociedad civil colombiana en articulación con la comunidad internacional, enviaron [un comunicado a las delegaciones del Gobierno y las FARC en La Habana](http://bit.ly/1MVHNgn), en el que incluyeron a la comandancia del ELN y reiteraron su apoyo al actual proceso de paz, pero llamaron la atención respecto a su **preocupación por la presencia paramilitar en distintos territorios de la geografía nacional**.]

[Sara Vuorensola, vocera de la comunidad internacional asegura que la participación de sociedad civil internacional, tanto dentro como fuera de Colombia ha sido fundamental para la **visibilización de lo que sucede en el país con las organizaciones sociales y las comunidades**, no sólo en el marco del conflicto armado, sino en el actual proceso de negociación que ya cumple 3 años.]

[La vocera afirma que el balance general frente a estos diálogos de paz es positivo, no obstante ven la necesidad de llamar la atención frente a los **riesgos que pueden afectar el acuerdo final** relacionados con el **desconocimiento por parte de la sociedad civil** de los temas en discusión y acuerdos a los que se han llegado para que pueda opinar y apoyar el proceso, sumado al **accionar paramilitar** que se mantiene en distintas comunidades.    ]

[El objetivo primordial del acompañamiento que la comunidad internacional brinda a la sociedad civil colombiana es la promoción y protección de los derechos humanos “buscamos que la población civil pueda disfrutar de los derechos que tiene, que realmente no hayan **excusas como el conflicto para que el Gobierno no implemente políticas públicas** que generan reducción de pobreza en las zonas que han sido más afectadas por el conflicto… lo primordial es llegar a un fin del conflicto para que en esas zonas la población las víctimas del conflicto puedan comenzar a gozar de sus derechos”, concluye Vuorensola.]
