Title: Paren ya la crueldad contra los gallos
Date: 2017-11-07 13:08
Category: Columnistas invitados, Opinion
Tags: Animalistas, derechos de los animales, Peleas de gallos
Slug: paren-ya-la-crueldad-contra-los-gallos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pelea-de-gallos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tomas Rojas 

#### **Por [Juana Vitale]** 

###### 7 Nov 2017

Es inadmisible que la **Alcaldía de Mocoa y la Gobernación del Putumayo**, patrocinen en el marco de la feria Expomocoa 2017 el “reto gallístico departamental”. Lo es porque según la ley de maltrato animal “Los animales como seres sintientes no son cosas, recibirán especial protección contra el sufrimiento y el dolor, en especial, el causado directa o indirectamente por los humanos”.

Para colmo la misma ley establece como competencia de los alcaldes e inspectores de policía conocer las contravenciones, apoyados armónicamente por el Ministerio de Ambiente y Desarrollo Sostenible, Corporaciones Autónomas Regionales y el sistema de Parques Nacionales. Sin embargo Corpoamazonia también patrocinó este evento. Sí, los responsables de la protección animal son los patrocinadores de las peleas de gallos, dando un amplio respaldo institucional, político y social a estos eventos de crueldad. Les sugerimos leer la ley si es que no lo han hecho. Y aplicarla, claro está.

Niños y niñas observan la tortura de estos seres sintientes, a expensas del goce que produce en adultos que vociferan excitados  haciéndole fuerza al gallo verde o al colorado. Me pregunto ¿qué pensarán? ¿Qué sentirán?. En realidad es lamentable ver como la sangre de los gallos exacerba el ánimo en el público por las apuestas, en medio de estados alterados de conciencia por el consumo de alcohol, el goce por ganar o perder, la proyección de la virilidad de aquellos hombres con ilusiones y esperanzas, buscando fortuna en esos gallos que se juegan la vida.

Les aseguro que hay otras formas de ser hombre y conectarse con el más profundo ser masculino: la protección y el cuidado de la vida. Hay quienes dicen que los gallos nacieron para eso, pero sin las espuelas que les ponen antes de las peleas, ellos no podrían hacerse tanto daño entre sí, hasta ocasionarse la muerte. Sin esas espuelas no podrían quitarnos a los humanos el privilegio de matarnos entre sí. (**Ver: [Animalistas quieren consulta antitaurina en 2018](https://archivo.contagioradio.com/animalistas_consulta_antitaurina_enrique_panalosa/))**

Si bien es cierto, en la Colombia rural existen estas costumbres arraigadas a la tradición y a una economía legal e ilegal, no quiere decir que la cultura sea inamovible en el tiempo y no se pueda transformar. En este territorio hay muchas personas y tradiciones sensibles al mundo de la vida que rechazan este tipo de violencia contra los animales. Por otro lado no se justifica que los recursos públicos que son para destinarlos hacia el bien común, financien actos de crueldad con otros seres sintientes.

\[caption id="attachment\_48799" align="alignnone" width="800"\]![Expomocoa 2017 el “reto gallístico departamental”](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/gallos.jpg){.size-full .wp-image-48799 width="800" height="530"} Foto en Expomocoa 2017 el “reto gallístico departamental”\[/caption\]

Ya es hora de parar. Es un mensaje equivocado y contradictorio decir que estamos construyendo un país en paz y reconciliado, que estamos en “tiempos de renovación” como afirma el lema de la alcaldía, cuando se promueve este tipo de violencia y las autoridades públicas y personajes políticos como representantes a la cámara, diputados y concejales avalan este tipo de violencia en actos culturales de encuentro ciudadano, en presencia de niñas y niños. La reconciliación también es con la naturaleza, como nos lo recuerda la sabiduría de los 102 pueblos indígenas de Colombia, líderes espirituales de todas las tradiciones de fe y los nuevos movimientos sociales ambientalistas y animalistas.

Es hora de parar, basta ya de tortura, ellos son seres sintientes.

1.  ###### En la entrada de la gallera un hombre mayor nos comentó sobre la pasión del exalcalde por los gallos y los cuantiosos montos de sus apuestas.

    ###### 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
