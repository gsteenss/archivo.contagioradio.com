Title: "Tu plato de carne contamina más que tu auto" Ambiente y Sociedad
Date: 2015-10-05 13:49
Category: Ambiente, Entrevistas
Tags: Ambiente y Sociedad, cambio climatico, Consumo de carne, contaminación por producción de carne, deforestación
Slug: tu-plato-de-carne-contamina-mas-que-tu-auto-ambiente-y-sociedad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/vaca_cara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.proexgan.com]

<iframe src="http://www.ivoox.com/player_ek_8802777_2_1.html?data=mZ2dlJybe46ZmKiak5iJd6Kll5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmtdqY0tHFuNCfxcqYxcbWssafxNTb1sbRrc%2FVjNKSpZiJhZLnjNbix5DYuYzV1tnch5eWaZO3jKbaxM7JstWhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Milena Bernal, Ambiente y Sociedad] 

###### [5 Oct 2015]

**“Tu plato de carne contamina más que tu auto”**, es la conclusión que a la que llega la Asociación Ambiente y Sociedad de acuerdo al estudio de Environmental Working Group, donde se evidencia que **el consumo de carne representa el 20% de emisiones de gas de efecto invernadero.**

De acuerdo a cifras del estudio “Role of dairy in the carbon footprint of US beef”, de la Universidad de Bard College , la producción de carne implica el **uso de una gran extensión de tierras, deforestación de las mismas, contaminación del suelo y de las fuentes de agua, además de gases de efecto invernadero que emiten los animales**, (como el metano que es uno de los que más aporta al calentamiento global).

Otros estudios, citados por Ambiente y Sociedad, señalan que “si se le compara con alimentos básicos como la papa, el trigo y el arroz, el impacto de la carne roja por calorías es más extremo, ya que se requiere de **160 veces más tierra y se producen 11 veces más gases de invernadero**”.

“Nos sirven un plato de carne pero nunca pensamos de donde viene o qué pasó para que se produjera”, dice Milena Bernal, experta en el tema de cambio climático de Ambiente y Sociedad, quien explica que explica que de acuerdo a otro estudio publicado en Proceedings of the National Academy of Sciences, **para criar una vaca se utiliza una superficie 28 veces más amplia que la que se necesita para producir huevos, además se necesita 11 veces más agua para regar los campos donde crece el ganado** y granos para alimentar a los bovinos.

Con estos estudios, se muestra entonces, que **“comer menos carne podría ser una solución ambiental incluso mejor que la de dejar de usar los autos, **ya que se estima que una dieta rica en carne de vacuno (más de 100 gramos diarios) por parte de una persona, emitiría 7,2 kg. de CO2 por día”.

[![14725071068\_98c7031c21\_b](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/14725071068_98c7031c21_b.jpg){.wp-image-15244 .aligncenter width="610" height="829"}](https://archivo.contagioradio.com/tu-plato-de-carne-contamina-mas-que-tu-auto-ambiente-y-sociedad/14725071068_98c7031c21_b/)
