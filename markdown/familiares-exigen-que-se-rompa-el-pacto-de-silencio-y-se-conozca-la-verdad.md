Title: Familiares exigen que se rompa el "Pacto de silencio" y se conozca la verdad
Date: 2015-11-06 13:12
Category: DDHH, Nacional
Tags: CIDH, Condena estado Colombiano, Desaparecidos palacio de justicia, ex Presidente Belisario Betancur, Palacio de Justicia
Slug: familiares-exigen-que-se-rompa-el-pacto-de-silencio-y-se-conozca-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Sin-Olvido-Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [6 Nov 2015]

En consideración al acto de perdón ordenado por la Corte Interamericana de Derechos Humanos al Estado colombiano por su responsabilidad en los casos de muerte, tortura y desaparición durante la retoma del Palacio de Justicia, familiares de las víctimas manifestaron estar de acuerdo con el acto público adelantado la mañana de este viernes.

Sin embargo, através de un comunicado expresaron su inconformidad por no ser el ex presidente Belisario Betancur quién encabece el reconocimiento de responsabilidades y asuma el juicio que le corresponde, al igual que su gabinete de ministros, y la cúpula militar de la época, autores materiales de las retenciones, torturas y desapariciones.

La exigencia de los familiares esta orientada a que se rompa el "despiadado pacto de silencio" de 30 años, y que se asuman las culpabilidades de aquellos que teniendo la obligación de protegerlos los desaparecieran y se dedicaran a "empañar, mancillar y agredir su memoria"

<iframe src="https://www.youtube.com/embed/IDMShoillt0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

##### [Intervención de familiares de los desaparecidos ] 

<div style="font-size: 11px; padding-top: 10px; text-align: center; width: 560px;">

</div>

Compartimos el comunicado completo de los familiares de los desaparecidos del Palacio de Justicia.

[Intervencioìn acto PJ familias](https://es.scribd.com/doc/288767727/Intervencio-n-acto-PJ-familias "View Intervencioìn acto PJ familias on Scribd")

<iframe id="doc_1295" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/288767727/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
