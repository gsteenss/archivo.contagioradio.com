Title: Animales
Date: 2014-11-25 15:45
Author: AdminContagio
Slug: animales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/animales-18-e1507765242706.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/0c2991e1-967e-4d5e-9314-e76333a7ed8b.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/8c15e044-669c-4917-b49f-d904c854e542.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/05-feb2018-osezcnoantioquia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EPDzwD9WsAE4SdH.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### ANIMALES

[](https://archivo.contagioradio.com/toros/)  

###### [Cancelación de temporada taurina en Medellín: un triunfo de la ciudadanía](https://archivo.contagioradio.com/toros/)

[<time datetime="2019-01-09T16:35:56+00:00" title="2019-01-09T16:35:56+00:00">enero 9, 2019</time>](https://archivo.contagioradio.com/2019/01/09/)El alcalde de Medellín, Federico Gutiérrez anunció que los dueños mayoritarios del Centro de Espectáculos La Macarena no prestarán el lugar para la temporada taurina.[Leer más](https://archivo.contagioradio.com/toros/)  
[](https://archivo.contagioradio.com/prohibicion-caza-deportiva/)  

###### [Colombia “ad portas” de la prohibición de la caza deportiva](https://archivo.contagioradio.com/prohibicion-caza-deportiva/)

[<time datetime="2018-09-19T17:32:20+00:00" title="2018-09-19T17:32:20+00:00">septiembre 19, 2018</time>](https://archivo.contagioradio.com/2018/09/19/)Foto: Pinterest 19 Sep 2018 Actualmente está cursando una demanda en la Corte Constitucional que buscaría prohibir la caza deportiva, y podría salir favorable en un plazo de uno a dos meses; la acción constitucional pretende[Leer más](https://archivo.contagioradio.com/prohibicion-caza-deportiva/)  
[](https://archivo.contagioradio.com/en-colombia-10-especies-de-aves-migratorias-estan-en-peligro-de-extincion/)  

###### [En Colombia 10 especies de aves migratorias están en peligro de extinción](https://archivo.contagioradio.com/en-colombia-10-especies-de-aves-migratorias-estan-en-peligro-de-extincion/)

[<time datetime="2015-05-11T16:44:07+00:00" title="2015-05-11T16:44:07+00:00">mayo 11, 2015</time>](https://archivo.contagioradio.com/2015/05/11/)Foto: laschivasdelllano.com.co 11 de May 2015 De acuerdo al Ministerio de Ambiente y Desarrollo Sostenible, en Colombia existen 275 especies de aves migratorias, de las cuales 10 están en peligro de extinción clasificadas por la Unión[Leer más](https://archivo.contagioradio.com/en-colombia-10-especies-de-aves-migratorias-estan-en-peligro-de-extincion/)  
[](https://archivo.contagioradio.com/por-cambio-climatico-desaparecera-el-16-de-de-las-especies-de-la-tierra/)  

###### [Por cambio climático desaparecerá el 16% de de las especies de la tierra](https://archivo.contagioradio.com/por-cambio-climatico-desaparecera-el-16-de-de-las-especies-de-la-tierra/)

[<time datetime="2015-05-04T14:25:20+00:00" title="2015-05-04T14:25:20+00:00">mayo 4, 2015</time>](https://archivo.contagioradio.com/2015/05/04/)Foto: share.america.gov De acuerdo a un estudio de la Universidad de Connecticut, de Estados Unidos, publicado en la revista Science una de cada seis especies de fauna y flora desaparecerán del planeta, a causa del cambio[Leer más](https://archivo.contagioradio.com/por-cambio-climatico-desaparecera-el-16-de-de-las-especies-de-la-tierra/)
