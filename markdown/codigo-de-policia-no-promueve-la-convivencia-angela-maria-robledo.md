Title: Código de Policía no promueve la convivencia: Ángela María Robledo
Date: 2018-01-31 14:29
Category: DDHH, Nacional
Tags: abuso de autoridad, audiencia pública, Bogotá, código de policía, policia
Slug: codigo-de-policia-no-promueve-la-convivencia-angela-maria-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/policia-alcaldia-1200x800-e1507224817473.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro] 

###### [31 Ene 2018] 

A un año de la aplicación del nuevo Código de Policía en Bogotá, los congresistas Ángela María Robledo, Alirio Uribe e Iván Cepeda, realizaron una audiencia pública para evaluar los impactos y los objetivos que ha tenido este código desde su reformulación. Hay 250 investigaciones por abuso de autoridad, **un incremento del 100% en hurtos** y 60 demandas contra el código ante la Corte Constitucional.

Las cifras que presentó el representante Alirio Uribe muestra que en Bogotá “los indicadores de hurto a personas, hurto de celulares y hurto de bicicletas en Bogotá todos se **incrementaron de 2016 a 2017**”. Manifiesta que el hurto a personas pasó de 37.896 a 62.501 y el hurto a bicicletas pasó de 1.977 a 3.133 en 2017. Además, el hurto de celulares, pasó de 12.295 a 30.801 en sólo un año.

### **Denuncias por abuso de autoridad han aumentado** 

De acuerdo con la Personería de Bogotá, en el último semestre la Personería de bogotá abrió **100 investigaciones por exceso de uso de la fuerza** y abuso de autoridad en diferentes localidades, esto tras la entrada en vigencia del código de policía. En la Procuraduría General de la Nación hay 150 casos más que están en investigación.

Además, en un derecho de petición que envió el representante a la cámara, el Ministerio de Defensa maneja cifras donde “la Policía reportó a septiembre de 2017, tener **9 mil investigaciones disciplinarias** vigentes por abuso de autoridad, incumplimiento de órdenes, agresión física, entre otras iniciadas contra agentes de policía”. (Le puede interesar: ["Aumentan denuncias por abuso de autoridad en implementación del nuevo código de Policía"](https://archivo.contagioradio.com/aumentan-denuncias-por-abuso-de-autoridad-en-bogota/))

Esta situación la retrato a Contagio Radio, Ángela María Robledo quien se ha opuesto de manera enfática a este nuevo código de Policía que lo denomina **“draconiano y represivo** pues ha perdido el espíritu de lo que debe ser un código de buen comportamiento”. Dijo que el código contempla más de 100 comportamientos que reciben multa que van desde los 92 mil pesos hasta los 800 mil pesos.

### **Código tiene margen de arbitrariedad para el Policía** 

Robledo argumentó que las poblaciones que más se enfrenta con la Policía son los **habitantes de calle, los vendedores ambulantes y los estudiantes** de ciertas localidades en Bogotá “a quienes les andan muy duro”. Por esto, la representante desde hace varios meses ha venido impulsando una campaña en redes sociales para recordarle a la ciudadanía los derechos que quedaron consagrados y así, ante una arbitrariedad, sepan que se debe hacer.

Por esta arbitrariedad, el Código de Policía ha tenido más de **60 demandas ante la Corte Constitucional**. Una de ellas y que fue aprobada por la Corte, fue la de declarar inexequible la norma que avala que un Policía se puede llevar a la fuerza a los habitantes de calle a los llamados centros de protección. (Le puede interesar: "[¿Qué hacer si considera injusta alguna sanción o procedimiento del código de Policía?](https://archivo.contagioradio.com/que-hacer-si-considera-injusta-alguna-sancion-o-procedimiento-del-codigo-de-policia/)")

Además, la Corte condicionó el hecho de que a los vendedores ambulantes “no se le pueden aplicar multas, ni decomisa o destruir la mercancía hasta tanto se les haya ofrecido por las autoridades competentes **programas de reubicación o alternativas de trabajo**”. También, se reguló, a través de demanda, que la Policía no se puede llevar a personas a los centros de protección si una orden judicial pues, en 2017, se efectuaron 5,673 traslados sin autorización judicial.

Finalmente, la representante afirmó durante la audiencia pública que “el código de Policía **no promueve la convivencia**, afecta los derechos de los pobres y es un negocio por las multas”. Además, recordó que la labor de los Policía es muy importante pero el nuevo Código “es una herramienta que vulnera los derechos”.

<iframe id="audio_23479757" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23479757_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
