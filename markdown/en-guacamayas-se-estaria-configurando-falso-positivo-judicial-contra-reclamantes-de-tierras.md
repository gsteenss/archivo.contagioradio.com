Title: En Guacamayas se estaría configurando falso positivo judicial contra reclamantes de tierras
Date: 2019-11-29 18:06
Author: CtgAdm
Category: DDHH
Tags: despojo de tierras, Guacamayas, Restitución de tierras
Slug: en-guacamayas-se-estaria-configurando-falso-positivo-judicial-contra-reclamantes-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/4639061927_0363a38865_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Flickr/ Tiago Brandão] 

A raíz de la denuncia realizada desde el pasado 27 de noviembre, que alerta sobre las capturas que se han realizado contra nueve líderes reclamantes de tierras de la zona rural de Guacamayas, en Turbo, Antioquia,  organizaciones sociales advierten que podría tratarse de un falso positivo judicial orquestado por empresarios y administradores de las fincas restituidas, en retaliación de la sentencia que favoreció a los campesinos, dueños legítimos de los predios.

Tras conocerse la noticia, tres de los casos fueron asumidos por la Fundación Forjando Futuros, mientras otro fue asumido por el **Instituto Popular de Capacitación (IPC),** sin embargo, en  la audiencia el juez dictaminó aseguramiento mural para los líderes; ante el fallo, defensores de DD.HH. y organizaciones sociales solicitaron una revocatoria que no ha sido concedida.

Una de las lideresas de la organización Tierra de paz, señala que dentro de los líderes capturados, tres de ellos tienen una avanzada edad y un estado de salud frágil. Tras el fallo del juez Saulo David, uno de los líderes afectados tuvo que ser hospitalizado por un desmayo que le produjo la decisión. [(Le puede interesar: Denuncian captura de ocho líderes reclamantes de tierras en Guacamayas en el Urabá) ](https://archivo.contagioradio.com/denuncian-captura-de-ocho-lideres-reclamantes-de-tierras-en-guacamayas-en-el-uraba/)

**"Estamos preocupados, no es justo que campesinos que tienen su tierra legal los dejen en la cárcel solo porque nosotros no movemos plata para pagarle a las instituciones"**, manifestó la lideresa, agregando que en el territorio han sido asesinados 21 reclamantes y que quienes están detrás de este accionar son los herederos de alias Don Mario en el territorio. [(Le recomendamos leer: Paramilitares arremeten de nuevo contra reclamantes de Las Guacamayas) ](https://archivo.contagioradio.com/paramilitares-arremeten-nuevo-reclamantes-las-guacamayas/)

### "Intentan por todos los medios una retaliación contra los reclamantes de tierras" 

Desde el IPC, su directora Nelly Osorno afirma que se trata de "una acción orquestada por empresarios y administradores de las fincas restituidas en retaliación de la sentencia de la Corte Suprema de Justicia", la que en el  diciembre de 2018 ordenó restituir varios de estos predios, cuya entrega material se hizo en el mes de mayo y que antes de ser regresadas a sus legítimos dueños estaban bajo el poder de la firma Inversiones e Inmobiliaria ASA LTDA.

**"Nos preocupa que se esté configurando un falso positivo judicial"** resalta Osorno, quien agrega que se está desconociendo que dichos campesinos fueron víctimas del conflicto, al desconocer el contexto que han vivido estas familias tras una década de lucha por sus tierras. [(Lea también: Corte Constitucional ordena restitución de tierras a 27 familias en Guacamayas)](https://archivo.contagioradio.com/corte-constitucional-ordena-restitucion-de-tierras-a-27-campesinos-en-guacamayas/)

La directora del IPC advierte que lo preocupante de este es caso es que no solo podría afectar a la vereda Guacamayas sino que buscaría "hacer desistir a los campesinos de hacer reclamación de tierras, una estrategia para desligitimar su proceso".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
