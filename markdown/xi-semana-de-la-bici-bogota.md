Title: Llega la XI versión de la Semana de la Bici en Bogotá
Date: 2018-09-19 12:23
Author: AdminContagio
Category: Ambiente, Otra Mirada
Tags: Bicicleta, Bogotá, Cultura, Deporte
Slug: xi-semana-de-la-bici-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/2bici.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Plan Bici]

###### [19 Sept 2018]

Del 22 al 30 de Septiembre se llevará a cabo la **Semana de la Bici,** un espacio deportivo y cultural a favor de la utilización de este medio alternativo de transporte, que busca reducir la congestión vial y promocionar la bicicleta como una forma ecológica de moverse por la ciudad.

La celebración tendrá diversos momentos en los que podrá participar cualquiera que tenga una bicicleta. Entre las distintas actividades se encuentran **talleres interactivos con enfoque poblaciónal, carreras de obstáculos y picnics.**

Conozca toda la información del [evento aquí](https://www.planbici.gov.co/)

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).] 
