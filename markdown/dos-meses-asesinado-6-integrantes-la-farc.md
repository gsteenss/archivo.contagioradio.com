Title: En dos meses han asesinado a 6 integrantes de la FARC
Date: 2018-02-11 10:34
Category: DDHH, Nacional
Tags: Clan del Golfo, FARC, Víctor Sánchez
Slug: dos-meses-asesinado-6-integrantes-la-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Conferencias-FARC-e1474397780364.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [11 Feb 2018]

A través de un comunicado de prensa, el partido político FARC, informó el que ayer 10 de febrero, hacía las 9 de la mañana fue asesinado Víctor Alonso Sánchez, miembro activo de esta organización y ex combatiente, en el municipio del Bagre, Antioquia, a manos del grupo paramilitar El Clan del Golfo. **Con Víctor Alfonso Sánchez son 6 los integrantes de esta organización asesinados en los dos primero meses del año.**

El ETCR Juan Carlos Castañeda manifestó que **"rechaza y denuncia la campaña de exterminio que se viene desarrollando en todo el territorio nacional** contra los dirigentes de organizaciones sociales que promueven la implementación de los acuerdos de Paz y contra los miembros de nuestro movimiento político Fuerza Alternativa Revolucionaria del Común". (Le puede interesar: ["FARC denuncia el asesinato de 3 de sus integrantes en Nariño")](https://archivo.contagioradio.com/farc-afirma-posible-asesinato-de-3-integrantes-por-estructura-del-eln/)

En el comunicado de prensa la FARC señaló que estas acciones violentas hacia los integrantes de su partido evidencia la agudización de la situación de seguridad de las comunidades y de sus ex combatientes en los territorios del Bajo Cuca y el Nordeste Atioqueño.

El pasado 9 de febrero, la FARC también dio a conocer su decisión sobre suspender temporalmente su campaña política en los diferentes departamentos del país, debido a la falta de garantías para el ejercicio de la política y las estrategias de estigmatización por parte de sectores de corrientes de derecha que incentivan al odio. (Le puede interesar: ["Hay un plan para impedir nuestras participación política: FARC"](https://archivo.contagioradio.com/hay-un-plan-coordinado-para-impedir-participacion-politica-farc/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
