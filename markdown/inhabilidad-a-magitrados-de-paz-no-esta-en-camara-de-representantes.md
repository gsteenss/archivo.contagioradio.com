Title: Inhabilidad a Magistrados de Paz no aparece en Cámara de Representantes
Date: 2017-11-21 15:13
Category: Nacional, Paz
Tags: acuerdos de paz, Jurisdicción Espacial de Paz
Slug: inhabilidad-a-magitrados-de-paz-no-esta-en-camara-de-representantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ausentismo-camara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [21 Nov 2017] 

Llegó la ponencia sobre la Jurisdicción Especial para la Paz a la Cámara de Representantes, **proyecto en donde se cambió el punto de las inhabilidades a los integrantes del Tribunal de Paz**, debido a que como ya lo había señalado la Secretaría Ejecutiva de la Jurisdicción Especial para la Paz, es incostitucional y demuestra una clara estigmatización hacia los defensores de derechos humanos en el país.

De acuerdo con la representante a la Cámara Ángela María Robledo, de las Alianza Verde, lo que si trae la ponencia es un **conjunto de impedimentos para que los Magistrados no puedan participar en procesos en los que previamente hayan trabajado** o estén involucrados.

“Esperemos que estos debates se den sin marrullerías, **sin operación reglamento como la que nos ha aplicado el presidente de la Cámara, Rodrigo Lara**, sino que fluya y que en la conciliación, no se reviva esta inhabilidad” afirmó Robledo y agregó que en caso de que esto suceda, será la Corte Constitucional la que tome la decisión sobre los Magistrados del Tribunal de Paz. (Le puede interesar:["Inhabilidad a Magistrados es Inconstitucional: Claudia Vacca"](https://archivo.contagioradio.com/incostitucional-comite-escogencia-jep/))

### **La Conciliación podría salvar Tribunal de paz ** 

Una vez se dé la aprobación en plenaria de Cámara del proyecto sobre la JEP, se deberá pasar al escenario de conciliación en donde una comisión tanto de senado como de cámara, **revisaran ambos proyectos de ley y presentarán el articulado final de la JEP**. Este escenario para Ángela Robledo, es de igual forma un pulso político.

“Es difícil anticipar que va a pasar, lo que yo creo es que incluso Cambio Radical, de donde salió esta propuesta, y partido que extrañamente hablan de la limpieza y transparencia de los jueces, **cuando tienen en sus listas gobernadores asesinos, congresistas involucrados en la parapolítica**, seguramente lo que querían con esto era dejar un precedente” afirmó la representante.

### **Los terceros y su responsabilidad** 

Frente al acogimiento de los terceros a la JEP, la representante manifestó que ve con dificultad que se vaya a quitar la posibilidad de que el sometimiento al Tribunal de Paz sea voluntario, además Robledo señaló que tanto el **Centro Democrático como Cambio Radical, quienes mayor fuerza hicieron por estos cambios “están llorando por un ojo**”.

“Acá hay una práctica de protección e encumbramiento a quienes han participado de manera decisiva, desde la sociedad civil, o desde los funcionarios públicos en el conflicto armado o la financiación de grupos” expresó Robledo  y manifestó **que actualmente hay más de 17 mil demandas contra terceros que no se han movido**. (Le puede interesar: ["Corte Constitucional todavía puede corregir el fallo de la JEP: Alberto Yepes"](https://archivo.contagioradio.com/corte_constitucional_fallo_jep/))

<iframe id="audio_22207600" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22207600_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
