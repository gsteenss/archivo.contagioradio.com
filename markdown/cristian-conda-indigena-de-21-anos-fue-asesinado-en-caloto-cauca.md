Title: Cristian Conda, indígena de 21 años fue asesinado en Caloto, Cauca
Date: 2020-05-24 12:22
Author: CtgAdm
Category: Actualidad, DDHH
Tags: asesinato, resguardo indigena, violencia
Slug: cristian-conda-indigena-de-21-anos-fue-asesinado-en-caloto-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/7c08d1d7-3744-4b87-a146-2249347415c6.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Jorge-Sánchez-CRIC-mp3.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Consejo Regional Indígena del Cauca, denunció en las últimas horas el asesinato del indígena **Cristian Conda Conda** de 21 años, integrante del resguardo López Adentro, en el municipio de Caloto [Cauca](https://www.justiciaypazcolombia.com/asi-tratan-los-funcionarios-del-gobierno-nacional-a-los-indigenas-del-cauca/).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/jorgeeliecersa/status/1264392237942792193","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/jorgeeliecersa/status/1264392237942792193

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según **Jorge Sánchez, coordinador político del[CRIC](https://archivo.contagioradio.com/50-anos-de-promesas-fallidas-a-los-embera/)**, los hechos se presentaron sobre las 5:15 pm del 23 de mayo en la vereda la cancha de la vereda Guamito, cuando 10 hombre armados ingresaron al territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*" Los hombre fuertemente armados ingresaron en una camioneta gris oscura de platón, **luego disparando indiscriminadamente dando muerte al joven Cristian Conda con múltiples disparos de arma de fuego de corto y largo alcance**"*, señaló Sánchez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que los hombres armados huyeron hacia la vía que conduce a Caloto, luego de observar la reacción de la comunidad y la [Guardia Indígena](https://archivo.contagioradio.com/el-racismo-permanece-en-las-instituciones-del-estado-cric/), quienes aseguraron el área donde sucedieron los hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Coordinador, **Cristian Conde fue integrante del Cabildo de López Adentro** en el año 2018, apoyó la actualización del plan de vida de territorio, y en a actualidad realizaba estudios en Cali, como tecnólogo en sistemas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*" Esos hechos desarmonizan la vida y el territorio, son constantes las amenazas que lleva la comunidad en temor y zozobra por el accionar violento de los grupos armados de derecha e izquierda"* , señaló el CRIC, y agregó qué rechazan la muerte de sus comuneros y exigen respeto a la vida así como un llamado a la protección y al acompañamiento para la garantía de la vida en el territorio..

<!-- /wp:paragraph -->

<!-- wp:audio -->

<figure class="wp-block-audio">
<audio controls src="blob:https://archivo.contagioradio.com/d2f87e15-44e6-4b9c-894a-7f6a52f1d346">
</audio>
  

<figcaption>
*Escuche la entrevista completa con Jorge Sánchez*

</figcaption>
</figure>
<!-- /wp:audio -->
