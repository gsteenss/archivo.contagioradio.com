Title: Así afectaría explotación petrolera a Caño Cristales
Date: 2016-04-14 18:18
Category: Ambiente, Entrevistas
Tags: Explotación petrolera, Hupecol, La Macarena
Slug: presumo-que-hubo-torciditos-para-lograr-esa-licencia-lamacarena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Cristales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caño Cristales ] 

###### [14 Abril 2016]

Quien diga que la explotación petrolera en La Macarena no va a afectar el ecosistema "es totalmente ignorante", asegura el ingeniero de petróleos Oscar Vanegas e insiste en que desconocer todos los impactos generados por la industria petrolera en distintas partes del mundo, como la ANLA lo está haciendo, puede llevar a **"un crimen, no sólo con la región, sino con el país y con el mundo"**, teniendo en cuenta que esta región ha sido declarada como patrimonio de la humanidad.

###  Interconexión del ecosistema 

De acuerdo con Vanegas, pese a que el director de la ANLA manifieste que no se afectará el ecosistema, por la interconexión de los Parques Naturales, las **exploración de los 150 pozos petroleros impactarían negativamente los humedales, caños y morichales**, que por el arrastre de sedimentos y químicos cancerígenos se contaminarían y provocarían la muerte de especies animales y vegetales endémicas.

Según el ingeniero, para la producción de cada barril de petróleo se utilizan 30 barriles de agua, y teniendo en cuenta que la recarga hídrica de los yacimientos se da tanto a nivel de la superficie como del subsuelo, y que no necesariamente se genera a través de los cuerpos de agua más cercanos, los impactos de la **explotación petrolífera en La Macarena saldrían de lo que se denomina bloque para impactar Caño Cristales**, por la conectividad de la serranía con los yacimientos.

Sí se tiene en cuenta que los yacimientos de Caño Limón se recargan con aguas del Nevado del Cocuy, pese a que entre uno y otro están a más de 500 km de distancia, del mismo modo en que los de Rubiales se recargan con las aguas del río Guaviare aún cuando hay 150 km que los distancian, pensar en que la explotación en La Macarena no pueda llegar a afectar las aguas de Caño Cristales, resulta erróneo, cuando según la ANLA las separan 68 km.

### Contaminación hídrica 

El agua residual va a ser vertida o reinyectada en formaciones poco profundas como caños y ríos cuyas aguas se contaminarán con residuos de crudo, pues **en ninguna parte del mundo la industria del petróleo ha logrado tratar 100% el agua**, y mucho más cuando se trata de un crudo pesado como el que se encuentra en esta zona que por su alto contenido de azufre y otros elementos químicos cancerígenos, provocaría mutaciones en seres vivos acuáticos, extinción de algunas especies y migración de otras, según explica Vanegas.

### Aumento poblacional 

El ingeniero agrega que la creación de vías de acceso para la **entrada de maquinaría pesada y la migración poblacional** para la implementación de la licencia, generarían cambios drásticos en el hábitat de la región, pues según se ha evidenciado en otras zonas petroleras como Castilla, Tauramena y Aguazul, la población se ha multiplicado por 10 en menos de una década, generando una **demanda masiva de recursos naturales**, que implicaría la caza, deforestación, siembra y establecimiento de ganado en regiones de reserva ambiental.

"Esta licencia ambiental es irregular por no decir que ilegal", afirma Vanegas, refiriéndose a las inconsistencias que se han presentado en su aprobación, pues **hace dos años la ANLA la había negado y resulta "raro" que ahora acepte**, "no me atrevo a decir que sea corrupción, que hubo manipulación o soborno, porque no me consta, pero hubo funcionarios del Gobierno nacional que me pusieron precio y me sugirieron que no volviera a La Macarena, ni hiciera resistencia porque Hupecol necesitaba entrar a la zona", agrega.

Según el ingeniero en la página de la petrolera en junio del año pasado, se indicaba a los accionistas que no debían preocuparse porque la salvación de la empresa estaba en el bloque serranía y que las reservas petroleras eran equivalentes a las de Rubiales, **"presumo que hubo torciditos para lograr esa licencia"**.

Además de esta inconsistencia, Vanegas asegura que no se explica porqué **la licencia de la ANLA es aprobada para San Vicente del Cagúan**, Caqueta, pero el contrato firmado con Hupecol dice que el bloque estaría en La Macarena y en La Uribe, Meta. En el documento también se específica que la petrolera puede disponer de todo el petróleo y Colombia **deberá pagarlo a precio internacional y en boca de pozo**, en ese orden de ideas "saldría más barato importar combustible y dejar el ecosistema quieto", como indica el ingeniero.

Vanegas concluye asegurando que la lucha por la reversión de la licencia no es sólo de las comunidades de La Macarena, sino que debe vincularse la **sociedad colombiana y la comunidad internacional**, pues este ecosistema además de ser estratégico, es considerado uno de los diez lugares más hermosos del planeta.

Vea también: [[Empresa petrolera teñiría de negro las aguas de Caño Cristales](http://Empresa%20petrolera%20teñiría%20de%20negro%20las%20aguas%20de%20caño%20cristales)]

<iframe src="http://co.ivoox.com/es/player_ej_11169200_2_1.html?data=kpaemJ6WdJGhhpywj5abaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncbDnxMbfjbvFssbbwtiajbXWs8fZ1NTfjarctMbm1dSYqs7IttDXwtfP19fTt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
