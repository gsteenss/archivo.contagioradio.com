Title: La paz se construye desde ”el sentir y el pensar de cada persona”
Date: 2015-10-08 12:17
Category: Nacional, Paz
Tags: Cese al fuego, CRIC, Movimiento indígena campesino, Movimiento indígena colombiano, ONIC, proceso de paz, Radio derechos Humanos
Slug: la-paz-se-construye-desde-el-sentir-y-el-pensar-de-cada-persona
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/nasa-e1444325836872.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:radiomacondo.com 

###### [8 Oct 2015] 

“**Cuenten con nosotros para la paz, no para la guerra**” es un corto-documental, realizado por la Consejería de Cabildos indígenas del Norte del Cauca (ACIN) y el Consejo Regional de Indígenas del Cauca (CRIC), donde se **recogen distintas voces del pueblo Nasa, en representación de los pueblos indígenas de Colombia **sobre lo que significada para ellos vivir en medio de la guerra.

El sólido argumento de los pueblos originarios es que “50 años de guerra, aportando muertos, son suficientes para sentarse y construir la paz” y para esto **la cimentan con modelos participativos** que van desde abajo, hacia arriba, en donde el territorio, la salud, la educación, unos hijos sanos, la comida, el trabajo comunitario, son puntos de partida para su construcción.

Las comunidades que nunca han esperado la guerra, han sido vulnerados en su persona, en la de sus niños, mujeres y ancianos durante el conflicto armado. **Cuando la guerra llega a sus territorios “impacta y afecta sin medir condiciones”**, perjudicando principalmente a las mujeres,  quienes se convierten en viudas, huérfanas y desplazadas, abandonando sus tradiciones y “no pueden seguir dando vida allí”.

El mensaje de las comunidades representadas en ”**Cuenten con nosotros para la paz no para la guerra**”, es que la paz no se consigue solo con declarar un cese al fuego, sino que desde sus propios territorios, desde la familia y **desde ”el sentir y el pensar de cada persona” hay que construirla**.

https://www.youtube.com/watch?v=ZstRiJsg49A
