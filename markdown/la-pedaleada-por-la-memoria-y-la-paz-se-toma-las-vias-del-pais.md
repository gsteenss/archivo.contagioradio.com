Title: Pedaleada por la Memoria y la paz se toma las vías del país
Date: 2017-06-30 18:08
Category: DDHH, Nacional
Tags: Comisión de Justicia y Paz, Pedaleada por la paz
Slug: la-pedaleada-por-la-memoria-y-la-paz-se-toma-las-vias-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/pedaleada-popayan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Justicia y Paz] 

###### [30 Jun 2017] 

Desde el 30 de junio arrancó la pedaleada por la paz, una actividad que hace parte del Festival de **Memoria Soy Paz**, que tiene como finalidad que jóvenes de diferentes regiones de la Colombia rural y urbana se encuentren para **compartir sus experiencias y construcciones de paz desde la palabra, la danza, la música, el teatro, el deporte, la pintura y la poesía.**

La pedaleada partirá desde Popayán y culminará en Palestina, Huila y en su recorrido tendrá varios puntos de concentración para llevar a cabo diferentes actividades. El 30 de junio la pedaleada llegará hasta Pitalito, Huila, en donde **se realizará un encuentro familiar y comunitario con la ciudadanía**. Le puede interesar: ["Comunidades del país empiezan a vivir el sueño de paz"](https://archivo.contagioradio.com/comunidades-del-pais-empiezan-a-vivir-el-sueno-de-la-paz/)

El primero de Julio se desarrollará la segunda etapa denominada “Resignificación de la memoria, forma y sentido de la vida”, en ese espacio se llevará a cabo el encuentro juvenil, el Festival cultural por la memoria y la paz; el conversatorio **“Paz, Acuerdos, Implementación y Retos” y culminará con una actividad a cargo de los jóvenes.**

El dos de Julio iniciará la segunda etapa de la pedaleada que saldrá desde Pitalito, Huila hasta Palestina, en el mismo departamento y el punto de encuentro será la Zona de Biodiversidad La Esperanza. **Allí se realizará el conversatorio Mecanismos de protección Ambiental: el buen existir en el territorio**.

La jornada finalizará el lunes tres de julio con un balance final de la actividad y el regreso de los participantes a sus regiones. De acuerdo con la defensora de derechos humanos e integrante de la Comisión de Justicia y Paz, Viviana Martínez “**es una apuesta por la contrucción de seguir soñando y aportarle a la paz**, además de tener una mirada a la memoria, lo que significa la memoria para realmente alcanzar la paz y sobre todo desde la mirada de los jóvenes”.

<iframe id="audio_19567313" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19567313_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
