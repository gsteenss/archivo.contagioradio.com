Title: Gobierno incumple a campesinos e indígenas del sur de Córdoba
Date: 2016-11-03 11:38
Category: Movilización, Nacional
Tags: campesinos, Gobierno Nacional, Montería, Movilización, paz
Slug: gobierno-incumple-a-campesinos-e-indigenas-del-sur-de-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/movimienot-campesino-e1478190781832.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Villano Radio] 

###### [4 Nov. de 2016] 

**Más de 500 personas, entre campesinos e indígenas habitantes de sur del departamento de Córdoba se cansaron de los incumplimientos del gobierno nacional y departamental que habían sido adquiridos desde el año  2011** por lo que decidieron tomarse la carretera que conduce de Montelíbano a Puerto Libertador durante más de 3 horas. Se espera que en horas de la tarde se lleve a cabo una reunión con autoridades del departamento para atender las exigencias en salud, educación e infraestructura.

Según Francisco Osuna, vocero de la Asociación de Campesinos del Sur de Córdoba ASCSOCUR, **son varios los problemas que atraviesan estas comunidades y que son importantes para el desarrollo efectivo de sus vidas.** El vocero asegura que el tema de educación, que a falta de profesores ha provocado una desescolarización cercana al 25%, preocupa a sus habitantes, sumado al mínimo cupo de alimentación escolar, que aseguran debe ser aumentado en por lo menos 1000 personas por municipio para cubrir la demanda. Adicionalmente, el pueblo Zenú está exigiendo su derecho a la etnoeduación que hasta el momento no ha sido respetado por el Gobierno nacional.

Otros puntos como el mejoramiento de vivienda, infraestructura deportiva, saneamiento básico, agua potable y desarrollo para el cuidado del ambiente, son también claves para superar la crisis que vive la región, asegura el vocero.

**El pliego de exigencias dado a conocer, contempla de igual manera la titulación de tierras y el respeto por los Derechos Humanos,** debido a que son 3 los municipios donde se han presentado allanamientos ilegales por parte de la Fuerza Pública contra 7 casas de los pobladores de la región.

Osuna asegura, que este jueves se realizará una reunión con autoridades departamentales en Montería para intentar concertar una salida a la aguda crisis que viven, sin embargo, reafirma que **las comunidades seguirán movilizadas hasta que se firmen los acuerdos y se establezcan las rutas de solución y agrega que la movilización y las manifestaciones “dependen de las respuestas” que se obtengan en ese escenario de discusión. **Le puede interesar: [15 mil mineros informales en paro indefinido por incumplimientos del gobierno](https://archivo.contagioradio.com/15-mil-mineros-en-paro-indefinido-por-incumplimientos-del-gobierno/)

<iframe id="audio_13600621" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13600621_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
