Title: Desplazamiento forzado instrumento del desarrollo
Date: 2015-04-15 17:38
Author: CtgAdm
Category: invitado, Opinion
Tags: Desalojo, desalojo forzoso, Desplazamiento HidroItuango, ESMAD, Hidroituango, Isabel Cristina Zuleta, Movimiento Ríos Vivos
Slug: desplazamiento-forzado-instrumento-del-desarrollo
Status: published

**Por [[Isabel Cristina Zuleta] @[ISAZULETA]{.u-linkComplex-target}](https://archivo.contagioradio.com/category/isabel-cristina/)**

Ahí en medio de la noche y por fin el silencio. Un día de gritos, de rabia, de desesperación, de angustia, de impotencia había terminado. Eran las 12 de la noche del 27 de marzo de 2015 en el Puente Pescadero, el mismo desde el cual arrojaron a tantos seres queridos que se los llevo el río, el mismo que será inundado por Hidroituango, el mismo en el que se posan los turistas del desarrollo.

Estábamos allí, en la vía que conduce hacia Ituango, 81 personas desoladas sin saber que dirección tomar, los niños dormían, los hombres también. Miraba en el río el reflejo de la luna que nos acompañaba sin dejar de escuchar la voz de mi amiga Eugenia que repetía entre lágrimas “*me siento igual que cuando me sacaron los paramilitares del Aro, Hidroituango es igual a los paramilitares, así* *llegaban nos daban un tiempo para salir, Hidroituango da menos tiempo, los paramilitares avisaban de un día para otro, así* *quedábamos a la intemperie, escondidos en el monte*” aun me da vueltas en la cabeza la indignación de saber cómo los funcionarios intentaron arrancar a los niños de sus familias, les ofrecieron otros padres, porque los barequeros libres no son dignos ni de tener hijos, juguetes y un futuro porque las orillas de los ríos dejaran de existir.

Algunos de los desplazados empezaron a exponer sus heridas adquiridas al correr cuando escucharon a la inspectora de Policía de Toledo dar la orden al Mayor Uribe ¡Proceda!, otros sus dolores por el gran esfuerzo al tratar de salvar la mayor parte de sus pertenencias. Pero la gran herida estaba ahí entre las lágrimas, la mirada perdida y los suspiros, perder la forma de vida, la fuente de sustento, el territorio por “algo” que llaman desarrollo.

Desde muy temprano en la mañana nos sentimos rodeados de Policía fuertemente armada que requisaron a algunos y preguntaron por los líderes. El Mayor y la Inspectora iniciaron la diligencia afirmando “Vinimos a desplazarlos” “Saquen los niños dejen que bienestar familiar se los lleve, no tienen porque ver lo que va a pasar aquí”. Empezó a llegar el ESMAD y hombres de la Vigilancia Privada de Empresas Públicas de Medellín, empresa que solicita el desalojo del río y afirma que el Cauca le pertenece.

Un Policía intento tomarse fotos con una de las niñas, otro saco su arma amenazando a los perros de una de las familias, otro interrogo a una joven de 15 años tratando de sacar información sobre los líderes. Todo esto mientras la sentencia avanzaba, la Inspectora determinó tres horas para desocupar y la delegada de la procuraduría salvaba a la Inspectora ante las crecientes preguntas que no pudo responder.

La preocupación por las arenas con oro, los enseres personales, las gallinas, los conejos, la yuca, la vida que no era posible empacar en un costal.  La Inspectora insistió en que se comprometía a hacer llegar los enseres a los diferentes lugares (sobre todo los de las mujeres solas que no eran capaces de cargarlos) poco después de salir supimos que el ESMAD lo habían destruido todo incluidos los enseres empacados como lo había indicado la Inspectora.

La procuradora hablaba de la libertad de elegir el lugar de destino y de las garantías que tendrían los desalojados si salían hasta el Puente Pescadero, todo se resolvería en el Puente las dudas, solicitudes, el acta, estaba la Defensoría, la Unidad de Víctimas una ambulancia y la comunidad debía agradecer.

Pasadas las tres horas comenzó el desastre, la Inspectora huía ante las preguntas por lo que pasaría con la Casa de la Memoria, con las pertenencias y el oro en las arenas, rápidamente dio la orden ¡Procedan! atendiendo por fin a la presión, casi persecución de los delegados de la Gobernación de Antioquia que no se separaron de ella ni un momento, ante cada pregunta le susurraban al oído y desgastaban su camisa.

El operativo del desalojo forzoso fue claramente orquestado y dirigido desde la Gobernación, los funcionarios locales fueron simples instrumentos de Santiago Londoño quien en la Cumbre Agraria se había negado a realizar una visita de verificación por los garantes de este espacio de dialogo al que pertenecen los mismos desplazados. ¿Cómo estar en una mesa de dialogo cuando con los que dialogas te quitan tu forma de vida? es la pregunta que muchos de los integrantes de Ríos Vivos se hacen hoy.

La Policía empezó por las fotos de los seres queridos desaparecidos y asesinados, las arranco de las paredes de plástico de la Casa de la Memoria, con ello acababa toda dignidad posible, la de los muertos y la de los vivos que aun los lloran.

Las mujeres fuimos las ultimas en salir, cada vez el cerco se cerraba más, a cada paso del ESMAD nos quitaban espacio para respirar. En el Puente las soluciones se tradujeron en humillaciones, el acta se pretendió realizar en los campamentos de EPM, La unidad de víctimas solucionó invitando a acercarse a las personerías, la ambulancia no estaba, no hubo ni libertad para buscar un destino común y la posibilidad de alimentos estaba condicionada a dirigirse hacia el lugar destinado por los funcionarios. La misma procuradora amenazó con el ESMAD si se tomaban decisiones colectivas ante la situación.

Pasamos de la orilla del hermoso río Cauca, en casas humildes pero acogedoras con la alegría del trabajo diario y seguro, de la libertad de hacer lo que se desee; a la orilla de una carretera en la intemperie con la incertidumbre como única certeza y el silencio como parte del alivio.
