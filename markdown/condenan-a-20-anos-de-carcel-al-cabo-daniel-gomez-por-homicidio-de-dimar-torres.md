Title: Condenan a 20 años de cárcel al cabo Daniel Gómez por homicidio de Dimar Torres
Date: 2019-11-28 12:50
Author: CtgAdm
Category: DDHH, Judicial
Tags: asesinato de excombatientes, Dimar Torres, Ejecuciones Extrajudiciales, Ejército Nacional
Slug: condenan-a-20-anos-de-carcel-al-cabo-daniel-gomez-por-homicidio-de-dimar-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Dimar-Torres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cortesía] 

El juzgado Segundo Penal del Circuito Especializado de Cúcuta, Norte de Santandér, condenó a 20 años de cárcel **Daniel Eduardo Gómez Robledo**, cabo del Ejército Nacional por el homicidio del excombatiente de las FARC, Dimar Torres Arévalo en eventos que denunció la comunidad de Convención, Norte de Santander el pasado 22 de abril, día en que ocurrieron los hechos.

El cabo Gómez Robledo, quien llegó a un preacuerdo con la Fiscalía, reveló durante la audiencia de imputación de cargos que tenía ordenes de realizar labores de seguimiento al campesino, tarea que realizó durante 15 días. Así mismo indicó que fue en el cumplimiento de esta función que interceptó y ordenó a Dimar detener su motocicleta de regreso a la vereda Campo Alegre para revisar sus objetos personales, suceso que desencadenaría en el asesinato del excombatiente.  [(Lea también: Militar implicado señala que Ejército ordenó seguir a Dimar Torres)](https://archivo.contagioradio.com/militar-implicado-senala-que-ejercito-ordeno-seguir-a-dimar-torres/)

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw) Unidad Especial Investigación contra Organizaciones Criminales [\#Fiscalía](https://twitter.com/hashtag/Fiscal%C3%ADa?src=hash&ref_src=twsrc%5Etfw) obtuvo condena de 20 años de cárcel contra cabo Daniel E. Gómez R. por homicidio de excombatiente Dimar Torres. Juez [\#Cúcuta](https://twitter.com/hashtag/C%C3%BAcuta?src=hash&ref_src=twsrc%5Etfw) avaló preacuerdo y negó prisión domiciliaria y libertad condicional.
>
> — Fiscalía Colombia (@FiscaliaCol) [November 28, 2019](https://twitter.com/FiscaliaCol/status/1200088389434904576?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### ¿Quien dio la orden en el caso Dimar Torres? 

El fallo se da a conocer semanas después de la salida de Guillermo Botero del Ministerio de Defensa,  quien mientras estuvo al frente de estar cartera, sostuvo que la muerte de Dimar se dio en medio de un forcejeo, versión que desvirtuaba los testimonios de la comunidad y que luego fue desmentida. [(Le recomendamos leer: General Villegas reconoce asesinato de Dimar Torres mientras MinDefensa pretende ocultarlo)](https://archivo.contagioradio.com/general-villegas-reconoce-asesinato-de-dimar-torres-mientras-mindefensa-pretende-ocultarlo/)

Con la condena al cabo Gómez Robledo no concluye el caso, pues a través de pruebas recolectadas por la Fiscalía también se determinó que **el coronel Jorge Armando Pérez, conocía del plan para asesinar a Dimar** y sería el autor intelectual del homicidio por lo que el ente de control anunció imputaría cargos en su contra.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
