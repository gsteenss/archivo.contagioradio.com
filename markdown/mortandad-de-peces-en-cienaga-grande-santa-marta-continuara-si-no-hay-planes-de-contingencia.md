Title: Miles de peces mueren en la Ciénaga Grande Santa Marta
Date: 2016-08-09 11:32
Category: Ambiente
Tags: Ciénaga grande de Santa Marta, mortandad de peces
Slug: mortandad-de-peces-en-cienaga-grande-santa-marta-continuara-si-no-hay-planes-de-contingencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Ciénaga-Grande.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pescadores Ciénaga Grande 

###### [8 Ago 2016] 

**Miles de peces amanecieron muertos el pasado viernes en el complejo de Pajarales, Ciénaga Grande de Santa Marta,** debido a la falta de agua dulce que es necesaria para el ecosistema en el que viven los peces, y que se produce debido a la conjunción del fenómeno del niño y demás afectaciones al complejo de humedales que han ocasionado la contaminación de las aguas y la falta de oxígeno en las mismas.

Una situación de la que ya venía advirtiendo la profesora Sandra Vilardy, junto a un  grupo de defensores del Ambiente. De acuerdo con la docente, aún no hay una cifra exacta sobre la gran cantidad de peces que murieron, aunque no se trata de una situación nueva, pues ha venido siendo algo sistemático debido a la **falta de implementación de planes de contingencia  por parte del Comité Departamental de Gestión de riesgos.**

Este complejo de humandales viene deteriorándose, debido al bajo nivel de las aguas del Río Magdalena, la implementación de distritos de riego destinados a monocultivos de palma y banano, los efectos de la construcción de la Vía de la Prosperidad y a la ineficiencia de las obras hidráulicas de la vía entre Barranquilla y Santa Marta. Actividades nocivas para el ecosistema de la ciénaga que han generado  que en tan solo 10 años la producción de peces ha bajado de 27 mil toneladas anuales a 1725 toneladas anuales, lo que significa **una reducción de más del 90% en la producción pesquera, sustento de más de 5000 familias** de la zona pero que realmente afecta directa e indirectamente a cerca de 20.000 pobladores.

Ante esta problemática, se espera la instalación de una mesa de crisis, teniendo en cuenta que los peces son el recurso económico y alimenticio de los habitantes de ese sector, es decir que **se trata de un "problema ecológico, ambiental y social"**, como lo expresa la profesora Vilardy, quien propone que se generen empleos de emergencia para las personas afectadas por la mortandad de peces.

**"Esto lamentablemente va a seguir ocurriendo**, hay mucha materia orgánica  en descomposición al interior de las ciénagas del sur lo que provoca que se siga consumiendo el oxígeno". explica la docente, quien agrega que la Ciénaga es la desembocadura del río Magdalena, el más importante de Colombia, lo que evidencia esta zona tiene una alta biodiversidad y además cumple "una función de riñón", por ello, la necesidad de proteger y conservar este complejo de humedales, que son **Reserva de Biósfera y Humedal Ramsar de importancia internacional.**

<iframe src="http://co.ivoox.com/es/player_ej_12480404_2_1.html?data=kpehmpWYdJWhhpywj5aVaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncbTVz8nfw5C6rc3V08nmh5enb7Hm0MvS1dTWpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
