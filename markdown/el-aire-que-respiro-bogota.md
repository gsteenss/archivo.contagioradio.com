Title: Con documental ciudadanos exponen su preocupación por calidad del aire en Bogotá
Date: 2018-12-13 13:32
Author: AdminContagio
Category: Ambiente, eventos
Tags: Bogotá, calidad aire, Documental
Slug: el-aire-que-respiro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/DOCUMENTAL-6_0-e1544725462844.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mesa Técnica por la calidad del aire 

###### 13 Dic 2018 

La preocupación por la baja calidad del aire en Bogotá, ha motivado a ciudadanos comunes a **emprender acciones para visibilizar las afectaciones producen en la salud y el ambiente de la capital**, lo que contrarresta con decisiones como la tomada por la administración de mantener vehículos públicos que funcionan a partir de diesel.

Una de esas iniciativas es la producción del documental **"El Aire que Respiro, Caso Bogotá"**, una pieza audiovisual realizada por la Mesa Técnica Ciudadana por la calidad del aire de la capital, producciones EnAction y Studio 93 que **se estrena oficialmente este jueves 13 de diciembre**.

El largometraje, **producido con el apoyo de la Fundación Heinrich Boll Colombia** busca sensibilizar a los espectadores sobre la situación actual de la ciudad y los factores que inciden en el deterioro de la calidad del aire; **exponiendo simultáneamente el trabajo que la ciudadanía realiza por garantizar su derecho a un ambiente sano y aun aire limpio.**

<iframe src="https://www.youtube.com/embed/Cgrf5f28pOg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

‘El Aire Que Respiro, Caso Bogotá’ se proyectará **hoy a las 6:30p.m en Fundación Heinrich Böll, Calle 37 No 15-40 Bogotá**, si desea asistir a este evento totalmente gratis debe inscribirse previamente [aquí](https://docs.google.com/forms/d/e/1FAIpQLSdaNcin4uT4S3ef4D0hLKNABpSc_o_RxU1VOx6QhE6c3pg11w/viewform?fbclid=IwAR16L08yII6NPjOdWKykhYMEArqOMc1GEMfYkqVrjkJ8A0UuxUWBP7D3cUg).
