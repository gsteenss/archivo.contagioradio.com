Title: El colegio de Tumaco que se cansó de la muerte: 21 asesinatos de estudiantes en 4 años
Date: 2019-08-01 17:24
Author: CtgAdm
Category: DDHH, Nacional
Tags: conflicto armado, nariño, Tumaco
Slug: el-colegio-de-tumaco-que-se-canso-de-la-muerte-21-asesinatos-de-estudiantes-en-4-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Colegio-Iberia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CasaTumaco] 

El pasado 31 de julio la comunidad educativa del Colegio Iberia en Tumaco se movilizó de forma pacífica en respuesta al  asesinato de estudiantes de la institución que ha ascendido a 21 víctimas en un lapso de 4 años, situación que hoy denota la situación de vulnerabilidad que denota el derecho a la educación y a la vida de los niños, niñas y adolescentes del municipio.

El más reciente de estos casos ocurrió el 22 de julio de 2019, cuando en medio de un partido de fútbol fue asesinado el estudiante de décimo Javier Adolfo Castro de 17 años, **"nuestra niñez se merece tener una educación segura, que sientan que venir al colegio no es poner la vida en riesgo"**, afirma la profesora de la institución, Nancy Arboleda.

La población ha declarado que no se tiene conocimiento sobre los responsables de esta ola de crímenes, pues pese a que hay presencia de varios grupos armados, ninguno se ha atribuido estos actos.  Según el Instituto de Estudios para el Desarrollo y la Paz, el accionar de grupos armados ilegales y el narcoparamilitarismo son los elementos que agudizan la violencia en el municipio.

En el Municipio hacen presencia además de las Fuerzas Armadas, las Guerrillas Unidas del Pacífico, la Gente del Orden, el Frente Estiven Prado, el Frente Oliver Sinisterra y el ELN, entre otros. La confrontación de algunos de estos actores causó en días

La docente expresó que la marcha en la que participaron estudiantes, padres de familia, docentes, directivos y administrativos, permitió visibilizar la situación que se vive en el sector y que no ha sido atendida, **"sentimos que nos han dejado solos con esta responsabilidad y de alguna manera es importante sentir el apoyo de instituciones"**, manifestó.

> Hay un colegio llamado Iberia donde han asesinado a 16 estudiantes en los últimos 4 años, hoy los estudiantes salieron en una marcha a pedir ayuda, pero son pobres, negros y viven en Tumaco ¿les escucharemos? ¿Tienen iguales derechos que el resto? Dale retweet por favor. [pic.twitter.com/vH1GMO29oD](https://t.co/vH1GMO29oD)
>
> — Casa Memoria Tumaco (@CasaTumaco) [July 31, 2019](https://twitter.com/CasaTumaco/status/1156674128860123136?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### El colegio Iberia en Tumaco pide acompañamiento de las autoridades 

Tras la marcha se dio inicio a una mesa de diálogo junto a la Policía, Alcaldía, el secretario de Educación y delegados de la Personería y la Defensoría con la intención de establecer unos compromisos y una comisión que acompañe a la institución.

La profesora advirtió que una reunión similar ya se había realizado en noviembre del 2018 después que Johan Hernán Riascos, estudiante de 18 años de edad fue asesinado, meses después aún no hay resultados, "en el momento se hacen los compromisos pero al pasar los días todo se queda allí". [(Le puede interesar: Tumaco: sin gobierno, sin agua y con mucha violencia)](https://archivo.contagioradio.com/tumaco-gobierno-agua-violencia/)

Dentro del pliego de peticiones, la comunidad educativa ha pedido que se reconozca que la institución que hace parte de la comuna 5 del municipio, está ubicada "en una zona de alto riesgo de conflicto", esto con el fin de otorgar mayores garantías de seguridad por parte de las autoridades en las diferentes sedes del colegio asentadas en la periferia del municipio.

> **"Ya no más indiferencia, ya no más abandono queremos que nos acompañen,"**

También se planteó que los estudiantes puedan acceder a espacios que les permitan alejarse de esta situación y participar en actividades culturales o deportivas para que también estén en otros contextos o espacios que los fortalezcan como jóvenes.

### "Nuestra niñez se merece tener una educación segura" 

Pese a que padres de familia y estudiantes han manifestado temor, también se sumaron a la marcha, "están convencidos que con el aporte de todos la situación puede mejorar, quieren salir adelante a través de la educación y ese es el sentir de toda la comunidad educativa" afirmó Nancy Arboleda.  [(Lea también: Tumaco, municipio con mayor número de líderes asesinados desde 2016)](https://archivo.contagioradio.com/cauca-narino-2/)

La docente, quien enseña ciencias sociales se refirió al trabajo que adelanta junto a sus estudiantes para hacer pedagogía, **"trabajamos muchos temas como la convivencia pacífica, la resolución de conflictos y lo importante de la hermandad, de apoyarnos como ciudadanos"** afirma la profesora quien se unió al clamor de la comunidad, "no aguantamos más no queremos ni uno más caído", concluyó.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_39460372" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39460372_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
