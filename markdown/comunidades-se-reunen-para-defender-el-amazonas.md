Title: "El Amazonas nos reclama"
Date: 2016-11-05 09:00
Category: Ambiente, Nacional
Tags: comunidades amazónicas, defensa ambiental, Foro Panamazónico
Slug: comunidades-se-reunen-para-defender-el-amazonas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/amazonas-e1478294687660.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Los.andes.com] 

###### [4 Nov de 2016] 

Del 3 al 5 de Noviembre, se desarrolla en Florencia Caquetá el Pre-Foro Panamazónico, un escenario regional que integra a 8 países de la región del Amazonas. Este escenario busca articular las propuestas de las comunidades en** defensa de este ecosistema, así como sus iniciativas para hacer frente a la minería, la tala indiscriminada y la actividad petrolera.**

Vanesa Torres, abogada integrante de Ambiente y Sociedad, una de las organizaciones convocantes, cuenta que a pesar de las dificultades de tiempo y desplazamiento, lograron congregar a **Gobernadores de los pueblos Kamentsá, Inga, Huitoto, entre otros, y a representantes de la Universidad del Amazonas.**

Los participantes trabajarán en mesas temáticas, la agenda que ha sido aprobada por las comunidades y organizaciones participantes. Algunos de los puntos neurales serán la deforestación, el **pago por servicios ambientales, la ley ZIDRES, los proyectos e impactos que las multinacionales mineras y petroleras** dejan a su paso.  
Le puede interesar: [40% del territorio colombiano está erosionado: IGAC.](https://archivo.contagioradio.com/40-del-territorio-colombiano-esta-erosionado-igac/)

Torres, llama la atención sobre la negligencia de los gobiernos municipales y el Gobierno Nacional, en cuento a la realización de las consultas previas, que “deberían dar información transparente a las comunidades sobre **las consecuencias de adoptar modelos como el pago por servicios ambientales y las explotaciones de hidrocarburos”. **Le puede interesar: [5 grandes daños de la minería a gran escala.](https://archivo.contagioradio.com/5-grandes-danos-de-la-mineria-a-gran-escala/)

La abogada y defensora ambiental, señala que si bien Caquetá, legalmente, no hace parte de la Amazonía, se escogió este lugar como escenario previo al Foro Panamazónico que se llevará a cabo el próximo año en Tarapoto Perú, porque “sus características ecosistémicas también son selváticas y además, **presenta las mismas problemáticas que los pueblos amazónicos debido a los fuertes intereses económicos”.**

Sólo para este departamento, las autoridades ambientales concedieron 93 bloques petroleros, afectando un área donde habitan 68 mil personas, **“Caquetá y el resto del Amazonas, son zonas estratégicas para la inversión extranjera”**, por eso el pre-foro busca diseñar estrategias desde los pueblos para evitar más daños a las comunidades y sus territorios.

Se hace necesaria una acción urgente, pues la empresa “Emerald Energy, comprada por la empresa china Sinochem, **tiene ya 8 bloques en el Caquetá (…) uno de ellos** **el bloque ‘El Nogal’, el más grande otorgado hasta ahora en el país”,** un precedente negativo frente al “compromiso y obligación de protección que debería asumir el Gobierno” concluye Torres.

Por último, la integrante de Ambiente y sociedad extiende la invitación a seguir el desarrollo del evento a través de **redes sociales y las plataformas de cada una de las organizaciones convocantes.**

<iframe id="audio_13619059" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13619059_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
