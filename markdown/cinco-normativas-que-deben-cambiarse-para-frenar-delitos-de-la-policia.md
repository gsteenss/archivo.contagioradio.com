Title: Cinco normativas que deben cambiarse para frenar delitos de la Policía
Date: 2020-09-11 10:50
Author: AdminContagio
Category: Nacional
Tags: Bogotá, ESMAD, exceso de fuerza pública, Policía Nacional
Slug: cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/117771279_10157430566870812_50413669622800229_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Policia Nacional / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los sucesos ocurridos desde el 9 de septiembre con el asesinato del abogado Javier Ordóñez a manos de la Policía y las posteriores manifestaciones en rechazo al suceso en diversas localidades de Bogotá y zonas del país dejando un saldo de más de 200 personas heridas y 13 personas asesinadas producto del accionar de la Fuerza Pública; han llevado a cuestionarse qué tan segura está la población civil ante la Policía Nacional y a reprochar una ausencia por parte del Gobierno e instituciones que deben velar por las y los colombianos. [(Lea también: El abogado Javier Ordóñez fue asesinado por la Policía afirman sus familiares)](https://archivo.contagioradio.com/el-abogado-javier-ordonez-fue-asesinado-por-la-policia-afirman-sus-familiares/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así las cosas son cinco los elementos que se pueden vislumbrar y que permiten orientar los cambios estructurales que deben darse para impedir que la policía siga abusando de su poder. Entre esos elementos encontramos el **Fuero Penal Militar que para muchos significa impunidad, la solidaridad de cuerpo, que la policía esté bajo jurisdicción de las FFMM y no del Ministerio del Interior como órgano civil, el código de policía que les otorgó poderes más allá de los que pueden controlar y la doctrina del enemigo interno.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No son unas cuantas manzanas podridas en la Policía

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, el representante a la Cámara, Inti Asprilla, quien durante varios años ha hecho seguimiento al abuso policial señala que existen varios componentes que permiten analizar el comportamiento de la Fuerza Pública empezando por **"una cultura de impunidad al interior de la Policía**, sistemática y organizada". [(Lea también: Están volviendo a la Policía una enemiga de los ciudadanos: Alirio Uribe Muñoz)](https://archivo.contagioradio.com/policias-enemigos-ciudadanos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Señala, que este comportamiento es avalado por varias instituciones jurídicas, en particular el **fuero penal militar** donde no existe ningún acompañamiento civil, ni una veeduría de personas formadas en derecho, algo sumado a la ausencia de una capacitación en derechos humanos hacia los nuevos uniformados, en medio de una administración que ha empoderado a la Fuerza Pública a través de "comportamientos fascistas", mientras un último factor, lo atribuye a la expedición del **Código de la Policía la expedición del código de Policía a través de la Ley 1801 de 2016** que permitió empoderar aún más a esta institución.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hay que hacer un cambio muy grande en el enfoque de la doctrina al interior de la fuerza policial" expresa Asprilla, al señalar cómo esta, aún **obedece a una ideología anterior incluso a la Guerra Fría y otras como a Solidaridad de Cuerpo**, que apela al silencio de los uniformados y pasar por alto los abusos de sus compañeros, incluso si no se está de acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Victor Correa, exrepresentante a la Cámara y médico agrega que la Fuerza Pública obedece a una función como aparato represor con el fin de "conservar un orden social que impera en medio del discurso hegemónico del país que incluye una concentración del poder, la **doctrina del enemigo interno** y una obediencia a la agenda impuesta por países como Estados Unidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde su rol como médico y con relación a la muerte del abogado Javier Ordóñez, quien fue herido en múltiples ocasiones con un taser eléctrico, advierte que promover su uso "es facilitar que los abusos tuvieran esa suerte de consecuencia" con mayores efectos nocivos sobre el corazón, ocasionando arritmias y posteriormente paros cardíacos, convirtiéndolos en elementos de tortura , tal como han advertido con anterioridad organizaciones como Amnistía Internacional, revelando **cómo se puede inflingir dolor mediante la descarga de 50.000 voltios al cuerpo sin dejar marcas relevantes ocasionando la muerte de 269 personas entre junio de 2001 y junio de 2007 en países como Canada y EE.UU.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Quién se hace responsable del accionar de la Policía?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre la responsabilidad que recae a raíz de los hechos ocurridos, Correa señala que aunque las Alcaldías tienen un rol importante, estas también tienen limitaciones pues la Policía - recuerda, - es un organismo de orden nacional, no municipal. Sin embargo, señala que estas tienen la potestad de señalar los abusos, acompañar a las víctimas y poner el debate del exceso de la Fuerza en la agenda pública, sin embargo advierte que se ha visto muchas veces una ausencia del compromiso de las mismas para transformar las cosas y justificando los comportamientos de la Policía contra la protesta social.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras Camila Forero, abogada e integrante de Defender la Libertad resalta la responsabilidad de las administraciones locales, gubernamentales y nacionales de garantizar el derecho constitucional a la protesta y de abrir las puertas al diálogo, Correa señala que por el contrario se ha evidenciado "una desidia por parte de los entidades para proteger a la población civil", algo que se ve directamente relacionado con las 55 masacres que se han conocido a lo largo del 2020 y donde "evidentemente se ve que la fuerza púbica no ha logrado dar garantías a las poblaciones". [(Lea también: No cesa la barbarie: 55 masacres en 2020)](https://archivo.contagioradio.com/no-cesa-la-barbarie-55-masacres-en-2020/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de estos hechos, [Comisión Interamericana de Derechos Humanos(CIDH)](https://twitter.com/CIDH) condenó los casos de brutalidad y abuso policial y le recordó al Estado "su deber de garantizar el derecho a la vida, integridad y libertad de manifestación", reiterando que el uso de la fuerza debe seguir principios de legalidad y razonabilidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante los continuos ataques de la Fuerza Pública, la ciudadanía ha exigido la renuncia de Holmes Trujillo al Ministerio de Defensa y del general Óscar Atehortua, quien es en la actualidad el director General de la Policía Nacional de Colombia y que asuman su responsabilidad como cabezas de la institución.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CIDH/status/1304052471833165825","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CIDH/status/1304052471833165825

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Policía recibió un poder excesivo en medio de la cuarentena

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Alejandro Rodríguez, coordinador del Área de Influencia Policial de Temblores ONG, organización que ha investigado las agresiones y homicidos cometidos por el ESMAD durante sus 20 años de existencia advierte sobre el exceso de poder que se le dio a la Policía en medio de la pandemia, llegando a registrar 170 casos de agresiones policiales, cifra que se elevó después de la noche del pasado 9 de septiembre. [Lea también: Abuso de la Fuerza Pública ha dejado 30 homicidios en medio de la movilización social en cuarentena)](https://archivo.contagioradio.com/abuso-de-la-fuerza-publica-ha-dejado-30-homicidios-en-medio-de-la-movilizacion-social-en-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que para el mes de julio según Defender la Libertad, desde que empezó el simulacro de aislamiento en Bogotá y la posterior cuarentena a nivel nacional, ocurrieron 30 asesinatos ocasionados por el exceso de la Fuerza Pública en el marco de las protestas sociales. A este elemento se suma la inversión de 9.515.844.030 millones de pesos durante el mes de mayo en la compra de 81.000 gases lacrimógenos y 13.000 balas para el Escuadrón Móvil Antidisturibios, ESMAD., según denunció el Senador Wilson Arias Castillo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Temblores ONG también agrega que si bien toda la población es propensa a ser víctima de violencia policial, hay sectores más propensos a ser agredidos, tal es el caso de las personas entre los 15 y los 25 años, lo que responde aun fenómeno de estigmatización contra la juventud,, los estratos más bajos, vendedores ambulantes y la población LGBTI.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un Gobierno que avala el abuso policial

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ante el anuncio del ministro de Defensa, Carlos Holmes Trujillo de **reforzar la Policía de Bogotá con 750 uniformados,** **además 850 que llegarán desde otras regiones del país junto a 300 soldados de la Brigada 13,** el coordinador de Temblores ONG señala que es tan solo otro síntoma de un Gobierno que no tiene interés en solucionar el conflicto y que expresa más preocupación por los bienes dañados que por las vidas que se arrebataron durante la noche del pasado miércoles. [(Le puede interesar: Policía gastaría más de 9.500 millones para comprar armas en plena pandemia)](https://archivo.contagioradio.com/policia-gastaria-mas-de-9-500-millones-para-comprar-armas-en-plena-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Ese tipo de comportamiento, los perfilamientos y señalamientos, rechazar la protesta pero no hablar sobre las condiciones que las generan , la forma en que ha operado el ejecutivo habla de la connivencia con esas doctrinas", afirma Correa, quien atribuye un temor que existe por parte del Gobierno hacia la movilización social, que ha tenido un crecimiento con el pasar de los meses y que alcanzó su punto más alto en el Paro Nacional de 2019, detenido únicamente por el inicio de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El informe de DDHH 2019, realizado por la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos (ACNUDH) ya había alertado sobre un incremento en la respuesta policial y un evidente y desproporcionado uso de la fuerza, incumpliendo con estándares internacionales. Ante ello ya había recomendado para el mes de febrero iniciar investigaciones al interior del ESMAD y revisar sus protocolos sobre el uso de la fuerza y de las armas y municiones menos letales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De cara a las elecciones presidenciales del 2022, concluye el médico Correa que dichas contiendas serán esenciales para transformar el Estado y elegir a las cabezas de las instituciones que tendrán en sus manos el poder de a apelar a transformacionales institucionales, fortalecer los ejercicios de veeduría y emprender acciones legislativas que permitan concluir con la impunidad al interior de la justicia penal militar.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
