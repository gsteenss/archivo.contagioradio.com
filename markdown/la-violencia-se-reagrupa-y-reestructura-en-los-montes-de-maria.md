Title: La violencia se reagrupa y reestructura en los Montes de María
Date: 2020-10-02 18:59
Author: AdminContagio
Category: DDHH, Nacional
Tags: montes de maría
Slug: la-violencia-se-reagrupa-y-reestructura-en-los-montes-de-maria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Montes-de-Maria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: *Corporación Desarrollo Solidario*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por medio de un comunicado encabezado por la Mesa de Garantías de los Montes de María, se da cuenta que más allá de los peligros por la pandemia, **las comunidades de Montes de María ven un continuo aumento en la violencia como consecuencia de la “presencia y reestructuración de grupos armados, dentro de un escenario de silencio, abandono institucional e  impunidad”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asesinatos selectivos, masacres, amenazas, extorsión y desplazamiento forzado de líderes sociales y sus familias; desplazamientos masivos, circulación de panfletos dirigidos a población LGBT, reclutamiento forzado en zonas rurales, entre otras acciones, es la situación que hoy día afronta una comunidad en la que "cerca del 86% de sus habitantes son víctimas del conflicto y el 52% del desplazamiento forzado". (Le puede interesar: [El Pueblo Awá lanza un S.O.S contra la guerra en Nariño](https://archivo.contagioradio.com/el-pueblo-awa-lanza-un-s-o-s-contra-la-guerra-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de los grupos armados, señalan que esto también se vive debido al accionar de empresarios y particulares “ por sus prácticas fraudulentas, despojo” en un territorio donde hay 81.642 hectáreas de tierra abandonadas, de las cuales al menos 40.000 han quedado en manos de estos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El comunicado también señala que la disminución de la violencia y de las confrontaciones entre grupos armados gracias a la desmovilización de las Autodefensas Unidas de Colombia (AUC) en 2005 y de la firma del acuerdo de paz permitió sepultar un conflicto que “solo había dejado miedo, dolor y pérdida”.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Es un territorio que no ha perdido la esperanza y en medio del dolor y el miedo, ha construido distintas experiencias de reconciliación, convivencia, memoria, desde sus propias instancias organizativas locales y regionales y es así como de forma creativa, pero a la vez muy política ha ido sanando poco a poco”.

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por esta razón, organizaciones del territorio, se unen por la paz que actualmente se encuentra en riesgo y exigen a los actores armados en el territorio que se respeten los derechos humanos y el ejercicio del derecho propio de las comunidades étnicas; al Ministerio Público, al Sistema Integral de Justicia, Verdad y Reparación y al Gobierno que acompañen y emitan alertas tempranas para la prevención de violaciones de derechos humanos en el territorio y para garantizar la no repetición.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las **AGC intensifican el miedo en Montes de María**
----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de la noche del 30 de septiembre, en cerca de 8 departamentos y más de 17 ciudades y municipios, incluido Montes de María, se encontraron grafitis de las Autodefensas Gaitanistas de Colombia (AGC).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los grafitis fueron puestos en tiendas, casas, calles y vehículos. Además de esto, repartieron panfletos a lo largo de los municipios justificando su accionar, al hacer mención de «la desprotección institucional”. (Le puede interesar: [Octubre comienza con campaña paramilitar de las AGC](https://archivo.contagioradio.com/campana-paramilitar-agc/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estos hechos y las acciones venideras de este grupo armado, aumentan el temor y la incertidumbre de las personas en los departamentos, ciudades y municipios.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/abrahamsoto01/status/1311783434902294529","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/abrahamsoto01/status/1311783434902294529

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
