Title: Octubre, fecha crucial para la restitución de tierras
Date: 2018-08-19 04:59
Category: Nacional, Paz
Tags: Ministerio de Agricultura, Octubre, Restitución de tierras, Unidad de Restitución de Tierras
Slug: octubre-limite-restitucion-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/tierras-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Ago 2018] 

Mediante el **Decreto 1167 del 11 de julio de 2018,** el Ministerio de Agricultura y Desarrollo Rural fijó  **11 de** **octubre** como el plazo para que las personas que estén en las zonas microfocalizadas, de la Unidad Administrativa Especial de Gestión de Restitución de Tierras **(UAEGRT)**, se inscriban en el Registro de Tierras Despojadas y Abandonadas Forzosamente.

Según el Ministerio de Agricultura, la modificación del **Decreto 1071 de 2015** sobre la misma materia, se da porque la "restitución de tierras se ha venido implementando de manera progresiva en todo el país", y la capacidad institucional le permite al Estado tramitar las solicitudes de forma más "eficaz, rápida, oportuna y creciente".

Adicionalmente, para el Ministerio, la firma del Acuerdo de paz ha significado el desescalamiento del conflicto en muchas zonas, "lo que sin duda ha mejorado las condiciones para que las víctimas puedan presentar solicitudes de inscripción" al Registro de Tierras despojadas y abandonadas, ante la UAEGRT. (Le puede interesar: ["La paz territorial, no se siente en los territorios: Misión Internacional"](https://archivo.contagioradio.com/la-paz-territorial-no-se-siente-en-los-territorios-mision-internacional/))

Por esas razones, el Decreto ordenó que "las personas que pretendan ser incluidas en el Registro de Tierras Despojadas y Abandonadas Forzosamente contarán con **tres (3) meses para presentar su solicitud, contados a partir de la vigencia de la presente modificación"** en las zonas en las que esté en curso la microfocalización, es decir, en los lugares que se han destinado para la restitución por parte de la UAEGRT. (Le puede interesar: ["En más del 90% de los casos de restitución de tierras existen intereses mineroenergéticos"](https://archivo.contagioradio.com/concesiones-minero-energeticas-en-restitucion-de-tierras-no-han-permitido-retorno-de-victimas-movice/))

Sin embargo, quienes residen en zonas dónde aún no ha empezado esta labor, tendrán 3 meses a partir de la publicación del Acto de microfocalización. Adicionalmente, el Decreto establece que se podrán realizar prorrogas de 2 periodos, de 3 meses cada uno, para presentar las solicitudes "cuando las circunstancias fácticas excepcionales del territorio impidan a los reclamantes hacerlo oportunamente". (Le puede interesar: ["Cinco propuestas para evitar el fracaso total de Ley de Restitución de Tierras"](https://archivo.contagioradio.com/42667/))

A pesar de estás prórrogas y las condiciones favorables que manifiesta el Ministerio, el Decreto no toma en cuenta las dificultades de un proceso que ha sido altamente conflictivo en las regiones por el desconocimiento que muchas personas tienen de los trámites necesarios para ser beneficiarios; y, por la ausencia de garantías de seguridad que se brinda a quienes hacen parte o lideran procesos de restitución. (Le puede interesar: ["Asesinan líder de restitución de tierras en Curvaradó"](https://archivo.contagioradio.com/asesinan-lider-de-restitucion-de-tierras-en-curvarado/))

\[caption id="attachment\_55826" align="aligncenter" width="462"\][![Unidad de Restitución de Tierras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-17-a-las-6.56.31-p.m.-462x665.png){.wp-image-55826 .size-medium width="462" height="665"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-17-a-las-6.56.31-p.m..png) Zonas de Microfocalización de la Unidad de Restitución de Tierras\[/caption\]  
 

[Decreto 1167 Del 11 de julio de 2018](https://www.scribd.com/document/386448804/Decreto-1167-Del-11-de-julio-de-2018#from_embed "View Decreto 1167 Del 11 de julio de 2018 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_39428" class="scribd_iframe_embed" title="Decreto 1167 Del 11 de julio de 2018" src="https://www.scribd.com/embeds/386448804/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-DOnVB2egmMn5erTSvC3B&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6531498913830558"></iframe>  
 

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
