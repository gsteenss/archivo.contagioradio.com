Title: En EE.UU ven el paramilitarismo como un riesgo para el proceso de paz
Date: 2016-03-04 13:53
Category: Entrevistas, Paz
Tags: Alvaro Uribe, Estados Unidos, proceso de paz, WOLA
Slug: en-ee-uu-ven-el-paramilitarismo-como-un-riesgo-para-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/paramilitares_gaitanistas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

<iframe src="http://co.ivoox.com/es/player_ek_10676468_2_1.html?data=kpWjmZuYepmhhpywj5aYaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncabijKqykLq5b9fZz5DSzpDUpdPVzs7Zy9nFtsrnztSYxdTRs4zpz5Dfy8rXq9Cf0cbfw5DJsIyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gimena Sánchez, WOLA] 

###### [4 Mar 2016] 

La Oficina en Washington para Asuntos Latinoamericanos, WOLA, realiza una visita durante esta semana a Colombia para dialogar con diferentes sectores sociales, con el objetivo de escucharlos y entender cómo están preparándose para la terminación del fin del conflicto con las FARC.

La visita tiene como objetivo analizar en qué áreas del país se necesita apoyo por parte de la comunidad internacional. De acuerdo con Gimena Sánchez, integrante de WOLA, uno de los temas que continúa preocupando a la organización **es la actividad y las amenazas constantes por parte de estructuras paramilitares en el país,** específicamente en el departamento del Chocó.

“Frente al paramilitarismo, el gobierno ha dicho que no existe más y no se ve como un problema de crimen organizado”, y añade Sánchez que “hay que tratar de buscar una solución a un problema que es real, un factor de riesgos para ciertas zonas donde se desmovilizará la FARC, donde se implementará la paz y proyectos económicos”, también agrega que le **preocupa que no se haya iniciado la fase pública  de diálogos con el ELN**.

WOLA se ha encontrado con el aumento de asesinatos, amenazas, represión y criminalización  en  contra los defensores de Derechos Humanos y el movimiento social que a su vez se ve atacado por los escuadrones de seguridad del Estado como el ESMAD. **“La protesta social y la relación con el ESMAD es muy desproporcionada y agresiva… se está censurando a la sociedad civil en un momento crítico donde se necesita que esas voces sean escuchadas”**, dice Gimena Sánchez.

De acuerdo con la Oficina en Washington para Asuntos Latinoamericanos, Estados Unidos está concentrado en encontrar estrategias para desmantelar el paramilitarismo. Así mismo, ese país “Está preocupado porque la paz se está haciendo sin participación de grupos étnicos e indígenas”, como lo señala Sánchez,  y afirma que los recursos del plan ‘Paz Colombia’, anunciado por Estados Unidos para apoyar la implementación de los acuerdos deberían dirigirse en gran parte a la sociedad civil, pues serán ellos “quienes van construir democracia y hacer veeduría de los acuerdos".

"El gobierno  colombiano ha pedido a Estados Unidos que todo el dinero sea manejado por el mismo Estado, pero sabemos que hay mucha corrupción, además, hay que tener en cuenta que en Colombia el concepto de participación no es real”, sostiene la integrante de WOLA.

La semana pasada el senador Álvaro Uribe expuso ante la** Comisión Interamericana de Derechos Humanos, CIDH**, sus críticas a la "impunidad" de las negociaciones de paz y para denunciar supuesta persecución política en contra de su partido, lo que Gimena Sánchez ve como un reflejo del poco respaldo que ahora tiene el expresidente de Colombia pues solo algunos congresistas lo atendieron.

**“La imagen de Uribe ha cambiado mucho en Estados Unidos, ya no tiene el mismo aval que durante el gobierno de Bush** y aunque todavía algunas personas lo ven como alguien que mejoró el país, eso puede cambiar en los próximos años”, concluye.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
