Title: Fue "un atentado contra el proceso de paz": Imelda Daza UP
Date: 2016-05-10 14:50
Category: DDHH, Nacional
Tags: atentado contra imelda daza, imelda daza, Unión Patriótica
Slug: fue-un-atentado-contra-el-proceso-de-paz-imelda-daza-up
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Imelda-Daza.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Pilón ] 

###### [10 Mayo 2016 ]

Como "un atentado contra el proceso de paz", califica Imelda Daza, dirigente de la Unión Patriótica, el ataque perpetrado contra los líderes de la UP, Marcha Patriótica, la JUCO y Juventud Rebelde, que se encontraban reunidos el pasado viernes en la sede de SUTIMAC en Cartagena, cuando tres hombres armados irrumpieron disparando, e hirieron a tres de los escoltas de la UNP.

"Desconcertada, frustrada, dolida, es difícil aceptar que hemos retrocedido 30 años y que quienes nos causaron tanto daño, sigan pendientes de destruir lo que se viene haciendo para la paz de Colombia", asegura Daza, quien tras 26 años de exilio se vuelve a enfrentar a la muerte. "No me arrepiento de volver al país porque ésto es lo mío, aquí tengo derecho a estar, aquí tengo derecho a ejercer mi actividad política, quiero creer que es posible hacerlo", agrega.

Este fin de semana, la Policía capturó a dos de los sicarios atacantes, quienes mantienen la hipótesis de que iban a hurtar cadenas de oro y celulares de alta gama, lo que de acuerdo con Daza, "es absolutamente falso", pues los tres escoltas agredidos no tenían ninguno de estos elementos y fueron heridos con disparos directos y sin intento de robo. "La intención era matarlos para subir la escalera y masacrarnos a quienes allí estábamos, de no haber sobrevivido uno de los escoltas, hubiera ocurrido una masacre espantosa".

"No renunciamos a nuestro propósito de lograr la paz en este país (...) invito a todos los comprometidos con la paz y la democracia a aunar esfuerzos a unirnos en un solo brazo y a luchar por el derecho legítimo de los colombianos a vivir en paz y a construir democracia, porque aquí esa ha sido siempre una asignatura pendiente", concluye la lideresa.

<iframe src="http://co.ivoox.com/es/player_ej_11480036_2_1.html?data=kpahmpWUd5ehhpywj5acaZS1kZ2ah5yncZOhhpywj5WRaZi3jpWah5yncarhxtHRw5CopdvVjJKYt7WRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
