Title: Nuevos incendios en Sierra Nevada afectan a más de 600 familias
Date: 2019-03-13 15:37
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: incendios forestales, Sierra Nevada
Slug: incendios-sierra-nevada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-41.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [13 Mar 2019] 

Dos semanas después de que incendios forestales arrasaran con el pueblo Arahuaco de Séynimin en la Sierra Nevada, las comunidades indígenas de esta región se encuentran nuevamente en un estado de emergencia por nuevos fuegos que se presentaron en los departamentos de La Guajira, Magdalena y Cesar.

Según el Instituto de Hidrología, Meteorología y Estudios Ambientales, estos incendios se deben a los **fuertes vientos, las altas temperaturas del aire y antecedentes de vegetación seca** de los últimos tres meses, que han producido el fenómeno de El Niño. Cuando una fogata escapa del control humano, estas condiciones climáticas alimentan a las llamas y crean incendios de grandes proporciones.

Actualmente, el cuerpo de bomberos ha controlado los incendios en la zona arriba del río Guatapurí, en Cesar, que **afectaron a más de 600 familias**. Sin embargo, sus esfuerzos no han logrado los mismos resultados en la zona veredal de Tigrera, en Minca, Santa Marta, donde siguen intentando de sofocar las llamas en esto territorio indígena.

El pueblo Arhuaco no ha podido determinar cuantas familias quedaron damnificadas por este nuevo desastre social y ambiental. No obstante, Arukín Torres, líder del pueblo Arhuaco, manifestó que la situación es alarmante dado que los incendios previos ya causaron "una gran cantidad de impactos irreversibles". (Le puede interesar: "[Incendios en la Sierra Nevada han afectado a más de 70 familias Arhuacas](https://archivo.contagioradio.com/incendios-la-sierra-nevada-afectado-mas-70-familias-arhuacas/)")

El líder indígena indica que aún hacen falta las herramientas adecuadas para proveer las ayudas humanitarias a los pueblos afectados. Sin embargo, agradece a los gestos de solidaridad que han demostrado "los amigos y amigas de la Sierra" al enviar apoyos a las familias afectadas. "No quiero decir que todo está resuelto sin embargo sigue la solidaridad de mucha gente", afirmó Torres.

<iframe id="audio_33349389" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33349389_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
