Title: Gobierno pidió anular decreto de Corte Constitucional que limitó el Fast Track
Date: 2017-06-16 09:10
Category: Judicial, Nacional
Tags: acuerdo de paz con las FARC, Corte Constitucional, Fast Track, Gobierno
Slug: gobierno-corte-constitucional-fast-track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Fast-Track-corte-contagioradio.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Espectador] 

###### [16 Jun 2017]

La agencia de seguridad de jurídica del Estado habría interpuesto un recurso contra la decisión de la Corte Constitucional que limitó el trámite del acuerdo del fin del conflicto con las FARC-EP a través del Fast Track. Uno de los argumentos es que el **Magistrado Bernal no estudió el caso ni permitió ningún tipo de recurso de recusación** de quienes no estarían autorizados para intervenir en la decisión.

El recurso habría sido elaborado por el Ex fiscal Eduardo Montealegre, el jurista Jefferson Dueñas y la abogada Andrea Nuñez, ex directora jurídica de la Fiscalía, y contiene 5 argumentos que tienen su centro en la violación al debido proceso tanto en la toma de decisiones como en el fondo de la decisión que sería incongruente con otras sentencias anteriores que avalaban la posibilidad, por ejemplo, de votación en bloque.

### **Las cinco razones de la solicitud de nulidad** 

En un primer elemento del recurso se hace referencia a que se violó el trámite para las recusaciones, en ese momento varias de ellas debieron ser aprobadas, por ejemplo, el caso del magistrado Carlos Bernal que participó en la votación pero no tuvo tiempo de estudiar la demanda y tampoco permitió ser recusado.

En otro de los 5 puntos fundantes se señala que hay volación al debido proceso en materia de la potestad del congreso, según la demanda no es legible el argumento de que se estarían reemplazando las funiciones de ese escenario.

También argumentan que hay cambios injustificados del precedente puesto que la Corte ya había tomado decisiones en el sentido que es posible votar en bloque y restringir únicamente al gobierno la posibilidad de incluir o no incluir artículos, además no se vaciaba de contenido ni se sustituía las funciones legislativas.

Por último se argumenta que hay violación al debido proceso por falta de deliberación suficiente teniendo en cuenta que un magistrado se posesionó un día antes de emitir la sentencia que limitó el Fast Track.

###### Reciba toda la información de Contagio Radio en [[su correo]
