Title: Palestina: Una historia de dignidad y resistencia
Date: 2020-07-09 22:04
Author: PracticasCR
Category: Actualidad, El mundo
Tags: Anexión de Palestina, Benjamin Netanyahu, Conflicto Israel-Palestina, Israel, Palestina
Slug: palestina-una-historia-de-dignidad-y-resistencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Anadolu Agency

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El plan del primer ministro israelí, Benjamin Netanyahu, quien había fijado el **1° de julio como fecha en la que iniciaría la anexión de colonias judías en Cisjordania**; aunado al plan presentado a inicios de año por Donald Trump para Oriente Medio, ha situado nuevamente el foco de atención de la Comunidad Internacional en el conflicto que se vive entre Palestina e Israel. (Lea también: [Comienza la Semana Contra el Apartheid Israelí en América Latina](https://archivo.contagioradio.com/comienza-la-semana-contra-el-apartheid-israeli-en-america-latina/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este conflicto según el analista internacional Víctor de Currealugo, se remonta a finales del siglo XIX, pasando por la **decisión de Naciones Unidas de partir el territorio palestino en 1947**, la anexión de Jerusalén, Cisjordania y Gaza en 1967; el establecimiento de la denominada Línea Verde y la barrera israelí en Cisjordania; y que de nuevo **toma vigencia con la decisión de Israel de apropiarse del valle del Rio Jordán**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La resistencia palestina desde el territorio

<!-- /wp:heading -->

<!-- wp:paragraph -->

Juani Rishmawi, de nacionalidad palestina e integrante de la ONG [Health Work Committess](http://hwc-pal.org/), relató desde Belén la cruda realidad que se vive en Palestina y la crisis humanitaria que se enfrenta en dicho territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Rishmawi la crisis humanitaria actual pasa por tres frentes principales. El primero, la **ocupación israelí** que ha generado la expulsión y el desplazamiento del pueblo palestino de su propio territorio, con la destrucción de varios asentamientos en la zona C en Cisjordania.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra lado, la **inoperatividad de la Autoridad Palestina**, la cual, no tiene un brazo institucional lo suficientemente fuerte para proteger las garantías de sus nacionales frente a la violencia ejercida por parte del gobierno y las fuerzas israelíes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En tercer lugar, **la crisis derivada por la pandemia**, agravada porque por una parte, con el congelamiento de la economía que se experimenta en varias regiones, al menos 50.000 trabajadores y trabajadoras se han visto obligados a migrar a la parte israelí para obtener ingresos y a su vuelta al territorio palestino, han traído el virus generando un rebrote de Covid-19; y por otra parte, existen regiones como Hebrón donde **la Autoridad Palestina no puede brindar tratamiento médico a las personas  porque el gobierno israelí lo prohíbe**, agravando así la ya difícil situación de salubridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Rishmawi la moral palestina se encuentra menguada por todas estas problemáticas, aunque enfatiza en que el palestino «*es un pueblo que nunca se rinde*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Nuestro pueblo ha sido expulsado de su propia tierra**»
>
> <cite>Juani Rishmawi, palestina integrande la ONG Health Work Committess</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Rishmawi también se refirió a los palestinos que se encuentran fuera del territorio por diversas circunstancias y particularmente a la situación de sus hijos, quienes se encuentran fuera de Palestina por temor a la persecución de las fuerzas israelíes.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Al pueblo palestino fuera del territorio no se le puede exigir que vengan a pelear porque tienen hijos \[que cuidar\]. Yo asumí lo que me pueda pasar, pero no puedo hacerlo con mis hijos que pueden tener una oportunidad \[…\] para una vida mejor**»
>
> <cite>Juani Rishmawi, palestina integrande la ONG Health Work Committess</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### La defensa por la liberación fuera del territorio

<!-- /wp:heading -->

<!-- wp:paragraph -->

Alí Nofal, quien hace parte de los cerca de 120.000 palestinos que residen en Colombia y está nacionalizado en nuestro país, se refirió a las luchas que lideran los palestinos, que como él, se encuentran fuera del territorio, en defensa de la liberación de su pueblo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señaló que si bien se trabaja mucho para apoyar la causa desde el extranjero, cualquier esfuerzo es poco, comparado con el de **las personas que defienden la liberación palestina desde el territorio quienes se ven obligados a «regar su propia sangre» en defensa de su pueblo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alí, quien estuvo preso en la cárcel de Nablus, se refirió al maltrato y tortura al que son sometidos los prisioneros, quienes son apresados por el simple hecho de ser palestinos a manos del **Ejército y el Mossad israelíes, quienes los someten a crueles torturas para que acepten la comisión de conductas en las que nunca incurrieron.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También afirmó que **«son más de 7.000 palestinos prisioneros en cárceles como las de Nablus, Ramala»** entre otras y que **las violaciones a Derechos Humanos que se producen a diario afectan incluso a niños y niñas quienes también son encerrados en las cárceles.** (Le puede interesar: **[56 niños palestinos fueron asesinados en 2018 por el ejército Israelí](https://archivo.contagioradio.com/ninos-palestinos-asesinados-israel-2018/)**)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Añadió que los medios masivos de comunicación en Colombia atienden a una agenda sionista y que el Gobierno encabezado por Duque acepta la anexión de Israel porque es afín con ese Estado, haciéndole un llamado al Presidente para que desde su política exterior apoye y se solidarice con la causa palestina.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Es un acto repudiable y condenable la política del gobierno colombiano frente a la cuestión palestina»**
>
> <cite>Alí Nofal, colombo-palestino residente en Colombia</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/614046866172793","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/614046866172793

</div>

<figcaption>
Otra Mirada: Palestina: Un ejemplo de dignidad.

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
