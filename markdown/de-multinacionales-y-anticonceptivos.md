Title: De multinacionales y anticonceptivos
Date: 2018-07-29 22:20
Category: Mision Salud, Opinion
Tags: Acceso a medicamentos, Bayer, Medicamentos, sistema de salud
Slug: de-multinacionales-y-anticonceptivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/1063666466.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:[Sputnik Mundo]

#### **Por[ Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

###### 29 Jul 2018 

[El propósito de lograr un sistema de salud en Colombia en el que lo primero seamos las personas requiere que todos nos pongamos la camiseta y, justamente en este momento actores relevantes del sistema, incluidos los colombianos “de a pie”, tenemos la oportunidad de decir sí a este empeño respaldando la iniciativa de controlar el precio de los anticonceptivos en el país.]

**El contexto es muy sencillo:**

1.  [En cumplimiento de la Política Farmacéutica Nacional, el Ministerio de Salud sigue avanzando en el][[control de precios de medicamentos]](https://www.minsalud.gov.co/sites/rid/Lists/BibliotecaDigital/RIDE/VS/MET/proyecto-circular-07-de-2018-comision.pdf)[ contribuyendo así a un óptimo uso de los recursos públicos, que en últimas se traduce en que el Estado tenga la capacidad de cumplir sus obligaciones según la Ley Estatutaria en Salud, a saber:]*[“El Estado es responsable de respetar, proteger y garantizar el goce efectivo del derecho fundamental a la salud”]*[.]
2.  [En esta ocasión y por primera vez las medidas de control de precios de medicamentos van a ser aplicadas también a medicamentos que, como los anticonceptivos, podemos comprar directamente en las droguerías pagando desde nuestro propio bolsillo.]
3.  [¿Por qué los anticonceptivos? Porque también en ellos fue evidente que en Colombia se estaba][[cobrando considerablemente más]](https://www.elespectador.com/noticias/salud/la-pelea-por-evitar-que-los-anticonceptivos-bajen-de-precio-articulo-800047)[ que en países de altos ingresos. “Mientras que en una farmacia colombiana un sobre de 24 píldoras de Yasminq®, de Bayer, rodeaba los \$61.000, en Canadá podía valer \$27.000 y en Noruega \$21.000][[reportó el Ministerio de Salud]](https://www.minsalud.gov.co/Paginas/1645-medicamentos-a-control-de-precios.aspx)[.]
4.  [¿Qué ha pasado? Que las multinacionales farmacéuticas que más se verían afectadas por esta medida, Bayer y Abbvie, así como el gobierno de los Estados Unidos han expresado su rechazo a la iniciativa.]

[En resumidas cuentas, tanto][[Bayer como Abbott]](https://www.minsalud.gov.co/sites/rid/Lists/BibliotecaDigital/RIDE/VS/MET/comentarios-consulta-publica-proyecto-circular07-2018.zip)[ han expresado que regular los precios en el mercado de los anticonceptivos es una práctica que va en contra de la libre competencia. Pero lo que muestran los datos de gasto en estos medicamentos es que, si la libre competencia nos lleva a que sus precios sean progresivamente ascendentes y más elevados que en otros países, se hace entonces necesario que el Gobierno intervenga.]

[En lo que respecta a la][[Embajada de los EEUU]](https://goo.gl/K7cizG)[, deja claro que su intervención se da en respuesta a una queja elevada por Abbott, empresa estadounidense, y hace énfasis en que]*[“Parte del éxito de nuestras compañías en Colombia y su apuesta a futuro está estrechamente relacionada con un entorno estable y predecible…”.]*[ Valga la oportunidad para parafrasear esta afirmación diciendo que “]*[Parte del éxito de la realización del derecho a la salud por parte de la ciudadanía y su apuesta a futuro está estrechamente relacionada con un entorno estable y predecible…]*[”.]

1.  [¿Qué hay que hacer? Normalmente estas presiones técnico-políticas se dan en esferas alejadas de lo que percibimos día a día, pero con los anticonceptivos, más colombianos podemos ver cómo una práctica indebida de una multinacional farmacéutica (precios progresivamente elevados) atenta contra el interés público.]

[Les invitamos a respaldar en redes sociales esta iniciativa gubernamental en favor del acceso a medicamentos y del derecho a la salud expresando su opinión y utilizando los hashtags \#SialaCircular07 \#AnticoncpetivosaPreciosJustosYA, y compartiendo el comunicado de más 20 organizaciones de la sociedad civil, agremiaciones, redes de trabajo y personas solicitando al Ministerio expedir la circular sin más dilaciones]

[[Comunicado](https://drive.google.com/file/d/1TF1Wb_MSKYFqhOv_gLDagLiOiLJqax-M/view?usp=sharing)]

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

------------------------------------------------------------------------

#### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<div class="osd-sms-wrapper">

</div>
