Title: Permitir minería en Santurbán es arriesgar el 85% del agua de los colombianos
Date: 2019-11-15 17:37
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Ambiente, Comuneros, Páramo de Santurbán, Santander
Slug: permitir-mineria-en-santurban-es-arriesgar-el-85-del-agua-de-los-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/SANTURBAN-Redacción-Pares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El miércoles 13 de noviembre se desarrolló la Asamblea General del Comité para la Defensa del Agua y el Páramo de Santurbán, que contó con la participación de 400 personas entre expertos, empresarios y organizaciones sindicales y políticas. Allí se aprobó una hoja de ruta para frenarle el paso a Minesa, se ratificó el compromiso para la defensa del páramo de Santurbán, se reiteró el rechazo al nombramiento de Carrasquilla como Ministro de Ambiente Ad Hoc, además se programó la movilización masiva en Bogotá convocada para el 16 de marzo del 2020 en defensa del Agua y la vida.

Según Mayerly López integrante del Comité de Santurbán,  los abogados de la organización están trabajando en una acción jurídica que esperan interponer a principios de año y demandar el proceso de licenciamiento del proyecto Minesa, por violar derechos fundamentales como al ambiente sano, agua potable y el derecho a la vida; la líder ambiental afirmó "de esta acción no podemos adelantar mucho, pero queremos informar a la comunidad que estamos adelantando todas las acciones legales, jurídicas y sociales para defender Santurbán" (Le puede interesar: [Razones por las que Carrasquilla no es idóneo para decidir sobre Santurbán](https://archivo.contagioradio.com/razones-de-los-santandereanos-no-a-carrasquilla/))

### ¿Porqué  salir a marchar el 16 de marzo por Santurbán? 

Para líderes ambientales como Mayerly López las razones son claras y se resumen en que Santurbán es el ecosistema que surte de agua a 2,5 millones de personas de los santanderes y es el nacimiento de fuentes hídricas para el nororiente del país, además resaltó la lucha de 10 años contra proyectos de minería, "en 2011 derrotamos el proyecto de explotación a cielo abierto de la GreyStar y esta no va ser la excepción, tenemos el poder de un pueblo organizado que defiende su único recurso hídrico, como es el caso de Bucaramanga que no tiene plan B. Sin Santurbán se acaba el agua" añadió.

De igual forma extendió la  invitación para toda la población del país a salir, resaltando que los 36 paramos que  representan un 50% de estos ecosistemas en el mundo le dan agua al 85% de la población colombiana, " lo que pase en Santurbán va a pasar en el resto de páramos del país. Si permitimos que se apruebe la licencia a este proyecto de minería, van a venir mas organizaciones internacionales a explotar". (Le puede interesar:[¡No se vende! Hasta Bogotá llegaron para defender el Páramo de Santurbán](https://archivo.contagioradio.com/paramo-de-santurban/))

> Alistamos una hoja de ruta para impedir el proyecto d Minesa, estamos adelantando las acciones jurídicas y llamamos a los Santanderes a movilizarnos hacia Bogotá el 16 de marzo (2020), día d los comuneros, le recordaremos a [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) que este pueblo no dejará destruir Santurbán [pic.twitter.com/Q96EIy1kAA](https://t.co/Q96EIy1kAA)
>
> — Comité Santurbán (@ComiteSanturban) [November 15, 2019](https://twitter.com/ComiteSanturban/status/1195303569697759232?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Finalmente López aseguró que "revindicando nuestra historia y luchando por nuestros recursos  naturales vamos a marchar hacia Bogotá, y el presidente Duque tendrá que escucharnos, porque los santanderes no vamos a permitir la mega minería en Colombia" y añadió que seleccionan esta fecha  por ser  el día de los comuneros, y un evento importante para Santander recordado esa lucha sus antepasados en  la defensa del  derechos y territorio.
