Title: La Corte seguramente ratificará la condena contra Andrés Felipe Arias: Gallón
Date: 2020-05-23 14:17
Author: AdminContagio
Category: Actualidad, Judicial
Tags: Corte Constitucional, Doble conformidad
Slug: pero-te-vas-a-arrepentir-gustavo-gallon-a-andres-felipe-arias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Andres-Felipe-Arias-e1564453367903.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @CaceroloArt {#foto-caceroloart .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al finalizar esta semana se conoció la decisión de la Corte Constitucional sobre una tutela interpuesta por la defensa del ex ministro Andrés Felipe Arias, que le permitirá tener acceso a la doble conformidad. Aunque el recurso no le concede a Arias la salida de su pena intramural, supone que la Corte Suprema de Justicia revise el caso, lo que para muchos se traduce en un tratamiento especial para el ya condenado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para entender lo que significa la doble conformidad, la decisión de la Corte Constitucional y la respuesta, aplaudida por algunos, de la Corte Suprema de Justicia sobre el caso Contagio Radio consultó a dos expertos abogados en materia constitucional: Cielo Rusinque Urrego y Gustavo Gallón. (Le puede interesar: ["La estrategia del uribismo que busca lavar la imagen de Andrés Felipe Arias"](https://archivo.contagioradio.com/uribismo-andres-felipe-arias/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CConstitucional/status/1263637826438037504","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CConstitucional/status/1263637826438037504

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La diferencia entre la doble conformidad y la doble instancia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cielo Rusinque Urrego, constitucionalista y master en estudios políticos, señala que parece haber desinformación en la opinión pública sobre lo que significa la doble instancia y la doble conformidad. En algunos casos, pareciera que aunque se entendiera que son términos diferentes, los asemejan en los resultados: la revisión de un fallo judicial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, Rusinque explica que por sus efectos, la doble instancia significaría, en el caso de Arias, que se suspendiera la condena a 17 años que le fue impuesta en 2014, y por lo tanto, le daría la libertad. Adicionalmente, afirma que se podría dar una posible prescripción, en tanto se pensaría que no tenía un fallo en los terminos que señala la Ley.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La experta explica que la doble conformidad establece que una persona tenga un segundo fallo, "un segundo juicio, si se quiere, de un fallo condenatorio". Es decir, aplica para casos en los que los sindicados son encontrados culpables y se revisa su caso para determinar que el fallo fue correcto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"En la doble instancia, tal como está previsto, se resuelve mediante un recurso extraordinario de apelación y ese es el que tiene efectos suspensivos", eso es lo que sería problemático y que posibilitaría la libertad de algunos aforados. En la doble conformidad que definió la Corte, lo que se está creando es un recurso extraordinario de revisión (que no es lo mismo que de apelación) para garantizar en el caso de un fallo condenatorio un segundo pronunciamiento sin efectos suspensivos, "es decir que la sentencia inicial que es revisada, queda en firme", añadió Runsinque.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La Corte Constitucional tenía que hacer lo que hizo en caso Andrés Felipe Arias"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para el director de la Comisión Colombiana de Juristas ([CCJ](https://www.coljuristas.org/)) Gustavo Gallón, la determinación de la Corte Constitucional, aunque criticada, era la que debía tomar. "El derecho a la doble comprobación es un derecho constitucional, es un derecho consagrado como derecho humano", explica el analista, agregando que una violación al mismo sería imputable al sistema jurídico y político colombiano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se suma que el comité de DD.HH. de las Naciones Unidas, encargado del cumplimiento del pacto internacional de los derechos civiles y políticos, le había dado la razón a Arias en diciembre de 2018 sobre la necesidad de que se le concediera este derecho. (Le puede interesar:["Andrés Felipe Arias y el escándalo de corrupción de Agro Ingreso Seguro"](https://archivo.contagioradio.com/en-estados-unidos-se-definira-situacion-de-andres-felipe-arias/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gallón destacó tanto la garantía al derecho, como el que la corte sorteara los riesgos anteriormente mencionados por Rusinque: el que pese a un juicio "con todas las garantías", Arias quedara en libertad por la revisión de su condena; y que se presentara la prescripción de la acción penal en su contra por el paso del tiempo desde el fallo final.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Andrés Felipe Arias tiene derecho a la revisión de su sentencia, pero se arrepentirá de pedirla

<!-- /wp:heading -->

<!-- wp:paragraph -->

El experto sostiene que quien estudie la sentencia de la Corte Suprema de Justicia encontrará que fue ajustada al derecho, "porque los delitos por los que se condenó a Arias fueron muy graves". Aseguró que estamos ante un delito grave que el ex ministro ha querido hacer ver como una cuestión menor o una irregularidad administrativa, pero que en realidad significó la entrega de dineros públicos a grandes terratenientes que esperaba que después financiaran su campaña a la presidencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También recuerda que cuando la Comisión de DD.HH. de Naciones Unidas revisó su caso, negó 11 peticiones que hizo Arias sobre la sentencia que lo condenó. Eso significa que, "de alguna manera, esta revisión de la sentencia ya la hizo una instancia internacional", y para el abogado, será un elemento importante para la sala de la Corte Suprema que revise el caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ambas razones, Gallón considera que aunque es su derecho, Arias se arrepentirá de pedir la revisión de su caso "porque lo más probable es que se confirme su sentencia". (También lo invitamos a leer: ["La doble instancia retroactiva no salvará a Andrés Felipe Arias"](https://archivo.contagioradio.com/doble-instancia-andres-felipe-arias/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Se abre un boquete para que más de 200 aforados pidan revisión en sus sentencias?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gallón dice entender la preocupación ante una posible avalancha de aforados solicitando la revisión de sus casos, pero reitera que es necesario garantizar también sus derechos. Y al igual que en el caso de Arias, afirma que sus procesos condenatorios estuvieron sujetos a un estudio meticuloso, por lo que la revisión de las sentencias no significará la absolución de sus delitos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cambio, lo que sí significará será que la Corte necesitará disponer de más recursos para resolver los casos que se presenten. No obstante, Gallón reitera que el negar el derecho a la doble comprobación sería desconocer la validez del derecho internacional de los DD.HH. en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte Rusinque fue enfática en que la Corte Constitucional estableció una serie de criterios y requisitos, como el criterio de temporalidad que solo aplicaría para condenados a partir de 2014, lo que imposibilitaría la idea de que sea masivo el pedido de doble revisión en sus condenas por parte de aforados.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ricarospina/status/1263806596926377984","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ricarospina/status/1263806596926377984

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Una última opinión sobre el comunicado de la Corte Suprema de Justicia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tan pronto como se conoció la decisión de la Corte Constitucional, la Corte Suprema de Justicia emitió un comunicado en el que decía acatar el fallo, aunque mostraba su desacuerdo con el mismo considerando que se habían cambiado "las reglas de juego" para favorecer a Arias, abriendo un escenario de incertidumbre.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CorteSupremaJ/status/1263652136266629120","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CorteSupremaJ/status/1263652136266629120

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Tanto Gallón como Rusinque explicaron el comunicado en el sentido de entender la Corte Suprema como un organismo que actuó en derecho conforme a las normas legales vigentes en el momento, y que no cometió errores en el proceso; pero Rusinque advirtió también sobre los efectos directos que podría tener el mismo en el caso de Andrés Felipe Arias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La constitucionalista alerta que la comunicación de manera contundente dice que no está de acuerdo con la decisión de la Corte Constitucional, por lo que se intuye que la Corte Suprema tiene una idea sobre el caso de Arias. En se sentido, "se podría esperar que la defensa de Arias acuda a las recusaciones cuestionando la imparcialidad" del ente que revisará la sentencia del exministro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tales recusaciones, en visión de Rusinque, podrían llevar a que se tengan que nombrar conjueces que adelanten la revisión, entre otros efectos desconocidos que pudieron ser evitados con un pronunciamiento distinto de la Corte Suprema de Justicia. (También lo invitamos a consultar: ["La injerencia del Uribismo en las cárceles según trabajadores del INPEC"](https://archivo.contagioradio.com/inpec/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
