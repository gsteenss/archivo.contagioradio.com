Title: Pobladores de Jiguamiandó entre fuego cruzado e incursiones de grupos armados
Date: 2019-03-15 16:34
Category: Comunidad, DDHH
Tags: conflicto armado, Crisis humanitaria, Jiguamiandó
Slug: pobladores-de-jiguamiando-fuego-cruzado-incursiones-grupos-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/mujer-camina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 15 Mar 2019 

[Crecientes enfrentamientos entre diversos grupos armados como el ELN, paramilitares y el Ejército, que hacen presencia en los alrededores del territorio colectivo de **Jiguamiandó en Chocó,** han ocasionado temor entre los habitantes, los han confinado y han destruido sus bienes colectivos como el acueducto comunitario, fruto de la cooperación internacional.]

[Según la denuncia de los habitantes, después de observarse un desembarco de tropas muy cerca de los lugares humanitarios, el acueducto comunitario donado por la Unión Europea en 2013  fue encontrado descompuesto y sus tanques, manguera, válvulas dañadas y algunas sustraídas.  ]

[Aunque en la comunidad desconocen qué actor armado pudo estar detrás de las afectaciones al acueducto, indican que en la zona ha sido recurrente la presencia de efectivos del Ejército e incluso fue avistado un helicóptero sobrevolando las cercanías del lugar. [(Lea también: Indígenas de Alto Guayabal desplazados y confinados por grupos armados) ](https://archivo.contagioradio.com/enfrentamiento-entre-grupos-armados-crisis-humanitaria-y-166-personas-desplazadas-en-alto-guayabal/)]

El poblador afirma que por parte de la comunidad se han asegurado de visibilizar su lugar de habitación como una zona humanitaria, lugar exclusivo de población civil,  **"tenemos pancartas, banderas, y un pedacito de carretera que nos visualiza mucho"** como una medida de protección frente a la presencia del ELN y los paramilitares quienes no han sido enfrentados por la Fuerza Pública.

[El habitante de la zona humanitaria explica que debido a la presencia de diversas organizaciones garantes de DD.HH.  los pobladores del lugar se sienten seguros en el territorio, sin embargo aclara que la comunidad siente temor pues creen que alguno de los grupos armados que recorren los alrededores de la zona podrían incursionar nuevamente como lo han hecho en ocasiones anteriores el **ELN y los grupos paramilitares. **]

### **Contexto de la zona ** 

Las comunidades ubicadas en las cuencas del **Curvaradó y Jiguamiandó**, son parte del Carmen del Darién, en la región del Bajo Atrato en el Chocó, lugar en el que los procesos extractivos de palma aceitera las convierte en regiones de alto interés para **sectores empresariales quienes han establecido alianzas con actores armados con el fin de lucrarse.** [(Le puede interesar Paramilitares están retomando el control del Bajo Atrato)](https://archivo.contagioradio.com/paramilitares-estan-retomando-el-control-del-bajo-atrato/)

Dichos vínculos han dado como resultado la intimidación de los pobladores y han generado desplazamientos masivos, evidentes desde 1997 con los operativos de contraguerrilla del Ejército en la región orquestados junto a grupos paramilitares disponiendo así de sus tierras.

<iframe id="audio_33418908" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33418908_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
