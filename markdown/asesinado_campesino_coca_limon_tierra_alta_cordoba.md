Title: Comunidad de El limón, en Córdoba, exige que Brigada 16 del Ejército sea retirada
Date: 2017-12-21 09:09
Category: DDHH, Nacional
Tags: ASODECAS, cordoba, Cultivos de uso ilícito
Slug: asesinado_campesino_coca_limon_tierra_alta_cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DRWx4p6W0AAqKYw.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [20 Dic 2017] 

Continúan las acciones de la fuerza pública contra los campesinos cultivadores de coca, que han decidido firmas procesos de sustitución. Esta vez, el hecho sucedió en la vereda Limón, municipio de Tierra Alta, Córdoba, donde e**l campesino Alexander José Padilla Cruz** habría sido asesinado, por el teniente Ferney Vega Padilla, brigada 16 fuerza de tareas conjuntas Nudo del Paramillo del Ejército.

Padilla tenía 33 años de edad, y **era uno de los campesinos que junto con la comunidad de dicha vereda ya se había suscrito al Plan Nacional Integral de Sustitución,** con el que esperaba dejar de depender de los cultivos de uso ilícito, gracias a los proyectos productivos anunciados por el gobierno con lo que quería darle otro futuro a sus 4 hijos. Sin embargo, en esa región como en el resto de municipios cultivadores de coca, el **Gobierno Nacional ha incumplido con la estrategia de sustitución.**

### **Los hechos** 

De acuerdo con el relato de los hechos, de Asociación de Campesinos para el Desarrollo del Alto Sinú (ASODECAS), la víctima se encontraba  trabajando en su finca cuando seis miembros de dicha brigada llegaron a la propiedad donde lo abordaron junto con otro campesino, a quienes les dijeron que **serían puestos a disposición de la justicia por tener en sus predios hoja de coca.**

Tras ese hecho que generó temor en ambos campesinos, Padilla se asustó y corrió unos cuantos metros, es entonces cuando uno de los soldados identificado como el Teniente Vega le apunta con su arma de fuego y le dispara, cegando su vida instantáneamente, posterior a esto el otro campesino pretende correr y este mismo soldado le amenaza puntualmente diciéndole “como corras a ti también te mato”.

Según Andrés Chica, vocero de ASODESCA, fueron **siete impactos de bala los que recibió el campesino por la espalda, **sin que se hubiera registrado ningún tipo de altercado. Tras los hechos, la comunidad retiene al teniente**, "y lo que dice es que 'ahí les deja ese perro', como si fuera un delincuente", relata Chica.**

### **La versión del Ejército** 

Por su parte, es otra la versión del **Comando de la Fuerza de Tarea Conjunta Nudo del Paramillo. Desde allí se indica que el teniente y los militares se encontraban **desarrollaron labores de inspección. Según el Ejército, fue en ese momento cuando "detuvieron a dos sujetos que se movilizaban a caballo **transportando dos bultos con hoja de coca que sería empleada para el procesamiento y producción de clorhidrato de cocaína**", dice un comunicado.

Asimismo, señalan que " posteriormente, los aprehendidos inician un intento de huida que desencadena una reacción por parte de las tropas, que termina en el deceso de uno de ellos. De forma inmediata, la Inspección del Ejército** **destinó una comisión especial para adelantar las indagaciones pertinentes sobre lo ocurrido y poner a disposición de las autoridades competentes toda la información necesaria para esclarecer los móviles del hecho".

### **Las exigencias de la comunidad ** 

Hasta el momento, las autoridades informaron el pasado lunes que el teniente del Ejército Ferney Vega Padilla fue capturado por la Policía, no obstante, la comunidad exige que haya una investigación exhaustiva de los hechos se sancioné a todos los que estuvieron inmersos en el asesinato del campesino.

Ante tal situación el temor de los habitantes de la vereda es evidente, a tal punto que ya estarían organizándose para desplazarse masivamente. Según cuenta el vocero de ASODECAS, ya se hizo una reunión con la fuerza pública, con entes nacionales, e internacionales tales como la Defensoría del Pueblo, la ONU, en donde la comunidad exigió que **se cambie a sea retirada la Brigada 16 de ese territorio, o bien, sean cambiados los militares, debido a desconfianza que tienen en ellos.**

<iframe id="audio_22767940" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22767940_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div>

</div>
