Title: La fuerza de los hechos y de la unión se impuso en la OMS
Date: 2018-02-04 23:25
Category: Mision Salud, Opinion
Tags: Acceso a medicamentos, Acceso a Salud, OMS, Organización Mundial de la Salud
Slug: la-fuerza-de-los-hechos-y-de-la-union-se-impuso-en-la-oms
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Consejo-Ejecutivo-de-la-Organización-Mundial-de-la-Salud-2018.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: diariochaco 

#### **Por: [Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

###### 4 Feb 2018 

[La semana pasada se realizó en Ginebra la sesión N°142 del Consejo Ejecutivo de la Organización Mundial de la Salud (OMS), en la que se debatió activamente sobre el futuro del acceso a medicamentos y su estrecha relación con el monopolio farmacéutico.]

[Desde comienzos del presente siglo los expertos en la materia encontraron la causa de la angustiosa realidad que viven millones de personas en los países del sur de no contar con los medicamentos que necesitan para prevenir o superar enfermedades propias o predominantes en su región, como dengue, mal de Chagas, tuberculosis, entre otras. En pocas palabras, la causa es que, como el modelo con el que estamos acostumbrados a incentivar la investigación de medicamentos está basado en el atractivo de ganar cantidades exageradas de dinero con las ventas monopólicas de lo que se desarrolle, no es buen negocio si lo que se investiga tendría como clientes potenciales personas o países con poco dinero. Es decir que estamos frente a un modelo que solo sirve a los derechos humanos en la medida en que se satisfaga el interés privado de obtener ganancias exageradas. Es un modelo con las prioridades invertidas.]

[Para superar este problema, en el 2008 la Asamblea Mundial de la Salud aprobó la][[Estrategia Mundial y Plan de Acción sobre Salud Pública, Innovación y Propiedad Intelectual]](http://apps.who.int/medicinedocs/documents/s21429es/s21429es.pdf)[. Y a lo largo de los años de debate, análisis, evaluación y más debate al interior de la OMS, mientras unos hemos sido testigos de la resistencia a la solidaridad y a la justicia social de parte de empleados públicos provenientes de diferentes países, principalmente de países sede de multinacionales farmacéuticas, como Estados Unidos, Japón y Suiza, millones de personas en el mundo han sufrido las consecuencias de esta resistencia y de la falta de unión y fuerza de parte de los servidores públicos de más alto rango en salud de los países del sur.]

[No obstante, diez años después, es decir, la semana pasada, en el Consejo Ejecutivo de la OMS diferentes líderes mundiales dijeron algo así como: no nos vamos de aquí hasta que no acordemos avances reales para resolver este problema. ¡Aplaudimos conmovidos en la distancia! La fuerza de los hechos es tal que no fue una sino varias las voces, entre ellas la de Brasil, Congo, Ecuador, Argelia, Filipinas y Colombia, las que se unieron y perseveraron en este empeño.]

[A las voces de los países del sur se unieron también las de países como Portugal y Países Bajos, debido a que no están pudiendo pagar los excesivos precios monopólicos a los que les venden los medicamentos nuevos. Por ejemplo, el tratamiento de 12 semanas contra la Hepatitis C tiene un precio de US\$147.000 por persona (US\$1.750 diarios), no obstante que el costo de producción del tratamiento completo es de sólo US\$133.]

[Al finalizar la semana se lograron dos acuerdos que nos acercan globalmente a la solución del problema de acceso a medicamentos: por un lado, se][[insta a los Gobiernos a aprobar recomendaciones concretas de la Estrategia y a seguir debatiendo sobre las 3 de ellas que generan controversia]](http://apps.who.int/gb/ebwha/pdf_files/EB142/B142(4)-sp.pdf)[, lo cual se espera que ocurra en la Asamblea Mundial de la Salud de mayo de este año. Por otro lado, se le][[solicita al equipo técnico de la OMS que proponga un mapa de trabajo para los siguientes 5 años en materia de acceso a medicamentos]](http://apps.who.int/gb/ebwha/pdf_files/EB142/B142(3)-sp.pdf)[.]

[Ya despegamos, ahora hay que avanzar y con fuerza. Lo que viene son escenarios donde se requiere que la voluntad, determinación y estrategia de los países que actúan en favor del derecho a la salud de las personas persistan, para que se aborde prioritariamente este problema como lo que es, un problema de salud pública y derechos humanos y no un problema comercial. Será allí donde se decida sobre los aportes de todos los países para financiar la innovación de medicamentos, sobre la obligación que tienen las corporaciones farmacéuticas de decir cuánto realmente les cuesta innovar y desarrollar un medicamento o sobre el apoyo a los países para que ejerzan plenamente el derecho de utilizar las salvaguardas de la salud pública establecidas en la normativa internacional para contrarrestar los abusos del monopolio. Tenemos la esperanza de que en las reuniones de la AMS 2018 y el Consejo Ejecutivo 2019 se adopten las decisiones conducentes a la superación definitiva del problema.]

[Los temas abordados en escenarios como el Consejo Ejecutivo o la Asamblea Mundial de la Salud son temas que a la mayoría de la población del mundo nos parecen ajenos y lejanos. Pero para que no nos quejemos mañana necesitamos hoy entender que las decisiones que allí se toman tienen efecto directo en nosotros y nuestros entornos, independientemente de donde vivamos. Entender esta realidad significa exigir a los representantes de nuestros países en estos escenarios, como servidores de los ciudadanos, defender y promover la equidad, la solidaridad y la justicia social.]

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
