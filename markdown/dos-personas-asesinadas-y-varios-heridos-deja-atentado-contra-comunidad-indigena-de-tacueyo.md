Title: Cinco personas asesinadas y varios heridos deja atentado contra comunidad indígena de Tacueyó
Date: 2019-10-29 18:27
Author: CtgAdm
Category: DDHH, Nacional
Tags: atentando, Cauca, indígenas, tacueyo
Slug: dos-personas-asesinadas-y-varios-heridos-deja-atentado-contra-comunidad-indigena-de-tacueyo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/asesinato-de-lider-indigena-en-tacueyó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El **Consejo Regional Indígena del Cauca** (CRIC) denunció que en la tarde de hoy integrantes del resguardo de Tacueyó, Cauca,  fueron emboscados por un grupo de hombres armados, identificados como la columna Dagoberto Ramos. En el hecho fue asesinada  la gobernadora **Neehwe'sx Cristina Bautista**  y el guardia indígena **José Gerardo Soto,** así mismo informaron de tres personas más fallecidas: **Asdrúbal Cayapu (guardia indígena)**, **Eliodoro Finscue** y **James Wilfredo Soto**.

Cuatro personas resultaron heridas, **Matías Montaño Noscué, José Norman Montano Noscué,** [**Crescencio Peteche Mensa**, **Dora Rut Mesa Peteche.**]{.text_exposed_show}

Las autoridades indígenas fueron emboscadas en el sector La Luz, Tacueyó. Según el comunicado los vehículos en los que se transportaban fueron[ lanzados al vacío. Lo que se convierte en el segundo hecho de este tipo después del ataque a una caravana de la guardia ocurrido el [25 de Julio sobre la vía que comunica a Caloto con Toribío](https://archivo.contagioradio.com/guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca/).]{.text_exposed_show}

[Las autoridades indígenas hacen un llamado a los pueblos indígenas a unirse en solidaridad con las familias y con la comunidad de Tacueyó y convocaron a los pueblos indìgenas del departamento a declararse en Asamblea Permanente]{.text_exposed_show}

En desarrollo...
