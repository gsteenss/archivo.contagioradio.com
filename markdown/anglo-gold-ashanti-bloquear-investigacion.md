Title: Anglo Gold Ashanti pretende bloquear investigación sobre violencia en territorios en los que hace presencia
Date: 2019-09-04 11:58
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: Anglogold Ashanti, titulos mineros
Slug: anglo-gold-ashanti-bloquear-investigacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Titulos-mineros-de-AGA-en-relación-con-masacres.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Minería-de-Anglo-Gold-Ashanti.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:][@cla\_huircan] 

La Fundación Forjando Futuros hizo un estudio en el que se presentan las cifras de los títulos mineros que tiene Anglo Gold Ashanti, en relación con el despojo de tierras, el proceso de restitución de las mismas, el asesinato de líderes sociales y la comisión de masacres. Aunque la investigación aclara que de ello no se concluye que la empresa sea culpable por estos hechos, sí debería motivar al Gobierno para que tome medidas respecto a la explotación del subsuelo.

El director de La Fundación Forjando Futuros, Gerardo Vega, denunció que pese a que han sostenido reuniones con Anglo Gold Ashanti, la multinacional ha realizado visitas a las embajadas o agencias de cooperación "buscando que no tengamos apoyo para hacer las investigaciones que hacemos, pero nosotros vamos a continuar en nuestra labor independientemente de todo". (Le puede interesar: ["Nueva derrota en Jericó para A.G.A."](https://archivo.contagioradio.com/nueva-derrota-en-jerico-para-anglogold-ashanti/))

### **El foco sobre Anglo Gold Ashanti** 

Según información publicada por las Dos Orillas, 11 grandes empresas son las encargadas mayoritariamente de extraer metales preciosos en Colombia: Gran Colombia Gold Corp., Continental Gold Corp., Miranda Gold, Royal Road Minerals, Libero Copper Corporation, Newrange Gold Corp., Max Resources, Metminco, Atico Mining Corporation y Anglo Gold Ashanti. Gerardo Vega, director de Forjando Futuros, declaró que Anglo Gold tiene 184 títulos de explotación minera, y hay 109 títulos más de empresas con las que tienen alguna relación porque son sus filial o porque comparten miembros de juntas directivas.

Gerardo Vega resaltó que esos municipios donde la empresa tiene títulos mineros hay 18.422 solicitudes de restitución de tierras; además, en esos mismos municipios se han presentado 57 asesinatos de líderes sociales desde 2016. El director de la Fundación manifestó que muchos de estos lugares tienen en común que "son zonas olvidadas, donde hubo presencia del conflicto, actores armados ilegales y que de una u otra forma en medio del conflicto, causó el desplazamiento". (Le puede interesar:["A.G.A. desconoce acuerdo municipal que prohíbe la minería"](https://archivo.contagioradio.com/anglo-gold-ashanti-desconoce-acuerdo-municipal-que-prohibe-la-mineria/))

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Titulos-mineros-de-AGA-en-relación-con-masacres.png){.aligncenter .size-full .wp-image-73065 width="557" height="513"}

Vega aseguró que luego del mapeo y cruce de información, la Fundación no afirma que Anglo Gold Ashanti haya causado estos delitos, "pero sí vemos que estas empresas han aprovechado la situación de violencia para adquirir estos títulos mineros, o para compra y concentración de tierra". El defensor de derechos humanos argumentó que ello se argumenta en la figura de la buena fé exenta de culpa que está en la Ley 1448 DE 2011, que señala que las empresas o personas que compren en lugares donde ha habido conflicto deben investigar suficientemente para saber que el precio que pagan por la tierra es justo.

Asimismo, recordó que también tendrían que estar seguros que quien vende es el verdadero propietario y que no hay hechos de violencia que ocasionan la venta de dichos bienes, en ese sentido, sostuvo que comprar tierras a precios bajos en zonas de conflicto significaba que "tiene una tragedia atrás, que se la quitaron, que a alguien despojaron" y las empresas "si tienen que saber que allá había ocurrido algo". Por esta razón, Vega dijo que el Gobierno debía tomar medidas en relación con la política para la adjudicación de títulos, que impidiera que este tipo de situaciones siguieran pasando.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
