Title: Sub-comisión de Género en La Habana se reúne en conmemoración del Día Internacional de la Mujer
Date: 2015-03-05 16:40
Author: CtgAdm
Category: Mujer, Paz
Tags: estudiante, federacion, feu, genero, habana, lgtbi, mujer, paz
Slug: en-dia-internacional-de-la-mujer-se-reune-sub-comision-de-genero-para-conversaciones-de-paz
Status: published

###### Foto: Carmela María 

<iframe src="http://www.ivoox.com/player_ek_4170977_2_1.html?data=lZakkp6be46ZmKiakpmJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dizsra0dfFssXjjMmSpZiJhaXVjM7b1srWssLXytTbw9GPqMafzcaYz9rOqdOZk6iY1cqPtsaZpJiSpKbSqYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Angélica Aguilar, Federación de Estudiantes Universitarios] 

Este jueves 5 de marzo viaja la tercera y última delegación de 5 personas que se reunirá con la Sub-Comisión de Género, de la Mesa de Conversaciones de Paz en La Habana, Cuba.

El grupo compuesto, al igual que los dos anteriores, por 6 líderes y **lideresas de procesos LGTBI como Colombia Diversa, de mujeres campesinas de los Montes de María y el Catatumbo,** y estudiantes, sumará sus experiencias a las consignadas en semanas anteriores por organizaciones como la Red Nacional de Mujeres Excombatientes, Casa de la Mujer y Ruta Pacífica de Mujeres.

Para una de las delegadas, la estudiante Angélica Aguilar, de la **Federación de Estudiantes Universitarios**, es muy importante hacer parte de este escenario para visibilizar la situación de mujeres jóvenes en el conflicto y sus aportes para la construcción de la paz, máxime en una coyuntura de conmemoración del 8 de marzo, que "reivindica a las mujeres luchadoras y en resistencia al rededor del mundo".

En los próximos tres días, la **Sub Comisión escuchará las ponencias del tercer grupo con las propuestas** construidas desde las organizaciones sociales para tratar los conflictos políticos, económicos y sociales con enfóque de género, recolectará la información de violación de Derechos Humanos relacionadas, y generará una agenda de trabajo con base en los pre-acuerdos definidos entre las FARC y el gobierno.

A pesar de que se tiene proyectado que esta sea la última delegación, el grupo considera que no es suficiente con la participación de las 18 personas que han viajado desde diciembre del 2014. "Es necesario que la Mesa se abra para el conjunto de las mujeres y el sector LGTBI", señaló la estudiante.
