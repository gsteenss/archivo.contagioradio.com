Title: Opinión
Date: 2014-11-21 15:05
Author: AdminContagio
Slug: opinion-2
Status: published

### OPINION

[![¿Polarizar o no polarizar Dr. Wasserman?](https://archivo.contagioradio.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400.png "¿Polarizar o no polarizar Dr. Wasserman?"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400.png 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-300x156.png 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-768x399.png 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/polarizar-o-no-polarizar-770x400-370x192.png 370w"}](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)  

###### [¿Polarizar o no polarizar Dr. Wasserman?](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)

[<time datetime="2019-01-19T07:24:22+00:00" title="2019-01-19T07:24:22+00:00">enero 19, 2019</time>](https://archivo.contagioradio.com/2019/01/19/)El movimiento a favor de la polarización no es tan furioso como lo presentan, pero sí es un movimiento en tanto que, como aseguraba Alain Touraine, se enfila no solo ideológicamente sino en la práctica[LEER MÁS](https://archivo.contagioradio.com/polarizar-o-no-polarizar-dr-wasserman/)  
[](https://archivo.contagioradio.com/el-2019-en-las-urnas-del-mundo/)  

###### [El 2019 en las urnas del mundo](https://archivo.contagioradio.com/el-2019-en-las-urnas-del-mundo/)

[<time datetime="2019-01-14T13:59:58+00:00" title="2019-01-14T13:59:58+00:00">enero 14, 2019</time>](https://archivo.contagioradio.com/2019/01/14/)En más de 60 Estados a lo largo y a lo ancho del mundo están previstas para este año elecciones generales o presidenciales. Algunas de ellas van a tener un fuerte impacto sobre los equilibrios[LEER MÁS](https://archivo.contagioradio.com/el-2019-en-las-urnas-del-mundo/)  
[](https://archivo.contagioradio.com/2018-un-ano-de-movilizaciones-sociales/)  

###### [2018: Un año de movilizaciones sociales](https://archivo.contagioradio.com/2018-un-ano-de-movilizaciones-sociales/)

[<time datetime="2018-12-23T11:55:27+00:00" title="2018-12-23T11:55:27+00:00">diciembre 23, 2018</time>](https://archivo.contagioradio.com/2018/12/23/)El 2018 terminó con una representativa y creativa movilización del pueblo y la sociedad colombiana en torno a la defensa o incidencia sobre cuestiones esenciales para el país, incluida la cuestión ambiental.[LEER MÁS](https://archivo.contagioradio.com/2018-un-ano-de-movilizaciones-sociales/)  
[](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)  

###### [Mensaje de navidad para un Estado secuestrado](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)

[<time datetime="2018-12-19T06:00:46+00:00" title="2018-12-19T06:00:46+00:00">diciembre 19, 2018</time>](https://archivo.contagioradio.com/2018/12/19/)A esa gente, que tiene secuestrado al Estado Colombiano y que participa de los mecanismos de ese secuestro, un solo mensaje de navidad[LEER MÁS](https://archivo.contagioradio.com/mensaje-de-navidad-para-un-estado-secuestrado/)
