Title: Tumaco alerta sobre la presencia de paramilitares
Date: 2017-02-20 16:09
Category: DDHH, Nacional
Tags: Derechos Humanos, paramilitares
Slug: tumaco-alerta-sobre-la-presencia-de-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/paramilitares-e1472682962275.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [20 Feb 2017] 

**Comunidades de El Playón, en Tumaco, Nariño, denunciaron las operaciones armadas de grupos paramilitares tanto en la zona rural como en casco urbano** del municipio y en cercanías a las zonas veredales transitorias, sin que exista algún control por parte de las Fuerzas Militares o presencias de las autoridades.

Los habitantes expresaron que en el caserío hizo **presencia un grupo de 120 paramilitares que respondías a los “Urabeños”  y 30 más que pertenecían a “Los Negritos”**. A su vez, en los caseríos El Pital y El Firme personas informaron que un grupo de 70 hombres vestidos con camuflados y portando armas de largo alcance estuvieron en este lugar. Le puede interesar: ["Sí hay presencia paramilitar en el Catatumbo: Comisión de Verificación"](https://archivo.contagioradio.com/comision-de-verificacion-en-catatumbo-verifico-denuncias-sobre-presencia-paramilitar/)

Esta misma situación se presentó en la vereda El Ceivito, ubicada a 5 minutos de la vía Tumaco – Pasto, **contiguo a la base militar Chilvi, en donde pobladores alertaron sobre un grupo de 50 hombres**, también con camuflados y armas de largo alcance. De igual forma habitantes en las Bocanas del Río Mira informaron que permanentemente se ven paramilitares de civil o con camuflados, pertenecientes a los “Negritos”. Le puede interesar: ["Paramilitares arremeten de nuevo contra reclamantes de Guacamayas"](https://archivo.contagioradio.com/paramilitares-arremeten-nuevo-reclamantes-las-guacamayas/)

Con estas denuncias Nariño ingresa a la lista de departamentos como Norte de Santander, Putumayo, Valle del Cauca y Córdoba, en donde comunidades ha denunciado el accionar de grupos paramilitares. Le puede interesar: ["Paramilitares arremeten en diferentes regiones del país"](https://archivo.contagioradio.com/paramilitares-arremeten-contra-diferentes-regiones-del-pais/)

###### Reciba toda la información de Contagio Radio en [[su correo]
