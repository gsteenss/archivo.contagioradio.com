Title: Las luchas lesbofeministas y trans unidas por sus derechos
Date: 2017-07-12 12:26
Category: LGBTI, Libertades Sonoras
Tags: feminismo, Lesbianas, Lesbofeminismo, LGBTI, Marcha Gay
Slug: las-luchas-lesbofeministas-y-trans-unidas-por-sus-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Marcha-LGBTI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Diversa] 

###### [12 Jul. 2017] 

En Colombia y en el mundo se conmemoró el Día del Orgullo LGBTI, con marchas coloridas, pancartas, música y bajo el lema 'Estado Laico, cuerpos libres' cientos de personas reivindicaron la diversidad sexual.

**En \#LibertadesSonoras quisimos escuchar lo que piensan algunas personas que se enuncian como parte de la comunidad LGBTI** de la marcha, la reinvindicación de los derechos, la coptación de las marchas y la unión para poder seguir luchando por sus derechos.

En el programa contamos con la voz de **Marcela Salas quien es activista lesbofeminista,** gorda y machorra quien entre otras cosas aseguro que "hay que entender que el feminismo es algo que significativamente salva vidas, que te transforma la realidad, siempre para lo LGBT más feminismo puro y duro, más lesbofeminismo".

Así mismo, desde Chile contamos con **Claudia Rodríguez, activista, travesti y feminista** quien hizo un panorama de la realidad vivida en ese país en materia de acceso a derechos para su comunidad y añadió "que hay que seguir trabajando para que cada vez mueran menos compañeras, para que cada vez un mayor grupo de compañeras pueda prepararse (...) hay que hacer cambios al sistema educativo, para contar con una educación no sexista".

<iframe id="audio_19767320" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19767320_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
