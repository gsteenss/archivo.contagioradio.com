Title: Por orden judicial Gobierno pide perdón al exmagistrado Iván Velásquez
Date: 2020-10-13 22:32
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Uribe, das, Inteligencia militar ilegal, Iván Velásquez
Slug: por-orden-judicial-gobierno-pide-perdon-al-exmagistrado-ivan-velasquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Ivan-Velasquez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Por orden del Tribunal Administrativo de Cundinamarca, el Gobierno Nacional pidió perdón al exmagistrado auxiliar de la Corte Suprema de Justicia, Iván Velásquez**, por los seguimientos e interceptaciones ilegales de los que fue víctima durante el gobierno del expresidente Álvaro Uribe Vélez.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/infopresidencia/status/1314675724373037058","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/infopresidencia/status/1314675724373037058

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este acto obedece al cumplimiento de un fallo de segunda instancia emitido por el Tribunal de Cundinamarca, que **condenó al extinto Departamento Administrativo de Seguridad -DAS- y al Departamento Administrativo de la Presidencia -[DAPRE](https://dapre.presidencia.gov.co/dapre)- por las interceptaciones ilegales, también conocidas como «chuzadas» de las que fue víctima Iván Velázquez** mientras era auxiliar de la Corte Suprema de Justicia entre los años 2007 y 2009.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El exmagistrado Velásquez investigaba los casos de «parapolítica», en ejercicio de su cargo como coordinador de las investigaciones de vínculos de sectores políticos con el paramilitarismo en la Corte Suprema.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El fallo se dio ya que el Tribunal consideró que Iván Velásquez fue víctima de seguimientos ilegales por agentes del Estado que vulneraron su derecho a la intimidad, infiltrando incluso su esquema de seguridad. **En ese sentido, se logró constatar con declaraciones de exfuncionarios del DAS, que el escolta asignado al exmagistrado, fue reclutado como fuente por esa entidad para que aprovechando la confianza de Velásquez, diera detalles sobre su vida laboral y personal.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El ofrecimiento de disculpas estuvo a cargo del Jefe de la Oficina Jurídica de Defensa del Estado, Camilo Gómez, y del director del Departamento Administrativo de la Presidencia, Diego Molano, quienes después de casi 3 meses de la emisión del fallo que se dio el 17 de julio de 2020, tuvieron que reconocer que el Estado se equivocó y señalaron, como representantes del Gobierno, que actos como ese, no podían volver a repetirse.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto, el exmagistrado Velasquez, señaló que el Gobierno no ha tenido un sincero propósito de acabar con las prácticas de espionaje adelantadas durante el gobierno de Álvaro Uribe y que **la eliminación del DAS fue solo una «*cortina de humo*» para que no se conociera la verdad detrás de estos hechos.** (Le puede interesar: [Los crímenes de la inteligencia militar llegaron a la JEP](https://archivo.contagioradio.com/los-crimenes-de-la-inteligencia-militar-llegaron-a-la-jep/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Este es el segundo acto de perdón del Gobierno Nacional, que se da por cuenta de una orden judicial, en menos de una semana.** El pasado miércoles, el ministro de Defensa, Carlos Holmes Trujillo, ofreció excusas públicas al país en medio del debate de moción de censura que se adelantaba en su contra en el Congreso, para evitar que el mismo Tribunal de Cundinamarca lo declarara en desacato.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Iván Velásquez es solo una de las víctimas de la inteligencia ilegal

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aunque el Gobierno pidió perdón por intermedio de sus delegados Camilo Gómez y Diego Molano, quienes además señalaron que actos como ese no podían repetirse; **los seguimientos ilegales en contra de periodistas, magistrados, líderes defensores de DD.HH., opositores políticos y en general, personas que no muestran afinidad con el Gobierno, siguen presentándose.** (Le puede interesar: [Espionaje de Ejército colombiano no puede seguir impune: Organizaciones de EE.UU.](https://archivo.contagioradio.com/espionaje-de-ejercito-colombiano-no-puede-seguir-impune-organizaciones-de-ee-uu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así quedó documentado en la audiencia sobre esta problemática, que se llevó a cabo la semana pasada ante la Comisión Interamericana de Derechos Humanos -CIDH-, en la que **los solicitantes expusieron numerosos casos en los que la inteligencia militar fue utilizada de manera ilegal para realizar actos de espionaje y seguimiento en contra de varias personas de manera «*sistemática*» y sostenida en el tiempo.** (Lea también: **[Inteligencia militar ilegal es reincidente en Colombia: CIDH](https://archivo.contagioradio.com/inteligencia-militar-ilegal-es-reincidente-en-colombia-cidh/)**)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
