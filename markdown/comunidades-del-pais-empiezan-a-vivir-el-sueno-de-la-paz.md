Title: Comunidades del país empiezan a vivir el sueño de la paz
Date: 2017-06-28 13:33
Category: Nacional, Paz
Tags: Comuidades Construyendo Paz, comunidades, Dejación de armas, paz
Slug: comunidades-del-pais-empiezan-a-vivir-el-sueno-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/sueños-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Jun 2017] 

La dejación del 100% de las armas de las FARC **fue recibida por las comunidades como una oportunidad para crear alternativas de paz.** En el Putumayo celebran que niños y niñas vivirán una nueva historia y en el Cauca anhelan poder tener oportunidades de desarrollo y participación hacia la construcción de una Colombia en paz.

<iframe src="http://co.ivoox.com/es/player_ek_19526009_2_1.html?data=kp6ilJuUdJqhhpywj5aVaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5ynca3p25C6w9fNssKfpNrQytrRpsaZk6iY25DQs9Sf1Mrb1s7Rrcbi1dTgjcnJb9biwpDjh6iXaaK4xNnWz8aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Luz Marina Cuchumbe, víctima del conflicto armado en el Cauca, asegura que “sentimos la necesidad de que los colombianos vean la oportunidad que tenemos todos de vivir en paz”. La alegría de saber, que las armas que alguna vez se usaron para asesinar a personas inocentes, ya no hacen parte de la realidad de los campesinos del país, **se evidencia en la tranquilidad que sienten ellos y ellas en sus territorios.**

Cuchumbe manifestó además que Colombia está viviendo un momento en donde **“necesitamos estar unidos para crear oportunidades de esperanza para crear proyectos de desarrollo y paz”.** Ella afirmó que las mujeres en el Cauca ya están trabajando en huertas ecológicas y que esperan poder tener tierras propias para que los y las campesinas de Colombia vuelvan a tener alternativas con calidad de vida. (Le puede interesar: ["Comunidades del Catatumbo comienzan a construir agenda de paz"](https://archivo.contagioradio.com/42740/))

<iframe src="http://co.ivoox.com/es/player_ek_19525939_2_1.html?data=kp6ilJqdd5qhhpywj5WaaZS1lJ6ah5yncZOhhpywj5WRaZi3jpWah5yncavVz86Ytc7QusKZk6iY1dTGtsafytLd0dfYpc_XysaY09rJb9XdxtPSjdXFtsKfxNTa19PNqMLYxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Jani Silva, integrante de la zona de reserva campesina Perla Amazónica del Putumayo, la dejación de armas es  **"una oportunidad para que los campesinos vuelvan a su tierra** y para nuestros hijos vivan en un país donde no peligren por la presencia de las armas”. El acto en Mesetas, Meta  fue visto por ella como un momento de reflexión donde “los colombianos se dieron cuenta de que las armas solo dejan muerte y dolor”.

A pesar de esto, Jani Silva aseguró que aún hay temores en las comunidades por la presencia de grupos paramilitares y porque **“hay muchos enemigos de la paz que no han vivido la guerra en carne propia”**. Ella manifestó que sueña con que “los colombianos seamos gestores de nuestro desarrollo y seamos gobernantes de nuestros territorios”. (Le puede interesar: ["Con la dejación de armas se está cerrando la página de la guerra en Colombia"](https://archivo.contagioradio.com/dejacion-armas-farc/))

<iframe src="http://co.ivoox.com/es/player_ek_19526052_2_1.html?data=kp6ilJuUeZOhhpywj5WXaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncbPjxdfWydSPh8Ln1c7ZztSJdqSf1NTP1MqPqMbewsjWh6iXaaOnz5DRx5DFts7V1JDRx5DQpdSfp6a_pZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De igual forma, Rodrigo Castillo, miembro de la organización Comunidades Construyendo Paz, CONPAZ, aseguró que **“la dejación de armas es un sueño de paz que ayer se materializó** y donde la historia de Colombia se parte en dos”. Adicionalmente dijo que desde la organización seguirán trabajando para que las comunidades puedan participar de la implementación de los acuerdos de paz.

En un comunicado a la opinión pública CONPAZ califica como **valiente la decisión de las FARC de entregar el armamento a los representantes de la ONU**. Igualmente establecieron que “son valientes de asumir este proceso en medio de la constatación de nuevas formas de paramilitarismo, de mecanismos de burocratización que hace a un Estado ineficaz y en medio del miedo a la verdad de muchos sectores de la sociedad”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
