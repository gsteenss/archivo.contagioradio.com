Title: 7 departamentos del país se quedarían sin el único Hospital de 3er nivel
Date: 2015-07-22 15:18
Category: DDHH, Movilización
Tags: alejandro, caprecom, coomeva, crisis, EPS, Gaviria, hospital, hospital universitario, julian mora, paro, Salud, valle
Slug: 7-departamentos-se-quedarian-sin-el-unico-hospital-de-3er-nivel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Hospital-Valle1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sara Bolaños 

<iframe src="http://www.ivoox.com/player_ek_5143530_2_1.html?data=lpahlZqXdI6ZmKiak5aJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtbgysbbjbLTtsKZk6iYr4qnd4a1msnWxdSPq8bixtfOzpDIqc2fqdTg0s7Ypc2fttPW2MrWt8rowtfW0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Julian Mora, Médico general del Hospital Universitario del Valle] 

###### [22 jul 2015] 

Cerca de 3 mil trabajadores y trabajadoras del Hospital Universitario del Valle completan 1 semana de movilizaciones y denuncias sobre la grave situación financiera que vive el único centro de salud de tercer nivel del sur occidente colombiano.

Médicos generales del Hospital aseguran que, aunque la crisis es estructural, en el último año se ha visto agravada como consecuencia de una deuda que haciende a los 5.2 billones de pesos. Sólo EPS como Coomeva, Caprecom y Coosalud, le deben al hospital cerca de 120 mil millones de pesos. La consecuencia inmediata es que a los trabajadores con contrato de nómina del deben 2 meses de salario, mientras a los médicos generales que contratan por cooperativa les deben casi 3.

"Nosotros tratamos de seguir trabajando, de ponerle el pecho al Hospital, a la situación, pero hay un punto en que la crisis social es evidente y hay que tomar cartas en el asunto", asegura Julian Mora, médico general del Hospital.

Y es que el HUV que atiende a pacientes desde el departamento del Caquetá, pasando por el Putumayo, Valle, Huila y Valle del Cauca hasta el Chocó no sólo no tiene recursos para pagar nómina, sino que se quedó corto en la compra de insumos tan básicos como tubos para hemogramas para atender a sus pacientes.

La capacidad de atender pacientes por urgencias se ha visto tan afectada que la Secretaría de Salud Departamental declaró la alerta amarilla. "Se han cancelado cirugías programadas. Hay gente que lleva mas de 1 año esperando una cirugía. Hay niños con leucemia o cáncer que necesitan quimioterapia como su única oportunidad de vida, y no se pueden comprar porque no hay plata", añade el doctor Mora.

La consigna principal de los y las trabajadores del HUV es lograr sacar al Hospital de la crisis en que está sumergida, responsabilizando en principio al Ministro Alejandro Gaviria, a las instituciones gubernamentales de orden departamental y local, tanto como a las EPS.

"Necesitamos un cambio del sistema de fondo. No podemos andar de crisis en crisis. Es necesario evaluar la responsabilidad de forma y fondo de las EPS"

A propósito de la movilización nacional convocada para el miércoles 22 de julio, el sector salud ha insistido en la necesidad de reconfortar la Mesa Nacional por el Derecho a la salud, para establecer diálogos sobre el modelo de salud que necesita Colombia, según el doctor Mora, basado en el "modelo de negocio y en la intermediación, o un modelo basado en la atención primaria y la prevención como derecho a la salud".

[  
](https://archivo.contagioradio.com/7-departamentos-se-quedarian-sin-el-unico-hospital-de-3er-nivel/carta-abierta-huv-2/) [![Carta Abierta HUV](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Carta-Abierta-HUV.jpg){.aligncenter .wp-image-11496 .size-full width="537" height="623"}](https://archivo.contagioradio.com/7-departamentos-se-quedarian-sin-el-unico-hospital-de-3er-nivel/carta-abierta-huv/)

[![Carta Abierta HUV 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Carta-Abierta-HUV-2.jpg){.aligncenter .wp-image-11495 .size-full width="539" height="603"}](https://archivo.contagioradio.com/7-departamentos-se-quedarian-sin-el-unico-hospital-de-3er-nivel/carta-abierta-huv-2/)
