Title: 76 años sin las letras de Miguel Hernández
Date: 2018-03-28 15:08
Category: Viaje Literario
Tags: Generación del 27, poesia, presos politicos
Slug: miguel-hernandez-poesia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/1490640393860.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fundación Miguel Hernández 

###### 28 Mar 2018 

Cuando se cumplen 76 años de la muerte de Miguel Hernández, poeta y dramaturgo considerado el último representante de la Generación del 27 en España, sus letras parecen no perder vigencia. Su temprana muerte a manos del fascismo, que lo llevó a una prisión e Alicante donde falleció por una tuberculosis, ha servido para inmortalizar sus versos libertarios.

Hernández nació un 30 de octubre de 1910 en Orihuela, en el seno de una humilde familia de cabreros, uno de los oficios tradicionales por entonces en  la municipalidad. Desde muy joven desarrolló un gusto particular por la poesía clásica y descubrió que poseía la sensibilidad necesaria para escribir sus propias composiciones.

Con 20 años en 1930 empezó escribiendo en publicaciones locales y luego viajaría a Madrid donde se vincularía con fuerza al movimiento poético de la época, llegando a encumbrase como uno de los grandes poetas de España no sólo con sus versos, también con sus textos dedicados a motivar moralmente a las fuerzas Republicanas en los frentes de guerra contra el fascismo.

Su activismo en la Guerra Civil, lo obliga a abandonar España al final de la misma, sin embargo es capturado en la frontera con Portugal donde es detenido y luego sentenciado a muerte, condena que le sería conmutada por una pena de 30 años de cárcel que no llegaría a cumplir por su prematura partida en 1942.

*Si me muero, que me muera*  
*con la cabeza muy alta.*  
*Muerto y veinte veces muerto,*  
*la boca contra la grama,*  
*tendré apretados los dientes*  
*y decidida la barba.*

(Fragmento de Vientos del pueblo me llevan)

   
 
