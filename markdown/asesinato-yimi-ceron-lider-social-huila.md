Title: Asesinan a Yimi Cerón hijo de líder social y político del Huila
Date: 2017-11-16 18:06
Category: DDHH, Nacional
Tags: congreso de los pueblos, Coordinador Nacional Agrario, lideres sociales, Yimi Cerón
Slug: asesinato-yimi-ceron-lider-social-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/Yimi-Ceron.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo familiar] 

###### [16 Nov 2017]

A través de un comunicado el Congreso de los Pueblos denunció que en la madrugada de este 16 de Noviembre fue asesinado Yimi Cerón Jiménez de 22 años de edad. El joven se desempeñaba como vigilante de la Institución Educativa del Salto de Bordones, zona rural del municipio de Isnos, sur del departamento del Huila. La versión que se conoce hasta el momento relata que dos hombres a bordo de una motocicleta le propinaron varios impactos con arma de fuego.

Aunque las organizaciones firmantes de la denuncia afirman que los hechos son materia de investigación, no se puede descartar que el asesinato del joven tenga motivaciones políticas dado que era hijo de Silvio Cerón, líder de la región y no había manifestado tener ningún tipo de amenazas en contra de su vida.

Yimi era hijo de Silvio Cerón Samboní, reconocido líder comunitario, defensor de Derechos Humanos e integrante del Coordinador Nacional Agrario, de la plataforma social Congreso de los Pueblos y también dirigente y militante del Polo Democrático Alternativo. [Lea también: Asesinatos de líderes sociales en aumento](https://archivo.contagioradio.com/asesinan_campesino_albert_martinez/)

El siguiente es el texto de la denuncia

[Comunicado Jimy Ceron](https://www.scribd.com/document/364646058/Comunicado-Jimy-Ceron#from_embed "View Comunicado Jimy Ceron on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_41677" class="scribd_iframe_embed" title="Comunicado Jimy Ceron" src="https://www.scribd.com/embeds/364646058/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-7HvI63IulIFWzEPcx1Zk&amp;show_recommendations=true" width="80%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
