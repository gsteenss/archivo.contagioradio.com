Title: ¿Por qué Ecuador se vuelca a las calles?
Date: 2019-10-07 17:32
Author: CtgAdm
Category: Movilización, Política
Tags: ecuador, estado excepción, FMI, paquetazo, Paro Nacional
Slug: ecuador-tras-protestas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EGCjgcZWsAUkCDt.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Ecuador-imagen.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CONAIE] 

Ecuador mantiene la paralización en respuesta al paquete de medidas económicas anunciadas por el presidente Lenín Moreno el pasado miércoles, lo cual generó un descontento en gran parte de la población, especialmente en el sector del transporte. Por parte de las organizaciones, se ha anunciado una huelga general programada para el 9 de octubre. A ésta, se sumarían las comunidades indígenas del país, quienes han decretado el estado de excepción en sus territorios y ya se están movilizando hacia la capital con el propósito de exigir al gobierno del presidente Lenín Moreno el respeto a los derechos colectivos de los pueblos.

Las manifestaciones han cobrado la vida de dos víctimas mortales. Las organizaciones sociales y la gobernación de Azuay han informado sobre el fallecimiento del manifestante Raúl Chilpe, quién presuntamente fue atropellado por un vehículo particular mientras participaba en el bloqueo de la carretera Cuenca-Molleturo, sector El Chorro, al sur del país.

Además, se han cuantificado decenas de heridos y 477 personas han sido detenidas. “Actualmente hay más de 400 personas detenidas a las cuales no se les ha respetado sus derechos. Sus familiares no han sido comunicados de su condición y están sin abogados” denuncia una ciudadana.

Ante esta situación de paro nacional, el gobierno decretó el estado de excepción como medida para reducir las concentraciones en las calles. “Tanto la Ministra del Gobierno como el Ministro de Defensa afirmaron que policías y militares harán uso progresivo de la fuerza y éste último llamó ayer a la ciudadanía a no desafiar a los uniformados. Esto puede ser leído como un anuncio de que la represión va a continuar” afirma Isabel Ramos, profesora investigadora del Departamento de Estudios Internacionales y Comunicación de FLACSO Ecuador.

Por otro lado, Felipe Mosquera, comunicador comunitario, alerta que es de suma importancia contrastar y validar la información, puesto que han aumentado las fake news: "[han comenzado a circular vídeos de años anteriores, de otras manifestaciones, que hacen pasar por actuales. Hay que ser muy cuidadosos a la hora de verificar la información que nos llega". ]

### **¿Qué implica el estado de excepción en Ecuador?** 

“Con el fin de precautelar la seguridad ciudadana y evitar el caos, he dispuesto el estado de excepción a nivel nacional”, así declaró Lenín Moreno en una rueda de prensa el pasado jueves.

Con la declaración del estado de excepción de Ecuador se pretende minimizar las protestas que se llevan produciendo en todo el país en contra del “paquetazo”. Pero, ¿qué implica realmente esta situación?

El estado de excepción da potestad al presidente de limitar el ejercicio de los derechos de la ciudadanía En ese sentido, Moreno suspendió en todo el territorio “el ejercicio del derecho a la libertad de asociación y reunión”, lo que restringe “la conformación de aglomeraciones en espacios públicos durante las 24 horas del día”. Es decir, da luz verde a las fuerzas de seguridad del Estado para reprimir las protestas.

### **¿Cómo afectan las medidas económicas?** 

Las medidas económicas concretadas en el Decreto Ejecutivo 883 consisten en la eliminación de subsidios a las gasolinas, lo que supone un aumento del 123%: las gasolinas extras y ecopaís aumentan de 1,85 dólares a 2,39 y el diésel pasa de 1,03 dólares a 2,29. Asimismo, se contempla la liberación de los precios y que éstos se establezcan de manera mensual, lo cual, según expertos en la materia, puede provocar un efecto inflacionario.

“Esto ha sido muy mal recibido, no solamente por los gremios de transportistas. Tras esta medida hay otras que forman parte del acuerdo con el Fondo Monetario Internacional (FMI) que nunca se ha hecho público, que deterioran las condiciones de vida seriamente y son vistos como un golpe al salario y al trabajo” afirma Isabel Ramos.

El gobierno de Moreno apunta hacia un modelo liberal, en el que pretende implementar otras medidas como la eliminación o reducción de los aranceles para la compra de equipo, maquinaria y materia prima y la importación de ciertos equipos electrónicos como celulares.

En esa misma línea, el paquete de reformas también contempla modificaciones de las condiciones laborales, como el recorte de vacaciones para los trabajadores del sector público, pasando de 30 a 15 días y que además estos aporten mensualmente un día de su salario o la renovación con un 20% menos de remuneración de los contratos ocasionales.

“La fuerza del ajuste lo van a sentir los trabajadores asalariados, no solamente el sector público del país. Hay un paquete de reformas laborales que tiene que ver con retroceder en las conquistas sociales conseguidas en términos de jornada, de remuneración” alerta Isabel Ramos.

<iframe id="audio_42763178" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42763178_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
