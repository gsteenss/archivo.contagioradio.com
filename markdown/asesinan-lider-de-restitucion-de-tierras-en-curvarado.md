Title: Asesinan líder de restitución de tierras en Curvaradó
Date: 2018-06-26 12:11
Category: DDHH, Nacional
Tags: Comisión Intereclesial de Justicia y Paz, Cuvaradó, Lider social, Restitución de tierras
Slug: asesinan-lider-de-restitucion-de-tierras-en-curvarado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [26 Jun 2018] 

El sábado 23 de junio Adrian Pérez,  hijo de uno de los líderes de restitución en tierras de la Comunidad "La Nevera", en el municipio de Curvaradó, Chocó, fue asesinado por neoparamilitares. El jóven de 33 años de edad intentó escapar y fue asesinado en medio de un grupo de trabajadores según lo indicó la Comisión de Justicia y Paz que tiene presencia en la zona.

Pérez se encontraba en el caserío de Llano Rico, corregimiento de Curvaradó, cuando fue retenido por 4 miembros de las AGC, quienes lo obligaron a abordar una moto de alto cilindraje para trasladarlo en dirección del corregimiento de Pavarandó. Sin embargo, antes de llegar allí, Adrian vio una docena de trabajadores saliendo de una finca, por lo que intento huir de sus captores, lo persiguieron y asesinaron delante de ellos.

Según el comunicado de la Comisión Intereclesial de Justicia y Paz, emitido el mismo día de los hechos, "en el momento de la retención ilegal de Adrián, las Fuerzas Militares se encontraban a menos de un kilómetro de Llano Rico" y, momentos después de su deceso, integrantes de las AGC intimidaron a la comunidad de La nevera.

Adrián, y su padre se sumaron al liderazgo en la reclamación de predios de Cuvaradó desde hace 3 años, momento en el que regresaron a su tierra después de haber sido desplazados. Allí iniciaron el proceso de restitución en el territorio colectivo de La Larga Tumaradó en donde su familia habitó desde hace más de 30 años.

De acuerdo con el comunicado, el padre de Adrián ya había recibido amenazas de muerte en su contra por iniciar el proceso de restitución y por ocupar sus territorios en Cuvaradó, Pedeguita y Mancilla en el Chocó. (Le puede interesar: ["Asesinado líder reclamante de tierras Hernán Bedoya en Chocó"](https://archivo.contagioradio.com/asesinado-reclamante-de-tierras-hernan-bedoya/))

### **La restitución en Curvaradó** 

En el mismo comunicado se detalla que, la familia Pérez fue “despojada por la empresa palmera Urapalma desde 1996 por operaciones paramilitares amparadas por el modelo contrainsurgente de la brigada 17”. Luego, en 1997 fueron desplazados del territorio colectivo de Pedeguita y Mancilla; y allí “se sembró banano, se inició la ganadería extensiva y también se montó muy cerca una cocina para el procesamiento de cocaína”.

En ese mismo territorio hace un año fue asesinado Duberney Gómez, hijo del reclamante de tierras Rafael Taquero y hace cerca de 5 meses asesinaron a Mario Castaño, reclamante de tierras. (Le puede interesar: ["Asesinan a Mario Castaño, líder reclamante de tierras en el Choco"](https://archivo.contagioradio.com/asesinan-a-mario-castano-lider-reclamante-de-tierras-en-el-choco/))

Por estas razones la [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/hijo-de-lider-de-restitucion-de-tierras-asesinado/) pidió que los factores de riesgo estructurales para la restitución de tierras sean atendidos y enfrentados por parte del Estado, así como que haya medidas eficaces de protección a las comunidades.

###### [Reciba toda la información de Contagio Radio en [[su correo] 
