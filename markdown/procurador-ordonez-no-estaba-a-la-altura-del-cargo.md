Title: Procurador Ordoñez "no estaba a la altura del cargo"
Date: 2016-09-08 12:22
Category: Entrevistas, Judicial
Tags: Gustavo Petro, Iván Cepeda, piedad cordoba, plebiscito por la paz, Procurador
Slug: procurador-ordonez-no-estaba-a-la-altura-del-cargo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/procurador2-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: semana] 

###### [08 Sep 2016]

Las declaraciones del procurador son una ofensa “in admisible” contra los magistrados y magistradas del Consejo de Estado y comprueban que Alejandro Ordóñez no estaba a la altura del cargo de la Procuraduría. Además, se demostró judicialmente que se cometieron delitos como **prevaricato y tráfico de influencias** para lograr la reelección, situación que podría acarrear demandas de tipo penal, afirma Gustavo Gallón director de la **Comisión Colombiana de Juristas.**

Según Gallón, las irregularidades que cometió Ordoñez para buscar su elección fueron muy evidentes, fiestas, nombramientos y otros se convirtieron en el centro **de un acto bochornoso en que se incurrió para esa elección** y para otras decisiones como la destitución de Piedad Córdoba y Gustavo Petro, así como la absolución de personas de su misma línea política. “*Es grato por una lado pero es triste saber que hemos tenido que sufrir eso porque [son muchos los estragos causados a muchas personas pero también al conjunto de la institucionalidad](https://archivo.contagioradio.com/reelecccion-del-procurador-no-existe-en-la-constitucion-ni-en-la-ley-dice-consejo-de-estado/)*” afirma el jurista.

Respecto de la demora en la toma de la decisión por parte del Consejo de Estado, Gallón afirma que fueron varias las maniobras dilatorias que aplicó el procurador para intentar impedir que avanzara la discusión, **incluso a última hora intentó, sin éxito, recusar a la magistrada encargada de la ponencia** que al final terminó siendo aprobada por 14 votos a favor y 5 en contra.

Por otra parte, ante la duda de la **posibilidad de investigación penal o disciplinaria** que acarrearía una sanción para elección en cargos públicos, el director de la CCJ afirma que hay lugar a procedimientos en los dos escenarios, sin embargo, si una decisión del Consejo de Estado se demoró 3 años, en otras instancias se pueden demorar mucho más, “*en el papel es posible pero **en la práctica probablemente no prosperen***”.

Además, como consecuencia directa de la acción que lo destituye no habrá otros escenarios hacia una sanción de otro tipo, [sería necesario interponer otras acciones para que se proceda a la investigación y sanción](https://archivo.contagioradio.com/el-procurador-ordonez-y-su-abuso-de-poder-contra-la-comunidad-lgbti/), en ese sentido afirma que estudiarán esta posibilidad. En el mismo sentido se pronunció el senador **Iván Cepeda quién afirmó que el Consejo de Estado actuó en derecho** y que era una decisión que se esperaba por la contundencia de las pruebas.

Respecto a la carrera política del procurador, Cepeda afirma que **están listos para dar la batalla de las ideas** bien sea en la campaña por el plebiscito o la campaña por la presidencia de la república. Por su parte Gallón afirmó que **no cree que el procurador tenga muchos réditos políticos** como para aspirar seriamente a la elección presidencial.

<iframe src="https://co.ivoox.com/es/player_ej_12829499_2_1.html?data=kpellJ6YfZqhhpywj5acaZS1lZuah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPi8LgzYqwlYqmd8-fpKi3j4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
