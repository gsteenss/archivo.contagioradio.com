Title: Assaults against human rights defenders increased more than 40% in 2018
Date: 2019-04-23 15:41
Author: CtgAdm
Category: English
Tags: Aggressions against human rights defenders, Somos defensores
Slug: assaults-against-human-rights-defenders-increased-more-than-40-in-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-02-25-at-9.02.58-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

With its most recent report titled "**La Naranja Mecánica,"** **Somos Defensores ** reveals that flaws with the peace deal's implementation and the absence of security measures have been some of the reasons that have triggered an escalation in the armed conflict. The appearance of more armed groups has led to an increase in displacement and subsequently **an intensification in aggressions against defenders by 43.75%, in comparison to 2017**.

The report is divided into four sections. The first one analyzes the transition of the presidential administraion and what this has meant in terms of peace and security. The second section critically examines how the vacuum left by the demobilized FARC guerrillas resulted in territorial disputes. This is followed by an investigation on the low capacity of the State to respond to these threats and finally the report includes data analysis of the Information System on Aggressions against Human Rights Defenders in Colombia.

**New government, different priorities**

Despite the government transition, which required that the implementation of the peace deal signed by the former FARC be considered a priority, there was an upsurge in violence in May during the first round of elections and also during the second round in June. As a result, **97 human rights defenders had been murdered and 591 had been assaulted by August 7.**

The situation worsened with the arrival of Duque’s administration, which identified its top  priorities to be the economy and entrepreneurship and left aside its commitment to the peace agreement. Duque's administration is showing a liking to private companies and this is the reason why the report is titled 'La Naranja Mecánica'.

The summary states that President Iván Duque "**is ruling by offering its good side to the economy and showing its back to peace. "** The authors also express concern for the rhetoric used by the government to deny the existence of an armed conflict and the ongoing systemic attacks against human rights defenders.

 

\[caption id="attachment\_65226" align="alignnone" width="512"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Colombia-277x300.png){.wp-image-65226 width="512" height="555"} Foto: Somos Defensores\[/caption\]

**Territorial fights**

The report also reveals that "the defense of land and territory" is trying to be silenced by different actors, and such aggressions is the demonstration. Those that [were present in 27 of the 32 departments o]**f the country are directly related to the presence of crops of illicit use**, extractive activities, strong social processes, and proximity to the former Transitional Normalization Zones or current Territorial Training and Reintegration Spaces.

This situation is stronger in Cauca, followed by Antioquia, Valle del Cauca, Bogota, Cesar, Norte de Santander and Nariño. New criminal structures have appeared, such as: the Gaitanist Self-Defense Forces of Colombia, Los Puntilleros, La Cordillera, La Constru, The Pachenca, Los Caparrapos, Los Pachely, Los Rastrojos, Clan Isaza, Las Fuerzas Unidas del Pacífico, Frente Oliver Sinesterra or the Frente Antiparamilitarismo Especial 36.

\[caption id="attachment\_65227" align="alignleft" width="240"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Líderess-240x300.png){.size-medium .wp-image-65227 width="240" height="300"} Somos Defensores\[/caption\]

**Paramilitary defense raise**

According to the data registered by the Information System on Attacks against Human Rights Defenders, never before Colombia has registered such high number of assaults as in 2018, counting on 805 cases of violence among which 155 murders appear.

It also highlights that of these 155 homicides, 12.2% were related to crop substitution. It is important to point out that the persecution of leaders who promote this activity has been intensified, armed actors continue to persecute community leaders, defenders and representatives of the communities working on National Comprehensive Program for the Replacement of Illicit Use Crops.  
.  
Regarding this kind of violence in Colombia, Somos Defensores declares that during the year 2018, on average 2.2 person was attacked per day. In July occurred 119 episodes, in May 112, August 109 and June 77. They also highlights that among the 805 cases of aggression 29% were against women and the remaining 71% against men revealing an increase in the attacks against women human rights defenders.

> Las agresiones contra los defensores de derechos humanos aumentaron en 2018 ¿En 2019 esta cifra podría aumentar de cara a las elecciones regionales? Diana Sánchez, directora [@SomosDef](https://twitter.com/SomosDef?ref_src=twsrc%5Etfw) [\#LaNaranjaMecanica](https://twitter.com/hashtag/LaNaranjaMecanica?src=hash&ref_src=twsrc%5Etfw) <https://t.co/sbUMsDIrbC> [pic.twitter.com/APlOB6x0rw](https://t.co/APlOB6x0rw)
>
> — Contagio Radio (@Contagioradio1) [April 23, 2019](https://twitter.com/Contagioradio1/status/1120706255931285504?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Finally, Somos Defensores states that of these **805 attacks registered in 2018, it is presumed that: 55.5% were committed by paramilitary groups or similar structures, 33.5% by unknown persons, 5% by dissident groups of the FARC, 4.2% by the Public Force and 1.8% by the ELN**.  
This shows a greater responsibility of the armed paramilitary groups.

\[caption id="attachment\_65300" align="alignnone" width="347"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/SomosDefensores-120x300.jpeg){.wp-image-65300 width="347" height="868"} Foto: Contagio Radio\[/caption\]
