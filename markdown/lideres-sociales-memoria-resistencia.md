Title: Asesinato de líderes sociales, memoria y resistencia
Date: 2017-08-16 14:51
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: asesinato, Berta Cáceres, Carlos Pizarro, lideres sociales, UP
Slug: lideres-sociales-memoria-resistencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/1493263312374-1493260793846-_DEI8192-e1502910629338.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pacifista 

###### 16 Ago 2017 

En el marco del IV Festival Internacional de cine por los Derechos Humanos, este miércoles 16 de agosto se realizará en Bogotá el panel: "Asesinato de líderes sociales, memoria y resistencia", un espacio abierto en el que la imagen y la palabra sirven como insumos para el diálogo y la reflexión.

El evento inicia a las 5:30 de la tarde con la proyección de "Ceder es más terrible que la muerte, una producción del Centro nacional de Memoria histórica, que aborda los hechos que rodearon el asesinato de Josué Giraldo Cardona, líder de la Unión Patriótica ocurrido el 13 de octubre de 1996.

<iframe src="https://www.youtube.com/embed/Vu11tPnXriQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Acto seguido, desde las 6 de la tarde iniciará una reflexión, a partir del concepto de memoria viva, sobre los asesinatos de líderes sociales desde los hechos del pasado pero con una postura crítica frente a la situación actual. (Le puede interesar: [Somos Defensores documentó 51 asesinatos de líderes de DDHH en 2017](https://archivo.contagioradio.com/somos-defensores-documento-51-asesinatos-de-defensores-de-derechos-humanos-en-2017/))

El evento contará con la presencia como moderadora de María José Pizarro, hija de Carlos Pizarro Leóngómez y como invitados al artista del Grafitti Tóxicomano, el director editorial de Pacifista Carlos Jiménez, Luisa Giraldo, sobrina de Josué Giraldo y la investigadora del Centro Nacional de memoria histórica Lina Pinzón.

Adicionalmente, durante el evento que se realizará en el hemiciclo de la Universidad Jorge Tadeo Lozano de Bogotá se presentará el documental "Volveré y sere millones: homenaje a Berta Cáceres de Tatiana Vila Torres.

<iframe src="https://www.youtube.com/embed/C9ceON9ng4g" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
