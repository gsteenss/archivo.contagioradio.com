Title: Un kit y 150 mil pesos, solución de la Alcaldía para desalojados en Bilbao, Suba
Date: 2017-04-26 12:48
Category: DDHH, Nacional
Tags: Alcalde Enrique Peñalosa, Desalojo Suba Bilbao
Slug: un-kit-y-150-mil-pesos-solucion-de-la-alcaldia-para-desalojados-en-bilbao
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Incendio-Suba-Bilbao-RCN3-e1493227906691.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RCN Radio] 

###### [26 Abr 2017 ] 

Los habitantes desalojados del barrio Bilbao, en Suba, denuncian que ni la alcaldía Distrital ni la alcaldía local, han tomado acciones para ayudarlos o reubicarlos en otra parte de Bogotá, por el contrario, solo les han ofrecido un kit más 150 mil pesos para que se vayan de este lugar, **las personas han expresado que tras haberlo perdido todo, no tienen ningún otro sitio al cual dirigirse, ni oportunidades para reconstruir sus vidas**.

Cuatro voceros que se reunieron con el alcalde de Suba, Edgar Andrés Sinesterra, afirmaron que este les indicó que la única alternativa que se les iba a ofrecer “**era el kit y que debían desalojar los predios**” sin prestarles mucha atención durante la reunión. Le puede interesar: ["Desalojo violenteo de 1200 habitantes del barrio Bilbao en la localidad de Suba"](https://archivo.contagioradio.com/bilbaosubadesalojo/)

El Kit que estaría entregando la Alcaldía de Enrique Peñalosa, está conformado por **una frazada, una colchoneta, elementos de aseo y \$150.000 pesos**, que según los habitantes solo es entregado a las personas que demuestren que tenían una dirección, sin embargo los habitantes expresan que al ser quemadas las viviendas no pueden colocarlas.

Jeimmy Díaz, vocera de la comunidad expresó que la situación es crítica porque no hay **ningún plan de reubicación, razón por la cual niños, adultos mayores, mujeres embarazadas y adultos han tenido que dormir a la intemperie** recibiendo ayuda de organizaciones defensoras de derechos humanos y de los vecinos, además, denuncian que la Policía no les ha permitido ingresar a sus viviendas para sacar las pocas pertenencias que quedan y agregó que “de la alcaldía no hemos recibido ni para darle a los niños un desayuno ni un vaso de agua”.

De acuerdo con el censo que han logrado establecer líderes al interior de la comunidad habría **120 niños, 50 mujeres embarazadas, la mayoría de ellas adolecentes, 50 adultos mayores** y 120 adultos bajo extremas condiciones y permaneciendo aún calles aledañas a sus viviendas. Sin embargo, no han podido establecer el censo de las personas que estarían heridas porque no han retornado al lugar ni han tenido comunicación con ellas.

El Consejo de Bogotá anunció que **llamará a juicio al Alcalde Enrique Peñalosa por llevar a ecabo este desalojo sin tener un plan de contingencia** y re ubicación para las personas que les garantizará posibilidades de traslado y no el desplazamiento en el que se encuentran actualmente. Mientras que los habitantes están solicitando que se instaure un diálogo con las instituciones encargadas para resolver con urgencia la situación que afrontan.

Como parte de las ayudas que ya se han venido recogiendo desde las comunidades aledañas y los vecinosHoy se recogerán alimentos no perecederos, frasadas, colchonetas y medicamentos, en el salón comunal de Aures Uno, ubicado en la **calle 131 \#100-24, desde las 2 de la tarde hasta las 7 pm**,  ali Le puede interesar:["Desalojo del Bronx no tuvo una planeación integral"](https://archivo.contagioradio.com/desalojo-del-bronx-no-tuvo-una-planeacion-integral/)

<iframe id="audio_18359527" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18359527_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
