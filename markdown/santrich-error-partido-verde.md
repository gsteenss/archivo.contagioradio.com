Title: El mensaje no llegó como queríamos que llegara: Partido Verde
Date: 2019-06-13 18:23
Author: CtgAdm
Category: Entrevistas, Política
Tags: Alianza Verde, Congreso, Representante a la Cámara, santrich
Slug: santrich-error-partido-verde
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Partido-Verde-protesta-contra-Santrich.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @intiasprilla  
] 

El pasado miércoles en el Congreso, representantes a la cámara de la Alianza Verde fueron protagonistas de una protesta en medio de una plenaria en este recinto, al levantar letreros que decían "Paz sí, Santrich no,"durante la llegada de este integrante de FARC a la institución. **El representante Mauricio Toro** afirmó que se trató de un error de comunicación el elegir este mensaje, que fue interpretado como una forma de juzgar el caso de Santrich.

El Representante explicó que el mensaje que se quería dar es que a los congresistas no les corresponde juzgar a Seuxis Pausias Hernández Solarte, más conocido como Jesús Santrich, sino a la Corte Suprema de Justicia y que en su lugar, el deber de esa bancada es defender la paz. Como lo resumió Toro: **"Nosotros sí defendemos la paz, pero no nos corresponde juzgar, defender o atacar a Santrich porque eso le corresponde a las Cortes"**.

Según el Integrante de la Alianza Verde, además querían pedirle a Santrich que asista a los llamados de los órganos judiciales cuando se le requiera y acate las decisiones que allí se tomen, aunque **admitió que el exjefe guerrillero ha sido respetuoso de la justicia.** Adicionalmente, querian hacer un llamado a la Corte Suprema para que produzca resultados ágiles sobre el caso y que "evolucione rápidamente".

### **"Mientras la Corte decida si es culpable o no, tenemos que seguir legislando"**

Un acto similar ocurrió en la plenaria de la Comisión Séptima de la Cámara, cuando Santrich llegó a ejercer su labor como Congresista y el presidente de esa Comisión, Jairo Cristancho decidió levantar la sesión. En ese momento, Toro sostuvo que la reacción de su partido fue llamar a sus colegas a continuar con la agenda legislativa, "y mientras la Corte decida si es culpable o no, tenemos que seguir legislando".

De hecho, a raíz de la protesta realizada por los representantes de la Alianza Verde, el partido emitió un comunicado en el que recordaban que su trabajo como legisladores era discutir leyes y no juzgar a sus colegas; razón por la que sería eso lo que harían. (Le puede interesar: ["Congresistas denuncian que DEA violó la soberanía colombiana en caso Santrich"](https://archivo.contagioradio.com/denuncian-dea-violo-soberania-santrich/))

> Acá el comunicado y la posición del partido [@PartidoVerdeCoL](https://twitter.com/PartidoVerdeCoL?ref_src=twsrc%5Etfw) [\#DefendemosLaPaz](https://twitter.com/hashtag/DefendemosLaPaz?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/qTLBczjbEt](https://t.co/qTLBczjbEt)
>
> — León Fredy Muñoz (@LeonFredyM) [13 de junio de 2019](https://twitter.com/LeonFredyM/status/1139176413540622336?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **"La protesta fue un error de comunicación que tenemos que asumir"**

Para concluir, Toro consideró que la protesta fue una falta y un error de comunicación que como bancada tendrían que asumir, "porque el mensaje no llegó como queríamos que llegara y era este: nos corresponde a nosotros defender la paz y no juzgar al señor". (Le puede interesar: ["¿Qué viene para Santrich tras recuperar su libertad?"](https://archivo.contagioradio.com/que-viene-para-santrich-libertad/))

> Álvaro Uribe tiene al menos 10 procesos en la Corte Suprema y nadie se sale de ningún lado cuando llega.
>
> Y otros 42 congresista están investigados por la misma Corte.
>
> Acá necesitamos celeridad en todas la investigaciones, respetar la institucionalidad y no convertirse en jueces
>
> — Ángela María Robledo (@angelamrobledo) [12 de junio de 2019](https://twitter.com/angelamrobledo/status/1138951189473746944?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_37112097" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37112097_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
