Title: EPM debe dar orden de desalojo a comunidades aledañas al Río Cauca: Isabel Cristina Zuleta
Date: 2019-01-10 14:57
Author: AdminContagio
Category: Ambiente, DDHH
Tags: Hidroituango
Slug: epm-activo-alerta-roja-15-dias-despues-de-conocer-riesgos-de-avalancha-en-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [10 Ene 2018] 

Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos Antioquia manifestó a través de su cuenta de Twitter, que entidades como la Procuraduría General de la Nación y la Contraloría, ya habían alertado la generación de socavónes y cavernas en la construcción de Hidroituango y señaló que las** comunidades aguas abajo del Río Cauca están expuestas a graves riesgos debido a que EPM no ha emitido una orden de evacuación. **

El socavón se encontró el pasado 27 de diciembre, en la casa de máquinas, lugar que conecta el flujo de agua de la parte de arriba del Río Cauca, con la parte de abajo, razón por la cual según el Movimiento Ríos Vivos EMP **tendría que haber informado de inmediato a las comunidades que han retornado a sus territorios**, aun bajo la alerta roja de avalancha, y no esperar 15 días para dar a conocer la irregularidad.

"¿Por qué no hay orden de evacuación desde que supieron de las gigantescas cavernas? Que hacen las familias a las que presionaron para retornar, se devuelven..., para dónde? Sí hay alerta roja porque hay familias en la zona?", son algunas de las preguntas que ha formulado la vocera del movimiento.

### **Las advertencias de las entidades estatales** 

Durante el año 2018, la Contraloría General de la Nación advirtió a EMP sobre el peligro de socavaciones en la obra debido a la falta de previsión y manejo de la sedimentación propia del río Cauca, así como a los derrumbes presentados al interior de la montaña, hecho al que de acurdo con Zuleta se suma el estado de la Loma El Capitán, ubicada en el estribo derecho de la obra, que estaría carcomida por la presión del agua.

Asimismo un reciente informe llamado **¿Qué pasó, por qué pasó, qué está pasando y qué podría pasar?**, advierte sobre el delicado estado del macizo rocoso, que de ser sometido a más presión, fallaría y produciría un colapso total en la represa, ocasionando una avalancha. (Le puede interesar: ["Hay riesgo de avalancha en municipios aledaños a Hidroituango"](https://archivo.contagioradio.com/informe-revela-riesgos-de-un-desastre-mayor-en-hidroituango/))

Según el informe, el macizo rocoso de la zona sobre el que se han construido explanaciones, graderías, portales de entrada y de salida de túneles y galerías está altamente fracturado, **esto sumado al constante flujo de agua a altas presiones que atraviesa el  macizo rocoso lo hace más débil y susceptible a la desestabilización**.

EPM afirmó que en un plazo de 72 horas entregarán un informe detallado sobre el estado de la casa de maquinas y el socavón que les permitirá tomar decisiones definitivas sobre la represa, no obstante, para el Movimiento Ríos Vivos, estos hechos se pudieron prever, desde la puesta en marcha del proyecto.

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
