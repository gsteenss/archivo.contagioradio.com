Title: Uribismo pretende imponer y no negociar en el llamado Pacto Nacional
Date: 2016-10-03 12:24
Category: Entrevistas, Paz
Tags: impunidad, Jurisdicción Especial de Paz, paramilitarimo
Slug: uribismo-pretende-imponer-y-no-negociar-en-el-llamado-pacto-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/ramiro-bejarano-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [03 Oct 2016]

Los elementos sobre los cuales el uribismo pretende renegociar con las FARC es que los jefes vayan a la cárcel, que no participen en política, que haya una protección para los empresarios y un alivio judicial para la fuerza pública y esos temas serían imposibles de negociar, además Uribe le apostó a reubicarse políticamente después de una derrota regional en las pasadas elecciones, **por eso ese pacto está llamado al fracaso y está "montado con babas"** asegura Ramiro Bejarano[**.**]

Para el abogado, el uribismo no quiere la paz ni la negociación y lo que quiere es posicionarse políticamente, *“van a tener preso al presidente de aquí a las próximas elecciones”* y lo que van a tratar de llegar al poder en las próximas elecciones, además lo que habrá será guerra, puesto que, para él, las bases de las FARC podrían desconocer el mando del Secretariado y **también porque se cae la posibilidad de negociar con el ELN.**

Bejarano afirma que tal vez la salida que queda es adelantar una conversación sobre la base de que no se pueden imponer los [parámetros de la negociación](https://archivo.contagioradio.com/reunion-de-uribe-con-martinez-favorece-las-intenciones-politicas-del-super-ministro/), “una conversación para que de frutos tendría que **partir del supuesto de que el uribismo acepte que la negociación implica que las farc no necesariamente tengan que pagar prisión efectiva**”, para el abogado afirma que también se debe garantizar la participación en política.

Otros analistas han señalado que lo que está en juego es un verdadero "**pacto de impunidad"** en el que se garantice que los crímenes del paramilitarismo no sean investigados y que **acabe la independencia que está planteada para la [Jurisdicción Especial de Paz](https://archivo.contagioradio.com/?s=jurisdiccion+especial+de+paz)**, que incluso dejó sin poder para actuar sobre ella al actual fiscal Nestor Humberto Martínez.

Respecto de la presentación de la renuncia de Humberto de la Calle, Bejarano afirma que es un acto de responsabilidad política que casi nadie está acostumbrado a asumir en Colombia, sin embargo, el tema de las responsabilidades está por analizarse, aseguró también que la **renuncia protocolaria debería ser por parte de todo el gabinete ministerial,** pero resaltó como muy positiva la actitud del jefe del equipo negociador de paz.

<iframe id="audio_13163915" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13163915_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
