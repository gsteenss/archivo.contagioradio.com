Title: Fiscalía se niega a recibir denuncias por violaciones de DDHH en Sur de Bolívar
Date: 2017-04-20 13:22
Category: DDHH, Nacional
Tags: Casa de Barro, defensores de derechos humanos, Fiscalía
Slug: fiscalia-se-niega-a-recibir-denuncias-de-habitantes-de-casa-de-barro-en-el-sur-de-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/comité.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [20 Abr 2017]

Habitantes de las zonas el Arenal, Micohumado y Casa de Barro, afirmaron que la Fiscalía se estaría negando a recibir las **denuncias sobre integrantes de la Policía, el Ejército y la Sijín que ingresaron a las viviendas sin órdenes judiciales y hurtaron elementos de valor** y dinero, durante la captura de 12 líderes de estos territorios, el pasado 22 de marzo.

Así mismo buscaban **exponer las olas de asesinatos contra líderes sociales, las capturas masivas que se llevarían a cabo en los próximos días contra más miembros de estas comunidades y el control paramilitar**, que ponen en riesgo el trabajo social que estas personas han hecho en la defensa de los derechos humanos y la reconstrucción de la memoria. Le puede interesar: ["Así fueron las capturas de integrantes de Congreso de los Pueblos"](https://archivo.contagioradio.com/capturas-congreso-de-los-pueblos/)

La Comisión de Interlocución del Sur de Bolívar, Centro y Sur del César, en conjunto con la Cumbre Agraria Étnica y popular habían convocado la visita de una delegación conformada por integrantes del Ministerio del Interior, la Fiscalía y la Unidad Nacional de Protección a estos territorios en los días 19, 20 y 21 de abril. **Sin embargo, señalaron que la Fiscalía ha expresado que no hay garantías para ingresar a Casa de Barro.**

La defensora de derechos humanos Zoraida Hernández, indicó que esta afirmación se contrapone al hecho del ingreso el pasado 22 de marzo a esta misma zona para realizar las capturas “**desarrollaron todo un operativo para capturar a líderes y líderesas, justamente porque los están persiguiendo por su manera de pensar**”. Le puede interesar:["Fiscalía no tiene argumentos para mantener en presión a líderes del Sur de Bolívar"](https://archivo.contagioradio.com/fiscalia-no-tiene-argumentos-para-mantener-en-prision-a-lideres-sociales-del-sur-de-bolivar-defensa/)

Otra de las preocupaciones de las comunidades es la falta de presencia de la Defensoría del Pueblo y el acompañamiento durante estos procesos. Finalmente, Zoraida expresó que los habitantes están reclamando coherencia por parte de las instituciones Estatales “**estamos esperando coherencia en el discurso estatal y que preste reales garantías a defensores de derechos humanos**” y agrego que una de las formas para detener los asesinatos a estos defensores y líderes es acabar con la estigmatización hacía el trabajo que realizan.

<iframe id="audio_18253221" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18253221_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
