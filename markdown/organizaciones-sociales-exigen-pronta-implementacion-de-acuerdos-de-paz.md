Title: Organizaciones sociales exigen pronta implementación de Acuerdos de Paz
Date: 2016-11-23 14:19
Category: Nacional, Paz
Tags: #AcuerdosYA, comisión de no repetición, Sectores del No
Slug: organizaciones-sociales-exigen-pronta-implementacion-de-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/paz-col.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio] 

###### [23 Nov 2016]

Organizaciones sociales le pidieron al presidente Santos y a quienes entrarán hoy en lo que se ha denominado “el cónclave”, en un comunicado de prensa, que salgan de esta reunión solamente hasta que se concrete un mandato e implementación de los acuerdos de paz, esto debido a que desde el **2 de octubre, se han reportado en el país 7 asesinatos de líderes sociales y defensores de derechos humanos, y se ha atentado contra la vida de otras 4 personas.**

Estos hechos han encendido las alarmas y han puesto en alerta a organizaciones defensoras de derechos humanos y pertenecientes al movimiento social, ya que en tan solo 53 días  se suman a los asesinatos y atentados la desaparición forzada de **un líder estudiantil, amenazas contra 44 personas, y la detención arbitraria de 12 personas, para un total de 68 víctimas. Lo que significa cerca de dos hechos violentos contra líderes sociales por día.**

Para la defensora de derechos Humanos y miembro del Movimiento de Víctimas de Crímenes de Estado, Diana Gómez la importancia de la implementación de los acuerdos es urgente y debe ser rápida “estaremos actuando de manera activa y permanente para lograr que la implementación no lleve muchos meses, un periodo de 7 u 8 meses es mucho tiempo y de eso da cuenta **la vidas que se han cobrado este fin de semana por el estado de indecisión en el que están los acuerdos**”.

En el comunicado, las organizaciones señalan que estos hechos sistemáticos de violencia que se vienen originando son producto de los sectores del No, que pese a las modificaciones que se realizaron al acuerdo, siguen oponiéndose al mismo. Además indican que “**No queremos que se repita el genocidio político que padeció la Unión** **Patriótica** por defender la paz, ni la que han vivido cientos de colombianos y colombianas que le han apostado a la construcción de un país equitativo, silenciados por pensar distinto”.Le puede interesar: ["Uribismo busca dilatar y quitar tiempo a refrendación: Medina"](https://archivo.contagioradio.com/uribismo-dilatar-la-refrendacion-de-acuerdo-de-paz/)

Frente a los atentados y  asesinatos a defensores de derechos humanos, Diana Gómez afirma que están planteando la posibilidad de **crear una comisión de alto nivel, de garantías de no repetición** que tendría entre sus tareas garantizar condiciones para defensores de derechos humanos y líderes del movimiento social, esto de inmediato agrega Gómez significa el desmonte de estructuras paramilitares. Le puede interesar:["Estos son los líderes campesinos asesinados o perseguidos de Marcha Patriótica"](https://archivo.contagioradio.com/estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica/)

Mañana desde las 10:00 am diferentes organizaciones están convocando a realizar un plantón en la Plaza de Bolívar para acompañar la nueva firma de los acuerdos de paz y el próximo 29 de Noviembre en el mismo lugar se está convocando otro plantón para presionar al congreso en ser diligente con la implementación de los acuerdos.

###### Reciba toda la información de Contagio Radio en [[su correo]
