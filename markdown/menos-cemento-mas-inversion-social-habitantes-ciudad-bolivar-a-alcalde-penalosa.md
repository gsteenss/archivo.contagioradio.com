Title: "Menos cemento, más inversión social": habitantes Ciudad Bolívar a alcalde Peñalosa
Date: 2016-07-13 16:01
Category: DDHH, Nacional
Slug: menos-cemento-mas-inversion-social-habitantes-ciudad-bolivar-a-alcalde-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Peñalosa-en-Ciudad-Bolívar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía ] 

###### [13 Julio 2016] 

A través de un vídeo conocido como "Peñalosa no deja hablar y no quiere escuchar", la población discapacitada de Ciudad Bolívar denuncian que **el alcalde de Bogotá no permite que los habitantes le expresen sus inconformidades** y exigencias de aumentar la inversión social para esta localidad que en los próximos cuatro años se reduciría en por lo menos un 85%.

El pasado sábado, miembros del Consejo de Planeación Local de Ciudad Bolívar y otros ciudadanos, adelantaban un recorrido para diagnosticar temas estratégicos de movilidad en la localidad, cuando se encontraron en el barrio Juan Pablo II con el alcalde mayor a quien **interrogaron sobre el plan de desarrollo distrital y particularmente sobre la inversión social destinada para este sector de la capital**.

De acuerdo con pobladores como John Pachón, cuando intentaron dialogar con el alcalde, **el mandatario respondió de forma retadora, arrogante, con poca actitud de diálogo y dando la espalda**, así mismo varios de sus funcionarios agredieron a algunos de los habitantes, empujándolos y trataron de evitar que grabaran con celulares lo que estaba sucediendo.

Lo que se necesita son acciones integrales para la población y **no destinación de recursos para proyectos de construcción en zonas de alto riesgo**, como el que se contempla en el Parque Altos de la Estancia, asegura Pachón, e insiste en que las comunidades exigen espacios de participación en los puedan discutir la destinación de dineros públicos, por lo que piden al alcalde Enrique Peñalosa que escuche y respete sus demandas.

 

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio ]
