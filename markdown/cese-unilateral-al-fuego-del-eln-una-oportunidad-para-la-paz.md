Title: Cese unilateral al fuego del ELN, una oportunidad para la paz
Date: 2020-03-30 23:33
Author: AdminContagio
Category: Actualidad, Paz
Tags: cese al fuego unilateral, ELN, paz
Slug: cese-unilateral-al-fuego-del-eln-una-oportunidad-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Cese-al-fuego-unilateral-por-parte-del-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @AscamcatOficial {#foto-ascamcatoficial .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En un comunicado, el Ejército de Liberación Nacional (ELN) anunció un cese al fuego unilateral durante el mes de abril, considerando que el país atraviesa un momento difícil mientras enfrenta al Covid-19. Para defensores de derechos humanos, el cese es otra muestra de intenciones de paz por parte de la guerrilla, que podría conducir a un restablecimiento de la mesa de conversaciones con el Gobierno.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RGeneracionPaz/status/1244482983081336838","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RGeneracionPaz/status/1244482983081336838

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Lo que significa el cese al fuego decretado por el ELN**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El movimiento [Defendamos La Paz](https://twitter.com/DefendamosPaz/status/1244605984301088768) saludó el gesto del ELN, señalando que era una respuesta a los pedidos de comunidades rurales del país así como al llamado hecho desde la [ONU](https://twitter.com/ONUHumanRights/status/1244716665553723396). En el comunicado se enfatiza que este acto significa un alivio para poblaciones que están afectadas por la violencia y, puede ser el camino para pensar nuevamente en la salida negociada al conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Diana Sánchez, directora de la Asociación Minga e integrante de Defendamos la Paz sostiene que es relevante destacar la palabra 'unilateral' en el cese al fuego, porque el ELN nunca estuvo de acuerdo en dar pasos en solitario sino bilaterales, como se venían dando con el gobierno anterior. (Le puede interesar: ["Comunidades piden cese al fuego y atención del Gobierno a problemas en los territorios"](https://archivo.contagioradio.com/comunidades-piden-cese-al-fuego-y-atencion-del-gobierno-a-problemas-en-los-territorios/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En línea con la intención de bilateralidad, Sánchez destaca el llamado del ELN al Gobierno para intentar un nuevo acercamiento en Cuba con el que se puede acordar el cese de ambas partes a las hostilidades. Para Sánchez, este llamado, sumado a las recientes liberaciones que ha acompañado el [Comité Internacional de la Cruz Roja](https://www.icrc.org/es/document/colombia-liberacion-de-una-civil-en-arauca) ([CICR](https://www.icrc.org/es/document/colombia-liberacion-de-tres-civiles-en-cauca)) son gestos positivos en el sentido de afianzar el camino de la paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El ELN dio un paso, ¿y el Gobierno?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

A través del comisionado de Paz, Miguel Ceballos. el Gobierno dio a conocer que expidió una resolución en la que se designa a los ex integrantes del ELN, Francisco Galán y Felipe Torres, como promotores de paz. Con esta designación se espera que trabajen "con su conocimiento y experiencia en la estructuración de estrategias y acciones para la construcción de paz, la convivencia y la reconciliación”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Sánchez, esta acción permite generar condiciones para el acercamiento que pidió el ELN, y añade que sería saludable reconocer también como gestor de paz a Juan Carlos Cuellar, que actualmente está detenido. Pero el gesto, la defensora de [DD.HH.](http://www.comitedesolidaridad.com/es/content/saludamos-el-cese-al-fuego-acordado-por-el-eln-y-exhortamos-retomar-la-v%C3%ADa-de-la-soluci%C3%B3n) lo interpreta como una buena señal, que ojalá se corresponda con otras acciones para convocar nuevamente las conversaciones de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un último llamado al Gobierno en ese sentido de Sánchez es a que la Fuerza Pública también cesen sus hostilidades, entendiendo que esto permite enfrentar el Covid-19, así como detener las acciones violentas que ejercen otros grupos que hacen presencia en los territorios. (Le puede interesar: ["Delegación de paz del ELN rechaza captura de Juan Carlos Cuellar, Gestor de paz"](https://archivo.contagioradio.com/delegacion-de-paz-del-eln-rechaza-captura-de-juan-carlos-cuellar-gestor-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Comunidad internacional, un aliado incondicional de la paz**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La directora de la Asociación Minga considera como una barrera la forma en que Colombia ha manejado sus relaciones con Cuba recientemente, tomando en cuenta la abstención de votar contra el bloqueo económico sobre la isla entre otros hechos. No obstante, sostuvo que la diplomacia y la política son herramientas precisamente para solventar dichas dificultades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En consecuencia, la solicitud que hizo al Gobierno de Iván Duque es a reorientar la política internacional y la diplomacia hacia Cuba para recomponer las relaciones con un país que junto a Noruega, ha jugado un papel importante en la paz de Colombia. (Le puede interesar:["No se puede menosprecia el rol de Cuba en los procesos de paz del mundo](https://archivo.contagioradio.com/no-se-puede-menosprecia-el-rol-de-cuba-en-los-procesos-de-paz-del-mundo/)")

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
