Title: Llega el V Festival de las Memorias #SomosGenesis al Cacarica
Date: 2020-02-27 17:18
Author: CtgAdm
Category: Nacional
Tags: cacarica, Festival de las Memorias, Operacion genesis
Slug: llega-el-v-festival-de-las-memorias-somosgenesis-al-cacarica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/festival-de-las-mermorias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Del 28 de febrero al 1 de marzo, se realizará el quinto Festival de las Memorias que regresa a Cacarica, Chocó, para conmemorar a las víctimas de la Operación Génesis y dialogar con distintos responsables del conflicto armado, organizaciones y sectores sociales, sobre las garantías de no repetición y las propuestas de justicia restaurativa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este evento, que se hace por segunda vez en el marco de la conmemoración de \#SomosGenesis, es una iniciativa puesta en marcha por la Comisión de Justicia y Paz, que involucra a personas, comunidades afectadas, responsables  del conflicto armado, para tejer, lo que han llamado, una memoria restaurativa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta edición del festival, el foco principal estará en las garantías de no repetición, como eje fundamental para evitar la revictimización de las comunidades y la transformación de las mismas, a partir de las distintas propuestas que han hecho en pro de ,la justicia restaurativa, la memoria y la verdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para ello, se recorrerán lugares emblemáticos de memoria de las comunidades, como el monumento en homenaje a las víctimas de la operación Génesis, instalado en el coliseo de la ciudad de Turbo, Antioquia y el monumento "Respuestas" en la zona humanitaria de Nueva Vida en Cacarica, Chocó.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Festival de las Memorias, recorriendo la historia, la vida y la esperanza**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este festival, es el primero del año 2020 que realiza la Comisión de Justicia y Paz. Durante el año 2019, se celebraron en total 4 eventos, el primero de ellos se hizo en Cacarica (Ver video <https://www.youtube.com/watch?v=IzSUcL2Qn7s>) ; el segundo tuvo como lugar de encuentro el Resguardo Ambiental Humanitario So Bia Drua, Jiguamiandó, el terrcero en en Caño Manso-Camelias, Curbaradó (Ver <https://youtu.be/CIidQ3LhP7Y> ) y el cuarto en la Zona Humanitaria La Balsita en el Municipio de Dabeiba (Le puede interesar: «[Del tres al seis de octubre se realizará el tercer Festival de las Memorias en Caño Manso, Curbaradó](https://archivo.contagioradio.com/octubre-realizara-festival-de-las-memorias-en-cano-curvarado/)«).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunas de las propuestas de justicia restaurativa y de protección territorial que ya están en marcha tienen que ver con la Universidad de Paz que ya tiene un sede en la Zona Humanitaria Nueva Esperanza en Dios del territorio colectivo de Cacarica y CAVIDA y las propuestas de Bosques de la Memoria que desde hace varios meses se han instalado en las fincas de personas en la región del Bajo Atrato.

<!-- /wp:paragraph -->
