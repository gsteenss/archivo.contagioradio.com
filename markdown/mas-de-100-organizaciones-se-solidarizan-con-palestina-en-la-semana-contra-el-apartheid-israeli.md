Title: Más de 100 organizaciones se solidarizan con Palestina en la 'Semana Contra el Apartheid Israelí'
Date: 2016-04-18 12:55
Category: eventos, Nacional
Tags: BDS, Ocupación israelí, Palestina
Slug: mas-de-100-organizaciones-se-solidarizan-con-palestina-en-la-semana-contra-el-apartheid-israeli
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/palestina-solidaridad-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 Abril 2016 ] 

Por lo menos 170 organizaciones de la sociedad civil que trabajan por el fin del [[apartheid israelí](https://archivo.contagioradio.com/?s=gaza)] contra el pueblo palestino realizan "la semana contra el Apartheid". Las actividades lideradas por el movimiento global Boicot Desinversiones y Sanción a Israel (BDS) contarán con la presencia de **poetas, músicos y ponentes procedentes de Brasil, Palestina, Sudáfrica y Colombia**.

De acuerdo con Oscar Vargas, integrante de BDS, los eventos incluyen la entrega de una petición de más de 100 organizaciones internacionales para que el Congreso y la Corte Constitucional no ratifiquen el Tratado de Libre Comercio que Colombia negocia con Israel desde hace dos años, teniendo en cuenta que éste, además de ir **en contra de leyes internacionales, no representa beneficios para la economía nacional ni para el pueblo palestino**.

Según Vargas, este **TLC plantea que Israel puede comercializar todos los productos de los territorios controlados**, entre ellos los de las colonias en las que se han asentado ilegalmente por lo menos 800 mil israelíes, quienes además de utilizar todos los recursos naturales explotan laboralmente al pueblo palestino.

El evento académico tendrá lugar este viernes de 4 de la tarde a 8 de la noche, en la sede principal de la Universidad Distrital, en que se ahondará sobre las implicaciones del TLC, el régimen de apartheid que padecen los palestinos, y las acciones que desde Colombia se pueden adelantar para exigir, a través de medios no violentos, el **cese la ocupación de Israel en Palestina, que los refugiados palestinos retornen a sus territorios y que haya igualdad de derechos, tanto para israelíes como para palestinos**.

Artistas de distintos países se han movilizado para impedir que Israel se presente como un país que actúa conforme a las leyes, y en esta oportunidad poetas palestinos y músicos de Colombia se unirán para **exigir que no siga normalizándose el régimen de apartheid con Palestina**,** **a través de una jornada cultural que tendrá lugar este sábado desde las 4 de la tarde en la sede de Teatrova, teatro construido por un descendiente palestino que se radicó en nuestro país.

La campaña BDS fue conformada por 170 organizaciones entre sindicatos, estudiantes y ciudadanos, quienes en 2005 decidieron extender el llamado a la comunidad internacional para que se opusiera a la **desigualdad en la que viven israelíes y palestinos en el territorio que fue delimitado tras la guerra de 1967**.

[![BDS](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/BDS.jpg){.aligncenter .size-full .wp-image-22829 width="1014" height="579"}](https://archivo.contagioradio.com/mas-de-100-organizaciones-se-solidarizan-con-palestina-en-la-semana-contra-el-apartheid-israeli/bds/)

<iframe src="http://co.ivoox.com/es/player_ej_11208158_2_1.html?data=kpafkp2VeZmhhpywj5aYaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncbDnxMbfjbvFtsjV1JCajaeol46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

 
