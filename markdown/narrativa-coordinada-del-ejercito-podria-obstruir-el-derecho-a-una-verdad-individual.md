Title: Narrativa coordinada del Ejército podría obstruir el derecho a una verdad individual
Date: 2019-08-21 21:11
Author: CtgAdm
Category: Nacional, Paz
Tags: comision de la verdad, Ejército Nacional, JEP
Slug: narrativa-coordinada-del-ejercito-podria-obstruir-el-derecho-a-una-verdad-individual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Ejército-Nicacio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Este 21 de agosto se conoció una nueva directriz del **general del Ejército Nicacio Martínez** a la que tuvo acceso el diario El País, que daría pautas para la construcción de una narrativa única en el marco del conflicto armado colombiano para los militares que comparecerán ante la JEP y la Comisión, de la Verdad (CEV). El documento, con el nombre de Plan 002811, fechado el 13 de marzo de 2019,  no solo genera dudas sobre su incidencia en los relatos que los militares lleven ante el sistema integral de justicia sino que podría negar o manipular los hechos de los que también participaron integrantes de la Fuerza Armada.

Si bien el Ejército señala que se trata de "guiar la posición institucional en los espacios de verdad y memoria histórica”, para Alberto Yepes, de la Coordinación Colombia Europa Estados Unidos se trata de una preocupante directiva que trata de "imponer una versión de manera oficial", lo que buscaría eludir la obligación que tienen todos los que deben comparecer ante la JEP. [(Le puede interesar: Son 1.615 miembros de la Fuerza Pública los que aún no se han reportado ante la JEP)](https://archivo.contagioradio.com/son-1-615-miembros-de-la-fuerza-publica-los-que-aun-no-se-presentan-ante-la-jep/)

La Comisión de la Verdad, que comenzó sus tres años de mandato a finales de 2018, requiere la información que pueda aportar la Fuerza Pública para que sea contrastada y expuesta en el informe final que será entregado en 2021, por su parte, la JEP ha acogido a cerca de 2.000 militares quienes en el 99% de los casos están vinculados a ejecuciones extrajudiciales o desaparición forzada.

### "Ejército impondría un negacionismo oficial"

El Ejército dirigirá esta iniciativa a través de tres ejes, el primero que incluye las "líneas de contra-argumentación" que "constituirán una guía para las contribuciones que potencialmente hagan los miembros del Ejército Nacional en el marco del trabajo de la CEV".

El segundo eje, que destacará los “casos emblemáticos” de violaciones al Derecho Internacional Humanitario apoyándose en documentos y estadísticas de cómo grupos como la antigua guerrilla de las FARC afectó a cada jurisdicción militar y finalmente, el tercer eje que buscará visibilizar cómo el Ejército también ha sido víctima del conflicto armado y anexará un listado de las acciones sociales del Ejército en los territorios.

“El Ejército Nacional debe construir y reconstruir su verdad sobre los orígenes, causas, desarrollo e impacto del conflicto armado, para que le sea posible definir los referentes identitarios en relación con su accionar”, menciona uno de los apartes del documento, reflejando el interés de la Fuerza Pública en la memoria histórica del país.

Yepes advierte, que esta directriz es una obstrucción de un reconocimiento individual de verdad y que **"podría ser reemplazada por una verdad prediseñada"** por los altos mandos y que al acudir a estas pautas, todo a punta a que existe "un miedo profundo a la verdad".   [(Lea también: 12 casos de ejecuciones extrajudiciales en Casanare serán revisados por la JEP)](https://archivo.contagioradio.com/jep-revisara-12-casos-de-ejecuciones-extrajudiciales-en-casanare/)

Por su parte, desde el Ministerio de Defensa se expresó que dicha directriz se expidió desde que la antigua cúpula militar contaba con el general Alberto Mejía al mando del Ejército en noviembre del año pasado después de tener sostener un diálogo con el padre Francisco De Roux, presidente de la Comisión de la Verdad.

Cabe resaltar que a lo largo del 2019, el Ejército ha sido protagonista de diferentes polémicas que incluyen la orden firmada por Nicacio Martínez en la que pide aumentar las bajas en combate sin importar los resultados a cambio de incentivos y más recientemente las denuncias de corrupción al interior de este organismo militar.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"><iframe id="audio_40561918" frameborder="0" allowfullscreen="allowfullscreen" scrolling="no" height="200" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40561918_4_1.html?c1=ff6600"></iframe></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
