Title: Cumbre Agraria reactiva mesa única con Gobierno Nacional
Date: 2018-02-14 13:34
Category: Movilización, Nacional
Tags: asesinatos de líderes sociales, Cumbre Agraria
Slug: cumbre_agraria_reinstalacion_mesa_unica_gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Cumbre-Agraria.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Feb 2018] 

Este miércoles la Cumbre Agraria Campesina Étnica y Popular,  reinstaló la Mesa Única de Negociación con Gobierno Nacional en Bogotá. El objetivo esta vez, es que **las instituciones del Estado den respuestas claras sobre la protección de los líderes sociales y los autores de los asesinatos de más de 200 líderesas y líderes** que ha perdido el país en los últimos dos años.

El año pasado se realizaron varias actividades de protesta y movilización exigiendo al gobierno que cumpla con el pliego nacional de peticiones de la Cumbre Agraria. Desde junio del 2017 no ha funcionado la subcomisión de derechos humanos, y apenas hasta la semana pasada se logró una reunión e exploración con Ministerio del Interior, en el marco de la preocupación por los asesinatos de líderes sociales.

En octubre se llevó a cabo la última sesión de la mesa única donde se seguía discutiendo sobre temas estructurales del país tales como el problema de las tierras, los cultivos de uso ilícito, el modelo extractivista, la situación de derechos sociales, y las garantías para los derechos humanos.

De acuerdo con Jimmy Moreno, integrante de la Cumbre Agraria, esta vez la mesa se retoma ante la alerta por la situación de amenaza de diferentes líderes sociales en todo el país. (Le puede interesar[: Gobierno Santos tiene desinterés por la vida: Cumbre Agraria)](https://archivo.contagioradio.com/?p=37570)

### **Esperan verdadera voluntad política del gobierno** 

Este miércoles, el gobierno y la Cumbre iniciarán debatiendo sobre los cultivos de coca, marihuna y amapola  y en la tarde se trabajará en torno al aumento de asesinatos de líderes y defensores de derechos humanos. Allí se expondrán cifras como que **en 2016 fueron víctimas de estos hechos 114 personas, en 2017 hubo un total de 174 y a la fecha de este año ya se contabilizan 31, según datos de la Cumbre Agraria.**

Aunque el Gobierno creó equipos de investigación y mesas técnicas para esclarecer los homicidios de líderes y lideresas, para el integrante de la Cumbre Agraria "el tema tiene que ir más allá, debe haber una voluntad política real del para esclarecer los asesinatos y la conexión con los grupos paramilitares y la relación de estos  mismos grupos con la fuerza pública".

Asimismo, desde las organizaciones sociales en todo el país que hacen parte de la Cumbre, se trabaja en la construcción y fortalecimiento de las guardias campesinas, indígenas y cimarrones a través de las cuales buscan una protección colectiva y popular de los territorios. Estas mesas sesionarán en Bogotá durante los próximos 3 días, al final de los cuales se esperan respuestas claras y rutas concretas de acción.

<iframe id="audio_23785154" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23785154_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]{.s1}
