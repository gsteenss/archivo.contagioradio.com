Title: “El país está empezando a expresar su ansiedad por la paz”: Imelda Daza
Date: 2016-12-18 17:05
Category: Nacional, Paz
Tags: Congreso, FARC, paz, voces de paz
Slug: pais-esta-empezado-expresar-ansiedad-la-paz-imelda-daza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/voces-de-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Europa press 

###### [18 Dic 2016] 

Desde este lunes empezarán a asistir y participar, con voz pero sin voto, a Senado y Cámara los seis representantes de 'Voces de paz'. El movimiento que se encargará de velar por la implementación del acuerdo entre el gobierno colombiano y las FARC.

Gracias a la conformación de este movimiento, medios tradicionales están empezando a escuchar esas voces que antes no aparecían en la agenda mediática colombiana como lo expresa Imelda Daza, integrante del movimiento, **“Tradicionalmente hemos sido ignorados y desconocidos, hoy es una fortuna que los medios quieran escucharnos** La reacción de los medios me pareció extraordinariamente favorable”.

Daza resalta que **“el país está empezado a expresar su ansiedad por la paz”,** y por eso manifiesta que ve un ambiente favorable para la puesta en marcha de este movimiento que procura ser una herramienta más para llegar al fin de conflicto.

Desde 'Voces de Paz' se ha afirmado que en ningún momento la conformación de ese movimiento implica que estas seis personas hagan parte de las FARC, más bien señalan que coinciden con el interés político de las FARC frente a la necesaria implementación del acuerdo alcanzado entre las partes.

“Ellos quieren que en el congreso defendamos el acuerdo, que nada se vaya a modificar. **'Voces de paz' representa la implementación del acuerdo** y debe verse como un organismo garante de que eso se va a cumplir”, indica Imelda Daza, quien agrega que serán veedores de lo que se tramite en el Congreso donde se discutirán los proyectos de ley para implementar el acuerdo de paz.

Aunque aún hace falta que se concreten algunos aspectos logísticos para los voceros, los sillones de **Jairo Estrada Álvarez, Pablo Cruz y Judith Maldonado, en Senado; y los de Francisco Tolosa, Jairo Rivera e Imelda Daza, en Cámara** ya están listos. Al primer debate al que asistirán, será al de amnistía que fue presentado el pasado miércoles por el Gobierno.

<iframe id="audio_15171453" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15171453_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
