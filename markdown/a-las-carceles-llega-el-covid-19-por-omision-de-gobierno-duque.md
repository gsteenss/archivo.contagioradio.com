Title: A las cárceles llega el covid 19 por omisión de gobierno Duque
Date: 2020-04-13 18:49
Author: CtgAdm
Category: Actualidad, DDHH
Tags: colombia
Slug: a-las-carceles-llega-el-covid-19-por-omision-de-gobierno-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-13-at-3.20.24-PM.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-13-at-6.17.08-PM.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La llegada del covid- 19 a las cárceles del país empieza, con la muerte de tres [reclusos](https://archivo.contagioradio.com/fiscal-barbosa-esta-encubriendo-responsables-de-masacre-en-carcel-la-modelo-uldarico-florez/) en la cárcel de Villavicencio y el **positivo en la prueba de un guardia del INPEC en la cárcel Distrital**, confirma la llegada del Covid 19 a los centros penitenciarios. Situación que según el abogado Uldarico Florez, demuestra la omisión del gobierno de Duque en salvar las vidas de la población privada de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Florez, integrante de la Brigada Eduardo Umañana señala que el gobierno no atendió los llamados de los organismos de derechos humanos, del mismo Estado en cabeza de la Procuraduría y la Defensoria, y de los mismos prisioneros».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Duque no escuchó el llamado que le hacen desde las cárceles

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el caso de la Cárcel de [Villavicencio](https://archivo.contagioradio.com/muere-otro-interno-de-carcel-de-villavicencio-se-estudia-si-esta-relacionado-con-covid-19/) asegura que tendrá que iniciarse una serie de demandas al Estado por su *«grave omisión en prestar la prevención y protección a las personas privadas de la libertad».*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, asegura que la responsabilidad de estos contagios recaerán en la guardia del [INPEC](https://archivo.contagioradio.com/reclusos-en-la-picota-inician-huelga-de-hambre-por-llegada-de-covid-19-a-carceles/), porque son quienes continúan entrando y saliendo de la cárcel. En ese sentido manifiesta que debe iniciarse un control sobre la guardia y los contratistas para evitar la propagación del virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente expresa que las autoridades estatales deben actuar con inmediatez, de lo contrario su omisión podría provocar una masacre por la pandemia. (Le puede interesar: <https://archivo.contagioradio.com/inicia-protesta-pacifica-de-la-poblacion-reclusa-en-colombia/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Familias temen por la vida de reclusos

<!-- /wp:heading -->

<!-- wp:video {"src":"blob:https://archivo.contagioradio.com/10625f7e-daf1-4bae-8a34-24f49ca181e9"} -->

<figure class="wp-block-video">
<video controls src="blob:https://archivo.contagioradio.com/10625f7e-daf1-4bae-8a34-24f49ca181e9">
</video>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Entre las propuestas que había realizado el Movimiento Carcelario, para mitigar la pandemia, se encontraba la posibilidad de dar medida domiciliaria a algunas personas. Sin embargo, debido al hacinamiento, el riesgo de [contagio](https://www.justiciaypazcolombia.com/pandemia-y-derechos-humanos-en-las-americas/) en las cárceles es alto, y por lo mismo podría ampliarse a las familias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por este motivo reclusos de la cárcel de Valledupar exigen que se creen espacios de traslados para las y los reclusos que tengan síntomas del Covid 19. Mientras que las familias le piden a la Ministra de Justicia, Margarita Cabello que no condene a la población reclusa a una pena de muerte.

<!-- /wp:paragraph -->

<!-- wp:video {"id":83102,"src":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-13-at-3.20.24-PM.mp4"} -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-13-at-3.20.24-PM.mp4">
</video>
</figure>
<!-- /wp:video -->
