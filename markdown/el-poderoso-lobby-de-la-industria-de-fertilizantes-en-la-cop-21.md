Title: El poderoso lobby de la industria de fertilizantes en la COP 21
Date: 2015-12-02 11:40
Category: Ambiente, Entrevistas
Tags: Agricultura industrial, cambio climatico, COP 21, Gases de efecto inverna, GRAIN, Industria fertilizantes en la COP 21
Slug: el-poderoso-lobby-de-la-industria-de-fertilizantes-en-la-cop-21
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Grain.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: GRAIN ] 

###### [2 Dic 2015 ]

[De acuerdo con la organización internacional GRAIN las **empresas productoras de fertilizantes químicos**, responsables de la emisión del **10% de los gases que agudizan la crisis climática**, son quienes lideran la ‘Alianza de Agricultura Climáticamente Inteligente’ creada en el marco de la COP21 para discutir propuestas en torno a la agricultura y sus impactos en el cambio climático.]

[Al analizar las propuestas que en materia de agricultura se discuten en la COP 21, es evidente que la industria de fertilizantes no protagoniza iniciativas como la de esta Alianza, para aprobar la erradicación del uso de fertilizantes químicos, sino [para **proteger sus intereses económicos**](https://archivo.contagioradio.com/fertilizantes-una-de-las-incongruencias-rumbo-a-la-cop21/), asegura Henk Hobblink coordinador de GRAIN. ]

[El impacto que generan estos fertilizantes en el medio ambiente supera el de todos los carros que actualmente se movilizan en Estados Unidos, agrega Hobblink, e insiste en que **en la COP21 la discusión sobre la agricultura no está en el centro del debate** como debería ser, puesto que el **sistema alimentario industrial vigente aporta el 50% de los gases de efecto invernadero**, desde la siembra hasta la producción de alimentos, debido al uso de combustibles fósiles en todo el proceso.]

[Distintos movimientos campesinos han articulado propuestas que evidencian que el impulso de la **agricultura ecológica orientada a mercados locales**, reduce la cantidad de emisiones de gases de efecto invernadero, así como la presencia de C0~2~ en la atmosfera, afirma Hobblink anotando que “hay que volver a invertir en la agricultura campesina que es la que produce **cerca del 70% del alimento consumido en el mundo**”.]

[En las próximas semanas en Paris se reunirán delegaciones de distintas organizaciones campesinas del mundo junto con organizaciones sociales, para buscar articular sus propuestas alternativas para frenar el cambio climático.    ]
