Title: 419 violaciones a los DD.HH. en 5 meses a manos de paramilitares
Date: 2017-02-13 18:33
Category: DDHH, Nacional
Tags: Fiscal General, marcha patriotica, paramilitares
Slug: 419-violaciones-a-los-dd-hh-en-5-meses-a-manos-de-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Foto-Lider-asesinado-marcha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [13 Feb. 2017] 

**“En los últimos 5 meses las víctimas civiles, producto de la violencia socio-política se incrementaron exponencialmente”** así lo asegura un reciente informe publicado por Marcha Patriótica, en el que se analiza cada uno de los patrones bajo los cuales se ha producido la violencia contra líderes en el país luego de la firma de los acuerdos de paz en La Habana y alerta sobre el rearme de paramilitares.

**David Flores, vocero de Marcha Patriótica**, manifestó que si bien la mayoría de las agresiones registradas fueron contra ese movimiento político, existen otras expresiones sociales en el país que han sido afectadas, lo que se traduce en que **“estamos ante un fenómeno sistemático que tiene como agravante el asesinato, que en 2017 ha cobrado la vida de 23 líderes”. **Le puede interesar: [Con lista en mano paramilitares amenazan pobladores de Cacarica](https://archivo.contagioradio.com/paramilitares-amenazan-pobladores-cacarica-36208/)

Además, manifestó que “hay reaparición de la dinámica de panfletos en el país, del ingreso a casas de líderes y de organizaciones sociales para robar sus computadores, sus memorias usb y la información sensible de estas organizaciones”. Le puede interesar: [Nuevas amenazas a integrantes de Marcha Patriótica en Córdoba](https://archivo.contagioradio.com/nuevas-amenazas-a-integrantes-de-marcha-patriotica-en-cordoba/)

El informe asegura que logró documentar 317 violaciones a los DD.HH. por parte de grupos paramilitares y 102 infracciones al DIH y violaciones a los DD.HH. para un total de **419 violaciones en contra de los miembros y comunidades y organizaciones sociales y populares.**

Otro de los temas en los que hace énfasis el informe es en **el rearme paramilitar “estamos ante un fenómeno que evidenciamos va creciendo**, no solamente en número sino también en territorios. Hay nuevos departamentos que han registrado hechos donde anteriormente no los habíamos registrado” contó Flores.

Situación que, dice Flores, demostraría que **el Gobierno no viene tomando las medidas necesarias para que se detenga el paramilitarismo. **Le puede interesar: [Paramilitares arremeten contra diferentes regiones del país](https://archivo.contagioradio.com/paramilitares-arremeten-contra-diferentes-regiones-del-pais/)

### **Tres aristas del informe** 

Según el informe de Marcha Patriótica, existen 3 aristas desde donde se puede analizar la situación que vive el país en la actualidad:

1.  **Hay actores de extrema derecha que a través de sus actos intentan echar atrás los avances del proceso de paz con las FARC** y en términos generales, la perspectiva de lo que piensan los colombianos hoy frente a la necesidad de una salida pacífica al conflicto y no mediante la guerra.
2.  **Hay una intención de grupos paramilitares por tomar los territorios donde antes hacía presencia armada las FARC**, e imponer un orden social y político que encuentran en los líderes sociales una clara amenaza frente a ese orden que se quiere imponer.
3.  **Hay una serie de factores de la economía ilegal**, ligados a la minería, al narcotráfico y a la captura de redes que está viendo en la salida de las FARC la posibilidad de ampliar sus negocios.

"Todos estos elementos están ligados, es decir existe una relación de agentes del estado con grupos ilegales y con empresariales” asevera Flores. Le puede interesar: [Paramilitares amenazan a defensor de DH de Marcha Patriótica](https://archivo.contagioradio.com/paramilitares-amenazan-a-defensor-de-dh-de-marcha-patriotica/)

Marcha Patriótica añade que estos sucesos que acontecen en el país no son una actuación de “bandolas”, como lo ha querido manifestar la institucionalidad o hechos aislados sino que “hay sectores que están en **la institucionalidad, que siguen respaldando el paramilitarismo y creemos que debe haber una acción mucho más concreta”**

### **Falta de interés del Fiscal Néstor Humberto Martínez** 

Flores denunció en Contagio Radio que si bien han visto voluntad política en ciertos sectores para tratar el tema de asesinatos, persecución a líderes sociales y el proceso de paz, en otros no lo hay y menciona puntualmente al actual Fiscal General. Le puede interesar: [Presencia Paramilitar impide llegada de FARC-EP a zona veredal](https://archivo.contagioradio.com/presencia-paramilitar-impide-llegada-de-farc-ep-a-zona-veredal/)

**“Vemos en el fiscal Néstor Humberto Martínez un interés claro de sabotear el proceso de paz, de no adelantar investigaciones.** No se quiere poner a andar las instituciones que se crean en el acuerdo de paz para combatir el fenómeno del paramilitarismo”.

Según Flores, **el Fiscal no se ha querido reunir con Marcha Patriótica “es la única entidad del Estado que no se ha reunido con nosotros.** Nos hemos reunido con Defensoría, con los militares, con el Presidente, con el Procurador, pero el único que no ha querido es el fiscal y se supone que es esa entidad la que investiga y da los resultados en cuanto a los asesinatos de líderes”.

### **Propuestas** 

Algunas de las propuestas que han realizado desde Marcha Patriótica son, por ejemplo, el **incremento del trabajo de sensibilización con la sociedad, medidas de autoprotección campesina** tomando como guía a las guardias indígenas y campesinas.

Así mismo, desde el movimiento **Marcha Patriótica, han radicado una solicitud de medidas cautelares ante la CIDH**, como estrategia para salvaguardar la vida de sus integrantes.

Flores concluye diciendo que no van a dejar retroceder la paz, ni se van a dejar amilanar **“creemos en un país en paz, sobre esa base no podemos declinar sobre lo que queremos** la mayoría de los colombianos y los líderes que han sido asesinados que es la paz”. Le puede interesar:[ "No les vamos a dar el gusto de desfallecer" Marcha Patriótica](https://archivo.contagioradio.com/cuanto-mas-podra-resistir-marcha-patriotica/)

<iframe id="audio_17002175" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17002175_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
