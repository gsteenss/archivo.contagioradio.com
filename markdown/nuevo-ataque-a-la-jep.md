Title: Nuevo ataque a la JEP
Date: 2020-06-13 16:01
Author: AdminContagio
Category: Camilo Fagua, Columnistas invitados, Opinion
Tags: #YoDefiendoLaJEP, Centro Democrático, JEP, uribismo
Slug: nuevo-ataque-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/D2iaQgZWkAAcYF7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: Pares.com {#foto-de-pares.com .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Uribismo le hace una nueva embestida a la[JEP](https://www.jep.gov.co/Paginas/Inicio.aspx).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por tercera vez el partido de gobierno intentará fracturar el componente de justicia del Acuerdo de Paz, esta vez buscan revivir un debate ya saldado con relación a la comparecencia de miembros de la fuerza pública vinculados con crímenes ocurridos en el contexto del conflicto armado. Los argumentos  centrales de esta *“nueva”* propuesta de reforma se centran en dar una Sala especial para el juzgamiento de militares y que a su vez exista un profundo conocimiento del Derecho Internacional Humanitario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Flaco favor le hacen a los cientos de militares que tienen en el Sistema Integral, la única oportunidad para que el país conozca lo ocurrido durante el conflicto y satisfacer el derecho a la verdad de miles de familias que hoy piden conocer el paradero de sus hijos desaparecidos y develar la política de los “*Falsos positivos”.* Estos nuevos cantos de sirena dejarán sin seguridad jurídica a agentes estatales que han estado bajo la lupa de la Corte Penal Internacional, y que tal  como lo reiteró recientemente la Fiscal de la Corte Penal Internacional Fatou Bensouda, no parará el  examen preliminar sobre Colombia que adelanta el tribunal internacional desde 2004.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El otro argumento, además de contradictorio, es aquel que clama por la aplicación fehaciente del Derecho Internacional Humanitario, pues no deja de ser paradójico que hoy los representantes de la política que ha intentado negar por casi veinte años la existencia del conflicto interno quieran acogerse a los estándares del derecho internacional. Hay que recordarles a los congresistas del Centro Democrático que uno de los principales blindajes del Acuerdo de Paz y particularmente de su sistema de Verdad, Justicia y Reparación,  es que este mismo se fundamente y se obligue a aplicar  los estándares del DIH y el Derecho Internacional de los Derechos Humanos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Al Uribismo se le olvida muy rápido todas las movidas que hicieron para copar los cargos públicos ofertados para la nueva jurisdicción, eso sin contar que varios funcionarios que hoy integran la Justicia para la Paz realizaron asesorías y ejecutaron contratos en los gobiernos del ex presidente Uribe.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Le puede interesar:[La frustrante JEP](https://archivo.contagioradio.com/frustrante-jep-mancuso/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo nutrido, selecto y variopinto de la magistratura cuenta con perfiles como el de la magistrada Claudia Rocio Saldaña con más de 18 años de experiencia dentro de la Justicia Penal Militar, que integra la Sala de Definición Jurídica y se encargará de definir la situación de  las personas de la fuerza pública que hayan cometido conductas que tengan relación directa o indirecta con el conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta reforma no tendrá mucho futuro en la legislatura que se abre el próximo 20 de julio. La Corte Constitucional, luego del  análisis de constitucionalidad del Acto Legislativo de 2017 en octubre de ese mismo año, el alto tribunal determinó que  conforme a los principios de buena fé, estabilidad y seguridad, el Estado colombiano está en la obligación de cumplir el Acuerdo en su integralidad por 12 años consecutivos, pues la Paz no puede ser sometida al capricho y las circunstancias políticas de cada gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que se esconde detrás de esta nueva cortina de humo es la intención  de desviar la atención respecto de la avalancha que hoy supera más de 70 decretos expedidos por el gobierno del presidente Duque durante la emergencia, que van desde el aislamiento social hasta las modificaciones en derechos laborales como la fragmentación de la prima de servicios del mes de junio para los trabajadores e irregularidades en la ejecución delos  dineros destinados para la Paz. Ni que decir, de la flagrante violación a la Constitución, donde aprovechando la imposibilidad del Senado de la República de ejercer el control político, el presidente permite el tránsito de tropas militares norteamericanas en una clara trasgresión a nuestra soberanía nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Vea mas de nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
