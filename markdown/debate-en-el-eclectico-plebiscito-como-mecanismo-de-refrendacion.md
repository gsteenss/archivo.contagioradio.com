Title: Debate en el Ecléctico "Plebiscito como mecanismo de refrendación"
Date: 2016-07-30 10:00
Category: El Eclectico
Tags: Acuerdos de paz en Colombia, Opinión paz en Colombia, plebiscito por la paz, refrendación acuerdos de paz
Slug: debate-en-el-eclectico-plebiscito-como-mecanismo-de-refrendacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/images_cms-image-000032284.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: El Heraldo 

##### [25 Jul 2016] 

Para el cuarto debate en 'El Ecléctico",  nos acompaño el representante a la Cámara por el Polo Democrático Victor Correa, el asesor legislativo y exdirector de juventudes del Centro Democrático Hernán Cadavid, la politóloga y estudiante de derecho Tania Lopez, quien además fue directora de Juventudes con Marta Lucía Ramírez en las pasadas elecciones presidenciales, Nicolás Riaño, estudiante de derecho e integrante del Movimiento Contra la Rosca y columnista de El Ecléctico, y Marcela Vargas, licenciada en Ciencias Sociales que trabaja en temas de investigación sobre paz y en la implementación y desarrollo de la cátedra para la paz.

En esta emisión los invitados discutieron el hecho de que el plebiscito haya sido el mecanismo elegido para refrendar los acuerdos de paz firmados entre el gobierno de Colombia y la guerrilla de las Farc en La Habana. Algunos cuestionaron este mecanismo y afirmaron que habrían preferido que se hubiera optado por un referendo.

Hernán Cadavid cuestionó el hecho de que el umbral de participación establecido para este plebiscito sea del 13%, lo que, como dijo él, le resta toda legitimidad a la refrendación pues se necesitan alrededor de cuatro millones y medio de participantes para alcanzar el umbral, en un país de más de 40 millones de habitantes.

Por su parte el representante Víctor Correa manifestó que el mecanismo de refrendación no era un requisito para la implementación de los acuerdos y señaló que incluirlo es una muestra de la voluntad política de las partes para involucrar a la ciudadanía.

Todos ellos convinieron en afirmar que es necesario que los colombianos que conozcan los acuerdos alcanzados, que los lean y se informen de cuáles son las implicaciones que traería para el país implementar estos acuerdos, para después poder tomar una decisión respecto de votar sí o no en el plebiscito.

<iframe src="http://co.ivoox.com/es/player_ej_12368150_2_1.html?data=kpegmJ2VeZGhhpywj5WVaZS1kpaSlaaWfY6ZmKialJKJe6ShkZKSmaiRdI6ZmKiah5eWlM3Zw87gxc7Ys4zX0NLcjdLJp8Liytja0ZDIqYzmxsvfx9PIpcTdhqigh6eXsoamk5CajanJpsLoxpDSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
