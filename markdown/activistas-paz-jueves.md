Title: Activistas por la paz se dan cita este jueves para hablar sobre negociación con ELN
Date: 2019-01-23 12:40
Author: AdminContagio
Category: Movilización, Paz
Tags: ELN, guerra, paz, Paz a la Calle
Slug: activistas-paz-jueves
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-22-a-las-12.03.25-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paz a la Calle] 

###### [22 Ene 2019] 

El próximo **jueves 24 de enero a las 5:30 pm se darán cita los ciudadanos en Bogotá para pedir que se continúe la mesa de diálogos entre el Ejército de Liberación Nacional (ELN)** y el Gobierno, tal como ocurrió hace algo más de 2 años, activistas por la paz de diversos sectores se darán cita en el **ParkWay**, frente a la estatua del almirante Padilla para evaluar las acciones ciudadanas que respalden el proceso de paz con la mencionada guerrilla.

Andrés Camacho, uno de los activistas por la paz que ha activado este llamado a la ciudadanía afirmó que el encuentro en el ParkWay no ha sido pensado como una re-edición de PazALaCalle, pese a que en su análisis, **la situación es muy parecida a lo ocurrido en 2016 con el plebiscito**: Se puede estar escapando la posibilidad de la paz. (Le puede interesar: ["Paz a la Calle, iniciativa de los jóvenes por los Acuerdos de La Habana"](https://archivo.contagioradio.com/no-somos-una-patria-boba-y-vamos-a-demostrarlo-pazalacalle/))

Camacho sostuvo que en esta ocasión, y tras la marcha de rechazo al terrorismo en la que se evidenció la distancia entre posiciones políticas respecto al proceso de paz que hay en Colombia, los jóvenes serán nuevamente quienes deban salir a las calles para **manifestarle al Gobierno la necesidad de una salida negociada al conflicto, y pedirle al ELN que lea adecuadamente el momento político.** (Le puede interesar:["Colombia no puede omitir el DIH ni violar los protocolos de la mesa con el ELN"](https://archivo.contagioradio.com/colombia-no-puede-omitir-el-d-i-h-ni-violar-los-protocolos-de-la-mesa-con-eln/))

El activista expuse que, justamente, tras la muestra de la polarización que vive el país, es momento para que los jóvenes muestren que ésta es la generación de la paz, y vuelvan a hablar con los sectores del "no" para abrir espacios de debate públicos sobre la paz. De esta manera, esperan promover la idea de que a diferencia de lo planteado en la Marcha contra el terrorismo, la salida a la situación de violencia es eliminar la guerra, no el proceso de paz. (Le puede interesar: ["Mujeres víctimas piden no cerrar la puerta del diálogo con el ELN"](https://archivo.contagioradio.com/queremos-la-paz-lideresas-comunitarias-piden-al-gobierno-no-cerrar-puerta-al-dialogo-con-eln/))

> No podemos permitir que nos impongan la agenda del odio y la guerra. Actuemos para evitar la violencia perpetua, Colombia merece la paz.
>
> Nos vemos este jueves a las 5.30 PM en el Park-Way en Bogotá.
>
> Repliquemos esta iniciativa en todo el país.[\#ParaLaGuerraNada](https://twitter.com/hashtag/ParaLaGuerraNada?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/L5pdnu1aoZ](https://t.co/L5pdnu1aoZ)
>
> — Diego Carrero ?? (@DiegoCarreroB) [21 de enero de 2019](https://twitter.com/DiegoCarreroB/status/1087387714071216128?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
