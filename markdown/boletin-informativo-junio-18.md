Title: Boletín informativo junio 18
Date: 2015-06-18 17:25
Category: datos
Tags: ACNUR desplazamiento en Colombia, Enciclica Papal Medio ambiente, Festival Anti Mili Sonoro en Bogotá, Marcha Dignidad del Pensionado Colombiano, Nuevo Código de Policia en Colombia, Robinson Masso
Slug: boletin-informativo-junio-18
Status: published

*[Noticias del Día:] *

<iframe src="http://www.ivoox.com/player_ek_4659925_2_1.html?data=lZuim56WeY6ZmKiakpmJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZaccYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-La **marcha por la Unidad y Dignidad del Pensionado Colombiano**, que partió el pasado 8 de junio desde la ciudad de Cali, l**legó en la mañana de este jueves a Bogotá**, con el objetivo de presentar un **pliego petitorio** al presidente Juan Manuel Santos ante la critica situación que atraviesa el sector pensional, así como los adultos mayores del país,. Habla **Robinson Masso presidente de la Corporación de jubilados y pensionados del Cali**.

-Este jueves se publicó la **encíclica del Papa Francisco** sobre la temática ambiental, donde **acusa a las multinacionales y los gobiernos** de ser causantes del cambio climático, debido a “**el uso desproporcionado de los recursos naturales**”.

-En el Congreso de la República se debate el nuevo **Código de Policía**, que ha generado bastante polémica debido a que diversos sectores de la sociedad **denuncian que se trata de un Código de Policía autoritario y dictatorial**, por medio del cual las autoridades podrían abusar de su poder, lo que conllevará a la **violación de derechos humanos** como lo asegura **Alberto Yepes**, integrante de la **Coordinación Colombia Europa Estados Unidos**.

-Este jueves, se publicó un nuevo **informe** de la Agencia de la ONU para los refugiados, **ACNUR**, sobre la situación de **desplazamiento forzado en el mundo**. De acuerdo a este, el **2014** fue el año en el que se alcanzó el **más alto nivel de población desplazada** con **59,5 millones de víctimas**. Por su parte Colombia ocupa el segundo puesto entre los países con mayores índices de personas desplazadas con 6 millones.

-En una jornada llena de arte, cultura y música reggae, rap, rock y punk, se llevará acabo este **jueves 18 y viernes 19 de junio** la 4ta versión del **Festival Anti Mili Sonoro** en Bogotá, un esfuerzo de organizaciones juveniles, antimilitaristas y objetoras de conciencia por **visibilizar los procesos de resistencia** **contra la guerra** y el militarismo en Colombia. Habla **Maria Camila**, del **Proceso Distrital de Objeción de Conciencia**.
