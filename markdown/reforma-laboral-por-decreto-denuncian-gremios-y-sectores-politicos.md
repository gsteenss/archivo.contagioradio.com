Title: «Reforma laboral por decreto» denuncian gremios y sectores políticos
Date: 2020-09-01 21:49
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Decreto 1174, Decreto 1174 de 2020, Precarización laboral, Reforma laboral
Slug: reforma-laboral-por-decreto-denuncian-gremios-y-sectores-politicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Reforma-laboral-por-decreto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 27 de agosto el Gobierno Nacional a través del Ministerio de Trabajo expidió el Decreto 1174 de 2020, el cual ha sido señalado por centrales obreras y algunos sectores políticos como una **«reforma laboral por decreto»** que busca precarizar y desmejorar las condiciones laborales de los trabajadores. (Le puede interesar: [Sí hay recursos para Avianca pero no para sectores más vulnerables](https://archivo.contagioradio.com/si-hay-recursos-para-avianca-pero-no-para-sectores-mas-vulnerables/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las principales críticas que ha recaído sobre el Decreto es el hecho de que reglamenta el acceso al sistema general de seguridad social a personas que perciben ingresos inferiores a un salario mínimo legal, por lo cual, según aseguran algunos sectores, **lo que se estaría haciendo es institucionalizar un ingreso por debajo de los mínimos legales establecidos en la normatividad laboral.** (Le puede interesar: [Menos impuestos para las empresas, menos salario para los trabajadores](https://archivo.contagioradio.com/menos-impuestos-para-las-empresas-menos-salario-para-los-trabajadores/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1300800578679791617","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1300800578679791617

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Otro de los serios cuestionamientos que ha recaído sobre la norma, es el hecho de que prevé la contratación parcial y por horas de los trabajadores, lo cual, según el senador Wilson Arias, hará que se generalice esa forma de contratación, tal como sucedió en su momento con el contrato de prestación de servicios, el cual ha sido utilizado por muchos empleadores para desconocer y evadir su obligación de realizar los pagos por concepto de seguridad social a sus empleados, a quienes vinculan irregularmente con esta figura, como trabajadores «independientes»; poniendo en su cabeza la responsabilidad de costear los aportes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Arias, agregó que con este Decreto **una persona que cotice por horas «*nunca adquiriría el derecho pensional*»**, lo cual tendría también, una seria repercusión sobre el fondo de pensiones público Colpensiones.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1300852946125680642","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1300852946125680642

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Adicionalmente, se alertó que con base en el segundo artículo del Decreto, **los usuarios de este régimen son remitidos al Sistema Subsidiado de Salud, en el que, por su misma naturaleza, no existen licencias de maternidad o paternidad, ni pago de incapacidades por enfermedad.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JERobledo/status/1300886771585159169","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JERobledo/status/1300886771585159169

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Centrales obreras demandarán el Decreto y lo consideran **«**una reforma laboral a cuenta gotas**»**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Julio Roberto Gómez, presidente de la Confederación General del Trabajo -[CGT](https://twitter.com/CGTCol)-, señaló que era inapropiado el método de **utilizar un Decreto para «*ir implementando una reforma laboral a cuenta gotas*»**, especialmente en el marco de la actual pandemia que ha generado la pérdida de millones de empleos y una desmejora notable en la situación de la clase trabajadora. (Le puede interesar: [Con dos sendos fallos, Corte Constitucional pone freno al «Gobierno por Decreto»](https://archivo.contagioradio.com/con-dos-sendos-fallos-corte-constitucional-pone-freno-al-gobierno-por-decreto/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el dirigente gremial, aseguró que **el Gobierno quiere aprovechar el difícil acceso al empleo y la necesidad de las personas, que se ha agudizado con la pandemia, para que acepten las precarias condiciones que formula el Decreto.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «Si hoy en día, laborando 8 horas diarias, 48 a la semana, se imposibilita cada vez más la posibilidad de acceder a una pensión de jubilación \[…\] cómo será ahora, que lo usual en las empresas será la contratación de la gente por horas»
>
> <cite>Julio Roberto Gómez, presidente de la Confederación General del Trabajo -[CGT](https://twitter.com/CGTCol)-</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otra parte, agregó que la naturaleza del Decreto hacía necesario que su contenido, se discutiera en la Comisión Permanente de Concertación de Políticas Salariales y Laborales; y que **desde las centrales obreras lo van demandar por considerarlo «*abiertamente inconstitucional*».**

<!-- /wp:paragraph -->
