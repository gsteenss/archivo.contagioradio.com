Title: Francia inaugura carretera pavimentada con energía solar
Date: 2016-12-23 22:40
Category: Ambiente, El mundo
Tags: energía solar, francia
Slug: francia-inaugura-carretera-pavimentada-energia-solar
Status: published

###### Foto: EFE 

###### [23 Dic 2016] 

Una vez más Francia le demuestra al mundo que es posible implementar prácticas amigables con el ambiente. Este jueves inauguró la primera carretera solar, **con un área total de 2.800 metros cuadrados**, cuya energía mantendrá el alumbrado público de 5.000 habitantes.

Con un costo de 5 millones de euros, la carretera de un kilómetro está conformada por paneles solares que alumbrarán un sector de Normandia, cerca del pueblo Tourouvre-au-Perche. La vía fue construida para soportar el peso de cualquier vehículo.

De acuerdo la ministra de Ambiente de Francia,  Ségolene Royal, de ser exitoso ese primer tramo, más adelante se instalarán mil kilómetros de placas solares en las carreteras francesas.

Sin embargo, organizaciones ambientalistas y expertos aseguran que **el Gobierno busca un efecto sin auténticos progresos con este tipo de obras. Así mismo, se ha criticado e**l costo del proyecto, que hubiese sido menor si los paneles hubieran sido instalados en otras zonas de ese país donde existan más días de sol.

[**En**] **Estados Unidos** y en **Holanda** funciona desde hace un año un tramo de carril exclusivo para bicicletas, pero la carretera inaugurada en Tourouvre-au-Percheen es la primera en todo el mundo que será usada para vehículos.

###### Reciba toda la información de Contagio Radio en [[su correo]
