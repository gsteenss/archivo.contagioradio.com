Title: BOLETÍN INFORMATIVO ABRIL 29
Date: 2015-04-29 17:23
Author: CtgAdm
Category: datos
Tags: Annelen Nicut, Baltimore, Boko Haram, Carlos Celorio, Corte Internacional de justicia de la Haya, de la Red de comunidades Construyendo Paz, Detroit, Liliana Gómez, maria alejandra rojas, Ministro de defensa, Putumayo, Sandra Lagos, Unión europea, Universidad Nacional de Colombia, zonas de reserva campesina
Slug: boletin-informativo-abril-29
Status: published

[Noticias del día:]

<iframe src="http://www.ivoox.com/player_ek_4424882_2_1.html?data=lZmflp2cdo6ZmKiakpqJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjabGtsrgjJemj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Las comunidades rurales del **Putumayo** que se ven afectadas por las fumigaciones con el glifosato, aseguran que han presentado varias propuestas alternativas para acabar con los cultivos ilíctios, pero no hay voluntad del gobierno para acabar con esta problemática. Habla **Sandra Lagos** de **Zonas de Reserva Campesina** en **Putumayo**.

-Pese a que el **Ministro de Defensa** se mantiene en su desición de continuar con las fumigaciones con **glifosato**, las comunidades rurales denuncian que su salud se ha visto afectada por el uso del herbicida. **Carlos Celorio**, de la **Red de comunidades Construyendo Paz**, relata la situación en **el Naya**.

-Algunos estados de la **Unión Europea** podrían prohibir que las personas homosexuales donen sangre, la inciativa fue puesta a consideración por parte del Parlamento y ya se ha aplicado en Francia en donde se prohibió a un médico realizar la donación. Analistas afirman que con medidas como esta se profundizan las expresiones xenófobas en varios países europeos.

-Organizaciones de Mujeres de Colombia presentaron un informe ante la **Corte Internacional de justicia de la Haya**, en la que demuestran la persistencia de la impunidad en los crímenes sexuales contra mujeres cometidas por las FFMM, **Annelen Nicut** presenta las características del informe.

-**FFMM** de Nigeria liberaron a 293 mujeres que se encontraban en poder de la milicia de **Boko Haram**, Sin embargo ninguna de las víctimas hace parte del grupo de niñas retenidas a finales del año pasado. La indignación creció en las familias de las menores por la respuesta del gobierno que les sugiere escuetamente... no perder la esperanza.

-Ya se cumplen 3 semanas desde que los y las trabajadoras de la **Universidad Nacional de Colombia** se declararan en paro, ante lo que consideran, son incumplimientos por parte de la administración en el incremento de salarios de manera equitativa al interior de la institución. Estudiantes y profesores decidieron tomarse el edifico de la rectoría como lo describe **María Alejandra Rojas**, representante estudiantil.

-Persiste la indignación de los ciudadanos y ciudadanas en **Baltimore** Estados Unidos, sin embargo se presentó un nuevo asesinato de un afrodescendiente en la ciudad de **Detroit** en manos de la policía. Analiza **Liliana Gómez** integrante del movimiento de los indignados en EEUU.
