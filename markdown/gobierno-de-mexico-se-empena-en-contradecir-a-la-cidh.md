Title: Gobierno de México se empeña en contradecir a la CIDH
Date: 2015-10-09 15:17
Category: El mundo, Movilización
Tags: 43 desaparecidos, Ayotzinapa, CIDH, Fiscalía Mexicana, impunidad, mexico, Procuraduria Gneral de la República de México, Salvador Cienfuego
Slug: gobierno-de-mexico-se-empena-en-contradecir-a-la-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ayotzinapa1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:elnoticieroenlinea.com 

<iframe src="http://www.ivoox.com/player_ek_8890429_2_1.html?data=mZ2mkpmWfY6ZmKiak5iJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPqMafroqwlYqlfdndxNSY1cqPqc7kxoqwlYqmdcKfxtOYxdTSuNPVxcrQy9ePpYzgwpCwq6mscYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Juan Carlos Gutiérrez, Coordinador General de IDHEAS AC] 

###### 9 oct 2015 

Después de que el grupo de la CIDH evidenciara en su informe la crisis en derechos humanos que actualmente **vive México, la Procuraduría General de la República (PGR)**, convocó una nueva comisión de siete** expertos independientes internacionales**, por seis meses para que nuevamente se investigue sobre el caso de los 43 jóvenes desaparecidos de la escuela rural de Ayotzinapa.

Este anuncio se da después de que la OEA respaldara el informe que presentó la CIDH, lo que para Juan Carlos Gutiérrez, Coordinador General de IDHEAS AC - litigio en derechos humanos de México, representa que la Procuraduría "no acepta en varios apartados el informe, y es una propuesta de cuestionar el informe al proponer otro nuevo peritage”.

El anterior grupo de expertos indicó que esta investigación ha dejado más de “**7 procesos abiertos, con más de 110 personas detenidas”** , pero “no ha tenido una línea clara de investigación”.

Al renovar el grupo de expertos por seis meses más se trataría entonces de una decisión “contradictoria” ya que tendrían la función de “desvirtuar lo que se dijo antes”, según señala Gutiérrez, quien añade que es **“preocupante nombrar de forma unilateral sin consultar a los expertos de la OEA**” o “sin consultar a las víctimas o abogados".

Tras el informe de la CIDH, la **Procuraduría decidió **abrir una nueva Fiscalía que se encargue de estudiar los casos frente a las más de 28.000 personas desaparecidas en México, sin embargo, para el coordinador general de IDHEAS AC esto no es garantía debido a la “situación deficiente de aparatos de administración de justicia”, por lo que se requeriría un “análisis profundo de las estructuras de corrupción y de poder”.

Cabe recordar que esta semana el Ministro de Defensa mexicano, Salvador Cienfuegos se pronunció frente al caso de los 43 estudiantes desaparecidos de Ayotzinapa y afirmó que **ninguno de los miembros del ejército se prestarían para ser interrogados por expertos de la CIDH.**
