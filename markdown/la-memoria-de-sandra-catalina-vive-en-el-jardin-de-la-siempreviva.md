Title: La memoria de Sandra Catalina vive en el jardín de la Siempreviva
Date: 2016-02-27 15:00
Category: DDHH, Sin Olvido
Tags: Homicidio niña CAI Germania, MOVICE, Sandra Catalina Váquez Guzmán
Slug: la-memoria-de-sandra-catalina-vive-en-el-jardin-de-la-siempreviva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/B-9ERGjXAAAFUY4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Jineth Bedoya 

<iframe src="http://co.ivoox.com/es/player_ek_10589053_2_1.html?data=kpWimp6UeZShhpywj5WdaZS1lZuah5yncZOhhpywj5WRaZi3jpWah5ynca3VjNLSz9TWrcKfxcqYtcbSqNPVjKjO1sbQrc%2FVjNvW2MqPqc%2BfxtGYzMbWqIa3lIquptOPqMafzcaYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Blanca de Guzmán] 

##### [26 Feb 2016] 

El 28 de febrero de 1993, con tan sólo 9 años de edad Sandra Catalina Vásquez Guzmán, fue violada y asesinada en el CAI de Germania, centro de Bogotá. 23 años después sus familiares y amigos realizarán por cuarta ocasión un ejercicio de reivindicación y ante todo de no repetición de crímenes por parte de agentes del Estado colombiano, en particular contra niños y niñas.

El evento de memoria, convocado por amigos y familiares de la menor, contará con la presencia de Organizaciones de Mujeres, el Movimiento de Víctimas de crímenes de Estado MOVICE y el Colectivo de Abogados Jose Alvear Restrepo entre otras, quienes se reunirán en el denominado Jardín de la Siempreviva, donde se realizarán algunos actos simbólicos y culturales por Sandra Catalina y varias de las víctimas despojadas de su vida y dignidad por agentes estatales.

De acuerdo con Blanca de Guzmán, abuela de la niña, la invitación busca alcanzar a la mayor cantidad de personas que se unan en un abrazo solidario para no olvidar ni repetir “eso es lo que estamos buscando, reunir más personas que se unan a nosotros y que este parque de la Siempreviva no solamente sea para conmemorar los 23 años de ella sino de muchas personas más que han sido víctimas como nosotros”

Las actividades que cuentan con el apoyo de la alcaldía local de La Candelaria y el Jardín Botánico de Bogotá,  iniciarán a las 11 de la mañana y culminarán a la 1 de la tarde, en el jardín ubicado en el parque Germanía frente de la estación de Policía y en diagonal a la Universidad de los Andes, subiendo por el eje ambiental.

**El asesinato de Sandra Catalina.**

Por el caso de la “Siempreviva” la responsabilidad del Estado nunca fue aceptada completamente y el agente Fernando Valencia Blandón, que la justicia declaró culpable en 1995 y condenó a 45 años de prisión, únicamente pago 10 años en instalaciones policiales, según relata la abuela de Sandra Catalina.

Por el crimen considerado de lesa humanidad, el Estado colombiano tuvo que indemnizar al agente Pedro Vásquez, padre de la menor a quien inicialmente había sido culpado por el homicidio y a la mama de la niña Yaneth Guzmán, quien pudo instaurar una acción de tutela luego de muchas trabas ante la corte constitucional hasta el año 2012 y que de acuerdo con la señora Blanca es “creer que con 4 pesos es ponerle un valor a la niña, eso no es lo que estábamos esperando del Estado”.
