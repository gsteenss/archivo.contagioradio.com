Title: Fiscalía de Roma tiene indicios sobre posible asesinato de Mario Paciolla
Date: 2020-09-07 12:19
Author: AdminContagio
Category: Actualidad
Tags: Fiscalía de Roma, Mario Paciolla, Misión de Verificación de la ONU
Slug: fiscalia-de-roma-tiene-indicios-sobre-posible-asesinato-de-mario-paciolla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Mario-Paciolla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto:[@demagistris](https://twitter.com/demagistris)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este fin de semana, el diario La República reveló los resultados parciales de la autopsia realizada en Italia a **Mario Paciolla, voluntario de la Misión de Verificación de la ONU para el proceso de paz en Colombia**, quien fue hallado muerto en su apartamento el 15 de julio en el municipio San Vicente del Caguán, Caquetá; el dictamen indicaba que Paciolla se había ahorcado. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, desde Italia las dudas fueron creciendo. Además, se habían omitido investigaciones cruciales en Colombia, por lo que una segunda autopsia se realizó en el país europeo. (Le puede interesar: [Consejo de Seguridad de la ONU llama a un cese al fuego global](https://archivo.contagioradio.com/consejo-de-seguridad-de-la-onu-llama-a-un-cese-al-fuego-global/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto, **la Fiscalía de Roma ha abierto oficialmente una investigación, debido a que hay muchas inconsistencias** entre las heridas de Mario y la sangre encontrada, los nudos hechos en la sábana con las que se habría ahorcado parecían tan bien hechos que difícilmente él las habría podido realizar. Adicionalmente, si una persona muere ahorcada no habría consistencia con rastros de sangre en algunos de sus objetos personales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Anteriormente **Mario había expresado que se sentía en tal grado de peligro, que deseaba “olvidarme para siempre de Colombia. Colombia ya no es segura para mí”,** por lo que esto también ponía en duda que hubiera decidido suicidarse. De acuerdo con la exnovia de Mario, Ilaria Izzo, él había presenciado algo que no debió haber visto y el día anterior a su muerte, estaba tan alterado que se dejó llevar por un estallido escalofriante: "no quiero vivir más", sin embargo, al mismo tiempo, él le pedía ayuda para comprar un boleto de regreso a Italia. (Le puede interesar: [Ataques a la CIDH son un golpe al corazón de las víctimas](https://archivo.contagioradio.com/ataques-a-la-cidh-son-un-golpe-al-corazon-de-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según cercanos, Paciolla ya había tenido discusiones con miembros de la Misión. Al preguntarle a Ilaria **“¿en quién desconfiaba Mario dentro de la ONU?''**ella respondió, “Christian. Christian Thompson", quien estaba a cargo de la seguridad de los miembros de la misión de la ONU. Thompson, de hecho, fue el primero en encontrar el cuerpo sin vida de Mario. También fue quien tiró algunos de los objetos encontrados en la escena del crimen. Además, organizó la limpieza de la casa, antes de que la policía colombiana terminara sus investigaciones.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Paciolla documentó el bombardeo en Aguas Claras
-----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la investigación de la periodista Claudia Julieta Duque, en parte publicada en un medio nacional, el voluntario de la ONU **documentó junto a sus colegas de Misión los detalles y las circunstancias del bombardeo del 29 de agosto en la vereda Aguas Claras, del municipio de San Vicente del Caguán, en el que murieron siete menores de edad entre los 12 y 17 años.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, fue uno de los encargados de verificar las circunstancias del posterior desplazamiento forzado de las familias de las víctimas y las amenazas al personero de Puerto Rico Herner Evelio Carreño, quien previamente había informado sobre el reclutamiento de menores en la zona.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
