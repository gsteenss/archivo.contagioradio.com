Title: Estos son los puntos de concentración del Paro Nacional
Date: 2016-03-16 15:38
Category: Movilización, Nacional
Tags: 17 de marzo, Centrales Sindicales, Comando Unitario de Paro, Paro Nacional
Slug: estos-son-los-puntos-de-concentracion-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/paro-nacional-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [16 Mar 2016] 

Este 17 de marzo, se llevará a cabo el [paro nacional](https://archivo.contagioradio.com/?s=paro+nacional)convocado por  las centrales sindicales, organizaciones sociales y estudiantiles, para presentar ante el gobierno nacional [un pliego de peticiones de 15 puntos,](https://archivo.contagioradio.com/el-pliego-de-peticiones-del-paro-nacional/) con las que se pretende hacer frente a las principales problemáticas del país, para ello habrá diferentes puntos de concentración a nivel nacional.

[**Bogotá:** ]En la capital los puntos de encuentro serán: El Parque Nacional, La Universidad Nacional, El Centro Administrativo Nacional, La calle 72 con carrera Séptima, el Hospital La Hortúa, a su vez, localidades como Ciudad Bolívar, Usme, Suba y el municipio de Soacha contarán con puntos de concentración.

[**Buenaventura:**]Concentración en el Sena, salida 8 de la mañana hasta el Centro "Bulevar".

[**Medellín:**]Concentración será en Carabobo entre los sectores  Jardín Botánico y Parque explora, y harán un recorrido hasta La Alpujarra.

[**Cali**:] Concentración en el parque de Las Banderas y recorrido hasta la gobernación.

[**Barranquilla:**] Concentración en la Plaza de la Paz y recorrido hasta la gobernación.

[**Cartagena:**] Concentración en Universidad de Cartagena sede Piedra de Bolívar y recorrido por la avenida principal hasta llegar al centro de la ciudad.

[**Ibagué:**] Concentración en el parque Galarza y recorrido hasta la gobernación.

[**Bucaramanga:**] Concentración en la Plazoleta Luis Carlos Galán y recorrido hasta la gobernación.

[**Villavicencio:**] Concentración en el Parque de los Fundadores y recorrido hasta la gobernación.

[**Ocaña:**] Concentración en la plazuela del complejo histórico y recorrido hasta el parque San Agustín realizando un pare en la plaza del 29 de Mayo.

[**Tunja:**] Concentración plaza de Bolívar y recorrido por la Avenida Norte hasta la entrada de Combita.

[**Manizales:**]Concentración en el parque Antonio Nariño y recorrido por la Avenida Santander hasta la Plaza de Bolívar.

[**Armenia:**]Concentración en el parque Los Fundadores de Armenia y recorrido por vías principales hasta llegar a la gobernación.

[**Neiva:**]Concentración en el parque Lesburg y recorrido por vías principales hasta el parque Santander frente a la gobernación.

[**Yopal:**] Concentración en el parque Resurgimiento y recorrido por vías principales.

<iframe src="https://www.google.com/maps/d/embed?mid=zsuX01kF3txo.ktX9nrl3ZEXk" width="640" height="480"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
