Title: ¿Qué hay detrás de la visita de Obama a Argentina?
Date: 2016-03-23 12:28
Category: El mundo, Política
Tags: barricada tv, dictadura militar, visita Obama a Argentina
Slug: que-hay-detras-de-la-visita-de-obama-a-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Dictadura-Argentina.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Abuelas Plaza de Mayo ] 

###### [23 Mar 2016 ]

El actual presidente de Estados Unidos Barak Obama viaja a territorio argentino en medio de movilizaciones promovidas por organizaciones sociales y defensoras de los derechos humanos que rechazan las intenciones de esta visita, que según afirma el historiador e integrante de 'Barricada TV' Guillermo Caviasca, se da en el marco del **retroceso que se está viviendo en Argentina por cuenta de las reformas políticas decretadas por Mauricio Macri** y que profundizan el neoliberalismo.

De acuerdo con Caviasca, esta visita de Obama significa un reposicionamiento a nivel político, diplomático y económico a través del que EEUU pretende expandir y consolidar políticas económicas y sellar acuerdos con Macri, que entre otras desorientarían la lógica 'Mercosur' para guiarla hacia los parametros de la 'Alianza Para el Pacífico'. Todo ello teniendo en cuenta la velocidad con la que el actual mandatario de Argentina implementa medidas económicas de corte derechista, como **no cobrar impuestos a empresas mineras, quitar el control del Estatal sobre el petróleo o reducir el poder adquisitivo de la ciudadanía**.

Sellar el acuerdo sobre los fondos buitre, según afirma Caviasca, es uno de los principales objetivos de esta visita, determinación que podría traer un **"nuevo ciclo de endeudamiento, con el sector más rapaz del mercado financiero mundial"**, para implementar proyectos demarcados por el 'Fondo Monetario Internacional', que beneficiarían al capital privado en detrimento de los derechos de la ciudadanía. El historiador agrega que esta visita representa **una nueva etapa en las relaciones entre EEUU y América Latina**, en la que estas naciones podrán buscar mejores condiciones de vida para su población sin que ello signifique dejarse dominar por la potencia.

Cabe resaltar que esta visita se da en el marco de la conmemoración de la dictadura argentina que inició en 1976, y teniendo en cuenta la responsabilidad de los EEUU en este cruento proceso, Caviasca asegura que el acto que el mandatario estadounidense liderara en el Parque de la Memoria es una provocación para las víctimas, más cuando **los actuales funcionarios de gobierno son "herederos directos de la dictadura"**, por lo que las organizaciones de víctimas han programado un acampe en este parque, así como movilizaciones en los lugares en los que Obama se reunirá con los empresarios para sellar acuerdos.

<iframe src="http://co.ivoox.com/es/player_ej_10910756_2_1.html?data=kpWmk5WbeZehhpywj5abaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncYa3k4qvqLbZaaSnhqamjc3FvYzYxtnfh6iXaaKl1JDRx5DQpYzqytjW1saPqMafsMfOz8aPpYy108zS0NnNssKZlKuah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

https://www.youtube.com/watch?v=cSLlC3t9hms

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

 
