Title: Por daños ambientales en Putumayo congelan millonaria cifra a petrolera Amerisur
Date: 2020-01-10 17:20
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Amerisur, Comunidades campesinas, Petrolera, Tribunal
Slug: por-danos-ambientales-en-putumayo-congelan-millonaria-cifra-a-petrolera-amerisur
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Diseño-sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El Tribunal Superior de Inglaterra y Gales ordenó, preliminarmente, el congelamiento de tres millones de libras esterlinas a la empresa Amerisur Resources Plc en respuesta a una demanda interpuesta por los habitantes de la Zona de Reserva Campesina de la Perla Amazónica, que con el acompañamiento de la Comisión Intereclesial de Justicia y Paz y la organización Leigh Dey, reclaman por los daños ambientales y sociales provocados por su operación en los territorios campesinos. (Le puede interesar: ["Ante embajador británico comunidades denuncian afectaciones de petrolera Amerisur"](https://archivo.contagioradio.com/ante-embajador-britanico-comunidades-putumayo-amerisur-37897/))

### **Los daños de Amerisur se estiman en £ 82.500**

Amerisur hace presencia con exploración, sísmica y explotación petrolera desde hace más de 10 años en el departamento del Putumayo. Siendo filial de la britanica Amerisur Resources, ha sido cuestionada reiteradamente, tanto por comunidades campesinas como indígenas, debido al alto impacto ambiental de sus operaciones en el territorio. De hecho, en 2017 el pueblo indígena ZioBain denunció ante el embajador británico, Peter Tibber, que las actividades de la empresa ponían en riesgo su pervivencia, gracias a la intervención de la Comisión Intereclesial de Justicia y Paz.

La demanda interpuesta en diciembre de 2019 ante el Tribunal Superior de Inglaterra y Gales busca proteger los derechos a la vida y el territorio de los habitantes de la Zona de Reserva Campesina de la Perla Amazónica, pero también de las comunidades indígenas que habitan allí ancestralmente. Según los habitantes, las afectaciones provocadas por la empresa se estiman en cerca de £ 82.500, es decir, cerca de 350 millones de pesos. (Le puede interesar:["Comunidad indígena en Putumayo dijo NO a actividad petrolera de Amerisur"](https://archivo.contagioradio.com/comunidad-indigena-en-putumayo-dijo-no-a-actividad-petrolera-de-amerisur/))

Aunque el congelamiento de estos recursos podría no afectar significativamente la operación empresarial, la decisión judicial sí representa un avance en materia de garantía de derechos para los y las afectadas, al tiempo que abre la puerta a la posibilidad de avanzar en sanciones concretas contra este tipo de empresas en otras jurisdicciones. (Le puede interesar: ["Amerisur pone en riesgo la pervivencia del pueblo ZioBain"](https://archivo.contagioradio.com/amerisur-pone-riesgo-la-pervivencia-del-pueblo-ziobain/))

**Síguenos en Facebook:**

<iframe frameborder="0" scrolling="no" style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo](http://bit.ly/1nvAO4u)][[Contagio Radio](http://bit.ly/1ICYhVU).]
