Title: "Min Defensa está desactualizado de información" Gustavo Gallón
Date: 2017-12-18 18:29
Category: Entrevistas, Paz
Tags: Juan Carlos Villegas, Lideres, Min Defensa
Slug: min-defensa-esta-desactualizado-de-informacion-gustavo-gallon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/min-defensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [18 Dic 2017] 

Tras las afirmaciones hechas por el Ministro de Defensa, Luis Carlos Villegas sobre que el asesinato a líderes sociales en el país son producto de “líos de faldas o linderos”, el director de la Comisión Colombiana de Jurista y miembro de la Comisión Nacional de Garantías de Seguridad, Gustavo Gallón manifestó que **Villegas esta “desactualizado de información”** debido a que en otras reuniones que se realizaron con instituciones como la Fiscalía, se ha reconocido hipótesis en donde los asesinatos son producto de la labor que realizaban las personas en el territorio como líderes y defensores.

“La hipótesis que privilegia la Fiscalía en este momento, según se nos informó a un grupo de personas de la Comisión Nacional de Garantías de Seguridad, es que los están asesinando por ser líderes sociales y por ser defensores de derechos humanos” afirmó Gallón y señaló que esta situación **no se puede “minimizar su gravedad ni banalizarlo”**. (Le puede interesar: ["Aumentan las amenazas contra líderes del Bajo Atrato y Urabá")](https://archivo.contagioradio.com/aumentan-las-amenazas-contra-lideres-sociales-del-bajo-atrato-y-uraba/)

De igual forma sobre sobre la cifra de 50 líderes asesinados que mencionó Villegas, el director de la Comisión Colombiana de Juristas, señaló que la Fiscalía está cruzando la información de diferentes organizaciones que han estado registrando los asesinatos como la Defensoría del Pueblo, la plataforma política Marcha Patriótica, la Oficina del Alto Comisionado de Naciones Unidas para la Defensa de los Derechos Humanos, de Somos Defensores y la de la Cumbre Agraria, y **en todas el número ya supera los 100 asesinatos**.

### **Lo que se sabe de los líderes asesinados en Colombia** 

Las reuniones con la Fiscalía se retomarán a partir de enero, para conocer el trabajo que se ha adelantado desde esta institución a nivel Nacional, “la Fiscalía dice que ha aumentado el índice de acusación frente a estos casos y tiene que ver con el esclarecimiento de los mismos” afirmó Gallón y señaló que de los casos que están en investigación de la mitad se conoce información sobre la autoría y a su vez, **sobre la mitad de este mismo grupo hay acusaciones**.

De igual forma, la Fiscalía habría establecido que este fenómeno es atribuible a más de un autor y organización, pero que tiene elementos comunes en las diferentes regiones del país, “hay una serie de factores comunes en las regiones, **hay una presencia de grupos paramilitares que actúan contra líderes y defensores** porque los ven como obstáculo para sus actividades, de diverso orden” explicó Gallón.

Además, serían estos grupos paramilitares los responsables de la gran mayoría de asesinatos, en ese sentido Gallón **reitero la urgencia de que se establezcan políticas que desmonten** a estos grupos y expresó que la función de la Comisión Nacional de Garantías de Seguridad es que se discuta que se puede hacer con esta situación que continúa arriesgando la vida de líderes y defensores.

<iframe id="audio_22719169" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22719169_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
