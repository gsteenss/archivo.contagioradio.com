Title: Con más del 90% de votos, habitantes de Jesús María le dicen no a la minería y el petroleo
Date: 2017-09-17 19:30
Category: Ambiente, Nacional
Tags: Consulta populares, Jesús María, Santander
Slug: con-mas-del-90-habitantes-de-jesus-maria-le-dicen-no-a-la-mineria-y-el-petroleo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/jesus_maria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Consulta Popular] 

###### [17 Sept 2017] 

Los habitantes del municipio de Jesús María, en Santander, dijeron no a la exploración y explotación minera y petrolera en su territorio, con una votación de **1.677 votos contra 22.** Jesús María es la primera consulta popular de tres que se realizaran en este último semestre del año en Santander y que pretende que la provincia de Vélez, este libre de proyectos de esta índole.

El umbral de votación que se necesitaba era de **1.087 personas y hubo un total de sufragantes de 1.728 personas, además se instalaron 6 mesas en el municip**io. De esta manera, Jesús María, se convierte en el primer municipio en Santander en decir no a la exploración y explotación minera y petrolera. (Le puede interesar: ["Provincia de Vélez será escenario de tres consultas populares contra la minería"](https://archivo.contagioradio.com/provincia-de-velez-sera-escenario-de-3-consultas-populares-contra-la-mineria/))

Las próximas consultas populares se realizaran en Sucre, el primero de octubre, y en El Peñón, el 5 de noviembre, en donde se espera que la población de estos dos municipios se sumen a la decisión de la comunidad de **Jesús María y de esta forma, prohibir estas actividades en la Provincia de Vélez**.

![Jesús María](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/WhatsApp-Image-2017-09-17-at-7.04.10-PM.jpeg){.alignnone .size-full .wp-image-46751 width="575" height="345"}

### **Títulos mineros en Jesús María** 

En el municipio de Jesús María hay 8 títulos mineros, 6 de ellos para la explotación de carbón y dos, para la explotación de material de construcción, estos últimos estaban localizados en una Peña, **que surte de agua a diferentes veredas del lugar afectado a 8 acueductos. **

### **Las alternativas a la minería y el petroleo** 

De acuerdo con Mauricio Mesa, integrante de la Corporación Compromiso, que ha estado acompañando a esta comunidad, los habitantes han manifestado que existen alternativas más rentables y **menos nocivas con el ambiente, como lo es el ecoturismo, que desde hace muchos años se ha venido impulsando en el municipio o la agricultura**, actividad histórica en el territorio que ha servido como sustento a muchas familias.

"La proyección es el turismo, tenemos varias cuevas, ríos, paisajes, un turismo donde se incluye a los campesinos, igualmente tenemos el canal de la leche y pensamos mejorar los cultivos de la mora, sin utilizar tóxicos para posicionarnos" afirmó Mesa. (Le puede interesar: ["Especial Consultas populares en Colombia, alternativas al modelo extractivista"](https://archivo.contagioradio.com/especial-consultas-populares-en-colombia-alternativas-al-modelo-extractivista/))

<iframe id="audio_20944777" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20944777_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
