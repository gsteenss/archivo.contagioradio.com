Title: "Intentaron hacernos un falso positivo judicial" Camioneros
Date: 2017-02-17 15:13
Category: DDHH, Nacional
Tags: Asociación Colombiana de Camioneros, Falsos Positivos Judiciales
Slug: intentaron-hacernos-un-falso-positivo-judicial-camioneros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Paro-Camionero-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [17 Feb 2017] 

Los 7 líderes de la Asociación Colombiana de Camioneros, que fueron retenidos el  14 de febrero, denunciaron ser víctimas de falsos positivos judiciales y criminalizados por parte de las autoridades, quienes en un principio los señalaron de hacer parte del cartel de la chatarrización y **al no tener pruebas de ello, un juez les imputo cargos por obstrucción a la vía pública y daño a servicio público, dictándoles medidas de aseguramiento extra murales.**

Los camioneros afirmaron que las detenciones se realizaron por miedo a que el gremio regrese a paro debido a los incumplimientos  del gobierno frente a los acuerdos realizados durante el año pasado. Sobre esta posibilidad, Óscar Tapias, integrante de la Asociación Colombiana de Camioneros indicó que **sí el gobierno incumple el punto dos del acuerdo del paro del año pasado y permite que se acabe con los pequeños transportadores “harán una movilización pacífica, como siempre”** en la exigencia de sus derechos al trabajo

Para Tapias, el presidente Santos estaría rompiendo su pacto de no acabar la regulación del parque automotor uno a uno, a través del decreto 153 firmado por el Ministro de Transportes, que **permitiría el ingreso de más camiones al mercado**. Le puede interesar:["Las respuestas de los camioneros a las medidas de Santos"](https://archivo.contagioradio.com/la-respuesta-de-los-camioneros-a-las-medidas-de-santos/)

“El decreto permite el ingreso de nuevos vehículos a Colombia, acabando el programa uno a uno y sobre ofertando el parque automotor, **generando que los fletes caigan y acabando con el pequeño transportador,** que es el más débil de la cadena se vería desplazado porque quebraría” afirmó Tapias. Le puede interesar: ["ESMAD arremete contra movilización de camioneros vía Guarne-Bogotá"](https://archivo.contagioradio.com/esmad-arremete-en-contra-de-movilizacion-de-camioneros-via-guarne-bogota/)

<iframe id="audio_17078610" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17078610_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
