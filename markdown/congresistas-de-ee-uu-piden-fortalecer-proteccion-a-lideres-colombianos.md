Title: Congresistas de EE.UU piden fortalecer protección a líderes Colombianos
Date: 2018-05-31 14:05
Category: DDHH, Nacional
Tags: congresistas Estados Unidos, defensores de derechos humanos, Estados Unidos, lideres sociales
Slug: congresistas-de-ee-uu-piden-fortalecer-proteccion-a-lideres-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/líderes-rueda-de-prensa-2-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 May 2018] 

En una carta dirigida al secretario a Mike Pompeo, un grupo de 73 congresistas de EEUU, le pidieron al Departamento de Estado, a la Agencia para la Cooperación Internacional  USAID y a los demás departamentos y agencias de ese país, que **fortalezcan los esfuerzos** para “llevar a la justicia a los culpables de los crímenes” contra defensores de derechos humanos y líderes sociales en Colombia.

De acuerdo con la misiva, “hay una preocupación respecto al aumento en el número de asesinatos de defensores y líderes” que hace necesario **crear acciones que protejan** la vida de estas personas, de sus familias y de las comunidades. Exigieron además que las acciones se enfoquen en evitar que se debilite la implementación del Acuerdo de Paz.

### **Asesinatos de líderes y defensores ha aumentado un 30% desde la firma del Acuerdo de Paz** 

Para sustentar la petición, los congresistas indicaron que “de acuerdo a la base de datos de la **Oficina del Alto Comisionado de las Naciones Unidas** para los Derechos Humanos, los ataques contra los defensores de derechos humanos en Colombia se han incrementado en un 30% durante el último año”.

Específicamente, la “OACNUDH reportó **121 asesinatos**, cuyas fatalidades incluyen 84 defensores de derechos humanos en roles de liderazgo, 23 miembros de movimientos sociales y políticos, y 14 personas asesinadas durante protestas sociales”.

Hicieron referencias también a las cifras de la Defensoría del Pueblo que registró **282 asesinatos** desde 2016 y de la organización Somos Defensores que contabilizó 27 asesinatos en los primeros dos meses de 2018. (Le puede interesar:"[Solicitan medidas cautelares a la CIDH para proteger a los líderes sociales"](https://archivo.contagioradio.com/solicitan-medidas-cautelares-a-cidh-para-proteger-a-los-lideres-sociales/))

### **Exigencias de los Congresistas de Estados Unidos** 

Con esto en mente, pidieron que las acciones que fortalezcan los mecanismos de protección deben ser combinadas con llevar “rápidamente a la **justicia** a aquellos que planean y ejecutan estos asesinatos, y no simplemente a los sicarios que oprimen el gatillo”.

Además, se debe lograr que “las autoridades colombianas comuniquen que los perpetradores, colaboradores y **beneficiarios de estos crímenes** van a enfrentar consecuencias legales por sus acciones”. Como algo prioritario, consideran que se debe desmantelar a los actores armados ilegales “que continúan asesinando y atacando a los líderes sociales, y a las estructuras económicas que los apoyan”.

Las autoridades en Colombia deben garantizar **la seguridad y proveer los recursos** necesarios y la presencia estatal en las regiones que fueron entregadas por las FARC al momento de su desmovilización. Recordaron que es fundamental que se alcance un acuerdo de paz con la guerrilla del ELN para poder terminar con el conflicto armado.

Para lograr esto, le pidieron al Departamento de Estado y a USAID, “que suministren los recursos y el apoyo necesario para que **Colombia alcance estos objetivos**, incluyendo presionar al actual Gobierno colombiano y a su sucesor para que den prioridad a estas acciones”. De manera específica, indicaron que estos recursos se deben invertir en la investigación, el enjuiciamiento y la protección de los líderes sociales y defensores de derechos humano.

[Carta de Congresistas a Secretario de Estado](https://www.scribd.com/document/380679692/Carta-de-Congresistas-a-Secretario-de-Estado#from_embed "View Carta de Congresistas a Secretario de Estado on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_83032" class="scribd_iframe_embed" title="Carta de Congresistas a Secretario de Estado" src="https://www.scribd.com/embeds/380679692/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Vm25jqzq9lFoaZnCVFvB&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
