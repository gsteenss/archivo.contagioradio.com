Title: 43 bloques petroleros en Caquetá amenazan la Amazonía colombiana
Date: 2016-04-19 15:29
Category: Ambiente, Entrevistas
Tags: Amazonía, Caquetá, extracción petrolera
Slug: 43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Extracción-de-petroleo-e1461090623162.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticaribe] 

###### [19 Abr 2016] 

En el departamento de Caquetá comunidades denuncian que ya está llegando maquinaria para iniciar la perforación de pozos petroleros en los municipios de Paujil y Doncello, lo que **causaría graves efectos ambientales en la Amazonía colombiana**, zona productora de agua.

De acuerdo con Mercedes Mejía, coordinadora de la Mesa Departamental por la Defensa del Agua y el Territorio, desde el domingo se inició el ingreso de maquinaria de la empresa Monterico, hacia  los municipios de **El Doncello y El Paujil**. Además según reportes de habitantes del municipio de Puerto Rico, allí  también estarían llegando las máquinas.

Según la Agencia Nacional de Hidrocarburos, ANH, **en Caquetá se planea la puesta en marcha de 43 bloques petroleros.** La última licencia fue concedida a la empresa Monterrico S.A. para el proyecto "Área de Perforación Exploratoria Portofino Sur", que abarca Doncello, Puerto Rico, Paujil y San Vicente, **municipios productores de agua que hacen parte del departamento que representa el 18.67% de la región Amazónica** colombiana, área que requiere altos estándares de protección ambiental.

"La función de esta región debe ser producir agua, las fuentes hídricas empezaron a disminuir sin haber entrado la locomotora minero-energética, cuando ésta ingrese con toda su fuerza, esta condición de productor de agua cambiará y perderemos la cantidad de agua que se produce", dice la profesora Mejía.

**Meta Petroleum, Pacific Stratus, Canacol, Emerald Energy, Monterico, Hupecol, C&C Energy, Optima Range y Ecopetrol,** son las empresas interesadas en realizar actividad petrolera en este departamento.

Las alcaldías están solicitando información a estas petroleras para explicar a la comunidad los verdaderos impactos de la actividad, debido a que con la llegada de las multinacionales la población se encuentra dividida pues "la situación social es difícil, y ante la posibilidad de obtener unos ingresos por contratos de pocos días", no se entiende el impacto ambiental que la explotación de petróleo ocasionaría al ambiente pero también al derecho al agua de las comunidades, explica la ambientalista.

**La comunidad ha evidenciado presencia del ESMAD justo después de que llegó la maquinaria,** así mismo, denuncian que a causa de la llegada de las petroleras se vive un conflicto socio-ambiental que ha generado amenazas contra algunos concejales y lideres que se oponen a estos proyectos minero-energéticos.

Los pobladores del  municipio de Valparaíso, adelantan procesos de resistencia desde el año 2014. La Mesa Departamental por la Defensa del agua y el Territorio, trabaja para sensibilizar a la población sobre este tema, para lo que se ha implementado una **comisión accidental de hidrocarburos** que trabajará con los Consejos municipales con el fin de socializar en las  juntas de acción comunal las consecuencias de la extracción y explotación petrolera.

Para el próximo 22 de abril estudiantes de la Universidad de la Amazonía, en Florencia realizarán un plantón cultural en chazo a este tipo de proyectos que generan graves daños al ambiente en el marco del Día Internacional de la Tierra.

<iframe src="http://co.ivoox.com/es/player_ej_11226356_2_1.html?data=kpaflJuXeZehhpywj5aWaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5ynca7Z08jSxsrXb67Zy87Oh5enb67Z1MaYpsrUuMLgjKnSyMrSt8KfxcrZjabLucKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
