Title: 30 años después los claveles rojos vuelven a marchar en Medellín
Date: 2017-08-25 13:11
Category: DDHH, Nacional
Tags: Antioquia, Defensores DDHH, Héctor Abad Gómez, memoria
Slug: marcha-claveles-rojos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/DIFmFeNXcAA6vVo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Generación Paz 

###### 25 Ago 2017 

En la memoria de los antioqueños vive aun el recuerdo del **13 de agosto de 1987**, fecha en la que **3000 personas desfilaron las calles de su capital Medellín portando claveles rojos** a manera de protesta a la violencia paramilitar contra estudiantes y profesores de la Universidad de Antioquia simpatizantes de ideas de izquierda.

Algunos de los promotores de la marcha entre quienes se encontraban **Héctor Abad Gómez, Pedro Valencia Giraldo, Luis Felipe Vélez y Leonardo Betancur**, fueron asesinados pocos días después de caminar por la defensa de la vida. **Hoy, en conmemoración a su lucha y sacrificio los claveles rojos vuelven a marchar por las calles de la ciudad**. Le puede interesar: [La memoria de Héctor Abad Gómez en "Carta a una sombra](https://archivo.contagioradio.com/la-memoria-de-hector-abad-gomez-en-carta-a-una-sombra/)"

"**Queremos conmemorar el acto valeroso que ellos hicieron llamar la atención sobre la situación de los defensores de derechos humanos en Colombia que sigue estando en alto riesgo**" asegura Mayra Duque, integrante del Movimiento "No matarás", una de las plataformas que convoca a la actividad.

![claveles rojos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/DIF1tgeWAAAC7vk.jpg){.wp-image-45762 .aligncenter width="304" height="304"}

La actividad que proponen no es una concentración masiva como la de 1987, de acuerdo con Duque "**lo que se busca es que cada persona que transita en sus rutas cotidianas con un clavel en la mano estará marchando por la vida**" además de llamar la atención en todo el país a través de las redes sociales utilizando la etiqueta \#NoMatarás.

A pesar que Duque valora el hecho que ahora existan hay mas espacios de conversación entre posturas de derecha e izquierda, asegura que aun los asesinatos por convivencia en Medellín "siguen en un alto número y **los asesinatos selectivos en Colombia a personas que concretamente defienden derechos humanos**,  hacen preguntas incomodas hacia ciertos sectores y grupos del país siguen sucediendo".

"Venimos haciendo actos simbólicos y vamos a seguirnos movilizando, reconocemos que hay varias organizaciones que vienen trabajando el tema, que vienen haciendo campañas desde hace mucho tiempo y queremos unirnos a ellos queremos dar visibilidad a los asesinatos que en esta sociedad están casi normalizados" expresa la activista en referencia a los [indices de violencia que viene registrando en Antioquia](https://archivo.contagioradio.com/en-antioquia-se-han-presentando-111agresiones-contra-defensores-de-dd-hh/) en los últimos meses.

Duque afirma que apesar que el proceso de paz ha dado algunas luces en lo que puede seguir en Colombia "**los niveles de impunidad en el país por el asesinato de lideres sociales es inmensamente alto**",  en gran medida porque "el aparato judicial  sigue siendo bastante lento"asegura.

Los promotores esperan repartir claveles rojos entre los ciudadanos y aquellos que deseen replicar esta labor lo hagan portando carteles con la frase “**Hoy marchamos con claveles rojos por la defensa de la vida. \#NoMatarás”**. Se ubicarán en puntos de la ciudad en horas de la mañana como la **Plazoleta la Alpujarra**, **la Universidad de Antioquia, a medio día en la Univerisdad EAFIT y en la tarde a las 4:00 pm en el Parque Berrio**.

<iframe id="audio_20524241" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20524241_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
