Title: Comités de revocatoria escépticos por decisión del CNE
Date: 2017-05-18 13:52
Category: Nacional, Política
Tags: CNE, Firmas revocatoria, revocatoria Peñalosa
Slug: comites-de-revocatoria-escepticos-por-decision-del-cne
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/laestampida4-e1495133458736.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [18 mayo 2017]

Tras conocer la decisión del Consejo Nacional Electoral de retirar el proyecto con el que pretendía reformar el proceso de revocatoria de mandatarios electos por voto por voto popular, el Comité Unidos Revocaremos a Peñalosa celebra la decisión, pero **mantienen sus preocupaciones frente a las garantías de este mecanismo democrático.**

Una de las preocupaciones del Comité para la revocatoria es que el CNE quiere evaluar la exposición de motivos del proceso de revocatoria. Para ellos, esta competencia la debe tener la ciudadanía y no esta institución. Sin embargo, a **la Registraduría le quedan 29 días para dar el aval de la recolección de las 700 mil firmas.**  Seguidamente, el gobierno debe entregar el dinero para las elecciones y convocar a las urnas. Le puede interesar: ["Consejo Nacional Electoral tiende trampa a ciudadanía: Unidos revocamos a Peñalosa"](https://archivo.contagioradio.com/consejo-nacional-electoral-tiende-trapa-a-ciudadania-unidos-revocaremos-a-penalosa/)

Adicionalmente, uno de los principales argumentos que exponían los defensores de Peñalosa era que la recolección de firmas para la revocatoria, se habría hecho mintiéndole a la ciudadanía, tesis que de acuerdo con Carrillo defiende el CNE. “**El Consejo Nacional Electoral defiende esta postura que es falsa, ellos no pueden decidir si unas motivaciones son o no válidas”.**

Otra preocupación es que el CNE repartió los  107 procesos de revocatoria en marcha, entre los 9 magistrados que conforman el órgano. El proceso de Bogotá lo revisará la magistrada Ángela Hernández Sandoval. Su elección en el CNE, hace tres años, fue promovida voto a voto por el exprocurador Alejandro Ordóñez y  esta decisión según Carlos Carrillo, promotor del Comité Revocaremos a Peñalosa, **"le hace daño al proceso en Bogotá por ser la cuota del ex procurador Alejandro Ordoñez".**

Para Carrillo, las decisiones que tome la magistrada Hernández **pueden estar ligadas a sus convicciones y cercanía a figuras políticas,**  Carrillo afirmó que “la magistrada se puede extra limitar en sus funciones y nos preocupa que le dé más peso a sus convicciones que al respeto por la ley”. Le puede interesar: ["Enrique Peñalosa, el alcalde del conflicto"](https://archivo.contagioradio.com/enriquepenalosarevocatoria/)

Ahora, el CNE abrirá un debate en donde discutirá si tiene la facultad de definir el futuro de los procesos de revocatoria,  **revisando la exposición de motivos** y estableciendo si los comités de revocatoria engañaron o no a los ciudadanos para adquirir las firmas.

<iframe id="audio_18773284" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18773284_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
