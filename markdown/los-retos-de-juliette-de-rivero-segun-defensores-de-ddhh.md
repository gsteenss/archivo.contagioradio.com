Title: Los retos de Juliette de Rivero según defensores de DDHH
Date: 2020-08-03 22:46
Author: PracticasCR
Category: Actualidad, DDHH
Tags: ACNUDH, Alta Comisionada de las Naciones Unidas para los Derechos Humanos, DD.HH., Derechos Humanos, Juliette de Rivero
Slug: los-retos-de-juliette-de-rivero-segun-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/juliette-de-rivero.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Juliette-de-Rivero-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Juliette de Rivero / ONU

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 1° de agosto, **Juliette de Rivero asumió el cargo como nueva representante de la Alta Comisionada de las Naciones Unidas para los Derechos Humanos -ACNUDH- en Colombia**; tras la finalización de la gestión de Alberto Brunori, cuya labor fue destacada por organizaciones sociales y defensoras de DD.HH. y criticada por el gobierno de Iván Duque, especialmente tras la publicación de su informe de relatoría correspondiente al año 2019 que daba cuenta de un preocupante aumento de las violencias y vulneraciones a los derechos humanos en el país. (Le puede interesar: ["Colombia acumula más de 10 solicitudes sin aceptar de Relatores de la ONU"](https://archivo.contagioradio.com/colombia-acumula-mas-de-10-solicitudes-sin-aceptar-de-relatores-de-la-onu/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/ONIC_Colombia\/status\/1283481063835172871","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1283481063835172871

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La nueva representante cuenta con 24 años de experiencia en el trabajo por los derechos humanos y ha ostentado entre otros cargos el de jefa de la sección de África Oriental y Austral en la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos y el de directora de la oficina de Human Rights Watch en Ginebra, Suiza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque la labor del saliente representante de ACNUDH en Colombia fue fructífera y significó un avance en materia de derechos humanos en el país, Juliette de Rivero asume el cargo en medio de un preocupante panorama de violencia en los territorios. Por este motivo, **Contagio Radio consultó a representantes de organizaciones sociales defensoras de derechos humanos para que formularán según sus perspectivas los principales retos que enfrenta la nueva represente a su llegada al cargo.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los retos para Juliette de Rivero según los líderes de organizaciones sociales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Gerardo Vega, director de la [Fundación Forjando Futuros](https://www.forjandofuturos.org/), el principal reto que asume Juliette de Rivero es **contribuir para que cese el asesinato en contra de líderes y lideresas sociales y excombatientes.** Adicionalmente, señaló que desde ACNUDH debe hacerse acompañamiento a la implementación del Acuerdo de Paz ante un querer del Gobierno y su partido político para «*destruirlo*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Diana Sánchez directora de la [Asociación Minga](https://asociacionminga.co/) y vocera de la Coordinación Colombia Europa Estados Unidos -[Coeuropa](https://coeuropa.org.co/)-; expresó que el  primer reto consistía en «*lograr una relación armónica con el gobierno de Iván Duque que ha mostrado no ser un gobierno muy proclive a los derechos humanos y menos a la observación internacional como se evidenció en su reacción al informe del representante \[saliente\] Alberto Brunori*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También, señaló que otro reto era mantener la agenda de DD.HH. en el radar de la ciudadanía y la opinión pública para que **la «*negación y el reduccionismo*» del Gobierno Nacional** frente a los problemas en este rubro no logre posicionarse como una verdad absoluta. Sánchez recalcó que junto con esos retos también era importante que ACNUDH mediante su nueva representante acompañe a las comunidades en territorios apartados especialmente en el marco del confinamiento actual donde son blanco de múltiples violencias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, Luz Marina Monzón directora de la Unidad de Búsqueda de Personas dadas por Desaparecidas -[UBPD](https://www.ubpdbusquedadesaparecidos.co/)-, **entidad encargada de buscar más de** **100.000 personas desaparecidas en el marco del conflicto armado**, afirmó que el principal reto de la nueva funcionaria de ACNUDH es **hacer un seguimiento y supervisión al Gobierno Nacional** en el marco de su autonomía e independencia como representante de un organismo internacional y no gubernamental. No obstante, añadió que será una labor compleja por el difícil contexto colombiano caracterizado por la violencia, el despliegue de actores armados y la polarización social y política.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desafíos en el escenario pospandemia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Alfonso Castillo, vocero nacional del Movimiento Nacional de Víctimas de Estado -[Movice](https://movimientodevictimas.org/)-; aseguró que la nueva delegada tiene como reto **garantizar la continuidad de la gestión del representante Brunori caracterizada por una rigurosa vigilancia del Proceso de Paz y su implementación.** Frente a este punto, Diana Sánchez añadió que Juliette de Rivero debe influir para que el Gobierno implemente las recomendaciones realizadas por Alberto Brunori en su informe del año 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el vocero del Movice, aseguró que uno de los retos es **«*exigirle al Gobierno Nacional mayores y mejores garantías para los defensores y defensoras de DD.HH. que permitan contener el genocidio que actualmente se está dando en Colombia*** *con el asesinato \[de estas personas\] a lo largo y ancho del país*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, avizoró los desafíos de la nueva delegada una vez superada la contingencia del Covid-19, afirmando que debía promover la «*garantía de derechos pospandemia*», ya que, para Castillo se prevé que **tras el confinamiento va a haber una agudización de la crisis económica del país y «*esto se va a traducir en un aumento de las movilizaciones y las protestas*** *de ciudadanos que exigirán al Gobierno medidas efectivas para sortear el hambre, el desempleo y la pobreza; a lo que el Gobierno va a responder con mucha agresividad y con una feroz política represiva*». Para el dirigente social, ahí debe entrar ACNUDH para exigir las garantías constituciones para el libre ejercicio a la protesta social.

<!-- /wp:paragraph -->
