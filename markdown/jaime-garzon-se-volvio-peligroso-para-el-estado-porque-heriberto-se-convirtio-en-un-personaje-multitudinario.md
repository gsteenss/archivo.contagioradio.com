Title: Jaime Garzón se volvió peligroso para el Estado porque Heriberto se convirtió en un personaje multitudinario
Date: 2020-08-18 09:17
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Jaime Garzón, JEP, Paramilitarismo
Slug: jaime-garzon-se-volvio-peligroso-para-el-estado-porque-heriberto-se-convirtio-en-un-personaje-multitudinario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Aljeandro-Angel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Alejandro Ángel/ Jaime Garzón

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A 21 años del homicidio del humorista Jaime Garzón, crimen de Estado en el que participaron agentes estatales y grupos paramilitares y del que aún no se conocen todos los responsables, periodistas, amigos y quienes buscan ante la justicia la verdad tras su homicidio, se refieren al impacto que tuvo y tiene en la actualidad su legado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Eduardo Árias, periodista y libreta quien acompañó a Garzón en producciones como Quack y Zoociedad** destaca del abogado y periodista su labor como pedagogo y en particular "una lectura muy certera de la realidad del país", capacidad que le permitió saber "de dónde venían y para dónde iban estos personajes que 20 años después adquirieron una trascendencia distinta por los nuevos cargos que asumieron", afirma con relación a personajes como el expresidente Álvaro Uribe y Néstor Humberto Martínez vinculados a casos de corrupción. [(Lea también: Lo que no se debe olvidar de Jaime Garzón, 20 años después de su asesinato)](https://archivo.contagioradio.com/jaime-garzon-20-anos-asesinato/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Tenía una memoria prodigiosa lo que permitía que su humor tuviera una base muy sólida de formación, otro asunto es que conocía el poder por dentro, al ser alcalde de San Juan de Sumapaz tuvo que lidiar con temas de administración pública lo que lo hacia un humorista único", lo recuerda Árias.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Su último encuentro con Jaime en la entrada de Radionet, emisora donde el humorista trabajaba y en la que se encontraron cuando el libretista salia de una entrevista junto a la también periodista María Jimena Duzán, **"mire Jaime usted está hablando con todo el mundo de temas muy delicados y está poniendo en serio peligro su vida, por favor cálmese" -** recuerda Árias - "esa fue la última vez que vi a Jaime, María Jimena se lo advirtió y al mes fue asesinado, a Jaime lo mataron porque estaba en el mundo real y tenía conocimiento de cosas tenaces que estaban pasando en el mundo real, se metió a manejar temas muy complicados en los que la discreción era clave. [(Le recomendamos leer: Juicio en caso Jaime Garzón revela nueva actividad criminal de la Brigada 13)](https://archivo.contagioradio.com/juicio-en-caso-jaime-garzon-revela-nueva-actividad-criminal-de-la-brigada-13/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Tan profético como fue Jaime Garzón, así ha sido la impunidad en su caso

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Sebastián Escobar abogado del caso** señala que se trata de un gran rompecabezas del que aún faltan fichas por encontrar, "es un crimen de Estado que tiene muchos vacíos en su comprensión en ese contexto de relacionamiento tan fuerte que hubo entre las fuerzas militares y los grupos paramilitares".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque se ha podido esclarecer que la orden de asesinar a Jaime provino de instancias militares y han existido reconocimientos de grupos paramilitares que han señalado a la fuerza pública como quienes habrían instigado a su asesinato, lo que llevó a identificar la responsabilidad de **José Miguen Narváez, asesor del Ministerio de Defensa, subidrector del DAS como enlace con grupos paramilitares para este y otros crímenes.** [(Lea también: JEP negó, en segunda instancia, tutela presentada por el Coronel (r) Plazas Acevedo)](https://archivo.contagioradio.com/jep-nego-en-segunda-instancia-tutela-presentada-por-el-coronel-r-plazas-acevedo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque se logró la judicialización de Narváez y condenarlo a 30 años de cárcel, aún falta encontrar las piezas que lo conectan con el crimen y poder establecer quien al interior de las instancias militares dio la orden de asesinarlo. Como pieza clave de ese rompecabezas, el abogado también señala a Jorge Eliecer Plazas Acevedo, coronel (r) vinculado no solo al caso Garzón sino a otros hechos como la **Masacre de Mapiripán, las incursiones que se dieron en Chocó y Bajo Atrato a finales de la decáda de los noventa y el homicidio de los investigadores Mario Calderon y Elsa Alvarado**. Según los avances en el caso, Plazas Acevedo habría sido determinante en el rol de la inteligencia militar al facilitar información a Carlos Castaño y permitir la estructuración del homicidio. [(Le puede interesar: Tras 22 años de impunidad, caso de Elsa Alvarado y Mario Calderón será llevado a la CIDH)](https://archivo.contagioradio.com/tras-22-anos-de-impunidad-caso-de-elsa-alvarado-y-mario-calderon-sera-llevado-a-la-cidh/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tanto Narvaez como Plazas Acevedo han acudido a la JEP y tienen la obligación de aportar a la verdad, Escobar advierte que el esclarecimiento por su parte no ha sido específico, por lo que esperan sea una verdad que se construya de la mano de las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Existen otros nombres que no han podido ser del todo vinculados al caso del humorista como el de **Rito Alejo del Río,** comandante (r) de la XIII Brigada quien trabajó de la mano con Plazas Acecedo y habría tendido algún tipo de colaboración en el caso al igual que **Mauricio Santoyo,** jefe de seguridad del ex presidente Álvaro Uribe y quien prestó apoyo a grupos paramilitares y quien fue señalado por álias Don Berna de desviar la información del caso ante el DAS.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por otro, los integrantes de la banda La Terraza, señalados de ser autores materiales del asesinato de Jaime, fueron blancos de exterminio durante la década de los 2000 en medio de una guerra que involucró al Bloque metro de las AUC, y a la Policía Nacional; **se cree que La Terraza poseía información clave en este crimen y otros de relevancia nacional.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Cuando el humor se vuelve peligroso

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Árias atribuye el paso de Garzón de programas como Quack, donde hacía imitaciones y permanecía en la ficción a ser un personaje de la vida real, "Jaime ya no estaba disfrazado de sus personajes de Quack, estaba en un set de noticiero y empezó a hacer entrevistas a personas reales en tiempo real, y cuando le decía algo a un personaje no se lo decía disfrazado, el personaje ya lo estaba atacando en el mundo real a través de lo que decía Heriberto, concluye Árias quien también adjudica el homicidio al trabajo que este desarrollaba liberando personas secuestradas

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Jaime se volvió peligroso porque Heriberto se convirtió en un personaje multitudinario, llegaba a todo el mundo, con el que la mayoría del pueblo se identificaba, Heriberto representaba gran parte de la población
>
> <cite>Eduardo Árias</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre la formula de hacer humor político, **Diego Alarcón, periodista y director de proyectos en Riaño Producciones junto al humorista Alejandro Riaño,** señala que mas allá de un personaje, se trata de lo que representa en su puesta de escena y lo que al tiempo está comunicando a la audiencia desde la plataforma en la que se está parando a hablar, lo que se resumen en un "golpe de honestidad para la audiencia y que está entiende y por tanto acoge.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, Alarcón identifica en los contenidos que realizan con personajes como [Juanpis González](https://twitter.com/juanpisrules) que existe un referente automático hacia la labor de Jaime Garzón lo que puede ser leído de dos formas, por un lado su recordación y su forma de hacer humor y por otro el caso de su asesinato y que hasta el día de hoy tiene sus consecuencias, **"hay algo que me impresiona y es la referencia constante a Jaime Garzón y la mención de "ojalá no te maten", "sigue siendo valiente", es un mensaje frecuente y eso es una cicatriz que como sociedad tenemos y que demuestra lo naturalizada que está la violencia en nuestro entorno"**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ha cambiado el paradigma en los medios

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Fabiola León, periodista, docente y analista de medios** señala que en el periodismo de la actualidad, "no existe una clara división entre información y opinión", por lo que al mezclar ambos se confunde a la audiencia ante la interpretación de un hecho".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por tal motivo, considera que es necesaria la democratización de la información pues ante el alcance que pueden tener los medios corporativos y su sistema capitalista más ligado a la consecuón de un rating y no necesariamente a la calidad de información, por lo que son necesarias otras propuestas. Agrega la importancia de diferenciar entre crítica, periodismo crítico y el humor. destacando el trabajo de Jaime en Zoociedad como un ejercicio de periodismo de investigación, "el periodista es un defensor de DD.HH. nos han querido enseñar que son cosas diferentes pero cuando uno es periodista trabaja por la construcción de la democracia y siempre va a estar del lado de la gente". [(Lea también: 30 años de prisión pagará José Miguel Narváez por homicidio de Jaime Garzón)](https://archivo.contagioradio.com/condena-narvaez-homicidio-garzon/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Creo que ese escenario de construcción verdad en el mundo de hoy ya no existe, ese totem de verdad que podía ser los medios tradicionales o las grandes productoras, ya han sufrido esa pérdida de verdad que han ganado otros formatos", resalta Diego Alarcón.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre esa construcción de verdad, León advierte cómo Jaime sabía que lo iban a matar, la sociedad lo sabía y sin embargo se permitió que se perpetuara la impunidad de este y otros casos por más de 20 años, "hay que hacer una reflexión de cómo sociedad permitimos que sucediera durante tanto tiempo, por lo que el periodismo debería estar pensando en narrar la verdad".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Lo que hacía Jaime era decir la verdad en la cara, él no disfrazó nada, se disfrazaba pero lo que decía no estaba disfrazado",** afirma Fabiola León resaltando la importancia de la Comisión de la Verdad y que abre la posibilidad de que personajes como Narvaéz revelen sucesos del conflicto armado, algo a lo que deberían apostarle los medios, expresa la analista.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Su finalidad no era el humor, el uso de esa herramienta también lo hacía peligroso, tenía la virtud de acercar mucha gente a la crudeza y a la realidad del país y al tiempo hacia posible el escrutinio público sobre el Estado, el humor de Jiame era un antidoto contra la censura y la autocensura", concluye Escobar.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
