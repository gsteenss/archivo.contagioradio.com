Title: Persisten hostigamientos a defensores de DDHH en Meta y Chocó
Date: 2015-10-21 14:36
Category: DDHH, Nacional
Tags: Chocó, Comisión Intereclesial de Justicia y Paz, Danilo Rueda, mapiripan, Paramilitares en Chocó, Paramilitarismo, Persecución a defensores de derechos humanos, Riosucio, UNP
Slug: persisten-hostigamientos-a-defensores-de-ddhh-en-meta-y-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Riosucio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: You Tube 

###### [21 Oct 2015 ] 

[De acuerdo con la Comisión Intereclesial de Justicia y Paz integrantes de sus equipos de acompañamiento jurídico y psicosocial, han sido **perseguidos y hostigados** en los municipios de Rio Sucio, Chocó y Mapiripán, Meta, **por miembros de estructuras paramilitares**.]

[El pasado viernes 16 de octubre, en Riosucio los defensores de derechos humanos, María **Eugenia Mosquera** y **Danilo Rueda** fueron ** observados por paramilitares** mientras desarrollaban actividades de formación en derechos políticos con delegados de las comunidades del Bajo Atrato, quienes aseguran que el **candidato a la alcaldía** por el Partido de la U **Luis Mena, recibe apoyo de paramilitares**, así como del actual alcalde  del municipio, Cecilio Moreno.]

[El domingo 18 de Octubre, sobre las 8 de la noche los defensores de derechos humanos **Johana Rodríguez**, **Viviana Cortes** y **Fabio Ariza** junto a una organización internacional de acompañamiento,  fueron observados insistentemente e **interrogados por 3 hombres** cuando ingresaron a un local de comidas en el municipio de Mapiripán. Esa misma noche, 3 motocicletas en las que se movilizaban **6 paramilitares** pasaron frente a la casa del reclamante de tierras William Aljure, en la que se encontraban hospedados los defensores.  ]

[En la tarde de ayer, estos defensores cuando regresaban de una comunidad indígena en vehículos de la UNP, fueron **grabados en un celular**, por un hombre vestido con camiseta blanca que se movilizaba en un bus al servicio de la **empresa de palma aceitera POLIGROW**.]

[Durante la noche continuaron los s**eguimientos para los defensores y sus escoltas de la UNP**, quienes fueron hostigados por hombres de civil que se movilizan en motos, mientras notificaban los hechos ante unidades policiales, uno de estos hombres **golpeó los vidrios de la camioneta** para cerciorarse del blindaje del vehículo.]
