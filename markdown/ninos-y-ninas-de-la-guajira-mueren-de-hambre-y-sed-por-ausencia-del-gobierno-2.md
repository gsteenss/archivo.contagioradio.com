Title: Niños y niñas de la Guajira mueren de desnutrición por ausencia del gobierno
Date: 2015-06-03 13:48
Category: DDHH, Entrevistas
Tags: empresa el Cerrejón, extinción de comunidades indígenas, Guajira, Luis Fernando Arias, niños desnutridos, ONIC, Presidente Santos, Pueblo Wayú, sed y hambre en la Guajira
Slug: ninos-y-ninas-de-la-guajira-mueren-de-hambre-y-sed-por-ausencia-del-gobierno-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Wayuu_Desierto_La_Guajira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [commons.wikimedia.org]

<iframe src="http://www.ivoox.com/player_ek_4590244_2_1.html?data=lZqmkpeYeI6ZmKiak5uJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRksqZpJiSpJbTt4ztjNPWh6iXaaOlwtiYxsqPsMKfqNrOzM7WpYzh1srfx9OPqMafycbaxNfJb9qf1MrRjdXTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Fernando Arias, consejero mayor ONIC] 

###### [3 de Junio 2015] 

**Desde el año 2008, a la fecha han muerto 5.000 niños y niñas de la comunidad Wayú** en la Guajira, según datos de la Defensoría del Pueblo, citados por el consejero mayor de la Organización Indígena de Colombia, ONIC, quien asegura que la **desnutrición es la principal causa de la desaparición de las comunidades indígenas** en el país.

Arias, afirma que los pueblos indígenas están en riesgo de desaparición física y cultural. El caso de la Guajira es uno de los más graves, debido a que en los lugares donde están asentadas las comunidades no hay agua, **el Río Ranchería “no tiene una gota de agua”,** y la respuesta de las autoridades regionales y nacionales es que no existe contratos para enviar carrotanques con el mínimo vital.

Cabe recordar, que la primera región que visitó el presidente Santos cuando se posesionó el pasado 7 de agosto, fue la zona extrema de la alta Guajira, donde **prometió hacer un pozo por cada corregimiento y  hasta la  fecha no se iniciado ningún** tipo de obra para proveer de agua a los indígenas.

“No hay agua potable para el consumo humano, no hay agua para los animales y no hay agua para los cultivos de pancoger”, lo que genera la problemática de la desnutrición, como consecuencia de **la explotación mineroenergética a gran escala por parte de empresas como el Cerrejón** que quieren desviar el Río Ranchería para “condenar a muerte a todo el pueblo” que “lo único que han dejado estas empresas a las comunidades es  miseria”, dice Luis Fernando Arias.

**“Desde el gobierno nacional se está intentado ocultar la realidad de la Guajira**”, expresa el consejero mayor de la ONIC, ya que según él, el gobierno se olvidó de defender los intereses nacionales y se decidió “entregarle el país a los intereses de las multinacionales” que son los que se están llevando las grandes utilidades, mientras que los pueblos siguen en la miseria.

Sumado a la problema de las multinacionales, **la corrupción** es otra lo de las causas que nombra Arias, y lo que tiene a la **comunidad Wayú, conformada por 300 mil indígenas,** en condiciones muy difíciles, que solo se pueden solucionar aplicando mecanismos que apunten a una respuesta estructural donde se convoque a la población y la hagan partícipe, como lo señala el líder indígena.

Por su parte, la ONIC  ha dado a conocer esta problemática a todas las instancias nacionales, y ahora, **se está llevando el caso a organismos internacionales** para que se presione al gobierno colombiano y se adopten medidas con el fin de que se atienda esta problemática.
