Title: Más de 30 municipios del país marcharán en defensa de las consultas populares
Date: 2017-05-31 15:30
Category: Ambiente, Nacional
Tags: consultas populares, marcha por el agua, Marcha por la Vida, Territorios
Slug: mas-de-30-municipios-del-pais-marcharan-en-defensa-de-las-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/consulta-popular-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía de Ibagué] 

###### [31 may 2017] 

Este 2 de Junio, cerca de 30 municipios de Colombia se darán cita para la **Marcha Carnaval Nacional e Internacional en Defensa del Agua la Vida y el Territorio.** El objetivo de la movilización será sentar la voz de las comunidades en contra de las actividades extractivas que destruyen el ambiente y exigir al gobierno respeto por las consultas populares.

Los y las integrantes de la Mesa Ambiental Nacional, 30 municipios en Colombia que participarán en la movilización y  los 3 plantones a nivel internacional seguirán insistiendo en la **defensa del territorio y la consulta popular como mecanismo legítimo y democrático.** Esto frente a la amenaza de los proyecto mineros y energéticos que ponen en riesgo el derecho colectivo al ambiente sano. Le puede interesar: ["Actividad petrolera ya había contaminado 49 aljibes y 58 pozos de agua en Acacias, Meta"](https://archivo.contagioradio.com/actividad_petrolera_habia_contaminado_pozos_aljibes_ecopetrol/)

Participantes de comunidades del Tolima, Caquetá, Bogotá, Quindío y Cesar, **promovieron y exigieron formas de vida ,** de construcción de paz y de desarrollo respetuosas de los territorios y el medio ambiente. Para Renzo Alexander García, miembro del Comité Ambiental en Defensa de la Vida "las comunidades se la van a jugar toda por la defensa del ambiente sano y por la democracia en la defensa a la consulta popular como un mecanismo de participación ciudadana para proteger los territorios".

**Defensa de la consulta popular**

En repetidas ocasiones, el gobierno nacional ha hecho énfasis en que la consulta popular es un mecanismo que está siendo utilizado por las comunidades de manera desproporcionada y pone en entre dicho el modelo de desarrollo. Según García, "el gobierno y especialmente el Ministro de Minas Germán Arce, **tiene que respetar la ley colombiana que ratifica a las consultas populares como mecanismos vinculantes que agrupan la voluntad de las comunidades".** Le puede interesar: ["Ecos de la consulta popular en Cajamarca"](https://archivo.contagioradio.com/ecos-de-la-consulta-popular-de-cajamarca/)

El día de hoy, la Mesa Ambiental Nacional, le hizo llegar al Ministro de Minas, un derecho de petición para que aclare porqué ha dicho que las consultas populares no son vinculantes y que desconocen la ley. Para García, **"estas expresiones no tienen ninguna certeza y queremos preguntarle porqué lo afirma así".** Le puede interesar:["Aprueban consulta popular sobre actividades petroleras en Pasca, Cundinamarca"](https://archivo.contagioradio.com/aprueban_consulta_popular_pasca_petroleo/)

Los diferentes comités que velan por la protección de los territorios, el agua y la vida, defienden la consulta popular como **"un reflejo de la situación actual del país**, donde las actividades mineras amenazan con crear cambios significativos del uso del suelo".  Con la defensa de las consultas populares, García aseguró que "la población está poniendo como prioridad proyectos agro alimentarios, agropecuarios y ecoturísticos frente a  proyectos mineros que se fortalecen en recursos no renovables". Le Puede interesar: ["Lista la consulta popular de minería en Pijao, Quindío"](https://archivo.contagioradio.com/lista-la-consulta-popular-de-mineria-en-pijao-quindio/)

Finalmente las comunidades miembros de la Mesa Nacional Ambiental le recordaron a la población en general que la marcha será una actividad que combina actividades como el Festival de Caricatura contra la gran minería en el Quindío y la participación juvenil de encuentros para la protección de los territorios el agua y la vida. Han denominado la marcha como **"un grito de alegría por el privilegio de vivir en un país mega diverso, multicultural y rico en agua".**

<iframe id="audio_19008319" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19008319_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en **[su correo](http://bit.ly/1nvAO4u) **o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por **[Contagio Radio](http://bit.ly/1ICYhVU).** 
