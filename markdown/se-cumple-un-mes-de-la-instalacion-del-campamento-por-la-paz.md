Title: Se cumple un mes de la instalación del Campamento por la Paz
Date: 2016-11-03 11:58
Category: Movilización, Nacional
Tags: acuer, Campamento por la Paz
Slug: se-cumple-un-mes-de-la-instalacion-del-campamento-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/campamento-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia] 

###### [3 de Nov 2016] 

Desde el 5 de octubre un pequeño grupo de personas llego a instalarse en la Plaza de Bolívar, en el **Campamento por la Paz**, con la intensión de quedarse allí hasta que se firmen los acuerdos de paz entre las FARC-EP y el Gobierno Nacional, hoy ya son aproximadamente 70 carpas las que se mantienen en este lugar y han logrado gestar una acción nacional que se **replica en ciudades como Montería, Cali, Manizales, Cartagena, Armenia, entre otras.**

De igual forma, el Campamento por la Paz estaría pensándose, hacía un futuro como una **veeduría ciudadana** **que participe de la implementación de los acuerdos y de la verificación y dejación de armas**. “Es en el ciudadano y en el día a día que nosotros construirnos paz, nuestro papel va a estar muy encaminado en que si bien este es un primer paso, debemos empezar a generar confianza entre desconocidos y transformar la cultura”, expreso Catherine Miranda,  una de las ciudadanas que se ha integrado a este movimiento.

Para  Miranda, lo más importante **“ha sido ver que Colombia está despertando y la política se está transformando de lo electoral a la incidencia en la vida día a día”**. Sin embargo, también han existido muchos riesgos y dificultades para mantener el campamento como lo ha sido el clima durante este último mes de lluvias y las diferentes amenazas que han llegado a algunos miembros del campamento. Le puede interesar:["Amenazados integrantes  del Campamento por la Paz"](https://archivo.contagioradio.com/amenazados-integrantes-del-campamento/)

Sobre la coordinación con los otros campamentos Miranda señala que **se han mantenido en permanente comunicación y apoyo**, colaborándose mutuamente con lo que se necesite “generamos una solidaridad muy linda alrededor de los campamentos” afirmó.

Todos los días hay actividades en los Campamentos por la Paz, desde yoga hasta cine foros, pasando por tertulias y debates, actividades enfocadas a seguir construyendo paz desde la ciudadanía. **Visitas como la del presidente Santos y el cantautor Piero**, reafirman la fuerza que ha tomado esta iniciativa que incluso fue invitada por el equipo de conversaciones de las FARC-EP a la Habana. Le puede interesar:["Entre vigilias y campamentos se hace llamado por la paz"](http://%22Entre%20vigilias%20y%20campamentos%20se%20hace%20llamado%20por%20la%20paz%22)

<iframe id="audio_13600673" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13600673_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
