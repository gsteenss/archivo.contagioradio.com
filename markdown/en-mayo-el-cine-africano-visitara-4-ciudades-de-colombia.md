Title: En Mayo el cine africano visitará 4 ciudades de Colombia
Date: 2019-04-30 09:22
Author: CtgAdm
Category: 24 Cuadros
Tags: Cine, cine africano, mes afrocolombianidad
Slug: en-mayo-el-cine-africano-visitara-4-ciudades-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Muica-2019.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Muica 

Con una parada en Buenaventura, pasando por Cali, Cartagena y Bogotá, vuelve al país la **Muestra Itinerante de Cine Africano MUICA**, que para su tercera edición llega cargada de "vaqueros y samurais, de dictadores títere y títeres de dictadores, de músicos de colores y super héroes de barrio" contenidas en 21 producciones sobre el continente y su diáspora.

La muestra que tendrá lugar del 2 al 29 de mayo, mes de la herencia africana en Colombia, vendrá acompañada por **conversatorios, debates, talleres, espacios de exhibición y experiencias de realidad virtual**, a desarrollarse en universidades, centros culturales, bibliotecas, colegios y barrios de las ciudades que hacen parte de la MUICA.

#### Una muestra, tres espacios

La programación está dividida en 3 secciones: **Hecho en África**, el plato fuerte, exclusivo para las historias de creativos africanos; **Diáspora**, que acompaña con historias desde la afrodescendencia en el mundo; y, finalmente, se suma **Otras Miradas**, con relatos extranjeros que se sumergen en el universo africano. En cada una el público podrá disfrutar estrenos mundiales y obras que por primera vez se verán en nuestro país.

Desde los bosques de Liberia y el corazón del Sahara, hasta la sabana sudafricana y los barrios de Nairobi, con una dosis de realismo mágico entrelazado con folclor africano.  también se levantan por la resistencia social, la reivindicación de la diversidad y la celebración del placer femenino. abren una ventana a un continente vasto y diverso, tanto en sus realidades sociales como en sus expresiones artísticas.  el taller de cine africano África Rexiste,

##### Realidad virtual y fotografía

Adicionalmente, por primera vez en Colombia se presentará la **muestra de realidad virtual New Dimension**s, donde cuatro cortometrajes de artistas provenientes de Kenia, Ghana y Senegal, abren una ventana a realidades imaginadas y urbanas, oníricas y fantásticas, que los realizadores del continente exploran a través de las nuevas tecnologías.

Otro de los espacios es la exposición fotográfica **ANGOLA CINEMAS: Una ficción de la libertad.** Un viaje en el tiempo, nostálgico y sorprendente, por una serie espacios que en otra época fueron centros para el entretenimiento y el consumo audiovisual en la Angola colonial.

Los autores del proyecto, los angolanos **Miguel Hurst y Walter Fernandes**, registran estos espacios construidos por los portugueses durante los años en que ocuparon el país; esas antiguas salas de cine –en las que el art-deco se encuentra con la arquitectura experimental en medio del trópico africano– que ahora se encuentran en desuso, abandonadas u olvidadas, y que invitan a reflexionar sobre el pasado y el futuro del legado cultural en una sociedad marcada históricamente por un brutal conflicto.

<iframe id="doc_84601" class="scribd_iframe_embed" title="CONTENIDOS - MUICA 2019" src="https://es.scribd.com/embeds/408181272/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-pQSkkn9rM8qS4kPOkfQh&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
