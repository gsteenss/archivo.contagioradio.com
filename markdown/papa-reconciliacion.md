Title: Visita del Papa aportará a la reconciliación
Date: 2017-09-06 13:32
Category: Política
Tags: iglesia católica, Papa Francisco, visita del Papa
Slug: papa-reconciliacion
Status: published

###### [Foto: Archivo] 

###### [06 Sept. 2017] 

El Papa Francisco llega a Colombia en un momento importante para la construcción histórica del país debido a su apoyo al proceso de paz entre el Gobierno Nacional y la Guerrilla de las FARC además ha hecho hincapié a dar “el primer paso” y propender por la reconciliación de los y las colombianas.

**Henry Ramírez Soler, misionero claretiano asegura que el Papa Francisco sin tapujos ha “planteado el tema central de su visita que es el tema de la reconciliación** desde la perspectiva de la construcción de la paz y yo creo que hay que entenderlo en el contexto en que estamos”.

### **Discurso del Papa también invita a la reconciliación con el ambiente** 

En su más reciente discurso, el Papa Francisco manifiesta que hay que cuidar el medio ambiente porque hay una destrucción salvaje y que daña la creación de la madre tierra, lo que para el padre Ramírez es una apreciación acertada y que llega en un momento muy importante para el país. Le puede interesar: [Especial: Siete pecados de la iglesia católica en el conflicto sociopolítico](https://archivo.contagioradio.com/especial-siete-pecados-de-la-iglesia-catolica-en-el-conflicto-sociopolitico/)

“Por ejemplo muchas poblaciones hoy han manifestado a través de las consultas populares su negativa a la explotación petrolera, hay una fuerte movilización social en contra de los proyectos minero-energéticos y ese mensaje es significativo para el movimiento ambientalista que adelantan este tipo de causas en defensa de la vida y la creación”.

### **¿Algo va a cambiar en Colombia con la visita del Papa?** 

Para el padre Ramírez es importante tener claro que frente a cualquier hecho hay posibilidades y limitaciones, lo que se traduce en que el Papa Francisco si bien conoce las realidades de los países que visita, también está atado a una doble condición entendida como el “hermano mayor” de la Iglesia Católica y el jefe de Estado. Le puede interesar: [Ante llegada del Papa católicas invitan a "dar el primer paso" por los derechos de las mujeres](https://archivo.contagioradio.com/46276/)

“En ese orden de ideas el tema de la diplomacia estatal incide. **Que vaya a haber cambios sustanciales o estructurales en la situación colombiana no creo**, lo que si es que la visita del Papa va a significar es que será un motor para sentir que estas causas no son de unos pocos, sino que hoy tienen eco en un sector de la Iglesia. Es decir, el Papa incidirá, pero no cambiará la compleja realidad del país”.

### **Cómo interpretar que la Diócesis Castrense organice la visita papal** 

La existencia de un obispado castrense en Colombia ha generado contradicciones al interior de esa Iglesia porque, dice el Padre, que de alguna manera las fuerzas militares han estado implicadas en serias y sistemáticas violaciones a los derechos humanos. Le puede interesar: [Respetado Papa Francisco - Carta desde el Closet](https://archivo.contagioradio.com/respetado-papa-francisco-carta-desde-el-closet/)

**“Muchas veces los militares han estado acompañados por miembros de la Iglesia Católica,** entonces creemos que esto es un signo bastante negativo pero que de alguna manera es la realidad que tiene la Iglesia colombiana. Pero esta es la oportunidad de estar hablando de la necesidad de suprimir la diócesis castrense”.

### **El Papa Francisco no ha dicho nada nuevo hasta ahora** 

Las referencias a las que hace alusión el sumo pontífice en sus alocuciones no son nuevas, puesto que todas esas referencias estaban ya escritas en el Vaticano II, sin embargo, la gran novedad es que luego de 50 años de esa promulgación de documentos **se comience a enunciar la riqueza ecológica, pastoral y de doctrina social de la Iglesia** que no ha sido suficientemente explorada, desarrollado o asumida.

“Lo que está haciendo Francisco hoy es poner esos principios doctrinales que el Vaticano II nos dejó pero que desafortunadamente por la inercia, la apatía, los intereses mezquinos de distintos sectores de la Iglesia pues no se han desarrollado”. Le puede interesar: [La petición de Gloria Gaitán al Papa por beatificación de sacerdote](https://archivo.contagioradio.com/gloria-gaitan-beatificacion-sacerdote/)

### **¿Habrá cambios profundos en la Iglesia Católica con el papado de Francisco?** 

No son muchos los cambios profundos que la Iglesia podrá hacer pese a la apertura del discurso del Papa Francisco, puesto que, asevera Ramírez, hoy existen movimientos al interior de esa institución que van en contravía de lo que el Vaticano II ha planteado, incluso llegando al punto de deslegitimar al Papa o solicitar misas en latín, de espaldas como en otras épocas.

**“Los cambios en la Iglesia y en cualquier estructura social no necesariamente se dan desde arriba**, el reconocimiento de la mujer, de la ministerialidad de la mujer, una Iglesia pobre, no se da por decreto sino por el trabajo de movimientos de base de la Iglesia católica que generen esos cambios, porque estamos en tiempos de retrocesos”.

Concluye diciendo que es importante entonces ser contradictores y confrontadores de muchas realidades y posturas de la Iglesia católica y sus feligreses más conservadores para generar cambios.Le puede interesar: [Francisco pidió perdón por escándalos de la iglesia](https://archivo.contagioradio.com/papa-francisco-perdon/)

<iframe id="audio_20730784" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20730784_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
