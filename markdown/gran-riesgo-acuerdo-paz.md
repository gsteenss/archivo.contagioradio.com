Title: Asesinato de excombatientes, el gran riesgo para el Acuerdo: Defendamos la Paz
Date: 2019-07-13 10:49
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdo de paz, Consejo de Seguridad, excombatientes, Naciones Unidas
Slug: gran-riesgo-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Riesgos-del-proceso-de-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @DefendamosPaz  
] 

Tras la no comparecencia de Jesús Santrich ante la Corte Suprema de Justicia, algunas corrientes de opinión aseguraron que era el mismo líder político de FARC quien ponía en riesgo el Acuerdo de Paz; sin embargo, en medio de la visita al país del Consejo de Seguridad de la Organización de las Naciones Unidas (ONU), Defendamos la Paz emitió un comunicado en el que señala que no es Santrich, sino las constantes amenazas a los más de 10 mil excombatientes que están cumpliendo con su proceso de reincorporación los que ponen en vilo el Acuerdo.

El senador Iván Cepeda resaltó que el Consejo de Seguridad es la máxima instancia de la ONU, allí participan las principales potencias del mundo "y tienen una influencia en las decisiones esenciales que se toman en el seno de la comunidad internacional", de forma tal, que la visita de esta entidad es de suma importancia para el proceso de paz en Colombia. (Le puede interesar: ["ONU insta a que Gobierno acelere la implementación del Acuerdo"](https://archivo.contagioradio.com/onu-insta-gobierno-acelerar-implementacion-acuerdo-paz/))

> ?[\#Comunicado](https://twitter.com/hashtag/Comunicado?src=hash&ref_src=twsrc%5Etfw) “El Consejo de Seguridad viene a expresar su apoyo absoluto al proceso de paz de Colombia”: Carlos Ruiz Massieu, Jefe de [@MisionONUCol](https://twitter.com/MisionONUCol?ref_src=twsrc%5Etfw) ▶️ <https://t.co/tig22JiZ4Z> [pic.twitter.com/eXfDZCcCsv](https://t.co/eXfDZCcCsv)
>
> — Misión de la ONU en Colombia (@MisionONUCol) [11 de julio de 2019](https://twitter.com/MisionONUCol/status/1149385349497610241?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Los factores de riesgo para la implementación del Acuerdo**

Cepeda afirmó que en la reunión que tendrá el Consejo con las comisiones de paz de Senado y Cámara, plantearán los avances del proceso de implementación, pero también de las dificultades, "ello tiene que ver con las amenazas del proceso". Entre los factores de riesgo, el Senador destacó el asesinato de líderes sociales y excombatientes, el intento de hacer constantes modificaciones al acuerdo, y el incumplimiento a la implementación con enfoque integral y territorial. (Le puede interesar: ["La desaparición de Santrich no puede ser un impedimiento para buscar la paz"](https://archivo.contagioradio.com/la-desaparicion-de-santrich-no-puede-ser-un-impedimento-para-buscar-la-paz/))

En el mismo sentido, el comunicado de Defendamos la Paz sostiene que los "10.708 exintegrantes de las FARC-EP (...) están cumpliendo a cabalidad las responsabilidades adquiridas en el Acuerdo Final" y son la mejor demostración de que el "proceso de paz es una realidad en desarrollo y consolidación". Defendamos la Paz dice que lo ocurrido con Santrich no puede poner cuestionar la efectividad del Acuerdo de Paz, y en el marco de la visita del Consejo de Seguridad, "ponemos de presente que los crímenes contra las personas que están en proceso de reincorporación constituyen una de las más serias amenazas contra el proceso de paz".

> Garantías para quienes dejaron las armas | Declaración del movimiento [\#DefendamosLaPaz](https://twitter.com/hashtag/DefendamosLaPaz?src=hash&ref_src=twsrc%5Etfw)
>
> El movimiento [@DefendamosPaz](https://twitter.com/DefendamosPaz?ref_src=twsrc%5Etfw) registra con gran preocupación la cadena sistemática de violencia contra quienes dejaron las armas en varias regiones del país. [pic.twitter.com/U2iLQ36Lwf](https://t.co/U2iLQ36Lwf)
>
> — DefendamosLaPazColombia (@DefendamosPaz) [12 de julio de 2019](https://twitter.com/DefendamosPaz/status/1149624496451489792?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
**Síguenos en Facebook:**

</p>
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38372664" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38372664_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
