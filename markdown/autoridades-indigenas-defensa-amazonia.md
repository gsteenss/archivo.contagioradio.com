Title: Autoridades indígenas piden que la Amazonía sea declarada libre de concesiones minero-energéticas
Date: 2019-10-08 16:26
Author: CtgAdm
Category: Ambiente
Tags: Amazonía, defensa del medio ambiente, Ficamazonía, Líderes indígenas
Slug: autoridades-indigenas-defensa-amazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/FICAMAZONIA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @FICAMAZONIA] 

Abuelos y abuelas de siete comunidades indígenas de Putumayo, Caquetá, Amazonas, Boyacá y Venezuela se reunieron para conversar sobre la importancia de preservar el bosque tropical más grande del planeta. Los 18 abuelos y abuelas, reunidos para hablar sobre el presente y el futuro de la Amazonía, hicieron un llamado a todo el mundo a cuidar el pulmón del planeta.

El encuentro de sabiduría ancestral se produjo en el marco del Festival Internacional de Cine y Ambiente de la Amazonía, “Ficamazonía” en Mocoa, Putumayo. Éste ha sido muy significativo puesto que hacía más de 50 años que no se llevaban a cabo los Consejos de Ancianos Mayores de los Pueblos Indígenas de América Latina. (Le puede interesar: "[400 federaciones y comunidades se movilizan en defensa de la Amazonía](https://archivo.contagioradio.com/movilizacion_amazonia/)")

### Un manifiesto en pro de la Amazonía

En el Diálogo apelaron a la cancelación de licencias para la exploración y explotación de los recursos naturales de la “madre tierra” y pidieron “que la Amazonía sea declarada libre de concesiones minero-energéticas”. En el manifiesto, también hacen una clara alusión al Gobierno y a todos aquellos grupos al margen de la ley a que velen por la conservación de la Amazonía, sus tribus y sus tradiciones, puesto que remarcan que para ellos "el bosque lo es todo". Además, alertan que sus pueblos están declarados en riesgo de desaparecer física y culturalmente (Le puede interesar "[Con la Amazonía está en juego la supervivencia de la humanidad](https://archivo.contagioradio.com/amazonia-supervivencia-humanidad/)")

Asimismo, insistieron en la necesidad de no ser perseguidos por el cultivo de coca, así como que sí se frene el uso de semillas transgénicas y no implementar el fracking ni el uso del glifosato, ya que consideran que estas prácticas destruyen la biodiversidad, la vida, la salud, la autonomía y la soberanía alimentaria de sus comunidades. "Pedimos que la Amazonia sea declarada libre de concesiones minero-energéticas”. También reiteraron en la importancia de no perseguir ni estigmatizar a sus líderes sociales.

En el documento, se exponen una serie de propuestas de los abuelos indígenas para la preservación de la Amazonía, así como para la preservación de sus pueblos. Tras la presentación del manifiesto, se espera que se pueda organizar otro encuentro para analizar en qué puntos se muestran avances.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
