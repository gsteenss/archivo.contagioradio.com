Title: Comunidades Negras exigen discusión de sus propuestas en la Habana
Date: 2016-06-20 13:24
Category: DDHH, Nacional
Tags: comunidades negras, proceso de paz
Slug: comunidades-negras-exigen-mayor-espacio-de-discusion-en-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/comunidades-negra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFRODES] 

###### [20 Junio 2016 ]

Se realizó en Melgar, Tolima, la segunda sesión de Consulta Previa del Censo Nacional con las Comunidades Afrocolombianas. En el escenario participaron 200 delegados y pesé a que ratificaron su apoyo al proceso de paz, expresaron su preocupación por la invitación a una audiencia con las mesas de negociación en la Habana, ya que consideran que este **no es el escenario propicio para la participación de estas comunidades** puesto que no permite la discusión y concertación de sus debates y propuestas.

La invitación que se realizó el pasado 2 de junio, por parte de los grupos negociadores, a diez integrantes de las comunidades afrocolombianas, raizales y palenqueras,  se llevará a cabo los días 26 y 27 de Junio, y tendrá una metodología a modo de audiencia, espacio que según las diferentes comunidades Afros **no otorga las herramientas apropiadas para poder genera debates y acuerdos  o plantear las propuestas entre el Gobierno, las FARC-EP y las comunidades.**

Marino Córdoba, miembro de Consejo Nacional de Paz Afrocolombiano y de la Asociación Nacional de Afrocolombianos Desplazados (AFRODES), asegura que la apuesta de consulta previa es válida y positiva. Sin embargo,  organizaciones como AFRODES han adelantado por casi dos años un trabajo de incidencia para que los grupos negociadores en la Habana acepten una delegación de grupos étnicos que les permita **plantear sus propuestas y preocupaciones,** [con el fin de que el proceso de paz, sea un proceso duradero, incluyente y que permita aclarar dudas](https://archivo.contagioradio.com/al-lo-menos-2-de-los-4-3-millones-de-afros-en-colombia-son-victimas-del-conflicto/).

De otro lado, Córdoba expone la necesidad de un tiempo más amplio para que las personas que conforman la delegación que viaja a la Habana, [puedan exponer sus propuestas de paz y unificarlas](https://archivo.contagioradio.com/indigena-y-afro-se-unen-para-presentar-propuestas-frente-al-proceso-de-paz/), esto con el fin de que exista un **compilado que devele las necesidades de todas las comunidades** negras del país.

"Es necesario que se escuchen las propuestas de las comunidades negras en la Habana, de lo contrario si no hay un proceso de consultas con estas comunidades, incluyendo las zonas en donde se realizarán concentraciones de los desmovilizados, **se dará paso a situaciones difíciles para el mismo proceso**" ratifico Marino Córdoba.

<iframe src="https://co.ivoox.com/es/player_ej_11967418_2_1.html?data=kpammJyYdZmhhpywj5aVaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5ynca7V087b0ZCnaaSnhqeg1MnTpsKfjpCuqLeziKbHjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
