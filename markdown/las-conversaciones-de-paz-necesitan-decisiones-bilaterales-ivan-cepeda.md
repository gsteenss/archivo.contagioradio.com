Title: Las conversaciones de paz necesitan decisiones bilaterales: Ivan Cepeda
Date: 2015-02-13 20:24
Author: CtgAdm
Category: Entrevistas, Paz
Tags: cese al fuego unilateral FARC-EP, Cese unilateral de fuego, conversaciones de paz, FARC, Iván Cepeda
Slug: las-conversaciones-de-paz-necesitan-decisiones-bilaterales-ivan-cepeda
Status: published

###### Foto: univisiónkansas 

<iframe src="http://www.ivoox.com/player_ek_4081179_2_1.html?data=lZWlk5abfY6ZmKiak5aJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMLnjMjc0NvJttTVxM7c0MrXb8XZjNXO3JDSqcTZ1M7hw9OPqMbXytjW0dPJt4zWytHO1srWpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### **[Entrevista con el Senador Iván Cepeda]** 

Tras el anuncio de la delegación de paz de las FARC, de no permitir, de ahora en adelante, que personas menores de 17 años integren sus filas, es necesario que el gobierno colombiano adopte medidas, puesto que las conversaciones de paz se desarrollan entre dos partes, señala el senador del **Polo Democrático, Iván Cepeda**, quien también integra el Frente Amplio por la Paz. **No se puede esperar que sean solo las FARC las que sigan tomando las decisiones**, puntualiza el senador.

Según el senador, el anuncio de las FARC y los resultados del último ciclo de conversaciones son decisiones históricas, entre ellas está el informe de la **Comisión de Memoria Histórica del Conflicto y sus Víctimas**, la definición de los puntos a discutir en la **Sub Comisión de cese bilateral de fuego y dejación de armas** y los resultados de la **sub comisión de género**.

En la mañana del 12 de Febrero las FFMM anunciaron la captura del comandante del **Frente 28 de las FARC** tras un bombardeo contra una fuerza guerrillera en tregua, por ello el senador del Polo Democrático insistió al gobierno en la necesidad de dar pasos concretos y certeros para no seguir poniendo en riesgo el cese unilateral de las FARC. Cepeda también recordó que esa guerrilla lleva **5 treguas unilaterales y hasta el momento no recibe una respuesta en el mismo sentido por parte de las FFMM**.
