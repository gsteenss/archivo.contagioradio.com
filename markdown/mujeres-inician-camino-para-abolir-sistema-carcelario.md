Title: Mujeres inician camino para abolir sistema carcelario
Date: 2019-08-27 15:09
Author: CtgAdm
Category: Expreso Libertad
Tags: abolición, carceles, cárceles colombia, cárceles mujeres, movimiento internacional feminista abolicionista
Slug: mujeres-inician-camino-para-abolir-sistema-carcelario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/enceuntro-mujeres-carceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Durante el mes de Julio, en Bogotá, diversas mujeres de distintas latitudes se encontraron para dar el primer paso en la conformación de un movimiento internacional feminista abolicionista.

Estas mujeres, tienen el objetivo de que la sociedad comprenda lo nefasto del sistema carcelario y las afectaciones que genera sobre las vidas de quienes estuvieron allí adentro. ([tambien puede ver: Los retos de las mujeres en Libertad](https://archivo.contagioradio.com/los-retos-de-las-mujeres-en-libertad/))

En este programa del Expreso Libertad escucharemos las voces de Toedora del Carmen, del Salvador y Kenya Cuevas, de México, quienes además de participar en este encuentro, comentaron para nuestros micrófonos la situación que viven las mujeres en las cárceles de sus respectivos países.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F632224337184200%2F&amp;width=800&amp;show_text=false&amp;appId=1237871699583688&amp;height=413" width="800" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
