Title: “La limpieza social, una violencia mal nombrada” informe del CNMH
Date: 2016-04-19 11:04
Category: DDHH, Nacional
Tags: Bogotá, Centro Nacional de Memoria Histórica, ciudad bolivar, Limpieza social, paramilitares
Slug: la-limpieza-social-una-violencia-mal-nombrada-informe-del-cnmh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/informe-limieza-social-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CNMH] 

###### [19 Abril 2016] 

Según el informe más reciente del CINEP **entre 1988 y 2013 fueron asesinadas por lo menos 5000 personas en la marco de la mal llamada “limpieza social”**, fenómeno que ha afectado a 356 municipios del país y que, según el Centro Nacional de Memoria Histórica, cuenta con una estela de “aprobación” generalizada y cotidiana bajo consignas como “lo que no sirve que no estorbe”.

Además, una de las ciudades más golpeadas por esta manera de asesinar es Bogotá, donde se han registrado 189 casos con 346 homicidios, de los cuales el 2**8% se perpetraron en la [localidad de Ciudad Bolívar](https://archivo.contagioradio.com/aparecen-dos-panfletos-mas-en-bogota-del-bloque-capital-de-las-aguilas-negras/)**, además el CNMH reconoce que el sub registro es abrumador, lo que significaría que hay muchos casos más.

El CNMH menciona que uno de los problemas para enfrentar este fenómeno es el silencio y el hermetismo, que en algunas ocasiones parte de las mismas familias y de la presión social que se ejerce tras la divulgación de mensajes **como que los jóvenes asesinados son “desviados” y que por ello los crímenes se justificarían**, además, los perpetradores, muchas veces hacen parte de círculos de poder enquistados en los barrios en los que se realiza esa práctica macabra.

Por su parte, organizaciones sociales en Ciudad Bolívar y otras localidades de Bogotá han coincidido en afirmar que este tipo de violencia ha servido a [grupos paramilitares](https://archivo.contagioradio.com/?s=paramilitarismo) para consolidar su poder en los barrios, a la sombra de esa práctica “justificada” y **con la complicidad de los lenguajes de los medios de comunicación y las autoridades de policía que atribuyen muchas de las muertes a “enfrentamientos entre bandas”.**

Lo cierto es que la mal llamada “Limpieza Social” no solamente ha echado raíces en los barrios de Bogotá, sino que **se ha convertido en una práctica que azota a todo el país** y que podría avanzar de no ser enfrentado.

El CNMH lanzará este 20 de Abril su informe "[[Limpieza Social: Una violencia Mal Nombrada"](https://archivo.contagioradio.com/limpieza-social-una-muestra-de-la-degradacion-de-la-guerra-en-colombia/)] a las 5 pm en el auditorio Jorge Isaacs, en el marco de la 29 Feria del Libro de Bogotá. Según el informe, este fenómeno continúa y **“la ausencia de un pronunciamiento del Estado extiende patente de corso a quienes perpetran la matanza”**.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
