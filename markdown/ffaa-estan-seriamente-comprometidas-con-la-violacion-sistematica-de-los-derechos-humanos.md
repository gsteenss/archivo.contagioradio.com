Title: “FFAA están seriamente comprometidas con la violación sistemática de los Derechos Humanos”
Date: 2015-06-24 15:57
Category: DDHH, Entrevistas
Tags: comision de la verdad colombia, Conversaciones de paz de la habana, Coorporación Jurídica Libertad, falsos positivos, FFMM, Human Rights Watch, madres de soacha
Slug: ffaa-estan-seriamente-comprometidas-con-la-violacion-sistematica-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/falsos-positivos-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4683833_2_1.html?data=lZullZ2Xd46ZmKiak56Jd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56niqe1opDS1dmJh5SZopbbjdjJtsrVzsrb1sqPp9Dh0dfcz8rYrcXV1JDQ0dOPsMKf187czsbHrYa3lJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Adriana Arboleda. CJL] 

###### [24 jun 2015] 

Reciente informe de **Human Rights Watch (HRW)** señala que por lo menos 16 generales, algunos de ellos en servicio activo como el actual Comandante de las FFMM Juan Pablo Rodríguez, y el comandante del Ejército, General Jaime Lasprilla, están comprometidos con la comisión de ejecuciones extrajudiciales, conocidas como "Falsos Positivos".

Según la ONG hay una práctica sistemática que debería conducir a los altos mandos militares ante los tribunales y a que el gobierno de Estados Unidos suspenda la ayuda militar a Colombia, que está condicionada al respeto de los DDHH.

Por su parte, para Adriana Arboleda, abogada de la **Coorporación Jurídica Libertad**, de Medellín, es muy favorable que una organización internacional corrobore lo que organizaciones de DDHH nacionales han venido denunciando y **es la práctica sistemática de las ejecuciones extrajudiciales** y también la responsabilidad de altos mandos de las FFMM en dichos hechos “era imposible que ellos no supieran” señala Arboleda.

Los informes tanto nacionales como internacionales dan cuenta de prácticas macabras, como el **pago a investigadores judiciales para acomodar las escenas del crimen**, la contratación de reclutadores de jóvenes víctimas, las amenazas contra los directos responsables para que no se vincule a altos mandos, las directrices del Ministerio de Defensa para elevar las cifras de “positivos” y la **dilación de los procesos de juzgamiento contra militares en etapa de juicio.**

La bogada agrega que aunque el presidente Santos ha reiterado que la doctrina militar no se toca en el proceso de conversaciones de paz, es imposible que no se hable de ello, puesto **que los “falsos positivos” no se dieron en el marco del combate,** y eso amerita que en esta etapa de los diálogos se plantee la situación de los altos mandos que hoy están en retiro, y de los comandantes en servicio que siguen aplicando la misma doctrina que promueve esos crímenes.

En el tema de la justicia, el debate se centra en ver qué hacer con estos casos. Para Adriana Arboleda, debe considerarse el punto hacia el cual dirigir la investigación de la Fiscalía y en ese sentido organizaciones de DDHH afirman que **debe primar el derecho de las víctimas** y se deben orientar los procesos para que se juzgue a los **altos mandos militares, como máximos responsables.**

Sin embargo para algunas organizaciones, **no todo debe estar orientado a la justicia penal**, sino que debe revisarse el problema de manera integral, entendiendo los alcances políticos de las violaciones a los DDHH, que lleve a plantear una reestructuración de las FFMM.
