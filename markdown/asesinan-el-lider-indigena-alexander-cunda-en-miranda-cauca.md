Title: Asesinan al comunero indígena Alexander Cunda en Miranda, Cauca
Date: 2019-03-11 18:12
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: EPL, FARC, FARC-EP, Norte del Cauca, Resguardo Indígena
Slug: asesinan-el-lider-indigena-alexander-cunda-en-miranda-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-39.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapio.net] 

###### [10 Mar 2019] 

En las horas de la mañana del pasado 8 de marzo, Alexander Cunda, **comunero indígena y reincorporado de las FARC-EP,** fue asesinado** **por sujetos desconocidos con armas de fuego en la zona veredal La Unión de Miranda, Cauca. Según algunos reportes mediáticos, Cunda era reconocido en su comunidad por su liderazgo, sin embargo Fredy Guevara, gobernador de Miranda, negó esa afirmación y sostiene que Cunda era comunero pero nunca fue nombrado por su comunidad a una posición de líder.

El gobernador indica que Cunda participó en la entrega de armas de las FARC-EP en el 2017 y que su asesinato podría estar relacionado a su condición como reincorporado de la guerrilla. Guevara señala que el comunero pudo haber sido otra víctima de la "purga" que se realiza entre los integrantes del **Ejército Popular de Liberación** y los miembros de** las disidencias de las FARC-EP**. Los ex-combatientes de la guerrilla quienes no se unieron a las disidencias son especialmente vulnerables ante esta violencia.

Hasta la fecha, el gobernador, quien participó en el levantamiento del cuerpo, indica que la Fiscalia no ha contactado su oficina para investigar los hechos del crimen. El autoridad indígena tampoco conoce de alguna investigación que hayan emprendido las autoridades. (Le puede interesar: "[Jesús Díaz, comunero indígena del resguardo Las Delicias es asesinado](https://archivo.contagioradio.com/jesus-diaz-comunero-indigena-del-resguardo-las-delicias-es-asesinado/)")

Por último, Guevara indica que el conflicto armado en Miranda ha intensificado y incluso, ha llegado a los resguardos indígenas, donde las guardias se han visto obligados de sacar, de manera forzosa, a los grupos armados ilegales. "Es una situación muy crítica que estamos viviendo en Miranda y eso no le veo solución porque cada día suceden más cosas", concluyó el gobernador.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
