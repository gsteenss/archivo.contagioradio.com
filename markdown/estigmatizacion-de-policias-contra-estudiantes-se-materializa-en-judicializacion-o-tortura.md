Title: Estigmatización de Policías contra estudiantes se materializa en judicialización o tortura
Date: 2020-09-28 16:49
Author: AdminContagio
Category: Actualidad, DDHH
Tags: #Doctrina, #Enemigointerno, #FuerzaPública, #Guerrillero, #Policía
Slug: estigmatizacion-de-policias-contra-estudiantes-se-materializa-en-judicializacion-o-tortura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/violencia-policial-ESMAD.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Joseph Rodríguez, estudiante de Artes Plásticas de la Universidad Distrital, fue capturado el pasado 9 de septiembre en medio de las protestas en contra de la brutalidad policial. Durante las 36 horas que estuvo retenido, no solo **vivió torturas psicológicas, además fue víctima** de la estigmatización de agentes de la Policía Nacional contra las y los estudiantes de universidades públicas en Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La captura de Joseph
--------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Rodríguez hace parte de la Facultad de Artes A.S.A.B y desde hace más de 7 años es bailarín de danza urbana. El día de su captura se encontraba en la plaza Ferial del 20 Julio, como todos los días, junto a sus compañeros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con su relato, tras escuchar una bomba aturdidora y salir del lugar caminando, fueron encerrados por agentes de la Policía que empezaron a insultarlos **"nos decían cállense o nos amenazaban con el *"taser"*, entonces era mejor hacer silencio"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de haber visto como golpeaban a distintas personas, Rodríguez fue trasladado a la estación de San Blas, en donde los malos tratos no terminaron; "había un chico sentado a mi lado al que empezaron a golpear de forma brutal. Él les decía ¡por favor mi señor agente!, pero terminó casi encima mío".

<!-- /wp:paragraph -->

<!-- wp:heading -->

"Este guerrillero comunista"
----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Posteriormente, Rodríguez afirma que un agente, de forma arbitraría, señaló con el dedo a cinco personas, entre las que se encontraba él, para ser judicializadas. Procedieron con un interrogatorio en donde **incluso le preguntaron por sus redes sociales y cómo aparecía en ellas**. Después, fue llevado a la URI de Molinos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estando allí, logró establecer que se le culpaba de incendiar una moto de la Policía y agredir a una agente de esa institución. Tras los procedimientos rutinarios, fue nuevamente trasladado a la estación de San Blas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esa mañana, y luego de estar más de doce horas sin entrar a un baño o recibir comida, Rodríguez manifiesta que durante el cambio de turno, los policías nuevamente empezaron a insultar a las personas que se encontraban adentro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Me preguntaron usted qué, dónde estudia. Yo dije que estudiaba Artes en la Distrital, a lo que respondieron -ah, **este guerrillero hijueputa**, este comunista, es que esas universidades si están jodidas. Es mejor no estudiar y ser policía".

<!-- /wp:paragraph -->

<!-- wp:heading -->

El rostro del enemigo interno
-----------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con el congresista Inti Asprilla, hay que hacer un cambio en el enfoque y la doctrina de la fuerza policial y fuerzas militares para desmontar el imaginario de enemigo. "Acá hay una doctrina de los años 80, producto de la Guerra Fría, en la que se pensaba que cualquier manifestación de descontento era un camino al Comunismo y que se valía en su contra cualquier cosa" asegura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco de esa doctrina, este año se conoció cómo la Fuerza Pública hizo el perfilamiento de defensores a derechos humanos, periodistas, congresistas e integrantes del movimiento social. Hechos que son reiterativos y recuerda otros escándalos como las chuzadas del D.A.S, Andromeda o el accionar en los años 60 del BINCI[. (Le puede interesar: "Las peores prácticas de inteligencia militar nunca se fueron")](https://archivo.contagioradio.com/las-peores-practicas-de-inteligencia-militar-nunca-se-fueron/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, estudiantes de Universidades Públicas han denunciado en reiteradas ocasiones la estigmatización por parte de las instituciones estatales que se evidencian en montajes judiciales o persecuciones. Algunos de los casos más relevantes son los montajes judiciales contra el profesor Miguel Ángel Beltrán o los casos conocidos como Lebrija y Mateo Gutiérrez. [(Le puede interesar: "Primeras declaraciones de Miguel Ángel Beltran")](https://www.justiciaypazcolombia.com/primeras-declaraciones-de-miguel-angel-beltran/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Joseph afirma que tras vivir estos abusos de poder, ahora teme por su seguridad. Sin embargo, asevera que junto a otros compañeros, iniciarán un proceso en contra de los policías por los malos tratos de los que fueron víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"No es mentira que la gente no confía en la Fuerza Pública, la gente les teme. Yo les tengo mucho miedo, antes pensaba que debía tener cuidado. Ahora te cogen, **te golpean, te llevan y da miedo**" afirma Joseph Rodríguez.*

<!-- /wp:paragraph -->
