Title: Con movilizaciones estudiantes presionarán al Gobierno para negociar
Date: 2018-11-07 19:00
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, estudiantes, marcha, Movilización
Slug: movilizaciones-estudiantes
Status: published

###### [Foto: Contagio Radio] 

###### [7 Nov 2018] 

Después del primer encuentro entre estudiantes y Gobierno en la mesa de concertación que buscaba encontrar soluciones a la crisis que vive la educación superior en el país, **estudiantes y docentes decidieron suspender las conversaciones porque no encontraron voluntad por parte del Gobierno** para aportar soluciones al déficit que tienen actualmente las universidades.

Al primer encuentro oficial de la Mesa, **asistió nuevamente el Viceministro de Educación y no la ministra María Victoria Angulo como estaba previsto**; adicionalmente, según denuncia del Frente Amplio por la Educación Superior (FAES), el representante del Gobierno no mostró voluntad política para negociar el dinero faltante para que las Instituciones de Educación Superior (IES) públicas culminen 2018, y puedan operar en 2019.

Según **Jennifer Pedraza, integrante de la ACREES**  organización que participa en la mesa de negociación, el Gobierno Nacional se presentó al encuentro con la posición de **no estar "dispuestos a entregar un peso más para las IES públicas"**, añadiendo que su única apuesta es Generación E, y el acuerdo firmado entre el presidente Duque y los rectores de algunas Instituciones.

Pedraza sostuvo que adicionalmente, el Gobierno no reconoció a estudiantes y profesores como un interlocutor válido para acordar una agenda que permita salir de la crisis; por lo tanto, no tenía sentido continuar una mesa de concertación en la que una de las partes no está dispuesta a ceder, y desconoce la facultad de su interlocutor para dialogar. (Le puede interesar: ["Así funcionará mesa de concertación entre estudiantes y Gobierno"](https://archivo.contagioradio.com/mesa-concertacion-estudiantes/))

En un comunicado emitido este miércoles, el Ministerio de Educación Nacional anunció la suspensión de las conversaciones, y lamentó que  "el esfuerzo presupuestal" realizado por el presidente Duque "no haya sido tenido en cuenta para un análisis profundo por parte de los estudiantes y docentes miembros de la Mesa"; mientras el FAES señaló que  **analizándola "con rigor evidenciamos que el faltante para terminar el año 2018 es aproximadamente \$500 mil millones"**, lo que daría cuenta de la diferencia de posiciones en la Mesa.

### **El acuerdo con los Rectores: Otra disputa de los estudiantes** 

Aunque el movimiento estudiantil reconoció el avance logrado tras el encuentro del presidente Duque con rectores miembros del Sistema Universitario Estatal (SUE), para la comunidad académica, **los recursos aún son insuficientes para cubrir el déficit inmediato que presentan las IES**, e incluso, dichos dineros no están asegurados. (Le puede interesar: ["Generación E: ¿El reencauche de Ser Pilo Paga"](https://archivo.contagioradio.com/generacion-e/))

Pedraza sostuvo que la relación entre profesores, estudiantes y rectores es difícil, "porque **la condición del Gobierno para cumplir con el Acuerdo es que se levante el paro**"; razón por la que pide a los directivos que integran el SUE se respeten las movilizaciones estudiantiles y tomen una "posición digna en defensa de la educación superior pública". (Le puede interesar: ["¿Por qué aún no se levanta el paro estudiantil?"](https://archivo.contagioradio.com/no-levanta-paro/))

Para retomar la mesa de concertación, el FAES señaló que debería haber una voluntad política de negociar, y pidió la presencia del presidente Duque en la mesa; no obstante, Pedraza señaló que veía poco probable esta presencia, razón por la que invitó a la **movilización que se realizará este jueves junto a la Centra Unitaria de Trabajadores (CUT) y FECODE desde las 2 de la tarde,** para mostrar la fuerza de los sectores sociales.

[Comunicado por la suspensión de la mesa con estudiantes](https://www.scribd.com/document/392629101/Comunicado-por-la-suspensio-n-de-la-mesa-con-estudiantes#from_embed "View Comunicado por la suspensión de la mesa con estudiantes on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_77520" class="scribd_iframe_embed" title="Comunicado por la suspensión de la mesa con estudiantes" src="https://www.scribd.com/embeds/392629101/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-0dJuB8n5pjFWx02vV8xN&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

[![Comunicado del Ministerio de Educación Nacional](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-07-a-las-6.10.33-p.m.-740x567.png){.alignnone .size-medium .wp-image-58086 width="740" height="567"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-07-a-las-6.10.33-p.m..png)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
