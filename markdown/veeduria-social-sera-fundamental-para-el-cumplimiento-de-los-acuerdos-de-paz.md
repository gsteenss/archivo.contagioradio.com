Title: "Veeduría social será fundamental para el cumplimiento de acuerdos de paz"
Date: 2016-01-29 15:12
Category: Nacional, Paz
Tags: Conversacioines de paz en Colombia, DIPAZ, ELN, FARC, Frente Amplio por la PAz
Slug: veeduria-social-sera-fundamental-para-el-cumplimiento-de-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/rudelmar_consejo_mundial-iglesias_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: flickr] 

<iframe src="http://www.ivoox.com/player_ek_10251337_2_1.html?data=kpWfl5aXd5ihhpywj5aXaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncYamk7vSx8nZtoa3lIqupsaPt9DXysbZjdjJtoa3lIquk5DKuc%2FYwtLS0NnFsIzkwtfOjcrQb8TpztXZy9LNqc%2FojoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Rudelmar Bueno] 

###### [29 Ene 2016.] 

A través de su delegado ante las **Naciones Unidas el Consejo Mundial de Iglesias** dio otro espaldarazo al proceso de paz en Colombia. Rudelmar Bueno da Faria, representante de esa institución ante la ONU afirmó que tanto la propia resolución aprobada como la labor decidida de su institución son la garantía de un respaldo irrestricto al proceso de paz.

“Más que un gesto político es un respaldo al proceso” y que la resolución se haya firmado antes de la firma del acuerdo final es la manera de mostrar que **hay garantías de respaldo por parte de la comunidad internacional** pero también requiere una resolución de las partes en conversaciones para ser coherentes y se llegue a una paz duradera.

Desde el Consejo Mundial también estamos a favor de la paz en Colombia y una de las muestras de ello es la presencia en meses pasados del  secretario general. Bueno da Faria afirma que también **se busca la paz con justicia y se quiere que los acuerdos estén marcados con aspectos justos y resaltó la importancia de la creación de la[Comisión de la Verdad](https://archivo.contagioradio.com/?s=comision+de+la+verdad)** como garantía del reconocimiento de la verdad histórica.

En ese mismo sentido el delegado del Consejo Mundial de Iglesias resaltó que a**unque en la resolución de la ONU no está incluida la posibilidad de [vigilar el problema del paramilitarismo](https://archivo.contagioradio.com/?s=paramilitarismo) destacó la importancia de la veeduría social** que se realiza al proceso y a la aplicación de los acuerdos y en ello también reafirmó el compromiso de esa organización.

Aunque la decisión del Consejo de Seguridad de la ONU no implica temas como las garantías de no repetición, ni la financiación de los acuerdos de paz que se alcancen entre el gobierno y la guerrilla de las FARC, **Rudelmar Bueno, afirma que el momento político en Colombia hace posible que el respaldo de la ese organismo sea irrestricto y apegado a las peticiones que se le hagan.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) y escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
