Title: Este fin de semana se reúnen 250 delegados en encuentro nacional de Clamor Social por la Paz
Date: 2014-12-26 16:14
Author: CtgAdm
Category: Otra Mirada
Slug: este-fin-de-semana-se-reunen-250-delegados-en-encuentro-nacional-de-clamor-social-por-la-paz
Status: published

Crédito contagioradio.com  
[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsq1ik7T.mp3)

En el Centro de Memoria Histórica se darán cita delegados de todo el país y de experiencias internacionales de construcción de paz. Uno de los objetivos de este encuentro es **impulsar el inicio de las conversaciones con las guerrillas del ELN y el EPL, y lograr encontrar propuestas de paz desde las regiones del país que participaran con sus delegados y delegadas.**

Rita Gómez, secretaria ejecutiva de Clamor Social por la Paz señala que una de las características es que Clamor Social es un escenario de confluencia y que ello permitirá dejar las diferencias en un segundo plano frente a las necesidades urgentes de unidad que exige la coyuntura de construcción de la paz en Colombia. El evento será un espacio abierto en el que también confluirá el Frente Amplio por la paz.
