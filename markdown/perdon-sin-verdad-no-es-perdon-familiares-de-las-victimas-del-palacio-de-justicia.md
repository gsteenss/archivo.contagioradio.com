Title: "Perdón sin verdad, no es perdón": Familiares de las víctimas del Palacio de Justicia
Date: 2015-11-09 14:28
Category: Sin Olvido
Tags: 1985, 6 y 7 de noviembre, Belisario Betancur, Corte Interamericana de Derechos Humano, Lucy Amparo Oviedo, Luz Mary Portela, M-19, María del Pilar Guarín, Palacio de Justicia, Presidente Juan Manuel Santos, Víctimas del Palacio de justicia
Slug: perdon-sin-verdad-no-es-perdon-familiares-de-las-victimas-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Acto-de-perdón-de-presidente-Juan-Manuel-Santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.eluniverso.com 

<iframe src="http://www.ivoox.com/player_ek_9328816_2_1.html?data=mpifmp2Veo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZOmscrfxoqnd4a2lNOY1c7Sb9fZ08nOxoqWh4zi0JDS1ZDUqdPYhqigh6eXsoamk4qgo5Cqpc7dzc7O1MrXb8XZjNHO1ZCRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rosa Milena Cárdenas] 

Frente a los familiares de los desaparecidos del Palacio de Justicia, el presidente de la República, Juan Manuel Santos, pidió perdón el pasado viernes a nombre del estado colombiano por sus responsabilidades atribuídas por el fallo de la Corte Interamericana de Derechos Humanos en los hechos cometidos durante la toma y retoma del Palacio.

El acto inició con la intervención de Héctor Beltrán, padre de Héctor Jaime Beltrán, en una sentida reflexión donde manifestó su indignación expresando que "Quienes tenían la obligación de protegerlos los desaparecieron y luego se dedicaron a mancillar su memoria", así mismo hablaron **Francisco Lanao, esposo de Gloria Anzola;** Alejandra Rodríguez, hija de Carlos Rodríguez Vera; **Mairé Urán, hija del exmagistrado Carlos Ignacio Urán** y **Orlando Quijano, en representación de las víctimas de torturas en el Palacio**, todos exigiendo verdad y justicia en la memoria de sus familiares desaparecidos, asesinados y torturados.

Armida Oviedo, hermana de Lucy Amparo Oviedo, de quién se encontraron algunos restos óseos el pasado 20 de octubre, asegura que pese a que espera que el acto de perdón de Santos haya sido verdadero  y “de corazón”, lo cierto es que para ella, “perdón sin verdad no es perdón” por lo que mantiene la esperanza de que se empiece a destapar la verdad impulsada por este acto del presidente.

**Pilar Navarrete esposa de Héctor Jaime Beltrán**  y Rosa Milena Cárdenas, hermana de Luz Mary Portela, indican que estos 30 años han sido de “angustia y lucha”, ambas afirman que el pronunciamiento de Santos  se trata de un acto protocolario **impuesto por la Corte Interamericana de Derechos Humanos**.

De acuerdo con Navarrete, se debería pedir perdón por “tanto silencio en tanto tiempo, por tanta mentira, por no contarle al mundo que nosotros los familiares somos los que hemos hecho la lucha de todo lo que se está destapando en el Palacio de Justicia”. A su vez, Rosa Milena, señala que “Al haber un acto de perdón se debe activar todas las reglas del juego de la sentencia de la CIDH, y se validen las pruebas que hemos encontrado en el caso, eso sería un perdón de corazón”.

**Cárdenas, también resalta que es importante que en 30 años Juan Manuel Santos haya tomado la vocería, sin embargo, no comparte que el discurso del presidente confirmara únicamente los actos violentos del** M-19, y en cambio pusiera en duda los actos de tortura y asesinatos cometidos por las fuerzas militares.

Para la mayoría de los familiares las palabras de perdón de Santos y el expresidente Betancur, "no han sido de corazón" debido a que han impedido que se conozca la verdad, por la que seguirán luchando con el fin de “visibilizar nuestros familiares y el caso, para que la sociedad entienda que pelamos porque en los hechos de 1985 hubo desmanes de la fuerza pública en contra de civiles inocentes”, agrega la hermana de Luz Mary Portela.
