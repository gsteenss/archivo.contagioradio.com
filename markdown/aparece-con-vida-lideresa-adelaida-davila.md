Title: Lideresa Adelaida Dávila apareció con vida
Date: 2017-06-16 11:19
Category: Mujer, Nacional
Tags: Adelaida Dávila, mujer, Ruta Pacífica de las Mujeres
Slug: aparece-con-vida-lideresa-adelaida-davila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Adelaida.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [16 Jun. 2017]

Este viernes se conoció de la noticia de la **aparición con vida de la lideresa integrante de la Ruta Pacífica de las Mujeres, Adelaida Dávila,** luego de estar más de 3 días desaparecida.

Según lo manifestó **Sandra Luna vocera de esa organización** ella se comunicó con ellas y activaron los protocolos para que fuese llevada por las autoridades a un centro hospitalario donde en la actualidad se encuentra en observación médica.

“En este momento Adelaida está en observación. En un lugar cercano a donde desapareció que fue en Pitalito. **Esperaremos las revisiones de los médicos. Ella está consciente y ha conversado con nosotras”** relató Luna.

Hasta el momento no han podido establecer qué fue lo que sucedió durante los días de su desaparición **“poco a poco se irán dando los procesos de investigación y en eso vamos a seguir haciendo, seguimiento para que las instituciones hagan las investigaciones** de qué fue lo que pasó y determine lo que le sucedió a Adelaida” recalcó la integrante de la Ruta Pacífica.

Además de verificar cómo se encuentra de salud y de la respectiva investigación que esperan se comience, **desde la Ruta Pacífica también harán todo un acompañamiento psicosocial** “cuando uno recién llega es un momento muy difícil con todo lo que haya pasado. La estamos acompañando, pero le estamos permitiendo también que se tranquilice y estabilice” manifestó Luna.

### **Adelaida ha sido víctimas de amenazas y la UNP solo le otorgó un celular y un chaleco antibalas** 

Adelaida tenía medidas de protección en el momento que fue víctima de esta desaparición, las cuales eran un celular y un chaleco antibalas. En la actualidad, luego de conocer que se encontraba en un hospital las instituciones le están brindando las medidas de protección que consideren una vez analizada su situación. Le puede interesar: ['Águilas Negras' amenazan a organizaciones y lideres sociales del Cauca](https://archivo.contagioradio.com/panfletos-amenazantes-aguilas-negras/)

“Nosotros hemos denunciado que las medidas de protección para las mujeres lideresas es diferente que para los hombres líderes. Ahí tenemos una mirada en términos de que no es igual proteger a un hombre que a una mujer. Por lo que **pedimos medias distintas que van más allá de un celular y un chaleco. La situación de Adelaida lo demuestra”** dice Luna.

Quien añade que con este caso hay un gran aprendizaje y es que **se debe continuar haciendo denuncia a través de los medios** y las redes sociales para activar de manera rápida todos los protocolos para que las instituciones hagan su trabajo.

“Así logramos disminuir el riesgo de asesinato, de que una persona líder hombre o mujer termine asesinada. Damos gracias a las instituciones y a los medios por la solidaridad. Cuando rodeamos entre todos y todas a las defensoras eso hace la diferencia” aseveró Luna. Le puede interesar: [Las mujeres y la defensa de los derechos humanos en Colombia](https://archivo.contagioradio.com/las-mujeres-y-la-defensa-de-los-derechos-humanos-en-colombia/)

<iframe id="audio_19305380" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19305380_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
