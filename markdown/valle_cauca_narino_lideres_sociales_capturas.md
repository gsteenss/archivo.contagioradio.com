Title: Organizaciones sociales señalan como "falso positivo judicial" captura masiva en Valle y Nariño
Date: 2018-04-23 13:24
Category: DDHH, Nacional
Tags: ELN, Falsos Positivos Judiciales, nariño, Tumaco, Valle del Cauca
Slug: valle_cauca_narino_lideres_sociales_capturas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/falsos-positivos-judiciales-e1524507146917.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pacifista ] 

###### [23 Abr 2018] 

Tras la detención por parte de la Dijín de **42 personas en el Valle del Cauca y Nariño, por supuestos nexos con la guerrilla del ELN,** organizaciones defensoras de derechos humanos, han denunciado este acto como un nuevo caso de "falsos positivos judiciales", teniendo en cuenta que entre las capturadas se encuentran líderes/as sociales y comunitarios, y defensores/as y defensoras de derechos humanos.

"Es un proceso de criminalización y es a lo que estamos acostumbrados a ver por parte de la Fiscalía que muestra su posición contra los diálogos con el ELN", dice Sharo Mina, integrante del Proceso de Comunidades Negras (PCN), quien agrega que lo que buscan las autoridades es **"desmovilizar, crear y caos confusión frente al proceso de paz, para confundir políticamente a la sociedad civil, teniendo en cuenta que estamos en periodo previo a elecciones".**

### **El perfil de os detenidos por la Fiscalía** 

Entre los detenidos del pasado viernes, por supuestamente hacer parte del ELN, se encuentran cuatro ex alcaldes, un ex personero, dos exconcejales y dos lideresas de Tumaco. Por ejemplo, en el caso de Sara Quiñonez y Tulia Marys Valencia, madre e hija, ambas hacen parte del Consejo Comunitario Alto Mira y Frontera, en Tumaco. Sara no solo ha participado en los procesos en defensa del territorio, sino que además ha acompañado a mujeres, y educa a niñas y niños de su municipio.

"Son personas que tienen una carga emocional enorme por ser amenazadas y desplazadas y ahora son revictimizadas", manifiesta la integrante del PCN. De hecho, la líder afro explica que muchas de las personas capturadas son personas protegidas por medidas cautelares de la Corte Interamericana de Derechos Humanos, y cuentan con esquemas de protección otorgadas por  la Unidad Nacional de Protección y la Unidad de Víctimas.

### **Continúan las capturas** 

Después de la primera captura masiva, durante el fin de semana hubo nuevas capturas. De acuerdo con Mina, existe una lista con nombres de aproximadamente 80 personas que son familiares y amigos de los ya detenidos. Según cuenta Sharo Mina, este domingo, una señora cercana a los 80 años fue a buscar a su hija capturada por la Dijín, y ella también resultó detenida pues aparecía en dicha lista.

"Están capturando exalcaldes, líderes sociales, defensores y defensoras de derechos humanos, amas de casa y otras personas que vienen de sectores muy pobres. Es una cacería de brujas y un patrón que se repite", expresa la líder afro.

### **En libertad dos personas capturadas** 

Las personas capturadas han sido señaladas por cometer delitos como **rebelión, financiar el terrorismo, concierto para delinquir, enriquecimiento ilícito y homicidio,** entre otros.

No obstante, en el marco de la audiencia que se llevó a cabo este domingo, en el **Palacio de Justicia de Cali,** fueron dejados **en libertad dos de los detenidos, debido a que no se encontraron pruebas contra ellos, como es el caso de** Andrés Felipe Zambrano, indígena dejado en libertad.

<iframe id="audio_25575530" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25575530_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
