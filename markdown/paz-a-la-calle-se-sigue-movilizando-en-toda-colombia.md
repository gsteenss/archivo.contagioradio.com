Title: Paz a la Calle se sigue movilizando en toda Colombia
Date: 2016-10-10 13:10
Category: Nacional, Paz
Tags: comunidades victimas, Movilización, Paz a la Calle
Slug: paz-a-la-calle-se-sigue-movilizando-en-toda-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/14567474_1775961736006222_2004116949525314119_o-e1476122926313.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paz A La Calle] 

###### [10 Oct 2016] 

Durante la segunda Asamblea Ciudadana de Paz A La Calle realizada el pasado jueves en el Park Way a la que **asistieron más de 500 personas, se definieron las líneas de trabajo de las comisiones y la agenda de las próximas actividades** que se realizarán en la capital y otras ciudades del país y el mundo.

Las comisiones conformadas son:

-   Comisión de Organización, la cual se encarga de dar continuidad a las asambleas y plantear la metodología del desarrollo de las mismas.
-   Comisiones de Comunicaciones, Arte y Pedagogía, las cuales se encargan de planear y desarrollar las apuestas callejeras.
-   Comisión de Género, la cual da enfoque de género a las actividades que se proponen y promueve la participación de mujeres y sectores LGBTI.
-   Comisión Jurídica, la cual aporta desde acciones legales que acompañen las iniciativas y por último
-   Comisión de Paz Territorial, que tienen la tarea de articular las iniciativas surgidas en Bogotá con otras ciudades de Colombia y el mundo.

Lorena Aristizábal, una de las integrantes de esta iniciativa manifestó que si bien Paz A La Calle no es un nuevo movimiento social, si **es un espacio de convergencia para la movilización social**, en el que a todos y todas “[nos une un respaldo al espíritu de los acuerdos de paz](https://archivo.contagioradio.com/victimas-reciben-con-alegria-el-premio-nobel-de-paz-para-colombia/) para resolver lo mas pronto posible en el marco de una garantía para el cese al fuego bilateral el conflicto armado”.

Desde la Comisión de Paz Territorial se ha logrado la articulación con otras ciudades del país como Manizales, Barranquilla, Medellín, Cali, entre otras y se han sumado fuerzas con ciudades como Barcelona, Nueva York, Madrid y Ciudad de México. **“El objetivo es hacer que paz a la calle se amplíe a otras localidades y ciudades”** afirmó Aristizábal.

Además proponen algunas actividades para esta semana. Este lunes a las 7pm se llevará a cabo la tercera Asamblea Ciudadana en el monumento Almirante Padilla del Park Way para dar continuidad a las tareas de las comisiones, por otra parte, en la asamblea pasada decidieron **acompañar las movilizaciones del 12 y 14 de Octubre para apoyar las exigencias de [comunidades víctimas, indígenas, afro y campesinas](https://archivo.contagioradio.com/guardia-indigena-y-cimarrona-serian-garantes-de-acuerdo-de-paz-en-sus-territorios/)** en torno a la implementación de los acuerdos.

**12 de Octubre, Marcha de las flores**

** **Llegarán 3 mil víctimas del conflicto y 7 mil indígenas del país que vienen a Bogotá a exigir un acuerdo inmediato, en alianza con la ONIC, algunas organizaciones de mujeres y el movimiento estudiantil se hará un corredor humano desde el Planetario hasta la Plaza de Bolívar en el que con flores se dará recibimiento a estas comunidades. Los puntos de encuentro son la Universidad Nacional y el Centro de Memoria, Paz y Reconciliación.

**14 de Octubre, “Por la Paz del Campo Vengo Yo”**

Es el nombre que dieron a la movilización del próximo Viernes que será simultánea diferentes ciudades del país. Las exigencias, exigir el mantenimiento del cese bilateral como garantía de paz, la no creación de un Frente Nacional donde se excluyan a otros sectores políticos minoritarios, la construcción de gran pacto político nacional, defender lo acordado en La Habana y respaldar los diálogos con el ELN.

Para obtener más información de las actividades de PazALaCalle conéctese en el Twitter: [@PazALaCalleCol](https://twitter.com/PazALaCalleCol) y al Correo electrónico: <pazalacallecolombia@gmail.com>

<iframe id="audio_13259661" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13259661_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
