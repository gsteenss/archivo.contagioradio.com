Title: Otra Mirada: Construir paz en los territorios
Date: 2020-07-22 22:36
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Comisión de Justicia y Paz, Territorios
Slug: otra-mirada-construir-paz-en-los-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Jornada-por-la-vida-de-líderes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comisión intereclesial de Justicia y Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 20 de julio a través de la Comisión de Justicia y Paz, se presentó la octava carta, **\#SomosGénesis**, escrita por más de 110 comunidades de diversas partes del país en la cual invitan a grupos armados, a la JEP y a l CEV y la UBPD, así como al gobierno para que haya un cese al fuego, se respete el derecho de las comunidades de participar en decisiones con el fin de lograr la paz que tanto se ha esperado. (Le puede interesar: [Comunidades claman por la verdad y por acuerdos humanitarios que alivien el dolor de la guerra](https://archivo.contagioradio.com/comunidades-claman-por-la-verdad-y-por-acuerdos-humanitarios-que-alivien-el-dolor-de-la-guerra/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Haciendo parte del panel y compartiendo cuál es la situación que los llevó a realizar este compilado de cartas, estuvieron representantes de diferentes comunidades y áreas del país como Alicia Mosquera de la Asociación de Mujeres Desplazadas de Riosucio, Clamores, Orlando Castillo de Espacio Humanitario Puente Nayera, Buenaventura, Valle del Cauca y Ricardo Barragán de la Red por la defensa del agua, la vida y el territorio, Inzá, Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cada representante explica lo que se vive actualmente en las comunidades que han sido olvidadas e ignoradas por el gobierno y a quiénes están dirigidas las ocho cartas que vienen con una serie de peticiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También comentan que desde inicios de año que se empezaron a entregar las cartas, no han recibido ningún tipo de respuesta o solución, sin embargo se mantienen motivados y con las esperanzas en alto para que estos escritos generen algún tipo de cambio en cada uno de los territorios afectados. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, los invitados explicaron cuál consideran que es la razón por la los territorios vulnerados son foco y punto estratégico entre grupos armados e igualmente comentan que se ha observado que hay una relación entre las multinacionales y los grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Si desea escuchar el programa del 21 de julio: [Otra Mirada: ¿Vientos de cambio en el congreso o más de lo mismo?](https://www.facebook.com/contagioradio/videos/291521582185652)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Si desea escuchar el análisis completo del programa:

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/1400891780099615","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/1400891780099615

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
