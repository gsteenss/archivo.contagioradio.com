Title: ¡Vamos por la consulta popular! Vereda San Jorge de Suacha
Date: 2016-04-01 11:35
Category: Nacional, yoreporto
Tags: Mineria, soacha
Slug: vamos-por-la-consulta-popular-vereda-san-jorgue-de-suacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/vereda-san-jorge-mineria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Revelados 

#### Por [Caminando el Territorio y Red Juvenil de Suacha - Yo Reporto ] 

Los campesinos de la vereda San Jorge con el acompañamiento de la Red Juvenil de Suacha y de Caminando el Territorio, continúan con el proceso resistencia a la actividad minera que persiste en instaurarse en este territorio de páramo.

En la audiencia pública celebrada en septiembre de 2015 en el Congreso de la República, la organización Caminando el Territorio presentó los resultados parciales de la investigación que viene adelantando sobre la gestión y el daño ambiental de la actividad minera en el municipio de Suacha. Uno de los compromisos de tal audiencia, fue realizar un recorrido para evidenciar el impacto de la minería de materiales de construcción en el municipio.

El pasado 6 de marzo junto al representante a la cámara por Bogotá Alirio Uribe, jóvenes, comunidad en general y campesinos de San Jorge; se efectuó el recorrido por el corregimiento Uno de Suacha, en el que se reconocieron las nefastas realidades que la actividad minera le hereda al municipio bajo la mirada sospechosamente cómplice e indolente de las autoridades ambientales competentes, no sólo en términos ecológicos sino también sociales. El recorrido finalizó en la vereda San Jorge en donde la comunidad expreso los argumentos técnicos y jurídicos que ignoran las autoridades e inviabilizan la actividad minera en este territorio campesino y paramuno.

La caminata por la vereda San Jorge coincidió con la presencia del alcalde Eleazar Gonzáles en la zona, la comunidad aprovechó para abordarlo y expresarle su sentir de rechazo contundente a la actividad minera en la vereda. El alcalde desde la retórica barata propia de aquellos políticos en los que es mejor no confiar, se “comprometió” con los campesinos a proteger esta vereda como territorio agrícola y no minero.

[![vereda san jorge](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/vereda-san-jorge.png){.wp-image-22156 .alignleft width="431" height="240"}](https://archivo.contagioradio.com/vamos-por-la-consulta-popular-vereda-san-jorgue-de-suacha/vereda-san-jorge/)

El recorrido dejó dos compromisos: el primero, realizar un recorrido por la zona de influencia de la mina caracolí con la Defensoría del Pueblo y la Corporación Autónoma Regional de Cundinamarca (CAR), para evaluar técnicamente el potencial ecológico de la zona y el segundo, efectuar una audiencia pública en el municipio de Suacha, con autoridades nacionales, regionales y locales para determinar el futuro de la actividad minería en el corregimiento Uno.

El recorrido se llevó a cabo el pasado 17 de marzo, como conclusión la CAR técnicamente comprobó, como siempre lo ha esgrimido la comunidad, que el área de influencia de la mina Caracolí se encuentra en zona de páramo y tiene un alto potencial y valor en términos hídricos y de diversidad biológica. Además encontró hallazgos que permiten establecer que no se está cumpliendo con el plan de manejo ambiental y que se están efectuando actividades no contempladas en éste, que revisten un impacto grave al ecosistema.

La audiencia pública se realizará en el mes de mayo. La principal propuesta que llevarán la Red Juvenil de Suacha, Caminando el Territorio y los ciudadanos que padecen los estragos de la minería, a las autoridades, pero principalmente a la alcaldía, será proponerle al alcalde municipal que convoque una Consulta Popular en las veredas San Jorge, Hungría, Alto del Cabra y Romeral del corregimiento Uno y en las comunas 4, 5 y 6 de la zona urbana. Para que sean los habitantes y las comunidades quienes se ven directamente afectadas y no los señores de corbata desde un escritorio, los que decidan sobre la presencia de la actividad minera en sus territorios. Veremos qué tan seria es la retórica del señor alcalde, pues si se comprometió con los campesinos a defender la vocación agrícola de la vereda San Jorge, seguramente no tendrá reparos en apoyar la Consulta Popular.

Por otro lado hay que informar que el pasado 29 de marzo, la comunidad campesina de la vereda San Jorge (Suacha), ante la lentitud e ineptitud de las autoridades competentes de impedir la explotación de materiales de construcción en esta zona de protección ambiental, en pleno ecosistema de páramo y amparada en una licencia ambiental con múltiples vicios e inconsistencias; no tuvieron otra alternativa que materializar su derecho a la protesta a la luz de la carta constitucional, y ante el inicio de operaciones de la mina caracolí, decidieron trancar el paso de las volquetas que transportan y comercializan el material de arena. El contundente bloqueo obligó a las volquetas a devolverse, la comunidad manifiesta que con firmeza pero de forma pacífica, mientras sea necesario, continuará con este ejercicio legal y legítimo.

La Red Juvenil de Suacha, Caminando el Territorio y los campesinos del corregimiento Uno le decimos NO a la minería en este territorio ecológicamente estratégico y seguiremos nuestra lucha jurídica y de movilización hasta desterrarla del corregimiento y de la zona urbana de Suacha.

Vamos por la consulta popular, no a la minería en la zona urbana ni en el corregimiento uno de Suacha!
