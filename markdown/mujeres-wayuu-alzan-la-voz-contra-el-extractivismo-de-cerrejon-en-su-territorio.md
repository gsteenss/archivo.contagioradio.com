Title: Cerrejón ha incumplido 14 fallos judiciales que favorecen a los wayuú
Date: 2020-10-07 12:27
Author: AdminContagio
Category: Actualidad, Ambiente
Tags: DDHH, El Cerrejón en La Guajira, extractivismo, La Guajira
Slug: mujeres-wayuu-alzan-la-voz-contra-el-extractivismo-de-cerrejon-en-su-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Mujeres-indigenas-Cerrejon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 6 de octubre fue presentado el informe, ***"Minería de carbón y des-arroyo: Etnocidio y ecocidio en la Guajira",*** una mirada a las afectaciones que viven las comunidades Wayúu, campesinas y afros, que habitan en inmediaciones de la mina de Carbón del Cerrejón.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CINEP_PPP/status/1313510474751848451","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CINEP\_PPP/status/1313510474751848451

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

El trabajo es realizado por el **equipo de interculturalidad del Centro de Investigación y Educación Popular (CINEP)**, en la presentación de la revista Noche y Niebla, en la edición 61, en donde dan cuenta de "***la incumplida promesa del desarrollo tras más de 40 años de explotación minera La Guajira"***, una acción extractiva que durante dos décadas ha visibilizado que esta acción de progreso no ha sido más que una intensificación de la pobreza que viven las comunidades de La Guajira.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos incumplimientos contrastan con **14 fallos judiciales que respaldan las vulneraciones de Cerrejón y el Estado colombiano, a derechos como la vida, el agua y a la salud** de las comunidades indígenas afro y guajiras víctimas de la extracción de carbón. ([Pandemia profundiza riesgo de desnutrición en indígenas wayús](https://archivo.contagioradio.com/pandemia-profundiza-riesgo-de-desnutricion-en-indigenas-wayus/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este *"etnocidio y ecocidio en La Guajira"* ,se respalda con los 32 millones de toneladas de carbón térmico qué es extraído y exportado en su totalidad de esta mina de carbón y que **deja en vulnerabilidad a más de 25 comunidades Wayúu, afro y campesina**, que no solamente se han visto afectadas con los residuos químicos, sino también, han sido desplazadas, despojadas y confinadas, por esta actividad extractiva que se realiza desde 1985.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Uno de los impactos más claros según el Cinep, que ha generado Cerrejón en La Guajira con la explotación de 14.493 hectáreas, además del despojo y privatización de 69.393 hectáreas, son los **150 kilómetros de línea férrea que atraviesan la Guajira transportando carbón, los cuales han exterminado 17 arroyos, 30 más han sido deteriorados,** causando daños irreparables en el Río Rancherías; sumiendo así a la Guajira en una crisis hídrica, en donde el acceso al agua potable es un privilegio que pocos tienen.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"40% de los recursos de agua en la Guajira, se han perdido por parte de la actividad minera aumentando el estrés hídrico en el departamento y la crisis de acceso al agua".*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Ese impacto ambiental, enfocado directamente en las fuentes hídricas es respuesta de la operación minera que utiliza 24 millones de litros de agua al día, según el [Cinep](https://www.cinep.org.co/Home2/component/k2/836-mineria-de-carbon-y-des-arroyo.html), *"agua suficiente para alimentar a 150,00 personas".* (

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al mismo tiempo, indican que previo a la actividad extractiva las comunidades hallaban a tan sólo 5metros de profundidad abastecimiento de agua, ***"hoy estás tienen que escalar entre 20 y 30 metros bajo el suelo para encontrar agua dulce"***, agua que al mismo tiempo contiene altos niveles de contaminación generando considerables afectaciones en la salud de quienes la consumen.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el centro de investigación las afectaciones más comunes en la salud producto de la minería son, *"las alteraciones en el ADN, la inestabilidad de cromosomas, cambios celulares, **dejando en un inminente riesgo de contraer cáncer, padecer infartos, enfermedades cardíacas, entre otras alteraciones a la salud."***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Está grave crisis humanitaria que lleva atravesando la comunidad de La Guajira hace más de 10 años, h**a afectado especialmente a la niñez Wayúu y con ellos a un legado de un pueblo indígena que puede llegar al exterminio producto del extractivismo.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Respaldo a la lucha de las mujeres Wayúu en contra el extractivismo minero
--------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 25 de septiembre el Relator Especial de Ambiente y Derechos Humanos de la ONU, evidenció la necesidad de suspender las actividades mineras que rodean a las comunidades de la Guajira, especialmente a quienes habitan el resguardo Provincial, reconociendo el impacto que ha tenido este en la salud de los niños y niñas de este territorio.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Los agentes ambientales deben abstenerse de ejecutar una actividad, siempre que exista una duda razonable de que el acto pueda causar daños al ecosistema y a la salud humana"***

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Al mismo tiempo, diferentes organizaciones sociales, colectivos ambientales, líderes e integrantes de la academia, se han sumado en una carta respaldando y reconociendo el trabajo histórico que han encabezado las mujeres indígenas, campesinas y afros, en contra del extractivismo en la Guajira.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Habitantes de la región como **María de los Ángeles García, abogada, presidenta de la veeduría ciudadana Akuaippa y** vicepresidenta del Consejo Comunitario (CONRCIMACRU) que sin importar el poder corporativo que tiene la empresa Carbones del Cerrejón en la Guajira, **sus actividades de explotación minera no pueden continuar vulnerando los derechos fundamentales de comunidades enteras**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"No es suficiente con que señalen que son una empresa comprometida con desarrollar una operación respetuosa del medio ambiente y los derechos de los trabajadores y comunidades vecinas, e**s necesario que sus actividades sean efectivamente irrespetuosas del marco normativo colombiano e internacional"***

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

El infomre agrega que los diferentes fallos judiciales y sentencias de la Corte dan cuenta de que **no es la primera vez que se resuelve un caso en contra de Carbones del Cerrejon Limited por poner en riesgo y generar afectaciones al ambiente y a la salud** de las poblaciones cercanas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Denuncias que han estado respaldadas y sustentadas con evidencia jurídica y científica, presentada por las mujeres que lideran estos procesos en la Guajira, y que además **evidencia un contexto histórico y reiterado de vulneraciones a los Derechos Humanos de estas comunidades y que se han agravado aún más** como consecuencia de la contaminación ambiental de las actividades extractivas por parte de esta empresa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

María de los Ángeles, señala además que la intervención de las empresas extractivas mencionadas, ha creado una ruptura en la cultura de las comunidades Wayúu y pone en riesgo su pervivencia y la de sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al mismo tiempo rechazan y lamenta las respuestas que en los últimos días ha ofrecido el Gobierno de Colombia en cabeza del Ministerio de Relaciones Exteriores, que ***"no solamente desconoce la evidencia documentada y recopilada por las comunidades y grupos de expertos, además entrega mensajes que lejos de contribuir a la defensa de las comunidades indígenas los territorios y el ambiente empeoran el contexto de estigmatización**, persecución y violencia que sufren los líderes y lideresas sociales que trabajan por la defensa del ambiente".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, hacen un llamado a las personas de la comunidad internacional especialmente a la australiana, Suiza e inglesa, a dónde pertenecen las multinacionales accionistas de Carbones del Cerrejón, *"invitamos a activar campañas de presión a sus gobiernos, para que estas empresas cesen en las violaciones a los Derechos Humanos, ambientales y territoriales contra el pueblo Wayúu, guajiro y afro".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
