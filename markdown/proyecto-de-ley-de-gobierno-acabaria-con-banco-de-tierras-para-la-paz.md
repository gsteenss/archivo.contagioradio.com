Title: Ley de Tierras de gobierno acabaría con Banco de tierras para la paz
Date: 2018-05-08 17:18
Category: DDHH, Política
Slug: proyecto-de-ley-de-gobierno-acabaria-con-banco-de-tierras-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/despojo-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [08 May 2018] 

Más de 30 organizaciones internacionales expresaron su preocupación por la nueva propuesta de Ley de tierras, que está llevando a cabo el gobierno y que ya estaría en etapa de consulta con las comunidades indígenas y afro, para ser presentada próximamente al Congreso; porque podría profundizar la concentración de tierras en el país, **favorecería la destinación de tierras a actividades minero-petroleras, sanearía apropiaciones indebidas de baldíos y atenta contra la implementación del Acuerdo de Paz**.

### **Los Acuerdos de paz se están incumpliendo** 

El principal llamado de atención que hacen estas organizaciones al proyecto presentado por el gobierno, radica en que va en contravia de la agenda de la implementación de los Acuerdos de Paz. Por ejemplo, el proyecto de ley permitiría que, a través de una nueva figura, se legitimaría la apropiación de baldíos, por parte de sujetos privados, diferentes a los planteados en la reforma agraria.

De acuerdo con Andrés Fuerte, integrante de OXFAM Colombia, "el hecho de entregar tierras que son de la Nación y debería estar destinadas a la reforma agraria**, a sectores poderosos del campo, sin ninguna limitación o perfil definido, nos preocupa mucho** ". El último informe de esta misma organización evidencia que Colombia ocupa el primer lugar en materia de concentración de tierras en la región.

De igual forma, el proyecto de Ley estaría planteando preservar tierras para el desarrollo de proyectos minero energético y no para la producción agraria. (Le puede interesar: ["Proyecto de Ley de Gobierno acabaría con el banco de tierras para la paz"](https://archivo.contagioradio.com/proyecto-de-ley-de-gobierno-profundizaria-modelo-de-acaparamiento-de-tierras/))

### **El Banco de tierras por la paz, cada vez más sin tierras** 

Uno de los puntos más importantes del Acuerdo de Paz, tenía que ver con la conformación del Banco de Tierras que ayudaría a la democratización de las tierras en el país. Sin embargo, Fuerte afirmó que el proyecto de Ley del gobierno entorpecería procesos de extinción de dominio, necesario para la consolidación de este banco.

"Los baldíos en vez de ser recuperados para ser entregados en la reforma agraria, serían entregados en procesos agroindustriales, **en actores que los han acaparado de forma indebida o en proyectos minero energético,** de tal forma que sería imposible consolidar el fondo de tierras" afirmó Fuerte.

Fuerte expresó que este proyecto incluso podría ser más perjudicial que las ZIDRES, debido a que "entrega baldíos, casi que a cualquiera, en cualquier región del país",  extendiendo ese modelo de concesión de tierras incluso a los predios que hacen parte de los Acuerdos de Paz. (Le puede interesar: ["¿Por qué la Ley ZIDRES es Anticampesinos?"](https://archivo.contagioradio.com/ley-zidres-es-anticampesinos-willson-arias/)

### **Los mínimos para una ley de tierras justa y equitativa** 

En el comunicado de prensa, las 30 organizaciones internacionales plantearon que para que exista una verdadera reforma agrícola con una Ley de Tierras que propenda por la democratización del territorio en el país se tiene que tener en cuenta por lo menos 5 características.

La primera de ellas es que sea coherente con el Acuerdo de Paz y con los principios generales de la Reforma Rural Integral; que garantice los plenos derechos de las víctimas del conflicto armado y del desplazamiento forzado; que respete los derechos territoriales de las comunidades indígenas y afrocolombianas y su derecho a la consulta previa, libre e informada; que involucre de manera participativa a las organizaciones campesinas en los territorios; y  que atienda las desigualdades de género que persisten en el mundo rural y que afectan a las mujeres en cuanto a acceso a tierras y otros activos productivos.

<iframe id="audio_25862597" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25862597_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
