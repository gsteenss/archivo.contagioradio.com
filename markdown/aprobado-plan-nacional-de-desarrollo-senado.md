Title: Así fue aprobado el Plan Nacional de Desarrollo en Senado
Date: 2019-05-06 08:03
Author: CtgAdm
Category: Nacional, Política
Tags: Congreso, Duque, economia, oposición, Plan Nacional de Desarrollo
Slug: aprobado-plan-nacional-de-desarrollo-senado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Plan-Nacional-de-Desarrollo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @SenadoGovCo] 

Mientras la opinión pública enfocaba su mirada sobre lo que ocurría en el Senado en relación con las objeciones presidenciales a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), el Gobierno envió el proyecto del Plan Nacional de Desarrollo (PND) a votación en esta corporación; el resultado fue la aprobación 'a pupitrazo' del Proyecto: un documento con más de 300 artículos, algunos de ellos muy cuestionados por ser contrarios a los intereses de trabajadores, profesores, pensionados y víctimas.

### **No hubo tiempo para estudiar el Proyecto de Plan Nacional de Desarrollo**

Como lo explicó la senadora de la lista de Decentes, Aída Avella, la discusión se hizo de espaldas al país y con irregularidades en el proceso. La primera de ellas fue la convocatoria a las sesiones, que dada la premura de los tiempos para la aprobación del Plan, impidió una conciliación entre Cámara de Representantes del Proyecto; la segunda fue que los Congresistas tuvieron que votar sobre un texto que no conocían, puesto que el mismo -en el caso de Avella- llegó el día después de ser votado.

Sobre la primer situación, la Senadora por la lista de Decentes explicó que el error consistió en citar la sesión de Cámara de Representantes para aprobar el PND el martes y no el lunes; de esta forma, el Senado habría tenido tiempo para estudiar más el Proyecto y alcanzar a hacer una sesión de conciliación. Este tipo de sesiones es importante porque unifican los textos que aprueban Cámara y Senado, conciliando las posiciones que emiten ambas corporaciones.

Avella además reclamó porque el Proyecto que pasó al Senado, tras la negación de las objeciones presidenciales a la JEP, fue votado sin que los congresistas conocieran el documento del mismo; hecho que sumado a la premura con que se votó en Cámara dio indicios de que se estuviera enviando un Proyecto para ser votado a pupitrazo. En el caso de la Senadora, ella solo recibió el texto que salió de la Cámara de Representantes hasta el viernes en horas de la mañana, razón por la que probablemente ningún senador votó con pleno conocimiento de lo que aprobaba o negaba, y aún no había claridades sobre lo aprobado.

Sin embargo, entre los artículos más destacados por sus implicaciones se incluyen la creación de facultades extraordinarias para que el Presidente modifique el esquema de la administración pública y la viabilidad para practicar Fracking; a ello se suma la transición del Catastro de un servicio del Estado a una función, lo que para Avella anuncia una privatización del Catastro, y la correspondiente cooptación por parte de grandes intereses económicos para legalizar el despojo de tierras.

### **¿Qué se puede hacer para cambiar el PND?**

La Senadora de la oposición afirmó que hay abogados con muchas inquietudes sobre el proceso, razón por la que están evaluando opciones legales para desestimar lo ocurrido con el Plan. De igual forma, movimientos sociales están preparando acciones colectivas para enfrentar un PND que afecta algunos de los principales intereses de los ciudadanos en términos de recursos durante los próximos tres años. (Le puede interesar: ["El PND, un respaldo al sector minero-energético"](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/))

> El Plan Nacional de Desarrollo está lejos de ser la ruta para la construcción de paz.
>
> Aquí encuentran un balance de lo que logramos y lo que perdimos.[\#NoEnredenLaPaz](https://twitter.com/hashtag/NoEnredenLaPaz?src=hash&ref_src=twsrc%5Etfw) [\#PND](https://twitter.com/hashtag/PND?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/7DGOwjtdnG](https://t.co/7DGOwjtdnG)
>
> — Juanita Goebertus (@JuanitaGoe) [3 de mayo de 2019](https://twitter.com/JuanitaGoe/status/1124140405933604865?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
