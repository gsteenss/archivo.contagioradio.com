Title: En Antioquia se han presentando 111agresiones contra defensores de DD.HH
Date: 2017-08-10 15:57
Category: DDHH, Nacional
Tags: Asesinatos a defensores de DD.HH, Fuerza Pública, paramilitares
Slug: en-antioquia-se-han-presentando-111agresiones-contra-defensores-de-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/marcha-de-las-flores101316_264.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [10 Ago 2017] 

La Corporación Jurídica Libertad, el nodo Antioquia del a Coordinación Colombia, Europa y Estados Unidos, el Proceso Social de Garantías de Antioquia y la Escuela Nacional Sindical, presentaron el informe “Para que la paz no nos cueste la vida, hagamos posible la paz”, **sobre las agresiones hacia los defensores de derechos humanos en el departamento de Antioquia, durante el primer semestre de este año**.

### **La defensa de los DD.HH en Antioquia** 

De acuerdo con el informe, Antioquia es el segundo departamento en donde más han asesinados a líderes sociales y defensores de los derechos humanos con un reporte total de **10 asesinatos en zonas en donde hay control de estructuras paramilitares,** a su vez se han presentado 111 agresiones de las cuales 42 fueron amenazas, 21 fueron ataques físicos y 7 fueron hostigamientos colectivos. (Le puede interesar: ["Somos defensores documentó 51 asesinatos de líderes en el 2017"](https://archivo.contagioradio.com/somos-defensores-documento-51-asesinatos-de-defensores-de-derechos-humanos-en-2017/))

**El principal agresor, según el informe, es el Escuadrón Móvil Antidisturbios (ESMAD), con 29 casos**, las autodenominadas Autodefensas Gaitanistas de Colombia, serían las responsables de 25 casos, mientras que 27 no se ha podido especificar la autoría. Las zonas en donde más ataques se han presentado son El Valle de Aburra con 46 casos, Urabá con 37 casos y el Oriente Antioqueño con 14 casos.

En términos de la impunidad, la Coordinadora Colombia, Europa y Estados Unidos, manifestó que se presentó un derecho de petición ante la Fiscalía, en donde preguntaba por 150 casos de asesinatos de líderes sociales ocurridos entre el año 2010 al 2017, de los cuales 21 casos solo han sido presentados, 45 están en proceso de indagación, en 43 de los casos se desconoce el estado de la investigación, 14 de ellos fueron archivados, 4 están en etapa de juicio, 4 casos tienen condenas absolutorias, un caso se trasladó a justicia penal militar y solo hay 17 condenas, ninguna contra autores materiales, **dejando en un 86% la impunidad.**

### **El paramilitarismo en Antioquia** 

Otra de las preocupaciones que expone el informe, es **la connivencia entre el paramilitarismo y la Fuerza Pública** en diferentes municipios del departamento, que han venido denunciando las comunidades, sin que se tomen medidas de protección. La organización CAHUCOPANA, señaló que entre las denuncias se encuentra la citación por parte las Autodefensas Gaitanistas de Colombia, a las personas para “explicarles cómo va a funcionar” el territorio bajo su control.

Además, se ha denunciado amenazas a través de panfletos y **asentamientos fijos de grupos paramilitares en municipios como Remedios, Segovia, El Bagre y Saragosa** en donde hay campamentos que servirían como centros de control para desplegar acciones hacia otros municipios. (Le puede interesar: ["Paramilitares imponen controles a transporte público en Chocó"](https://archivo.contagioradio.com/44498/))

### **El movimiento Sindical en Antioquia** 

De acuerdo con el informe, durante el primer semestre del 2017 se presentaron **115 agresiones en contra de sindicalistas**, a nivel nacional, y pese a que el 68% de las agresiones no fueron letales, 10 sindicalistas fueron asesinados.

De igual forma 8 mujeres, que desempeñaban liderazgos o eran dirigentes sindicales, fueron víctimas de amenazas, 7 de ellas hacían parte de organizaciones rurales. Sobre la situación de los docentes en el departamento, el informe manifestó que **hay 50 maestros que han sido amenazados, 30 de ellos se encuentran en Medellín**. (Le puede interesar: ["Luego de 5 amenazas agreden a Mayerli Hurtado, defensora de DD.HH"](https://archivo.contagioradio.com/luego-de-5-amenazas-agreden-a-mayerli-hurtado-defensora-de-dd-hh/))

### **Crimen Organizado y la implicación Estatal** 

El informe evidenció que en Antioquía hay tejidas relaciones entre las organizaciones criminales y diferentes instituciones estatales, que permiten que exista el control de estos grupos en zonas determinadas del departamento. Fernando Quijano, vocero de COLPAC afirmó que hay **35 grupos de las CONVIVIR controlando el centro de Medellín** “muchos de ellos podrían estar otra vez camuflarse en esto que se ha denominado la red de apoyo de la Policía Nacional”.

Sobre la guerra urbana entre las organizaciones que se disputan Medellín, el informe señaló que hay tres zonas en un alto riesgo: Loma San Cristóbal, Altavista y Belén Zafra, todas ellas **disputadas por disidencias del paramilitarismo, que usan mecanismos como la extorsión para infundir miedo y control**. (Le puede interesar:["En 2017 han sido asesinados 15 líderes sociales en el Cauca"](https://archivo.contagioradio.com/en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca/))

### **El movimiento LGTBI** 

Según el informe “Para que la paz no nos cueste la vida, hagamos posible la paz”, la situación de los defensores de derechos de la comunidad LGTBI continúa siendo preocupante, debido a que desde el 2015 las agresiones y amenazas vienen en aumento, el último reporte que se realizó fue del **2016 que tuvo 106 casos contra la comunidad, con una impunidad del 100%**.

En ese sentido los defensores han expresado que hay una falta de acción por parte de las autoridades a la hora de activar mecanismos de defensa para esta población, que provoca que los asesinatos y las amenazas sean desvirtuadas y no llegues a ser investigadas por parte de la Fiscalía. (Le puede interesar: ["En Colombia persiste la violencia hacia la comunidad LGTBI"](https://archivo.contagioradio.com/en-colombia-persiste-la-violencia-hacia-la-comunidad-lgtbi/))

### **Las recomendaciones para proteger la vida de los defensores** 

Entre las conclusiones que hace el informe, asegura que es urgente que se activen los mecanismos de alarmas tempranas y **se reconozca la situación que están denunciando las comunidades referente a la presencia y control de estructuras paramilitares** en el departamento.

De igual forma, las organizaciones que impulsaron el informe, rechazaron la estigmatización de la que han sido víctimas por parte del gobernador de Antioquía, Luís Pérez Gutiérrez, debido a que **“no colabora con la construcción de paz”**.

Finalmente en el informe se expresa que para lograr construir paz en los territorios y avanzar en la implementación de los acuerdos **es necesario que se realice el desmonte de las estructuras paramilitares** y se investigue quienes son los responsables de sostenerlas actualmente. (Le puede interesar:["Paramilitares asesinan a un jóven mientras jugaba fútbol"](https://archivo.contagioradio.com/paramilitares-asesinan-a-un-joven-mientras-jugaba-futbol/))

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
