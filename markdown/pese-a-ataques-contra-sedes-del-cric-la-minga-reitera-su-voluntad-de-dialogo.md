Title: Pese a ataques contra el CRIC, Minga reitera su voluntad de diálogo
Date: 2019-04-04 12:36
Author: CtgAdm
Category: DDHH, Nacional
Tags: CRIC, Manifestaciones en popayán, Minga Nacional
Slug: pese-a-ataques-contra-sedes-del-cric-la-minga-reitera-su-voluntad-de-dialogo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D3PzWMUWkAAORhB-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [4 Abr 2019] 

A raíz de los ataques realizados contra las sedes del **CRIC en Popayán**, por detractores de la Minga Nacional, dejando 10 heridos y grandes daños materiales, los pueblos indígenas han manifestado que desean continuar por la vía del diálogo con el Gobierno y que su intención nunca ha sido afectar a la comunidad payanesa, además, señalan al **Centro Democrático** y a algunos sectores empresariales como los promotores de estas agresiones quienes habrían pagado a ciudadanos para que se manifestaran.

**Enrique Perdomo, consejero del CRIC** se refirió a las denuncias hechas señalando que **existen audios y comunicaciones de WhatsApp que demostrarían que se le pagó a la gente por asistir**, sin embargo, indica que es un tema que está en investigación, pero que a pesar de los hechos delictivos, estos los motivan a avanzar en la Minga "con más fuerza, además hizo un llamado a la calma a los ciudadanos de Popayán, "es entendible que nos veamos afectados muchos pero es la única manera en que el Gobierno nos escuche".

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FSomosREMAP%2Fvideos%2F346262406005638%2F&amp;show_text=0&amp;width=476" width="476" height="476" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**¿Qué decisión tomará La Minga frente a estos ataques? **

Una directiva decidirá qué hacer frente a las manifestaciones de los ciudadanos payaneses, "no queremos llegar a ningún tipo de violencia, queremos manejar muy bien la situación para que no haya más violencia, aquí la confrontación no es con la población civil" asegura Perdomo.

Ante la destrucción de la IPS del CRIC y las declaraciones de dirigentes indígenas quienes manifestaron que, de continuar estas agresiones romperían los diálogos y optarían por tomarse la ciudad, el consejero hace énfasis en que no pueden "dejar desviar las políticas con las que se está haciendo la Minga" y agregó que fueron hechas con premura, "nos sentíamos acosados por la comunidad, esperábamos que la Fuerza Pública protegiera nuestras sedes, lo que no se dio".

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcric.colombia%2Fvideos%2F419523575532314%2F&amp;show_text=0&amp;width=560" width="560" height="280" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Perdomo espera que hoy se tomen decisiones sabias al interior de las comunidades indígenas, quienes siguen con el propósito de seguir dialogando con el Gobierno y que durante el día se realizarán una serie de reuniones con el alcalde de Popayán, la Defensoría del Pueblo y la Fuerza Pública, "para ver de qué manera se logra avanzar sin ningún tipo de agresión".

<iframe id="audio_34097544" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34097544_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
