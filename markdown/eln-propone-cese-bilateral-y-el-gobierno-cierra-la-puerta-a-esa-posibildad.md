Title: ELN propone cese bilateral y el Gobierno cierra la puerta a esa posibildad
Date: 2020-07-08 18:14
Author: CtgAdm
Category: Nacional
Tags: Cese al fuego bilateral, ELN, Gobierno Duque
Slug: eln-propone-cese-bilateral-y-el-gobierno-cierra-la-puerta-a-esa-posibildad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/ivan-duque-y-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: Pablo Beltrán del ELN

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Después de conocerse el pronunciamiento hecho por el Consejo de Seguridad de la Onu proponiendo un cese al fuego global en medio de la pandemia respaldado por el papa Francisco, el ELN propuso al Gobierno pactar un cese al fuego bilateral durante 90 días con el fin de crear "un clima de distensión humanitaria, favorable para reiniciar los Diálogos de Paz", pese a ello, el Gobierno cerró la puerta a esa posibilidad. [(Lea también: Consejo de Seguridad de la ONU llama a un cese al fuego global)](https://archivo.contagioradio.com/consejo-de-seguridad-de-la-onu-llama-a-un-cese-al-fuego-global/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El grupo guerrillero ya había decretado un cese al fuego unilateral durante el mes de abril, fecha en la que la Covid-19 comenzó a escalar en Colombia, en aquella ocasión Pablo Beltrán, jefe de la Delegación de paz del ELN manifestó que se acogían al llamado hecho por la ONU el papa Francisco, las comunidades y múltiples organizaciones en toda Colombia. [(Lea también: Comunidades del Pacífico exigen a Gobierno y grupos armados una inmediata salida negociada al conflicto)](https://archivo.contagioradio.com/comunidades-del-pacifico-exigen-a-gobierno-y-grupos-armados-una-inmediata-salida-negociada-al-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aquella decisión, según el jefe negociador fue denominada "cese al fuego bilateral activo", resaltando que se disponían a silenciar las armas pero se reservaban el derecho a defenderse de otros grupos armados. Durante aquel lapso y **según la Misión de Observación Electoral, desde que el ELN anuncio el cese, su actividad disminuyó un 83%** aunque a su vez destacaron cuatro hechos importantes atribuidos al grupo armado. [(Lea también: En necesario negociar un cese bilateral: ELN)](https://archivo.contagioradio.com/en-necesario-negociar-un-cese-bilateral-eln/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la propuesta hecha por el ELN, el presidente Duque manifestó su rechazo a la tregua y expresó[vía Twitte](https://twitter.com/IvanDuque)r que el país "les exige que liberen los secuestrados y pongan fin a sus actos criminales", descartando un acuerdo de cese bilateral, que tal como señaló la ONU alcanza a todos los actores armados, incluidas las fuerzas militares casi en su totalidad con excepciones hechas por el Consejo. [(Lea también: Con este Gobierno creo que no veremos un proceso de paz con el ELN: Carlos M. Gallego)](https://archivo.contagioradio.com/con-este-gobierno-creo-que-no-veremos-un-proceso-de-paz-con-el-eln-carlos-m-gallego/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde enero de 2019, el Gobierno suspendió las conversaciones de paz con el ELN tras el atentado con un carro bomba en la Escuela de Policía General Santander, en donde murieron 22 uniformados. Desde entonces, en cabeza del Alto comisionado para la Paz, Miguel Ceballos se ha exigido la entrega y extradición de los miembros negociadores del ELN que permanecen en Cuba, protegidos por los protocolos internacionales que blindan la negociación. [(Lea también: Miguel Ceballos no le cumple a la paz sino que "promueve el conflicto armado": Cepeda)](https://archivo.contagioradio.com/miguel-ceballos-no-le-cumple-a-la-paz-sino-que-promueve-el-conflicto-armado-cepeda/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ceballos expresó que no existe una voluntad de paz y que las condiciones para una eventual negociación, como lo son el cese de secuestros e instalación de minas antipersona no se ven reflejadas en el comunicado del ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

«Esa es la sordera y la falta de voluntad del Gobierno para una solución política» manifestó en aquel entonces Pablo Beltrán resaltando cómo incluso desde el ELN se escuchaban las voces de las comunidades que exigían una salida al conflicto a través del diálogo, sin embargo, el escenario ha sido rechazado por el Gobierno. [(Le recomendamos leer: Por tercera vez comunidades insisten a Gobierno y ELN por un acuerdo humanitario)](https://archivo.contagioradio.com/por-tercera-vez-comunidades-insisten-a-gobierno-y-eln-por-un-acuerdo-humanitario/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
