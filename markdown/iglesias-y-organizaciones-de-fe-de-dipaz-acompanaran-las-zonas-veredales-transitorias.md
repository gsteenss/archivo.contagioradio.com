Title: Iglesias y organizaciones de fe de DiPaz acompañarán las Zonas Veredales Transitorias
Date: 2016-08-20 18:44
Category: Nacional, Paz
Tags: Cese Bilateral de Fuego, DIPAZ, FARC, Proceso de conversaciones de paz
Slug: iglesias-y-organizaciones-de-fe-de-dipaz-acompanaran-las-zonas-veredales-transitorias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/dipaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Daniela Uribe] 

###### [19 Ago 2016]

Más de 16 iglesias y organizaciones basadas en la fé han ratificado su respaldo al Si en el plebiscito por la paz, además anunciaron la creación de dos casas de acompañamiento al proceso de c**oncentración de integrantes de las FARC en las zonas veredales transitorias de normalización** que se ubicarán en 23 veredas del país.

Jenny Neme, representante de DiPaz afirmó que DiPaz han estado sosteniendo reuniones de alto nivel para discutir sobre la resolución de la ONU que crea el mecanismo tripartita de verificación firmado el 23 de Julio, y que uno de los principales portes es la necesidad de que **ese mecanismo amplíe las posibilidades hacia la verificación de situaciones con otros grupos armados y que monitoree las amenazas y hostigamientos a defensores de DDHH y otros líderes en las regione**s.

Según el comunicado de la **ACT Alliance**, una organización de 140 iglesias y organizaciones de fe que tienen presencia en los 5 continentes, están dispuestos a acompañar el proceso de Cese Bilateral de la mano de DiPaz en las **casas de protección y también el proceso de implementación de los acuerdos** luego de la firma del acuerdo final entre el gobierno y la guerrilla de las FARC.

Rudelmar Bueno de Faria, representante del **Consejo Mundial de Iglesias** ante el **Consejo de Seguridad de las Naciones Unidas**, afirmó que en este momento están comprometidos con la verificación del Cese Bilateral, a partir del trabajo de verificación realizado por DiPaz durante año y medio, además asegura que s**eguirán aportando en la construcción de la paz en los demás pasos del proceso de paz, incluyendo la implementación.**

Jany Silva, representante de las Comunidades Construyendo Paz en los Territorios, CONPAZ, que también hace parte de DiPaz, aseguró que tanto los escenarios de discusión y aportes en Estados Unidos con embajadas y el Consejo de Seguridad de la ONU, como el respaldo de las iglesias es una **garantía para las comunidades donde se ubican las ZVTN**, no solamente porque se verifica el cese bilateral sino porque podrían ser garantía de seguridad frente a la presencia paramilitar.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
