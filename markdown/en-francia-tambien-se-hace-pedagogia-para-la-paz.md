Title: En Francia también se hace Pedagogía para la Paz
Date: 2016-04-01 11:39
Category: Nacional, Paz
Tags: ELN, FARC, francia, paz, proceso de paz
Slug: en-francia-tambien-se-hace-pedagogia-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/3-Festival-por-la-Paz-en-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Crónica del Quindío 

###### [1 Abr 2016] 

Como una iniciativa de colombianos que le apuestan a la pedagogía de la paz, en la ciudad francesa, Montreuil, se realiza del 1 al 3 de abril, el tercer Festival **por la paz en Colombia, Memorias y Justicia Social, "Festival pour la Paix en Colombie" con el eslogan** “Juntos por la democracia construyamos una cultura de paz”.

Se trata de una iniciativa política y cultural en la que se contará con diversas actividades, como debates con participación de analistas y profesores universitarios colombianos y europeos, conciertos, recitales de poesía, teatro, artes plásticas, documentales, actividades manuales, además de noches de rumba al son de la música colombiana acompañada por los platos típicos del país.

“Esta coyuntura favorable a la paz nos condujo a escoger como eje de nuestro festival. Tal como los dos festivales precedentes, se trata de un evento de carácter festivo, cultural y político”, explican los organizadores.

De acuerdo con María Cecilia Gómez, una de las organizadoras del festival, esta vez **“se discutirá el papel de los medios de comunicación en la construcción de la paz”.** Así mismo, se invitará a la sociedad civil a que apoyen los dos procesos de paz con el ELN y las FARC, teniendo en cuenta que este momento es una oportunidad para todo el pueblo colombiano.

<iframe src="http://co.ivoox.com/es/player_ej_11010464_2_1.html?data=kpadk5WYepWhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5ynca7VjKjSxc7QrcKfqIqwlYqmd87Z25KYqMrXuMrqwtGYssbNvIzZz5Cw0dHTscPdxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[![Festival por la Paz en Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Festival-por-la-Paz-en-Colombia.jpg){.aligncenter .size-full .wp-image-22143 width="679" height="960"}](https://archivo.contagioradio.com/en-francia-tambien-se-hace-pedagogia-para-la-paz/festival-por-la-paz-en-colombia/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
