Title: "Noviolencia y Naturaleza" un foro por la resistencia a la megaminería
Date: 2015-08-11 17:03
Category: Ambiente, Nacional
Tags: CATAPA, Colectivo SocioAmbiental Juvenil Cajamarcuno COSAJUCA., Daan Janssens, Foro Internacional Noviolencia y naturaleza, Universidad del Quindío
Slug: noviolencia-y-naturaleza-un-foro-por-la-resistencia-a-la-megamineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/antiminera2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radiomacondo.fm 

###### <iframe src="http://www.ivoox.com/player_ek_6550675_2_1.html?data=l5qikpubeY6ZmKiakp6Jd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitDm0JDW0NnJts%2FVxM7c0MbQb9Tjw9fSjbPTb9fd0NHS0MjNpYztjLPO1trWpc3Z28aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [11 Ago 2015] 

Desde el 10 y hasta el próximo 12 de agosto, se realiza en la Universidad del Quíndio la IV edición del Foro internacional sobre Noviolencia y Naturaleza, un espacio para la reflexión y el debate frente a las diferentes formas de agresión contra el paisaje natural y los pueblos, como consecuencia de las desaforadas políticas extractivistas , que amenazan la sostenibilidad ambiental en el territorio nacional y el planeta entero.

Simultaneamente, durante los tres días se realiza la IV versión del Seminario Internacional "El Dorado IV", un encuentro con los futuros líderes y liderezas, de América Latina pertenecientes a comunidades campesinas e indígenas afectadas por las explotaciones mineras a gran escala.

Según Daan Janssens, voluntario del Comite Académico Técnico de asesoramiento a problemas ambientales CATAPA, en entrevista con Contagio Radio, el objetivo del seminario es "capacitar esas comunidades en el tema de resistencia a la mega minería en los territorios".

Organizaciones de Perú, Bolivia, Guatemala, Honduras, Bélgica, Holanda se unen a las delegaciones nacionales provenientes de regiones como Cauca, la Guajira y Santander, en un intercambio de experiencias y luchas comunes entre países en los que según Janssens "se notan las mismas dinámicas de un estado violento junto con las empresas de seguridad contratadas por las empresas mineras que van sembrando miedo y amenazando a la gente que trata de resistirse".

De acuerdo con el voluntario de nacionalidad Belga, es necesario "entrar en un diálogo con lo políticos que están diseñando las políticas extractivistas, por que no tiene sentido hablar con una empresa minera cuando explota un terreno considerado como "Intangible", es decir una zona libre de minería, donde no se garantiza la aplicación del principio de precacusión".

El evento tiene un enfoque particular en el caso de explotación aurífera en "La colosa", adelantada en el departamento del Tolima, contando con la participación e intervención de los jóvenes ambientalistas en resistencia pertenecientes al Colectivo SocioAmbiental Juvenil Cajamarcuno COSAJUCA.

Las conclusiones obtenidas al cierre del evento, serán llevadas a la ciudad de Bogotá para ser presentadas en rueda de prensa el jueves 13 de agosto, y compartidas en varios ministerios, la Embajada de Bélgica en Colombia, ante el Comisionado de Derechos Humanos de las Naciones Unidas y la presidencia de la República entre otros.
