Title: Grecia y Turquía inician negociaciones de Paz en Chipre
Date: 2015-05-29 13:10
Category: El mundo, Otra Mirada
Tags: Chipre, Conflicto armado Chipre, Gobierno GrecoChipiotra, Gobierno TurcoChipiotra, Negociaciones de Paz Chipre
Slug: grecia-y-turquia-inician-negociaciones-de-paz-en-chipre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/b030605ac.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Estambul.wordpress.com 

###### [29May2015] 

El líder **Grecochipiota Nikos Anastasiadis, y el Turcochipriota, Mustafá Akinci,** iniciaron este jueves una **ronda de conversaciones con el objetivo de comenzar un proceso de paz** y finalizar con el último muro de separación que existe en Europa.

Según Anastasiadis ¨...Hay que encontrar cuanto antes **una solución que asegure que este Estado cumplirá con los atributos europeos del resto de la UE**...”. A lo que añadió de su homólogo Turco que “...**Akinci está a favor de la solución**; esta vez la atmósfera es diferente...”.

Actualmente las negociaciones de paz dependen de la **reparación y búsqueda de los 2.000 desaparecidos durante la invasión Turca**, de los pasos fronterizos, y el final del muro entre ambos países. Como principal escollo, las **200.000 hectáreas de tierras y viviendas que los Grecochipriotas** tuvieron que abandonar y que actualmente llevan 40 años siendo ocupadas por Turcos**.**

Este es uno de los temas más complicados, puesto que la devolución de las propiedades es un mínimo para la parte Griega, pero es evidente que después de **40 años va a ser muy complicado que los nuevos ocupantes abandonen sus tierras.**

En últimas, la decisión dependerá del presidente Turco Erdogan para que ceda el control del norte de la isla y así permita **la autonomía de Chipre**.

La **ONU mantiene un contingente de más de 400 Cascos Azules** en la isla en los pasos fronterizos, de los cuales actualmente han sido desbloqueados 9 de ellos, aunque aún continúan los de la capital Nicosia, con un muro que recuerda al Berlín de la Guerra Fría.

El conflicto de la Isla de **Chipre se remonta a los años sesenta cuando Turquía invadió la isla** y proclamó la República Turca de Chipre en el norte iniciando hostilidades con Grecia.

La parte Turcochipriota solo ha sido reconocida por Ankara y actualmente la isla tiene estatus independiente como **República de Chipre y hace parte de la UE**.

El origen de las hostilidades entre las dos comunidades hace referencia a que la comunidad **Grecochipriota busca la unión con Grecia y la Turcochipriota busca no vivir bajo un régimen Griego y es partidaria de la Taksim o partición de la isla**.
