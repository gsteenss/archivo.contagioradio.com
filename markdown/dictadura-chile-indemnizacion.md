Title: Indemnizarán a 71 ex presos políticos de la dictadura en Chile
Date: 2017-04-18 13:10
Category: El mundo, Otra Mirada
Tags: Chile, dictadura, indemnizacion, presos politicos
Slug: dictadura-chile-indemnizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/pele-e-pinochet.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Divulg 

##### 18 Abr 2017 

Por orden de la Corte Suprema de Chile, **71 ex prisioneros políticos sobrevivientes a la dictadura militar serán indemnizados**. Los beneficiados que hacen parte de los casi 37.000 expresos políticos o torturados entre los años 1973 y 1990, pertenecen a un grupo especial conocido como Comisión Valech.

En su decisión el tribunal rechazó la afirmación del fisco respecto a que las torturas ya habían prescrito y **obliga al Estado a abonar 3.2 millones de dólare**s por la comisión de ese tipo de delitos asegurando que, por tratarse de hechos ocurridos con la intervención de agentes del Estado **es imposible "declarar la prescripción de la acción penal que de ellos emana"**.

La sentencia de la Corte Suprema confirmó el fallo de un tribunal inferior al tratarse de crímenes de lesa humanidad. Los crímenes cometidos por la policía del general Augusto Pinochet, alcanzaron la cifra de **3.095 opositores asesinados y 1.192 desaparecidos en 17 años de dictadura**.

**Detención de Cristian Labbé**

Este lunes se presentó voluntariamente ante la Brigada de Derechos Humanos de la Policía de Investigaciones **Cristián Labbé Galilea coronel del Ejército en retiro y ex alcalde de la comuna de Providencia**, para dar cumplimiento de la orden proferida en su contra **por torturar a cuatro personas en 1973** durante la llamada "Operación peineta"

En la orden judicial, emitida por la Corte de Apelaciones de Temuco, Labbé aparece fichado como **responsable de la aplicación de tormentos** en contra de Harry Edward Cohen Vera, Jaime Rozas González, Bernardo Santibáñez Álvarez y Juan Horacio Rosales Quintana, **en la localidad de Panguipulli, en la sureña región de Los Ríos**.

De acuerdo con el abogado de una de las víctimas, **la labor del entonces Coronel consistía en **"**enseñar a torturar**" e instruir a las unidades militantes y de inteligencia sobre las técnicas aprendidas por el exuniformado en cursos en Brasil, con alto grado de perversidad. Le puede interesar: [90 mil estudiantes se movilizan contra la reforma educativa en Chile](https://archivo.contagioradio.com/estudiantes-se-movilizan-en-chile/).

Labbé, ex alcalde de Providencia por más de 16 años , quien se desempeñó también como jefe de seguridad de Pinochet, **quedará en prisión preventiva por ser considerado "un peligro para la seguridad de la sociedad"** y se le negó el beneficio a la libertad bajo fianza que en dos detenciones anteriores le habian permitido quedar en libertad.

###### Reciba toda la información de Contagio Radio en [[su correo]
