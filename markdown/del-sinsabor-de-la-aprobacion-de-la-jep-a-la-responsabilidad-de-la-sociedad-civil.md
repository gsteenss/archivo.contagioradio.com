Title: Del sinsabor de la aprobación de la JEP a la responsabilidad de la sociedad civil
Date: 2017-03-23 08:21
Category: invitado, Opinion
Tags: JEP, Nestor Humberto Martínez, Paramilitarismo
Slug: del-sinsabor-de-la-aprobacion-de-la-jep-a-la-responsabilidad-de-la-sociedad-civil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/jep.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

#### [**Por @HeinerGaitan**] 

###### 23 Mar 2017

[Apenas han transcurrido unos días desde que la plenaria del Senado aprobó, mediante el mecanismo de fast track, la Jurisdicción Especial para la Paz. Las distintas objeciones de los congresistas alrededor de su impedimento para debatir este elemento esencial en la implementación del acuerdo; las proposiciones en cuanto limitar el alcance de la JEP; y un ejecutivo desgastado que perdió la iniciativa de la paz; componen el tablero sobre el cual salió aprobada una Justicia Especial malformada en su espíritu respecto del acuerdo firmado en el Teatro Colón.]

[La denominada “operación tortuga” mediante la cual los diferentes colectivos de derechos humanos denunciaron ante la opinión pública la poca celeridad con que el congreso discutió y aprobó la JEP, expuso el temor de la clase política en que el componente de Verdad, contemplado en la jurisdicción, expusiera los vínculos criminales entre empresarios, políticos y narcotraficantes, quienes le dieron vida a la estrategia paramilitar de exterminio del movimiento social, desplazamiento de campesinos y apropiación ilegitima de tierras.]

[Con los 26 senadores que inicialmente se declararon impedidos por conflictos de intereses (familiares y aliados políticos comprometidos con el paramilitarismo); las 70 proposiciones en el trámite de este proyecto, que finalmente tuvo 83 modificaciones respecto del original, se comprobó que no es la ausencia de justicia punitiva lo que preocupa. Todo lo contrario: es el exceso de verdad que arrincona y  produce pavor a los principales beneficiarios de la guerra.]

[Sobre ese componente, el de la verdad ante el país y la historia, la proposición esperpento promovida por Cambio Radical, y secundada por el fiscal, Néstor Humberto Martínez, alrededor de amortiguar el trato a los civiles comprometidos con el conflicto (instigadores, financiadores, determinadores y beneficiados de la violencia paramilitar), finalmente aprobada con el agravante que dichos terceros civiles irán de manera voluntaria a la Jurisdicción.]

[Con lo anterior, la armonía del Sistema Integral quedó herida. Al ir los terceros civiles voluntariamente, y sin el temor de someterse a sanción alguna, las víctimas y el país se exponen a recibir una verdad a medias, a no ser reparadas material y simbólicamente. Tampoco habrá, de parte de los mayores beneficiados de la estrategia paramilitar, el compromiso de no promover de cara al futuro atrocidades contra la sociedad civil y la democracia colombiana.]

[De igual modo, y de mayor escándalo resulta que, el tratamiento penal especial y diferenciado para agentes del Estado, contemplado en el título IV de la ley 1820 de 2016, y el decreto 277 de 2017, existentes para insertar en la justicia transicional los hechos de los militares en razón y en contexto del conflicto, sean la llave para asegurar la impunidad de este actor del conflicto, al quedar confirmado que la Responsabilidad de Mando aplicará en determinados casos, produciendo la no armonización de los elementos del acuerdo con los estándares internacionales.]

[En síntesis, los terceros civiles quedaron blindados ante la JEP y la fuerza pública tendrá un tratamiento especial diferencial sumamente laxo y flexible. Ante este escenario, el elemento central del Acuerdo de la Habana, el Sistema Integral de Verdad, Justicia, Reparación y Garantías de No Repetición, quedó limitado. Sus potencialidades ya no son ahora un elemento de disputa en escenarios normativos sino, por lo contrario, tendrá que trascender al terreno de la opinión y movilización pública para que, en el marco de la verdad procesal, se pueda construir el relato de lo sucedido en aras de configurar la verdad histórica.]

[Santos II ya vive el deshielo de su coalición de gobierno. A pesar que se discuten las reformas necesarias para viabilizar el acuerdo de La Habana, la paz es rehén de los cálculos politiqueros: de una parte de quienes quieren vender su voto en el capitolio con la garantía de no perder sus activos burocráticos, necesarios en una época preelectoral donde la clase política tiene sus apuestas; por otra parte, de quienes, con la apariencia de defender la paz, antagonizan la salida política del conflicto con la lucha contra la corrupción.]

[Por último, ante el evidente desgaste de Santos II, que pierde la iniciativa de la paz, le tocó asumir ese papel a la sociedad civil expresada en las veedurías que ejercieron control a los senadores en la plenaria de aprobación de la JEP. Ya es hora que esas plataformas cívicas le apuesten a una movilización del tamaño de aquella marcha del 5 de octubre: la persistencia de la estrategia paramilitar, acallando la vida de dirigentes sociales, la táctica de imponerle a la verdad y reconciliación vericuetos y leguleyadas, así lo exigen.]

[En todo caso, aprobada la Justicia Especial para la Paz, el escenario para reclamar la integralidad y rescatar las potencialidades de los componentes del sistema de justicia transicional corresponde a la opinión pública movilizada.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
