Title: Campesino fue víctima de la brutalidad policial del ESMAD en el Cauca
Date: 2019-03-06 13:43
Author: CtgAdm
Category: DDHH, Nacional
Tags: campesinos, Cauca, Docentes, ESMAD, paro
Slug: campesino-fue-victima-de-la-brutalidad-policial-del-esmad-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-05-at-3.31.30-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ciudad en Movimiento] 

###### [06 Mar 20219] 

Se cumplen 9 días de paro de la comunidad docente y campesina en el departamento del Cauca. Las personas denunciaron que durante la jornada de movilización de ayer, en el Cairo, el accionar del ESMAD dejo un saldo de **25 personas heridas, una de ellas en grave riesgo, debido a que fue impactado por un gas lacrimógeno en su ojo izquierdo.**

De acuerdo con Sebastian Linero, integrante de Ciudad en Movimiento, desde la Mesa de Derechos Humanos por la Defensa de la Vida y el Territorio, ya habían interactuado con la Fuerza Pública para hacer tomas intermitentes a la vía Panamericana, sobre el municipio del Cairo, sin que se presentaran inconvenientes.

Sin embargo, aseguró que durante el día de ayer llegó una arremetida del ESMAD inesperada y contundente, "creemos nosotros que fue una acción militar en contra de la protesta social, debido al uso indiscriminado de gases, **bombas aturdidoras, el uso de armas no convencionales como caucheras, con las que nos lanzaban piedras**".

Además, señaló que mientras tenían un espacio de diálogo con el comandante de la Policía a cargo de la Fuerza Pública, se desarrolló una segunda arremetida en contra de las organizaciones sociales e ingresando al lugar de encuentro de las mismas, en donde denuncias que hay robo de elementos y herramientas para la subsistencia de las personas.

"Rompieron plásticos, se robaron maletines, herramientas de trabajo de los campesinos como machetes y azadones, **se robaron los camping, dañaron las cocinas que teníamos, votaron los alimentos y agredieron a compañeras con insultos y groserías**", aseveró Linero.

Asimismo, la ciudad de Popayán también se ha visto gravemente afectada por la brutalidad policial del ESMAD que ha ocasionado el cierre de dos jardines infantiles, afectando a menores de edad que inhalaron gases. (Le puede interesar: ["Enfrentamientos con el ESMAD dejan 18 heridos durante paro en Cauca"](https://archivo.contagioradio.com/enfrentamientos-con-el-esmad-dejan-18-heridos-durante-paro-campesino-en-el-cauca-2/))

### **La solución del gobierno Duque** 

Linero informó que en estos momentos las organizaciones sociales se encuentran cercados por la Fuerza Pública, en donde también habría presencia de militares, de igual forma, manifestó que temen que se este preparando un **operativo para acabar con el paro docente y campesino, y capturar de forma masiva a las y los líderes sociales de este escenario.**

"Sobretodo queremos proteger la vida de nuestros compañeros y compañeras que están en movilización y estamos evaluando cual es la mejor acción en este contexto de represión" afirmó Linero.

Igualmente, las organizaciones están a la espera de la llegada de delegados del gobierno de alto nivel para iniciar las conversaciones, **no obstante, aún no se conocen quiénes serán esas personas.**

###### Reciba toda la información de Contagio Radio en [[su correo]
