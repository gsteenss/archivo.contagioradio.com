Title: “Si no cerramos el puerto, el Gobierno no nos presta atención” habitantes de Buenaventura
Date: 2017-05-17 16:19
Category: DDHH, Nacional
Tags: buenaventura, paro, Paro cívico
Slug: si-no-cerramos-el-puerto-el-gobierno-no-nos-presta-atencion-habitantes-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/parocivicobuenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [17 May. 2017]

Aunque el Comité del Paro Cívico ya recibió el oficio que formaliza una posible reunión del Gobierno con los habitantes de Buenaventura, hasta la fecha no se ha dado. El Comité aseguró que analizará el documento para ver cuáles son las propuestas y ver cómo procederán. Actualmente bloquean** los puertos el transporte intermunicipal y varios comercios han cerrado sus puertas.**

“Desafortunadamente el único tema que le interesa a los grandes empresarios, al Gobierno y al Estado son los puertos, no las vergüenzas que se viven en la ciudad. Entonces nos toca usar las herramientas que tenemos, **paralizar la ciudad, el territorio y la actividad portuaria** que es la que definitivamente mueve a Colombia” dice Victor Vidal integrante del Proceso de Comunidades Negras y del Comité del Paro.

Contrario a lo que manifestó el Ministerio de Salud y de Ambiente, **este miércoles no se llevará a cabo ninguna reunión**, sino que la misma delegación que se encuentra en Chocó será la que se desplace hasta Buenaventura para reunirse con el Comité del paro.

“Todos vemos que el tema de Quibdó no está fácil, ya llevan una semana en paro y según se sabe ayer hubo unas dificultades, entonces **suponemos que ya hoy no va a haber ninguna delegación acá” relató Vidal**. Le puede interesar: ["Gobierno hace oídos sordos al paro del Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/)

### **Nisiquiera se cumplió con el plan de choque** 

Ante el cumplimiento del 71% de los acuerdos pactados por parte del Gobierno, Vidal dice que no entiende con qué criterio miden eso "porque a hoy el plan de choque está incumplido, la nueva sede del SENA que va apenas en sedimentación 3 años después, que ha avanzado en un 40% y que está abandonada porque se le terminó el contrato al contratista, las obras de acueducto están en proceso, **no sé de dónde sacan esa cifra” recalcó el líder.  Le puede interesar:["Buenaventura irá a paro y exigirá vivir con dignidad y paz en sus territorios"](https://archivo.contagioradio.com/40089/)**

Las comunidades dijeron que continuarán exigiendo **se declare la emergencia social, económica y ecológica** porque según ellos esa es la posibilidad para que el Gobierno actué de manera inmediata en Buenaventura, de modo que pueda disponer recursos, modificar normas y en generar contrataciones inmediatas que den por finalizadas las obras que se encuentran paralizadas.

“Necesitamos que se actúe inmediatamente, que se hagan planes inmediatos y a mediano plazo y **que la realidad de Buenaventura se vaya transformando porque realmente es vergonzosa** la realidad que tenemos nosotros sabiendo que somos el primer aportante a la riqueza nacional” concluyó Vidal.

<iframe id="audio_18766190" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18766190_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
