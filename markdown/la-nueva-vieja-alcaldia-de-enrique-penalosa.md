Title: La nueva vieja alcaldía de Enrique Peñalosa
Date: 2016-02-16 14:54
Category: Javier Ruiz, Opinion
Tags: Bogotá, Peñalosa, Petro, reserva Van der Hammen
Slug: la-nueva-vieja-alcaldia-de-enrique-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Enrique_Penalosa_bike_hr.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: olapolitica 

#### **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/)- [@contragodarria ](https://twitter.com/ContraGodarria)** 

###### [16 Feb 2016 ] 

A mes y medio de gestión se puede decir que le está quedando grande la ciudad de Bogotá a Enrique Peñalosa. No más en una semana demostró toda su incapacidad en la administración de la ciudad cuando no supo manejar el incendio en los cerros orientales afectando el aire de toda la ciudad y en el Día sin Carro en donde amenazó a un conductor de Transmilenio porque se averió el bus en donde iba el alcalde (su creación predilecta) y el desprecio clasista y arribista a una vendedora ambulante de tintos que le exigía soluciones para superar la informalidad laboral y pidiendo que no fueran agredidos por la policía en los operativos de recuperación del espacio público. Sin contar las protestas contra Transmilenio por su pésimo servicio en toda la ciudad.

[Peñalosa gano la alcaldía con el discurso de “recuperar a Bogotá”. El actual alcalde en plena campaña electoral posaba como un hombre pragmático alejado de ideologías y populismos que prometía soluciones a todas las problemáticas de Bogotá. Al alcalde actual le gusta que lo llamen “gerente” más no “alcalde” y en la campaña electoral para la alcaldía del 2007 dijo una vez que no ve a Bogotá como una ciudad sino como una empresa en donde un gerente debe poner “orden” a las cosas.]

[Los grandes medios ayudaron a reforzar la imagen del hombre pragmático, técnico y gerencial vendiéndonos la idea de que esa persona era la salvación de Bogotá ante la supuesta crisis de la ciudad por culpa de los gobiernos de izquierda que, según el relato que impuso los mismos grandes medios a los bogotanos, el caos en la movilidad, la inseguridad, la pobreza y demás males eran culpa de Petro y de la izquierda en general.]

[Al parecer, lo anterior fue determinante para que Peñalosa ganara la alcaldía y muchos votantes ingenuamente gritaban que habían “recuperado” a Bogotá. Pero al iniciar su nuevo mandato con sus propuestas quiere retornar al pasado, retornar al año 1998 en donde se ha vuelto ver el clasismo hacia los pobres, el abandono al sur de Bogotá, las agresiones a los vendedores ambulantes por parte de la policía, el no definitivo al metro porque según el “gerente” Transmilenio hace lo mismo que un metro pero más barato y de ñapa gana una “comisión” gracias a la compañía sueca de buses “Volvo”, el daño ambiental por llenar de cemento y construcciones a toda la ciudad y la posible privatización de la ETB.]

[El Enrique Peñalosa del 2016 no es diferente al Enrique Peñalosa de 1998 porque ahora ha vuelto atacar nuevamente a los vendedores ambulantes y a los habitantes de calle porque “afean” la ciudad y deben ser “erradicados” (gestión neoliberal de la pobreza). Ha propuesto la venta de la ETB con el eufemismo de que con lo que se obtenga con la venta de la empresa se harán más vías y colegios. Ha dicho de manera falsa que no hay “estudios” que digan que en los humedales no se puedan construir y que la reserva Van der Hammen solo hay vacas y potreros.]

[Pero lo más grave es que su “hijo predilecto” ha colapsado y la gente protesta por eso siendo acusados de ser “vándalos y terroristas organizados” y no como en la era Petro de “ciudadanos indignados pidiendo un transporte más digno” pervirtiendo el lenguaje para desinformar y ocultar la mala gerencia de Peñalosa con su invento.]

[Al parecer en la “Bogotá Mejor Para Todos” aplican condiciones y restricciones. Ingenuos, sigan creyendo que “recuperaron” a Bogotá porque vamos de vuelta a 1998.]
