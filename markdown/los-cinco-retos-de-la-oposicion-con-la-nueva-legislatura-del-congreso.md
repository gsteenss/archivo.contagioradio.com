Title: Los cinco retos de la oposición con la nueva legislatura del Congreso
Date: 2019-07-23 10:40
Author: CtgAdm
Category: Nacional, Política
Tags: Congreso, Doble Instancia, La Jugadita, Legistatura
Slug: los-cinco-retos-de-la-oposicion-con-la-nueva-legislatura-del-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón] 

La apertura de una nueva legislatura en el Congreso de la República llega de la mano de nuevos proyectos de ley a debatir:  la reforma rural integral, la reforma a la justicia, el estatuto anticorrupción e incluso la polémica doble instancia retroactiva son algunos de los proyectos que avanzarán o no, dependiendo de la forma en que sean presididas en la Cámara de Representantes y el Senado a lo largo de este periodo legislativo entrante.

### 1. Nueva legislatura, nuevas formas de presidir el Congreso

Para el representante del Partido Verde, Mauricio Toro, la permanencia de un partido alternativo como Cambio Radical y la llegada de Carlos Cuenca a la presidencia de la Cámara del Congreso, es un indicio de independencia para los partidos alternativos; una situación que contrastó con la del Senado, donde hasta el pasado 20 de julio presidía el senador del Centro Democrático, Ernesto Macías.

### "Macías trata de burlarse de todas las personas que piensan diferente"

Mauricio Toro señala que a lo largo de su trayecto, Macías **"ideó mecanismos para acallar voces distintas a las que él consideraba deberían hablar, utilizando artimañas y procesos"**, actuación que, para Toro se debate entre la legalidad y la ilegalidad y que tendrá que evaluar la Procuraduría. [(Lea también: Ernesto Macías está censurando a la oposición: Jorge Robledo)](https://archivo.contagioradio.com/macias-censurando-oposicion/)

Sobre su última actuación el pasado 20 de julio, cuando Macías trató de censurar la replica de la oposición al discurso del presidente Duque, para Toro quedó claro que la "jugadita" del senador del Centro Democrático, fue hecha únicamente con la intención que "el país no conociera la verdad" con respecto a temas como la realidad económica, el incremente del desempleo y las problemáticas de medio ambiente y seguridad que en el discurso del presidente Duque no se vieron reflejadas y que la oposición estaba dispuesta a visibilizar.

### 2. Ley  de Doble Instancia: un vestido hecho a la medida de Andrés Felipe Árias

Para el representante la ley de doble instancia o ley Andrés Felipe Árias como algunos la han denominado, es un tema delicado, señalando que **"se está haciendo un proyecto de ley dirigido a una sola persona"**, el que buscaría beneficiar al exministro de Agricultura quien ya fue investigado y condenado.

Toro además advirtió sobre el peligro que significaría apoyar un proyecto con retroactividad, es decir que existen al menos 238 casos, según el representante de personas condenadas por corrupción que podrían demandar al Estado por no haber tenido derecho a una doble instancia pese a cumplir su pena.

### 3. Reformas en el Congreso al campo y a la justicia

Otros retos que llegarían para esta nueva legislatura en el Congreso serían la reforma rural integral y la reforma de justicia que buscarían dar al campo y a las instituciones que conforman el sistema judicial del país, una forma de operar con eficiencia. Además, indicó que también se debe insistir en sacar adelante los puntos de la consulta anticorrupción, que incluyen el congelamiento de los salarios para los congresistas y la anulación de casa por cárcel para personas ligadas a delitos de corrupción.

### 4. Celeridad en la Comisión de Acusaciones de la Cámara

En torno al funcionamiento de la  Comisión de Acusaciones de la Cámara, el representante señaló que la reforma a la justicia debe contemplar un mecanismo para este organismo funcione con mayor eficacia  y celeridad, de cara a los procesos que tienen en contra diferentes figuras como magistrados y expresidentes y que aún no han sido resueltos.

### 5. ¿Qué pasará con la curul de Santrich?

Sobre la curul del partido FARC, que hasta hace poco ocupaba Jesús Santrich y que, en la actualidad se discute si debe ser otorgada nuevamente a la colectividad, Toro expresó que debe ser el Consejo de Estado el que defina cómo proceder ante este escaño. Sin embargo, expresó que es deber de quienes ocupan actualmente las curules de FARC, atender al llamado de la justicia, tal como lo han venido haciendo la mayoría de sus integrantes.  [(Le puede interesar: El mensaje no llegó como queríamos que llegara: Partido Verde)](https://archivo.contagioradio.com/santrich-error-partido-verde/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38803741" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38803741_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
