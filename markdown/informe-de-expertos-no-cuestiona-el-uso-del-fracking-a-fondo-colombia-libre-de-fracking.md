Title: "Informe de expertos no cuestiona el uso del fracking a fondo": Colombia Libre de Fracking
Date: 2019-04-09 22:57
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Alianza Colombia Libre de Fracking, fracking
Slug: informe-de-expertos-no-cuestiona-el-uso-del-fracking-a-fondo-colombia-libre-de-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-27-a-las-5.17.01-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CarlosSantiagoL  ] 

[En relación con el texto completo del informe sobre la conveniencia del uso del fracking en Colombia, realizado por un comité de expertos, publicado el pasado 5 de abril, Carlos Santiago, integrante de la Alianza Colombia Libre de Fracking, asegura que el documento de 173 páginas "busca cual es la mejor manera de realizar el fracking, pero no hace un cuestionamiento de la técnica de fondo ni se discute si la técnica es viable o no en el país".]

Por un lado, [el comité indica que es necesario obtener lo que llaman la "**licencia social**", entendida como la aprobación o la aceptación de las comunidades locales a un proyecto, para adelantar con actividades de exploración y extracción de los proyectos piloto. Sin embargo,] los habitantes de el Cesar y el Magdalena Medio, donde se pretende iniciar estos proyectos, se han manifestado en múltiples instancias en contra del uso de esta técnica de extracción.

Si bien el documento reconoce esta inconformidad y llama al fortalecimiento de los mecanismos de participación ciudadana, también resalta que "**una consulta popular no es equivalente a la licencia social** ya que las comunidades podrían verse coaccionadas por diferentes grupos de presión para decidir sobre el desarrollo de proyectos extractivos en su territorio". (Le puede interesar: "[Comunidades rechazan el avance de proyectos piloto de fracking](https://archivo.contagioradio.com/comunidades-rechazan-avance-proyectos-piloto-fracking/)")

El integrante de la Alianza contradice esta oposición, manifestando que e[l mecanismo que debería implementarse es el de la consulta popular para que las comunidades decidan democráticamente si están de acuerdo o no con los proyectos propuestos. Santiago asegura que el informe busca "inventarse otros mecanismos que no le den el peso suficiente a las decisiones democráticas que puede tomar un municipio".]

Por otro lado, las conclusiones del informe también se quedan cortas en cuanto a los riesgos que estarían ocasionando estos proyectos. "[Dice que todos estos impactos se pueden evitar o se pueden disminuir", dice el vocero, esto a pesar de estudios científicos en Estados Unidos que demuestran la relación entre el fracking, el movimiento sísmico y la contaminación del agua. ]

Santiago contrastó esta conclusión con las declaraciones del informe técnico que publicó recientemente la Contraloría General, donde advierte que Colombia no está lista para el fracking por la debilidad de instituciones de seguimiento y control y la falta de líneas de base. Además, el texto resaltó las posibles afectaciones al agua que ocasionaría el fracturamiento hidráulico.

Frente la publicación de estos dos informes, la Alianza [Colombia Libre del Fracking indica que solicitará a la Comisión Quinta del Senado iniciar el debate sobre el proyecto de ley para la prohibición del uso de la técnica.]

<iframe id="audio_34370596" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34370596_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
