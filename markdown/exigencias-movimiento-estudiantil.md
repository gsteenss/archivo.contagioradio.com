Title: Las 10 exigencias del movimiento estudiantil al Gobierno Nacional
Date: 2018-09-27 17:38
Author: AdminContagio
Category: Educación, Movilización
Tags: Educación Superior, ENEES, Iván Duque, UNEES
Slug: exigencias-movimiento-estudiantil
Status: published

###### [Foto: Contagio Radio] 

###### [27 Sep 2018] 

Hoy fue radicado en el **Congreso de la República, la presidencia y el Ministerio de Hacienda**, el pliego de exigencias de la **Unión Nacional de Estudiantes de la Educación Superior (UNEES)** en el que reclaman más presupuesto para el sector educación, subsanar la deuda de las Universidades Públicas y revisar el estado de la educación privada.

Con este petitorio, los estudiantes esperan que el Gobierno Nacional estudie las exigencias, **se comprometa a saldar la deuda histórica de 16 billones de pesos** con la educación superior pública y derogue la Ley 1911 de Financiación Contingente al Ingreso que afecta los recursos económicos que reciben las universidades. Según Santiago Rodríguez, integrante de la UNEES, con el pliego también se busca un espacio de incidencia del movimiento estudiantil en las decisiones que atañen al sector educativo.

### **El primer espacio de incidencia será el Presupuesto General de la Nación** 

Uno de los puntos fundamentales del pliego es el aumento del presupuesto para Colciencias en un 100%, basado en el rubro que recibió la entidad durante este año, además  piden que se modifiquen los criterios de este instituto para financiar proyectos. Según Rodríguez, **es inexplicable que Duque diga que va a destinar más dinero para financiar una guerra que no existe, mientras se destinan sólo 130 mil millones para Ciencia y Tecnología.**

Sobre el Programa Ser Pilo Paga, el líder estudiantil señaló que la UNEES está pidiendo garantías para quienes desertaron del programa y para quienes se mantienen en él, de forma tal que se aseguren los recursos que les permitan finalizar sus estudios. Los estudiantes también solicitaron que se revisaran los incrementos en las matriculas de las universidades privadas y la **reliquidación de las deudas de los estudiantes con el ICETEX con tasa real de interés en 0%.**

Rodríguez manifestó que **el pliego hace propuestas sobre un modelo educativo diferente que permita mejorar la garantía de acceso, permanencia y calidad de la educación;** y afirmó que el movimiento estudiantil está pidiendo que se invierta en este sector como un camino para hacer el tránsito de una sociedad en conflicto, a una que asegure la educación superior digna con acceso universal. (Le puede interesar: ["El presupuesto para 2019 de Colombia es el de un país en guerra"](https://archivo.contagioradio.com/presupuesto-pais-guerra/))

### **¿Qué viene después del Pliego de Exigencias?** 

El pliego fue creado por la UNEES que es el espacio amplió de participación para los estudiantes de educación superior, mientras el ENEES es la instancia nacional en la que el encuentro estudiantil presenta sus peticiones; y será desde allí, que se construirá próximamente un programa que presente en detalle los pasos e instituciones del Estado encargados de cumplir las demandas contenidas en el Pliego de Exigencias.

Finalmente, Rodríguez sostuvo que **el próximo 10 de octubre se realizará una movilización nacional,** en la que esperan que asistan diferentes sectores de la organización social que les permita construir una agenda común para interpelar el plan de desarrollo del Gobierno. (Le puede interesar: ["En septiembre se realizará el segundo encuentro de estudiantes de educación superior"](https://archivo.contagioradio.com/en-septiembre-se-realizara-el-segundo-encuentro-de-estudiantes-de-educacion-superior/))

[PLIEGO NACIONAL DE EXIGENCIAS UNEES](https://www.scribd.com/document/389624106/PLIEGO-NACIONAL-DE-EXIGENCIAS-UNEES#from_embed "View PLIEGO NACIONAL DE EXIGENCIAS UNEES on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_35347" class="scribd_iframe_embed" title="PLIEGO NACIONAL DE EXIGENCIAS UNEES" src="https://www.scribd.com/embeds/389624106/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-F7AOoPl8GJDVs1RhDULy&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_28939492" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28939492_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
