Title: Paramilitares de AGC  amenazan con limpieza social en Ciudad Bolívar
Date: 2020-04-03 23:16
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: AGC, Amenazas, Ciudad Bollívar
Slug: paramilitares-de-agc-amenazan-con-limpieza-social-en-ciudad-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/AGC-amenazan-en-Ciudad-Bolívar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Bucaro B {#foto-bucaro-b .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Habitantes de los barrios de Bella Flor la Torre y Tierra Nueva, en Ciudad Bolívar (Distrito Capital) denuncian que hace tres noches se están presentando hombres armados identificados como parte de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) que están amenazando a la población, y distribuyen panfletos alertando sobre una limpieza social.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Con disparos al aire pretenderían amedrentar a la gente y mo**strar su poder

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Comité de Solidaridad con los Presos Políticos [(CSPP)](http://www.comitedesolidaridad.com/es/content/amenazas-de-grupos-armados-en-ciudad-bol%C3%ADvar) hizo pública la denuncia este viernes, señalando que la noche del jueves cerca de diez hombres armados que se identificaron como parte de las autodenominadas AGC estuvieron en horas de la noche intimidando a las personas que habitan en los barrios Bella Flor la Torre, Tierra Nueva y Guabal en Ciudad Bolívar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El edil de la localidad Cristian Robayo recorrió este viernes el lugar, y precisó que según le contó la comunidad, eran aproximadamente ocho hombres vestidos de camuflado y con el rostro tapado que estuvieron entre las ocho y diez de la noche intimidando a los habitantes de la zona. Robayo declaró que según la comunidad, la del jueves es la tercera noche en que ocurren este tipo de hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la denuncia del Comité se añade que los habitantes el día de ayer escucharon disparos mientras los armados estaban en la zona. El edil sostuvo que presumiblemente quienes hicieron los disparos fueron los violentos, como "una forma de amedrentar a la comunidad y mostrar su poder".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Hay panfletos y hombres armados en Ciudad Bolívar, pero la Policía dice no saber nada**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El CSPP manifiesta que tras escuchar los disparos, la comunidad llamó a la policía, pero no hubo respuesta al llamado. Por su parte, Robayo aseveró que desde hace días se ven panfletos firmados por las AGC en la zona alta de la localidad anunciando una limpieza social, y en diálogo con el Comandante de Policía de la zona señala que no tiene información de este hecho.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El edil añadió que aunque el Comandante dijo que se habían hecho patrullajes, varias personas de la comunidad ratificaron la versión entregada al Comité. Por esta razón, Robayo recordó que el territorio está abandonado por la pobreza, hay un problema de venta de predios y ahora de intimidación armada, ante lo que se hace necesario la presencia integral de la Alcaldía Mayor de Bogotá.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La violencia a la que se enfrenta el sur de Bogotá**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Recientemente Contagio Radio habló con Heiner Gaitán, concejal de Soacha sobre las denuncias de reclutamientos forzados en el muncipio. Gaitán dijo en su momento que se conocía de la presencia de grupos armados en la Comuna 3, 4 (Altos de Cazucá) y 6. (Le puede interesar: ["Reclutamiento forzado, pobreza e ineficacia oficial, las tres amenazas para jóvenes de Soacha"](https://archivo.contagioradio.com/reclutamiento-forzado-pobreza-e-ineficacia-oficial-las-tres-amenazas-para-jovenes-de-soacha/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, declaró que la modalidad del crimen era la tercerización de las tareas ilegales, de tal forma que grupos como las AGC 'contratan' pandillas y bandas de la zona para operar. Gaitán explicaba que por esta razón no se veían en el territorio hombres de camuflado ni armados, aunque sí actuaran a nombre de estructuras como Los Rastrojos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Robayo coincide con Gaitán al señalar que en la zona de Tres Reyes, en límites con Santo Domingo y Cazucá también se han recibido denuncias sobre presencia de grupos armados ilegales, y en la localidad han amenazado a un exedil, a él mismo y a la edilesa Luceris Segura, por lo que aseveró que "este tema es más sistemático de lo que se cree".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para concluir, el edil solicitó la realización de una audiencia defensorial en la zona para evaluar la respuesta institucional a las distintas alertas tempranas que ha emitido la Defensoría del Pueblo sobre riesgos a la población, buscando que la comunidad allí sea escuchada y se atiendan sus denuncias.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
