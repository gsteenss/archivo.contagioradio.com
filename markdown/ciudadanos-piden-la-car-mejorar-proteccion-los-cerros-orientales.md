Title: Ciudadanos piden a la CAR mejorar protección de los Cerros Orientales
Date: 2019-01-21 11:01
Author: AdminContagio
Category: Ambiente, Nacional
Tags: cerros orientales, Corporación Autónoma Regional de Cundinamarca, Jorge Torres, San Cristóbal, Secretaría Distrital de Ambiente
Slug: ciudadanos-piden-la-car-mejorar-proteccion-los-cerros-orientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/cerros-orientales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 16 ene 2019 

Ciudadanos de la localidad de San Cristobal en Bogotá, junto al concejal Jorge Torres, realizaron un llamado esta semana a la Corporación Autónoma Regional de Cundinamarca (CAR) de mejorar las medidas de prevención y protección de los Cerros Orientales.

Según un comunicado del concejal, los Cerros Orientales, particularmente el sector Bosque de los Alpes de San Cristobal, se ve amenazado por urbanización, construcciones ilegales y recientemente incendios forestales. Este sector, declarado parte de la zona de reserva forestal, tiene gran importancia para el bienestar de los ciudadanos de Bogotá debido a que sus bosques regulan la cantidad y calidad de las agua que proveen de los páramos.

Sin embargo, la Corporación ni la Secretaría Distrital de Ambiente ha implementado las medidas suficientes para proteger efectivamente a los Cerros de estas amenazas. El representante de la Alianza Verde destacó un reciente incendio en los Bosques de los Alpes que arrasó con un hectárea de vegetación después de que Torres realizara una llamada a la Secretaría sobre el riesgo de incendios causados por la circulación constante de personas, el inadecuado manejo de vegetación y la presencia del "Retamo espinoso".

Estos hechos se suman al proceso de urbanización en San Cristobal que Torres denunció a la Secretaría el año pasado. Allí, las autoridades ambientales comenzaron un proceso sancionatorio por la ocupación de cauce, vertimiento directo y tala ilegal de doce árboles sobre la quebrada los Toches. Sin embargo, el concejal manifestó que "aun no se resuelve la situación del predio Bosque de los Alpes en cuanto a la sanción emitida por la entidad."

Frente a estos hechos, además de los efectos ambientales que han causado la construcción de por lo menos cinco megamansiones en los Cerros Orientales, Torres hizo una llamada para que se tomen "medidas más drásticas" y que se defienden estos ecosistemas de "manos irresponsables con el objetivo de cuidarlos."
