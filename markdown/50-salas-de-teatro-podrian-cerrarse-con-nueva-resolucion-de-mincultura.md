Title: 50 salas de teatro podrían cerrarse con nueva resolución de MinCultura
Date: 2016-03-11 18:18
Category: Cultura, Nacional
Tags: mincultura, salas concertadas, teatro colombia
Slug: 50-salas-de-teatro-podrian-cerrarse-con-nueva-resolucion-de-mincultura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Teatro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Teatro Colombia ] 

<iframe src="http://co.ivoox.com/es/player_ek_10783596_2_1.html?data=kpWkmpiZfZehhpywj5WcaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncZakjNjOzsbXb8XZjNnSw9nWs4zk0Mnfh6iXaaK4wtOYxcrWtsLm1MqYxdTSb8%2FpxtvOjdfJt9Dg1sjWj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Patricia Ariza, CCT] 

###### [11 Mar 2016 ] 

De acuerdo con Patricia Ariza, directora de la 'Corporación Colombiana de Teatro', la resolución al programa 'Salas Concertadas' propuesta por el Ministerio de Cultura que incluye una nueva medición determinada por puntajes, podría provocar el cierre de 50 salas de teatro en todo el país, “este cambio en la medición convierte el apoyo en una competencia y lotería, con unos puntajes que no necesariamente están relacionados con la programación artística de una sala, el asunto es que en la convocatoria pasada se quedaron 9 salas por fuera y ahora hay unos puntajes difíciles de alcanzar que seguramente harán que muchas salas queden por fuera”, asegura.

La directora plantea que este cálculo se hizo a través de un estudio de simulación que fue aplicado en varios teatros y que arrojó como resultado que estos índices de medición propuestos por MinCultura dejarían por fuera a un total de 50 salas teatrales. Situación frente a la que el 'Movimiento Teatral' propone generar una mesa de concertación en la que se discutan y acuerden las políticas públicas en materia teatral. “En Colombia se ha vuelto muy difícil que la gente emprenda ese camino de tener un teatro independiente, entonces esto también cierra el camino a que existan más salas. A nosotros no nos interesa que sean poquitas salas con más dinero, sino que sean muchas salas y que se incremente el presupuesto, las salas y los centro culturales son como el pulmón cultural de las ciudades, por esto el programa debe defenderse” afirma Patricia Ariza.

El programa de 'Salas Concertadas' tiene como finalidad democratizar el acceso de todas las personas a los bienes, servicios y manifestaciones de la cultura y el arte, así como velar para que los escenarios independientes cuenten con las condiciones para continuar con la producción de cultura, tarea que cuenta con el apoyo económico que MinCultura otorga a diferentes salas independientes. Sin embargo cada vez es más difícil pertenecer a este programa y los recursos destinados han ido disminuyendo. En la actualidad el aporte del convenio de Salas Concertadas a cada teatro es de \$72.800.000

El 'Movimiento Teatral' emprenderá acciones de movilización para convocar una mesa de concertación con el Ministerio de Cultura, que ya otorgó una prórroga para la presentación de los proyectos al programa Salas Concertadas, lo cual permite a los teatros prepararse para los nuevos puntajes.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
