Title: El poder de los medios masivos de comunicación se desmorona
Date: 2017-05-19 10:33
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Canal RCN, Caracol TV, medios colombianos
Slug: el-poder-de-los-medios-masivos-de-comunicacion-se-desmorona
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/medios-masivos-de-comunicación-se-desmorona1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: expertosenmarca 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 19 May 2017 

[Hay que comenzar aclarando que decir “**medios masivos de información” no significa imaginar un poder etéreo que navega en un mundo ideal que maldecimos** cada vez que observamos esa narración de los hechos que acontecen en Colombia y que es realizada desde posiciones editoriales politizadas pero solapadas con una falsa objetividad.]

[Por el contrario, es importante que al pensar en “medios masivos de información” asociemos el significado real del término, con los dueños de esos medios masivos de comunicación, es decir, con personas de carne y hueso, personas que tienen familias y amigos tan inmensamente ricos como ellos y lo más importante: personas que tienen un pensamiento político y económico muy bien definido.]

[¿Es acaso un crimen que en los medios de información se evidencie una posición editorial? Por supuesto que no. Eso lo reconoce hasta la mismísima derechista y defensora “a muerte” del uribismo, Sra. Claudia Gurisatti.]

[Realmente, el problema es llegar a pretender que una posición editorial es una posición política objetiva, o lo que es peor, pretender convencer de que un medio masivo de comunicación tiene una posición neutral frente a la realidad. Intentar afirmar eso no es algo ilícito, simplemente es una falacia.]

[Ahora bien, los medios de información son sólo objetos, pero sus dueños son de carne y hueso, tienen intereses particulares y concretos, por tanto,]**jamás utilizarán posiciones editoriales en sus propios medios para atentar contra sus propios intereses.**

[Por eso vemos cómo por ejemplo hoy, desde los medios masivos de comunicación colombianos, se habla de las luchas callejeras contra la policía anti motín en Venezuela, vemos cómo elogian al que prepara una bomba molotov en Caracas, cómo le dicen “mujer maravilla” a la que aquí le dirían terrorista o mamerta; asimismo hemos escuchado cómo elogian a los estudiantes que les hacen frente a esas tanqueticas tan tiernas que tiene la guardia en el vecino país. ¡sí! Tanqueticas… mire una del Esmad y compare.  ]

[Dejando atrás ese mamertismo de derecha que impulsa la idea falaz y hueca de que por llamar la atención sobre lo que sucede en Colombia s ignifica que uno es un espía norcoreano o un fervoroso madurista; la verdad es que con el dolor de ver la gravedad de nuestros problemas colombianos uno piensa:]

[¡¡Con qué inmensas ganas de luchar en las calles colombianas vemos esto!! Cuando aquí por ejemplo los indígenas en el Cauca, los estudiantes en las universidades, los profesores en las ciudades, los discapacitados en las plazas, las familias en los barrios de lata y plástico, los campesinos o conductores en las carreteras le hacen frente a las enormes y poderosas tanquetas del Esmad, le hacen frente a su puntería entrenada que busca la cabeza, le hacen frente a sus recalzadas, hasta le hacen frente a su miedo, cuando el mismísimo Esmad se aterroriza cuando la gente se para duro y tiene que pedir corriendo ayuda al ejército para que mate a algún jovencito de 16 años hijo de indígenas por el que los medios masivos no se indignarán, pues están a acostumbrados a la estigmatización, la criminalización o en el mejor de los casos, a una omisión intencional de todo aquello que atente contra el poder que domina Colombia.  ]

[La perspectiva de la importancia de un problema político hoy está definida por dos cosas fundamentalmente. La primera:]*[el que la sufre es el que lucha]*[. La segunda:]*[quien cuenta la historia genera opinión política así no la sufra]*[.]

[Por tanto, desde las cómodas ciudades colombianas, siempre hemos recibido noticias sobre esos “pérfidos indígenas que invaden la propiedad privada y que quieren arruinar a empresarios de bien”, o de esos “encapuchados violentos, ¡que ni serán estudiantes! y se toman universidades durante los tropeles” o de “la inocencia del Esmad que dispersó a los manifestantes para protegerlos” ….  ]

[En Colombia todo lo que se opone a este régimen de falsa democracia, a este Estado secuestrado por el poder económico, todo, absolutamente todo lo que emerja como oposición o resistencia a lo mencionado, termina envuelto en narrativas desde los medios masivos que lo tergiversa, que lo estigmatiza o que lo ignora a conveniencia … ¡es obvio! Dirán muchas y muchos ¡ni tan obvio! Pues las razones que a algunos les sobra para reconocer el engaño, son las mismas que les faltan a muchos y muchas que hoy lo siguen dudando.]

[La luz de esperanza ha aparecido desde donde menos se pensó; las redes sociales contienen tanta basura como riqueza, y parte de esa riqueza constituye un arma directa contra los medios masivos de información. ¿Recuerda a Uribe Noguera? Uno de tantos que ha violado y matado a niñas en este país… pues bien, de no ser por las redes sociales, ni caracol radio, ni caracol T.V ni RCN hubieran profundizado el hecho… dirán que “le doy duro a los medios masivos” pero en mi cabeza retumba la frase que una de las periodistas decía… “un prestigioso arquitecto del que por respeto a la familia nos abstenemos de revelar su identidad, está envuelto en un grave escándalo” …  ]

[Las redes hicieron lo suyo, y a pesar de los modales arribistas de algunos (que se la pasan comiendo humillaciones y vomitando aires de éxito), una muchedumbre casi lo lincha, publicó su nombre, y ni modo… se les cayó la estrategia de encubrimiento.  A lo que voy, es a que el poder de los medios masivos de comunicación lo está desmoronando ese reguero de basura y riqueza que viaja en las redes sociales, una combinación peligrosa que ha minado las bases de esa supuesta “objetividad” que nunca han tenido y en la que ya pocos en Colombia creen.]

[En las redes sociales no dejará de circular basura, no obstante, es en ellas dónde la riqueza de los que no tienen voz, ha podido tener un eco.]

[El poder de los medios masivos se desmorona porque ya existen medios alternativos con reconocimiento social, que tiene enfocada su mirada en la otra cara de la historia, la que no está atada al poder dominante, sumado eso, las redes sociales han funcionado como fuente de duda, desasosiego e incluso de desespero para los que creían que por ejemplo Caracol y Rcn eran un relato de la verdad…. Ahora sabemos que no es así, ahora sabemos que no mienten, sino que ponen la cámara donde les conviene… cuentan lo que les convienen… y a quienes le conviene.]

[¿Qué les queda a los movimientos sociales? ¿batallar por establecer una única verdad? No. Diría mejor, que batallar para que su voz siga oponiéndose, no por demagogia, ¡¡sino porque existe!! porque es real y lo que pasa en este país no merece que sea contado solo por una voz… esa antigua voz mediática que hoy se desmorona poco a poco.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
