Title: Las Penas Alternativas una respuesta a la crisis carcelaria
Date: 2020-05-19 16:05
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: #Cárcel #covid19 #IvánDuque #contagio #Crisis carcelaria, crisis carcelaria, Crisis carcelaria colombia, Expreso Libertad
Slug: las-penas-alternativas-una-respuesta-a-la-crisis-carcelaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/1528458096_425274_1528458400_noticia_normal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: el país {#foto-por-el-país .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras el cumplimiento de más de dos meses del **Decreto 546** de excarcelación ante la pandemia del Covid 19, el **INPEC** confirma que solo han salido 572 personas. Razón por la cual las organizaciones defensoras de derechos humanos exigen que se revise el sistema penitenciario y se recurra a penas alternativas para garantizar la vida de la población privada de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad** el profesor y abogado Camilo Villegas Rondón, especialista en instituciones jurídico penales y la abogada Gloria Silva, exponen la importancia de poner en marcha las penas alternativas en Colombia, no solo para la descongestión de las cárceles en medio de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sino principalmente para dar respuesta a acciones efectivas que humanicen el sistema penitenciario y que pongan en consideración el concepto de "castigo" que existe en la sociedad colombiana.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/914516485662228/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/914516485662228/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Ver mas: programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
