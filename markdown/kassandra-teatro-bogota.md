Title: "Kassandra" un monólogo épico sobre los inmigrantes
Date: 2017-11-01 12:23
Category: eventos
Tags: Bogotá, teatro
Slug: kassandra-teatro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/image001-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa La Maldita Vanidad 

###### 1 Nov 2017 

Del 2 al 12 de noviembre la Compañía de teatro la Maldita Vanidad presenta “ Kassandra ”, una adaptación de la obra creada por el dramaturgo uruguayo Sergio Blanco, bajo la dirección de Jorge Hugo Marín. Un monólogo sobre los inmigrantes, que ha sido versionado en más de doce oportunidades alrededor del mundo.

Ella Margarita Becerra encarna el personaje que tiene como origen la mítica figura de Casandra, un monólogo en el cual la heroína troyana en su precario inglés de migrante contemporánea, que vende productos de contrabando, nos viene a contar su historia y a desmitificar su propio mito.

La obra busca que el espectador viva de cerca la historia y las vivencias de esta mujer; para esto Jorge Hugo Marín y su equipo han creado un espacio sin tiempo, un lugar de tránsito, solitario y lúgubre, donde la necesidad de comunicación rebasa las fronteras “Kassandra maneja un lenguaje de supervivencia y aunque toda la obra es en inglés, es un inglés tan precario que cualquier persona lo entiende.

<iframe src="https://www.youtube.com/embed/oKFIpRXAwkw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Es parte del éxito mundial que ha tenido la obra. De la necesidad comunicarse surge esta historia” “También es un mensaje sobre la migración, sobre esas personas que se van de su país por necesidad buscando el progreso, pero que sufren mucho en esos nuevo lugares” afirma el director.

Es así como través de su historia pasada, Kassandra nos cuenta nuestra historia presente: nuestras insoportables guerras, nuestras infortunadas Troyas, nuestros exilios inevitables, nuestros Aquiles de alta tecnología, nuestros Héctores martirizados, nuestras nuevas cartografías, nuestras Helenas de lujo y nuestras barbaries impunemente mediatizadas.

Esta pieza, estrenada en el 2016, ha sido acogida con gran éxito por parte del público y se ha presentado en festivales como: Festival internacional de Nuevo León, México, Festival Brújula al sur en Cali, Festival de Manizales 2017, Festival colombiano de teatro en Medellín y el Festival Mujeres en escena. (Le puede interesar: [Las artes escénicas tejen redes en la Plataforma Conecta](https://archivo.contagioradio.com/plataforma-conecta-2017/))

Del 2 al 12 de noviembre, Jueves a sábado a las 8:00 p.m. y Domingo a las 6:30 p.m.  
Casa la Maldita Vanidad carrera 19 No 45ª – 17 Palermo  
Bono de Apoyo: \$30.000 general \$20000 estudiantes  
50% de descuento con la membresía Maldita Vanidad  
Reservas en: 6055312 - 3192487560

###### Con información de Prensa La Maldita Vanidad 
