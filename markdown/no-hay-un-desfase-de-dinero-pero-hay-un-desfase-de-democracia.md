Title: En revocatoria “No hay un desfase de dinero, pero si un desfase democrático”
Date: 2018-08-24 15:55
Category: Nacional
Tags: Bogotá, Peñalosa, revocatoria
Slug: no-hay-un-desfase-de-dinero-pero-hay-un-desfase-de-democracia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/peñalosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Ago 2018] 

Frente a la decisión del Consejo Nacional Electoral de archivar la revocatoria del alcalde Enrique Peñalosa y sancionar a **Gustavo Merchán**, uno de sus principales impulsores por supuestas irregularidades en la financiación del comité promotor; decisión frente a la cual Merchán asegura que "**no hay un desfase de dinero, pero hay un desfase democrático".**

Aunque para el momento de la entrevista el integrante de SINTRATELÉFONOS afirmó que no había sido notificado formalmente sobre algún tipo de sanción, señaló que este tipo de decisiones se ajustan a la forma en que **se han usado estos mecanismos institucionales en contra de procesos de participación democrática** por parte de sectores políticos.

En la ponencia del magistrado Emiliano Rivera, opositor al proceso revocatorio, propone al CNE **suspender la revocatoria al alcalde electo por irregularidades en los estados financieros del movimiento “Unidos revocamos a Peñalosa”**, lo que acarrearía además **una multa para Merchán por 26 millones de pesos** y contempla investigar al Polo Democrático.

Aquí se cometieron algunos errores en la radicación de cuentas y facturas 3 o 4 días después de presentarse el gasto, lo que  vulneran las cuentas, asegura el promotor e indica que incluso 1800 folios fueron enviados la Fiscalía concluyendo que, a pesar de algunos errores de forma, **no se superó el limite de financiación del 10% que puede otorgar el Sindicato, pues solo se uso el 9.5% de lo aportado por SINTRATELÉFONOS y 5.6% de inversión externa**.

Merchán sostiene que este tipo de disposiciones son un entorpecimiento del proceso, y asegura que el rescatar la tutela por parte de la Corte en la que se alegaba que a Peñalosa se le vulneraba el derecho a legislar, parte de una consideración errónea, pues no se busca cercenar la posibilidad de gobierno sino **defender el derecho al uso de los mecanismos de participación ciudadana**.

“Me he sentido perseguido por Emiliano Rivera y sus seguidores”, pues se cercena la voz del ciudadano que esta inconforme con el mandato del Alcalde Mayor de Bogotá que esta dejando la ciudad endeudada e insegura asegura Merchán, y agrega que **el magistrado dilato lo que más pudo los trámites de revocatoria, hasta llevarlo al limite de tiempo para que no pudiera realizarse**.

<iframe id="audio_28068709" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28068709_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
