Title: Necropolítica en tiempo de pandemia
Date: 2020-04-01 16:34
Author: AdminContagio
Category: DDHH, yoreporto
Tags: Achille Mbembe, carceles de colombia, Covid-19, decreto 417, decreto 444, genocidio, necropolítica, pandemia, politica
Slug: necropolitica-en-tiempo-de-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/BHIXQDXG2CBYQXXPSDLA7LIHDU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#717c85"} -->

###### Imagen tomada de El País 

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### \*De los Decretos 417 y 444 de Marzo de 2020 o de la Política y el Poder de Muerte

<!-- /wp:heading -->

<!-- wp:heading {"level":6,"customTextColor":"#4e4848"} -->

###### **Por: John Freddy Caicedo-Álvarez^[\[1\]](https://archivo.contagioradio.com/wp-admin/post.php?post=82713&action=edit#_ftn1)^ y Jenny Marlody Arias Durán[^\[2\]^](https://archivo.contagioradio.com/wp-admin/post.php?post=82713&action=edit#_ftn2).** 

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->

*Debemos pasar del aislamiento preventivo obligatorio, al tejido organizativo preventivo emancipatorio.*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### La Masacre en **La Modelo** en Bogotá

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la Cárcel La Modelo de Bogotá se presentaron fuertes protestas reclamando condiciones dignas para la población detenida en los centros carcelarios de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A propósito de las protestas, calificadas como intento de fuga por el Gobierno Nacional, unos minutos después de las ocho de la noche del **23 de marzo**, el periódico El Tiempo informó que fueron identificados los **23 presos fallecidos,** indica además que **hubo 83 heridos**, todos en la cárcel La Modelo. El **25 de marzo**, no habían pasado ni cien horas, cuando ya en el portal de la principal revista nacional y en los portales de los dos periódicos nacionales de mayor circulación, no había ni una mención directa a dicha masacre, apenas si se conservaba en sus titulares de segundo y tercer orden, alguna noticia al respecto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Días después han reaparecido los hechos en algunos titulares, sin denunciar con la Masacre que cometió el Estado de Colombia en las instalaciones de la Cárcel La Modelo de Bogotá. [Achille Mbembe](https://aphuuruguay.files.wordpress.com/2014/08/achille-mbembe-necropolc3adtica-seguido-de-sobre-el-gobierno-privado-indirecto.pdf) señala que la racionalidad occidental se encarna en la síntesis entre la masacre y la burocracia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Para Mbembe, la política de muerte y el poder de muerte ejercen el poder de matar, se endilgan el derecho de matar y de decidir a quién dejar vivir.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La política como un trabajo de muerte, expresa su soberanía como derecho de matar, invocando la excepción, la urgencia y la noción ficcionalizada del enemigo. **El Estado de Emergencia** es el escenario en el cual funciona este sistema de muerte, ejerciendo su poder sobre las personas condenadas a la masacre.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### No importa quiénes murieron.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la Cárcel La Modelo fueron Pedro, Jesús, Cristian, Jhon, Daniel, Miguel, Freddy, Edgar, Milton, Cirus, Diego, Andrés, Michael, Brandon, Euclides, Yeison, Campo, Diego, Joaquín, Henry, Eberzon, José y Daniel.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No importa si más allá de los muros de la cárcel La Modelo, lloraron en silencio a sus muertos, aplastadas por el ruido de la avalancha de la Crisis de Salud Pública provocada por el COVID-19, las familias Arévalo Rocha, Gómez Rojas, Gonzáles Linares, Peña Jiménez, Gonzáles Espitia, Lemos Roa, Díaz Rodríguez, Gómez Romero, Rodríguez Álvarez, Rojas Ospina, Rodriguez Peña, Melo Sánchez, Melo Cubillos, Avendaño Quevedo, Pérez Espinosa, Galvis Forero, Carranza Sanabria, Rodríguez Fuentes, Mejía Aguirre, Gómez Méndez, Palomino Hernández, Hernández Páez y Carabaño Plazas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No importan los muertos ni sus familias sufriendo, sus muertes se legitiman porque se dieron las condiciones de aceptabilidad de la matanza: eran enemigos de la Ley, de la Salud Pública, eran parte de un movimiento organizado en todo el país para fugarse de las cárceles, eran una amenaza para la sociedad y unos oportunistas, que pretendían aprovecharse de la situación generada por el coronavirus para violar nuevamente la Ley, actos típicos de la aplicación del famoso *Derecho Penal del enemigo* que se caracteriza por ser un sistema mediante el cual el Estado justifica sus conductas delictivas, asesinas, de abuso y de ejercicio del poder a partir de la construcción de un discurso basado en el peligrosismo de su “enemigo”, precisamente cuando no existe tal amenaza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, cuando un Estado legitima el ejercicio de un Derecho Penal del enemigo va en contravía de toda garantía a la seguridad jurídica del ciudadano, de los principios democráticos y de derecho, esto se ve reflejado en su política criminal, penitenciaria y carcelaria, entonces ese “enemigo” es considerado: “una amenaza constante”, “representa una peligrosidad fáctica” y, lo más importante “es quien no se deja reconducir como ciudadano”, en consecuencia: “debe ser castigado o reducido”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aquí, la comisión del delito y la finalidad de la pena como parte del castigo pasan a un segundo plano, el ejercicio de éste tipo de Derecho está mediado por intereses económicos, políticos, filosóficos y culturales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Pueden citarse algunos ejemplos:

<!-- /wp:heading -->

<!-- wp:list {"ordered":true} -->

1.  **La invasión de Irak** por parte de Estados Unidos de Norte América en coalición con los países de Reino Unido, Australia y Polonia, del **20 de marzo al 1 de mayo del 2003**, la justificación principal: encontrar las armas de destrucción masiva que tenía Irak en su poder, pero nunca fueron encontradas las armas ni lograron aportar evidencia alguna de su existencia, resultado: crisis humanitaria, violación de Derechos Humanos, víctimas, muertes masivas, destrucción del patrimonio material e inmaterial, etc.
2.  **La creación de cárceles**, en principio ocultas, por parte de los Estados Unidos de Norte América en diversos lugares del mundo con la justificación de “controlar el terror”, siendo una de ellas la Cárcel de Guantánamo. Allí, mantuvieron en prisión a los cinco supuestos musulmanes que participaron del atentado a las Torres Gemelas el **11 de septiembre del 2001**, por su alta “peligrosidad”, pero quienes por más de 18 años no han sido judicializados aún, vulnerándoles sus garantías procesales y de Derechos Humanos.

<!-- /wp:list -->

<!-- wp:paragraph -->

Nada lejano es lo sucedido a las 23 personas privadas de la libertad que fueron ultimadas en la Cárcel La Modelo de Bogotá que, por las circunstancias en las que se dieron los hechos, al parecer, fue por mano propia del Instituto Nacional Penitenciario y Carcelario-**INPEC**, pero a la situación le dieron un manejo distinto desde todos los dispositivos informativos, como si se hubiese dado con ocasión a un motín organizado por una “fuerte estructura de insurrección carcelaria”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Según Mbembe, en Estados de Excepción, la enemistad de tan peligrosos enemigos se convierte en la base normativa del derecho de matar.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Efectivamente el Estado mató, justificó la masacre y los medios y la sociedad en su conjunto miraron la cortina del día y escucharon el ruido de la pandemia. Pandemia que en todo caso, hasta ahora es menos mortífera que la actuación del Estado de Colombia en su intervención en la cárcel La Modelo de Bogotá, durante la noche del 21 de marzo de 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre las víctimas de la Masacre, las más jóvenes tenían 21 años, la persona de mayor edad tenía 46 años. La condición de éstos que debían morir, es similar a la que Mbembe detalla para el esclavizado en la Colonia, a juicio de las “mayorías” los presos eran personas sin hogar, sin derechos sobre su propio cuerpo, sin estatus político.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El ejercicio de la dominación sobre ellas fue absoluto.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Nunca se aceptó, ni se aceptará que estaban reclamando derechos humanos, se sembró a precio de sangre su condición de ser gentes de mala fama que desde un lugar de mala fama, estaban ejecutando un plan de fuga. Por ello, “legítimamente” en el marco de esa relación, el verdugo ejecutó la sentencia de muerte.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### La Cárcel Extendida

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sobre los hechos de la masacre en la cárcel La Modelo de Bogotá, con los cuerpos aún tibios y olvidados en el subsuelo de quien sabe qué lugar de Colombia donde fueron sepultados, las luces del espectáculo se enfocan en el **Decreto 444 del 21 de marzo de 2020** por medio del cual se creó el Fondo de Mitigación de Emergencias, que a su vez se soporta en el **Decreto 417 del 17 de Marzo de 2020**, con el que la Presidencia de la República emitió la Declaración del **Estado de Emergencia Económica, Social y Ecológica**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la mima semana, se dio la Declaración del Estado de Emergencia Carcelaria, el que según los presos, costó la vida de 23 personas y la integridad de 83 más, que resultaron heridas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las dos declaraciones - Declaración del Estado de Emergencia Económica, Social y Ecológica y Declaración del Estado de Emergencia Carcelaria - trasladan la prisión a la totalidad de la geografía nacional, la segunda para los presos y presas que seguirán pagando sus sentencias en sus casas; la primera hará del territorio nacional donde viven los pobres y los miserables, una cárcel.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es la cárcel extendida a la geografía de Colombia, donde las familias empobrecidas y miserables pagan su crimen de pobreza aguantando hambre, miedo y angustia, sean niños y niñas, personas adultas y adultas mayores.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Se puede argumentar con suficiente razón, que la pandemia amerita la cuarentena o aislamiento preventivo obligatorio. Claro que sí y no se pretende discutir la pertinencia de esa acción de salud pública. Lo que se discute en éstas líneas son las condiciones en que dicha decisión toma a la población colombiana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En primer lugar**, partiendo de afirmar que la cuarentena o aislamiento preventivo obligatorio se funda en un argumento falso, en una razón mentirosa: el supuesto de que es buena para toda la población por igual.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es desde estos supuestos que es plausible identificar la presencia de dos tipos de violencia en las que se recoge un discurso en virtud del bien común y el bienestar para todos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de ellas es la violencia normativa de tipo simbólico, pues, el fundamento de los Decretos en mención lo que evidencia es una clara reproducción del ejercicio de dominación, de la preservación de la desigualdad y de una total discriminación en las relaciones sociales, esta vez, desde la legalidad , naturalizando dicho ejercicio a través del Estado como institución de control social, esto a la par de una violencia institucional de parte de los agentes del Estado, en primera instancia del Presidente de la República, al abusar del ejercicio de poder a partir de la Declaración del Estado de Emergencia, por ejemplo, en el favorecimiento económico a ciertos sectores de la economía, específicamente, grandes empresas y entidades financieras, respaldándose en una crisis de Salud Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A continuación, se señalan específicamente los principales preceptos normativos: **- Decreto 417 del 17 de marzo del 2020**, (por medio del cual se Declara el estado de Emergencia económica, social y ecológica en todo el Territorio Nacional.) en su presupuesto valorativo, estableció que:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### “sumado a los efectos económicos negativos que se han venido evidenciando en la última semana, es un hecho que además, de ser una grave calamidad pública, constituye en una grave afectación al orden económico y social del país que justifica la declaratoria del Estado de Emergencia Económica y Social, (…)”

<!-- /wp:heading -->

<!-- wp:paragraph -->

Éste Decreto le dio potestad al Presidente de la República para dictar Decretos con fuerza de Ley para mitigar la crisis por **COVID-19**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

- **El Decreto 444 del 21 de marzo del 2020**, es uno de los Decretos legislativos resultado de la declaración del Estado de Emergencia Económica, Social y Ecológica, pues el Presidente de la República crea el Fondo de Mitigación de Emergencias-**FOME** y dicta disposiciones sobre el manejo de los recursos, es aquí donde empiezan a utilizar la expresión “viabilizados”, para referirse al uso de recurso públicos como los provenientes de: el Fondo de Ahorro y Estabilización-**FAE** del Sistema General de Regalías y del **FONPET**-Fondo Nacional de Pensiones de las Entidades Territoriales, a título de préstamo, teniendo como respaldo a las Entidades Financieras y el Fondo de reserva de para la Estabilización de la Cartera Hipotecaria **FRECH**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Considerando lo anterior, es posible evidenciar que los Decretos de la referencia emitidos dentro del Estado de Emergencia económica, social y ecológica, están direccionados a proteger los principales capitales de los sectores de la economía colombiana y dejar a su merced la administración de los recursos, con la máscara de la atención a ésta grave crisis de salud pública, se privilegia directa e indirectamente a los poderes económicos y políticos hegemónicos, y en términos concretos, para la mayoría de la población la posibilidad de acceder a dichos beneficios está vedada, siendo esto una muestra de que las clases sociales y su lucha, aún subsisten porque están presentes y latentes, en lo estructural como el derecho a la vida y, en lo coyuntural, como el derecho al alimento del día.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### El Genocidio por Otros Medios

<!-- /wp:heading -->

<!-- wp:paragraph -->

La cuarentena o aislamiento preventivo obligatorio no toma a toda la población en las mismas condiciones. Para comprenderlo, cobra importancia la categoría de racismo estructural que ha propuesto [Francia Márquez](https://twitter.com/FranciaMarquezM), en relación a las profundas realidades de desigualdad y segregación en las cuales viven las comunidades afrocolombianas, negras, palenqueras y raizales, las comunidades indígenas, las comunidades campesinas, los sectores populares de las áreas urbanas y, actualmente, la población migrante pobre de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mbembe, recurriendo a Foucault y Arendt, aborda la política racista, a propósito de la relación entre muerte y política en sistemas que solo pueden funcionar en estado de emergencia. Con Foucault, en tanto el racismo es una tecnología que pretende soportar el ejercicio del biopoder como ejercicio del viejo derecho soberano de matar, expone que la segregación como separación de las personas que deben vivir y las personas que deben morir, es una división soportada en el racismo, pues constituye la distribución de la especie humana en diferentes grupos, la subdivisión de la población en grupos y el establecimiento de la ruptura biológica entre unos y otros.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En Colombia matar de hambre, de sed, de enfermedades curables y a bala, motosierra y horno crematorio a indígenas, afrocolombianos, campesinos y habitantes de barrios pobres “no es grave”, ya hace parte de la cotidianidad y el genocidio en curso, que se estaba volviendo un problema diplomático de gran calado y que hoy apenas si tiene eco bajo los gritos del miedo a la pandemia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mbembe apunta que la raza está presente en las relaciones sociales como una sombra siempre presente en el pensamiento y prácticas políticas occidentales, estableciendo la condición de inhumanidad de los pueblos que deben ser dominados. Con Arendt concluye que en lo político de la sociedad occidental, la raza está ligada a la política de la muerte, como claramente sucede en la lógica de la política paramilitar del Estado de Colombia que en el 2019, significó el asesinato de **250 líderes y lideresas**, 23 de ellas en el mes de diciembre, sin vislumbrarse acciones reales de prevención y protección.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si cruzamos esta lectura con la denuncia de Francia Márquez del racismo estructural, latente y galopante en Colombia, la aplicación del Estado de Emergencia Económica, Social y Ecológica, será el ejercicio de una economía de la muerte, cuya función será regular la distribución de muerte, visibilizando las funciones mortíferas del Estado en un ambiente de generación de las condiciones de aceptabilidad de la matanza.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### No murieron por culpa del Estado, ni del Sistema, murieron por su culpa se dirá: no se lavaron las manos, no se quedaron en casa como se les recomendó, intentaron fugarse del aislamiento.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Nada importará si se presentaron a las plazas de los centros de administración a gritar que tenían hambre. Para el caso de Colombia, con un Estado históricamente terrorista y mafioso, el ejercicio del Estado Nazi es potencialmente alto durante la vigencia del Estado de Emergencia Económica, Social y Ecológica. Estado nazi cuya gestión de la protección y cultivo de la vida, opera en articulación con el derecho soberano de matar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Escribe Mbembe, el Estado nazi se expresa como fusión completa de guerra y política, racismo, homicidio y suicidio, este último, en la coyuntura actual muy probablemente puede trascender en culpar a las personas infectadas de su propia muerte y de calificarles como criminales que pusieron en riesgo a la población escogida para vivir. Criminales que con “su cuerpo convertido en arma”, se fugaron de sus ranchos de lata a regar el *coronavirus* más allá del territorio en el que tenían autorizado ir muriendo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El **Decreto 444 de 2020** se legitima en la necesidad de conjurar la crisis, impedir la extensión de sus efectos y en contribuir a enfrentar las consecuencias adversas en lo económico y social, en ese orden: lo económico y lo social, con el Capital priorizado. Con un nombre más amigable, es la puesta en marcha del Estado de Excepción y el Estado de Sitio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La caracterización de la situación, la justificación de las decisiones y la realización de las acciones están apoyadas en el miedo. ¿Por qué tenemos miedo? Claramente es una crisis gravísima, pero, si las condiciones sociales, políticas y económicas de Colombia estuviesen asentadas en la justicia, la democracia, la libertad y en la armonía con la Madre Tierra, ¿tendríamos el mismo miedo? Claro que no.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La injusticia estructural reproduce el miedo aritméticamente y valida la política de muerte y el poder de muerte y su ejercicio del poder de matar, su derecho de matar y de decidir a quién dejar vivir.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este terror al virus o a la extensión de la pandemia han colocado en tensión los relatos sobre el poder sin límites de la razón humana, tanto como de los relatos de la dominación y de la emancipación. Nadie parece tener una respuesta. Las utopías están enredadas en las barbas de **Marx**, de **Freire**, de **Freud**, últimamente en los cabellos desordenados de **Zizek** y en los bien cuidados de **Byung-Chul Han**: que si el fin del capitalismo, que si la continuidad del capitalismo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Mientras las utopías discuten azarosas y azaradas sobre el quehacer, la dominación repunta hegemónica e impone la distopía.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta recuperación de la iniciativa de la extrema derecha en el gobierno, se convierte en una reivindicación a los postulados de **Thomas Malthus** cuando propuso en su ensayo sobre *el principio de la población* una visión económica sobre las formas de control tanto de la natalidad como de los índices de mortandad, como la base de una economía sostenible que, más allá del control natural, los Estados deberían tener participación activa en ésta en pro de la sostenibilidad de su modelo económico, en términos del espacio, la alimentación, las necesidades e intereses posibles por satisfacer. En ese sentido, podría construirse un discurso que valide un tipo de política económica de muerte en la que se decida quién vive o quién no vive para el bienestar general, nada distante de la realidad que hoy está viviendo gran parte de la población colombiana, como es el caso de la población privada de la libertad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El Estado y el gobierno nacional de Colombia de un momento a otro, recuperaron el aliento.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Un presidente torpe, errático, lento en sus decisiones, pero persistente, se hace con el control total de la población, mientras las administraciones locales, muchas de ellas más lúcidas y seguras, poco pueden hacer ante el establecimiento del Apartheid COVID-19. Este Apartheid COVID-19 cumple perfectamente con las características que Mbembe relaciona para éstos regímenes, esto es, aquellos en los cuales se expresan claramente las relaciones entre vida y muerte, políticas de la crueldad y símbolos del sacrilegio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bajo las lógicas del Estado de Emergencia Económica, Social y Ecológica, en una sociedad racista, patriarcal, heterosexista, clasista y crecientemente xenofóbica como la de Colombia, la concatenación del biopoder, el estado de excepción y el estado de sitio, fácilmente se está convirtiendo en procesos de selección, prohibiciones, control extremo y exterminio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las buenas intenciones promulgadas en las pantallas de televisores, celulares y ordenadores devendrán en sometimiento de los cuerpos, reglamentaciones médicas, darwinismo social, teorías sobre la superioridad biológica y genética y de degeneración racial. En medio de tan macabro espectáculo, los y las habitantes de la calle, la población carcelaria, las personas que trabajan en la prostitución, las familias migrantes pobres y un inmenso número de comunidades urbanas y rurales en la pobreza y miseria, pasan a ser invisibles de la solución, aunque si oportunas justificaciones del inmenso negocio que abrió el **Decreto 444 de 2020**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Territorializando la Muerte y la Vida

<!-- /wp:heading -->

<!-- wp:paragraph -->

¿Dónde se llevarán a cabo estas lógicas de Apartheid? Ahí en donde viven las personas, familias y comunidades sumidas en la pobreza y la miseria. Las comunidades de los más ricos están seguras, entre otras cuestiones, por tres razones:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   A través de uno de sus partidos de extrema derecha, tienen el control del Estado;

<!-- /wp:list -->

<!-- wp:list -->

-   Legislaron desde el primer momento en función de las compañías de mercenarios, de los bancos, de los gremios de la ganadería, la agroindustria, la industria, el comercio y las transnacionales;

<!-- /wp:list -->

<!-- wp:list -->

-   Por *casualidad* afortunada para ellas, lograron desmovilizar al grueso del sector armado de la insurgencia, a la vez, tienen en una Isla del Caribe a la comandancia de la insurgencia activa, esperando la reapertura de las negociaciones de paz.

<!-- /wp:list -->

<!-- wp:paragraph -->

Pero no sólo estas tres razones, además están las condiciones generadas por medio del **Decreto 444 de 2020**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un negocio en el que claramente el Estado dispone toda la capacidad pública central y de las entidades territoriales a favor del sector financiero y las empresas privadas y mixtas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La lectura pausada del **Decreto 444 de 2020**, va exponiendo línea tras línea las disposición del presupuesto y de recursos adicionales para fortalecer el sistema de salud privatizado, el otorgamiento de créditos con condiciones de ganga, la participación en los mercados de deuda, el énfasis en restablecimiento de las relaciones crediticias de los hogares y las empresas, la participación del Estado en operaciones accionarias y/o financiación de empresas privadas o mixtas, el desarrollo de operaciones crediticias a favor de empresas privadas o mixtas, como operaciones monetarias, cambiarias y de mercado en atención a sus necesidades de adicionales de recursos, la ejecución de instrumentos y contratos, la transferencia de valores y depósitos a plazo, la emisión de instrumentos de capital o deuda para empresas privadas y mixtas, la provisión directa de financiamiento a empresas privadas y mixtas, la ejecución de recursos por las autopistas virtuales el sistema financiero privado, el desarrollo de operaciones de apoyo de liquidez transitoria al sector financiero, la recepción de títulos representativos de cartera comercial, de consumo y/o leasing financiero del sector financiero y de empresas privadas y mixtas, la emisión de pagarés emitidos por la Nación para respaldar al sector privado, la realización del conjunto de operaciones descritas en dólares de los Estados Unidos de América.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un conjunto de procesos que en términos de créditos estarán para su trámite y aprobación exceptuados del régimen de autorizaciones del crédito público, es decir, desprovistos de control alguno; unos capitales que el Estado central, sin permiso de los gobiernos departamentales y locales, se apropia por auto préstamo para entregar al sistema financiero y a las grandes empresas privadas y mixtas. Recurso que devolverá a una tasa de interés del 0% y que incluye recursos de privatizaciones y sus rendimientos, capitalizaciones y sus rendimientos y otros. Recursos para inversiones que se harán a toda costa, aunque al ser efectuadas o realizadas evidencien probabilidades mayores de resultados financieros adversos, rendimientos iguales a cero o negativos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recursos del ahorro de los colombianos y colombianas, de los impuestos que pagan los colombianos y colombianas, recursos de los bienes de los colombianos y colombianas obtenidos vía privatización, de rendimientos de capital y de inversiones, al servicio del gran capital, porque a las **PYMES** y a las familias empobrecidas y excluidas en la miseria, les llegarán unos pocos créditos, unos cuantos bonos y mercados de menor cuantía, que en todo caso, pasaran por el control del sistema financiero privado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Gobierno, como brazo ejecutor efectivo del Estado de Colombia, ejerce su soberanía, entendida ésta, dice Mbembe citando a Schmitt, como el ejercicio del poder de decidir el Estado de Excepción.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Como en la colonia, el gobierno en el Estado de Emergencia Económica, Social y Ecológica es el ejercicio del poder al margen de la ley, que además tiene en la guerra el rostro de la paz.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Varias personalidades expertas en el campo constitucional lamentan la falta de control, la imposibilidad pareciera, para las más altas Cortes del poder judicial y constitucional, de intervenir y observar con lupa crítica la entrada de lleno del conjunto de disposiciones de política pública dictadas por el poder ejecutivo encabezado por el presidente Iván Duque.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La propaganda sobre la pandemia, así como la amenaza real del COVID-19, hacen prácticamente imposible actuar bajo los tiempos del control constitucional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Simultáneamente, las ciudades, los centros poblados y el territorio en general, quedan en manos de las fuerzas militares, incluida la policía y las agencias de seguridad privada. La libertad se mantiene legalmente sólo dentro de las casas, el derecho a la movilidad se mantiene únicamente para una persona por familia y para las excepciones en aquellos renglones de la administración y la economía que son imprescindibles para el funcionamiento de las disposiciones del Estado de Emergencia Económica, Social y Ecológica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Los demás derechos humanos se desdibujan entre lo virtual y la pérdida legalizada de los mismos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Estado de Colombia a cargo del poder central del presidente Duque, establece las condiciones de la guerra contra el virus y ordena jurídicamente su legitimidad de Estado en estado de guerra, con el derecho de tomar vidas como las de cárcel La Modelo de Bogotá y como empieza a pasar ante las protestas de los hambrientos que violan la cuarentena, pues colocan en riesgo su ejercicio de gobierno en estado de emergencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Su derecho de matar o de tomar vidas, está eficazmente fundado en su relegitimada hegemonía sobre el territorio, porque sus formas de matar son civilizadas y porque la masacre tiene el fin de contribuir al objetivo racional de civilizar a los inhumados y responder a la necesidad de conjurar la crisis, impedir la extensión de sus efectos y de contribuir a enfrentar las consecuencias adversas en lo económico y social, insistimos, en ese orden: lo económico y lo social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La amenaza que significa la pandemia en curso, ha fortalecido al Estado y ha debilitado a sus opositores, que hoy ni siquiera pueden ocupar las calles y plazas de las capitales. Para ello territorializa, determina nuevas fronteras, declara su deber de ocupar regiones específicas, a la vez, como expone Mbembe, declara como legítima su guerra contra los desobedientes, porque así lo permite su supuesta condición de Estado civilizado, modelo de unidad política, principio de organización racional, encarnación de la idea de bien universal y signo de moralidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### El Control Nacional y el Poder Imperial

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pese a reivindicar las características de la soberanía de muerte, el de Colombia no es un Estado soberano, en tal sentido las reflexiones de Mbembe sobre el necropoder y la ocupación en la modernidad tardía son de gran utilidad. Se trata de comprender la amenaza que representa el ejercicio del Estado de Emergencia Económica, Social y Ecológica. Su acaecer a la manera de la colonia implicará conquista y anexión al servicio del Imperio y sus auxiliares locales. Los vencidos, en este caso, las gentes sumidas en la pobreza y la miseria, se enfrentarán a la violencia como forma original del derecho, para ello se incorporan tecnologías de acción directa, por ejemplo el **ESMAD** y, tecnologías de acción indirecta, como programas de caridad humanitaria que estructuralmente sólo significan muerte lenta, proceso progresivo de desnutrición, riesgo creciente de enfermedades mentales y, en no pocos casos, destrucción mutua y competencia por las sobras y las migajas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como demuestra Mbembe, en la colonia, este proceso transita por la adquisición, la delimitación, el control físico y geográfico de un conjunto nuevo de relaciones sociales y espaciales bajo la perorata de lávate las manos, no salgas de casa, la vacuna eres tú mismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Quizá por la poca vergüenza que tienen, no han dicho el virus pueden ser ustedes. Esta territorialización del aislamiento preventivo obligatorio se ejecuta como inscripción de nuevas relaciones espaciales, se valida a favor del control de la amenaza sanitaria, en resguardo de la salud pública, como promoción del bienestar general, construyendo para ello: líneas de demarcación, jerarquías de seres humanos, zonas autorizadas y zonas vedadas, enclaves para el autocuidado de los que sí deben vivir y enclaves para el aislamiento de los que no deben vivir, ni amenazar a los que sí tienen derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Prontamente la propiedad pública sobre los recursos territoriales ha sido cuestionada a favor de la banca y las grandes empresas nacionales y transnacionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En breve las personas han sido clasificadas en diferentes categorías. Los recursos naturales se siguen extrayendo con mayor protección porque no se debe romper la línea de producción que ayudará a enfrentar la pandemia, su saqueo a menor precio está garantizado por una tasa de cambio cada vez más desfavorable, reflexiona Mbembe, mientras en simultáneo se cultiva una amplia reserva de imaginarios culturales que legitiman el ejercicio de la soberanía estatal o gubernamental, como derechos diferentes para diferentes categorías de personas y con objetivos diferentes, sobre un mismo espacio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la práctica se gobierna sobre el presupuesto de la existencia de personas con la categoría de ciudadanos que deben vivir y personas que morirán enfermas, de hambre, de violencia intrafamiliar o de violencia social de unas contra otras, porque su ciudadanía apenas si merece la caridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mbembe expresa que la soberanía opera sobre el espacio, pues éste es la materia prima de la soberanía, que a su vez, es materia prima de la violencia. En este sentido, dice que la soberanía es ocupación y la ocupación en la práctica es segregación y estatus de objeto para los inhumanos y lo no humano.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La violencia más intensa, la opresión y la pobreza, se sufre a partir de la distinción de clase y de raza, aunque no se diga en la propaganda informativa.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Estado de Emergencia Económica, Social y Ecológica como entidad sociopolítica, cultural y económica, inventa el espacio y planifica científicamente los objetivos de control, restringiendo la producción, la propiedad, la residencia, el flujo y la ciudadanía. Acudiendo a **Fanon**, Mbembe nos explica la especialización de la ocupación colonial como división del espacio en compartimentos, despliegue de límites y fronteras por medio de los militares en movimiento y en puestos fijos, regulación por medio del lenguaje de la fuerza, con la presencia inmediata y la acción frecuente y directa contra el colonizado. Se trata, de nuevo acudiendo a Fanon, de la espacialización como operación del poder de muerte, creando para ello estigmas, etiquetas se dice en estos tiempos de virtualidad, sobre los lugares de mala fama, sobre las gentes de mala fama, sobre quienes nacen y mueren y viven en cualquier parte y de cualquier manera. Ciudadanías hambrientas de lo esencial, ciudadanías arrodilladas y humilladas, desposeídas de todo poder de reacción, sentencia Mbembe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mbembe, a esta altura de sus reflexiones, evidencia las preguntas que se hace este tipo de soberanía: ¿quién tiene y quién no tienen importancia?, ¿quién está desprovisto de valor?, ¿quién es y quién no es fácilmente sustituible? El Estado se convierte en la nueva deidad, poseedor del derecho divino sobre la existencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los pobres y miserables pueden morir tranquilamente en su ejercicio obediente de aislamiento preventivo obligatorio. La población detenida en los centros carcelarios puede ser masacrada impunemente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las presas políticas como la artista plástica **Lina Jiménez Numpaque** y las abogadas defensoras de Derechos Humanos, **Lizeth Johana Rodríguez Zarate y Alejandra Méndez Molano,** pueden ser extraídas de sus celdas, sin mediar el debido proceso legal y sin dar cuenta a familiares y apoderados, sembrando miedo y terror por la potencial tortura, muerte o desaparición, que ojalá no suceda, pero, que puede suceder en Colombia. A fin de cuentas las condiciones que legitiman la matanza están dadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo disciplinar, lo biopolítico y lo necropolítico, en palabras de Mbembe, como encadenamiento de poder, se traducen en dominación absoluta, la que se valida en el Estado de Sitio, con el brazo militar operando y concibiendo la identidad contra el otro y contra otras formas de comprender la situación. Para ello se apoya también en la ciencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La historia, la geografía, la cartografía, la arqueología y otras disciplinas, voluntaria o ingenuamente se pueden colocar al servicio de la ocupación, de la formación específica del terror, de la fragmentación territorial, de la infraestructura y las tecnologías de la política de muerte y del poder de muerte.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Paramilitarismo o del Genocidio por los Viejos Medios

<!-- /wp:heading -->

<!-- wp:paragraph -->

En sus conclusiones, Mbembe habla de las armas de la muerte, las que se despliegan con el objetivo de la destrucción máxima de las personas y la creación de mundos de muerte, donde las personas tienen el estatus de muertos-vivientes. Para ello, la política y el poder de muerte acuden en el proceso a la fragmentación territorial, prohibiendo el acceso a ciertas zonas, expandiendo su dominio de modo disperso, haciendo imposible todo movimiento y segregando a la población.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esa soberanía sobre el espacio, instala dispositivos dispersos de vigilancia y de ejercicio de poder, para controlar, vigilar y separar a la población, tanto en el suelo, como en el subsuelo y el aire. La circulación de los poderosos por redes rápidas, separadas y exclusivas, genera las condiciones a favor de los que tienen derecho de vivir, evitando entrecruzamientos con los que no tienen derecho de vivir. Los que van a morir, como después de la Masacre de la cárcel La Modelo, decía un preso en un comunicado de audio, se ven impelidos a zonas de conflicto en la superficie, el subsuelo y el espacio aéreo, en ellas se ejercerá el poder de matar, bien a través de tecnologías de precisión, también por medio de prácticas de sitio, sabotaje de la infraestructura vital, ocupación de tierra, agua y aire, ejecuciones a cielo abierto y matanzas invisibles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recogiendo la definición de Bauman sobre las guerras en la era de la globalización, Mbembe analiza lo que denomina máquinas de guerra. Dice Mbembe que Bauman apunta que las guerras en la era de la globalización ya no tienen como objetivo la conquista, adquisición y requisa de territorios, sino el ejercicio aplastante de la fuerza, cuya capacidad de destrucción se ha multiplicado, paralizando las capacidades del enemigo, mermando todo el sistema de supervivencia de éste, provocando daños duraderos en la vida civil y forzando al enemigo a la sumisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Explica que esas máquinas de guerra son agrupaciones militares, mano de obra armada y entrenada, que se compran y venden en el mercado. Su derecho de ejercer violencia y matar está acompañado de su existencia o desaparición de acuerdo a las tareas y las circunstancias que asume en un momento específico. Según Mbembe, son organizaciones que establecen una relación móvil con el espacio y asumen una ágil capacidad de metamorfosis. Estas máquinas de matar en la medida en que generan enormes beneficios, mantienen relaciones complejas con el Estado y conexiones directas con las transnacionales para expoliación de recursos naturales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Los Estados se las apropian o las crean, usan para ello parte de los ejércitos regulares o imprimen a sus ejércitos algunas características de estas máquinas de guerra.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como Maquinas de Guerra tienen algunos rasgos de organización política y de sociedad mercantil, de ahí que actúan capturando y depredando para maximizar sus beneficios. Esas máquinas de matar en el complejo panorama de la pandemia en Colombia, siguen operando en el territorio nacional, asesinando a los líderes y lideresas sociales, masacrando comunidades, amenazando a las personas más visibles de los procesos organizativos y trabajando de la mano de las mafias de la cocaína, la minería y los monocultivos, en el mercado capitalista legal e ilegal de comodities, seres humanos, armas y corrupción. A la existencia de las Maquinas de Muerte, suma Mbembe el análisis de la circulación monetaria en dos sentidos.

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Primero, en cuanto existencia de inestabilidad, fragmentación espacial, pérdida de valor de la moneda, ciclos de hiperinflación, evaporación general de la liquidez y acceso sometido a condiciones draconianas de endeudamiento y trabajo, condiciones extremadamente duras, excesivas, severas como los créditos gota a gota, las cooperativas de trabajo asociado y las esclavitudes modernas.

<!-- /wp:list -->

<!-- wp:paragraph -->

Por ser un aspecto histórico central para el funcionamiento de la política de muerte y el poder de muerte, la creación y mantenimiento de la deuda se mantiene. La deuda determina la producción de personas y el tipo de relación política, por la capacidad de endeudamiento se determina el valor de las personas y se juzga su utilidad. Quienes no tienen valor, ni utilidad pasan al estatus de esclavos, peones y clientes.

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Segundo, se establece un flujo controlado de capitales y un control del movimiento de los capitales en las zonas de extracción de recursos específicos, formando enclaves económicos, modificando la relación entre personas y cosas y convirtiendo los territorios donde hay recursos valiosos, en espacios privilegiados de guerra y muerte, donde la relación predominante es la relación entre guerra, máquinas de guerra y extracción de recursos, enlazadas fuertemente con la economía transnacional, local y regional, bien con la presencia del Estado o fuerzas paramilitares.

<!-- /wp:list -->

<!-- wp:paragraph -->

Sin duda, una cuestión de circulación monetaria que en sus dos aspectos funciona en los territorios rurales y también en los espacios urbanos de los sectores populares. Poderes de facto que no está claro como ejercen su poder de matar en estos tiempos de confinamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mbembe nos habla de la gestión de multitudes como una forma inédita de gubernamentalidad, caracterizada por la extracción y pillaje de los recursos naturales, tentativas brutales de inmovilizar y neutralizar espacialmente categorías completas de personas o de liberarlas forzándolas a diseminarse más allá de los territorios propios. Así, instala una realidad de poblaciones disgregadas, que tras el éxodo son encerradas en campos y zonas de excepción. Acudiendo a tecnologías de destrucción más táctiles, más anatómicas y más sensoriales se funda un contexto de muerte, donde el poder consiste en el estrecho control de los cuerpos y su concentración en campos en los que llegado el momento, se inscribirán esos cuerpos en la orden de la economía máxima: la masacre.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Para esa economía máxima, la generalización de la inseguridad es funcional.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Solo está seguro quien lleve armas, que en general son grupos armados que actúan con la máscara del Estado controlando territorios y la población civil, ejerciendo la guerra, masacrando.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Denuncia Mbembe, que la masacre tendrá la forma de cuerpos sin vida, cuerpos cual reliquias en duelo perpetuo, cuerpos vacíos, cuerpos desprovistos de sentido, cuerpos sumergidos en el estupor, cuerpos entre la frialdad de la masacre y la voluntad de sentido de la masacre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, la masacre también se ejecuta como cirugía demiúrgica en la que las formas de las vidas humanas son convertidas en piezas, fragmentos, pliegues, inmensas heridas que se mantienen a la vista de la víctima y de las personas de las comunidades, como huellas que no cicatrizan ni en el cuerpo, ni en el territorio o como cicatrices visibles y como mórbido espectáculo de la violencia que ha tenido lugar, pero sobre todo que puede tener lugar si se actúa contra la política de muerte y el poder de muerte.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### A modo de cierre: ¿qué hacer para resistir?

<!-- /wp:heading -->

<!-- wp:list -->

-   Si otros están decidiendo quién debe vivir y quién debe morir, tenemos que arrebatarles ese poder de muerte y transformarlo en poder de vida, potenciar una política de vida.

<!-- /wp:list -->

<!-- wp:list -->

-   Si la soberanía se ejerce como control de la mortalidad, si la política tiene forma de guerra, si lo político es un trabajo de muerte y la soberanía el ejercicio del derecho de matar, tenemos que defendernos, salvar nuestras vidas y todas las vidas.

<!-- /wp:list -->

<!-- wp:list -->

-   Si los derechos humanos se suspenden por regímenes de estado de excepción, de sitio y de emergencia, tenemos que presionar la construcción de la democracia real.

<!-- /wp:list -->

<!-- wp:list -->

-   Si la segregación y el racismo se ejercen desde el Estado nazi, debemos luchar contra todos los fascismos, los racismos y el patriarcado.

<!-- /wp:list -->

<!-- wp:list -->

-   Si la eliminación física del otro, si su ejecución en serie se convierte en cuestión técnica, si todo se militariza, si las esclavitudes modernas aumentan y niegan la humanidad de los seres humanos víctimas de esclavización, tenemos que evitar la matanza, el genocidio y la explotación.

<!-- /wp:list -->

<!-- wp:list -->

-   Si bajo precauciones sanitarias, fundadas o irreales, se establecen políticas de Apartheid que territorialización zona de inhumanos y de guerra donde opera la ley del poder de muerte contra las poblaciones segregadas y racializadas, tenemos que romper las tecnologías de la expoliación, de las restricciones de todo tipo y las estigmatizaciones.

<!-- /wp:list -->

<!-- wp:list -->

-   Si las máquinas de guerra se hacen con el dominio, ejerciendo su fuerza aplastante, destructiva, paralizante y de sumisión absoluta, tendremos que reconstruir nuestra capacidad de lucha, acumular fuerza suficiente y enfrentar esas máquinas de muerte.

<!-- /wp:list -->

<!-- wp:list -->

-   Si la autoridad policial, militar y las actuales tecnologías de destrucción ejercen a control de los cuerpos y el territorio, tendremos que potenciar las guardias indígenas, cimarronas, campesinas y populares.

<!-- /wp:list -->

<!-- wp:list -->

-   Si la circulación monetaria nos precariza por vía de la deuda y controla a través de los flujos financieros nuestros movimientos, tendremos que construir economías de resistencia, consolidar la soberanía alimentaria y establecer redes económicas alternativas y de equilibrio con la madre naturaleza.

<!-- /wp:list -->

<!-- wp:paragraph -->

Mbembe expone que en la adversidad de la tecnología de la esclavitud, las personas esclavizadas en la plantación eran consideradas propiedad, cosas sin comunidad y, por tanto, sin ejercicio del poder de la palabra y del pensamiento. No tenían autoridad para vivir su propio tiempo, tenían precio como instrumento de trabajo y valor como propiedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las personas esclavizadas eran mantenidas vivas pero mutiladas, no eran asesinadas porque eran necesarias para la explotación de su fuerza de trabajo. Asistían cada minuto a un mundo de violencias permanentes. Tenía en la esclavización no una forma de vida sino una forma de muerte porque en su humanidad se ejercía el ejercicio del comercio de la vida ajena, de la vida como propiedad del esclavista, de la vida como cosa, de la existencia como sombra. Las personas esclavizadas vivían en sí, en su humanidad el escenario del terror y el encierro. Y sin embargo, dice Mbembe, pese a todo ese horror indescriptible con palabras, en la extensión de su crueldad y dureza, pese a todo, el esclavizado adoptó puntos diferentes sobre el tiempo, el trabajo y sobre sí mismo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El esclavizado acudió a varias estrategias, por ejemplo, el suicidio, la evasión, la queja silenciosa y la rebelión.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En su proceso de resistencia el esclavizado estilizó objetos, instrumentos, lenguajes, gestos, hizo música, hizo danza, combatió su humanidad y la disputó a quienes se la negaban.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nos heredaron el ejemplo de la resistencia y la lucha, aún en la adversidad más absoluta. Hoy no tenemos claro ¿qué hacer?, pero, si tenemos intuiciones y experiencias que iluminan la posible respuesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que está más claro, es lo que no debemos hacer. No debemos permitir el aislamiento, la ruptura de los tejidos sociales y organizativos, dejarnos invadir por el miedo y el afán de acumulamiento, obrar según el principio de sálvese quien pueda. Tenemos mucho que aprender de las experiencias de los pueblos afrocolombianos, negros, palenqueros y raizales, de los pueblos y naciones indígenas, de las comunidades y organizaciones campesinas y de los movimientos y organizaciones cívicas de los sectores populares urbanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones sindicales, artísticas, estudiantiles, religiosas y feministas tienen un acumulado de lucha de inmenso valor. Los nuevos movimientos ambientalistas y de reconocimiento de la diversidad han acumulado un conjunto de saberes que las colocan en el centro de las demandas estratégicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No tenemos el cuerpo vacío. Nuestras piernas, tronco, brazos y cabeza viven y enriquecen cinco siglos de luchar, quinientos y más años de sabiduría y ciencia, lucha y resistencia. Un inimaginable acumulado que en la siembra de nuestros días y años por venir. Debemos pasar del aislamiento preventivo obligatorio al encuentro organizativo preventivo emancipatorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### [^\[1\]^](#_ftnref1) <jcaicedo@unicatolica.edu.co>; Docente – Investigador Especialización en  
Educación en Derechos Humanos - EDHUM. UNICATÓLICA Cali. Actualmente con  
funciones administrativas en la Dirección de EDHUM.

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### [^\[2\]^](#_ftnref2) <jmarias@unicatolica.edu.co>; Docente – Investigadora Especialización en  
Educación en Derechos Humanos. UNICATÓLICA Cali.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Mas de Yo Reporto](https://archivo.contagioradio.com/?s=yo+reporto)

<!-- /wp:paragraph -->
