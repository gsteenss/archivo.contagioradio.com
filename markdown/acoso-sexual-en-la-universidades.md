Title: Acoso sexual en la universidades, una violencia silenciosa contra las mujeres
Date: 2018-11-15 11:01
Author: AdminContagio
Category: DDHH, Mujer
Tags: acoso sexual, Hombres, mujeres, Universidades
Slug: acoso-sexual-en-la-universidades
Status: published

###### [Foto: Colectiva La Manada] 

###### [15 Nov 2018]

Este 15 noviembre en Colombia, se realizará la primera audiencia pública sobre acoso sexual contra las mujeres en las universidades del país, un escenario que tiene como objetivo posicionar este tema en la opinión publica y lograr que todas las instituciones de educación superior **tengan mecanismos para proteger a las estudiantes de este tipo de violencias sin revictimizarlas.**

De acuerdo con Laura Benavides, integrante de la colectiva Blanca Villamil y estudiantes de la Universidad Nacional, si bien el acoso sexual es una práctica que se da en todas las esferas de la sociedad, **las universidades deben ser lugares en donde las mujeres estén seguras, y esto no esta pasando actualmente**.

Por un lado, Benavides asegura que no existen mecanismos para que las mujeres denuncien este tipo de violencias en su contra, y por el otro, si llegan a denunciar estos hechos, las directivas **no toman con seriedad los abusos, generan prácticas de revictimización y no dan soluciones concretas**, acciones que conllevan a que la gran mayoría de mujeres desista de las denuncias. (Le puede interesar: ["Universidad públicas y privadas en deuda con las mujeres víctimas de violencia sexual"](https://archivo.contagioradio.com/universidades-publicas-y-privadas-en-deuda-con-las-mujeres-victimas-de-acoso-sexual/))

**"Lo que las universidades no logran entender, es que no simplemente por tener un protocolo se esta dando una solución"** afirma Benavides, que además señala que los presupuestos que se designan desde las universidades para este tipo de mecanismos son irrisorios, y usualmente en instituciones como la Nacional, solo cubren la sede Bogotá.

### **¿Quién es el acosador?** 

Laura Benavides manifiesta que lo más difícil es generar un perfil físico de los agresores, porque si bien en el imaginario social se ha establecido que los acosadores son profesores mayores, pero la realidad **es que cualquier hombre puede ser un acosador. **

Sin embargo, asevera que sí hay conductas que identifican a los acosadores sexuales, una de ellas y tal vez la más importante, es que **son personas que ostentan lugares de poder sobre sus víctimas**, las manipulan a partir de esa relación y las sitúa como sujetos inferiores.

"Varias chicas han denunciado que hay profesores, que en medio de la clase, les han dicho a estudiantes, usted esta sentada en su nota", asegura Benavides, frase que de inmediato señala un acoso sexual y media esa relación de poder.

###### Reciba toda la información de Contagio Radio en [[su correo]
