Title: Menos del 10% de quienes presentan el ICFES se gradúan de la Universidad
Date: 2019-08-14 09:14
Author: CtgAdm
Category: DDHH, Educación
Tags: Educación Superior, estudiantes, Finaciación, universidad
Slug: menos-icfes-graduan-universidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/IMG_2962-e1565735386296.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/IMG_2962-1-e1565735479125.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/IMG_2870-e1565735621472.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

El pasado fin de semana, según cifras del ICFES, cerca de 650 mil estudiantes de educación básica presentaron las pruebas Saber 11; de acuerdo a cifras del Ministerio de Educación Superior (MEN), de ellos, **solo cerca del 48% logrará acceder a la Universidad.** Esta sería solo una de las grietas presentes para acceder a las Universidades denunciadas por la Unión Nacional de Estudiantes de Educación Superior (UNEES), que también ha planteado propuestas para cerrar brechas.

### **Bienestar de la universidad, financiación y precarización** 

**Cristian Guzmán, vocero nacional de la UNEES,** añadió a las cifras de acceso al nivel superior de estudios otra igualmente importante: "Solamente el 20% de las personas que ingresan a la educación superior se gradúan". Esto significa que de esta generación que pronto se graduará de bachiller, **solo cerca de 60 mil lograrán obtener un título como profesional,** técnico o tecnólogo. (Le puede interesar: ["Estudiantes retomarán acciones ante incumplimiento del Gobierno a los acuerdos de 2018"](https://archivo.contagioradio.com/estudiantes-incumplimiento-gobierno-acuerdos/))

Según Guzmán, esto se debe a que el sistema de educación superior ha buscado aumentar su capacidad para recibir mayor cantidad de población sin mejorar sus condiciones de financiación ni bienestar universitario, dejando de lado la dignidad en el acceso a este derecho. El activista resaltó que esta estrategia ha sido posible mediante el ICETEX principalmente, es decir, "personas con vulnerabilidades económicas están ingresando a la educación superior y si se gradúan, lo hacen supremamente endeudados".

Las más recientes cifras encontradas en la página del MEN sobre acceso a la educación revelan que además, las ofertas de educación superior están concentradas en grandes capitales. Por ejemplo, **en 2015 la mayoría de estudiantes estaban matriculados en Bogotá (32%),** mientras Antioquia, Valle del Cauca, Atlántico y Santander concentraron el 32,5% de los matriculados. (Le puede interesar: ["A la minga indígena llegaron primero de los estudiantes que Duque"](https://archivo.contagioradio.com/minga-indigena-estudiantes-duque/))

Guzmán indicó que esta concentración es grave, teniendo en cuenta que las personas que vienen de otras regiones llegan a las capitales a estudiar encuentran que no tienen los apoyos y estímulos económicos para culminar sus carreras. En ese sentido, el movimiento estudiantil ha analizado el Acuerdo de Paz, que pone buena parte de su enfoque en garantizar derechos fundamentales en regiones apartadas del país; en el caso de la educación, el Vocero de la UNEES criticó que se buscará hacer de forma virtual.

### **¿Cuál es la propuesta del Gobierno?**

En campaña, el entonces candidato Iván Duque dijo, "pondremos en funcionamiento a gran escala los esquemas de educación virtual"; para ello dijo que una de las instituciones que debían verse fortalecidas era el Servicio Nacional de Aprendizaje (SENA). Sin embargo, Guzmán declaró que "nosotros somos muy realistas sobre lo que llaman la revolución digital, porque estamos viendo la precarización de los derechos"; ello, considerando que para la UNEES, **el campo colombiano necesita la educación presencial.**

El movimiento estudiantil considera necesario crear campos de calidad para que cada región encuentre potencialidades, y genere conocimiento que resuelva las problemáticas de cada contexto; en ese sentido, cuestionan que se esté buscando **"crear salas de computadores donde los compañeros supuestamente pueda acceder a la educación".** (Le puede interesar: ["Universidades públicas: Otro escenario donde se vive el conflicto armado"](https://archivo.contagioradio.com/universidad-publica-conflicto-armado/))

Aunque el movimiento estudiantil alcanzó un acuerdo con el Gobierno Nacional en diciembre de 2018 para inyectar recursos a las Instituciones de Educación Superior públicas, Guzmán resaltó que este logro radicó en una lucha de más de 25 años por la financiación, y que se reduce a eso: "una inyección de presupuesto, lo que no mejorará los problemas que tiene el país en términos de inversión para la educación, la falta de mano de obra cualificada y el acceso a nuevos niveles de innovación".

En ese sentido, resumió **las grandes grietas que impiden a los jóvenes lograr tener educación superior:** La baja cantidad de graduandos, por los bajos niveles de bienestar en la universidad; la relación lejana que tienen las universidades con el contexto y las realidades del país; y la dificultad de acceso a la universidad por parte de sectores económicamente vulnerables. (Le puede interesar: ["Al Ministerio de Educación se le salieron de las manos las universidades privadas"](https://archivo.contagioradio.com/ministerio-educacion-universidad-privada/))

**La propuesta del movimiento estudiantil: Una reforma constitucional**

Para corregir las brechas de acceso a la educación y garantizar la apertura de más cupos dignos en los centros educativos, Guzmán aseveró que es necesaria una reforma constitucional, cuyo punto central sea el aumento de recursos para las Universidades e Institutos técnicos y tecnológicos públicos. Asimismo, dijo que **se requería el apoyo de la sociedad en este propósito**, porque aunque  "es un derecho costoso, vale la pena".

<iframe id="audio_39960563" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_39960563_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
