Title: Sólo en diciembre han asesinado a 17 personas en Tumaco
Date: 2015-12-21 16:23
Category: DDHH, Nacional
Tags: Paramilitarismo, Tumaco
Slug: solo-en-diciembre-han-asesinado-a-17-personas-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/tumaco-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [danielabularistizabal] 

###### [21 Dic 2015]

En una carta pública firmada por la Diócesis de Tumaco, la alcaldía municipal y otras autoridades se evidencia la ola de violencia **ligada con la presencia paramilitar y de otros actores del conflicto en la ciudad de Tumaco** y los municipios aledaños. Entre las personas asesinadas se encuentran dos menores de edad, varios estudiantes y dos mujeres.

La fuerte ola de violencia ha obligado a cancelar diversas actividades programadas por las parroquias de la diócesis para las fechas decembrinas. Según los firmantes de la misiva dirigida al presidente Juan Manuel Santos en Tumaco **“Se vive una situación generalizada de gran temor en la mayoría de los barrios del Municipio…”**

En la carta también se hace un recuento del  alto número de víctimas de la guerra en eses municipio, que según la Unidad de Víctimas desde 1985 hasta ahora se registran unas **9065 víctimas de asesinatos, más de 109167 víctimas de desplazamiento, 1157 desapariciones forzadas** y un alto número de personas víctimas de minas antipersonas y otros artefactos.

Otro de los asuntos que preocupan a las autoridades eclesiales y civiles del municipio es que en medio de la presencia de la fuerza pública han circulado amenazas de diversos grupos paramilitares como “Los Gaitanistas”, “Los Paísas” el “Clan Usuga” y también la presencia de las guerrillas de las FARC y el ELN.

Por esas razones exigen la realización de un Consejo de Ministros en el territorio, la investigación y sanción de los responsables de los asesinatos y otros crímenes, y a las autoridades en general que se **impida que los grupos paramilitares tomen el control del territorio** una vez se firmen los acuerdos de paz en el marco de las conversaciones entre el gobierno nacional y la guerrilla de las FARC.

[Comunicado Tumaco 18122015](https://archivo.contagioradio.com/solo-en-diciembre-han-asesinado-a-17-personas-en-tumaco/comunicado-tumaco-18122015/)
