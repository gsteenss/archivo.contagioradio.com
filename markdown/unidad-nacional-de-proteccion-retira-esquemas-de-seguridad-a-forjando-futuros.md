Title: Unidad Nacional de Protección retira esquema de seguridad a Forjando Futuros
Date: 2019-10-30 18:22
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Forjando Futuros, protección, Restitución de tierras, Unidad Nacional de protección
Slug: unidad-nacional-de-proteccion-retira-esquemas-de-seguridad-a-forjando-futuros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-28-at-4.49.53-PM-e1548712266586-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

La Fundación Forjando Futuros denunció que el pasado martes 29 de octubre, la Unidad Nacional de Protección (UNP) decidió retirar el esquema de seguridad colectivo asignado para la organización, lo que genera un aumento en el peligro de sus funcionarios. Según Gerardo Vega, director de la Fundación, el año pasado se había pedido la ampliación de este esquema debido al riesgo que supone acompañar los procesos de restitución de tierras en Urabá, y las amenazas que han recibido.

### **El trabajo de la Fundación, una labor de riesgo** 

Según explicó Vega, funcionarios de la UNP que llegaron de Medellín, Antioquia, les dijeron a integrantes de la Fundación que retiraban el esquema en cumplimiento de órdenes que venían de Bogotá. Dicho esquema, de carácter colectivo, servía como articulador para el trabajo de campo que realizaba la organización, como el levantamiento de datos topográficos y el acompañamiento jurídico a comunidades. (Le puede interesar: ["Anglo Gold Ashanti pretende bloquear investigación sobre violencia en territorios en los que hace presencia"](https://archivo.contagioradio.com/anglo-gold-ashanti-bloquear-investigacion/))

Vega aseguró que la Fundación trabaja acompañando el proceso de restitución de tierras principalmente en Urabá, "región donde han sido asesinados 23 reclamantes de tierras". Allí, Forjando Futuros acompaña más de 900 personas en total, y lleva cerca de 250 procesos judiciales, lo que le ha permitido también presentar informes ante el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR). (Le puede interesar: ["Denuncian plan criminal contra acompañantes de reclamantes de tierras en Urabá"](https://archivo.contagioradio.com/denuncia-acompanantes-reclamantes-uraba/))

### **La situación de riesgo** 

El director de la Fundación recordó que el año pasado habían pedido el fortalecimiento del esquema de seguridad, en vista de agresiones y amenazas que los mismos organismos de seguridad habían advertido. Según referenció Vega, recientemente acompañaron una entrega de tierras restituidas en la vereda Guacamayas, allí asistieron diferentes organizaciones defensoras de derechos humanos, y mientras se adelantaba la diligencia vieron que había personas buscando intimidar a los reclamantes, así como a los miembros de la Fundación.

El experto señaló que hechos similares ocurren cuando se realizan procesos que involucran a los topógrafos, que reciben amenazas o vigilancia por parte de hombres armados no identificados. Por esta razón, señaló que ya se envió un oficio a la UNP solicitando que se restablezca y fortalezca el esquema de protección colectiva. (Le puede interesar: ["Paramilitares arremeten de nuevo contra reclamantes de Las Guacamayas"](https://archivo.contagioradio.com/paramilitares-arremeten-nuevo-reclamantes-las-guacamayas/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
