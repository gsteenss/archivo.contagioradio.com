Title: Activistas secuestradas por el ejército de Israel recuperan su libertad
Date: 2016-10-14 15:56
Category: El mundo, Mujer
Tags: Conflicto Israel-Palestina, Franja de Gaza, Mujeres Rumbo a Gaza
Slug: activistas-secuestradas-por-el-ejercito-de-israel-recuperan-su-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/gaza1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Infolibre] 

###### [12 Oct de 2016 ] 

Tras varios días de estar secuestradas en zona militar de Israel, las 13 activistas de la flotilla Mujeres Rumbo a Gaza, recuperaron su libertad y fueron deportadas a sus países de origen. Algunas de ellas, **informan que las autoridades israelíes les impidieron cualquier tipo de comunicación y dormir durante su tiempo en prisión.**

Estuvieron detenidas durante un tiempo, relativamente corto, en comparación con flotillas anteriores, según la organización Rumbo a Gaza, fue **gracias al apoyo de organizaciones, medios de prensa alternativa, diputados y personalidades**  como la banda de rock Pink Floyd. Le puede interesar: [Mujeres Rumbo a Gaza son asaltadas por la armada de Israel.](https://archivo.contagioradio.com/mujeres-rumbo-a-gaza-asaltadas-por-la-armada-de-israel/)

Hasta el momento las activistas no han dado declaraciones oficiales, la información que se conoce ha sido compartida a través del portal de internet de la organización. Una de ellas, la española Sandra Barrilaro manifestó que **“nuestro trabajo no habrá acabado hasta que se levante el bloqueo ilegal y termine la complicidad de nuestros gobiernos con la ocupación israelí”. **Le puede interesar: [“Palestina: su lucha es también la nuestra” Kwara Kekana](https://archivo.contagioradio.com/palestina-su-lucha-es-tambien-la-nuestra-kwara-kekana-lider-sudafricana/)

Por otra parte las Naciones Unidas, manifestaron que temen por una nueva guerra israelo-palestina, luego de las continuas presiones por parte de Israel a Palestina. Así mismo, Nickolay Mladenov coordinador especial de la Organización de las Naciones Unidas para el proceso de paz de Oriente Medio, **reprochó que el régimen de Tel Aviv no haya hecho lo suficiente para aliviar el bloqueo paralizante en Franja de Gaza.  **

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
