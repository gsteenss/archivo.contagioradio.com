Title: MOVICE se afirma en favor de la tipificación del paramilitarismo como delito
Date: 2020-09-29 14:48
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Congreso de la República, MOVICE, Paramilitarismo, Proyecto de Ley
Slug: movice-se-afirma-en-favor-de-la-tipificacion-del-paramilitarismo-como-delito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este lunes el Movimiento Nacional de Víctimas de Crímenes de Estado -MOVICE- dio a conocer su intervención en la audiencia pública del Congreso en la que se debate **un proyecto de ley para establecer el paramilitarismo como un delito consignado en el Código Penal.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El proyecto tiene como autores a [varios](https://congresovisible.uniandes.edu.co/proyectos-de-ley/por-medio-de-la-cual/9354/#tab=2) congresistas entre los que destacan Iván Cepeda, Gustavo Petro, María José Pizarro y Angela María Robledo; y busca puntualmente **incorporar «*al Título XII del Código Penal (Ley 599 de 2000) disposiciones tendientes a combatir grupos armados organizados ilegales de cualquier tipo, incluyendo los denominados autodefensas; grupos de seguridad que ejecuten actos ilegales; paramilitares, así como sus redes de apoyo, estructuras o prácticas u otras denominaciones equivalentes*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto el MOVICE, aseguró que **«l*a tipificación del delito de paramilitarismo*** *en el Código Penal Colombiano es para las víctimas de crímenes de Estado, **una de las principales medidas que el Estado podría implementar para detener este horrendo capítulo de sangre que estamos presenciando, y está en las manos del Congreso hacerlo realidad*****».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, enfatizaron en **la importancia de que la ley que pudiera aprobarse, tenga en cuenta la participación de las víctimas**, y que «*las medidas que a partir de allí se implementen estén basadas en la verdad, en la aplicación de la justicia y en la contribución a la reparación*». (Le puede interesar: [Exparamilitares dicen sí a la verdad](https://archivo.contagioradio.com/exparamilitares-dicen-si-a-la-verdad-para-reparar-a-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, sostuvieron que el paramilitarismo se ha desarrollado en instancias institucionales de todos los órdenes (nacionales, regionales y locales), como **una estrategia de «*control social*» y  de exterminio a la oposición política y las acciones de reivindicación de derechos de diversos sectores sociales.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «Hoy se hace necesario reconocer el origen estatal del paramilitarismo y la continuidad de las estructuras paramilitares para afrontarlas de manera decidida».
>
> <cite>Movimiento Nacional de Víctimas de Crímenes de Estado -MOVICE-</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Frente a esto, el MOVICE agregó que la institucionalización del paramilitarismo ha tenido origen incluso normativo, con la promulgación de estatutos como el [Decreto 3398 de 1965](https://www.funcionpublica.gov.co/eva/gestornormativo/norma.php?i=66354#:~:text=Todos%20los%20colombianos%2C%20hombres%20y%20mujeres%20no%20comprendidos%20en%20el,al%20restablecimiento%20de%20la%20normalidad.) el cual fue adoptado como legislación permanente a través de la [Ley 48 de 1968](https://www.funcionpublica.gov.co/eva/gestornormativo/norma.php?i=31239), en los que **se promovía la autodefensa civil.** Asimismo, el **Decreto 356 de 1994 que «*hizo posible la conformación de grupos de particulares –posteriormente llamados “Convivir”-,*** *encargados de servicios de vigilancia y seguridad, con facultades especiales en lo que se refiere al uso de armas, técnicas*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **«**Como lo constató la Corte Interamericana de Derechos Humanos en varios casos sometidos a su examen^,^ con esta normatividad el Estado permitió e incentivó la creación y proliferación de los grupos paramilitares, y dio pie para que, en lugar de reprimirlos, las autoridades públicas y los miembros de la Fuerza Pública, colaboraran con ellos, facilitando o participando incluso en la comisión de sus crímenes».
>
> <cite>Movimiento Nacional de Víctimas de Crímenes de Estado -MOVICE-</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### El negacionismo del Gobierno y los medios de comunicación frente al paramilitarismo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al tiempo que destacó el origen estatal del paramilitarismo el MOVICE, se pronunció sobre **el negacionismo y la invisibilización que tiene esta problemática por parte del Gobierno y los medios tradicionales**, pese a que es un fenómeno que sigue vigente y los intereses a los que sirve siguen siendo económicos, políticos y de control territorial.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Hoy asistimos a un reciclaje, con nuevos nombres y con intensas campañas mediáticas e institucionales que permiten ubicarlas en el campo de la delincuencia común mediante la sugestiva sigla de “Bandas Criminales –BACRIM”, neo paramilitares, “Grupos Armados Organizados” o  Grupos de Delincuencia Organizada (GDO).
>
> <cite>Movimiento Nacional de Víctimas de Crímenes de Estado -MOVICE-</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Sin embargo, señala el MOVICE, que contrario a lo expresado en los discursos institucionales y mediáticos, el paramilitarismo conserva los mismos rasgos esenciales:  «*su orientación contrainsurgente extendida a las posiciones ideológicas anticapitalistas; su defensa de megaproyectos de empresas transnacionales a las cuales han escoltado con métodos de barbarie; sus acuerdos secretos o discretos con la fuerza pública \[…\], su defensa del \[G\]obierno y su amenaza permanente a los sectores inconformes o críticos; \[y\] sus acuerdos con la clase dirigente que orienta la economía y la política del Estado*». (Lea también: [Otra Mirada: Estas son las características del paramilitarismo después del acuerdo de paz](https://archivo.contagioradio.com/otra-mirada-estas-son-las-caracteristicas-del-paramilitarismo-despues-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
