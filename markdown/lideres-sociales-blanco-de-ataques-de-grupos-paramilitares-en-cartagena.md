Title: Líderes sociales blanco de ataques de grupos paramilitares en Cartagena
Date: 2016-09-22 14:08
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas, Cartagena, Grupos Paramilitares Sucesores, paramilitares en Colombia
Slug: lideres-sociales-blanco-de-ataques-de-grupos-paramilitares-en-cartagena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Movimiento-Mujeres-Cartagena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Social de Mujeres Cartagena ] 

###### [22 Sept 2016] 

De acuerdo con el más reciente Informe de Riesgo emitido por la Defensoría del Pueblo, los **habitantes de 40 barrios de la zona urbana de Cartagena y 4 corregimientos rurales, están en inminente peligro** ante las extorsiones y amenazas, así como los desplazamientos, homicidios y reclutamientos por parte de grupos paramilitares como las [[Autodefensas Gaitanistas de Colombia](https://archivo.contagioradio.com/familias-confinadas-en-sus-fincas-por-ataques-paramilitares-en-guacamayas/)], Los Rastrojos y Las Águilas Negras. La población más afectada es la infantil y juvenil, además de las mujeres y líderes vinculados a procesos sindicales, organizativos y de víctimas.

Según la Defensoría los grupos están ligados al narcotráfico, la extorsión y el préstamo a usura; expulsan de sus territorios a las comunidades para la [[implementación de proyectos agroindustriales, turísticos o de construcción](https://archivo.contagioradio.com/documental-evidencia-despojo-paramilitarismo-y-accion-empresarial-en-mapiripan/)] y aquellos relacionados con el tráfico de drogas; **perpetran homicidios y amenazas contra líderes sociales que tienen trabajo comunitario** en sectores populares; establecen normas de conducta por medio de la violencia y el control territorial; y reclutan a menores de edad para usarlos como vigilantes y mensajeros en sus organizaciones.

Estos grupos que cuentan con un número más reducido de integrantes, en comparación con las estructuras paramilitares de vieja data, se han concentrado en los barrios de la periferia cartagenera, por medio de bandas criminales que ejercen control territorial tanto en los vecindarios como en las vías y carreteras; vigilan el tránsito de mercancías legales e ilegales, **atemorizan y vulneran los derechos de los pobladores y sus líderes quienes son amenazados** a través de [[panfletos, llamadas telefónicas, mensajes de texto](https://archivo.contagioradio.com/autodefensas-gaitanistas-amenazan-a-lideres-en-barrancabermeja/)] y seguimientos en motocicletas.

Organizaciones como la Mesa Distrital de Víctimas, aseguran que los ataques se dirigen principalmente a miembros de procesos comunitarios de defensa de los derechos humanos y étnicos, liderados por mujeres, víctimas y sindicatos, quienes constantemente denuncian todo tipo de problemáticas. Los líderes vinculados a la restitución de tierras y los derechos de la comunidad LGBTI, también han sido blanco de las agresiones. Para estos pobladores la situación es alarmante pues han identificado que **los hechos tienen directa relación con su activismo social o sus orientaciones sexuales**, por lo que exigen que haya [[presencia estatal para atender la situación](https://archivo.contagioradio.com/es-fantasioso-asegurar-que-el-paramilitarismo-no-sigue-vigente/)].

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
