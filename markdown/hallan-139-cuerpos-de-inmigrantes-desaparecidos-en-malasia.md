Title: Hallan 139 cuerpos de inmigrantes desaparecidos en Malasia
Date: 2015-05-25 13:07
Category: El mundo, Otra Mirada
Tags: 139 fosas comunes por trata de personas en Malasia, 17 campamentos ilegales de inmigrantes descubiertos en Malasia, Inmigración Ilegal, Malasia, Trata de personas en Malasia
Slug: hallan-139-cuerpos-de-inmigrantes-desaparecidos-en-malasia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/malasia-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Globovision.com 

**Malasia ha descubierto más de 30 fosas comunes** en las que habrían al menos **139 cuerpos de inmigrantes ilegales desaparecidos**, víctimas de la **trata de personas**. También se han encontrado **17 campamentos donde eran recluidos los inmigrantes** por las mafias ilegales que controlan el tráfico de personas.

En una **operación conjunta de los gobiernos de Tailandia y Malasia contra las mafias** y la trata de personas, fueron descubiertos 17 campamentos abandonados donde los inmigrantes malvivían a la espera de poder trabajar.

En estos mismos campamentos fueron encontradas al menos **30 fosas comunes donde depositaban los cadáveres de los inmigrantes que morían** debido a las condiciones insalubres en las que estaban viviendo.

El origen de los **inmigrantes ilegales es de Rohingya y Bangladesh** y todo apunta a que son refugiados de los distintos conflictos que se están viviendo en sus regiones.

Todos ellos son **víctimas de las mafias de trata de personas** entre Tailandia, Birmania y Bangladesh hacia Malasia, país que vive una situación social, política y económica mejor que la de sus vecinos.

Las **mafias** aprovechan la **situación de extrema pobreza, de exclusión social de las personas afectadas y de persecución para engañarles**, cobrándoles por el transporte y por un futuro con trabajo y vivienda que más tarde los inmigrantes comprueban que era un engaño, siendo abocados a la explotación laboral y a la esclavitud.

Además, al no tener documentos legales, se exponen ha ser detenidos y **expulsados del país, llevándoles a vivir constantemente en medio de la persecución** y obligados a aceptar todo tipo de trabajos forzados.

Un ejemplo, es la **etnia Rohingya de Birmania, que constituye una minoría de musulmanes en el país perseguida históricamente** por el racismo en donde viven en condiciones de máxima pobreza y vulnerabilidad.
