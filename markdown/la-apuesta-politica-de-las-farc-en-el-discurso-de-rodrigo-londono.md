Title: La apuesta política de las FARC en el discurso de Rodrigo Londoño
Date: 2016-09-27 14:19
Category: Nacional, Paz
Tags: discurso timochenko, Timoleon Jimenez
Slug: la-apuesta-politica-de-las-farc-en-el-discurso-de-rodrigo-londono
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/WhatsApp-Image-2016-09-26-at-6.02.05-PM-e1474999900582.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 de Sep 2016] 

Uno de los discursos que más ha generado opiniones, después de la firma protocolaria de los acuerdos, es el del máximo jefe de las FARC-EP Rodrigo Londoño. Durante su alocución **hizo referencia a problemáticas internas del país** como la salud, la marginalidad y la pobreza o el daño ambiental de las multinacionales en el territorio colombiano, **estas afirmaciones podrían vislumbrar el camino que adoptaría las FARC-EP en su proyecto como movimiento político**.

Para el analista, docente e integrante del Centro de Pensamiento de la Universidad Nacional, Carlos Medina Gallego hay **3 componentes claves dentro del discurso de Rodrigo Londoño **que evidencian, no solo su evolución en el escenario de la democracia sino un giro retórico hacia la sociedad.

En primera instancia, Medina señala que el discurso pretendía darle a conocer al país y a la comunidad internacional, la disposición y decisión de las FARC-EP, de [salir de la guerra hacia el escenario de la democracia](https://archivo.contagioradio.com/1-641-dias-de-dialogos-para-poner-fin-a-52-anos-de-guerra-en-colombia/) y de conformar el nuevo movimiento político resultante de este proceso.

Por otro lado, la alocución se enfocaba en la idea de **ampliación y profundización de un modelo democrático**, que tome en consideración los aspectos de orden social, por ende, hizo un llamado a acabar con la pobreza, el hambre, la desigualdad, la exclusión y en general con todos los factores de la violencia que son causa estructural de los conflictos internos del país. En este mismo sentido, **llamó a los gobiernos de Siria e Israel y Palestina a finalizar los conflictos bélicos en sus países** y los invitó a que se unan al camino de la paz como Colombia.

Para finalizar, está la retórica y poesía que reflejó el máximo líder de las FARC-EP, al mencionar los personajes y el relato de Gabriel García Márquez, para simbolizar con “Cien años de soledad” y las mariposas amarillas, el momento histórico que vivie Colombia.  Frases como **“… aquí nadie ha renunciado a sus ideas, las confrontaremos en la arena política por la convivencia pacífica”** o “Colombia requiere transformaciones profundas para hacer realidad sus sueños”, ratifican el cambio de escenario y [la nueva apuesta elaborada en la décima y última conferencia del grupo como guerrilla.](https://archivo.contagioradio.com/con-un-parte-de-victoria-para-la-paz-las-farc-concluye-x-conferencia/)

<iframe id="audio_13082414" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13082414_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
