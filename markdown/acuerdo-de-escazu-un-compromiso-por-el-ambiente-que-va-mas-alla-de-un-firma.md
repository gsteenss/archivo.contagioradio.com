Title: Acuerdo de Escazú, un compromiso por el ambiente que va más allá de un firma
Date: 2019-12-11 17:41
Author: CtgAdm
Category: Ambiente, Líderes sociales
Tags: Ambiente, Defensores, Derechos Humanos, Escazú
Slug: acuerdo-de-escazu-un-compromiso-por-el-ambiente-que-va-mas-alla-de-un-firma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Marcha-por-el-agua-e1464906866438.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

A partir del 12 de diciembre, **Colombia hará parte del acuerdo de Escazú**, esto según el anuncio que hizo el 9 de diciembre el Presidente Duque. El Acuerdo hace parte de un compromiso firmado por más de 21 países en Latinoamérica y el Caribe el 4 de marzo de 2018, y que busca proteger los protocolos para la protección ambiental y especialmente de los líderes que velan por el territorio. (Le puede interesar:[Acuerdo de Escazú una herramienta para proteger la vida de líderes ambientales en Colombia](https://archivo.contagioradio.com/acuerdo-de-escazu-una-herramienta-para-proteger-la-vida-de-lideres-ambientales-en-colombia/)

Este acuerdo  estipula que los gobiernos firmantes deben garantizar el acceso a la información ambiental y a la justicia de los asuntos del medio ambiente, al igual que la participación pública de los mismos; al igual que **busca proteger a los líderes ambientales que luchan por el derecho a vivir en un espacio territorial sostenible y sensible a las necesidades de la naturaleza**.

Colombia se sumó a  Bolivia, Guyana, San Vicente de las Granadinas, Uruguay y San Cristóbal y Nieves, países que ya ratificaron el Acuerdo, sin embargo, aún hace falta que se sumen al menos 11 países más, para así poder lograr que este tratado sea obligatorio. (Le puede interesar: [Gobierno Colombiano sigue diciendo no a la protección de líderes Ambientales](https://archivo.contagioradio.com/gobierno-niega-acuerdo-escazu/)).

El **Acuerdo de Escazú** da apertura al acceso a la información pública, participación de las comunidades y líderes en debates de control ambiental, normas y políticas judiciales en temas ambientales, **protección a los defensores del ambiente y fortalecimiento de capacidades internas y la cooperación entre los paíse**s.

### **Se firma el Acuerdo de Escazú, pero su mayor reto será que sea cumplido. **

En semanas anteriores líderes ambientalistas se levantaron de la mesa  de negociación que se adelantaba con el presidente Duque en el contexto del paro nacional, la decisión fue tomada ante el silencio del gobierno en la protección del Páramo de Santurbán y la continuación de los pilotos de fracking. No obstante, este martes reconocieron que la firma del Acuerdo es un avance; "*Es un paso más hacia  el acceso a la información y la mejora de los procesos participativos y la justicia ambiental; sin embargo en  Colombia uno de los mayores problemas es que se firman acuerdos que se ejecutan de manera intermitente, además no hay quien los cumpla, porque cuando llega el momento de seleccionar la organización esto queda en un limbo*" **Diana Giraldo ** integrante del Movimiento Ríos Vívos.

En Colombia, según un informe del 2019 de la organización Somos Defensores,  se registraron 805 agresiones en contra de líderes sociales, de los cuales 155 fueron asesinados; y destacando que 63 de estas muertes corresponden a líderes ambientales que trabajaban en temas de cultivos de uso ilícito, defensa de territorio ancestral, semillas nativas, nacederos de agua y la conservación de los bosques. (Le puede interesar: [Caso de líderes ambientales que están en la cárcel llega a la ONU](https://archivo.contagioradio.com/caso-lideres-presos-ante-onu/))

Por otro lado según un informe presentado por  Global Witness en 2018, **Colombia es el segundo país donde más líderes ambientales son asesinados,** destacando que el número de muertes es de 83, más de la mitad del total reportado por la ONG inglesa, que registró 164 asesinatos en todo el mundo. (Le puede interesar:[«No nos dejen solos», la petición de líderes sociales del Chocó ante amenazas](https://archivo.contagioradio.com/no-dejen-solos-lideres-sociales-choco/)).

 

** **  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
