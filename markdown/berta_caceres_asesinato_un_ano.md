Title: "Asesinato de Berta Cáceres demuestra el deplorable estado de los DDHH en Honduras"
Date: 2017-02-28 20:00
Category: Ambiente, Nacional
Tags: Agua Zarca, Berta Cáceres, honduras
Slug: berta_caceres_asesinato_un_ano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/berta_caceres-e1488329283306.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Altreconomia]

###### [28 Feb 2017] 

Se cumple un año del asesinato de lideresa indígena Berta Cáceres. Un año de amenazas a su familia. Un año de incertidumbre. Un año de pocos avances en la investigación. Un año sin verdad. Y un año en el que han asesinado a otros líderes que acompañaban la lucha de Berta.

En la madrugada del 3 de marzo de 2016 la activista del pueblo Lenca, se encontraba en su casa preparándose para presentar alternativas al proyecto hidroeléctrico Agua Zarca acompañada del ambientalista mexicano Gustavo Castro. Según el relato de los hechos, los asesinos forzaron las puertas de su vivienda en La Esperanza provincia de Intibucá, para cometer el crimen. **Castro cuenta que escuchó cuando Berta preguntó: “¿Quién está ahí?”, y un sicario le disparó en repetidas veces** ocasionándole la muerte e hiriendo al activista mexicano.

Tras el asesinato, organizaciones de derechos humanos y ambientales, Misiones Internacionales, e incluso el Papa Francisco han pedido al gobierno hondureño que avance en la investigación del crimen de la ambientalista, y dé con aquellos que ordenaron el asesinato. Sin embargo **la represión contra las protestas que se han desarrollado para exigir el esclarecimiento del crimen, y las amenazas al COPINH y a los familiares de Berta, han sido la constante.** [(Le puede interesar: Vaticano pide investigar crimen de Berta Cáceres)](https://archivo.contagioradio.com/vaticano-pide-investigacion-imparcial-e-independiente-por-crimen-contra-berta-caceres/)

Paralelamente, como un "incidente" calificó el presidente del Banco Mundial Jim Yong Kim, el asesinato de la lideresa. A su vez el Consejo Cívico de Organizaciones Populares e Indígenas de Honduras, denunciaba que **el gobierno estaba intentando desviar la investigación,** tratando de responsabilizar a su ex esposo y al dirigente campesino Gustavo Castro. El objetivo: absolver la responsabilidad de la empresa que construye el proyecto hidroeléctrico Agua Zarca.

Este panorama propició el enérgico llamado de atención que hizo Amnistía Internacional a Honduras. “El mensaje es claro: si tu labor de derechos humanos molesta a los que tienen el poder, te matarán”, expresó Erika Guevara-Rosas, directora para las Américas de Amnistía Internacional.

En el marco de una reunión que se desarrolló este martes, entre la directora de AI con autoridades de Honduras en Tegucigalpa, para conocer los avances en la investigación del asesinato de la ambientalista, Guevara-Rosas fue enfática. "La vergonzosa ausencia de una investigación efectiva para hallar a quienes ordenaron el asesinato de Cáceres envía un aterrador mensaje a los cientos de personas que se atreven a manifestarse contra “los poderosos”, aseguró Amnistía Internacional.

**Para la ONG, la investigación del asesinato de Berta es vergonzosamente deficiente. **Hay ausencia de un mecanismo eficaz para proteger a testigos y otros defensores y defensoras de los derechos humanos, lo que muestra la falta de interés de las autoridades de Honduras para haya justicia. “Cada día que pasa sin que se haga justicia acerca a los y las activistas medioambientales hondureños es un paso más a un final trágico", advirtió la directora de AI.

### La investigación 

El **Grupo Asesor Internacional de Personas Expertas, GAIPE,** ** 122 agresiones en contra de integrantes del COPINH,** de los cuales 36 fueron directamente en contra de Berta. Cifra, que hasta el momento no se sabe si se ha investigado, en relación al asesinato de la ambientalista. De manera que desde el GAIPE se señala que "**aún no se tiene conocimiento de que el Estado Hondureño haya abordado integralmente las investigaciones** por los crímenes contra defensores y defensoras de derechos humanos, como tampoco lo ha realizado con respecto al asesinato de Berta Cáceres".

Y es que en medio de un año en el que la justicia hondureña parecía no querer esclarecer los hechos y pese a que no se ha dado con los autores intelectuales del crimen, la presión internacional, ha logrado  que se capture a ocho personas señaladas como responsables del asesinato. **Tres de ellos son militares hondureños, quienes habrían recibido entrenamiento militar por parte de los Estados Unidos,** según una investigación de The Guardian.

Se trata de **Mariano Díaz Chávez,** veterano de las fuerzas especiales hondureñas. Desde 2015 dirigía la inteligencia militar del país. Díaz y el teniente Douglas Giovanny Bustillo — también detenido por el asesinato y exempleado de la empresa responsable del proyecto hidroeléctrico — habrían recibido entrenamiento antiterrorista en EE.UU en 2005. Bustillo recibió entrenamiento en la infame Escuela de las Américas.

El tercer militar detenido, el sargento **Henry Javier Hernández,** es un francotirador y se convirtió en informante de la inteligencia militar cuando se retiró en 2013. Los tres visitaron el pueblo de La Esperanza, donde vivía Cáceres, en varias ocasiones, de acuerdo a los registros telefónicos.

Por otra parte, **Sergio Rodríguez,** ejecutivo del proyecto Agua Zarca también fue detenido. La empresa responsable del proyecto era Desarrollos Energéticos S.A. DESA, cuyo presidente, Roberto David Castillo Mejía, es miembro retirado de los servicios de inteligencia militar.

**Las revelaciones del expediente jurídico confirmarían que se trató de una ejecución extrajudicial** organizada por altos mandos del ejército hondureño, en respuesta a las acciones de resistencia de la activista en contra del proyecto hidroeléctrico Agua Zarca. [(Le puede interesar: Dos militares estarían implicados en asesinato de Berta Cáceres)](https://archivo.contagioradio.com/militares-berta-caceres/)

Además “los señalamientos también exhibirían el papel ambiguo del gobierno estadunidense en el entrenamiento de las fuerzas de élite hondureñas y la dotación de decenas de millones de dólares de ayuda militar al país, a pesar de las sospechas que los militares participen en asesinatos de activistas y líderes comunitarios”, resume el Proceso.

Desde el GAIPE, se espera que el Estado hondureño se levante el secretismo frente a la investigación, ya que de no hacerlo "las garantías judiciales de los sujetos procesales estarían seriamente agraviadas en este procedimiento penal y si no es así, se evidenciaría una grave discriminación con respecto a las víctimas del caso", dice la declaración pública del GAIPE.

### El legado de Berta 

"Las amenazas en mi contra siempre han sido por defensa del territorio y por el hecho de ser mujer, se me prohibió salir del país, querían prohibirme mi actividad política y organizativa" dijo la ambientalista  en la última entrevista con Contagio Radio.

Tras su asesinato, el proyecto hidroeléctrico se encuentra detenido. La familia de Berta y el COPINH continúan no solo exigiendo la verdad sobre la muerte de la ganadora del premio Goldman, sino que también **siguen luchando para que la empresa DESA se vaya definitivamente del territorio indígena.**

Pese a su asesinato la líder indígena continúa siendo reconocida por su labor en la defensa de la naturaleza. La Organización de las Naciones Unidas, ONU, le concedió en diciembre del año pasado, **el premio Campeón de la Tierra en la categoría de Inspiración y Acción,** en el marco de la 13 Conferencia de las Partes (COP13) del Convenio sobre Diversidad Biológica (CDB) de las Naciones Unidas. [(Le puede interesar: La lucha de Berta Cáceres es reconocida por la ONU)](https://archivo.contagioradio.com/lucha-berta-caceres-reconocida-la-onu/)

La lucha de Cáceres  logró que la compañía China Sinohydro, e incluso, el Banco Mundial renunciaran a continuar apoyando el proyecto hidroeléctrico en el Río Gualcarque, sitio sagrado para la etnia lenca.

Berta  luchó por un mundo en el que “pudiéramos vivir mejor”, cuenta su amiga Miriam Miranda, defensora de derechos humanos. Junto a ella, Berta trabajó hasta lograr la ratificación del Convenio 169 de la OIT para exigir el derecho a la consulta previa. Hoy su mensaje continúa inspirando las luchas ambientalistas en toda América Latina **“¡Despertemos¡ ¡Despertemos Humanidad¡ Ya no hay tiempo.”**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
