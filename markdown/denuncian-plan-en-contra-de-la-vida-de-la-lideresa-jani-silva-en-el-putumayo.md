Title: Denuncian nuevo plan para atentar contra lideresa Jani Silva en el Putumayo
Date: 2020-07-03 13:44
Author: CtgAdm
Category: Actualidad, DDHH, Mujer
Slug: denuncian-plan-en-contra-de-la-vida-de-la-lideresa-jani-silva-en-el-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-03-at-1.11.12-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves 2 de julio, de acuerdo con la **Comisión de Justicia y Paz, se conoció un nuevo plan de atentado en [contra la vida de la lideresa](https://www.justiciaypazcolombia.com/nuevos-planes-para-atentar-contra-lideresa-jani-silva/) Jani Silva por parte de una estructura criminal identificada como La Mafia**, en el departamento del Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia de la organización de DDHH, **el grupo armado estaría inconforme con el liderazgo de Silva, debido a su trabajo en torno a la construcción de paz** desde la promoción de la sustitución voluntaria de cultivos de uso ilícito, a pesar de los incumplimientos del gobierno sobre los planes de manejo y protección ambiental.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Jani Silva y la defensa de la vida

<!-- /wp:heading -->

<!-- wp:paragraph -->

Jani Silva, que hace parte del proceso organizativo de la Zona de Reserva Campesina de la [Perla Amazoníca](https://archivo.contagioradio.com/otra-mirada-del-feminicidio-y-la-violencia-contra-la-mujer/), ha impulsado la implementación integral del Acuerdo Final de Paz, **liderando una red interétnica de construcción de paz territorial que suma cerca de 110 procesos comunitarios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, recientemente ha **denunciado los incumplimientos del gobierno Nacional frente al PNIS y las afectaciones socioambientales generadas por la empresa británica Amerisur** en las comunidades de la Zona de Reserva Campesina de la Perla Amazoníca.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La violencia en contra de las y los líderes en Colombia no cesa

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este nuevo plan en contra de la vida de Silva es antecedido por las informaciones del mes de mayo en el que se señaló que Jani Silva fue **objeto de operaciones ilegales de inteligencia militar.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma es importante resaltar que en lo corrido de este año, está es la segunda denuncia que realiza la Comisión de Justicia y Paz, en donde alerta de **un plan en contra de la vida de la lideresa y otros tres líderes de la región**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estos hechos se suma el asesinato en el mes de marzo del líder Marco Rivanadeira, en medio de una de las reuniones que constantemente realizaba para agilizar la mesa de organizaciones sociales del departamento, en torno a los planes para adelantar la sustitución de cultivos de uso ilícito y frenar los daños provocados por la actividad de empresas petroleras en la zona. Lea también: [Las amenazas que enfrentan los líderes del Putumayo](https://archivo.contagioradio.com/en-putumayo-no-cesa-la-violencia-contra-la-comunidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se suma la presencia permanente y el control que realizan los grupos armados en medio de la fuerte presencia militar. Recientemente se ha denunciado que varios de los asesinatos cometidos e**n el departamento se llevan a cabo en los llamados [«recorridos de la muerte](https://archivo.contagioradio.com/asesinatos-puerto-guzman-desplazamientos/)» en los que sicarios a bordo de motocicletas asesinan a las personas en sus casas o cerca de ellas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a estás situaciones la **Comisión de Justicia y Paz exige a las correspondientes instituciones, cómo la Policía y la Unidad Nacional de Protección garantizar la seguridad de la lideresa**. Asimismo señala que existe una una inacción judicial y la ausencia de una investigación eficaz que posibilite el esclarecimiento y el desmonte de estructuras armadas que hoy están controlando el departamento de Putumayo, extendiendo su acción al departamento del Cauca.

<!-- /wp:paragraph -->
