Title: Radicado proyecto de Ley que prohibiría el Fracking en Colombia
Date: 2018-08-01 16:52
Category: Ambiente, Nacional
Tags: Alianza contra el Fraking, Alianza Verde, fracking, Fracking colombia
Slug: listo-proyecto-de-ley-que-prohibiria-el-fracking-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/FRACKING-COLOMBIA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gert Steenssens ] 

###### [01 Ago 2018] 

Con el respaldo de 30 congresistas pertenecientes a diferentes partidos políticos fue radicado el proyecto de ley que busca la prohibición de la exploración y explotación de yacimientos no convencionales de hidrocarburos a través de la técnica de fracturamiento hidráulico o** mejor conocida como Fracking**.

El proyecto que pasará a la Comisión Quinta de Cámara y Senado para ser revisada, pretende cuidar el ambiente, proteger las fuentes de agua, la salud pública, los recursos naturales y prevenir conflictos socio ambientales, señala Carlos Andrés Santiago, integrante de la **Alianza contra el Fraking. **(Le puede interesar:[ "¿Existe Fracking en Boyacá?"](https://archivo.contagioradio.com/existe-fracking-en-boyaca/))

Esta iniciativa esta compuesta de 8 artículos en los que se dictan medidas para la prohibición del Fracking a nivel nacional; ordena la realización de un informe sobre los impactos socio ambientales y de salud pública de la explotación de hidrocarburos, **promueve la diversificación energética y la transición a energías renovables**.

Frente a los posibles cambios que pueda sufrir el proyecto de Ley en el Congreso, Carlos Santiago señaló que esperan que el trámite durante este escenario, "no vaya a sufrir modificaciones sustanciales, porque sabemos que algunos congresistas son expertos en meter micos en las iniciativas ciudadanas".

Los congresistas que apoyaron este proyecto de ley hacen parte de** 9 fuerzas distintas de partidos como la Alianza  Verde**, el partido Conservador, la lista de Decentes, FARC, Liberales, entre otros; y una vez la iniciativa sea revisada por la Comisión Quinta se conocerá el ponente de acto legislativo.

<iframe id="audio_27491045" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27491045_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
