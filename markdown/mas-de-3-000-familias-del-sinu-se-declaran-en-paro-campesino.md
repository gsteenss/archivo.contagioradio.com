Title: Ante incumplimiento de PNIS, más de 3.000 familias del Alto Sinú se declaran en paro
Date: 2019-03-03 10:55
Category: Comunidad, DDHH
Tags: cordoba, paro campesino, PNIS, Sinú
Slug: mas-de-3-000-familias-del-sinu-se-declaran-en-paro-campesino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/photo5172757418034178068-1280x640.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Anzorc 

###### 2 Mar 2019

[Según un comunicado de la **Asociación Campesina para el Desarrollo –ASODECAS,  son casi 3.500 familias del Alto Sinú en Córdoba** las afectadas por los incumplimientos por parte del Gobierno frente a lo pactado en los acuerdos en el marco del Programa Nacional Integral De Sustitución De Cultivos De Uso Ilícito (PNIS), razón por la cual se declaran en *[movilización social campesina]*[permanente,]]

Tal como se especifica en el**[comunicado](http://anzorc.com/mas-de-3-mil-familias-en-paro-campesino-en-el-alto-sinu-por-incumplimientos-en-el-pnis/),** **el vocero de ASODECAS, Eduard Petro** indicó que a pesar de firmarse un acuerdo colectivo en abril de 2017, han sido dos años de incumplimiento por parte del Gobierno frente a la sustitución de cultivos pues la hoja de rutas que se planteo en aquel momento no ha ofrecido las garantías ofrecidas, “[realmente, tenemos problemáticas porque las familias no tienen los derechos económicos ni sociales que necesitan las familias en territorio”.]

[Según el vocero, la movilización que tuvo inicio el pasado 27 de febrero en el Alto Sinú es [“un paro político que referencia el inconformismo que tienen las personas en el territorio” razón por la que demandan se establezca una mesa de negociación con la **Junta de Direccionamiento Estratégico del PNIS, la gobernadora de Córdoba, Sandra Patricia Devia el alcalde de Tierralta,  un delegado de DD.HH. del Ministerio del Interior y la Defensoría** **del Pueblo.**]]

De igual forma los campesinos exigen la concertación de fechas y tiempos para el cumplimiento de los PNIS, llegar a un acuerdo para la generación de ayuda económicas para las familias que acogieron la medida y otorgar condiciones de seguridad para las comunidades y líderes del Alto Sinú. [(Lea también Cultivadores de Coca llevan seis meses esperando una reunión con el Gobierno)](https://archivo.contagioradio.com/cultivadores-coca-gobierno/)

###### Reciba toda la información de Contagio Radio en [[su correo]
