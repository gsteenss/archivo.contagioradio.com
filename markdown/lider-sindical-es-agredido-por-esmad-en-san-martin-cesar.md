Title: Líder sindical es agredido por ESMAD en San Martín Cesar
Date: 2016-10-28 13:02
Category: Movilización, Nacional
Tags: abusos Fuerza Pública, brutalidad policial, san martin
Slug: lider-sindical-es-agredido-por-esmad-en-san-martin-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/líder-sindical-San-Martín.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unión Sindical Obrera] 

###### [28 de Oct 2016] 

Después de la liberación del abogado de la Unión Sindical Obrera, Moisés Barón, capturado el pasado 26 de octubre junto con 7 personas más, mientras se desarrollaba el plantón pacifico en San Martín, Cesar,   se pudieron conocer las condiciones en las que se dieron las capturas y **los diferentes abusos y violaciones a derechos humanos cometidas por la Fuerza Pública durante la privación de su libertad.**

Barón expresó que se estaba como mediador entre la población civil de San Martín, que realizaban un plantón pacífico en contra de la entrada de maquinaria de la multinacional Conocophillips, y la Fuerza Pública, cuando **estos últimos comenzaron a lanzar gases lacrimógenos en contra de la comunidad indicando que los habitantes estaban bloqueando la vía**. Le puede interesar: ["Gases y aturdidoras del ESMAD contra de pobladores de San Martín"](https://archivo.contagioradio.com/gases-aturdidoras-del-esmad-pobladores-san-martin/)

Moisés relata que fue detenido en medio de la huida de las personas de la comunidad que no soportaban los gases, **“en ese momento yo me dirijo a comunicarme con los agentes para hacer una interlocución y buscar parar la situación, ahí soy detenido”**. El dirigente sindical, explico que fue tomado por el cuello y golpeado en la parte baja de la espalda y posteriormente llevado con los otros 7 detenidos. Barón señalo que además, después de la detención fueron constantemente golpeados por los agentes. **Él recibió dos golpes en la cara,  otro de los detenidos sufrió fractura de clavícula y uno más tiene contusiones en el cuerpo. **

Todos los capturados fueron puestos en libertad y según Moises Barón, eran menores de edad. El paso a seguir será instaurar demandas por lesiones personales. **“Los ciudadanos seguiremos reivindicando nuestros derechos, este pueblo no quiere el fraking** y esperamos que nuestro clamor sea atendido en el marco de las políticas públicas que reclamamos” afirmó el abogado. Le puede interesar: ["En medio de gases y lágrimas Conocophillips ingresa maquinaria a San Martín" ](https://archivo.contagioradio.com/en-medio-de-gases-y-lagrimas-conocophillips-logra-entrar-maquinaria-a-san-martin/)

<iframe id="audio_13520531" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13520531_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
