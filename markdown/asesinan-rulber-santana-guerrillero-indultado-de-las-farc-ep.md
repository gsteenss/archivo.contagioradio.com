Title: Asesinan a Rulber Santana guerrillero Indultado de las FARC-EP
Date: 2017-05-31 11:21
Category: DDHH, Nacional
Tags: acuerdo de paz, FARC-EP
Slug: asesinan-rulber-santana-guerrillero-indultado-de-las-farc-ep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Asesinado-integrantes-de-las-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 May 2017]

El guerrillero Rulber Santana, que había sido indultado, fue asesinado anoche en la jurisdicción de La Esmeralda, en el municipio de Puerto Rico, en Caquetá. Su cuerpo fue hallado con **dos impactos de arma de fuego a la altura del abdomen y el tórax**. El hecho se produjo cuando Rulber se movilizaba con una persona en moto, se desconoce hacía donde se dirigía.

Santana había salido de la cárcel del Cunduy, en Florencia, como parte de los indultos otorgados en el marco de los Acuerdos de Paz.  Con Rulber **ya son 3 los integrantes de las FARC-EP que son asesinados tras la firma del Acuerdo del Teatro Colón**, sin que se reporten avances en las investigaciones de los responsables materiales e intelectuales de los homicidios. Le puede interesar: ["José Yatacúe, integrante de las FARC-EP, fue asesinado en Toribio, Cauca" ](https://archivo.contagioradio.com/jose-huber-yatacue/)

De igual forma, familiares de los guerrilleros que se encuentran en las Zonas Veredales también han sido víctimas, en **Chocó fueron asesinados los hermanos Dalmiro Cárdenas y Anselmo Cárdenas,** hermanos del guerrillero de las Farc, Robinson Victorio recluido en la Cárcel de Chiquinquirá y en el departamento de Putumayo, se conoció del **asesinato de dos familiares** del guerrillero de las FARC conocido como Fabián García.  Le puede interesar: ["Asesinan en Putumayo a familiares de Guerrillero de las FARC-EP"](https://archivo.contagioradio.com/asesinan-familia-de-guerrillero/)

Noticia en desarrollo...
