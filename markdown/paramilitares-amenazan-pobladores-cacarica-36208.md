Title: Con lista en mano paramilitares amenazan pobladores de Cacarica
Date: 2017-02-13 13:05
Category: Nacional, Paz
Tags: cacarica, Operacion genesis, paramilitares
Slug: paramilitares-amenazan-pobladores-cacarica-36208
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paramilitares....png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [13 Feb. 2017] 

Paramilitares asentados de manera permanente en la comunidad Bijao en Cacarica, Chocó ingresaron a la Zona Humanitaria Nueva Esperanza, **allanando casa por casa y amenazando a los pobladores del lugar humanitario**. Lo más grave de la situación es que dicha incursión estaba avisada y las FF.MM. y otras instituciones del Estado no hicieron nada.

El hecho se presentó este domingo cuando **8 paramilitares que portaban armas largas,** iban vestidos con camuflados, bolsos negros y algunos no se identificaron ante la comunidad, **ingresaron a la Zona según un poblador casi de forma “vandálica e intimidaron a las familias”.**

Para la comunidad de Cacarica este escenario es bastante grave, dado que existe una responsabilidad del Gobierno **“esta nueva situación se está denunciando hace más o menos un mes y de urgencia tenemos 7 días** de estar alertando que esto iba a pasar” cuenta el habitante de la Zona Humanitaria. Le puede interesar: [Cerca de 650 Paramilitares ejercen control territorial en Cacarica](https://archivo.contagioradio.com/650-paramilitares-ejercen-control-en-cacarica-35568/)

Aunque para el momento de esta incursión paramilitar a la Zona no encontraron a las personas que estaban buscando, relata el poblador que amenazaron con volver “lo expresaron claramente, que se **iban pero que ellos iban a volver para buscar a las personas que tienen en la lista. Ellos insisten que están en la Zona Humanitaria”**.

Con base en la denuncia entregada por el poblador, las personas que estarían buscando los paramilitares son aquellas **quienes se han negado al reclutamiento forzado de estos grupos “entonces la idea es someterlos y de una u otra forma que trabajen con ellos”.**

De acuerdo con el habitante, la comunidad ha estado alertando a las autoridades ya hace tiempo “estamos pidiendo una comisión para verificar la situación y así evitar nuevos desplazamientos o que hubieran asesinatos o desapariciones en la región. Y se nos ha hecho caso omiso a esta información”.

Según la denuncia, los paramilitares ingresaron a la Zona Humanitaria de manera abrupta rompiendo el esquema de seguridad que esta tiene por lo que **“la gente ya está bastante atemorizada” relata el poblador. **Le puede interesar: [Comunidades del Cacarica sitiadas por presencia paramilitar](https://archivo.contagioradio.com/choco-paramilitares-gaitanistas/)

La comunidad asentada en este territorio, ha dicho el día de la incursión tuvieron comunicación con la Defensoría del Pueblo, quien le aseguró que las entidades estaban al tanto.

Tuvieron que pasar 8 horas una vez entregada la información sobre este suceso, para que la fuerza pública hiciera presencia en la Zona “**a la hora que llegó el Ejército, los paramilitares ya estaban lejos.** Estaban otra vez en la comunidad de Bijao” dice el poblador. Pese a ello, la comunidad exigió un control perimetral de las Fuerzas Militares, que no fue respondida.

Los pobladores completan 2 años denunciando las operaciones de los paramilitares dentro del territorio colectivo, y la ineficaz acción por parte de la fuerza pública que se encuentra a menos de 1 hora del lugar donde están ocurriendo los hechos.

Por ello, han insistido al Gobierno Nacional se desplace hacia la comunidad de Bijao para que “miren cómo sacan estos grupos de la Zona (…) porque **nuestras vidas están en riesgo, hay gente que no va al monte por temor, no pueden laborar**, entonces es una situación muy compleja” pidió el poblador.

**Pese al riesgo** en el que se encuentran las comunidades de Cacarica, **el próximo 25 de Febrero se darán cita para conmemorar 20 años de la Operación Génesis,** en la que los batallones de la Brigada 17 del Ejército, al mando del entonces general Rito Alejo Del Río Rojas, de la mano de paramilitares ocasionaron el desplazamiento forzoso de las comunidades y la tortura y asesinato de Marino López Mena.

**“Tenemos temor, pero eso no quiere decir que la comunidad va a echar atrás la fecha de la conmemoración**. Ojalá esta actividad pueda ser bien acompañada para que las familias se sientan en esos días un poco más tranquilas” concluyó el poblador. Le puede interesar: [Mujeres de Cacarica: 20 años espantando la guerra](https://archivo.contagioradio.com/mujeres-de-cacarica-20-anos-espantando-la-guerra/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
