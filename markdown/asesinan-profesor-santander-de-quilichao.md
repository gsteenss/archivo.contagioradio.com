Title: Asesinan a docente indígena en Santander de Quilichao, Cauca
Date: 2018-10-23 12:22
Author: AdminContagio
Category: Nacional
Tags: Cauca, Docentes, indígenas, lideres sociales
Slug: asesinan-profesor-santander-de-quilichao
Status: published

###### Foto: Archivo familiar 

###### 23 oct 2018 

Este lunes fue asesinado el profesor **José Domingo Ulcué Collazos**, perteneciente a la comunidad Nasa de **Santander de Quilichao, Cauca**. Según versiones el también comunero y protector de la madre tierra, fue atacado con impactos de bala, cuando se desplazaba entre los barrios La Esperanza y San Bernabé, al rededor de las 7:30 de la noche.

Jose Domingo se desempeñaba como docente de la Institución Educativa Benjamín Dindicué , sede Sek Dxij en la parcelación La Esperanza, donde **dictaba las clases de Agropecuaria, Ética y Valores.**  Quienes lo conocieron lo describen como un buen profesor, exigente, alegre y dedicado en su labor pedagógica.

**Luis Alberto Trochez**, Gobernador indígena de Munchique Los Tigres, resguardo al que pertenecía Ulcué, manifesto que "**es lamentable en este momento perder un comunero, un dinamizador nuestro, que venia apoyandonos hace 8 años** en el tema de la educación propia, en el fortalecimiento de nuestro idioma materno".

Según la información aportada por la comunidad, **Jose Domingo logró llegar con vida al hospital local donde falleció**. Hasta el momento no ha sido posible establecer si había sido objeto de algún tipo de amenazas directamente en contra de su vida. (Le puede interesar: [ESMAD agrede a pueblo indígena del Cauca: ACIN](https://archivo.contagioradio.com/esmad-agrede-acin/))

### **Los pueblos del Cauca resisten por el control territorial** 

Trochez asegura que la situación que viven actualmente es compleja **en el marco del control territorial por cuenta de los intereses del narcotráfico en la región**, y a pesar de que lleva poco tiempo como gobernador asegura que las autoridades ancestrales que le han antecedido en esa responsabilidad "**han recibido amenazas de todo tipo**" como también sobre los guardias indígenas y toda la comunidad.

En varias ocasiones los comuneros han interceptado cargamentos de marihuana y pasta de Coca, lo que el Gobernador señala como razón de las amenazas. "Nosotros como municipio de Tigre hemos trabajado día y noche **para que en nuestro territorio no entren grupos armados ilegales y tampoco de derecha**, nosotros hemos dicho las fuerzas militares pueden pasar, pueden patrullar pero no se pueden quedar viviendo".

<iframe id="audio_29548526" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29548526_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
