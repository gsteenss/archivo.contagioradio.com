Title: Más de 214 millones de mujeres en el mundo no acceden a métodos anticonceptivos
Date: 2017-07-11 17:38
Category: Mujer, Nacional
Tags: Anticoncepción, mujeres, Naciones Unidas
Slug: acceso-a-metodos-anticonceptivos-empodera-personas-y-desarrolla-naciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/derechos-sexuales-y-reproductivos-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Promsex] 

###### [11 Jul. 2017] 

A propósito del Día Mundial de la Población se está hablando del tema de la anticoncepción como derecho sexual y reproductivo de las personas y de manera especial de las mujeres, esto debido a que **en la actualidad cerca de 214 millones en el mundo de mujeres se sienten insatisfechas para acceder a los métodos anticonceptivos**, las barreras que existen son numerosas y van desde la falta de información, la negativa de las entidades prestadoras de salud para entregarlos a las mujeres o la falta de educación al respecto.

Además de estas barreras, otras **dificultades que enfrentan las mujeres son la posibilidad de decidir si desean o no tener hijos**, cuántos hijos desean tener y en qué momento de sus vidas, lo que según el Fondo de Población de las Naciones Unidas en Colombia, impide que millones de ellas tengan acceso a oportunidades laborales dignas y a una mejor calidad de vida.

Para **Jorge Parra, Representante del Fondo de Población de las Naciones Unidas** en Colombia la “anticoncepción es una herramienta para que las personas puedan ejercer sus derechos reproductivos, es decir decidir libre e informadamente cuántos hijos tener y cuándo”. Le puede interesar: [Mujeres de Mapiripán resisten al olvido tras 20 años de la masacre](https://archivo.contagioradio.com/masacre-de-mapiripan-obligo-a-las-mujeres-a-reconstruirse/)

### **Anticoncepción y proceso de paz en Colombia ¿cuál es la relación?** 

Parra añade que, si bien la Constitución y las normas nacionales defienden el ejercicio de los derechos sexuales y reproductivos, estas deben ser usadas en el contexto de paz en el que comienza a trabajar Colombia.

**“La paz implica un contexto de no violencia y la violencia sexual es un tema importante.** Los contextos humanitarios o de conflictos hacen que estos temas sean más prevalentes y aumenten los embarazos no deseados, porque no hay servicios apropiados de salud en esas zonas a los que las mujeres puedan acceder” recalcó Parra.

Razón por la cual dice el representante, se espera que esta nueva fase de paz, de no conflicto, favorezca que hayan mejores servicios, acceso a los métodos anticonceptivos y sobre todo que no haya violencia contra las mujeres. Le puede interesar: [Así participaron las mujeres en el Paro Cívico de Buenaventura](https://archivo.contagioradio.com/las-mujeres-en-el-paro-civico-de-buenaventura/)

### **Acceso a anticonceptivos empodera a las personas y hace países más desarrollados** 

El acceso a nivel mundial a los métodos anticonceptivos ha permitido que las mujeres se hayan desligado de la labor de la procreación, es decir, en la actualidad ya se piensa que las mujeres pueden mantener actividad sexual sin que eso redunde en un embarazo.

Esto lo que ha hecho es que las mujeres **“al disminuir su carga reproductiva, menos hijos que criar, puedan dedicarse a estudiar**, a prepararse para el trabajo y lógicamente a entrar a la actividad productiva de un país” cuenta Parra. Le puede interesar: [Investigación destaca aporte de las mujeres en el proceso de paz de La Habana](https://archivo.contagioradio.com/43151/)

Quien asevera que las naciones que presentan en la actualidad mayor crecimiento económico como por ejemplo el Sudeste Asiático, han logrado crecer por haber incorporado a las mujeres en las actividades productivas calificadas “y eso es aprovechar el potencial humano” recalca.

### **Recomendaciones al Estado y a la sociedad civil** 

Buena educación para la sexualidad, información sobre los métodos anticonceptivos y considerar que solicitar estos a un sistema de salud no es un favor, son algunas de las recomendaciones que el Fondo de Población de las Naciones Unidas deja en la mesa y asegura son necesarias para tener mejores países.

“**Los países latinoamericanos carecen de una buena educación para la sexualidad,** una educación que no es educar para usar métodos anticonceptivos sino educarse en valores y en una sexualidad responsable. (…) Además es de recordar que las mujeres colombianas, a través de cualquiera de los sistemas de salud tienen el derecho a exigir que se les provea de información y de los métodos anticonceptivos apropiados” concluye Parra. Le puede interesar: Le puede interesar: ["Participación de las mujeres: una clave para la construcción de paz con justicia social y ambiental"](https://archivo.contagioradio.com/la-participacion-de-las-mujeres-una-clave-para-la-construccion-de-paz-con-justicia-social-y-ambiental/)

### **La anticoncepción en cifras** 

En Colombia, el acceso a los métodos anticonceptivos modernos ha incrementado en los últimos años. En las mujeres unidas, es decir **casadas, su uso ascendió de 72,9% en el 2010 a 75,9% en el 2015 y en las mujeres no unidas,** es decir sin estar casadas pero con actividad sexual reciente, su uso pasó de 75,1% en el 2010 a 77,7% en el 2015. Le puede interesar: [“Mujeres en Colombia están viviendo una tragedia humanitaria”: FDIM](https://archivo.contagioradio.com/mujeres-en-colombia-estan-viviendo-una-tragedia-humanitaria-fdim/)

Así mismo, Según la Encuesta Nacional de Demografía y Salud del año 2015, la prevalencia de necesidad insatisfecha más alta está entre las mujeres de 15 a 29 años y en departamentos como Chocó, Guainía, La Guajira, Vaupés, lugares que también tienen altos niveles de pobreza del país.

Por su parte el promedio ideal de fecundidad es de dos hijos o hijas por mujer. El 31% de las mujeres que tienen dos hijos o hijas, no desea tener más, y el 9,7% desea otro en dos años o más.

<iframe id="audio_19753314" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19753314_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
