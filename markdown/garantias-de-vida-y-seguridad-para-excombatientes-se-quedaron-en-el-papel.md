Title: Garantías de vida y seguridad para excombatientes se quedaron en el papel
Date: 2020-02-04 18:23
Author: AdminContagio
Category: Expreso Libertad
Tags: ex combatientes FARC, excombatientes, Expreso Libertad, FARC, INDEPAZ
Slug: garantias-de-vida-y-seguridad-para-excombatientes-se-quedaron-en-el-papel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/asesinados_farc-e1522961371379.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El 2019 fue el año con mayor asesinatos a excombatientes en Colombia con 73 homicidios, hecho que evidencia la falta de garantías para la vida y seguridad de quienes firmaron el Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esta situación se suma la reciente alarma que activaron 62 integrantes del **ETCR** de Santa Lucía en Ituango, señalando el alto riesgo de desplazamiento forzado en el que se encuentran, producto de la falta de acciones por parte del Gobierno para brindar protección a los firmantes de la paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa de Expreso Libertad, Manuel Garzón, defensor de derechos humanos e integrante del partido **FARC** y Leonardo González integrante de **Indepaz**, analizaron la situación actual del Acuerdo de paz y las causas de la violencia en contra de las y los firmantes de Paz. 

<!-- /wp:paragraph -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F3828305397179662%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<!-- wp:paragraph -->

[Mas programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/):

<!-- /wp:paragraph -->
