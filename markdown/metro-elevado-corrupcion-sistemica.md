Title: "El Metro Elevado de Peñalosa es el mejor ejemplo de corrupción sistémica" Hollman Morris
Date: 2018-09-13 17:13
Author: AdminContagio
Category: Entrevistas, Política
Tags: Bogotá, Concejo de Bogotá, Metro para Bogotá, Transmilenio
Slug: metro-elevado-corrupcion-sistemica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Peñametro-e1504548847329.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [13 Sept 2018] 

[El pasado 12 de Septiembre se llevó a cabo el debate sobre la construcción del Metro elevado en Bogotá, en donde el concejal Hollman Morris realizaró varias denuncias que argumentaban **presunta ilegalidad y corrupción** en los trámites del proyecto, además de costos duplicados y estudios realizados para el Metro Subterraneo que se están aplicando a un Metro Elevado, sin que exista ningún control por parte de las autoridades encargadas. ]

<iframe src="https://co.ivoox.com/es/player_ek_28587415_2_1.html?data=k52impyYdZahhpywj5adaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncanjzdHaw9OPkdDm087gjdjTptPZjMrZjcnJpsLoxpDT1MrSuMafwtGYz8rYttCfxcqYssqJh5SZo5bOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Julio Cesar Acosta, concejal  por Cambio Radical aseguró que **el Metro elevado es una realidad, y que su costo es mucho menor en comparación del  Metro subterráneo**, del otro lado, el concejal progresista, Hollman Morris, afirmó que los cambios en la estructuración del proyecto son ilegales, y **los estudios requeridos para su adjudicación no existen.**]

### **El metro elevado podría costar mas del doble de lo planeado** 

[Durante el debate, Morris expuso pruebas entre las que se encontraban la aprobación de los estudios para el metro subterraneo durante las gestiones de Bogotá Humana, pero en el documento, modificado por la actual Alcaldía, se cambió la cláusula de “alcance y estudio” con la finalidad **modificar los estudios relacionados a un Metro Subterráneo para aplicarlos en la construcción del Metro Elevado**, acto que para Morris, se describe como ilegal y falto de lógica.  (Le puede interesar: [Se venció el plazo para Metro de Bogotá)](https://archivo.contagioradio.com/plazos-metro-bogota/)]

[Entre las pruebas también se encontraba el presupuesto final para la ejecución de este medio de transporte masivo, en donde se reveló que el proyecto tiene un costo de **entre 27 a 30 Billones de pesos, no de 12 billones como lo había asegurado la Alcaldía.**]

[Morris manifestó su descontento y afirmó que el Metro Elevado no tiene “pies ni cabeza” y que se basa en una “cadena de ilegalidades”. Aseguró que con el dinero de la construcción de esta estructura, que según él, no beneficiará a nadie más que a los funcionarios, **se podrían hacer 4 reformas tributarias o construir 2 Hidroituango.**]

### **Bogotá, la “capital mundial de Transmilenio”** 

<iframe src="https://co.ivoox.com/es/player_ek_28587460_2_1.html?data=k52impyYepGhhpywj5aUaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncavpzc7cjabHs9TowoqfpZCns8_Xxs_OzpDIqYy20Mzc1oqnd4a1kpCww9LGrdCfs8bRy8jFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El Concejal Acosta sostuvo que el presidente Iván Duque aprobó los fondos necesarios para la construcción de las troncales de Transmilenio que van por la **Carrera 7, la Avenida 68 y la Avenida Cali,** y según el, la construcción se iniciará lo más pronto posible. Acosta asegura que con más estaciones se garantiza una supuesta mejora en la calidad del servicio.

[Por lo contrario Hollman, afirmó que ninguno de los proyectos de construcción de las troncales tiene estudios que comprueben su factibilidad, y no debería continuar la ampliación del sistema “[este] medio de transporte no tiene nada que ver con una ciudad desarrollada del siglo XXI, Colombia podría tener el mejor metro de América Latina, el problema es que **11 millones de Colombianos votaron por la corrupción, es pura carreta y doble moral lo que hablan en este país**”.]

[Finalmente el concejal Hollman confirmó que el debate se va a plantear en el congreso y ampliarán las pruebas para la ejecución de la denuncia penal contra el Alcalde de Bogotá “**este proyecto está tomando los mismos pasos que Hidroituango,** si seguimos con esta corrupción sistémica. El mejor Metro para Bogotá es el que cumple con la ley”. A pesar de las evidencias presentadas, Según Andrés Escobar, gerente del Metro de Bogotá, **la obra se adjudicará en agosto del 2019.** (Le puede interesar:[Los millonarios contratos del gerente de un Metro que no existe](https://archivo.contagioradio.com/los-millonarios-contratos-del-gerente-de-un-metro-que-no-existe/))  ]

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
