Title: 4 indígenas heridos y 2 detenidos deja represión de ESMAD en vía Cali Buenaventura
Date: 2015-10-13 15:19
Category: Movilización, Nacional
Tags: 12 de octubre, ACIVAR.P, Feliciano Valencia, Movimiento Indígena en Colombia, ONIC, ORIVAC, Protestas en Buenaventura, Represión contra los indígenas
Slug: 4-indigenas-heridos-y-2-detenidos-deja-represion-de-esmad-en-via-cali-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/indigenas_buenaventura_contagioradio-e1490306482847.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: orivac 

<iframe src="http://www.ivoox.com/player_ek_8964041_2_1.html?data=mZ6jlpWYdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiReIzdz8mSpZiJhaXbxtPO1ZDMqdPdxdTgjd6PdozYxtnS0M7Is9SfxcrXw5DWqdHmxtjWh6iXaaOnz5DRx5CpcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Fernando Chirimia] 

###### [13 oct 2015]

Las protestas que se presentan desde el pasado viernes en la vía que comunica Cali- Buenaventura y **participan aproximadamente 600 indígenas** pertenecientes a los pueblos indígenas Wouanaan Nonam, Embera Chamí, Eperara Siapidara y Nasa, en Buenaventura,  protestan con tres exigencias a las cuales el gobierno nacional a respondido con la presencia de la fuerza pública y la violencia.

Fernando Chirimia, integrante de la comunidad, indicó los puntos alrededor de los cuales giran las exigencias de las comunidades que protestan por el cumplimiento de sus derechos

-   "Respeto de jurisdicción indígena en sus territorios, que haya respeto hacia las comunidades para que ellos manejen sus presupuestos o sus organizaciones manejen sus presupuestos"
-   **"Liberación del líder indígena Feliciano Valencia**, apoyo para que el gobierno y la Fiscalía General de la Nación den libertad a Feliciano Valencia. La exigencia a la corporación regional CVS para que las comunidades indígenas puedan manejar sus propios recursos y tengan la posibilidad que los proyectos vayan a diferentes comunidades".
-   "Que la administración Distrital de Buenaventura para que la encargada a alcaldesa de viabilidad de los presupuestos para que los proyectos que se han firmado vayan a las comunidades indígenas a trabajar con sus respectivos recursos".

Las protestas que continúan el día de hoy **no han recibido atención por parte de delegados del gobierno** que de la única forma que se ha hecho presente es con la presencia de la fuerza pública y el ESMAD “de parte del gobierno trata de invalidar e injustificar que nuestra jurisprudencia indígena no sirve para nada”, indica Miller Mejía representante legal de la organización ACIVAR.P.

Los enfrentamientos con la fuerza pública presentan **al día de hoy han dejado 4 detenidos y 2 miembros de la comunidad en grave estado de salud**, además con agresiones a miembros de la guardia indígena menores de edad y niños pertenecientes a la misma“**la policía sigue provocando donde hay niños simplemente niños de 4 a 5 años**”, afirma Mejía. A esto se le suman denuncias por parte de la comunidad de que el ESMAD, está arrojando "Papas bombas" contra las personas que protestan.

Con esta situación las autoridades indígenas han decidido irse a asamblea permanente “porque esta es una exigencia nacional,las autoridades están empezando llamar a más personal” ya que “**sin los justos derechos no nos vamos a los territorios**”, afirma Miller Mejía, representante legal de ACIVAR.P.
