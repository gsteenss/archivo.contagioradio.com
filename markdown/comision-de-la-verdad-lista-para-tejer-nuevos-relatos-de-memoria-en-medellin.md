Title: Comisión de la Verdad lista para tejer nuevos relatos de memoria en Medellín
Date: 2019-04-12 21:51
Author: CtgAdm
Category: Memoria, Paz
Tags: Asesinato de líderes comunales, lideres sociales, Macarena, Meta
Slug: comision-de-la-verdad-lista-para-tejer-nuevos-relatos-de-memoria-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Comisión.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ComisiónVerdadC] 

Desde el pasado 10 de abril, la Comisión de la Verdad dio apertura a una de las 19 Casas que se establecerán en puntos estratégicos del territorio colombiano, esta vez en la ciudad de  Medellín, segundo espacio en la región cafetera que permitirá a los habitantes del departamento de Antioquia compartir sus historias y vivencias concernientes al conflicto armado. La primera de estas casas, ubicada en **Apartadó fue inaugurada el 22 de marzo y ya está en funcionamiento.**

El acto de inauguración tuvo lugar en el Museo de Antioquia, en el que se dieron cita **el comisionado Alejandro Valencia, el coordinador regional Max Yuri Gil, el coordinador de Medellín Rubén Darío Jaramillo**, delegados internacionales y diversas organizaciones sociales, quienes celebraron con una serie de conversatorios la apertura de este espacio en la ciudad. [(Le puede interesar: Antioquia y Eje Cafetero, listos para trabajar junto a la Comisión de la Verdad) ](https://archivo.contagioradio.com/antioquia-y-eje-cafetero-listos-para-aportar-sus-relatos-a-la-comision-de-la-verdad/)

Por su parte, **el presidente de la Comisión, padre Francisco de Roux,** resaltó el rol fundamental de las Casas de la Verdad en la construcción de la paz, "tenemos que hacer una tarea, la estamos haciendo comprometiendo lo mejor de nosotros mismos y confiando en que las cosas en Colombia permitan que vayamos hacia un futuro en el que quepamos todos, por lo menos para dejar las bases de algo que es irreversible", indicó.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
