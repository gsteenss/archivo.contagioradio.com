Title: Demain, acciones para salvar el planeta
Date: 2016-08-11 12:28
Author: AdminContagio
Category: 24 Cuadros
Tags: cambio climatico, Documentales ambiente
Slug: demain-acciones-para-salvar-el-planeta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Mañana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Demain Doc 

##### [11 Agos 2016]

Las visiones apocalípticas sobre el destino del planeta por causa de la crisis ecológica, económica y social a nivel global, motivaron al escritor y activista **Cyril Dion** y a la actriz y cantante **Melanie Laurent** a producir "**Demain**" (Mañana), un documental colaborativo que recoge algunas de las iniciativas que propenden por disminuir el impacto de la actividad humana en la Tierra.

"**Demain**", contiene entrevistas realizadas en 10 países del mundo, donde las acciones de la gente están reinventando la agricultura, la energía, la economía la democracia y la educación. Voces y rostros entre los que se cuentan personajes como la líder ecologista y feminista indú **Vandana Shiva**, el fundador del movimiento Transition Towns (Ciudades en transición) **Rob Hopkins** y el pionero de la agricultura ecológica en Francia, promotor del movimiento de los Colibris **Pierre Rabhi**.

https://www.youtube.com/watch?v=bmL89rs0mSI

Desde el documental se proponen **5 cambios** que a escala individual, colectiva y política podrían generar transformaciones significativas en el destino de la humanidad, relacionadas con la manera en que se producen y se consumen los alimentos; la disposición y uso de energías, la preferencia por los sistemas alternativos de comercio y de educación; y la participación ciudadana activa en la construcción de políticas favorables a la producción responsable y disminución de impactos ambientales.

El proyecto de Dion y Laurent, contó con el apoyo de mas de 10 mil personas que a través de una plataforma virtual de crowfounding, lograron juntar 200.000 euros en 2 días y 444.390 en los dos meses que fijaron como cierre de la campaña, convirtiéndose en cifra récord para una propuesta documental financiada bajo esta modalidad.

La cinta, ganadora en la más reciente edición de los premios Cesar como mejor documental, fue estrenada en Francia a finales del año anterior y ahora **llega a las salas colombianas** en únicas funciones el jueves 11 y viernes 12 de agosto a las 8:30 p.m. y el domingo 14 de agosto a las 12 del medio día.
