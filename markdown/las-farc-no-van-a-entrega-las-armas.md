Title: Las Farc no van a entregar las armas
Date: 2015-09-25 16:57
Category: yoreporto
Tags: Acuerdo de Justicia, entrega de armas, proceso de paz, Quintin Lame
Slug: las-farc-no-van-a-entrega-las-armas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [25 de Sep 2015 ] 

#### **[Yo Reporto -  Xiomara Taborda]** 

Tal como se lee, la guerrilla de las FARC no va entregar las armas, las va a dejar. Pero cualquier incauto ciudadano colombiano pensaría, tal como quiere que piense personas como el senador Álvaro Uribe, que la guerrilla al no entregarlas sino dejarlas, lo que hará será mantenerlas y usarlas cuando quiera. {1}   Déjeme comunicarle que o el senador y ex presidente, no cuenta con buenos asesores a su alrededor que lo informen bien acerca de los estándares internacionales de desarme, desmovilización y reintegración, o con toda la intensión lo que quiere es confundirlo respecto a estos procesos en el mundo.

El desarme no viene solo, este se acompaña de una desmovilización y una reintegración y son parte fundamental, no aislada, dentro de un proceso de paz como el que asiste a Colombia hoy, justamente el desarme es lo más importante no solo para el acuerdo, sino para la estabilidad posterior al mismo.

Existen varias referencias guía elaboradas por diferentes organismos y países para su efecto; las Naciones Unidas por ejemplo, en su manual cartilla ‘los Estándares Integrados de Desarme, Desmovilización y Reintegración’  entiende el desarme como “‘la recolección, documentación, control y eliminación de armas pequeñas y ligeras, municiones y explosivos de combatientes y, a veces, de la sociedad civil.”

La experiencia internacional incluso la nacional, indica que la dejación de armas significa fundamentalmente que las armas de los combatientes guerrilleros no van a parar en manos del Estado, por el contrario estas son entregadas a un tercero para que sean destruidas o resguardadas, incluso ha habido experiencias en las que el mismo grupo que deja las armas  las destruye en presencia de un tercero como un veedor y testigo. {2}

En Colombia han existido varios ejemplos y experiencias en este sentido, nombrare apenas tres. En 1990 se firma el pacto entre el Gobierno colombiano y el M19, esta guerrilla solicita una veeduría internacional europea, la cual fue la Internacional Socialista que vino a Colombia para fundir las armas que entregó en aquel entonces el grupo desmovilizado.

El EPL después de proponer la constituyente y al hacerse ésta realidad, en el marco del pacto entre las partes, crean tres listados previos con el inventario del armamento, explosivos y material de guerra para que las armas fueran posteriormente entregadas a una comisión mixta, conformada por la Internacional Socialista y miembros de la Asamblea Nacional Constituyente. Las armas fueron fundidas y con estas se construyó un parque y un monumento a la paz en la ciudad de Medellín.

La tercera experiencia es la del Quintin Lame, unos comandos de autodefensa indígena que operaron en sus territorios y no pretendían la toma del poder, los indígenas entregaron armas  a través  del Consejo Mundial Indígena y los veedores fueron también las autoridades de sus resguardos.

Sin duda el desarme no es solamente un asunto operativo dentro de un proceso de paz, todo lo contrario, es asunto de vital importancia, pues sin su cumplimiento no se lograría el objetivo de desescalar el conflicto y controlar efectivamente el uso privativo de armas por parte del Estado Colombiano. De igual manera este proceso de  desarme, desmovilización y reintegración necesita de la confianza mutua entre dos actores que se han reconocido como adversarios legítimos, que no se derrotaron militarmente y que necesitan reafirmar con actos de este tipo la confianza de los ciudadanos incautos o no, para que en lo venidero exista una estabilidad política que permita la construcción de la verdadera paz en Colombia.

###### {1} Preocupaciones iniciales sobre el acuerdo entre Gobierno y FARC’. En: [http://www.centrodemocratico.<wbr></wbr>com/atencion-preocupaciones-<wbr></wbr>iniciales-sobre-el-acuerdo-<wbr></wbr>entre-gobierno-y-farc/](http://www.centrodemocratico.com/atencion-preocupaciones-iniciales-sobre-el-acuerdo-entre-gobierno-y-farc/)  

