Title: Audiencia pública busca frenar 'El Quimbo'
Date: 2016-11-09 16:27
Category: Ambiente, Nacional
Tags: ASOQUIMBO, audiencia pública ambiental, Hidroeléctrica El Quimbo
Slug: audiencia-publica-busca-frenar-quimbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/quimbo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 Nov de 2016] 

El 11 de Noviembre la Mesa Temática por la Defensa del Territorio intentará frenar el proyecto hidroeléctrico "El Quimbo". Esta mesa conformada por más de una decena de organizaciones sociales, campesinas e indígenas, afirma que el Quimbo ha dejado más de 1500 familias desplazadas, **destrucción del hábitat de 179 especies de animales y la destrucción de hallazgos arqueológicos y arquitectónicos, en 6 años de intervención.**

Otro de los impactos más doloroso ha sido la destrucción del patrimonio arquitectónico como la capilla de San José de Belén y graves **perdidas de hallazgos arqueológicos como piezas de barro y restos de cementerios indígenas ubicados en las laderas del río Magdalena.** Según los habitantes no solamente se está destruyendo la vida sino también la historia de la región.

Los municipios de **Gigante, El Agrado, Garzón, Tesalia, Altamira y Paicol** **han sido los más afectados** por la hidroeléctrica que abarca hasta el momento un total de 8.586 hectáreas.

La Mesa Temática para la verificación, promoción, conservación del Medio Ambiente, el Agua y el Territorio’ ** tuvo sustento jurídico en el decreto 489 de 2016** emitido por la Gobernación del Huila. (Ver abajo) que es un escenario en que las comunidades intentan llegar a consensos para salvar lo que sea salvable, reparar los daños e indemnizar a los afectados desde las necesidades propias y no bajo el criterio de las empresas ENEL y ENDESA, como se ha pretendido hasta el momento.

### **Las exigencias a los responsables del Quimbo** 

La mesa exige a la ANLA que derogue la licencia ambiental otorgada a Emgesa S.A, que se realice un nuevo censo de afectados y **se cumpla lo ordenado por la Sentencia T-135/13 de la Corte Constitucional, que respalda los derechos de las comunidades a un territorio y vida dignos.** Le puede interesar: El Quimbo [ha dejado 1500 familias en la pobreza absoluta.](https://archivo.contagioradio.com/hidroelectrica-el-quimbo-deja-pobreza-absoluta/)

Es por estas razones que la Mesa temática exige además de la revocatoria de la licencia, **la indemnización a las comunidades afectadas y la creación de una Comisión de la Verdad para que no se repitan estos hechos.** Le puede interesar: [Uribe, Santos y ANLA los responsables del ecocidio en el](https://archivo.contagioradio.com/uribe-santos-y-anla-los-responsables-del-ecocidio-en-el-quimbo/)Quimbo

Se espera que los avances en producción de conocimiento técnico científico y jurídico por la defensa de los derechos humanos de las comunidades, generados por la mesa temática, sean **la base para implementar nuevos mecanismos de concertación, conciliación y además sean puente para promover formas de mitigación y compensación de los daños.**

La invitación es a participar de la Asamblea que se realizará el 11 de Noviembre en el **Centro Recreacional Manilla, en Garzón Huila a partir de  las 8:A.M.**

[Decreto Prodefensa Del Territorio 2016](https://www.scribd.com/document/330544511/Decreto-Prodefensa-Del-Territorio-2016#from_embed "View Decreto Prodefensa Del Territorio 2016 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_57738" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/330544511/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-AR0qxPPBk9sSB0P8GMTn&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6536231884057971"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)
