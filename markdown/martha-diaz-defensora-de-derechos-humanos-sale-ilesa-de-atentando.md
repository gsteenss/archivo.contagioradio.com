Title: Martha Díaz, defensora de Derechos Humanos, sale ilesa de atentando
Date: 2016-11-25 15:32
Category: DDHH, Nacional
Tags: atentados contra defensores, víctimas
Slug: martha-diaz-defensora-de-derechos-humanos-sale-ilesa-de-atentando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/martha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [25 Nov 2016] 

La líder social y defensora de Derechos Humanos **Martha Díaz**, fue víctima de un atentado el jueves 25 de noviembre en Barranquilla, mientras se desplazaba en su camioneta. Los hechos se reportaron en la mañana cuando dos sujetos **dispararon al vidrío posterior del vehículo, que afortunadamente era blindado y detuvo los impactos de bala, permitiendo que Díaz saliera ilesa del atentado.**

Martha Díaz, además es **madre de uno de los jóvenes víctimas de ejecuciones extrajudiciales en Barranquilla** y hace parte de la Asociación de Madres Unidas Por Un Solo Dolor (AFUSODO), organización defensora de derechos humanos que reúne a  madres víctimas de los mal llamados “Falsos Positivos”. De igual forma hace parte del Movimiento de Víctimas de Crímenes de Estado (MOVICE).

Sin embargo este no es el primer hecho que involucra agresiones contra Martha, **en años pasados, esta defensora ha sido víctima de diferentes amenazas**, la última llego vía correo el 30 de octubre del año 2013, en donde la acusaban de ser guerrillera. Le puede interesar:["Estos son los líderes asesinados o perseguidos de Marcha Patriótica"](https://archivo.contagioradio.com/estos-son-los-lideres-campesinos-asesinados-o-perseguidos-de-marcha-patriotica/)

Con este atentado son tres los hechos que se presentan en tan solo noviembre, en contra de la vida de defensores de derechos humanos en el país. Situaciones que evidencia la escalada de este tipo de agresiones, de forma sistemática hacia líderes del movimiento social y defensores. Le puede interesar: ["Estamos frente a un nuevo Genocidio: Carlos Lozano"](https://archivo.contagioradio.com/estamos-frente-a-un-nuevo-genocidio-carlos-lozano/).

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
