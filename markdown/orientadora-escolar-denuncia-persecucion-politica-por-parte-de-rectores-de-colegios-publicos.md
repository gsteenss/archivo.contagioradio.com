Title: Orientadora escolar denuncia persecución política por parte de rectores de colegios públicos
Date: 2018-05-22 15:58
Category: Nacional, yoreporto
Tags: carta, Docentes
Slug: orientadora-escolar-denuncia-persecucion-politica-por-parte-de-rectores-de-colegios-publicos
Status: published

###### [Foto: Contagio Radio] 

###### [17 May 2018] 

La Secretaría de Educación Distrital, los rectores, y de manera particular el rector del Colegio Ricaurte, Guido Caicedo, se amparan en una norma inexistente, (aplicación de parámetro: relación de niños por orientador, desestimando que el Ministerio de Educación, no aplica parámetro en orientación) para  sancionar, para acallar el pensamiento distinto.

Luego de 12 años y 6 meses de labores, en el Colegio Agustín Nieto caballero, Institución del Distrito,   el rector de este colegio expresó en el mes de febrero de 2017, que me seleccionaba para traslado, porque no compartía la política educativa, que se estaba aplicando en el colegio (he sido crítica de la política educativa de esta administración y de las anteriores). El traslado se hace efectivo en Agosto de 2017.

Así  llegué al Colegio San Francisco de Asís  y la rectora, hizo  la  exigencia de  laborar en jornada global, cuando en mi historial laboral ha sido la jornada mañana, esta negativa conlleva nuevo traslado para el colegio Ricaurte, y desde mi llegada, en Febrero del presente año, el rector aduce no tener “parámetro” para recibir otra orientadora.

Como consecuencia de la postura del rector, la SED, al regreso de Semana Santa, me realiza de nuevo traslado para otro colegio. Presento  mi inconformidad a la SED, y ésta entidad, responde que: “validada la planta de Personal Docente se encuentra que, por un error técnico se reportó una vacante en el área de Orientación”. ¿Es posible desaparecer una plaza, y dejar unos niños y jóvenes sin orientadora escolar, en una población tan vulnerable como la de Mártires, sólo por persecución política? ¿Es decir, así como el Colegio Agustín Nieto (ubicado cerca al Bronx) perdió una plaza de orientación, ahora la historia se va a repetir en el  Colegio Ricaurte?

Lo expuesto es vulneración flagrante de mis derechos, tres traslados en tan breve lapso de tiempo  me han creado inestabilidad laboral y emocional, amparándose en norma inexistente, han afectado mis derechos constitucionales como el debido proceso, el derecho de expresión, el trabajo en condiciones dignas y humanas.

La Secretaría de Educación Distrital, la Dirección Local de  Mártires, los rectores, de manera especial el rector del colegio Ricaurte, no son conscientes de tantas afectaciones a los derechos de los educadores en función de orientación, y de las afectaciones a la atención de los niños y jóvenes  con la problemática tan dura en esta población. En momentos en los cuales en el país se habla de proceso de paz y la SED, la DILE Mártires y los rectores dicen estar comprometidos. Así se resuelven las diferencias, discriminando, maltratando, vulnerando derechos?

### Mercedes Trujillo Yara 

Orientadora Escolar

###### Contagio Radio no se hace responsable de lo señalado en este artículo, es una nota de una persona particular. 
