Title: Comisión de la Verdad abre convocatoria para integrante del equipo
Date: 2020-01-23 14:58
Author: CtgAdm
Category: eventos, Paz
Tags: Alfredo Molano, comision de la verdad, Convocatoria, paz
Slug: comision-de-la-verdad-abre-convocatoria-para-integrante-del-equipo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Comisión.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ComisiónVerdadC

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 23 de enero la Comisión para el Esclarecimiento de la Verdad, la Convivencia y la No Repetición, abrió públicamente la convocatoria para la selección de un nuevo comisionado o comisionada que pueda reemplazar a Alfredo Molano quien falleció el 31 de octubre del 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El proceso de selección tendrá siete etapas, las cuales se desarrollarán en un plazo de cuatro meses, iniciando el 23 de enero con la apertura de la convocatoria, hasta el 1 de abril del 2020 con la publicación del nombre de la nueva comisionada o comisionado de la verdad.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComisionVerdadC/status/1220427635886641152?s=19","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComisionVerdadC/status/1220427635886641152?s=19

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El próximo representante de la Comisión debe cumplir varios requisitos, entre ellos contar al menos con 10 años de experiencia relacionadas con las funciones del cargo, *"****deber ser imparcial, desinteresado y tener conocimiento del conflicto armado colombiano, de la historia y situación del país"*** Francisco de Roux**,** presidente de la Comisión de la Verdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma De Roux aclaró, *"esta convocatoria no es para encontrar un remplazo a Alfredo, a el no podemos sustituirlo como persona, no podemos esperar tener la calidad de compañero y amigo que teníamos, pero si estamos buscando un perfil similar de comisionado".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El trabajo de Alfredo Molado como comisionado debe continuar

<!-- /wp:heading -->

<!-- wp:paragraph -->

La labor que asume el nuevo integrante de la Comisión debe ir de la mano con el proceso que había desarrollado Alfredo Molano recopilando testimonios en la Orinoquía y la Amanazonía. (Le puede interesar: <https://archivo.contagioradio.com/alfredo-molano-un-caminante-de-la-verdad/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Teniendo en cuenta que el trabajo como investigador  y cronista de Molano le permitió dar a conocer al mundo la violencia, el desplazamiento forzado y las problemáticas existentes en la ruralidad del país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El primero de abril se conocerá el nuevo integrante de la Comisión de la Verdad**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 18 de marzo se revelarán los nombres de los aspirantes que entrarán a la fase de preselección, etapa donde serán solo seleccionados 10 candidatos como resultado de la observación ciudadana y el estudio por parte del pleno de la Comisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos nombres se darán a conocer el 25 del mismo mes y solo 5 días después serán llamados a entrevista con la totalidad de los actuales comisionados, quienes se encargarán de seleccionar al nuevo integrante cuyo nombre será dado a conocer el 1 de enero del 2020. (Le puede interesar:<https://comisiondelaverdad.co/actualidad/blogs/alfredo-molano-bravo>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asímismo la Comisión tendrá en cuenta en los candidatos criterios como; su experiencia nacional o internacional en terreno trabajando temas como la reconstrucción de la memoria y la verdad en contexto de justicia transicional, reconciliación, convivencia, resolución de conflictos y construcción del tejido social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma en su experiencia debe destacar su trabajo por la promoción de los Derechos Humanos, Derechos de las víctimas; y el conocimiento para capacitar o formar en temas como construcción de paz, género y asuntos étnicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último tendrán en cuenta que esta persona haya realizado investigaciones y/o publicaciones sociólogas, históricas, antropológicas o periodísticas en los temas anteriores. (Le puede interesar: <https://archivo.contagioradio.com/pueblo-bello-30-anos-comprometidos-con-la-busqueda-de-sus-seres-queridos/>)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
