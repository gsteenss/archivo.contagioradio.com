Title: "Hay un plan para asesinarme y se va a ejecutar" advierte líder Ingrid Vergara
Date: 2020-01-10 17:01
Author: CtgAdm
Category: Entrevistas, Líderes sociales
Tags: amenazas contra líderes sociales, Clan del Golfo, San Onofre, Sucre
Slug: hay-un-plan-para-asesinarme-y-se-va-a-ejecutar-advierte-lider-ingrid-vergara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/ingrid-vergara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Ingrid Vergara, líder social e integrante del Movimiento de Víctimas de Crímenes de Estado (MOVICE) en el departamento de Sucre, denuncia que a finales de 2019 recibió la notificación que la Unidad Nacional de Protección (UNP) decidió levantar su esquema de protección aún cuando ha sido demostrado que su situación en el departamento es de extremo riesgo e incluso se ha advertido sobre la existencia de un plan para asesinarla a ella y a otro defensor de DD.HH. de la región.

### A Ingrid la cobija la CIDH

**Desde el 8 de noviembre de 2006 la Comisión Interamericana de Derechos Humanos (CIDH) dictó medidas cautelares a favor de ella y otros 17 integrantes del MOVICE** quienes han denunciado violaciones a los derechos humanos cometidas en las regiones de Sucre, Bolívar, Sur de Bolívar y Montes de María y que han sido blanco de amenazas y hostigamientos.

Pese  a ello,  en noviembre de 2019 la UNP notificó a la líder que retiraba su esquema de seguridad. Tras ser enviado un recurso de reposición sobre la resolución y una serie de reuniones en las que la entidad se comprometió a estudiar de nuevo su caso, finalmente el pasado 2 de enero el esquema de Ingrid es retirado, semana en la que supuestamente se habría resuelto el recurso.

Según relata Ingrid, pese a la decisión del tribunal y al accionar de su hija quien realizó todo el trámite necesario ante la UNP, solo hasta este 10 de enero fue reintegrado su esquema, resalta además que en principio le fue asignada para su protección una persona proveniente del antiguo Departamento Administrativo de Seguridad (DAS), hecho que le resultó de desconfianza teniendo en cuenta los antecedentes de una institución que realizaba seguimientos a defensores de derechos humanos, por lo que exigió que la persona encargada de su seguridad, fuera la misma que la ha acompañado desde hace ocho años. [(Lea también: Preservar los documentos del DAS para esclarecer la verdad y hacer justicia)](https://archivo.contagioradio.com/documentos-das/)

"La gente piensa que el tema de la seguridad tiene que ver con medidas de protección, pero están son integrales, hay otras medidas que el Estado no ha cumplido, hay muy poco interés e incluso hay casos que ya están archivados" además, la desprotección de la defensora se da en medio del trabajo con la denuncia de casos ante la Jurisdicción Especial para la Paz y la solicitud de medidas cautelares para cementerios en San Onofre y la finca El Palmar.

### "A mí no me garantiza la vida un esquema de seguridad"

Ingrid advierte que la negligencia del Estado para garantizar su protección se da en medio de una serie de denuncias que ha realizado, entre estas, la información que conoció el pasado 3 de enero, cuando fuentes cercanas le alertaron sobre la existencia de un plan para asesinarla a ella y al también defensor de derechos, Rodrigo Ramírez. [(Le puede interesar: Prácticas paramilitares del pasado resurgen en Sucre)](https://archivo.contagioradio.com/practicas-paramilitares-del-pasado-resurgen-en-sucre/)

**"Hay un plan para asesinarme y se va a ejecutar, ya está el dinero, son 150 millones de pesos"** , además denuncia que detrás de la operación está alias 'El Pezuña', persona quien estaría hospedada en el barrio Boston, en Sincelejo, vecindario ubicado frente al barrio en el que habita Ingrid. Además afirma que tras este pago estaría una de las personas que perdió en las elecciones por la Gobernación del departamento en 2019, "nos han comentando que se han reunido diferentes sicarios para asesinarnos", situación que ha sido denunciada en diversos escenarios incluidas las mesas de garantías.

### En Sucre, prevalece el control paramilitar

Ingrid señala que más de una década después que la CIDH estableciera dichas medidas cautelares, la situación en Sucre continúa siendo compleja, **elevándose a 133 el número de personas asesinadas en este departamento hasta noviembre de 2019,** eso sin contar la ausencia de garantías en territorios donde se acompañan el proceso de restitución de tierras como la finca La Europa o la exploración de cementerios y lugares en los que habría personas inhumadas y donde la Unidad de Búsqueda de Personas Dadas por Desaparecidas (UBPD) ha comenzado su trabajo.

La defensora de DD.HH. agrega que el control territorial ejercido por el Clan del Golfo se ha fortalecido en San Onofre, Sincelejo y Montes de María donde se han emitido alertas tempranas y pese a que existe una decisión internacional que obliga al Estado a garantizar su integridad física, así como la continuidad de su labor, el riesgo sigue siendo latente.  [(Lea también: Paramilitares amenazan candidatos a la Alcaldía en Ovejas, Sucre)](https://archivo.contagioradio.com/paramilitares-amenazan-candidatos-a-la-alcaldia-en-sucre/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
