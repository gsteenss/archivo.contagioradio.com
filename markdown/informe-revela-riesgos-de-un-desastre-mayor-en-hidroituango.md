Title: Hay riesgo de avalancha en municipios aledaños a Hidroituango
Date: 2018-11-21 15:22
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Antioquia, Hidroituango
Slug: informe-revela-riesgos-de-un-desastre-mayor-en-hidroituango
Status: published

###### Foto: Archivo 

###### 21 Nov 2018 

Los municipios de Cáceres, Tarazá, Puerto Antioquia y Puerto Valdivia podrían estar en riesgo de avalancha de acuerdo al informe **Hidroituango ¿Qué pasó, por qué pasó, qué está pasando y qué podría pasar?**, en el que se advierte sobre el delicado estado del macizo rocoso, que de ser sometido a más presión, fallaría y produciría un colapso total en la represa.

El profesor del Departamento de Geociencias de la Universidad Nacional y autor del documento, Modesto Portilla advierte que a pesar de las medidas impuestas por la **Autoridad Nacional de Licencias Ambientales (ANLA)**, que exigen el desarrollo de acciones que permitan implementar medidas preventivas para minimizar el riesgo de las comunidades aledañas, **Empresas Públicas de Medellín (EPM)**, quiere terminar la represa que tiene como objetivo aprovechar el potencial hidroeléctrico del río Cauca.

### **¿Cuáles son los riesgos para las comunidades víctimas de Hidroituango?** 

Según el informe, el macizo rocoso de la zona sobre el que se han construido explanaciones, graderías, portales de entrada y de salida de túneles y galerías está altamente fracturado, esto sumado al constante flujo de agua a altas presiones que atraviesa el  macizo rocoso lo hace más débil y susceptible a la desestabilización.

Si bien hace la salvedad que “la geología no es impedimento para construir pero si es un condicionante”, el docente alerta que de ser sometido a más fuerza, el macizo rocoso podría fallar produciendo un colapso en ese sector de la represa y ocasionar “un flujo hiperconcentrado”, es decir **una avalancha de agua que arrastraría  sedimentos y escombros ya almacenados en el embalse, además de todo lo que recogería aguas abajo.**

De producirse esta avalancha, a los municipios de Cáceres y Caucasia  llegaría agua con lodo, pero los efectos catastróficos llegarían a los municipios de **Tarazá, Puerto Antioquia y Puerto Valdivia** hasta llegar al muro, por lo que lo más recomendable sería que las autoridades locales evacuaran a la población  para disminuir el riesgo de una tragedia ocasionada no por la naturaleza sino por  el hombre.

<iframe id="audio_30246293" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30246293_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
