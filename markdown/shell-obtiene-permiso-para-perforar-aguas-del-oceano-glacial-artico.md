Title: Shell obtiene permiso para perforar aguas del Océano Glacial Ártico
Date: 2015-05-12 13:10
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ártico, Barack Obama, cambio climatico, Casa Blanca, contaminación, daño ambiental, Estados Unidos, extracción petrolera, petroleo, Shell, WWF
Slug: shell-obtiene-permiso-para-perforar-aguas-del-oceano-glacial-artico
Status: published

##### [Foto: cambia.pe]

###### [12 de May 2015]

[La compañía petrolera **Shell logró aval por parte del gobierno de Estados Unidos para realizar perforaciones en aguas del Océano Glacial Ártico,** pese al riesgo que estas actividades le generan al ambiente, específicamente en la Antártida considerada como una zona de alto valor ecológico.]

[Aunque el Departamento de Interior de USA, señala que está decisión se toma teniendo en cuenta la importancia de los recursos ecológicos, ambientales y sociales de esta región, y que se establecerán altos estándares para la protección de este ecosistema, hace tres años, **Shell había sido obligada a detener sus exploraciones petroleras en el Ártico,** y fue multada por contaminación en sus vuelos sobre la zona. ]

[Además del cambio climático, los residuos de petróleo que llegan a la Antártida generan **graves consecuencias en la fauna, como los pinguinos, ballena azul, leopardo marino, focas, morsas que son una especie protegida**, entre otros animales que se ven afectados por este tipo de actividades.]

[La decisión se toma cuatro meses después de que la Casa Blanca abriera diferentes espacios de la costa atlántica para la exploración de hidrocarburos a cambio de aumentar las protecciones en el Ártico.]

[De acuerdo a los expertos, el equilibrio de este ecosistema se encuentra en grave peligro de romperse, lo que **conllevaría a consecuencias catastróficas para la humanidad y la naturaleza,** ya que el clima cambia, el nivel del agua aumenta, se extinguirían varias especies, teniendo en cuenta que durante los últimos 30 años hemos perdido tres cuartas partes de la capa de hielo flotante de la cima de la Tierra.]

[De acuerdo a Rebecca Noblin, directora del Center for Biological Diversity, esta autorización a la compañía petrolera  “**no solo pone en riesgo parajes únicos, sino que también es incompatible con la retórica del presidente Obama** sobre la lucha contra la crisis climática". Por su parte, World Wildlife Fund, WWF, asegura que esta decisión de EE.UU "representa un paso atrás", ya que "este es un lugar donde las condiciones extremas de clima, vientos huracanados y fuertes marejadas hacen que toda operación sea extremadamente difícil".]
