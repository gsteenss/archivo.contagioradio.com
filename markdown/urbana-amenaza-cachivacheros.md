Title: Renovación urbana amenaza los Cachivacheros y Habitantes de Calle
Date: 2019-09-07 11:23
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cachivacheros, Habitantes de calle, Renovación Urbana, Vendedores informales
Slug: urbana-amenaza-cachivacheros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/2fa8ac28-9e36-4042-b036-0f82140bb402.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fray Gabriel Gutiérrez] 

Desde la semana pasada vendedores informales, conocidos como cachivacheros, que trabajan en el centro de Bogotá han denunciando que son víctimas de la persecución de la Policía al ejercer su derecho al trabajo. De acuerdo a sus testimonios, el asedio por parte de la Fuerza Pública se estaría dando a raíz del proceso de "renovación urbana" que desarrolla la Alcaldía en el parque Tercer Milenio, afectando así los ingresos económicos para más de 230 familias.

### **Violencia y desplazamiento contra cachivacheros y habitantes de calle  
** 

La presencia de trabajo informal ambulante y de personas sin hogar en Colombia encuentra su razón en varias causas como la insuficiencia de ingresos económicos, el desplazamiento o la falta de garantías de acceso a la educación. Estos ciudadanos han sido estigmatizados durante mucho tiempo por la Policía, que los suele considerar como generadores de basura e inseguridad. (Le puede interesar:["Peñalosa y el fracaso de la política para atender a los habitantes de calle"](https://archivo.contagioradio.com/penalosa-fracaso-habitantes-de-calle/))

En este caso, la orden de desplazamiento de los vendedores informales y ciudadanos habitantes de calle del parque Tercer Milenio está ligada a la política de "recuperación del espacio público" y al proyecto de renovación urbana del centro de Bogotá. Ello ha significado que los cachivacheros, comerciantes de elementos usados que tradicionalmente se establecen en la plaza de La Perseverancia, sobre la Carrera Séptima y en la esquina de la Calle Sexta con Carrera Décima no puedan encontrar medios de subsistencia para sus familias.

Además de la no garantía a su derecho al trabajo, los cachivacheros han señalado que son víctimas de malos tratos por parte de algunos agentes de la Policía que dañan sus productos y los golpean. (Le puede interesar: ["Más de 4 mil habitantes de calle han sido asesinados en 10 años"](https://archivo.contagioradio.com/habitantes-de-calle-asesinados/))

Carlos Alberto Bermúdez, cachivachero representante del gremio, declaró que son cerca de 230 familias que viven de estos ingresos. «Hay compañeros que se venden 10,15, 20 mil pesos al día, no tenemos ni para pagar el arriendo que pagamos a diario, no tenemos ni que vender, llevamos desde hace 25 años que nos vienen maltratando y día día nos van acabando», afirmó.

### **La indiferencia de las instituciones** 

Los vendedores en el mes de agosto hicieron algunas reuniones con Gustavo Niño, alcalde local de Santa Fe, en las que llegaron al compromiso de retirarse del Parque Tercer Milenio y la Carrera Séptima siempre que les dejaran trabajar en la plazoleta del barrio San Bernardo. Este acuerdo se sellaría en un documento que el alcalde local se ha negado a firmar en reiteradas ocasiones. (Le puede interesar: ["Piden declarar emergencia humanitaria por asesinatos de habitantes de calle en Bogotá"](https://archivo.contagioradio.com/policia-realizo-operativo-contra-habitantes-de-calle-en-bogota/))

«La plazoleta de San Bernardo la hemos trabajado hace unos 18 años, ese punto es conocido como cachivachero. Nosotros no pedimos plata, estamos pidiendo es un sitio para poder trabajar tranquilos y que la Policía no nos moleste; nosotros tenemos niños, pagamos impuestos», fue la conclusión de Bermúdez. (Le puede interesar: ["Fin del Bronx no acaba con la violencia contra habitantes de Calle"](https://archivo.contagioradio.com/fin-del-bronx-no-acaba-con-la-violencia-contra-habitantes-de-calle/))

Según la sentencia T 904 de 2012: “Si bien los comerciantes informales pueden limitar el disfrute de otras personas del espacio público, el Estado no puede desconocer que lo hacen con el fin de conseguir medios efectivos que aseguren su mínimo vital y les permitan la realización de otros derechos fundamentales”. En consecuencia, cachivacheros y defensores de derechos humanos han pedido que se acaten las decisiones constitucionales, y se impulsen políticas de carácter social que protejan el derecho al trabajo de estas personas.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
