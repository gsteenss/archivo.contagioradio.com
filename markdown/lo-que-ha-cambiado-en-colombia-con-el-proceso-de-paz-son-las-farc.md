Title: Lo que ha cambiado en Colombia con el proceso de paz son las FARC
Date: 2017-11-18 06:00
Category: Abilio, Opinion
Tags: FARC, JEP, partido politico
Slug: lo-que-ha-cambiado-en-colombia-con-el-proceso-de-paz-son-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Farc-en-Yari.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo - Contagio Radio 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 18 Nov 2017

**A proposito del desastre de la JEP**

La Decisión de la Corte Constitucional del 14 de noviembre sobre el Acto Legislativo de la Jurisdicción Especial de Paz (JEP), la alocución presidencial del día 14  y la decisión del Senado de la República del 15 de noviembre, muestran los tres poderes del poder público unidos en el pacto de impunidad de las élites, ante  la ya debilitada, en materia de Verdad Justicia y Reparación, Jurisdicción de Paz,  parte del punto quinto del acuerdo sobre  víctimas.

[La Corte Constitucional   en su sentencia sobre la JEP  tumbó de un tajo la responsabilidad de mando de  militares, civiles sobre operaciones de subalternos implicados en crímenes de guerra y crímenes de lesa humanidad, pero además  falló que los terceros responsables, es decir, los no armados implicados en los crímenes como han sido políticos, empresarios, medios de información, entre otros, comparezcan  ante este tribunal sólo si así lo desean.]

[El Presidente Santos felicitó ese fallo y lo calificó como “una gran noticia” e invitó a las plenarias  del Senado y Cámara a aprobar la Ley Estatutaria de la JEP en las que valoraba positivamente que “se están  teniendo en cuenta en el trámite legislativo varias  de las observaciones realizadas por diferentes sectores y por los mismos congresistas, que han ayudado a mejorar y hacer más preciso el texto”.]

[Y el ascenso de los mecanismos institucionales de  impunidad llegó  a su clímax,  para terminar de  enterrar los pequeños logros en materia de justicia del acuerdo, con el vergonzoso parágrafo al artículo 100  de la ley estatutaria sobre las inhabilidades para ser elegido como magistrado del Tribunal para la Paz. Increíble, pero así lo aprobaron: “quienes dentro de los cinco (5) años anteriores a la fecha de posesión; al (sic) título personal o por interpuesta persona haya gestionado o representado intereses privados en contra del Estado en materia de violaciones  a los Derechos Humanos, al Derecho Internacional Humanitario,  o al Derecho Penal Internacional o pertenezcan o hayan pertenecido a organizaciones  o entidades  que hayan ejercido tal representación”.]

[Se refieren a magistrados miembros elegidos de la JEP como Pedro Mahecha y Sandra Gamboa, defensores de Derechos Humanos que lograron, en varios casos,  “una gota de esperanza en el mar de impunidad”  en el que ha vivido sumido nuestro país. Los derechos humanos y su defensa, uno de los logros más destacados de la humanidad contra la barbarie, es de acuerdo con los senadores que aprobaron ese adefesio, un grave antecedente en la hoja de vida de los magistrados que los inhabilita para hacer “justicia” dentro de la jurisdicción.]

[El miedo a que algo de justicia pueda recaer contra los determinadores de los más horrendos crímenes de lesa humanidad en nuestro país no tuvo tintes políticos en esa decisión. Ahí se evidenció la verdadera coalición de la paz en el senado, la que defiende el]*[status quo]*[, como liberales, conservadores, que en esta vez hicieron causa común con el Centro Democrático de Uribe. Sólo la vergüenza – de caber-  que les pueda hacer sentir la comunidad internacional haría enmendar esta infamia en la plenaria de la cámara o en la corte constitucional. Amanecerá y veremos.]

[Este grave hecho es uno de los más monumentales incumplimientos al acuerdo firmado en la Habana que se suma a la ausencia de logística en las antiguas zonas veredales,  a la negación de la libertad de exguerrilleros amnistiados o indultados, a las dificultades en la monetarización, a la ausencia de tierras para el desarrollo productivo de excombatientes, a la esquizofrenia de la sustitución-erradicación de cultivos de uso ilícito que produjo la masacre de Tumaco por parte de la policía,  a la ausencia de garantías afectivas para la protección que ha llevado a que 32 miembros de las FARC hayan sido asesinados, a la ausencia de acciones efectivas para el desmonte de grupos paramilitares que siguen operando amparados en la complicidad y omisión de la fuerza pública, entre otros incumplimientos.]

[En este escenario el único cambio real que se ha dado es el de las FARC-EP a la FARC, partido político,  lo que ha implicado, luego de la dejación de armas, un cambio de método en la  acción política para la búsqueda de transformaciones sociales, como lo han manifestado y que no cuenta  con un correlato desde el establecimiento que garantice condiciones  para estas transformaciones.]

[Da esperanza el que los presos de FARC, por ejemplo,   acudan a la extrema acción de la huelga de hambre, antes que a motivar una retoma de armas o que los excombatientes de los espacios territoriales  decidan comprar o alquilar tierras como en  el Gallo o en Mesetas, para poder sembrar alimentos y reconstruir su vida, por su cuenta, usando los recursos que les entregó el gobierno para su sostenimiento, por fuera de los planes de reincorporación. O que los únicos implicados en el conflicto que se animen a pedir perdón hayan sido, justo, los miembros de la FARC.]

[¿Qué estará leyendo el ELN de estos incumplimientos mientras se encuentra dialogando en Quito con el gobierno y en cese bilateral en las selvas? ¿Qué pasa por la mente de los miembros de la FARC?]

[En todo caso la esquiva movilización para empujar las transformaciones, sigue siendo un sueño. Entre tanto, vale ensayar, desde lo más auténtico de las tradiciones de la Acción No violenta,   acciones creativas que permitan alcanzar  desde las comunidades y los excombatientes, los cumplimientos a pactos y acuerdos   que la ausencia de  voluntad política del establecimiento les niega.]

------------------------------------------------------------------------

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
