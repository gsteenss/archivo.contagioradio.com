Title: El 28 de abril abre las puertas el Refugio Humanitario por la vida de los líderes sociales
Date: 2019-04-23 16:30
Author: CtgAdm
Category: DDHH, Paro Nacional
Tags: Bogotá, Comunidad Internacional, lideres sociales, Paro Nacional, Refugio Humanitario
Slug: refugio-humanitario-vida-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-22-at-2.51.38-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/refugio-humanitario.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

En medio de un contexto que es hostil para las y los líderes, organizaciones idearon el "Refugio Humanitario por la Vida de Líderes y Lideresas Sociales" como una forma de sensibilizar y visibilizar las problemáticas que enfrentan quienes dedican su vida a la defensa de los derechos humanos. El refugio se llevará a cabo entre el 28 de abril y el 2 de mayo en un espacio que aún está por definirse en Bogotá, y a lo largo de esos 5 días participarán en diferentes actividades en las que expondrán sus experiencias.

Luis Emil Sanabria, director de la **Red Nacional de Iniciativas Ciudadanas por la Paz y Contra la Guerra (REDEPAZ)**, afirma que el refugio humanitario es una alerta para la sociedad y la comunidad internacional sobre el costo que tiene asumir la defensa de los derechos humanos. En esa medida, el Refugio se plantea cuatro objetivos: Informar y sensibilizar a la ciudadanía; realizar un ejercicio de control político al Gobierno; proponer medidas de protección colectivas; e informar a la comunidad internacional lo que está ocurriendo.

### **La Agenda de Eventos que tendrán los líderes en la Capital**

El lunes 28 de abril los líderes serán recibidos sobre el medio día y harán parte de actos simbólicos a las 2 de la tarde; posteriormente se realizará un acto de armonización e instalación del Refugio. Durante el martes se realizará una solicitud masiva de protección de medidas internacionales en horas de la mañana para llamar la atención de la comunidad internacional, y a las 5 de la tarde se realizará un acto político y cultural en el Refugio. Posteriormente, el 3 de abril desde las 9 de la mañana se llevará a cabo una audiencia pública de control político sobre la protección a líderes y sobre las 3 de la tarde sostendrán una reunión con la misión de verificación de la ONU en Colombia y la comunidad internacional.

El primero de mayo los líderes acompañarán la tradicional marcha de los trabajadores, a propósito del Día Internacional del Trabajo; y el 2 de mayo, desde las 8 de la mañana se realizará el encuentro nacional de líderes y organizaciones amenazadas, cuyo cierre será un acto simbólico sobre las 5 de la tarde. (Le puede interesar: ["Agresiones contra defensores de DD.HH. aumentaron más de 40% en 2018"](https://archivo.contagioradio.com/en-2018-se-presentaron-mas-de-dos-agresiones-diarias-contra-defensores-de-dd-hh/))

Con este evento los organizadores esperan que haya "miles de corazones latiendo con más fuerza por la vida, la libertad y la dignidad de los líderes y líderesas sociales". (Le puede interesar: ["¡Que no se queden en papel promesas para defender la vida de líderes sociales"](https://archivo.contagioradio.com/defender-vida-lideres-sociales/))

\[caption id="attachment\_65319" align="aligncenter" width="460"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-22-at-2.51.38-PM-235x300.jpeg){.wp-image-65319 width="460" height="587"} El refugio se realizará entre el 28 de abril y el 2 de mayo.\[/caption\]

<iframe id="audio_34855923" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34855923_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
