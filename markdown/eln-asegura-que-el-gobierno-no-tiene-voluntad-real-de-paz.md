Title: ELN asegura que el gobierno no tiene voluntad real de paz
Date: 2015-10-16 16:39
Category: Nacional, Paz
Tags: conflicto armado en Colombia, cultura de paz, diálogos de paz con el ELN, ELN, Pablo Beltrán, presos politicos
Slug: eln-asegura-que-el-gobierno-no-tiene-voluntad-real-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/51f750370c08f.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [tiempo.infonews.com]

###### [16 Oct 2015]

En el marco de la conmemoración del Día Nacional del Prisionero y la Prisionera Política, Pablo Beltrán, jefe de la delegación de paz del **ELN, aseguró que esa guerrilla centrará sus esfuerzos para que el proceso de paz entre en su fase pública y no fracase**. Sin embargo, afirmó que el ELN desconfía del gobierno porque **“no hay voluntad de paz más allá del discurso”**, expresó Beltrán, quien añadió que “el gobierno tiene voluntad de atacar con guerra sucia”.

“Estamos dispuestos a pasar a una fase pública pero mientras que el gobierno dice que hay voluntad de paz, se contradice al criminalizar la protesta social… **creemos más en los hechos que en las promesas”** expresó.

Así mismo, señaló que el objetivo del proceso de paz no será buscar un sistema judicial que juzgue a los guerrilleros, como lo esperan algunos sectores de la sociedad.  Dijo que el  fin de los diálogos es que se realicen “**cambios en Colombia, que las clases dominantes se comprometan, y haya una mejor situación y bienestar para el conjunto a la sociedad colombiana”**.

Mediante el video enviado por el ELN, se indicó que el gobierno no quiere reconocer su responsabilidad frente a los más de 50 años de conflicto armado en Colombia, lo que demuestra que no hay una voluntad real de paz, dejando de lado el **“respeto por las víctimas” teniendo en cuenta que “el que más tiene deuda con las victimas es el Estado”.**

Finalmente, reafirmaron que el ELN seguirá **“construyendo una cultura de  paz basada en la resistencia”** y que en el proceso de paz, las víctimas y la población civil deberán ser el eje central para que se pueda desarrollar la agenda pactada.
