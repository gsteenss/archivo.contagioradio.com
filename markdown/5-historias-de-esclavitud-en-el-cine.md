Title: 5 historias de esclavitud en el cine
Date: 2015-08-23 15:23
Author: AdminContagio
Category: 24 Cuadros, DDHH
Tags: 12 años de esclavitud, Amazing Grace película, Amistad película, Día Internacional del Recuerdo de la Trata de Esclavos y de su Abolición, Little Senegal, Películas sobre esclavitud
Slug: 5-historias-de-esclavitud-en-el-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/12-años.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Fograma: 12 Years a Slave Fox Interacional 

###### [23, Ago, 2015] 

Con el propósito de reivindicar la trata transatlántica de esclavos,  la Conferencia General de la Organización de las Naciones Unidas para la Educación, la Ciencia y la Cultura (UNESCO), decretó que el 23 de agosto de cada año se conmemore el “Día Internacional del Recuerdo de la Trata de Esclavos y de su Abolición”, una oportunidad para el reconocimiento colectivo y el enfoque sobre las causas históricas, los métodos y las consecuencias de la esclavitud.

La decisión tomada en el año 1999 durante el 29° período de sesiones de la UNESCO, quedo registrada como la resolución 29C/40, en la que se invita a los Estados Miembros darle toda la importancia que merece la conmemoración, y a movilizar a todas las comunidades educativas, científicas, artísticas y culturales, a la juventud y a la sociedad civil en general, con actividades que promuevan la tolerancia, el respeto, la aceptación y aprecio de la dignidad igual de los seres humanos.

Desde el cine, varios directores han presentado su visión de la esclavitud, situándola en diferentes contextos históricos, políticos y sociales; algunos como reconstrucción de hechos reales, de movimientos y personajes que tomaron, y aun toman parte, de las luchas abolicionistas en diferentes latitudes del planeta. Recomendamos 5 películas que abordan el tema de la esclavitud, sumándonos a esta celebración mundial.

**12 años de esclavitud** (Twelve Years a Slave)  
EE.UU. 2013/ Dirección: Steve McQueen.

<iframe src="https://www.youtube.com/embed/8MsETvQYFZ8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Basada en la autobiografía de Solomon Northup, un mulato afroamericano nacido libre en el estado de Nueva York que fue secuestrado en el Distrito de Columbia en 1841 para ser vendido como esclavo y que trabajó en plantaciones en Louisiana durante 12 años, hasta su liberación.

[[**Amazing Grace** ]]  
[[Reino Unido. 2006/ Dirección: Michael Apted]]

<iframe src="https://www.youtube.com/embed/cRuWRy5McCM" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Basada en hechos reales, la vida del parlamentario británico William Wilberforce (1759-1833), pionero en la lucha contra la trata de esclavos en el siglo XVIII y cuyos ideales lo enfrentaron a algunos de los hombres más poderosos de la época, opuestos a la abolición de la esclavitud a causa de los beneficios que la actividad les reportaba.

[[**Amistad**]]  
[[EE.UU. 1997/Dirección: Steven Spielberg]]

<iframe src="https://www.youtube.com/embed/BJFDOvGMD0U" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Narra la rebelión de unos esclavos africanos en el barco español La Amistad, cuando eran enviados a América. El barco es apresado por la Armada estadounidense y son sometidos a un sistema judicial completamente desconocido para ellos, en un país extraño.

**Little Senegal**  
Francia. 2001./Dirección: Rachid Bouchareb

<iframe src="https://www.youtube.com/embed/iGPs18G9l_E" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Un hombre ya mayor que trabaja en un museo de Senegal dedicado a la esclavitud, investigando su árbol genealógico descubre que varios de sus antepasados fueron raptados y vendidos a tratantes de esclavos de Carolina del Sur. Entonces decide viajar a los Estados Unidos con la intención de localizar a los descendientes de estos antepasados esclavizados.

**Django Unchained**

EEUU, 2012/Dirección: Quentin Tarantino

<iframe src="https://www.youtube.com/embed/tY_NyluaMO8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Dos años antes de estallar la Guerra Civil Americana, King Schultz, un cazarecompensas alemán que sigue la pista a unos asesinos para cobrar por sus cabezas, le promete al esclavo negro Django dejarlo en libertad si le ayuda a atraparlos. Él acepta, pues luego quiere ir a buscar a su esposa Broomhilda (Kerry Washington), esclava en una plantación del terrateniente Calvin Candie.
