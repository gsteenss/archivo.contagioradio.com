Title: Con la JEP llegó la hora para las víctimas
Date: 2018-03-14 12:52
Category: Nacional, Paz
Tags: JEP, justicia especial para la paz, Patricia Linares, víctimas del conflicto armado
Slug: jep_arranca_paz_victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/marcha-de-las-flores101316_264.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Mar 2018] 

[Este 15 de marzo arranca en firme la Jurisdicción Especial para la Paz, **con un total de 7392 postulados entre excombatientes, integrantes de la fuerza pública, y otros terceros que han decidido acogerse a la JEP,** y que en audiencias públicas rendirán cuentas a las víctimas frente a sus responsabilidades en los más de 50 años de conflicto armado entre el gobierno y el ahora FARC.  ]

No obstante, cabe recordar que la JEP está funcionando desde el pasado 15 de enero una vez fueron  posesionados 30 magistrados de los 38. Desde ese momento ya se** **han producido decisiones judiciales de tutela, así como también se tiene la elaboración del reglamento interno de funcionamiento de dicho sistema.

Será a partir de este jueves que los magistrados ya empiezan a trabajar de cara a las víctimas de la guerra, **recepcionando todos los informes de la Secretaría Ejecutiva de la JEP, de la Fiscalía, Contraloría, Procuraduría y, también de los informes de organizaciones de derechos humanos.**

En materia de recursos, la Jurisdicción Especial para Paz, cuenta con una asignación aproximada de 200 mil millones para el 2018. Sumado a ello, están los recursos para la sede de la JEP que destinó el gobierno colombiano desde el llamado “Fondopaz” y que garantizaría el despegue de esta institución.[(Le puede interesar: inicia el camino de la JEP)](https://archivo.contagioradio.com/inicia-el-camino-de-la-jep-como-una-esperanza-para-las-victimas-linares/)

### **Ya está listo el proyecto de Ley sobre el funcionamiento de la JEP** 

Por su parte, el presidente Juan Manuel Santos ya recibió el proyecto de **ley de procedimiento para el funcionamiento de los nuevos tribunales, y que los magistrados de la JEP esperan que se tramite con urgencia y respetando lo establecido.**

Se trata de un proyecto clave,  mediante el cual se garantiza a las víctimas, los postulados, y a la sociedad en general un debido proceso, seguridad jurídica y todas las garantías que se imponen dentro de un estado social de derecho para que funcione un sistema de justicia como el pactado en La Habana.

### **Ex paramilitares podrán ingresar a la JEP** 

Con el objetivo de afianzar la construcción de paz en el país, además de los exguerrilleros y los integrantes de la Fuerza Pública, mediante el decreto 2199 se permite que los desmovilizados que participaron en la Ley de Justicia y Paz de 2005, puedan ingresar en el proceso de reincorporación, a través de la JEP, **si se comprometen a contribuir con la verdad y reparación a las víctimas.**

En esa medida, si los exparamilitares contribuyen en el esclarecimiento de la verdad, la Agencia para la Reincorporación y la Normalización será la encargada de revisar los procedimientos que permitan el acceso de estos excombatientes al proceso de reincorporación. [(Le puede interesar: Exparamilitares podrán acogerse a la JEP)](https://archivo.contagioradio.com/exparamilitares-que-se-comprometan-con-la-verdad-podran-ingresar-a-jep/)

### **Una luz de verdad y reparación para las víctimas** 

Muchas organizaciones de víctimas y de Derechos Humanos han coincidido en señalar que, a pesar de los ataques, esta Jurisdicción es una oportunidad para la verdad, la justicia restaurativa y la reconciliación, y por eso hoy aplauden que sea una realidad que la JEP entre en funcionamiento.

"Cada uno de nosotros tenemos posiciones ideológicas políticas, religiosas y académicas, que hacen que construyamos nuestra propia historia, de eso se trata. Pero ahora, nuestro comportamiento y nuestra actividad debe acotarse al mandato mismo que especificó el Comité. **Un trabajo orientado al debido proceso, con garantía de imparcialidad y el mayor rigor para que se brinde seguridad jurídica** en las decisiones que se produzcan, y así aportar a una reconciliación", expresa Patricia Linares, presidenta de la JEP, quien agrega que la tarea ahora será tener una dedicación al 100% para que todo funcione de manera armónica.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
