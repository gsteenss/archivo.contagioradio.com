Title: Asesinado líder de sustitución de cultivos en San José de Uré, Córdoba
Date: 2018-02-01 15:37
Category: DDHH
Tags: asesinato de líderes sociales, Fuerzas militares, lideres sociales, paramilitares
Slug: asesinado-lider-de-sustitucion-de-cultivos-en-san-jose-de-ure-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [01 Feb 2018] 

Antonio María Vargas era el tesorero de la Junta de Acción Comunal de la vereda Nueva Ilusión del corregimiento del Batatillo en **San José de Uré, Córdoba** y desempeñaba acciones para realizar la sustitución voluntaria de cultivos de uso ilícito. Su asesinato ocurrió durante el día del 31 de enero y en lo que va de 2018 han sido asesinados 24 líderes sociales en Colombia.

De acuerdo con Andrés Chica, director de la Fundación Cordobexia, en lo que va del año han sido asesinados **3 líderes en el sur de Córdoba**, esto “en el marco de la implementación de la sustitución voluntaria de la hoja de coca”. Indicó que hay una incoherencia por parte del Gobierno de cara a la implementación de los aspectos acordados en la Habana.

### **Asesinos de Antonio Vargas fueron los mismos que asesinaron al líder Plinio Pulgarín** 

Chica informó que Antonio Vargas se encontraba el día de su asesinato **departiendo con su comunidad** en la vereda La Ilusión “a escasos 40 minutos de donde ocurrió el asesinato de Plinio Pulgarín el 18 de enero”. Indicó que “unos sujetos lo sacaron de su casa y lo llevaron a un lugar visible donde estaba la comunidad y parte de su familia y lo ultimaron de varios impactos de bala”. (Le puede interesar: ["Enero, un mes trágico para líderes y reclamantes de tierra"](https://archivo.contagioradio.com/enero_asesinatos_lideres_sociales/))

Otros líderes y miembros de la comunidad reconocieron que quienes asesinaron al líder comunal fueron los mismo que cometieron el crimen del también líder Plinio Pulgarín y “**amenazaron de muerte a otras personas** a la vez que desplazaron a otras veredas”. Reiteró que hay más de 400 personas desplazadas y hay un riesgo inminente de que otras comunidades sean desplazadas.

### **Comunidad de La Ilusión está en riesgo de desplazamiento** 

Con el caso del asesinato de Antonio Vargas, como en el caso de Plinio Pulgarí donde se **desplazaron más de 60 familias**, la comunidad donde ocurrieron los hechos están en riesgo de ser desplazadas. Los líderes de diferentes organizaciones han manifestado que esto ocurre por la presión de los grupos armados y la poca presencia estatal que hay en los territorios.

Además, consideran que el desmonte del paramilitarismo **“es letra muerta”** y “no existe una coherencia en lo que hoy llaman el plan Victoria de estabilización y consolidación de las Fuerzas Militares frente a las dinámicas del conflicto armado del país”. Chica dice que hay presencia de grupos armados extranjeros como los carteles mexicanos que se suman a los grupos paramilitares y bandas criminales que ya están en los territorios.

### **Negocio de la coca en Córdoba ha crecido** 

Para estas comunidades la preocupación es grande en la medida en que los asesinatos de líderes se dan teniendo en cuenta la labor que ellos y ellas hacen para **defender la implementación de la sustitución** voluntaria de cultivos ilícitos que choca con la lucha por mantener el negocio del narcotráfico por parte de los grupos armados. (Le puede interesar: ["Líderes del Bajo Atrato denuncian plan para asesinarlos"](https://archivo.contagioradio.com/lideres-del-bajo-atrato-denuncian-plan-para-asesinarlos/))

Argumentan que “ahora el negocio de la coca **es mucho más grande** y más devastador para el campesino del que tenía cuando estaban las FARC porque la zona está copada por estos grupos armados”.  Esto ha hecho que aumenten las amenazas contra campesinos, afrodescendientes e indígenas, dijo que “los tienen azotados, ya hay más de 400 personas desplazadas y muy seguramente vamos a tener a las familias de La Ilusión desplazadas”.

### **Plan Victoria de las Fuerzas Armadas no ha servido** 

Según lo retrata Chica, “antes de que existiese el acuerdo final, las Fuerzas Militares tenían el plan Troya que es reemplazado por el Plan Victoria”. Esta es la ofensiva que tenían los militares contra las FARC y que ahora “en el marco jurídico, el plan busca **consolidar la paz estable** y duradera en el territorio”.

Sin embargo, los líderes han denunciado que esto no ocurre porque “no se puede hablar de estabilizar y consolidar **cuando hay otros grupos que han emergido** o que estaban en el territorio desde hace mucho antes”. Para las comunidades, las Fuerzas Militares y el Gobierno Nacional “se están haciendo los de la vista gorda y no confrontan a estos grupos”.

Lo que está pasando en los territorios ha sido denominado como “**un plan macabro de negocio** que está llevando al campesino a ser la víctima directa porque se ha puesto en la tarea de decirle sí a la implementación”. Además, los actores armados han llegado a las comunidades “a decirles que si vuelven a recibir plata relacionada con la sustitución los van a matar a todos, más de 7 mil familias que firmaron el acuerdo”.

<iframe id="audio_23502694" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23502694_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
