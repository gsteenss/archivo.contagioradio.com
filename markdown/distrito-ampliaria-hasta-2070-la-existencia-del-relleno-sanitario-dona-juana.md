Title: Distrito ampliaría hasta 2070 la existencia del relleno sanitario Doña Juana
Date: 2017-08-22 13:05
Category: DDHH, Nacional
Tags: alirio uribe, Angela María Robeldo, ciudad bolivar, Enrique Peñalosa, Inti Asprilla, Relleno sanitario Doña Juana, Usme
Slug: distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/relleno-sanitario-doña-juana-e1548870723322.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: uniminutoradio] 

###### [22 Ago 2017]

A pesar de los múltiples problemas que se han dado alrededor del relleno sanitario Doña Juana que afectan habitante de cerca de 137 barrios de las localidades de Ciudad Bolívar y Usme en el sur de la ciudad, el distrito propuso **ampliar la vida de este relleno sanitario por 50 años más y anunció que así sea por la fuerza se hará con los terrenos que hagan falta para tal fin.**

Durante una audiencia de control político, por los recientes problemas **generados por el relleno sanitario doña juana** citada por el representante Inti Asprilla, el alcalde de Bogotá anunció que para la puesta en marcha de la Fase IV se necesitarían por lo menos 330 hectáreas de tierra, 130 para el relleno propiamente dicho y 200 más para zonas de amortiguación.

> Hasta el 2070 planea [@EnriquePenalosa](https://twitter.com/EnriquePenalosa) ampliar operación de Doña Juana. [pic.twitter.com/VIaCjh3Rud](https://t.co/VIaCjh3Rud)
>
> — Ángela Robledo (@angelamrobledo) [22 de agosto de 2017](https://twitter.com/angelamrobledo/status/900030273345257475)

Los habitantes de las localidades temen que los más afectados serán los pobladores de las veredas que colindan con el **relleno sanitario como Mochuelo I y Mochuelo II, en donde se presume se daría la construcción, que no ha sido consultada o concertada** con los y las habitantes que también han salido a protestar en las últimas semanas. [Lea también ESMAD arremete contra afectados por las basuras de la ciudad](https://archivo.contagioradio.com/esmad-continua-arremetida-contra-manifestantes-en-el-relleno-dona-juana/)

### **Es necesario concertar con las comunidades lo que se va a hacer con la basura de Bogotá** 

El representante a la Cámara por Bogotá, Alirio Uribe, quien participa en una audiencia de control político que se realiza en la Cámara de Representantes, propuso una mesa de concertación entre el distrito y los habitantes para que se socialice la propuesta, y se busquen salidas integrales para los vecinos de relleno sanitario, dado que hasta el momento el alcalde no ha querido darles la cara. [Lea también: ¿Cómo podrían aprovecharse las basuras que generamos en Bogotá?](https://archivo.contagioradio.com/como-podria-aprovecharse-la-basura-del-relleno-sanitario-dona-juana/)

### **Alcaldía sigue negando los efectos nocivos para la salud que crea Doña Juana** 

Por otra parte, la representante Angela María Robledo, denunció en su cuenta en Twitter que el Alcalde de Bogotá, Enrique Peñalosa sigue negando los efectos nocivos de la presencia del relleno en el sur de Bogotá “En debate relleno Doña Juana, Peñalosa responde diciendo que hizo cientos de colegios en USME y que relleno no afecta salud. Qué tal!!”.

> En debate relleno Doña Juana, Peñalosa responde diciendo que hizo cientos de colegios en USME y que relleno no afecta salud. Qué tal!! [pic.twitter.com/F6PzKYoJFh](https://t.co/F6PzKYoJFh)
>
> — Ángela Robledo (@angelamrobledo) [22 de agosto de 2017](https://twitter.com/angelamrobledo/status/900032986783117313)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
La representante publicó varios videos de la audiencia de control político, en que los campesinos le exigen al alcalde que “ponga la cara”, que vaya al sur de la ciudad a plantear soluciones y que insisten que el **ESMAD no sea la “oficina de atención al ciudadano”** cuando ponen de manifiesto las problemáticas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
