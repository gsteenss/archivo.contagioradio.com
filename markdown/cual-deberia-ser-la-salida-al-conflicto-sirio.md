Title: ¿Cuál debería ser la salida al conflicto sirio?
Date: 2018-04-16 23:39
Category: El mundo
Tags: conflicto sirio, Donald Trump Estados Unidos, Estados Unidos, Siria
Slug: cual-deberia-ser-la-salida-al-conflicto-sirio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/Siria-e1523926720494.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: kilermt.com] 

###### [16 Abr 2018] 

Tras el bombardeo de Estados Unidos, Francia e Inglaterra contra Siria, uno de los mayores temores tiene que ver con la posibilidad de que haya una tercera guerra mundial, sin embargo, de acuerdo con Víctor de Currea Lugo, profesor y experto en temas de medio oriente, para que dicho conflicto sea la punta de Iceberg de una situación aún peor, esa guerra debería trasladarse a territorio Europeo. **Lo que si preocupa, es que los muertos los va a seguir poniendo Siria, mientras que la comunidad internacional aumenta la tensión, al tiempo que le cierra la puerta a los miles de refugiados sirios.**

### **Bombardeo contra Siria no resuelve nada** 

En el marco del conflicto sirio ha habido cerca de 80 posibles ataques con armas químicas, "es decir que no es una novedad o un hecho aislado, sino que es una política que se viene dando", dice el profesor Víctor de Currea. De acuerdo con él,  esta guerra que ha dejado cerca de medio millón de muertos, en su mayoría víctimas de armas convencionales, no se resuelve con acciones bélicas como las implementadas por el gobierno norteamericano.

"El bombardeo de Trump es de la misma esencia que hizo Obama en años anteriores. No es una estrategia, no resuelve el problema de las armas químicas y tampoco resuelve el problema de guerra en Siria", expresa el experto, quien agrega que  **la guerra en Siria empezó hace 7 años, "**y no fue con los bombardeos de EEUU, de Rusia o Inglaterra".

### **El origen de la guerra** 

En contraposición a algunas de las teorías de la izquierda Latinoamericana, el profesor de Currea no cree que el conflicto sirio haya sido planeado a partir de una estrategia de corte imperialista por parte de Estados Unidos. Según sus investigaciones, **la crisis inicia en la época de marchas contra las políticas neoliberales en el marco de la primavera árab**e que se desarrolló entre los años 2010 y 2013. Para ese entonces caen los gobiernos de Egipto,  Túnez y Libia.

Entre tanto, Siria opta por una rápida militarización para no ser derrocado como los otros gobiernos. Dicha militarización es contestada por el apoyo de las monarquías del golfo como Arabia Saudita, Qatar y Emiratos Árabes. A su vez, la oposición que es laica también se alza en armas creando un conflicto armado interno del que, explica el profesor, se aprovecha Al Qaeda de Irak y arma un frente en Siria, **"con lo cual se tiene un Estado sirio, una oposición laica, y unos grupos islamistas",** que son los principales sectores que se han disputado  el control de varias zonas del territorio sirio.

### **Lo que está detrás del bombardeo contra Siria** 

"La comunidad internacional ha mirado para otro lado", considera el experto.  Asimismo, señala que la ONU no ha sido capaz de imponer un mecanismo pacífico de solución de conflictos. Por el contrario la población civil no existe ni en la agenda de Estados Unidos, Rusia, China, Francia o Inglaterra.

**"Las grandes potencias juegan ajedrez en Siria, pero no les importa la población",** precisamente una prueba de ello es que esos bombardeos del pasado viernes fueron supuestamente a nombre de unos principios internacionales que "son traicionados cuando se cierra la puerta en la cara a los miles de refugiados sirios que intentan llegar a esos países. No es el Derecho Humanitario Internacional, ni los Derechos Humanos lo que les preocupa, es el control y la  presencia política dentro de oriente medio", asegura el profesor.

### **Rusia le ha ganado la partida a EEUU en medio oriente** 

Según analiza de Currea, lo que busca Estados Unidos con actuaciones como las del pasado viernes, es re posicionarse en Oriente Medio. "Los gobierno norteamericanos han sido muy torpes, se han distanciado de Arabia Saudita, y sigue obedeciendo a lo que diga Israel", manifiesta el profesor, por eso cree que Trump está buscando empezar a sobreponerse frente al avance sólido de Rusia "no menos imperial que EE.UU, pero que ha sido más asertivo, creando una cadena de poder y un frente de guerra sólido".

### **Las claves para una solución al conflicto** 

Ante tal situación, para el experto, el primer elemento clave para solucionar el conflicto sirio es el diálogo, es decir una salida negociada ya que casi el 60% de la población es víctima de desplazamiento forzado, una situación que se recrudecen ante las acciones bélicas de los países que finalmente no ponen no ponen las víctimas.

Por otra parte, es necesario que exista un modelo multilateral, donde el espacio de la Organización de las Naciones Unidas funcionara desde e "deber ser". Sin embargo, el problema para el experto es que este organismo no es un espacio democrático, y en cambios "es esquizofrenico", ya que, por un lado tiene un mecanismo que es la Asamblea General donde cada país tiene un voto, y por el otro, tiene un Consejo de Seguridad donde hay una jerarquía en manos de los cinco miembros permanentes. **"El diálogo y la salid multilateral es el deber ser, y la realidad es otra, es que los bloques y la doble moral se imponen con lo cual uno", concluye De Currea.**

<iframe id="audio_25395971" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25395971_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
