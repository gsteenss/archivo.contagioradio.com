Title: Entre Fujimori y Kuczinsky se definiría la presidencia en Perú
Date: 2016-04-11 17:05
Category: El mundo, Política
Tags: Elecciones en Perú, Keiko Fujimori, Pedro Pablo Kuczinsky
Slug: entre-fujimori-y-kuczinsky-se-definiria-la-presidencia-en-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/fuji.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [11 Abr 2016] 

Tras la jornada electoral de este domingo en Perú y con el 82.55% de las actas procesadas, la candidata Keiko Fujimori, se impuso en su aspiración por alcanzar la presidencia de ese país, sin embargo, no alcanzó la mitad más uno necesaria para evitar ir a una segunda vuelta en la que hasta el momento se enfrentaría al ex ministro de economía Pedro Pablo Kuczinsky.

De acuerdo con las cifras presentadas por la Oficina Nacional de Procesos Electorales ONPE, los resultados favorecen a la candidata de Fuerza Popular con el 39, 55% de los votos, seguido por Kuczinsky de ‘Peruanos por el Kambio’ con el 22,11% y la candidata del Frente Amplio Unido Verónica Mendoza con el 17, 12 %, con la significativa participación del 84% del censo electoral lo que equivale a 15.400.000 ciudadanos.

Los resultados parciales se acercan a lo previsto en las encuestas, replicándose en la elección del congreso de la república donde el fujimorismo alcanzaría 60 de los 130 escaños configurándose como la primera fuerza legislativa, 25 serían para Peruanos por el Kambio, 22 para el Frente Amplio Unido, 11 para Alianza por el progreso, 6 para Acción Popular y  la misma cantidad para Alianza Popular.

La segunda vuelta que tendrá lugar el próximo 5 de junio, enfrentaría a dos candidatos sobre los que existen fuertes cuestionamientos en su contra, por un lado, Keiko Fujimori fue investigada por entregar dádivas durante eventos públicos de su campaña y por otro lado está la memoria de su participación como primera dama durante el gobierno de su padre, Alberto Fujimori, en el que se cometieron crímenes de corrupción y de lesa humanidad por los que paga 25 años de prisión., incluidos 6.600 casos de desaparición y la esterilización forzada de por lo menos 300.000 mujeres campesinas entre otros.

Adicionalmente, en el escándalo de los llamados ‘Papeles de Panamá’, han sido relacionados los nombres de por lo menos dos aportantes de dinero a la campaña de Fujimori: Jorge Yoshiyama, quien contribuyó con poco más de 113.000 dólares a las postulaciones de Fujimori en 2011 y 2016, y Sil Yok Lee quien aportó portó 41.200 dólares en 2011.

Por los lados de Kuczinsky, se le involucra también en los Papeles de Panamá por la aparición de una nota dirigida a a Mossack Fonseca en 2006 en la que recomendaba que decía le “era grato presentar al señor Francisco Pardo Masones” dando fe de conocerle de tiempo atrás, de sus cualidades y prestancia; un personaje que resultaría haciendo negocios en Cuba con pasaportes venezolanos.

Al candidato de Centro Derecha se le atribuye la responsabilidad de permitir que la Internacional Petroleum Company (IPC) se llevara 17 millones de dólares del erario nacional, cuando se desempeñaba como asesor económico y gerente del Banco Central de Reserva, teniendo conocimiento que la empresa había sido nacionalizada en 1968.

Otro de los escándalos con los que la prensa relaciona a Kuczinsky es el generado su ONG ‘Agua limpia’, por medio de la cual habría canalizado los recursos para su campaña con fondos de USAID y la CIA, con la excusa de proveer agua potable y alcantarillado a poblaciones, dinero que sería recuperado por los inversionistas de resultar electo con la explotación de los recursos hídricos con un alto costo para los pobladores.
