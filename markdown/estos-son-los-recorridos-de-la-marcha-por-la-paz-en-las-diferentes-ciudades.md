Title: Recorridos de la "Marcha por la Paz" en las diferentes ciudades
Date: 2015-04-09 07:59
Author: CtgAdm
Category: Movilización, Nacional
Tags: 9 de abril, Barranquilla, Bogotá, Bucaramanga, Cali, colombia, Marcha por la Paz, Medellin, Organizaciones sociales, pasto
Slug: estos-son-los-recorridos-de-la-marcha-por-la-paz-en-las-diferentes-ciudades
Status: published

#####  Foto: [nelsonlombana.wordpress.com]

Este 9 de abril, diversas organizaciones sociales, sindicales, de víctimas, animalistas, entre otras, se congregarán en diferentes puntos del país, con el objetivo de movilizarse por las víctimas, el cese bilateral al fuego y en respaldo al proceso de paz.

Barranquilla, Bucaramanga, Medellín, Cali, Neiva, Pasto y Bogotá, serán las ciudades donde se espera la mayor concentración de personas. Aquí les contamos cuáles serán los recorridos que se realizarán en cada ciudad.

**En Bogotá,** empezará a las 10:00 am y tomará la calle 26 hacia el occidente hasta la carrera 50 calzada oriental, donde los participantes irán hacia el norte hasta la calle 63 para llegar al parque Metropolitano Simón Bolívar.

**En Pasto, **la marcha sale a las 8:00 a.m. del parque Santiago para llegar a la Plaza de Nariño. Allí se busca acompañar a las más de 300 mil víctimas que ha dejado el conflicto armado en Nariño.

**En Barranquilla,** las personas saldrán de la  carrera 21 con Murillo, de allí tomará la carrera 45, finalizando en la Plaza de la Paz.

**En Medellín,** la movilización saldrá del parque de Los Deseos a las 9:00 am, y recorrerá varias calles del centro de la ciudad, avanzando por la carrera Carabobo, para tomar luego la calle Las Paz, empalmar con la carrera Sucre, en sentido sur hasta el parque de San Antonio, bajar a la Avenida San Juan hacia el occidente y concluirá en el parque de Las Luces.

En esta plazoleta de las Luces, los marchantes permanecerán concentrados para vivir un gran concierto por la paz con participación de varias agrupaciones musicales y artistas que han querido sumarse a la jornada, hasta las 8 .00 pm.

**En Neiva,** se espera la llegada de más de 40 mil personas procedentes de los departamentos de Quindío, Tolima, Caquetá, Putumayo y Huila. Habrá tres puntos de concentración, en Surabastos al sur, El triángulo al norte, Reservas de la Sierra al oriente y el centro de convenciones José Eustasio Rivera, para finalmente llegar y reunirse a la altura de la Estación del Ferrocarril.

La marcha está programada para iniciar a las 9:00 de la mañana desde la Calle Octava hasta la Carrera Séptima, tomarán la Calle Décima hasta la Carrera Cuarta y llegarán al Parque Santander donde se llevará a cabo el gran acto cultural por la paz.

**En Bucaramanga,** La marcha partirá a las 8:30 a.m. de los alrededores del Estadio Alfonso López. Desde allí, irán hacia el sector de Guarín y la carrera 33, para luego bajar por la calle 36 hacia el Centro de Bucaramanga.

En la Plaza Cívica Luis Carlos Galán estará habilitada una tarima en donde se presentarán durante todo el día actos culturales. El cierre estará a cargo del grupo musical Velandia y La Tigra, que se subirá al escenario entre las 3:00 y las 4:00 de la tarde.

**En Cali**, están previstas dos concentraciones, una en el Parque del avión, en el norte y otra en el Coliseo del Pueblo, en el sur, ambas a las 8:00 de la mañana, para movilizarse hacia la Manzana T, frente al CAM.

La del norte tomará por la calle 34 para avanzar por la Avenida 3 hasta la manzana T. La del coliseo sigue por la calle quinta para tomar la carrera 50 , la calle 9 hasta la carrera 4 donde dobla hacia la calle 12. Tránsito, Policía y organismos de socorro estarán atentos. Se esperan delegaciones del norte del Valle, sur del país y la costa Pacífica.
