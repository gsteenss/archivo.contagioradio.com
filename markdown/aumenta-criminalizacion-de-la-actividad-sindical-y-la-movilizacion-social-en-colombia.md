Title: En 3 años se han cuadriplicado las protestas de trabajadores contra empresas
Date: 2015-11-18 17:15
Category: DDHH, Nacional
Tags: Actividad sindical en Colombia, cajar, CUT, declaratorias de ilegalidad de huelgas, desmanes y atropellos de la fuerza pública contra manifestantes, Encuentro Nacional ‘Criminalización de la acción sindical y social’, ENS, procesos penales contra dirigentes sindicales
Slug: aumenta-criminalizacion-de-la-actividad-sindical-y-la-movilizacion-social-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/CUT-e1473195661246.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País ] 

###### [18 Nov 2015 ]

[En Bogotá se desarrolla el Encuentro Nacional ‘Criminalización de la acción sindical y social’, convocado por la Central Unitaria de Trabajadores, CUT, y apoyado por la Escuela Nacional Sindical y el Colectivo de Abogados José Alvear Restrepo, en el que diversas organizaciones sindicales y de defensa de derechos humanos se dan citan para **evaluar la situación de criminalización de la acción sindical entre 2010 y 2015**, así como las posibles acciones jurídicas y políticas de defensa para el movimiento social.]

[En el centro de la agenda de la CUT se sitúa la exigencia de garantías para el ejercicio de la libertad sindical, en particular para el derecho a la movilización. De acuerdo con la CUT **en los últimos 3 años se han cuadriplicado el número de protestas adelantadas por trabajadores** con motivos laborales, así como la cantidad de sus afiliados, actualmente registra **150 mil nuevas inscripciones**. Avances frente a los que tanto el **sector empresarial como el estatal han respondido con** acciones que ellos denominan **“guerra jurídica”**.]

[En este sentido, las situaciones que alarman a la CUT tienen que ver con el alarmante **incremento de hechos de criminalización contra organizaciones y dirigentes sindicales**, “se han vuelto comunes los **desmanes y atropellos de la fuerza pública** contra manifestantes participantes de movilizaciones, los **procesos penales** contra dirigentes sindicales, las **declaratorias de ilegalidad de huelgas**, las demandas para la disolución y liquidación de organizaciones sindicales”, aseguran sus directivas, por lo que realizar un Encuentro de esta envergadura "se hace necesario".]
