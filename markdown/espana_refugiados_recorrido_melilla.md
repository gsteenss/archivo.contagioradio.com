Title: Más de 500 personas recorren España exigiendo la acogida de las y los refugiados
Date: 2017-07-19 02:50
Category: El mundo
Tags: crisis migratoria, españa, Refugiados, Unión europea
Slug: espana_refugiados_recorrido_melilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/DEriNHXXUAAlZqA-e1500447237495.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ecuador Etxea 

###### [19 Jul 2017] 

[“No vamos a dejar que la xenofobia y el racismo se apodere de Europa”, es el grito de más de 500 personas que iniciaron el pasado 14 de julio un recorrido por España hasta llegar a la frontera sur de ese país para denunciar las políticas migratorias de Europa y especialmente del Estado Español, junto a las **violaciones a los derechos humanos de quienes buscan refugio fuera de sus lugares de origen.**]

[Se trata de la caravana ‘Abriendo Fronteras’, que inició el viernes y finalizará el próximo sábado 22 de julio. “Queremos denunciar las violaciones sistemáticas de derechos humanos y señalar a instituciones, organismos y transnacionales que están detrás de los desplazamientos forzados y que no garantizan el derecho a migrar, ni la protección internacional en caso del refugio”, señala Beatriz Plaza, integrante de la Plataforma Ongi Etorri Errefuxiatuak, una de las organizaciones que participa en la actividad.]

[Decenas de colectivos del País Vasco, Castilla y León, Catalunya, Valencia, Madrid y Andalucía participan en la caravana que ya ha realizado varias de las acciones planteadas para reivindicar el refugio, y exigir que se tomen medidas eficaces contra **los abusos, la violencia, la trata y el tráfico de personas, especialmente de las mujeres, la comunidad LGBTI y las y los niños; sectores doblemente victimizados** durante su huída de las guerras y las condiciones precarias en las que viven en sus territorios.]

[Para este lunes las y los integrantes de la caravana ya llegaron a Málaga, desde donde han cruzado el estrecho hasta Melilla. El lugar por donde miles de migrantes buscan pisar territorio español, pero cuyo paso se impide por una valla que además se encuentra bajo estrictos niveles de seguridad.]

[Allí, en la frontera sur de España, en las costas del Mediterráneo,  los manifestantes realizan una serie de actividades culturales, protestas y asambleas hasta el 21 de julio, para plantear propuestas y, entre otros temas, hablar sobre] **la embarcación C-Star, alquilada por el movimiento xenófobo Defend Europa, que quiere dificultar las labores de rescate de las ONG que socorren a las personas migrantes.**[ Luego las movilizaciones se devolverán a Almería para denunciar la situación de explotación en los invernaderos.]

### [España no ha cumplido] 

[“Nuestras fronteras deben estar abiertas para quienes migran en busca de una mejor vida (...) el Mar Mediterráneo, hoy, es una fosa común" denuncia Plaza, quien agrega que existe toda una política de criminalización contra las y los migrantes. El cierre de fronteras europeas está abriendo vías de migración “cada vez más peligrosas”, que a su vez  están causando la muerte de miles de personas en su intento por llegar a España.]

[La caravana exige que el Gobierno cambie su política de recepción de personas víctimas de desplazamiento forzado y cumpla con los pactos internacionales que estableció la Unión Europea en 2015. ]Y es que según datos del informe Frontera Sur 2016, de  la Asociación Pro Derechos Humanos de Andalucía (APDHA) en dos años, la cifra de migrantes fallecidos o desaparecidos se han duplicado. **Para 2014 hubo 131 personas fallecidas en su intento por llegar a a las costas de España, en 2015 fueron 195 y en 2016 ya se contabilizaban 295.**

Patricia Bárcenas, directora de CEAR-Euskadi explica que pedir asilo es  una carrera de obstáculos: “el primero de ellos es salir del país para pedir protección, el segundo es llegar a una nación segura, y el tercero es que el sistema jurídico está conformado de tal manera que tiende a denegar la protección”.

[Diversas ONG advierten que el Estado Español se había comprometido a acoger a un total de 17.337 personas. **En septiembre debía haberse cumplido con dicha cifra,  y sin embargo solo han sido acogidas 1497,** según el conteo que lleva Amnistía Internacional - España. Por ejemplo, de acuerdo con un reciente informe de la Comisión Española de Ayuda al Refugiado, CEAR, en 2016 hubo 10.250 peticiones de asilo, pero tan solo 355 obtuvieron el estatuto de refugiado.]

\

<iframe id="audio_19890177" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19890177_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
