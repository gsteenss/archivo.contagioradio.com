Title: El mundo
Date: 2014-11-21 15:04
Author: AdminContagio
Slug: el-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/EKc-kG-XYAI4Q2K.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Nasa-e1502385039236.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Captura-de-pantalla-2016-12-14-a-las-12.34.23.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Donald-Trump.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Ni-una-menos-e1476815647809.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/dc22b51c-0abb-465a-83ed-c6df5ed90683.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### EL MUNDO

[![\#5000TroopsToColombia: ¿Agresión mediática contra Venezuela?](https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400.png "#5000TroopsToColombia: ¿Agresión mediática contra Venezuela?"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400.png 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400-300x156.png 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400-768x399.png 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400-370x192.png 370w"}](https://archivo.contagioradio.com/5000troopstocolombia-venezuela/)  

###### [\#5000TroopsToColombia: ¿Agresión mediática contra Venezuela?](https://archivo.contagioradio.com/5000troopstocolombia-venezuela/)

[<time datetime="2019-01-29T13:14:19+00:00" title="2019-01-29T13:14:19+00:00">enero 29, 2019</time>](https://archivo.contagioradio.com/2019/01/29/)¿Es la nota de Boltón sobre 5000TroopsToColombia una amenaza a Venezuela? ¿Qué papel jugaría Colombia en medio de esta afirmación?[LEER MÁS](https://archivo.contagioradio.com/5000troopstocolombia-venezuela/)  
[](https://archivo.contagioradio.com/amenazas-diputado-gay-brasil/)  

###### [El mensaje de despedida del diputado gay amenazado en Brasil](https://archivo.contagioradio.com/amenazas-diputado-gay-brasil/)

[<time datetime="2019-01-26T14:41:26+00:00" title="2019-01-26T14:41:26+00:00">enero 26, 2019</time>](https://archivo.contagioradio.com/2019/01/26/)Jean Wyllys primer diputado abiertamente gay en la historia de Brasil se vió obligado a no asumir su curul por tercera vez consecutiva tras recibir amenazas[LEER MÁS](https://archivo.contagioradio.com/amenazas-diputado-gay-brasil/)  
[](https://archivo.contagioradio.com/fujimori-vuelve-prision/)  

###### [“El final de mi vida esta cerca…” Fujimori tras volver a prisión](https://archivo.contagioradio.com/fujimori-vuelve-prision/)

[<time datetime="2019-01-24T12:30:55+00:00" title="2019-01-24T12:30:55+00:00">enero 24, 2019</time>](https://archivo.contagioradio.com/2019/01/24/)Según la Corte de Justicia y el Instituto de medicina legal los problemas cardíacos de Fujimori pueden atenderse de forma ambulatoria en centro carcelario[LEER MÁS](https://archivo.contagioradio.com/fujimori-vuelve-prision/)  
[](https://archivo.contagioradio.com/relaciones-salpican-flavio-bolsonaro/)  

###### [Hijo de tigre… las cuestionadas acciones de Flávio Bolsonaro](https://archivo.contagioradio.com/relaciones-salpican-flavio-bolsonaro/)

[<time datetime="2019-01-23T13:57:53+00:00" title="2019-01-23T13:57:53+00:00">enero 23, 2019</time>](https://archivo.contagioradio.com/2019/01/23/)Además de las dudas por sus cuentas bancarias, se cuestiona a Bolsonaro su relación con la familia del principal sospechoso del asesinato de Marielle Franco[LEER MÁS](https://archivo.contagioradio.com/relaciones-salpican-flavio-bolsonaro/)
