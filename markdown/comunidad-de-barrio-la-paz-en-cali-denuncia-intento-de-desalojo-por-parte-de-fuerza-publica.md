Title: Comunidad de Barrio La Paz, en Cali, denuncia intento de desalojo por parte de Fuerza Pública
Date: 2018-04-05 16:25
Category: DDHH, Nacional
Tags: Cali, Jarillón
Slug: comunidad-de-barrio-la-paz-en-cali-denuncia-intento-de-desalojo-por-parte-de-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/latoma.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Toma ] 

###### [05 Abr 2018] 

El pasado 2 de abril, los habitantes del barrio La Paz, en Cali, denunciaron haber sido víctimas de un intento de desalojo por parte del alcalde Maurice Armitage, **que desplegó un operativo con presencia de agentes del ESMAD, cuerpo de Carabineros, Policía, GOES** y Ejército, para ser desalojados de su territorio, para lograr poner en marcha parte del Plan Jarillón.

De acuerdo con la denuncia, la presencia de la Fuerza Pública se presentó en horas de la madrugada, **portando armas largas, mientras que un helicóptero, perteneciente a esta institución, sobrevolaba el área**, provocando intimidación en la población. Así mismo, la comunidad aseveró que los agentes del ESMAD intentaron cortar los cables de la alarma del barrio para que la población no estuviera atenta de lo que pasaba.  (Le puede interesar:["Fuerza Pública arremete contra población en Popayán"](https://archivo.contagioradio.com/fuerza-publica-arremete-contra-comunidad-en-popayan/))

Posteriormente la Fuerza Pública se ubicó en una zona verde en la que la existía un gimnasio, un parque y una cancha de fútbol, que segundos después fueron demolidos, por la constructora Enlace S.A e I.C Prefabricados S.A, **que pretenden construir allí un megaproyecto de 2.000 apartamentos**.

### **El Plan Jarillón** 

Los 2.000 apartamentos que serían construidos, harían parte del Plan Jarillón de vivienda de interés social, para reubicar a las familias del Jarillón y del Centro. Sin embargo, la comunidad ha rechazado enfáticamente este proyecto manifestando que tendría un nefasto impacto social y ambiental, **razón por la cual ya han activado mecanismos jurídicos y acciones colectivas para defender su territorio**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
