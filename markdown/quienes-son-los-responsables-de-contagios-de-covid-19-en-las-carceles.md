Title: ¿Quiénes son los responsables de contagios de Covid 19 en las cárceles?
Date: 2020-05-27 18:43
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: #MovimientoCarcelario #Covid-19 #, carceles de colombia, Coronavirus, Covid-19, Expreso Libertad, INPEC, situacion en las carceles de colombia
Slug: quienes-son-los-responsables-de-contagios-de-covid-19-en-las-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/proteccion_guardias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En este programa del **Expreso Libertad**, el abogado Uldarico Flores y la ex prisionera política Liliany Obando hablan sobre la pandemia de Covid-19 en las cárceles de Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### A la fecha ya hay más de mil afectados y tres personas muertas producto del virus.

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con ellos, es urgente que las instituciones correspondientes inicien investigaciones hacia el Ministerio de Justicia, en cabeza de Margarita Ceballos y en contra del **INPEC**, dirigido por el general Roberto Mojica. Esto con la finalidad de esclarecer cómo se dieron los contagios al interior de los centros de reclusión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, expresan que también se debe investigar por qué hay una ausencia de medidas en torno a salvaguardar la vida de la población privada de la libertad. Falta de acciones que para ambos defensores de derechos humanos podría desembocar en un "genocidio del movimiento carcelario".

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/174023304033666/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/174023304033666/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Mas programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
