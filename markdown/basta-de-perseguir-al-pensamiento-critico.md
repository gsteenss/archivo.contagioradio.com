Title: ¡Basta de perseguir al pensamiento crítico!
Date: 2020-02-19 15:19
Author: AdminContagio
Category: Expreso Libertad, Programas
Tags: Expreso Libertad, pensamiento crítico, persecución, Persecución Judicial, Persecución Sistemática, Universidad Pedagógica Nacional
Slug: basta-de-perseguir-al-pensamiento-critico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/e12fd043c0ef3d5fef8938c7ec147f6b.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/clase-universidad.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Los profesores **Natalia Carusso **y **Helbert Choachí** manifestaron en los micrófonos del **Expreso Libertad** las distintas formas de persecución de las que ha sido víctima la comunidad académica de la Universidad Pedagógica Nacional, que no solamente ha cobrado la vida de estudiantes y docentes, sino que actualmente los ha convertido en objetos de montajes judiciales. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con los docentes detrás de este tipo de hechos se encontraría la inteligencia militar y el Estado, con la intención de acabar con el pensamiento crítico al interior de este centro educativo, a través de estrategias como el amedrentamiento. Razón por la cual aseguraron que el "pensar" en sí mismo ya es un acto crítico y el Estado no puede cercenarlo, sino que por el contrario debe continuar la profundización del mismo. 

<!-- /wp:paragraph -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2744882778965100%2F&amp;width=600&amp;show_text=true&amp;appId=1237871699583688&amp;height=404" width="600" height="404" frameborder="0" scrolling="no"></iframe>

<!-- wp:paragraph -->

Vea mas [programas de expreso libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
