Title: Consejo de Estado pide nueva comisión de expertos sobre fracking
Date: 2019-06-10 17:20
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Comisión de Expertos, Consejo de Estado, fracking
Slug: consejo-de-estado-pide-nueva-comision-de-expertos-sobre-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/impactos-del-fracking-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: aimdigital] 

[El pasado 7 de junio, mientras que más de 100 municipios se movilizaban en contra del fracking, el Consejo de Estado estudiaba una demanda del Grupo de Litigio e Interés Público de la Universidad del Norte, por medio del cual pidieron la nulidad de dos normas que dan vía libre a la implementación de esta técnica de extracción no convencional.]

[Durante esta audiencia pública, el alto tribunal solicitó la]**conformación de una nueva comisión de expertos para evaluar los riesgos que correría el ambiente y las comunidades si se activara el fracking en Colombia,** teniendo en cuenta los antecedentes en otros países del mundo.

[A diferencia de la comisión anterior, **esta no estaría conformada por el Ministerio de Ambiente y el de Minas y Energía sino por la Universidad Nacional**. El magistrado Ramiro Pazos explicó que la Universidad tiene los conocimientos específicos de ecosistemas, geología, hidrología, sociología, antropología, entre otros temas, para responder a las preguntas técnicas restantes.]

[Sin embargo, Carlos Andrés Santiago, integrante de la Alianza Colombia Libre del Fracking, aseguro que esta decisión del Magistrado Pazos corresponde a los señalamientos que se presentaron frente a las conclusiones de la previa comisión por sus vínculos con la industria petrolera, entre otras razones. (Le puede interesar: "][[Expertos en fracking se quedan cortos con sus recomendaciones]](https://archivo.contagioradio.com/expertos-fracking-se-quedan-cortos-recomendaciones/)[")]

["Ante las dudas, las preguntas y los argumentos de las contrapartes, teniendo en cuenta que esta comisión de expertos también tenía un sesgo y tuvo una conformación de parte del Gobierno nacional, el magistrado tomó la decisión que fuera la Universidad Nacional la que en 15 días tendría que conformar una comisión que pueda elaborar un estudio", afirmó Santiago. (Le puede interesar: '"][[En este momento en Colombia, no es legal hacer fracking]](https://archivo.contagioradio.com/en-este-momento-en-colombia-no-es-legal-hacer-fracking/)["')]

### **La nueva comisión de expertos**

[Esta nueva comisión de expertos tendrá un plazo de tres meses para evaluar los impactos de esta actividad en el ambiente y en la salud de las personas en las zonas de influencia, en especial, estará investigando los efectos de esta técnica para el agua y la estabilidad sísmica de las regiones.]

[Según Santiago, estas conclusiones serán de mayor importancia para el Alto Tribunal en el momento que analice la demanda presentado en contra del Decreto 3004 del 2013 y la Resolución 90341 del 2014 del Ministerio de Minas, dado que los accionantes argumentan que estas normas no tuvieron en cuenta el principio de precaución como lo pide la Ley.]

[En la Ley 99 de 1993, se establece que "cuando exista peligro de daño grave e irreversible,]**la falta de certeza científica absoluta no deberá utilizarse como razón para postergar la adopción de medidas eficaces para impedir la degradación del medio ambiente**[".]

[A pesar de que el término de entrega del estudio es de tres meses, el líder ambiental aseguró que la Universidad del Norte puede solicitarle al Consejo de Estado a prolongar el tiempo para la realización de la investigación. Además, aseguró que la conformación de una comisión independiente de la industria petrolera y el Gobierno es un importante paso adelante para dar a conocer los riesgos verdaderos de la implementación del fracking.]

<iframe id="audio_36912778" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36912778_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
