Title: Hidroituango no acata las medidas ambientales
Date: 2018-08-14 12:24
Category: Ambiente, Nacional
Tags: EPM, Hidroituango, megaproyectos, megaproyectos en colombia, represas
Slug: hidroituango-no-cumple-con-medidas-ambientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural] 

###### [13 Ago 2018] 

La plataforma **Ríos Vivos** denunció que la sociedad Hidroeléctrica Ituango S.A. E.S.P. está incumpliendo las [medidas impuestas por  la Agencia  Nacional de Licencias Ambientales](http://www.anla.gov.co/Noticias-ANLA/anla-impone-17-medidas-manejo-ambiental-hidroituango), ANLA, al seguir  con las obras sin atender las medidas de manejo ambiental que emitió dicha autoridad.

Para la organización esto se debe a que la ANLA, no dio ordenes claras frente a cuáles son las obras relacionadas con la emergencia y cuáles son las obras relacionadas con el avance del megaproyecto que tendrían que frenarse para disminuir el riesgo.

**EPM oculta información importante**

De acuerdo con Zuleta, los derrumbes en ambas márgenes del cañón, a la altura de la megaobra,  han sido constantes y el más reciente se presentó el día de ayer,** información que estaría siendo ocultada por la empresa. **(Le puede interesar:[ "EPM demanda a víctimas de Hidroituango: Ríos Vivos"](https://archivo.contagioradio.com/epm-demanda-a-victimas-de-hidroituango/))

> Atención: nuevo derrumbe en [@hidroituango](https://twitter.com/hidroituango?ref_src=twsrc%5Etfw) en sector El Palmar, entre la puerta de El Bombillo y la presa, al interior de la obra. Imágenes de hace cinco minutos, tomadas por transportadores que se manifiestan en contra de [@EPMestamosahi](https://twitter.com/EPMestamosahi?ref_src=twsrc%5Etfw) [\#ConlaVidaNOsejuega](https://twitter.com/hashtag/ConlaVidaNOsejuega?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/KyFiLvN3Ki](https://t.co/KyFiLvN3Ki)
>
> — ClaudiaJulietaDuque (@JulieDuque1) [12 de agosto de 2018](https://twitter.com/JulieDuque1/status/1028766892302000130?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Sumado a ello, las quebradas estarían completamente saturadas, afectado la movilidad tanto del municipio de Ituango como de las diferentes veredas. A su vez la empresa EPM estaría continuando con **la tala de bosque y vertiendo los residuos a las quebradas que se encuentran estancadas, aumentando los riesgos**.

Otra de las preocupaciones que tienen las comunidades es que tampoco se han dado los subsidios que prometió EPM, para el pago de arriendos, "hay personas a las que les dieron el primer subsidio y no les han pagado el segundo, entonces están a punto de que los saquen de sus viviendas, hay personas que no fueron censadas y en la mayoría de los municipios no se hizo un censo adecuado" afirmó Zuleta.

Asimismo, la vocera de RíosVivos aseveró que a las comunidades se les ha dicho que deben acostumbrarse a esta situación, hecho que no tiene sentido cuando las personas se encuentran sin trabajo, sin alimentación adecuada o atención médica.

**Abren investigación por obras en los** [**túneles sin autorización **]**ambiental**

Luego del informe emitido por la Contraloría General, la ANLA abrió una investigación preliminar por trabajos que habría desarrollado EPM sin tener autorización ambiental, según el informe en el año 2015 la empresa adelantó obras para el Sistema Auxiliar de Desviación sin previa autorización.

El ente ambiental contará con seis meses para dar resultados en esta investigación que podría acarrear sanciones a la Sociedad Hidroituango, propiedad de las Empresas Públicas de Medellín.

 <iframe id="audio_27780137" src="https://co.ivoox.com/es/player_ej_27780137_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
