Title: Petra: Una mirada al Universo trans desde la danza afro
Date: 2018-05-04 10:59
Category: eventos
Tags: afro, Bogotá, danza, trangénero
Slug: petra-una-mirada-al-universo-trans-desde-la-danza-afro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Petra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zarabanda 

###### 05 May 2018 

La compañía Zarabanda con la dirección del bailarín y coreógrafo René Arriaga traen al escenario de Factoria L´expose "PETRA", un montaje de danza afro-contemporánea inspirada en un cuento escrito por el mismo Arriaga, enmarcado en el universo transgénero.

La historia se desarrolla en un pequeño apartamento, Petra ha citado a un hombre que conoció en una fiesta, se ha dado una nueva oportunidad para conocer a alguien. Durante ese día, antes de la cita, se creara un mundo onírico para poder afrontar sus pensamientos, escapar de sus conflictos y poder estar plena. Donde los recuerdos de dolor y tristezas aparecen atormentándola constantemente, pero su amor por si misma, por sus deseos, logran sacarla de la oscuridad.

PETRA utiliza varias características del movimiento afro como el manejo del torso, brazos, piernas, agilidad y saltos, entre otros,  todo esto enmarcado en una temática actual, gracias a la interpretación de los bailarines Patricia Ondo, Mónica Chávez, Gabriela Pardo y Rossana Montoya.

“Todos tuvimos un cambio importante de comprensión y reconocimiento sobre las personas transgenero y de todo lo que pasa en su cotidianidad, para de esa manera visibilizarlos desde la danza”, afirma el director. Otro de los retos de la pieza, fue la composición musical a cargo de Felipe Meza, quién en esta oportunidad, incursiona en el uso de instrumentos como el violonchelo y el piano junto a tambores, una mezcla de música clásica y africana.

Si desea asistir a esta obra que atraviesa al mundo transgénero, las dificultades y señalamientos por las que atraviesa esta comunidad en la sociedad, puede hacerlo del 10 al 19 de mayo, de jueves a sabado a las 8:00p.m. en Factoria L´expose, ubicada en la carrera 25 \#50-34, Bogotá.

Bono de apoyo: \$30.000 general y \$25.00 para estudiantes y tercera edad. Programe su agenda y sea espectador de esta nueva pieza artística que llega a la capital.
