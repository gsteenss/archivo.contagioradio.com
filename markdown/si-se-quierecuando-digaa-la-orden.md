Title: Si se quiere…cuando diga…a la orden
Date: 2015-01-26 16:22
Author: CtgAdm
Category: Ambiente y Sociedad, Opinion
Tags: colombia, normas ambientales, protocolo de kioto
Slug: si-se-quierecuando-digaa-la-orden
Status: published

**[Por Margarita Flórez]**

Desde hace una década las normas ambientales han venido siendo sustituidas por estándares y guías de voluntario cumplimiento, así como de acuerdos mundiales con alguna capacidad de coerción se pasó a la buena voluntad de los países, como es el caso del documento que parece se convertirá en la continuidad del protocolo de Kyoto. Del boom normativo a partir de Río 92, que fue el origen de las numerosas disposiciones sobre información, y participación ambiental se pasó a los enunciados voluntaristas acompañados con procesos de certificación privados que poco a poco le han quitado papel de las instituciones ambientales, las cuales a su vez, han sufrido una transformación en pro de una menor disponibilidad de dispositivos que ejerzan la defensa de derechos fundamentales.

Las normas nacionales se complementaban con los condicionamientos palabra de no buen recibo que establecieron las entidades de crédito multilateral como el Banco Mundial, y que de manera clara determinaba en qué momento, y de qué clase eran los requisitos que se exigían para otorgar o seguir desembolsando un crédito. No quiere esto decir que todas las obras financiadas de esta manera cumplieran con las consideraciones que los ambientalistas pensamos se deben observar cuando de asegurar el derecho al medio ambiente sano nos referimos pero funcionaban como una talanquera que podía ser aprovechada por los afectados de determinada obra para colocar algunas de sus pretensiones; y colocaba a los gobiernos frente a cierta vigilancia. Se tenía un marco.

Pero las cosas han cambiado y estas salvaguardias socio ambientales también se están eliminando, y se convierten en estándares que en la mayoría de los casos obedecen a la lógica definida en el ámbito nacional. Por ejemplo respecto de las evaluaciones ambientales y sociales (EIAS) se cambió la anterior disposición que señalaba el término en que debían hacerse, por un enunciado sobre que se deben iniciar “lo más temprano posible”. Estas imprecisiones producen efectos claros como es el deterioro de la participación y consulta de sectores de la población, quienes quedan sin información sustantiva sobre un proyecto determinado, es decir se sigue respetando este derecho, pero convertido en un ejercicio inocuo sin argumentos científicos o técnicos para controvertir.

Esta desregulación en materia ambiental en todos los niveles nacional, regional, e internacional muestra claramente cuál es la intención de aquellos que invierten en los proyectos, de los gobiernos que establecen dispositivos para asegurar esos intereses, y está acorde con las numerosas disposiciones que a nivel internacional, y nacional se dictan sobre protección a las inversiones y los severos tribunales de arbitramento que obran como cortes frente al cumplimiento de dichas normas.[ ]{.Apple-converted-space}En la carrera de las regulaciones las únicas que aseguran mecanismos de coerción son las financieras.

No sería adecuado sostener que la sola presencia de las normas ambientales sin indicadores y previsiones claras como las que existentes hayan detenido conflictos socio ambientales pero su ausencia si debilita a los ciudadanos, y su reemplazo por guías voluntarias sitúan el tema en el plano de lo privado, y ahí sí habría un gran cambio por la casi imposibilidad de conocer sus alcances,[ ]{.Apple-converted-space}y la inocuidad de los mecanismos de información y participación ambiental.

**[Margarita Flórez]**

Directora de Ambiente y Sociedad
