Title: A 8 asciende la cifra de asesinatos tras represión policial en Oaxaca
Date: 2016-06-21 12:10
Category: El mundo, Movilización
Tags: Comité de Defensa Integral de Derechos Humanos Gobixha AC, Coordinadora Nacional de trabajadores de la Educación, reforma educativa México, represión policial México
Slug: a-8-asciende-la-cifra-de-asesinatos-tras-represion-policial-en-oaxaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Represión-Oaxaca.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: VTV ] 

###### [21 Junio 2016 ]

De acuerdo con el más reciente informe de la 'Coordinadora Nacional de Trabajadores de la Educación' y del 'Comité de Defensa Integral de Derechos Humanos Gobixha AC', **a ocho asciende la cifra de personas asesinadas** **en el marco de la represión de la Policía Federal** a la movilización magisterial en Oaxaca, siete por impactos de bala y una por una explosión, dos de ellas serían docentes.

Las organizaciones reportan **8 personas desaparecidas y otras 24 detenidas**, y pese a la tensión, continúan en la región intentando precisar las cifras y acompañando a las comunidades. Sara Méndez, integrante del Comité, asevera que el "uso de la fuerza fue completamente desproporcionado, llegaron a disparar contra la población civil (...) ésta no puede ser la manera en que se implemente una reforma educativa". En estos momentos en Oaxaca continúan los bloqueos a las vías de acceso.

"Crece la indignación y la solidaridad de la gente (...) por un lado hay temor de que llegue el Ejército a desalojar, también hay indignación ante la falta de atención médica en los hospitales y clínicas de la localidad, a las personas que resultaron heridas de diferentes formas", asegura Méndez, e insiste en que se confirma que [[autoridades ordenaron que no se atendiera a los heridos civiles](https://archivo.contagioradio.com/6-asesinatos-y-108-personas-heridas-tras-represion-de-movilizacion-en-oaxaca/),]por lo que la **iglesia local abrió sus puertas para montar una clínica ambulante para atender a la población**. [ ]

Vale la pena recordar que en mayo de este año, Christof Heyns, relator especial de la ONU para Ejecuciones Extrajudiciales, Sumarias o Arbitrarias, aseveró que "**en México se mantiene el uso desmesurado de la fuerza pública**. Las medidas de protección siguen siendo insuficientes e ineficaces; la impunidad y la falta de rendición de cuentas en relación a violaciones al derecho a la vida se mantienen como serios desafíos, al igual que la ausencia de reparación para las víctimas".

El alto funcionario insistió en la urgencia de que en la nación latinoamericana se apruebe una Ley General sobre el uso de la fuerza, que permita la consolidación de una institución nacional autónoma de servicios forenses y la **garantía inmediata de que la seguridad pública esté a cargo de civiles y no de las fuerzas armadas**.** **

<iframe id="audio_11979366" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11979366_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
