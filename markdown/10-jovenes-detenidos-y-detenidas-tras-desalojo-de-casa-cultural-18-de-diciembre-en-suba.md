Title: 11 jóvenes detenidos y detenidas tras desalojo de Casa Cultural 18 de Diciembre en Suba
Date: 2015-07-16 18:39
Category: DDHH, Nacional
Tags: Barrio Corinto, Casa Cultural 18 de Diciembre, Colectivo Suba Nativa, desalojo a familia, ESMAD, Localidad de Suba
Slug: 10-jovenes-detenidos-y-detenidas-tras-desalojo-de-casa-cultural-18-de-diciembre-en-suba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11722256_1053047288038716_4906329120469884496_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colectivos Suba 

##### [16 jun 2015]

En la mañana de hoy fue **desalojada la Casa Cultural 18 de Diciembre**, en el barrio Corinto de la localidad de Suba. **Esta Casa** se había convertido en una **referente de cultura y organización**, tras repetidas amenazas de desalojo. Durante el operativo, **10 personas fueron  detenidas** luego de oponerse a que fuese entregada.

\

El desalojo se ha llevado a cabo con la **presencia del Escuadrón Movil Anti Disturbios, ESMAD**, y fuerzas policiales, las cuales se han ubicado frente a la Casa Cultural junto con una tanqueta, atemorizando  a vecinos y vecinas del sector.

A pesar de la presencia de cientos de personas que se opusieron al operativo, 1o jóvenes integrantes de organizaciones y colectivos juveniles de la localidad que se han negado a la decisión, han sido detenidos. Una primera versión indica que los detenidos habrían sido acusados por "agresión a servidores públicos". Sus nombres son Juan Bernal, Jorge Leon, Cristian Rozo, Carlos Rondón, Jeison Ortíz, Antonio Torres, Miguel Echeverri, Daniel Luna, Paula Quemba, Kimberly Moreno, Jessenia Gutierrez y Karen Gutierrez. Agresión a servidor público

En el transcurso de la tarde se conoció que las personas detenidas fueron conducidas a los juzgados de Paloquemao, donde se realizó un plantón de solidaridad con estas personas, a partir de las 3 Pm en las mismas instalaciones de los juzgados.

La Casa Cultural 18 de Diciembre habría sido fundada en esta fecha **tras múltiples amenazas de desalojo a la familia de Antonio Torres**, uno de los propietarios, por parte de la Inspección 11, sede Policía. Vea también: [Casa Cultural 18 de Diciembre en suba podría ser desalojada](https://archivo.contagioradio.com/casa-cultural-18-de-diciembre-en-suba-podria-ser-desalojada/). Esta se había constituido en un **espacio artístico y cultural** donde niños y niñas de la zona tomaban clases de pintura y dibujo los fines de semana.
