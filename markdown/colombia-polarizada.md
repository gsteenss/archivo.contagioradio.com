Title: Colombia polarizada
Date: 2017-04-19 09:31
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: derecha en Colombia, Gobierno Santos, izquierda y derecha, María Fernanda Cabal
Slug: colombia-polarizada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/colombia-polarizada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagioradio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 19 Abr 2017 

[Qué gran problema en el que se ha convertido hablar críticamente de los horrores y errores políticos, sociales, administrativos, modélicos y económicos en los que vive Colombia. Hablar algo en contra del poder corrupto y neoliberal establecido en el país, hace emerger casi de manera automática gritos airados que pregonan: “¡vete a Cuba!” “¡quieres que seamos Venezuela!” “¡la izquierda es un fracaso!” y una cantidad incalculable de improperios típicos de esa mamertería de derecha que vive amparada en las comparaciones subjetivas, para evadir el peso objetivo de una realidad como la nuestra.  ]

[Los portavoces del poder dominante en Colombia, en no pocas ocasiones se presentan como “objetivos” o “neutrales”, inclusive en las universidades algunos señalan como “demasiado ideologizado” a un discurso que no comparta directa o indirectamente los intereses de ese poder. Aquellos “neutrales”, son una derecha no declarada ¿qué significa esto?, bien, algo típico en Colombia es que la izquierda siempre sea identificada, expuesta, nombraba, conceptualizada, pero la derecha en cambio, parece como si no existiera; parece como si luego de la caída de los dictadores europeos o suramericanos, la historia se hubiera “tragado a la derecha” como por arte de magia.]

[¿Quién dijo acaso que la derecha es sólo sinónimo de totalitarismo? ¿Quién dijo que derecha solo significa una dictadura militar nacionalista? Para nada. La derecha también hoy comanda regímenes democráticos por todo el mundo, y para identificarlos, hay que partir de lo más elemental: ya no andan vestidos de militar, ahora usan corbata y un discurso caramelizado con las palabras “libertad” y “democracia” hasta un punto realmente hostigante.]

[No es un secreto que el poder en Colombia lo han tenido “los mismos de siempre”, no obstante, tratar de comprender quienes son esos “mismos de siempre” pasa por algo más allá de sindicarles con un discurso trasnochado que les grita “¡oligarcas!”. En este punto de la historia, la oligarquía no se ha extinguido en Colombia, pero ha hecho nuevos amigos, algunos más liberales que otros ¡¡pero al fin amigos!! con los cuales salvaguardan ese poder. Un poder claramente de derecha.]

[¿Quién o qué representa a la derecha en Colombia? Solo por hablar del presente, la derecha es esa amalgama entre clases tradicionales y emergentes tales como, una oligarquía política (Lleras, Santos), oligarquía industrial (Lule, Santo Domingo), terratenientes (Lafaurie, Uribe), clase industrial de ascenso histórico (Villegas, Sarmiento), clase política en ascenso (Parody, López), y grandes figuras de los medios masivos de información (Arizmendi, Gurisatti), como para ponerle una cereza a semejante cóctel.  ]

[Esa derecha cuyos protagonistas son unos más arrebatados que otros, constituye el poder dominante en todos los escenarios de la sociedad colombiana. Por tanto, hay que aclarar que las razones para que la derecha permanezca dominando la situación, se hallan inmersas en su capacidad para encubrir ese papel dominante. Dicha tarea es lingüística y es simbólica, es decir que está inmersa en la cultura, no es fácil de detectar, pero cuando se logra, provoca la reacción iracunda de todo el sistema mismo a través de cualquiera de sus representantes.  ]

[¿Entonces se podría hablar de una psicología inversa del poder? Parece que sí. A decir verdad, cuando uno escucha a mucha gente decir que es]*[a-política]*[, o cuando se escucha a algunos docentes autoproclamarse como “objetivos”, incluso, aún en el caso más típico, es decir el de representantes de un medio de información masiva que se declaran como emisores de la verdad, lo que ocurre realmente en dichas situaciones es un ocultamiento figurado o intencional de intereses que son protegidos con gran fuerza, intereses del poder dominante.]

[La derecha en Colombia y en gran parte de Latinoamérica, parece más un]*[“no ser conceptual”]*[ ya que sí existe, se encuentra dominando todos los escenarios políticos, pero nadie la llama por su nombre; aclarando que ese “nadie” corresponde a los medios masivos de información que tienen a su vez posiciones editoriales alineadas con dicho poder. Con algo de certeza el ejemplo que valida ese “]*[no ser conceptual”]*[ es el caso de la ultra derecha colombiana, pues ésta, ni siquiera es denominada ni se denomina como derecha, de hecho se hacen llamar “centro” y a parte de todo “centro democrático”.]

[La voz al unísono es que la izquierda es un fracaso, que la izquierda para aquí y que la izquierda más pa’ allá. Todos los medios de derecha, le dedican aunque sea una vez al día una pequeña nota que pone en evidencia su desagrado, o lo que sería mejor llamar, su cuota ideológica y política. ¿Si es tan fracasada por qué se preocupan tanto por atacarla cuando conviene o ignorarla cuando conviene? Es tan increíble el terror que le tienen a la izquierda, que hasta los sectores más progresistas y aparentemente democráticos como por ejemplo Claudia López, elevan la voz insistiendo en que la alianza de izquierdas no le conviene al país… la pregunta subsiguiente y crítica es ¿a qué país? o ¿al país de quién?...]

[En realidad, no hay problema con que exista la izquierda o la derecha, se supone que la política misma, ronda desde hace tiempo en este debate y tal parece que aún no termina. Si bien hoy ya no hay una guerra fría que sustente semejante división, no es un secreto que por una parte, la derecha es esa tecnocracia neoliberal que no gobierna sino que gerencia, y por otra parte, la izquierda es todo lo que se opone a ese modelo imperante, y digo todo, porque tiene muchos matices corroborados por una gran característica crónica: no ponerse de acuerdo.]

[El problema es que de las dos, sólo una esté bien definida y la otra no. Es decir, cualquier crítica, cualquier protesta contra el poder dominante es tildada de izquierda y siempre hay un consenso al respecto. Incluso cuando un Estado socialista está en crisis, todos los defensores del poder dominante se vuelven expertos “economistas políticos” hablan de modelos e incluso de historia, pero cuando un Estado capitalista está en crisis, entonces se abandona el “análisis” y se pasa al moralismo anti-corrupción y queda todo como un simple “escándalo”.  ]

[La derecha existe y aporta a la polarización en Colombia. Que no vengan los grandes gurus del periodismo indignados por lo que pasa en Venezuela, con esos tonos de indignación frente esa situación, o con esos tonos de burla al referirse a dicho modelo, pero tan frescos, tan indolentes a la hora de hablar de los problemas del modelo colombiano.]

[La derecha existe y aporta a la polarización en Colombia. Que no vengan los empresarios a dar conferencias en las universidades si las críticas a sus discursos las van a llamar “palabras ideologizadas” pues es precisamente esa naturalización de la falsa objetividad lo que acarrea toda su etiqueta derechista.]

[La derecha existe y aporta a la polarización en Colombia. Que no se ofendan a la hora que les quitamos la máscara, que no respondan con insultos e improperios, y más bien que estudien, que estudien más que la politóloga Maria Fernanda Cabal o que la filósofa y abogada Paloma Valencia quienes concluyen luego de sus arduos estudios que Juan Manuel Santos es comunista.  Que la derecha no se ofenda por ser derecha, que den la cara por lo que defienden, que hagan como Rangel, que le tocó pasar de “Analista político” a senador por la ultra derecha, que combinen todas las formas de lucha, pero que no se hagan los neutrales.]

[La derecha existe y aporta a la polarización en Colombia … protege los intereses del poder o está en el poder, domina sin la fuerza, pues ese método se ha constituido en su modus operandi, pero ojo… quitémosle la máscara de neutralidad y veamos si eran tigres de papel o enemigos decentes.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
