Title: Asesinan a líder social en Orito, Putumayo
Date: 2018-03-03 09:51
Category: DDHH, Nacional
Tags: Lider social, Orito, Putumayo
Slug: asesinan-a-lider-social-en-orito-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [03 Mar 2018]

**Iver Larraonda, presidente de la Junta de Acción Comunal del bario Los Alpes, en Orito, Putumayo, fue asesinado la noche del 2 de marzo**. De acuerdo con las primeras versiones, el líder social se encontraba en el sector del Parque Lineal, cuando dos sujetos llegaron y lo abordaron, disparandole con un arma de fuego que acabo con su humanidad.

Con el asesinato de Larraonda continúa aumentando **la escabrosa cifra de líderes asesinados en el país que asciende a 25**, en tan solo los tres primeros meses del año. En ese sentido organizaciones como Somos Defensores, en su más reciente informe sobre acciones violentas contra defensores de derechos humanos y líderes sociales, alerto sobre el incremento que se viene registrando desde el año 2016 hasta ahora, en agresiones.

De igual forma, señaló que aún existe un gran reto por parte de las entidades encargadas de investigar a los autores intelectuales de estos crímenes, debido a que, de un total de 253 casos reportados por 4 fuentes de información **29 se ha logrado el esclarecimiento 30 de 99 que corresponden al 39.13%. **(Le puede interesar:["Enero, un mes trágico para líderes y reclamantes de tierra"](https://archivo.contagioradio.com/enero_asesinatos_lideres_sociales/))

Mientras que de los 99 casos que cuentan con esclarecimiento 10 **tienen sentencias**  
**condenatorias en la jurisdicción ordinaria**, 4 en la jurisdicción especial indígena, 44 se encuentran en juicio, 25 en etapa de investigación, 15 en indagación con orden de captura vigente y un caso fue archivado por conducta atípica. (Le puede interesar: ["Líderes sociales: "La piedra en el zapato" de quienes se oponen a la construcción de paz"](https://archivo.contagioradio.com/51836/))

Noticia en desarrollo ...
