Title: Corte Constitucional estancó el derecho a la adopción igualitaria
Date: 2015-02-18 23:04
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Adopción igualitaria, Corte Constitucional, LGBTI
Slug: adopcion-igualitaria-seria-posible-solo-si-padre-o-madre-son-biologicos
Status: published

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4107079_2_1.html?data=lZadmZWbfY6ZmKiakpmJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRktCfwtvO0N%2BJh5SZo5iYw8nTtMTdhqigh6eXsozk0NeY0sbWqcvV1JDRx9GPscrnztSY1crcs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Germán Rincón, abogado] 

<iframe src="http://www.ivoox.com/player_ek_4107071_2_1.html?data=lZadmZWbdY6ZmKiakpmJd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRktCfwtvO0N%2BJh5SZo5iYw8nTtMTdhqigh6eXsozk0NeY0sbWqcvV1JDRx9GPscrnztSY1crcs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Gaviria, Exmagistrado de la Corte Constitucional] 

La tesis del magistrado José Roberto Herrera, nombrado conjuez por la Corte Constitucional, según la cual parejas del mismo sexo podrían adoptar sólo si una de las personas adoptantes es padre o madre biológica, fue aceptada por la Corte Constitucional y finalmente, se le negó la posibilidad a las parejas homosexuales de adoptar. Por otro lado, se quitó la posibilidad de que miles de niños y niñas que están en las manos del ICBF, que encuentren un hogar.

A pesar, de los múltiples estudios internacionales y nacionales, entre los que se destacan los comunicados del ICBF y la Asociación Nacional de Psiquiatría donde se evidenciaba que los menores que se desarrollan en medio de una familia homoparental no tienen ningún tipo de consecuencia psicológica o social, el alto tribunal **resolvió  de manera negativa, la demanda contra el artículo 68 del Código de Infancia y Adolescencia que se refiere a la adopción,** y se basaba en la ponencia del magistrado Jorge Iván Palacio, que proponía la posibilidad de que las parejas del mismo sexo pudieran adoptar.

El fallo, se conoció mediante un tuit de la Corte Constitucional, donde, se conoció que “**Las parejas del mismo sexo solo pueden adoptar cuando la solicitud recaiga en el hijo biológico de su compañero o compañera permanente”**, hasta el momento no se ha conocido los argumentos por los cuales se dijo no a este derecho.

“La Corte nos ha condenado a las parejas homosexuales a tener únicamente hijos biológicos (…) **nos obliga a tener una doble vida, muchos a casarse y ser heterosexual, para poder tener un hijo o hija,** frente a niños y niñas abandonados por personas heterosexuales que les han dado mal trato y nosotros queremos darles nuestro amor, valores, cariño, pero eso para la Corte no es suficiente”, expresó el abogado Germán Rincón Perfetti.

Para el exmagistrado de la Corte Constitucional, Carlos Gaviria, afirma que “la corte no siguió adelante con una línea jurisprudencial promisoria”, en cambio, este fallo de la corte, no significa otra cosa que “**un estancamiento… esperaba que la corte diera un paso hacia adelante, pero no se atrevió**”, dijo el exmagistrado.

Por su parte, el gobierno dice haber aceptado el fallo sobre adopción homosexual, pero pidió que este mismo debate se lleve al Congreso, sin embargo, para el abogado Rincón, esta no es una solución, ya que **el Congreso se han tramitado 11 proyectos de Ley sobre temas de la comunidad LGBTI,**  **y todos han sido archivados “**con la frase ‘Dios los creó Adán y Eva y no Adán e Iván, ni otros en contra’, lo que hace que el fundamentalismo y la contaminación religiosa haga que los congresistas no respetan el Estado social y de derecho", señaló Rincón.

Con esta decisión **la Corte Constitucional podría perder el prestigio que ha ganado como progresista,** debido a que la reciente decisión no significa ningún avance en materia de igualdad de derechos para las personas de la comunidad LGBTI, según explica Germán Rincón.

Sin embargo, la comunidad seguirá luchando por tener garantía de sus derechos, ya que como lo afirma Germán Rincón, “**el matrimonio y la adopción igualitaria son imparables”.** Para el ex magistrado, lo importante es **“pensar en estancias como nuevas tutelas, seguir por la vía jurisprudencial”**, por la cual, cree Gaviria, que habrá más avances derivados de la constitución misma, aprovechando que “la opinión va evolucionando en un sentido correcto”, aseguró el exmagistrado.

[![infografia-lgbti](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/infografia-lgbti.jpg){.aligncenter .size-full .wp-image-5080 width="648" height="734"}](https://archivo.contagioradio.com/actualidad/adopcion-igualitaria-seria-posible-solo-si-padre-o-madre-son-biologicos/attachment/infografia-lgbti/)
