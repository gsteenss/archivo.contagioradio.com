Title: ¿En Colombia se encarcela el pensamiento crítico?
Date: 2018-05-15 14:22
Category: Expreso Libertad
Slug: en-colombia-se-encarcela-el-pensamiento-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Cárcel-1-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Florencianos.com 

###### 11 May 2018 

En este programa del Expreso Libertad conozca el relato de Julian, que se desempeñaba como docente en la Universidad Nacional cuando fue capturado y acusado de rebelión. De acuerdo con él, pese a que siempre ha existido la cárcel como centro de reclusión para quienes piensan diferente a quienes están en el poder, desde el periodo presidencial del actual senador Álvaro Uribe Vélez, ha existido un aumento y persecución más fuerte hacia el pensamiento crítico.

<iframe id="audio_25927401" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25927401_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio
