Title: MinTrabajo no podría haber convocado Tribunal de Arbitramento según la Ley: ACDAC
Date: 2017-10-03 17:44
Category: Movilización, Nacional
Tags: ACDAC, Avianca, Ministerio del Trabajo
Slug: pilotos_avianca_ministerio_trabajo_huelga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/acdac-e1507067887830.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [3 Oct 2017] 

Ante el aval que ha dado la Aeronáutica para la contratación de pilotos, un comunicado de ACDAC señala que esta medida desconoce lo dispuesto en las **Leyes del Código Sustantivo del Trabajo, el Código del Comercio y los Reglamentos Aeronáuticos,** donde se expresa que está prohibido reemplazar a los trabajadores que estén en huelga.

[Frente al tribunal de arbitramento, según la Ley 1210 del 2008, el Ministerio de Trabajo no tendría la potestad para convocar un Tribunal de Arbitramento, la ministra Griselda Restrepo afirmó que si la Asociacion Colombiana de Aviadores Civiles (ACDAC) no nombra un arbitro será definido por sorteo.  Pero además, este martes, se ha puesto en jaque la huelga de los pilotos, pues  la Aeronáutica Civil ha autorizado la constratación de hasta 800 aviadores extranjeros.]

De acuerdo con la sentencia **C-330/12 de la Corte Constitucional, **"Cuando una huelga se prolongue por sesenta (60) días calendario, sin que las partes encuentren fórmula de solución al conflicto que dio origen a la misma, el empleador y los trabajadores durante los tres (3) días hábiles siguientes, podrán convenir cualquier mecanismo de composición", dice la Ley, entre ellos una conciliación o arbitraje para poner término al conflicto laboral. ([Le puede interesar: continúa huelga de pilotos de Avianca por soberbia de directivas de Avianca)](https://archivo.contagioradio.com/continua-huelga-de-pilotos-por-soberbia-de-directivos-de-avianca/)

ACDAC, ha anunciado, que no convocará un arbitro, e incluso interpuso una **acción de tutela contra la resolución del** **Ministerio del Trabajo** ante la Sección Cuarta del Tribunal Contencioso Administrativo de Cundinamarca argumentando que el Tribunal de Arbitramento vulneró el derecho al debido proceso, a la asociación sindical y a la huelga que adelantan los pilotos sindicalizados.

### **ACDAC niega afirmaciones sobre división al interior del sindicato** 

<article class="single-content page-content clearfix">
<div class="news-content debug">

Esta semana la aerolínea ha venido asegurando que algunos pilotos que hacen parte de ACDAC se han retirado de la huelga, señalando que hay divisiones al interior del sindicato, sin embargo, la asociación de aviadores indica todo lo contrario, incluso se afirma que cerca de 70 aviadores han decidido unirse a la huelga, que este martes ya cumple 14 días.

No obstante, en su último comunicado el sindicato de aviadores mantiene su disposición de negociar el pliego de peticiones para encontrar una pronta solución a este conflicto laboral. Además **dicen estar preparándose "para atender la cita este miércoles en el Tribunal Superior de Bogotá** sobre la demanda para la declaración de legalidad de la huelga".

</div>

</article>
<div id="relacionado">

</div>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
