Title: Fémina Intramuros
Date: 2019-09-13 14:08
Author: CtgAdm
Category: yoreporto
Tags: danza, femenina, femenino, territorio
Slug: femina-intramuros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-13-at-09.15.19.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Reseña:

De la casa a la calle, de la calle a la casa, trazadora de puentes, lo femenino y sus múltiples facetas es energía que sostiene el mundo.

Reconocerte es encontrar tu danza, el movimiento propio, único e irrepetible, es encontrar el amor en el asfalto, la furia contenida, potencia creativa, agresividad animal, fuerza pura... Reconocerte tras habitar y caminar los barrios apilados en un pedacito de montaña es encontrar una vista que solo se tiene aquí, en esta calle, en este barrio, en este país, en este territorio.

Fémina Intramuros es una propuesta de Video-Danza que narra en tres momentos o escenas la historia de vida en la danza de Breyi una bailarina de break-dance contextualizada en el barrio Las Cruces, Bogotá, Colombia.

Descubran Fémina intramuros, el jueves 19 de septiembre en el Teatro Luis Enrique Osorio, de los sótanos de la Av. Jiménez, a las 7:00 p.m. y el 25 de septiembre en la Facultad de Artes ASAB salón 307, a las 5:00 p.m. Entrada libre a las dos funciones hasta completar aforo.

Direccion y Coreografia: Indira Sofía Molina Cáliz

 
