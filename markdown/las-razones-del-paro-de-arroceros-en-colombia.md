Title: Las razones del paro de arroceros en Colombia
Date: 2017-08-29 12:34
Category: Movilización, Nacional
Tags: Importaciones, Paro de arroceros
Slug: las-razones-del-paro-de-arroceros-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/la-voz-del-ci.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [29 Ago 2017] 

A partir de hoy cerca de 150 mil  pequeños arroceros del país se encuentran en paro debido a los incumplimientos por parte del gobierno nacional en la expedición de resoluciones y medidas que protejan las cosechas nacionales, **afectadas por las importaciones que realiza Colombia en el marco del TLC y el monopolio de los molinos para procesar el arroz**.

### **El arroz de Colombia es suficiente para abastecernos** 

De acuerdo con Eudoro Álvarez, presidente de Asometa, una de las asociaciones de arroceros del Meta, la crisis que afronta el arroz no se había registrado desde hace 40 años y es **producto principalmente de los acuerdos de importaciones que se han hecho para que este cereal entre al país**, pese a una producción nacional que la abastece.

Álvarez señaló que actualmente los arroceros están perdiendo por cosecha un millón de pesos, y aunque el gobierno se había comprometido a generar medidas que redujeran los impactos de las exportaciones esto no ha sucedido “en este momento pagan \$50.000 pesos por bulto de arroz, una buena producción son 80 bultos, esto da \$4.000.000 de pesos y **los campesinos invierten \$5.000.000 millones, se pierde uno**”. (Le puede interesar: ["Minería en Colombia ocupa 5 millones de hectáreas y la agricultura solo 4 millones"](https://archivo.contagioradio.com/mineria-en-colombia-ocupa-5-millones-de-hectareas-y-la-agricultura-solo-4-millones/))

Sin embargo, esta situación ya había sido advertida por el gremio de arroceros, quienes le manifestaban al Ministerio de Agricultura que la oferta del 2016 y del 2017 ya estaba abastecida por las cosechas nacionales, y si se realizaban importaciones habría sobre oferta, provocando que el precio del arroz bajara. No obstante, el ministro de Agricultura aprobó la importación de **64 mil toneladas de arroz, como parte del tratado de libre comercio con Estados Unidos.**

### **Los compromisos incumplidos del gobierno con los arroceros** 

Según el lider, el gobierno se comprometió con los arroceros a generar una compensación en las cosechas de junio y posteriormente compensaría las de julio y agosto, a través de una resolución para subsidiar parte de las pérdidas. Sin embargo “**el ministró dejo sin proteger los arroces recogidos en junio, julio y agosto,** solamente dice la resolución que se cubrirá con el auxilio los arroces que se recolecten a partir de la fecha de la publicación de la resolución” es decir los 5 días que quedan de agosto en adelante.

Este incumplimiento podría llevarlos a la quiebra porque es entre junio y agosto cuando se recolectó el 60% de la cosecha. Además, para el próximo 15 de octubre se realizaría otra importación del cereal, “esto es gravísimo porque el campo es uno de los habilitadores más importantes que hay el país, que va a hacer la gente que vive del arroz” afirmó Álvarez y agregó que serían **210 municipios los que viven de este cultivo y más de dos millones de personas las que dependen de él**. (Le puede interesar: ["¿Qué le ha dejado a Colombia estos 4 años de TLC con EE.UU?")](https://archivo.contagioradio.com/que-le-ha-dejado-a-colombia-4-anos-de-tlc-con-estados-unidos/)

### **El monopolio de los Molinos de arroz en Colombia** 

De acuerdo con Eudoro, actualmente 3 molinos tienen el 85% del mercado de molino de arroz, por tal motivo pueden intermediar en el precio del arroz “mediante el mecanismo de manejar la distribución, con empaquetado y disponibilidad inmediata para el consumidor, hace que ellos tengan una posición dominante” y explicó que con el precio en el que los molinos pagan el arroz, **la libra podría estar a menos de \$800 pesos, mientras que las de marca están por encima de \$3.500 pesos.**

### **Las importaciones de arroz que llegan a Colombia aportan a la quiebra del gremio** 

Álvarez manifestó que el tercer factor que está llevando a la crisis a los arroceros son las empresas que realizan las importaciones. A través de un sistema de subastas los arroceros de Estados Unidos, ganan la entrada en el mercado y venden directamente a los almacenes de cadena, que a su vez contratan al **15% de molinos que no pueden competir con los más grandes, para que procesen el arroz.**

Así se teje un negocio en que se mantiene el "oligopolio" de los molinos, se favorecen los almacenes que venden el arroz con marca propia a **precios más bajos y a costa de la quiebra de los pequeños** y medianos arroceros que, buscando un mejor precio a sus cosechas, venden a esos pequeños molineros.

“El gobierno debe poner orden y decir no importemos más, yo no entiendo si usted tiene la alacena llena usted tiene que comprarle al vecino” aseveró Álvarez y agregó que “este es un gobierno que gobierna para quién, **para lo arroceros norteamericanos o para los arroceros y consumidores colombianos**”.

### **El paro de los arroceros** 

Frente a esta problemática, arroceros del Tolima, Meta y Casanare expresaron que paralizarán sus actividades durante la jornada de hoy, y estarán a la espera de establecer una **mesa de conversaciones para definir nuevas medidas y defender la producción nacional**.

“Sí hay arroz es porque los agricultores organizados hemos peleado, **se acabó el algodón, se acabó el sorgo, el maíz está moribundo, hemos visto la crisis en otros cultivos**, hemos sido persistentes en reclamar el derecho que tenemos a producir el arroz para los colombianos” afirmó Álvarez.

<iframe id="audio_20583888" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20583888_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
