Title: CONPAZ exige voluntad política del gobierno para implementación de Acuerdos
Date: 2016-11-24 13:30
Category: DDHH, Nacional
Tags: #AcuerdosYA, Conpaz
Slug: conpaz-exige-voluntad-politica-para-implementacion-de-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_202-e1495706645670.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Nov 2016] 

En un comunicado de prensa comunidades y víctimas que conforman CONPAZ, expresaron al gobierno del presidente Santos y a las FARC-EP su alegría y compromiso con el nuevo acuerdo de paz, firmado el 24 de noviembre en Bogotá. Sin embargo hicieron un **llamado a diferentes sectores del gobierno para que se comprometan a implementar los acuerdos.**

En la misiva, las comunidades argumentan que si bien reciben estos nuevos acuerdos con esperanza, consideran que el anunció se ve empañado por **“la continuidad de operaciones neoparamilitares con intimidaciones, amenazas, atentados y asesinatos a miembros de la organización Marcha Patriótica** y casos anteriores a miembros de Congreso de los Pueblos”.

Actos que se han dado en los 54 días después del plebiscito por la paz y que han evidenciado la urgencia por la implementación de los acuerdos de paz. Además CONPAZ señala que avanzar no será posible si **“quienes ostentan el poder institucional, los partidos y empresarios no cumplen su palabra sobre lo ya acordado”** y si sectores sociales continúan aferrándose a la idea de imponer su forma de pensar y actuar sobre los demás. Le puede interesar:["Pedimos participación, voz y voto en Pacto por la paz: CONPAZ"](https://archivo.contagioradio.com/pedimos-participacion-directa-voz-y-voto-en-el-nuevo-pacto-social-conpaz/)

Reiteraron que como comunidades, personas y víctimas que ha sufrido el conflicto armado, no quieren que se repitan estos hechos en sus territorios  y esperan en particular esfuerzos de quienes han **“instado, planificado y beneficiado la violencia”** en el país. De igual manera CONPAZ reafirmó su voluntad por construir y aportar a la paz desde sus apuestas socioambientales, económicas y de género. Le puede interesar:["Con firmaton popular sociedad refrendará nuevo acuerdo de paz"](https://archivo.contagioradio.com/firmaton-nuevo-acuerdo-paz/)

[Comunicado Público Noviembre CONPAZ](https://www.scribd.com/document/332297726/Comunicado-Publico-Noviembre-CONPAZ#from_embed "View Comunicado Público Noviembre CONPAZ on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_97756" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/332297726/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ee0u4Y1aUL68oKqIOdj8&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
