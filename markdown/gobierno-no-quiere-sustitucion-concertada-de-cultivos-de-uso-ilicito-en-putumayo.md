Title: Gobierno no quiere sustitución concertada de cultivos de uso ilícito en Putumayo
Date: 2016-08-03 13:59
Category: Comunidad, Nacional
Tags: coca, Cultivos de uso ilícito, Gobierno Nacional, zonas de reserva campesina
Slug: gobierno-no-quiere-sustitucion-concertada-de-cultivos-de-uso-ilicito-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/cultivos_de_coca_-e1470250623934.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

###### [3 Ago 2016] 

Las comunidades habitantes de la zona de reserva campesina del Putumayo rechazan el **desconocimiento del gobierno a la propuesta de sustitución manual, gradual y concertada de cultivos de uso ilícito que ha sido construida por las comunidades campesinas**, y que han venido negociando desde hace más de dos años con el gobierno del presidente Juan Manuel Santos.

De acuerdo con Sandra Lagos, integrante de la Asociación de Desarrollo Integral Sostenible Perla Amazónica,  ADISPA, lo que está haciendo el gobierno es evadir su responsabilidad, queriendo que sean los alcaldes los encargados de enfrentar el tema de la sustitución de cultivos de uso ilícito, **en cambio si se reactivaron las erradicaciones manuales y  las fumigaciones pese a que cientos de familias viven del cultivo de la hoja de coca.**

**“Es indignante que después de dos años de diálogo salgan con esa respuesta”**, expresa Lagos, quien explica que el problema radica en que si son los alcaldes los responsables probablemente van existir trabas para la financiación de los programas de sustitución, así mismo, no habría un plan de contingencia que mitigue el impacto que genera la erradicación.

Sumado a lo anterior, la integrante de ADISPA, asegura que en la Zona de Reserva Campesina Perla Amazónica, llevan 16 años trabajando en esta propuesta de sustitución  y ni las alcaldías ni gobernaciones han querido reconocer el plan de desarrollo sostenible de las comunidades de la zona.

Frente a esta situación y debido al inicio de las fumigaciones y erradicaciones manuales en diferentes puntos del departamento de Putumayo las comunidades se movilizan desde la semana pasada exigiendo que junto al gobierno se continúe se continúe en la construcción de un acuerdo que reivindicativo para las familias, sin embargo, Lagos expresa que **“El gobierno está muy lejos de dar una respuesta positiva a las comunidades”.**

<iframe src="http://co.ivoox.com/es/player_ej_12430309_2_1.html?data=kpehlZWXdJqhhpywj5WbaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncbTVz8nfw5Cwpcjj1IqfpZCliKrHsaaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
