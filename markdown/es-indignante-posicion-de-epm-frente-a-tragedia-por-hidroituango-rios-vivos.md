Title: "Es indignante posición de EPM frente a tragedia por Hidroituango": Ríos Vivos
Date: 2018-05-03 14:45
Category: Ambiente, Nacional
Tags: afectados por hidroituango, Antioquia, EPM, Hidroituango, Movimiento Ríos Vivos
Slug: es-indignante-posicion-de-epm-frente-a-tragedia-por-hidroituango-rios-vivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DcMNMHwWkAECdSq-e1525375369701.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [03 May 2018] 

Continúa la grave situación para las comunidades que habitan en el bajo Cauca Antioqueño producto del represamiento en el túnel de **desviación de las aguas del río Cauca** que hace parte del proyecto hidroeléctrico Hidroituango. Pese a que Empresas Públicas de Medellín argumentó haber controlado la emergencia, las comunidades están viviendo una situación de angustia debido a que puede ocurrir una avalancha que dejaría a un gran número de personas damnificadas.

### **Obstrucción del túnel no ha sido controlada** 

De acuerdo con el Movimiento Ríos Vivos, “la obstrucción se dió por la gran cantidad de material vegetal talado que **no fue recolectado por EPM** y fue arrastrado por el río". A se suma los reiterados desprendimientos de tierra de las laderas del Cañón y del interior de los túneles. (Le puede interesar:["Asesinan a Hugo Albeiro George líder social y afectado por Hidroituango"](https://archivo.contagioradio.com/asesinan-hugo-albeiro-george-lider-social-afectado-hidroituango/))

Esta situación ha hecho que crezca el temor de las comunidades que denunciaron explosiones que habrían sido ocasionadas por la empresa “como medio para superar la situación o para realizar un **llenado prioritario de la presa** con los riesgos que esto implica”. De igual forma, afirmaron que no hay ninguna medida para proteger a la población que esta solicitando una reubicación urgente a EPM.

En repetidas ocasiones, el Movimiento ha denunciado que el territorio donde se realiza el megaproyecto **tiene fallas geológicas**. Isabel Cristina Zuleta, integrante de Ríos Vivos, informó que los estudios para Hidroituango “no han sido rigurosos” y la organización en defensa del ambiente ya alertó sobre el riesgo del desprendimiento de las laderas.

Zuleta recordó que “hay una falta de claridad por parte de EPM que **no dice toda la verdad** o que dice verdades a medias”. Afirmó que, las comunidades han desarrollando diferentes movilizaciones en el territorio teniendo en cuenta esta problemática  “porque no saben si se va a destaponar el túnel arrasando con la gente”.

### **“No es cierto que EPM tenga todo bajo control”** 

Adicional a esta problemática, desde Riós Vivos consideran que “es indignante que EPM siga diciendo que tiene bajo control la situación”, teniendo en cuenta que la empresa [**ha negado que haya comunidades**] en las riberas del río aumentando el riesgo para las personas que se encuentran allí.

Zuleta se refirió a los postulados de la Organización Mundial de la Salud que manifiesta que “los principales impactos que tiene una mega obra son los psicosociales”. Estas afectaciones no han sido tenidas en cuenta por la empresa y, según denuncian, las personas “están desesperadas porque puede suceder una tragedia”. (Le puede interesar:["15 familias afectadas dejó la inundación producto del taponamiento de un túnel para Hidroituango"](https://archivo.contagioradio.com/15-familias-afectadas-dejo-la-inundacion-producto-del-taponamiento-de-un-tunel-para-hidroituango/))

Finalmente, recordó que la empresa debía haber tenido en cuenta que "en el Cañón **se derrumban las montañas** y las laderas con mucha frecuencia”. Enfatizó en que EPM ha dicho que va a realizar un llenado prioritario que consideran "absurdo",  teniendo en cuenta que para hacer esto tienen que llenar toda la obra y "no le han avisado a la gente que lo van a hacer”.

###### Reciba toda la información de Contagio Radio en [[su correo]
