Title: "Si me preguntan hoy qué quiero, quiero verdad, pido verdad" Jineth Bedoya ante CIDH
Date: 2016-04-05 17:34
Category: DDHH, Nacional
Tags: CIDH, Jineth, violencia sexual
Slug: agentes-estatales-deben-ser-sancionados-por-crimen-contra-jineth-bedoya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/JINETH-BEDOYA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH  ] 

###### [5 Abril 2016 ]

El proceso que cursa por el **secuestro, la tortura física y psicológica y la violación** de la que fue víctima la periodista Jineth Bedoya, en mayo del año 2000 por cuenta de alias 'El Panadero' y dos paramilitares más, en articulación con **agentes de las Fuerzas Militares, La Policía y el INPEC**, fue llevado ante la Corte Interamericana de Derechos Humanos.

De acuerdo con Bedoya, el Estado colombiano debe hacerse responsable de las omisiones que se han presentado en su caso y que han llevado a la **pérdida de pruebas, información y expedientes determinantes, que ha impedido el esclarecimiento de la verdad**, la sanción de quienes planearon y ejecutaron este crimen en su contra, y las garantías de no repetición, teniendo en cuenta las amenazas que ha recibido recientemente.

Los últimos 17 años de su vida, Jineth los ha dedicado a la búsqueda de justicia y verdad, ante la vulneración de sus derechos por cuenta de la investigación que adelantaba sobre una red de corrupción en 1999, sucesos de intimidación y amenazas frente a los que solicitó **medidas de protección que fueron otorgadas semanas después de que ella alertara a las autoridades del peligro que corría**.

Según afirmó ante la CIDH la impunidad en los casos de violencia sexual contra mujeres es del 97% y aunque en 2008 la Corte Constitucional pidió a la Fiscalía priorizar 183 casos, en los que se incluye el suyo, el Estado colombiano sólo ha respondido con dos condenas a bajos mandos en la comisión del delito en su contra y con **un llamado el pasado 7 de enero para que "renegociara" su caso**, por lo que pide a la CIDH presionar avances jurídicos que lleven al esclarecimiento de la verdad y la sanción de los agentes estatales que participaron.

Escuche la sesión completa:  
https://www.youtube.com/watch?v=fkSZFzqLtP8

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

 
