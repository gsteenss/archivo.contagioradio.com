Title: CIDH condena al Estado en el caso de Gustavo Petro
Date: 2020-08-18 18:06
Author: CtgAdm
Category: Actualidad, Comunidad, Política
Tags: alcaldia de bogota, CIDH, Contraloria, Gustavo Petro, procuraduria
Slug: cidh-condena-al-estado-por-la-destitucion-en-la-alcaldia-de-gustavo-petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/gustavo-petro-colombia-rtr-img.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","style":{"typography":{"fontSize":14}}} -->

*Foto: El sonajero*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La [Corte Interamericana](http://www.oas.org/es/cidh/)de Derechos Humanos**, CIDH, **falló a favor del actual senador Gustavo Petro, haciendo responsable al Estado colombiano por las violaciones a sus derechos políticos**, en el marco de la acción de su destitución como Alcalde Mayor de Bogotá e inhabilitación por el término de 15 años para ocupar cargos públicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la Sentencia, la CIDH asegura que la decisión tomada por la Procuraduría General de la Nación, el 9 de abril del 2013, durante el periodo de Alejandro Ordoñez; **viola la convención Interamericana: irrespetando el debido proceso y el principio de presunción de inocencia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente al hecho, el abogado, Daniel Prado, representante de Gustavo Petro aseguró que esto implica que, *"el Estado colombiano debe [adecuar la legislación interna](https://archivo.contagioradio.com/la-cadena-perpetua-desde-la-optica-constitucional/)para que **no se puedan destituir funcionarios de elección popular por vía administrativa, sólo por una sentencia de carácter penal emitida por un juez de la República**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente la CIDH ordenó, primero *"publicar el resumen oficial de la sentencia una sola vez, en el Diario Oficial y en otro diario de circulación nacional"*, es decir, una publicación en el **portal web de la Procuraduría General y otra en uno de los periódicos de circulación nacional**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Segundo, publicar la sentencia en su integridad en el sitio web oficial de la Procuraduría General de la Nació, y junto a esto en una acción de garantías de no repetición, manifestando que se debe, *"adecuar su ordenamiento interno de acuerdo a lo señalado en la Sentencia, d**e forma tal que los funcionarios de elección popular no puedan ser destituidos ni inhabilitados por decisiones de autoridades administrativas**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También dispuso que se debe hacer entrega de una indemnización Compensatoria, en la que se debe, *"pagar las sumas monetarias fijadas en la Sentencia por los conceptos relativos al daño inmaterial, y el reintegro de gastos y costos"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acto en el que se reconocen **10 mil dólares de reparación y más 18 mil dólares de gastos del proceso**, mientras que los abogados del senador pedían una compensación de 40.000 dólares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta orden proferida por los magistrados Eugenio Raúl Zaffaroni, Eduardo Ferrer Mc Gregor y Pablo Saavedra, no solo modifica la orden dada en el 2013 por el **actualmente embajador de Colombia ante la OEA**, Alejandro Ordoñez, también pide modificar funciones de la Procuraduría al impedir que esta entidad junto con la Contraloría, puedan retirar a un funcionario elegido popularmente de su cargo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/petrogustavo/status/1295848343378288640","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/petrogustavo/status/1295848343378288640

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
