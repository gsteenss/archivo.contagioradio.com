Title: Mujeres seguirán luchando en Colombia por la paridad democrática
Date: 2018-12-14 15:58
Author: AdminContagio
Category: Entrevistas, Mujer
Tags: Beatriz Quintero, Congreso, Democriacia, mujeres, paridad
Slug: mujeres-seguiran-luchando-en-colombia-por-la-paridad-democratica
Status: published

###### [Foto: Contagio Radio ] 

###### [14 Dic 2018] 

Se hundió en la Cámara de Representantes el artículo de la Reforma Política que buscaba poner en marcha la paridad en los partidos políticos. Hecho que para Beatríz Quintero, coordinadora de la Red Nacional de Mujeres**, es una deuda histórica con las mujeres,** porque conforman más de la mitad de la sociedad colombiana y en ese sentido deberían tener una mayo representación electoral.

Sin bien es cierto que desde el 2015 la Corte Constitucional ordenó que la paridad fuese una realidad, este debate lo que buscaba era reglamentar a partir de cuándo entraría en vigencia, lo que convierte esta norma en "papel" y según Quintero, demuestra que "no hay un compromiso por parte de los congresistas" con la democracia.

**Actualmente solo el 20% de las curules en el Congreso son ocupadas por mujeres,** situación que para la coordinadora de la Red Nacional de Mujeres "no ha cambiado mucho en los años anteriores", mientras que en la última elección disminuyo la participación de las mujeres.

### **¡Las mujeres seguiremos exigiendo paridad!**

Quintero señaló que, aunque este año no se pudo reglamentar la paridad, seguirán insistiendo en que esta norma se aplique a partir del año entrante y exista un verdadero reflejo de la sociedad en el Congreso de la República. (Le puede interesar: ["Mujeres logran creación del primer subcomite de género institucional en Medellín"](https://archivo.contagioradio.com/las-mujeres-en-colombia-ganan-una-batalla-contra-el-estado/))

"La sociedad colombiana esta conformada por hombres y mujeres, entonces necesitamos que en el Congreso hayan hombres y mujeres, en la misma proporciones que está representada la sociedad y **apróximadamente somos 50 y 50, esa es la principal razón de esta democracia paritaria"** afirmó Quintero.

Sumada a la paridad, las organizaciones defensoras de los derechos de las mujeres también estaban reclamando una alternancia en las candidaturas, es decir que se intercale mujer y hombre en un sistema cremallera, y las listas cerradas.

<iframe id="audio_30797564" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30797564_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
