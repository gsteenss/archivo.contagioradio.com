Title: "Tejeremos la bancada de oposición" Ángela María Robledo
Date: 2018-06-19 16:59
Category: Nacional, Política
Tags: Angela María Roble, Colombia Humana, elecciones 2018, Gustavo Petro, Sergio Fajardo
Slug: tejeremos-la-bancada-de-oposicion-angela-maria-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-06-19-at-4.47.31-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [19 Jun 2018] 

Contagio Radio conversó con la ex candidata a la **vicepresidencia de Colombia, Ángela María Robledo**, luego de haber obtenido más de 8 millones de votos en la segunda vuelta, junto con su formula de campaña Gustavo Petro. Robledo afirmó que una vez llegue a la Cámara de Representantes, continuará con su labor en defensa de los derechos humanos y el ambiente, y aseguró que trabajará para que se logre conformar la bancada de oposición.

**Contagio Radio: ¿Cómo fue este periodo de campaña?**

**Ángela María Robledo:** Fue una campaña muy bella, llena de ilusión, de esperanza, sin un episodio de violencia, y vemos que asoma una condición de lo que es el saldo del proceso de paz que hay que proteger, junto con la mesa del ELN. Ahora, de nuevo desde el Congreso de la República, **estaremos allí resguardándolo porque no queremos y no vamos a permitir volver a la guerra**.

**CR:** Fueron 8 millones de votos que se dieron a una candidatura diferente, que no pertenece a las castas políticas y que representa a muchas personas ¿Cómo se siente con el resultado final del 17 de junio?

**AR:** Hay que celebrar, esta es una votación histórica, son 8 millones de votos de personas que expresaron su desacuerdo con lo que se viene haciendo en Colombia, expresaron que la vida no está bien para millones y millones de colombianos y que hay que cambiar el rumbo. ([Le puede interesar: "Duque no desconozca el camino ya andado": FARC](https://archivo.contagioradio.com/duque-no-desconozca-el-camino-ya-andado-farc/))

Ya empezamos a escribir esta nueva historia con los 10 millones de votos del 27 de mayo y los 8 del pasado domingo. Ya estamos programando una reunión para este jueves con un grupo de colegas del partido Verde, del Polo, **con líderes que han dicho que nos acompañaran en esta oposición deliberativa**.

**CR:** ¿Se podría tejer una alianza con Sergio Fajardo y la ciudadanía que lo apoyó en la primera vuelta?

**AR:** El liderazgo de Sergio Fajardo en la coalición Colombia queda bastante herido, me parece que fue una persona que no estuvo a la altura del compromiso con el que hizo campaña y les incumplió a sus votantes. 4 millones 600 mil personas votaron con la esperanza de la paz, de la educación y de la consulta anticorrupción que ha sido una iniciativa importante en especial para los verdes. (Le puede interesar: ["Los retos de Gustavo Petro en el Congreso de la República"](https://archivo.contagioradio.com/los-retos-de-petro-y-angela-robledo-en-el-congreso-de-la-republica/))

Salir a decir me voy a ver ballenas y después, salir a decir vote y ahora me voy a ver el partido de México, me parece una actitud irresponsable. No me extraña, Sergio es eso, un hombre con una enorme incapacidad de asumir grandes responsabilidades a la hora de la verdad. Con Sergio lo veo muy difícil, **siempre miró con desprecio la propuesta de una alianza y ahora tendrá que ocuparse de responder por sus actuaciones en Hidroituango.**

Del otro lado, estamos esperando que Jorge Robledo, que siempre ha sido un senador importante en Colombia con sus denuncias contundentes frente a cómo funciona la salud, la educación, pueda estar de este lado y sumándose **a esta consolidación de una bancada de oposición, no ciega, no guerrera, sino deliberativa**.

**CR:** ¿En qué creen que estuvo el error que evito que se ganará la presidencia?

**AR:** Creo que no es tanto error, claro que habrá muchas cosas para mejorar y aprender, hacer una evaluación a manera de saldos pedagógicos como aprendimos con Antanas. Dificultades se dieron para conseguir en la segunda vuelta los recursos a tiempo, hubo enormes dificultades para conseguir el crédito que generaron incertidumbre, pero es que estamos enfrentando a un establecimiento que por siglos se ha defendido a sangre y fuego, son partidos que han estimado los recursos del Estado, que sacan leyes para beneficiar a unos cuantos en el Congreso.

Los grandes medios de información tampoco son fáciles de enfrentar, en nuestras entrevistas se desnudó intereses atados a los grandes poderes económicos. Esta fue una campaña en resistencia civil, llena de imaginación y alegría, pero llena de dificultades. **Es casi la primera vez que llega un candidato de la izquierda, que nombro los problemas con todas sus letras y responsables y no lo mataron, eso hay que celebrarlo**.

<iframe id="audio_26623103" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26623103_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
