Title: Si los transgénicos no son "peligrosos", ¿por qué prohibirlos"
Date: 2020-08-06 11:53
Author: CtgAdm
Category: Ambiente, Voces de la Tierra
Tags: transgénico
Slug: si-los-transgenicos-no-son-peligrosos-por-que-prohibirlos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/wsi-imageoptim-Maiz-maiz-222-640x426-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Archivo Grain*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Qué son y cómo se fabrican los transgénicos? ¿Hay razones para prohibirlos? ¿Qué impactos han tenido en aquellos países en los que se han cultivado masivamente?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un proyecto de ley que hace curso en el Congreso de la República busca la prohibición del uso de[semillas transgénicas](https://archivo.contagioradio.com/la-crisis-no-viene-de-la-tierra-sino-del-modelo-de-habitarla/)en Colombia. ¿Cuál es su alcance e importancia?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el año 2003, la corporación Syngenta publicó un aviso publicitando sus servicios en los suplementos rurales de los diarios argentinos Clarín y La Nación, bautizando con el nombre de "República Unida de la Soja" a los territorios del Cono Sur en los que se siembra soja - integrados por Brasil, Argentina, Uruguay, Paraguay y Bolivia. ¿Por qué los críticos de este "neocolonialismo" hablan de un mapa de la república tóxica de la soja? ¿Cómo logró Ecuador decir "no" a los transgénicos en 2008?.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un análisis de los expertos y experta :

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Eizabeth Bravo**. Coordinadora de la Red por una América Latina Libre de Transgénicos. Bióloga y doctora en Ecología de microorganismos. Integrante de Acción Ecológica. **Ecuador**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Carlos Alberto Vicente.** Integrante de[Grain](https://www.grain.org/es/article/entries/979-el-negocio-de-los-cultivos-transgenicos-en-america-latina) y Alianza por la Biodiversidad. **Argentina**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Germán Vélez.** Ingeniero agronomo. Director de la revista Semillas y el [Grupo Semillas](https://www.semillas.org.co/es/los-alimentos-transgnicos-en-colombia#:~:text=En%20a%C3%B1os%20recientes%20a%20partir,animales%20y%20microorganismos%2C%20originando%20as%C3%AD). **Colombia**

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F3648194681881030%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
