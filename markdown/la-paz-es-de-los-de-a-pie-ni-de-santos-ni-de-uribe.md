Title: La paz es de los de a pie, ni de Santos, ni de Uribe
Date: 2016-10-10 10:23
Category: Camilo, Opinion
Tags: acuerdo de paz, Juan Manuel Santos, plebiscito por la paz
Slug: la-paz-es-de-los-de-a-pie-ni-de-santos-ni-de-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@CamilodCasas](https://twitter.com/CamilodCasas)** 

###### 10 Oct 2016 

Así es la realidad. 62 % de los colombianos se abstuvieron de votar hace 8 días en el plebiscito de la paz. Es más o menos lo normal en las democracias occidentales, el reflejo de la banalización de lo político o mejor de la administración de lo público para lo privado.

La crisis de la democracia es la de sus ciudadanos, ahora concebidos como mercaderes o como mercancía o consumidores. Más que preocuparse hay que ocuparse de ellos. ¿Por qué nadie de la vida política les atrae? ¿Por qué no votan? ¿Por qué a esa masa nadie les representa?. Ni siquiera el valor de las libertades o de la represión, ni el Sí ni el No?

Ganaron los abstencionistas, y ¿quiénes son ellos? ¿Los cómplices o los indiferentes? ¿Las masas infantilizadas o los sobreviviente del día a día? ¿Quiénes son?, urge reconocerlos ¿Ni de izquierda, ni de derecha, ni de centro?.

Los del No aparentemente ganaron, como también hubieran podido ganar los del Sí, por una pequeña diferencia numérica. El hecho de la perdida del Sí generó un cambió en el ambiente político, esperanzado en implementar un Acuerdo entre el gobierno y la guerrilla en transición a la vida civil de las FARC EP, no es de la izquierda, no es el movimiento social rural, no son las víctimas, son otros, los ciudadanos que rompen espontáneamente el logro del No.

Fue sorpresivo, incluso para Uribe. Si bien la salida depende del juego de Santos en su pragmatismo, es a la sociedad poder consciente a la que le corresponde intentar lograr no solamente evitar un Pacto excluyente para reversar lo logrado en La Habana sino avanzar en la modernidad y en la mesa con Lo firmado en La Habana trata de intentar la constitución de un país moderno, de libertades religiosas, de respeto a las identidades sexuales, a los derechos de la mujer, y por supuesto de una reforma agraria imposibilitada desde la independencia.

El asunto entonces es más que la guerra militar y el intento por resolver una de sus causas. Es más allá de los derechos de las víctimas sustancialmente protegidos en el Sistema Integral de Verdad, Justicia y Reparación y garantías de no repetición es de intentar llegar a profundizar la Constitución del 91. Hoy el país pre moderno, maniqueo, así sea constituido sobre estratagemas de mentira y engaño como la impunidad, se identifica en un caudillo que habla de Dios, que discrimina a las etnias, que nutre de odios a las nuevas expresiones afectivas y las nuevas formas de familia.Es un ciudadano nutrido en siglos de historia de la colonia, del catolicismo y todas las derivaciones cristianas fundamentalistas que propiciaron el miedo al comunismo, al liberalismo, que se identifica con la negación a las realidades sociológicas existentes como las separaciones de las parejas, como la existencia de amores más allá de la heterosexualidad o de un sentido de justicia distinto a la venganza y de la necesidad de básicas reformas para que la miseria y la exclusión no sean el sino de nuestra nación.

Ese miedo introyectado en el subconciente colectivo también lo uso el establecimento del Sí. Es el Santos afirmando contra toda evidencia que si se votaba por el No, al otro día la guerrilla empezaría la toma de las ciudades o el César Gaviria retando absurdamente a Uribe al estilo de las peleas colegiales, que hubieran podido terminar en le "pego en la cara marica". Al final nadie comprende la diferencia entre uno y otro hablando de la paz

¡Ese miedo aterra!. Y y en un país conservador se prefiere la seguridad a las libertades; seguridades religiosas para que todo se conserve; seguridades en mentiras para que las falsas verdades se mantengan; seguridades armadas para que la putrefacción institucional no salga a flote; seguridades que el propio Santos comparte. Hay miedo a la verdad asegurarse que ni Barco, ni Gaviria, ni Samper, ni Pastrana, ni Uribe, ni Santos tuvieron algo que ver con el paramilitarismo. Porque no hay uno que éste exento. Ni uno solo puede hablar de ese ciudadano nutrido en siglos de historia de la colonia, del catolicismo y todas las derivaciones cristianas fundamentalistas que propician el miedo al comunismo, al liberalismo, a los homosexuales, a las mujeres.

Ese ciudadano del No, se identifica con la negación a las realidades sociológicas existentes como el patriarcalismo; que el amor no es eterno o la existencia de amores más allá de la heterosexualidad o de un sentido de justicia distinto a la venganza carcelaria o que solo vive del complejo de pirámide negando la necesidad de  reformas para que la miseria y la exclusión se reduzcan.

Ese miedo introyectado en el subconsciente colectivo también ha pasado en voceros del establecimiento del Sí, usando sus mismos métodos incentivaron el miedo. Desde Santos afirmando contra toda evidencia que si se votaba con el No, al otro día la guerrilla empezaría la toma de las ciudades o César Gaviria retando absurdamente a Uribe al estilo de las peleas colegiales, que hubieran podido terminar en le "pego en la cara marica" y razón tiene, al final nadie comprende la diferencia entre uno y otro. Hablar del Sí con el miedo a la guerra urbana, cómo si ni existiera y hablar del No cómo miedo a un irreal Castro Chavismo. Miedo aquí y allá.

Es como la acusación fácil para hacer ver a  las FARC EP y al ELN como narcoterroristas por su participación en la cadena productiva del tráfico de drogas en uno de las fases más bajas de la gran cadena de las economías ilícitas. Imagen de miedo para ocultar los nefastos vínculos con la fuerza pública, los políticos y empresarios con la droga y su circulación y consumo, para beneficios de diverso tipo, entre ellos el desarrollo del paramilitarismo.

Así que en medio de tanta hipocresía, de tan simulada democracia, de tanta mentira empolvada, lo que sigue es creatividad; silencio creativo como las marchas, palabras visuales como se divulgan en las redes rompiendo el inveterado maniqueismo. Creatividad que identifica al pueblo consumo para ir siendo pueblo sensiblemente crítico.  
Bienvenidos los abstencionistas, bienvenidos los del No, bienvenidos los del Sí a un nuevo mecanismo refrendatorio del proceso hacia la paz, que no será el  neo Frente Nacional o un congreso a sus anchas, pues estamos creciendo,   se está deshaciendo  los clisés de la clase política que nos ha arropado con la mentira para tenernos felizmente dominados.

Mas que Santos y Uribe, aquí los ciudadanos estamos para construir otra democracia realmente pluralista, incluyente y transformadora, y para eso la firma de los Acuerdos de La Habana postula unos principios y mecanismos y el espacio con el ELN ayudará a profundizar, complementar y desarrollar unos nuevos.

Santos y Uribe cabalgan sobre el miedo y sobre la falacia de un inexistente Estado de Derecho.
