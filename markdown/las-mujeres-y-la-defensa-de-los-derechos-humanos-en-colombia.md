Title: Las mujeres y la defensa de los derechos humanos en Colombia
Date: 2016-09-09 11:00
Category: Mujer, Nacional
Tags: Defensa de derechos de las mujeres, Francia Márquez, Mujeres y Paz, Violencia contra las mujeres en Colombia
Slug: las-mujeres-y-la-defensa-de-los-derechos-humanos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Marcha-por-la-paz.-Manifestantes-con-Pancartas-en-la-Plaza-de-Bolívar-001.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: notimundo] 

###### [9 Sept 2016] 

[Como Francia Márquez, cientos de mujeres en Colombia han dedicado su vida a la defensa de los derechos humanos, enfrentando con valor las [[múltiples amenazas, persecuciones y estigmatizaciones](https://archivo.contagioradio.com/lideres-de-movilizacion-afro-al-norte-del-cauca-son-amenazados-por-paramilitares/)] de las que son objeto, **por defender como a sus propios cuerpos, los territorios en los que nacieron** y han visto crecer a los suyos.]

[En este largo camino de lucha y resistencia, Francia ha aprendido que no sólo en su comunidad se defienden los derechos humanos y ambientales, lo ha comprendido caminado junto a quienes **desde otras latitudes aportan a la construcción de una sociedad más justa y equitativa**, así como desde hace 18 años lo hace la [[Corporación Sisma Mujer](https://archivo.contagioradio.com/organizaciones-de-mujeres-proponen-5-claves-para-enfrentar-la-violencia-sexual/)]. ]

[Esta Corporación ha apoyado procesos de exigibilidad para que el Estado colombiano cumpla con sus obligaciones y compromisos frente a los derechos de las mujeres, a través del impulso de procesos de investigación y las alianzas con diversas organizaciones de derechos humanos. Este colectivo de mujeres **desde la diversidad de saberes han aportado a transformar la realidad de las mujeres** que han vivido en sus cuerpos la crudeza del conflicto armado, como también lo han hecho desde 1996, las 315 organizaciones que conforman la Ruta Pacífica de las Mujeres.]

[Desde el feminismo, la Ruta Pacífica, ha trabajado para visibilizar la forma en que la guerra ha impactado sus vidas para sumar sus voces y gritar ¡No parimos hijos ni hijas para la guerra!, para dejar claro que no quieren **¡Ni una guerra que las destruya, ni una paz que las oprima!** y para insistir en que las transformaciones tienen que darse tanto en lo público como en lo privado.]

[Y es que en los índices de violencia en Colombia, han llevado a que cada trece minutos una mujer sea agredida, cada media hora otra sea víctima de violencia sexual y **cada cuatro días una mujer sea asesinada**, de allí la importancia de haber incluido el [[enfoque de género en los acuerdos de paz](http://bit.ly/2ctNGqt)], gracias al esfuerzo de organizaciones como la Corporación Humanas. ]

[En agosto de 2015, la Humanas y otras tres organizaciones enviaron insumos a la Mesa de Diálogos de La Habana para lograr que la **violencia sexual no sea objeto de amnistías ni indultos**, que se cree un grupo especial de investigación para documentar los delitos de violencia sexual cometidos en el marco del conflicto armado y que haya [[amnistía para las mujeres que están en prisión](http://bit.ly/23TwsST)] por delitos menores relacionados con el narcotráfico y cuya condición pone en [[peligro la subsistencia de sus familias](http://bit.ly/2bxYJum). ]]

<iframe id="audio_12846237" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12846237_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
