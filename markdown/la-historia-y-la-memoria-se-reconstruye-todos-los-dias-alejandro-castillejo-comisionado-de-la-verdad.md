Title: La historia y la memoria se reconstruye todos los días: Alejandro Castillejo, comisionado de la Verdad
Date: 2020-04-12 17:51
Author: AdminContagio
Category: Actualidad, Paz
Tags: comision de la verdad, Comisionado
Slug: la-historia-y-la-memoria-se-reconstruye-todos-los-dias-alejandro-castillejo-comisionado-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Alejandro-Castillejo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Comisión para el Esclarecimiento de la Verdad {#foto-comisión-para-el-esclarecimiento-de-la-verdad .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tras un proceso de selección de cuatro meses, la Comisión para el Esclarecimiento de la Verdad (CEV) seleccionó a **Alejandro Castillejo** como nuevo comisionado, él será el encargado de adelantar el trabajo que llevaba Alfredo Molano, que recogió las verdades del conflicto en la Amazonia y Orinoquía hasta su fallecimiento en octubre de 2019. (Le puede interesar: ["Alfredo Molano un caminante de la verdad"](https://archivo.contagioradio.com/alfredo-molano-un-caminante-de-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Castillejo, viene del seno de las Ciencias Humanas, antropólogo con estudios de posgrado en Paz y Conflicto, doctor en antropología. Él como su predecesor quiere ser un comisionado itinerante: caminar los territorios y escuchar la verdad de voz de quienes de padecieron del conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta entrevista con Contagio Radio habló sobre lo que será su trabajo, los retos y las apuestas de la Comisión de la Verdad y las [comunidades](https://www.justiciaypazcolombia.com/ii-sesion-de-la-catedra-de-la-universidad-de-la-paz-bajo-atrato/) en la construcción del esclarecimiento de la verdad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"customTextColor":"#006699"} -->

#### Contagio Radio: Usted tiene que llegar a un empalme que debe hacerse de forma virtual por la contingencia del Covid-19, ¿cómo ha sido ese proceso? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Alejandro Castillejo: Por una parte el tratar de enterarme sobre el trabajo de la CEV de por sí es una tarea dispendiosa, pero he tenido una disposición y colaboración de los colegas que están ahí, pero sí, por Internet es una locura. Pero es bueno que estamos teniendo esta discusión sobre lo digital: el teletrabajo, el teleestudio y la telecomisión... porque estamos descubriendo que necesitamos encontrarnos físicamente con los seres humanos para construir los mundos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ha habido una mitificación al rededor de la tecnología, pero lo que nos está pasando nos demuestra que necesitamos físicamente de las personas para encontrarnos y construir saber. (Le puede interesar: ["Lanzan propuesta de Universidades de Paz"](https://archivo.contagioradio.com/lanzan-propuesta-de-universidades-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"customTextColor":"#006699"} -->

#### CR: ¿Qué significa para Alejandro Castillejo llegar a esta Comisión de la Verdad, tras ser incluso consultor de una comisión similar como la de Perú? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

AC: Siento una confianza de una institución, de un grupo de colegas. También el reto de poner a circular mi trabajo de muchos años, yo también he trabajado sobre los problemas de las transiciones y los procesos de verdad y reconciliación. Para nadie es un secreto que yo hablo del evangelio global del perdón y la reconciliación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora tengo el reto de apoyar lo que se ha hecho, así como de inspirar otras cosas. Yo estoy anonadado con las expresiones de afecto y las expectativas. Son retos intelectuales, políticos, administrativos y logísticos y después de estos años, es una forma de cerrar un circulo que ha empezado con la teoría, la praxis y se podría cerrar con esta práctica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"customTextColor":"#006699"} -->

#### CR: ¿Cómo evalúa lo que ha sido el funcionamiento de esta Comisión? ¿Podría haber funcionado de otra manera? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

AC: Yo creo que todos los ejercicios comicionales, están en el seno de contextos donde se aplican lenguajes de transiciones; yo me sitúo en el discurso de la verdad, justicia, reparación y garantía de no repetición, por lo general, esos lenguajes se basan sobre presupuestos fundacionales como que la palabra y el testimonio libera.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entonces digo que todas las Comisiones iluminan tanto como oscurecen algunas cosas, es decir, que hay elementos que pueden hacer audibles ciertas experiencias pero pueden correr el riesgo de hacer inaudibles otras experiencias, del balance de ambas situaciones está el reto. (Le puede interesar:["Con Diálogos para la No Repetición, Comisión de la Verdad promoverá defensa de líderes sociales"](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Yo creo que uno podría extenderse en las discusiones y conversaciones sobre esas cosas sobre las que las Comisiones no han estado tan preocupadas por muchas razones, pero creo que la conciencia sobre eso podría ampliar la escucha, para alcanzar un balance. (Le puede interesar: ["Quien pretenda negar el conflicto puede intentarlo, pero conocer la verdad es cuestión de tiempo: Sofía Macher"](https://archivo.contagioradio.com/quien-pretenda-negar-el-conflicto-puede-intentarlo-pero-conocer-la-verdad-es-cuestion-de-tiempo-sofia-macher/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"customTextColor":"#006699"} -->

#### CR: Hay muchas expectativas puestas sobre el trabajo de la CEV y los alcances que pueda tener, ¿qué opinión tiene sobre esto? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

AC: Han habido Comisiones donde sus mandatos son más conservadores, en últimas, la Comisión es un grupo de investigación sobre la violencia en un Estado-nación, pero finalmente cada contexto es distinto. En la Comisión de Colombia se le pusieron una gran cantidad de responsabilidades que me parece que si la sudafricana, que era más acotada era tan difícil...

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero una con tanta expectativas, es decir, esclarecimiento histórico, convivencia... cada tarea tiene su grupo de expectativa y a la colombiana le pusieron una gran cantidad de cosas pero también se entiende por qué. (Le puede interesar: ["Comisión de la verdad en Colombia: entre lo imaginable y lo posible, una entrevista con Alejandro Castillejo"](https://archivo.contagioradio.com/comision-de-la-verdad-en-colombia-entre-lo-imaginable-y-lo-posible/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El presente es una pregunta que yo me hago, porque a veces suponemos que los lenguajes de la verdad, justicia, reparación y no repetición ha implicado dejar el pasado atrás. Pero en situaciones de posguerra hay una porosidad entre el pasado, el presente y el futuro, y a mí lo que me llama la atención es que en cualquier escenario de estos uno descubre que así como hay violencias que han quedado atrás, hay unas que están presentes

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sobre esa porosidad tenemos que hablar porque tenemos que hablar de las violencias que han quedado atrás y las violencias que continuan, y sobre eso, las discusiones en la justicia transacional son bastante pobres. Y yo creo que deberíamos poner sobre esa porosidad la discusión, el asesinato sistemático, la muerte de líderes sociales... el asesinato de gente que está imaginándose otra sociedad distinta. Lo que me hace pensar esa continuidad de la guerra.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"customTextColor":"#006699"} -->

#### CR: En Colombia muchos territorios han hecho apuestas importantes por la paz, la reconciliación y la convivencia, tal es el caso de la Universidad de Paz, ¿cómo reconocer esos saberes y esos aportes de la gente? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

AC: Yo diría que necesitamos una Universidad Itinerante por la paz, yo tengo fe en la posibilidad transformadora del conocimiento que se mueve. No solo el conocimiento "académico" que a veces se mueve y a veces no, pero también en el conocimiento de la gente sobre cómo sobrevivir. Es decir, en última instancia, en los territorios en Colombia las organizaciones sociales organizaron mecanismos para sobrevivir.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero también ha habido microexperiencias, escenarios invisibles a muchos ojos que han adquirido y construido cosas en torno a la supervivencia, física, simbólica y material de su mundo. Una experiencia que me interesa apoyar desde la Comisión es una universidad con conocimientos que se mueven de aquí para allá, y habrá que ver si hay gente que le interesa eso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Porque ello implica alterar la forma en que funcionan las Universidades más allá de sus producciones de saberes institucionalizados e indexados, para descolonizar la Universidad, si se quiere. (Le puede interesar: ["Diez pautas para presentar un caso ante la Comisión para el Esclarecimiento de la Verdad"](https://archivo.contagioradio.com/10-pautas-para-presentar-un-caso-consolidado-ante-la-comision-de-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"customTextColor":"#006699"} -->

#### CR: A veces queda la impresión de que las cosas que ocurren en el país no todas interesan a todas las personas, entonces se puede ver truncada esa construcción de la verdad como bien público. ¿Cómo cambiar eso? 

<!-- /wp:heading -->

<!-- wp:paragraph -->

AC: Creo que primero uno tendría que pensar qué es lo público: cuando uno empieza a pensar que lo público puede estar asociado con el Estado o con los medios de comunicación... pero reunirse con una comunidad también es una dimensión de lo público. (Le puede interesar: ["La Comisión de la Verdad y el esclarecimiento de crímenes ambientales"](https://archivo.contagioradio.com/crimenes-ambientales-marco-del-conflicto-armado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entonces, tenemos que empezar a pensar en una serie de escenarios sobre lo público. Pero hay que darse la pela con el hecho de que tienen que ser escenarios que logren juntar visiones distintas de esta guerra, y de estas guerras continuas. Yo soy enemigo de hacer profilaxis a la historia y dejarla en el pasado, la historia y la memoria se reconstruye todos los días.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Mi método de trabajo es itinerante, yo soy amigo de caminar por los territorios y juntar gente** para permitir que se encuentren diferentes posturas, y tarde o temprano habrá que darse discusiones sobre las verdades que puedan venir y las aventuras que la Comisión esté dispuesta a hacer.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hay que escuchar mucha gente y su historia, y crear las condiciones para oír. En ese orden de ideas yo tengo un trabajo para hacer, con el pasar de los años yo he hablado con sectores de esta sociedad, y hoy hay sectores divididos: Por ejemplo, las Fuerzas Militares y FARC no son homogéneas, hay que explorar esas fisuras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También hay que aprender a escuchar otros discursos de violencias que se salen de las formas tradicionales y los lenguajes legales de los DD.HH. y del conflicto armado. Hay otras maneras de construir paz, reconciliación, lo que yo llamo proginidad, que no están tradicionalmente situados en el lenguaje tradicional de la transición y de la memoria pero que representan oportunidades también.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4,"customTextColor":"#006699"} -->

#### Un último apunte: La paz a pequeña escala y la construcción de los mundos posibles 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Yo veo la paz como un proceso de pequeña escala, nadie negará que se necesita un encuadre institucional y social, pero dentro de 10 años, además del encuadre, veremos un grupo de éxitos y fracasos simultáneos, y lo que quedará es lo que los seres humanos podamos reproducir en la vida cotidiana para transformar los mundos posibles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Yo pienso que precisamente la CEV es ese encuadre: El permitir que esos mundos se creen en la vida de la gente en 10 años, y me parece que ese es un compromiso político que todos asumimos. (Le puede interesar: ["Comisión de la Verdad expuso sus avances tras un año de trabajo"](https://archivo.contagioradio.com/comision-de-la-verdad-expuso-sus-avances-tras-un-ano-de-trabajo/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
