Title: Condena a Policía Nacional por muerte de estudiante Jhonny Silva en Cali
Date: 2017-06-18 13:22
Category: Nacional
Tags: Abuso de fuerza ESMAD, Ejecuciones Extrajudiciales, Universidad del Valle
Slug: jhonny-silva-condena-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ESMAD_.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [17 Jun 2017] 

Un fallo del Consejo de Estado condenó a la Policía colombiana por el caso del estudiante **Jhonny Silva Aranguren**, asesinado durante una protesta al interior de la Universidad del Valle en septiembre de 2005, manifestaciones en las que también resultó herido el estudiante Eduardo Perdomo Abello.

Por la sanción del máximo órgano jurisdiccional se conmina a la policía a pagar **700 millones de pesos**, considerando que con su actuación el Escuadrón Móvil Anti Disturbios ESMAD **desconoció en el operativo los principios de precaución y proporcionalidad** para evitar que se ocasionaran daños a la integridad de los manifestantes.

En la sentencia se reconoce que los habitantes tienen el derecho a expresar su disenso frente a las medidas que adopten las autoridades del estado y agrega que la policía debe estar preparada "para **mantener el orden pero siempre respetando los derechos más caros a la persona humana**" incluyendo su derecho a la protesta.

El fallo en favor de la ponencia, presentada por el magistrado Hernán Andrade, **rechaza la decisión tomada por la justicia ordinaria de archivar las investigaciones** en el caso contra miembros del ESMAD, por no se esclarecidas las circunstancias que antecedieron los hechos se configuraría como una **ejecución arbitraria** lo que implica una violación grave a los Derechos Humanos que podría ser objeto de conocimiento de la justicia internacional.

**ESMAD debe aprender sobre Derechos Humanos**

La sanción impuesta por el Consejo de Estado ordena al Ministerio de defensa a **impartir cursos sobre Derechos Humanos a los agentes del ESMAD** en todo el país. La sentencia señala que los cursos deben dictarse en un plazo máximo de dos meses y deberán estar enfocados a la protección integral de quienes ejercen el derecho a la protesta.

Adicionalmente, el órgano jurisdiccional solicitó a **la Fiscalía General de la Nación evaluar la posibilidad de reabrir la investigación contra los integrantes del ESMAD** que estuvieron presente en las protestas del 2005, sobre el modo en que actuaron para controlarlas. Le puede interesar: ["Gobierno debe investigar abusos cometidos por el ESMAD"Congresistas de EEUU](https://archivo.contagioradio.com/gobierno-debe-investigar-los-abusos-cometidos-por-la-policia-de-esmad-congresistas-de-eeuu/)

### **El caso Jhonny Silva** 

Por el caso de Jhonny Silva, estudiante de Ingenería Química,  el Tribunal Administrativo del Valle del Cauca declaró responsable a la Policía y ordenó a la Nación indemnizar a su familia con **250 salarios mínimos legales mensuales vigentes**.

Según reza la sentencia, el 22 de septiembre de 2005, a las siete de la noche, el Esmad ingresó a la Universidad del Valle **después de terminada una manifestación** hasta el edificio administrativo para desmantelar un campamento que había sido levantado por unos estudiantes.

La Comisión Interamericana de Derechos Humanos, Cidh, admitió en 2016 **investigar la muerte de Silva ante las potenciales violaciones a los derechos de la vida, integridad personal, garantías judiciales, libertad de expresión y de pensamiento**, entre otros, pactados en la Convención Americana de Derechos Humanos.
