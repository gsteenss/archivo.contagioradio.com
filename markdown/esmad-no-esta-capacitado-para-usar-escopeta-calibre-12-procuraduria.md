Title: ESMAD no está capacitado para usar Escopeta calibre 12: Procuraduría
Date: 2020-01-14 19:15
Author: CtgAdm
Category: Judicial, Movilización
Tags: Dilan Cruz, Escopeta, ESMAD, procuraduria
Slug: esmad-no-esta-capacitado-para-usar-escopeta-calibre-12-procuraduria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/27537460656_ed3d676360_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Chavo Censura {#foto-chavo-censura .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Este 14 de enero, a raíz de la investigación que se adelanta por el asesinato de Dilan Cruz, la Procuraduría General de la Nación le solicitó a la Policía que suspenda de inmediato el uso de la escopeta Calibre 12 y su munición de impacto dirigido usada por el Escuadrón Móvil Antidisturbios (ESMAD), **al encontrar que los uniformados no están capacitados para su operación.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según explicó la Procuraduría en su página, el uso de la Escopeta Calibre 12 **"representa un serio peligro para la comunidad**, lo cual se ha hecho palpable no solo en Colombia, sino de manera reciente en Chile y desde épocas pretéritas en varios países". El ente también recuerda que la incorporación de esta arma al inventario del ESMAD vulnera la Constitución en su artículo 216, que señala el objetivo de la Fuerza Pública:

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *...el mantenimiento de las condiciones necesarias para el ejercicio de los derechos y libertades públicas, y para asegurar que los habitantes de Colombia convivan en paz*.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

La Institución, en consonancia con los reclamos de defensores de derechos humanos, también reclamó por la proporcionalidad en el uso de la fuerza, así como que la misma solo debe ser empleada como último recurso "para proteger la vida e integridad física de las personas incluída la de ellos mismos". (Le puede interesar: ["ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles"](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Los policías solo aprenden a usar el arma cuando las operan en las manifestaciones**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El argumento central de la Procuraduría para pedir que se suspenda el uso de esta arma se centra en que los uniformados que las emplean no tienen la capacitación necesaria para hacerlo. Según la investigación del ente, los programas de capacitación duran 48 horas en las que tendrían que aprender sobre el uso de la fuerza, técnicas de intervención y correcto empleo de los elementos que pueden usar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pero la capacitación específica para el uso de la Escopeta Calibre 12 solo ocurre cuando "existe disponibilidad de munición, aunque comúnmente, dado su elevado costo, hay déficit, al punto que en **muchas ocasiones para el desarrollo de un seminario sólo proporcionan 100 cartuchos para capacitar 1000 hombres"**. (Le puede interesar: ["Las razones para pedir el desmonte del ESMAD a 20 años de su creación"](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido, la Institución señala que hay eventos en los que 100 integrantes del ESMAD disparan el arma una vez, mientras 900 hombres solo los observan; por lo que concluye que la "destreza, habilidad y 'memoria muscular' respecto del uso de la Escopeta Calibre 12 con su munición de impacto dirigido, la adquiere el policial sólo cuando se enfrenta a la realidad del caso en el que participa, es decir con ciudadanos".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Otras dudas que surgen sobre el ESMAD

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En la comunicación, la[Procuraduría](https://www.procuraduria.gov.co/portal/Procuraduria-pide-a-la-Policia-suspension-inmediata-del-uso-de-Escopeta-calibre-12-utilizada-por-el-Esmad-para-disolver-disturbios-y-bloqueos-de-vias.news) sostiene que de acuerdo a los testimonios recogidos en el marco de la investigación se estableció que el arma fue usada por primera vez en el marco del paro nacional, pero su uso no tiene jerarquías. Lo que llamó la atención del Ministerio Público sobre este hecho es que ninguno de los instructores que están capacitados para usar el arma manifestaron portarla en esas fechas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Duda que se suma a la que han expresado defensores de derechos humanos, sobre el respeto por los protocolos de intervención que tiene el ESMAD, y el uso que hacen los uniformados de elementos como las aturdidoras o marcadoras, asi como de las recalzadas. (Le puede interesar: ["En veinte años el ESMAD ha asesinado a 34 personas"](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
