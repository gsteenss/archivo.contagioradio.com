Title: Situación de trabajadores de Aseo de Bogotá ya se había presentado en primera Alcaldía de Peñalosa
Date: 2018-02-07 17:02
Category: Movilización, Otra Mirada
Tags: Alcaldía, Peñalosa, Trabajadores de Bogotá
Slug: situacion-de-trabajadores-de-aseo-de-bogota-ya-se-habia-presentado-en-primera-alcaldia-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/rcn-radio.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RCN Radio] 

###### [07 Feb 2018] 

Orlando Quiroga trabaja desde hace 33 años en la empresa pública Aguas de Bogotá, y no es la primera vez que vive la actual situación de crisis laboral que actualmente enfrentan los trabajadores de esa empresa. De acuerdo con él, en el tiempo que lleva trabajando para esta compañía, los despidos a los trabajadores públicos han sido sistemáticos, de hecho, 1998 la alcaldía de Enrique Peñalosa ya había enfrentado la movilización de los trabajadores cuando quiso privatizar el acueducto de la ciudad.

Para Orlando las afirmaciones de Peñalosa en las que señala que la protesta de los trabajadores estaba permeada por intereses políticos, no son ciertas, “el criterio número uno fue unidad y no vamos a permitir que esta coyuntura electorera permee el movimiento y la actuación de los trabajadores. Hasta el momento eso se ha cumplido y** no hemos dejado que se involucren ni candidatos ni partidos políticos”**.

Asimismo, Quiroga manifiesta que la falta de planificación es la que tendría a toda la flota de camiones dañada, y a más de mil trabajadores con afectaciones laborales. (Le puede interesar: ["Distrito no permite que trabajadores de Aguas de Bogotá realicen su trabajo"](https://archivo.contagioradio.com/distrito-no-permite-que-los-trabajadores-de-aguas-de-bogota-realicen-sus-labores/))

### **No avanzan los diálogos sobre las basuras** 

Los trabajadores estuvieron reunidos este martes con una delegación de la Procuraduría General de la Nación, en donde reiteraron que desde el mes de **diciembre los trabajadores habían alertado de la situación sobre la empresa y la posible pérdida de miles de empleos.**

“No había capacidad operativa, el gerente de manera sistemática vino retirando los vehículos, acabando con la flota. Además n**o había inversión en repuestos para los equipos que estaban con necesidades de mantenimiento,** tanto preventivo como correctivo, y estábamos en total abandono”, afirma Quiroga.

Sumado a esto, Quiroga asegura que la empresa estaba en pleno conocimiento de que no podía realizar un despido masivo sin la autorización del Ministerio de Trabajo y además, retirando de sus cargos a personas que están próximos a pensionarse. A su vez, el auto 587 del 15 de diciembre de 2015 de la Corte Constitucional, **le decía a las instituciones distritales que debe prevenir los riesgos ambientales y sociales.**

De igual forma, de acuerdo con el trabajador, no existía ningún impedimento legal que no permitiera que en la licitación se exigiera la vinculación de los trabajadores de Aguas de Bogotá. (Le puede interesar: ["Peñalosa no da respuesta a futuro laboral de más de 2.000 trabajadores de Aguas de Bogotá"](https://archivo.contagioradio.com/penalosa-no-da-respuesta-a-futuro-laboral-de-mas-de-2000-trabajadores-de-aguas-de-bogota/))

### **Las basuras en las calles de Bogotá** 

Referente a los bloqueos de las personas en las calles con barricadas de basuras, Quiroga afirmó que los trabajadores también están a la espera de que esa situación se resuelva prontamente, sin embargo, denunció que personas no capacitadas para recolectar las basuras como habitantes de calle o integrantes de la Fuerza Pública son los que están haciendo esta labor, sin tener el conocimiento para realizar dicha tarea.

“No se va a solucionar en 8 días el problema de las basuras porque no hay ni la flota, ni los operarios capacitados para estas labores, miren el caso de Engativá, es muy diciente por eso decimos que aquí la responsabilidad solo tiene un nombre: Enrique Peñalosa”, concluye Quiroga.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
