Title: Maestros argentinos en paro por 48 horas exigen alzas salariales
Date: 2016-07-25 14:29
Category: El mundo
Tags: Federación Nacional Docente de Argentina, Paro maestros Argentina, Sistema educativo Argentina
Slug: maestros-argentinos-en-paro-por-48-horas-exigen-alzas-salariales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Federación-Docentes-Argentina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CTA Nacional] 

###### [25 Julio 2016]

La Federación Nacional Docente de Argentina convocó este lunes un paro de 48 horas en las provincias de Buenos Aires, Catamarca, Chaco, Chubut, Entre Ríos, Santa Cruz y Tierra del Fuego, ante la **falta de mejoras salariales, la precarización laboral y la carestía de la vida** que enfrentan actualmente. La medida podría extenderse a otras regiones de no haber una respuesta efectiva por parte del Gobierno, según informó la Federación en el congreso celebrado el pasado viernes.

En su más reciente comunicado, los maestros aseguran que además de la caída del poder adquisitivo del salario y la precarización de las condiciones laborales, enfrentan **constantes persecuciones, judicializaciones, despidos, congelamientos de vacantes, problemas de infraestructura** en las escuelas y comedores escolares, así como en el sector de transportes; condiciones que dificultan su labor y agudizan la crisis por la que atraviesa el sistema marcada por una creciente deserción escolar.

Por su parte los docentes de Buenos Aires, denunciaron que la gobernadora María Eugenia Vidal planea destinar 100 mil dolares para espiarlos y controlarlos con la implementación del "observatorio analítico, actualizado en tiempo real, sobre las diversas problemáticas de la educación", que será ejecutado por una empresa informática. De acuerdo con los maestros **el dinero debía ser destinado a mejorar el sistema educativo y no a espiarlos**.

<div class="txt_newworld">

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

</div>

 
