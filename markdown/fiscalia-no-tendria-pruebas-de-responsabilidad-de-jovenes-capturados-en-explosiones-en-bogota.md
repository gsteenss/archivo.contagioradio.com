Title: Fiscalía no tendría pruebas de responsabilidad de jóvenes capturados en explosiones en Bogotá
Date: 2015-07-09 16:53
Category: DDHH, Nacional
Tags: Capturas en Bogotá, congreso de los pueblos, ELN, Explosiones en Bogotá, Jóvenes capturados
Slug: fiscalia-no-tendria-pruebas-de-responsabilidad-de-jovenes-capturados-en-explosiones-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/planton-paloquemao-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: el rebelde medios alternativos 

###### [9 Jul 2015] 

Según fuentes cercanas al proceso, los hechos de los que se acusa a los 13 jóvenes capturados en la mañana de este 8 de Julio en Bogotá no tienen que ver con las explosiones de la semana pasada en la ciudad. Esto se corroboraría en la acusación que hace la fiscalía por hechos ocurridos el pasado 20 de Mayo en medio de una protesta en la Universidad Nacional.

En medio de la audiencia de legalización de captura que se desarrolla desde las 11 PM de ayer, el juez aceptó la legalidad de los allanamientos y hacia las 4 de la tarde la fiscalía presenta el alegado de legalización de la captura. La legalización de los allanamientos da cierta legitimidad a la actuación de la fiscalía aunque los cargos no correspondan a lo que se ha publicitado en algunos  medios de información.

Según la fuente es muy probable que los jóvenes permanezcan en prisión hasta las próximas audiencias de descubrimiento de las pruebas de la fiscalía, sin embargo, la etapa inicial del proceso debe cumplirse en un plazo de 36 horas, de lo contrario las personas capturadas deberán recobrar la libertad por vencimiento de términos.

Otra de los cuestionamientos de la defensa es que la emisora del ELN a través de su cuenta de Twitter negó que las personas capturadas fueran parte de esa guerrilla y acusaron al presidente Santos de presentar a los jóvenes como falsos positivos judiciales.

Se espera que en las próximas horas el juez decida si deja en libertad a los jóvenes o por el contrario inicia juicio formal contra los acusados.
