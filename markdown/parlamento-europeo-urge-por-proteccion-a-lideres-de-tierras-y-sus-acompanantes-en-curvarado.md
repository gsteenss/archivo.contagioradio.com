Title: Parlamento europeo urge por protección a líderes de tierras y sus acompañantes en Curvaradó
Date: 2015-02-16 23:05
Author: CtgAdm
Category: DDHH, Nacional
Slug: parlamento-europeo-urge-por-proteccion-a-lideres-de-tierras-y-sus-acompanantes-en-curvarado
Status: published

###### Foto: europafocus.com 

Trece integrantes del **Parlamento Europeo**, instaron al gobierno colombiano a proteger a líderes de restitución de tierras en el Curvaradó, a sus acompañantes y a realizar las tareas necesarias para la restitución efectiva de sus tierras en el Chocó colombiano. Para los y las parlamentarias, no se entienden las razones por las cuales **no se han realizado los desalojos de los ocupantes de “mala fe” que fueron ordenados por la Corte Constitucional** desde Noviembre de 2014.

[El comunicado del 5 de febrero](http://justiciaypazcolombia.com/IMG/pdf/150205_carta_al_presidente_santos-curba.pdf), dado a conocer este 16 de febrero los parlamentarios también insistieron en la necesidad de establecer a los responsables de las amenazas e intentos de asesinato contra **Yomaria Mendoza y Enrique Cabezas** quienes sufrieron más de 80 agresiones durante el 2014, luego de denunciar la permanencia ilegal de empresarios en la región, la apropiación de tierras y otras violaciones a los Derechos Humanos.

Los parlamentarios y parlamentarias consideran que los "a**taques, amenazas y asesinatos es imprescindible que se avance en las investigaciones** que deriven en la captura y detención de los autores intelectuales de estas agresiones que permanecen en la impunidad".
