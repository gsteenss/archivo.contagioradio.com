Title: "Dejar de explicar los acuerdos", la estrategia del Centro Democrático
Date: 2016-10-06 12:38
Category: Nacional, Paz
Tags: acuerdos de paz, Centro Democrático, Plebiscito
Slug: dejar-de-explicar-los-acuerdos-la-estrategia-del-centro-democratico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/juan-carlos-velez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [6 Oct de 2016] 

**30 grupos económicos **y personas naturales, entre las que se destacan la Organización Ardila Lülle, Grupo  Bolívar, Grupo Uribe,  Colombiana de Comercio, dueños de Alkosto y Codiscos **financiaron con \$1.300 millones la campaña por el NO**. Además Juan Carlos Vélez, gerente de la campaña del Centro Democrático **aceptó que tergiversaron información y que la estrategia fue dejar de explicar los acuerdos. **

Vélez ex candidato a la alcaldía de Medellín afirmó que la campaña tuvo asesoría de expertos de Brasil y Panamá quienes recomendaron “generar un sentimiento de indignación” en poblaciones de distintas regiones basándose en puntos como “la no impunidad, la elegibilidad y la reforma tributaria”.

A través de redes sociales y emisoras radiales en regiones como la Costa Caribe **“individualizamos  el mensaje de que nos íbamos a convertir en Venezuela”**, en municipios del Cauca “**pasamos propaganda  por radio la noche del sábado centrada en víctimas”**, y en ciudades como Bogotá **[“el No ganó sin pagar un peso”](https://archivo.contagioradio.com/diez-ventajas-del-no-monsenor-monsalve/)**, afirmó Vélez.

Velez reveló que durante la campaña falsearon información, justamente **para provocar una reacción negativa frente al [acuerdo de paz entre el gobierno y las FARC](https://archivo.contagioradio.com/alcaldes-y-gobernadores-respaldan-el-caracter-humanitario-de-los-acuerdos-de-paz/)**. Agregó que le apuntaban a poner en la mesa “68 objeciones, especialmente relacionados con la política de narcotráfico, elegibilidad y justicia”.

En las ultimas horas **el Centro Democrático emitió un comunicado en el que desautoriza las declaraciones de Juan Carlos Vélez sobre la estrategia de publicidad de la campaña** y afirman que su campaña "no apeló a la mentira ni a la tergiversación de mensajes, acudió a los argumentos para que las personas votaran a conciencia sobre el gran daño que se hubiese hecho al país si estos acuerdos se hubieran aprobado e incorporado automáticamente a la Constitución"

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
