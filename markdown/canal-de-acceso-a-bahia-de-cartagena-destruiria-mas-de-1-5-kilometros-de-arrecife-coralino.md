Title: Canal de Acceso a Bahía de Cartagena destruiría más de 1.5 kilómetros de arrecife coralino
Date: 2018-09-20 13:12
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Arrecifes de coral, Canal, Cartagena, Varadero
Slug: canal-de-acceso-a-bahia-de-cartagena-destruiria-mas-de-1-5-kilometros-de-arrecife-coralino
Status: published

###### [Foto: Salvemos Varadero] 

###### [20 Sept 2018] 

Ambientalistas en Cartagena denunciaron que la expansión portuaria que se pretende realizar entre Isla Draga e isla Abanico, con la finalidad de que se de la licencia al proyecto "Variante al canal de Acceso", destruiría parte del espacio de **vida marino de Varadero y acabaría con más de 1.5 kilómetros de arrecifes coralinos, descubiertos recientemente.**

El abogado Rafael Vergara, que impulsa la iniciativa salvemos Varadero, explicó que el ingreso de los barcos que llegarían a los muelles en Cartagena, se haría a través de un espacio marítimo en el que se descubrió la existencia de **3.6 ha de arrecife que tiene alta densidad de colonias de gran tamaño, 0.95 ha de arrecife con densidad media de colonias grandes y 4.91** ha de arrecife de baja densidad de colonias grandes, que en conjunto ha sido denominado como arrecife de Varadero.

Este ecosistema se ubica al lado del canal de Bocahica,  por donde ingresan y salen los barcos cargueros a la Bahía de Cartagena, y pese a estar expuesto a unos altos niveles de contaminación, se mantiene intacto.

Lo preocupante, según los ambientalistas es que para obtener la profundidad que los barcos necesitarían con el objetivo de llegar al puerto, tendría que dragarse hasta **20 metros de profundidad en el mar**, destruyendo la barrera de corales.

### **La ANLA autorizo los estudios para el canal** 

Vergara señaló que este proyecto previamente había contratado una misión internacional para que realizara los estudios pertinentes ambientales. Esa documentación arrojó como resultado **que no era prudente realizar el canal entre Isla Draga e isla Abanico** por el arrecife de coral, sin embargo, según el abogado, el proyecto cambió de denominación a "Variante de Acceso" y la Autoridad Nacional de Licencias Ambientales concedió el permiso para que se realicen nuevos estudios.

"Por qué si se sabía que **ahí estaba el arrecife coralino, por qué autorizaron ese impacto ambiental, en el caso de la ANLA"** afirmó Vergara y agregó que se esta haciendo un estudio de impacto ambiental debido a que si se "corta un pedazo de la terraza del arrecife, se puede cambiar el impacto de la corriente"  y sumado a ello, el tránsito permanente de las embarcaciones, causaría daños irremediables en el arrecife.

Asimismo Vergara, manifestó que este arrecif**e no aparece en el Atlas Nacional de Arrecifes del país, por la tanto no se reconoce su existencia**, a pensar de que los estudios demuestren lo contrario. (Le puede interesar: ["ANLA aprueba licencias ambientales en el único bosque de Niebla del Tolima"](https://archivo.contagioradio.com/anla-licencia-bosque-niebla/))

### **El arrecife coralino una riqueza ambiental** 

De acuerdo con Vergara, este arrecife coralino podría tener millones de años, y además de ser el hogar de miles de especies marinas, sirve para regular la energía de las corrientes de agua. Además, se estima que produce hasta 70 toneladas de alimentos para su sistema de cadena alimenticia.

Durante el 2017, la ONU señaló que el planeta tierra ha perdido la mitad de los ecosistemas de arrecifes coralinos y se estima que Colombia, en una década, **pierda el 20% de arrecifes. **(Le puede interesar:["El planeta ha perdido el 50% de los arrecifes de coral"](https://archivo.contagioradio.com/39211/))

<iframe id="audio_28753388" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28753388_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
