Title: Europa se levanta contra el BCE: 500 manifestantes detenidos en protestas en Frankfurt
Date: 2015-03-18 20:58
Author: CtgAdm
Category: Economía, El mundo
Tags: Banco Central Europeo, BCE, Frankfurt, inaguración de la sede del BCE Frankfurt, Protestas en Alemania contra el BCE
Slug: europa-se-levanta-contra-el-bce-500-manifestantes-detenidos-en-protestas-en-frankfurt
Status: published

###### Foto:24-horas.mx 

###### **Entrevista con [Jonas Pentzien], miembro de Blockupy:** 

###### <iframe src="http://www.ivoox.com/player_ek_4232788_2_1.html?data=lZeglJycfI6ZmKiakpyJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3Zzsbby8aPt8afztTjy9HNvsKfxNTb1tfFb8bgjKewp5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La **inauguración de la nueva sede del BCE en Frankfurt**, Alemania, ha sumido a la ciudad en protesta[s] en contra de la institución, de la Troika, de las políticas neoliberales y en **contra del propio capitalismo**.

La nueva sede **ha costado 1.200 millones de euros, en comparación con los 2.000 millones que el BCE exige a Grecia como recorte en presupuesto social** para paliar la pobreza extrema causada por las propias políticas impuestas por el BCE.

En el marco de las protestas convocadas por el **Blockupy,** plataforma que agrupa cientos de **organizaciones sociales** en contra del capitalismo y de la crisis europea, **ha convocado a 8.000 personas**, esta jornada ha estado acompañada de acciones de desobediencia civil pacífica como la de los manifestantes que se han **colgado del edificio del BCE** con una pancarta que ponía **“El capitalismo mata”**.

También se han realizado acciones violentas en la mañana de este miércoles, que finalizaron con el ataque a una comisaría de policía y más de **10 coches policiales ardiendo**.

El presidente de la institución financiera, **Mario Draghi, que califica el edificio de "lo mejor que Europa puede lograr junta"**, ha tenido que **entrar en helicóptero**, igual que otros presidentes de bancos invitados, debido a los bloqueos de calles por parte de los manifestantes.

En horas de la **tarde** unos **22.000 participantes acudieron a la convocatoria**, donde se han realizado cientos de acciones de sabotaje contra bancos y contra símbolos del capitalismo. La marcha ha transcurrido en un ambiente de unidad con la participación de todo tipo de organizaciones sociales.

Según **Jonas Pentzien**, miembro de la plataforma **Blockupy** y de la red de organizaciones sociales en contra de la crisis en la UE, la represión no se ha hecho esperar, ya que hay más de **500 detenidos y más de 130 heridos. En total cerca de un 5% de los manifestantes de la mañana han sido detenidos.**

Para el activista este ha sido un paso importante para el **rechazo de las políticas neoliberales y en solidaridad con los países que las están padeciendo como Grecia**, también destaca la fuerte represión y vaticina una nueva oleada de protestas en toda Europa contra el BCE.
