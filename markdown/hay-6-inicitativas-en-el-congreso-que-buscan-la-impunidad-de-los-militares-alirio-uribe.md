Title: "Por lo menos 6 iniciativas en el Congreso buscan la impunidad de los militares" Alirio Uribe
Date: 2015-06-11 13:16
Category: DDHH, Entrevistas
Tags: alirio uribe, corte, cpi, DIH, fuero penal, paz, polo
Slug: hay-6-inicitativas-en-el-congreso-que-buscan-la-impunidad-de-los-militares-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Alirio-Camara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4627974_2_1.html?data=lZufmZ6beI6ZmKiakp2Jd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmqcbmjZuPrc%2FXytnO1s7apdSfxtOYxdrWt9Cf0trSjcfZt8TVz5DZw5DNsdHpz87Rw8mPqMafjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe, Representante a la Cámara por el POLO] 

#####  

###### [11 jun 2015] 

El miércoles 10 de junio la Cámara de Representantes aprobó la reforma del artículo 221 de la Constitución Política de Colombia, y una ley con más de 100 artículos que modifica el Código Penal Militar. Ambas amplían el fuero penal militar, según denunció el representante a la Cámara, Alirio Uribe, y aún habría en curso 6 iniciativas más promovidas por la bancada del Centro Democrático que buscarían dar impunidad a los militares.

La bancada del Polo Democrático anunció que demandará por inconstitucionalidad las dos normas aprobadas, en tanto la reforma del artículo 221 incluye que se adelanten investigaciones conforme al DIH, lo que, para el abogado y representante Alirio Uribe, "es absurdo porque el DIH no es un mecanismo de investigación, sino un derecho protectivo y prohibitivo", por lo que la norma es innecesaria e inconveniente, porque no puede interpretarse, y abre la puerta a un proyecto de ley en el que, incluyendo términos como "blanco legítimo, daño colateral y DIH", se aplicaría acciones militares no solo en el marco del conflicto armado interno sino en otro tipo de enfrentamientos, con BACRIM o bandas narcotraficantes, dando ventajas a las fuerzas militares sobre la población civil.

Por su parte, lo aprobado frente al Código Penal Militar, incluye la creación de un cuerpo técnico de investigaciones conformado únicamente por militares, transfiriendo funciones de policía judicial y dando principios de oportunidad para los militares. Son manifestaciones claramente inconstitucionales -aseguró Alirio Uribe-, ya que el artículo 250 de la Constitución niega esta posibilidad a los militares". La reforma, además, modificó tipos penales, como las chuzadas y el peculado, para que sean juzgados por tribunales militares.

Aunque el 80% de la reforma al Fuero Penal se cayó, incluyendo la competencia castrense en los crímenes contemplados por el DIH, una lista de exclusión de delitos y fechas límites para que la justicia civil investigue los casos; Sin embargo, para el representante es importante señalar que aún hay varios proyectos de ley promovidos por el Centro Democrático que apuntan a buscar la impunidad para los crímenes de Estado.

\[caption id="attachment\_9977" align="aligncenter" width="867"\][![Iniciativa del Centro Democrático](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Captura-de-pantalla-2015-06-09-a-las-10.06.24.png){.size-full .wp-image-9977 width="867" height="483"}](https://archivo.contagioradio.com/hay-6-inicitativas-en-el-congreso-que-buscan-la-impunidad-de-los-militares-alirio-uribe/captura-de-pantalla-2015-06-09-a-las-10-06-24/) Iniciativa del Centro Democrático\[/caption\]

"Estamos trabajando en función de que esto se caiga en la Corte", finalizó Alirio Uribe.

La bancada del Polo en Senado y Cámara presentó también una demanda ante la Corte, contra el Plan Nacional de Desarrollo aprobado para 2014-2018, por vicios de forma y fondo, que incluyen los artículos sobre Zonas de reserva minera, Planes de interés nacional estratégico, Derechos de las víctimas y páramos. Esperan presentar 5 demandas más contra el PND en los próximos días.
