Title: Empezó el guayabo de ellos
Date: 2018-03-17 13:02
Category: Camilo, Opinion
Tags: Iván Duque, Petro
Slug: 52243-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/elecciones-tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: hbs noticias] 

#### **Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** {#porcamilo-de-las-casas dir="auto"}

###### [15 mar 2018] {#mar-2018 dir="auto"}

El país es otro. Lo que se denomina el fenómeno Petro es más que él mismo. Es la expresión en lo electoral de una insatisfacción creciente. Y más allá, de lo electoral hay en muchos de esos electores, la manifestación callada de otro sector conciente de la sociedad nueva en crecimiento.

Si bien, los votos en la consulta llamada de derecha, en realidad la refrendación del proyecto de país autoritario por los borregos lógicos, los lobos vestidos de ovejas,  y el arribismo social es significativa, la sorpresa electoral es la expresión  de otras nuevas sensibilidades, en esos más de tres millones de votos.

El nuevo presidente será de derecha, ojalá me equivoque, lo más probable, la expresión de esa conciencia que teme a la libertad y que adopta los fantasmas del castro chavismo, la estatización, la inseguridad para perpetuar en la cotidianidad un miedo que justifique el ascenso social, la apariencia, el plástico.

El llamado centro se encuentra encerrado en sus propios egos, que temen a una construcción más social incluyente, a quiénes le es más fácil distanciarse de Petro, por sus propios celos y venganzas.

En realidad el miedo es a lo que representa a un amplio sector de negados y excluidos, a sectores medios e incluso a un sector del capital moderno. Un miedo distinto a la llamada derecha que quiere mantener el poder institucional con la maquinaria, la corrupción, imposibilitando la implementación reformista del Acuerdo de Paz con las FARC; que usara los errores del ELN para revivir el alma autoritaria de la sociedad y sostener la república bananera.

Se ha perdido una oportunidad electoral para lograr una ruptura histórica dentro de lo institucional. Sin embargo, sí hay madurez en las agrupaciones políticas llamadas así mismas alternativas, y se continúa sintiendo para transformar con los de a pie, lo que se expresa como inconformidad ira creciendo; sí hay interpretación de los votantes en blanco y sobre todo si se toma a los escépticos y distantes.

Una tarea titánica espera en esa construcción del poder conciente, en particular en llegar, cómo ya se ha llegado, a los que en marzo de 2008, salieron a marchar contra las FARC, atravesados por un imaginario de odio, de insensatez, y de amor ciego a un caudillo, que hoy está disminuido pues no logró el millón de votos que auguraba.

El país es otro y se está abriendo un nuevo escenario de construcción de lo nuevo, de todos nosotros depende, de nadie más. El fenómeno Petro es más que él mismo. Empezó el guayabo del poder tradicional, de nosotros depende que sea posible el cambio.

Decía Ghandi, la sustitución del sistema se hace instalando otro sistema. Es posible que estemos empezando que sigue en el escenario político, el de la JEP, el de la CEV, UBPD; los PDETs; Mesa de Quitos y nuestros asuntos sobre nuestra propia naturaleza, el ambiente.

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)
