Title: Aumentan las víctimas del terremoto en Ecuador
Date: 2016-04-18 17:11
Category: El mundo, Nacional
Tags: ecuador, Terremoto, víctimas
Slug: aumentan-las-victimas-del-terremoto-de-7-8-en-la-escala-de-richter-en-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/terremotoecuador-e1460996080402.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sumarium (Edición US)] 

###### [18 Abril 2016] 

Luego del terremoto ocurrido este sábado, entre las zonas ecuatorianas de Cojimíes y Pedernales sobre las 6:58 pm hora local, el número de víctimas mortales, heridos y desaparecidos sigue en asecenso. El último reporte registra un total de 413  muertos y 2.527 heridos, mientras que la cifra de damnificados aún no se ha establecido debido a que no se ha podido realizar un censo el total de daños.

Las provincias más afectadas por el sismo de 7,8 grados en la escala Richter son Porto Viejo, Manta y Pedernales que se encuentran completamente destruidas. La Cruz roja ecuatoriana informó que recibieron **300 pedidos de búsqueda de personas desaparecidas**, 12 de las cuales ya fueron encontradas.

De acuerdo con Jorge Glass, Vicepresidente del país, **la cifra de fallecidos y damnificados podría aumentar** y explicó que la situación es sumamente compleja, ya que hay zonas como Pedernales donde el ingreso de los equipos de rescate y asistencia han tenido dificultades para llegar.

El presidente Rafael Correa declaró **el estado de excepción en todo el país** y desplazo a 4.600 policías y 10.000 militares a los lugares de mayor devastación además la fuerza pública se ha encargado de controlar los supermercados para organizar el abastecimiento de agua y alimentos. Por otro lado, Correa hizo un llamado a la población en general para que mantengan la calma y procuren organizarse.

El vicepresidente expreso que solicitan ayuda internacional y que los implementos que más necesitan son plantas generadoras de luz, así como alimentos no perecederos, agua, carpas, bolsas para dormir y baños portátiles, mientras que el alcalde de Pedernales Gabriel Alcívar pidió colaboración con ataúdes para dar sepultura a las víctimas mortales.

La cancillería de Ecuador confirmo que dentro de las víctimas **fatales se encuentran 7 colombianos y 58 personas localizados en los diferentes refugios**. En  Bogotá se podrán realizar donaciones en la Embajada de Ecuador desde las 8:00am hasta 4:00pm a los damnificados del terremoto de Ecuador.

En Colombia la línea de atención para ubicar familiares en **Ecuador es 01800010410, en Bogotá el número habilitado es 3826999.**

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
