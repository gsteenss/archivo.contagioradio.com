Title: Estudiantes chilenos vuelven a las calles y convocan paro multisectorial
Date: 2016-06-09 15:38
Category: Educación, El mundo
Tags: Confech, Marcha estudiantes Chile, Movimiento estudiantil CHile
Slug: estudiantes-chilenos-vuelven-a-las-calles-y-convocan-paro-multisectorial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Marcha-Confech.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Uno ] 

###### [9 Junio 2016 ] 

Con las consignas "Chile se cansó" y "A cambiar la mala educación", organizaciones estudiantiles lideradas por la Confederación de Estudiantes de Chile Confech, convocaron para este jueves una movilización nacional y un paro de diversos sectores, para exigir al Gobierno el fortalecimiento de la educación pública, un marco de regulación que impida a las instituciones educativas seguir lucrándose, así como un sistema de financiamiento, en el que **la gratuidad cobije a toda la población y la educación se entendida como un derecho fundamental**.

Pese a que la fuerza pública solicitó la cancelación de la movilización, desde la Plaza Italia y hasta la calle Echaurren, colectivos estudiantiles, como la Coordinadora Nacional de Estudiantes Secundarios y la Coordinadora de Estudiantes Secundarios, **marcharon junto a otras 150 mil personas por la calzada sur de la Alameda en Santiago de Chile**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
