Title: Generación E: ¿El reencauche de Ser Pilo Paga?
Date: 2018-10-22 17:36
Author: AdminContagio
Category: Educación, Entrevistas
Tags: Atehortúa, Generación E, ser pilo paga, UNEES
Slug: generacion-e
Status: published

###### [Foto: Contagio Radio] 

###### [19 Oct 2018] 

Mientras los estudiantes participaban en el Encuentro Nacional por la Educación superior, en el que discutieron los pasos a seguir del movimiento estudiantil en su reclamación por mayores recursos para la educación superior pública, el presidente Iván Duque presentaba el programa **"Generación E" con el que buscan sustituir a Ser Pilo Paga.**

<iframe src="https://co.ivoox.com/es/player_ek_29522939_2_1.html?data=k56ilJedd5qhhpywj5aVaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncbfVzcrb1s7SpYyZpJiSmpbarc3VhpewjcnJsMbbwsnOjcnJb83VjLq7p6q3cYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Según Valentina Ávila, delegada de la Unión Nacional de Estudiantes por la Educación Superior (UNEES),** 'Generación E' es lo que esperaban: Un programa que financia la demanda, cuya inversión sería de 3,6 billones de pesos para los próximos 4 años pero que estaría focalizado en términos de meritocracia, lo que quiere decir, que el acceso a la educación superior estará limitado a ciertas personas.

Para Ávila, esto es una prueba de que no existe voluntad política para financiar la educación pública superior. Por otra parte, desde el punto de vista de los docentes, Jairo Torres, presidente del Sistema Universitario Estatal (SUE) se pronunció en un comunicado contra el nuevo programa. (Le puede interesar: ["Profesores a paro por 24 horas en defensa de la educación pública"](https://archivo.contagioradio.com/profesores-paro-por-24-horas/))

> COMUNICADO SUE  
> Consejo de Rectores del Sistema Universitario Estatal SUE. [pic.twitter.com/pG6508nyau](https://t.co/pG6508nyau)
>
> — Udenar (@UdenarDigital) [22 de octubre de 2018](https://twitter.com/UdenarDigital/status/1054456206054572032?ref_src=twsrc%5Etfw)

En el documento criticó que **los recursos económicos seguirán destinados para las universidades privadas**, se beneficiará a pocos estudiantes con condiciones económicas vulnerables, quienes además, deberán recurrir a este sistema de crédito condonable. En ese sentido se pronunció el ex rector de la Universidad Pedagógica Nacional (UPN) Alfonso Atehortúa, quien cuestionó recientemente si el programa 'Generación E', no se referiría a **"Generación de Endeudados".**

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **"La Huelga de Atehortúa es un acto inmenso de grandeza"** 

Ya completan 5 días sin recibir alimentos los cuatro profesores de universidades públicas que hace poco iniciaron una huelga de hambre para que el Gobierno inyecte recursos económicos a las Instituciones de Educación Superior (IES). Atehortúa, uno de los más reconocidos, señaló que aún confía en el corazón de la ministra de educación, María Victoria Angulo, para que establezca una mesa de diálogos con los estudiantes.

<iframe src="https://co.ivoox.com/es/player_ek_29522958_2_1.html?data=k56ilJedeZmhhpywj5WXaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncaLgx9Tb1dSPhdXZydTf1trFaZO3jMncxcrSuMaf2pDS2pLWqcTo0NeYxsqPsMKftrW7j4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Sin embargo, el docente manifestó que **hasta el momento no han tenido respuestas del Gobierno Nacional** que permitan llegar a acuerdos sobre las exigencias de los estudiantes. Por su parte, Ávila agregó que el Gobierno se ha reunido con algunas personas para tratar el tema, pero no ha hecho los llamados amplios a la negociación. (Le puede interesar: ["Profesores universitarios se suman a la defensa de la educación pública"](https://archivo.contagioradio.com/profesores-defensa-educacion-publica/))

La integrante de la UNEES se refirió a la huelga de hambre que adelanta el ex rector de la UPN y señaló que es un **"acto inmenso de grandeza"**, poner en peligro su salud para lograr mejoras en la educación superior pública, situación que este lunes llevó al docente a recibir atención médica. Siguiendo el ejemplo del profesor, estudiantes iniciarán una huelga de hambre y encadenatón colectivo en la Pedagógica este miércoles 24 de octubre.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fmargaract%2Fvideos%2F10213113365413108%2F&amp;show_text=0&amp;width=267" width="267" height="476" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Las jornadas de paro y movilización social por la educación **[**continúan**] 

La delegada de la UNEES aseguró que en el encuentro de emergencia que tuvo el movimiento en Bogotá durante este fin de semana se aclaró que **el paro nacional de estudiantes se levantará cuando el Gobierno cumpla con las exigencias** mediante una mesa de diálogo. (Le puede interesar: ["Las 10 exigencias del movimiento estudiantil al Gobierno Nacional"](https://archivo.contagioradio.com/exigencias-movimiento-estudiantil/))

Ávila aseguró que en algunas IES como la Universidad Nacional, de Magdalena y la de Antioquia se ha presentado una "represión institucional en contra de las dinámicas de resistencia estudiantil"; sin embargo, sostuvo que **los estudiantes seguirán su agenda de movilizaciones** en las que llamarán a la articulación de diferentes sectores, lo que incluye la participación en la marcha que se adelantará este martes por parte de FECODE.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
