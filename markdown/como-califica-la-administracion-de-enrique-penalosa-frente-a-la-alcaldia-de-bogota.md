Title: ¿Cómo califica la administración de Enrique Peñalosa?
Date: 2017-01-27 13:50
Category: Nacional, Otra Mirada
Tags: Bogotá, Enrique Peñalosa
Slug: como-califica-la-administracion-de-enrique-penalosa-frente-a-la-alcaldia-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Enrique-peñalosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 27 Ene 2017 

Salimos a las calles de Bogotá a preguntar a las personas cómo califican la administración de Enrique Peñalosa durante su primer año de gestión frente a la alcaldía de la ciudad.
