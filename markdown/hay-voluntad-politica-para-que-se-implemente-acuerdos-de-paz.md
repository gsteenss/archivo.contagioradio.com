Title: ¿Hay Voluntad política para qué se implementen Acuerdos de Paz? Danilo Rueda
Date: 2016-11-24 17:21
Category: Nacional, Política
Tags: #AcuerdosYA, Implementación de Acuerdos
Slug: hay-voluntad-politica-para-que-se-implemente-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/voluntad-política.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Nov 2016]

Una de las grandes preocupaciones en estos momentos en Colombia es la violencia que se ha intensificado en contra de líderes y defensores de derechos humanos del país. **Acciones que podrían cesar con la puesta en marcha de los nuevos acuerdos de paz que se firmaron en el Teatro Colón.**

Sin embargo para el analista y periodista Danilo Rueda, la firma de estos nuevos acuerdos se dan en medio de un panorama de incertidumbre, “más allá de los debates jurídicos y en el congreso  que se esperan hay una sensación de vacío, poca esperanzadora que deja pregunta en el ambiente”. Le puede interesar:["Estos son los retos de implementación de los Acuerdos Paz"](https://archivo.contagioradio.com/estos-son-los-retos-de-implementacion-del-acuerdo-de-paz/)

De acuerdo con Danilo Rueda, todavía existen amarres en el sector del gobierno que le impiden soltarse y ser más contundentes a la hora de construir paz para el país.  Otro de los elementos que analista señala que no pueden pasar desapercibidos es la realidad política desde donde habla Rodrigo Londoño y en donde es evidente que **prevalece un sector muy fuerte en la sociedad colombiana que se opone a escenario de construcción de paz conversada, que ejerce una gran presión sobre el gobierno** y que tendría un temor a la verdad, a la justicia y a una reforma agraria.

Contexto que deja espacio a la pregunta de **¿hasta qué punto hay una fortaleza al interior del ejecutivo para lograr que el congreso apruebe el conjunto de medidas que se requiere para la paz y su implementación?** Y más allá del tema legal qué voluntad política existe para qué esto sea una realidad.

Para finalizar el analista considera que el mensaje más positivo es el anuncio del presidente Santos frente al día D y la amnistía , ya que podría significar un mensaje de tranquilidad para la guerrilla y su tránsito a las zonas campamentarías, sino también para los militares que no están incursos en delitos de lesa humanidad o de guerra también saldría de la cárcel.

######  Reciba toda la información de Contagio Radio en [[su correo]
