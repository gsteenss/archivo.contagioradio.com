Title: Mapiripán a 20 años todavía recuerda y sobrevive
Date: 2017-08-13 08:00
Category: Nacional, Reportajes
Tags: conmemoración, mapiripan, memoria
Slug: mapiripan-a-20-anos-todavia-recuerda-y-sobrevive
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Conmemoracion-Mapiripan-5-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Agosto 2017 

#### **[Por Carol Sanchez]** 

Hay lugares que no saben para lo que sirvieron y, aún así, cargan con la culpa. Está la carretera entre Villavicencio y Mapiripán, esas 16 horas que muchas víctimas de la masacre no habían querido recorrer de vuelta por miedo a los recuerdos; también el Río Guaviare, que sirvió de entrada a los paramilitares de la Autodefensas Campesinas de Córdoba y Urabá (ACCU) ese 14 de julio de 1997 y es el reposo de nadie sabe cuántos muertos; y el antiguo matadero, que ahora es un espacio abandonado que nadie quiere visitar, que evoca a los que allí fueron desmembrados durante esa semana.

Mapiripán recuerda, después de 20 años todavía recuerda y sobrevive, como puede, a la desidia estatal que siempre ha caminado sus calles.

> #### [*¿Valientes? Sí, somos valientes, pero es porque después de todo es lo único que nos queda.*] 

Mientras un grupo de paramilitares se quedó en el casco urbano del municipio, otro se desplazó hasta La Cooperativa, una de las veredas, para avanzar en su camino de muerte. Allí permanecieron 15 días y asesinaron a un número indeterminado de personas. Gloria Aragón, que durante ese tiempo tuvo que enterrar a varios de sus amigos, recuerda la zozobra y el desespero: los estaban matando y nadie hacía nada.

-   #### [*Nosotros no tuvimos quién nos auxiliara. A nosotros no nos dieron una voz de aliento, no tuvimos nada. Nada.*] {#nosotros-no-tuvimos-quién-nos-auxiliara.-a-nosotros-no-nos-dieron-una-voz-de-aliento-no-tuvimos-nada.-nada. dir="ltr"}

Volver a caminar estas calles, después de haberlas abandonado un día con lo que se tenía puesto es un acto de dignidad, valentía y resistencia. Y eso fue lo que hicieron las víctimas que el 16 de julio de 2017 se atrevieron a visitar Mapiripán de nuevo, a pesar del dolor, porque los 20 años no han borrado los recuerdos.

![Conmemoracion Mapiripan 14](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Conmemoracion-Mapiripan-14-.jpg){.alignnone .size-full .wp-image-45143 width="800" height="533"}

En Mapiripán muchas cosas no han cambiado. Los líos de tierras están más presentes que nunca y ellos, los que se fueron, ven difícil que su vuelta sea más que una visita de conmemoración. Del retorno soñado y la reparación -del volver al estado de cosas- no hay sino promesas.

Las sabanas infinitas en el paisaje avisan inevitablemente que se está en los Llanos Orientales, hay una especie de calma que sólo puede ser producto del desconocimiento, del no saber lo que estos terrenos han soportado. Varios de los que se fueron y de los que se quedaron tienen algo en común: creen que Mapiripán fue masacrada y desplazada porque había intereses económicos en sus tierras, porque para hacerlas agroindustrialmente productivas y explotarlas había que sacar a los campesinos de este pueblo.

11 años después de la masacre llegó Poligrow, una multinacional italo-española que se ha dedicado a sembrar miles de hectáreas de palma en la región. Para algunos, como Mónica Espinosa Valencia, delegada del comité departamental de víctimas de Mapiripán, Poligrow ha permitido que el municipio salga adelante. Pero esta visión no es compartida. A pesar de que los cultivos de palma son el sustento económico de muchas familias en la región, algunos son conscientes de que, en el futuro, serán perjudiciales.

![Conmemoracion Mapiripan 7](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Conmemoracion-Mapiripan-7-.jpg){.alignnone .size-full .wp-image-45136 width="800" height="533"}

#### [*A largo plazo esto nos va a traer muchos perjuicios, de pronto ya no a nosotros, a los nietos o a los bisnietos de nosotros. En el momento en que esta palmera se caiga o se vaya, el municipio va a caer en un caos monetario muy bajo. Y sabemos, sabemos que el daño dentro de 20 o 25 años está hecho.*] 

Y cuando Ramiro Ferrerosa habla del daño no se refiere sólo al económico, también piensa en el ambiental: en la cantidad de agua que requiere la palma, en lo que le hace a la tierra. Según un informe realizado por SOMO e Indepaz en el 2015, los monocultivos de palma en Mapiripán amenazan la biodiversidad y contaminan ecosistemas como morichales, sabanas y cuerpos de agua. Entonces pueda que sea cierto, que en 20 años o 25 años, incluso menos, el daño sea irreversible.

Ahora, ¿cuál es el beneficio que ha recibido Mapiripán con la presencia empresarial? No es tan claro, el municipio sigue con las mismas calles sin pavimentar hace años y la cobertura de acueducto, es decir de agua potable, es de tan solo el 1.2%. Para llegar aún toca recorrer horas de trocha que se hacen imposibles si hay un poco de lluvia, incluso puede que estén peor, pues el constante paso de tractomulas de la palmicultora deja inmensos huecos que dificultan más el paso.

Dice Ramiro que si se le pregunta por regalías no sabría qué decir, porque ni sabe en dónde están invertidas ni cuántas son.

#### [*Y si las da, ¿dónde las han invertido? Porque yo no las veo. En los cinco años que llevo aquí no sé cuáles son los beneficios. Han arreglado los puentes y los pasos pero porque necesitan que pasen sus camiones, y por ahí por fe también pasamos nosotros.*] 

Tal vez a esos pasos sean a los que se refiere poligrow cuando afirma que han invertido en “más de 150 km de vías terciarias y 7 km de casco urbano”. Como dice Ramiro, lo han hecho, seguramente, porque les toca. De hecho, de los \$1.923.236.547 millones destinados para inversión social entre 2010 y 2014, el 41.2%, casi la mitad, se han gastado en “gastos de administración”, según la misma multinacional.

Pero los problemas ambientales y de inversión no son los únicos que enfrenta la multinacional en Mapiripán. En el 2015, el entonces Incoder (Instituto Colombiano de Desarrollo Rural) determinó que Poligrow adquirió 5.680 hectáreas de tierras baldías violando la Ley 160 de 1994. ¿Por qué? En esta zona del país la Unidad Agrícola Familiar es de máximo 1.800 hectáreas, es decir que es lo máximo que una familia o empresa puede tener en baldíos. En este caso, Poligrow adquirió 3.880 hectáreas más de las permitidas y las llenó de palma.

La idea de esta empresa italo-española es tener 15.000 hectáreas de palma sembradas en esta región. Una idea: esto corresponde a más o menos 15.000 canchas de fútbol profesional.

Teniendo en cuenta que Mapiripán ha sido epicentro de la violencia y que desde 1985 han sido desplazadas, según el Registro Único de Víctimas, 24.639 personas, resulta importante analizar la relación entre los cambios del uso de la tierra y las cifras anuales de desplazamiento.

![Conmemoracion Mapiripan 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Conmemoracion-Mapiripan-4-.jpg){.alignnone .size-full .wp-image-45133 width="800" height="533"}

En 1997, año de la masacre, 1.621 personas salieron de Mapiripán. En el 2002, cuando las AUC consolidaron su presencia en el territorio, se dio un éxodo masivo y 4.895 personas salieron del municipio. Según el portal VerdadAbierta.com, Teodosio Pabón alias 'El Profe', confesó en una entrevista que en ese año los Castaño “habían diseñado un plan para convertirse en grandes productores de palma de la región. Según Pabón, tenían ya montado un vivero con suficiente plántulas de palma para cubrir cinco mil hectáreas”.

La cifra de desplazamiento empezó a bajar hasta el 2005, cuando llegó a 855 personas. Sin embargo, en el 2006, dos años antes de que Poligrow llegara, empezó a subir estrepitosamente. Ese año se registraron 2.186 personas desplazadas, el siguiente 1.359. Para el 2008, cuando la multinacional ya operaba en Mapiripán, 2.569 personas dejaron sus tierras.

Desde entonces la cifra ha ido bajando, tanto que en el 2016 sólo se registaron 71 personas desplazadas. Sin embargo, cada vez hay menos tierra para los campesinos y más para la palma. Basta con recorrer estos caminos y estas sabanas para darse cuenta de que el paisaje particular de los Llanos, plano y con poca vegetación, ha sido reemplazado por pequeñas palmeras que parecen salir despeinadas del suelo.

Si los mapiripenses creen que el conflicto y las violaciones que han tenido que sufrir se han debido a intereses sobre sus tierras, estas cifras de desplazamiento podrían confirmarlo. Las inmensas extensiones de tierra son la bendición y el castigo de los Llanos Orientales.

\*\*\*

A Mapiripán la masacraron y desplazaron los ‘paras’, sus pobladores vieron de cerca lo que es la sevicia humana y los que se fueron hoy luchan porque sus tierras sean para ellos y no para las empresas. Colombia es vivir resistiendo, Mapiripán lo ha sabido hacer.

![Conmemoracion Mapiripan 8](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Conmemoracion-Mapiripan-8-.jpg){.alignnone .size-full .wp-image-45137 width="800" height="533"}

¿Qué piden 20 años después? “Que el Estado reconozca que la masacre sucedió en Mapiripán, que hay unas víctimas que están esperando una verdad y el regreso a su territorio con garantías, con inclusión social, inversión en proyectos para las comunidades, mejoras al sistema de salud y a las vías”.

Saben, lo saben, que la reparación económica no es suficiente, que el dinero no resarce el daño que se hizo. Entonces se enfocan en hacer de Mapiripán un territorio de paz, uno que garantice que su pueblo nunca volverá a ser arrasado por la violencia.

Desde la memoria pretenden lograr justicia y, sobre todo, verdad. La consigna es que el Estado debe aclarar cómo se dieron los hechos de la masacre, cuál fue su participación y por qué permitió que sucediera.

El trabajo que tiene la Agencia Nacional de Restitución de Tierras es, además, fundamental en esta construcción de paz. Reconocer que las personas que se fueron tienen derecho sobre las propiedades que dejaron y devolverles sus proyectos de vida es el primer paso para logar el país del posconflicto.

A Mapiripán intentaron destruirla en cinco días, y resistió, conservó la fuerza de sus sabanas, encontró consuelo en su río y hoy le hace frente a lo que insiste en dejarla estancada. Responde con memoria al abandono y grita, con todas sus fuerzas “Ante el olvido y la impunidad: memoria con dignidad”.

\
