Title: Defensores de derechos humanos "Contra las cuerdas"
Date: 2017-02-21 16:20
Category: DDHH, Entrevistas
Tags: Asesintos, Lideres, paz, Somos defensores, víctimas
Slug: disminuyeron-las-amenazas-pero-aumentaron-los-asesinatos-somos-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Feb. 2017] 

**Los homicidios contra defensores de DD.HH incrementaron en Colombia alcanzando 80 casos registrados durante el 2016**, es decir un 22% más que en el 2015, siendo **Cauca el departamento que ocupa el primer lugar en cifras**, así lo asegura el reciente informe publicado por Somos Defensores y que además da cuenta de la inoperancia de la Fiscalía en materia de avances en investigaciones.

**Durante el 2016 Somos Defensores encontró que fueron 481 personas agredidas**, cifra que disminuyó en un 29% respecto al 2015 “en lo que respecta a amenazas o panfletos”, infortunadamente aumentó el número de homicidios con un total de 80 casos, todos en la impunidad.

**“El 42% de esos 80 homicidios reportados se registraron en el sur-occidente, especialmente en el Cauca con 22 homicidios**, lo que es muy alarmante sobre todo porque es un departamento altamente militarizado y ahí nos preguntamos ¿qué es lo que pasa con la fuerza pública?” relató Diana Sánchez, vocera de Somos Defensores.

**En todos los departamentos del país se registró por lo menos un homicidio de activistas de derechos humanos**, donde la región más complicada es sur-occidente que comprende al Valle del Cauca, Cauca, Nariño y Putumayo. Le puede interesar: [Continúan las amenazas a líderes en el Cauca](https://archivo.contagioradio.com/continuan-las-amenazas-a-lideres-en-el-cauca/)

Según Sánchez “esa cifra es muy preocupante porque quiere decir que la afectación que es la más grave, que es el derecho a la vida y a la integridad física, sigue muy vulnerado en Colombia para los activistas de derechos humanos”.

**Luego de Cauca se encuentra Antioquia con un reporte de 10 casos de homicidios,** lo que según Somos Defensores, puede ser un subregistro en la medida en que esta organización no alcanzó a documentar y verificar todos los casos de asesinatos contra líderes.

Otras zonas afectadas por el tema de los homicidios son **Norte de Santander con 6 personas, Valle del Cauca 5, Nariño 5, Córdoba 4, Huila 3, Arauca 2,** entre otros. Le puede interesar: [Asesinan a Yoryanis Isabel Bernal, lideresa indígena Wiwa](https://archivo.contagioradio.com/asesinan-yoryanis-isabel-bernal-lideresa-indigena-wiwa/)

Somos Defensores añade que **los paramilitares y la delincuencia común, son los mayores autores de los asesinatos de los líderes sociales y defensores de derechos humanos** en el país.

Dice Sánchez que es lamentable tener que dar este reporte en la medida que “uno esperaría que como nos encontramos en un contexto de conversaciones, de negociaciones entre el Gobierno y las insurgencias, mejoraría la situación de defensores y defensoras de derechos humanos, pero encontramos que no”.

### **Proceso de paz y ataques a Defensores de DD.HH.** 

Para esta organización la realidad que se está viviendo en Colombia con el tema del paramilitarismo no es nueva “creemos que no es que las FARC hayan salido de las zonas y automáticamente hayan llegado otros actores a controlar el territorio y a matar gente, porque los homicidios ya se venían dando” dijo Sánchez. Le puede interesar: [Asesinatos de líderes sociales son práctica sistemática: Somos Defensores](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/)

Lo que si cree Somos Defensores, es que existen **muchos homicidios asociados al contexto del 2016 especialmente con el proceso de paz entre el Gobierno y las FARC** “hay actores que atacan a todo el movimiento social en la medida en que ellos están jalonando el proceso de paz” aseveró Sánchez.

### **La Fiscalía no hace bien la tarea** 

Esa fue otra de las conclusiones de este informe en el que aseguran que la **Fiscalía cuenta con todas las herramientas**, posibilidades humanas y técnicas **para realizar las investigaciones en los casos, pero no lo han hecho.**

“La Fiscalía no se ha dado a la tarea, que es lo que le corresponde, de investigar a fondo quién está detrás de los homicidios. El Fiscal dice que ya se han investigado casos, que ninguno tiene que ver con otro, pero nosotros no compartimos esa tesis y tampoco conocemos esos avances” recalcó Sánchez. Le puede interesar: [Fiscalía niega asesinatos sistemáticos contra líderes y defensores de DDHH](https://archivo.contagioradio.com/fiscalia-niega-asesinatos-sistematicos-en-territorios/)

Y es que hasta el momento **de los 80 casos,** cifra que Somos Defensores cree es un subregistro, **no se conoce quiénes son los responsables, no se saben si hay detenidos, ni quiénes son los autores intelectuales. **

### **La Fuerza Pública tampoco hace su tarea** 

Para Somos Defensores, preocupa la omisión que puede estar haciendo la Policía y el Ejército en los territorios porque eso coincide con que “en **donde las personas están siendo asesinadas son los territorios con buena presencia militar y de Policía”.**

Finalmente, Somos Defensores hace un llamado para que de manera urgente se den resultados en materia de investigación, pero adicionalmente en sanciones contundentes a los responsables.** **

Otras organizaciones como Indepaz, han asegurado que cuentan con un registro mucho mayor, según ellos, son cerca de 117 asesinatos a defensores de derechos humanos durante 2016. Le puede interesar: [117 líderes fueron asesinados durante 2016](https://archivo.contagioradio.com/117-lideres-fueron-asesinados-durante-2016/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
