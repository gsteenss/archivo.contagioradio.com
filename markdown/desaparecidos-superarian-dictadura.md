Title: Desaparecidos en Colombia superarían 4 veces los de la dictadura Argentina
Date: 2019-02-15 16:18
Author: AdminContagio
Category: DDHH, Paz
Tags: CIDH, personas desaparecidas, Unidad de Búsqueda de Personas Desparecidas
Slug: desaparecidos-superarian-dictadura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/escombrera-contagioradio-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [Feb 15 2019] 

Según cifras del Centro Nacional de Memoria Histórica en Colombia existe un registro de 83 mil víctimas de desaparición forzada, mientras que organizaciones sociales y de derechos humanos advierten que contando los casos no registrados podrían ser entre 90 y 120 mil, razón por la cual este viernes acudieron ante la Comisión Interamericana de Derechos Humanos (CIDH) para pedir que se tomen medidas urgentes.

De acuerdo a la información de las 18 organizaciones que participaron en la audiencia, Colombia podría superar entre 3 y 4 veces el número de personas desaparecidas durante la dictadura de Argentina, calculadas en 30 mil hombres y mujeres; por lo que se hace necesario reconocer la importancia de insistir en su búsqueda, preservar los espacios en los que se cree que están sus cuerpos óseos, e incluir a las familias en el proceso.

### **Es imperativo que se busque a los desaparecidos** 

El primer llamado que hicieron las organizaciones fue reconocer la necesidad de que el Estado haga una búsqueda efectiva de las personas dadas por desaparecidas, lo que implicaría una mayor asignación presupuestal para investigar y desarrollar las operaciones necesarias, al tiempo que apoyar la labor de la Unidad de Búsqueda de Personas Dadas por Desaparecidas (UBPD). (Le puede interesar: ["La Unidad de Búsqueda, una oportunidad para los familiares ausentes"](https://archivo.contagioradio.com/la-unidad-de-busqueda-una-oportunidad-para-los-familiares-de-desaparecidos/))

### **Se deben proteger fosas y cementerios ** 

Según el Ministerio de Interior, hay cerca de 26 mil cuerpos sin identificar en 426 cementerios, no obstante, la preservación que se hace de estos espacios no es la adecuada y puede terminar afectando el proceso de su búsqueda. La situación se hace más difícil en las llamadas fosas comunes, o sitios irregulares en los que se enterraron algunos desaparecidos, como el caso de La escombrera en Medellín, Antioquia. (Le puede interesar:["Faltas en búsqueda de ausentes de Comuna 13 se presentan ante la CIDH"](https://archivo.contagioradio.com/fallas-en-busqueda-de-desaparecidos-de-comuna-13-se-presentan-ante-la-cidh/))

### **Incluir a víctimas y familiares** 

Por último, las organizaciones destacaron la importancia de incluir a las víctimas y familiares en la búsqueda de los desaparecidos, así como respetar los procedimientos en el momento de la investigación, exhumación y entrega de los cuerpos óseos; "en algunas entregas no se permite a las familias participar en la preparación de las ceremonias ni conocer de qué forma las entidades identificaron el cuerpo", detallaron las instituciones.

**Las respuestas del Estado**

Por su parte, el Estado colombiano presentó las fases que cumplen las instituciones encargadas de buscar a las personas desaparecidas, en un plan de 4 fases que fue explicado por la Fiscalía como ente director de Medicina Legal, Institución encargada de desarrollar parte de este proceso. No obstante, entre las organizaciones civiles y las propias comisionadas que participaron de la audiencia se hicieron evidentes las preguntas sobre la eficacia del proceso, la articulación entre la UBPD y Medicina Legal, así sobre cómo se están tratando los casos de desaparición que tienen lugar después de la firma del Acuerdo de Paz.

> La [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) presenta en [\#ColombiaEnLaCIDH](https://twitter.com/hashtag/ColombiaEnLaCIDH?src=hash&ref_src=twsrc%5Etfw) las fases del plan nacional de búsqueda de desaparecidos.
>
> Sin embargo, las entidades estatales no actúan de manera articulada. Se necesitarían cerca de 100 años para identificar los cuerpos de las víctima de desaparición forzada. [pic.twitter.com/jn2yUjDzGd](https://t.co/jn2yUjDzGd)
>
> — Colectivo OFB (@CsOFB) [15 de febrero de 2019](https://twitter.com/CsOFB/status/1096421484178882561?ref_src=twsrc%5Etfw)

Por último, las organizaciones señalaron su desconcierto ante el Estado de Colombia, porque a la audiencia no fue invitada la Unidad de Búsqueda, situación que deja en entredicho el reconocimiento que hace el actual Gobierno de la misión que tiene esta Institución. (Le puede interesar:["Más de 83.000 personas desaparecidas y 26.000 cuerpos sin identificar en los cementerios de Colombia"](https://www.colectivodeabogados.org/IMG/pdf/comunicado_de_prensa_audiencia_cidh.pdf))

> Nos llama la atención que [\#ColombiaEnLaCIDH](https://twitter.com/hashtag/ColombiaEnLaCIDH?src=hash&ref_src=twsrc%5Etfw), el Estado no incluyó o llamó a la [@UBPDBusqueda](https://twitter.com/UBPDBusqueda?ref_src=twsrc%5Etfw) para la audiencia sobre desaparición forzada. No queremos leer esta acción como que el Estado no le está dando importancia al rol de la Unidad. [pic.twitter.com/KFr65KVuad](https://t.co/KFr65KVuad)
>
> — Colectivo OFB (@CsOFB) [15 de febrero de 2019](https://twitter.com/CsOFB/status/1096425339092381701?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### ** ** 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
