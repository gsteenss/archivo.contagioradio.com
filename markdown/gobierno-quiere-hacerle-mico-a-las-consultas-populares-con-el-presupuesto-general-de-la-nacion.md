Title: Gobierno quiere hacerle mico a consultas populares con el presupuesto general de la Nación
Date: 2018-10-03 12:34
Author: AdminContagio
Category: Ambiente, DDHH
Tags: consultas populares, Gobierno Duque, Presupuesto General de la Nación
Slug: gobierno-quiere-hacerle-mico-a-las-consultas-populares-con-el-presupuesto-general-de-la-nacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Accesos] 

###### [03 Octu 2018] 

La Misión de Observación Electoral (MOE), denunció un posible 'mico' que se habría metido a última hora en el presupuesto general de la Nación. Se trata de un artículo que dictaminaría que las consultas populares serán financiadas por los municipios y departamentos y no por el Gobierno y la Registraduría, **hecho que para la MOE, podría acabar con este mecanismo democrático.**

Felipe Jiménez, integrante de la MOE, aseguró que el Gobierno junto con algunos congresistas, se pusieron de acuerdo para meter un artículo que afirma que, "de ahora en adelante, las consultas populares departamentales y municipales, deberán ser costeadas y financiadas por los municipios y departamentos", **provocando que la gran mayoría de consultas no se puedan realizar,** porque los entes territoriales no tendrían ni los recursos, ni los incentivos para costear estos mecanismos.

Además, Jimenéz expresó que este artículo, derrumbaría un proceso democrático, debido a que la puesta en marcha de las **consultas populares se vería supeditada a los intereses de las alcaldías y si estas no quisieran hacerlas**, podrían excusarse en la falta de recursos para no llevarlas acabo.

### **El conejo a la participación social** 

Para Jimenéz, que este artículo se haya metido a última hora en el Presupuesto General de la Nación (PGN) genera **"mala espina"** y puede interpretarse como una oportunidad para hacerle "conejo a la participación ciudadana".

En esa medida, aseveró que la democracia tiene un costo y hay que asumirlo, "porque de lo contrario se estaría limitando la participación y haciendo caso omiso a los deberes y derechos que están consagrados en la Constitución". (Le puede interesar: ["¿Por qué las Consultas Populares están en riesgo?"](https://archivo.contagioradio.com/consultas-populares-riesgo/))

Asimismo, recordó que el PGN será prontamente votado en plenaria de Senado y Cámara, razón por la cual organizaciones sociales deberán hacer un gran trabajo de **incidencia para que se retire este artículo que sería perjudicial para la democracia del país.**

### **¿Cómo financiar consultas populares, economizando costos?** 

La MOE manifestó que el principal argumento de los congresistas y del gobierno para colocar este artículo es que no se sabe cuánto presupuesto se necesita en Colombia para financiar las consultas, porque no se puede establecer cuántas se realizarán.

Sin embargo, Jiménez sostuvo que si se generaran fechas específicas para realizar las consultas, es decir, **un día semestralmente en que se convoquen las consultas aprobadas,** se podría ahorrar costos y establecer un presupuesto mucho más concreto para este mecanismo. (Le puede interesar:["¿Qué pasará con las consultas populares en Colombia?"](https://archivo.contagioradio.com/que-pasara-con-las-consultas-populares-en-colombia/))

<iframe id="audio_29075779" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29075779_4_1.html?c1=ff6600"></iframe>  

######  Reciba toda la información de Contagio Radio en [[su correo]
