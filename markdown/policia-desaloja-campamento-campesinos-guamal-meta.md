Title: Policía desaloja campamento de campesinos en Guamal Meta
Date: 2017-02-24 15:10
Category: DDHH, Nacional
Tags: Abuso de fuerza ESMAD, Desalojo del ESMAD, Guamal Meta
Slug: policia-desaloja-campamento-campesinos-guamal-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/esmad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vanguardia] 

###### [24 Feb 2017] 

A través de un comunicado, el comité por la Defensa de la Vida y el Agua de la vereda Pio XII en Guamal Meta, informó que "policías con cascos, escudos y bolillos"** ingresaron de forma violenta sobre las 4:00am al campamento instalado el 9 de febrero**, en el que las familias de la zona protestan contra el proyecto petrolero CPO-9 y la construcción del pozo Trogon, por parte de ECOPETROL.

Edgar Humberto Cruz uno de los líderes comunitarios de Guamal, señaló que “abusando de su fuerza y poder” la policía de manera violenta “**empujando a uno de los ciudadanos y destruyendo un celular con el fin de que no fueran grabadas las actuaciones”,** sacaron a todas las familias “violando el derecho a la protesta pacífica de la comunidad”.

El líder comenta que a pesar de las reiteradas ocasiones que la comunidad ha llamado al diálogo a Ecopetrol y a las instituciones estatales, **no ha sido posible que las exigencias de los y las habitantes “sean tenidas en cuenta en decisiones** que den cuenta de garantías reales de los derechos de la comunidad”. ([Le puede interesar: Sigue firme la defensa del río Humadea en Guamal](https://archivo.contagioradio.com/sigue-firme-la-defensa-del-rio-humadea-en-guamal/))

Señaló además que los uniformados que realizaron la operación no se encontraban plenamente identificados, “violando flagrantemente los procedimientos establecidos para esta clase de acciones” y denunció que **la personería municipal se ha negado públicamente a realizar sus funciones de orden legal y constitucional a “garantizar el derecho** que tienen las comunidades a la protesta pacífica”, argumentando que cumple un rol imparcial bajo un precepto netamente personal.

En la misiva, solicitan a los organismos internacionales defensores de derechos humanos a la comunidad nacional, que “intervengan y exijan una pronta y real solución a la situación que se presenta en el departamento del Meta” **como consecuencia de proyectos petroleros y exigen al Gobierno Nacional, dar respuestas efectivas y su presencia en el territorio** para entablar un diálogo entre las empresas, autoridades y la comunidad. ([Le puede interesar: Proyecto petrolero acabaría con el río Humadea en el Meta](https://archivo.contagioradio.com/proyecto-petrolero-acabaria-con-el-rio-humadea-en-el-meta/))

Responsabilizan al Estado Colombiano, en cabeza de Juan Manuel Santos Calderón, al presidente de Ecopetrol Juan Carlos Echeverry y a la Oficina de Derechos Humanos de la Policía Nacional Seccional, por las violaciones a los derechos humanos **“sin perjuicio de los delitos contemplados en el código penal colombiano”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
