Title: Cinco casos por los que la JEP pone la lupa a crímenes en Urabá y Bajo Atrato Chocoano
Date: 2018-09-26 18:05
Author: AdminContagio
Category: Nacional
Tags: Bajo Atrato, Chocó, Génesis, paramilitares, septiembre negro
Slug: cinco-casos-los-la-jep-pone-la-lupa-crimenes-uraba-atrato-chocoano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Acaparamiento-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [26 Sep 2018] 

Este martes se dio a conocer que la JEP, mediante el caso 004 comenzará la etapa de esclarecimiento de los crímenes cometidos en el marco del conflicto armado que sucedieron entre el primero de enero de 1986 y el primero de diciembre de 2016, concretamente en los municipios del Turbo, Apartadó, Carepa, Chigorodó, Mutatá y Dabeiba, en el departamento de Antioquia; y El Carmen del Darién, Riosucio, Unguía y Acandí, en el departamento de Chocó.

Algunos de los casos más sonados y que seguramente harán parte del expediente:

**1.Operación Génesis:**

La Operación Génesis tuvo lugar en Riosucio, Bajo Atrato chocoano, acción militar que se ejecutó entre el 24 y 27 de febrero de 1997 y que se justificó bajo el argumento de capturar y dar de baja a miembros de las Fuerzas Armadas y Revolucionarias de Colombia (FARC). Simultáneamente, se llevó a cabo la “Operación Cacarica” con acciones conjuntas entre Ejercito y paramilitares. Oficialmente, se reconocen los hechos en 4 días, sin embargo, según confesiones de alias “El Alemán” la operación ocurrió  entre el 23 de febrero al 5 de marzo.

Los delitos cometidos son de lesa humanidad, porque no se garantizaron los derechos de integridad personal a las comunidades, particularmente afrodescendientes. Se cometieron asesinatos indiscriminados, desmembramientos, torturas, despojo de tierras y con ello desplazamiento forzado por ataque a la población y a los territorios con bombardeos aéreos.

Adicionalmente, se presentaron incumplimientos estatales por no garantizar asistencia humanitaria, y abandono ante condiciones de hacinamiento, falta de salud y posibilidades de retorno al territorio que vivieron durante 3 años de desplazamiento en lugares como el Coliseo de Turbo. Existe además violaciones al derecho de la propiedad colectiva de comunidades afrodescendientes. Aunque no se tienen cifras exactas, se calcula que, al rededor de 3500 personas fueron desplazadas y ante el proceso judicial fueron reconocidas como victimas 341 adultos y 234 menores.

La Corte Interamericana de Derechos Humanos, dicto sentencia el 20 de noviembre de 2013 adjudico responsabilidad al Estado Colombiano, encontrando que la operación fue ejecutada por las Fuerzas Especiales 1, Contraguerrillas 35, adscritos a la Brigada 17 del Ejercito Nacional al mando del entonces general Rito Alejo del Río Rojas. Los grupos paramilitares estuvieron al mando de Fredy Rendón Herrera, alias “El Alemán”, jefe del Bloque Elmer Cárdenas, de las Autodefensas Campesinas de Córdoba y Urabá. También se determinó el vínculo del excoronel Jorge Eliecer Plazas Acevedo.

**2. Masacre de Brisas:**

El 6 de octubre de 1996, paramilitares de las Autodefensas Campesinas de Córdoba y Urabá (ACCU), llegaron al corregimiento Brisas de las Madres, en Riosucio, Chocó, los paramilitares escogieron ocho personas que se dedicaban al comercio de madera y las reunieron para luego asesinarlos. Se tiene solo registro de cinco de ellos, lo nombres de tres de las víctimas no se pudieron obtener. Según las investigaciones la masacre fue ordenada por el paramilitar Fredy Rendón Herrera, alias “El Alemán”.

**3. Desplazamiento forzado de las comunidades del Curvaradó 'Septiembre Negro':**

En 1996 se inició la Operación 'Septiembre Negro' perpetrada por paramilitares y militares del departamento de Chocó y de Antioquia, la macabra incursión se dio en las Cuencas de Curvaradó, Jiguamiandó, en el municipio Carmen del Darién, Riosucio, Piedeguita, Mancilla, Larga Tumarado, Turbo y Dadeiba.

Ligia María Chaverra, matriarca, víctima de la operación y líder social ha afirmado que los crímenes se cometieron como estrategia de despojar el territorio por los monocultivos de palma de aceite.

La Comisión Intereclesial de Justicia y Paz ha registrado 143 asesinados y desapariciones forzadas, de las cuales, 136 son responsabilidad de militares y paramilitares y siete son de laguerrilla de las FARC. La Corte Interamericana de Derechos Humanos condeno al Estado colombiano, la operación fue diseñada por la Brigada 17 del Ejercito Nacional, sin embargo, no se ha adjudicado responsabilidad a actores armados concretos.

**4. Empresarios, palma de aceite y despojo de tierras:**

El Centro Nacional de Memoria Histórica (CNMH) ha indicado que al menos en cinco departamentos (Bolívar, Magdalena, Choco, Cesa, Nariño) existe una relación directa entre despojo de tierra y el cultivo de palma de aceite. Por medio del Plan Colombia se sustituyeron cultivos de uso ilícito por dicha producción, adicionalmente, dentro del ajuste del desarrollo de Colombia desde el 2008 la palma ha sido parte primordial de la economía por la producción de biocombustibles.

La región del Urabá, en el Bajo Atrato Chocoano, fue uno de los focos agroindustriales para la siembra de la Palma de aceite. Desde los años 90 la población fue objeto de masacres y operaciones militares que produjeron la convergencia de diversos intereses y actores armados que propicio el despojo de tierra y adelanto el saqueo, el aprovechamiento, uso y apropiación irregular de territorios afrodescendientes para acrecentar estos monocultivos. De esta forma es como se dio la llegada de empresas como Urapalma S.A, Palmas de Curvaradó S.A, Palmura S.A, Palmadó Ltda, Inversiones Agropalma &amp; Cia Ltda, Palmas S.A, Palmas de Bajirá e Inversiones Fregni Ochoa.

La industria palmicultora ha evidenciado los nexos de paraeconomía y parapolítica, no solo como una conveniencia en estos dos ámbitos sino además como una estrategia de poder y dominación territorial, que dejo un numero de victimas incalculable.

La Fiscalía pidió condena a 21 empresarios de Palma, que en 2005 se les dicto resolución de acusación, que son: Orlando Moreno Mora, Gabriel Jaime Sierra Moreno, Raúl Alberto Penagos, Sor Enid Ospina Rendón, Gabriel Segundo Fernández, Robín Manuel Calongue, Javier José Daza, Sor Teresa Gómez Álvarez, Hernán Iñigo de Jesús Gómez Hernández, Claudio Adolfo Fregni Ochoa, Jesús Ignacio Roldan, Remberto Manuel Álvarez, Katia Patricia Sánchez, José Miguel Ruiz Cossio, Mario León Villa Pacheco, Javier Morales Estrada, Mario Alberto Vélez Giraldo, Manuel Gregorio Denis Blandón, Jorge Luis Santo Ortega, Juan José Palacios y Dagoberto Antonio Montiel Mercado.

De este proyecto agroparamilitar se han judicializado a 14 de estos empresarios, por concierto para delinquir, desplazamiento forzado e invasión de áreas de especial importancia ecológica, las condenas se dictaron de la siguiente manera:

![Presentación sin título](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Presentación-sin-título-800x450.jpg){.alignnone .size-medium .wp-image-57078 width="800" height="450"}

**5. Operaciones de Chiquita Brands**

Chiquita Brands International es una de las 439 empresas que han participado en el conflicto armado colombiano, con violaciones graves a los derechos humanos, por medio de crímenes de guerra en la región del Urabá, esta empresa ha sido acusada de detención, tortura, narcotráfico, explotaciones, entre otros, se encuentra el financiamiento a grupos paramilitares y asesinatos perpetrados por los mismos.

En 2011, la Fiscalía pidió investigar los filiales de esta multinacional en Colombia en especial Banadex S.A, Banacol y la Compañía Frutera de Sevilla, por declaraciones de paramilitares en el proceso de Justicia y Paz, en donde, se revelo nexos de Chiquita con estos grupos armados. Como retribución, el paramilitarismo brindo seguridad y la acción de acabar y reprimir los sindicatos de la agroindustria del banano. Su participación en el conflicto se remonta hasta 1928, por lo que es difícil establecer un número exacto de víctimas.

[A partir de 2014 se ha dictado diversas sentencias por los vínculos con el paramilitarismo y de manera más reciente, en agosto de este año la Fiscalía ha llamado a  juicio a 13 exdirectivos como posibles autores responsables de concierto para delinquir agravado, estos se presentaran ante la justicia ordinaria y no ante la Jurisdicción Especial de Paz (JEP), los acusados son: Reinaldo Escobar de la Hoz, Luis German Cuartas, Víctor Buitrago Sandoval, Álvaro Acevedo Gonzales, Víctor Manuel Henríquez, Javier Ochoa Velásquez, Juan Diego Trujillo, Jorge Alberto Cadavid, Dorn Robert Wenninger, John Paul Olivio, Charles Dennis Keiser, Jose Luis Laverde, Fuad Alberto Giacoman.]

[A demás de los exdirectivos, durante el presente mes el senador Álvaro Uribe Vélez también será investigado por omisión, no seguimiento y control de las Convivir cuando era gobernador de Antioquia, la Fiscalía investigará a otros exgobernadores, exsuperintendentes y exalcaldes entre 1997 y 2004. Este ente gubernamental indagara en las licencias que los gobernadores otorgaron a las Convivir y los requisitos legales, puesto que tuvieron nexos con el paramilitarismo siendo intermediarias en pagos con Chiquita Brands y las AUC.]

[Se ha encontrado una conexión con el expresidente por medio de la Compañía Frutera de Sevilla una de las filiales de Chiquita, esta reseño un documento de un pago de 5.935 dólares a la campana de Álvaro Uribe cuando seria gobernador y que además fue quien más impulso y defendió las Convivir. Finalmente, la Fiscalía tendrá la última palabra, pero aún no se ha concretado nada.]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
