Title: Autoridades indígenas, satisfechas con la visita de Hollande
Date: 2017-01-24 13:42
Category: Nacional, Paz
Slug: autoridades-indigenas-satisfechas-la-visita-hollande
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Nasa-e1502385039236.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vice] 

###### [24 Ene 2017] 

Demoras en la adecuación de las Zonas Veredales Transitorias de Normalización –ZVTN–, **amenazas, hostigamientos y asesinatos a líderes sociales y presencia paramilitar, son algunas de las situaciones** que presentaron las autoridades indígenas, del resguardo de Caldono en el Cauca, ante los presidentes François Hollande y Juan Manuel Santos, durante su visita a la Zona Veredal ubicada en dicho municipio.

Oscar Dizu Gobernador del resguardo de Caldono, señaló que la visita del presidente francés fue una oportunidad para “conversar y denunciar” las situaciones que se han venido presentando en el territorio, violaciones a derechos humanos por parte de la fuerza pública y el registro, por parte de la Guardia Indígena, de presencia paramilitar en inmediaciones a la ZVTN, advirtieron que **“sabemos de la participación de fuerzas oscuras en estos días, y el Gobierno tiene que hacer algo”.**

El Gobernador, llamó la atención sobre la urgencia de activar los mecanismos de seguridad, estipulados en los acuerdos de La Habana, para garantizar no sólo la seguridad de los integrantes de las FARC **sino la tranquilidad y armonía de las comunidades que habitan ancestralmente el territorio.**

### Los compromisos de Hollande 

Respecto a ello, aseguró que tanto el presidente Santos como Hollande agendaron para el próximo 2 de febrero, una reunión con las Fuerzas Militares y las autoridades territoriales con el fin de **corroborar la información denunciada sobre presencia paramilitar** y el presidente francés **se comprometió a fortalecer la Comisión de Búsqueda de personas desaparecidas. **

Dizu explicó que aunque saben que se trata de un encuentro entre jefes de Estado, la exigencia de su participación en dicha reunión era fundamental “porque se trata de nuestro territorio”, puntualizó que el pueblo Nasa se ve afectado con el proceso de preagrupamiento y que a pesar de hacer públicas una serie de denuncias “no hemos visto resultado”.

Por otra parte, el Gobernador manifestó que **la Guardia Indígena “es un alimento fundamental del territorio” y seguirá jugando un papel crucial** en el apoyo y acompañamiento a los mecanismos de seguridad y verificación en las ZVTN.

### ¿Cómo avanza la Zona Veredal de Caldono? 

De acuerdo con el Gobernador, aspectos fundamentales para los avances en la adecuación del punto de agrupamiento no se han resuelto, **“creemos que el Gobierno Nacional ha incumplido en algunos compromisos,** **por ejemplo el tema de las comunicaciones** que es tan importante”.

Además, manifestó que condiciones de salubridad y alimentación son necesarias para que el 1 de febrero se haga efectivo el total agrupamiento de los miembros de las FARC y en ese sentido, la visita del jefe de Estado francés fue una ocasión para que “el Gobierno se vea más comprometido”.

Por ultimo, Oscar Dizu dijo que las autoridades indígenas del Cauca han estado desde el inicio comprometidas con la construcción de Paz pues “queremos que la guerra que ha vivido las comunidades del Cauca finalicen, queremos conversar con quienes nos han hecho daño”. También mencionó que es vital la reconciliación y el perdón y añadió que **“no podemos pensar en sólo 180 días, el trabajo es bastante largo,** **como autoridades indígenas estaremos trabajando en eso “.**

<iframe id="audio_16623645" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16623645_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
