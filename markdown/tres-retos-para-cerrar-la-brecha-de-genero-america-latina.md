Title: Tres retos para cerrar la brecha de género en América Latina
Date: 2019-07-24 15:50
Author: CtgAdm
Category: Entrevistas, Mujer
Tags: Brecha de género, genero, Humanas Colombia, mujeres
Slug: tres-retos-para-cerrar-la-brecha-de-genero-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG_0454.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/IMG_0424.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cortesía Humanas Colombia] 

Un reciente informe realizado por las organizaciones: Humanas Colombia, la Articulación Regional Feminista y el Fondo de mujeres del Sur, evidenció que tanto en Latinoamérica, como en Colombia podría gestarse un retroceso en  los derechos de las mujeres, debido a la falta de compromiso de los gobiernos por generar políticas públicas de transparencia incluyendo los avances teóricos y discursivos, y por la falta de definición de recursos públicos con destinación específica a políticas de género.

Según las investigadoras entre los retos y las características de las instituciones que cobijan a las mujeres en Latinoamérica, se encontró la falta de conceptos vitales como género e igualdad, que orientarían las políticas públicas; razón por la cual se recomendó "definir acertadamente los términos que emplean los colectivos y las instituciones en América Latina"; especialmente de México, Perú, Bolivia, Argentina, Chile y Colombia.

El informe manifiesta que “los marcos conceptuales de género e igualdad son relevantes, porque marcan la pauta sobre cómo entienden las instituciones sus objetivos y sus acciones", pues en cada país de la región tienen especificidades y son abordados desde enfoques particulares. En el caso de Colombia, la definición de género hace referencia a una construcción social de “qué es y cómo deben ser las mujeres y los hombres”.

### Los retos para disminuir la brecha de género 

Uno de los mayores retos que afrontan las instituciones encargadas de cerrar la brecha de género tiene que ver con la transparencia en información de las diferentes instituciones de los gobiernos latinoaméricanos. Esto debido a que se esperaría que cada una de las inversiones y gastos que se realicen con el erario público sean de conocimiento para toda la población.

Sin embargo, la investigación reveló que las instituciones y entidades que tienen como deber tratar los asuntos de género, no tienen toda la información cargada en sus plataformas o muchos de los datos no son de conocimiento público, lo que traduce que no hay mayor transparencia activa e imposibilita conocer qué pasa con el dinero que se tendría que ejecutar para la protección de los derechos de género.

Adicionalmente en la región, el funcionamiento de estas entidades tampoco es visible, razón por la cual es necesario recurrir a mecanismos de transparencia pasiva, como lo son los derechos de petición, para obtener respuestas incluso frente a su funcionamiento interno. (Le puede interesar: ["En las cárceles colombianas no existe un enfoque de género"](https://archivo.contagioradio.com/carceles-colombianas-enfoque-genero/))

### ¿Qué pasa con los presupuestos para el género en Latinoamérica? 

Según el informe, el  presupuesto es otra de las falencias que tienen las instituciones que luchan por la transversalidad en Latinoamérica, debido a que se desconoce la cantidad de  rubros y la forma en la que son administrados.

Además, en la región, como explicó Luz Piedad Caicedo,  subdirectora de Humanas Colombia, las personas que se encargan de los programas de género e igualdad no se encuentran altamente capacitadas para asumir cargos en estas instituciones, es decir que los recursos humanos dispuestos para estas instituciones son limitados.

Por esas razones, el informe concluye que  existen evidentemente restricciones para conocer los recursos materiales y humanos en Latinoamérica, lo que impide un funcionamiento adecuado de transversalidad  y debilita a los programas que trabajan en la protección de los derechos de género.

### Transparencia y transversalidad en Colombia 

Otra de las feministas y ponentes, Adriana Arboleda, Directora Humanas Colombia,  manifestó respecto a la violencia de género que en el país “el mecanismo de la transparencia juega un rol muy importante, pues es el derecho que tienen las feministas y las mujeres de solicitar la información”.

En Colombia, según Arboleda, es muy grave que la Alta Consejería Presidencial para la Equidad de la Mujer, en lugar de alcanzar un rango mayor como ministerio de la mujer, sea solamente una consejería. Hecho que implica que, en primer lugar, no cuente con autonomía para definir sus políticas y en segundo lugar, depende del gobierno de turno y sus visiones sobre género.

La Corporación Humanas también aseguró que solicitó a esta Alta Consejería información sobre su funcionamiento. No obstante el material entregado no es ni concreto ni específico, e incluso muchos datos tuvieron que ser descifrados por la organización.

### Los Acuerdos de Paz, una oportunidad para la igualdad 

Según Arboleda en el marco de las negociaciones se lograron más de cien medidas de género que tienen que ser aplicadas en el proceso de reincorporación y que darían paso a superar, en algunos contextos las brechas de desigualdades de género.

Lastimosamente, la directora de la Corporación Humanas expresó que “la implementación del enfoque de género de los acuerdos está 13%  por debajo del enfoque de paz, además el trabajo realizado en las mesas de negociación a lo largo de cinco años se ve diluido  porque se requiere de un compromiso activo y de presupuesto”.

Para la investigadora "también es determinante tener conciencia diferenciadora del impacto de conflicto para los hombres y las mujeres, pues no se puede medir de igual manera. (Le puede interesar: ["¿Cómo va la implementación del acuerdo de paz en temas de género?"](https://archivo.contagioradio.com/implementacion-acuerdo-de-paz-en-genero/))

Los retos de las instituciones que trabajan por la transversalidad en Latinoamerica, en definitiva son muchos, por ello el informe manifiesta que "la transparencia se convierte en una herramienta fundamental de la sociedad civil para hacer que los estados rindan cuentas". Para que los mecanismos sean efectivos es vital conocer qué y cómo lo hacen en las instituciones de cada país.

El informe también especifica la relevancia que tiene establecer las funciones de cada institución en la región, para afrontar la situación particular en  cada país, referente a  la brecha de género.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
