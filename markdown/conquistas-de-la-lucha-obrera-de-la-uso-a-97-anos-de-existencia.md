Title: Conquistas de la lucha obrera de la USO a 97 años de existencia
Date: 2020-02-10 19:20
Author: CtgAdm
Category: Entrevistas, Movilización
Tags: Ecopetrol, petroleo, sindicato, USO
Slug: conquistas-de-la-lucha-obrera-de-la-uso-a-97-anos-de-existencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/uso-97-años-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Foto: @usofrenteobrero {#foto-foto-usofrenteobrero .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/jairo-ramirez-a-97-anos-nacimiento-de_md_47665922_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Jairo Ramirez | Integrante del comité de derechos humanos y de paz de la USO

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Este lunes 10 de febrero la Unión Sindical Obrera (USO) de la industria del petróleo cumple 97 años desde su creación, tiempo en el que han acompañado la lucha de los trabajadores logrando hitos como la creación de Ecopetrol, o impulsando la transición de energías contaminantes a otras limpias para el uso de la ciudadanía.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Luego de casi 100 años, una vuelta al pasado en las condiciones laborales**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Jairo Ramírez, integrante del comité de derechos humanos y de paz de la USO afirmó que contrario a lo que se decía, la puesta en marcha del modelo neoliberal en Colombia significó una forma de recortar los derechos de los trabajadores a través de la flexibilización laboral, así como tercerizando la contratación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esta situación Ramírez agregó los recientes anuncios del Gobierno de impulsar la contratación por horas, eliminar las cajas de compensación familiar e impulsar la reforma pensional como una forma más de afectar el derecho al trabajo digno, y de regresar a las condiciones en que se encontraban los trabajadores cuando se [creó](http://www.usofrenteobrero.org/index.php/el-sindicato/historia) la USO.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Los cinco hitos de la USO**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El integrante de la USO resaltó como una victoria importante el que el sindicato haya contribuido, desde sus inicios, a construir una legislación laboral que estableciera el derecho de asociación en el país, es decir, la posibilidad de formar sindicatos. En ese mismo sentido, resaltó el aporte para reconocer el derecho a la huelga como una forma de acceder a otros derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro hito fue lograr la constitución de Ecopetrol en 1951, 28 años después de creada la USO, como una forma de avanzar en la nacionalización del Petróleo y sacar provecho a una industria que aún mueve millones de pesos en el país. (Le puede interesar: ["Ni Ecopetrol, ni contratistas atienden derrame de petróleo en cuerpo hídrico de Barrancabermeja"](https://archivo.contagioradio.com/ni-ecopetrol-ni-contratistas-atienden-derrame-de-petroleo-en-cuerpo-hidrico-de-barrancabermeja/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ramírez, como parte del Comité de DD.HH. y paz resaltó también como un hecho trascendente la firma del Acuerdo de Paz entre FARC y el Estado de Colombia, porque la USO "ha sido una de las organizaciones en el país que ha desarrollado una búsqueda de la solución política al conflicto", y que continúa insistiendo en esta salida también con el ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, resaltó la persistencia de quienes integran la Unión Sindical, tomando en cuenta que ha sido víctima en innumerables ocasiones de persecución, estigmatización y asesinatos debido a los planteamientos políticos que como organización ha tomado. (Le puede interesar:["Protesta pacífica de la USO se ve amenazada por el ESMAD"](https://archivo.contagioradio.com/protesta-pacifica-la-uso-se-ve-amenazada-esmad/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Trabajadores del petróleo pujan por la transición energética**

<!-- /wp:heading -->

<!-- wp:paragraph -->

A dos años de que la refinería de Barrancabermeja cumpla un siglo de operaciones, y conscientes de la necesidad de cambiar el modelo extractivo por las consecuencias ambientales que conlleva la USO ha llamado la atención para que se ponga fín a la explotación de recursos fósiles. En consecuencia, la USO plantea la necesidad de transformar a Ecopetrol de una empresa petrolera a la empresa energética nacional que lidere la transición a energías limpias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La USO ha llamado la atención sobre el hecho de que los recursos fósiles están llegando a su fin porque se agotan. Por eso hemos planteado que es necesario que el Estado haga un tránsito, y Ecopetrol se convierta de una empresa petrolera a la empresa energética nacional que lidere una transición hacia las energías limpias

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ramírez sostuvo que ese proceso debe ser de carácter nacional, y debe tener como orientación el beneficio de toda la población colombiana, "porque vemos que la transición energética se deja al capital transnacional", generando costos elevados para muchas poblaciones del país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
