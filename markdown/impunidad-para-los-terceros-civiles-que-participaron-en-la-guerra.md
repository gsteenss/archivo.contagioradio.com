Title: ¿Impunidad para los terceros civiles que participaron en la guerra?
Date: 2018-05-10 18:35
Category: DDHH, Política
Tags: acuerdo de paz, audiencias CIDH, JEP, participación de terceros civiles en la guerra, terceros civiles
Slug: impunidad-para-los-terceros-civiles-que-participaron-en-la-guerra
Status: published

###### [Foto: Contagio Radio] 

###### [10 May 2018] 

En el marco del periodo 168 de sesiones de la Comisión Interamericana de Derechos Humanos, se llevó a cabo la audiencia sobre la violación de los derechos humanos fruto de la participación de **terceros civiles en el conflicto armado**. Los integrantes de las organizaciones sociales denunciaron que lo acordado en el punto cinco de víctimas del Acuerdo de Paz se ha ido desmoronando y no hay garantías para que los terceros civiles que participen en la guerra, cuenten la verdad.

La Comisión Intereclesial de Justicia y Paz, enfatizó en que las partes involucradas en la creación del Acuerdo de Paz, habían incluido en el borrador del texto “a ex mandatarios como sujetos de la JEP”. Sin embargo, “semanas después **fueron excluidos** e infortunadamente, el Congreso y la Corte Constitucional se sumaron a esta serie de exclusiones agregando a los terceros determinantes y agentes estatales no armados”.

Además, la Corte Constitucional, en su examen de constitucionalidad del acto legislativo 01 de 2017, **“dejó a merced de su voluntariedad** y conveniencia el comparecer o no en la JEP”. La sociedad civil le recordó al Estado y a los comisionados que “los terceros determinantes participaron en estructuras criminales con roles precisos en los que definieron estratégicamente modelos económicos del uso del suelo y el subsuelo, entre otros”.

### **Justicia ordinaria no ha sido capaz de identificar las responsabilidades de los terceros** 

“Casi la totalidad de los presumibles responsables en esta cadena criminal **siguen en la impunidad**” afirmaron las organizaciones. Dijeron que la justicia ordinaria no ha servido como garantía para la superación de la impunidad en los casos en los cuales se ha pedido que se investigue a los terceros civiles. (Le puede interesar:"Ante renuncia de secretario de la JEP las víctimas de Estado insisten en proteger arhivos del DAS")<https://archivo.contagioradio.com/ante-renuncia-de-secretario-de-jep-las-victimas-de-estado-insisten-en-proteger-archivos-del-das/>

Frente a esto, enfatizaron que la justicia ordinaria ha actuado con omisión por lo que no ha sido posible identificar las actividades de terceros civiles “que utilizando una logística armada **hicieron parte de un plan criminal”.** Esto ha hecho que aumente los niveles de impunidad en casos como la violencia sexual, la tortura y los despojos de comunidades de sus territorios.

### **No hay garantías para asegurar la participación en a JEP de los terceros civiles** 

Teniendo esto en cuenta, recordaron que el sistema de justicia transicional que estableció el Acuerdo de Paz, permitía que todos los involucrados en la guerra aportaran verdad como mecanismo de reparación y garantías de no repetición. Sin embargo, criticaron que las modificaciones que se han hecho a la Justicia Especial para la Paz, pues "crean un ambiente de impunidad” e **incertidumbre para las víctimas.**

Por su parte, el Estado colombiano indicó que las decisiones que tomó la Corte Constitucional frente al sistema de justicia transicional, se hicieron teniendo en cuenta “el ejercicio propio de un sistema democrático”. Por lo tanto, manifestó que “en ningún caso, **la responsabilidad de terceros civiles**, por crímenes cometidos en el conflicto armado ha sido excluido o estará siquiera en riesgo de impunidad”. (Le puede interesar:["Víctimas de crímenes de Estado exigen celeridad en investigaciones de la JEP"](https://archivo.contagioradio.com/victimas_crimenes_estado_jep/))

Esto fue cuestionado por  las organizaciones de la sociedad civil en la medida en que argumentaron que “si los terceros **no han participado en la justicia ordinaria** y gozan de total impunidad, tampoco lo harán de manera voluntaria en la Jurisdicción Especial de Paz”. Recordaron que la institucionalidad en Colombia no ha podido “responder a las compulsas de copias e investigaciones que han surgido de procesos de justicia transicional ya implementados” como lo fue Justicia y Paz.

Afirmaron que aún hoy “hay una oscuridad en los procesos de la participación de los terceros en la guerra en crimenes como el despojo” y que es evidente que, de ser la justicia ordinaria la que investigue a quienes no quieren comparecer ante la JEP, **"continuará la impunidad”.**

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
