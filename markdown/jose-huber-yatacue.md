Title: José Yatacúe, integrante de las FARC-EP fue asesinado en Toribio, Cauca
Date: 2017-04-26 17:48
Category: Nacional, Paz
Tags: FARC-EP, Ley de Amnistia
Slug: jose-huber-yatacue
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Jose-Yatacue-toribio-cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Abr 2017] 

El pasado martes 25 de abril, fue asesinado José Huber Yatacúe, comandante de la estructura de las FARC-EP en Toribio. El guerrillero, que hacia parte de la lista de amnistías y se preparaba para la reincorporación a la vida civil, **fue abordado por un hombre que le propino varios disparos**.

De acuerdo con el comunicado de la Punto de Normalización Transitorio Dagoberto Ortíz Monterredondo, en Miranda, Cauca, Yatacúe **se encontraba en su hogar cuando recibió una llamada de alguien identificado como Jhon Ramírez**, quién le manifestó que “le llevará un espejo a la salida del pueblo de Toribio, en frente del Hospital”.

Una vez Yatacué llegó a este lugar, **Ramírez le disparó en repetidas ocasiones, provocando la muerte inmediata**. En el comunicado de la zona veredal, señalan que Ramírez pertenece a grupos que se hacen pasar por integrantes del EPL y ELN en la región. Le puede interesar: ["Asesinado guerrillero de las FARC-EP que había sido beneficiado con Ley de Amnistía"](https://archivo.contagioradio.com/asesinado-guerrillero-de-las-farc-que-habia-sido-beneficiado-con-ley-de-amnistia/)

Este es el segundo asesinato de un integrante de las FARC tras la firma de los acuerdos de paz, el primer caso fue el de **Luís Alberto Ortíz, registrado el pasado 16 de abril, en la vereda la Guayacana, en el corregimiento de Tumaco, Nariño**. El guerrillero también, había sido beneficiario de la ley de amnistía e indulto y había recobrado su libertad hace 15 días, cuando salió de la cárcel de Vista Hermosa en Cali.

Al finalizar esta nota, no se conoce ningún pronunciamiento por parte del Mecanismo de Monitoreo sobre los hechos sucedidos en el Cauca.

Noticia en desarrollo…

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
