Title: 5 heridos deja agresión del ESMAD a comunidad de Cuatro Bocas en San Martín
Date: 2016-12-25 13:54
Category: DDHH, Nacional
Tags: Fracking colombia, san martin
Slug: 5-heridos-deja-agresion-del-esmad-a-comunidad-de-cuatro-bocas-en-san-martin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/cordatec.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CORDATEC] 

###### [25 Dic 2016] 

Durante la celebración de la fiesta de Navidad en San Martín, Cesar, **efectivos del ESMAD agredieron a decenas de pobladores en la finca del señor Miguel Lara, un total de 5 personas quedaron heridas.** La agresión se presentó luego de un pequeño altercado tras el paso de una camioneta de la empresa Conoco Philips y la policía.

**La comunidad denunció que el altercado con los integrantes de Conoco Philips se produjo porque la camioneta de la empresa pasó intentando amedrentar a la comunidad**, sin embargo la decisión de resistir pacíficamente por parte de los campesinos que se oponen al Fracking. Le puede interesar:["Conoco Phillips no cumple requisitos de licencia ambiental para Fracking en San Martín"](https://archivo.contagioradio.com/conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin/)

Luego del altercado, unos **200 integrantes del ESMAD llegaron hasta predio, destruyendo bienes privados y agrediendo incluso a mujeres embarazadas** que se encontraban en familia celebrando la navidad. Por lo menos 5 personas resultaron heridas con golpes, contusiones y cortaduras como se aprecia en las imágenes. Le puede interesar: ["Gases y Aturdidoras del ESMAD contra pobladores de San Martín"](https://archivo.contagioradio.com/gases-aturdidoras-del-esmad-pobladores-san-martin/)

Según el comunicado de CORDATEC, desde el pasado mes de Noviembre se **vienen presentando fuertes agresiones, intimidación y señalamientos por parte de la empresa** y la policía contra quienes se han opuesto a la realización de Fracking en sus territorios y concretamente en el sector de Cuatro Bocas.
