Title: ¿Fallará la Corte Constitucional en favor de las corridas de toros?
Date: 2019-03-28 17:35
Category: Animales, DDHH
Tags: corridas de toros, Corte Constitucional
Slug: fallara-la-corte-constitucional-favor-las-corridas-toros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/no-a-las-corridas-de-toros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Mar 2019] 

Según interpretación de algunos medios, la Corte Constitucional falló este miércoles en favor de las corridas de toros, concluyendo que esta actividad es una tradición cultural arraigada y no maltrato animal como señalan los activistas en defensa de los animales. Natalia Parra, directora de la organización animalista Plataforma Alto, aseguró que si los reportes son correctos, el alto tribunal estaría contradiciendo una sentencia previa.

Para la activista, la decisión de la Corte de 2018 por medio del cual las corridas de toros son permitidas exclusivamente en municipios donde existe un arraigo cultural, deja claro que la tauromaquia consiste en maltrato animal. "Ha dicho[ que **es el deber del Estado buscar que este tipo de espectáculos no se fomenten** y que de hecho, se destimulen", afirmó. Adicionalmente la misma sentencia había determinado que el Congreso debería legislar para resolver el tema.]

Por tal razón, Parra indica que podría haber un error en la interpretación que están realizando los medios y que "no hay que sacar conjeturas desde ya sino **esperar el comunicado de la Corte**". Sin embargo, si el comunicado público confirma los reportes mediáticos, organizaciones en defensa de los animales tendrían que tomar medidas para demostrarle a la Corte que se está contradiciendo.

### **"Cada ves tiene menos arraigo en el pueblo colombiano"** 

Si es como lo indican los medios, el fallo actual de la Corte estaría dejando en firme la sentencia C-666 de 2010, que determinó que las **corridas de toros, las peleas de gallos** y otros espectáculos con animales, **hacen parte del arraigo cultural en Colombia**, y por lo tanto, no pueden calificarse como un acto de maltrato animal. Sin embargo, Parra sostiene que este espectáculo cada día está perdiendo vigencia en el pueblo colombiano.

"Cada vez tiene menos arraigo, menos legitimidad en el pueblo colombiano, pero, como siempre, nuestras legislaciones, nuestras jurisprudencias en algunos casos van atrás de lo que va avanzando la sociedad", sostuvo Parra. El comunicado de la Corte Constitucional podría publicarse este mismo jueves.

<iframe id="audio_33822255" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33822255_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
