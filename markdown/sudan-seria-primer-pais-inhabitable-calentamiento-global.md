Title: Sudán sería el primer país inhabitable por el calentamiento global
Date: 2016-12-12 11:16
Category: Ambiente, Voces de la Tierra
Tags: Calentamiento global, cambio climatico, Sudán
Slug: sudan-seria-primer-pais-inhabitable-calentamiento-global
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Sudan-e1481558748427.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AP 

###### [12 Dic 2016] 

[Para el 2060 la temperatura de **Sudán tendrá un aumento entre 1,1 °C y 3,1 °C**, lo que lo convertirá en el primer país inhabitable si no se toman las medidas necesarias para enfrentar el calentamiento global, según explica un grupo de expertos del Instituto Max Planck para la Química Jos Lelieveld.]

[“En África del Norte ya hace mucho calor y las temperaturas siguen en alza. **En algún momento del siglo XXI una parte de la región podría ser inhabitable"**, afirma Jos Lelieveld, científico del Instituto.]

[El incremento de las temperaturas en el país africano, ha desencadenado extensas sequías, perjudicando la soberanía alimentaria de las familias de diferentes aldeas, y generando que unas **600 mil personas se hayan visto obligadas a desplazarse**. [(Le puede interesar: Cambio climático afecta el 80% de las funciones de la tierra)](https://archivo.contagioradio.com/cambio-climatico-afecta-80-las-funciones-la-tierra/)]

[Michelle Yonetani, asesora senior de desastres del Centro de Monitoreo del Desplazamiento Interno, IDMC, advierte que la “situación de emergencia es sumamente compleja”, pues agrega que “**La sequía agrava la desertificación**, que a su vez afecta al cinturón de la sabana en el norte del país, así que estos desiertos invasores han desplazado a ciudades enteras”.]

[Ante esta situación el Gobierno de Sudán implementa desde julio de este año, **un plan de adaptación renovable,** que se enfoca en la situación de las comunidades que viven en zonas rurales. Esta apuesta incluye el cultivo de diferentes alimentos resistentes a las sequías y la construcción de pozos para solucionar la escasez de agua. Además se está promoviendo la plantación de árboles, para contrarrestar la desertificación. [(Le puede interesar: Estos son los países más afectados por el cambio climático)](https://archivo.contagioradio.com/estos-los-paises-mas-afectados-cambio-climatico/)]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
