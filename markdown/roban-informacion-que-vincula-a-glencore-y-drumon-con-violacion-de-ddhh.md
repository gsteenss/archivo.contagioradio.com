Title: Roban información que vincula a Glencore y Drummond con violación de DDHH
Date: 2016-01-25 16:37
Category: DDHH, Nacional
Tags: Glencore en el Cesar, Robo computador Tierra Digna, Tierra digna
Slug: roban-informacion-que-vincula-a-glencore-y-drumon-con-violacion-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Tierra-Digna.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tierra Digna ] 

<iframe src="http://www.ivoox.com/player_ek_10195031_2_1.html?data=kpWem5qUd5Khhpywj5aZaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncYamk6jS0NnWs4zYxpCy1dnZqMrj1JDdw9fFb83VjK%2Fi1dnNp8rVjLjcxc7FsIzIysrf1MaPiMrbz5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Johana Rocha, Tierra Digna] 

###### [25 Ene 2016. ] 

El pasado miércoles el ‘Centro de Estudios para la Justicia Social Tierra Digna’ denunció que **personas no identificadas entraron a sus instalaciones y hurtaron un computador que contenía información relacionada con la vulneración de derechos fundamentales** individuales y colectivos en el marco de la implementación de distintos proyectos extractivos en departamentos como Cesar, Magdalena, Tolima, Chocó y Cauca.

Johana Rocha, integrante de Tierra Digna y dueña del computador, asegura que se trató de un robo dirigido a la obtención de información pues otros objetos de valor que estaban en las oficinas quedaron intactos. “En ese computador se encontraba **información sensible frente a proyectos mineros que existen en Colombia en la zona del Cesar y en Magdalena**, proyectos que operan las empresas Drummond, Glencore y Murray Energy, y de Anglo Gold Ashanti en Tolima, Chocó y Cauca”, agrega.

En el ordenador también se encontraba información relacionada con los seguimientos que Tierra Digna a hecho a las **políticas públicas implementadas en el marco del Plan Nacional de Desarrollo**. El hecho ocurre en un momento en el que la organización se encontraba culminando sus **solicitudes de audiencias temáticas ante la CIDH** y concretando los últimos detalles de dos **audiencias públicas ambientales que se realizaran esta semana en los departamentos de Chocó y Cesar**, en relación con los efectos de la minería y la omisión de distintas entidades frente a la degradación de los territorios ancestrales de comunidades afro e indígenas.

Parte de estas afectaciones por las que las comunidades aún no han sido reparadas tienen que ver con los **desplazamientos forzados por contaminación** a los que se han visto obligadas, por cuenta de los **impactos ecológicos de la actividad extractiva** en municipios como La Jagua, afectaciones que de acuerdo con Rocha **no tienen reparación económica**, y frente a las cuales las empresas están pensando ampliar su operación, en detrimento de los derechos fundamentales de la población.

Pese a que la pérdida de esta información resulta muy inoportuna para la organización, pues es el resultado de **años de monitoreo del accionar de distintas multinacionales en el país, y de creación de estrategias para la defensa de los derechos humanos**, Rocha asegura que dirigirán todos sus esfuerzos para que las acciones legitimas que vienen desarrollando junto a las comunidades no se vean entorpecidas, e insiste en que “a pesar de lo duro del golpe estamos motivados para seguir adelante en la defensa de los derechos humanos”.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
