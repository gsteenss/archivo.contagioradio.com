Title: Dos historias de mujer buscan nominación al Oscar y Goya para Colombia
Date: 2016-09-12 16:44
Author: AdminContagio
Category: 24 Cuadros
Tags: Academia colombiana de artes cinematográficas, Cine Colombiano, Premios Goya, Premios Oscar
Slug: dos-historias-de-mujer-buscan-nominacion-al-oscar-y-goya-para-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/pelicolomb.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotogramas Alias María y Anna 

###### [12 Sep 2016] 

La Academia colombiana de las Artes y Ciencias cinematográficas, en cabeza de su presidenta Consuelo Luzardo, anunció este lunes cuáles serán las películas que representarán al pais en las próximas ediciones de los **Premios Oscar** y los **Premios Goya** que tendrán lugar en 2017.

Se trata de "Alias María", de Jose Luis Rugeles y "Anna" de Jacques" Toulemonde, cintas que buscarán un lugar entre las producciones extranjeras nominadas que compiten en ambos Festivales.

"**Alias María**", buscará repetir lo alcanzado por El abrazo de la serpiente, primera película colombiana nominada a un Oscar de la Academia, desde una propuesta de memoria que habla sobre el conflicto armado en el país, puntualmente sobre el reclutamiento de menores para la guerra.

<iframe src="https://www.youtube.com/embed/u1Y_i5FHh1s" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Por su parte "**Anna**", co producción colombo-francesa protagonizada por la reconocida actriz **Juana Acosta**, cuenta la lucha de una madre por conquistar el amor de su hijo y vencer sus conflictos internos; una historia que contiene tintes drámaticos en una narrativa cosmopólita, favorable a sus opciones de alcanzar una nominación en los premios más importantes de la industria española.

<iframe src="https://www.youtube.com/embed/J42fwlYgSyw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
