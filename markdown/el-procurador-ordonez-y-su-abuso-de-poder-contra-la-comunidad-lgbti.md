Title: El Procurador Ordoñez y su abuso de poder contra la comunidad LGBTI
Date: 2015-06-22 11:54
Category: Escritora, Opinion
Tags: Corte Constitucional, LGBTI, Manuel José Bermúdez, Procurador, Procurador Alejandro Ordoñez
Slug: el-procurador-ordonez-y-su-abuso-de-poder-contra-la-comunidad-lgbti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/procurador.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

**[Escritora Encubierta](https://archivo.contagioradio.com/escritora-encubierta/) - [~~@~~OneMoreHippie](https://twitter.com/OneMoreHippie){.twitter-atreply .pretty-link}**

###### [22 Jun 2015] 

¿Hasta qué punto llegará la tiranía Procurador, o deberíamos decir Inquisidor, Alejandro Ordoñez? Esa es la pregunta que cientos de ciudadanos nos hacemos y a la que ni la mismísima Corte Constitucional puede dar respuesta. Es por eso que la entidad jurídica antes mencionada tuvo que verse en la penosa obligación de darle a Ordoñez un severo jalón de orejas.

El 20 de Junio del 2013 se venció el plazo que le dio la Corte Constitucional al Congreso para legislar sobre el matrimonio homosexual (C-577 de 2011). Al no haber decidido al respecto, a partir de esa fecha los jueces y notarios fueron habilitados para formalizar las uniones, sin embargo muchos se rehusaron a hacerlo. ¿La razón? El Procurador les expidió una circular con directrices sobre cómo actuar respecto a la sentencia.

''Los notarios temen al poder disciplinario del Procurador. Saben que llevar a cabo matrimonios igualitarios les costará retirarlos del cargo y por ello interpretan la sentencia de matrimonio de la Corte en forma restrictiva (uniones solemnes), siendo este contrato un hueco legal sin efectos claros y prefieren mantenerse en la notaria, antes que quedarse sin ella pagando el precio de una interpretación adecuada de la sentencia de la Corte'', comentó al respecto el abogado de la comunidad LGBTI Germán Rincón.

No conforme con eso, Ordoñez también exigió que se recolectara toda la información posible sobre las parejas que habían realizado las solicitudes de matrimonio llegando a tener toda una base de datos en la Procuraduría, cosa que disgustó fuertemente a los miembros de la comunidad LGBT. Manuel José Bermúdez, activista y promotor de los DDHH expresó su rechazo: ''Sentimos que es un acoso por parte del procurador Alejandro Ordoñez, es una persecución exagerada en contra de la población gay en el país''.

Muchos han calificado los actos del Procurador como métodos nazis. "Ir a recoger datossensibles de un grupo específico como para hacer un mapeo, identificarlos, pillarlos, estigmatizarlos, satanizarlos, eso no es acorde con la Constitución de 1991, ni con un

Estado Social de Derecho, ni con el bloque de constitucionalidad, ni con la dignidad humana, ni con nada; eso no se hace" comentó el Magistrado Néstor Raúl Correa.

Mauricio Albarracín, líder de Colombia Diversa, en una entrevista para EL HERALDO expresó su opinión frente la oposición del Procurador: ''Ordóñez ha convertido a la Procuraduría en la inquisición, y en la inquisición de verdad, porque ha perseguido físicamente parejas del mismo sexo que buscan casarse, ha mandado procuradores para que lleguen a las ceremonias, ha presentado nulidades de todas las decisiones sobre parejas del mismo sexo que se han producido en los últimos años y todas las ha perdido, ha hecho declaraciones estigmatizantes que han resucitado viejos cadáveres homofóbicos y les ha dado vida en la institucionalidad''.

En su defensa, el Procurador Ordoñez insiste en decir que su oposición es meramente jurídica y que no tiene nada que ver con sus creencias religiosas. No es de extrañar que nadie le crea cuando dice que no hay nada personal detrás de todas sus acciones puesto que este personaje no sólo ha atentado contra el matrimonio igualitario, también ha metido la cuchara en tantos temas referentes a la comunidad LGBT como sea posible, además de sus bien conocidas discusiones acaloradas con activistas.

Los antecedentes de Ordoñez no son de mucha ayuda. Su catolicismo influye tanto en todos los aspectos de su vida que incluso fue tema de su tesis de abogado, en la cual critica todos los modelos de Estado cuya fuente de autoridad no provengan de Dios, además concluye que es vital volver a un Estado basado en la doctrina católica. Eso sin mencionar su presunta participación en la quema de libros que fue llevada a cabo hace 37 años en Bucaramanga. En la lista de libros quemados se encuentra la traducción de una biblia no católica así como libros de Carl Marx, René Descartes, Friedrich Nietzsche, Víctor Hugo, Marcel Proust, José María Vargas-Villa, Thomas Mann, algunas obras del premio Nobel de literatura Gabriel García Márquez y revistas de educación sexual.

Afortunadamente por fin alguien se encargó de darle un pare al Procurador. En referencia a la circular que Ordoñez expidió a los notarios y jueces, en su más reciente fallo la Corte Constitucional dijo: ''No es válido que una persona o institución que no ostenta lacompetencia para interpretar con autoridad la Constitución ni las decisiones de la CorteConstitucional pretenda imponer su particular lectura de esta sentencia \[C-577 de 2011\] por vía de advertencias generales que puedan estar respaldadas en el uso del poder disciplinario''. Esperemos que el Procurador se atenga a lo ya dicho y no se meta donde no lo han llamado.
