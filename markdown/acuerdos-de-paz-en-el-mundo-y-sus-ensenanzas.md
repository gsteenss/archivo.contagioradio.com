Title: Acuerdos de paz en el mundo y sus enseñanzas para Colombia
Date: 2017-01-06 13:49
Category: Entrevistas, Paz
Slug: acuerdos-de-paz-en-el-mundo-y-sus-ensenanzas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Acuerdos-de-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 5 Ene 2017 

El año 2017 será trascendental para Colombia. La implementación de los acuerdos entre el gobierno y las FARC EP,  en transición a la vida civil están atravesados por la ausencia de una planificación eficaz del gobierno, el proceso preelectoral de congreso y presidencia, la crisis económica, los nuevos brotes de conflictos socio ambientales y lo que serán las políticas de Donald Trump.

Estos mismos elementos, al que se suma la amenaza neoparamilitar en sus diversas formas y una expresión retardataria de la sociedad, alentada por el uribismo son también los factores que se mueven en el entorno del eventual inicio de las **conversaciones entre el gobierno y el ELN.**

Si bien muchos analistas indican que la sociedad es la base de la paz, la colombiana padece de una emocionalidad y cultura de valores en los que se ha demonizado el alzamiento armado desconociendo las raíces del conflicto armado.

Muchos interrogantes se han abierto frente a la posibilidad del aumento de la violencia en el país, el cumplimiento real de estos acuerdos y la voluntad política del gobierno para cumplirlos y de abrir la mesa con el ELN y con una fuerza minoritaria como el EPL, que sigue viviendo en el ostracismo ante el país nacional.

Frente a estas preguntas consultamos a dos analistas en el tema, el investigador del Centro de Memoria Histórica, **Álvaro Villarraga** y el historiador y docente de la Universidad Nacional **Víctor de Currea,** que a partir de las experiencias de procesos de paz de otros países como Sudafrica, El Salvador o Nepal, abordaron los acuerdos de paz y su implementación**.** Estas fueron las respuestas que nos dieron. Le puede interesar [Donald Trump y el proceso de paz con las FARC y el ELN](https://archivo.contagioradio.com/trump-y-la-paz-con-farc-y-eln/)

\[embed\]https://youtu.be/TlYe0auGpGo\[/embed\]
