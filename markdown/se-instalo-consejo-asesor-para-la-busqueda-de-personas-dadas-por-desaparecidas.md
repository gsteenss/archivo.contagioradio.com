Title: Se instaló Consejo Asesor para la Búsqueda de personas dadas por desaparecidas
Date: 2020-06-11 22:02
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Organizaciones sociales, UBPD
Slug: se-instalo-consejo-asesor-para-la-busqueda-de-personas-dadas-por-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Personas dadas por desaparecidas en Villavicencio / Colectivo Orlando Fals Borda

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al instalar el primer Consejo Asesor, órgano de consultoría de la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD) - integrado por organizaciones sociales que durante décadas se han dedicado a dar con el paradero de sus familiares - se reafirma el compromiso con las más de 80.000 desaparecidos en Colombia en el marco del conflicto armado y se continúa con el mandato de una entidad que tendrá los próximos 20 años para implementar acciones humanitarias de búsqueda centradas en las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Diana Arango**, parte del Consejo Asesor y directora del [Equipo Colombiano Interdisciplinario de Trabajo Forense y Asistencia Psicosocial](http://equitas.org.co/) [(Equitas)](https://twitter.com/EQUITASColombia), organización forense que apoya la búsqueda de personas dadas por desaparecidas en 22 departamentos del país, señala que en medio de la pandemia se ha intentando mantener el contacto con las comunidades y hacer un seguimiento a la situación de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La directora alerta sobre el riesgo que en tiempos de Covid-19 puede correr el manejo de cuerpos inhumados como no identificados y que podrían ser víctima de desaparición en el marco del conflicto armado e incluso casos de personas que han fallecido por la pandemia y de las que ahora sus familiares no saben dónde están sus restos. [(Le puede interesar: Manejo erróneo de cementerios por Covid-19 pondría en riesgo memoria histórica del conflicto)](https://archivo.contagioradio.com/manejo-erroneo-de-cementerios-por-covid-19-pondria-en-riesgo-memoria-historica-del-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha advertencia ya la habían hecho otras organizaciones sociales como el Colectivo Orlando Fals Borda, señalando que es necesario que alcaldías y gobernaciones respondan de una mejor manera a los efectos de la pandemia en particular en los cementerios, lugares que sufren de un abandono histórico **"pues presentan problemas de infraestructura y de hacinamiento para atender las necesidades del país"**, donde no hay suficientes morgues y donde sus funcionarios no están entrenados ni equipados o protegidos de forma correcta para su empleo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque los desaparecidos por el conflicto en Colombia también se encuentran en otros sitios como fosas comunes, cañones y el fondo de los ríos, dada la contingencia del Covid-19 se requiere que en los cementerios se realice una inyección de recursos para que sean modernizados y puedan cumplir con la legislación existente que se requiere para la búsqueda de los desaparecidos y su protección. er hacer la documentación de prospección de las personas desaparecidas [(Lea también: En cementerio de Tumaco podrían haber más de 150 personas sin identificar)](https://archivo.contagioradio.com/en-cementerio-de-tumaco-podrian-haber-mas-de-150-personas-sin-identificar/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los riesgos en la búsqueda de personas dadas por desaparecidas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Son muchos territorios donde la labor de búsqueda ha llevado a que las organizaciones sean amenazadas por los grupos armados que hacen presencia en las regiones, un hecho que para la directora de Equitas es un síntoma de que las comunidades siguen en riesgo permanente al construir verdad por lo que también se requiere una política para protegerlas lo que implica un desmantelamiento de lass estructuras que siguen operando en el territorio, un apartado del Acuerdo de paz que hasta este momento no ha sido cumplido.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Es un llamado para las instituciones de Verdad, Justicia y Reparación, especialmente para la UBPD para buscar alterativas de cómo trabajar en medio de una situación de violencia"** y al tiempo proteger a comunidades y víctimas, un esfuerzo que debe involucrar la unión de todas las entidades del Estado para funcionar de forma integrada, conjunta y coordinada y sin corrupción. [(Lea también: Comprender fenómeno de los desaparecidos en Colombia aporta a la memoria histórica del país)](https://archivo.contagioradio.com/comprender-desaparecidos-capitulo-historia-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Advierte que además de los factores humanos hay otras variables a las que se enfrenta la búsqueda de los desaparecidos que a pesar de implicar años requieren de celeridad, como lo es el paso del tiempo y la necesidad que exista un relevo generacional pues **"los familiares que buscan a los desaparecidos son en su mayoría población de adultos mayores".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Concluye, que hay una situación más técnica y es la situación riesgo de los sitios de intervención susceptibles a la instauración de proyectos agropecuarios, minería o hidroeléctricas que afectan el territorio y la composición geográfica y espacial donde podrían estar las fosas, "esta situación está afectando las zonas en que se podrían recuperar a los seres queridos por lo que las intervenciones deben ser urgentes por parte de las entidades del Estado".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
