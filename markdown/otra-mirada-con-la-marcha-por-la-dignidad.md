Title: Otra Mirada: Con la marcha por la dignidad
Date: 2020-07-11 13:23
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Cauca, Líderes Sociales Asesinados, Marcha por la dignidad, Movilización social
Slug: otra-mirada-con-la-marcha-por-la-dignidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/marcha-por-la-dignidad.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio/Carlos Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El año 2019 estuvo marcado por diversas protestas, paros y manifestaciones para denunciar las acciones del gobierno tal como los cambios en el sistema de pensiones y también para exigir el cumplimiento de los acuerdos de los estudiantes, sin embargo en este año 2020, estas dinámicas se vieron imposibilitadas desde la aparición de la COVID-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así, a raíz del desmedido asesinato de líderes sociales,  la erradicación forzada, la militarización de los territorios y las violaciones a mujeres de comunidades indigenas, las comunidades del suroccidente de Colombia iniciaron el 25 de junio desde el Cauca, **la Marcha por la Dignidad,** cuyo objetivo era rechazar todos esto actos de violencia ante los cuales el gobierno ha permanecido callado. (Le puede interesar: [Todo lo que necesita sobre el avance de la Marcha por la Dignidad](https://archivo.contagioradio.com/a-pasos-de-gigante-avanza-la-marcha-por-la-dignidad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, y para conocer las voces de los territorios y de las comunidades, en Otra Mirada hicimos acompañamiento directo a los marchantes que se dirigían al centro de Bogotá. Además tuvimos como invitados a Javier Marín de la Asociación Minga, Cristian Llanos, investigador CINEP y desde la marcha a Katherine Pencue y Miguel Ángel, quienes nos comentaron de dónde nació la iniciativa, cómo fue el recorrido y qué esperan lograr en los próximos días.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la vez, exponen qué tanta fuerza creen y esperan que llegue a tener la movilización y dado que algunos marchantes denunciaron días previos que hubo persecución por parte de la policía y del ejército, explican cuál ha sido el rol de la fuerza pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En adición, con esta marcha esperan reconvocar la dinámica que se traía con los anteriores paros y que sea un aliento para que a nivel nacional retomemos las movilizaciones. (Si desea escuchar el programa del 9 de julio: [¿En Colombia se habla de genocidio?](https://www.facebook.com/contagioradio/videos/751932185643838)

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/1010514952701522","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/1010514952701522

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
