Title: 3 indígenas asesinados y 150  heridos es el saldo de represión a Minga en Cauca
Date: 2016-06-02 12:05
Category: Paro Nacional
Tags: Minga Agraria
Slug: un-indigena-nasa-muerto-y-otro-mas-herido-en-la-minga-agraria-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/ONIC-INDIGENA-MUERTO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Mientras en Bogotá se realizaba rueda de prensa por parte de los voceros de la Cumbre Agraria, en el departamento del Cauca se recrudecía la represión por parte del ESMAD, la policía y las FFMM a los integrantes de las comunidades indígenas en el Norte del Cauca.

La Organización Nacional Indígena de Colombia, informó sobre el asesinato del indígena Nasa **Gersaín Cerón Cuainás**, en Caldono (Cauca), información reiterada por la Asociación de Cabildos Indígenas del Norte del Cauca ACIN, que en un comunicado de prensa emitido en la tarde de hoy, reporta el uso indiscriminado de armas de fuego contra la protesta social por parte de la fuerza publica, que ha dejado hasta el momento dos comuneros muertos y tres heridos con arma de fuego en este departamento:

**Gersaín Cerón Cuainás, Resguardo de las Mercedes, Caldono (Asesinado)**

**Obidio Escue Dagua, del Resguardo de Huellas, Caloto (Herido)**

**Oscar Guetio Resguardo de Munchique los Tigres **(Herido)****

**Evelio Hurtado Resguardo de Pioya, Caldono **(Herido)****

**Rafael Pazú, Resguardo de Jámbalo **(Herido) ****

Hacia las 3 de la tarde se confirmó el asesinato de Marco Aurelio Diaz del resguardo "La Aguada" Según el comunicado de la ONIC el comunero falleció mientras era trasladado hacia el hospital de Santander de Quilichao luego de recibir dos impactos de arma de fuego.

Los demás indígenas heridos han resultado afectados por bombas recalzadas, granadas de aturdimiento y gases lacrimógenos usados por integrantes de la policía y las FFMM.

Por esta situación también agentes de la fuerza pública han salido heridos. De acuerdo con el reporte del Ministerio de Defensa 31 agentes han resultado afectados.  Además se aseguró que "la Fuerza Pública ofrecerá recompensas para quienes ayuden a esclarecer los hechos violentos que culminaron con la muerte de civiles y con graves heridas entre el personal uniformado", dice el comunicado.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10153518437845812%2F&amp;width=500&amp;show_text=false&amp;height=281&amp;appId" width="500" height="281" frameborder="0" scrolling="no"></iframe>
