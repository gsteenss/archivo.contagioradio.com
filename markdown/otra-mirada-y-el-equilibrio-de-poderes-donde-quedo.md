Title: Otra Mirada: ¿y el equilibrio de poderes dónde quedó?
Date: 2020-08-27 20:50
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: Corte Suprema de Justicia, Fiscalía General de la Nación, magistrados
Slug: otra-mirada-y-el-equilibrio-de-poderes-donde-quedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/corte-suprema-de-justicia-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Corte Suprema de Justicia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde que la Corte Suprema ordenó medida de aseguramiento contra el expresidente Uribe por delitos de soborno y fraude procesal, varias han sido las veces en que se ha tratado de entorpecer la continuidad del proceso por medio de pronunciamientos y tutelas que piden la libertad del expresidente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, después de que Álvaro Uribe renunció a su curul en el Senado, su partido político y allegados han pedido diversas veces que el caso pase a manos de la Fiscalía, intentando generar polarización y presión sobre los magistrados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por si fuera poco, tanto la Defensoría del Pueblo, como la Procuraduría General de la Nación quedaron en manos de funcionarios del gobierno copando así todos los entes de control.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa de Otra Mirada sobre la decisión de la Corte y la posible pérdida de equilibrio de poderes estuvieron Jaime Arrubla, exmagistrado de la Corte Suprema de Justicia, José Gregorio Hernández, docente y exmagistrado de la Corte Constitucional, Gustavo Gallón, director y fundador de la Comisión Colombiana de Juristas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el conversatorio los expertos afirmaron que se debe permitir obrar a los magistrados respetuosamente y con relación al caso Uribe, explican si es legítimo que por parte de los partidos políticos y del mismo presidente se ejerza presión a las altas cortes. (Le puede interesar: [Colombia debe respetar a sus Magistrados y sus decisiones](https://archivo.contagioradio.com/colombia-debe-respetar-a-sus-magistrados-y-sus-decisiones/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Debido a que el mismo expresidente Uribe ha revelado lo que dice el expediente, describen qué implicaciones tiene esto al entenderse que se estaría violando la reserva del sumario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, explican qué peso tiene lo que dijo la Procuraduria sobre trasladar el caso a la Fiscalía, qué intereses podrían haber para que se esté pidiendo esto y hablan de la pérdida de equilibrio de poderes en los órganos de control. (Si desea escuchar el programa del 25 de agosto: [Otra Mirada: La aspersión aérea no es la respuesta) ](https://www.facebook.com/contagioradio/videos/786884488783449)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa completo [Haga click aquí](https://www.facebook.com/contagioradio/videos/626675811319905)

<!-- /wp:paragraph -->
