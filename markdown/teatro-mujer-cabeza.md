Title: Teatro y acrobacias en "La mujer que perdió la cabeza"
Date: 2017-10-19 14:29
Category: eventos
Tags: Bogotá, teatro
Slug: teatro-mujer-cabeza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/6-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Factoria 

###### 19 Oct 2017 

[Llega este mes, en tres únicas funciones "La Mujer que perdió la cabeza", pieza teatral que combina el teatro físico con acrobacias aéreas. Un unipersonal ganador del premio Teatro en Movimiento de Idartes 2016.]

[Desde la poesía del mimo corporal, el espectáculo aéreo y la manipulación de objetos, la actriz, directora y dramaturga Andrea Meneses da vida a Antonia, una mujer bogotana en un país extranjero, atrapada en los recuerdos de un corazón roto y un trabajo que aborrece.]

[La vida de  este personaje colapsa cuando en una pesadilla surrealista, su cara se transforma, fantasmas del pasado vuelan sobre sus cabeza y el miedo de enfrentarse así misma la conducen a la locura.]

[La obra que busca comunicar mundos invisibles del subconsciente cuando se experimenta desamor, apego, estancamiento y soledad, se estará presentando los días 19, 20 y 21 de Octubre a las 8:00 p.m. en Factoría L’EXPLOSE Cr 25\#50-34 Bogotá.]

[La boletería tiene un costo de \$30.000 general \$25.000 estudiantes y personas de la tercera edad. ([Le puede interesar: Una mirada común a los "Días y noches entre guerra y paz"](https://archivo.contagioradio.com/chuma-raizal-teatro/) )]

<iframe src="https://www.youtube.com/embed/8RPw2m5oSpM" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
