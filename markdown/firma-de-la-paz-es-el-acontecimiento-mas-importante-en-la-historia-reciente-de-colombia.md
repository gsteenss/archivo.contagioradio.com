Title: Firma de la paz es el acontecimiento más importante en la historia reciente de Colombia
Date: 2016-09-26 16:04
Category: Nacional, Paz
Tags: Apoyo al proceso de paz, proceso de paz
Slug: firma-de-la-paz-es-el-acontecimiento-mas-importante-en-la-historia-reciente-de-colombia
Status: published

###### [Foto: Contagio radio] 

###### [26 de Sep 2016]

Desde muy temprano en la mañana, víctimas del conflicto armado, políticos y líderes han expresado su voz de apoyo y unen a la celebración de la firma de los acuerdos de paz entre el Gobierno de Colombia y las FARC-EP, que se llevará a cabo en la Plaza de las Banderas en Cartagena.

Una de estas voces es la de la presidenta de la Unión Patriótica, Aida Avella, sobreviviente al exterminio que vivió el partido político durante la década de los ochenta. Para Aida hace 30 años la paz se escapó y ahora es la oportunidad de que el gobierno cumpla los acuerdos y se respete la vida de las personas que dejan las armas **“aquí nadie puede morir por sus convicciones políticas independientemente del sector político del que sea”**, además afirmo que se abre la esperanza para que las generaciones de venideras vivan contando las historias de la paz.

Para el defensor de derechos humanos y representante a la Cámara por el Polo Democrático, Alirio Uribe, la firma de los acuerdos hacen parte del cumplimiento de una tarea que ha empujado el movimiento social, a su vez, expresó que “los acuerdos que se van a firmar en Cartagena no son un punto de llegada sino un[punto de partida para la posibilidad de que haya transformaciones democráticas en el país”](https://archivo.contagioradio.com/ya-estan-dadas-las-condiciones-para-un-cese-bilateral-alirio-uribe/), además ratifico su esperanza y alegría para la votación del próximo 2 de octubre en el plebiscito por la paz, para que la población Colombiana lo apruebe en su conjunto.

Andres Gil, vocero de Marcha Patriótica, afirmó que este es un gran momento para Colombia que implica un **ejercicio de pedagogía con la ciudadanía y explicación de cada uno de los puntos** de los acuerdos para su posterior implementación. De igual forma aseguro que esta firma, es producto de la movilización social y de tejer luchas con diferentes organizaciones sociales.

Según Carlos Lozano, director del semanario Voz y miembro del Partido Comunista, se le pone punto final a una confrontación de 52 años y abre las posibilidades para la renovación democrática de la vida del país, de igual modo expresó que para el Partido Comunista esta es una victoria de la persistencia y resistencia a todos los años de persecución política y a su vez, **es un reconocimiento a los mártires que ayudaron a construir la este momento para el país.**

El senador Ivan Cépeda, afirmó que **“este es el momento más importante de la historia contemporánea y la concreción de décadas de esfuerzos para llegar a la paz”**, además, dijo que el hecho de hoy tiene múltiples significados que le hacen un llamado a la construcción de país que se cimiente sobre las realidades políticas que han quedado ausentes y que han permitido la guerra. De igual forma, expresó que en esta oportunidad también debe [rendirse honores a todos aquellos que han sacrificado su vida por la paz y por todas las luchas sociales](https://archivo.contagioradio.com/en-22-anos-no-se-han-determinado-los-maximos-responsables-del-crimen-de-manuel-cepeda/).

 De acuerdo con la vocera de Congreso de los Pueblos, Marylen Serna, el proceso de paz ha significado momentos de reflexión y esfuerzos muy importantes por construir la unidad entre diversos sectores políticos.  A su vez, expuso que Congreso de los Pueblos tiene tres tareas claras con la firma de los acuerdos, la primera es estar en los territorios con las comunidades y en los escenarios de implementación y participación; la segunda es insistir en la **instalación de la mesa de diálogo entre el gobierno y el ELN** y para finalizar continuar en la movilización y discusiones con el gobierno frente a los puntos que no están incluidos en los acuerdos de paz de la Habana.

Al evento de la firma de los acuerdos de paz, también fueron invitadas delegaciones de víctimas del conflicto armado en Colombia, comunidades indígenas despojadas y desplazadas de sus territorios y representantes de organizaciones sociales.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
