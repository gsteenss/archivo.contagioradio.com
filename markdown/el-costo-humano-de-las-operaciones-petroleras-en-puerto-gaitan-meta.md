Title: El costo humano de las operaciones petroleras en Puerto Gaitán, Meta
Date: 2016-07-13 17:47
Category: Economía, Nacional
Slug: el-costo-humano-de-las-operaciones-petroleras-en-puerto-gaitan-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/petroleo1-e1468449932490.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Inteligencia petrolera 

###### [13 Jul 2016] 

Este martes el FIDH, el Colectivo de Abogados José Alvear Restrepo y el Proyecto de Acompañamiento y Solidaridad Internacional en Colombia, presentaron el informe que revela los costos humanos y ambientales del petróleo en Colombia, puntualmente frente a las operaciones de la multinacional canadiense Pacific Exploration & Production Corp.

La investigación titulada “el costo humano del petróleo: Estudio de impacto en los derechos humanos de las actividades de Pacific Exploration & Production Corp. en Puerto Gaitán”, concluye a grandes rasgos que este tipo de extracción “causa daños ambientales, e impactan de manera negativa la pervivencia de las comunidades indígenas en la zona de Puerto Gaitán, departamento del Meta, de donde proviene una cuarta parte del petróleo del país. Además ha aumentado la represión y criminalización de líderes sindicales, sociales y ambientales en la zona de explotación petrolera”.

En ese sentido, se constataron violaciones al derecho de libre asociación, pues el 81% de los trabajadores encuestados expresaron que la empresa donde trabajaban no permitía la afiliación libre y voluntaria a un sindicato, y 79% de ellos pensaban que podrían ser despedido como represalia por afiliarse al sindicato USO, indica el informe. También se concluyó que el 92% de los trabajadores encuestados son tercerizados, revelándose violaciones a los derechos laborales  específicamente sobre la asociación, autonomía sindical, negociación colectiva y huelga.

Frente a las movilizaciones y manifestaciones se conocieron tres casos de lesiones personales graves por parte de la fuerza pública hacia trabajadores, cuyos actos violentos continúan en la impunidad. Así mismo, no se ha llevado a cabo una investigación con respecto a los 32 asesinatos selectivos que se denunciaron en Puerto Gaitán luego de las protestas del 2011.

Por otra parte, respecto a las violaciones de los derechos de las comunidades, las poblaciones indígenas cercanas a los bloques petroleros señalan que luego de la llegada de la empresa sufrieron cambios en sus costumbres  debido a la pérdida de cultura y espiritualidad, cambios en los modos de producción, pérdida de autonomía y auto suficiencia, y escenarios de desarticulación de las comunidades a raíz de la intervención de las empresas petroleras en el territorio.

Todo esto se posibilita debido a que el Estado colombiano no efectúa un control y seguimiento oportuno sobre las licencias ambientales; adicionalmente, las instituciones no cuentan con marcos regulatorios estrictos para garantizar los derechos de los trabajadores por lo que se evidencia que los entes de control del Estado son responsables en gran medida de las violaciones a los derechos humanos por la incapacidad y falta de voluntad de actuar para las violaciones de los derechos humanos.

El informe, se basó en una investigación realizada sobre los bloques petroleros de Rubiales-Pirirí y Quifa, y los documentos oficiales, entrevistas a autoridades nacionales y empresas relevantes y la realización de 598 encuestas a pobladores, trabajadores y comunidades indígenas, para obtener una Evaluación de Impacto sobre los Derechos Humanos elaborada desde la perspectiva de los grupos afectados por la industria petrolera en Puerto Gaitán.

[Informe Pacific Rubiales](https://es.scribd.com/document/318229847/Informe-Pacific-Rubiales#from_embed "View Informe Pacific Rubiales on Scribd")

<iframe id="doc_86118" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/318229847/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true&amp;show_upsell=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
