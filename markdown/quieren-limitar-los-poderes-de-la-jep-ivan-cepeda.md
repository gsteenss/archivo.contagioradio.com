Title: "Quieren limitar los poderes de la JEP" Iván Cepeda
Date: 2018-06-08 14:46
Category: Paz, Política
Tags: Camila Moreno, Iván Cepeda, Jurisdicción de paz
Slug: quieren-limitar-los-poderes-de-la-jep-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [08 Jun 2018]

Una vez más la Jurisdicción Especial para la paz esta en el centro del debate. Por un lado se ha manifestado que no tiene competencias frente al caso Santrich, en donde Estados Unidos ya hizo la solicitud de extradición, y por el otro se le cuestiona los permisos otorgados a integrantes de la FARC para salir del país.

### **Las competencias de la JEP en caso Santrich** 

<iframe src="https://co.ivoox.com/es/player_ek_26434903_2_1.html?data=k5uhlZmddJShhpywj5aaaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Zw5Cxs9PZz9SSlKiPiMrmxsjh0dfFb8XZzZCwx9PYttCfqtPhx9fSpcTd0NPOzpDIqYy-1pKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Camila Moreno, directora del Centro Internacional de Justicia Transicional, señaló que en el artículo 19 del Acto legislativo 001, se menciona que le corresponde a la sección de revisión de la JEP, evaluar la conducta para establecer si el hecho se cometió antes del 1 de diciembre de 2016 o después.

"Sí el artículo 19 dice que la sección de revisión tiene que evaluar la conducta, para hacerlo ha de tener un mínimo de información que debe ser aportada por las autoridades que han hecho la investigación, es **decir se tienen que presentar un informe completo en el que Estados Unidos solicita en extradición a Santrich, y mediante el cual la Fiscalía profiere la captura por orden de extradición"** afirmó Moreno.

En ese sentido la sección ya habría establecido los protocolos para el tema de extradición, y cuales son esos momentos en el proceso para evaluar la fecha, facultades también otorgadas por el acto legislativo. (Le puede interesar: ["JEP tiene la última palabra en extradición de integrante de partido FARC"](https://archivo.contagioradio.com/jep-tiene-la-ultima-palabra-en-extradicion-de-integrantes-del-partido-farc/))

En esa vía para Moreno, lo más grave de esta situación es que se traslade un debate político, del Congreso de la República a lo jurídico y que de esta forma se restrinjan los mandatos de los magistrados.

<iframe src="https://co.ivoox.com/es/player_ek_26434931_2_1.html?data=k5uhlZmdd5Khhpywj5WdaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5yncarqhqigh6aVsoy3xtXSxsaPt9DW08qYzsbXb83dzs7hw8jNs8_Z1JDe18qPt8af0dfS1srSqMbijM7aj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por su parte el senador Iván Cepeda afirmó que se esta buscando profundizar y avanzar en limitar la JEP y darle más herramientas a la Fiscalía General de la Nación s**obre temas que no son de su competencia y que están especificadas en el Acuerdo de Paz para la Jurisdicción**.

"Es un absurdo pensar que se puede determinar la fecha de ocurrencia de un hecho, si no se ha establecido claramente el hecho. Acá no nos han explicado cómo se pretende que la JEP no valoré probatoriamente lo que le llegue de las agencias estadounidenses, si se pretenden que establezca cuando ocurrieron los hechos que se le intentan imputar a Santrich" expresó Cepeda.

### **Permisos de la JEP no violan ninguna restricción** 

Para Camila Moreno , la dificultad más grande de estos permisos es que no habrían sido autorizados por la autoridad competente dentro de la JEP, sin embargo, no estarían violando ninguna restricción.

Moreno manifestó que para marzo, momento en el que se hicieron las solicitudes, ya estaba funcionando los magistrados de este Tribunal, por lo tanto era la sala de Amnistía y no la secretaría ejecutiva la que debia realizar las autorizaciones. De igual forma, manifestó que una vez alguien se somete a la JEP y tiene la aprobación y el respaldo del permiso para salir del país, **no hay ningún otro condicionamiento sobre el uso de ese permiso. **

En ese sentido Moreno señaló que si bien no hay ningún reparo frente al permiso, si se generó un "mal recibe" tanto en la opinión pública como las víctimas del conflicto armado en el país, que sienten que es una afrenta a los compromisos que se han asumido con ellos. (Le puede interesar:["Las razones para no llevar a la JEP el caso de Jaime Garzón"](https://archivo.contagioradio.com/las-razones-para-no-llevar-caso-de-jaime-garzon-ante-la-jep/))

"**Lo cierto es que en ningún momento el hecho de haber salido del país, por el motivo que sea, significa que se va a incumplir con la obligación** de aportar la verdad, reparar a las víctimas y reconocer la verdad. No estamos adelantando al debate" aseguró Camila Moreno.

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
