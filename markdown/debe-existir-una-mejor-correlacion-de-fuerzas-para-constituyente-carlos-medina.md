Title: "Debe existir una mejor correlación de fuerzas para Constituyente": Carlos Medina
Date: 2016-10-03 13:16
Category: Nacional, Política
Tags: acuerdo cese al fuego farc y gobierno, constituyente, ELN
Slug: debe-existir-una-mejor-correlacion-de-fuerzas-para-constituyente-carlos-medina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/confidencial-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CondifendialColombia] 

###### [3 de Oct 2016] 

Después de las elecciones del plebiscito por la paz, muchas son las incertidumbres que quedan frente al camino para que se de en Colombia la anhelada paz, por un lado, está el **pacto nacional que han mencionado diversos sectores políticos**, la inclusión de las organizaciones sociales en el debate de la paz y la puesta en marcha de la **mesa de diálogos con el ELN**, por el otro está la sociedad civil, **la desconexión de la ciudad con la guerra y el alto porcentaje de abstención de 62,6%**.

La mención al pacto Nacional, vuelve a la escena política como una posible salida a los acuerdos de paz, para Carlos Medina, miembro del Centro de Pensamiento de la Universidad Nacional, la constituyente debe pensarse en unas circunstancias específicas que posibiliten un cambio en la democracia, **si se hace en medio de una disputa política, como la que atraviesa el país actualmente, puede generar reverso de resultados a la constitución**, motivo por el cual debería primero establecerse un [gran pacto político democrático de todos los sectores políticos y sociales](https://archivo.contagioradio.com/pedimos-participacion-directa-voz-y-voto-en-el-nuevo-pacto-social-conpaz/), que plantee ajustes, hecho que por los resultados de ayer, evidencia una correlación de fuerzas en contra de un cambio favorable para la constitución de Colombia.

Sin embargo, uno de los actores que podría estar moviendo esa balanza de correlación de fuerzas es el ELN, que en los últimos días ha tenido acercamientos con el gobierno y muestras por aportar en la construcción de paz, en su en su cuenta de twitter la guerrilla expresó que **“llaman a la sociedad colombiana a continuar buscando una salida negociada al conflicto armado”** y esto para Medina podría ser un avance que destrabe los procesos y que camine en vía de un diálogo abierto y “un consenso que favorezca al conjunto de la nación Colombiana y no a determinados sectores que son los que se han opuesto al acuerdo”.

Sobre el alto porcentaje de abstención que hubo en estas elecciones, Medina considera que es fruto de la despolitización que atraviesa la **sociedad colombiana que no encuentra relación entre la política y su cotidianidad**, sin embargo, frente al aumento de las personas que no votaron al plebiscito, una de las variables que estaría a favor de esta cifra, sería la falta de movilización y acción por parte de los partidos políticos. Igualmente, la **falta de consonancia entre el avance de los acuerdos y las políticas del gobierno del presidente Santos**, podrían haber jugado en contra del plebiscito.

Para el profesor Carlos Medina, la votación del día de ayer reflejó la cultura política y la falta de conocimiento del país frente a los acuerdos, hecho que demuestra que primo la decisión emotiva sobre la racional a la hora de votar: **“ganó el miedo contra la esperanza de paz”**. De igual modo reitera que la labor de las organizaciones sociales será[comprender las características de la población civil](https://archivo.contagioradio.com/del-20-al-22-de-mayo-se-realizara-la-asamblea-nacional-de-procesos-constituyentes-por-la-paz/) que no se encuentran organizadas y que están revirtiendo el trabajo de los diferentes movimientos sociales.

<iframe id="audio_13164195" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13164195_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
