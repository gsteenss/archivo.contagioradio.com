Title: En manos de Claudia López está la posibilidad de levantar el paro en la Distrital
Date: 2020-01-21 17:32
Author: CtgAdm
Category: Entrevistas, Estudiantes
Tags: Alcaldía, Claudia López, Universidad Distrital
Slug: en-manos-de-claudia-lopez-esta-la-posibilidad-de-levantar-el-paro-en-la-distrital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Claudia-Lopez-Universidad-Distrital.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Alcaldía de Bogotá {#foto-alcaldía-de-bogotá .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La semana pasada se llevó a cabo la primera reunión del Consejo Superior Universitario (CSU) de la Universidad Distrital, presidido por la alcaldesa Claudia López, que tiene en sus manos la decisión de levantar el paro en la Institución aprobando la propuesta de Asamblea Universitaria. Sin embargo, **aún no se conoce la posición de la Alcaldía sobre este tema.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Universidad Distrital: Corrupción, desfinanciación y ausencia de participación**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Julián Baez, representante estudiantil ante el CSU recordó que el paro en la Distrital continúa por los hechos de corrupción que fueron descubiertos por la Procuraduría en septiembre de 2019, y que tienen a un funcionario sancionado con el impedimento para ocupar cargos públicos por 17 años. (Le puede interesar: ["Corrupción en la Universidad Distrital"](https://archivo.contagioradio.com/corrupcion-universidad-distrital/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, y como forma de apertura democrática, los estudiantes propusieron nuevamente la **Asamblea Universitaria, para garantizar la participación efectiva de estudiantes, profesores y trabajadores** en el devenir de la Universidad, incluso en aspectos presupuestales que impidan repetir hechos de corrupción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La propuesta pasó todos los análisis financieros y jurídicos necesarios para ser aprobada a finales de 2019, pero no hubo voluntad política de la alcaldía Peñalosa para hacerlo. Más grave que eso, Báez denunció que el CSU aprobó por correo web el 31 de diciembre de 2019 el presupuesto para este año con **un déficit de 89 mil millones de pesos.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"No solo queremos que (López) presida el CSU, sino que nos escuche y materialice nuestras exigencias"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Durante campaña López se comprometió a hacerse presente en las reuniones del CSU, contrario a la costumbre de Peñalosa que solo enviaba delegados. La Alcaldesa cumplió con su palabra y presidió la primera cita, en la que también se hizo presente la ministra de educación María Victoria Angulo. 

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ClaudiaLopez/status/1217237632599711746","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ClaudiaLopez/status/1217237632599711746

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Sin embargo, según explicó Báez, la propuesta de Asamblea no fue aprobada porque la [Alcaldía](https://bogota.gov.co/mi-ciudad/educacion/universidad-distrital-aprueba-medidas-reforma-estatutaria-y-asamblea) presentó diferentes contrapropuestas que no permitieron llegar a conclusiones. Para resolverlo, se decidió crear una Comisión para estudiar las propuestas y presentar una idea final que **debería ser discutida la próxima semana.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El líder estudiantil aseguró que aún no se sabe la posición que asumirá López respecto a propuesta de la Asamblea, pero declaró que como estudiantes "no solo queremos que presida el CSU, porque eso está en sus funciones y debería hacerlo sin que lo pidiéramos, sino que nos escuche y materialice nuestras exigencias".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Baez concluyó que **la única exigencia para levantar el paro es ampliar la participación mediante la Asamblea Universitaria**, y tomando en cuenta que todo el trámite se ha surtido de forma correcta, si alguien se opone a la propuesta será por voluntad política. (Le puede interesar:["Los compromisos de Claudia López con la Universidad Distrital"](https://archivo.contagioradio.com/los-compromisos-de-claudia-lopez-con-la-universidad-distrital/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46857454" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46857454_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
