Title: "El gobierno es el que determina si nos movilizamos o no" Cumbre Agraria
Date: 2015-05-13 11:35
Author: CtgAdm
Category: Movilización, Nacional
Tags: Asamblea Nacional de la Cumbre Agraria, Cumbre Agraria, ministerio de defensa, Plan Nacional de Desarrollo, Proceso de Comunidades Negras
Slug: el-gobierno-es-el-que-determina-si-nos-movilizamos-o-no-cumbre-agraria
Status: published

###### Foto: sobreamericalatina.com 

###### [13May2015] 

Cerca de 300 personas, delegadas de todas las regiones del país y de las organizaciones sociales que hacen parte de la **Cumbre Agraria** se reúnen en la segunda asamblea nacional, tras cumplirse 2 años de diálogos con el gobierno nacional y un año de la instalación oficial de la mesa única de la Cumbre Agraria. Esta asamblea podría **definir que se reanudan las movilizaciones para volver a presionar al gobierno.**

Una de las principales preocupaciones de las organizaciones gira en torno al incumplimiento y la estrategia de **dilación del gobierno nacional** que no ha definido un vocero con poder de decisión sobre las exigencias de la Cumbre.

Según José santos, vocero del **Proceso de Comunidades Negras**, PCN, la asamblea se dirige a hacer un balance político, el balance de la unidad, pero también las principales afectaciones que se han producido tras el incumplimiento del gobierno, una de ellas la aprobación inconsulta del **Plan Nacional de Desarrollo.**

Santos señala que un tema neurálgico son las **garantías de Derechos Humanos**, puesto que durante este periodo se ha presentado una arremetida de amenazas, estigmatización persecución a los integrantes de las organizaciones de la Cumbre. Además, los líderes y lideresas detenidos en el proceso de movilización siguen en las prisiones. Además se está elaborando un manual de DDHH para que no se presenten más agresiones por parte de las FFMM y de seguridad, en dicho manual no se ha podido incluir el tema del DIH puesto que Min Defensa se niega a discutirlo.

Uno de los ejemplos es que el gobierno no ha dicho nada frente al desembolso para proyectos de **fortalecimiento productivo de la economía campesina**, negra e indígena, que asciende a 250 mil millones de pesos, con los que se había comprometido para el 2014.

Santos concluye que el **balón está en la cancha del gobierno y el gobierno será que determine si hay nuevamente movilización** a nivel nacional. Sin embargo hacia las 4 de la tarde de este mismo 13 de Mayo se tendrán las conclusiones de las comunidades.
