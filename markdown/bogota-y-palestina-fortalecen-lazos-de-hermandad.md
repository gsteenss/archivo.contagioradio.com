Title: Bogotá  y Palestina fortalecen lazos de hermandad
Date: 2015-10-01 17:46
Category: Nacional, Paz
Tags: Acuerdo de hermandad, Alexander Montero, Bogotá Humana, Estados Unidos, gloria florez, Gustavo Petro, Israel, ONU, Palestina, Secretaría de Gobierno de Bogotá
Slug: bogota-y-palestina-fortalecen-lazos-de-hermandad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/palestina.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Este jueves se realizó la izada de bandera de Palestina en  Bogotá en la Plaza de Bolívar, donde se contó con la participación del **alcalde Gustavo Petro y el embajador de Palestina, Raouf Almalki, con el fin de fortalecer los lazos de hermandad** entre ambas naciones que comparten  el mismo número de años de conflicto armado.

Según Gloria Flórez, Secretaria de Gobierno de Bogotá , el  acuerdo  que  firmó el alcalde Petro y Almalki permitirá construir lazos de solidaridad **“ya que se necesita construir  paz para excluir  la segregación y  la inclusión social”**, para ella existe una unión  entre ambas naciones debido a que” son pueblos hermanos que claman justicia y libertad”.

Por otra parte, el Embajador de Palestina Raouf Almalki, aseguró que izar la bandera en territorio colombiano significó **"Paz, justicia e independencia**", para él, es de gran orgullo este acontecimiento, teniendo en cuenta que la Asamblea General  de las Naciones Unidas confirmó  que Palestina  no está sola en la búsqueda de libertad.

Actualmente el pueblo Palestino se ve afectado por la negación de los derechos fundamentales  por parte de Israel, **“existen más de 5 mil millones de refugiados  en 1948 nuestros hogares y patrimonios fueron desaparecidos fuimos obligados el exilio”**, afirma el representante de Palestina, quien no pierde la esperanza de lograr la independencia y la paz a través del diálogo.

Alexander Montero, asesor político de la embajada de afirmó que este acuerdo representa “unión espiritual y cultural,” demostrando cercanía y convirtiendo a las dos naciones en una sola.

Finalmente el alcalde Gustavo Petro **solicitó al presidente  de Estados Unidos,**  en nombre de la ciudadanía de Bogotá, **reconocer el Estado palestino resaltando que “así no ayudamos a construir la paz,**
