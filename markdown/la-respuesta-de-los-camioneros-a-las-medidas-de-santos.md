Title: La respuesta de los camioneros a las medidas de Santos
Date: 2016-07-14 13:30
Category: Movilización, Nacional
Tags: Juan Manuel Santos, Paro camionero, Protesta social, Represión
Slug: la-respuesta-de-los-camioneros-a-las-medidas-de-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/paro-camionero1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [14 Jul 2016] 

Inmovilizar e incautar los vehículos que se usen para bloquear las vías o impidan el servicio de transporte, cancelar la licencia a los conductores que participen en estos bloqueos y la licencia de operación a propietarios o empresas, imponer multas hasta por 480 millones de pesos, además de cancelar las matrículas de los vehículos que se presten para vías de hecho y duplicar el número de efectivos de la Fuerza Pública en las carreteras a 50 mil, son las medidas que anunció el presidente Juan Manuel Santos, asegurando antes que el gobierno respeta la protesta social.

Para Alfonso Medrano, presidente de la Asociación Colombiana de Camioneros, ACC de Boyacá, estas imposiciones emitidas por el gobierno evidencian que cuando “Se acaba la razón, se usa la represión”, y agrega que **en ningún momento han sacado sus camiones para atravesarlos en las vías y bloquearlas, pues estos se encuentran parqueados** como un acto de movilización social y pacífica, de manera que no se está cometiendo ningún ilícito con los vehículos.

Santos también aseguró en su alocución presidencial que “esta protesta va en contravía de los derechos de los colombianos a la seguridad alimentaria, a la libre circulación, a la tranquilidad y al trabajo”, sobre lo que Medrano le contesta  “**No es que nosotros queramos perjudicar a todo un pueblo, nos cansamos de servir a pérdida"**

A su vez, desde la ACC, aseguran que continuarán con el paro, pese a las medidas represivas del gobierno, teniendo en cuenta que sus camiones que se encuentran guardados no cuentan con el mantenimiento necesario para salir a trabajar, pues la situación económica del gremio desde hace 5 años es bastante grave **“¿Por qué Juan Manuel Santos, no habla del detrimento económico en que nos tiene hace 5 años?”, se pregunta Medrano, agregando que no van a salir a para seguir trabajando a pérdidas.**

Frente a las declaraciones del presidente, el gremio llevó en la mañana de este jueves su propuesta al gobierno para poder terminar el paro, pero aseguran que **no van a firmar ningún acuerdo lesivo para el futuro de los camioneros**, como lo acordaron con sus bases.

<iframe src="http://co.ivoox.com/es/player_ej_12224629_2_1.html?data=kpeflJmadpqhhpywj5aXaZS1kZaah5yncZOhhpywj5WRaZi3jpWah5yncaLgx9Tb1dSPkcbY08bb0YqWh4y1pKiYpNTdpcSZpJiSo5aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio 
