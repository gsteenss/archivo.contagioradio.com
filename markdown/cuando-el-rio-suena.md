Title: Cuando el río suena…
Date: 2018-10-01 08:43
Author: AdminContagio
Category: Mision Salud, Opinion
Tags: Acceso a los medicamentos, Medicamentos, Presidente Duque, TLC
Slug: cuando-el-rio-suena
Status: published

###### Foto: Presidencia Col 

**Por[ Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

###### 29 Jul 2018 

[Preocupante e inesperado desde la esquina de la salud el anuncio hecho recientemente a los medios por el Canciller Trujillo de que habrá una reunión bilateral Colombia-Estados Unidos para resolver controversias relacionadas con el Tratado de Libre Comercio (TLC), específicamente en propiedad intelectual][[(CM&, Emisión Central, 7.09.2018)]](http://www.mision-salud.org/informe-de-gestion-semestral-no-1-marzo-septiembre-2018/)[.]

[Preocupante porque]**no hay ningún antecedente de que cuando Estados Unidos se sienta con un país en desarrollo a negociar propiedad intelectual sea para beneficio del débil. Y menos ahora bajo el abrigo de la política comercial proteccionista y egocentrista del Presidente Trump**[.]

[Inesperado porque el anuncio del Canciller se produce en momentos en que]**el Presidente Duque ha dicho reiterada y claramente a los colombianos que durante su mandato no se negociarán nuevos acuerdos comerciales con otros países sino que se tomarán las medidas necesarias para sacar mayor provecho de los existentes** [(][[Tibú, Norte de Santander, 9.08.2018]](https://id.presidencia.gov.co/Paginas/prensa/2018/180809-En-Tibu-el-Presidente-Duque-inaugura-planta-productora-de-aceite-de-palma.aspx)[), lo que ha ratificado el Ministro de Comercio (La][[FM, 8.07.2018]](https://www.lafm.com.co/economia/mincomercio-no-se-firmaran-nuevos-tlc-se-aprovecharan-los-que-tenemos)[) y otros altos funcionarios del gobierno nacional.]

*[“Cuando el rio suena, piedras lleva”.]*[En salud, en este caso el torrente de piedras sonoras se identifica con la]**aspiración perpetua de la agencia comercial de  Estados Unidos (USTR) de aprovechar toda oportunidad para conseguir las ventajas que en materia de propiedad intelectual en medicamentos no pudo obtener a través del TLC, para beneficio exclusivo de su industria farmacéutica**[, por ejemplo la extensión del espectro patentable mediante el reconocimiento de patentes para segundos usos terapéuticos y para cambios insignificantes de productos conocidos, entre otras, la extensión del plazo de las patentes y otras formas de monopolio farmacéutico existentes para retardar aún más la competencia genérica con precios razonables, y la prohibición de negar patentes sobre la base de que el nuevo producto no sea más eficaz que el producto conocido. Todo lo cual sería catastrófico para la salud de los colombianos, en términos de incremento de los precios de los medicamentos, pérdida del acceso de la gente a estos bienes esenciales, aumento de los índices de morbimortalidad y nuevos riesgos para la estabilidad financiera del sistema nacional de salud, hoy en crisis.]

[De igual manera, múltiples precedentes autorizan pensar que de llevarse a cabo la conferencia anunciada por el Canciller, sería aprovechada por el USTR para obtener del gobierno colombiano el compromiso de derogar el Decreto regulatorio de los medicamentos biotecnológicos, expedido por el gobierno anterior tras larga lucha de Misión Salud y otras organizaciones de la sociedad civil,]**el cual abre las puertas del mercado nacional a las versiones genéricas que demuestren calidad, seguridad y eficacia, lo que es fundamental para la salud pública si se tienen en consideración los precios exorbitantes e inasequibles de las versiones pioneras.**

[También para bloquear el proceso administrativo iniciado por el Ministro de Salud anterior, Dr. Alejandro Gaviria, también a instancias de la sociedad civil, cuyo objetivo es declarar de interés público con fines de licencia obligatoria el acceso a los antivirales de acción directa contra la hepatitis C (AAD), a fin de bajar los precios de estos productos, hoy cercanos a los \$100 millones el tratamiento de 12 semanas, a niveles que el sistema de salud pueda pagar sin comprometer su sostenibilidad financiera y la atención de otras necesidades sanitarias prioritarias. Una licencia obligatoria permitiría el ingreso al mercado de medicamentos genéricos con precios infinitamente menores, como los vigentes en Egipto, India y otros países (200 dólares el tratamiento de 12 semanas) o los que Médicos Sin Fronteras consiguió el año pasado para sus programas de ayuda humanitaria (120 dólares).]

[Basados en todo lo anterior, aprovechamos este espacio que generosamente nos brinda Contagio Radio para abogar por que la reunión no se realice. Porque nada bueno para el país se puede esperar de ella. El gobierno debe honrar la palabra del Presidente. No es comprensible por la opinión pública que mientras el primer mandatario y su Ministro de Comercio anuncian que durante este gobierno no habrá nuevos acuerdos comerciales, lo que todos celebramos y aplaudimos, la Cancillería promueva o acepte reuniones binacionales que van en sentido contrario.]

[Desde la sociedad civil hacemos un llamado urgente a la coherencia política en el conjunto de la acción gubernamental.]**No son aceptables iniciativas fragmentadas. Máxime cuando pueden estar en juego la salud y la vida de miles de colombianos.**[  ]

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

------------------------------------------------------------------------

#### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
