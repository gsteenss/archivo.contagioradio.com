Title: Se venció el plazo para el metro de Bogotá
Date: 2017-09-04 13:14
Category: Economía, Nacional
Tags: Enrique Peñalosa, Metro de Bogotá, metro elevado, sistema de transporte
Slug: plazos-metro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Peñametro-e1504548847329.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [04 Sept 2017] 

Luego de que se venciera el plazo, para que en Bogotá el Distrito cumpliera con el cronograma de estructuración técnica del primer tramo del metro elevado propuesto por Enrique Peñalosa, sectores políticos indicaron que **los costos para la realización del proyecto han incrementado** por haber desechado los estudios existentes del metro subterráneo. A la ciudad, ambos estudios le han costado 165 mil millones de pesos.

El concejal del Polo Democrático, Manuel Sarmiento, indicó que los incumplimientos en las fechas establecidas para la presentación de los estudios de ingeniería básica del metro elevado, **“demuestran una vez más la negligencia con la que está actuando la alcaldía de Peñalosa”.**

Dijo además que **ahora es poco lo que se puede hacer para que los bogotanos cuenten con un sistema moderno de transporte** que atienda las demandas de movilidad que genera la ciudad. Fue enfático en manifestar que “Peñalosa hace promesas que no puede cumplir”. (Le puede interesar: ["Revocatoria de Enrique Peñalosa podría realizarse en Noviembre"](https://archivo.contagioradio.com/revocatoria-a-enrique-penalosa-podria-realizarse-en-noviembre/))

Esto pues el actual alcalde había dicho que en diciembre de 2016 iniciaría la licitación del metro y eso no sucedió, luego se comprometió a que el 31 de agosto se realizaría este proceso y también se venció el plazo.

Los estudios del metro subterráneo que ya se habían realizado y terminado y “tuvieron tenido un costo de 140 mil millones de pesos ”. Sin embargo, cuando Peñalosa fue elegido alcalde **hizo una nueva inversión de 25 mil millones en nuevos estudios** que contemplan la construcción de un metro elevado que, según el concejal, no es viable y no tiene los estudios del suelo necesarios.

Ante las perdidas monetarias que ha ocasionado esto para la ciudad, el concejal advirtió que **la Contraloría de Bogotá deberá abrir un proceso de responsabilidad fiscal** “para que los responsables del detrimento patrimonial respondan con su patrimonio personal por la forma negligente como han actuado”. (Le puede interesar: ["Los millonarios contratos del gerente de un metro que no existe"](https://archivo.contagioradio.com/los-millonarios-contratos-del-gerente-de-un-metro-que-no-existe/))

### **No hay soluciones a corto plazo para mejorar el sistema de transporte público en la ciudad** 

Sarmiento aseguró que Peñalosa **“sigue empeñado en invertir dinero en Transmilenio para favorecer a los operadores privados** de un sistema que ya colapsó”. Ante esto dijo que es necesario que se modifiquen los contratos que benefician exclusivamente a los operadores privados no solo del sistema Transmilenio sino del SITP para así “hacer una reingeniería del sistema de transporte público”.

Finalmente, el concejal dijo que **el 23 de septiembre es la última fecha según el cronograma para completar los estudios del metro elevado**. De igual forma, tienen hasta el 11 de noviembre, cuando se cumple la ley de garantías, para completar los 10 requisitos necesarios que se solicitan para la financiación, hasta el momento ha cumplido con 4.

 Sin embargo, él cree que **a menos de un mes es difícil que la alcaldía haga todas las evaluaciones** y firme la financiación, “nos preocupa que Peñalosa actúe de manera irresponsable y que el estudio quede mal hecho por el afán”.

<iframe id="audio_20681915" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20681915_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
