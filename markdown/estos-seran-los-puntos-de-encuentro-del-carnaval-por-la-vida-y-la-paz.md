Title: Estos serán los puntos de encuentro del "Carnaval por la vida y la Paz"
Date: 2018-08-06 09:41
Category: Movilización
Tags: Ángela María, Carnaval por la vida y la paz, FARC, Gustavo Petro, Polo Democrático
Slug: estos-seran-los-puntos-de-encuentro-del-carnaval-por-la-vida-y-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/paziempre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paziempre] 

###### [06 Agosto 2018]

Este 7 de agosto se hará el Carnaval por la vida y la paz, una iniciativa que ha sido acogida por 82 ciudades en Colombia y 35 en el resto del mundo que busca pedirle al presidente Iván Duque que respete y continúe con la **implementación de los Acuerdos de paz, continúe los diálogos con el ELN y defienda los derechos de la ciudadanía. **

Esta iniciativa parte del llamado que hizo la bancada alternativa de oposición conformada por el partido Alianza Verde, el Polo Democrático, la alianza Progresista, la Lista de los Decentes, MAIS y FARC. (Le puede interesar:["En más de 90% de casos de asesinatos a líderes sociales aún no se alcanza justicia"](https://archivo.contagioradio.com/en-casos-de-lideres-sociales-no-se-alcanza-justicia/))

### **Puntos de encuentro en Colombia** 

1\. Bogotá Parque de La Hoja 1:00 pm  
2. Medellín Teatro Pablo Tobón 1:00 pm inicia recorrido hacia el Parque de Los Deseos llegada 3.00 pm  
3. Cali Boulevard del Río 1:00 pm  
4. Barranquilla Cementerio Universal 2:00 pm  
5. Santa Marta Parque de los Novios 9:00 am  
6. Bucaramanga Parque San Pio - Gorda de Botero 1:00 pm  
7. Cartagena Camellón de Los Mártires 9:00 am  
8. Montería Plaza Roja del P5 3:00 pm  
9. Villavicencio integración Parque Guayuriba 8:00 am, Concentración Parque Los Centauros 1:00 pm  
10. Popayán Plazoleta San Francisco 1:00 pm  
11. Riohacha Parque Simón Bolívar 2:00 pm  
12. Ibagué Parque Murillo Toro 1:00 pm  
13. Cúcuta Inicia Parque Santander 3:00 pm concentración Plaza de Banderas 4:00 pm  
14. Sincelejo Plaza El Progreso hasta el Parque Santander, 9:00 am  
15. Neiva Centro de Convenciones 9:00 am recorrido Carrera 7ª hasta la Calle 10ª y a la carrera 4ª hasta llegar Parque Santander  
16. Manizales Frente del Palacio de Justicia  
17. Pasto Plaza de Nariño 12:40 pm  
18. Tunja Parque Recreacional del Norte de Tunja 10:00 am  
19. Armenia Calle Peatonal Universidad del Quindío 2:00 pm  
20. Pereira Plaza de Bolívar  
21. Yopal parque la Herradura, 3:00 pm  
22. Quibdó Parque Centenario 1:00 pm  
23. San José del Guaviare Parque La Tarima 3:00 pm  
24. Valledupar  Parque del Barrio Garupal 5:00 pm  
25. Arauquita Inicia Asojuntas 8:00 am finaliza en concentración Parque General Santander 10:00 am  
26. Florencia Inicia en la Glorieta de los Colonos hasta el Parque Santander cerrando con un acto cultural, 8:30 am  
27. Dosquebradas inicia Frente Makro 1:00 pm movilización hacia Pereira  
28. Santa Rosa de Cabal  Parque Las Araucareas 1:00 pm  
29. La Dorada Parque Santander 3:00 pm  
30. Chinchiná Parque Bolívar esquina de la 9a con 11. 2:00 pm  
31. Barrancabermeja Inicia Puerto Yuma (Muelle) 2:30 pm Parque Camilo Torres 3:45 pm  
32. Apartadó Parque de Los Bomberos 4:00 pm  
33. Rionegro Plaza de La Libertad 1:00 pm  
34. Palmira  Parque de La Factoría 10:00 am  
35. Buga  Parque Santa Bárbara 3:00 pm  
36. Cartago Salida desde la Concha Acústica del Parque de la Isleta a las 1:00 pm para llegar al Parque de Bolívar  
37. Jamundí Parque principal  
38. Tuluá Plaza Cívica Boyacá 3:00 pm  
39. Yumbo Movilización Inicia Capilla de Pizarro Llegada Parque Belalcázar  
40. Tumaco Cancha San Judas 1:00 pm  
41. Zarzal Movilización calles principales 6:00 pm  
42. Ciénaga Plaza del Centenario 5:00 pm  
43. Pivijay Plaza Simón Bolívar 6:00 pm  
44. Plato Parque Santander 5:00 pm  
45. Guamal - Magdalena, Parque Centenario 9:00 am  
46. San Zenón Plaza Central 5:00 pm  
47. Banco Magdalena Plaza Roja 3:00 pm  
48. Mosquera- Funza Inicia recorrido Villanueva Mosquera 1:00 pm continua a Grival 2:00 pm finaliza en Funza Cundinamarca Parque principal 3:00 pm  
49. Cajicá Centro Cultural Cajicá 2:00 pm  
50. Chía Parque Principal 2:00 pm  
51. Tabio Parque Principal 1:00 pm  
52. Tenjo Parque Principal 1:00 pm  
53. Cota Parque Principal 1:00 pm  
54. Facatativá Parque Las Tinguas 1:00 pm A 5:00 pm  
55. Tocancipá Parque Principal 1:00 pm  
56. Fusagasugá Inicia Avenida Las Palmas Calle 6ª (Sabrosito) finaliza en la Plaza Pública 1:30 pm  
57. Girardot Parque Principal 1:00 pm  
58. Agua de Dios Parque Principal 1:00 pm  
59. La Calera Parque Principal 1:00 pm  
60. Suacha Frente Centro Comercial Mercurio 12:00 pm  
61. Zipaquirá Parque de La Independencia 1:00 pm  
62. Tenjo  Parque principal 1:00 pm  
63. Pitalito Parque José Hilario López 10:00 am  
64. Garzón -Huila, Parque Nazaret 10:00 am  
65. Maicao Plaza de Bolívar 9:00 am  
66. Saravena- Arauca, concentración Parque Modelo 9:00 am  
67. La Hormiga- Putumayo, concentración Parque Los Fundadores 1:00 pm  
68. Aguachica  Parque San Roque 5:00 pm  
69. Purificación -Tolima, concentración en el Parque del Antiguo Colegio Pérez 10:00 am  
70. El Guamo- Tolima, el punto de encuentro será Foto Guapo calle 10 No 8-61 centro pasa por colegio Caldas y llegada al Parque de Bolívar, 9:00 am  
71. Espinal Parque Bolívar 1:00 pm  
72. Libano marchará desde la Y a entrada del Líbano hasta el Parque Principal  
73. Duitama Plaza Los Libertadores 2:00 pm  
74. Chiquinquirá Parque Julio Flórez 2:00 pm  
75. Paipa Hotel Zue 2:00 pm  
76. Sugamuxi inicia en la plazoleta Iraka 10:00 am, se marcha por la calle 11 y carrera 11 finaliza en concentración Parque El Laguito hasta la 4:00 pm  
77. Acacias Parque principal, desde las 8:00 am  
78. Santander de Quilichao Las comunidades indígenas del norte de Cauca se movilizan  hasta Cali-Valle. Marchan desde el Parque Las Banderas hacia el Centro Administrativo Municipal CAM  
79. Ocaña Plazoleta San Francisco 2:00 pm  
80. Santo Tomás- Atlántico, Parque el Recuerdo 3:00 pm  
81. Soledad- Atlántico, Plaza de Soledad Museo Bolivariano 10:00 am

82\. Soacha el punto de encuentro será La Huerta El Porvenir a la 1:00pm

### **Concentraciones ciudades en el exterior** 

1\. Brisbane Australia Square, 3:00 a 6:00 pm Agosto 4  
2. Bahnhofplatz -Bern Suiza, 14:00-16:30 Hs Agosto 7  
3. Ginebra Suiza, Place des nations 1202, 17:00 H s Agosto 7  
4. Berna Siuza, Bahnhofplatz, 14:00 Hs Agosto 7  
5. La Haya Países Bajos, Embajada de Colombia finaliza International Criminal Court, 15:30 Hs Agosto 7  
6. Londres Inglaterra, Parliament Square, 16:00 Hs Agosto 7  
7. Bruselas Bélgica, Place de la Monnaie, 18:00 Hs Agosto 7  
8. Estocolmo Suecia, Segelstorg, 17:30 – 19:30 Hs Agosto 7  
9. Oslo Noruega Stortinget, Karl Johans Gate 22,0026, 18:00 Hs Agosto 7  
10. Edimburgo Escocia, West Parliament Square, 18:00 Hs Agosto 7  
11. Madrid España, Parque del Retiro, Puerta de Alcalá, 20:00 Hs Agosto 7  
12. Valencia España, Plaza de La Virgen, 20:00 Hs Agosto 7  
13. Barcelona España, PL de la Catedral, 20:00 Hs Agosto 7  
14. Ciudad de Elche España, Ayuntamiento de Elche (Alicante), 11:00 Hs Agosto 7  
15. Berlin Alemania, Branderburger Tor, 19:00 Hs Agosto 7  
16. Herdelberg Alemania, HauptstraBe 49, Anatomien Garten, 19:00 Hs Agosto 7  
17. München Alemania, Münchner Freiheit, LeopoldsBe 65, 14:00 Hs Agosto 7  
18. Mannheim Alemania, Paradeplatz, 19:00 Hs Agosto 7  
19. Hamburgo Alemania, Spritzenplatz, Altona, 18:00 Hs Agosto 7  
20. Viena Astria, Peace Museum, Blutgasse 3, 1010, 17:00 Hs Agosto 7  
21. Lisboa Portugal, Círculo de meditación por la vida/ Placa do comercio, 18:00 Hs Agosto 7  
22. Lyon Francia, Place de la Comédie, 69001, 19:00 Hs Agosto 7  
23. Londrés Inglaterra, Ground Floor Southbank Centre Entrada principal (frente al río)  
4pm Caminata hasta el Parliament Square Houses of Parliament, Agosto 7  
24. Miami Estados Unidos, 33132 401 Biscayne Blvd, 18:00 Hs Agosto 7  
25. Washington DC, Estados Unidos, Frente a la casa del embajador Q ST & 20TH ST NW, 5:30 Hs Agosto 7  
26. New York City Estados Unidos, Frente Naciones Unidas 50 Unied Nations Plaza (Firs Avenue and 46 Street), 5:00 pm Agosto 7  
27. Montreal Canadá, 1010 Rue Sherbrooke 0 Frente Consulado colombiano, 18:00 Hs Agosto 7  
28. Québec Canadá, Frente Chateaur Frontenac caminata hasta Radio Canadá, 18:00 Hs Agosto 7  
29. Montevideo Uruguay, Frente a la Facultad de Derecho de la Udelar (Av 18 de julio 1824), 18:00 Hs Agosto 7  
30. San José Costa Rica, Frente a la Corte Interamericana de Derechos Humanos, 12:00 m Agosto 7  
31. Ciudad de Panamá Panamá, Frente al consulado colombiano, 12:30 pm Agosto 7  
32. Buenos Aires Argentina, Obelisco BS AS, 18:00 Hs Agosto 7  
33. Mendoza Argentina, Plaza Independencia 17:00 Hs Agosto 7  
34. Caracas Venezuela, Frente a la Plaza Venezuela, 10:30 Hs Agosto 7  
35. Sao Paulo Brasil, MASP Av. Paulista, 1578 – Bela Vista, Sao Paulo, 17:00 Hs Agosto 7

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]{.s1}

<div class="osd-sms-wrapper">

</div>
