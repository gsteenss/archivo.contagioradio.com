Title: La seguridad decae en antiguas zonas de conflicto alerta informe de la ONU
Date: 2019-07-19 16:49
Author: CtgAdm
Category: Nacional, Paz
Tags: amenazas contra líderes sociales, Consejo de Seguridad de la ONU, etcr, Implementación del Acuerdo, Plan de Sustitución de Cultivos de Uso ilícito
Slug: la-seguridad-decae-en-antiguas-zonas-de-conflicto-alerta-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ONU.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ONU-informe.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

**Carlos Ruiz Massieu, jefe de la Misión de Verificación de la ONU** en Colombia dio a conocer el informe trimestral sobre la implementación del Acuerdo de Paz, que refuerza lo expresado por el Consejo de Seguridad de la ONU durante su visita al país, evidenciando la ausencia de seguridad para las poblaciones que habitan en zonas afectadas por el conflicto e identificando la falta de recursos y acceso a tierras, como uno de los mayores desafíos para la reincorporación de excombatientes.

Durante seis semanas, el Gobierno junto a integrantes de FARC y la Misión de la ONU realizaron recorridos en 10 Espacios Territoriales de Capacitación y Reincorporación (ETCR) para evaluar el  futuro de excombatientes, comunidades y autoridades locales, estas visitas permitieron concluir algunos aspectos fundamentales de la implementación que se sumaron a la visita del **Consejo de Seguridad los pasados 11,12,13 y 14 de julio.**

Ruiz Massieu explicó que el Consejo escuchó de las comunidades Cauca la situación de seguridad en antiguas zonas de conflicto. "En la reunión con líderes de la zona, incluidos grupos indígenas, afro colombianos, cooperativas y la iglesia, expresaron sus preocupaciones frente a distintos desafíos que dificultan su trabajo", agregó el embajador de Reino Unido, Jonathan Allen.

El embajador de China se refirió en particular a la situación de inseguridad derivada de la presencia de actores armados en el territorio que han tenido como consecuencia  el asesinato de 702 líderes sociales y 135 excombatientes desde firma del Acuerdo,  "la situación de seguridad sigue siendo muy frágil. Todas las partes deben seguir sus esfuerzos. Esperamos que todos le den prioridad a los avances de la paz.”

### "El Gobierno de Colombia debe dar respaldo político y financiero a las instituciones que trabajan por la paz."

El Consejo también se refirió al rol que debe cumplir la Jurisdicción Especial para la Paz (JEP), instando al Gobierno  que garantice su autonomía y funcionamiento, "e[speramos que la JEP sea el pilar principal por el medio de cual se fomente la confianza necesaria que cree un entorno político propicio, en miras a las elecciones de octubre", afirmó el secretario de la nación de Kuwait. ]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

De igual forma, se celebró el avance en el trabajo de la Comisión de la Verdad, “confiamos en que la tarea de la búsqueda de la verdad es un compromiso de todo el pueblo colombiano ” afirmó el embajador de Perú, Gustavo Meza-Cuadra quien encabezó la visita del Consejo el pasado fin de semana. [(Le puede interesar: Sabemos que hay desafíos para la paz, los hemos visto y escuchado: Consejo de Seguridad de la ONU)](https://archivo.contagioradio.com/sabemos-que-hay-desafios-para-la-paz-los-hemos-visto-y-escuchado-consejo-de-seguridad-de-la-onu/)

### ONU pide darle relevancia a la sustitución de cultivos

Este fue uno de los temas sobre las que el Consejo de Seguridad llamó la atención y lo necesario de desplegarlo en todas las regiones donde fue establecido, "una paz verdaderamente sostenible requiere avances complementarios en componentes Acuerdo Final, incluyendo reforma rural integral, sustitución de cultivos ilícitos, garantías de seguridad, reincorporación de excombatientes, justicia y reparación para víctimas”, indicó el jefe de la Misión.

### Aspectos cruciales para la reincorporación

Sobre la situación de los 13 ETCR que continuarán funcionando y la reubicación de otros 11, el Consejo consideró alentador la ampliación de los plazos para aclarar su situación jurídica, sin embargo precisó que para garantizar a los excombatientes un acceso a las actividades económicas, "el Gobierno debe desembolsar los recursos para el acceso a iniciativas económicas".

"Es urgente aumentar el número de proyectos productivos para los excombatientes y proporcionar la asistencia técnica necesaria para tener acceso a mercados para asegurar su sostenibilidad", agregó el diplomático.

Ruiz Massieu destacó la importancia de las elecciones locales de octubre de este año, las que "serán clave para la  reincorporación política de FARC", según el Jefe de Misión, el partido ha registrado más de 120 candidatos, de los que la mitad son excombatientes.

[Durante su intervención, Ruiz también advirtió que debe prestarse atención a los cerca de 8.000 excombatientes que viven fuera de los ETCR en asentamientos y zonas urbanas que deben ser  reconocidas para adaptar nuevas estrategias de reincorporación y garantías de seguridad. [(Le puede interesar: Crece la expectativa ante reubicación de 11 Espacios Territoriales de Capacitación y Reincorporación)](https://archivo.contagioradio.com/crece-la-expectativa-ante-reubicacion-de-11-espacios-territoriales-de-capacitacion-y-reincorporacion/)]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

Finalmente el diplomático reiteró que una paz verdaderamente sostenible requiere avances que incluyen una "reforma rural integral, la sustitución de cultivos ilícitos, las garantías de seguridad, la reincorporación de excombatientes, y la justicia y reparación para las víctimas.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
