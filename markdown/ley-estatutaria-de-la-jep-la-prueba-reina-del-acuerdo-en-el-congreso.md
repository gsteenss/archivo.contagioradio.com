Title: Ley estatutaria de la JEP, la prueba reina del acuerdo en el congreso
Date: 2017-05-25 13:37
Category: Nacional, Paz
Tags: acuerdos de paz, JEP, Ley estatutaria, proceso de paz
Slug: ley-estatutaria-de-la-jep-la-prueba-reina-del-acuerdo-en-el-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/518486_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana.com] 

###### [25 may 2017]

El Ministro de Justicia encargado, Fabián Marín Cortés y el Ministro del interior, Juan Fernando Cristo, **radicaron ante el Senado de la República el proyecto de Ley Estatutaria de la Administración de Justicia en la Jurisdicción Especial para la Paz (JEP).** En esta se establece el régimen legal propio, la autonomía administrativa, presupuestal y técnica y el carácter transitorio de la administración de justicia.

La JEP, es el mecanismo de justicia transicional acordado en la Habana con las FARC para investigar, **juzgar y sancionar a los máximos responsables de graves crímenes** cometidos con ocasión del conflicto armado en Colombia. Le puede interesar: ["General Uscátegui no cumple requisitos para acceder a la JEP: CCAJAR"](https://archivo.contagioradio.com/no-se-deben-conceder-libertades-a-militares-que-no-se-comprometen-con-la-verdad/)

El texto del Proyecto hace énfasis en que este mecanismo de justicia, conocerá las conductas cometidas antes del 1 de diciembre de 2016 que se encuentren **“por causa, con ocasión o en relación directa o indirecta con el conflicto armado”** y estos procesos tendrán como eje central los derechos de las víctimas.

### **Los 5 puntos de la Ley Estatutaria** 

Según un comunicado del Ministerio del Interior los detalles iniciales de la Ley Estatutaria giran en torno a 5 aspectos.

-   El proyecto establece que para acceder al tratamiento previsto en la JEP, es necesario **aportar a la verdad plena, reparar a las víctimas y garantizar la no repetición.**
-   Las normas procesales de este mecanismo de justicia van a regular las consecuencias del incumplimiento de cualquiera de las partes, teniendo en cuenta que pueden perder el tratamiento especial de justicia y sus beneficio. En el caso de las FARC, **su participación está sujeta a la dejación de armas.**
-   Dentro del compromiso por el respeto de los Derechos Humanos y el Derecho Internacional Humanitario, el Estado debe garantizar la **no repetición de los delitos cometidos contra la Unión Patriótica,** la inclusión de las minorías y la consulta a los pueblos indígenas.
-   Para su entrada en funcionamiento, las Salas del Tribunal de la JEP y la Unidad de Investigación y Acusación, deberán comenzar a operar tres meses después de que se posesionen los magistrados designados.
-   Estos organismos operarán por **los próximos 10 años con un plazo posterior de 5 años más** para concluir su actividad jurisdiccional.

El proyecto establece reglas claras sobre la aplicación de indultos, amnistías y mecanismos que se incluyen en el tratamiento especial penal para los agentes del Estado. Con la presentación de este Proyecto de ley estatutario, se espera que se designen los ponentes en **el Senado y la Cámara  para realizar el respectivo debate.**

### **La prueba reina del acuerdo en el congreso** 

Según Voces de Paz, el acuerdo, en el trámite que llevaba antes de la decisión de la Corte Constitucional de regular el mecanismo del Fast Track, **ha enfrentado una serie de situaciones que pretenden desvirtuarlo,** es por eso que esta ley estatutaria enfrentaría una fuerte oposición por parte de quienes le temen. Le puede interesar: ["Falsos positivos no deben pasar por la JEP".](https://archivo.contagioradio.com/38886/)

Uno de los puntos que más levanta voces en contra tiene que ver con la aplicación de la amnistía y la posibilidad de juzgar a integrantes de la Fuerza Pública que se comprometan con la verdad de los hechos. **Para el Centro Democrático esta medida equipara a las FFMM con la guerrilla lo cual sería inaceptable.** En ese aspecto Ivan Cepeda ha afirmado que no se le puede temer a la verdad y que ese derecho es uno de los primeros que debe garantizar la JEP.

Otra de las voces de oposición están en las altas cortes, según varios analistas, pareciera que las Cortes no están de acuerdo con la creación de un nuevo tribunal que podría entenderse como sustitutivo del aparato de justicia vigente. Sin embargo **los altos niveles de impunidad y la ineficiencia de judicial** fueron los principales argumentos para la creación de un tribunal independiente y este sigue siendo uno de los puntos a favor. Le puede interesar: ["JEP mantiene abierta la posibilidad para la impunidad" ](https://archivo.contagioradio.com/jep-mantiene-abierta-la-posibilidad-para-la-impunidad-wola/)

Según afirmó Ivan Cepeda, **es hora de que los partidos y los congresistas se quiten la máscara** y si respaldan el derecho a la paz no desdibujen el acuerdo, puesto que la posibilidad de presentar proposiciones sería una oportunidad para salvar las impunidades.

Así las cosas el trámite podría ser uno de los más largos, con mayor cantidad de proposiciones y con mayor riesgo al espíritu del acuerdo. **Se espera que la semana entrante comience su trámite con la discusión en la Comisión Primera de la Cámara.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
