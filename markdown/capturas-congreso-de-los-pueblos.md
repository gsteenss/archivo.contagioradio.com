Title: En 24 horas han detenido 3 líderes del Congreso de los Pueblos
Date: 2017-03-22 11:38
Category: DDHH, Nacional
Tags: capturas masivas, congreso de los pueblos, Fedeagromisbol
Slug: capturas-congreso-de-los-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/fedeagromisbol-congreso-de-los-pueblos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: notiagen] 

###### [22 Mar 2017]

Según la información preliminar que ha entregado la oficina de Derechos Humanos de la plataforma Congreso de los Pueblos, en las últimas 24 horas han sido detenidas 5 personas en la región del Sur de Bolívar. Según Zoraida Hernandez serían 3 los líderes del congreso de los pueblos y se intenta establcer si los demás detenidos son integrantes de esa misma plataforma.

Algunas horas después de la entrevista se logró establecer que se trata de 12 personas capturadas y sindicadas de ser parte de redes de apoyo del ELN en la región del Sur de Bolívar, en los municipios de Noris, Tiquisio y Morales. Esto según fuentes de la policía nacional. Todos ellos serían trasladados a la ciudad de Cartagena.

### **Los capturados** 

Hacia las 2 de la magrugada de este 22 de marzo fue detenida **Milena Quiroz**, una de las líderes detenidas es vocera de la Comisión de Interlocución del Sur de Bolívar, presidenta de varias cooperativas de campesinos, comunicadora popular y vocera de la región en la Cumbre Agraria Étnica y Popular. **Su casa también fue objeto de allanamiento**.

En el marco de estos operativos también fue detenido **Isidro Alarcón Bohorquez**, líder del proceso comunitario de Micoahumado en medio de una fuerte militarización del casco urbano en el que también se habrían realizado diversos allanamientos.

En la vereda Casa de Barro del municipio de Norosi, Bolívar, fue detenido, durante la madrugada de hoy, el líder **Manuel Francisco Zabaleta**, presidente de la Asociación de Mineros Casa de Barro.

Adicionalmente se conoció que sobre las 11 de la mañana de este 22 de marzo la SIJIN intentó detener a **Nancy Arévalo** mientras participaba de un foro sobre garantías de participación política.

Ya son varias las reacciones por parte de otras organizaciones así como de políticos como el representante Victor Correa, que exige libertad inmediata para estos líderes que serían víctimas de un proceso de ataque contra los movimientos sociales. En el mismo sentido se conoció un comunicado oficial del **Congreso de los Pueblos** que ya intenta establecer si se trata de montajes judiciales contra sus integrantes.

Hasta el cierre de esta nota se están presentando más denuncias en las que se da cuenta de otros operativos de captura contra integrantes de esa organización social. Por us parte el Seandor  Iván Cepeda habría asegurado que según las fuerzas de policía **serían 12 las capturas y en todas ellas se sindica a las personas de pertencer al ELN** y que serían trasladadas a la ciudad de Cartagena.

A continuación el comuncado de FEDEAGROMISBOL en que se relatan los procesos de captura de por lo menos 6 personas más en la región.

[Acción Urgente y Denuncia Pública Detenciones Sur de Bolivar](https://www.scribd.com/document/342709661/Accion-Urgente-y-Denuncia-Publica-Detenciones-Sur-de-Bolivar#from_embed "View Acción Urgente y Denuncia Pública Detenciones Sur de Bolivar on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_48053" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342709661/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-rReF4OFH2sVNHKC3hc4v&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
<iframe id="audio_17710637" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17710637_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
