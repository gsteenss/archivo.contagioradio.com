Title: Incremento y reorganización del paramilitarismo alerta a comunidades
Date: 2015-11-19 18:39
Category: DDHH, Nacional
Tags: Carta enviada por CONPAZ a La Habana, Conpaz, Diálogos de La Habana, ELN, FARC
Slug: incremento-y-reorganizacion-del-paramilitarismo-alerta-a-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitarismo-en-Choco-foto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

<iframe src="http://www.ivoox.com/player_ek_9455838_2_1.html?data=mpmil52XfI6ZmKialJWJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2FX08rax9PYs4ztjNfS0dfLpc%2Fd28bQy4qnd4a2lNOYxsrQb9HV08bay9HNuMLmytja0ZDFsMbm1caah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Martínez, vocero CONPAZ] 

###### [19 Nov 2015 ] 

[La Red de Comunidades Construyendo Paz en los Territorios **CONPAZ, ha enviado una carta** al presidente de la República Juan Manuel Santos, a Timoleón Jiménez y Nicolás Rodríguez Bautista, comandantes máximos de las FARC y el ELN respectivamente, ante la percepción de **estancamiento del actual proceso de paz.**]

[De acuerdo con Juan Martínez, vocero de CONPAZ, desde las comunidades se observan **avances positivos en los puntos hasta el momento acordados**, “en los acuerdos a los que ha llegado la mesa de negociaciones entre las FARC y el Gobierno vemos reflejadas propuestas construidas desde las bases, desde los territorios”, así mismo "hay satisfacción y una gran esperanza en los acuerdos finales dada la disminución del desplazamiento" en estos tres años.  ]

[Pese a ello, Martínez asegura que las comunidades sienten gran preocupación frente a la situación actual de las “**víctimas que fueron despojadas de sus tierras** entre los años 1997 y 2000 y que volvieron a su territorio entre 2004 y 2005, quienes **hoy en día se ven afectadas nuevamente**. Han sido revictimizadas por el **incremento y la reorganización del paramilitarismo** en las regiones, no solamente en Chocó, sino en otros departamentos de Colombia, incluso en ciudades como Barranquilla y Bogotá”. ]

[Según afirma este vocero, a las comunidades les genera gran preocupación el que la acción paramilitar ocurra frente a la presencia de las Fuerzas Militares, "no entendemos como el **Ejército** estando en el río Atrato con su batallón de Infantería de Marina, **permite la movilización de paramilitares** hacia las cabeceras municipales y las comunidades". Lo que **articulado con el accionar de sectores empresariales** revictimiza a quienes han regresado a los territorios.  ]

[Ante esta preocupación expresa, proponen la creación de una subcomisión étnica territorial integrada con organizaciones indígenas y negras como expresión de la gran diversidad de nuestro país y dada la necesidad de inclusión de las dimensiones étnicas en elementos fundamentales como la comisión de esclarecimiento de la verdad, agrega Martínez.]

[El vocero de CONPAZ, manifiesta a su vez el llamado al Gobierno nacional para que responda la carta enviada, puesto que las demás comunicaciones enviadas en las que comentan sus preocupaciones, propuestas e inquietudes sobre el actual proceso de diálogos, **no han sido respondidas por el presidente**, mientras que la comandancia de las FARC y el ELN han emitido sus respuestas.]

Conozca la carta enviada por CONPAZ

*[Señor Presidente]*

*[JUAN MANUEL SANTOS]*

*[Señor]*

*[TIMOLEÓN JIMÉNEZ]*

*[Comandante Máximo de las FARC EP]*

*[Señor]*

*[NICOLAS RODRÍGUEZ BAUTISTA]*

*[Comandante Máximo del ELN]*

*[Reciban un respetuoso saludo,]*

*[Nuestras comunidades y organizaciones de víctimas que construyen propuestas sobre la base que se ha dicho y escrito que las víctimas están y estamos  en el centro de los Acuerdos reiteramos nuestro respaldo al anuncio del pasado 23 de septiembre sobre la Jurisdicción Especial de Paz, JEP, pues hemos planteado y abogado desde marzo pasado en nuestra Asamblea Nacional por el Derecho Reparativo.]*

*[En carta pública, invitamos a todas las partes, incluyendo a la comandancia del ELN, en cabeza de Nicolás Rodríguez a pronunciarse. Este movimiento guerrillero se expresó respetuosamente, manifestando la necesidad de conocer los contenidos del acuerdo sobre JEP, que aún se mantienen bajo reserva, respetando la posición pública expresada por Iván Márquez de las FARC EP. Al mismo tiempo recibimos respuesta de las FARC-EP, para respetar profundamente los derechos de las víctimas. Aún seguimos a la espera de la respuesta del gobierno.]*

*[Si bien habrá asuntos que perfeccionar, hoy ante la percepción del estancamiento que nos informan los medios masivos, respecto a este tema de la JEP, que forma parte del Sistema Integral de Verdad, de Justicia, de Reparación y Garantías de No Repetición, les queremos plantear respetuosamente:]*

1.  *[Dado que el propósito es la consecución de las bases de una sociedad  hacia la paz, la Jurisdicción Especial de Paz, debe asegurar la igualdad ante la ley, tanto de todas las víctimas como de todos los indiciados, o postulados o requeridos.]*

-   *[Así,  los fueros existentes (para los eclesiásticos, los presidentes, los legisladores, los magistrados, los de facto para los empresariales, y para responsables de delitos que no pueden ampararse bajo el derecho a la rebelión), deben estar subordinados o abolirse sobre la primacía del derecho a la paz y a la justicia reparativa. Lo que debemos estimular es el propósito de construcción de ese nuevo país, y ahí la grandeza, la generosidad de la clase política, en particular de los exmandatarios es uno de sus mayores  aportes.]*

2.  *[Como el JEP pretende resolver asuntos de impunidad en crímenes de lesa humanidad, crímenes de guerra, entre otras, abogamos por la verdad como base para la sanción moral y social, no carcelaria, pues se trata de evitar chivos expiatorios, de lograr mecanismos reparadores desde la verdad que habiliten la Justicia Reparadora.]*

-   *[Desde este propósito, invitamos al gobierno dar a conocer en la mesa exploratoria con el ELN, lo acordado en la JEP, para que se vayan aclimatando y armonizando temas que son comunes a ambas mesas.]*

3.  *[Muchas de las víctimas de la violencia socio política y la sociedad en su conjunto hemos tenido serias limitaciones para acceder a todas las versiones en la ley 975, a las que existen de aforados en la Corte Suprema de Justicia, a las investigaciones ordinarias y a las que existen de los extraditados en los Estados Unidos, España y Alemania, estas deben ser parte de los insumos para la Comisión de la Verdad, pero también para las labores de la JEP.]*

-   *[Otro aspecto al que  nos queremos referir es a la necesidad de inclusión de las dimensiones étnicas, tanto en la composición de la Comisión de Esclarecimiento de la Verdad, así como, la JEP, asuntos que requieren antes de la firma de los acuerdos sobre víctimas, y el conjunto de los otros 5 puntos, el que se habilite, así como ha sucedido con la subcomisión de género, una sub comisión étnica territorial con organizaciones indígenas y negras, como la nuestra y otras que expresan la gran diversidad existente en nuestro país.]*

*[Agradeciendo su atención y en espera de su respuesta,]*

*[COMUNIDADES CONSTRUYENDO PAZ EN LOS TERRITORIOS (CONPAZ)]*
