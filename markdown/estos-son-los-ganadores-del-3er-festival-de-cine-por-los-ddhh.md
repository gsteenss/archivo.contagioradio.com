Title: Estos son los ganadores del 3er Festival de cine por los DDHH
Date: 2016-05-30 11:36
Author: AdminContagio
Category: 24 Cuadros
Tags: 3er Festival de cine por los DDHH, Fundación Konrad Adenauer, Ganadores Festival Cine DDHH, Impulsos Films
Slug: estos-son-los-ganadores-del-3er-festival-de-cine-por-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/13301516_949094328521973_5013127326325545455_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: 3er Festival de Cine por los DDHH 

##### [3 Jun 2016] 

Con el sabor de la música del pacífico de la talentosa cataora, compositora e instrumentista Inés Granja y el músico Juan David Castaño, el pasado 28 de mayo tuvo lugar en Bogotá la Ceremonia de premiación y clausura del 3er Festival Internacional de Cine por los DDHH de Bogotá.

El auditorio 'Fabio Lozano' de la Universidad Jorge Tadeo Lozano, acogió una vez más al público, invitados especiales, anfitriones y participantes del evento que durante 4 días adelantó actividades de exhibición y académicas en torno a la producción audiovisual con enfoque en Derechos Humanos.

Al evento, organizado por Impulsos Films, asistió como invitado especial  Cristian Steiner de la Fundación Konrad Adenauer (KAS), organización que patrocina el Festival, así como representantes de Hangar Films y los 18 integrantes del jurado encargado de seleccionar dentro de las [80 películas](https://archivo.contagioradio.com/estas-son-las-81peliculas-del-3er-festival-de-cine-por-los-ddhh/) participantes, las mejores en cada categoría.

El jurado integrados por Jorge Forero, Bladimir Sánchez , Juan Pablo Morris, Victor Gaviria, Carlos Hernandez, Ninoska Dávila, Gustavo Valencia Patiño, Paola Pérez, Mauricio Aristizábal, Carlos Hernando Sánchez, Antonio Girón, Patricia Ortega, Víctor Palacios, Simón Santos, Jairo Carrillo, Juan Carlos Cocha, Oriol Marcos y Alina Hleap. eligió los ganadores por calidad y pertinencia, el jurado escogió los siguientes trabajos audiovisuales como los ganadores.

**Mejor Película**  
*La última noticia*  
Dir. Alejandro Legaspi  
País: Perú

**Mención especial**  
*La semilla del silencio*  
Dir. Felipe Cano  
País: Colombia

**Mejor Documental Nacional**  
*Ríos de crudo*  
Dir. Bianca Bauer  
País: Colombia

**Mejor Documental Internacional**  
*Hija de la laguna*  
Dir. Ernesto Cabellos  
País: Perú

**Menciones especiales**

*Noche herida*  
Dir. Nicolás Rincón Gille  
País: Bélgica / Colombia

*Dibujando memorias*  
Dir. Marianne Eyde  
País: Perú

**Mejor Cortometraje Nacional**  
*Rojo como la leche*  
Dir. Santiago Londoño Gallego  
País: Colombia

**Mención especial**  
*Volver*  
Dir. Iván Luna  
País: Colombia

**Mejor Cortometraje Internacional**  
*No es nada*  
Dir. José Antonio Cortés  
País: España

**Mejor Película de Animación**  
*Un 9 de abril*  
Dir. Édgar Álvarez  
País: Colombia

**Nuevos Realizadores**

*Amordiscos*  
Dir. Cristian Barreto  
País: Colombia  
Premio del público

*Historia de la paz*  
Dir. Luis Alejandro Pérez  
País: Colombia  
Premio del jurado

*Utopía posible*  
Dir. Daniel Londoño  
País: Colombia  
Premio del jurado
