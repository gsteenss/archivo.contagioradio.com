Title: Glifosato: una aclaración y tres observaciones
Date: 2015-05-19 19:21
Author: CtgAdm
Category: Carolina, Opinion
Tags: Cauca, fumigaciones, Glifosato, ministerio de defensa
Slug: glifosato-una-aclaracion-y-tres-observaciones
Status: published

[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) - ([@E\_vinna](https://twitter.com/E_Vinna))

El anuncio de la suspensión del uso del glifosato en Colombia ha sido, sin duda, un avance en materia de derechos humanos. Desde el inicio de las fumigaciones en 1978 las comunidades rurales han expresado los diversos problemas que derivan del uso de este químico: desde afectaciones directas a la salud hasta la pérdida de los cultivos de pancoger. Celebro que tras 37 años de oídos sordos, el Gobierno por fin decidiera ponerle fin al uso de este mortal compuesto.

Sin embargo, es necesario realizar una aclaración sobre el tema y dejar sobre la mesa tres observaciones que son, más bien, preocupaciones.  La aclaración es que sí bien el Ministerio de Salud elevó la solicitud de la suspensión del uso del glifosato al Consejo Nacional de Estupefacientes (CNE) para que suspendiera las fumigaciones, **el CNE** **suspendió el** **uso del glifosato, únicamente, sobre los cultivos ilícitos.** Es decir, no está prohibido en el uso agrario. Y aún más, no se suspendieron las aspersiones en general, de hecho, se reveló en medios que **un equipo de expertos del Ministerio de Defensa está trabajando en la creación de un mapa de los sitios donde se podría continuar asperjando**, según ellos, en zonas lejanas a poblaciones, pero vaya uno a saber.

Una vez hecha esta aclaración, se pueden mencionar las tres preocupaciones que derivan del tema. La primera es que, a pesar de tanto anuncio, **la orden de suspensión del uso de glifosato no se cumpla y que continúen las aspersiones en zonas pobladas.** Para evitar que esto ocurra es necesario crear un mecanismo de verificación que constate la progresiva reducción del uso del glifosato y tener los oídos atentos a las denuncias de la población rural.

La segunda observación va encaminada a los recientes anuncios que indican que la Policía Antinarcóticos estaría realizando pruebas de entre cinco y siete moléculas de herbicidas que podrían eventualmente reemplazar al glifosato. **Hay que tener cuidado con el uso de estos nuevos herbicidas y de otros agentes químicos, porque no sería la primera vez que los colombianos cambiaríamos un mal por otro mal mayor.** Este el momento para que las universidades que han dedicado parte de su investigación a la botánica, agroecología, química y otras ciencias similares den a conocer sus estudios y se pronuncien de forma propositiva sobre las alternativas que existen al glifosato y otros agentes químicos.

La tercera preocupación me surgió en una reciente conversación con un habitante del Valle del Cauca: **puede que, con la excusa de mantener el control sobre los cultivos ilícitos, se aumente la militarización en las zonas rurales de Colombia, incluso en las que nunca había ingresado la fuerza pública**. ¿Cuál es el inconveniente? Que la suspensión del glifosato sea la excusa para aumentar el pie de fuerza y la presencia militar, una acción que va en contravía del “desescalamiento” del conflicto armado y podría poner a la población civil en medio del campo de batalla.

En conclusión, es necesario realizar un seguimiento juicioso a la suspensión del glifosato y los efectos colaterales de esta importante decisión para que no terminemos peor de lo que estábamos. En cuanto a la erradicación de los cultivos ilícitos las organizaciones campesinas, indígenas y afrocolombianas han sido muy claras en su propuesta: el Estado debe fortalecer y apoyar integralmente los planes de sustitución de los cultivos de coca. Ese acompañamiento del gobierno no puede ser a través de la militarización del territorio, sino de acciones precisas encaminadas al crecimiento económico, la sostenibilidad de los proyectos propios de los campesinos y la inversión en el campo colombiano.
