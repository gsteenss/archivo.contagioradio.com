Title: 2do día del foro Diálogos de paz y libertad de prensa
Date: 2015-05-08 07:41
Author: CtgAdm
Category: eventos, Nacional
Tags: FLIP, Foro Diálogos de paz y libertad de prensa, Libertad de Prensa, Marcha de Antornchas, medios de comunicación
Slug: continua-el-foro-dialogos-de-paz-y-libertad-de-prensa
Status: published

###### Foto:Contagioradio.com 

El segundo día del **Foro Internacional, Diálogos de paz y libertad de prensa** organizado por la Fundación para la libertad de Prensa en Colombia ha comenzado con la ponencia ¨Modelos y experiencias internacionales de medios públicos¨ con la presencia de **Guilherme Canela,** de la oficina regional de la **UNESCO para Montevideo**, Uruguay, en dicha ponencia se ha tratado la temática de los **medios de comunicación públicos y su papel en la democratización de la sociedad. **

En el conversatorio sobre el impacto de la **pauta publicitaria en la libertad de prensa**, los periodistas de FLIP, Juán Sebastián Salamanca y Jonathan Bock han presentado una investigación realizada en todas las regiones colombianas sobre la adjudicación de las pautas, la acumulación por parte de determinados medios de comunicación, el **uso indebido por parte de las alcaldías y la perdida de independencia por parte de los medios de comunicación** a la hora de su financiación.

En la misma ponencia también hemos podido escuchar la voz de **Gustavo Gómez de OBSERVACOM**, quién ha explicado la futura **ley de regularización de las pautas publicitarias** que se está debatiendo en el parlamento Uruguayo, como alternativa para poder impedir la acumulación por parte de los medios y su uso partidario.

En el foro también se ha debatido sobre las **diferencias que existen en el cubrimiento en las regiones y desde las capitales**. En este panel se ha contado con la presencia de Pedro José Arenas, periodista del Guaviare y Diana Pedraza , periodista de El Espectador.

En la discusión entablada se ha **criticado** por parte del público la **poca pedagogía por la paz desplegada por los medios** y se ha entablado un debate sobre el papel educativo de estos en el seno de la sociedad. Arenas ha puntualizado que en las **regiones la unica información proviene de las radios del ejército** o de la policía y que los medios nacionales también recurren a ella.

Por la tarde se presentó el **informe del Observatorio de Medios de Comunicación** para la Autoridad Nacional de Televisión de Colombia, en el que se resaltaba que los medios de comunicación Colombianos eran en su totalidad **dependientes de la institucionalidad**, además el informe apuntaba hacia la **escasa presencia de minorías étnicas, de la mujer o del colectivo LGBT** y también de la poca programación referida al género de ficción u otros que no tuviesen que ver con opinión pública, siendo este el mayoritario.

La presentación del informe dio paso a el último panel en el que participaron, distintos representantes de medios como Señal Colombia, Telecafé o el representante del ejército para las emisoras de la institución militar.Este conversatorio creó polémica al espetarle varios de los asistentes al **representante militar que habían contribuido mediante las 116 emisoras que tienen en el país a desinformar y crear una cultura de guerra en las regiones.**

Por último los asistentes acudieron al acto organizado por la secretaría de Integración social de la Bogotá Humana en el parque de los periodistas que consistía en un concierto de música popular y como **símbolo de la libertad de prensa y en recuerdo de los periodistas fallecidos** defendiéndola se encendió una **llama entre los distintos representantes institucionales.**

Entrevista con **Jonathan Bock** periodista de FLIP sobre el informe del impacto de la **pauta publicitaria en Colombia:**

<iframe src="http://www.ivoox.com/player_ek_4465435_2_1.html?data=lZmjl5mXeY6ZmKiak5WJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2Fa0Nfax5DXs8PmxpDdw9rYpYzk1sfZy8jNuMLmysaYx9OPh9Dg0NLPy8aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Entrevista con **Paula Martínez** periodista de Mapa de Medios en Colombia sobre el Informe **"Medios públicos en Colombia"**:  
<iframe src="http://www.ivoox.com/player_ek_4465451_2_1.html?data=lZmjl5mZdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRic%2Fo08qY2M7XuMKfxNTbjbXFuc3VjLLO1NmJh5SZoqnbx9%2BPqMafrsbdw5DIqYzBxsnW0diPqc%2BfpNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Entrevista con **Pedro José Arenas**, periodista del **Guaviare** sobre el cubrimiento en las regiones de Colombia:  
<iframe src="http://www.ivoox.com/player_ek_4465458_2_1.html?data=lZmjl5mZfI6ZmKiakp2Jd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYssrIttCfq9Tgh6iXaaKtjKbfx9PFt4ampJDdx9fNs8Xd1NnOjcnJsIy71sbjy5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
 

**Llama por la libertad de prensa**:

\
