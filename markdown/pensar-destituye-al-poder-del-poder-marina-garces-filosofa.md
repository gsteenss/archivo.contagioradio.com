Title: “Pensar destituye al poder del poder” Marina Garcés, filosofa española
Date: 2018-02-11 09:07
Category: Cultura, Entrevistas
Tags: Cartagena, Cultura, hay festival
Slug: pensar-destituye-al-poder-del-poder-marina-garces-filosofa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Marina-Garces-hay-festival.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ana María Rodríguez 

#### [**Por Carolina Garzón para Contagio Radio**] 

[**Contagio Radio** entrevistó a la **filósofa española Marina Garcés** quien estuvo de visita en Colombia para el **Hay Festival Cartagena 2018**. Durante nuestra conversación con ella, la autora de los libros “]*[En las prisiones de lo posible”]*[ y “]*[Filosofía inacabada”,]*[nos habla sobre la crisis de pensamiento que atravesamos en la actualidad y cómo la herramienta política más potente en estos tiempos es cultivar la confianza. “Pensar destituye al poder del poder”, es una de las frases que nos da Marina Garcés al analizar la naturaleza subversiva del pensamiento.]

**Contagio Radio:**[ En entrevistas anteriores ha mencionado que el mundo está “en un momento de crisis y que ante los abismos que se abren reaparecen preguntas radicales.” ¿Cuáles son esas preguntas radicales?]

**Marina Garcés:** [Las preguntas radicales son las que van a la raíz de los problemas. No sólo aquellas que lo rompen todo sino que lo abren todo para podernos poner a pensar. Yo creo que estas preguntas radicales son radicales hoy porque alcanzan el mismo sentido de lo humano, no como una pregunta retórica o teórica sino porque las condiciones de lo vivible hoy están en cuestión: tremendas desigualdades, crisis ecológica ambiental, escasez de recursos en el planeta. Es decir, hay toda una serie de cuestiones que nos ponen a pensar ¿cómo sostener una vida digna hoy?]

[Una vida digna implica, no una de privilegios para unos cuantos y de bienestar para unos pocos, sino condiciones compartidas de vida buena para todos. Cuando esto se pone en cuestión, está cuestionando el sentido mismo de lo humano. Estas son las preguntas radicales de nuestro tiempo que después se despliegan y se desgranan en interrogantes concretos en cada contexto, porque no hay preguntas únicas para todos. Yo creo que la cuestión es esta.]

**C.R:**[ Cuando surgen tantas preguntas hay una necesidad humana de encontrar respuestas. ¿Qué pasa con las respuestas a esas preguntas radicales y con algo que Marina ha denominado “el solucionismo”?]

**M.G:** [Tenemos una concepción muy estrecha de lo que es dar respuesta, pensamos que tenemos que dar soluciones inmediatas y, además, exitosas a los problemas o a cuestiones por resolver. Entonces, cualquier cosa que no sea “una solución inmediata y exitosa” deja de tener valor y en vez de atrevernos a pensar lo que no sabemos cómo pensar, en vez de atrevernos a experimentar con las formas de vida, con las tomas de decisiones colectivas, con las maneras de vivir, consumir y producir y de amar, buscamos soluciones rápidas.]

[¿Quién tiene soluciones rápidas? Quien tiene el poder de darlas: los expertos, las empresas, las corporaciones y los gurús, es decir, los que “venden recetas” en vez de mantenernos en la posición de construir las respuestas juntos. Ahí, en la construcción conjunta, es donde nacen las verdaderas respuestas y a lo mejor no son perfectas ni son acabadas. No creo en las cosas acabadas, de hecho, uno de mis libros es “Filosofía inacabada”, es decir, aprender a estar en lo que queda por hacer. Eso es lo que mantiene vivo el presente y lo que permite que sea un presente entendido en colectivo, en plural.]

[Hay que recuperar este sentido de la respuesta. Responder es poder responder a las situaciones que nos toca vivir no tener soluciones para todo.]

**C.R:** [En algunas de las conferencias en las que participó en el Hay Festival mencionó que “la filosofía nace en la calle” y a algunos les sonó extraño por aquella idea de una filosofía bastante lejana. ¿A qué se refiere con que nace y existe en la calle?]

**M.G:**[ La filosofía en la historia de la academia y de la cultura occidental se presenta como una cosa muy académica, en el sentido más incomprensible y especializado, o muy extraña ligada a personajes casi “frikis”. Hay todo un folklorismo del filósofo como un personaje extraño, alejado de la comunidad, inhábil para la vida práctica, que está en sus cosas, que tropieza por la calle y no tiene familia. Esto es una falsedad histórica que construye estos imaginarios para separar al común de la gente de su potencia de pensar, y pensar radicalmente, y de poder pensar las cosas siempre de otra manera, que es lo que propone la filosofía. No dar nada por acabado, nada por sabido, nada por obvio. Pensar es poder volver a pensar, es tan simple como eso y poder preguntar ¿por qué lo hemos pensado de esa manera? ¿Y cómo podríamos pensarlo de otra manera? Es una potencia que tiene todo el mundo.]

[La filosofía es una herramienta de encuentro y de interpelación. Es un encuentro con los otros, y ese encuentro es la calle, no solamente físicamente, sino en el sentido de que es allí donde pasa la vida.]

**C.R:** [¿Tenemos tiempo para pensarnos el mundo? Cuando se tiene la idea de que debemos estar produciendo y pensar es inútil o un desperdicio. ¿Hay tiempo para hacerlo?]

**M.G:**[ Esta es una pregunta muy importante porque es uno de los grandes obstáculos que hacen que esta potencia que tenemos de pensar no florezca, es porque no encuentra tiempo. No encuentra espacio, pero tampoco encuentra tiempo. Yo me encuentro con mucha gente, incluso gente con responsabilidades culturales o educativas importantes, es decir, que están en puestos de responsabilidad porque trabajan con la vida y con el sentido de la vida de otros, y me dicen “Es que no tengo tiempo de pensar lo que hago”. Entonces, no sólo quien está todo el día en un trabajo mecánico o físico no tiene problemas para pensar, incluso también desde trabajos cognitivos no tenemos el tiempo.]

[Claro, es una paradoja porque estamos utilizando la mente, pero la utilizamos de una manera que no piensa, ¿qué significa eso? Que para pensar hay que parar. Y en lo que consiste la explotación de la vida hoy, tengamos trabajo o no, cobremos o no, sea como sea la manera en la que vivimos hoy, es sin poder parar. Incluso cuando estamos descansando seguimos conectados a nuestras redes, siempre hay mensajes que contestar aunque sean los más banales e insustanciales, pero no podemos parar. La gran condición para mí, para pensar, es poder parar y eso implica hoy en día, casi un acto de resistencia colectiva. Hay que construir espacios para detenerse.]

[¿Por qué las palabras tienen sentido? Porque entre ellas hay espacios en blanco. Pues la vida es eso, podemos decir cosas porque podemos crear espacios en blanco que nos permiten encontrar las palabras que verdaderamente importan. Esa interrupción es necesaria para que lo que podemos pensar tome sentido.]

**C.R:**[ Menciona en otras entrevistas la palabra “miedo”. A veces nos da miedo pensar y pensarnos. ¿Qué ha percibido sobre el miedo a pensar y cómo lidiar con él?]

**M.G:**[ Una de las razones por las que no podemos parar]*[no es porque no podamos]*[ en el sentido objetivo de la palabra. Es porque nos da miedo. Nos invade una cultura en la que “parar es abismarse en algo que nos despertará todo tipo de males”, entonces, llenamos y rellenamos continuamente nuestra atención de estímulos que alejen esos miedos. Claro, esa es la mala estrategia contra el miedo. Los miedos están, porque además nuestro mundo no es que sea precisamente para no tener miedo, pero si no se les deja expresarse, si sólo los alejamos, los tapamos o los disimulamos, ya sea con protección, distracción o seguridad, no nos abandonan. Siguen operando. Es mejor enfrentarlos, mirarlos de frente, darnos los contextos y las maneras de estar juntos que nos permitan acoger estos miedos y deshacernos de ellos.]

[¿Cómo se deshacen los medios? Dando confianza. La herramienta política más potente en estos tiempos de tanta violencia, destrucción e incertidumbre, es cultivar la confianza. No tanto la esperanza, que es hacer promesas de futuro, sino generar las condiciones para que confiemos unos en otros. Eso nunca es general, no se confía en todo el mundo en abstracto, es muy concreto: se trata de generar redes de confianza desde lo más básico, en relaciones de base, horizontales, recíprocas, íntimas pero también públicas, desde la vida privada y política. Esa es la confianza que permite relacionarnos con lo que no sabemos, dejar de apartar, negar, tapar lo que no sabemos y relacionarnos con lo que desconocemos de nosotros y del mundo. Ahí hay un empoderamiento político claro, una capacidad de estar en el mundo y recuperar la capacidad de actuar en él: con miedos, pero no desde los miedos.]

**C.R:** [En otras ocasiones ha mencionado que “el pensamiento es subversivo”. En Colombia esta palabra ha sido cargada negativamente a lo largo de la historia reciente, ¿a qué se refiere con esta frase y cómo apropiarnos del pensamiento?]

**M.G:** [Las palabras tienen sus contextos y yo las utilizo desde los que vivo. Yo entiendo subversivo en un sentido muy literal: “subvertir” es girar algo, es poner de patas arriba cualquier cosa, es hacer ese gesto de girar las cosas.]

[En este sentido la filosofía es subversiva porque no acepta las cosas tal como se nos presentan, por ejemplo los discursos de poder, los saberes técnico-científicos, los valores morales, etc. Es decir, ante todo aquello que dice “esto es lo que hay”, la filosofía lo gira. Esto es subversivo porque pone patas arriba lo obvio, lo que se supone que es evidente, y permite pensar “quizá no es tan evidente y hay otras formas posibles de ver las cosas”. Eso es subversivo, además, porque destituye al poder del poder.]

[El poder se basa siempre en enunciar un discurso, una manera de hacer las cosas, en decir “el mundo es así y tú debes vivir de esta manera”, así la subversión entendida como esa posibilidad de poner patas arriba lo que sabemos y lo que somos también es algo que destituye al poder de su poder de representar al mundo y de decirnos cómo tenemos que vivir.]

<iframe id="audio_23618762" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23618762_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
