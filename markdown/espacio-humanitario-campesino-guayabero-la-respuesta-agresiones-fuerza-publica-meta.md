Title: Espacio Humanitario Campesino del Guayabero, una alternativa para defender la vida
Date: 2020-11-05 19:09
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Erradicación Forzada, Espacio Humanitario Campesino del Guayabero, Meta, PNIS
Slug: espacio-humanitario-campesino-guayabero-la-respuesta-agresiones-fuerza-publica-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Guayabero.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/57940cae-601e-4cc9-a3db-8c931b2e5bcc.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Guayabero Archivo Personal/ @IAP\_Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el acompañamiento de al menos [15 organizaciones de la sociedad civil e internacional,](https://twitter.com/IAP_Colombia) se ha construido y consolidado el **Espacio Humanitario Campesino del Guayabero en la vereda Nueva Colombia en Vista Hermosa, Meta** cuyo objetivo es entablar espacios de diálogo con las comunidades campesinas y el Ejército para evitar que se reproduzcan las violaciones a los DD.HH. que han ido en aumento en el marco de la erradicación forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según **Camila Escobar, trabajadora socia**l y quien acompaña el proceso, en al menos cinco municipios del sur de Meta y del San José del Guaviare se han identificado las problemáticas de fondo que afectan no solo a comunidades campesinas sino a población reincorporada, por lo que se ha buscado con esta construcción, crear un marco de protección bajo el amparo del Derecho Internacional Humanitario y beneficiar a las comunidades campesinas que se encuentran alrededor del Espacio Humanitario. [(Le recomendamos leer: Organizaciones sociales convocaron a un Pacto por la Vida y la Paz)](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Históricamente, señala **Óscar Martínez, coordinador regional del Espacio Humanitario Campesino del Guayabero,** la región se ha caracterizado por la creación de diferentes juntas de acción comunal y crear un modelo de desarrollo pensado desde las necesidades de las personas del territorio ante la ausencia del Estado que en medio del contexto del conflicto armado acusó en numerosas ocasiones a la población de ser un foco de la entonces guerrilla de las FARC, dando un tratamiento de guerra hacia las comunidades campesinas que hoy en día persiste.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para **Juan David Espinel, secretario técnico de la red Llano y Selva** dicho trato se ve reflejado en la militarización de la región y el despliegue de operaciones como el Plan Artemisa que según el Gobierno busca recuperar parques y zonas naturales de la deforestación, desplazando a sus habitantes de dichas áreas; cabe señalar que municipios como **Macarena, Vista Hermosa, Puerto Rico y Puerto Concordia,** y sus veredas no tienen titulación por encontrarse en dichas zonas y no cuentan con la posibilidad de sembrar, en una zona de múltiples recursos naturales y que resulta ser la transición georgráfica entre el ecosistema amazónico y la Orinoquia, además,

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hoy en día en el Meta, operan conjuntamente **la Fuerza de Tarea Conjunta Omega, el Comando Especifico de Oriente y la Fuerza de Despliegue Rápido (FUDRA),** que acompañadas del ESMAD han sido responsables de disparos contra campesinos, ataques a la prensa, quema de cultivos, destrucción de casas y sucesos similares que han sido denunciados.

<!-- /wp:paragraph -->

<!-- wp:image {"id":92324,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Guayabero](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/57940cae-601e-4cc9-a3db-8c931b2e5bcc-1024x472.jpg){.wp-image-92324}  

<figcaption>
Archivo Personal

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Crisis de seguridad alimentaria tiene consecuencias en El Guayabero

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El coordinador del espacio relata que pese a ser tierras fértiles que no requieren de mucha inversión para cultivar con éxito, **el trasladar un producto como el plátano, que cuesta 1.000 pesos por kilo para ser llevado a otra región como Guaviare, no permite una ganancia pues el mismo kilo se vendería a 600 o 700 pesos,** "trabajar a pérdida nadie lo hace en este momento, los campesinos se han dedicado al cultivo de pancoge y al único que es rentable que es la coca", relata.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque según el Espacio Humanitario, en las visitas realizadas se constató que no hay grupos de narcotráfico en la zona y las comunidades han manifestado que exista la voluntad de erradicar de forma voluntaria, se ha acudido nuevamente a la planta de coca ante experiencias fallidas del Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS) en Guaviare y el incumplimiento de los Acuerdos por parte del Gobierno pues contrario a lo acordado ha privilegiado la erradicación forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Si se trata del PNIS, programa del que Gobierno ha reconocido tiene problemas para cubrir a las cerca de 100.000 familias que hacen parte de la iniciativa, el coordinador afirma que en esta región únicamente se ha dado a las familias dos o tres pagos de los seis que estaban programados para el 2020. [(Lea también: Persecución y desplazamiento campesino: el resultado de incumplimiento del Gobierno en Guaviare)](https://archivo.contagioradio.com/persecucion-y-desplazamiento-campesino-el-resultado-de-incumplimiento-del-gobierno-en-guaviare/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que pese a que el campesinado ha señalado que este transitar a otros proyectos productivos debe ser gradual y verificado, los pocos que han llegado a las comunidades han sido inconsistentes en su calidad y en la salud de animales como gallinas ponedoras y cerdos, mientras persisten los altos costos para las semillas que han impedido el desarrollo de una seguridad alimentaria y mantiene en un limbo los proyectos productivos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Erradicación forzada es responsabilidad directa de Iván Duque

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El coordinador señala que la erradicación forzada no solo es generadora de violencia sino de una crisis humanitaria, "entendemos que es una orden presidencial y en ese sentido la responsabilidad directa de la crisis de derechos humanos es del presidente Iván Duque". [(Lea también: Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano)](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de ese ejercicio, se ha denunciado además que la Fuerza Pública ha cometido excesos que se han venido denunciando por parte del Espacio Humanitario. [(Lea también: Operativo de erradicación forzada en Guayabero deja 4 heridos y 20 campesinos retenidos por las FF.MM)](https://archivo.contagioradio.com/guayabero-erradicacion-forzada/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Algunos de estos casos se dieron a lo largo del mes de octubre, incluidos los del 7 y 8 de octubre de 2020. cuando soldados del **Ejército dispararon a los pies de los campesinos en la Vereda La Tigra, Puerto Rico**, mientras otro uniformado propinó un culatazo con su fusil a un campesino que intentó grabar un proceso de erradicación forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, organizaciones de DD.HH. como la Corporación Claretiana Norman Pérez Bello han manifestado su preocupación por las amenazas proferidas por los militares en contra de las comunidades campesinas y el mismo Espacio Humanitario Campesino que verifica y defiende los derechos en dichas veredas de la región del Guayabero.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El diálogo como solución al conflicto

<!-- /wp:heading -->

<!-- wp:quote -->

> "Los campesinos se sienten como en la década de los noventa cuando estaban en medio de dos fuegos enemigos, es necesario que se revisen esos estatutos de seguridad vigentes desde hace más de una década que plantean que el enemigo es el campesino, porque realmente son los héroes de este país".
>
> <cite>Óscar Martínez</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que han sucedido hechos atribuibles a grupos ilegales, presuntamente disidencias de las FARC, los que generan preocupación, e inestabilidad en la población campesina, desde el Espacio Humanitario destacan que las verdaderas garantías las ofrece el mismo apoyo de la comunidad y la perspectiva que se busca de un diálogo participativo, intergeneracional y despojarla del concepto de que son invasores de un territorio, sino de que son víctimas de una política de Estado.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
