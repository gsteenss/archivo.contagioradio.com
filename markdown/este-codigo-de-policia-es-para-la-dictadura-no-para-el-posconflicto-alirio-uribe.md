Title: "Código de Policía es para una dictadura, no para el posconflicto": Alirio Uribe
Date: 2016-06-16 16:29
Category: DDHH, Nacional
Tags: código de policía, congreso colombia, Nuevo Código de Policia en Colombia
Slug: este-codigo-de-policia-es-para-la-dictadura-no-para-el-posconflicto-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/operativos-de-policc3ada-en-caldas-e1466179873321.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Santafe] 

###### [16 Junio 2016] 

A pupitrazo, a altas horas de la noche, y sin siquiera leer el articulado completo, el Congreso está aprobando el nuevo Código de Policía que permitiría capturas administrativas tanto de mayores como de menores de edad, el ingreso de policías a colegios y universidades, arrestos de quienes transiten en la calle en estado de embriaguez, allanamientos sin orden judicial, y por si fuera poco, **facultaría a los alcaldes para determinar toques de queda, "legalizando lo que han venido haciendo los paramilitares en muchas zonas del país"**, según afirma Alirio Uribe, representante a la Cámara por Bogotá.

"Es un código prohibicionista que lo único que va a lograr es que los colombianos odien a los policías; **no va a cooperar a la convivencia sino que va a generar abusos permanentes**;** **es un código hecho por policías para otorgarle poderes exorbitantes a la Policía, y el Congreso no lo objeta, no lo analiza, ni lo discute"; agrega el Representante.

El articulado del Código también prohíbe el uso del espacio público para intervenciones artísticas y culturales, y para las ventas informales de las que viven cientos de familias en Colombia, además **faculta a los policías para que ingresen a las salas de cine a verificar qué edad tienen los asistentes**, facilita el reclutamiento forzado y la imposición de multas por cualquier motivo; en suma, "interviene en la vida, la libertad y la intimidad de los ciudadanos", como asegura Uribe.

El código de Policía "se ha venido aprobando sin ningún debate, sin ninguna discusión". Antes de llegar a la plenaria, la Comisión Primera aprobó 173 artículos en bloque y **este miércoles se aprobaron, también en bloque, 119 artículos**, bajo el pretexto de que no habían proposiciones, cuando éstas han sido presentadas ante la inconstitucionalidad del articulado.

"Es un código peligrosista que parte de la lógica de que los ciudadanos son delincuentes, de que los jóvenes son peligrosos, igual que los niños y las niñas. Invierte todos los principios de un Estado social de derecho", y se convierte en un código inaplicable, "para dictaduras, y que no sirve para el posconflicto ni la convivencia ciudadana", por lo que  **algunos de los congresistas lo llevarán a la Corte Constitucional apenas lo sancionen**.

<iframe src="http://co.ivoox.com/es/player_ej_11928937_2_1.html?data=kpamlJ2dd5ihhpywj5aXaZS1kpyah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpCajbfJtNPZ1Mrb1sbSuMafsdTZ0ZCoqc7jxNeSpZiJhZLoysjcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
