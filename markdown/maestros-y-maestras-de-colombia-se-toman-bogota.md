Title: Maestros y maestras de Colombia se tomaron Bogotá
Date: 2015-02-27 13:10
Author: CtgAdm
Category: Nacional, Otra Mirada
Tags: ade, educacion, fecode, gina parody, maestro, ministerio, profesor
Slug: maestros-y-maestras-de-colombia-se-toman-bogota
Status: published

El jueves 26 de febrero, maestros y maestras de todo el país se reunieron en Bogotá en una gran movilización convocada por Federación colombiana de educadores, FECODE.

El objetivo de la movilización, era radicar un pliego de exigencias ante el ministerio de educación nacional, con demandas fundamentales para el ejercicio docente: un régimen especial en salud para las y los maestros, un incremento salarial pertinente, y nuevo modelo de asenso y nivelación salarial.

\[embed\]https://www.youtube.com/watch?v=IQrZ86JEaoc&feature=youtu.be\[/embed\]
