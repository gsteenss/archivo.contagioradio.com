Title: "Sin transformación social no hay libertad sexual"
Date: 2015-05-08 15:10
Author: CtgAdm
Category: closet, LGBTI
Tags: Alejandra Rendón, Closet Abierto, Derechos Humanos, Homosexualidad, Igualdad, LGBTI, Polo de Rosa, Polo Democrático Alternativo, Transformación Social
Slug: sin-transformacion-social-no-hay-libertad-sexual
Status: published

##### Foto: Contagio Radio 

##### <iframe src="http://www.ivoox.com/player_ek_4457921_2_1.html?data=lZmimZ6WdY6ZmKiakpaJd6Koloqgo5qbcYarpJKfj4qbh46kjoqkpZKUcYarpJKSp5eJfJGZmqjAy9OPuNPVz9jT0dfRpcTdhqigh6eXsozn0MjWw9GPstCfycbmjdHNpsbm1cbRjdjJvNbVzYqylIqcdIatpZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Programa 27 de Abril] 

La transformación social parte de la influencia cultural que tiene la política sobre la población mundial, específicamente en Colombia, el tema político predomina e impone ciertas normativas que en varios aspectos limitan a las minorías en su libre desarrollo.

Una de las minorías de las que se habla es la comunidad LGBTI, su libertad de expresión depende casi en un 100% de las decisiones políticas y legislativas que se toman en el país y aun así, los partidos políticos no se han comprometido con esta minoría, pues desde sus ideales no hay iniciativas que busquen respaldarlos de alguna manera.

El Polo Democrático Alternativo es el único partido político en Colombia que cuenta con una vertiente LGBTI llamada Polo de Rosa, que se encarga de abrirse a nuevas posibilidades que signifiquen avanzar en materia de derechos para la comunidad, todos luchando por un mismo objetivo que es la transformación social.

La invitada Alejandra Rendón se encarga de sentar un precedente sobre la influencia que tienen los jóvenes en la política haciendo énfasis en políticas de diversidad sexual como la del Polo de Rosa, demostrando que por medio de esta se puede emprender una lucha por los derechos humanos y lograr transformaciones de vital importancia en nuestra sociedad.
