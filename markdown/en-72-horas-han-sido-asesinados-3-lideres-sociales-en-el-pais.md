Title: En 72 horas han sido asesinados 3 líderes sociales en el país
Date: 2017-10-20 14:58
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, defensores de ddhh, lideres sociales
Slug: en-72-horas-han-sido-asesinados-3-lideres-sociales-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Asesinatos-DDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Siglo] 

###### [20 oct 2017] 

La situación de garantías para la seguridad de los líderes y defensores de derechos humanos en Colombia se agrava con el pasar de los días. En 72 días fueron asesinados 3 líderes sociales, muertes que se suman a la cifra de la Defensoría del Pueblo que ha registrado **186 homicidios entre 2016 y 2017.**

### **Liliana Astrid Ramírez Martínez** 

Era profesora en el departamento del Tolima del colegio de la vereda San Miguel en la zona rural de Coyaima. De acuerdo con la información, la profesora fue asesinada **el 19 de octubre** cuando se bajaba de un taxi a la entrada del colegio. Los sicarios huyeron en una moto tras haberle disparado en varias ocasiones.

Otros docentes han denunciado que **han sido víctimas de amenazas de muerte** en repetidas ocasiones y no ha habido una reacción por parte de las autoridades. La comunidad ha interpretado el asesinato como un atentado contra el derecho a la educación y contra las acciones de liderazgo que ejercía la profesora. (Le puede interesar: ["Asesinan a Liliana Astrid Ramírez, docente y lideresa en Tolima"](https://archivo.contagioradio.com/asesinan-liliana-astrid-ramirez-tolima/))

### **Eliecer Carvajal** 

La Federación Comunal del Putumayo, denunció que **el 18 de octubre** fue asesinado en el municipio de Puerto Garzón, el fiscal de la vereda Bajo Cañoavena. Aún no se conoce quienes fueron los responsables del asesinato ni los motivos del mismo, por lo que la Federación le exigió a las autoridades que “brinden en el menor tiempo una explicación de este asesinato y se capture a los responsables”.

Cabe resaltar que ésta organización social ha denunciado en varias ocasiones las **amenazas de las cuales han sido víctimas sus dirigentes**. Argumentan que no ha habido una acción por parte de las autoridades que garantice la protección de los líderes sociales “comprometidos con la convivencia, el desarrollo y la paz con justicia social”. (Le puede interesar: ["Violencia contra líderes sociales y defensores, el mayor obstáculo para la paz"](https://archivo.contagioradio.com/a-mayor-posibilidad-de-participacion-mayor-es-la-violencia-contra-lideres-informe/))

### **Liliana Patricia Cataño Montoya** 

Las autoridades de Medellín, en Antioquia, indicaron que la líder social de 39 fue asesinada **el 18 de octubre** producto de 4 impactos de bala en el sector de La Loma en la comuna 13 donde vivía. Indicaron que Cataño lideraba procesos de protección y defensa de tierras, por lo que esto pudo haber sido la causa de su asesinato.

De acuerdo con el diario El Colombiano, el secretario de Seguridad de Medellín afirmó que en el 2017 han sido asesinadas de forma violenta **45 mujeres en esa ciudad.** Dijo que esto representa un aumento del 50% con respecto al año anterior. (Le puede interesar: ["¿Porqué fue asesinado Jair Cortés, promotor de la sustitución de cultivos"](https://archivo.contagioradio.com/48032/))

### **Paren de matarnos** 

En varios espacios y desde diferentes sectores sociales **se ha exigido que se garantice el derecho a la vida de los líderes y defensores**,  en medio de la jornada de la indignación, se han realizado movilizaciones en el país en donde han exigido que cese la represión, la estigmatización, que se combatan las estructuras criminales y se aceleren en las investigaciones sobre los asesinatos y las reiteradas amenazas de muerte.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
