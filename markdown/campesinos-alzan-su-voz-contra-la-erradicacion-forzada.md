Title: 3 mil campesinos de Cauca rechazan erradicación forzada
Date: 2017-02-09 18:55
Category: Nacional, Paz
Tags: acuerdos de paz, coca, cocaleros, Cultivos de coca, erradica, Erradicación Forzada
Slug: campesinos-alzan-su-voz-contra-la-erradicacion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_0383-770x400-e1486684946745.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Remap Valle] 

###### [9 Feb. 2017] 

**En Mercaderes, Cauca más de 3 mil campesinos se dieron cita para rechazar la erradicación forzada** que se está llevando a cabo en sus territorios desde el 26 de Diciembre en contra vía de lo acordado en La Habana. De igual modo exigieron al Gobierno implemente los  Planes Integrales Comunitarios y Municipales de Sustitución y Desarrollo Alternativo PISDA. Le puede interesar: [Erradicaciones forzadas de coca no contribuyen a la paz](https://archivo.contagioradio.com/campesinos-del-catatumbo-dicen-no-a-erradicacion-forzada-de-coca/)

Y es que está no es la primera vez que una comunidad campesina expresa su indignación frente a la erradicación de cultivos ilícitos de manera forzada, desde inicios del 2016 cultivadores de coca han estado denunciando que pese a que hagan parte de los planes de sustitución de cultivos ilícitos continúan siendo agredidos, amenazados por parte del Ejército y **"señalados de ser narcotraficantes".**

Acusaciones que ponen en riesgo la vida de los campesinos, y que no están en la misma vía de lo pactado en **los acuerdos de paz de la Habana así como de su implementación. **

Por ello, este lunes se realizó el lanzamiento de la **Coordinadora de Cultivadores de coca de Mercaderes como alternativa para aunar esfuerzos** en la implementación del punto cuatro de los acuerdos de paz. Le puede interesar: ["Plan de sustitución voluntaria no se está cumpliendo"COCCAM](https://archivo.contagioradio.com/plan-de-sustitucion-voluntaria-no-se-esta-cumpliendococcam/)

De esta coordinadora hacen parte los lugares que tienen mayor presencia de cultivos de coca en sus territorios como Catatumbo, Magdalena Medio,  Arauca, Nariño, Cauca, Putumayo, Guaviare, Córdoba, Antioquia y Chocó.

En una entrevista reciente dada por Cesar Jerez, vocero de la Asociación Campesina del Catatumbo a Contagio Radio aseguró que "hay más de 500 puntos en donde el gobierno quiere llevar a cabo erradicaciones forzadas y a**llí encontrarán una resistencia y acción masiva de protesta de los sectores cocaleros”.** Le puede interesar: [“Erradicación forzada y sustitución no son compatibles con la implementación”: Cesar Jerez](https://archivo.contagioradio.com/erradicacion-forzada-y-sustitucion-no-son-compatibles-con-la-implementacion-cesar-jerez/)

Por su parte, La Coordinadora municipal de Cultivadores y Trabajadores de Coca de Mercaderes -Coccam- ha dicho que apoya la sustitución voluntaria acordada en el punto 4 de los acuerdo de La Habana, pero que **rechazan la erradicación forzada que han estado realizando los militares pues desconoce completamente los acuerdos de paz.** Le puede interesar: [Erradicaciones forzadas son otra manera de matar al campesinado](https://archivo.contagioradio.com/erradicaciones-forzadasotra-manera-de-matar-al-campesinado/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
