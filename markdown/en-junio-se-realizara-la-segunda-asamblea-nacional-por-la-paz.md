Title: En junio se realizará la segunda Asamblea Nacional por la Paz
Date: 2015-02-05 19:24
Author: CtgAdm
Category: Movilización, Paz
Tags: asamblea nacional por la paz, obreros, paz, USO
Slug: en-junio-se-realizara-la-segunda-asamblea-nacional-por-la-paz
Status: published

###### Foto: USO 

<iframe src="http://www.ivoox.com/player_ek_4043916_2_1.html?data=lZWhlZ6Veo6ZmKiak5WJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8bi1dfOzpDXrc%2FYysjOzpDHs8%2Fq0MjOjcaPdsXVjKbgw9LGsMbVjLPOxc7TssLgjNXc1JDQpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Jairo Ramirez, Comité Ejecutivo de la USO] 

La **Unión Sindical Obrera** -USO-, en diálogo con **ECOPETROL y el gobierno nacional**, han acordado la realización de la 2da **Asamblea Nacional por la Paz en junio del 2015.**

La primera Asamblea se desarrolló en 1994, según explica Luis Jairo Ramirez, miembro del Comité Ejecutivo de la USO. Para esta central sindical, el **proceso de paz de La Habana** y el que espera el país que se formalice en los próximos días con el **ELN**, están en peligro gracias a las maniobras de la extrema derecha y el ministro de Defensa, y es **responsabilidad de organizaciones sociales y sociedad civil** garantizar su continuidad y exigir "de las partes, una **tregua bilateral** que le permita a la población quitarse de encima los bombardeos y la represión contra la movilización popular"

La **Asamblea Nacional por la Paz tendría una convocatoria "amplia, plural e incluyente**" que no se limitaría a las organizaciones sindicales y movimientos sociales, tradicionales en este tipo de discusiones-; también espera reunir a sectores académicos, empresariales y a la iglesia. Su pluralidad y amplitud serían garantizadas por la realización de 4 pre-asambleas en **Barrancabermeja, Cartagena, Villavicencio, Arauca, Neiva, así como Tibú, Orito y Caquetá**, para preparar los temas con los diferentes sectores sociales convocados.

La asamblea tendrá como eje principal la discusión sobre "**recursos naturales, conflicto  y la paz**". Sin embargo también abordará la problemática de **crisis de DDHH**, la desmilitarización de la sociedad, garantías a las libertades sindicales, la ley de seguridad ciudadana y fuero penal militar.
