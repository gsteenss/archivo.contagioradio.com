Title: Ruta de autoprotección colectiva del consejo comunitario Guajuí, Guapi
Date: 2020-10-08 11:37
Author: CtgAdm
Category: yoreporto
Slug: ruta-de-autoproteccion-colectiva-del-consejo-comunitario-guajui-guapi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/SLS09687-scaled.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/SLS09687-1-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*Por: Fernando González Santos*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La sistematización que aquí se presenta da cuenta del proceso de construcción de la Ruta de Autoprotección del Consejo Comunitario del Río Guajuí.  En ella, se recoge el enfoque metodológico, los criterios y estrategias para su construcción, como también, los resultados e impactos de dicha experiencia acorde con la cultura ancestral y la visión étnica de las comunidades afrodescendientes con quienes se construyó la ruta.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta experiencia fue acompañada por Pensamiento y Acción Social – PAS, en su contribución de aumentar las capacidades de organizaciones, comunidades y personas que defienden los derechos humanos, principalmente los asociados a la tierra y el territorio, que enfrentan elevados niveles de riesgo y agresión, para que gestionen de manera autónoma su seguridad desde una perspectiva de prevención, protección colectiva y autoprotección. En este proceso de construcción del enfoque de la Ruta de Autoprotección participa activamente la Coordinación de Consejos Comunitarios y Organizaciones de Base del Pueblo Negro de la Costa Pacífica del Cauca, COCOCACUA; un espacio en el que confluyen los consejos comunitarios, las organizaciones sociales y de víctimas de comunidades negras y que promueve la reivindicación de los derechos humanos y la paz, desde un enfoque etno-cultural. De COCOCAUCA, hacen parte 8 consejos comunitarios y cerca de nueve organizaciones de base.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La construcción del enfoque de esta Ruta de Autoprotección con comunidades negras afrocaucanas, nos permite reconocer una experiencia particular de educación popular, en vínculo con los postulados de la Investigación y Acción Participativa y la concepción ancestral. En esta dirección, la Escuela Itinerante con que se lleva a cabo el proceso formativo advierte lo que podríamos llamar la condición entre *protección* y *riesgo* para las comunidades, de acuerdo con tres aspectos: Amenazas, vulnerabilidad y capacidades:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuando los conviteros, como espacio de organización fundamental, en la Escuela Itinerante hablan de amenazas se refieren a cuestiones, tales como: el asesinato de líderes, la presencia y control de los grupos armados en el territorio, la estigmatización de los y las líderes, el hostigamiento y la falta de reconocimiento de las autoridades tradicionales por parte de las instituciones locales y nacionales. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuando se refieren a las vulnerabilidades se plantean asuntos como: la pérdida de espacios de reunión, el debilitamiento organizativo, la división comunitaria, el irrespeto a la autoridad ancestral, los chismes de la población, la cooptación de los líderes por parte de los grupos armados, los incentivos al individualismo y las rupturas familiares. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y cuando se refieren a las capacidades mencionan: la fuerza colectiva del territorio, las mingas, las fiestas patronales, las relaciones de parentesco, las iniciativas cooperativas, los lazos de solidaridad, los arrullos, los cantos, la producción agrícola, la pesca, la participación y el liderazgo de las y los conviteros. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al respecto, puede afirmarse que lo que los conviteros en ese proceso de Investigación y Acción Participativa van dilucidando que la protección no proviene de un factor externo a manera de ley, de estrategia de seguridad, de intervención solamente gubernamental; sino que, sin negar todas estas herramientas que en algún momento han de utilizar, la capacidad viene de sus propias fuerzas como grupo étnico, con toda su cosmovisión, sus tradiciones y sus hábitos. Aquí radica en gran medida la estrategia planteada por el Enfoque de la Ruta de Autoprotección, lo que tiene efectos en las redes de confianza de los grupos étnicos para posicionarse en la vida pública de la propia comunidad.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El haber recreado una narrativa de la historia comunitaria, acorde con las comunidades negras, su origen, sus luchas, su propio movimiento y sus formas de resistencia, permitió valorar sus tradiciones y expresiones ancestrales, pero a la vez retomar nuevamente el sentido de su existencia como pueblo y como raza, puesto que la versión histórica de lo ancestral adquiere desde la práctica pedagógica algo muy importante y que tiene que ver con la idea de resistencia colectiva y con la de conciencia. En la primera, se logran retomar aquellas conquistas de los pueblos afro y darle todo el realce posible, lo que íncide directamente en la participación de las 8 comunidades y sus representantes. Frente a la conciencia, no solo se habla de información, ni siquiera de comprensión, sino de memoria colectiva. Y esta es uno de los aprendizajes más importantes con las comunidades. Memoria, viéndolo desde el saber significa origen, raíz e identidad. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para conocer más a profundidad el tema, lo invitamos este jueves, 8 de octubre a participar del conversatorio “Protección Colectiva: Una Alternativa para la Protección de la Vida de las Comunidades y los Territorios” que se realizará a las 4:00 pm y se transmitirá por el FACEBOOK LIVE de Pensamiento y Acción Social- PAS ([www.facebook.com.co/pensamientoyaccionsocial/live](http://www.facebook.com.co/pensamientoyaccionsocial/live)).  Aquí, con la participación de las autoridades del territorio, representantes de ONG y también de la institucionalidad se analizará a fondo los hallazgos de la Ruta y se discutirá frente a la importancia de implementar estos enfoques al trabajar la protección en comunidades afrodescendientes.  

<!-- /wp:paragraph -->

<!-- wp:block {"ref":88544} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
