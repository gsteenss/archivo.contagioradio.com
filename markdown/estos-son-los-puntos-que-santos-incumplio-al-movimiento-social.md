Title: Estos son los puntos que Santos incumplió al movimiento Social
Date: 2016-05-29 19:14
Category: Nacional, Paro Nacional
Tags: Cumbre Agraria Campesina Étnica y Popular, Paro Agrario
Slug: estos-son-los-puntos-que-santos-incumplio-al-movimiento-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/paro-e1464566821984.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Carolina Moreno] 

###### [29 May 2016]

El pasado 27 de mayo diferentes voceros de la Cumbre Agraria, Étnica y Popular convocaron a la sociedad colombiana a juntarse y apoyar el paro Nacional que iniciará el **próximo 30 de mayo**.

De acuerdo con Robert Daza, vocero de la Cumbre Agraria, Étnica y Popular, se hizo un llamado a paro debido a los múltiples incumplimientos por parte del gobierno del presidente Santos a los **acuerdos pactados en el año 2013 y 2014 en las mesas de interlocución que se gestaron en ese entonces y las reuniones del año 2015 con la Minga Indígena**, por otro lado el paro también responde a las diferentes políticas sociales y económicas que **profundizan un modelo de explotación**  como lo es la ley ZIDRES o la concesión de títulos mineros en zonas de reserva.

Estos son los acuerdos que han pactado en las diferentes mesas y reuniones de negociación entre el gobierno y las vocerías de la Cumbre Agraria, Étnica y Popular y que hasta el momento no se han cumplido

1\. Tras la expedición del decreto 870 del 8 de mayo de 2014 por el que se conforma la Mesa Única de Participación y Concertación, que sería el espacio en donde interlocutarían el gobierno y la Cumbre Agraria Étnica y Popular sobre los 8 puntos del pliego de exigencias, **hasta el momento no se ha abordado ninguno de los puntos**: 1. Tierras, territorios colectivos y ordenamiento territorial; 2. Economía propia contra el modelo de despojo; 3. minería, energía y ruralidad; 4. cultivos de coca, marihuana y amapola; 5. Derechos políticos, garantías, víctimas y justicia; 6. Derechos sociales; 7. Relación campo-ciudad; 8. Paz, justicia social y solución política.

2\. En ese mismo escenario se generaron 5 mesas de trabajo conformada por delegados del gobierno y delegados de la Cumbre Agraria, Étnica y Popular, una de esas mesas **tendría como tema el Incoder y la restitución de tierras a campesinos, actualmente no solo no se ha avanzado en esta discusión** sino que se generó la ley Zidres que según el vocero Robert Daza "despoja de tierras a los campesinos y campesinas para otorgárselo a los grandes empresarios y multinacionales", esta ley ha sido duramente criticada por senadores como Jorge Robledo y en la[actualidad afronta una demanda radicada por el senador Alberto Castilla y respaldada por la Cumbre Agraría Étnica y  Popular](https://archivo.contagioradio.com/polo-partido-verde-y-cumbre-agraria-radican-demanda-contra-zidres/).

3\. Otra de las mesas que se creó debatiría el tema de garantías y derechos humanos, al igual que las judializaciónes a miembros del movimiento social, sin embargo las r**espuestas por parte del gobierno han sido nulas y por el contrario aprueba leyes como la de Seguridad Ciudadana que crea sanciones más fuerte y criminaliza la protesta social**. De otro lado, también propone reformas al Código de Policía que de ser aprobado daría más libertades a esta fuerza pública sin un debido control. Ambos hechos se reflejan según Luis Fernando Arias vocero de la Cumbre Agraría Étnica y Popular y de la ONIC, en el **aumento de la represión y pie de fuerza**, las amenazas a líderes del movimiento social, detenciones arbitrarias y la [violación de derechos humanos por parte de la fuerza pública en diferentes departamentos del país](https://archivo.contagioradio.com/esmad-deja-varios-heridos-en-marcha-de-comunidades-negras-en-cauca/).

4\. Durante los paros del año 2013 y 2014 se acordó con el gobierno generar otra mesa de interlocución que t**rataría el tema del agro y que escucharía las propuestas de los voceros de la Cumbre Agría, Étnica y Popular frente al tema de tierras.** De acuerdo con los voceros **ninguna de las propuesta ha sido escuchada ni contemplada** por parte del gobierno, [por ende expresan que se oponen al Plan Nacional de Desarrollo que según ellos pone en venta los recursos estratégicos](https://archivo.contagioradio.com/plan-nacional-de-desarrollo-mercantilizacion-de-la-naturaleza-y-desastre-ambiental-anunciado-1er-parte/) de la Nación, da cabida a la privatización de derechos fundamentales como la salud y la educación y privilegia a través de los Tratados de Libre Comercio y otros mecanismos a las multinacionales y empresarios.

5\. De igual forma se habría acordado una participación **más directa de las comunidades sobre los planes de desarrollo locales o las decisiones sobre su territorio** como los títulos mineros, sin embargo las desviación de ríos o la explotación minera en diferentes regiones del país, s[in la aprobación o consulta previa a las comunidades](https://archivo.contagioradio.com/cerca-de-2-millones-de-hectareas-del-departamento-del-cauca-estan-en-manos-de-la-locomotora-minera/), ratifica de acuerdo con el vocero Daza, no solo el incumplimiento por parte del gobierno sino la falta de voluntad del mismo.

6\. Frente al acuerdo de generar un Financiamiento de Proyectos Productivos por parte del Ministerio de Agricultura y Desarrollo Rural, las diferentes vocerías han señalado que no existe voluntad por parte del gobierno, ya que **se han venido dilatando todas las reuniones y no se ha entregado ninguna respuesta formal por parte del mismo**, estos hechos generan que se haya postergado por más de un año el acuerdo de Apoyo a la Economía Propia, evitando que se pueda cultivar tan siquiera una hectárea de alimentos y que genera por otro lado, que se amplíen los cultivos ilícitos; a lo que el gobierno ha respondido con políticas [de erradicación forzada y militarización de algunos territorio](https://archivo.contagioradio.com/gobierno-incumple-acuerdos-en-erradicacion-de-cultivos-de-uso-ilicito-en-putumayo/)s.

Se espera que el próximo 30 de mayo el movimiento social en cabeza de los sectores que conforman la Cumbre Agraria Étnica y Popular salgan a las calles en lo que será la Minga Nacional Agraria, Campesina, Étnica y Popular, bajo la consigna de "Por un Buen Vivir, una reforma Agraria Estructural y una Ciudad digna".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
