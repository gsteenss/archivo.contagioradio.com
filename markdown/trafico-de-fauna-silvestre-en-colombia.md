Title: Tráfico de fauna silvestre en Colombia
Date: 2015-02-01 20:23
Author: CtgAdm
Category: Voces de la Tierra
Slug: trafico-de-fauna-silvestre-en-colombia
Status: published

##### Foto: [traficoilegaldefaunayfloraencolombia.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4025183_2_1.html?data=lZWfl5acd46ZmKiakpaJd6KkmIqgo5idcYarpJKfj4qbh46kjoqkpZKUcYarpJK80MnFb6LiytLOzs7XuMKfjpDB1Iqnd4a1ksvWxdSPqMafx8bi0MaPt8rg18rg1tfJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En el anterior programa de Onda Animalista hablamos sobre una de las problemáticas que más agrede los derechos de los animales. Se **trata del tráfico de fauna silvestre, una actividad incrementa en época de vacaciones**, no solo por personas que deciden lucrase con este negocio, sino también por los turistas, que muchas veces deciden llevarse a sus hogares como mascotas, animales que ven como exóticos, desconociendo el daño que les hacen.

Los animales son víctimas del ser humano, cuando son sacados de sus hábitats y pasan por proceso de maltrato que finalmente **puede llevarlos a la muerte.**

**Colombia es reconocida como la cuarta nación en biodiversidad mundial,** la primera nación en anfibios y aves, la tercera en reptiles y quinta en mamíferos, según datos de la Secretaría Distrital de Ambiente.

Cifras oficiales resaltan que **en la primera mitad de año del 2014 en el país se incrementó un 26%** el número de especies víctimas del tráfico de animales. Las guacamayas, loros, monos, tortugas, aves y serpientes son los animales que más se usan para comercializar, un negocio que después del tráfico de drogas y armas, es la tercera actividad criminal más lucrativa en el mundo.

Generalmente se piensa que el único responsable del comercio ilegal de animales es la persona que trafica, pero **también es culpable, y así mismo será judicializado quien consuma o participe de esta actividad.**
