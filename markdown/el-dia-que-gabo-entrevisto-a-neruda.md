Title: El día que Gabo entrevistó a Neruda
Date: 2018-03-06 13:51
Category: Cultura, Viaje Literario
Tags: Gabriel García Márquez, Literatura Latinoamericana, Pablo Neruda aniversario muerte
Slug: el-dia-que-gabo-entrevisto-a-neruda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/gabo-neruda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Tan solo dos días después de recibir en Estocolmo el premio nobel de literatura, Pablo Neruda le cumplió la cita a su gran amigo y colega Gabriel García Márquez para conversar. Una charla que a manera de entrevista puede ser disfrutada en el presente gracias a la presencia en ese histórico momento de una cámara de televisión.

La exclusiva fue para la Televisíón Nacional de México y el deleite para todos los amantes de las letras. Para la fecha en que se presentó el encuentro, García Márquez llevaba 16 años alejado del periodismo, profesión que había ejercido desde 1948 a nivel regional en el diario el Universal de Cartagena y en 1954 para todo el país en el diario el Espectador, por lo que, por algunos minutos, representó el retorno a esta faceta para el escritor.

Hablaron sobre la realidad vista en el ejercicio de cada uno en su campo: poesía y la literatura, "El poeta tiende a alejarse de la realidad viva, de la realidad actual" apuntó Neruda "he envidiado la condición del novelista que tiene, de alguna manera u otra ese acceso directo al relato a contar cosas que se ha abandonado la poesía" añadió.

Por su parte, García Marqúez reflexionó sobre la forma en que lo lírico aparece en su trabajo "tengo la tendencia verdaderamente a ir convirtiendo el relato, la novela, en poesía ... casi lo que estoy haciendo y la aspiración en mi trabajo es encontrar mas bien soluciones poéticas que narrativas.

Desde el inicio de la conversación, Neruda insiste en la necesidad de volver a la poesía épica en la forma en que lo hacían clásicos como Homero y Dante, aquella que "contaba algo" y que en su juicio estaba perdida en la nueva generación de escritores, asi como a la poesía didactica "yo propuse hacer que se enseñara cosas con mi poesía".

Gabo destaca la necesidad de coexistir novelista y poetas de manera pacífica, y propone que "los poetas sean cada día más narradores y los novelistas cada vez más poetas" mientras que Neruda asegura que se siente incapaz de relatar en prosa las cosas y que hay momentos que desearía tener cerca a alguien para contarle las miles de historias que pasan por su cabeza.

Una conversación de 15 minutos que vale la pena ver y acunar con gran legado de la literatura de nuestros pueblos latinoamericanos que les invitamos a disfrutar hoy cuando se cumple un año más del natalacio de "el más grande poeta del siglo XX en cualquier idioma",en palabras de su entonces interlocutor.

<iframe src="https://www.youtube.com/embed/1520QZIclmI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
