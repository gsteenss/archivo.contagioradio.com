Title: 6 Campesinos heridos  en manifestaciones contra erradicación forzada
Date: 2017-02-24 12:42
Category: Ambiente, Nacional
Tags: Erradicación Forzada, implementacion acuerdos
Slug: campesinos-exigen-presencia-del-gobierno-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/erradicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [24 Feb 2017] 

1.200 Campesinos retornaran a la vía Panamericana entre Tumaco y Nariño, **tras el desalojo del ESMAD que dejó 6 personas heridas**. Los campesinos que reclaman la presencia del gobierno para exigir que se frene la erradicación forzada en sus territorios, denunciaron que hasta el momento no se ha establecido ninguna mesa de interlocución.

De acuerdo con Davinson Hurtado, integrante de COCCAM (Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuna) **“los heridos podrían ser más y las heridas más graves son con balas de perdigones”. **Le puede interesar: ["ESMAD atacó a campesinos que protestaban contra erradicación Forzada"](https://archivo.contagioradio.com/campesinos-exigen-al-gobierno-frenar-erradicaciones-forzadas/)

De igual forma expresaron que durante el transcurso de los plantones, que se realizaron en diferentes puntos  de la vía Panamericana, n**o ha existido acompañamiento por parte de la Defensoría del Pueblo** debido a que desde Bogotá no se ha dado una orden de acompañamiento a la movilización,  en el mismo sentido no ha existido presencia de otras organizaciones que garanticen la seguridad de los manifestantes.

Los campesinos le están **exigiendo al gobierno que deje de realizar fumigaciones y erradicaciones forzadas de sus cultivos e implemente el punto 4 de los Acuerdos de Paz**, en donde se establecerán planes de sustitución de cultivos ilícitos Además de acuerdo con Devin Hurtado, estos planes deben estar acordes y responder a las necesidades de las familias de Nariño. Le puede interesar: ["Erradiciones forzadas y sustitución no son compatibles con la implementación: Cesar Jerez"](https://archivo.contagioradio.com/erradicacion-forzada-y-sustitucion-no-son-compatibles-con-la-implementacion-cesar-jerez/)

Por ahora, los campesinos se encuentran adelantando un censo para establecer c**uántas familias dependen de los cultivos ilícitos y construir su plan de sustitución**, además están a la espera de que el gobierno acepte finalmente generar una mesa de interlocución para acabar con la erradicación forzada.

<iframe id="audio_17207037" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_17207037_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
