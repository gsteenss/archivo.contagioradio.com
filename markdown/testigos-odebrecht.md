Title: ¿Qué está sucediendo con los testigos del caso Odebrecht?
Date: 2018-12-27 17:18
Author: AdminContagio
Category: Nacional, Política
Tags: Corrupción en Colombia, Odebrecht
Slug: testigos-odebrecht
Status: published

###### Foto: @jpserna 

###### 27 Dic 2018

[Este jueves en Bogotá, fue encontrado sin vida Rafael Merchán, ex secretario de Transparencia del gobierno de Juan Manuel Santos y reciente testigo del caso **Odebrecht**. Con su fallecimiento y junto al del ingeniero, Jorge Pizano en noviembre de este año, Merchán es el segundo funcionario vinculado al caso de corrupción muerto en extrañas circunstancias.  
  
Tres semanas atrás, el abogado y politólogo]**Rafael Merchán,** [fue presentado como testigo a favor de **Luis Fernando Andrade, ex director de la Agencia Nacional de Infraestructura, ANI** quien es investigado por la Fiscalía dada su vinculación al caso Odebrecht. [(Lea también Por primera vez veo a Néstor Humberto Martínez acorralado: Ramiro Bejarano)](https://archivo.contagioradio.com/por-primera-vez-veo-a-nestor-humberto-martinez-acorralado-ramiro-bejarano/)  
]

[Ante el tribunal Merchán, quien fue secretario de Transparencia durante el primer Gobierno de Juan Manuel Santos, debía confirmar si fue alertado de las conductas de Odebrecht para apropiarse de proyectos de infraestructura  y si fue el mismo Andrade el quien le proporcionó esta información.  
  
Las autoridades informaron que  aunque se desconocen las causas, ya están investigando las circunstancias de su fallecimiento; el cual junto al de Jorge Enrique Pizano, ex controller de la Ruta del Sol y testigo clave del caso Odebrecht quien murió el pasado 8 de noviembre y la de su hijo, **Alejandro Pizano** envenenado con cianuro días después, continúan generando más incógnitas alrededor del caso.]

Noticia en desarollo.

###### [Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}  
] 
