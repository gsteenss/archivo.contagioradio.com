Title: Denuncian asesinato del líder indígena, Arnulfo Catimay
Date: 2018-06-20 11:04
Category: DDHH, Nacional
Tags: Derechos Humanos, ONIC, Vichada
Slug: denuncian-asesinato-del-lider-indigena-arnulfo-catimay
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/onic.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [20 Jun 2018] 

La Organización Nacional Indígena de Colombia (ONIC), denunció por medio de un comunicado de prensa el asesinato del líder indígena Sáliba Arnulfo Catimay Blanca, "por parte de un miembro de la policía nacional" en el Municipio de Santa Rosalía, Vichada. Los hechos ocurrieron luego de un altercado entre terceros el martes 19 de junio.

Aida Quilcué Vivas, Consejera de Derechos de los Pueblos Indígenas, Derechos Humanos y Paz de la ONIC indicó que Sáliba, era hermano del gobernador de Caño Mochuelo y **tanto el Gobernador como su familia señalan como responsable a la Policía**, y añade que la policía no se ha referido al caso.

Por su parte, en el comunicado de prensa, la ONIC denuncia que la comunidad no ha permitido que el cuerpo del líder sea trasladado al hospital local de Santa Rosalía pues **para las autoridades del resguardo no hay “garantía de la cadena de custodia”**.

Finalmente, la ONIC solícita que se respeten los derechos individuales y la garantía al debido proceso. **Razón por la que esperan que ¨la Policía Nacional se abstenga de realizar cualquier tipo de acción que altere el material probatorio¨.** (Le puede interesar: ["Asesinan a 3 indígenas integrantes de la ONIC en el departamento del Valle del Cauca"](https://archivo.contagioradio.com/asesinan-a-3-indigenas-integrantes-de-la-onic-en-el-departamento-del-valle-del-cauca/))

[Comunicado de Prensa ONIC](https://www.scribd.com/document/382190482/Comunicado-de-Prensa-ONIC#from_embed "View Comunicado de Prensa ONIC on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_6175" class="scribd_iframe_embed" title="Comunicado de Prensa ONIC" src="https://www.scribd.com/embeds/382190482/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-vhrd6JcmUGUC7kNOkFuw&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
<iframe id="audio_26641068" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26641068_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
