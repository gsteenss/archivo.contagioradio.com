Title: Pensamiento crítico y luchas sociales en nuestra América Latina
Date: 2015-11-12 16:34
Category: eventos
Tags: congreso de los pueblos, Luchas sociales en America latina, marcha patriotica, Universidad Nacional de Colombia
Slug: pensamiento-critico-y-luchas-sociales-en-nuestra-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/pensamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [12 nov 2015]

Con el objetivo de establecer un diálogo abierto entre la Academia y los Movimiento sociales en la región, se realizará este viernes 13 de noviembre en la Universidad Nacional de Colombia el evento "Pensamiento crítico y luchas sociales en nuestra América Latina".

Organizado por la maestría en Estudios Políticos Latinoamericanos, Facultad de Derecho, Ciencias Políticas y Sociales de la Universidad Nacional, el evento servirá además para conocer el estado de las luchas sociales en el continente y la importancia de su articulación con la Academia, partiendo de la exposición de las experiencias invitadas.

La [programación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/programacion.jpg) que inicia a las 8 de la mañana, está dividida en 3 mesas que se desarrollaran durante la jornada, y contará con la intervención de invitados internacionales de primera línea entre los que se encuentran Antonio Elias de Uruguay, Gabriela Rofinelli  de Argentina, José Luis Rodriguez  de Cuba, Luis Rojas de Paraguay, Marcelo Carcanholo de Brasil.

Por Colombia participan del evento Carolina Jimenez, Jairo Estrada, José Francisco Puello Socarrás, y los líderes sociales Andrés Gil, vocero de Marcha Patriótica, Marylen Serna del Congreso de los Pueblos.

"Pensamiento crítico y luchas sociales en nuestra América Latina", cuenta con el apoyo de la Sociedad Latinoamericana de Economía Pública y Pensamiento Crítico y tendrá lugar en el auditorio Virginia Gutierrez Edificio de Posgrados de Ciencias Humanas de la Universidad Nacional de Colombia hasta las 4p.m.

 
