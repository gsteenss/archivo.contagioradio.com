Title: La plutocracia colombiana
Date: 2015-10-26 10:38
Category: Camilo, Opinion
Tags: elecciones, elecciones bogota, Fiscal Montealegre, Gustavo Petro, Transmilenio
Slug: elecciones-colombia-plutocracia-e-intereses-economicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/plutucracia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Por[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas)** 

###### [26 Oct 2015] 

La combinación de intereses económicos y políticos no es nueva, hoy ha tenido su expresión en las elecciones locales y regionales, de alcaldías y de gobernadores, evidentemente con algunas excepciones, que nos permiten sostener que la tendencia de nuestra cultura política es la plutocrática, que no por eso deja de ser mafiosa e ilegal.

Entendida más allá del gobierno de los ricos, como un negocio rentable para invertir y reproducir la inversión, asegurar proyectos económicos y proteger riqueza, comprando el voto de los empobrecidos que ven en una motosierra, en unos pesos, su salida, tratando de obviar que su futuro será peor, una esclavitud aparentemente gozosa, pues aún en el recinto donde nadie lo puede ver, ese pobre vota lealmente por ese al que le recibió la dádiva. Y por supuesto, los electores sectores medios formateados por las empresas mediáticas algunos de ellos con su foco en casas gratis, en la idea del bienestar con una buena via que les permita usar un carro y el complejo de pirámide, todo ello, gracias a una izquierda incapaz de enamorar y generar rupturas en el ejercicio de poder.

El modelo de Estado y sociedad se encuentra atado a una cultura de compra venta, no a la de la sensibilidad y la construcción del bien común o de la felicidad para todos,  incluso, los responsables de la corrupción, o del desfalco de los dineros públicos, logran pasar de agache y son nuevamente reelegidos con ese disfraz de la maquinaria política, que todo lo compra o vende.

La alcaldía de Bogotá y Barranquilla, la gobernación del Valle y el Antioquia, las de Sucre, Atlántico, Guajirá, y los reportes de municipios en donde las organizaciones de más variado tipo lanzaron sus candidatos ante una maquinaria paraempresarial, son una expresión visible del negocio de la política y de esa plutocracia

En Peñalosa, los incautos votantes olvidan que Transmilenio es parte de esos negocios en que se privatiza la calle pública, y se les paga con impuestos de los bogotanos a los privados por prestar un servicio con hacinamiento. Desde hoy la apuesta de ciudad es  la carpeta de negocios de los hermanos Vargas Lleras, Enrique y Germán, con los empresarios de las basuras como los Ríos, los Vélez Sierra. Ahí está, la chequera abierta para nuevos negocios con la multinacional Emgesa, la CEMEX y por supuesto los Sarmiento Angulo con poderosos intereses en las tierras de más alta valorización en Bogotá. El espíritu Vargas Llerista es Uribista. Esa amplia gama de negocios asegurados a través de las viviendas gratis, de las 4G, entremezclados con los dineros heredados de los neoparamilitares, y de los acuerdos de la cúpula de Cambio Radical, con Martín Llanos y el Bloque Centauros, como lo revelan ilegales de esas estructuras, temerosos ante el poder del Vice. Plutocracia como en Barranquilla. Los negocios de la familia Char, con sus grandes cadenas comerciales y en medios, las calculadas de algunos narcos, y unas multinacionales con intereses turísticos, portuarios en esa ciudad. Y qué no decir, de la gobernación del Valle, en cabeza de Lilian Francisca Toro, del partido de Santos, beneficiaria de narcos y de paramilitares, según reposa en expedientes judiciales que nadie quiere ver, estratega del control de la salud en el Valle, con el cual ha hecho riqueza, por decir, lo menos. Ah y, que no se dice, del gobernador de Antioquia, Luis Pérez, el poderoso, preparado, pero manzanillo, de sectores del  narcotráfico y el paramilitarismo. Ninguno de esos hoy elegidos, ni la mayoría de ellos, apuestan por la justicia socioambiental, ellos, la mayoría de los elegidos, van a administrar esos cargos para negocios particulares con algunos beneficios de fachada para la sociedad, entre esos negocios, algunos del neoparamilitarismo empresarial como se ve claramente en localidades del pacífico.

La llamada oposición o gobiernos opuestos a los partidos que reflejan a las agrupaciones políticas tradicionales, liberal conservadora, que se expresan en Cambio Radical, la U, Centro democrático, en el caso de Bogotá, Medellín, además de llorar, por tan malos resultados, debe asumir con autocrítica su realidad.

Ninguna administración en Bogotá, desde Luis E Garzón hasta Gustavo Petro, tuvieron una adecuada comunicación con la ciudadanía. Nunca explicaron con pocas palabras cuál es problema de la movilidad en la ciudad, las dificultades de Transmilenio con Peñalosa, en su primer gobierno; se afirmaron en un discurso confrontador no audible. En ocasiones, despreciaron los asuntos técnicos, y algunos de ellos convivieron con la corrupción como fue tan evidente con Samuel Moreno. Tampoco esa izquierda fue capaz de denunciar a evidentes gestores de la corrupción en el partido de la U, Cambio Radical, Liberal y Conservador. Además, esa izquierda hizo acuerdos políticos en los que cogobernaron y tomaron la dirección y decisiones de fondo, los partidos tradicionales, el intento más cercano a algo alternativo lo hizo Petro. En la campaña de Clara López, se juntaron personajes tan cuestionables para las organizaciones de víctimas, como el “elefantico”, el hijo de Ernesto Samper, tan proclive a la mimetización, experto en la justicia transicional de los poderosos, defensor acérrimo de una ley de víctimas que juega con las palabras de las víctimas con Santos; luego, asesor del Fiscal Montealegre, en la burda concepción del derecho penal del enemigo para judicializar a los integrantes de las FARC EP y del ELN.Por eso la izquierda o lo progresista no avanza

Todo ese conjunto de factores, y no solo los medios empresariales, que tomaron partido, son las razones de la derrota. Esa oposición llamada de izquierda no aprende, se alía sin principios; se vuelve pragmática y por eso se hunde. No hay que ser solo coherente, hay que mostrarse coherente y comunicarse coherente en sus actuaciones.

 Queda probado, por una vez más, que los cambios profundos nacen de proyectar y construir identidades con lo que la gente quiere de un gobierno, con respuestas concretas a situaciones concretas para los empobrecidos, con mensajes cómodos y posibilitadores para los sectores medios y con aliados que en lo mínimo que deben expresar es algo de honestidad.

Lo de hoy en Colombia debería llevar a las guerrillas a pensar su rol, en la recomposición de un proyecto político ecohumanista. El punto de partida es la actitud  de reconocimiento en imputaciones sociales por hechos de violencia; la creatividad en la comunicación de un nuevo proyecto de país, sin vanguardismo y con sus mejores hombres y mujeres, sin tacha. Esto supone una oposición evaluada en su interior y dispuesta a ser parte de otra apuesta política, más allá del Polo Democrático. El país nacional de la ciudad quiere una pax neoliberal y barata, quiere cemento y confort; el país nacional rural y el país ciudadano dormido busca un gobierno hacia la justicia socio ambiental.  Hay mucho por recorrer y mucho por aprender en la apuesta de un país donde el voto deje de ser mercancía y la conciencia se imponga sobre la tentación del dinero fácil, esa sigue siendo la nueva fase de la plutocracia, esa es nuestra cultura política, que no logra ser transformada ni llamar a los abstencionistas.

<div>

<div id=":1ei" style="text-align: justify;" tabindex="0" data-tooltip="Mostrar contenido reducido">

![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif)

</div>

</div>
