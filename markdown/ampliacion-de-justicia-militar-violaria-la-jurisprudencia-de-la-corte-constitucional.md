Title: Ampliación de justicia militar violaría la jurisprudencia de la Corte Constitucional
Date: 2015-11-24 13:32
Category: Judicial, Nacional
Tags: Congreso de la, Demanda ante la Corte Constitucional, Justicia Penal Militar en Colombia, Violación a DDHH en colombia
Slug: ampliacion-de-justicia-militar-violaria-la-jurisprudencia-de-la-corte-constitucional
Status: published

###### Foto: usahispanicpress 

<iframe src="http://www.ivoox.com/player_ek_9494467_2_1.html?data=mpmmlpmae46ZmKiakpqJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc7kzc7Oxc6Jh5SZo5jbjcnJb8vp1NnWxc7Fb87dzc7hw9ePusrjzcbfh6iXaaK4wpDZw5DOudPd1NXf18nJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe Muñoz] 

###### [24 Nov 2015 ]

[Los congresistas Ángela María Robledo, Alirio Uribe Muñoz, Germán Navas Talero e Iván Cepeda Castro, junto con representante de organizaciones sociales radican ante la Corte Constitucional una **demanda de inconstitucionalidad contra algunas de las disposiciones del Código Penal Militar y la ley 1775** expedida por el Congreso en julio de este año, que se reestructura la Justicia Penal Militar y que pretende implementar el sistema penal acusatorio.]

[De acuerdo con los demandantes esta reforma desconoce la competencia restrictiva para conocer delitos distintos a los propios de las funciones militares, **viola los derechos de las víctimas**, atenta contra la prohibición de **juzgamiento de civiles por parte de militares** y la restricción para otorgar funciones de policía judicial a las FFMM, al tiempo que resulta **violatoria de principios básicos de la administración de justicia** como la autonomía y la imparcialidad.  ]

[El Representante Alirio Uribe asegura que son tres los aspectos que se demandan, el primero de ellos la **declaratoria de inexequibilidad de la ley**, puesto que ya que había sido tramitada como ley estatutaria y luego para evitar el control de constitucionalidad de la Corte, fue tramitada como ley ordinaria, "lo que viola la Constitución".  ]

[En segunda instancia lo que se demanda es que sea **declarada la inexequibilidad de forma parcial** de la ampliación de la competencia de la justicia penal militar para conocer delitos que están más allá de su competencia, puesto que son delitos que no tienen nada que ver con su prestación del servicio y que por tal violan la jurisprudencia de la Corte Constitucional, asegura Uribe.]

[El **juzgamiento de civiles por parte de la justicia penal militar**, es otro de los elementos que harían parte de esta demanda de inexequibilidad, puesto que **las FFMM no pueden tener competencia para estas funciones**.]

[El Representante indica que esta demanda pretende exhortar al Congreso para que no siga reviviendo aquellas leyes que han sido declaradas inconstitucionales o que violan la jurisprudencia de la Corte Constitucional y que así mismo se **respete la jurisprudencia histórica de la Corte Constitucional** puesto que la competencia de la justicia militar es sólo para delitos que tengan que ver con el servicio, pues se trata de **“un autojuzgamiento y una autoimpunidad que incluye crímenes graves como violaciones de derechos humanos o delitos comunes graves”**.     ]
