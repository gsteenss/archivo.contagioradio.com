Title: Amenazan a lideresa que se opone a proyectos petroleros en Guamal, Meta
Date: 2017-11-23 14:54
Category: Ambiente, Nacional
Tags: Guamal, Pozo Lorito
Slug: amenazan-a-lideresa-que-se-opone-a-proyectos-petroleros-en-guamal-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/guamal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [23 Nov 2017] 

La líder social y defensora de derechos humanos Marlen Arévalo, denunció que el pasado 21 de noviembre, **fue víctima de intimidaciones por dos personas, que se transportaban en una moto sin placas, cuando salía de un juzgado en Guamal**, luego de interponer un incidente de desacato contra Ecopetrol y la ANLA por incumplimientos a la sentencia de la Corte Constitucional, y continuar sin permisos con la construcción del pozo Lorito.

De acuerdo con la denuncia, las personas iban con cascos de moto completamente cerrados que impedían identificarlos y que una de ellas le dijo “si paran Lorito, se mueren hijos de puta”. (Le puede interesar: ["Acciones y tutela frenarían daños ambientales en Guamal, Metas"](https://archivo.contagioradio.com/las-acciones-populares-y-tutela-frenarian-danos-ambientales-en-guamal-meta/))

Edgar Cruz, esposo de Marlen y defensor de derechos humanos, manifestó esas amenazas **son producto de la defensa del territorio en Guamal, que iniciaron desde el 2011**, y están relacionadas con su trabajo contra las actividades de extracción petrolera que se querían realizar en el territorio, en el pozo Lorito de Ecopetrol y que no tiene todas las autorizaciones para hacerse.

“Nos enteramos que se iba a desarrollar un proyecto en la vereda Monte Cristo, en el municipio de Guamal, zona de recarga hídrica, Marlen Arévalo instauró una tutela que fue fallada en contra, en septiembre de 2013 la Corte Constitucional **profirió una sentencia en donde dice que la ANLA es la última que tiene la palabra con respecto al proyecto”** afirmó Cruz. (Le puede interesar: ["Ecopetrol hace caso omiso a Procuraduría y comunidad en Guamal"](https://archivo.contagioradio.com/ecopetrol-hace-caso-omiso-a-contraloria-procuraduria-y-comunidad-para-entrar-en-guamal/))

### **Los desacatos de Ecopetrol y la ANLA** 

Hace tres días la señora Arévalo interpuso ante un Juzgado en Guamal un incidente de desacato, para obligar tanto a Ecopetrol como a la ANLA a cumplir con la sentencia, debido a que los habitantes de esta vereda denuncian que actualmente hay maquinaria trabajando en el territorio y **hay construida una nueva plataforma, sobre una reserva subterránea de agua dulce, sin los estudios y aprobaciones previas**.

En junio de este año, Ecopetrol radicó ante la ANLA, un plan de manejo específico para la nueva plataforma, sin embargo, la ANLA no lo aprobó como ordena la sentencia. Además de acuerdo con Edgar Cruz, Ecopetrol estaría violando la licencia debido a que en ella se prohíbe, **que la empresa utilice vías de acceso que se encuentren por encima de 500 metros sobre el nivel del mar**.

Tanto Edgar Cruz como Marlen Arévalo, están solicitando de forma urgente que se tomen medidas de protección, por parte de las entidades responsables, para que se proteja la vida de estos defensores de derechos humanos en el Meta, y para que se vigile el accionar tanto de Ecopetrol como de la ANLA en el manejo de las licencias.

<iframe id="audio_22331817" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22331817_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
