Title: JEP oportunidad única para satisfacer los derechos de víctimas: COEUROPA
Date: 2017-06-12 17:52
Category: DDHH, Nacional
Tags: COEUROPA, Jurisdicción Espacial de Paz
Slug: jep-oportunidad-unica-para-que-maximos-responsables-satisfagan-derechos-de-las-victimas-coeuropa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/pazcalle.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Jun 2017]

Durante la asamblea nacional de la Coordinación Colombia Estados Unidos, que agrupa a 271 organizaciones sociales y de derechos humanos, resaltaron que se deben mantener los derechos de las víctimas en el centro del acuerdo y señalaron que la Jurisdicción de paz es una **oportunidad única para que se satisfagan los derechos de las víctimas**. Además señalaron que es necesario que se establezcan acuerdos con el ELN que caminen hacia un cese al fuego.

Las organizaciones de COEUROPA que realizaron su asamblea anual reconocieron los avances del proceso con las FARC, insistieron en que se deben mantener a las víctimas en el centro y señalaron que para ello se debe trabajar en la puesta en marcha la **Comisión de Búsqueda de personas desaparecidas**, así como garantizar la participación efectiva de las víctimas en la Comisión de la Verdad y en la JEP. Le puede interesar: ["Unidad de Búsqueda tendrá que encontrar a más de 50.000 desaparecidos"](https://archivo.contagioradio.com/unidad-de-busqueda-tendra-que-encontrar-a-mas-de-50-000-desaparecidos-en-el-pais/)

Así mismo insistieron en la necesidad de que el Estado reconozca la existencia del paramilitarismo así como la sistematicidad de los crímenes contra líderes sociales y defensores de Derechos Humanos e instaron a que se ponga en marcha la unidad de combate a esas estructuras y del mismo modo recordaron el compromiso del gobierno con la creación de una Comisión de Alto Nivel de Garantías de No Repetición.

Por último señalaron que **es necesario que se avance en el proceso de conversaciones de paz con el ELN** y sobre todo en la definición de medidas humanitarias que apunten al desescalamiento del conflicto y que se avance en la posibilidad de declarar un cese bilateral de fuego que permita que el número de víctimas del conflicto armado se reduzca. Le puede interesar:["Paramilitarismo y secuestro puntos álgidos de la mesa en Quito"](https://archivo.contagioradio.com/eln-y-gobierno-deben-superar-tensiones-para-alcanzar-cese-bilateral/)

[ASAMBLEA 2017Declaracion PoliticaCCEEU Final](https://www.scribd.com/document/351106634/ASAMBLEA-2017Declaracion-PoliticaCCEEU-Final#from_embed "View ASAMBLEA 2017Declaracion PoliticaCCEEU Final on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_10870" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/351106634/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-FLCSQ8iTUlbSMjDqbu2n&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
