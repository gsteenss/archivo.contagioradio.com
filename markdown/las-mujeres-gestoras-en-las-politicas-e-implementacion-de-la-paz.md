Title: Las mujeres: gestoras en las políticas e implementación de la paz
Date: 2016-09-27 16:46
Category: Mujer, Nacional
Tags: enfoque de género, Mujeres y Paz
Slug: las-mujeres-gestoras-en-las-politicas-e-implementacion-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Mujeres-marchan-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mujeres marcha por la paz] 

###### [26 de Sep 2016] 

Las tres intervenciones durante la firma protocolaria de los acuerdos de paz, el del Secretario General de la ONU Ban Ki-moon, el de Rodrigo Londoño, máximo jefe de las FARC-EP y el del presidente Santos, **concordaron en mencionar la ardua labor que han tenido las mujeres en la construcción de paz para el país**.

Los acuerdos tienen como elemento[innovador la inclusión del enfoque de género, que se traducen en un marco de garantías de derechos políticos](https://archivo.contagioradio.com/acuerdos-deben-implementarse-con-enfoque-de-genero-y-territorial/), económicos y sociales, para las mujeres que han sido víctimas de los diferentes tipos de violencia, a lo largo del conflicto armado y que estuvieron presentes en la construcción de los acuerdos.

Según Olga Amparo Sánchez, directora de Casa de la Mujer **“las mujeres han jugado un papel muy importante desde hace muchos años,** sosteniendo que el diálogo era la alternativa para salir del conflicto armado, lo que quedo en los acuerdos fue el producto del trabajo de las mujeres que se comprometieron con las mujeres”.

Para la representante a la Cámara Ángela María Robledo lo más esperanzador fue el discurso de Rodrigo Londoño y el ofrecimiento de perdón por haber causado dolor a todas las víctimas, **“el perdón es inconmensurable, no tiene medidas y generará la base para construir la convivencia de todos y todas”**.

Sánchez también señala que uno de los restos que deberán asumir las organizaciones sociales será transformar la metodología y temáticas que se abren de cara a un país en construcción  de paz["no es lo mismo una gestión política y social en un país en guerra, que en un país en paz."](https://archivo.contagioradio.com/incorporacion-del-enfoque-de-genero-un-hecho-historico-que-ningun-proceso-de-paz-ha-logrado/)

<iframe id="audio_13083984" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13083984_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_13097059" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13097059_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
