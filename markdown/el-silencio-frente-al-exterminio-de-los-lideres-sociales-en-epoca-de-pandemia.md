Title: El silencio frente al exterminio de los líderes sociales en época de pandemia
Date: 2020-07-23 17:07
Author: Foro Opina
Category: Columnistas, Opinion
Tags: Amenazas a defensores de Derechos Humanos, Defensa de líderes sociales, defensores de derechos humanos, Derechos Humanos, lideres sociales, lideres sociales asesinados, pandemia
Slug: el-silencio-frente-al-exterminio-de-los-lideres-sociales-en-epoca-de-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/IMG_1169-scaled.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/IMG_1169-1-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### Según datos del **INDEPAZ**, en lo que va corrido del año se contabilizan 166 líderes sociales y defensores de derechos humanos asesinados.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ni siquiera el COVID 19 ha frenado esta atroz situación sobre la cual el gobierno nacional continúa insistiendo que se trata de hechos aislados, negando la sistematicidad del fenómeno. Es así como desde la declaratoria de la pandemia el 12 de marzo, se han registrado **90 homicidios de líderes.** Esto en cifras representa cinco casos cada semana, siendo el departamento del Cauca el que ha colocado el número más significativo de víctimas con treinta y cuatro. Por colectividades son los indígenas y los campesinos quienes han puesto la mayor cuota, cada uno, con treinta y uno de sus miembros.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta coyuntura ha generado condiciones que facilitan el exterminio de los líderes sociales; el confinamiento y la falta de garantías de seguridad han permitido que los grupos armados los encuentren fácilmente en sus casas o cercanías. Igualmente, el no poder salir a defender el territorio permite que los grupos al margen de la ley tengan mayor posibilidad de apoderarse de dichas zonas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La pregunta obligada es **¿por qué los están asesinando y quiénes están perpetrando estos actos de violencia?**; guardábamos la esperanza de no corroborar la hipótesis que sustentaba que con la firma del Acuerdo posiblemente aumentaría la conflictividad social como también el asesinato de líderes sociales y de excombatientes de las FARC, como retaliación de la extrema derecha criminal. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desafortunadamente no hay cifras oficiales que puedan ayudarnos de manera contundente en esta respuesta y organismos internacionales, como las mismas plataformas de derechos humanos, están a la expectativa de las escasas investigaciones que se realizan para tratar de esclarecer los hechos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En la mayoría de los casos son las mismas organizaciones sociales quienes denuncian y reclaman justicia con el objetivo de que no haya impunidad.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Y es que, como se ha dicho, con la dejación de armas de las FARC se generó un proceso de reacomodación de fuerzas criminales en los territorios donde el Estado no ha logrado copar estas áreas, hacer presencia y establecer el orden.  El ritmo de implementación de los Acuerdos, en gran parte por la poca voluntad política del gobierno de Duque, ha generado en las comunidades una sensación de “promesa incumplida” fundada en la baja ejecución de programas tan importantes como la sustitución de cultivos de uso ilícito o la puesta en marcha de las obras PDET, identificadas aún más con el argumento de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vale la pena destacar el caso del Cauca, un departamento con una tradición de profundas desigualdades sociales en el que convergen todos los factores generadores de violencia del país como son la disputa por la tierra, la minería ilegal, los cultivos de uso ilícito, el monocultivo, la presencia de grupos armados ilegales, la pobreza extrema, la corrupción y la baja o nula presencia del Estado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Sin embargo, en medio de este complejo escenario, allí se erige un fuerte liderazgo social pluriétnico y cultural que promueve la autonomía y la defensa del territorio construyendo identidades ligadas a la ancestralidad y al respeto por la madre tierra. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Se trata de hombres y mujeres que se toman en serio la defensa de proyectos de vida articulados a modelos de desarrollo alternativos donde la soberanía alimentaria y la productiva son un principio fundamental. Esta postura ha generado incluso mecanismos de protección y autoprotección para blindar los territorios en aras de proveer una mayor seguridad. Es claro que los líderes se conviertan en un estorbo para los fines de lucro y la lucha por el poder que buscan el sometimiento de las comunidades y establecer control y dominio de los recursos naturales y las economías ilegales a partir de alianzas promiscuas desde la legalidad y la ilegalidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con los hechos de violencia perpetrados no solo caen los líderes, con ellos se va un acervo de conocimientos, relaciones, confianzas y afectos que soportan la amalgama de un tejido social que ha llevado años construir. La afectación trasciende la familia de origen instaurando emociones de indefensión y orfandad como también de miedo y vulnerabilidad en las comunidades. Procesos mutilados por acción u omisión de un Estado ausente que, como decíamos, ni siquiera da cuenta de los móviles y autores de los asesinados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Consideramos que las autoridades civiles y militares deben rendir cuentas.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las otras preguntas obligadas son: ¿Todo ese pie de fuerza para qué?; ¿Todas esas interceptaciones, tanta inteligencia para qué? Líderes en sus casas y ni ahí el Estado es capaz de protegerlos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Instamos al gobierno nacional para que actúe de manera contundente y garantice la vida de los líderes sociales. Las políticas definidas para brindar seguridad como el PAO (Plan de Acción Oportuna de Prevención y Protección) no están arrojando los resultados esperados; de otra parte, es importante fortalecer los planes de seguridad y convivencia de los gobernantes locales como una forma de contrarrestar la situación en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Invitamos a rodear a los líderes sociales y a los defensores y defensoras de derechos humanos. Sus asesinatos están siendo silenciados por la pandemia. **Reclamemos por sus vidas y no por sus muertes.**  Reclamemos por la garantía de una labor que desde diversos ámbitos busca que el país sea más equitativo, incluyente y democrático.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La Fundación Foro Nacional Por Colombia**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### ·  Es un Organismo Civil no Gubernamental sin ánimo de lucro, creado en 1982, cuyos objetivos son contribuir al fortalecimiento de la democracia en Colombia. Desarrolla actividades de investigación, intervención social, divulgación y deliberación pública, asesoría e incidencia en campos como el fortalecimiento de organizaciones, redes y movimientos sociales, la participación ciudadana y política, la descentralización y la gestión pública, los derechos humanos, el conflicto, la paz y las relaciones de género en la perspectiva de una democracia incluyente y efectiva. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente).

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### **Contáctenos: 316 697 8026\_ 031282 2550 /Comunicaciones@foro.org.co**

<!-- /wp:heading -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Le puede interesar: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
