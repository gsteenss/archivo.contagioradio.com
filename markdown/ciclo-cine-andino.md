Title: Con "La tierra y la sombra" inicia la 2da Semana del Cine Andino
Date: 2018-05-22 13:26
Author: AdminContagio
Category: 24 Cuadros
Tags: cine andino, Cine Colombiano, La tierra y la sombra
Slug: ciclo-cine-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/63_Laura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Burning Blue 

###### 22 May 2018 

Seis producciones componen la segunda edición del ciclo itinerante "Semana del Cine Andino" de la Cinemateca Distrital de Bogotá, una muestra del cine que se realiza en Argentina, Chile, Colombia, Ecuador, Perú y Venezuela, países que integran el denominado bloque andino.

Del 24 al 29 de mayo, se presentaran las películas *El invierno* de Emiliano Torres; *El Amparo*, producción colombo venezolana de Rober Calzadilla; *Mala Junta* de la chilena Claudia Huaiquimilla; *Tan Distintos* del ecuatoriano Pablo Arturo Suárez y la cinta peruana *Climas* de Enrica Pérez.

A estas producciones se suma La tierra y la sombra del director César Acevedo, cinta colombiana ganadora de la Cámara de Oro del Festival de Canes en 2015, que vuelve a la a la pantalla de la Cinemateca para inaugurar la muestra.

El ciclo es resultado del Foro Internacional para un Mercado de Cine Andino, que se realiza anualmente en el marco del Festival Internacional de Cine de las Alturas, evento del cual Marcelo Pont es co-director artístico, quien va a estar presente en el preámbulo de la apertura en un pequeño conversatorio con medios de comunicación.

Adicionalmente, antes de la proyección inaugural, se realizará un panel académico a las 5:00 p.m. con los productores de La tierra y la sombra (Burning Blue), Yenny Chaverra de la Dirección de Cinematografía del Ministerio de Cultura de Colombia y Marcelo Pont, Director Artístico del Festival.

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
