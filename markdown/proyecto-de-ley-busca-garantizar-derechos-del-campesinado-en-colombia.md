Title: Proyecto de Ley busca garantizar derechos del campesinado en Colombia
Date: 2016-04-05 16:31
Category: Nacional, Política
Tags: campesinos, conflicto armado, Congreso, constitucion
Slug: proyecto-de-ley-busca-garantizar-derechos-del-campesinado-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Campesina-colombiana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Controinfo Colombia] 

###### [5 Abr 2016] 

Alberto Castilla, senador del Polo Democrático Alternativo, **presentó ante la Secretaría General del Senado un proyecto de acto legislativo con el fin de reconocer los derechos del campesinado** y en donde además se tiene un enfoque especial que busca garantizar los derechos de la mujer rural y garantizar la consulta previa.

El proyecto tiene como objetivos principales: aplicar de manera más adecuada las normas vigentes que deben dar garantías a los pobladores rurales; llenar las lagunas existentes en la Ley; elaborar un instrumento jurídico sobre los derechos de las personas campesinas y; por lo tanto proteger este sector de la población por medio del análisis de la realidad rural Colombiana, lo cual exige reforzar el Estado de social de derecho con el fin de que se logre el reconocimiento del campesinado.

De acuerdo con el senador, este proyecto legislativo nace teniendo en cuenta los niveles de desigualdad y pobreza en los que vive el campesino Colombiano, pues de acuerdo con datos del **DANE en el sector rural la pobreza alcanza el 41,4%.** Por otra parte, las cifras sobre pobreza monetaria extrema se acercan al 19,1%, además una  situación de extrema vulnerabilidad social golpea con mayor fuerza a **las mujeres campesinas, quienes según el PNUD viven en condiciones sociales críticas.**

La población rural es además la más afectada por el conflicto armado que vive el país desde hace más de 50 años, y por el cual los campesinos y campesinas han sido víctimas del despojo violento y desplazamiento forzado, pues se estima que 6 millones de hectáreas fueron abandonas en el marco de la guerra y gran parte de ellas están en manos de latifundistas.

Organizaciones campesinas se establecieron con sus mercados campesinos en el Congreso de la República para acompañar al senador Castilla, quien radican este proyecto con el apoyo de México, Ecuador y Bolivia.

**Mediante la reforma del artículo 64 de la Constitución, esta iniciativa sería una herramienta efectiva para garantizar el reconocimiento del campesino** como sujeto de especial protección y reconocer el derecho a la tierra individual y colectiva.

Los elementos constitutivos de la reforma son los siguientes:

-   Reconoce al campesinado como sujeto de especial protección, al reemplazar el concepto de “trabajador agrario” por los conceptos de “campesino” y “campesina”.
-   Pasa de reconocer el acceso a la tierra a reconocer el derecho a la tierra individual y colectiva.
-   Reconoce la construcción social del territorio por parte de las comunidades campesinas.
-   Reconoce el derecho a las semillas.
-   Reconoce a las mujeres campesinas y garantiza la equidad de género en la distribución de los recursos productivos en el campo.
-   Establece la obligatoriedad del mecanismo de participación de consulta popular en caso de que haya afectación de tierras y territorios campesinos.
-   Ordena adaptar los derechos sociales reconocidos universalmente a las necesidades particulares del campesinado y de la vida en el campo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
