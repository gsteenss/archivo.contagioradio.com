Title: Colombia se moviliza por el sí a la paz
Date: 2016-07-14 12:29
Category: Movilización, Nacional
Tags: Si al plebiscito por la paz
Slug: colombia-se-moviliza-por-el-si-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/13720559_10153751808445098_257494622_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ContagioRadio] 

###### [14 de Jul] 

Este 15 de julio se realizará la movilización nacional **"La Paz sí es contigo"**, iniciativa impulsada desde diversos sectores de la sociedad que pretende generar, a partir de actos **culturales un debate al interior de la ciudadanía sobre la refrendación de los acuerdos del proceso** **de paz** en la Habana y la necesidad de la construcción de paz desde la participación para el país.

Durante el lanzamiento de la iniciativa la vocera de la plataforma Marcha Patriótica, Piedad Córdoba dijo "las y los convocamos a darle la bienvenida a la paz en las calles y en las urnas, en las aulas y en los hogares, en la esquina y la vereda. Haremos sentir las voces de quienes no queremos más muerte, más persecución, más genocidio, ni violencia. **Les invitamos a poner la firma en los acuerdos, revocado la guerra y diciendo la Paz sí es conmigo**".

Ciudades como Bogotá, Medellín, Manizales, Cúcuta, Calí, Cartagena, Popayan, entre otras, tendrán diferentes actividades el viernes, que como lo han denominado las organizaciones "**prenderán la fiesta por el si**". Durante la jornada se realizarán conciertos, encuentros teatrales, comparsas y presentaciones de batucadas.

En **Manizales** el punto de encuentro será la plaza del Agua, a las dos de la tarde y finalizará con un concierto en el parque Ernesto Gutiérrez con un concierto en donde participarán artistas y grupos musicales como Reincidentes.

**Cúcuta** sera el centro de concentración del Magdalena medio y la movilización arrancará desde las diez de la mañana en dos puntos de encuentro: El Estadio Ginmasia Centenario y en el Redona García Herreros.

En **Medellín** el punto de encuentro será el Parque de las Luces a las nueve de la mañana y finalizará en el Parque de los Deseos, en donde habrá un concierto que contará con la participación de artistas como Yemaya, Frankie Ha Muerto, entre otros.

Para **Bogotá** el punto de concentración será el Parque Santander a las 5:30 de la tarde y culminará en la Plaza Eduardo Umaña Mendoza con un concierto a las siente de la noche.

En **Neiva** la movilización se encontrará en el Centro de Convenciones José Eusrasio Rivera, sobre las diez de la mañana y finalizará en la Plaza de la Alcaldía de Neiva.

En **Popayán** las personas se podrán concentrar a partir de las 8 de la mañana en la Villa Olímpica y finalizarán el recorrido en el Parque Caldas.

En **Cali** el punto de encuentro será en el Parque de las Banderas, en San Fernando, sobre las 9 de la mañana.

Organizaciones como Congreso de los Pueblos, Marcha Patriótica, Fuerza Común, Vamos por la Dignidad, la Unión Patriótica, la Juventud Comunista, entre otras respaldan esta iniciativa y participaran de la misma desde cada una de las regiones del país.

[  
](https://archivo.contagioradio.com/colombia-se-moviliza-por-el-si-a-la-paz/15j/) [  
](https://archivo.contagioradio.com/colombia-se-moviliza-por-el-si-a-la-paz/15j1/)

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en [[Otra Mirada](http://bit.ly/1ICYhVU)] por Contagio Radio 
