Title: ¡El Refugio Humanitario cumplió sus objetivos!
Date: 2019-05-04 23:34
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Bogotá, defensores de derechos humanos, lideres sociales, Refugio Humanitario
Slug: refugio-humanitario-cumplio-sus-objetivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/3-e1556904478498.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado jueves el "Refugio humanitario por la vida de líderes y lideresas sociales" culminó sus actividades en Bogotá luego de 5 días, en los que cerca de 3 mil defensores de derechos humanos, participaron de diferentes actividades que pretendían denunciar ante la comunidad nacional e internacional las constantes amenazas contra sus vidas. Para Olimpo Cárdenas, integrante de Congreso de los Pueblos, el Refugio fue un éxito y cumplió sus objetivos.

<iframe src="https://co.ivoox.com/es/player_ek_35295334_2_1.html?data=lJqfm5qXd5Whhpywj5aUaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncbDgytLd0ZCnaaSnhqae1MnJssLnjNjcxNfJb83VjMjiztLNssLXyoqwlYqmd8-fxcrZjbfJqtbbytSYytrRpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Como lo explicó Cárdenas, el asesinato de más de 600 líderes y lideresas desde la firma de los Acuerdos de Paz en el Teatro Colón es razón más que suficiente para plantearse acciones que irrumpan la cotidianidad de la ciudad, por eso se decidió la realización de un refugio humanitario con el que se pudiera alertar a la ciudadanía en general, el Gobierno Nacional y la Comunidad Internacional sobre los grandes riesgos que enfrentan quienes defienden los derechos humanos.

En esa medida, para el integrante de Congreso de los Pueblos el balance fue bueno porque después de 5 días de esfuerzo, lograron el primer objetivo del refugio, que era sensibilizar a las personas sobre lo que estaba ocurriendo en los territorios del país y hacer que las y los líderes se encontraran, sintiéndose acompañados y llenos de esperanza. También notaron que la sociedad es sensible ante el asesinato de líderes; lo que es un avance para caminar hacía acciones de ciudadanía más contundentes.

En segundo lugar, lograron ser recibidos por embajadas de países integrantes de la Unión Europea, lo que significó poner en conocimiento de la Comunidad Internacional la situación de algunos de los líderes del país; y de esta manera lograr unos aliados importantes para adelantar acciones de prevención y protección. A ello se sumó la construcción de rutas para la protección y autoprotección de las comunidades; con lo que se construyeron propuestas claras para frenar los asesinatos.

### **¿Y el objetivo que planteaba interlocución con el Gobierno?** 

Cárdenas indicó que del Gobierno Distrital tuvieron el apoyo y respeto por la iniciativa, también en el acompañamiento para realizar el refugio en la Plaza de Toros que renombraron Plaza de la Vida. Sin embargo, no ocurrió lo mismo del gobierno Duque, con el que intentaron hablar en el desarrollo de una audiencia pública en el Congreso al que no asistieron los funcionarios invitados.

Pese a ello, quienes integraron el Refugio Humanitario acordaron no crear una nueva plataforma de este espacio, sino articular las ya existentes, para insistir las mesas nacional y departamentales de garantías; de igual forma, continuar buscando el diálogo con el Gobierno Central y acudiendo a los espacios internacionales. (Le puede interesar:["El 28 de abril abre las puertas el R. Humanitario por la vida de los líderes sociales"](https://archivo.contagioradio.com/refugio-humanitario-vida-lideres-sociales/))

En el siguiente programa hablamos con Cristian Cabrera, integrante del Consejo Nacional de Paz y Erika Prieto, integrante de la comisión nacional de derechos humanos del Congreso de los Pueblos sobre los resultados del Refugio Humanitario.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F356696831639459%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
