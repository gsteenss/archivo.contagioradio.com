Title: "Regreso de María del Pilar Hurtado abre un compás de verdad"
Date: 2015-02-03 17:45
Author: CtgAdm
Category: Uncategorized
Slug: regreso-de-maria-del-pilar-hurtado-abre-un-compas-de-verdad
Status: published

##### Foto: Ana Br 

##### <iframe src="http://www.ivoox.com/player_ek_4033433_2_1.html?data=lZWglZmXd46ZmKiakpuJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOms8rU1MrXs4zYxpC6w9eJh5SZoqnOjcnJsIzEytHO1JCsudPowsncjcbGtsaf1tOYxdTRtIa3lIquk9iPqMahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Ramiro Bejarano, analista político] 

Para el analista Ramiro Bejarano, el regreso al país de María del Pilar Hurtado "abre un compás de verdad muy esperado".

En sus primeras declaraciones, la ex-directora del DAS aseguró que el computador en que guardaba la información relacionada con el DAS le fue robado en Panamá. Si este fuese el caso, señala el analista, **Hurtado aún cuenta con la información de su propia experiencia a la cabeza de la entidad**; además la Fiscalía General cuenta con copias de los informes que el DAS guardó en el Archivo General de la Nación, antes que desaparecieran.

María del Pilar Hurtado deberá responderle a las autoridades por los delitos cometidos entre el 30 de agosto del 2007 y el 22 de octubre del 2008, cuando era directora del DAS.

Se le acusa de Concierto para delinquir agravado (por reunirse con otros miembros del gobierno nacional para realizar actividades ilícitas. Según lo establece la ley, **los servidores públicos están obligados a negarse a seguir las ordenes de superiores cuando estas violen la constitución política colombiana)**, Peculado por apropiación (por contratar testigos falsos en montajes judiciales en contra de miembros de la oposición), Falsedad ideológica en documento público (por certificar que el DAS no estaba siguiendo a los miembros de la Corte Suprema, cuando estaba ocurriendo lo contrario), Violación ilícita de comunicaciones agravada y Abuso de función pública (por realizar interceptaciones y seguimientos sin orden judicial).

**La ex-directora del DAS aún podría recibir beneficios jurídicos**, como rebaja de pena y un tratamiento penitenciario amable, si colabora con el esclarecimiento de la verdad en el caso del DAS.
