Title: "Convocar a la sociedad civil legitima el proceso de paz", Carlos Medina
Date: 2016-02-11 12:18
Category: Nacional, Paz
Tags: dialogos de paz, FARC, Foro fin del conflicto, Paramilitarismo
Slug: convocar-a-la-sociedad-civil-legitima-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Foro-fin-del-conflicto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: HSB Noticias 

<iframe src="http://www.ivoox.com/player_ek_10400007_2_1.html?data=kpWhkpWUdJihhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncYamk6jc0NvTp8LmjMaYzsaPt9DXysrRw8mPp8rqytGYzsrLrdXdzsaYx9GPtNPjxMrg0ZDIqYzkwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Carlos Medina Gallego, U.N.

###### [11 Feb 2016.] 

Finalizó el último foro sobre los puntos más álgidos de las conversiones en la Habana, organizado por la ONU y el Centro de Pensamiento y Seguimiento al Proceso de Paz de la Universidad Nacional. Las organizaciones presentaron sus propuestas frente a los puntos 3 y 6 sobre el fin del conflicto y la dejación de armas, esperando que estas sean tenidas en cuenta en la recta final del proceso de paz.

“**Convocar a la población civil es de la mayor importancia para legitimar cualquier tipo de proceso**”, asegura Carlos Medina, integrante del Centro de Pensamiento y Seguimiento al Proceso de Paz de la Universidad Nacional, quien afirma que “las propuestas de la comunidad fueron muy variadas” pero pertinentes para enviar a la mesa de negociaciones.

Frente al punto tres sobre el fin del conflicto, las organizaciones sostuvieron que ese tema debe analizarse desde dos puntos, es decir, sobre lo que significa el cese al fuego, pero también el cese de hostilidades, teniendo en cuenta que además de que las FARC y los militares deberán silenciar los fusiles, en el cese de hostilidades deben **suspenderse las acciones de estigmatización y criminalización del movimiento social y popular, y en cambio, se propicien espacios de diálogo democrático.**

Medina, sostiene que en la población “hay una preocupación mayor por el desmonte del paramilitarismo”, para lo que se propuso la creación de una comisión para que el Estado opere sobre este fenómeno, exigiendo un mayor trabajo de los servicios de inteligencia. Así mismo, será indispensable “**trabajar sobre la desparamilitarización de la fuerza pública”, que ha sido cómplice del actuar de las estructuras paramilitares.**

En consonancia con lo anterior, la organizaciones, determinaron que la **doctrina militar deberá ajustarse a "un modelo de sociedad que ya no está en guerra", dice el profesor.**

Del mismo modo, “**es necesario que ganaderos,  agricultores y comerciantes que han financiado  el paramilitarismo y puedan desarrollar sus actividades económicas de manera transparente.** Las empresas deben tomar distancia, y deben ser investigadas y sancionadas”, indica el profesor de la Universidad Nacional, agregando que “se debe hacer una desparamilitarización de la ciudadanía que acepta este fenómeno como algo que hace parte de la vida de las comunidades”.

Por otro lado, se propuso una serie de **instrumentos que ilustren el contenido de los acuerdos**, frente a esto, Medina señala que se propone implementar un mecanismo para implementar la pedagogía de la paz desde el más alto nivel del Estado a hasta los sectores más marginados.

Finalmente, sobre el plebiscito para la paz se concluyó que este mecanismo no genera elementos de Ley para que los acuerdos sean efectuados, por lo tanto, deberá tramitarse una discusión democrática para acordar el instrumento más pertinente de refrendación de los acuerdos.

El foro contó con la **participación de 715 personas, de 21 sectores de la población civil,** como indígenas, afros, campesinos, empresarios, académicos y organizaciones de derechos humanos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
