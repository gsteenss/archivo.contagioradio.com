Title: Los primeros cien días de la Bogotá recuperada
Date: 2016-03-29 07:00
Category: Opinion, Tatianna
Tags: Alcaldía Enrique Peñalosa, Bogotá
Slug: los-primeros-cien-dias-de-la-bogota-recuperada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/penalosa-100-dias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:1080plus 

#### **Por [Tatianna Riveros](https://archivo.contagioradio.com/opinion/tatianna-riveros/)- [@[tatiannarive]

###### 29 Mar 2016 

Dentro de las propuestas del candidato que hoy es el alcalde y el que a veces parece olvidar que ya no está en campaña estaban la movilidad y la seguridad, intervención de puntos, pintar las cebras de las calles, limpiar los postes y capturar a ladrones de celulares, pues bien, este es un pequeño recuento de lo que ha sido y ha hecho nuestro alcalde a pocos días de terminar su plan de choque, “los primeros cien días de la Bogotá recuperada”. Aquí va:

[El primer punto del que habló Enrique, fue el de “Intervenir de manera inmediata 750 puntos calientes del crimen, pues bien, a 16 de febrero de 2016, nuestro alcalde no había iniciado dicha intervención, se limitó a contarnos que  en Suba hay 101 puntos, en Kennedy 77 y en Ciudad Bolívar 55, para sorpresa de muchos, de las 750 cuadras de las que habla el alcalde (que representan sólo el 0.6% de todas las cuadras de Bogotá), concentran además sólo el 25% del crimen; Para resaltar, dentro de su propuesta estaba llenar de policías toda Bogotá cosa que hicieron, sin contar con los agentes del Esmad. Para este punto, se incluyen los grafitis que quitaron, se echaron al lomo el plan de iluminación que el alcalde saliente había dejado y se habló de una limpieza general; también se incluyen los postes “recuperados”, tarea que no es del distrito pero que aún sí venden como la gran hazaña.]

[El segundo punto, hablaba de recuperar parques, “quitarle” los parques a los criminales y a las pandillas, hasta ahora no he visto la primera gran hazaña de nuestro mesías, yo creí que recuperar parques era sembrar más árboles, arreglar senderos peatonales y hacer reparaciones a jardines, pero seguro estoy equivocada.]

[El tercer punto, es el de identificar y lean bien “Ojalá capturar” a las diez bandas más peligrosas de robo de celulares, sobre este punto, encontré sólo una noticia, del 01 de marzo, en la que desarticularon una banda que se dedicaba al robo y comercio de celulares no sólo en Bogotá, sino en ciudades como Barranquilla y Cali.]

[El cuarto punto, (el más interesante y chistoso para mí) hablaba de reformas legales para que los delincuentes capturados vayan a la cárcel, chistoso porque el alcalde no puede hacer reformas legales, y porque la cárcel no es la solución, pero bueno, menos mal el alcalde es gerente urbanista y no tiene que hacer política con populismo punitivo y barato como algunos congresistas. Aquí es importante resaltar, que aunque La evidencia muestra que las tasas de reincidencia criminal en Bogotá altas, no se debe sólo a que sean delitos excarcelables, pues en muchos de los casos la captura se hizo de forma ilegal o no hay denuncia para proceder.]

[Para el quinto punto, el Bloque de búsqueda para desmantelar las bandas que venden drogas a menores de edad cerca de Colegios, encontré una nota de Zipaquirá pero Zipaquirá no hace parte de Bogotá.]

[El sexto punto, para mí el único en el que se salva el alcalde, es el de tapar huecos, pues bien, a hoy se taparon 3.164 huecos, taparon huecos en la glorieta de la carrera 114 por la calle 80; la calle 167 entre autopista Norte y la carrera 48; la calle 74 entre las carreras 9 y 10. La avenida carrera 11 entre las calles 96 y 97, y entre las calles 99 y 100, esto sólo de los 7.000 huecos priorizados por esta administración.]

[El séptimo punto, otro en el que se raja el alcalde, era el de mejorar la programación de las rutas de Transmilenio, y creo que la rajada es evidente por los múltiples bloqueos presentados durante estos tres meses (que aún no acaban), protestas que hasta ahora, no se ha demostrado que fueron organizadas por la izquierda, sino que más bien es la vía de hecho de mucha gente cansada del sistema de Enrique y sus amigos. Peñalosa dijo que iba a reorganizar y programar las rutas para que el sistema fuese eficiente, hasta ahora sólo he visto noticias de usuarios que se quejan por el recorte de las rutas y porque no se reemplazan, buses que salen de circulación, gente que gasta más tiempo en llegar a su casa y gente que utiliza hasta tres buses para llegar a su trabajo.]

[Para el punto ocho, Peñalosa habló de mejorar la movilidad en cincuenta intersecciones críticas lo que me llama la atención es que no haya utilizado el Sistema Inteligente de Transporte SIT que la anterior administración le dejó a la ciudad, en cambio sí habló de crear un Centro de Control de Tráfico y Seguridad desde el cual se podría monitorear y tomar decisiones en cuanto a los semáforos, prever la congestión y así tomar medidas a priori, algo muy similar a lo que este sistema hace. Aquí quiero mencionar el cambio del sentido de la carrera once, que a mi parecer no sirvió de mucho porque se eliminó un carril y se dejó el mismo sentido, es decir, no se hizo nada.]

[En el punto nueve, encontramos algo parecido a la recuperación del espacio público, pero no, no es el tema del desalojo de los vendedores públicos de la Calle 72 porque eso no se los contó en campaña, aquí dijo que iba a quitar los carros mal parqueados en las vías principales, siquiera no dijo que iba a poner más bolardos.]

[Y en el número diez, está pintar las cebras, medida que no debería hacer parte de un plan de choque, no creo que los 322 peatones que murieron en el 2014 haya sido por falta de cebras, más bien hay que educar a la gente, y por educar no me refiero a que les enseñen a cruzar las calles, no.]

[La conclusión entonces es que ganaron las elecciones vendiendo humo sobre un mesías, el redentor que venía a volver Bogotá una maravilla, y que hasta ahora ha hecho muy poquito, por no decir nada, la ciudad sigue con huecos, se están disminuyendo recursos para la salud, se quiere vender Etb, cerraron jardines, acabaron contratos, se envía al Esmad a mamás con niños en brazos y a señoras que mueren en un andén por falta de atención, se murió un niño con desnutrición, se está reorganizando la salud, se van a vender las acciones de Isagén de la EEB, se a urbanizar la reserva Van Der Hammen, los secretarios del gabinete tienen intereses personales en las labores que desempeñan (como Andrés Ortiz, cuyo problema no es la casita que tiene en la reserva, sino que esa casita con cambio del suelo de la reserva gracias al POT se va a super valorizar).]

[Lo triste es que no le contaron a sus electores y al resto de la ciudadanía ¿para quién están recuperando Bogotá?]
