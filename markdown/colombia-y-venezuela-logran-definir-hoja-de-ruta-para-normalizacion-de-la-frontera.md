Title: Colombia y Venezuela logran definir hoja de ruta para normalización de la frontera
Date: 2015-09-24 12:48
Category: El mundo, Nacional
Tags: Casas de cambio en la frontera, gobierno colombiano, Gobierno venezolano, Ministerio de defensa de Colombia, Ministerio de defensa Venezuela, Situación en la frontera colombo-venezolana
Slug: colombia-y-venezuela-logran-definir-hoja-de-ruta-para-normalizacion-de-la-frontera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Ministros-Venezuela-Colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [24 Sept 2015] 

[Los ministros de Defensa de Colombia y Venezuela, Luis Carlos Villegas y el general Vladimir Padrino López, se reunieron ayer en la sede de la cancillería venezolana, para iniciar la **construcción de los acuerdos que permitirán la normalización progresiva de la situación en frontera**.]

[En la reunión a puerta cerrada uno de los temas más álgidos fue el de las **operaciones cambiarias en la frontera**. De acuerdo con el gobierno venezolano las casas de cambio colombianas son las responsables de la devaluación del bolívar, pues permiten el **canje de bolívares por pesos o dólares a un valor diferente al establecido por el Banco Central de Venezuela**.]

[En las cuatro horas de reunión otros de los asuntos tratados fueron el narcotráfico, la minería criminal, el contrabando, el crimen organizado y el lavado de activos. Con la pretensión de reactivar los mecanismos de cooperación binacional, los ministros acordaron el **siguiente encuentro para el próximo martes en Santa Marta**.]
