Title: Los impactos sociales y ambientales de inversiones chinas en Latinoamérica
Date: 2016-03-29 16:51
Category: Ambiente, Nacional
Tags: Canal de Nicaragua, inversiones chinas
Slug: foro-en-bogota-discutira-impactos-sociales-y-ambientales-de-inversiones-chinas-en-latinoamerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Canal-Nicaragua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Revista Enie 

###### 29 Mar 2016 

Este miércoles  representantes de organizaciones ambientales y sociales de China, Estados Unidos, Argentina, Brasil, Colombia, Ecuador, Perú y Bolivia se reunirán en Bogotá, en el ‘Foro Público Inversiones chinas en América latina: posibles caminos para la incidencia’, con el objetivo de intercambiar opiniones sobre la naturaleza, modalidades y sectores hacia los cuales se dirigen las inversiones chinas en América latina y sus estándares sociales y ambientales, específicamente frente al actual proyecto sobre el Canal de Nicaragua.

El Foro busca ampliar el conocimiento sobre las implicaciones de dichas inversiones, y los estándares sociales y ambientales que las rigen ante un público integrado por organizaciones sociales, estudiantes, defensores de derechos humanos y ambientalistas. El Gran Canal de Nicaragua que conectará el océano Pacífico con el Atlántico y cruzará el territorio nicaragüense de este a oeste, sería uno de los temas principales teniendo en cuenta que es una ruta interoceánica que podría tener ciertas implicaciones sociales y ambientales.

El encuentro está organizado por: IBASE de Brasil, IISCAL de Estados Unidos, y Ambiente y Sociedad de Colombia), con el apoyo de AIDA, ILSA, CEDETRABAJO y la Red por la Justicia Ambiental, y se llevará a cabo de 5 a 7 de la noche, en el segundo Piso Hotel  GHL Los Héroes.

**Los panelistas:**

**Mónica López-Baltodano,** abogada de la  FUNDACIÓN POLPONA Nicaragua, será la encargada de presentar este caso, y explicará a fondo las problemáticas con esta inversión.

**Wawa Wang, **representante de Banktrack quien hablara acerca de la estrategia de inversiones china y su impacto global.

**Paulina Garzón,** investigadora ecuatoriana que escribió un Manual sobre la Iniciativa para las Inversiones Sustentables China-América Latina, quien aportará una nueva perspectiva sobre los mecanismos existentes en las regulaciones chinas para que las comunidades afectadas puedan hacer valer sus derechos.

**Margarita Flórez, **representante de la  Asociación Ambiente y Sociedad, expondrá sobre los convenios de cooperación entre Colombia y China.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
