Title: Comunidades buscan que se suspenda definitivamente desviación del Arroyo Bruno
Date: 2016-05-18 20:27
Category: Ambiente, Nacional
Tags: arroyo Bruno, El Cerrejón, La Guajira, Mineria
Slug: comunidades-buscan-que-se-suspenda-definitivamente-desviacion-del-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario del Norte 

###### [18 May 2016] 

El Tribunal Contencioso Administrativo de La Guajira, resolvió un fallo de tutela, con el que se ordenó la suspensión durante un mes de los actos administrativos que autorizaban la intervención del [arroyo Bruno,](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/)**[afluente principal del Río Ranchería](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/) y fuente de agua de la que dependen comunidades wayúu, afrodescendientes y cabeceras municipales como las de Albania y Maicao**.

La decisión se toma tras la demandas interpuesta por Marcela Lorenza Gil Pushaina, una autoridad tradicional del asentamiento ancestral La Horqueta, a través de la cual se buscaba la reivindicación de los derechos humanos de los pueblos indígenas y afro de la región.

La empresa había logrado obtener el permiso para intervenir el Arroyo Bruno, gracias a que en noviembre de 2014, la Autoridad Nacional de Licencias Ambientales, ANLA, permitió al Cerrejón la modificación del plan de manejo ambiental. Un procedimiento para el cual CORPOGUAJIRA se declaró sin posibilidad de emitir permisos por incapacidad técnica, por lo que la ANLA, que ya había avalado el desvío, fue quien terminó realizando **“estudios amañados”, que dieron paso a la ampliación de la mina en el Tajo La Puente,** es decir, que fue juez y parte, como asegura Danilo Urrea, integrante de Censat Agua Viva.

Pablo Ojeda Gutiérrez, abogado de la demandante y Rogelio Ustate, lider de la comunidad de Tabaco, aseguran que no solo se debe aplicar el derecho a la consulta previa a la comunidad **La Horqueta, sino a todas las comunidades que habitan alrededor del área de influencia del proyecto** La Puente y el arroyo Bruno, pero además que están sobre la línea férrea de Cerrejón.

Cabe recordar  la desviación hace parte del proyecto de expansión del proyecto minero de El Cerrejón, que ha sido denunciado por la población que ve en riesgo su permanencia territorial. Además **no se trata únicamente de los 3,6 kilómetros que se le desviarían al [arroyo Bruno,](https://archivo.contagioradio.com/13-razones-para-no-desviar-el-arroyo-bruno/)también son los 9,3 km que también se piensa desviar a esta misma fuente para el 2020**. Pero además, se tiene planeado intervenir al arroyo Cerrejón, Tabaco y el río Palomino.

Teniendo en cuenta que el riesgo continúa, **las comunidades sostienen que mantendrá la lucha jurídica y la movilización**, para impedir que otra fuente de agua se vea afectada, como ya sucedió con otros 27 cuerpos hídricos del departamento por cuenta de la actividad minera de El Cerrejón.

<iframe src="http://co.ivoox.com/es/player_ej_11589444_2_1.html?data=kpaimp6YeJWhhpywj5WaaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncbPjyMrZy9SPmdTowtnSjZKPh9Di1MrX0ZCns87pz87Rw8mPhc%2FXxtjh1MbQb6%2FZyNfOjcnJb7Whhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
