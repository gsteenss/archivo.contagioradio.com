Title: A 28 años del asesinato de Bernardo Jaramillo se abre una posibilidad de Justicia
Date: 2018-05-21 11:44
Category: Paz, Política
Tags: Bernardo Jaramillo, CIDH, Jael Quiroga, Unión Patriótica
Slug: a-28-anos-del-asesinato-de-bernardo-jaramillo-se-abre-un-una-posibilidad-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/zona-cero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [21 May 2018] 

Tras 28 años del asesinato de Bernardo Jaramillo Ossa, la Fiscalía General de la Nación llamó a rendir indagatoria a 12 agentes del Estado. Decisión que para Jael Quiroga, directora de la Corporación Reiniciar y militante de la Unión Patriótica, **hace parte de la búsqueda de verdad que finalmente está encontrando un camino en Colombia**.

Las doce personas llamas a testificar son **9 integrantes que fungían como escoltas del entonces DAS, en el esquema de seguridad de Jaramillo y 3 policías**, que, de acuerdo con las investigaciones adelantadas, harían parte del homicidio que se perpetuó el 22 de marzo de 1990.

Asimismo, Quiroga afirmó que este hecho hace parte del cumplimiento que el Estado colombiano debe tener con las diferentes recomendaciones que hizo la Comisión Interamericana de Derechos Humanos, y **en donde se solicita que se re abran los procesos penales y disciplinarios de los crímenes cometidos contra la Unión Patriótica**. (Le puede interesar:["Luego de 25 años Corte IDH fallará sobre genocidio de la Unión Patriótica"](https://archivo.contagioradio.com/luego-25-anos-corte-idh-fallara-genocidio-la-union-patriotica/))

“El proceso va muy bien encaminado. Hay una sensibilidad individual de algunos fiscales por sacar esto adelante. Hay mucha gente que en este país se ha dado cuenta que lo que pasó con la Unión Patriótica fue muy grave, y que nadie puede justificar la muerte y desprotección de tanta gente” afirmó Quiroga.

### **Bernardo Jaramillo ¡Venga esa mano país!** 

El asesinato de Bernardo Jaramillo hace parte del genocidio de los más de 6 mil integrantes de la Unión Patriótica. En el momento de su asesinato era candidato a la presidencia de Colombia. Según Jael Quiroga, Jaramillo era un “gran demócrata”, y lo describe como **un hombre visionario, que quería generar una convergencia de partidos, sectores y movimiento social**.

“Siempre insistió en que la Unión Patriótica, era un partido de reforma sociales, un partido social demócrata, en donde podían entrar todos y todas las vertientes ideológicas que tenían un denominador común: **superar las circunstancias de pobreza, desigualdad y exclusión política” expresó Quiroga**.

### ** Las recomendaciones de la CIDH** 

En el año 2012 el gobierno nacional expidió un decreto especial con la finalidad de proteger a los miembros de la Unión Patriótica y del Partido Comunista, y dio cumplimiento a la solicitud hecha por la CIDH que dictaminó medidas cautelares sobre los sobrevivientes, integrantes y dirigentes de esta organización política.

“En este país se tienen que tener opciones políticas, no es justo que este todavía tan interiorizado el bipartidismo” afirmó Quiroga y agregó que **“la Unión Patriótica viene de exterminio y ha renacido”.**

<iframe id="audio_26093971" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26093971_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
