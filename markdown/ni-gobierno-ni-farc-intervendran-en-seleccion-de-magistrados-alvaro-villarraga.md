Title: "Ni Gobierno, ni FARC intervendrán en selección de magistrados": Álvaro Villarraga
Date: 2016-08-12 16:27
Category: Entrevistas
Tags: jurisdicción especial para la paz, Papa Francisco, paz, proceso de paz
Slug: ni-gobierno-ni-farc-intervendran-en-seleccion-de-magistrados-alvaro-villarraga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/jurisdiccion-especial-de-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: posta.com.mx 

###### [12 Ago 2016] 

El Papa Francisco, el Secretario General de las Naciones Unidas, la Sala Penal de la Corte Suprema de Justicia, la delegación en Colombia del Centro Internacional de Justicia Transicional (ICTJ), la Comisión Permanente del Sistema Universitario del Estado serán quienes elijan a los magistrados que harán parte de la Jurisdicción Especial para la Paz, un acuerdo que para Álvaro Villarraga, director de la Fundación Cultura Democrática e investigador del Centro Nacional de Memoria Histórica, se trata de un **mecanismo apropiado para demostrarle a los colombianos que con la JEP** se enfrentará la impunidad que ha imperado en el país.

“**Se trata de un mecanismo acertado, muy ponderado,** ni el gobierno, ni las FARC tendrán incumbencia en la selección de estos magistrados (…) Opositores han dicho que habría magistrados a favor de las FARC, eso sería absurdo como sería absurdo que fueran a favor del gobierno”, dice el analista.

Son 20 magistrados colombianos y 4 extranjeros para el Tribunal de Paz, 18 magistrados con nacionalidad colombiana y 6 magistrados extranjeros para las Salas de Justicia, además de una lista  adicional de 19 nacionales y 5 extranjeros para el Tribunal y las Salas en caso de que se requiera aumentar el número de magistrados o sustituir a alguno de los miembros, según se ha explicado por medio del comunicado emitido este viernes desde La Habana.

Jueces que de acuerdo con Villarraga, serán seleccionados a partir de los **“criterios éticos, académicos y de experiencia judicial, de las más altas calidades”**, y por medio de un proceso de calificación bastante estricto teniendo en cuenta que deberán contar con por lo menos cuatro de las cinco personas e organismos que se encargarán de elegir a las y los jueces.

La presencia de instituciones educativas como uno de los determinadores de esta tarea, es esencial para el investigador en la medida de que se trata de la visión desde la academia. La Comisión Permanente del Sistema Universitario del Estado, reúne las **universidades públicas del país, las cuales deberán concertar un mecanismo a través de sus rectores para escoger candidatos que cumplan los requisitos para ser magistrados,** explica Villarraga.

Por otra parte, si bien, desde la iglesia católica no habría como tal una experticia en el ámbito jurídico, lo cierto es que el Papa Francisco, puede brindar mayor tranquilidad a los colombianos que en su mayoría profesan la religión católica. Según el investigador, **el sumo pontífice ha evidenciado su compromiso con la paz de Colombia y además representa no solo un liderazgo religioso, sino moral y político** que pueden dar mayor confianza al pueblo colombiano.

“En Colombia ha imperado la impunidad. Existen normas formales que pueden decirse coherentes y que si se aplicaran, la justicia sería eficiente, pero no ha funcionado, esta Jurisdicción es un mecanismo de justicia frente a esa impunidad”, concluye el director de la Fundación Cultura Democrática.

<iframe src="http://co.ivoox.com/es/player_ej_12528820_2_1.html?data=kpeilJ2cdpGhhpywj5eXaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncaLg18bf0ZC6rc3gwtffw8zFaZO3jKvi0MnFp8qZpJiSpJjSb6Tpzdni1MaPiMbh0Mjfh6iXaaKl1c7Qw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
