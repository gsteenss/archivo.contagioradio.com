Title: Foro "Una Mirada al Desplazamiento Forzado en Colombia"
Date: 2015-09-09 17:16
Category: eventos, Nacional
Tags: Abogados Sin Fronteras, Comisión de Justicia y Paz Colombia, Desplazamiento forzado, Universidad de los Andes
Slug: una-mirada-al-desplazamiento-forzado-en-la-universidad-de-los-andes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Desplazados-e1481538333709.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.reporterosasociados.com.co]

###### [9 Sept 2015]

Mañana  se realizará el foro y presentación del informe, "Una mirada al desplazamiento forzado, persecución penal, aparatos organizados de poder y restitución de tierras en el contexto colombiano". El evento inicia a las 7:30 de mañana y va hasta las 4:30 de la tarde en el  Edificio Mario Laserna A de la Universidad de los Andes.

"El foro y el informe destacan que hay herramientas disponibles para que las familias tengan acceso a la justicia", asegura Pascal Paradis, director general de Abogados Sin Fronteras Canadá, quien explica que el informe resalta el proceso de retorno de las comunidades de Curvaradó y Jiguamiandó.

Paradis, asegura que tras la presentación del informe, se tendrá reuniones para dar seguimiento a la situación de las comunidades de Curvaradó y Jiguamiandó, con el fin de dar herramientas al gobierno, las poblaciones afectadas y la ciudadanía  para enfrentar esta problemática.

El foro se desarrollará mediante 3 mesas de trabajo, en las que se analizará temas como la tipificación del delito de desplazamiento forzado en Colombia, la autoría mediata y aparatos organizados en poder, y la mesa sobre retorno y restitución para las víctimas de desplazamiento forzado.

Este foro contará con la participación de panelistas nacionales e internacionales, que han estudiado la problemática del Desplazamiento Forzado en Colombia. El **evento se realiza en coordinación con Abogados sin Fronteras Canadá, la **Comisión Intereclesial de Justicia y Paz, y la Universidad de los Andes.
