Title: Grl. Palomino hace graves señalamientos contra jóvenes procesados y abogados defensores
Date: 2015-07-14 16:15
Category: DDHH, Entrevistas, Judicial
Tags: acusados, congreso de los pueblos, falso positivo, jovenes, judicial, Manuel Garzón, palomino, provenir
Slug: grl-palomino-hace-graves-senalamientos-contra-jovenes-procesados-y-abogados-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/palomino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4804118_2_1.html?data=lZ2dlpaVfI6ZmKiak5aJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLi1srZjazFttuZpJiSpJjSb6Lpxc7cjZaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="http://www.ivoox.com/player_ek_4804117_2_1.html?data=lZ2dlpaVe46ZmKiakpyJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLi1srZjazFttvjz5DO18nNs4ymjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manuel Garzón, Abogado defensor] 

###### [14 jul 2015] 

El equipo de defensa de los 13 jóvenes capturados el 8 de julio, bajo la acusación de ser responsables de las explosiones ocurridas en Bogotá la semana anterior, rechazaron las declaraciones que el General de la Policía, Rodolfo Palomino, dio a la emisora La W, al considerarlos "señalamientos estigmatizantes contra los jóvenes y sus abogados defensores".

En dichas declaraciones, en Grl. Palomino cuestionó que cada uno de los imputados contara con un abogado, y la procedencia laboral de los mismos, indicando además que las demoras en el proceso se presentan por el equipo de defensa.

Los y las abogadas anuncian que emitirán un comunicado para que las instancias nacionales e internacionales encargadas de regular y vigilar el ejercicio de la abogacía, hagan un seguimiento a las irregularidades que se vienen cometiendo tanto en el proceso jurídico como con los hechos que le rodean.

Según señaló el abogado Manuel Garzón, por ejemplo, las declaraciones del Grl. de la Policía ponen un manto de duda sobre el ejercicio de la bancada de defensa, lo cual constituye una amenaza a su seguridad e integridad; además, el conocimiento del General de la composición del grupo de abogados indicaría seguimiento ilegales contra los mismos. Por otra parte, en lo relacionado con los jóvenes capturados, las declaraciones desconocen el derecho a la defensa de los acusados, establece indicios falsos y viola la presunción de inocencia.

Tanto los medios tradicionales de información como entidades estatales han intentado justificar la captura de los jóvenes con la presentación de videos y fotografías, sin embargo, según expone la defensa, algunos de estos videos y fotografías presentados como "pruebas reina" no han sido parte del material probatorio de la Fiscalía.

"La misma representante de la Fiscalía fue enfática en reiterar que ella no había hablado ni una sola vez sobre los hechos de Porvenir", declara Garzón, lo que demuestra que uno es el juicio que se realiza mediáticamente, y otro el proceso jurídico que se adelanta contra los capturados.

"Exigiremos la retractación del General Palomino, y que se suspenda la injerencia indebida que hace ante la opinión publica sobre el proceso", puntualizó el abogado Garzón.

El siguiente es el comunicado definitivo de los abogados de la defensa en relación a las declaraciones del general Palomino, y la forma como se ha manejado el proceso.

***COMUNICADO A LA OPINIÓN PÚBLICA DE LOS ABOGADOS DEFENSORES DE LOS JÓVENES DETENIDOS EL 8 DE JULIO EN LA CIUDAD DE BOGOTÁ***

*Los  abogados defensores de los jóvenes detenidos el día 8 de julio del presente año, nos pronunciamos enérgicamente contra el manejo de la información referida al caso por parte del director de la Policía General Rodolfo Palomino.*

*Refiriéndose a la situación procesal de los jóvenes detenidos, en entrevista realizada el día de hoy a un medio radial, el General Palomino, de manera insidiosa estigmatizó el ejercicio profesional de los abogados que hemos asumido la representación judicial de éstos, al sacar a relucir como “sospechoso” el tipo de procesos o causas, en que algunos han intervenido en el pasado.*

*Este hecho, constituye una transferencia desleal y estigmatizante a nosotros como abogados, de las sindicaciones que han pesado contra nuestros defendidos en otros procesos, para extenderlas ahora al grupo de jóvenes detenidos.*

*Tal manifestación, implica un ataque frontal al ejercicio libre e independiente de la abogacía y un abierto desconocimiento a los "Principios básicos sobre la función de los abogados", aprobados por las Naciones Unidas, que orientan que los abogados no podrán ser expuestos a persecución, ni “serán identificados con su cliente ni con las causas de su cliente como consecuencia del desempeño de su función”.*

*Las declaraciones del General Palomino ponen en evidencia el seguimiento ilegal que la Policía realiza a nuestra vida profesional y personal, de lo cual se valió, no solo para cuestionarnos sino también para atacar la presunción de inocencia de nuestros representados, al extender a ellos el estigma que nos ha impuesto. Situación que se suma a la manifestación tendenciosa que efectuó en el sentido que el número plural de defensores le confirma a la Policía a quiénes ha detenido.*

*Las manifestaciones del mando policial, reducen aún más las garantías para el ejercicio profesional de los abogados defensores, ponen en riesgo la vida e integridad personal de quienes ejercemos la representación de los jóvenes en este caso, a la par que constituye una clara condena anticipada contra ellos.*

*Preocupan tales expresiones públicas del General, cuando durante las audiencias personas de civil, que no se han acreditado como periodistas, vienen tomando fotografías, cuyo propósito y destinación desconocemos, a los abogados defensores y nuestros representados.*

*Exigimos la retractación inmediata del General de la Policía y reclamamos que en adelante se abstenga dicha institución de interferir con el curso del proceso, filtrando pruebas que para la defensa han permanecido ocultas y efectuando aseveración que constituyen la condena mediática de nuestros prohijados.*

*Solicitamos a la autoridad de control competente, iniciar la investigación disciplinaria contra el General Palomino, por violentar con sus atestaciones los presupuestos de la sociedad democrática que él dice defender.*

*Abogados defensores de los jóvenes detenidos en Bogotá el 8 de julio*
