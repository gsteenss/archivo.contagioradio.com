Title: Andrés Felipe Arias debe ir a un centro carcelario si es extraditado a Colombia
Date: 2019-02-22 15:42
Author: ContagioRadio
Category: Nacional, Política
Tags: Andres Felipe Arias, extradicion
Slug: andres-felipe-arias-como-cualquier-colombiano-condenado-por-corrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/ANDRÉS-FELIPE-ARIAS28SEP-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 22 Feb 2019 

A través de una carta en la que el Departamento de Estado de Estados Unidos expresa haber revisado los documentos y pruebas relacionadas al **exministro de Agricultura, Andrés Felipe Arias,** condenado por el caso Agro Ingreso Seguro, el departamento de asuntos internacionales autorizó la extradición de Árias quien que fue sentenciado por la Corte Suprema de Justicia a 17 años de prisión.

Según el comunicado firmado por **Tom Heinemann, asesor legal del departamento,** Arias no sería objeto de amenazas o torturas en Colombia lo que garantizaría la seguridad del condenado, según la justicia estadounidense, únicamente se le impediría ser extraditado si existiera sobre el detenido un riesgo de tortura en el país que lo recibe.

**El abogado y defensor de DD.HH, Alirio Urib**e señaló que esta acción obedece a una cooperación judicial entre naciones, sin embargo apunta que la verdadera preocupación es la posición del presidente Duque y figuras como Francisco Santos y Álvaro Uribe quienes han abogado por su libertad, por tanto le inquieta que al llegar a Colombia quieran trasladar a Arias a "una guarnición militar o darle una detención domiciliaria" en lugar de ser llevado a un centro de máxima seguridad.

Frente a esta inquietud, Uribe aspira a que la Corte Suprema de Justicia mantenga la independencia que ha demostrado desde que se inició el juicio en contra del exministro de Agricultura quien fue declarado culpable por entregar beneficios destinados a los campesinos para tierras y proyectos rurales a particulares,  concretamente a personas que apoyaron la campaña de **Álvaro Uribe Velez.[(Le puede interesar: Ándrés Felipe Arias y el escándalo de corrupción de Agro Ingreso Seguro)](https://archivo.contagioradio.com/en-estados-unidos-se-definira-situacion-de-andres-felipe-arias/)**

La defensa de Arias ha intentado su liberación en Estados Unidos, el otorgamiento de asilo político o una segunda instancia en Colombia, para revisar la decisión judicial adoptada, sin embargo Alirio Uribe indica que las condenas que ya han sido proferidas no pueden ser afectadas por la reciente reforma constitucional, por lo que **"no hay ninguna posibilidad de que las decisiones de segunda instancia puedan ser revisadas o ser apeladas".**

Frente a la especulación que existe sobre si el regreso de Arias a Colombia sería usado como una ficha de cambio para establecer una negociación en el proceso del dirigente político, Jesús Santrich, el abogado descarta que pueda tratarse de un intercambio pues "hasta el día de hoy no se conocen pruebas que vinculen al exguerrillero con narcotráfico después de la firma del acuerdo de paz.

<iframe id="audio_32768262" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32768262_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
