Title: Corrupción en la Universidad Distrital
Date: 2019-08-16 19:01
Author: CtgAdm
Category: Educación, Entrevistas
Tags: corrupción, educación pública, Muñoz Prieto, U.Distrital
Slug: corrupcion-universidad-distrital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/391073_193613_1-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@udistrital] 

William Muñoz Prieto, quien ejercía como Director del Instituto de Extensión de la Universidad Distrital Francisco José de Caldas desde junio del 2012, **es investigado por el presunto uso de recursos públicos, en beneficio personal y de terceros**, durante su periodo de actividad. Todo se dio tras descubrir facturas por compras en almacenes de ropa, cuentas en costosos restaurantes, tiquetes aéreos para viajes, los cuales se desconoce que fueran por razones laborales. (Le puede interesar: [El ICBF se convirtió en una máquina de corrupción y vulneración de derechos )](https://archivo.contagioradio.com/el-icbf-se-convirtio-en-una-maquina-de-corrupcion-y-vulneracion-de-derechos/)

La Procuraduría General, encargada de esta investigación, encontró que, en su administración Muñoz giró alrededor de 333 cheques por más de \$10.495.100.000 COP, dinero destinado a una cuenta a nombre de la Universidad Distrital; hechos que ya están en conocimiento del Consejo Superior de la Universidad (CSU).

Según Julián Báez, represéntate ante el CSU, una de las funciones que lleva a cabo el Instituto de Extensión, es generar contratos y convenios con externos que apoyen las actividades estudiantiles, así, anualmente diferentes entidades públicas y privadas eran convocadas para ofrecer diferentes tipos de apoyo que impulsaran obras y proyectos universitarios. “Durante el periodo de dirección de William, vimos por medio de investigaciones de la Dirección Universitaria, que **cada vez era menor el presupuesto disponible para nosotros, sabiendo que esta oficina generaba grandes sumas**, pero no había justificación donde estaban estos fondos”, declaró.

El representante estudiantil informó que, desde la posesión del nuevo Rector de la Universidad se han planteado varias averiguaciones, “tal vez esta sea solo cabeza, detrás de las fallas al interior de la Universidad, queremos que los recursos sean destinados para el apoyo de nosotros, **los estudiantes y que sigamos siendo un referente académico y no de corrupción para Colombia”.**

### **¿Qué hay debajo de la punta del iceberg en la Universidad Distrital?** 

Según Báez, se pretende llevar a cabo dentro de la nueva rectoría varios temas, que desde hace años rondan las diferentes áreas de la Universidad, “algunos son secretos a voces, otros hacen parte de la caja de pandora que pretendemos destapar”.

En primera instancia, pretendemos adentrarnos en el costo-beneficio de los almuerzos estudiantiles, este apoyo alimentario que debe ser en pro de aquellos que no pueden costear sus alimentos en casa o fuera de la universidad, el cual lleva años a cargo de la misma empresa contratista, y que según Báez, no ofrece **un producto adecuado bajo los estándares nutricionales y de calidad. **(Le puede interesar: [Seguridad Alimentaria, la tarea pendiente en Colombia ](https://archivo.contagioradio.com/seguridad-alimentaria-la-tarea-pendiente-en-colombia/)

También los estudiantes quieren exponer aquello que han llamado contratos ´sastre´ (hechos a la medida), que estarían causando demoras en entrega de obras. Uno de los casos que destacan es de 2018, con la compra de sillas para una de las sedes, las cuales fueron facturadas en aproximadamente \$1.500.000 cada una, cifra que generó sospecha.

Jóvenes de la sede que descubrieron esta situación hicieron 3 cotizaciones aparte, y encontraron que estás no tienen un valor superior a \$320.000 cada una.

Báez concluyó que esperan avances en la investigación contra Muñoz Prieto, y que se investiguen las demás irregularidades que se podrían estar presentando, para evitar que la imagen de la universidad se vea desdibujada. **“Queremos que siga siendo un referente académico y no de corrupción para Colombia**”.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
