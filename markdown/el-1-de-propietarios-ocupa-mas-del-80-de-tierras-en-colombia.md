Title: El 1% de propietarios ocupa más del 80% de tierras en Colombia
Date: 2017-07-05 17:42
Category: DDHH, Nacional
Tags: concentración de tierras, Oxfam
Slug: el-1-de-propietarios-ocupa-mas-del-80-de-tierras-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 Jul 2017]

La organización internacional OXFAM revelo un dramático informe denominado “Radiografía de la Desigualdad: lo que dice el último censo agropecuario sobre la distribución de tierra en Colombia”, en que se confirma que el **1% de las explotaciones más grandes acaparan más del 80% de las tierras rurales**, mientras que el 99% restante de los cultivos que hay tan solo ocupan el 19%.

Otro de los datos que arrojó este estudio es que los predios de más de **500 hectáreas ocupaban en 1970 solo 5 millones de hectáreas mientras que en el año 2014 pasaron a ocupar 47 millones**. Es decir que su tamo promedio pasó de 1.000 a 5.000 hectáreas. Hecho que, para OXFAM demuestra como los intentos pasados en Colombia por generar una reforma agraria, solo terminaron provocando que aumentara la concentración  (Le puede interesar: ["Cinco propuestas para evitar el fracaso total de la Ley de Restitución de Tierras"](https://archivo.contagioradio.com/42667/))

Otra de las preocupaciones que evidencia esta radiografía, es que “**las grandes fincas contienen extensos territorios improductivos u ocupados por la ganadería extensiva,** con un área insignificante dedicada a cultivos agrícolas y casi siempre dedicadas a la agroexportación.

De igual forma el informe señala que estas grandes concentraciones de tierra **son el resultado de décadas de violencia y políticas públicas sesgadas que “han favorecido a latifundio especulador y rentista”**. [(Le puede interesar: "Fondo de tierras contaría con 7 millones de hectáreas de Baldíos") ](https://archivo.contagioradio.com/fondo-de-tierras-para-la-paz-contara-con-7-millones-de-hectareas/)

En cuanto a cuál ha sido la población que menos acceso a tenido a la tierra, el estudio revela que son **las mujeres en donde tan solo 26% de las unidades productivas están a cargo de ellas** y sus explotaciones son más pequeñas, predominando las menores a 5 hectáreas, con más dificultades para acceder a maquinaria, crédito y asistencia técnica.

Frente a la titulación de la tierra, las cifras también son alarmantes, de acuerdo con el último censo agropecuario realizado, **en el 43% de las unidades productivas de más de 2.000 hectáreas no se conoce cuál es la forma de tenencia**, razón por la cual se recomienda realizar un censo con un catastro completo y actualizado, que ayude a proteger los derechos colectivos de las comunidades campesinas, indígenas y afrodescendientes.

[Radiografia de La Desigualdad](https://www.scribd.com/document/353020326/Radiografia-de-La-Desigualdad#from_embed "View Radiografia de La Desigualdad on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_83489" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/353020326/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-aax5YrRUlWWvp8NpOjoU&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7707442258340462"></iframe>
