Title: Con rock y fusiones musicales, Festival Centro celebra una década
Date: 2019-01-31 14:53
Author: AdminContagio
Category: Cultura
Tags: Cultura, Festival Centro
Slug: con-rock-y-fusiones-musicales-festival-centro-celebra-una-decada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/49169628_1817444735032027_985273387557322752_n-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

Prepárese por que hoy llega el Festival Centro, un espacio que en su décima versión nos sigue entreteniendo con los sonidos populares y alternativos enmarcados en la música independiente de nuestro país.

Este evento que inicio como una iniciativa de la **Fundación Gilberto Álzate Avendaño FUGA** llega este 2019 con más de 20 artistas, 5 escenarios, 6 franjas musicales y dos conciertos especiales de inauguración y cierre, en los que se presentaran artista como Moügli, 1280 Almas, Rubén Albarrán-No DJ Set, Adriana Lucía, Nelson y sus Estrellas, La Billos Caracas Boys, La Etnnia, Durazno, Tequendama, entre otros.

El Festival Centro contará con espacios como el auditorio y el Muelle de la FUGA, La Milla en el Bronx Distrito Creativo, dos auditorios en el Centro Cultural Gabriel García Márquez y el Teatro Jorge Eliécer Gaitán.

Por ser el primer día del festival se ofrecerá un concierto totalmente gratis y tendrá en escena a la banda de rock colombiana **Lika Nova; Mougli y los rockeros 1280 Almas, La Etnia será el penúltimo en tocar para darle paso al proyecto de Visitante de Calle 13 Trending Tropics.** Todas estas actividades se realizarán en el teatro Jorge Eliécer Gaitán.

Si no quiere perderse ningunos de los artistas, costo de boleterías y demás programación del Festival Centro que ira hasta el 3 de febrero[puede consultar la programación aquí](http://festivalcentro.fuga.gov.co/programacion/)
