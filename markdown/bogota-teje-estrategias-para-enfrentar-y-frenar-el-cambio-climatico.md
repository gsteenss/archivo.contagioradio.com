Title: Bogotá está lista para el "Encuentro de las Américas Frente al Cambio Climático"
Date: 2015-09-13 10:00
Category: Ambiente, Nacional
Tags: cambio climatico, cumbre de cambio climático Bogotá 2015, Cumbre de París por el cambio climático 2015, defensa del medio ambiente, Encuentro de las Américas Frente al Cambio Climático, Estrategias contra cambio climático
Slug: bogota-teje-estrategias-para-enfrentar-y-frenar-el-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/pantallazo_opt_4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:culturarecreacionydeporte.gov.co 

<iframe src="http://www.ivoox.com/player_ek_8322097_2_1.html?data=mZiflJWde46ZmKiak56Jd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDb0NmSpZiJhZKf1MqY0tfJtMLmwpDdw9fFb8bgjIqflKrSp9bZz9nf0ZDIqYzgwtiYo9KJh5SZop7fy8jFt4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Hildebrando Vélez, Ambientalista] 

###### [11 sep 2015] 

Del 20 al 23 de septiembre, Bogotá será el escenario del **"Encuentro de las Américas Frente al Cambio Climático",** donde la ciudadanía, expertos y académicos, desarrollarán estrategias para enfrentar y frenar el cambio climático en el país y el mundo.

El ambientalista Hildebrando Vélez, indica que con la participación de distintos sectores y agentes sociales **se "trazarán los pasos para crear un pacto entre ciudades y ciudadanías**, entre ciudades y agentes de la vida rural y agraria en Latinoamérica", teniendo en cuenta que en diciembre se llevará a cabo la COP21 en París, donde las naciones se encontrarán con el objetivo de realizar un acuerdo mundial para reducir sus emisiones de gases de efecto invernadero.

Departamentos como el Tolima que sufren graves incendios forestales, la desaparición del Nevado el Ruíz, la deforestación de la Amazonía, son problemas que evidencian el avance del calentamiento global y ante los cuales es necesario realizar acciones urgentes y a largo plazo.

“Para Colombia esta cumbre da pie para que los movimientos sociales, de todas las vertientes ideológicas y políticas nos encontremos para avanzar en una agenda ambiental, transversal a todas las luchas sociales del país”, expresa Vélez.

Tras la realización de este ** evento se pretende crear un documento con la Alcaldía de Bogotá, donde se recogerá las causas del calentamiento  y propuestas para enfrentar el mismo**. Esta carta será presentada en diciembre durante la cumbre en París sobre el cambio climático.

**Agenda Encuentro de las Américas Frente al Cambio Climático Bogotá 2015**

<iframe id="doc_80320" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/280302082/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
 
