Title: Asesinan a líder social Omar Moreno en Nariño
Date: 2020-12-29 22:56
Author: CtgAdm
Category: Nacional
Slug: asesinato-a-lider-social-omar-moreno-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-29-at-8.17.51-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El día de hoy se conoció **el asesinato de Omar Moreno, líder social** y integrante del Sindicato de Trabajadores Agrícolas Independientes del Meta – SINTRAGRIM, sobreviviente de la Unión Patriótica y al ocurrido mientras se dirigía en un taxi desde el corregimiento de Llorente hacia Pasto en el departamento de Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la información suministrada por sus allegados, **el taxi donde se movilizaba el líder social, fue interceptado por una motocicleta, lo sacaron del vehículo y le propiciaron varios impactos de bala hasta causarle la muerte.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo en denuncias posteriores se ha afirmado que en la región circulan videos en los que se evidencian las torturas que habría sufrido el lider. ***"La comunidad ha informado que OMAR, fue asesinado y desaparecido por que circularon en esa región imágenes y videos desgarradores a la tortura y asesinato de la cual fue sometido, por un grupo armado que se hace llamar OLIVER SINISTERRA."***

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **&lt;&lt;*Aun lejos de su tierra violencia genocida lo alcanzó. Abrazo solidario a Aide y familia*&gt;&gt;.**
>
> <cite>Gabriel Becerra - Unión Patriótica </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Esta muerte se suma a la de **Armando Guanga,** líder indígena, asesinado el pasado 23 de diciembre. **Alberto Anay**, docente y líder social, quién fue encontrado asesinado el 27 de diciembre y **Jhonny Walter Castro**, líder social e integrante de la Mesa Nacional De Víctimas asesinado el 20 de noviembre. De acuerdo con el registro de [Indepaz](http://www.indepaz.org.co/), en lo corrido de 2020 van **308 líderes sociales asesinados y 90 masacres perpetradas.**

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/fundepaz/status/1344052130144014337","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/fundepaz/status/1344052130144014337

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

Le puede interesar leer: [Jonny Walter Castro, líder social es asesinado en Nariño](https://archivo.contagioradio.com/jonny-walter-castro-lider-social-es-asesinado-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:heading -->

Nariño se desangra, urge presencia del Estado
---------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Diferentes líderes y organizaciones e**xigen al Estado, que se garanticen investigaciones** minuciosas para dar con los perpetradores de estos hechos, además que brinde de una vez por todas las **garantías de seguridad para ejercer su labor.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***&lt;&lt;Exigimos a las autoridades activar los mecanismos de búsqueda urgente, al igual que se investigue y se juzgue a los perpetradores de este horrendo crimen. Así mismo, Reclamamos se entregue el cuerpo del compañero OMAR MORENO IBAGUE.**&gt;&gt;*
>
> <cite>Fensuagro y Sintragrim</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Le puede interesar leer: [Rechazo de la tortura, homicidio y desaparición forzada del líder Omar Moreno Ibagué - Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/rechazo-de-la-tortura-homicidio-y-desaparicion-forzada-del-lider-omar-moreno-ibague/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los asesinatos a líderes sociales han sido constantes desde la firma al acuerdo de paz, es por esto que tanto líderes como organizaciones sociales claman porque se cumpla lo pactado en la Habana y puedan ejercer su labor como defensores de derechos humanos, sin el miedo a morir en sus hogares o en sus regiones.

<!-- /wp:paragraph -->
