Title: En riesgo más de 15 especies animales en Humedal Tibaguya
Date: 2017-02-24 16:46
Category: Ambiente, Nacional
Tags: Bogotá, Humedal Tibaguya
Slug: en-riesgo-mas-de-15-especies-animales-por-ampliacion-de-planta-de-tratamiento-en-humedal-tibaguya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/tingua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Abriendo Espacios] 

###### [24 Feb 2017] 

El humedal Tibaguya, ubicado en el noroccidente de la ciudad de Bogotá, es hábitat de una gran cantidad de especies migratorias y nativas como las tinguas, patos canadienses, comadrejas, murciélagos, garzas, patos tibaguya, zarigüeyas, búhos y ranas sabaneras. Sin embargo, **esto cambiaría si se realiza la ampliación de la planta de aguas residuales del Salitre como lo denuncia el Colectivo Somos Uno.**

La comunidad y el Colectivo Somos Uno, llevan dos años realizando la campaña educativa y social **"Yo Soy Humedal" que tiene como objetivo concientizar a la ciudadanía por la defensa de la Reserva Tibaguya**

**El colectivo Somos Uno asegura que si se realiza esta construcción sobre el humedal, se destruiría el cuerpo de agua vital para estas especies**.  Lugar en donde habitan aves migratorias como el pato canadiense que realiza un viaje desde el centro de Alaska hasta el noroccidente de Estados Unidos, teniendo como paso obligado lugares que incluyen este humedal bogotano.

Para evitar que esto suceda, han estado **realizando actividades culturales con los jóvenes, jornadas de agricultura urbana con los barrios vecinos al humedal**,  proyectando películas al aire libre, programando pequeños espacios musicales de todos los generos y caminatas ecológicas al humedal, con el fin de que las personas se familiaricen con este y conozcan su importancia ecológica.

Uno de los integrantes del colectivo "Somos Uno" afirmó que aunque en octubre del año pasado, se firmó la licitación  ganada por las empresas  **Aktor,Aqualia y  Cass Constructor,** no obstante,  la comunidad se muestra firme en su postura*"***buscamos hacer un llamado a toda la ciudadanía para defender un espacio que no es solo de ellos, es de todos."**

Pasar sobre los hogares de estas especies sin hacer ningún tipo de prevención  podría ser fatal para ellos, **hasta el punto de ponerlos en peligro de extinción**, como en el caso del pato Tibaguya, animal propio de este lugar, y la muerte de muchos mamíferos como comadrejas e incluso especies de anfibios. Le puede interesar: ["Descontaminación del río Bogotá afectaría al Humedal Tibaguya"](https://archivo.contagioradio.com/pretenden-danar-humedal-tibaguya-para-descontaminar-rio-bogota/)

<iframe id="audio_17375274" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17375274_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
