Title: La lideresa María Caicedo Muñoz es hallada sin vida tras 7 días de desaparición forzada
Date: 2018-10-27 07:03
Author: AdminContagio
Category: DDHH, Nacional
Tags: Cauca, Lider social, lideresa social
Slug: hallada-sin-vida-la-lideresa-maria-caicedo-munoz-tras-7-dias-de-desaparicion-forzada
Status: published

###### Oct 27 de 2018 

Fue hallado sin vida el cuerpo de la lideresa **Maria Caicedo Muñoz**, desaparecida el pasado sábado 20 de octubre.

Según información de la comunidad el cuerpo fue encontrado en el río Micay a la altura de vereda La Cacharra, Corregimiento Puerto Rico del Municipio de Argelia, Cauca.

**Contexto:**

La lideresa había sido desaparecida forzadamente **por un grupo de hombres armados el pasado sábado 20 de octubre en horas de la madruga**da.

Según los denunciantes, la integrante del Comité de Mujeres de la Asociación de Mujeres Campesinas de Argelia AMAR, se encontraba en su vivienda ubicada **en el Corregimiento Sinaí, Vereda Desiderio Zapata**, cuando pasadas las 12 de la noche, los hombres tocaron a la puerta preguntando por María mientras intimidaban a sus hijas.

Acto seguido, **sacaron por la fuerza a la lideresa de su habitación y sus hijas fueron atadas y amordazas** dejándolas en el piso de la vivienda, advirtiéndoles que en la mañana de ese mismo día la entregarían y que no dieran aviso a las autoridades o les costaría la vida. Según el relato los hombres salieron con Caicedo abandonando el lugar en un vehículo.

En conversaciones Leider Miranda, vocero nacional de la COCCAM, asegura que en ante la falta de pronunciamientos de las entidades gubernamentales frente a la desaparición de la lideresa "**la comunidad de todas formas esta en una búsqueda pero hasta el momento no sabemos nada de la compañera**".

> Rechazamos el asesinato de Maria Caicedo Muñoz, líder social, en Argelia (Cauca). Solidaridad con su familia y las comunidades a las que representaba. [\#BastaYa](https://twitter.com/hashtag/BastaYa?src=hash&ref_src=twsrc%5Etfw) de la violencia entre colombianos. Esperamos que autoridades investiguen y esclarezcan este hecho violento. [pic.twitter.com/FbOTqLTywp](https://t.co/FbOTqLTywp)
>
> — Defensoría delPueblo (@DefensoriaCol) [27 de octubre de 2018](https://twitter.com/DefensoriaCol/status/1055999982740881408?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Frente al estado de **Liliana y Zoraida Papamija Caicedo**, hijas de la defensora, Miranda asegura que la comunidad esta acompañándolas en su preocupación por la suerte que pudo haber corrido su madre. Adicionalmente, señala que la Fiscalía y la Policía han manifestado que están **en proceso de recoger las pruebas necesarias para iniciar la búsqueda correspondiente**.

Frente a la situación del Cauca, el vocero asegura que **en sus territorios se mueven diferentes grupos al margen de la ley pese a la presencia de la fuerza pública**, y asegura que los perpetradores de estos crímenes "esperan es el momento oportuno para poder actuar" particularmente en la noche y la madrugada, **siguiendo a los líderes e identificando los lugares donde habitan y frecuentan**.

Desde la COCCAM y otras organizaciones que tienen incidencia en la región, **responsabilizan al Estado colombiano por la seguridad de los defensores de DDHH**, exigiendo que asuman su responsabilidad en **garantizar la vida e integridad de la lideresa y sus hijas**.

Con el crimen de la lideresa asciende a 85 los lideres sociales asesinados en Cauca luego de la firma del acuerdo de paz, convirtiendo a este departamento en el más peligroso para la defensa de los derechos humanos.
