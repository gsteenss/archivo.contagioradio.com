Title: Lavado de imagen corporativa + negocio financiero = BanCO2
Date: 2016-08-19 13:39
Category: CENSAT, Opinion
Tags: Ambiente, paz, Territorios
Slug: lavado-de-imagen-corporativa-negocio-financiero-banco2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Territorios-corporativos-para-una-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por [CENSAT Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### 19 Ago 2016

Esta fórmula resume en pocas palabras la esencia de la iniciativa BanCO2 lanzada en el año 2013 por iniciativa de Carlos Mario Zuluaga, director de CORNARE, una estrategia de financiarización de la naturaleza que convierte los elementos como el aire y el agua en nuevos títulos de propiedad que se desvincula de la propiedad de la tierra, de los derechos colectivos sobre el territorio y de su función social. Esta iniciativa, cuya visión para el 2020 es convertir a BanCO2 en el principal mecanismo de pagos por servicios ambientales en Colombia, crea nuevas formas de acumulación del capital, ahora llamado “capital natural”, adquirido por corporaciones para supuestamente compensar su uso excesivo, degradación o contaminación.

*[El lavado de dinero es un delito, el lavado de imagen una política de BanCO2]*

[BanCO2 se está implementando a través de una supuesta cooperación a familias rurales, a partir del apoyo de las corporaciones regionales (actualmente 20) y la inversión de empresas como ISAGEN, Ecopetrol, ARGOS, AngloGoldAshanti Colombia, EPM, Bancolombia y HMV Ingeniería, entre otros. Estas empresas figuran como compensadoras, quienes pagan una cuota por su emisión de carbono, es decir, pagan por la conservación de determinado “pedazo de medio ambiente” en alguna parte de Colombia para poder seguir con sus industrias extractivas en otra.]

Por ejemplo, el proyecto Gramalote de la empresa AngloGoldAshanti Colombia le paga a 15 familias campesinas por la protección de 215 ha de ecosistema estratégico, mientras su proyecto de extracción de oro abarca un área de más de 9.413 hectáreas, afectando potencialmente a 50 mil personas. De esta manera, las industrias extractivas tienen en BanCO2 una plataforma perfecta para presentarse como defensoras del “medio ambiente”.

Otro aspecto por contemplar es la vinculación estructural de Bancolombia a BanCO2. Bancolombia no sólo figura como empresa compensadora, sino además es una entidad patrocinadora y hace parte del comité de buen gobierno de BanCO2. De la misma manera, Bancolombia es la entidad financiera que suministra el mecanismo de pago a través del celular para cada familia campesina y espera con ello vincular a 20 mil nuevos clientes bancarios hasta los rincones más apartados de Colombia para el año 2020.

Ahora bien, parece que los sistemas de pagos por servicios ambientales, más que mecanismos de defensa y protección ambiental, son nuevos escenarios de inversión para la industria extractiva y el sector financiero. Por un lado, es un eficiente mecanismo para el lavado de cara de las empresas, una estrategia de marketing que presenta a las corporaciones como preocupadas por el ambiente, mientras que en otros territorios producen enormes desastres ecológicos y un sin número de conflictos ambientales. En el mismo sentido, Bancolombia es ampliamente beneficiado en el negocio financiero, ya que al mismo tiempo que bancariza a los sectores campesinos al suministrarles los pagos, también aumenta el número de usuarios que utilizan sus servicios.

Bajo esta lógica, un territorio esencial para la vida deja de ser valorado en tanto sus relaciones culturales, sagradas, medicinales o vitales para las comunidades, y se le impone un precio, según BanCO2 una tonelada de carbón representa ocho mil pesos colombianos. Y en últimas son cotizados por sus cualidades de captura de dióxido de carbono y sus servicios ambientales. Con ello, la naturaleza es reducida a una función meramente económica, pues, es convertida en capital natural. En definitiva, lo que ofrece BanCO2 es una herramienta que permite materializar la mercantilización de la naturaleza por medio del pago por servicios ambientales.

*[Territorios corporativos para una paz corporativa]*

Los territorios esenciales para la reproducción de la vida han sido construcciones antrópicas e históricas, cuyo resultado han sido producto de relaciones entre la naturaleza y el ser humano. Por ejemplo, los páramos y altas montañas en el país han sido resultado de relaciones que han implicado la degradación de los territorios, pero igualmente de protección por sus mismos pobladores, que reconocen su importancia para la vida, actividades productivas o valoraciones espirituales y medicinales, sin estar mediadas por el pago para su conservación. Con la implementación de BanCO2 se abre una verdadera disputa por los territorios campesinos, porque aunque no cambian de propietario, es decir, de quien tiene el derecho de propiedad, si cambian de dueño al modificarse la persona que tiene el dominio sobre el territorio. De esta manera, al firmar por parte de los campesinos el cumplimiento del compromiso de uso y cuidado, a la hora de inscribirse al BanCO2, cambian quienes ejercen poder sobre la finalidad de uso en determinado territorio.

La familia campesina pierde entonces su poder de decisión y lo otorga a las corporaciones regionales, quienes son los encargados de controlar el cumplimiento de dichos compromisos, al servicio del mercado.

¿Pero, si los territorios ya no son nuestros, de quien será entonces esta paz territorial que nos espera?

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
