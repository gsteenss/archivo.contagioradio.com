Title: Tres firmantes de paz asesinados en Chocó, Caquetá y Putumayo
Date: 2020-11-17 11:24
Author: AdminContagio
Category: Actualidad, Nacional
Tags: asesinato de excombatientes, Excombatientes de FARC, firmantes de paz
Slug: tres-firmantes-de-paz-asesinados-en-choco-caqueta-y-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Excombatientes-e1567187945860.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante este fin de semana, además de reportarse [una masacre](https://archivo.contagioradio.com/masacre-tierralta-noviembre/) y varios asesinatos a jóvenes y miembros de comunidades afrodescendientes, también se conoció que **tres excombatientes fueron asesinados en los departamentos de Chocó, Caquetá y Putumayo.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1328487595689566209","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1328487595689566209

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En la noche del pasado sábado 14 de noviembre, **Heiner Cuesta Mena**, firmante de paz en proceso de reincorporación, fue asesinado en el corregimiento del Neguá, municipio de Quibdó, Chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El partido Farc aseguró que Heiner Cuesta, también conocido como "Yilson Menas" fue asesinado con varios impactos de arma de fuego.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1328109416726585345?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1328109416726585345%7Ctwgr%5E\u0026amp;ref_url=https%3A%2F%2Fwww.elespectador.com%2Fnoticias%2Fjudicial%2Fasesinan-a-otro-excombatiente-de-las-farc-en-el-choco%2F","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1328109416726585345?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1328109416726585345%7Ctwgr%5E&ref\_url=https%3A%2F%2Fwww.elespectador.com%2Fnoticias%2Fjudicial%2Fasesinan-a-otro-excombatiente-de-las-farc-en-el-choco%2F

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Igualmente, exigió al gobierno **nacional brindar garantías de seguridad a la comunidad en general, incluyendo a los desmovilizados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuesta hizo parte del colectivo del ETCR de Vidrí, Vigía del Fuerte, Antioquia (ETCR Héroes de MurrI) que de manera unilateral el gobierno acabó mediante un decreto.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Asesinatos en Caquetá y Putumayo
--------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este lunes 16 de noviembre, el partido Farc **[denunció](https://twitter.com/PartidoFARC/status/1328386588821385217)** el asesinato del excombatiente **Jorge Riaños en Florencia, departamento de Caquetá y de Enod Lopez Verjano en Puerto Guzmán, Putumayo.** (Le puede interesar: [En primer semestre del 2020 se registraron 95 asesinatos contra defensores de DDHH](https://archivo.contagioradio.com/en-primer-semestre-del-2020-se-registraron-95-asesinatos-contra-defensores-de-ddhh/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a las primeras versiones, Riaños se encontraba en un establo en la Vereda Santander 2 alimentando a unos bovinos cuando cuatro personas armadas le dispararon.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al llegar al lugar de los hechos, las autoridades señalaron que el excombatiente de 39 años presentaba tres impactos con arma de fuego en el rostro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por otro lado, el excombatiente Enod Lopez Verjano fue asesinado junto a su esposa, **Eneriet Penna****, **concejala del partido Conservador.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el hecho, sus dos hijos, Jeymy Cristina Sánchez Penna, de 28 años, y Carlos Andrés Jiménez Penna, de 23 años, resultaron heridos, sin embargo, ya se encuentran fuera de peligro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Federico Montes, representante del partido Farc en el Caquetá, manifestó que «**Riaños y López se encontraba en  proceso de reincorporación y lamentablemente, los asesinatos se siguen presentando en contra de los excombatientes**, a la fecha van seis hechos de sangre, luego del asesinato de Albeiro Suárez, excombatiente que fue asesinado en el departamento del Meta».

<!-- /wp:paragraph -->

<!-- wp:heading -->

Continúa la violencia contra firmantes de paz
---------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

A la fecha ya son 241 los excombatientes de Farc asesinados desde que se firmó el acuerdo de paz. Estos crímenes son un tema preocupante para el país, ya que estas personas confiaron en la protección y compromiso del Estado. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, estas actuaciones no solo afectan a los firmantes de paz sino también a las comunidades que tenían la esperanza de dejar el conflicto atrás y ahora viven con el temor constante de que la guerra vuelva para quedarse en sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Misión de Observación de la ONU ha señalado que 19 firmantes de paz fueron asesinados mientras esperaban respuestas a peticiones de medidas de protección. «Ello pone de manifiesto la necesidad de disponer de suficientes fondos para atender las más de 400 solicitudes pendientes sin más demora».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte la Subdirección de la Unidad de Protección Nacional (UNP) ha aprobado 94 medidas de protección urgente, un 81 por ciento más que todas las que aprobó en 2019, según cuenta la misma ONU. Sin embargo, estas medidas no han sido suficientes. (Le puede interesar: [Ruta de Reconciliación inicia para demostrar que «la reconciliación sí es posible»](https://archivo.contagioradio.com/ruta-de-reconciliacion-inicia-para-demostrar-que-la-reconciliacion-si-es-posible/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El pasado mes de octubre, varios firmantes de paz se congregaron para realizar una marcha denominada "Peregrinación por la Vida" en la que exigían protección por parte del Estado colombiano.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la peregrinación afirmaron «nosotros llevamos 234 excombatientes muertos, sumado a eso los líderes sociales, entonces esto fue como el florero de Llorente ya nos rebosó la copa y dijimos no podemos seguir permitiendo esto… No podemos quedarnos cruzados de brazos y que nos sigan matando. Nosotros estamos luchando por la paz, porque se implementen los acuerdos y cuando sucede todo esto, crea desconfianza, crea preocupación».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Llama la atención que uno de los resultados de la peregrinación fue un encuentro con el presidente Duque en el que el gobierno se comprometió con brindar las garantías de seguridad para quienes se acogieron al acuerdo de paz, sin embargo, una vez de nuevo en sus lugares de reincorporación siguieron en aumento los asesinatos y amenazas en su contra.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
