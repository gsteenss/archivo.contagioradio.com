Title: Colis suspect: una mirada documental al cerrojo de la UE a los migrantes
Date: 2018-03-14 15:51
Author: AdminContagio
Category: 24 Cuadros
Tags: Documental, migraciones, UE
Slug: colis-suspect-documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/mohammed-metro-e1520975228433.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colis suspect 

###### 13 Mar 2018 

Colis Suspect (paquete sospechoso en francés) es un documental que busca responder a ¿Qué hay detrás de la Europa Fortaleza? ¿Por qué se fortifica Europa? ¿A quién afecta y a quién beneficia?, poniendo el foco en las fronteras de la Unión Europea, donde convergen las políticas de inmigración y seguridad. Una producción realizada por la Asociación Juvenil y cultural *Arsomnia* conformada en Barcelona en el año 2012.

El documental tiene su origen en un proyecto de investigación surgido desde las aulas cuando los integrantes del colectivo finalizaban su carrera de Comunicación audiovisual y periodismo en la Universidad Autónoma de Barcelona, que fue tomando forma hasta convertirse en un proyecto artístico y audiovisual.  A su vez, representa el cierre de un ciclo que inició en 2013 con el proyecto periodístico LaColumna.cat, un cibermedio alternativo que, en su trayectoria, dio lugar a voces críticas en temas de política, sociedad y cultura.

María Camila Ardila, periodista colombiana e integrante del equipo Arsomnia, asegura que a finales de 2015 con el contexto de la crisis de los refugiados en su punto más algido, se plantearon crear un documental con la intención de "explicar que había detrás del cierre de las fronteras, quien se estaba beneficiando y evidentemente pues mostrar las caras de los mayores perjudicados que son las personas migrantes y refugiadas"

Colis Suspect cuenta con diez entrevistas a expertos en derecho, migraciones, seguridad, industria armamentística y relaciones internacionales, consultas que ayudan a dar un contexto que permita ir más allá de las imágenes de las tragedias, los salvamentos en el Mediterráneo que ya están bastante vistas, en busca de responder "¿por qué la Unión Europea rechaza a las personas refugiadas a partir de ahí establecer quién se beneficia con eso?" afirma Ardila.

<iframe src="https://player.vimeo.com/video/250985408" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Paralelamente, el análisis de los expertos se articula con la historia de Mohammed Nour, poeta y activista sudanés por los derechos de las personas ‘sin papeles’, Con Mohamed nos encontramos en Julio de 2016 en París, el estaba viviendo en la calle, junto con muchas otras personas y es la persona que le da un rostro las personas migrantes refugiadas que van en busca de una vida fuera del peligro.

Pocos días después de su encuentro, Mohamed subió uno de sus poemas a redes sociales donde imprime sus sensaciones sobre la migración, quien es él, su situación y particularmente hace un llamado de atención sobre la posición que asumen los ciudadanos frente a las acciones emprendidas por la Unión Europea. Su texto llamo tanto la atención del equipo de trabajo que fue incluido en el cierre del documental.

Además de Mohamed, en Colis Suspect aparecen 3 personajes más: Nadym Maher ingeniero civil, la estudiante Bisan ambos sirios refugiados en Alemania y Elouh McQuenn un joven rapero camerunés; y es a partir de esas cuatro historias que se pretende humanizar la problemática llevándola más allá de "un número de personas muertas en el Mediterráneo, o aquellas que intentan re ubicar en los países de la UE"

El documental, que inicio con una estrategia de crowdfounding y luego apoyo del ayuntamiento de Barcelona, fue presentado en Cinema Tonalá de Bogotá el pasado mes de Febrero y espera poder estrenarse próximamente en otros espacios del país. Para conocer más del proceso de producción los invitamos a ingresar a su [sitio web oficial](http://colissuspect.com/es/el-documental/).

<iframe id="audio_24471561" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24471561_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio] 
