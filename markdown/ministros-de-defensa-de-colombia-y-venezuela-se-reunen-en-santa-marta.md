Title: Ministros de Defensa de Colombia y Venezuela se reúnen en Santa Marta
Date: 2015-10-02 12:37
Category: Nacional, Política
Tags: crisis fronteriza, Encuentro de ministros de defensa de Colombia y Venezuela, Juan Manuel Santos, Ministro de defensa de Colombia, Ministro de defensa de Venezuela, Nicolas Maduro
Slug: ministros-de-defensa-de-colombia-y-venezuela-se-reunen-en-santa-marta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ministros-de-defensa1200b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### www.radiomagdalena.com 

###### [01 oct 2015] 

Este viernes se realizará el **encuentro  de Ministros de Defensa de Colombia y Venezuela** en la ciudad de Santa Marta, donde tratarán el tema de crisis fronteriza luego de los dos aplazamientos que surgieron en el transcurso de la semana.

**Luis Carlos Villegas de Colombia y el General Vladimir Padrino López** de Venezuela, se reunirán hoy para crear estrategias para enfrentar el contrabando de combustible, el crimen organizado, el paramilitarismo y el narcotráfico, con el objetivo de buscar soluciones sobre la crisis fronteriza entre ambos países.

Durante el encuentro de las delegaciones ministeriales donde se discutieron temas como la **“normalización progresiva de la frontera”**  se acordó que esta reunión se realizaría el pasado 23 de septiembre en Caracas.  Sin embargo, hoy los ministros  se reunirán en la Quinta de San Pedro Alejandrino.
