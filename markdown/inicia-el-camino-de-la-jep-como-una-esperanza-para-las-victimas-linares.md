Title: Inicia el camino de la JEP como una “esperanza para las víctimas”
Date: 2018-01-15 17:36
Category: Entrevistas, Judicial
Slug: inicia-el-camino-de-la-jep-como-una-esperanza-para-las-victimas-linares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/pazpazpaz-e1512150798177.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: colombiainforma] 

###### [15 Ene 2018] 

Después de un largo trecho y muchos cambios en el congreso colombiano, la Jurisdicción Especial de Paz, JEP, tiene posesionados a 31 de 38 magistrados y magistradas. Muchas organizaciones de víctimas y de Derechos Humanos han coincidido en señalar que, a pesar de los ataques, esta Jurisdicción es una oportunidad para la verdad, la justicia restaurativa y la reconciliación.

<iframe src="https://co.ivoox.com/es/player_ek_23162758_2_1.html?data=k5iemJebeZmhhpywj5aVaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncbeZpJiSo6nHuMrhwtiYxsqPidTowsncjd6Ph9Dh1tPWxsbIqdSfwtPhx5DNssrXytSYxsqPsMKfq6q9j4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En esa misma línea se han manifestado organizaciones de comunidades como CONPAZ u organizaciones de familiares de detenidos y desaparecidos como ASFADES. Para ello recordaron que uno de los componentes principales sobre los que se tiene gran esperanza es el conocimiento de la verdad, y por ello reafirmaron que su participación en la construcción del SIVJRNR estaba en esa línea y esperan que se camine hacia ello. [Lea también: Trabajaremos con mayor rigor: Patricia Linares](https://archivo.contagioradio.com/magistrados_jurisdiccion_especial_paz/)

Una de las primeras tareas que asumirán los 30 magistrados y magistradas luego de su posesión, es la elaboración del reglamento interno de las diversas salas y un diseño de procedimiento que será puesto en conocimiento del presidente de la república para que le dé trámite ante el congreso de la república a la mayor brevedad posible. Los 7 restantes se posesionarán en las siguientes semanas, dos de ellos después del pronunciamiento de la Corte.

### **Posesión de magistrados es legal y legítima** 

<iframe src="https://co.ivoox.com/es/player_ek_23162610_2_1.html?data=k5iemJeadZGhhpywj5WcaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5ynca7d09nOjbXFuNPdxM7OjbHNssLmxtiSlKiPlNPZ1M7Rx9PYpYzYxpDZw5CuibGhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para Mirta Patricia Linares, presidenta de la corporación, la posesión de los magistrados de la JEP es legal y legítima en todos los sentidos. En cuanto que las inhabilidades que se plantearon en el congreso, Linares resalta que lo ha cuestionado a los magistrados es una ley estatutaria que tendrá que tendrá que ser sometida al examen de la Corte Constitucional para convertirse en Ley. [Lea también: Así fue el fallo de la corte constitucional con respecto a la JEP](https://archivo.contagioradio.com/corte_constitucional_fallo_jep/)

En esa medida, Linares expresa que la Corte Constitucional ha manejado una línea de jurisprudencia que seguramente va a declarar inexequibles las inhabilidades que el congreso pretende imponer sobre los magistrados que fueron seleccionados por el Comité de Escogencia en Noviembre de 2017.

Otra de las preocupaciones que se han manifestado frente a la puesta en marcha de la JEP es que pasaría por encima del sistema judicial colombiano, sin embargo, según la magistrada ya se han adelantado reuniones con los entes de control para que se abran los debates jurídicos que marquen los derroteros en torno a los conflictos que se puedan presentar, sin embargo la jurista señala que está muy optimista del buen trámite de los mencionados debates puesto que se realizarán entre organizaciones colegiadas y con altos estándares.

### **JEP tiene su presupuesto asegurado** 

En materia de recursos la JEP cuenta con una asignación aproximada de 200 mil millones para el 2018, sumado a ello los recursos para la sede de la JEP que destinó el gobierno colombiano desde el llamado “Fondopaz” y que garantizaría el despegue de esta institución.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
