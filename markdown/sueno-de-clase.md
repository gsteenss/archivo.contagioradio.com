Title: Sueño de clase
Date: 2016-05-07 12:11
Category: Camilo Alvarez, Opinion
Tags: 1 de mayo, Movimientos sociales
Slug: sueno-de-clase
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/1-DE-MAYO-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

#### **[Camilo Álvarez Benítez](https://archivo.contagioradio.com/camilo-alvarez-benitez/)- [@camiloalvarezb](https://twitter.com/CamiloAlvarezB)** 

###### 6 May 2016 

[Había tumultos de técnicos y profesionales que se agolpaban en las puertas de las ONGs más reconocidas luego de elecciones del 2015, hacían largas filas como en Subway a fin de mes; todos y todas con sus mejores caras de gestores, con sus hojas de vida, las chaquetas de tres periodos de gobierno como soporte bajo el brazo y una ampliación de la cédula al 150% debidamente plastificada. Se miraban poco a poco los rostros y sabiéndose multitud realizaban una asamblea al estilo 15M sobre la carrera 5ª en frente del CINEP,  gritando ¡YA BASTA! Y animándose en acto litúrgico como Iglesias: ¡PODEMOS!]

[Convocados por la indignación y por primera vez no indignados por una convocatoria; salían en grupos a afiliarse a la CUT y a la CGT;  Al principio, los porteros de los sindicatos sorprendidos, dudaban si llamar al ESMAD o a Secretaria de Gobierno; sólo al reconocer ex gestores de convivencia en el tumulto los dejaban pasar, no recuerdo haber visto una oficina de afiliaciones pero en recepción revoloteaban – ¡No hay papelería para carnet! es que afiliaciones en masa ni en Yerbalife- explicaba el portero de la CGT.]

[Otros de los indignados habían tomado otro camino y apelando a su lenguaje particular y el territorio común Chapineruno y Teusaquillero se pretendían convertir en resguardo, debatían si ir a la ONIC o al ministerio de interior donde alguno de los indignados dijo que existía una dirección de etnias; otros en un Juan Valdez decían que en todo caso las etnias no tenían dirección y que un resguardo se hacía de facto.]

[Luego de la masiva afiliación se daban cita todos de nuevo en el Park Way para definir la estrategia; mas por gustos que por razones se alinearon en dos grupos unos del lado de la Luna Lela y otros del lado de Trementina, básicamente unos decían que lo importante era el Periodo, los otros que lo importante era el periódico.]

[Quien zanjó la discusión, afirmó que es en la práctica donde se resuelve todo y que la cita para esta renovación de clase y de movimiento indignado debía ser en la plaza de Bolivar; ese sería el primer lanzamiento de estos sectores de la clase trabajadora; un acto performático, con fuerte poder simbólico; -Podemos hacerlo con un artista famoso, yo voy a Gaira de vez en cuando y tal vez Carlos Vives que tampoco trabaja se pegue- dijo una indignada.]

[Así, fueron preparando el gran lanzamiento, los pregoneros, los tambores, las cacerolas,  las pancartas y las banderas, cada cual con lo que pudo y quiso. Mucho color y sonido; todo era un mar de consciencia; alterada pero al fin y al cabo consciencia de clase. Socialist fest y comunist party cerraban el día en el que la tasa del sindicalismo subió por primera vez en 40 años más que el dólar y llegaba estrepitosamente a un 10% - y todo lo que puede hacer un colombiano con un 10%- decían los analistas.]

[Entonces Algo me incomodó bajo el brazo, era una Tablet de la que emergió un Genio de la política 3.0 diciendo: Te concederé tres deseos si tomas un arma de juguete y le apuntas al ESMAD en la Plaza de Bolivar  y superas tres pruebas:1) Salir en medios, 2) Salir libre en menos de 24 horas y por supuesto 3) quedar vivo.]

[Yo le decía al genio que no fuera iluso, que esa prueba no la superaba nadie y menos sin ambulancias disponibles, que solo un infiltrado con la ayuda de  RCN y Citytv podría lograrlo y que eso de llegar al 10% de sindicalización y con mis congeneracionales ¡eso si era irreal, casi un sueño¡]

[Y entonces cuando desperté, el dinosaurio aún estaba ahí… intentando vender la ETB.]

------------------------------------------------------------------------

###### [ ]Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
