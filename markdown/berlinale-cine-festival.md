Title: Dos cintas colombianas llegan hoy a la Berlinale
Date: 2018-02-15 17:41
Author: AdminContagio
Category: 24 Cuadros
Slug: berlinale-cine-festival
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/afiche-01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Virus tropical 

###### 15 Feb 2018 

Llega el **Festival Internacional de Cine de Berlín o Berlinale** que expondrá las mejores películas del cine internacional. Este evento que se realiza desde hoy hasta el 25 de febrero tendrá una importante cuota colombiana y latinoamericana.

La sección Genration ha escogido tres largometrajes Latinos: "El día que resistía", de la argentina Alessia Chiesa, "Unicórnio" del brasileño Eduardo Nunes y “**Virus Tropical**” del colombiano **Santiago Caicedo**, historia basada en la novela autobiográfica de Powerpaola, que cuenta todos los devenires de una chica de 13 años que vive su niñez y adolescencia entre Quito y Cali. Película que se estrenara en las salas de cine del país en abril.

<iframe src="https://www.youtube.com/embed/Fu2A-MRlCP8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Por otro lado el director colombiano Edison Sánchez estrenara “[Yover](https://archivo.contagioradio.com/yover-cortometraje-bojaya/)” que nos sitúa en Bojayá, donde un niño de 12 años que debe trabajar recorrerá estas calles que fueron uno de los escenarios más violentos en el país. Esta película que busca retratar una cara distinta de Bojayá competirá por el Oso de Cristal en la categoría Generation Kplus.

<iframe src="https://www.youtube.com/embed/OEpMwbZ_hfE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Entre otros filmes que representaran a Latinoamérica estarán “**La Omisión de Malambo**”, “El hombre bueno”, ambas de Argentina y los documentales brasileños “Ex Pajé” y “Bixa Travesty”.

Para este año el festival iniciará con la proyección del film animado "**Isle of Dogs**" del director norteamericano **Wes Anderson**, un largometraje que cuenta la historia de un niño de doce años que parte en la búsqueda de su perro, el cual fue deportado por un alcalde corrupto.

Entre algunos de los actores y actrices que asistirán al evento se encuentran Robert Pattinson, Joaquín Phoenix, Isabelle Huppert y **Willem Dafoe quien recibirá el Oso de Oro de Honor de Berliande por su larga y amplia trayectoria artística**.

Si desea saber que otras películas, actores y demás eventos que se realizaran en el festival puede consultar el sitio oficial del [Festival Internacional de Cine de Berlín.](https://www.berlinale.de/en/HomePage.html)
