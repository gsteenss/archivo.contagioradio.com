Title: También "Hay Festival" para las comunidades y barrios
Date: 2019-01-25 11:44
Author: AdminContagio
Category: eventos
Tags: Cartagena, comunidades, hay festival
Slug: hay-festival-para-las-comunidades
Status: published

###### Foto: Hay Festival 

###### 25 Ene 2019 

Dentro de la programación del **Hay Festival Cartagena de Indias 2019**, viene creciendo con fuerza la sección comunitaria del evento que busca **acercar la cultura artística, de manera gratuita y con libre acceso, a los barrios populares de la ciudad amurallada** y municipios del departamento de Bolívar.

Al poner en contacto a escritores, artistas y periodistas con las comunidades, los organizadores del Festival pretenden **liberar la imaginación y el arte como una forma de transformar al mundo**, objetivo canalizado en actividades lúdicas y de formación que tendrán lugar entre el **31 de enero y el 3 de febrero próximos**.

Uno de los eventos más destacados sera la intervención de la escritora nigeriana **Chimamanda Ngozi Adichie**, quien visitará al barrio Nelson Mandela, para dialogar sobre las luchas sociales de comunidades afrodescendientes en el territorio, género y diáspora junto con la periodista colombiana **Mabel Lara** y la doctora **Aurora Vergara**. Durante el conversatorio se presentará también el programa "Mujeres Afrocolombianas escriben su territorio" con presencia de 20 escritoras afrodescendientes de toda Colombia.

Otro de los grupos focales del Festival son los niños, para quienes se han programado varias actividades como el conversatorio sobre el libro  **"La historia, los viajes y la abuela"** de la periodista y escritora de literatura infantil **Pilar Lozano**, con ilustraciones de **Paula Bossio**, sobre la historia del país en un libro lleno de viajes, colorido y aventuras.

Por su parte, **Irene Vasco** impartirá un taller sobre su libro **"Conjuros y Sortilegios"**, la historia será recreada por los niños, quienes realizarán actos de magia para ampliar los universos literarios. Las experimentadas periodistas **Marta Orrantia y Catalina Gómez**, hablarán sobre herramientas para la investigación y el conocimiento de la verdad en la apasionada labor periodística a reporteritos de barrios populares en Cartagena.

También orientado a los pequeños, **Sara Sánchez**, ofrecerá un taller sobre el proceso que sigue para crear sus personajes y la importancia que tienen las ilustraciones en sus libros. Y con **Alekos **los niños escogerán un personaje que tendrán que imaginar e ilustrar.

Las actividades del Hay Fesival, adelantadas en coordinación con la Fundación Plan, las entidades territoriales y el Ministerio de Cultura, tendrán lugar en algunos espacios de **Cartagena de Indias, y en San Juan Nepomuceno, Bayunca, Membrillal, El Carmen de Bolívar, Magangué, Mompox, Santa Rosa de Lima y Turbaco**.

La programación completa de las actividades del Hay Festival Comunitario se puede consultar [Aquí](http://www.hayfestival.com/cartagena/comunitario)

###### Reciba toda la información de Contagio Radio en [[su correo]
