Title: Corte Constitucional congela acuerdo de paz con las FARC: Enrique Santiago
Date: 2017-05-18 12:39
Category: Nacional, Paz
Tags: acuerdos de paz, Corte Constitucional
Slug: corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/161124_05_FirmaNuevoAcuerdoPaz-e1495130925975.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: 360radio.com] 

###### [18 May 2017] 

El abogado Enrique Santiago, asesor del proceso de paz entre el gobierno y las FARC-EP, expresó que el accionar de la Corte Constitucional deja congelado los acuerdos de paz de La Habana y sin herramientas para poder **llevar adelante la implementación de lo urgente y necesario para concluir el proceso de dejación de armas**.

### **Debatir punto a punto el proceso de paz …** 

Una de las modificaciones que hace la Corte Constitucional es quitar la votación en bloque de cada uno de los proyectos de ley que se formulen, por consiguiente, deberán votarse artículo por artículo. Enrique Santiago señaló que no está de acuerdo con que deba aprobarse punto por punto el acuerdo y recordó el artículo 189 de la Constitución Colombiana que establece “**claramente que las competencias para firmar, implementar acuerdos de paz son competencias exclusivas del Presidente de la República**”.

Y agregó que en absoluto el acto legislativo del Fast Track, limita o priva alguna competencia del Congreso en la tramitación legislativa ordinaria, sino que establece un marco para respetar las competencias presidenciales respecto a los acuerdos de Paz. Le puede interesar: ["160 organizaciones y personas respaldan la constitucionalidad de la Comisión de la verdad"](https://archivo.contagioradio.com/comisionverdadpaz/)

### **La Corte Constitucional y los poderes del presidente** 

Esta resolución de la Corte Constitucional, es producto de una demanda impuesta por el senador Iván Duque, del partido Centro Democrático, que, para Enrique Santiago, tenía las motivaciones de que el Congreso invadiera las competencias presidenciales y expresó que “**lo que sorprendente es que la Corte Constitucional quisiera darle sustento a esa invasión**”.

Sin embargo, esta decisión de la Corte llega en un momento álgido de la implementación del acuerdo a solo estar a 13 días de que se cumpla el día D + 180 para culminar el proceso de dejación de armas. Con esta resolución Enrique Santiago manifestó que la **Corte estaría colocando más impedimentos a la implementación de lo urgente**. Le puede interesar:["Asesinan en Putumayo a Familiares de guerrillero de las FARC"](https://archivo.contagioradio.com/asesinan-familia-de-guerrillero/)

### **Las FARC –EP y el cumplimiento de los Acuerdos de Paz** 

A través de su cuenta de Twitter Rodrigo Londoño, máximo jefe de las FARC-EP aseguró que, tras la decisión de la Corte Constitucional, “**todas las zonas veredales y puntos transitorios de normalización entran en asamblea permanente**”, mientras que por su parte Iván Márquez señaló: “confiamos en que el presidente haga valer las facultades que le otorga la Constitución para sacar adelante el proceso de paz”.

Enrique Santiago, afirmó que ante esta situación las FARC-EP pedirá garantías para el cumplimiento del acuerdo de paz, “no creo que nadie sensato crea que en un acuerdo de paz deba tener la obligación de cumplir solo una parte, sino que deben ser ambas partes, como en cualquier acuerdo”. Le puede interesar: ["FARC espera que Santos haga valer sus facultades para sacar adelante acuerdos de paz"](https://archivo.contagioradio.com/corte_constitucional_fast_track/)

Otra de las determinaciones que tomo la Corte Constitucional tiene que ver con tumbar el decreto [ decreto 298 del 2017 con el que el Gobierno permitía que se ampliara la planta de personal de la Unidad Nacional de Protección, con la inclusión de integrantes de las FARC-EP, para cumplir con varios puntos del proceso de paz. Sin embargo, tras esta decisión existiría riesgos frente a la protección de los integrantes de esta guerrilla. ]

### **¿La Constituyente una salida al desconocimiento jurídico del Acuerdo de Paz?** 

Una de las posibles soluciones que plantea Enrique Santiago a este desconocimiento por parte de la Corte Constitucional a los acuerdos de paz de La Habana sería realizar un proceso Constituyente.

“Si la institucional del estado no es capaz de respetar las competencias del presidente, en una cuestión de tal trascendencia histórica como es finalizar una guerra de más de 50 años, **no cabe duda de que hay un problema con la institucionalidad y que tendría que ser un proceso constituyente el que cambie las reglas del juego político**” afirmó Enrique Santiago.

###### Reciba toda la información de Contagio Radio en [[su correo]
