Title: Política
Date: 2014-11-25 14:09
Author: AdminContagio
Slug: politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/politica-del-olvido.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/politica.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2019-02-07-a-las-2.47.26-p.m.-770x400.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/POLIITCAMENTE-INCORRECTO.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/180925_02_ReunionBilateral_1800.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/eeuu.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

POLÍTICA
--------

[![Santrich podría salir de la cárcel a mediados de Febrero: Defensa](https://archivo.contagioradio.com/wp-content/uploads/2019/01/Jesus-Santrich-en-entrevista-contagio-radio-770x400.jpg "Santrich podría salir de la cárcel a mediados de Febrero: Defensa"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/Jesus-Santrich-en-entrevista-contagio-radio-770x400.jpg 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Jesus-Santrich-en-entrevista-contagio-radio-770x400-300x156.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Jesus-Santrich-en-entrevista-contagio-radio-770x400-768x399.jpg 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Jesus-Santrich-en-entrevista-contagio-radio-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/jesus-santrich-podria-salir-de-la-carcel-a-mediados-de-febrero-defensa/)  

###### [Santrich podría salir de la cárcel a mediados de Febrero: Defensa](https://archivo.contagioradio.com/jesus-santrich-podria-salir-de-la-carcel-a-mediados-de-febrero-defensa/)

[<time datetime="2019-01-29T14:14:48+00:00" title="2019-01-29T14:14:48+00:00">enero 29, 2019</time>](https://archivo.contagioradio.com/2019/01/29/)La defensa de Jesús Santrich aseguró que a mediados del mes de febrero podría salir en libertad luego de que se suspenda la solicitud de extradición[Leer más](https://archivo.contagioradio.com/jesus-santrich-podria-salir-de-la-carcel-a-mediados-de-febrero-defensa/)  
[![\#5000TroopsToColombia: ¿Agresión mediática contra Venezuela?](https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400.png "#5000TroopsToColombia: ¿Agresión mediática contra Venezuela?"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400.png 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400-300x156.png 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400-768x399.png 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Diseño-sin-título-5-770x400-370x192.png 370w"}](https://archivo.contagioradio.com/5000troopstocolombia-venezuela/)  

###### [\#5000TroopsToColombia: ¿Agresión mediática contra Venezuela?](https://archivo.contagioradio.com/5000troopstocolombia-venezuela/)

[<time datetime="2019-01-29T13:14:19+00:00" title="2019-01-29T13:14:19+00:00">enero 29, 2019</time>](https://archivo.contagioradio.com/2019/01/29/)¿Es la nota de Boltón sobre 5000TroopsToColombia una amenaza a Venezuela? ¿Qué papel jugaría Colombia en medio de esta afirmación?[Leer más](https://archivo.contagioradio.com/5000troopstocolombia-venezuela/)  
[](https://archivo.contagioradio.com/sarmiento-angulo-corficolombiana/)  

###### [¿Sarmiento Angulo desconocía los millonarios sobornos pagados por Corficolombiana?](https://archivo.contagioradio.com/sarmiento-angulo-corficolombiana/)

[<time datetime="2019-01-24T15:34:32+00:00" title="2019-01-24T15:34:32+00:00">enero 24, 2019</time>](https://archivo.contagioradio.com/2019/01/24/)Uno de los empleados de los Sarmiento movió 6,5 millones de dólares para pagar sobornos, ¿y ellos no se daban cuenta de nada?[Leer más](https://archivo.contagioradio.com/sarmiento-angulo-corficolombiana/)  
[](https://archivo.contagioradio.com/si-hubo-censura-a-santiago-rivas-puros-criollos/)  

###### [Sí hubo censura a Santiago Rivas y los Puros Criollos](https://archivo.contagioradio.com/si-hubo-censura-a-santiago-rivas-puros-criollos/)

[<time datetime="2019-01-23T15:23:52+00:00" title="2019-01-23T15:23:52+00:00">enero 23, 2019</time>](https://archivo.contagioradio.com/2019/01/23/)Este 23 de enero se dio a conocer por parte de la FLIP un audio en el que cuatro personas acuerdan retirar el programa de la parrilla e impedir que Santiago Rivas trabaje de nuevo[Leer más](https://archivo.contagioradio.com/si-hubo-censura-a-santiago-rivas-puros-criollos/)
