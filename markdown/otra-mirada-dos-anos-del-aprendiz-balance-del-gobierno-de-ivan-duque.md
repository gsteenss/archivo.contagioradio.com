Title: Otra Mirada: Dos años del aprendiz, balance del gobierno de Iván Duque.
Date: 2020-08-10 20:59
Author: PracticasCR
Category: Nacional
Slug: otra-mirada-dos-anos-del-aprendiz-balance-del-gobierno-de-ivan-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/duque-juramento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Revista El Congreso

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 7 de agosto del 2018 Iván Duque se posesionó como presidente de la República de Colombia. Dos años en los que habría tenido que aplicar cada uno de los puntos del acuerdo de paz y hacer frente a una pandemia que trae consigo una crisis económica y de desigualdad. Ahora que se cumplen los dos años se hace un balance de la mano de comunidades, analistas y defensores de derechos humanos para evaluar cómo ha sido su tiempo en el gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta evaluación participaron Zulma Ulcue, consejera del pueblo Nasa en Putumayo, Lourdes Castro, coordinadora del programa Somos Defensores, Laura Vega, delegada de reincorporación de FARC para el departamento del Tolima y Mauricio Toro, representante de la Cámara por el partido Verde.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cada uno, compartió cómo evaluarían este gobierno en relación a falencias, formas de operar desde las diferentes comunidades y desde instituciones como el Congreso, así como qué cambios se han notado en estos dos años con respecto al acuerdo de paz. ([Le puede interesar: Defensoría del Pueblo será repartida )como cuota política del gobierno Duque](https://archivo.contagioradio.com/defensoria-del-pueblo-cuota-politica/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, exponen cuáles son las exigencias de las comunidades indígenas en relación a lo prometido y no cumplido, qué se está esperando de los dos años que restan y cuáles son las propuestas que se pueden hacer para cumplir con las distintas necesidades de los pueblos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez, el representante del partido Verde comparte qué acciones se deben generar desde el Congreso para asegurar que todo se lleve con transparencia y cumpliéndole a la comunidad.. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, llegan a la conclusión de que el balance en general no es muy bueno tanto en lo que dijo en campaña como lo que ahora está haciendo y hacen un llamado a la movilización social, teniendo en cuenta la unidad y la resistencia que debe caracterizar a los colombianos. (Si desea escuchar el programa del 7 de agosto: [Otra Mirada: Nariño: Resistir ante el abandono Estatal](https://www.facebook.com/contagioradio/videos/899527523868967))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo [Haga click aquí](https://www.facebook.com/contagioradio/videos/2946485895479093)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
