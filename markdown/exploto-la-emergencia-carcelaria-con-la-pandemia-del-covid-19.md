Title: ¿Explotó la emergencia carcelaria con la pandemia del Covid-19?
Date: 2020-04-14 14:42
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: Covid-19, Expreso Libertad, gobierno colombiano, Gobierno Duque, INPEC
Slug: exploto-la-emergencia-carcelaria-con-la-pandemia-del-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/images_cms-image-000001671.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#687175"} -->

###### Foto por: Defensoría del pueblo 

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad**, la abogada Lorena Medina y el abogado Uldarico Florez denunciaron la crítica situación que se vive en las cárceles, con la confirmación de dos muertos por contagio de Covid-19 en la cárcel de Villavicencio, y el resultado positivo de un guardia del **INPEC** en la cárcel Distrital.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con los defensores, la difícil situación que está afrontando la población carcelaria es producto de la sistemática e histórica violación a derechos humanos de la que son víctimas. Asimismo, señalaron que la dificultad para manejar esta crisis dentro de los centros de reclusión está en la ausencia de personal médico capacitado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Motivos por los cuales le exigieron al gobierno del presidente Iván Duque la promulgación de un decreto de excarcelación o la puesta en marcha de un plan que minimice los contagios dentro de las cárceles.

<!-- /wp:paragraph -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F1123351234679824%2F&amp;width=600&amp;show_text=true&amp;height=448&amp;appId" width="600" height="448" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<!-- wp:paragraph -->

[Mas programas de Expreso Libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
