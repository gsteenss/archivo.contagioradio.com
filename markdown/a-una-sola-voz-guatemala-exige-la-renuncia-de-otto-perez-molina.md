Title: A una sola voz Guatemala exige la renuncia de Otto Pérez Molina
Date: 2015-08-28 14:59
Category: El mundo, Entrevistas, Movilización
Tags: Guatemala, Movilización, Otto Pérez Molina, Paro Nacional, Renuncia
Slug: a-una-sola-voz-guatemala-exige-la-renuncia-de-otto-perez-molina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/PROTESTA-GUATE1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:soy502.c0m] 

###### [28 Ago 2015]

La **huelga general que dio inicio el día de ayer con multitudinarias manifestaciones en la Plaza de la Constitución** mantiene paralizada a Guatemala y ha movilizado a miles de personas y diferentes sectores tanto sociales, como políticos y   económicos que exigen la [renuncia del presidente Otto Pérez Molina](https://archivo.contagioradio.com/se-anuncia-jornada-de-paro-nacional-para-este-jueves-en-guatemala/).

En las manifestaciones que fueron convocadas por la organización Asamblea Social y Popular (ASP), no solo ha participado la mayoría del **pueblo Guatemalteco, cansado de la corrupción y de la intransigencia de Pérez Molina** **para renunciar a la presidencia**, sino que también se han unido distintos sectores empresariales los cuales se han solidarizado de distintas formas.

“*El cierre de las empresas no se debe tomar como una acción política sino como una forma de auténtica protesta ante la coyuntura del país*”, indicó José Mario López Palma, gerente corporativo de Domino's Pizza.

Las universidades quienes también convocaron a estas movilizaciones, lograron que los colegios y maestros hicieran parte de ellas y suspendieran actividades académicas. Uno de estos colegios fue el Liceo Loyola que declara en un comunicado "*No podemos permanecer  indiferentes frente a la realidad nacional que nos afecta y que evidencia un sistema de gobierno en el que predomina la corrupción y la injusticia*".

En total fueron **16 estados de los 22 que integran Guatemala los que se sumaron a la jornada de Paro Cívico Nacional** que también ha incluido marchas y medidas de boicot económico. A través de las redes sociales los ciudadanos han pedido no recargar los celulares o dejar de comprar ciertos productos como mecanismo de protesta contra empresas privadas.

Eunice Mendizabal, "Ministra de gobernación, dio ordenes al PNC  “***para que vele por el orden público y el libre ejercicio de los derechos ciudadanos para este jueves***”, el mismo ente dio por respuesta que desplegará para atender estas jornadas de movilización varios contingentes para resguardar el orden y evitar disturbios.

**Lo que sucederá el día de hoy**

El día de hoy Mario Linares “Presidente de la Comisión Pesquisidora” a cargo del análisis del antejuicio de Molina, anunció que convocará a los integrantes que conforman esta comisión para analizar el expediente de Pérez Molina esta  tarde.

Sergio Celis y Jorge Mario Barrios diputados que integran leste grupo, exigieron a Linares en una rueda de prensa  convocarlos para analizar los documentos que sustentan la solicitud de antejuicio contra Pérez Molina

Sin embargo, frente a este Proceso la diputada Nineth Montenegro, también miembro  de la comisión manifestó preocupación por quienes conformarían este grupo debido a que muchos no responden a los llamados para establecer una agenda.

Montenegro envió un comunicado al presidente del congreso Luis Rabbe en donde denunciaba falta de compromiso y conducta antijurídica de Linares al debido proceso.

“*No se puede aplazar el análisis es necesario empezar el análisis (…) no es justo burlarse de la dignidad del pueblo, es necesario trabajar por los interés de los guatemaltecos y no de unos cuantos*”, dijo Montenegro.
