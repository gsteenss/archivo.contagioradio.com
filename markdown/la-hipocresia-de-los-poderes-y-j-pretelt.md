Title: La hipocresía de los poderes y J Pretelt
Date: 2015-03-24 12:59
Author: CtgAdm
Category: Camilo, Opinion
Tags: Fiscal General, Pretelt
Slug: la-hipocresia-de-los-poderes-y-j-pretelt
Status: published

###### foto: Colprensa 

#### Por[[ **Camilo De Las Casas**]](https://archivo.contagioradio.com/camilo-de-las-casas/) 

Ni es el único magistrado de la Corte Constitucional al que se le puede cuestionar ética y penalmente, ni es la única expresión de la crisis de la rama jurisdiccional que padece Colombia de tiempo atrás. Lo que se está haciendo es tapar el sol con las manos.

Qué Pretelt sea investigado y sancionado por la Comisión de absoluciones de la cámara de representantes por tráfico de influencias; qué su familia sea investigada por ser beneficiarios de operaciones paramilitares en tres propiedades despojadas a víctimas en el Urabá, y por lavado de activos, está muy bien, y ojalá sean sancionados, si hay vencimiento en juicio.

Sin embargo, J Pretelt no puede ser el chivo expiatorio de una crisis institucional que es más de fondo. El linchamiento colectivo no puede convertirse en cortina de humo. El fondo del asunto es que lo que en Colombia se llama justicia, es excepcionalmente justicia, casi siempre ha sido, con honrosas excepciones, en un aparato que ha amparado a los poderosos, políticos, militares, empresarios, eclesiásticos.

O acaso, alguien puede poner las manos en el fuego por el actual Fiscal General de la Nación, Eduardo Montealegre,  y  su imparcialidad, cuando pasa de asesorar a grandes empresas de Salud y a los propios militares, por no decir más, de la manera como se realizan ciertas contrataciones de “expertas”; o alguien puede olvidar al ex Fiscal Guillermo Mendoza Diago y su defensa férrea y omisiones en casos de militares y paramilitares para proteger a grandes beneficiarios empresariales; o de Mario Iguarán, y de la manera como fue elegido con consentimiento y anuencia de un sector de paramilitares con apoyo de un Ministro de la época de Uribe, para asegurar la aplicación de la ley 975, burlándose de las víctimas; o quien se puede olvidar de Luis Camilo Osorio y los tentáculo con el paramilitarismo, elegido durante el gobierno de Andrés Pastrana Arango….. y eso para solo hablar de los Fiscales Generales.

Y si en la cabeza hay esos especímenes, que no decir, de la multiplicidad de fiscales o temerosos o corruptos permeados por el viejo y nuevo paramilitarismo y por los organismos de in-seguridad del Estado y empresarios, por los que se ha condenado a millares de colombianos inocentes por delitos de rebelión y terrorismo o de acusaciones injustas como parte de un plan de persecución. Ah ¿y qué no decir de un amplio sector de los jueces, y de la Corte Suprema de Justicia,  del Consejo de Estado y del Consejo Superior de la Judicatura?. Ah, y que no decir, de por lo menos unos ex magistrados de la Corte Constitucional que prestan servicios a empresas privadas y alguno de ellos, asesorando la firma de convenios entre este poder con los militares para lograr la explotación petrolera en zonas conflictivas, en donde habitan, indígenas, afros y mestizos. ¡Basta ya de tanta hipocresía!, basta de tanta adulación  y respeto a unas cortes y poderes públicos que no se lo merecen, porque estructuralmente, el sistema de poder hiede a podredumbre.

Aquí el problema es de fondo, y sería peor, si algunos de los magistrados de las altas cortes, y algunos fiscales y jueces, no hubieran obrando en conciencia y ética, esos son lo que excepcionalmente han honrado la justicia, pero debemos decirlo, no son la mayoría. Para qué el aparato de justicia, pueda acercarse a ser justo, se requieren reformas de fondo, que podrían hacerse solamente con voluntad política del establecimiento y quizás en un escenario de Asamblea Constituyente. Lo mismo sucede con el poder ejecutivo y legislativo, ¡hieden a podredumbre!

El nuevo pacto social por la justicia debería comprender una reforma de la manera como se eligen a los togados, que no puede seguir quedando en manos del ejecutivo y del poder legislativo. Si quiere ser democrático su forma de elección debe serlo con participación y aseguramiento de la presencia en los mismos de personas probas, de personas expertas y profesionales, no necesariamente abogados, ni doctores en derecho; con asientos de las diversas expresiones políticas, no solamente las neofrentenacionalistas, del Uribismo pura sangre, del uribismo santistas, y adicionalmente que reflejen la pluralidad étnicas y del movimiento social. Y valga agregar con una reducción de sus onerosos pagos por ser magistrados, reducción de los salarios que debe incluir al presidente, sus ministros, directores y todos los legisladores. Eso es una vergüenza y una injusticia. Una cosa es un pago justo, otra cosa es un abuso a nombre de la “justicia” o de la elección popular.

El Pacto por la Justicia que debe incluir a otras ramas del poder público, requiere mecanismos de veeduría ciudadana con poder sancionatorio social y de exclusión de quienes obren contra el interese general, el bien común y la supervivencia de las fuentes de vida. Obrar a favor de sectores empresariales, que desconocen derechos humanos y derechos de seres sintientes, del agua y del ambiente es una conducta que debe ser sancionada.

El Pacto por la Justicia debe limitar el cabildeo empresarial o proscribirlo, y debe sancionarlo. Si un tribunal obra en derecho sobre los principios de derechos humanos y ambientales, y por el interés superior de los derechos de los nacionales. Las empresas deberán asumir pagos onerosos para un fondo de víctimas y un fondo ambiental cada vez que se les descubra en sus mecanismos de presión, de incidencia y corrupción. Los intereses de FIDUPETROL por ejemplo, ni de un sector empresarial pueden definir la desgracia de millones de colombianos.

Seguir en la ilusión de que el aparato de justicia es justo e imparcial es lo más nocivo en un proceso ciudadano transformante. J Pretelt es un sofisma, y quiénes lo eligieron se ríen. Aquí son más los togados que juegan a la justicia y perpetúan la injusticia, es un asunto estructural. Y en ese río revuelto, mientras sigue el escándalo, el Ministro del Interior anuncia la limitación a la tutela porque a propósito del escándalo J Pretelt y la empresa FIDUPETROL, quiere quitarle derechos a los desfavorecidos pues la tutela les ha costado dinero público

La justicia no puede seguir siendo una mercancía y la tutela no puede usarse para favorecer a los empresarios, para que se sigan lucrando. J Pretelt no puede usarse para limitar los alcances de un mecanismo que ha sido de los pocos, en muchos casos, no en todos, que ha protegido los derechos de los desfavorecidos y tampoco la sanción de Pretel convertirse en el alivio para la crisis que padece el aparato de in justicia en Colombia.

<div class="yj6qo ajU" style="text-align: justify;">

21 de marzo de 2015

</div>
