Title: Futuro incierto en Bolivia tras la renuncia de Evo Morales
Date: 2019-11-10 18:08
Author: CtgAdm
Category: El mundo
Tags: Bolivia, Evo Morales, Golpe de Estado
Slug: futuro-incierto-en-bolivia-tras-la-renuncia-de-evo-morales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Evo-Morales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MRE\_Bolivia] 

Evo Morales, presentó su renuncia a la Presidencia de Bolivia este 10 de noviembre después de conocer la posición de las Fuerzas Armadas del país, quienes lo instaron a dejar el cargo luego de casi tres semanas de protestas en contra de los resultados electorales que posicionaban a Morales como mandatario hasta 2025, tras su renuncia, sectores afines al exmandatario señalan que se trata de un golpe de Estado.

Los resultados preliminares de las elecciones del pasado 20 de octubre publicados horas después de que cerraron las urnas evidenciaban un estrecho margen entre Evo Morales y su contendor, el expresidente Carlos Mesa, lo que habría provocado una segunda vuelta, sin embargo el conteo se detuvo durante 24 horas.

Al respecto, **la presidenta del Tribunal Supremo Electoral de Bolivia, María Eugenia Choque** explicó que se decidió suspender la transmisión de resultados preliminares para evitar confusiones, ya que se daría inicio al computo definitivo, al reanudarse el conteo, la ventaja de Morales garantizaba su cuarto mandato.

La oposición liderada por Mesa, consideró fraudulentos estos comicios y llevó este descontento a las calles, desde entonces, los enfrentamientos entre partidarios y detractores del Gobierno **ha dejado 3 muertos y 421 heridos, según la Defensoría del Pueblo de ese país.**  Ante la inconformidad del resultado electoral, un equipo de la Organización de los Estados Americanos (OEA), inició desde el 31 de octubre una auditoría de los resultados de las elecciones.

Tras 17 días de protestas, el pasado viernes, la Unidad Táctica de Operaciones Policiales (UTOP) de la ciudad de Cochabamba seguida de los comandos de Sucre y de Santa Cruz, se amotinaron contra el presidente Morales, finalmente el sábado en La Paz,  también se forzó el cese de la autoridad policial y mientras algunos uniformados se sumaron a las protestas otros permanecieron fieles al gobierno.

Este domingo, Morales había convocado a nuevas elecciones tras conocerse el informe de la auditoría de la Organización de los Estados Americanos (OEA) que concluyó que  la primera ronda de las elecciones celebrada el pasado 20 de octubre debe ser anulada y realizarse un nuevo proceso electoral al considerar “improbable estadísticamente” que Morales haya ganado las elecciones.

> Mi pedido al pueblo boliviano es garantizar la convivencia pacífica y acabar con la violencia para el bien de todas y todos. No podemos estar enfrentados entre hermanos bolivianos.
>
> — Evo Morales Ayma (@evoespueblo) [November 10, 2019](https://twitter.com/evoespueblo/status/1193519296263184385?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### "Ha habido un golpe cívico, político y policial", afirma Evo Morales

“Sugerimos al presidente del Estado que renuncie a su mandato presidencial” , expresó el comandante en jefe Willimas Kalima, solicitud a la que se sumó el comandante general de la Policía, **Vladimir Yuri Calderón**,  quien señaló, "nos sumamos al pedido del pueblo boliviano de sugerir al señor presidente Evo Morales presente su renuncia para pacificar al pueblo de Bolivia en estos duros momentos que atraviesa nuestra nación".

Durante este domingo, **más de 17 funcionarios del gobierno de Evo Morales han renunciado a sus cargos,** "el golpe de Estado se ha consumado, quiero que sepa el pueblo boliviano que hemos tomado esa decisión para que no se use la violencia por parte de estas fuerzas agresivas contra el pueblo" manifestó Álvaro García Linera, ex vicepresidente de Evo Morales. **Adriana Salvatierra, quien debía seguir a cargo luego de la renuncia de Evo Morales y Álvaro García, renunció a la presidencia de la Cámara de senadores.**

Según el artículo 169 de la Constitución de Bolivia, en caso de "impedimento o ausencia definitiva" del Jefe de Estado, este será reemplazado por el vicepresidente y ante la falta de este, la Carta Magna establece que será el presidente del Senado el encargado de reemplazarle, en última instancia, el cargo podría ser asumido por el presidente de la Cámara de Diputados - actualmente Víctor Borda -  de otro modo "se convocarán nuevas elecciones en el plazo máximo de noventa días".

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
