Title: Acampan en Plaza de Mayo exigiendo la libertad de Milagro Sala, lideresa indígena
Date: 2016-01-28 12:36
Category: El mundo, Judicial
Tags: Argentina, CTEP Argentina, Milagro Sala
Slug: milagro-sala-esta-presa-por-reclamar-ctep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Tupác-Amaru.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Ideas Graves 

###### [28 Ene 2016] 

Organizaciones sociales y populares, partidos políticos y sindicatos en Argentina adelantan desde ayer una jornada de acampe en la histórica Plaza de Mayo, a manera de reclamo por la **liberación de la lideresa de la organización Tupác Amaru, Milagro Sala**, detenida el pasado 16 de enero bajo cargos de “tumulto e incitación al delito”.

A través de un comunicado de prensa voceros de la Confederación de Trabajadores de la Economía Popular (CTEP), organización que hace parte de la manifestación, consideran la decisión de la justicia de Jujuy como “**una clara criminalización de la protesta social**” que “sienta un precedente nefasto para la democracia y para todos los movimientos populares”.

Miembros de la CTEP denunciaron que personal policial de civil rondaban el lugar del acampe minutos antes de la media noche para hacer efectiva una supuesta orden de desalojo impartida para reprimir la actividad, “Todo indica que van a venir”, aseguró Juan Martín Carpenco, integrante de la organización.

El derecho a la protesta en plaza pública que se viene adelantando de manera pacífica en uno de los espacios emblemáticos para este tipo de acciones en el país del sur, podría verse vulnerado por cualquier tipo de intervención represiva. “**No van por Milagro Sala, van por todos los que intenten ponerle un freno al avance de las políticas neoliberales**” afirmaron los manifestantes.

Por su parte, desde su lugar de reclusión, Milagro Sala envió a través de la cuenta en twitter de la organización que lidera, un mensaje de agradecimiento por las manifestaciones de apoyo por su causa, invitándolos a no bajar los brazos en la lucha de todos.

> El msj d [@SalaMilagro](https://twitter.com/SalaMilagro):Donde hay 1 necesidad ahi estamos.No bajemos los brazos en esta lucha d todos [\#LiberenAMilagro](https://twitter.com/hashtag/LiberenAMilagro?src=hash) [pic.twitter.com/mspX2ur0Hi](https://t.co/mspX2ur0Hi)
>
> — Prensa Tupac (@PrensaTupac) [enero 28, 2016](https://twitter.com/PrensaTupac/status/692667324495716352)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
El acampe que reúne organizaciones como el Movimiento Evita, la CTA, ATE, Seamos Libres, el Movimiento de Trabajadores Excluidos, el Partido Comunista, la Tupac Amaru, Suteba y La Cámpora y varios legisladores porteños kirchneristas, **continuará como han manifestado sus voceros hasta que se ordene la liberación de Sala** y finalice la persecución contra aquellos que defiendan los derechos de los trabajadores.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
