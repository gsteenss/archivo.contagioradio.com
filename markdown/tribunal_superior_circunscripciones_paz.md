Title: No fue correcto archivar acto legislativo de las curules de paz: Alfredo Beltrán
Date: 2017-12-19 16:45
Category: Nacional, Paz
Tags: circunscripciones especiales de paz, Senado, Tribunal Administrativo de Cundinamarca
Slug: tribunal_superior_circunscripciones_paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/1465514596_651889_1465514661_noticia_normal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [19 Dic 2017]

[El Tribunal Superior de Cundinamarca dio por aprobadas las Curules Especiales de Paz y ordenó al presidente del Senado que envíe el acto legislativo a la presidencia para que se promulgue la iniciativa y pase a control constitucional. Sin embargo, ]Gregorio Eljach, secretario del Senado, dijo que solo se acatará el fallo cuando el Consejo de Estado tome su decisión final sobre ese tema, y no queden más instancias jurídicas a las que puedan acudir.

En medio de ese escenario, Alfredo Beltrán, exmagistrado de la Corte Constitucional explica que la discusión quedó reducida a saber si definitivamente quedó bien la orden de haber archivado el proyecto de Ley sobre las curules de paz, o si se debió enviar al ejecutivo para promulgarlo.

"Al no haberlo pasado al ejecutivo, en la omisión radica la demanda, y el tribunal considera que el proyecto está aprobado, porque está demostrado que no había tres senadores por estar privados de la libertad, y  el quórum se disminuyó de 102 a 99, **lo que hay que aplicar es la jurisprudencia de la Corte Constitucional, lo que procede es aproximar la mitad al número entero siguiente que es 50,** y en consecuencia, no estuvo correcto considerarlo archivado y no enviarlo al presidente", dice Beltrán.

Es decir, que según el exmagistrado, el fallo del Tribunal Administrativo de Cundinamarca, no se refiere a la Constitución, sino al reglamento del congreso consagrado en la Ley 5 de 1992, donde se dispone que una vez haya sido evacuado el trámite del proyecto en el Congreso, inmediatamente debe enviarse al ejecutivo para que este sea promulgado.

Así las cosas, el constitucionalista señala que **la acción de cumplimiento que interpuso el gobierno se usa cuando existen omisiones en el cumplimiento de una Ley** o de un acto administrativo por parte de una autoridad pública, y en este caso lo que consideró el Tribunal es que efectivamente se incurrió en una omisión, al no enviarlo al ejecutivo para promulgarlo, pues previamente se cumplieron con los requisitos constitucionales, es decir con la mayoría de votos en el Senado para aprobarlo.

El expresidente de la Corte Constitucional también señala que la hipótesis de que se invade el poder legislativo con el poder judicial no es cierta, porque se dice a una autoridad pública que hubo una omisión, por eso la demanda es sobre si se cumplió o no con un deber provisto en una Ley, y que debe subsanar un ente como el Consejo de Estado.

[Por otra parte, sobre la posibilidad de juzgar por prevaricato al presidente del Senado Efraín Cepeda, Beltrán considera que para que exista ese delito debe haber "el propósito de violar la ley", sin embargo, en este caso lo que habría sucedido es una discrepancia en la ley y y no no necesariamente eso significa la existencia de ese delito.]

### **Impungnarán el fallo** 

La secretaría del Senado, en cabeza de **Gregorio Eljach** ha anunciado que impugnará el fallo, un trámite que puede ser procesado en los siguientes tres días que se cumplirían en enero, debido a que se ha acabado periodo judicial durante este 2017.

**Por su parte, Rodrigo Lara, presidente de la Cámara de Representantes** dijo que no firmara el proyecto de las circunscripciones de paz, pues que considera que la decisión del Tribunal viola el principio de separación de poderes, pues “**por acción de cumplimiento no se puede obligar al Congreso** a darle vida jurídica a una norma que nunca existió”.

### **Reacción de Juan Manuel Santos** 

Cabe recordar, que hace unos días el Consejo de Estado emitió un concepto mediante el cual respalda la tesis del gobierno que asegura que el proyecto quedó aprobado con 50 votos. A eso también se refirió esta mañana el presidente Juan Manuel Santos, en una rueda de prensa sobre los retos y logros durante el 2017.

"El Consejo de Estado falló a favor de las víctimas. Estamos en un Estado de derecho y cuando hay esas diferencias, los jueces son las que las dirimen. Hay que continuar con los procedimientos judiciales para definir esa situación. Respetamos los procedimientos y respetamos a los jueces", dijo el mandatario.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
