Title: DD.HH. en Colombia: un panorama confuso y desesperanzador
Date: 2018-12-10 17:34
Author: AdminContagio
Category: DDHH, Nacional
Tags: Derechos Humanos, Día internacional de los DDHH
Slug: el-panorama-es-confuso-y-desesperanzador-en-materia-de-dd-hh-en-colombia
Status: published

###### Foto: Flickr 

###### 10 Nov 2018 

**[A propósito de la conmemoración de los 70 años de la Declaración Universal de Derechos Humanos, el defensor Jaime León hizo un balance de la situación que se vive en Colombia en esta materia, señalando que el panorama es un poco confuso y desesperanzador pues aunque la implementación de los acuerdos generó un cambio en las organizaciones sociales y las comunidades, persisten las amenazas y los asesinatos contra los líderes, además de criminalizarse la protesta social y las alternativas populares.  
]  
Los acuerdos siguen faltando a la realidad**

En el tema de la implementación en los espacios territoriales, León señala que se ha cumplido muy poco de los compromisos establecidos por el Gobierno, lo cual ha generado “una desazón en la población de los excombatientes” que no vieron implementado a cabalidad lo pactado en temas como oportunidades de estudio, salud o representación política.

También destaca que en vista de dicho incumplimiento,  la población excombatiente se ha reducido en los espacios territoriales, mientras que en los alrededores se han incrementando los actores armados que amenazan  a aquellos que defienden los derechos de las comunidades.

**El Gobierno no avanza en materia de DD.HH.**

León advirtió de igual forma que las garantías en materia de Derechos Humanos con la llegada de la administración de Iván Duque son más reducidas, algo que puede no se evidencie en el discurso pero sí en la realidad de las regiones donde hay “una especie de ausencia mayor del Estado”, lo cual para el analista genera desesperanza  y cansancio en la población. [(Le puede interesar: Duque se raja en los primeros 90 días de su mandanto en defensa de derechos humanos)](https://archivo.contagioradio.com/duque-se-raja-en-los-primeros-90-dias-de-su-mandato-en-defensa-de-derechos-humanos/)

**Preocupa la situación de los líderes**

[Ante el incremento de líderes e indígenas asesinados, el defensor de derechos humanos asegura que Colombia **“está viviendo otro genocidio, una política de de exterminio”** que debilita la participación de las comunidades y el ejercicio de la protesta social, mecanismo de las comunidades para reivindicar sus derechos.  
  
En contraste, León advierte que se está dejando “el camino libre a los violentos para que que se reorganicen y permanezcan en los territorios”, haciendo referencia a grupos paramilitares que “aparecen y reaparecen pero que siguen existiendo” lo que ha ocasionado que el silencio sea una vez más la norma para que la comunidades puedan permanecer en los territorios.]

**Colombia ante el mundo**

[Sobre la visita del relator de la **ONU, Michael Forst** al país, el defensor señala que durante su visita, el diplomático pudo dar cuenta de la carencia de garantías por parte del Estado o entes como la Fiscalía y guarda la esperanza de que con el pronunciamiento que Forst dé en su informe el Gobierno finalmente acate sus obligaciones para con la población. ]

[León concluye que ante los ojos del mundo, basándose en los informes de las ONG que hacen presencia en el país, la imagen de Colombia en materia de protección de derechos civiles está muy mal, “Colombia no es un país donde se respeten los derechos, ni la participación, ni el disenso” apunta el defensor quien insta a seguir denunciando la represión, pues tal como afirma “la realidad no puede taparse con un dedo”.   
]

<iframe id="audio_30703451" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30703451_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
