Title: Este martes se debate proyecto de Ley de protección animal
Date: 2015-05-25 12:25
Category: Animales, Movilización
Tags: Bancada Animalista, Congreso de la República, defensa animal, Juan Carlos Losada, Natalia Parra Osorio, Plataforma Alto, protección animales en Colombia, Proyecto de Ley 087
Slug: este-martes-se-debate-proyecto-de-ley-de-proteccion-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/11350523_1646365175578484_4548441061694681463_n.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Juan Carlos Losada 

<iframe src="http://www.ivoox.com/player_ek_4546433_2_1.html?data=lZqhmJmXd46ZmKiakp6Jd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DijIqflanJpsLoxrHS26bSrc7Vzb7OjdjJb9Hm0NLix9vJb8XZw8bhx5DIqYzk09Tmx8jYs4zYjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Natalia Parra, secretaria de la Bancada Animalista del Congreso de la República] 

**Mañana martes 26 de mayo se debatirá en plenaria de Cámara de Representantes el Proyecto de Ley 087** de protección animal, con autoría del representante a la Cámara Juan Carlos Losada en compañía de diversas organizaciones animalistas. El proyecto tiene el tercer lugar en la agenda legislativa.

De acuerdo a Natalia Parra, directora de la Plataforma ALTO y secretaria de la Bancada Animalista del Congreso de la República, **es necesario que la ciudadanía genere presión a través de twitter con \#DebateLeyAnimalYa** para que se debata este martes, ya que existe el riesgo que se pueda correr el debate; por eso se requiere de la presión ciudadana para que este proyecto llegue el próximo semestre a los debates en senado.

Cabe recordar que este Proyecto de Ley 087, **modifica el código civil** para que los animales dejen de ser vistos como bienes, de manera que pasarían a ser entendidos como seres sintientes, y ,además, se estaría modificando el **Código Penal** para que ciertas formas de maltrato animal pasen a ser un delito.

“Un país que mínimamente avanza en la consideración hacia las distintas formas de vida debe tener este tipo de medidas; **quien se oponga a este proyecto de ley, se opone a los mínimos que un país debe tener en defensa animal**”, asegura Parra.

Cuando se inició la tarea de preguntar a los congresistas sobre la reformulación de la Bancada Animalista, **63 firmas de senadores y representantes apoyaron la propuesta y aseguraron que estar dispuestos para que el proyecto de Ley** fuera un hecho. Sin embargo, afirma Natalia, “hay que tener cuidado con los lobbies que puedan ejercer ciertos sectores que ven como una amenaza este tipo de iniciativas, ya que se lucran con actividades como la tauromaquia y las galleras donde evidentemente existe crueldad animal”.

Mientras se promueve la protección  animal en el congreso, en contraposición está en discusión un artículo en el Código de Policía, que promovería **la eutanasia para animales abandonados, algo que realmente significaría el sacrificio de animales,** entendiendo que la eutanasia es una forma de darle fin a la vida de un ser cuando está sufriendo; por ello no se justifica sacrificar la vida de un animal cuando está totalmente sano. “Lo que se promueve con este proyecto de ley es un sacrificio. Eso no es distinto a lo que pasa en el conflicto armado en Colombia, ya que cuando a alguien le resulta incomodo otro, por cualquier razón le quita la vida”.

Para la directora de la Plataforma ALTO, la forma de manejar la situación de los animales callejeros, debe ser de **carácter humanitario**, es por eso que la Bancada Animalista moviliza un lobby para que este artículo sea retirado del Código de Policía y al tiempo se vaya consolidando cada vez más el sentir de los animales con el proyecto de Losada.
