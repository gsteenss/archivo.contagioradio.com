Title: Zonas Veredales Transitorias reciben hoy toneladas de solidaridad
Date: 2017-02-25 15:56
Category: Nacional, Paz
Tags: Angela Maria Robledo, FARC, solidaridad, Zonas Veredales
Slug: zonas-veredales-transitorias-reciben-hoy-toneladas-de-solidaridad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_1931.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [25 Feb 2017]

Decenas de personas y **organizaciones sociales se han movilizado hacia algunas de las Zonas Veredales con alimentos, ropa y medicamentos** que han recogido en el marco de la campaña “Venga esa mano por la paz”. La iniciativa se desarrolla ante las evidentes dificultades de habitabilidad, de salud y de alimentación en las Zonas que ya se tendrían que albergar, en condiciones dignas, más de 7000 integrantes de las FARC,

Para la representante a la Cámara Ángela María Robledo, aunque las personas no pueden solventar las obligaciones que tiene el Estado, si es necesario que las **muestras de solidaridad o de paz se hagan manifiestas** ante una situación como la que se vive actualmente, y en que la paz todavía es una “paz frágil”. ([Le puede interesar Bibliotecas y ludotecas para las zonas veredales](https://archivo.contagioradio.com/zonas-verdales-contaran-con-biblioteca-y-ludoteca-para-la-paz/))

Aunque hay que valorar el hecho de que ya no hay tantas situaciones dolorosas producto de la guerra, para Robledo, hay una burocracia que no ha permitido que las situaciones fluyan y eso hay que mejorarlo, por ejemplo la burocracia estatal hace que en las ZVTN no se pueda comprar plátano de la región sino que el plátano llega a Bogotá y de ahí se devuelve a las Zonas. ([Lea también Campaña de solidaridad con guerrilleros en zonas veredales](https://archivo.contagioradio.com/inicia-campana-de-donaciones-para-guerrilleros-de-las-zonas-veredales/))

### **“El congreso que se ponga las pilas porque la gente de la guerrilla tiene que vivir con dignidad”** 

Robledo señala también es necesaria la presión social para que el congreso cumpla su misión, “esto ya de fast track no tiene nada”  y por ello es necesario que se siga impulsando el movimiento que por una parte materializa la solidaridad pero por otra parte ejerce prsión política con movilización para el congreso.

###### Reciba toda la información de Contagio Radio en [[su correo]
