Title: Empieza febrero y ya se registran dos asesinatos de lideresas sociales
Date: 2018-02-07 18:04
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales
Slug: febrero_asesinatos_lideres_sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/asesiantos-lideres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio radio] 

###### [7 Feb 2018] 

En los primeros días del mes de febrero, ya son dos las líderesas sociales en Colombia víctimas de la violencia. Se trata de Yolanda Maturana, líder ambientalista  asesinada el pasado 2 de febrero en Pueblo Rico, Risaralda, y Sandra Yaneth Luna, presidenta de la Junta de Acción Comunal de la vereda Totumito Carboneras, en Tibú, Norte de Santander.

### **Yolanda Matuarana** 

<div id="resumen" style="text-align: justify;">

La defensora del medio ambiente, Yolanda Maturana, fue asesinada en el interior de su vivienda. Maturana era conocida en los departamentos de Risalda y Chocó por su trabajado denunciando la minería ilegal y la contaminación de las fuentes hídricas.

</div>

<div id="contenido" style="text-align: justify;">

Su asesinato ocurrió cuando en el municipio en el que vivía cuando dos hombres llegaron encapuchados a su casa y le dispararon dentro de su vivienda.
</p>
### **Sandra Yaneth Luna** 

</div>

 El asesinato de esta lideresa se conoció este miércoles, aunque se encontraba desaparecida desde el mes de septiembre del año pasado. Su cuerpo fue hallado en horas de la noche en la vereda La Primavera, zona rural de Campo Dos, en horas de la noche.

De acuerdo con medios locales, ante esta situación la Alcaldía de Tibú ha hecho un  llamado a los grupos armados ilegales para que respeten el derecho a la vida y el trabajo de los l[íderes comunales y de toda la comunidad del municipio.]{.text_exposed_show}

<div class="text_exposed_show">

Ante estos hechos, nuevamente las organizaciones defensoras de derechos humanos prenden las alarmas, y hacen un llamado al Gobierno Nacional y a la Defensoría del Pueblo para que adopten medidas contundentes en materia de política pública de prevención de asesinatos de lideresas y líderes sociales en el país, ya que cabe recordar que solo en enero de 2018, fueron asesinados  22 líderes en el país. (Le puede interesar: E[nero un mes trágico para líderes sociales)](https://archivo.contagioradio.com/enero_asesinatos_lideres_sociales/)

###### Reciba toda la información de Contagio Radio en [[su correo]

</div>
