Title: Colombia sale a las calles a defender los Acuerdos de paz
Date: 2017-06-01 13:00
Category: Movilización, Nacional
Tags: acuerdos de paz, Paz a la Calle
Slug: colombia-sale-a-las-calles-a-defender-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [01 Jun 2017] 

Diferentes colectivos, organizaciones y la ciudadanía en general, han convocado movilizaciones en todo el país, para que a partir de las 5 de la tarde las y los colombianos salgan a las calles a exigirle al Congreso de la República que **respete los acuerdos firmados en el Teatro Colón y no genere modificaciones que alteren los puntos pactados y respaldar los diálogos con el ELN.**

En Bogotá la cita es las **5:00 pm en el Planetario** y culminará en la Plaza de Bolívar. Se espera que cientos de capitalinos se sumen a la marcha de antorchas. En Medellín la cita será enfrente del **Teatro Pabón desde las 6:00 pm** y el punto de llegada será el Parque de las Luces, en donde habrá actos culturales.

En Barranquilla las personas se reunirán desde las **5:30 pm en la Plaza de la Paz** y realizarán un plantón, en Cali también se realizarán actos culturales en el **Parque de los Poetas, desde las 6:00 pm**. Le puede interesar:["Proponen movilización continúa para defender Acuerdos de Paz"](https://archivo.contagioradio.com/sociedad-civil-se-moviliza-para-respaldar-acuerdo-de-paz/)

Paz a la Calle ha sido desde el pasado 2 de octubre, una de las organizaciones ciudadanas que se ha encargado de gestar escenario para hablar de la construcción de paz, Camila Reyes integrante de esta plataforma señaló que otra de las iniciativas que se podrán en marcha son las **veedurías ciudadanas durante los debates que se den del Fast Track para presionar a los congresistas a que no alteren los acuerdos y asistan a los debates**.

Reyes expresó que la ciudadanía en estos momentos “ hay que reafirmar que **hay una búsqueda colectiva por condiciones para que haya una paz con justicia social y ambiental** y los acuerdos con las insurgencias, todas las reivindicaciones de los sectores sociales giramos en la misma dirección y nos unimos a esta lucha”. Le puede interesar: ["Víctimas exigen a Congreso mantener esencia de los Acuerdos de Paz"](https://archivo.contagioradio.com/victimas-exigen-a-congreso-mantener-esencia-de-los-acuerdos-de-paz/)

<iframe id="audio_19025958" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19025958_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
