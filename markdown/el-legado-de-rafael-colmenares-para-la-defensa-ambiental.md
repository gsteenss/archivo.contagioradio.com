Title: El legado de Rafael Colmenares Faccini para la defensa ambiental
Date: 2017-02-08 15:53
Category: Ambiente, Nacional
Tags: Ambientalismo, Humedales, Rafael Colmenares
Slug: el-legado-de-rafael-colmenares-para-la-defensa-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Rafael-Colmenares-Faccini.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Censat] 

###### [8 Feb 2017 ] 

Rafael Colmenares Faccini, maestro del ambientalismo, abogado comprometido con la justicia socio-ambiental y uno de los referentes más importantes de la materia en Colombia y otros lugares del mundo, falleció el pasado 6 de febrero a los 69 años, debido a complicaciones en la tiroides. El maestro, parte dejando un gran legado para la defensa del **agua como un derecho fundamental y la necesidad de que sean las comunidades quienes lideren sus procesos de defensa territorial.**

Después de 20 años de arduas labores por la defensa del agua, la vida y los territorios, distintos amigos, familiares y organizaciones, resaltan su lucha que inició en los años 90, por el **reconocimiento y resignificación de los humedales en Bogotá,** proceso que de manera conjunta con otras 60 organizaciones, llevo a la construcción del **referendo por el agua y la moratoria minera.**

### Rafael Colmenares Faccini y las luchas en los territorios 

El abogado y ambientalista, se acercó a distintas realidades en diferentes departamentos, gracias a su interés por experiencias agroecológicas y de defensa territorial con grupos de campesinos, ambientalistas, indígenas, afrocolombianos, mujeres, jóvenes, trabajadores y grupos urbanos.

El referendo por el agua, tuvo como objetivo plasmar en la constitución el recurso hídrico como un derecho fundamental, **garantizar un mínimo vital gratuito y generar políticas públicas que impidieran su privatización.** Por otra parte, también impulsó la moratoria minera, la cual insta al gobierno para que **no se expida ni un título minero más, hasta que se resuelvan las problemáticas minero energéticas** y la crisis social que dichas actividades han desencadenado.

Tatiana Roa, directora de Censat y además entrañable amiga de Rafael Colmenares Faccini desde los años 90, manifestó que el trabajo de Colmenares **“fue la semilla que impulsó importantes iniciativas como los Comités Ambientales”**, que están presentes y han fortalecido la defensa del ambiente en distintos lugares del país como Santurbán, Cajamarca, San Martín, La Guajira, y el Huila.

### ¿Cuáles son los retos que quedan para la defensa ambientalista? 

Tatiana, señaló que las organizaciones que trabajan actualmente por la defensa ambiental, bien sea desde la comunicación, el derecho o la educación, **“deben iniciar una reflexión orientada hacia un ambientalismo profundo”**, resalta que es necesario el compromiso, la persistencia y el trabajo por un ambientalismo **que sea más aterrizado a las realidades de las comunidades.**

Agregó que Rafael tuvo la oportunidad de acompañar el último seminario interno de Censat y allí “expresó su profundo dolor por lo que sucede actualmente en el país, planteó que tenemos muchos retos y batallas por dar en el ámbito ambiental”. ([Le puede interesar: El tema ambiental y las conversaciones con el ELN](https://archivo.contagioradio.com/tema-ambiental-las-conversaciones-eln/))

Por último, Tatiana expresó que las organizaciones y comités deben “rescatar el pensamiento de Rafael”, indicó que es vital **dar continuidad a las iniciativas y luchas en cada territorio para lograr un modelo energético distinto en Colombia**, un modelo económico y social que respete los derechos humanos de las comunidades y valore el potencial ambiental y cultural del país.

<iframe id="audio_16907251" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16907251_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
