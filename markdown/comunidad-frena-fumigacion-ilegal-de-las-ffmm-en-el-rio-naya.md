Title: Comunidad frena erradicación inconsulta de las FFMM en el río Naya
Date: 2015-03-25 21:11
Author: CtgAdm
Category: Nacional, Resistencias
Tags: Comunidad "La Concesión" fumigaciones ilegales e inconsultas, Ejército colombiano fumigaciones ilegales río Naya, Río Naya
Slug: comunidad-frena-fumigacion-ilegal-de-las-ffmm-en-el-rio-naya
Status: published

###### Foto:Colombiadrogas.wordpress.com 

###### <iframe src="http://www.ivoox.com/player_ek_4264679_2_1.html?data=lZejlpubfY6ZmKiakp2Jd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRicuZpJiSo57Wp8ro0JDRx9jJscPV08jOjcrSb8Tjztrby8nFqMbnjLPO28aPtMLmwpDfx8bQrdvV05Cah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [José Narváez, líder Naya] 

Este fin de semana, el **ejército colombiano pretendió desembarcar tropas en el río Naya** para realizar la erradicación de cultivos de uso ilícito  **sin previa consulta a las comunidades.**

Ante la presencia de las FF.MM, y el recuerdo en las comunidades de las anteriores incursiones del 2001, en las que paramilitares, masacraron y desplazaron a los moradores, rápidamente se creó un clima de tensión que llevó a las comunidades a **impedir el aterrizaje de los helicópteros en el área prevista e impedir el avance de los efectivos militares** hasta que arribe al territorio una **misión humanitaria y de observación.**

José Garcéz  afirma que en el** año 2008** las comunidades **entregaron una propuesta al gobierno Colombiano** para que la erradicación de los cultivos de uso ilícito fuera **acompañada de una sustitución por otras siembras que pudieran sustentar económicamente a las comunidades**.

Esta erradicación mediante la propuesta enviada se realizaría **previa consulta a los afectados, manualmente, sin ningún tipo de elemento químico**, como el glifosato,  ya que este daña el ambiente y la salud de las personas habitan el territorio.

Pero la propuesta **no recibió ninguna respuesta por parte del gobierno,** y aún las comunidades esperan poder negociar para que las erradicaciones se realicen con el consentimiento de las comunidades.
