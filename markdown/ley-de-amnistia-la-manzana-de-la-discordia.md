Title: Ley de Amnistía la manzana de la discordia
Date: 2017-02-20 14:07
Category: Nacional, Paz
Tags: Cadena de Mando, Ley de Amnistia
Slug: ley-de-amnistia-la-manzana-de-la-discordia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/presos-politicos-climax-cover.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Posta.mx] 

###### [20 Feb 2017]

La Ley de Amnistía IURE permitiría que aproximadamente **3000 guerrilleros** que estén en las cárceles por delitos como rebelión y delitos conexos,  **sean indultados** y puedan trasladarse a las zonas veredarles, mientras que otros **7.000 guerrilleros,** que ya se encuentran en estos puntos de concentración, podrían beneficiarse de los alcances de la ley. Otros** 5000 militares estarían acobijados por Ley de Amnistía bajo renuncia de acción penal y libertad condicional**

De acuerdo con el abogado Enrique Santiago, los delitos que hacen parte de esta ley de amnistía son: rebelión, sedición, asonada, conspiración, seducción, usurpación  y retención ilegal del mando, así como 20 delitos más que hacen parte de la lista de delitos conexos. Le puede interesar: ["Crece el riesgo para los Acuerdos de Paz"](https://archivo.contagioradio.com/crece-el-riesgo-para-los-acuerdos-de-paz/)

Otro grupo que se encuentra acobijado por la ley de Amnistía IURE son aquellas personas que están en la cárcel por ser acusados de pertenecer a la guerrilla de las FARC-EP y colaborar con la misma, al igual que aquellos que han sido condenados por el ejercicio de la protesta social. Serían aproximadamente **4.500 personas las que se beneficiarían en estos casos.**

Frente a los militares, ninguno de ellos se encuentra bajo la amnistía IURE, debido a que esta es solo para delitos de rebelión,pero si están bajo la ley de amnistía, Enrique Santiago, consideró que puede haber un estimado de **5.000 militares que entren en estos beneficios.** Le puede interesar: ["Organizaciones sociales exigen justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

De otro lado, Enrique Santiago afirmó que se espera que con este decreto los jueces y fiscales comiencen a gestionar las amnistías de oficio “**en Colombia nunca ha existido ni una Ley de Amnistía tan extensa** y detallada como la aprobada el 30 de diciembre, ni un decreto reglamentario como este, que es todo un código de procedimiento penal”, siendo sumamente preciso e inmediato para no demorar más la aplicación de la amnistía.

### **“Miguel Vivanco tiene una gran confusión”  Enrique Santiago ** 

Sobre las declaraciones que hizo el director para América de la organización Human Rights Watch, Miguel Vivanco, en su cuenta de Twitter al señalar que **“el decreto sobre la ley de amnistía publicado, ni siquiera respeta el acuerdo de paz al suspender investigaciones sobre atrocidades”.**

Enrique Santiago expresó que las afirmaciones de Vivanco “son actitudes que coinciden con las de la derecha más retrograda de Colombia” evidenciando una **“gran confusión” entre lo que es un procedimiento judicial y lo que es la investigación de los delitos y crímenes que se hayan cometido en el conflicto. **

El decreto de Amnistía lo que prevé es la suspensión de los procesos judiciales para las personas que son trasladadas a las zonas veredales y tanto la Fiscalía General  como la Policía pueden continuar con la investigación. Le puede interesar: ["Ley de Amnistía es la más completa que se ha aprobado en Colombia"](https://archivo.contagioradio.com/enrique-santiago/)

**ACORE**

La Asociación Colombiana de Oficiales en Retiro de las Fuerzas Militares, expresó,  en la editorial de su periódico, que consideran **“bastante excluyente y lesivo” el marco de la Jurisdicción Especial para la Paz**, en algunos de sus contenidos, debido a que según esta organización “no se ha tenido en cuenta importantes observaciones previamente acordadas y formalmente aprobadas por parte del Gobierno Nacional”  en temas como la cadena de mando.

Frente a este tema, ACORE aclara que “La Corte Constitucional (sentencia C-578/2002), ha señalado que el artículo 28 del Estatuto de Roma, establece un tratamiento diferente al de la Constitución Nacional para el juzgamiento del mando por conductas penales de sus subordinados, circunstancia por la cual la ley penal colombiana **no está obligada a definir el concepto de “responsabilidad del mando”,** en la misma forma como lo determina el artículo en referencia”

Aludiendo a que las recomendaciones de la Corte Penal Internacional, sobre la necesidad de establecer la cadena de mando militar en delitos, únicamente tiene vigencia en los delitos que se cometan desde el año 2002 y que estén dentro de su jurisdicción. Le puede interesar:["Modificaciones a Jurisdicción Especial para la Paz abriría puerta a Corte Penal Internacional"](https://archivo.contagioradio.com/modificaciones-a-jurisdiccion-especial-de-paz-abririan-la-puerta-a-corte-penal-internacional/)

<iframe id="audio_17115455" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17115455_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
