Title: Siete personas son asesinadas en el Tarra, Norte de Santander
Date: 2018-07-30 16:43
Category: DDHH, Nacional
Tags: Catatumbo, Tarra
Slug: siete-personas-son-asesinadas-en-billar-de-tarra-nortesantander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/masacre-en-tarra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ASCAMCAT] 

###### [30 Jul 2018] 

En el municipio del Tarra, Norte de Santander, fueron asesinadas **siete personas y cuatro más resultaron heridas cuando  se encontraban departiendo en un billar del barrio La Esperanza, el hecho** [**sucedió**]** hacia las 3 p.m**. Según información, al local habrían llegado los hombres armados que abrieron fuego sin mediar palabra contra quienes allí se encontraban, de acuerdo con la población, los armados pertenecerían al grupo "Los Pelusos".

 La Defensoría del Pueblo rechazó los hechos a través de su cuenta de Twitter. Durante este año se han realizado dos misiones de verificación al Catatumbo que han manifestado la grave problemática que rodea a las comunidades debido a la presencia, control y enfrentamientos entre los grupos armados que operan en el territorio.

La organización criminal conocida como Los Pelusos ha buscado controlar las rutas del narcotráfico en el Catatumbo y de acuerdo con las autoridades su fuente de financiación depende principalmente de este negocio.

En Desarrollo...
