Title: Medidas Cautelares a resguardo indígena Uradá Jiguamiandó en Chocó
Date: 2017-11-01 16:32
Category: DDHH, Nacional
Tags: comunidades indígenas, Derechos Humanos, indígenas, medidas cautelares a indígenas, Restitución de tierras
Slug: medidas-cautelares-a-resguardo-indigena-urada-jiguamando-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/resguardo-jiguamiando.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [01 Nov 2017] 

Las comunidades indígenas del resguardo Uradá Jiguamiadó en el municipio del Carmen del Darien en el Chocó, **recibieron medidas cautelares por parte de la Agencia de Restitución de Tierras** para proteger la vida y el territorio teniendo en cuenta la constante violación a los derechos humanos de la cual han sido víctimas.

De acuerdo con Argemiro Bailarín, líder indígena Embera, la Agencia tuvo en cuenta **la sistematicidad en la violación a los derechos humanos,** la presencia de grupos armado ilegales y las operaciones extractivas que han perjudicado el territorio para que un juez decidiera, mediante providencia judicial, garantizar la protección de las comunidades indígenas de este resguardo.

Además, las medidas cautelares se otorgaron teniendo en cuenta la **presencia constante del grupo paramilitar** Autodefensas Gaitanistas de Colombia quienes han incursionado en el territorio de Jiguamandó en repetidas ocasiones. Por esto varias familias han salido desplazadas a la vez que algunos líderes han sido amenazados de muerte.  (Le puede interesar:["AGC incursionan en territorio colectivo de Juguamandó en Chocó"](https://archivo.contagioradio.com/agc-jiguamiando-choco/))

De acuerdo con la Comisión Interelesial de Justicia y Paz, las comunidades han **denunciado desde 2009** "el desarrollo de actividades de deforestación, extracción minera ilegal y siembra de coca por ocupantes de "mala fe". La Comisión indicó que desde hace varios años, las comunidades indígenas están en riesgo inminente de desplazamiento".

### **Acciones de protección deben tener en cuenta enfoque diferencial** 

Bailarían dijo que “la medida ordena al Ministerio de Defensa, a la Unidad Nacional de Protección, a la Unidad de Víctimas y otras instituciones, a que impongan y **desarrollen acciones para un mayor acompañamiento** a las comunidades”. Además, la Fuerza Pública está en el deber de realizar operativos para proteger la vida de los indígenas y el resguardo en general.

El juez también ordenó que “se debe **fortalecer la guardia indígena** como medida de protección para las autoridades y líderes indígenas”. Bailarín afirmó que todo esto se debe implementar teniendo en cuenta las costumbres y el enfoque diferencial de los pueblos indígenas. (Le puede interesar: ["Denuncian ingreso de 250 hombres en Jiguamandó, Chocó](https://archivo.contagioradio.com/jiguamiando-choco-agc/))

### **CIDH había otorgado medidas cautelares a este resguardo en 2010** 

A partir de la solicitud de medida cautelar que hicieron **ante la Comisión Interamericana de Derechos Humanos en 2010**, las comunidades del Resguardo Indígena Uradá Jiguamiandó alegaron haber sido víctimas de actos de violencia que pusieron en riesgo su vida.

Allí, relataron hechos como que, “el 30 de enero de 2010, dos helicópteros y un avión de las fuerzas armadas **habrían realizado un ametrallamiento y bombardeo** a 300 metros del asentamiento principal de la comunidad, dando con la casa de una familia donde se encontraban tres adultos y dos niños, que resultaron heridos”. (Le puede interesar: ["CIDH cobija a 9 mil mujeres Wayúu con medidas cautelares"](https://archivo.contagioradio.com/cidh-cobija-a-9-mil-madres-gestantes-y-lactantes-con-medidas-cautelares/))

Denunciaron también que, debido a ese hecho, “el señor José Nerito Rubiano Bariquí **fue herido en el tórax con arma de fuego**, a raíz del cual sufrió ruptura de columna vertebral y quedó parapléjico”. Por esto, la CIDH le solicitó al Estado que adoptara las medidas necesarias “a fin de proteger la vida e integridad personal de 87 familias”.

El Estado, **debió haber concertado las medidas a adoptar con los indígenas** y sus representantes y debió informar a la Comisión las acciones de investigación con respecto a los hechos “que dieron lugar a la adopción de medidas cautelares a fin de remover los factores de riesgo para los beneficiarios”.

<iframe id="audio_21827923" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21827923_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
