Title: Organizaciones piden que el Estado no monopolice la memoria histórica
Date: 2018-11-06 18:21
Author: AdminContagio
Category: Movilización, Nacional
Tags: Centro de Memoria Histórica, museo de la memoria
Slug: organizaciones-piden-que-estado-no-monopolice-memoria-historica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/centro-de-memoria-historica-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 6 Nov 2018

En una carta dirigida al presidente Iván Duque, firmada por la Red Colombiana de Lugares de Memoria (RCLM), la Mesa por la Verdad, la Red Nacional en Democracia y Paz (RNDP) además de diversas redes y organizaciones, solicitan que el nombramiento en la dirección del Centro Nacional de Memoria Histórica sea coherente con el momento coyuntural que vive el país, de cara al fortalecimiento de una reconciliación nacional.

La exigencia fundamental por parte de los firmantes es que les sean garantizados los mecanismos efectivos de inclusión y participación social a las víctimas y sobrevivientes del conflicto, pues reiteran que "la reconstrucción de la memoria debe recoger la complejidad de lo ocurrido y develar responsabilidades de todos los actores del conflicto en los hechos victimizantes”.

La carta advierte sobre el peligro que implicaría un dominio o monopolio del Estado sobre los ejercicios de memoria pues restringiría “los principios de pluralidad, participación, autonomía de los procesos de la verdad, dignificación y el reconocimiento de las víctimas y sobrevivientes”.

Tal inquietud deriva de los cuestionados  nombramientos de personas no aptas para dirigir diferentes instituciones estatales  como es el caso Mario Pacheco para la misma institución o el de Claudia Ortiz, quien aplicó a la dirección de la Unidad Nacional de Protección y tras un fallido proceso fue designada como directora de la Agencia de Desarrollo Rural.

De tal modo la persona designada para dirigir el Centro Nacional de Memoria Histórica  debe poseer la respectiva experiencia requerida para un cargo que estará en constante trato con comunidades, víctimas y sobrevivientes del conflicto, además de contar con méritos académicos comprobables y de calidad, que tenga una visión imparcial sobre el conflicto colombiano y los diversos actores del mismo.

Dichas garantías incentivarían la inclusión, y legitimidad del saber de las comunidades que han visto vulnerada su vida y su dignidad y hacer de estas una “parte integral de la construcción pluralista de las narrativas del pasado, del presente y del futuro de las regiones y del país.”

###### Reciba toda la información de Contagio Radio en [[su correo]
