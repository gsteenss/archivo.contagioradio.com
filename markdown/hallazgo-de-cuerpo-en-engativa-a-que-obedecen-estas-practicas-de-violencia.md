Title: Hallazgo de cuerpo en Engativa ¿a qué obedecen estas prácticas de violencia?
Date: 2019-07-17 16:32
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Alerta temprana, bandas criminales, Engativá
Slug: hallazgo-de-cuerpo-en-engativa-a-que-obedecen-estas-practicas-de-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/3213256204_25800175d4_b-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

El pasado martes 17 de julio fue hallado un cuerpo en el barrio Villa Luz de la localidad de Engativa. Este hecho se suma a otros similares que se han presentado en Bogotá, y que han sido denunciados por diversos líderes sociales, en donde evidencia el posible retorno de prácticas violentas puestas en marcha por estructuras armadas.  Asimismo, se ha hecho un llamado de atención frente a la falta de acciones por parte de las autoridades en estos hechos que se han catalogado de manera aislada.

Por esta misma situación, la Defensoría del Pueblo ha lanzado alertas tempranas en Ciudad Bolívar, Kennedy y Bosa debido a la presencia de grupos armados ilegales como el ELN, Las Águilas Negras (Bloque Capital), las Autodefensas Gaitanistas de Colombia, Los Rastrojos y otras como “Los Costeños”, “Los Paisas” y miembros de las disidencias de las Farc – EP.

Nestor Rosanía analista político y director ejecutivo del Centro de Estudios y Seguridad y Paz, expresó que este tipo de dinámicas de violencia, dan un claro mensaje de estructuras criminales que están detrás de estas acciones. El director aseguró que “casos similares se han presentado en la frontera entre Colombia y Venezuela, donde han aumentado significativamente decapitaciones, en una guerra entre el ELN, Clan del Golfo y estructuras de narcotráfico…”

**EL PARAMILITARISMO EN BOGOTÁ**

Según el analista Rosania, Bogotá es un punto estratégico para las rutas del narcotráfico, razón por la cual existe una constante disputa territorial. Sin embargo, a la aparición del cuerpo desmembrado, se han sumado previas apariciones de panfletos en donde se anuncian limpiezas sociales.

Además durante el 2018, localidades como Ciudad Bolívar presentaron un incremento en el asesinato a jóvenes, según Cristian Robayo, edil del Polo en Ciudad Bolívar, en el 2018 el número de homicidios en esta localidad fue de 259 personas, de los que el 60% era población joven. Asimismo, el abogado y analista político David Florez, podrían estar directamente relacionadas con el intento de estructuras paramilitar por tomar el control de esas localidades. [(Le puede interesar: Así opera el paramilitarismo en Bogotá)](https://archivo.contagioradio.com/asi-opera-el-paramilitarismo-en-bogota/)

Si bien este hecho no se ha esclarecido se espera que las autoridades tomen medidas, no desde un caso aislado, sino teniendo en cuenta los precedentes, y considerando las diferentes alertas tempranas emitidas por la Defensoría del Pueblo ante casos similares.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38643293" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38643293_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
