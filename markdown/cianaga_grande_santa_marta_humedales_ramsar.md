Title: Ciénaga Grande de Santa Marta será incluida en la lista de los humedales más amenazados
Date: 2017-07-02 18:46
Category: Ambiente, Voces de la Tierra
Tags: Ciénaga grande de Santa Marta
Slug: cianaga_grande_santa_marta_humedales_ramsar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Cienaga-Grande-de-Santa-Marta-e1461004237548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agencia Unal 

###### [2 Jul 2017] 

Pese a que hace algunos días el Ministro de Ambiente, Luis Gilberto Murillo anunciara que no se iba a considerar incluir la Ciénaga Grande de Santa Marta en el Registro Montreux, este sábado el ministro informó mediante su cuenta en twitter que seguirá todas las recomendaciones que dio la comisión de expertos de la Convención de Ramsar para salvar la Ciénaga.

**"Tras discusiones técnicas, consultas con líderes y diálogos con expertos, decidimos incluir la Ciénaga Grande S/Marta en Registro Montreux"**, dice el mensaje en twitter de Murillo. Esta decisión quiere decir que el complejo de humedales más grande del país,  será incluido en** la Lista de Humedales de Importancia Internacional que están en riesgo de muerte.**

El gobierno ya había anunciado que existe una estrategia para recuperar la Ciénaga, por medio de donaciones del Banco Interamericano de Desarrollo y del Fondo del Carbono. Pero ahora, al ser incluido en el **Registro Montreux significa que habrá una atención prioritaria para este ecosistema.**

Cabe recordar que su informe, la comisión de expertos señaló que “Dados los fuertes cambios en las características ecológicas (…) se requiere la toma de medidas urgentes por parte del Gobierno de Colombia que permitan mantener y recuperar su carácter ecológico y alcanzar su uso racional de acuerdo a los objetivos de la Convención”.

**La construcción de la vía La Prosperidad**, **los monocultivos de palma y banano, y la ineficiencia de las obras hidráulicas** de la vía entre Barranquilla y Santa Marta, han generado la disminución del agua dulce y la “gran pérdida” de bosques de manglar, netre otros problemas ambientales. [(Le puede interesar: Convención Ramsar confirma crisis ecológica de la Ciénaga Grande de Santa Marta)](https://archivo.contagioradio.com/cienaga-grande-santa-marta/)

<iframe id="audio_19620511" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19620511_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-ensu-correoo-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-porcontagio-radio .contenido}
