Title: Un pacto de silencio e impunidad en ciernes
Date: 2016-04-24 20:04
Category: Camilo, Opinion
Tags: Fiscalía General de la Nación, Juan Manuel Santos, Nestor Humberto Martínez, uribe
Slug: un-pacto-de-silencio-e-impunidad-en-ciernes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/URIBE-Y-SANTOS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@CamilodCasas](https://twitter.com/CamilodCasas)** 

###### 24 Abr 2016 

Un acuerdo hacia la impunidad se estaría configurando en los próximos días alrededor de Néstor Humberto Martínez.

Una jugada maestra que resolvería las tensiones existentes entre Santos Uribe, Santos Pastrana, asegurando el unanimismo en torno a la figura presidencial  de Vargas Lleras, dejando en buen ánimo a los Estados Unidos para asegurar los negocios territoriales de la pax neoliberal.

El cambio de tono en los trinos de Uribe a Santos es una señal. Uribe se ha referido al de la Casa de Nariño como Presidente, aceptando incluso, que si el plebiscito es un no, el proceso con la paz debe continuar pero con ajustes. Un giro de 180 grados.

Y por el otro lado, Santos ternando a Néstor Humberto para ser Fiscal General de la Nación ante la Corte Suprema de Justicia, luego de simular un proceso de participación amplio de candidatos, le expresa implicitamente a Uribe que su entrañable amigo y amigo de Vargas Lleras, y de Pastrana es prenda de confianza para los lios judiciales de la casta dirigente.

El tono de Uribe en sus recientes intervenciones es la respuesta más "humilde" de Uribe al mensaje que los EE.UU le envió al avalar la privación de la libertad de su hermano Santiago Uribe por su participación en el grupo paramilitar de los 12 Apóstoles, por una orden judicial.

El expresidente rebozó la paciencia de los gringos, pero por primera vez, ha ido comprendiendo que hay límites y con su larga cola pierde la amistad del norte y hasta puede resultar quemado.  Su última gira fue absolutamente torpe. Con el apoyo de unos pocos republicanos,  se le fue la lengua contra Obama. El Departamento de Estado que sabe de las andanzas históricas del expresidente y su familia, y de lo descortés que resulta desoir a su embajador en Colombia que le ha invitado a moderar sus ataques, tuvo su tatequieto con la detención de su hermano.

El asunto no para allí. Todos saben que la detención de Santiago Uribe  debe resolverse políticamente con el guiño del norte. Y ese entuerto en el Establecimiento, lo resuelve el nuevo Fiscal General de la Nación.

Es un secreto a voces que en este cargo, salvo que la Corte Suprema de una sorpresa, tiene nombre propio Néstor Humberto Martínez. Este abogado  cuenta con el visto del sector bancario más poderoso de Colombia, propietario del diario El Tiempo.

No hay que olvidar que Néstor Humberto siendo SuperMinistro de Santos se entrevistó con Uribe en dos ocasiones con conocimiento de EEUU, para zanjar las diferencias frente al proceso con las FARC EP.

Así las cosas en ese juego de lado y lado, el Establecimiento se reconcilia consigo mismo, todos con rabo de paja se aseguran impunidad e inmunidad unos más, otros menos, relacionados con el paramilitarismo, beneficiarios de éste. Y por supuesto, los empresarios que contarían con Fiscal de bolsillo. Uno que otro militar y policía, uno que otro capo paramilitar serán los chivos expiatorios.

En  medio de la parafernalia que asume la formalidad del debido proceso, cesarán las acciones contra Uribe y su familia, a cambio se morigeran las críticas al proceso con las FARC. Y finalmente los rostros del Establecimiento pactando la pax neoliberal con las FARC y el ELN, si esta mesa avanza, todo acordado sin mayores garantías o mejor las mismas garantías con las que ha sobrevivido el movimiento social y político organizado, asegura su "buen nombre", su "buena moral", su poder económico y político y los de la inversión extranjera.

Tal vez por esta decisión estratégica del poder, llega a destiempo la carta de los paramilitares Gaitanistas, diciendo que no son los responsables de la oleada de asesinatos y amenazas, qué son otros y que quieren decir la verdad. Esos otros son los de lógicas paramilitares de antaño.

Santos, si es hábil, le cojerá la caña a la cúpula Gaitanista con un sometimiento del sector que se desparamilitariza del neoparamilitarismo asentado en sectores del Establecimiento. Esa disposición  se puede leer entre líneas de la carta de los paramilitares de las Autodefensas Gaitanistas de Colombia que llegó a Contagio Radio dirigida al movimiento social

Es tarde quizás porque al someterse a esa Fiscalía de Martínez, de la cuál solo puede librarnos la ciudadanía empoderada que no existe y la Corte Suprema, su verdad será silenciada, pues revelaría capítulos nefastos de responsabilidad del Establecimiento en el neoparamilitarismo de los últimos 10 años.

Nos queda abogar ante la CSJ y [sumarnos a esta campaña virtual](https://www.change.org/p/corte-suprema-de-justicia-no-a-la-elección-de-néstor-humberto-mart%C3%ADnez-como-fiscal-general-de-la-nación?recruiter=24396200&utm_source=share_petition&utm_medium=facebook&utm_campaign=autopublish&utm_term=des-lg-share_petition-no_msg), por lo pronto.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
