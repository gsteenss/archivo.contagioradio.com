Title: ELN decreta cese de operaciones ofensivas para votación del plebiscito
Date: 2016-09-26 15:08
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, ELN, FARC, Juan Manuel Santos, Mesa Pública, Pablo Beltrán
Slug: eln-decreta-cese-de-operaciones-ofensivas-para-votacion-del-plebiscito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Proceso-de-paz-eln300316-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [26 Sep 2016] 

En una entrevista para la emisora del ELN, el comandante Pablo Beltrán afirmó que no serán un palo para atravesarse al proceso de paz entre el gobierno de Colombia y las FARC y por ello decretarán un **cese de operaciones ofensivas para las votaciones del plebiscito por la paz que se realizará el próximo 2 de Octubre.** Además resaltó que esa guerrilla no comparte algunos aspectos de los acuerdos pero que las dos mesas de conversaciones se complementan.

"Para despejar esa duda de que nosotros seamos unos obstáculos al proceso de refrendación o al plebiscito nuestra disposición es que no haya un accionar ofensivo del ELN en esos días del plebiscito para facilitar la participación de la gente" afirmó el integrante de la delegación de paz del ELN Pablo Beltrán y agregó que el ELN está **dispuesto a flexibilizar los puntos acordados con los que el gobierno no esté de acuerdo.**

Agregó que ya hay unas hipótesis de acuerdo en los temas álgidos, por ejemplo una serie de acuerdos parciales pero dejó claro que realizar ajustes no van a significar una renegociación de lo que ya está acordado, sin embargo señaló que **se espera que en los próximos días haya noticia en torno al **[[inicio de conversaciones de paz en la ciudad de Quito.](https://archivo.contagioradio.com/eln-el-pais-necesita-que-se-abra-la-mesa-sin-ningun-tipo-de-dilaciones/)]

A este respecto [[Luis Eduardo Celis](https://archivo.contagioradio.com/se-anuncia-inicio-fase-publica-de-conversaciones-de-paz-con-guerrilla-del-eln/)], integrante de la Fundación Paz y Reconciliación, aseguró que sin lugar a dudas es una buena señal por parte del ELN decretar el cese de operaciones ofensivas y aseguró **que hay un mensaje positivo de parte de esa guerrilla y muestra que no habrá oposición al acuerdo firmado entre el gobierno y las FARC,** y reiteró que sería muy buena noticia que [[en una próxima reunión se fije la fecha para la apertura de la mesa pública.](https://archivo.contagioradio.com/hay-que-insistir-en-que-gobierno-y-eln-se-sienten-a-negociar-victor-de-currea/)]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
