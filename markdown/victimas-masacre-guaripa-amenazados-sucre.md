Title: Víctimas de la masacre de la Guaripa son amenazados en Sucre
Date: 2019-06-14 13:52
Author: CtgAdm
Category: Comunidad, Judicial
Tags: Amenazas, cajar, Masacre de La Guaripa, MOVICE, Sucre
Slug: victimas-masacre-guaripa-amenazados-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/movice.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

**Candelaria Barrios y Malena Martínez, integrantes del MOVICE,** y familiares de víctimas de la masacre de la Guaripa denunciaron que han recibido amenazas días antes de la audiencia contra **William Rodolfo Martínez Santamaría**; un conocido ganadero de la región quien es uno de los acusados. Las amenazas expresan que ellas ni los abogados deben participar en la audiencia en cuestión, "o serían asesinados y desmembrados todos, hasta los escoltas"; según lo denunciado por el **Colectivo de Abogados José Alvear Restrepo (CAJAR).**

**Alirio Uribe Muñoz, abogado del CAJAR,** explicó que este viernes en San Marcos, Sucre, se desarrollará un juicio por un pleito de tierras que generó una masacre contra los hermanos Humberto Escobar Mercado, Preliciano Mercado García y Eusebio Osorio Mercado, más conocidos como los hermanos Pensó. Se trata de **la masacre de La Guaripa,** ocurrida en enero de 2018, y por la cual están en juicio cuatro personas, incluido William Rodolfo Martínez Santamaría; un conocido ganadero de la región, acusado de ser el presunto determinador.

Según el Colectivo de Abogados, la Masacre ocurrió luego de años en que el señor Martínez Santamaría había pretendido desplazar a los hermanos Pensó, argumentando que perturbaban su propiedad. En enero del año pasado, mientras los hermanos se disponían a cercar sus tierras, **tres hombres armados los asesinaron;** este caso llegó hasta el Juzgado Segundo Promiscuo del Circuito de San Marcos, dónde hoy se realiza la audiencia. (Le puede interesar: ["Tres hermanos reclamantes de tierras fueron asesinados en Sucre"](https://archivo.contagioradio.com/tres-hermanos-reclamantes-de-tierras-son-asesinados-en-sucre/))

### **Las amenazas serían una intimidación para evitar la audiencia** 

Uribe Muñoz, dijo que las amenazas llegaron contra los participantes en la audiencia tres días antes de la realización de la misma, y se les pedía incluso abandonar Sucre para salvaguardar su vida. El Abogado señaló que esta audiencia es preparatoria, y se están tramitando las solicitudes de pruebas contra **Neisser Contreras, Ever Madrid y Roger Contreras,** como presuntos autores materiales del hecho, y contra Martínez Santamaría. (Le puede interesar: ["Atentan contra Rodrigo Ramírez, integrante del MOVICE en Sucre"](https://archivo.contagioradio.com/atentan-contra-rodrigo-ramirez-integrante-del-movice-en-sucre/))

Para el Integrante del CAJAR **"todo indica que es una forma de intimidación porque este juicio se venía adelantando al interior de la cárcel para que las víctimas no participaran, y ahora las víctimas van a estar",** razón por la que esperan un acompañamiento institucional que brinde seguridad a quienes asistan a la audiencia. Aunque las entidades oficiales se han comunicado con las integrantes del MOVICE y los familiares, Uribe Muñoz solicitó medidas estructurales para garantizar su protección.

> ⚠[\#AlertaTemprana](https://twitter.com/hashtag/AlertaTemprana?src=hash&ref_src=twsrc%5Etfw) por riesgo de homicidio y agresiones contra víctimas y acompañantes del caso Masacre de Guaripa  
> ? Sujetos sin identificar se han comunicado repetidas veces con la señora CANDELARIA DEL CARMEN BARRIOS ACOSTA, dirigente y Secretaria Técnica del Movice Sucre. [pic.twitter.com/lJUylr0sT9](https://t.co/lJUylr0sT9)
>
> — Movice (@Movicecol) [14 de junio de 2019](https://twitter.com/Movicecol/status/1139573202911485952?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_37110244" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37110244_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
