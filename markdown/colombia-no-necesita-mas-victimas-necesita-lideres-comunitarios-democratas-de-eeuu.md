Title: “Colombia no necesita más víctimas; Necesita líderes comunitarios” Demócratas de EEUU
Date: 2016-12-14 17:50
Category: DDHH, Nacional
Slug: colombia-no-necesita-mas-victimas-necesita-lideres-comunitarios-democratas-de-eeuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Demócratas-de-Estados-Unidos-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: thepolitic] 

###### [14 Dic 2016]

Con el “mayor sentido de urgencia y extrema preocupación” **un grupo de 37 congresistas demócratas de Estados Unidos** instaron al secretario de Estado Jhon Kerry para que presione al gobierno de Colombia en torno a la reciente ola de asesinatos y amenazas contra defensores y defensoras de Derechos Humanos en Colombia. Además animaron al embajador Kevin Whitaker para que visite zonas de peligro para la defensa de los DDHH.

En la misiva los legisladores demócratas llamaron al gobierno de EEUU a que de manera urgente presione al gobierno de Colombia y se condenen las agresiones en los términos más fuertes posibles “le instamos a que presione y colabore con el gobierno colombiano para condenar públicamente esta violencia en los términos más fuertes posibles (…) **Tales acciones deben hacerse de manera repetida y consistente, en todas las regiones y en todos los niveles de gobierno”**

En la carta, los congresistas aseguran que es necesario que se implementen de manera urgente los puntos 3.4 del acuerdo que incluye la comisión de alto nivel para las garantías de seguridad y el punto 5.2 para establecer políticas de protección a defensores y defensoras de DDHH.

Los congresistas concluyeron que están dispuestos a brindar todo el apoyo necesario para proteger a defensores de DDHH “estamos dispuestos a brindar todo el apoyo que podamos para proteger a **estos defensores y activistas y avanzar en su papel en la construcción y consolidación de una paz justa y duradera en Colombia**.”

Este es el texo original de la carta...

#### Dear Secretary Kerry, 

#### We write to express our greatest sense of urgency and extreme concern regarding the escalation of murders, threats and attacks against Colombian human rights defenders, trade unionists, LGBT and women activists, land rights advocates, and community, campesino, Afro-Colombian and Indigenous leaders. 

#### We know that you share our concern. For this reason, we urge you to press and work with the Colombian government to publicly condemn this violence in the strongest possible terms, increase and expand mechanisms designed to prevent such attacks from being carried out, and provide defenders and social leaders protection so that they may continue exercising their invaluable leadership and work. Such actions must be done repeatedly and consistently, in all regions and at all levels of government. We also strongly recommend that protection measures be designed in close consultation with vulnerable communities, individuals and organizations in order to strengthen their effectiveness. 

#### We emphatically condemn these murders and the intimidation of social leaders and activists. They are the very human capital that Colombia requires as it now faces the challenges of implementing the peace accords, promoting development and political participation, and providing guarantees for truth, justice, reparations and an end to violence. Colombia does not need more victims; it needs community, regional and national leaders willing to take on these challenges. 

#### In June, U.S. Ambassador Kevin Whitaker joined the ambassadors from Sweden, the United Kingdom, Germany, France, Norway and Canada and denounced the killings of Colombian human rights defenders and pledged to work on behalf of defenders at risk. We strongly encourage you to direct Ambassador Whitaker and this diplomatic group to visit areas where defenders are most threatened and vulnerable, denounce the violence perpetrated against these defenders and activists, and provide mechanisms of protection. 

#### At the same time, we urge you, Mr. Secretary, to work with the appropriate authorities in Colombia to ensure the prompt implementation of point 3.4 of the Peace Accord in its entirety, which includes a fully functioning High Level Commission on Security Guarantees and an active Special Investigation Unit within the Public Prosecutor’s Office, along with point 5.2., which commits the government to establishing policies and strategies of protection for human rights defenders, in particular those who are victims of paramilitary and official authorities. Given the spike in killings and assaults, it is imperative that these mandates are achieved rapidly and vigorously. It is also notable that neither of these points was raised as a matter of controversy by opponents of the accords. 

#### Please know, Mr. Secretary, that we stand ready to provide whatever support we can to protect these defenders and activists and advance their role in building and consolidating a just and lasting peace in Colombia. 
