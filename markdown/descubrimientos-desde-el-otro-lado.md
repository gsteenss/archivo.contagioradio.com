Title: Descubrimientos desde el otro lado
Date: 2015-03-30 12:36
Author: CtgAdm
Category: Eleuterio, Opinion
Tags: David Segarra, documentalista, Gaza, Palestina
Slug: descubrimientos-desde-el-otro-lado
Status: published

###### Foto: David Segarra 

#### Por [[**Eleuterio Gabón**] ](https://archivo.contagioradio.com/eleuterio-gabon/) 

Acostumbrado a ver el mar desde niño, David se preguntaba, cómo lo hacen quienes crecen junto a las playas, qué habría al otro lado del horizonte. Guiado por la curiosidad y cierto afán de aventura, ya adulto decidió viajar al otro extremo del Mediterráneo, una vez formado como periodista.  En su equipaje llevaba un fuerte compromiso solidario, como no puede ser de otro modo en alguien que deja Valencia y cruza el mar hasta Gaza.

En tierras palestinas descubrió que el paisaje le era familiar. En primer lugar por la intensidad de la luz y de los atardeceres, con la diferencia de que si bien en su Valencia natal el sol salía por el mar, en Gaza por el contrario, el sol se hundía en éste hasta desaparecer en la noche. También reconoció los olivos, los naranjos y las higueras, las huertas y sus frutos y entre tantas semejanzas como no podía ser de otro modo, reconoció en los rostros de quienes allí vivían el parecido con los de sus amigos y familiares. Eso sí, había una diferencia fundamental entre su pueblo y este que empezaba a descubrir; los palestinos llevaban décadas viviendo bajo la ocupación militar israelí y este hecho había forjado en ellos una cultura de resistencia.

Esta forma de cultura les hacía mantener unos fuertes vínculos con su tierra, con su naturaleza, con su entorno y por supuesto, un fuerte arraigo con su comunidad. Las familias vivían juntas, el día a día era en común y la personalidad de cada uno se desarrollaba al son de la comunidad a la que pertenecía. Entendió David que esto no era en absoluto algo anecdótico sino más bien una consecuencia firme de su condición de resistentes. “*Resulta mucho más sencillo dominar a un pueblo individualista de esos educados en la filosofía del sálvese quien pueda. La unión entre los palestinos hace que ante un ataque del invasor, la comunidad responda unida, lo que significa que nadie queda aislado; los traumas, la tristeza, la pobreza no se asumen en soledad sino que se viven colectivamente. Esa es una de las claves de su fortaleza como pueblo.”*

Una tarde mirando al mar, allá del otro lado que cuando miraba niño, David pensó en su tierra. En la gran urbe que era su ciudad natal se vivía realmente al modo individualista, hedonista incluso, una sociedad fragmentada, alejada de sus tradiciones, de su vínculo con la tierra, perdiendo su identidad cultural por los dictámenes de la moda global y habiendo renunciado casi por completo incluso a su propia lengua. Haciendo memoria, reconoció como la ciudad con su lógica desarraigada se había tragado gran parte de la vida en los campos y las huertas del litoral de las que tanto se había acordado al llegar a Gaza. Gaza, un territorio sin duda hostil, la cárcel a cielo abierto más grande del mundo donde las masacres se programan sistemáticamente, sometida al perverso bloqueo de sus necesidades más básicas, le revelaba una verdad cruel.

La realidad de un monstruo con diferentes caras que era el mismo en todos lados. Ese que reproduce el mismo esquema explotador y opresor que destruye y saquea el Amazonas, expolia y tortura en África o reprime y aliena en Occidente. La eliminación de las diferencias, de la riqueza de la diversidad humana y natural en favor de una unificación espantosa, impuesta con violencia y decorada con nombres como progreso, prevención, civilización o derecho. Entonces entendió también el enorme valor de este pueblo en resistencia.

David Segarra, periodista de raza que fue a solidarizarse con el pueblo palestino, nos descubrió que había mucho que aprender de ellos.
