Title: Caso Santrich: una incertidumbre jurídica
Date: 2019-02-11 12:00
Author: AdminContagio
Category: Expreso Libertad
Tags: extradicion, FARC, proceso de paz, santrich
Slug: caso-santrich-incertidumbre-juridica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/santrich_-_archivo_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Espectador 

###### 8 Feb 2019 

**330 días completa Jesús Santrich en la cárcel** La Picota, pese a que según su defensa **no se ha demostrado el acervo jurídico para mantenerlo en prisión** y que aún se desconocen las pruebas que tiene la DEA como supuesta evidencia de los delitos por narcotráfico de los que se le sindica.

En este programa del Expreso Libertad hablamos con **Francisco Toloza, ex prisionero político del Partido FARC,** quien espera que la Jurisdicción Especial para la Paz (JEP), falle en defensa de las garantías de estabilidad jurídica para Santrich.

<iframe id="audio_32391286" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32391286_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
