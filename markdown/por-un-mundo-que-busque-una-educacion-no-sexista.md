Title: Cambiemos el chip, es hora de pensar en una educación no sexista
Date: 2017-07-28 14:09
Category: Libertades Sonoras, Mujer
Tags: educacion, Educacion no sexista, feminismo, mujeres
Slug: por-un-mundo-que-busque-una-educacion-no-sexista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/educaicon-no-sexista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diálogo Educativo] 

###### [26 de Jul. 2017]

[¿Cuáles son las principales problemáticas que presenta la educación formal, frente a la inclusión del enfoque de género? ¿]Qué es la educación popular feminista? ¿Cuáles son los principales retos tanto en la educación formal como no formal, frente al enfoque de género? son algunas de las preguntas que respondimos **en \#LibertadesSonoras frente al tema Educación no Sexista.**

En este programa contamos con la voz de dos importantes entrevistadas, [**Carolina González Moreno quien hace parte del Movimiento Popular de mujeres de la sureña,** es Feminista Popular desde hace 7 años, Fotógrafa, Licenciada en Educación comunitaria con énfasis en DDHH. Coordinadora del proceso de alfabetización y educación con mujeres jóvenes y adultas con enfoque de género en la Localidad de Bosa.]

Y con [**Lizeth Villada quien es Socióloga y feminista,** integrante de la Red de Educación Popular entre Mujeres, la Mesa 1257 y la Red de Mujeres Jóvenes.]

Además, **en \#LibertadeSonoras dejamos algunas propuestas que podrían ayudar a que la sociedad avance** en el camino de una sociedad no sexista desde la educación, no solo en el colegio o universidades, sino desde el hogar, la iglesia, el trabajo, entre otros.

<iframe id="audio_20052495" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20052495_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
