Title: Petroleras contaminan 2 litros de agua por segundo en Putumayo
Date: 2015-08-18 12:43
Category: Ambiente, DDHH, Entrevistas
Tags: Agencia Nacional de Licencias Ambientales, Amerisur, derecho al agua, Gran Tierra., licencias ambientales, Putumayo, Vetra
Slug: petroleras-contaminan-2-litros-de-agua-por-segundo-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/contaminacion_petroleo_putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: archivo Jani Silva 

###### [18 Ago 2015 ] 

El pasado 15 de agosto tuvo lugar en el municipio de Puerto Asís, Putumayo la Audiencia de control político a la explotación petrolera , los efectos de las fumigaciones con Glifosato, la militarización y la crisis de Derechos  humanos que afronta el departamento. **De acuerdo a la información entregada las empresas petroleras están contaminando 2 litros de agua por segundo.**

Juliana Chaparro defensora de derechos humanos e integrante de la Comisión de Justicia y Paz, afirma que la extracción de petróleo en Puerto Asís ha **impactado negativamente ecosistemas estratégicos, como humedales y bosques de selva tropical,** contaminando 2 litros de agua por segundo. Así mismo, Juliana asegura que las afectaciones sociales se relacionan con la militarización del territorio y la violación de derechos humanos.

Pese a que **las autoridades competentes en materia de legislación ambiental fueron convocadas con anterioridad, no comparecieron a la audiencia.** No obstante, las comunidades indígenas y campesinas afectadas, así como el congresista Alirio Uribe y las organizaciones defensoras de derechos humanos insisten en llamar la atención sobre la responsabilidad estatal en la suspensión de las licencias ambientales que han sido aprobadas para las ocho empresas con presencia en la región, entre ellas **Amerisur, Vetra y Gran Tierra.**

Durante el evento se presentó la documentación de los **impactos sociales y ambientales de esta actividad, recopilada  durante dos años por las comunidades indígenas y campesinas** afectadas, en articulación con organizaciones defensoras de derechos humanos.
