Title: En Mapiripán reprimen la protesta pero no atienden denuncias de delitos electorales
Date: 2015-10-26 16:37
Category: Nacional, Política
Tags: alcalde Jorge Iván, Alexander Mejía Buitrago, Bernardo Nievas, candidatos en Mapiripán, Compra de votos, elecciones en Mairipán, Jorge Iván Duque, mapiripan, Meta, Wilson Díaz Montoya
Slug: en-mapiripan-reprimen-la-protesta-pero-no-atienden-denuncias-de-delitos-electorales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Mapiripan.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CONPAZ 

<iframe src="http://www.ivoox.com/player_ek_9170628_2_1.html?data=mpakkpuWfI6ZmKiak5qJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Bfrsbdy9fNtIa3lIquk9OPtsbk087ax9OPsMKf0dfc1srXuMKf0crf0ZDSs4zV1c7S0MnJsozYxtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Bernardo Nievas] 

###### [26 Oct 2015]

En Mapiripán, Meta la jornada electoral transcurrió en medio de fuertes rumores de fraude que habían sido notificados desde hace una semana, pero a los que las autoridades no le prestaron la atención pese a las denuncias de varios pobladores. En cambio, tras la indignación de las personas, la policía reprimió la protesta social.

Según Bernardo Nievas, periodista del municipio, **el actual alcalde, Jorge Iván Duque, quien ha recibido varias críticas por su gestión, habría anunciado que para estas elecciones "pondría su remplazo", lo que efectivamente logró con la elección de Alexander Mejía** Buitrago, impulsada por la compra de votos, orquestada por Wilson Díaz Montoya, que se encuentra en condición de casa por cárcel para cumplir su condena de 6 años por corrupción.

Las comunidades indígenas y demás pobladores del municipio asumen esta situación de fraude como **“un insulto a la comunidad, una imposición arrogante”** y que se suma a la prohibición de circulación de automóviles en el casco urbano, que impidió la movilización de personas de la tercera edad y en condición de discapacidad a los puestos de votación.

Otro hecho que cuestiona la transparencia de estas elecciones es la entrada del alcalde Jorge Iván Duque a la escuela del municipio en la que se estaba realizando el conteo de los votos, lo que provocó la exaltación de los pobladores que protestaron y que luego fueron reprimidos con gases lacrimógenos. Otras personas decidieron dirigieron a la alcaldía para incendiar sus instalaciones. **Manifestaciones son una muestra de su rechazo a la intención de perpetuación en el poder por parte del actual alcalde,** asegura Nievas.

Se espera que los organismos de control hagan presencia en el municipio e investiguen las irregularidades de esta jornada electoral, a las que se suman el que los materiales como tarjetones y urnas hayan sido alojados en una finca y no en la Registraduría como debía ser.

Por su parte, las autoridades indígenas estiman la posibilidad de convocar **una marcha pacífica para pronunciarse en contra de estas elecciones por su ilegitimida**d.

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/VID-20151026-WA0001.mp4\[/KGVID
