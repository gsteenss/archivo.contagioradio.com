Title: Organizaciones y personalidades rechazan visita de Benjamin Netanyahu a Colombia
Date: 2017-09-13 13:38
Category: DDHH, Nacional
Tags: Benjamín Netanyahu, boicot, colombia, Israel, Juan Manuel Santos, Palestina, TLC, Tratado de Libre Comercio
Slug: rechazan-visita-de-benjamin-netanyahu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Santos-y-Netanyahu-e1505428077206.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Canal de Youtube Israelí PM] 

###### [13 Sept. 2017] 

En una carta firmada por 16 organizaciones y 30 personas entre académicos y personalidades expresaron al presidente Juan Manuel Santos y a la sociedad civil las razones por las cuales rechazan la visita de Benjamín Netanyahu a Colombia e instaron al Gobierno nacional a **rechazar los crímenes cometidos por Israel y a reconocer a Palestina como un Estado.**

“Rechazamos la presencia de Benjamín Netanyahu en suelo colombiano. Como constructores de paz, condenamos los crímenes israelíes, así como las múltiples medidas para impedir un proceso de paz entre Israel y Palestina. **Instamos al gobierno colombiano a que, en derecho, condene los crímenes de guerra que Israel** comete de manera cotidiana y, también en derecho, que Colombia se sume al conjunto de países de la región y del mundo que reconocen a Palestina como un Estado”.

Además el movimiento BDS (Boicot, Desinversión y Sanción) ha manifestado que existen motivos para que Colombia rechace el régimen de Netanyahu, como reafirmar el Tratado de Libre Comercio - TLC – entre las dos naciones y la inversión en equipos y tecnología para guerra.

Actualmente solo falta que la Corte Constitucional diga si es o no constitucional el TLC con Israel, razón por la cual parlamentarios y **organizaciones sociales hacen un llamado  a la sociedad a hacer descargos a la Corte manifestando que ese tratado no debe ser ratificado.**

Óscar Vargas integrante de BDS asegura que Netanyahu ha llevado a Israel a un punto agudo de crisis con Palestina, acelerando la construcción de colonias dentro del territorio Palestino y disminuyendo sus derechos. Le puede interesar: [Primer ministro israelí investigado por corrupción](https://archivo.contagioradio.com/primer-ministro-investigado-corrupcion/)

“En general ha estructurado lo que **a nivel internacional se está reconociendo como un régimen de Apartheid.** Hoy en día los Palestinos están pidiéndole al mundo que no permitan que las visitas de Netanyahu pasen desapercibidas”. Le puede interesar: [TLC Colombia-Israel afecta derechos del pueblo Palestino](https://archivo.contagioradio.com/israel-palestina-tlc/)

### **50% de importaciones a Colombia desde Israel son para la Guerra** 

“No se supondría que en un escenario de posconflicto se esté consolidando una relación con un país que económicamente lo único que ha traído ha sido guerra. **Cerca del 50% de las importaciones que tenemos de Israel son armas,** aviones, equipos y tecnología para la guerra”. Le puede interesar: [¿Por qué Colombia no debe aprobar el TLC con Israel?](https://archivo.contagioradio.com/por-que-colombia-no-debe-aprobar-el-tlc-con-israel/)

Vargas asegura que la importación de tecnología de guerra podría representar un mayor control de la población **“parte de lo que nos preocupa es que esas tecnologías lo que van a hacer es garantizar más represión y control** de la oposición y a las libertades que tenemos en Colombia”.

### **Lo que se puede hacer en contra del régimen israelí** 

La comunidad Palestina en todo el mundo ha instado a la sociedad a **acompañar la lucha de ese pueblo a través de acciones simbólicas,** que envíen un mensaje de rechazo a Israel frente a sus políticas estatales en contra de los palestinos. Le puede interesar: [En Colombia preparan jornada de rechazo a visita de Primer Ministro israelí](https://archivo.contagioradio.com/rechazo-visita-netanyahu-colombia/)

En Colombia varias organizaciones han comenzado **campañas de Boicot contra empresas que apoyan el apartheid,** como el que ha iniciado recientemente organizaciones magisteriales sindicales contra Hewlett Packard, o contra la empresa de seguridad G4S.  Le puede interesar: [Exigen a empresa francesa terminar complicidad con apartheid Israelí](https://archivo.contagioradio.com/francesa-empresa-axa/)

<iframe id="audio_20863638" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20863638_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<article id="post-45288" class="post__holder post-45288 post type-post status-publish format-standard has-post-thumbnail hentry category-onda-palestina tag-bds tag-benjamin-netanyahu tag-israel tag-palestina cat-9327-id">
<div class="post_content">

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio.

</div>

</article>

