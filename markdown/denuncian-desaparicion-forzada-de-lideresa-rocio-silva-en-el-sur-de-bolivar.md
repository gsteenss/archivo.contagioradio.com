Title: Denuncian desaparición forzada de lideresa Rocío Silva en el Sur de Bolívar
Date: 2020-05-25 12:15
Author: CtgAdm
Category: Nacional
Slug: denuncian-desaparicion-forzada-de-lideresa-rocio-silva-en-el-sur-de-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/William-Vergel-JAC-.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Rocio-Silva-desaparecita-vallecito-bolivar.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 23 de mayo integrantes de la Junta de Acción Comunal del corregimiento de Vallecito al sur del Bolívar, denunciaron la desaparición forzada de **María Rocío Silva Caballero de 52 años**, integrante de la Junta y defensora de los proyectos de sustitución voluntaria de cultivos de uso ilícito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una denuncia interpuesta ante la Fiscalía por el compañero de **Rocío Silva** señala que, **sobre las 6 de la tarde del 21 de mayo** fue la última vez que la vieron, y ante la falta de respuestas por parte de vecinos y conocidos, emprendió junto a la JAC, la búsqueda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el documento señala que **ella mide 1,60cm, es de contextura robusta, piel blanca, cabello negro y el día de su desaparición vestía tenis rosados**; otras características señaladas son, dos cicatrices ubicadas en la parte baja del abdomen productos a dos cesáreas, así como algunas cicatrices en sus brazos y rostro.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Preocupa la presencia del ejército por los planes de erradicación forzada

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otro lado la JAC en un comunicado señala que desde el Plan de Defensa y Seguridad y el Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito **([P](https://archivo.contagioradio.com/suspension-de-audiencia-virtual-el-primer-paso-en-la-lucha-contra-el-glifosato/)NIS) presentado por Iván Duque tienen como meta erradicar aproximadamente 280.000 hectáreas de hoja de coca, en 14 departamentos, uno de ellos Bolívar**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Plan que según la Junta, ha aumentado la presencia del [Ejército Nacional](https://archivo.contagioradio.com/ejercito-queria-que-ariolfo-sanchez-fuera-un-falso-positivo-comunidad-de-anori-antioquia/)en el territorio en los últimos años, causando ***"preocupación en nuestra comunidad y demás corregimientos vecinos pues sabemos que esto atenta contra nuestra dignidad y violan nuestros derechos humanos".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregan que **existen antecedentes de hostigamiento y situaciones violentas contra la comunidad a manos del ejército nacional** *"diferentes brigadas del ejército, sin aviso, alguno se instalan en camino cercanos a nuestras fincas en reiteradas ocasiones realizan actos de erradicación a nuestros cultivos entre otras acciones".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La desaparición de Rocío Silva pudo ser a manos de miembros del ejército nacional"

<!-- /wp:heading -->

<!-- wp:paragraph -->

En entrevista con **William Vergel presidente de la junta de la JAC**, afirmó que el 21 de mayo día en el que desapareció Rocío Silva, miembros de la Juanta se encontraban reunidos conversando sobre la situación de seguridad de la zona debido a la presencia constante del Ejército Nacional.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Una de las vecinas Mónica fue a buscarla en su casa sobre las 6 de la tarde y ella no se encontraba la puerta de la casa estaba abierta su cédula también estaba allí y uno de los cajones donde guardaba el dinero estaba abierto, luego de ese día no la hemos vuelto a ver".*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Agregó también, *"**en la comunidad somos personas unidas pero también humildes y sabemos que el dinero que tienen nuestros vecinos es producto de su trabajo** o de lo que los envíen sus familiares desde otras zonas del país por eso respetamos y cuidamos lo que tienen los otros"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el contexto que vive el territorio, Vergel señala que **desde los años 90 el municipio de Vallecito se ha convertido en víctima del conflicto armado,** al estar ubicado en un punto estratégico para el transporte de mercancías ilegales y la explotación minera y acuífera; acción que ha convertido a los campesinos y campesinas no solo en víctimas del conflicto sino también del olvido estatal.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" La presencia de grupos armados legales e ilegales dentro del territorio y sus **constantes enfrentamientos por el control territorial provocó que la población civil quedará en medio y se transformará en víctimas de diferentes actos violentos masacres violaciones a sus derechos humanos"**.*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente, frente a la desaparición de Rocío dijo que han agotado todas las posibilidades de búsqueda menos la de buscar con la Fuerza pública qué los últimos días ha llegado masivamente a ejecutar acciones de erradicacion.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Creemos que este grupo estatal pudo haberla retenido, queremos hablar con ellos y verificar si efectivamente la habían cogido, muchas sospechas apuntan a que la tenga la fuerza pública ya tenemos una experiencia del asesinato de Álvaro Vallejo, así como de capturas injustificadas en la zona.*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Reiteró el llamado a la autoridades locales en el apoyo para la búsqueda de Rocio, señalando que ante la desaparición el Ejército no ha apoyado ninguna de las acciones de búsqueda. *(Le puede interesar:* <https://archivo.contagioradio.com/ejercito-queria-que-ariolfo-sanchez-fuera-un-falso-positivo-comunidad-de-anori-antioquia/>)

<!-- /wp:paragraph -->

<!-- wp:audio {"id":84711} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/William-Vergel-JAC-.mp3">
</audio>
  

<figcaption>
*Escuche la entrevista con William Vergel Ortiz | Pdte. JAC Vallecito*

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
