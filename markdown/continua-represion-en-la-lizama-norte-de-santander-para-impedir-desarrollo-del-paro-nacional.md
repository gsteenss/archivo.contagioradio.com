Title: En La Lizama, Norte de Santander impiden desarrollo del paro nacional
Date: 2016-05-31 14:30
Category: Movilización, Paro Nacional
Tags: La Lizama, Norte de Santander, Paro Nacional
Slug: continua-represion-en-la-lizama-norte-de-santander-para-impedir-desarrollo-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Lizama-Paro-Nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Edgar Pernet 

###### [31 May 2016] 

Desde el 28 de mayo 600 personas de diferentes organizaciones sociales se concentran en el punto conocido como La Lizama, en Norte de Santander, donde acampan desde las 8 de la noche, y donde también las fuerzas militares instalaron un retén.

El 30 de mayo, **hacia las 9 de la noche 30 efectivos del ESMAD arribaron al punto de concentración de la Minga en ese punto** y se ubicaron inicialmente cerca al centro de acopio, luego rodearon todo el sitio, indicando que tenían orden de desalojo y que procederían a sacar a las comunidades.

Sin embargo, se alertó sobre la situación a las organizaciones sociales y a la comunidad internacional  quienes lograron establecer un diálogo con las autoridades e impedir esa acción, señalando que la manifestación era una acción pacífica y se sigue se insistiendo en que se retiren los agentes del ESMAD.

Por otra parte, **desde ese punto del Norte de Santander también se denuncia que desde la Administración Municipal se busca impedir la llegada de ayuda humanitaria de emergencia,** que debían ser proveídas a las comunidades asentadas. Allí se requiere con extrema urgencia la provisión de agua potable pues de no ser así, se podrían generar  epidemias o enfermedades gastrointestinales, afectando la integridad de quienes ejercen su derecho a la movilización.

Cabe recordar que en esta zona del país, además de exigir la cumplimiento de los acuerdos establecidos en el paro anterior, **se movilizan en contra del extractivismo, el acceso a la tierra de los campesinos, las fumigaciones con glifosato y la erradicación manual**. Por esas reivindicaciones anuncian que seguirán el La Lizama de manera indefinida hasta que el gobierno de respuestas y soluciones concretas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
