Title: Moción de censura a Botero, una oportunidad para rediseñar política de defensa nacional
Date: 2019-11-05 13:05
Author: CtgAdm
Category: DDHH, Política
Tags: Guillermo Botero, Ministro de defensa, Moción de Censura, Senado
Slug: mocion-de-censura-botero-oportunidad-para-redisenar-politica-de-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Guillermo-Botero-e-Iván-Duque-e1572976002839.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @GuillermoBotero  
] 

Este martes se adelantará la audiencia de censura contra el ministro de defensa Guillermo Botero, el segundo de tres pasos que se deben dar para que el Congreso decida su salida del Ministerio. **Es la segunda vez que se intenta una moción de censura contra Botero, y es el tercer ministro de este Gobierno que pasa por este proceso,** sin embargo, senadores advirtieron que la discusión sobre lo que está pasando en materia de defensa nacional no puede reducirse a la acción de Botero, sino al Gobierno en general.

### **Las razones para pedir la salida de Botero** 

La moción de censura es una figura que está en poder del legislativo, y tiene como función principal hacer control político a altos funcionarios del Gobierno. Para hacer efectiva esta acción se debe cumplir con tres pasos: La realización de un debate de control político en que se acude a la moción, el desarrollo de una audiencia de censura y la sesión de votación, que se debe realizar en los próximos 10 días. (Le puede interesar: ["Las razones para pedir la salida de Guillermo B. del Ministerio de Defensa"](https://archivo.contagioradio.com/las-razones-para-pedir-la-salida-de-guillermo-botero-del-min-defensa/))

En entrevista con Contagio Radio, el senador **Roy Barreras explicó que entre las razones para pedir la salida de Botero está la situación actual de seguridad que vive el país** tanto en confrontación interna como externa: Por una parte, "estamos en riesgo constante de una confrontación militar indeseable" con Venezuela, y por otra parte, se evidencia el crecimiento de los grupos armados ilegales en zonas como Catatumbo, Chocó y Antioquia. (Le puede interesar:["Aumenta el control paramilitar en Chocó"](https://archivo.contagioradio.com/aumenta-control-paramilitar-en-choco/))

A ello se suma el tratamiento militar que ha dado Botero a temas que son de índole humanitaria, como la protesta social por parte de diferentes sectores sociales o la escaldada en la violencia registrada recientemente en [Cauca](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/). Por su parte, **el senador Iván Cepeda recordó algunos de los motivos que llevaron a la Cámara de Representantes a que en junio promovieron una moción contra Botero,** pero que finalmente se frenó por falta de apoyos en esa corporación.

Entre estas razones se encontraban las denuncias de unas directrices al interior del Ejército que promoverían la práctica de ejecutar a civiles y presentarlos como bajas en combate, las denuncias de nuevos casos de esos 'falsos positivos', su actuación para enfrentar el caso de Dimar Torres o los escándalos de corrupción al interior de las Fuerzas Militares. (Le puede interesar: ["Denuncian intención de destituir al General Villegas, quien pidió perdón por asesinato de Dimar Torres"](https://archivo.contagioradio.com/denuncian-intencion-de-destituir-al-general-villegas-quien-pidio-perdon-por-asesinato-de-dimar-torres/))

### **"Más allá de eso, consideramos que el problema es la política general del Gobierno"** 

La moción de censura contó con un apoyo inicial de 15 senadores que junto a Barreras consideraron necesaria la salida de Botero del cargo, por lo tanto, Cepeda afirmó que "si uno se atiene a la lógica, lo que debería ocurrir es que mayoritariamente asumamos (en el senado) la moción de censura y que el Ministro tenga que salir". No obstante, considera que "el problema es **la política general del Gobierno, que va en contra del proceso de paz, vuelve a las viejas políticas de supuesta seguridad de Uribe** y sigue manteniendo esos factores de corrupción en las fuerzas militares y policiales".

En ese sentido, manifestó que tienen suficientes elementos para señalar que Botero responde a una política general del Gobierno, y por lo tanto, tras su salida "puede llegar otro ministro que cometa las irregularidades". Por lo tanto, se debe revisar la política de Defensa de seguridad del Gobierno, porque es evidente su fracaso, hecho que se evidencia en la seguridad de territorios como Cauca, pero también en la evaluación que hizo la ciudadanía en las urnas de esta situación.

### **En contexto: Las cifras de una adminsitración ineficaz de defensa**

Según INDEPAZ, hasta el 31 de octubre de 2019 se registró el asesinato de 203 líderes sociales y solo durante el gobierno Duque se cuentan 83 integrantes de los pueblos indígenas que han sufrido este flagelo. Adicionalmente, el senador Roy Barreras, resaltó que el Gobierno Duque recibió el ELN con 1.500 hombres y ahora son 4.300, mientras las disidencias de FARC tenían 300 hombres, ahora se calcula que hay 1.300, no redujo la cantidad de hectáreas cosechadas con cultivos de uso ilícito y en conclusión, "después de 4 años de hablar de paz, llevamos 1 año hablando de una guerra con Venezuela, de las disidencias y del ELN".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
