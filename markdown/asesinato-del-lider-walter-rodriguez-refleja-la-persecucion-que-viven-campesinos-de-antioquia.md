Title: Asesinato del líder Walter Rodríguez refleja la persecución que viven campesinos de Antioquia
Date: 2019-11-20 17:06
Author: CtgAdm
Category: Entrevistas, Líderes sociales
Tags: Antioquia, asesinato de líderes sociales
Slug: asesinato-del-lider-walter-rodriguez-refleja-la-persecucion-que-viven-campesinos-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Walter.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

En la tarde del pasado 19 de noviembre en Tarazá Antioquia, se conoció el asesinato de Walter Enrique Rodríguez, de 48 años quien se desempeñaba como presidente de la Junta de Acción Comunal de San Miguel, en el corregimiento de La Caucana. El líder también hacía parte de la Asociación de Campesinos del Bajo Cauca (ASOCBAC) y fomentaba **la sustitución de cultivos de uso ilícito en la región. Entre 2017 y 2019, la cifra de líderes que han aportado a la sustitución y que  sido asesinados asciende a más de 63 personas. **

Según Óscar Yesid Zapata, integrante de la Coordinación Colombia-Europa-Estados Unidos (COEUROPA), Walter fue raptado desde San Miguel y trasladado a a vereda vecina La Esmeraldad, donde sus captores dispararon en varias ocasiones contra el líder. [(Lea también:En menos de tres días asesinan a tres líderes sociales en Antioquia)](https://archivo.contagioradio.com/tres-lideres-antioquia/)

### **La comunidad no fue escuchada por las autoridades ** 

Zapata reiteró que la región se encuentra en medio de una disputa entre los grupos paramilitares de Los Caparrapos y las autodenominadas Autodefensas Gaitanistas de Colombia; aclaró que aún se desconoce cual de los grupos armados pudo ser el responsable del asesinato de Walter, pero resaltó que la comunidad del Bajo Cauca ya había advertido el pasado 5 de noviembre sobre la presencia de estas estructuras en el lugar, alertando a Policía, Ejército y Alcaldía.

Según información aportada por el Observatorio de DD.HH. de la Fundación Sumapaz y la Corporación Jurídica Libertad, en 2019 se han registrado 22 homicidios contra defensores de derechos en el departamento, convirtiendo a Antioquia en el segundo lugar con más asesinato de este tipo después de Cauca, en Colombia. [(Lo invitamos a leer: Diofanor Montoya, líder que trabajaba por la tercera edad fue asesinado en Antioquia)](https://archivo.contagioradio.com/diofanor-montoya-lider-que-trabajaba-por-la-tercera-edad-fue-asesinado-en-antioquia/)

La Defensoria del Pueblo, en la alerta 026 del 2018, había alertado sobre el alto nivel de riesgo que tiene la Asociación Campesina del Bajo Cauca, que hace parte de Marcha Patriótica y que tiene medidas cautelares de la la Comisión Interamericana de Derechos Humanos , "**no vemos voluntad en el Gobierno ni del Ministerio Interior por construir políticas públicas que garantice nuestra vida e integridad"**.

Óscar Zapata resalta que ASOCBAC, organización con cerca de 2.500 integrantes, ha sido uno de los colectivos que más ha aportado en la reinvidicación del territorio para los campesinos al impulsar los programas de desarrollo de enfoque territorial junto al  Plan Nacional Integral de Sustitución (PNIS).

Además, de los homicidios ocurridos en Antioquia desde el 2018, al menos 90 han sido contra integrantes de esta organización lo que revela el alto impacto de la reconfiguración de la guerra en este territorio. [(Le puede interesar: Aumenta control por parte de grupos ilegales en Tarazá Antioquia)](https://archivo.contagioradio.com/enfrentamientos-de-grupos-armados-en-taraza/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
