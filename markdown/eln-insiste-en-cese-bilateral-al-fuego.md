Title: ELN insiste en cese bilateral al fuego
Date: 2017-04-17 17:50
Category: Nacional, Paz
Tags: ELN, mesa de conversaciones Quito, proceso de paz
Slug: eln-insiste-en-cese-bilateral-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Telegrafo] 

###### [17 Abr 2017] 

A través de un comunicado de prensa, el ELN denunció que el gobierno hizo su **"propia interpretación" del acuerdo base al que se llegó al finalizar el primer ciclo de conversaciones**. Según esa guerrilla el gobierno elude los temas del Derecho Internacional Humanitarios, como el asesinato de líderes sociales y solo centra sus pronunciamientos en el problema del secuestro.

El ELN también instó al gobierno a que se declare **prontamente el cese bilateral de las acciones armadas y no se mal interprete el marco del Derecho Internacional Humanitario que sería el referente común** para tratar, acordar y realizar las acciones y dinámicas humanitarias tendientes a desescalar la intensidad del conflicto. Le puede interesar:["Gobierno y ELN no pueden seguir escalando la guerra"](https://archivo.contagioradio.com/gobierno-y-eln-no-pueden-seguir-escalando-la-guerra/)

Frente al tema del secuestro, el ELN ha respondido que esta práctica se realiza “como forma de financiar sus actividades revolucionarias”, sin embargo, afirmó que está **abierto a “tratar, acordar e implementar acciones sobre todos los temas de la agenda”** y en especial sobre el punto 5.F de dinámicas y acciones humanitarias. Le puede interesar: ["Gobierno y ELN logran acuerdo humanitario de desminado"](https://archivo.contagioradio.com/desminado-humanitario-eln/)

En esa medida, ratificaron su compromiso con el proceso de paz y señalaron que sus esfuerzos en este segundo ciclo de conversaciones que inicia en mayo, **estarán encaminados a lograr acuerdos de pronta aplicación, que disminuyan las acciones de combate y que faciliten garantías mínimas para la participación social**.

###### Reciba toda la información de Contagio Radio en [[su correo]
