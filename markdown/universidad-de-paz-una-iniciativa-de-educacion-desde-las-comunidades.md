Title: Universidad de Paz, una iniciativa de educación desde las comunidades
Date: 2019-09-06 18:39
Author: CtgAdm
Category: Educación, Paz
Tags: Derecho Restaurativo, Justicia Sociambiental, Universidad de Paz
Slug: universidad-de-paz-una-iniciativa-de-educacion-desde-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Universidad-de-Paz.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Durante el **III Encuentro Internacional de Estudios Críticos de las Transiciones Políticas** celebrado en la Universidad de los Andes se presentó la iniciativa Universidad de Paz, propuesta que contempla un modelo de educación y justicia restaurativa que abrió sus puertas en medio de un compartir que unió en una misma mesa a  líderes y lideresas de comunidades indígenas, campesinas y afro, actores que alguna vez participaron en el conflicto, exmagistrados, ambientalistas,  y demás miembros de la sociedad alrededor del diálogo.

### "La Universidad de Paz debe rescatar la sabiduría popular enterrada por el conflicto"

Bajo una premisa que busca **"sacar a las universidades de las universidades"**, como señaló Alejandro Castillejo, profesor de Antropología, la Universidad de Paz surge para compartir un conocimiento profundo popular y tradicional albergado por las comunidades en el país y orientado a la justicia social ambiental.  [(Le puede interesar: Desde Cacarica llegó la Universidad de Paz a la Filbo)](https://archivo.contagioradio.com/desde-cacarica-llego-la-universidad-de-paz-a-la-filbo/)

El encuentro, que giró entorno a la reconciliación, el perdón y los aportes que cada uno de los participantes puede brindar a través de su historia personal reafirmó la intención de la Universidad de Paz de actos de reconocimiento y encuentros entre responsables y afectados, como una forma de aportar al trabajo que realizan entes como la Comisión de la Verdad y la Jurisdicción Especial para la Paz.

La universidad, un proyecto que nace de la Comisión Intereclesial de Justicia y Paz, es  **"un espacio intercultural que tiene de fondo la verdad, la armonía y el arraigo con el territorio" -** manifestó Luis Ulcué, líder indígena del Putumayo quien agregó que "es la oportunidad de volver a cree en nosotros y recobrar nuestra confianza". [(Lea también: Lanzan propuesta de Universidades de Paz)](https://archivo.contagioradio.com/lanzan-propuesta-de-universidades-de-paz/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
