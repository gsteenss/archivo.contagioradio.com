Title: Familia pide que asesinato de Lucy Chamorro sea considerado feminicidio
Date: 2019-03-01 15:31
Category: DDHH, Mujer
Tags: feminicidio, nariño, pasto, Rosa Elvira Cely
Slug: familia-pide-asesinato-lucy-chamorro-sea-considerado-feminicidio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Lucy.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Suministrada] 

###### [28 Feb 2019] 

El feminicidio de Lucy del Socorro Chamorro Landázuri ha desconcertado la población de Pasto, Nariño. La mujer de 51 años fue encontrada en su residencia el 12 de febrero amordazada y sufriendo de un hematoma en la cabeza, producto de las agresiones generadas por un hombre que ingresó a su vivienda. Chomorro fue llevada al hospital donde murió por la gravedad de sus heridas.

La familia de la víctima dio a conocer que en horas de la tarde, el responsable del crimen entró al apartamento de Chamorro, en el barrio El Recuerdo, y ahí la agredió físicamente y sexualmente. A las 6 de la tarde, su esposo la encontró desnuda, amordazada y amarrada de pies y manos.

"En este momento todos estamos consternados, lidiando con la pérdida de nuestra familiar, en tanto que la manera como se dieron las cosas fue muy violenta," manifestó un familiar de la víctima y añadió que "Lucy fue una amorosa madre, excelente tía, entregada hermana y ejemplar hija. **Se nos fue una gran persona y mañana puede ser cualquiera**."

Frente a los hechos, afirmó que la familia está pidiendo que e**l homicidio de Chamorro sea catalogado como feminicidio, bajo la Ley 1761 de 2015**, que crea el tipo penal. Las penas por este crimen varían entre 250 y 500 meses de prisión. En ese sentido, el familiar expresa que"estamos esperando que siga el proceso judicial y confiamos que la justicia actúa de manera ejemplar y que se impute el delito de feminicidio".

Actualmente, **Robert Andrés Pupiales Pupiales, de 30 años**, está capturado como el presunto responsable de este hecho, y fue imputado por los delitos de homicidio agravado en concurso heterogéneo con acceso carnal violento y hurto calificado y agravado. La Fiscalía también dio a conocer que Pupiales es **investigado por otros dos casos de abuso sexual.**

La investigación determinó que el imputado usó el mismo modus operandi para entrar a las residencias de tres mujeres, una de ellas Chamorro, en el mes de febrero. Aparentemente, se identificaba como funcionario del Sena, para así abordar a sus víctimas e ingresar a sus residencias con el propósito de hurtar sus objetos de valor y abusar sexualmente de ellas. **Las otra dos víctimas sobrevivieron el ataque y una de ellas lo identificó a través de reconocimiento fotográfico**.

"Todos estamos desconcertados en la ciudad, uno piensa que esto no pasa," afirmó el familiar. "**Esta es una amenaza muy fuerte a las mujeres de Pasto y esperamos que se vea justicia y que no sucedan más este tipo de casos**." Agregó que están haciendo un llamado al público que si existen más víctimas que denuncien los crímenes. Dio a conocer que el investigado también vivió en Villavicencio, Meta; Ipiales, Nariño; y en ciudades de Ecuador.

### **Feminicidios en Nariño** 

Según un informe de la Fundación Desarrollo y Paz, las denuncias de violencia de género aumentaron en el 2017, **incrementando de 2.581 casos en 2016 a 3.133 en 2017**, con Pasto, Tumaco, Ipiales y Barbacoas los municipios en el departamento más afectadas.

En abril de 2018, fue denunciado que ninguno de **los 17 casos de asesinato de mujeres cometidos ese año habían sido catalogados como feminicidio,** ni se había realizado imputación de cargos de los presuntos responsables.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
