Title: Teatro en solidaridad con los líderes sociales
Date: 2018-07-19 15:10
Category: eventos
Tags: teatro, víctimas
Slug: la-caida-de-las-aguilas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/La-Caida-de-las-Aguilas-22-e1532030730724.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Casa Tea 

###### 19 Jul 2018 

En solidaridad con los líderes sociales del país, amenazados y asesinados de forma sistemática, el teatro estudio Alcaraván, presentará su obra **"La caída de las águilas"** en temporada **los días 19, 20 y 21 de julio**, con entrada libre.

La creación colectiva con la dirección y textos de Paola Guarnizo, **esta inspirada en la masacre de "El salado"**, ocurrida entre el 16 y 19 de febrero del año 2000, en la que se estima por lo menos 100 personas fueron víctimas de violación, decapitación y desmembramiento por paramilitares.

"Lo que me pareció también importante era cómo hablar de esa masacre haciéndole una metáfora a la cantidad de víctimas del conflicto armado en Colombia, que hay de todos los bandos, de todos los lados, y **me parecía importante que desde el arte y las nuevas dramaturgias jóvenes comenzáramos a hablar de como nosotros vemos la realidad el país** que es muy dolorosa pero que necesita también invitar a la memoria" asegura la directora.

En la Caída de las águilas, la historia de Ezequiel, **un joven perseguido por la guerra que debe decidir en que bando del conflicto situarse**, sirve para representar el rostro de las víctimas en un contexto des-humanización, donde la falta de opciones se convierte en el caldo de cultivo para generar más violencia.

El reparto cuenta con la actuación especial de Álvaro Rodríguez, reconocido actor de teatro, cine y televisión, quien apunta que con esta clase de montajes artísticos "no pretendemos solucionar nada, pero nos **ayuda a sensibilizar y preguntarnos sobre los acontecimientos del país** y hacer una especie de exorcismo para que esas cosas no se repitan".

La obra se presentará en el escenario de Casa Tea, ubicado en la **Calle 19 \#4-71 Centro Comercial Los Ángeles, local 405**, a partir de las 7:30 de la noche, el aporte de los asistentes es voluntario.
