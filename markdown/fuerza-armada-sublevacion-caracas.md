Title: Fuerza Armada Bolivariana controló sublevación militar en Caracas
Date: 2019-01-21 15:43
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: GNV, Maduro, Venezuela
Slug: fuerza-armada-sublevacion-caracas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxcL8KzWoAAJsjs-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### 21 Ene 2019 

Aproximadamente a las **2:50 de la madrugada de este lunes**, un grupo de militares adscritos a la Guardia Nacional Bolivariana realizó un **intento de levantamiento en la barriada caraqueña de Cotiza**, acción que fue conjurada por la Fuerza Armada Bolivariana.

Los uniformados que serían 40 según reportes de prensa, **llamaron a desconocer el gobierno de Nicolás Maduro pidiendo a los ciudadanos a respaldarlos en las calles.** Según el reporte oficial habrían robado un lote de armas en el destacamento de Petare.

El comunicado del Ministerio de defensa, asegura que **la situación fue controlada cerca de las 7:30 a.m.** en la misma sede de la unidad, donde los militares fueron "rendidos y capturados" y se recupero el armamento hurtado; mientras que **en inmediaciones del comando se produjeron algunas manifestaciones focalizadas** por parte de algunos habitantes.

A continuación el comunicado completo emitido sobre la acción.

LA FUERZA ARMADA NACIONAL BOLIVARIANA, INFORMA AL PUEBLO DE VENEZUELA, QUE EN LA MADRUGADA DEL 21 DE ENERO DE 2019, APROXIMADAMENTE A LA 02:50 AM, UN REDUCIDO GRUPO DE ASALTANTES ADSCRITOS AL COMANDO DE ZONA NRO 43 DE LA GUARDIA NACIONAL BOLIVARIANA, TRAICIONANDO SU JURAMENTO DE FIDELIDAD A LA PATRIA Y SUS INSTITUCIONES, SOMETIÓ AL CAP. GERSON SOTO MARTÍNEZ, COMANDANTE DEL PUESTO DE COORDINACIÓN POLICIAL MACARAO, DESDE DONDE SE DESPLAZARON EN DOS (02) VEHÍCULOS MILITARES; LUEGO IRRUMPIERON CONTRA LA SEDE DEL DESTACAMENTO DE SEGURIDAD URBANA UBICADO EN PETARE, MUNICIPIO SUCRE, SUSTRAYENDO DE ALLÍ UN LOTE DE ARMAS DE GUERRA Y SECUESTRANDO BAJO AMENAZA DE MUERTE, A DOS (02) OFICIALES Y DOS (02) GUARDIAS NACIONALES DEL REFERIDO DESTACAMENTO.

LOS DELINCUENTES FUERON RENDIDOS Y CAPTURADOS EN LA SEDE DE LA UNIDAD ESPECIAL DE SEGURIDAD WARAIRA REPANO, EN COTIZA, MUNICIPIO LIBERTADOR, TAMBIÉN ADSCRITA A LA GUARDIA NACIONAL BOLIVARIANA; A DONDE SE HABÍAN DIRIGIDO, ENCONTRANDO FIRME RESISTENCIA POR PARTE DE LOS OFICIALES Y TROPAS PROFESIONALES ALLÍ ACANTONADOS. CABE DESTACAR, QUE DURANTE LA DETENCIÓN SE LOGRÓ RECUPERAR EL ARMAMENTO ROBADO Y EN LOS ACTUALES MOMENTOS SE ENCUENTRAN BRINDANDO INFORMACIÓN DE INTERÉS A LOS ORGANISMOS DE INTELIGENCIA Y AL SISTEMA DE JUSTICIA MILITAR. A ESTOS SUJETOS SE LES APLICARÁ TODO EL PESO DE LA LEY.

LA FUERZA ARMADA NACIONAL BOLIVARIANA RECHAZA CATEGÓRICAMENTE ESTE TIPO DE ACTOS, QUE CON TODA SEGURIDAD ESTÁN MOTIVADOS POR OSCUROS INTERESES DE LA EXTREMA DERECHA Y SON CONTRARIOS A LAS NORMAS ELEMENTALES DE LA DISCIPLINA MILITAR, AL HONOR Y A LAS TRADICIONES DE NUESTRA INSTITUCIÓN. EN TAL SENTIDO, RATIFICA ANTE LA COLECTIVIDAD, QUE TODAS SUS UNIDADES OPERATIVAS, DEPENDENCIAS ADMINISTRATIVAS E INSTITUTOS EDUCATIVOS, SE ENCUENTRAN FUNCIONANDO BAJO COMPLETA Y ABSOLUTA NORMALIDAD. ASÍ MISMO RATIFICAMOS NUESTRO IRRESTRICTO APEGO A LA CONSTITUCIÓN Y LAS LEYES DE LA REPÚBLICA.
