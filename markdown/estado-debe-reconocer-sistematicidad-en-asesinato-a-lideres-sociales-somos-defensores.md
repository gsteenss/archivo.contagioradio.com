Title: Estado debe reconocer sistematicidad en asesinato a líderes sociales: Somos Defensores
Date: 2018-07-05 17:20
Category: DDHH, Nacional
Tags: Guillermo Rivera, lideres sociales, Ministerio del Interior, Somos defensores
Slug: estado-debe-reconocer-sistematicidad-en-asesinato-a-lideres-sociales-somos-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/lideres-asesonados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 Jul 2018] 

Tras las afirmaciones del ministro del interior Guillermo Rivera al insistir en que los asesinatos de líderes sociales no son sistemáticos, la integrante de la organización Somos Defensores, Diana Sánchez, reitero la importancia de reconocer la sistematicidad en los hechos de violencia que se vienen presentando en el país y a que las instituciones **gubernamentales reconozcan su responsabilidad en estos actos por omisión y falta de investigación**.

### **"Si hay sistematicidad en asesinato de líderes sociales" ** 

Para Diana Sánchez los asesinatos si son sistemáticos por varias características, la primera de ellas es que las personas asesinadas cumplen con un perfil de líder social, que ha generado procesos de base y empoderamiento en el territorio, que ha hecho denuncias frente a las violaciones **de derechos humanos y que tiene una visibilización en los escenarios públicos. **

El segundo elemento tiene que ver con el crecimiento en las cifras de asesinatos de líderes sociales que, según Sánchez, han venido en aumento y el tercer factor está relacionado con **el patrón que se ha podido establecer sobre las formas cómo son asesinadas las personas**, usualmente por sicarios, con armas de fuego, en cercanías a lugares que frecuentan como sus viviendas o trabajos.

En ese sentido recordó que en el genocidio a la Unión Patriótica también se le pidió al Estado que reconociera la sistematicidad contra los integrantes de ese partido político, para activar medidas de protección hacia los mismos; hecho que no sucedió y que solo hasta varios años después se logró el reconocimiento de los patrones detrás de los asesinatos que provocaron el exterminio. (Le puede interesar: ["Tres miembros de la Colombia Humana han sido asesinados en medos de dos meses")](https://archivo.contagioradio.com/tres-miembros-de-la-colombia-humana-han-sido-asesinados-en-menos-de-dos-meses/)

### **La falta de acciones por parte de las instituciones** 

Si bien Diana Sánchez reconoce que hay un avance en las investigaciones desde la Fiscalía en 170 casos, siguen siendo muy pocos para el volúmen de asesinatos de líderes sociales que de acuerdo con la cifra de Somos Defensores, **en los dos periodos presidenciales de Santos sumaron un total de 580 casos de homicidios.**

Asimismo, manifestó que, aunque se haya encontrado a los autores materiales del 50% de esas 170 investigaciones que adelanta la Fiscalía, **se desconoce quiénes son los autores intelectuales o los intereses políticos o económicos** que estaban detrás del asesinato de los líderes.

De igual forma, para Sánchez, la expedición de decretos que buscan la protección de los líderes a través de diferentes estrategias como los cambios en la Unidad Nacional de Protección o la conformación de medidas colectivas, se convierten en “saludos a la bandera” si no hay articulación con la realidad de las y los líderes en donde hay una ausencia del Estado.

Finalmente, sobre el decreto 660 de 2018, expedido por el Ministerio del Interior, Sánchez afirmó que, pese a que este busca la protección de los líderes e incluye recomendaciones hechas por organizaciones defensoras de derechos humanos, esta disposición solo entrará en vigencia hasta que así lo decida el gobierno entrante y además, puede ser modificado por otros decretos. (Le puede interesar: ["Ministerio del Interior propuso mecanismo de protección colectivo para líderes sociales"](https://archivo.contagioradio.com/ministerio-del-interior-propuso-mecanismo-de-proteccion-colectiva-para-lideres-sociales/))

<iframe id="audio_26914425" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26914425_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
