Title: Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU
Date: 2020-07-02 20:45
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Abuso sexual, Abuso sexual por Ejército, Derechos Humanos, Fuerzas militares
Slug: hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/General-Zapateiro.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Fuerzas-Militares.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Fuerzas Militares / CGFM

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras las revelaciones en torno a los **casos de abuso sexual por parte de miembros activos de las Fuerzas Militares – FF.MM**, se evidencia que tanto los crímenes sexuales como otras conductas delictivas hacen parte de un contexto que amerita una revisión de las condiciones institucionales que han permitido que este tipo de comportamientos y conductas se presenten. (Le puede interesar: [Agresión sexual del Ejército contra menor embera es un ataque a los 115 pueblos indígenas del país: ONIC)](https://archivo.contagioradio.com/agresion-sexual-del-ejercito-contra-menor-embera-es-un-ataque-a-los-115-pueblos-indigenas-del-pais-onic/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alberto Yepes, abogado e integrante de la Coordinación Colombia Europa Estados Unidos - [CCEEU](https://www.google.com/search?q=cceeu&oq=cceeu&aqs=chrome.0.69i59j0l4j5l2.4775j0j4&sourceid=chrome&ie=UTF-8), plataforma que agrupa más de 200 organizaciones de DD.HH., asegura que **la impunidad, las directrices por parte de los altos mandos y las políticas del Gobierno han permitido que las prácticas delictivas al interior de las FF.MM. se conviertan en acciones reiteradas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El defensor declaró que los abusos sexuales y otras vulneraciones de derechos «*son hechos que se han vuelto repetitivos y cada vez involucran a un número mayor de integrantes de la Fuerza Pública, vienen de tiempo atrás y no pueden ser considerados como hechos aislados*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También, que existe un «ambiente institucional deteriorado» que ha propiciado un **incremento de abusos contra los derechos humanos en el actual Gobierno**, los cuales tienen que ver no solamente con el tema de violencia sexual, sino con «*interceptaciones ilegales, persecución a defensores, periodistas y opositores políticos, la reanudación de ejecuciones extrajudiciales y el bombardeo de niños*».

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Revisión de condiciones institucionales** que propician conductas delictivas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Uno de los principales problemas para que crezcan los casos de actividades delictivas de la Fuerza Pública tiene que ver con **la impunidad que supera el 90%**. En ese sentido, Alberto Yepes aseguró que la laxitud con la que se investigan estos casos se debe a que muchas veces son asumidos por otros militares en las oficinas de control interno, ya que, la Procuraduría y la Fiscalía «*han renunciado a ejercer su poder preferente en las investigaciones*». [Vea Video: El enemigo interno](https://archivo.contagioradio.com/video-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo, según lo dicho por el comandante del Ejército, el general Enrique Zapateiro, **de los 118 integrantes que se han visto involucrados en casos de abuso sexual contra menores, solo 45 fueron retirados de la institución** y los 73 restantes están siendo investigados, dando a entender que aún permanecen ejerciendo sus funciones. (Lea también: [Anhelo de justicia](https://archivo.contagioradio.com/anhelo-de-justicia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, las **directrices impartidas por los altos mandos**, cuya estrategia operacional se centra en duplicar los resultados de bajas en combate, lo que ha sido catalogado como «órdenes de letalidad» y propició en su momento el episodio de ejecuciones extrajudiciales, en el que alrededor de 5.000 civiles fueron asesinados por militares que los presentaron como delincuentes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, **la política gubernamental de seguridad del presidente Duque**, que según Alberto Yepes, ha creado un ambiente institucional que ha permitido un aumento de abusos generalizados en contra de los derechos humanos de los ciudadanos. Ejemplo de ello, son las interceptaciones y seguimientos ilegales efectuados por el Ejército, dados a conoces este año, en los que la mayoría de sus objetivos fueron periodistas, políticos opositores, ONG’s y sindicalistas que integraron una lista de más de 130 víctimas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***Más allá de las responsabilidades individuales, es importante que el Ministerio de Defensa y las FF.MM. se tomen el trabajo de investigar cuáles fueron las condiciones institucionales que han permitido este tipo de comportamientos y conductas»***
>
> <cite>Alberto Yepes, director de la Coordinación Colombia Europa Estados Unidos (CCEEU)</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Sistematicidad en las conductas de las Fuerzas Militares

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pese al citado panorama, el general Zapateiro se sostuvo en decir que el actuar de los integrantes del Ejército **«No es una conducta sistemática, son conductas individuales»**, cuando fue indagado por este tema.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Decirles que en un Ejército, tan grande, de 240.000 hombres, no sucedan este tipo de hechos sería yo estar aquí prometiéndoles lo que no se puede cumplir»**
>
> <cite>Gral. Eduardo Zapateiro, comandante del Ejército Nacional en rueda de prensa.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por su parte, Alberto Yepes, aseguró que el proceder de la Fuerza Pública y su implicación en hechos delictivos y vulneratorios de los derechos humanos es un actuar repetitivo que se agudiza por la falta de efectividad de los entes de control en sus investigaciones y sanciones.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Todo este panorama lo que pone de presente es  una necesidad de que haya una reestructuración profunda de las Fuerzas Armadas en el país;  direccionadas  a los DD.HH y al respeto y las garantías de los ciudadanos. Unas Fuerzas Armadas para la paz**»
>
> <cite>Alberto Yepes, director de la Coordinación Colombia Europa Estados Unidos (CCEEU)</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
