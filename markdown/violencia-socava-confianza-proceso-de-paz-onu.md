Title: Ola de violencia “socava la confianza” en la paz: ONU
Date: 2016-11-21 17:32
Category: DDHH, Nacional
Slug: violencia-socava-confianza-proceso-de-paz-onu
Status: published

###### [Foto: ONU] 

###### [21 Nov 2016]

La oficina de las Naciones Unidas para los Derechos Humanos, ONU, expresó su preocupación por la oleada de asesinatos, y aseguró que entienden la desconfianza que esa situación suscita en los sectores sociales y campesinos del país. Además anunciaron que se **encuentran en terreno haciendo verificación de las situaciones y realizará un informe una vez se recopile la información.**

Esa oficina saludó el anuncio del presidente Santos de convocar una comisión de alto nivel para investigar los 3 asesinatos y 2 atentados que se han presentado en las últimas horas en Colombia y recordó que el **acuerdo final establece una serie de medidas concernientes a la protección de los líderes sociales y sectores de víctimas más afectados por el conflict**o.

Además reiteraron su compromiso con el acompañamiento a los esfuerzos de construcción de paz, así como la p**rotección a la población civil como respuesta a las expectativas de las comunidades.** (Le puede interesar: [Colombia no avanza en protección de DDHH](https://archivo.contagioradio.com/colombia-no-avanza-en-garantia-de-ddhh-onu/))

Este es el comunicado...

##### *"Comunicado de la ONU en Colombia sobre violencia y asesinatos en zonas afectadas por el conflicto* 

##### *Bogotá, 21 noviembre - El Equipo de País y la Misión de las Naciones Unidas en Colombia manifiestan su preocupación con acciones violentas, incluyendo recientes asesinatos, dirigidos a líderes sociales en varias zonas del país afectadas por el conflicto armado.*

##### *Las Naciones Unidas entienden el temor de las organizaciones sociales afectadas y su reclamo para que se tomen urgentemente las medidas necesarias para evitar el recrudecimiento de la violencia, que socava la confianza en las perspectivas de una paz estable y duradera, ante la expectativa de la firma del Acuerdo Final entre el Gobierno de Colombia y las FARC-EP.*

##### *Apoyamos la determinación del Presidente Juan Manuel Santos de convocar una reunión de alto nivel de la Comisión Nacional de Derechos Humanos y la decisión del Fiscal General de adelantar las investigaciones sobre los últimos hechos presentados. Esperamos que la celeridad en las investigaciones, como lo ha solicitado la Defensoría del Pueblo, y las medidas que puedan tomar los diferentes organismos del Estado, contribuyan a la protección de la población civil y a generar mayor tranquilidad en las comunidades.*

##### *El Acuerdo Final de Paz prevé la implementación de varias medidas dirigidas precisamente a garantizar la seguridad de líderes y lideresas de organizaciones sociales y defensores de derechos humanos, así como la seguridad para el ejercicio de la política, en particular mediante la instalación de la Comisión Nacional de Garantías de Seguridad.*

##### *En este momento la Oficina del Alto Comisionado de Naciones Unidas para los Derechos Humanos se encuentra adelantando una serie de misiones de verificación en terreno, tras lo cual se informará a la opinión pública de los resultados de las mismas.*

##### *Las Agencias, Fondos y Programas de las Naciones Unidas así como la Misión de la ONU en Colombia reiteran su compromiso con la protección de la población civil, así como, en el marco de sus respectivos mandatos, su continuo acompañamiento de los esfuerzos de construcción de paz y respuesta a las expectativas de la población colombiana, particularmente, en los territorios más afectados por el conflicto."*

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)
