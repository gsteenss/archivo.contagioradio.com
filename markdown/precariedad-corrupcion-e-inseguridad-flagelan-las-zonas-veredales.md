Title: Precariedad, corrupción e inseguridad flagelan las zonas veredales
Date: 2017-03-10 18:25
Category: Nacional, Paz
Tags: paz, Zonas Veredales, Zonas Veredales Transitorias de Normalización, ZVTN
Slug: precariedad-corrupcion-e-inseguridad-flagelan-las-zonas-veredales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/zona-veredal-farc-e1484067202705.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [10 Mar. 2017] 

Las denuncias sobre los **incumplimientos del Gobierno nacional a los Acuerdos de Paz continúan conociéndose**, además de las precarias condiciones en las que se encuentran las Zonas Veredales Transitorias de Normalización (ZVTN), donde están los integrantes de las FARC, se ha conocido de la **presencia de las Fuerzas Armadas muy cerca de dichas Zonas lo que según esta guerrilla estaría violando los protocolos.**

Así mismo, se han conocido los altos costos de los víveres, la nula atención para las madres gestantes y lactantes, así como para sus bebés, y la falta de materiales para continuar con las construcciones de las viviendas. Le puede interesar: [Duro reclamo de las FARC al gobierno por incumplimientos al acuerdo](https://archivo.contagioradio.com/duro-reclamo-de-las-farc-al-gobierno-por-incumplimientos-al-acuerdo/)

En una reciente rueda de prensa, la delegación de las FARC aseguró que **hay un 80% de incumplimientos por parte del Gobierno a las Zonas Veredales.** Estas son las situaciones más recientes acontecidas en algunas de las Zonas en las que se encuentran los integrantes de esta guerrilla.

### **TUMACO** 

A través de un comunicado, los voceros de la guerrilla de las FARC que se encuentran en la Zona de Normalización Ariel Aldana – La Variante **aseguraron que 3 lanchas de la Armada Nacional se ubicaron “a la orilla del rio y embarcaron elementos que no pudieron ser identificados”.**

Este hecho estaría dándose una violación a los protocolos, dado que según lo acordado alrededor de las ZVTN se tiene un sector de seguridad ubicado a 1 km y de esta manera evitar incidentes entre fuerzas militares e integrantes de las FARC. Le puede interesar: [Monterredondo: Contraste entre comodidad militar y precariedad guerrillera](https://archivo.contagioradio.com/monterredondo-contraste-entre-comodidad-militar-y-precariedad-guerrillera/)

### **ICONONZO** 

En esta ZVTN (Antonio Nariño) viven en la actualidad 345 integrantes de la FARC, de los cuales 39 se encuentran enfermos, incluyendo una madre gestante que tienen preclancia. Según la denuncia de varias organizaciones sociales de la región que han hecho veeduría, en el lugar hay una ausencia de condiciones mínimas para que las personas puedan vivir.

Según la presidenta del sindicato agrario de Icononzo “**los hijos de las guerrilleras están durmiendo en unos cambuches en mal estado, condiciones que evidencian la negligencia del gobierno** en cuanto a la satisfacción de los derechos de los niños, niñas y adolescentes”. Le puede interesar: [Hay un 80% de incumplimientos por parte de Gobierno en Zonas veredales: FARC](https://archivo.contagioradio.com/gobierno-ha-incumplido-en-un-80/)

### **CAQUETÁ** 

Jhon Chavarro defensor de DD.HH. en Caquetá manifestó que el pasado 8 de Marzo mientras participaban en una actividad conmemorativa del Día Internacional de la Mujer **en la ZVTN de Agua Bonita en el Municipio de Montañita se observó a varios hombres del Ejército Nacional grabando a quienes se encontraban presentes en el lugar.**

“Con cámaras de teléfono celular y con cámaras que traen incorporadas en sus cascos observamos como miembros de la Fuerza Pública comenzó a grabar. Toman fotografías a las placas de las motos”.

Pese a ello, las personas que decidieron acompañar esta conmemoración en la ZVTN estuvieron en el evento. Sin embargo, al salir de dicha zona el defensor de DD.HH. **Jhon Chavarro fue víctima de un comportamiento irregular por parte de los militares.**

“A las 8 p.m. cuando salgo de la ZVTN los militares me hacen el pare y me interrogan. Se me pregunta por 4 ocasiones que si tengo familia guerrillera. Luego me piden mi nombre, documentos. Me hacen el registro y esto lo hice de público conocimiento, teniendo en cuenta el alto número de asesinatos a líderes que ha habido en el país” concluyó Chavarro

### **LOS PRECIOS DE LOS VIVERES PARA LAS ZVTN ESTÁN POR LAS NUBES** 

Adicional a todas estas situaciones, ahora se suma una más que pareciera de no creer, los precios que son cobrados al Estado por parte de los proveedores que este contrato son mucho más altos que el promedio de los precios del mercado.

Esta denuncia fue hecha en una rueda de prensa por Pastor Alape, Marcos Calarca y Carlos Antonio Lozada, quienes aseguraron que **los productos que ellos compraban mientras se encontraban en guerra eran mucho más bajos.**

En la factura que fue dada a conocer por la Revista Semana se pueden detallar los altos precios de los productos, **una libra de café de 500 gramos, por ejemplo, está en \$14 mil pesos y un condimento trisalsina de 200 gramos está en \$25 mil.**

**Imágen de la factura**

![Imagen Factura](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Imagen-Factura.jpg){.alignnone .size-full .wp-image-37594 width="964" height="2094"}

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
