Title: Campesinos encadenados marchan para exigir la preservación de la Laguna de Fúquene
Date: 2016-04-25 15:50
Category: Movilización, Nacional
Tags: campesinos encadenados marchan, CAR, laguna de fuquene
Slug: campesinos-encadenados-marchan-para-exigir-la-preservacion-de-la-laguna-de-fuquene
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Laguna-Fuquene.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Prensa Rural ] 

###### [25 Abril 2016]

Desde el pasado viernes, campesinos de Chiquinquirá se movilizan encadenados hasta la ciudad de Bogotá, para exigir a la 'Corporación Autónoma Regional de Cundinamarca' la preservación de la Laguna de Fúquene. Según denuncian por cuenta del mal manejo del reservorio, los **pobladores deben consumir agua no potable y varios municipios llevan más de 30 días sin agua**.

El pasado 17 de abril en el centro de Chiquinquirá, comunidades campesinas de Simijaca, Susa, Carmen de Carupa, Cucunuba, Guacheta y San Miguel de Sema se movilizaron para dar a conocer el **deterioro ambiental de la Laguna desde hace más de 20 años**, insistiendo en la necesidad de revisar la inversión presupuestal que se ha destinado para su manejo pues la CAR no ha registrado evidencias de avances en la materia.

Este lunes los campesinos se manifestaron frente a las instalaciones de la CAR en Bogotá, para culminar en la Plaza de Bolívar.

Noticia en desarrollo...

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio}
