Title: Tras un año de detención injusta 15 campesinos vuelven a la libertad
Date: 2015-12-22 17:28
Category: Nacional
Tags: Falsos positivos extrajudiciales, Sumapaz
Slug: tras-un-ano-de-detencion-injusta-15-campesinos-vuelven-a-la-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/campesinos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: J Santacruz 

<iframe src="http://www.ivoox.com/player_ek_9818015_2_1.html?data=mp2empWVeY6ZmKiak5qJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmNPV1JDi0JDFaaSnhqee0ZDIqYzYxtnS0MjNaaSnhqeg0JDNssvp1NnOjZaZb8TVztXS1c7Ss9Sf19rSztvJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alexander Guzmán] 

###### [22 Dic 2015 ]

“Ojalá que cese la horrible noche”, es la esperanza que tiene Alexander Guzmán, uno de los 15 campesinos de la localidad 20 de Sumapaz, detenidos durante un año  y que hoy se encuentra en libertad condicional luego de que el juzgado octavo de Paloquemao tomará esa decisión por vencimiento de términos.

El proceso contra Alexander y sus 14 compañeros no acaba. Señalados como colaboradores de la guerrilla, tuvieron que vivir desde el 17 de diciembre del 2014 toda una pesadilla que afectó la estabilidad de sus familias, al volverse, junto a sus compañeros un nuevo caso de falsos positivos judiciales.

**Alexander vive con su familia, su hijo menor de edad, y además dos hermanos con discapacidad mental por los que debe responder.** Sin embargo, eso no le importó a las autoridades que arrestaron a Guzmán por el delito de rebelión, pese a que él asegura no tener ningún tipo de vínculo con la guerrilla de las FARC, pues se trata de un simple campesino que vive de lo que le da la tierra.

“Han sido dificultades para la familia enormes” cuenta el líder campesino, quien relata que el primer mes los estuvieron en la estación de la Policía de Funza donde tenían que alimentarse al lado del baño, **“fue un trauma muy duro al inicio",** dice. Luego pasó a la cárcel modelo en un patio temporal durante 5 días, y finalmente estuvo en reclusión domiciliaria, negándosele el derecho al trabajo, razón por la que no tuvo cómo sostener a su familia y su hijo debió irse a vivir con su abuela.

Con esta experiencia, **Alexander asegura haber sentido “en vivo y en directo las injusticias del Estado contra el pueblo colombiano”**, ya que mientras el gobierno de Santos habla de paz en la Habana sigue estigmatizando y reprimiendo a los sectores populares del país, pues, como lo cuenta Guzmán, a quienes tuvieron retenidos durante un año, no eran más que agricultores, líderes sociales e incluso uno de ellos trabajaba en Bogotá para la empresa de transporte público Transmilenio.

Por el momento, estas personas deberán esperar el 15 de febrero, fecha para la que se tiene citada una nueva audiencia.
