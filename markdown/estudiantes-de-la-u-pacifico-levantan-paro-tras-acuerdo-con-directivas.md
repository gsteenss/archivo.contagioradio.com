Title: Estudiantes de la U. Pacífico levantan paro tras acuerdo con Directivas
Date: 2017-03-30 11:59
Category: Educación, Nacional
Tags: Movimiento estudiantil, Universidad Del Pacífico
Slug: estudiantes-de-la-u-pacifico-levantan-paro-tras-acuerdo-con-directivas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Universidad-del-Pacífico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [30 Mar 2017] 

Los estudiantes de la Universidad del Pacífico Colombiano llegaron a un acuerdo con las directivas de la Institución, para levantar el plantón que mantenían desde hace dos semanas, entre los pactos se encuentra la **re apertura del restaurante escolar, con el aumento a 300 almuerzos, la creación de dos rutas diurnas y la adecuación de los baños de la universidad, en los que ya existen avances.**

Sin embargo, **los estudiantes aún mantienen su preocupación por la falta de planta docente**, de acuerdo con Lizeth Riazo, estudiante de Sociología de la Universidad, si bien es cierto que se llegó a un acuerdo para aumentar los docentes, ya se encuentran a mitad de semestre. Le puede interesar: ["Estudiantes de la Universidad de Pacífico en Paro denuncian mala administración"](https://archivo.contagioradio.com/estudiantes-de-la-universidad-del-pacifico-en-paro-por/)

“El panorama es un tanto complejo porque aún no se ha hecho la contratación de los docentes de planta, pero ya está plasmado que debe hacerse en este semestre, esto nos afecta directamente, **no tenemos las condiciones necesarias, pero estaremos al tanto, si no se cumple este acuerdo retomamos el plantón”** afirmo la Riazo. Le puede interesar: ["Ser Pilo Paga ha gastado 350 más de mil millones del presupuesto de Educación"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/)

De igual modo se creará un comité de veeduría conformado por algunos estudiantes, que estará en permanente vigilancia de que las directivas de la Universidad cumplan con los acuerdos, además participarán de escenarios como el Consejo Estudiantil y **podrán incidir en discusiones como la destinación del presupuesto para el siguiente semestre, en el mes de octubre.**

<iframe id="audio_17866341" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17866341_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
