Title: Ley de Víctimas fue prorrogada por 10 años más en el Congreso
Date: 2020-11-18 18:21
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Ley de Víctimas, Restitución de tierras, víctimas
Slug: ley-de-victimas-fue-prorrogada-por-10-anos-mas-en-el-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Ley-de-victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes la plenaria del Senado aprobó en cuarto y último debate, **la prórroga** **de la Ley 1448 de 2011 también conocida como “Ley de Víctimas y Restitución de Tierras” por 10 años más,** con lo que extendería su vigencia hasta junio del año 2031. **Ahora la ley pasa a sanción presidencial para su aprobación definitiva.** (Le puede interesar: [Leyner Palacios representará el legado de las víctimas ante la Comisión de la Verdad](https://archivo.contagioradio.com/leyner-palacios-representara-el-legado-de-las-victimas-ante-la-comision-de-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PizarroMariaJo/status/1328910006809464832","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PizarroMariaJo/status/1328910006809464832

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**La [Ley 1448 de 2011](http://www.secretariasenado.gov.co/senado/basedoc/ley_1448_2011.html) establece medidas de atención, asistencia y reparación integral a las víctimas del conflicto armado.** Por medio de esa norma el Estado reconoce a las víctimas y el derecho que estas tienen a una reparación integral. Además, la norma también prevé la restitución a quienes han sido despojados de sus tierras o han sido obligados a abandonarlas. (Le puede interesar: [Otra Mirada: Riesgos de reclamar tierras en Colombia](https://archivo.contagioradio.com/otra-mirada-riesgos-de-reclamar-tierras-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La aprobación fue unánime,** por lo cual el debate se centró más en discutir los diversos impedimentos presentados por algunos senadores, al considerar que podrían verse beneficiados por dicha ley al ser víctimas del conflicto o podrían tener conflicto de intereses al ser, por ejemplo, miembros de las antiguas FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, la prorroga fue aprobada en 10 y no en 15 años, como lo contemplaba la ponencia que estaba en discusión para el segundo debate en Senado, ya que, el texto debatido en la Cámara de Representantes se aprobó por 10 años, lo que en caso de aprobar por 15 años la extensión en el Senado, podría generar discrepancia y poner en riesgo el proyecto en la conciliación.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/UnidadVictimas/status/1328906442720600065","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/UnidadVictimas/status/1328906442720600065

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Cabe recordar, que en diciembre del año 2019 **la Corte Constitucional había declarado inexequible el término establecido para derogar la Ley de Víctimas y había exhortado al Gobierno y al Congreso a extender su vigencia** o a adoptar un nuevo régimen de protección de los derechos de las víctimas; por lo que esta prórroga del Congreso obedece a esa orden directamente emitida por la Corte.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1329053372247322624","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1329053372247322624

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CristoBustos/status/1329037921907826700","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CristoBustos/status/1329037921907826700

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
