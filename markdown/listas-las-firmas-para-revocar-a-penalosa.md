Title: Ya están listas las firmas para Revocar a Peñalosa
Date: 2017-03-24 15:38
Category: Nacional, Política
Tags: Alcaldía de Bogotá, Revocatoria a Peñalosa
Slug: listas-las-firmas-para-revocar-a-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/revocatoriaestampida.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica ] 

###### [24 Mar 2017] 

El comité Unidos Revocaremos a Peñalosa, informó que **ya recolectó las 350 mil firmas que solicitaba la Registraduría** para continuar con el proceso de revocatoria al alcalde, sin embargo, la nueva meta para el comité es la recolección de **600 mil firmas, que garanticen que el proceso no se vaya a caer en la primera etapa de verificación por anulación de firmas.**

De acuerdo con Carlos Carrillo integrante de Unidos Revocaremos a Peñalosa, recoger el doble de firmas “**permite no solamente demostrar que el mandato ciudadano es contundente, sino para blindar al proceso** de posibles ataques que podrían darse contra la iniciativa”. Le puede interesar: ["Administración de Peñalosa hace oídos sordos para detener la venta de la ETB"](https://archivo.contagioradio.com/administracion-penalosa-hace-oidos-sordos-para-detener-la-venta-de-la-etb/)

El comité, además ha venido denunciado una amenaza de investigación por parte del Consejo Nacional Electoral, que haría parte de un accionar ilegal que estaría gestandose desde intereses del gobierno. Sin embargo, **hasta el momento no han tenido conocimiento de ningún comunicado por parte del Consejo Nacional Electoral**, en donde se les informe de alguna investigación.

En términos de tiempo Carrillo expresó que, si bien hay un plazo establecido por la Registraduría Nacional para recolectar las firmas hasta el mes de junio, sí continúan al ritmo que van, esta recolección podría darse en menos tiempo. Una vez, la Registraduría tenga en su poder las firmas tendrá un plazo de **45 días para determinar si son válidas o no.**

Posteriormente el gobierno tendrá dos meses para convocar al referendo, **de ganar el sí en la revocatoria a Peñalosa, se delegará un alcalde encargado**, mientras se convoca nuevamente a elecciones para la Alcaldía de Bogotá. Le puede interesar: ["Enrique Peñalosa desconoce reservas de los Cerros Orientales"](https://archivo.contagioradio.com/enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales/)

###### Reciba toda la información de Contagio Radio en [[su correo]
