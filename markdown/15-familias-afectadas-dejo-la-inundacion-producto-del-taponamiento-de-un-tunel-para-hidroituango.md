Title: 15 familias afectadas dejó la inundación producto del taponamiento de un túnel para Hidroituango
Date: 2018-04-30 13:01
Category: DDHH, Nacional
Tags: EPM, Hidroituango, Movimiento Ríos Vivos, Río Cauca, tala de árboles
Slug: 15-familias-afectadas-dejo-la-inundacion-producto-del-taponamiento-de-un-tunel-para-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/creciente-hidroituango-e1525107887374.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [30 Abr 2018] 

Las comunidades del bajo Cauca antioqueño, que han sido afectadas por la construcción del proyecto hidroeléctrico Hidroituango, denunciaron que cerca de 15 familias que habitan allí, perdieron sus enseres y hay alerta de que pueda ocurrir una nueva emergencia producto del taponamiento en el túnel de descarga, hecho que sería el resultado de la **tala indiscriminada de árboles** que la empresa EPM habría desechado en las riveras del río generando así una grave inundación.

De acuerdo con Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, cuando empezó la tala del bosque seco tropical, **hace un año**, ya se habían hecho las denuncias correspondientes sobre lo que podría ocurrir con el caudal del río. Afirmó que “la empresa no recoge, como está obligada a hacerlo, los residuos de la tala del bosque y esta situación se aumentó”.

### **Taponamiento del túnel generó una inundación de 40 metros** 

Por esto, recordó que “hace una semana esta situación llegó a su limite en donde se presentó un taponamiento y una **inundación que generó estragos** en las poblaciones rivereñas aguas arriba”. Indicó que el fin de semana el agua alcanzó a subir hasta 40 metros “lo que ocasionó que más de 15 familias perdieran todas sus cosas”.

Además, reiteró que “se escuchó una gran explosión dentro del túnel principal que  no ha sido aclarado por **Empresas Públicas de Medellín**”. Además, expresó que el taponamiento se evidenció con una disminución del caudal del río en la parte de abajo “que asustó mucho a los pescadores” y arriba, la vía de acceso a Ituango quedó bloqueada por la inundación. (Le puede interesar:["Memoria y resistencia en el Cañón del río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/)).

### **Situación humanitaria aún no está controlada** 

Frente a las declaraciones del gerente general de EPM, Jorge Londoño de la Cuesta, quien afirmó que la situación está controlada y será monitoreada por la empresa, Zuleta manifestó que **no se reconoció la responsabilidad** ni las obligaciones que tienen por cuenta de la licencia ambiental.

Sin embargo, reconoció que la situación está retornando a la normalidad y el río está fluyendo con normalidad. Para el Movimiento Ríos Vivos queda aún la preocupación de una nueva emergencia teniendo en cuenta que los desechos **no han sido removidos** del río y “la tala continúa en la parte alta”. (Le puede interesar:["Autoridades de Antioquia descalificaron llamado de atención de Europa sobre Hidroituango"](https://archivo.contagioradio.com/autoridades-de-antioquia-descalificaron-llamado-de-atencion-de-europa-sobre-hidroituango/))

Zuleta enfatizó en que no hay una medida para que se establezcan los procesos sancionatorios para evitar que la empresa **siga lanzando madera al río**. Dijo que la emergencia de las comunidades no se ha superado en la medida en que no ha habido ayuda humanitaria y la empresa se ha negado a reconocer que aún hay comunidades viviendo en las riveras del río.

### **Estigmatización contra el Movimiento continúa** 

Zuleta afirmó que a la grave situación humanitaria que se vive en esa zona, los integrantes del Movimiento Ríos Vivos han sido **estigmatizados y señalados** por cuenta de su oposición al proyecto Hidroituango. Dijo que “la situación en Antioquia es de persecución y el mismo gobernador ha dicho que nos va a capturar a todos los que somos opositores a Hidroituango”.

https://youtu.be/L3RGMNQqFms<iframe id="audio_25710368" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25710368_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
