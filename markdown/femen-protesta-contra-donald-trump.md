Title: La protesta de FEMEN contra Donald Trump
Date: 2017-01-17 12:39
Category: El mundo
Tags: Donald Trump, Estados Unidos
Slug: femen-protesta-contra-donald-trump
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/FEMEN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Periódico ] 

###### [17 Ene 2017] 

 A tres días de la posesión de Donal Trump, continúan las manifestaciones por parte de la ciudadanía en contra de las políticas que ha dicho establecerá una vez sea presidente. El último hecho se registró durante la entrega de la estatua de cera de Trump en el Museo de Cera de Madrid, **cuando mujeres pertenecientes a FEMEN interrumpieron la  ceremonia y denunciaron los diferentes actos de violencia que Trump ha realizado.**

Las mujeres, que llegaron con sus torsos desnudos, tenían mensajes que decían **“coge el patriarcado por la pelotas”**, una de ellas se acercó hasta la estatua, agarro sus partes íntimas y gritó esta misma frase.  Sin embargo, esta no es la primera acción que las mujeres han hecho desde que Trump anunció su candidatura, durante las elecciones, las activistas de FEMEN aparecieron en los colegios electorales con este mismo mensaje en sus cuerpos.

De otro lado, el próximo presidente de los Estados Unidos afirmó, en una de sus apariciones, que Ángela Merkel  cometió un gran error al aceptar a miles de refugiados sirios: “**Creo que cometió un error muy catastrófico al aceptar a todos estos ilegales**, ya sabes, tomando a toda la gente de donde vengan…  Y nadie sabe de dónde vienen, así que creo que ella cometió un error catastrófico, terrible error”. Le puede interesar:["Ola de movilizaciones en EE.UU rechazan posesión de Donald Trump"](https://archivo.contagioradio.com/34791/)

De igual forma, Trump arremetió contra la **OTAN al afirmar que esta era una organización obsoleta, que no hizo mucho por combatir el terrorismo** y que los países miembros no pagaban lo suficiente. A lo que el primer mandatario de Francia, François Hollande respondió que la OTAN no está obsoleta.

No obstante, estas acciones por parte de Trump han significado divisiones al interior de la comunidad internacional, por un lado están todos aquellos que consideran que hay que calmar los ánimos, mientras que hay otros que aseguran que esta situación no cambiará y podría salirse de control. Le puede interesar:["Donlad Trump amenaza con poner fin a acuerdos entre USA y Cuba"](https://archivo.contagioradio.com/trump-amenaza-con-poner-fin-a-acuerdos-entre-usa-y-cuba/)
