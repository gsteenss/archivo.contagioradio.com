Title: Sin lineamientos, centros de memoria pueden convertirse en aparatos ideológicos de Gobierno
Date: 2020-02-04 17:03
Author: CtgAdm
Category: Memoria, Nacional
Tags: CNMH, Dario Acevedo, memoria, víctimas
Slug: sin-lineamientos-centros-de-memoria-pueden-convertirse-en-aparatos-ideologicos-de-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Captura-de-Pantalla-2020-02-04-a-las-6.30.53-p.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto:Comunicaciones Prosperidad Social {#fotocomunicaciones-prosperidad-social .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/jose-antequera-director-del-centro-memoria-paz_md_47314110_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; José Antequera | Director centro de memoria, paz y reconciliación

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

A partir del 1 de febrero el Centro Nacional de Memoria Histórica (CNMH) fue suspendido de la Coalición Internacional de Sitios de Conciencia y la Red de Sitios de Memoria Latinoamericanos y Caribeños porque su director, Darío Acevedo, no envió una carta aclarando su posición sobre temas claves en la agenda de las víctimas. La suspensión significa perder parte de criterios de orientación que proponen estas redes para la construcción de memoria.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las razones de la suspensión, y la excusa de Acevedo**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la Carta de la Red de Sitios de Memoria se explica que la suspensión se produce porque ellos enviaron una comunicación a Acevedo el pasado septiembre 24 de 2019, en la que le solicitaban aclarar esencialmente tres cosas: El reconocimiento de la existencia del conflicto en Colombia como está establecido en la Ley de Víctimas; su apoyo e inclusión de todas las víctimas en el CNMH; y su compromiso de construir memoria para la no repetición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, 4 meses después, la Red no obtuvo respuesta lo que conllevó a la suspensión de la membresía del CNMH. Ante la situación el director de esta organización señaló a medios de información que el hecho fue una confusión y que habían dado respuesta a la carta pero se había enviado a otra organización en Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Qué son las redes de memoria y para qué sirven?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según explicó **José Antequera, director del Centro de Memoria, Paz y Reconciliación**, tanto la Red como la Coalición son espacios creados a partir de la iniciativa de centros de memoria, para elaborar principios sobre el funcionamiento de este tipo de espacios, en respuesta a la necesidad de tener estándares mínimos para la construcción de memoria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, estas redes buscan ser un espacio de colaboración entre las iniciativas de memoria en torno a metodologías, buenas prácticas e incluso, dando la posibilidad de realizar intercambios "con el objetivo de comprender la memoria como un asunto de las políticas públicas, y como un elemento importante para construir democracia".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, Antequera sostuvo que la suspensión de la membresía del CNMH supone que no podrá participar en las actividades de la Coalición y la Red, así como que la sociedad tendría que evaluar el trabajo que realiza, tomando en cuenta que no contará con el apoyo de estos grupos en sus lineamientos para ejecutar nuevos proyectos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hecho que es relevante, pues organizaciones de víctimas están convocando a un plantón el próximo 5 de febrero, frente a las instalaciones del Centro para rechazar la construcción del Museo de Memoria, en tanto no responde al interés de las víctimas. (Le puede interesar: <https://www.justiciaypazcolombia.com/por-que-las-victimas-piden-la-renuncia-de-dario-acevedo-como-director-del-cnmh/>)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Movicecol/status/1224458048090996737","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Movicecol/status/1224458048090996737

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Sin lineamientos, los centros de memoria pueden convertirse en aparatos ideológicos de Gobierno**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Antequera señaló que los lineamientos base para construir políticas de memoria son relevantes, porque en caso de que no existieran, si una fuerza política autoritaria se impone en el país, se terminan utilizando los espacios de memoria como aparatos ideológicos del gobierno de turno, lo que termina siendo un retroceso para la democracia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a la situación del CNMH, y su director, Antequera resaltó que en el ámbito internacional se siguen valorando los esfuerzos de Colombia por la reconstrucción de lo ocurrido en el pasado desde espacios como el que él dirige, el Museo Casa de la Memoria en Medellín y las múltiples iniciativas de víctimas que hay en el país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Organizaciones piden la renuncia de Acevedo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el Movimiento de Víctimas de Crímenes de Estado son cinco las razones por las que debería salir Dario Acevedo de su cargo;

<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->

1.  **Negación de conflicto armado** en Colombia
2.  **Censura**, reflejada en los cambios significativos al guión del Museo de la Memoria.
3.  **Burla de la memoria**, y de la forma como las víctimas hacen representación de esta.
4.  **Niega la responsabilidad del estado**, *"en declaraciones a dicho que las ejecuciones extrajudiciales no fueron una política de estado y que ésta no debe ser la forma en la que se deben contar estos actos*.
5.  causa el r**etiro de Colombia de la coalición internacional de sitios de conciencia**. (Le puede interesar: <https://archivo.contagioradio.com/bogota-compromete-con-memoria-para-la-paz-y-la-reconciliacion/>)

<!-- /wp:list -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
