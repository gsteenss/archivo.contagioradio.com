Title: Yopal, the first municipality in Colombia to ban fracking
Date: 2019-05-28 17:23
Author: CtgAdm
Category: English
Tags: Alianza contra el Fraking, Fracking colombia, Fracking en Colombia, Yopal
Slug: yopal-the-first-municipality-in-colombia-to-ban-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-03-26-at-12.01.58-PM-1-e1544654159516.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

The Municipal Council of Yopal approved an agreement that prohibits the use of fracking to exploit oil in this region of Casanare, the first deal of its kind in the country.

According to Councilman Juan Vicente Nieves, spokesman of the project, the agreement seeks to protect the environment, especially, its water resources that have been badly damaged by the oil industry in the past 40 years. The councilman argued that the use of unconventional techniques such as fracking could even provoke more serious consequences than those caused by traditional methods.

"It's much more destructive than the methods that have been used up until now. There are studies that show how these new techniques contaminate and destroy the water and subsoil. There's a chance that radioactive minerals in the subsoil could be released into the atmosphere. This is an attack on the environment,"said the councilman.

The government has not formally approved the implementation of fracking in Colombia yet. Still, the administration of President Ivan Duque has expressed its interest by including two articles in the National Development Plan that lays out the feasibility of fracking in the country and a roadmap to proceed with its implementation.

Councilman Nieves said that the municipal agreement is a message to the government that that the townspeople of Yopal disagree with the Duque administration's position. He also emphasized that Yopal tried to push through seven referendums to reject all forms of oil exploitation in the region but these were struck down by the Administrative Court of Casanare.

"Since the Court blocked the referendum, we're building on the will of the people to move ahead with this municipal agreement, hoping that the Court will respect it. We have to see what will happen because it is the first municipal agreement that we know of in the country the bans fracking," said Nieves.

Two previous sentences of the Constitutional Court left the anti-mining referendums without legal grounds. As a result, municipal agreements have become the main democratic mechanism used by municipalities to decide over the land use in their territories.

While some anti-mining agreements were rejected by administrative courts, the State Council decided that the territorial entities have the constitutional competence to protect the environment in their region. For this reason, the Councilman hopes that "this agreement will be replicated around the country to see what the government will do next."
