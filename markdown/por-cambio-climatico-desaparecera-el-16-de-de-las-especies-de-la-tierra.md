Title: Por cambio climático desaparecerá el 16% de de las especies de la tierra
Date: 2015-05-04 14:25
Author: CtgAdm
Category: Ambiente, Animales, Nacional
Tags: Animales, animales en peligro de extinción, Australia, cambio climatico, Estados Unidos, Latinoamérica, Mark Urban, Nueva Zelanda, Revista Science, Universidad de Connecticut
Slug: por-cambio-climatico-desaparecera-el-16-de-de-las-especies-de-la-tierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/01-Walrus-AP445359020085.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: share.america.gov]

De acuerdo a un estudio de la Universidad de Connecticut, de Estados Unidos, publicado en la revista Science **una de cada seis especies de fauna y flora desaparecerán del planeta, a causa del cambio climático.**

El estudio, dirigido por el investigador Mark Urban, asegura que si la temperatura del planeta aumenta cuatro grados, se** generaría la desaparición del 16 % de las especiales de la tierra**, debido a los niveles de producción de dióxido de carbono.

Según la investigación donde se analizó la información de 131 estudios científicos sobre el peligro de extinción por el cambio climático, si las emisiones de dióxido de carbono permanecen con la misma intensidad, **Latinoamérica sería la región con mayores índices de especies animales y vegetales  en peligro de extinción con un 23%**, seguido por Australia y Nueva Zelanda con el 14 %.

Mark Urban concluyó que pese a que muchas especies podrán  cambiar sus rangos y adaptarse al cambio climático, otras no lo podrán hacer porque su hábitat habrá desaparecido o será difícil que accedan a él. Es por eso que “**el mundo se debe unir para controlar las emisiones de gases de efecto invernadero y no permitir la pérdida de una de cada seis especies" afirmó el investigador.**
