Title: 3 Páramos de Cundinamarca en riesgo por proyecto vial
Date: 2016-10-25 16:25
Category: Ambiente, Nacional
Tags: amenaza ambiental, Defensa de Páramos, Perimetral de Oriente
Slug: 3-paramos-cundinamarca-riesgo-proyecto-vial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/chingaza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Parques Naturales] 

###### [25 Oct 2016] 

En grave peligro se encuentran los páramos de Sumapáz, Cruz Verde y Chingaza, territorios ricos en biodiversidad de Alta Montaña Andina, en donde se encuentran las **fuentes de agua que benefician a los habitantes de la capital y además son el hábitat del Zorrillo Lanudo y de 3 especies de frailejon. **

Un reciente estudio realizado por la Universidad Javeriana, reveló que varias zonas de **Frailejones se están viendo amenazadas por la lluvia ácida y fuertes cambios de temperatura.** Esto, debido a la construcción de la vía perimetral de Oriente que, según uno de los integrantes del Colectivo Guardianes de Choachí **“busca sacar palma aceitera y agro tóxicos de los llanos orientales”.**

Ricardo Perdomo integrante del Colectivo, informó que Juan Ceballos, abogado y ambientalista, quien ha realizado litigios en defensa de territorios como La Colosa en el Tolima y Apaporis en el Chocó, se ha unido a las acciones legales del Colectivo en contra de la perimetral, y **“asumió la postura de 15 días de huelga de hambre, este martes 25 de Octubre se cumplen 8 días”.**

Este proyecto vial de 4ª generación, cuenta con el soporte jurídico del Decreto 1076 de 2015, “Único Reglamentario del Sector Ambiente y Desarrollo Sostenible” y el documento CONPES 3267 de 2011, sobre infraestructura minero energética y de transporte, el cual **pretende quitar las barreras legales de licenciamiento y evitar la participación ciudadana.**

Perdomo, señaló que **“ese decreto es un retroceso a la normatividad ambiental colombiana”** y que debido a ello han iniciado desde hace unos meses “acciones en defensa de lo social, lo jurídico y lo ambiental”. Le puede interesar: [Cerca de 400 títulos mineros en zonas de páramos deberán ser retirados.](https://archivo.contagioradio.com/400-tituloa-mineros-seran-retirados-por-fallo-de-la-corte-constitucional/)

Este integrante del Colectivo, manifiesta que el objetivo de las acciones legales y la huelga de Ceballos, son para **“visibilizar las modificaciones estructurales que debe tener el ANLA por todo lo que viene pasando en el país,** pues es la autoridad que emite resoluciones que dan vía libre a las multinacionales para explotar”.

Algunas entidades territoriales como la **Corporación Autónoma de la Orinoquía**, se han comunicado con Guardianes de Choachí y han dicho que **realizarán una revisión en uno de los puntos que tiene el proyecto.**

Perdomo señala que **“Bogotá depende en su estructura ecológica de municipios aledaños** (…) tenemos la obligación de mostrar a comunidades alrededor de Bogotá que estamos en la defensa del territorio”. Le puede interesar: [Delimitación de páramos en Colombia: nuevos páramos.](https://archivo.contagioradio.com/delimitacion-de-paramos-en-colombia-nuevos-paramos/)

Por otra parte, reveló que el Programa de Adaptación a las Vías Ambientales, “es un juego del ANLA, que pretende que vías como esta no requieran licencia ambiental”. Son **1.5 billones de pesos los que se dispondrán para una vía de 164 km en la que sólo 4.5 km del trayecto son nuevos.**

Concluyó anunciando que ya iniciaron acciones para “oficiar ante la contraloría y la procuraduría que **hubo complicidad del ANLA para la modificación de las conclusiones del documento inicial para lograr la viabilidad del proyecto”.**

<iframe id="audio_13475238" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13475238_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
