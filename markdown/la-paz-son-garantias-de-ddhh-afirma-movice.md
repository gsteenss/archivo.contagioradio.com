Title: 'La paz son garantías de Derechos Humanos' MOVICE
Date: 2016-03-15 10:00
Category: DDHH, Nacional
Tags: Conversaciones de paz de la habana, Juan Manuel Santos, MOVICE, Paramilitarismo, Unión Patriótica
Slug: la-paz-son-garantias-de-ddhh-afirma-movice
Status: published

###### [Foto: archivo] 

<iframe src="http://co.ivoox.com/es/player_ek_10812175_2_1.html?data=kpWlk5eVe5ahhpywj5WbaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5yncYammLHOjdXFvozn0NOYycbWpc%2Fohqigh6aopdSfxcqYpsrWqcTc0NiYqtrRpc%2Fj1IqfmZCxk7e9pKqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Soraya Gutiérrez, MOVICE] 

###### [15 Mar 2016] 

Hoy se realizará, en la Plaza de Bolívar en el centro de la Capital, la concentración Por la vida, los derechos humanos y la paz: garantías de no repetición, convocada por diferentes sectores del movimiento social y organizaciones defensoras de derechos humanos. La denuncia central girará en torno al [asesinato de líderes](https://archivo.contagioradio.com/8-asesinatos-en-menos-de-40-horas-en-putumayo/), **lideresas y amenazas a comunidades que se han presentado en los últimos 3 meses.**

De acuerdo con Soraya Gutiérrez, abogada del Colectivo José Alvear Restrepo y vocera del MOVICE ([Movimiento de Víctimas de Crímenes de Estado](https://archivo.contagioradio.com/?s=movice)) “El movimiento social exige al gobierno del presidente Juan Manuel Santos un pronunciamiento en relación con esta ola de asesinatos, creemos que él ha guardado silencio y esto significa que no resulta siendo importante que vidas humanas sigan siendo destruidas*”.*

De igual manera la abogada, vocera del MOVICE, afirma que es necesario que se atienda de manera urgente el recrudecimiento del paramilitarismo, Gutierrez resalta que la propuesta es que se *“***cree una comisión  de alto nivel de  [garantías de no repetición](https://archivo.contagioradio.com/cidh-rechaza-asesinatos-y-amenazas-a-defensores-de-ddhh-en-colombia/), creemos que el punto 5 de víctimas, no tiene un desarrollo profundo**”

El evento se llevará acabo a las 11:30 am y se espera que diferentes sectores del movimiento social, defensores de derechos humanos y la ciudadanía en general se movilicen a favor de esta concentración y denuncia. En Meta, Antioquia, Sucre y Cali se realizarán movilizaciones a la misma hora.
