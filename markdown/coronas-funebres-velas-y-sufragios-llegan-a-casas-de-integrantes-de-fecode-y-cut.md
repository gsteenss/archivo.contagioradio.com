Title: Coronas fúnebres, velas y sufragios llegan a casas de integrantes de FECODE y CUT
Date: 2020-10-28 08:03
Author: AdminContagio
Category: Nacional
Tags: Amenazas a sindicatos, CUT, fecode, Sindicalismo
Slug: coronas-funebres-velas-y-sufragios-llegan-a-casas-de-integrantes-de-fecode-y-cut
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Cream-and-Blue-Wallpaper-1.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/cut.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @Nelsonalarcon74

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los directivos de la [Federación Colombiana de Trabajadores de la Educación (Fecode)](https://www.fecode.edu.co/) y de la [Central Unitaria de Trabajadores de Colombia (CUT)](https://cut.org.co/)denunciaron que están siendo nuevamente víctimas de amenazas de muerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Federación señaló que a 15 miembros les llegó una **corona fúnebre, velas negras, sufragios y cartas con sus nombres que rezan “ore y descanse en paz”.** (Le puede interesar: [«Si no salimos nos van a masacrar a todos… volveremos a las calles»](https://archivo.contagioradio.com/si-no-salimos-nos-van-a-masacrar-a-todos-volveremos-a-las-calles/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Nelsonalarcon74/status/1320780685041348609","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Nelsonalarcon74/status/1320780685041348609

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Uno de los miembros que fue amenazado señalo que «esta es una advertencia para que nos quedemos callados y no lo vamos a hacer... Como seres humanos nos da miedo, pero vamos a derrotar el miedo». Además hizo un llamado a la solidaridad y unidad con la federación.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PFendidoc/status/1320908644360327169","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PFendidoc/status/1320908644360327169

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Ante esta situación, el presidente de Fecode, Nelson Alarcón, solicitó una reunión con el presidente Iván Duque y la ministra de Educación, María Victoria Angulo con el objetivo de mantener un diálogo sobre las garantías para el ejercicio del sindicalismo en Colombia. [En riesgo vida de Hernando Benítez líder campesino de Sucre](https://archivo.contagioradio.com/en-riesgo-vida-de-hernando-benitez-lider-campesino-del-movice/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La CUT, por otro lado, exigió al gobierno nacional garantías constitucionales para la protección del sindicalismo en Colombia, así como **alertó a «todas y todos los afiliados ante la continuidad de la violencia al sindicalismo».**

<!-- /wp:paragraph -->

<!-- wp:image {"id":91931,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/cut.png){.wp-image-91931}  

<figcaption>
Comunicado

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Por su parte, el [Ministerio de Educación](https://www.mineducacion.gov.co/portal/salaprensa/Noticias/401649:El-Ministerio-de-Educacion-Nacional-rechaza-amenazas-contra-dirigentes-sindicales-del-magisterio-colombiano) rechazó las amenazas y «reitera su compromiso indeclinable de continuar el trabajo coordinado que se adelanta entre diferentes entidades del Estado para establecer las formas de protección a la vida, la seguridad y la integridad de los educadores y sus familias».

<!-- /wp:paragraph -->

<!-- wp:heading -->

En 2019 Fecode ya había recibido amenazas
-----------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hace casi una semana el presidente de la agremiación denunció que un miembro del ejecutivo de Fecode, Fabio Herrera, había aparecido en la lista de amenazados por parte las autodefensas de la Guajira contra dirigentes sindicales y sociales .

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Nelsonalarcon74/status/1319458537408430080","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Nelsonalarcon74/status/1319458537408430080

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Asimismo, el 3 de septiembre de 2019 la organización denunció que el Comité Ejecutivo había recibido amenazas a través de un panfleto firmado por las Águilas Negras. (Lea también: [FECODE recibe nuevas amenazas después del Paro del Magisterio](https://archivo.contagioradio.com/fecode-rueda-prensa/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alarcón indicó que «en estos 25 años, más de mil compañeros, maestros y maestras han sido asesinados. **En el 2019 más de casi 700 compañeros y compañeras han sido amenazados en territorio colombiano**». 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este momento, Fecode ya había pedido al gobierno que se dieran garantías para la seguridad de los dirigentes sindicales y la CUT exigió al gobierno, cumplir con los acuerdos firmados con Fecode durante las negociaciones de los últimos años.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
