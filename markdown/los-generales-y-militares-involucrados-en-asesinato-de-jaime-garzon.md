Title: Los generales y coroneles involucrados en asesinato de Jaime Garzón
Date: 2016-03-10 18:32
Category: Judicial, Nacional
Tags: crímenes de estado, Jaime Garzon
Slug: los-generales-y-militares-involucrados-en-asesinato-de-jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Jaime-Garzón-e1457652708929.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Con la Oreja Roja 

<iframe src="http://co.ivoox.com/es/player_ek_10762683_2_1.html?data=kpWkmJeafJShhpywj5WZaZS1kpyah5yncZOhhpywj5WRaZi3jpWah5ynca3j1JDUx9PJtsLgxtiY25DHs9Pjz8rZx9iPrc%2Fq0NHixdfFqNDnjMrbjcbXqdTdz8bh0ZDIqYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [10 Mar 2016] 

**La Fiscalía señaló como crimen de Estado el asesinato del periodista y humorista**, Jaime Garzón. Según Iván Lombana, director de Articulación de Fiscalías Especializadas, se concluyó que la investigación fue desviada por algunos miembros de la Fuerza Pública, entre ellos, el general (r) Mauricio Santoyo, el coronel (r) Jorge Plazas Acevedo y el general (r) Rito Alejo del Río; así mismo, José Miguel Narváez habría sido una de las personas que más le habría insistido a Carlos Castaño de la necesidad de matar al periodista.

Para el abogado Sebastián Escobar, de la Comisión Colombiana de Juristas y quien lleva el caso del asesinato del humorista, se trata de un paso “En principio positivo en la medida en que nos permite avanzar en el reconocimiento de lo que verdaderamente aconteció en este caso”, evidenciando que “a lo largo del proceso se han recaudado pruebas sobre la participación de diversos agentes estatales, entre ellas el **Ejército, DAS y la Policía, reflejo de lo que ha arrojado la investigación durante años”**.

Sin embargo, Escobar también resalta que se trata de **una decisión insuficiente porque desde hace años se vienen solicitando que el homicidio de Garzón sea declarado como un crimen de lesa humanidad,** pero este es el momento en que la familia no ha logrado obtener una respuesta satisfactoria.

A su vez, el abogado señala que la hipótesis de desviación de la investigación debe avanzar pues es fundamental conocer las razones por las cuales se habría querido desviarla. Además porque según Sebastián Escobar, “en el contexto actual de lo que se avecina con la Jurisdicción Especial para la Paz, es importante que se establezca los vínculos estatales con estructuras paramilitares y se establezca cuál era ese contexto de persecución frente a defensores de Derechos Humanos, periodistas y opositores políticos”.

Lo anterior, teniendo en cuenta que Jaime Garzón se vuelve objeto de amenazas, señalamientos hasta su asesinato por sus diálogos con la guerrilla, pues intentaba mediar para que se liberara a personas que estuvieran en manos de la insurgencia, por lo que  Diego Fernando Murillo, alias “Don Berna” en coordinación con agentes estatales contrata cuatro sicarios para acabar con la vida del periodista, así como habría sucedido con los asesinatos de los defensores de Derechos Humanos,  Elsa Alvarado, Mario Calderón, Jesús María Valle y Eduardo Umaña Mendoza.

### Los implicados 

**General (r) Mauricio Santoyo:** Luego del crimen, el ex jefe de seguridad de Álvaro Uribe Vélez habría sido quien secuestró a dos de los sicarios que organizaron el asesinato de Garzón. De acuerdo con la Fiscalía, el general se los había entregado a “Don Berna” quien posteriormente los habría torturado, asesinado, descuartizado y desaparecido. Actualmente, Santoyo se encuentra condenado a 13  años de cárcel en Estados Unidos por vínculos con la el grupo narcoparamilitar, la Oficina de Envigado.

**Coronel (r) Jorge Eliécer Plazas Acevedo:** En ese entonces, Plazas era jefe de inteligencia de la Brigada XIII en Bogotá, y habría sido quien dio las ordenes a los sicarios para que asesinaran a Jaime Garzón, luego de haberle hecho todo un seguimiento a las actividades diarias que hacía el periodista. Esto se conoce debido a que en un allanamiento en la Brigada XIII se encontraron los documentos que evidenciaban el seguimiento.

**General (r) Rito Alejo del Río:** Era quien comandaba la Brigada XIII de Bogotá y habría sido el enlace con los paramilitares. Según Lombana,  sería una de las personas que planeó el asesinato del humorista por solicitud de jefe paramilitar Carlos Castaño.

**José Miguel Narváez:** el exsubdirector del DAS sería la personas que más insistió a Carlos Castaños para ejecutar a Garzón. Además desde el DAS se habría manipulado la investigación para desviarla y capturar a dos personas que finalmente no eran los autores materiales del crimen. Narváez es investigado por ser uno de los ideólogos políticos de las autodefensas.

El próximo 14 de marzo Rito Alejo del Río fue citado para rendir versión libre por  este caso. Por otra parte, Plazas Acevedo, está siendo procesado por el asesinato del humorista debido a los testimonios de varios exparamilitares que lo vinculan como uno de los autores intelectuales del crimen.
