Title: Reforma tributaria sacará del bolsillo 3% del salario a los trabajadores
Date: 2016-12-23 12:42
Category: Economía, Nacional
Tags: CUT, Reforma tributaria, salario minimo
Slug: reforma-tributaria-sacara-del-bolsillo-3-del-salario-a-los-trabajadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/salario-minimo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: 360 Radio Colombia] 

###### [23 Dic. 2016] 

Pese a la oposición de varios sectores sociales y políticos, además de algunos empresarios contra la Reforma Tributaria, ésta fue finalmente aprobada por el Congreso de Colombia el pasado jueves.

Uno de los temas más polémicos sigue siendo que **muchos productos que usan un alto número de personas de la sociedad, se verán gravemente aumentados en lo que respecta a los impuestos,** entre esos está la canasta familiar.

Las Centrales Unitarias de Trabajo aseguraron que el gobierno “jugó” con la Comisión Nacional de Concertación en lo que va del año en lo que respecta al aumento del salario mínimo.

Para Fabio Arias, Secretario General de la Central Unitaria de Trabajadores - CUT- **“la concertación estuvo un tanto dilatada, sin ninguna consideración porque estaba pendiente de resolver los asuntos en el Congreso de la República con la Reforma Tributaria”**.

Según el secretario de la CUT, este hecho debió haber sido expresado desde el primer día de tal modo que **“nos hubiéramos evitado muchas reuniones que terminaron siendo una frustración más para los trabajadores”** indicó.

**La CUT, así como otros sectores, manifiestan que esta reforma será muy regresiva “los impactos sobre los salarios de los trabajadores va a ser una medida muy lesiva (…) el IVA del 19% es muy perjudicial**, es como meterle la mano al bolsillo y sacarle el 3% de su salario” manifestó Arias. Le puede interesar: [Reforma Tributaria hace pagar mayores impuestos al ciudadano de a pie](https://archivo.contagioradio.com/reforma-tributaria-pagar-mayores-impuestos-al-ciudadano-pie/)

Con esta reforma además podría verse afectado el poder adquisitivo de los y las colombianas y de modo **que serán menos los bienes y servicios que se podrán adquirir durante el 2017.**

Arias indicó que dado el alza del IVA al 19%, las centrales unitarias de trabajadores seguirán insistiendo en un 14% de aumentó para el Salario Mínimo “porque evidentemente esos 3 puntos del IVA adicional que se han aprobado van a ser asumidos por los trabajadores”. Le puede interesar: [Gobierno se debe poner en los zapatos de los trabajadores y aumentar 14% salario mínimo](https://archivo.contagioradio.com/gobierno-se-debe-poner-en-los-zapatos-de-los-trabajadores-y-aumentar-14-salario-minimo/)

La próxima semana, entre el 26 y el 30 de Diciembre, se llevará a cabo la última reunión del año de la Comisión Nacional de Concertación.

<iframe id="audio_15351873" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15351873_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
