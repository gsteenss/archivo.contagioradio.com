Title: Rechazo a posible nombramiento de Claudia Ortiz como directora de la UNP
Date: 2018-08-09 17:09
Category: DDHH, Política
Tags: Claudia Ortiz, Iván Duque, Somos defensores, Unidad Nacional de protección
Slug: rechazo-claudia-ortiz-directora-unp
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/claudia-ortiz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @corplanetaip] 

###### [9 Ago 2018] 

Como preocupante calificó **Carlos Guevara**, coordinador del programa Somos Defensores, el posible nombramiento de **Claudia Ortiz** como directora de la Unidad Nacional de Protección, reacción que se suma a la insatisfacción por parte de líderes sociales y políticos, por sus convicciones ideológicas, que según Guevara sería como **“poner al ratón a cuidar del queso”**.

Si bien Iván Duque no se ha referido al nombramiento, la idea de que sea la encargada de proteger a los líderes sociales, ha generado duros cuestionamientos, debido a las relaciones estrechas que Ortiz tendría con las Fuerzas Militares y a las afirmaciones que ha lanzado desde su cuenta de Twitter contra integrantes del partido Polo Democrático, La Lista de los Decentes y FARC, a quienes los calificó como “una banda de forajidos”.

A través de Twitter, Gustavo Bolivar, senador por la Lista de la Decencia anunció que renuncia a su escolta a raíz del nombramiento de Ortiz. Por su parte, Gustavo Petro ha señalado que le preocupa que la UNP, encargada de proteger a los líderes sociales, quede en manos de una persona que ha mostrado su odio contra las personas que debe proteger.

**¿Quién es Claudia Ortiz?**

Ortiz es abogada y declarada simpatizante del Centro Democrático, sin embargo y según Carlos Guevara, no ha tenido experiencia en cargos públicos ni directivos de ese carácter, tampoco en temas de Derechos Humanos o justicia, por lo que se puede decir que viene "cero kilómetros en esos temas".

Según el Coordinador de Somos Defensores, **Ortiz es muy cercana a militares y miembros de la fuerza pública retirados,** hecho que resulta preocupante porque hay temas de contratación de la UNP en los que se podría ver impedida. Sin embargo, Guevara señaló que habría que esperar la llegada de la funcionaria y que no se retroceda en los avances que han tenido lugar durante el Gobierno Santos.

Para Guevara, es importante en general analizar la asignación de los integrantes del gabinete de Duque, debido a que se habrían nombrado a más personas con el talante de Ortiz que pueden poner en riesgo la vida de líderes sociales y defensores de derechos humanos. (Le puede interesar:["En más del 90% de casos de asesinatos de líderes sociales aún no se alcanza justicia"](https://archivo.contagioradio.com/en-casos-de-lideres-sociales-no-se-alcanza-justicia/))

“Los ministerios en general están en manos de la empresa privada Colombiana. El Consejo Nacional Gremial ayudó a escoger a dedo cada uno de los nombres, tanto Nancy Patricia Gutiérrez como las personas que ocupan estos puestos, están muy ligadas al pensamiento de extrema derecha y de los empresarios”.

### **Hay grandes retos en términos de protección a líderes sociales** 

Frente a un contexto social en dónde el asesinato y amenazas a líderes sociales va en aumento,  uno de estos retos más importantes para la Unidad de Nacional de Protección, es la definición de una hoja de ruta trazada en conjunto con el Ministerio del Interior  para la protección de defensores de Derechos Humanos.

En esa medida Guevara expresó que debería haber un avance en la evaluación de las medidas de protección actuales, que deben revisarse y crear un modelo que funcione y señaló que adicionalmente "debería haber un avance en el tema de **evaluación de riesgos colectivos,** porque en este momento solo se evalúa riesgos individuales". (Le puede interesar: ["Uriel Rodríguez: Primer Líder asesinado en Gobierno Duque"](https://archivo.contagioradio.com/primer-lider-asesinado-gobierno-duque/))

<iframe id="audio_27726991" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27726991_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]

<div class="osd-sms-wrapper">

</div>
