Title: Caracol Radio: ¿Los Antitaurinos son terroristas?
Date: 2017-01-25 11:04
Category: Abilio, Opinion
Tags: Bogotá, ESMAD, protestas
Slug: caracol-radio-los-antitaurinos-son-terroristas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/esmad-en-plaza-de-toros1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: GZK ¨Desmonte del Esmad¨ 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 25 Ene 2017 

El lunes 23 de enero, Darío Arizmendi, director del programa 6 am** Hoy por Hoy de Caracol Radio, **propiedad del grupo Prisa de España, afirmó que quienes se defendieron de la policía antimotines ESMAD, eran terroristas. Lo hizo después de referirse a las protestas de los antitaurinos que, el domingo anterior,  se congregaron en torno a la **plaza de toros La Santamaría** a protestar por la reapertura del lugar y el maltrato animal que significan las corridas que ahí se celebran.

[Descontada la justeza de la manifestación dentro del derecho a la protesta social de las personas que protegen la vida animal, queremos referirnos a la gravedad del señalamiento que se le zafó al director de la franja en cuestión.]

[Ya avanzados los diálogos entre el gobierno y la guerrilla de las Farc-Ep, el Ministro de defensa de Santos no desperdiciaba oportunidad para señalar de terroristas a sus interlocutores de la mesa de la Habana, hasta que por petición de ésa insurgencia, el gobierno ordenó a su ministro desescalar el lenguaje como condición de posibilidad para avanzar, en su momento, en el  desescalamiento, también,  de la confrontación armada. Esta decisión alcanzó a los medios de información que sacaron del  uso cotidiano el calificativo. Sólo les sigue llamando terroristas el expresidente Uribe y algunos de su séquito, pero medios como Caracol Radio le bajaron al uso de  ese calificativo para referirse a organizaciones, movimientos o personas colombianas.]

[Al salir de la escena, las Farc-Ep como organización armada causa profunda indignación que  Arizmendí en un espacio radial que  escuchan de  seis  a diez de la mañana cerca de un millón de oyentes  según el Estudio Continuo de Audiencia Radial de 2016, afirme  sin mas que son terroristas quienes participaron en las protestas, tras reaccionar a las agresiones del Esmad de la policía, como lo atestigua el joven estudiantes de medicina Sebastían Diás: “el Esmad empezó las agresiones, nosotros estábamos tranquilos cuando comenzaron a lanzar los gases, una lata cayó cerca a mis pies y yo lo pegué una patada y el señor del Esmad me disparó a dos metros de distancia en la cara” (Ver: http://www.rcnradio.com/nacional/joven-agredido-en-manifestaciones-antitaurinas-denuncio-a-miembro-del-esmad/).  ]

[**Afirmaciones como las hechas por el periodista afectan al movimiento antitaurino, animalista y al conjunto del movimiento social** y  se apoya en la emotividad irracional del conflicto armado que terminó ya con la guerrilla de las Farc-Ep y  que está en vías de resolución política con el ELN.]

[Como bien lo expresa William Ospina, contamos en Colombia con una dirigencia que es responsable de la situación que padece Colombia, “ una dirigencia tan precaria que en vez de dirigir, desprecia, en vez de estimular, desanima, en vez de iluminar, oscurece.” Los sectores de poder y sus dirigentes no contentos con tapar la olla a presión que   explotó en un conflicto armado de  mas de 50 años;   ni con la decisión  de las Farc-Ep de firmar la renegociación de  un acuerdo  contra las cuerdas  del  No en  plebiscito; buscan ahora “terroristas” en las movilizaciones sociales que contradicen su estética, su sentido de la belleza,  el significado de sus tradiciones, como en este caso ocurre con lo que mal han llamado “la fiesta brava”.]

[Justo en el acuerdo final gobierno  Farc-Ep,  quedó consignada la  necesidad de garantías para el derecho a la movilización social,  del que la protesta social es parte esencial, como una repetición persistente del deseo de que los crímenes de Estado, del que son responsables algunos medios empresariales de información, empresas, políticos, miembros de la iglesia, cesen en esta nueva etapa que se inaugura en Colombia.]

[Los mismos defensores de los animales que participaron en las marchas hablaron de personas infiltradas en las protestas y de los miembros del mismo Esmad que sabotearon su protesta pacífica. En muchas movilizaciones se han descubierto miembros de algún organismo de seguridad del Estado haciéndose pasar por manifestante. Y en no pocas oportunidades, tal como ocurrió con el paro campesino,  son los mismos miembros del Esmad de la policía los que se encargan de romper los vidrios de los buses y de agredir a quienes participan en las justas protestas. Estas violaciones a los derechos humanos pasan desapercibidas o son apenas registradas por los medios empresariales, a pesar que  las pruebas visuales se vuelven virales en las redes sociales.]

[El establecimiento y sus  medios de información deben comprender, también que la protesta es un derecho y que justo el cierre de los espacios para la movilización de la sociedad es el que a asfixió a muchos sectores llevándolos hasta la explosión en la rebelión armada. He ahí la inmensa irresponsabilidad del periodista.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.

 
