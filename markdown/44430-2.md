Title: Atentan contra dos líderes de la Consulta Popular en Cajamarca
Date: 2017-07-29 20:35
Category: Ambiente, DDHH
Tags: Ambientalista, Cajamarca, consulta popular, Lideres, Proyecto La Colosa
Slug: 44430-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [29 Jul. 2017] 

El Colectivo Socio-Ambiental Juvenil de Cajamarca  (COSAJUCA) dio a conocer a través de un comunicado que **2 de sus miembros sufrieron un atentado este viernes en el casco urbano de Cajamarca**. Según la información entregada, los dos líderes, que son promotores de la Consulta Popular que se realizó en ese municipio el pasado 26 de marzo, resultaron ilesos.

Se describe en la comunicación que los hechos se produjeron sobre las 7:30 p.m. cuando los líderes regresaban por el camino que conduce a las veredas Rincón Placer y la Ciudadela  Ismael Perdomo, lugares donde COSAJUCA realiza trabajo con acueductos comunitarios. **Los 2 disparos fueron hechos desde una casa** en inmediaciones de la Carrera 9 de ese casco urbano.

Por tal motivo, **COSAJUCA ha exigido a las autoridades que actúen de manera eficaz dando con los autores intelectuales y materiales** de este grave hecho, que pone en riesgo una vez más la vida de los y las defensoras del ambiente en Colombia. Le puede interesar: [Desde las entrañas de la tierra y junto a los gritos del agua, Cajamarca dijo NO a la minería.](https://archivo.contagioradio.com/cajamarca-consulta-minera/)

Así mismo han exigido se garantice la seguridad y la vida de estos dos líderes de COSAJUCA y para su familias de tal modo que “puedan seguir realizando nuestro trabajo como defensores de derechos humanos y del territorio”. Le puede interesar: [Ecos de la consulta popular de Cajamarca](https://archivo.contagioradio.com/ecos-de-la-consulta-popular-de-cajamarca/)

**Este hecho se suma a las amenazas que este colectivo ha recibido desde hace 4 años** “por defender nuestro territorio del proyecto minero La Colosa detenido gracias a la histórica consulta popular”.

De igual modo recordaron que como resultado de esa labor han asesinado a 2 de sus integrantes, Juan Camilo Pinto en 2013 y Daniel Humberto Sánchez en 2014. Le puede interesar: [Colombia es el segundo país más peligroso para defensores del ambiente](https://archivo.contagioradio.com/en-colombia-fueron-asesinados-37-ambientalistas-en-2016/)

![Cajamarca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/WhatsApp-Image-2017-07-29-at-3.59.01-PM.jpeg){.alignnone .size-full .wp-image-44431 width="989" height="1280"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
