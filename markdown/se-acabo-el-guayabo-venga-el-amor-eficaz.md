Title: Se acabó el guayabo, venga el amor eficaz
Date: 2016-01-22 08:54
Category: Camilo, Opinion
Tags: acuerdos de paz, desigualdad, FARC, Mineria
Slug: se-acabo-el-guayabo-venga-el-amor-eficaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/desigualdad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: **Cazadores de Graffitis**] 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas)

<div style="text-align: justify;">

[(]21 Ene 2016[)]

</div>

Vueltos a la  realidad o somnolientos en la adobada agenda mediática que nos hace sentir felices. Una gran paradoja ad portas de un Acuerdo de terminación del conflicto armado del gobierno con las FARC EP y el inicio de las conversaciones con el ELN, se profundizan las desigualdades sociales y la destrucción ambiental.

ZIDRES como definición del usos de la altillanura, un contra golpe a la eventual Zona de Reserva Campesina y la redistribución de tierras; proyección de nuevos Quimbos con los que se disfrazan los efectos nefastos sobre nuestros ríos y aguas y la crisis ambiental  y social generada por APP en el marco de los TLC; 4G para los postores privados, entre ellos, Sarmiento Angulo, uno de los beneficiados con la alcaldía de Peñalosa; venta de 52% de lo que nos quedaba de Isagen; Reforma tributaria con nuevas cargas impositivas, acelerando la caída de los sectores medios y la profundización de la inequidad; crisis de la salud con nuevas apropiaciones para conglomerados económicos en medio de nuevas formas de tránsito hacia la muerte indigna con atención dilatadas por el maldito dinero, como el caso de doña Rubiela con una respuesta represiva al llanto de sus familiares por generar un trancón. Se acabó tangencialmente el país del festejo nacional de la economía más prospera de América, el dato nada despreciable  es que Colombia es el país más afectado, según la CEPAL, en la caída de exportaciones e intercambio en cerca de 71.9. El espejismo del crecimiento por exportaciones de materias primas \[carbón y petróleo\] se ha ido cayendo

En medio de esa crisis social y ambiental, signos de aplicación básica del derecho humanitario aplicable y exigible a las partes combatientes. A un clamor de aplicación de condiciones humanitarias a privados de la libertad en cárceles colombianas, y el acuerdo de un indulto, y después de años de dilación, 12 de 30 excombatientes de las FARC EP logran su libertad para ser gestores hacia la paz con una concesión de aplicación de las normas internacionales por parte del gobierno de Santos. Y hasta el momento un silencio de ese mismo gobierno ante el planteamiento de esta misma guerrilla de dar el mismo trato a 30 militares, que se encuentren en condiciones no humanitarias, similares a la de sus combatientes. A un escrito del ELN de hace 12 días en el que se pide conocer el paradero y la entrega de los restos del sociólogo y sacerdote Camilo Torres Restrepo, una pronta respuesta del gobierno de Santos de ubicar los restos. Paradojas, gestos humanitarios en el conflicto armado, absolutamente nada en el conflicto socio ambiental. Deudas pendientes con los civiles víctimas y re victimizados, en particular, del Estado con los más de 45 mil desaparecidos forzados, todas las actuaciones con serias limitaciones institucionales por falta de voluntad política, de burocratización y ausencia de financiación

La vida toda convertida en mercancía, los bosques, las aguas, las tierras, la salud, la educación, la comunicación y el propio camino hacia la Pax Neoliberal con fuerzas en contravía para el intento de construcción de un nuevo proyecto de sociedad, sensible, solidaria, ética en lo social y en lo ambiental. Unas guerrillas tratando de lograr lo básico y lo sustancial para aportar a esa construcción de sociedad, pasando por el reconocimiento de los límites del derecho a la rebelión y sus exabruptos en el ejercicio de ese derecho, actitud ética, poco reconocida por el país y en medio de un gobierno que se ufana de ser el de las víctimas, y que obra en contravía de los derechos de las víctimas, espejismo,  relegitimado por la reciente visita del presidente del Banco Mundial. Una pluralidad de nuevas expresiones del movimiento social pataleando frente a serias restricciones a sus conquistas, como las decisiones contra las trabajadoras sexuales; un movimiento social tradicional entre tensiones e intentos de convergencia limitada por los egos, las metodologías, las diferencias ideológicas, que siguen siendo superiores a la tozuda realidad.

El hambre, la crisis ambiental, la exclusión no es azul o verde, ni roja y negra, ni amarilla y verde, el hambre y la injusticia social y ambiental es de quién la padece y la sufre, y no hay más posibilidad que quienes aún nos sentimos, nos llamamos a cambiar a nosotros mismos y a esa injusticia estructural, urge en soñar construir, insistiendo “en todo lo que nos une, y prescindir de todo lo que nos separa”, y saben quién lo expresó. Esos seres humanos bellos, que somos, que llevamos dentro, que necesitamos reconocer, que necesitamos perfeccionar en la libertad, para desatar desde el reconocimiento de lo fracasado, de lo limitado y del potencial que tenemos en el amor eficaz, el transformador de la propia vida y del sistema, sin arandelas, sin confort, solo libertad.
