Title: "Código de Policía tiene vicios de procedimiento y vulnera DDHH": Iván Cepeda
Date: 2016-09-08 14:16
Category: Judicial, Nacional
Tags: código de policía, Corte Constitucional, ESMAD, posconflicto
Slug: codigo-de-policia-tienes-vicios-de-procedimiento-y-vulnera-ddhh-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/codigo_de_policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

###### [8 Spe 2016] 

Hoy se radicó ante la Corte Constitucional una demanda contra el recién sancionado código de policía, interpuesta por organizaciones sociales y algunos congresistas quienes aseguran que este **nuevo código es dictatorial y afecta los derechos humanos de la ciudadanía.**

La demanda formula cargos por vicios de fondo contra varias disposiciones de la Ley 1801 de 2016 y se divide en dos partes: En la primera **se demandan algunas [disposiciones referidas a la protesta socia](https://archivo.contagioradio.com/este-codigo-de-policia-es-para-la-dictadura-no-para-el-posconflicto-alirio-uribe/)l**, ya que violan de forma directa los artículos 29 (principio de legalidad) y 37 (derecho a la manifestación pública y pacífica) de la Constitución, así como el artículo 21 del Pacto Internacional de Derechos Civiles y Políticos.

En la segunda parte **se demandan varias [normas que otorgan facultades a la Policía](https://archivo.contagioradio.com/debate-en-el-eclectico-nuevo-codigo-de-policia/)** para trasladar o conducir a los ciudadanos afectando la libertad personal reconocida por la Constitución de 1991.

“La policía es un organismo que está altamente cuestionado e investigado por [múltiples escándalos, por corrupción y violaciones a los derechos fundamentales](https://archivo.contagioradio.com/organizaciones-de-derechos-humanos-demandaran-codigo-de-policia/), y en vez de producir una reforma a la institución que es lo que debería ocurrir, **se decide crear un código que le da inmensas atribuciones a la policía”**, dice el senador Iván Cepeda, quien añade que, “**las autoridades podrán allanar el domicilio de cualquier ciudadano,** porque no habrá ningún control para que se pueda verificar que se está cumpliendo con los criterios del Código, además podrán impedirle a los ciudadanos que participen de una movilización social”.

Se trata entonces de una iniciativa que muchos sectores esperan sea atendida "en virtud de los tiempos que paz que se acercan", pues **se convierte en un código "para dictaduras,** y que [no sirve para el posconflicto ni la convivencia ciudadana](https://archivo.contagioradio.com/este-codigo-de-policia-es-para-la-dictadura-no-para-el-posconflicto-alirio-uribe/)", expresa el representante Alirio Uribe.

La acción fue presentada por los congresistas del Polo Democrático Alternativo, Alexander López, Víctor Correa, Alirio Uribe Muñoz e Iván Cepeda, así como por la Representante a la cámara del Partido Verde, Ángela María Robledo. Así mismo hacen parte de esta demanda, la **Comisión Colombiana de Juristas, Colectivo de Abogados José Alvear Restrepo y Observatorio de Derechos Humanos de la Coordinación Colombia Europa Estados Unidos**, son los promotores de esta iniciativa.

<iframe src="https://co.ivoox.com/es/player_ej_12830752_2_1.html?data=kpellZWbeZOhhpywj5WbaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVj5C90dHTb6XZztTQ1Iqnd4a1ktnWxdSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
