Title: En memoria de Berta Cáceres y todas las rebeldías
Date: 2018-03-08 10:01
Category: CENSAT, Opinion
Tags: Ambientalismo, ambientalistas asesinados, Berta Cáceres, mujeres
Slug: en-memoria-de-berta-caceres-y-todas-las-rebeldias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/mujeres-defensoras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Encuentro de Mujeres Tejiendo el Buen Vivir./ El Cuarto Mosquetero 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 8 Mar 2018 

[ A medida que recorríamos la parcela íbamos reparando en la frontera marcada por el verde de todos los tonos que se iba instalando, mientras, alrededor de ella, permanecía un paisaje fracturado por las laderas quebradizas y resecas resultado del monocultivo de la piña.  Esa frontera es una ruptura y como diría una de las mujeres asistentes al Encuentro-Taller Nacional: Mujeres Tejiendo el Buen Vivir,]*[es una consigna de resistencia.]*

[Esa fractura tan visible durante el Encuentro hace parte de una de las herencias de la modernidad en la que se instaló o acentuó una narrativa binaria, a través de la que se ha justificado y agudizado una relación de dominación de parte de uno de los lados del binomio, agricultura tradicional/revolución verde, mujer/hombre, naturaleza/civilización, emoción/razón,  tradición/modernidad, subjetividad/objetividad; y podríamos seguir enumerando pares, en los que la constante es que uno de esos lados pretende imponerse a través de la cultura, la política, la estética, la ética, la técnica y el conocimiento sobre el otro, como parte de un sistema estructural que reproduce la desigualdad.]

[Para el caso específico de las mujeres, esta ruptura ha servido para apartarlas de espacios y adelantos que se han construido del otro lado de la grieta, pero más importante aún, les ha hecho creer, por un buen tiempo, que los conocimientos, sistemas y estrategias que brotaban a partir de su propio potencial eran ilegítimos o simplemente inviables.]

[Sin embargo la narración que construyen las mujeres desde las defensas territoriales que llevan a cabo,  les ha servido para ampliar el margen de esa visión del mundo que han construido y llevarla más allá de la frontera doméstica que les fue asignada.  Mujeres defensoras del agua, mujeres guardianas, mujeres cuerpo-territorio, mujeres cuidadoras, mujeres comunicadoras, mujeres profesionales, mujeres custodias de semillas, mujeres líderes; posicionando una conciencia y una práctica ecológica de género, fruto de las divisiones de trabajo y los roles sociales concedidos.  El rol asumido culturalmente por muchas mujeres campesinas, afrodescendientes e indígenas ha sido el del cuidado y autocuidado, el mantenimiento de sistemas agrícolas y pecuarios que aseguren el alimento de su familia y comunidad, la generación de circuitos de intercambio y la protección del agua. Estos conocimientos son hoy en día parte de  las alternativas urgentes para la transformación del actual modelo de desarrollo, reclamado por las comunidades que protegen los territorios y que invocan a una transición justa hacia unas nuevas relaciones sociedad /naturaleza, que rompan con esa mirada dual que nos aísla.]

[Las transiciones posextractivas son amplias, no sólo se trata de cambiar los energéticos que han dominado el mundo en los últimos siglos: carbón y petróleo por paneles solares o molinos de viento; si no que a esta transición se deben sumar las transformaciones culturales, simbólicas, económicas, sociales y tecnológicas, que replanteen el metabolismo social de esta sociedad que consume incesantemente materia y energía a unas velocidades insustentables para la vida en el planeta.  La transformación también involucra el pensamiento y la construcción de conocimiento gestado desde las mujeres y desde esas otras orillas que se instalaron al otro lado del binomio (naturaleza, emoción, subjetividad).]

[Nos referimos en últimas a un cambio de paradigma donde a los saberes y sistemas locales se les garantizaría la autonomía y soberanía, así como su establecimiento en todos los espacios de decisión y construcción como parte de los elementos valiosos para la comprensión del mundo.  Muchos de los sistemas locales mediante los cuales las comunidades aún permanecen en los territorios, son nutridos por el trabajo de las mujeres, las cuales han garantizado que el ciclo de estos ejercicios sea vital, constante y se adapte según las circunstancias.]

[Las mujeres han hecho visible su potencial a través de la permanencia y defensa del territorio, sus voces se hacen escuchar en contra de la minería, la privatización de acueductos comunitarios y la ampliación de desiertos verdes (monocultivos), también a favor de la promoción de mercados agroecológicos y mecanismos participativos (consultas populares), la protección  de selvas y ríos, la custodia de semillas y la defensa e impulso del arte y la artesanía; todo esto para frenar el avance minero-energético. Los espacios de encuentro e intercambio, reafirman cada vez más, que son las mujeres las que al tomar la decisión de transformar el territorio que rodea a sus familias, transforman las dinámicas patriarcales que habían dominado su voz y su propia autonomía.]

[En este día y durante todos los días, celebramos a las mujeres que han decidido ampliar su voz, y celebramos todas esas rebeldías que se levantan con ellas  al otro lado de la grieta para resistir, para sembrar y avanzar a punta de vida.]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
