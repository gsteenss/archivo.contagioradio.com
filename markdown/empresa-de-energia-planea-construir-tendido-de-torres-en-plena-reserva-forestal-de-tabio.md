Title: Empresa de Energía planea construir tendido de torres en plena reserva forestal de Tabio
Date: 2015-03-30 20:56
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, ANLA, Autoridad Nacional de Licencias Ambientales, Cundinamarca, Empresa de Energía, especies en vías de extinción, Fauna silvestre, reserva forestal, Tabio, tigrillo, torres de energía
Slug: empresa-de-energia-planea-construir-tendido-de-torres-en-plena-reserva-forestal-de-tabio
Status: published

##### [Foto: www.pulzo.com]

<iframe src="http://www.ivoox.com/player_ek_4284534_2_1.html?data=lZellpqXeI6ZmKiak5uJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIb8XZjLnOxM7Tb8XZz9rbxc7Fb8rh0dHSz8rSuMLXyoqwlYqmd8%2BfxcqYlJuUb9Xj09eah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alexander Fonseca, líder comunitario de Tabio] 

La posible construcción de un **tendido de 260 torres de energía en plena reserva forestal, de Tabio, Cundinamarca, tiene preocupada a la comunidad** de ese municipio, quienes denuncian que la licitación que ganó la Empresa de Energía de Bogotá, se logró mediante un diagnóstico ambiental de alternativas “**a todas luces sesgado**, que no contempla todas las variables ambientales y tiene datos desactualizados que no concuerdan con la realidad”, asegura Alexander Fonseca, líder comunitario.

El proyecto denominado “**Construcción de la línea de transmisión CHIVOR-CHIVOR II – NORTE – BACATÁ A 230 KV**”, radicado en la ANLA (Autoridad Nacional de Licencias Ambientales) con el número de expediente NDA 0907, contempla un tendido con 260 torres de energía en un perímetro de 8 kilómetros del municipio. Cada torre a 30 metros de distancia, “**nos van a  llenar el municipio de torres de energía”**, expresa el líder comunitario.

Según Alexander, se escogió Tabio ya que “para la Empresa de Energía es más económico comprar predios por reserva forestal que por los corredores de desarrollo donde la tierra es más costosa”.

La construcción de este tendido de torres, serealizaría para transportar energía desde la represa de Chivor en Boyacá, pasando por municipio de Boyacá y Cundinamarca, por lo habría una subestación en el municipio de Tenjo para entregar energía a departamentos como Meta, Cundinamarca, Tolima y Boyacá.

De acuerdo a Fonseca “la comunidad no está en contra del desarrollo sino de que nos vulneren nuestros derechos al ambiente sano, **nos están castigando por conservar”**, pues la comunidad ya ha advertido por medio de cartas sobre los efectos negativos que traería la realización de este proyecto, sin embargo, afirman que no ha habido ningún apoyo por parte de las autoridades ambientales.

Entre las consecuencias más preocupantes que enumeran los habitantes de Tabio, se cuenta las afectación y amenaza a los acueductos que abastecen de agua el municipio, fragmentación de los bosques de la reserva forestal como incendios, cambios microclimáticos, erosión del suelo, efectos en la salud humana, transformación del paisaje, electrocución de aves, y además, se pondría en peligro la fauna silvestre, teniendo en cuenta que **en esa zona habitan especies en vías de extinción como tigrillos.**

Por otro lado, también abrían impactos económicos como la disminución en el turismo, la desvalorización de los predios por donde pase las líneas, migración, posibilidad de reubicación a las personas que el proyecto lo requiera, se encarecerá el agua y habría limitantes productivas en la zona de influencia del proyecto, entre otros.

Pero este no es el primer proyecto que se le trata de imponer al municipio de Tabio, ya que los pobladores han logrado defender la reserva forestal de la implementación de un poliducto de petróleo, intentos de abrir minas de carbón en veredas donde hay nacimientos de agua, y ahora luchan en contra de que se construya esta línea de alta tensión.

Por el momento, la comunidad va a estar unida para defender su territorio, y no van a permitir que funcionarios de la Empresa de Energía ingresen a sus fincas, ya que en un principio, los empleados de la empresa entraron a los predios gracias a que “engañaban a los propietarios”, dice Fonseca.

Los terceros intervinientes que están pendientes del caso son el alcalde del municipio, el secretario de gobierno y el líder comunitario, Alexander Fonseca, quien afirma que la comunidad se ha sentido sola y sin respaldo de las autoridades ambientales, que son quienes deberían velar por la defensa de reservas ambientales como esta.
