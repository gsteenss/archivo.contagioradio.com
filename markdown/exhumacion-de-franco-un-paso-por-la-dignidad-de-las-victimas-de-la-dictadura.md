Title: Exhumación de Franco: Un paso por la dignidad de las víctimas de la dictadura
Date: 2019-10-27 12:14
Author: CtgAdm
Category: El mundo, Memoria
Tags: dictadura, españa, Franco, Valle de los caidos
Slug: exhumacion-de-franco-un-paso-por-la-dignidad-de-las-victimas-de-la-dictadura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Franco-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @carnecrudaradio] 

El pasado jueves 24 de octubre, 44 años después de su muerte, fue exhumado el dictador español Francisco Franco de su monumento en el Valle de los Caídos, calificado como el único monumento del mundo al fascismo. En entrevista con Contagio Radio, el abogado defensor de derechos humanos y diputado español Enrique Santiago explicó lo que significa esta exhumación, y por que considera que es solo un primer paso de una serie que deben darse, en aras de garantizar los derechos a la verdad, justicia, reparación y garantías de no repetición de las víctimas de la dictadura.

### **Contagio Radio: ¿Qué significa el Valle de los Caídos, y por qué es tan importante que se haya sacado a Franco de allí?** 

Enrique Santiago: El  Valle de los Caídos es la tumba que Franco diseñó para él mismo en los años 50, al acabar la Guerra Civil. El dictador megalómano, con ínfulas de emperador, diseñó un monumento funerario emulando las pirámides de Egipto. Eso es el santuario de Cuelgamuros, que es la denominación original, que ahora es el valle de los caídos.

Allí se enterró en un primer momento a José Antonio Primo de Rivera, el fundador de la falange, y a personas que combatieron en la guerra civil, sin orden. En un primer momento estaba destinado para personas que fallecieron combatiendo en el bando golpista, pero posteriormente, el régimen en un afán de esconder los crímenes cometidos, trasladó sin identificar a fallecidos del bando republicano, cuyas familias ignoran su destino y si están entre los restos que están en el Valle, o no.

A su fallecimiento, el dictador fue enterrado con honores propios de emperador. Es el único monumento que existe en el planeta de un dictador fascista, además, teniendo en cuenta que el régimen  fue declarado contrario a la legalidad internacional por las propias Naciones Unidas. Pero es un caso único, no existe un lugar de homenaje y enaltecimiento del fascismo como este en ningún lugar del planeta.

Con mucho retraso, 45 años después del fallecimiento del dictador, por fin un gobierno en España ha accedido a las reclamaciones del movimiento de víctimas de crímenes del Franquismo, que desde que llegó la democracia viene pidiendo verdad, justicia, reparación y no repetición. Tras muchas movilizaciones y una ardua reclamación se ha podido dar este primer paso; que es simplemente sacar al Dictador del Valle de los Caídos porque el monumento sigue existiendo, allí sigue estando enterrado con honres militares el fundador de la Falange, José Antonio Primo Rivera, el equivalente al fundador del partido Nazi. Y sigue siendo un lugar de enaltecimiento del golpe de Estado de los sectores que participaron en lo que denominaron la cruzada, incluyendo la iglesia católica.

Desgraciadamente entre las mayores resistencias para proceder a la exhumación del Dictador está, precisamente, la de la orden de los Benedictinos, que son los que cuestionan el lugar y el interior de la basílica. Y que aún mantienen un espectáculo lamentable, que nadie entiende en España como no ha sido destituido directamente por la Conferencia Episcopal Española o por el Vaticano, y no hacerlo significa una cierta complicidad con esa iglesia, que fue la instigadora de Franco.

Nosotros, como defensores de derechos humanos, hemos exigido que esto no se quede simplemente en un gesto sino que se aplique la Ley de Memoria Democrática, que exige acabar con este tipo de monumentos, pero esta Ley no contempla ningún tipo de sanción a quien no cumpla sus contenidos. De igual forma, pedimos la aprobación de una ley integral de derechos de las víctimas, que garantice el derecho a la verdad, la justicia, la reparación y la no repetición, porque los cálculos hablan de más de 100 mil ejecutados políticos, desaparecidos, enterrados en fosas comunes sin exhumar.

Es decir, una situación anómala, solamente nos superan en el mundo, en cuanto a desaparecidos no identificados, Camboya. Y en todo caso, en Camboya hay una política de recuperación de restos, cosa que no existe en nuestro país porque no hay una norma que así lo exija. Una situación muy anómala para un país europeo, para un país democrático; lo que impide que en nuestro país haya un proceso de reconciliación después del fin de la dictadura, que permita mirar hacía el futuro y construir conjuntamente.

### **CR: ¿Por qué se saca al dictador ahora, *ad portas* de las elecciones de noviembre?** 

ES: Es triste decirlo, pero el partido socialista ha demorado estos últimos 44 años tomar esta decisión, ha demorado cumplir la Ley de memoria histórica, ha bloqueado reformas a esta Ley para que sus contenidos sean de obligatorio cumplimiento. Y ha bloqueado en 3 ocasiones una proposición de Ley presentada por Izquierda Unida, la Ley Integral de Memoria Democrática y derecho de las víctimas de los crímenes del Franquismo.

Entonces que tras haberlo bloqueado, hayan acometido la exhumación en medio de la campaña electoral, y además, dejando al fundador de la Falange o a los miles de personas sin identificar, trasladándose sin permiso de los familiares, pues pone de manifiesto que el partido socialista más que un convencimiento de garantizar los derechos de las víctimas, lo que quería era entrar en campaña electoral y obtener rentabilidad política con estas imágenes.

Pero además, unas imágenes que han sido lamentables porque ha terminado casi en una especie de funeral de Estado, con gritos al Dictador, y con una ministra del Gobierno y que con la retransmisión por la televisión que parecía una ceremonia de Estado cuando debió ser todo lo contrario, un acto de reparación. (Le puede interesar: ["No puedo hacer algo más satisfactorio que contribuir a acabar con el conflicto” Enrique Santiago"](https://archivo.contagioradio.com/enrique-santiago-evalua-acuerdo-final-28458/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
