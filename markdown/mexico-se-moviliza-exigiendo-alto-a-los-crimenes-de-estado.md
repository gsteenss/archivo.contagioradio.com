Title: México se moviliza exigiendo ¡Alto a los crímenes de Estado!
Date: 2015-11-18 17:08
Category: El mundo
Tags: Ayotzinapa, crímenes de estado, Frente Nacional de Lucha por el Socialismo, Marcha contra crímenes de estado México, mexico
Slug: mexico-se-moviliza-exigiendo-alto-a-los-crimenes-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Impunidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:diarioextra.com 

<iframe src="http://www.ivoox.com/player_ek_9433129_2_1.html?data=mpmglZaWfY6ZmKiakp2Jd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkYa3lIqum93Np9Cf1MqYz9Tarc3d28aYx93Nq8rZz8ncjYqndoa1kqbZ1tSPpYzg0NiYxdeJh5SZoqnax9PJt4zYxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Francisco Cerezo] 

###### [18 Nov 2015]

[Desde las 10 de la mañana de este miércoles, la ciudadanía mexicana se movilizó bajo el lema **¡Alto a los crímenes de Estado!** en rechazo a los **últimos hechos presentados en ese país y** principalmente contra la desaparición forzada que según cifras oficiales asciende a **30.000 personas, pero de acuerdo a las organizaciones sociales ya suma 120.000 víctimas**.]

[Francisco Cerezo, integrante del Comité Cerezo, indica que el último caso que se presentó fue el 7 de noviembre cuando **tres indígenas defensores de derechos humanos, fueron baleados por la policía**, dejando a uno de ellos en grave estado de salud por una bala que alcanzó su columna, los otros dos sufrieron disparos en el estómago y en la pierna, relata Cerezo, quien añade que además el Estado mexicano continúa impulsado campañas de desprestigio contra el movimiento social.]

[Una de estas situaciones es la del Consejo Ciudadano para la Seguridad Pública Penal que **criticó el trabajo de los delegados de la Comisión Interamericana de Derechos Humanos la cual adelanta investigaciones en el caso Ayotzinapa** y defendió al Ejercito. Frente a esto, Francisco Cerezo afirma que son críticas que vienen de “grupos de ultraderecha” **que tratan de crear una campaña de desprestigio** contra los defensores de las víctimas, incluso tachándolos de terroristas, siendo una situación generalizada contra los defensores de derechos humanos.]

Así mismo, esta marcha se realiza en el marco de las agresiones de la fuerza pública hacia estudiantes de la Normal Rural de Ayotzinapa, en la que al menos ocho jóvenes resultaron heridos durante choques con la policía en la carretera que conecta a las ciudades de Chilpancingo (capital) y Tixtla, en el estado de Guerrero.

Precisamente uno de los heridos es Ernesto Guerrero Cano, sobreviviente de los hechos ocurridos en Iguala en septiembre de 2014, cuando desaparecieron 43 estudiantes. Guerrero se encuentra en estado grave a causas de golpes propinados por los agentes de seguridad.

La marcha partió desde la colonia **San Jerónimo hasta el Comisión Nacional de los Derechos Humanos, CNDH.**
