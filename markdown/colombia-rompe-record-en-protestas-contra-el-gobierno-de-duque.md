Title: Colombia rompe record en protestas contra el gobierno de Duque
Date: 2020-11-10 15:14
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Gobierno Duque, ministerio de defensa, protestas
Slug: colombia-rompe-record-en-protestas-contra-el-gobierno-de-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/74802282_1249874088532329_945839746624520192_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Andrés Zea/Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según un informe presentado por el [Ministerio de Defensa](https://www.mindefensa.gov.co/irj/portal/Mindefensa) y entregado a Ricardo Ferro, representante a la Cámara por el Centro Democrático, durante el gobierno Duque se han registrado al menos 25.879 protestas. Lo que indica que este es uno de los gobiernos más impopulares de la historia reciente en Colombia y América Latina.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe indica que **desde el 7 de agosto del 2018, fecha en que se posesionó Iván Duque, hasta el 25 de octubre del presente año, se han registrado 25.879 movilizaciones entre huelgas, paros, bloqueos y asambleas; esto equivale a más de 32 movilizaciones por día en los últimos dos años.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta cifra, supera el número de protestas entre los años 2014 y 2018, tiempo en que se registraron 14.338 movilizaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, del total de movilizaciones que se registraron, el Escuadrón Móvil Antidisturbios -ESMAD- intervino en 2.607 casos, es decir el 10% del total.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El documento, además señala que desde febrero hasta el 15 de octubre solo en Bogotá se registraron 284 protestas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1324677395148902400","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1324677395148902400

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading -->

Ministerio defiende uso de escopeta calibre 12 en las protestas
---------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

A pesar de las decisiones judiciales y de las muestras claras del mal uso de a escopeta calibre 12, el Ministerio defiende el uso de la escopeta calibre 12 en medio de las protestas; misma arma con la que fue asesinado el joven de 19 años, Dilan Cruz.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **El empleo de la escopeta calibre en medio de los procedimientos de control de disturbios permitieron controlar situaciones que representaron un peligro inminente para la vida, debido a la utilización de elementos como artefactos explosivos improvisados.**
>
> <cite>Informe </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y agrega que «es una herramienta disuasiva en las intervenciones violentas, donde los otros elementos menos letales no han sido efectivos para detener mencionada acción». (Le puede interesar: [Suspensión de uso de gas lacrimógeno: Un freno más a la represión policial](https://archivo.contagioradio.com/suspension-de-uso-de-gas-lacrimogeno-un-freno-mas-a-la-represion-policial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta cifra de protestas y manifestaciones seguirá aumentando con el Paro Nacional del próximo 21 de noviembre, que vuelven a convocar organizaciones sociales, centrales obreras, estudiantes y muchos otros sectores del país exigiendo acciones por parte del Gobierno Nacional para atender la crisis de derechos humanos que atraviesa el país y que se ha agudizado a causa de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
