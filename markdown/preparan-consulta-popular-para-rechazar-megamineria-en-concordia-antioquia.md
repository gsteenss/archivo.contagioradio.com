Title: Preparan consulta popular para rechazar megaminería en Concordia, Antioquia
Date: 2016-08-08 14:38
Category: Ambiente, Nacional
Tags: Antioquia, Concordia, defensa del ambiente, Mineria
Slug: preparan-consulta-popular-para-rechazar-megamineria-en-concordia-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-e1457467593828.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Macondo 

##### [8 Agos 2016]

Este fin de semana cerca **3.000 pobladores del municipio de La Concordia** en el departamento de Antioquia salieron a movilizarse **en rechazo a las actividades de megaminería que ya se adelantan en cerca del 80% del suroeste antioqueño** que se encontraría concesionado principalmente para la multinacional Anglo Gold Ashanti.

De acuerdo con Catalina Avendaño, integrante del Comité Concordiano por la defensa del Territorio, aproximadamente **hay 16 veredas sobre las cuales se tiene solicitudes para llevar a cabo proyecto mineros**, en territorios que ancestralmente han tenido vocación agraria, y cuyas tradiciones la población no quiere cambiar por actividades mineras que han demostrado generar graves efectos ambientales, económicos y culturales.

Según han denunciado pobladores de la zona, la entrada de multinacionales al territorio es evidente. Por ejemplo en Jericó, la maquinaria para estas actividades  ya está en funcionamiento desde hace más de 5 años, lo que ha **secado acueductos comunitarios y afectado acuíferos por las labores con los taladros para realizar exploración miner**a donde se ha encontrado, cobre, plata y oro, generando que la producción de agua se haya minimizado en un 50%.

Asimismo se denuncia que **las empresas están llevando a cabo exploración minera por medio de engaños para ingresa a los predios**. “Llegan en 3 o 4 vehículos, dicen que son funcionarios de gobernación  o a veces del Ministerio de Ambiente, toman muestras de los nacimientos de agua, sacan tres o 4 bolsas de tierra y se van con eso para hacer la prospección”, explica Catalina Avendaño, quien agrega que en la cabecera municipal de Buriticá, el agua ya está contaminada con mercurio.

Por su parte, el alcalde de Concordia se encuentra a favor de las demandas de los habitantes que rechazan la minería, pues “entiende que se debe conservar la vocación agropecuaria y  las fuentes hídricas”. Además, aseguran que continuarán con las acciones de movilización y paralelo a ello, **tramitan una solicitud ante la Registraduría para realizar una consulta popular en el municipio,** “Sabemos que la minería legal e ilegal es completamente devastadora”, expresa Avendaño.

<iframe src="http://co.ivoox.com/es/player_ej_12479914_2_1.html?data=kpehmZ6ddZWhhpywj5aVaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncaTV1cbZy9PFb6LqxtPRw4qnd4a2ktSSlKiPh9DhytmSpZiJhZqfpNTbxdTWqMrVz9SY0tTWb83VjMnSyMrSt8KfjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
