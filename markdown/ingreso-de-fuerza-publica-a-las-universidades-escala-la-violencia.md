Title: Ingreso de Fuerza Pública a las universidades escala la violencia
Date: 2020-02-20 16:21
Author: AdminContagio
Category: DDHH, Estudiantes
Tags: ESMAD, estudiantes, Fuerza Pública, Universidades
Slug: ingreso-de-fuerza-publica-a-las-universidades-escala-la-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-06-at-18.03.08.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/alexandra-gonzales-sobre-ingreso-del-esmad-a-universidad_md_48221880_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Alexandra González | Integrante del Colectivo de Solidaridad con los Presos Políticos (CSPP)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Esta semana se conocieron algunas de las modificaciones que tendrá el protocolo para atender la manifestación en Bogotá, entre las medidas se encuentra la posibilidad de que la Fuerza Pública entre a los centros universitarios así como que estén cerca de los campus de las Universidades Públicas, para reaccionar cuando la Alcaldía lo considere necesario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las nuevas acciones del protocolo se asemejan a las ya emprendidas por la Alcaldía de Medellín, en cabeza de Daniel Quintero Calle, que permite el ingreso de la Fuerza Pública a los centros universitarios cuando haya indicios de la utilización de artefactos explosivos en los mismos. Defensores de derechos humanos y estudiantes rechazan estas medidas, porque consideran que escala la violencia, y puede afectar a los estudiantes.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F785936038843198%2F&amp;width=500&amp;show_text=false&amp;appId=2104425419856362&amp;height=275" width="500" height="275" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:heading {"level":3} -->

### **Críticas al 'protocolo' de Claudia López**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado miércoles 19 de febrero la Personera Distrital señaló que las medidas implementadas por la alcaldía de Claudia López no eran un protocolo, pues no había un acto administrativo que lo creara, sino unas medidas que legítimamente podía tomar la Administración de Bogotá. Sin embargo, señaló que estas medidas no habían sido satisfactorias en aras de garantizar el derecho a la protesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mismo sentido, Alexandra González, integrante del Colectivo de Solidaridad con los Presos Políticos (CSPP) recordó que el protocolo vigente para manejar la protesta está formalizado en el Decreto 563 de 2015, por lo que solo se necesita aplicar adecuadamente esta norma, en lugar de crear otra nueva. (Le puede interesar:["Protocolo de Claudia López para la protesta sin resultados satisfactorios: Personería"](https://archivo.contagioradio.com/protocolo-de-claudia-lopez-para-la-protesta-sin-resultados-satisfactorios-personeria/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

González también cuestionó que las medidas de López no hayan sido consultadas ni socializadas con organizaciones defensoras de derechos humanos, aunque sostuvo que el próximo 2 de marzo esperan que se instale la Mesa de Seguimiento a la protesta con la Alcaldía, en la que espera que se puedan discutir estos temas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lo que significa el ingreso de la Fuerza Pública a los campus universitarios**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Rafael Núñez, integrante del Movimiento por la Defensa de los Derechos del Pueblo (MODEP), manifestó que las declaraciones del Alcalde de Medellín, diciendo que ingresarán a las universidades cuando haya elementos explosivos era una forma más de estigmatizar a los estudiantes, porque no son ellos quienes tienen capacidad para tener este tipo de elementos en su poder.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En cambio, aseguró que el ingreso de la Fuerza Pública a los campus universitarios ha significado en el pasado atropellos, montajes de falsos positivos judiciales y hace 5 años, en la Universidad de Antioquia, significó que un estudiante estuviera en coma por los golpes que le dio la Policía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nuñez agregó que la autonomía universitaria está consagrada en el[artículo 69](http://www.constitucioncolombia.com/titulo-2/capitulo-2/articulo-69) de la Constitución porque las universidades son reconocidas como constructoras de la Nación a partir del conocimiento, de allí que se busque reducir al máximo la intervención de Gobiernos, para proteger ese espacio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Cómo acabar con la violencia?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El integrante del MODEP señaló que si bien hay grupos de personas que ingresan a las universidades públicas para adelantar acciones violentas, también es cierto que esas personas no habitan en los campus, es decir, tienen otras actividades en las ciudades que la inteligencia de Policía debería estar en capacidad de identificar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, Núñez sostuvo que el ingreso de la Fuerza Pública a los campus escalaría la violencia en los territorios académicos. Contrario a ello, propuso que se crearan foros, espacios académicos y de discusión sobre la utilización de la violencia en el marco de las protestas para hacer llamados tendientes a desescalar el conflicto y sacar la violencia de las Universidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Respecto a la Fuerza Pública, González manifestó que era necesario un "cambio de pensamiento global" para que la Policía entienda la protesta como un ejercicio de derechos. Ello permitiría que se trate a los manifestantes como ciudadanos y no como enemigos. (Le puede interesar: ["ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles"](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/))  

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
