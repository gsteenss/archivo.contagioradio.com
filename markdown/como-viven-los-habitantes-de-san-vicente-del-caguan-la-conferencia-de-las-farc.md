Title: ¿Cómo viven los habitantes de San Vicente del Caguán la Conferencia de las FARC?
Date: 2016-09-20 14:02
Category: Paz
Tags: FARC, Fuerza Pública, Gobierno, proceso de paz, San Vicente del Caguán
Slug: como-viven-los-habitantes-de-san-vicente-del-caguan-la-conferencia-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Conferencias-FARC-e1474397780364.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Revista Semana 

###### [20 Sep 2016]

**San Vicente del Caguán es la segunda ciudad más importante del departamento del Caquetá con más de 67.000 habitantes.** Por estos días ha regresado a la memoria de las y los colombianos, ya que muy cerca de ese lugar, más exactamente en la vereda El Diamante, se desarrolla la X y última Conferencia de la guerrilla de las FARC-EP, lo que dará paso a la conformación del movimiento político, después de la dejación de armas. Un hecho histórico que hace parte de los acuerdos de paz.

Sin embargo, está no es la primera vez que el departamento del Caquetá es usado como escenario de diálogos. Entre los años 1998 al 2002, San Vicente del Caguán se convirtió en la zona de distención como espacio de las negociaciones de paz entre la guerrilla de las FARC y el gobierno del entonces presidente Andrés Pastrana.

Esta vez, **San Vicente del Caguán tendrá una zona campamentaria, con un punto en la vereda Mira Valle y el  departamento tendrá dos puntos de transición**: uno en el municipio de Cartagena del Chairá, en la vereda la Esperanza; y  en el municipio de la Montañita en la vereda del Carmen.

Desde que se anunció que ese lugar sería el escenario de la gran asamblea de las FARC, hace cerca de 6 meses, muchos de ellos comenzaron a prepararse porque volverían a ser historia. Algunos comercios robustecieron sus inventarios y los rolos, paisas y extranjeros empezaron a ser parte de la cotidianidad.

“Hemos facilitado lo que esté a nuestro alcance, el trabajo ha aumentado”, dice una de las pobladoras que vive de la actividad hotelera, y quien señala que la X Conferencia reactivó el comercio de San Vicente. Algunos ya se acostumbraron a las preguntas de los periodistas, a ver nuevamente las cámaras de los medios aunque sólo sea para resaltar detalles acordes a su línea editorial.

Las estanterías no se desocuparon como lo afirmaron algunos noticieros, **ni se desmontaron los más de 10 retenes militares** ubicados en la carretera que de Florencia conduce a San Vicente, y que dejan ver todavía la gran inversión en guerra que se ha hecho en Colombia: tanques cascabel y camiones militares intentan camuflarse con el paisaje llanero, con soldados bien ataviados que levantan sus pulgares al paso de los viajeros.

Aunque se ha señalado que una de las grandes preocupaciones **los sanvicentinos será el tema de los cultivos de uso ilícito y su sustitución,** los habitantes de la región que sobreviven con la venta de la pasta de coca saben que si no hay un plan integral que permita que otras actividades campesinas emerjan, el negocio no se va a acabar.

Asuntos como la intromisión de las empresas petroleras está empezando a hacer ruido con el eco de las protestas en Paujil, Doncello y Montañita, municipios vecinos de San Vicente. Esa es otra de las preocupaciones para los caqueteños, pues aunque creen que es importante la llegada de empresas para generar empleo en la región, esto no puede ser una excusa para generar graves afectaciones ambientales, pues desde San Vicente incluso aseguran algunos pobladores que **“las aguas se han disminuido en un porcentaje considerable”.**

Una situación, que puede convertirse en una señal de lo que vendrá después de que las FARC dejen de estar paradas sobre el coltán, el petróleo y el oro. Tal vez la llegada de las empresas sea la nueva batalla que asuman las comunidades, que sin las FARC podrán vencer la estigmatización.

Sin embargo, habitantes de la zona, tienen algunas  preocupaciones frente al tema de la seguridad, no solo por quienes puedan ser disidentes de la guerrilla, sino por las acciones ilegales por parte del Ejército y la Policía, que también han cometido crímenes contra los pobladores, como lo denuncian los propios habitantes.

“La seguridad debe estar garantizada, necesitamos una fuerza pública fortalecida y honesta”, dice una de las pobladoras de San Vicente, **“Quiero vivir en un San Vicente seguro y próspero, en paz, donde podamos vivir sin ningún secuestros, ni vacunas, o asesinatos y podamos desarrollar trabajos que no le hagan daños al ambiente**”.

En San Vicente del Caguán todo el mundo es consciente de que el camino de la paz apenas está empezando, pero la mayoría de los habitantes aseguran estar dispuestos a recorrerlo para dejar a tras los años de estigmatización a lo que fueron relegados.

<iframe id="audio_12984982" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12984982_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
