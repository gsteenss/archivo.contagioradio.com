Title: Festival de Mujeres en escena por la paz 25 años
Date: 2016-08-18 12:59
Category: Cultura, eventos
Tags: Festival de mujeres en escena por la paz, Patricia Ariza, teatro en bogota
Slug: festival-de-mujeres-en-escena-por-la-paz-25-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/064c0b25-9bc7-47ad-95da-76f94845ec16-e1471538843870.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corporación Colombiana de Teatro 

###### [19 Agos 2016] 

25 años cumple el **Festival de Mujeres en escena por la paz**, un viaje que desde sus inicios se ha propuesto visibilizar a las mujeres que hacen teatro en Colombia y trabajar por la paz del país. Paradojicamente en tiempos donde la cultura y sus manifestaciones deben ser eje lugar fundamental para la reconciliación nacional los apoyos son escasos.

La maestra y dramaturga [Patricia Ariza](https://archivo.contagioradio.com/patricia-ariza-una-vida-por-la-cultura-y-la-paz/), directora del Festival, asegura que "**las artes no tienen espacio en institucionalidad colombiana**",calificando como una gran falla que "en el año de la paz nos dejaron [sin recursos](https://archivo.contagioradio.com/50-salas-de-teatro-podrian-cerrarse-con-nueva-resolucion-de-mincultura/)"; situación que como organizadores del evento han sabido capotear y sacar adelante "**contra viento y marea**".

En sus inicios, el Festival de Mujeres en escenas por la paz acogió pocos grupos y directoras del país, con el pasar de los años el número de participantes se ha incrementado alcanzando incluso a vincular agrupaciones provenientes de otros países del mundo.Para la presente edición estará representados por el **Frente de Danza independiente de Ecuador** y **María Magdalena o la Salvación desde Argentina**; que se unen a los artistas provenientes de Valle del cauca, Antioquia, Bolívar, Norte de Santander, Caquetá y Bogotá.

La participación incluyente de las mujeres en la búsqueda de la solución política al conflicto colombiano como una acto afirmativo de paz en todos implica, la posibilidad de expresarse a través de las artes, planteamiento que motiva la realización del gran **Foro Nacional Polifónico Mujeres y Paz**, una de las actividades centrales del encuentro, que tendrá lugar el 23 de Agosto en el Teatro la Candelaria de 9:00 a.m. a 6:00 p.m.

El domingo 21 de agosto, con la presentación de "**Memoria**", dirigida por la maestra Patricia Ariza inicia la [programación del Festival](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/programacion.png) que irá hasta el próximo 28 de agosto en las salas Sekisano (Calle 12 Número 2 - 65), el Teatro La Candelaria (Calle 12 \# 2- 59), y teatro El Galponcito de Umbral (Calle 19 Numero 4 - 71 Local 404). La boletería esta disponible en dos modalidades: por abonos con un costo de 30.000 para 6 obras; e individual para estudiantes con un valor de 10.000 y particulares de 15.0000.

<iframe src="http://co.ivoox.com/es/player_ej_12587702_2_1.html?data=kpeimpybdJOhhpywj5aUaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncbHV1dfWxc7Fb6Lmyt_Oh5enb6XmwtLO1trWq8KfjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
