Title: Comunidades Wayúu acuden a la ONU para frenar la actividad del Cerrejón en medio de la pandemia
Date: 2020-06-23 15:49
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Covid-19
Slug: comunidades-wayuu-acuden-a-la-onu-para-frenar-la-actividad-del-cerrejon-en-medio-de-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Mujeres-Wayúu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Mujeres Wayúu/ Ccajar

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque las medidas de confinamiento a raíz del Covid-19 siguen vigentes en el país, **la compañía Cerrejón continúa realizando su actividad minera en La Guajira**, razón por la que mujeres wayúu del Resguardo Provincial han pedido a las Naciones Unidas para que interceda ante el Gobierno y se suspenda la explotación carbonífera. Pese a que la Corte Constitucional se ha manifestado en repetidas ocasiones a favor de las comunidades, desde el Gobierno no se han tomado medidas de protección.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El [Colectivo de Abogados José Alvear Restrepo (Ccajar)](https://twitter.com/Ccajar), organización que representa a las mujeres Wayuu de Provincial - comunidad ubicada muy cerca de Cerrejón - ante la ONU, ha advertido que el Gobierno está en pleno conocimiento de que las poblaciones aledañas no gozan del derecho fundamental al agua a raíz de las afectaciones a la hidrografía del territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“La compañía reanudó sus operaciones durante el Covid-19, poniendo en riesgo a la comunidad y aumentando su vulnerabilidad en un contexto de escasez de agua”, denuncia la carta dirigida a los relatores de la ONU sobre los Derechos Humanos y el Medio Ambiente. **“En la cuarentena predominante es aún más difícil para los Wayuu acceder al agua”,** agrega. [(Lea también:“Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú)](https://archivo.contagioradio.com/guajira/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los Wayúu tienen antecedentes de problemas respiratorios

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las comunidades han advertido que existen antecedentes de enfermedades respiratorias en particular para la población infantil que se ve directamente afectada por el material particulado que proviene de la mina a cielo abierto. Esto de cara a estudios que han advertido cómo el Covid-19 se propaga con mayor rapidez en entornos de alta contaminación [(Lea también: Guajira sin agua por desvío de arroyo Bruno y sin ayudas para enfrentar al COVID)](https://archivo.contagioradio.com/guajira-sin-agua-por-desvio-de-arroyo-bruno-y-sin-ayudas-para-enfrentar-al-covid/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la sentencia de la Corte Constitucional T-614 de 2019, al examinarse las historias clínicas de la comunidad wayúu y un informe de la Secretaría de Salud y Sanidad Pública Municipal de Barrancas se concluye que **hay una incidencia negativa y recurrentes afecciones de nivel celular, cáncer, neumonías bacterianas no especificadas, neumocosis, bronquitis crónica, fibrosis masiva, asma, laringitis, entre otras**, afectando las vías respiratorias de los habitantes de Provincial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante tal evidencia, han solicitado se mantenga la suspensión de los tajos mineros aledaños a la comunidad de Provincial para así lograr adoptar medidas preventivas y evitar una propagación del Covid-19, ademá exigen al Gobierno iniciar el proceso de transición y cierre de la mina de carbón operada por Cerrejón.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la petición se suman informes internacionales que muestran que las minas alrededor del mundo son un punto crítico para la propagación del nuevo coronavirus. Los casos graves de COVID-19 devienen en enfermedades respiratorias que tendrían una mayor mortalidad en las personas con afecciones respiratorias de salud preexistentes como es el caso de la comunidad wayúu de Provincial. [(Lea también: Carbones del Cerrejón incumple sentencia que protege a comunidades y al Arroyo Bruno)](https://archivo.contagioradio.com/carbones-del-cerrejon-incumple-sentencia-que-protege-a-comunidades-y-al-arroyo-bruno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque expertos de la salud han advertido que la contaminación puede afectar aún más las infecciones por Covid-19, desde Cerrejón afirman que están listos para aportar información a la ONU, asegurando que han redigirido la inversión social al sistema de salud de la provincia y donado 12 millones de litros de agua a las comunidades, incluida Provincial. [(Lea también: Gobierno debe dialogar con Cuba para enfrentar la pandemia)](https://archivo.contagioradio.com/gobierno-debe-dialogar-con-cuba-para-enfrentar-la-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La minería expone a las comunidades alrededor del mundo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mientras empresas mineras en Sudáfrica y Perú proponen levantar las medidas de confinamiento, desde la Asociación de Trabajadores de la Minería y la Construcción de Sudáfrica (AMCU), en representación de al menos 250.000 mineros, advierten sobre el riesgo de reabrir las minas sin que existan regulaciones, pues resulta "una muy mala idea para trabajadores y comunidades de donde provienen”, por su parte sindicatos de minería de cobre en Chile han advertido sobre las deficiencias en las medidas de prevención de contagio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A principios de junio, siete Organizaciones No Gubernamentales del mundo, dieron a conocer que **en 18 países, incluidos Brasil, Ecuador, México, Honduras, Panamá, Colombia, Chile**, las operaciones mineras ya registraban al menos 3.000 personas contagiadas con Covid-19, no solo exponiendo sus vida sino las de comunidades campesinas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El informe advierte que la industria minera continúa operando pese a los brotes de coronavirus; **según la ONG MiningWatch Canada se estima que unas 4.000 personas que trabajan en al menos 100 lugares de explotación minera y 300 residentes de comunidades cercanas, han contraído la enfermedad.** [(Lea también: Comunidades de la Guajira fueron excluidas de discusión sobre el futuro del Arroyo Bruno)](https://archivo.contagioradio.com/arroyo-bruno-afecta-guajira/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma advierte sobre patrones a través de los que la industria minera buscaría lucrarse a través del Covid-19: ignorando el peligro que la enfermedad representa para trabajadores y habitantes aledaños, reprimiendo las protestas y promocionando su expansión, utilizando la pandemia para lavar la imagen de sus operaciones y presionando a autoridades públicas para lograr cambios en sus regulaciones.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
