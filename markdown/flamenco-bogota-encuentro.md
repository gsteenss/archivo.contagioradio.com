Title: En Bogotá febrero inicia con "F" de Flamenco
Date: 2018-02-01 13:30
Category: eventos
Tags: Bogotá, danza, flamenco
Slug: flamenco-bogota-encuentro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Encuentro_preview-e1517501356313.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Encuentro Flamenco 

###### 01  Feb 2017 

Dos vidas, dos estilos, dos historias de mujer unidas por la pasión del flamenco: **Marcela Hormaza** bailaora colombiana y **Laureen Manjarres** de origen australiano, se encuentran para danzar al compás de la guitarra de **Diego Bejarano** y la voz de la cantaora Clara Rozo, del primero al 17 de febrero en la [Factoría L’explose de Bogotá](https://archivo.contagioradio.com/ii-festival-de-flamenco-de-bogota/).

**“Encuentro flamenco”** es un espectáculo de la Casa Flamenco de Bogotá, bajo la dirección de Marcela Hormaza, en el que se destacan los elementos más importantes de este género. Un viaje entre estilos y temperamentos, sensaciones y sentimientos, que permiten que el espectador sienta el vibrar de esta manifestación artística, que tiene la combinación perfecta gracias al diálogo de la guitarra con la voz y el baile.

**Los artistas**

Laureen Manjares, bailaora invitada, se formó en Carmonas Flamenco Academy y The Sydney Flamenco Studio en su natal Sídney (Australia); posteriormente se trasladó a España para continuar su formación con profesores como: Adriana Rodríguez, Manuel Betanzos, Chachy Peñalver y Andrés Peña entre otros.

**Diego Bejarano**, es guitarrista flamenco con formación musical en la madre patria, quien ha tenido la oportunidad de ser telonero de Diego “El Cigala” y Alejandro Sanz. **Clara Rozo** es cantaora de familia de músicos y cantantes con una fuerte tradición oral, estudio música en la Academia Superior de Artes de Bogotá y ha sido parte de los grupos KALE FLAMENCO junto al guitarrista Anzor Mancipe y XAROP.

Marcela Hormaza, inicio su formación en danza en el año 1994 con el maestro Antonio González, desarrollo talleres con el Ballet Español de Cuba y el Ballet de Lizt Alfonso en la Habana, Cuba. En el año 2007 viajo a Sevilla, donde amplío su formación en la Fundación Cristina Heeren, de la mano de grandes artistas y maestros.

<iframe src="https://www.youtube.com/embed/rJESjDpHwfg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
