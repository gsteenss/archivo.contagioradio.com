Title: Asesinan a lider indígena Gerson Acosta en Timbio, Cauca
Date: 2017-04-19 18:36
Category: Uncategorized
Tags: ACIN, paramilitares, Timbío
Slug: asesinan-gerson-acosta-timbio-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Gerson-Acosta-en-Timbio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

###### [19 Abr 2017] 

Según información de la Asociación de Cabildos Indígenas del Norte del Cauca, ACIN, hacia las 5 de la tarde **fue asesinado Gerson Acosta, de 35 años de edad, autoridad Indigena Newexs, defensor de DDHH**, y lider de víctimas perteneciente al Cabildo Kite Kiwe, cabildo que también se relaciona con algunos procesos llamados "Liberación de la madre Tierra".

Las primeras versiones relatan que el Acosta salía de una reunión en el municipio cuando fue atacado por hombres que se movilizaban en una motocicleta. Las demás a**utoridades indígenas que se encontraban presentes lo auxiliaron y lo trasladaron al hospital de la cabecera municipal al que llegó sin signos vitales.**

A esta hora la guardia indígena de la comunidad Kite Kiwe, intenta retener a los responsables de este crimen, así como acordonar la zona para facilitar el trabajo de investigación y judicialización de los responsables. Además informaron que **tienen en custodia a un posible sospechoso** y pidieron el respaldo de la comunidad para evitar acciones de armados que permanecen en la zona.

Noticia en desarrollo...

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
