Title: Informe revela cómo las comunidades de fe fueron víctimas del conflicto armado
Date: 2019-11-01 16:54
Author: CtgAdm
Category: Nacional, Paz
Tags: colombia, conflicto, Iglesia, Sin Olvido, víctimas
Slug: informe-revela-como-las-comunidades-de-fe-fueron-victimas-del-conflicto-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/padre-tiberio-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Viajarenverano] 

[Integrantes de la Mesa Ecuménica se reunirán  este fin de semana  para celebrar el séptimo encuentro nacional de iglesias; en el que harán entrega del **primer informe que acoge los casos de integrantes de comunidades de fe asesinados en el marco del conflicto armado**, este es el resultado de un año de investigación de más de 100 sucesos de los cuales 48 estarán incluidos en este informe.  (Le puede interesar:[Una reflexión sobre la reconciliación ](https://archivo.contagioradio.com/una-reflexion-sobre-la-reconciliacion/))]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

<div style="text-align: justify;">

[El propósito de este informe, según Omar Fernández integrante del equipo Nacional de la Mesa Ecuménica, es **visibilizar en el país a uno de los sectores que también se vieron profundamente afectados en el conflicto armado de Colombia**, "los caídos al interior del movimiento de fe de los pobres no está en  la misma dimensión que otros movimientos como el indígena, el sindical o el  político como la UP que fueron cientos, miles de asesinatos, sin embargo para nosotros sí representan muchas perdidas, y por eso **vale la pena resaltar el  sector iglesias que no puede quedar escondido**", manifestó Fernández.]

</div>

<div>

</div>

<div class="css-1dbjc4n r-xoduu5" style="text-align: justify;">

[Así mismo añadió que **este es un relato sistemático de múltiples asesinatos que han sido presentados y abordados como hechos aislados y que con el tiempo se han venido olvidando**, "nosotros evidenciamos como que de manera ininterrumpida, los sacerdotes, las religiosas y  las comunidades comprometidas con el movimiento de iglesias, **fueron asesinados como una estrategia política**" (Le puede interesar:[Por primera vez, informes de población LGBTI llegan a una Comisión de la Verdad](https://archivo.contagioradio.com/por-primera-vez-informes-de-poblacion-lgbti-llegan-a-una-comision-de-la-verdad/))]

</div>

<div>

</div>

<div style="text-align: justify;">

Luego de los ataques a estas comunidades **muchas iglesias quedaron casi extintas a causa de los desplazamientos generados por el terror**, "m[uchos de los asesinatos fueron acompañados con torturas que tenían como fin generar un terror muy fuerte en la población, por eso este relato permite identificar cómo existió una política sostenida de ataque contra este movimiento de iglesias, con pensamiento progresista y liberadores". ]

</div>

### **Algunos Casos de comunidades de fe**

<div style="text-align: justify;">

Algunos de los casos que están incluídos en el informe son los del S**acerdote Católico Tiberio Fernandez, párroco del municipio de Trujillo en el Valle quien  fué víctima de los paramilitares en 1990**, uno de los procesos de violencia más crudos y dolorosos del país. También presentarán el caso de **Alvaro Ulcué Chocué, primer sacerdote Nasa, asesinado a manos de agentes del  F-2 de la Policía Nacional** en Santander  de Quilichao, Cauca, su muerte aún está en la impunidad.

</div>

### **Es necesario reactivar la esperanza lejos del conflicto armado**

<div style="text-align: justify;">

[Este informe además de ser una denuncia es un homenaje a la memoria de cientos de sacerdotes, religiosos, laicos y catequistas que fueron **perseguidos, muchos de ellos asesinados y otro grupo de ellos exiliados**, "la idea es que no nos dejemos derrotar  por la desesperanza y el impacto que ha tenido esta política de persecución tan dura", concluyó Fernández. (Le puede interesar: [Operación Orión ¡Nunca Más!, 17 años buscando justicia y verdad](https://archivo.contagioradio.com/victimas-operacion-orion/)).]

</div>

<div style="text-align: justify;">

</div>

<div style="text-align: justify;">

[Por otra parte el encuentro tiene un propósito central  y es hacer un análisis que le permita a las comunidades religiosas reactivar un movimiento que fue fuerte hace más de dos décadas y jugó un papel crucial en el proceso de paz en Colombia;]**hoy el sector cristiano reconoce su compromiso para aportar en la construcción de una nueva sociedad y al mismo tiempo para mostrar al país y al mundo una novedad en el proceso religioso. **

</div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44242197" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44242197_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
