Title: 8 brigadistas internacionales españoles detenidos por combatir en las regiones prorrusas de Ucrania
Date: 2015-02-27 18:40
Author: CtgAdm
Category: El mundo, Política
Tags: 8 brigadas internacionales detenidos en España por combatir en Donbass, Antifascistas españoles en Ucrania, Brigadistas internacionales en Ucrania, Donbass
Slug: 8-brigadistas-internacionales-espanoles-detenidos-por-combatir-en-las-regiones-prorrusas-de-ucrania
Status: published

###### Foto:Comitedeapoyoalaucraniantifascista.wordpress.com 

A pesar de que España se ha declarado como "neutral" ante la guerra independentista en Ucrania, la policía española ha detenido a **8 miembros de la Brigada Internacional Antifascista Carlos Palomino por haber combatido en las regiones separatistas de Ucrania**, se les acusa de distintos delitos como asesinatos, tenencia de armas y explosivos y de atentar contra los intereses de España.

Han sido muchos los jóvenes antifascistas de distintos países de la UE, que han apoyado a las milicias separatistas. Con **esta operación, la primera en Europa, se pretende desmovilizar e impedir el apoyo internacional**, pero también se criminaliza a los brigadistas internacionales.

Los 8 detenidos tienen edades entre los 20 y 30 años,  son de distintas regiones del estado español y habían regresado en las últimas semanas, algunos de ellos habían hecho visibles fotos con armamento y habían concedido entrevistas a periódicos como "Público".

La brigada internacional Carlos Palomino fue fundada por antifascistas españoles, casi todos **miembros de partidos comunistas, incluso hay uno de las juventudes de Izquierda Unida**, con el objetivo de combatir fuerzas fascistas o al ejército ucraniano y de defender las milicias separatistas que califican de antifascistas en Donbass, Donestk o Lugansk.

La brigada tiene el nombre de un  antifascista asesinado en Madrid por un exmilitar español. Dos de ellos estaban **integrados en el batallón Vostok, del que publicaron un video con combatientes comunicando su apoyo a la milicia**.

Diversas organizaciones sociales de izquierda española se han solidarizado con los detenidos exigiendo su puesta en en libertad.
