Title: Video | "Con la verdad es que se habla" Capitan (r) Alfonso Romero Buitrago
Date: 2020-01-03 08:49
Author: CtgAdm
Category: Nacional, Video
Tags: festivales de la memoria, paz, verdad
Slug: con-la-verdad-es-que-se-habla-capitan-r-alfonso-romero-buitrago
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-27-at-4.07.34-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El capitán [**Alfonso Romero Buitrago**](https://archivo.contagioradio.com/alfonso-romero-buitrago-el-capitan-que-le-apuesta-a-la-verdad/), retirado hace 14 años, es uno de los integrantes de las Fuerza Militares que ha decidido comparecer ante la JEP y la Comisión de la Verdad para aportar con su testimonio al esclarecimiento de la verdad, en entrevista con Contagio Radio, narra situaciones que vivió estando en el Ejército Nacional y porque ha decidido comparecer ante el SIJVNR para aportar en la construcción de un nuevo país.

-   [**Leer entrada:  [Alfonso Romero Buitrago el capitán que le apuesta a la verdad](https://archivo.contagioradio.com/alfonso-romero-buitrago-el-capitan-que-le-apuesta-a-la-verdad/)**]

