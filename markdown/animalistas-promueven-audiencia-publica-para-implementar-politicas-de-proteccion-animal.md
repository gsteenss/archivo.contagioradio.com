Title: Ley colombiana debe reconocer a los animales como seres sintientes
Date: 2015-02-27 18:14
Author: CtgAdm
Category: Animales, Voces de la Tierra
Tags: Animalistas, derechos de los animales, Guillermo García, Juan Carlos Losada, Maltrato animal, Natalia Parra, Plataforma Alto
Slug: animalistas-promueven-audiencia-publica-para-implementar-politicas-de-proteccion-animal
Status: published

##### Foto: Plataforma Alto 

<iframe src="http://www.ivoox.com/player_ek_4143835_2_1.html?data=lZahlZ2XeY6ZmKiakp6Jd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc%2FdzsbZy9jYpdSf0dfcz9rJusbijMbixs7JssTdwpDdh6iXaaO1w9HWxcaPtMLmwpDWz9XQqc7Zz9mah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Senador del Partido Liberal, Guillermo García] 

Estudiantes y organizaciones animalistas impulsaron **una audiencia pública en el senado, para discutir iniciativas por la defensa de los derechos de los animales.** Allí, con las voces del senador por el Partido Liberal Guillermo García; el representante a la cámara del mismo partido,  Juan Carlos Losada; y la directora de la Plataforma Alto, Natalia Parra, se habló sobre **un proyecto de Ley y el Plan Nacional de Desarrollo, como caminos claves para hacerle frente al maltrato animal.**

A través de estas iniciativas, se trataron cuatro temas con el objetivo de divulgar, defender y promocionar los derechos de los animales. El primero de ellos, es seguir impulsando el proyecto de Ley del representante a la cámara por el Partido Liberal Juan Carlos Losada, para **fortalecer la ley 84 del 1989**, de manera que se incrementen las penas por agredir animales, se declarare como delito el maltrato animal y se vea a los animales como seres sintientes.

Por otro lado, se habló sobre las acciones a proponer en el debate del **Plan Nacional de Desarrollo, para implementar políticas públicas sobre protección animal,** en coordinación con los planes de desarrollo locales y regionales, teniendo en cuenta que en palabras del senador Guillermo “El PND actual no menciona el plan animalista de ninguna manera”.

Así mismo, se plantearon propuestas para a**poyar logística y financieramente las diferentes organizaciones animalistas,** que trabajan en pro de los animales sin ningún tipo de ayuda del Estado.

Como tercer punto, se discutió el tema del **fortalecimiento en la educación de los niños, niñas y adolecentes**, para que desde tempranas edades, se vea  a los animales como sujetos de derechos, y a partir de las juventudes se contribuya a la defender la vida de los animales.

Finalmente, se resaltó que es crucial mantener la **bancada animalista en senado y cámara,** para que permanezcan acciones sobre el mismo tema.

Las personas interesadas en hacer alguna propuesta animalista, pueden hacerlas llegar a la página del senador Guillermo García Realpe, a través de www.guillermogarciarealpe.com o al correo guillergarciar@hotmail.com.
