Title: COCCAM denuncia contratación de civiles para erradicación forzada
Date: 2017-03-17 14:28
Category: Ambiente, Nacional
Tags: COCCAM, erradicación cultivos ilícitos
Slug: coccam-denuncia-contratacion-de-civiles-para-erradicacion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Erradicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [17 Mar 2017] 

**La plataforma COCCAM denunció la contratación de erradicadores civiles que en compañía de las Fuerzas Militares continúan con las erradicaciones forzadas en sus territorios**, al mismo tiempo que avanzan las instalaciones de las delegaciones en diferentes regiones del país para diseñar y hacer seguimiento a los planes de sustitución de cultivos ilícitos.

Territorios como Putumayo, La Macarena, el Nudo del Paramillo y Lozada Guayabal ya han pactado acuerdos con el gobierno para iniciar los planes de sustitución, sin embargo, las comunidades han expresado que existe una incertidumbre generada por los incumplimientos del gobierno y la **continuación de la práctica de la fumigación en departamentos como el Guaviare y Nariño que ya habían pactado planes de sustitución**.

Cesar Jerez, vocero de COCCAM, afirmó que “todas las medidas de fuerza en Colombia, fracasaron con una inversión muy alta y un impacto nefasto para el ambiente, h**ay un problema estructural en el campo, hay una demanda creciente de cocaína, hay un narcotráfico creciente, por lo que el camino son los compromisos del gobierno con las comunidades**”. Le puede interesar: ["3000 campesinos del Cauca rechazan erradicación forzada"](https://archivo.contagioradio.com/campesinos-alzan-su-voz-contra-la-erradicacion-forzada/)

Otra de las preocupaciones de los campesinos tiene que ver con las amenazas por parte de miembros de las Fuerzas Militares de ser judicializados por sus cultivos ilícitos, ya que de acuerdo con Jerez, en la actualidad **hay 3000 campesinos en la cárceles por este motivo, pese a que el gobierno ya se había comprometido a liberarlos en los acuerdos con la Cumbre Agraria, tras el paro del 2013**.

Los campesinos agremiados en COCCAM expresaron que si se continúan presentando las erradicaciones forzadas en sus territorios y los operativos de capturas contra cultivadores, se realizará una nueva movilización que aglutine a la Cumbre Agraria Étnica Campesina y Popular que **exija no solo el cumplimiento de lo pactado con esta plataforma, sino también lo pactado en el Acuerdo de Paz**. Le puede interesar: ["Con erradicaciones forzadas se atenta contra el Acuerdo de Paz: ASCSUCOR"](https://archivo.contagioradio.com/erradicaciones-forzadas-atentan-contra-acuerdo-de-paz/)

<iframe id="audio_17607723" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17607723_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
