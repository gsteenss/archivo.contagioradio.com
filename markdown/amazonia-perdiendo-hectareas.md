Title: La Amazonía está perdiendo 293 hectáreas cada día
Date: 2018-09-12 17:26
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: Amazonía, Fiscalía
Slug: amazonia-perdiendo-hectareas
Status: published

###### [Foto: Contagio Radio] 

###### [12 Sept 2018] 

**La Amazonía está perdiendo diariamente el equivalente a 293 canchas de fútbol;** por eso, el movimiento **Avaaz**  junto con un grupo de ambientalistas, realizó en la mañana de este miércoles una protesta pacífica frente a la **Fiscalía**, para pedir que se investigue la deforestación que esta sufriendo esta región.

Con la instalación de troncos cortados frente a la Fiscalía, en lo que denominaron **"Cementerio de la Amazonía"**, los activistas buscaron llamar la atención sobre los crímenes ambientales contra el 'pulmón del mundo', y entregaron información específica sobre el **Parque Nacional Natural (PNN) Serranía de Chiribiquete.**

### **"La deforestación en la Amazonía aumentó un 44% en 2016 y se duplicó en 2017"** 

En su pagina web, **Avaaz** registra que la deforestación en la Amazonía aumentó un 44% en 2016 y se duplicó en 2017, sin embargo, en el documento que será presentado a la Fiscalía se entregarán puntos de referencia específicos sobre el PNN Chiribiquete, en los que se percibe un aumento en la tala del bosque. (Le puede interesar: ["Deforestación aumentó un 23% en Colombia: IDEAM"](https://archivo.contagioradio.com/la-deforestacion-a-nivel-nacional-aumento-al-23-ideam/))

Como lo señaló Suárez, este parque, recientemente declarado **Patrimonio de la Humanidad,** "es un territorio estratégico para la supervivencia de la Amazonía porque conecta distintos ecosistemas como son el norte de esta región con la cordillera de los Andes", por lo tanto, si se pierde esta área, se verían gravemente afectadas las fuentes de agua que llegan al vital ecosistema.

Aunque es común creer que son los campesinos y habitantes de la región quienes están talando el bosque, la activista recordó que la deforestación se produce por diferentes actividades ilegales como **minería, ganadería extensiva y la tala para la industria maderera;** y agrego que son grupos y estructuras criminales organizadas las que están produciendo los graves daños.

Suaréz afirmó que la Amazonía es fundamental para el planeta, porque además de la producción de oxigeno, también regula la humedad de la región; sin embargo, el riesgo para la zona es evidente: "en los últimos años perdimos el equivalente a 165 veces la ciudad de Bogotá, de avanzar a este ritmo, perderemos 293 canchas de fútbol diarias", y **hay más de 200 especies en peligro de extinción en la región.**

https://youtu.be/KVUBOX3OF4s

### **Estamos multiplicando por 1.000 la extinción de las especies en el mundo** 

La activista afirmó que la crisis de biodiversidad que vive el mundo es igual a la crisis de cambio climático, "pero al ritmo que vamos, **estamos acelerando por 1.000 veces más la extinción de las especies del planeta**". Razones por las cuales, exigen que se investiguen a quienes están detrás de la deforestación y los demás crímenes ambientales. (Le puede interesar: ["Más de 2.700 especies de fauna y flora en peligro por la deforestación en Colombia"](https://archivo.contagioradio.com/flora_fauna_en_peligro_deforestacion/))

<iframe id="audio_28549373" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28549373_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
