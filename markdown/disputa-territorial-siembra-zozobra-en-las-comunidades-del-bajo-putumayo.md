Title: Disputa territorial siembra zozobra en las comunidades del Bajo Putumayo
Date: 2020-12-15 21:12
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Desplazamiento forzado, Disidencias de las FARC, Putumayo, reclutamiento de menores, reclutamiento forzado
Slug: disputa-territorial-siembra-zozobra-en-las-comunidades-del-bajo-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/EKWb2rWWoAEu9hJ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: diputado del Putumayo Andrés Cancimance

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fuentes han informado sobre la** crítica situación humanitaria y de DD.HH que se viene presentando en los municipios de Puerto Asís y Puerto Leguizamo** en el Bajo Putumayo debido a la disputa armada por control territorial entre los denominados **Comandos de La Frontera y el Frente Carolina Ramirez de las FARC** en los corregimientos de **Piñuña Blanco y la Inspección de Puerto Asís.** 

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las alertas hechas a la [Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/) afirman que los grupos ilegales han prohibido a los pobladores denunciar los sucesos que ocurren en el territorio incluyendo las bajas de ambos grupos armados, que según las denuncian serían mayores para los integrantes del Comando Frontera que ha reclutado a muchos jóvenes de forma reciente.  [(Le puede interesar: II Semana de la Juventud en defensa del territorio de la Perla Amazónica)](https://archivo.contagioradio.com/ii-semana-de-la-juventud-en-defensa-del-territorio-de-la-perla-amazonica/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, el movimiento de unidades del Frente Carolina Ramírez en las últimas tres semanas derivo en enfrentamientos que **han acrecentado la zozobra de las poblaciones que denuncian amenazas selectivas y generalizadas contra miembros de las comunidades.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A los hostigamientos y reclutamiento de jóvenes de la zona, se suman los crecientes desplazamientos forzados de familias que optan por salir del territorio en silencio para evitar ser amenazados. [(Lea también: Pese a amenazas contra la lideresa Jani Silva, UNP retira una de sus medidas de protección)](https://archivo.contagioradio.com/pese-amenazas-contra-jani-silva-unp-retira-proteccion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, miembros de los Comandos de la Frontera, han obligado a las comunidades a asistir a reuniones en las que ratifican las amenazas expresadas en panfletos que han aparecido en el centro urbano de Puerto Asís y que mencionan a líderes y lideresas de la zona. A su vez, **quienes se niegan a asistir a dichas reuniones son obligados a entregar sumas de dinero a los armados**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dichos hechos se suman a recientes denuncias del movimiento Marcha Patriótica que ha advertido cómo líderes sociales de la región como Yuri Quintero Wilmar Madroñero continúan siendo intimidados por los Comandos de la Frontera.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las denuncias también aseveran que los grupos promueven el reclutamiento forzado en la zona ofreciendo hasta dos millones de pago mensual, de igual forma las operaciones armadas de ambos grupos han impuesto restricciones en la movilidad de los habitantes. [(Lea también: Denuncian el asesinato de Ever Edwuardo Velásquez en Puerto Guzmán, Putumayo)](https://archivo.contagioradio.com/denuncian-el-asesinato-de-ever-edwuardo-velasquez-en-puerto-guzman-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma la situación de los liderazgos sociales permanece en incertidumbre, el pasado 25 de noviembre d**os hombres armados de la estructura Comandos de la Frontera, asesinaron al líder social Fernando Mejía Angarita** quien fue integrante de la** Comisión Municipal de Planeación Participativa instancia del Programa Nacional de Sustitución de cultivos ilícitos (PNIS) en la inspección de Puerto Asís.  **

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A la vez, se han evidenciado pintas y panfletos en contra de Sandra Lagos, presidenta de la comunidad de Puerto Playa al interior de la Zona de Reserva Campesina Perla Amazónica. Sandra es a su vez delegada de la zona de reserva ante el Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ejército incluso apoyaría operaciones ilegales en Putumayo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Como se ha denunciado con anterioridad, pese a la fuerte presencia militar y policial de la Brigada XXVII de Selva, y la Policía Nacional, no existe respuesta institucional que proteja a las poblaciones rurales de los municipios que hacen parte del Bajo Putumayo. **Fuentes agregan se habría prohibido la presencia de derechos humanos en los territorios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por el contrario, los testimonios aseveran que en medio de de las hostilidades, se observó el apoyo de las Fuerzas Militares a integrantes de los Comandos Frontera, Además, aseguran que el Ejército está al tanto de sus operaciones y movimientos y "**que ellos los cubrirán en caso de ataques del Frente Carolina Ramírez".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Comunidades campesinas e indígenas del Putumayo hacen parte de los más de 115 procesos de base que han pedido al Gobierno y a los diversos actores armados legales e ilegales que operan en el territorio un cese al fuego acorde al Acuerdo Humanitario Global solicitado por el **secretario general de las Naciones Unidas, António Guterre**s para que en medio de la pandemia les sea posible continuar con la defensa de la vida. [(Le puede interesar: Organizaciones sociales convocaron a un Pacto por la Vida y la Paz)](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
