Title: Más de 2000 campesinos exigen salida de petrolera del Lago de Tota
Date: 2016-11-09 17:01
Category: Ambiente, Nacional
Tags: defensa ambiental, Explotación petrolera, Lago Tota
Slug: mas-2000-campesinos-exigen-salida-petrolera-del-lago-tota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/mineria_tota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo por la Defensa de la Provincia de Sugamuxi] 

###### [9 Nov de 2016] 

El pasado 6 de Noviembre se realizó la segunda Audiencia Pública Ambiental en el municipio de Pesca Boyacá, con el fin de exigir la revocatoria de la licencia ambiental otorgada por la ANLA **a la multinacional francesa Maurel & Prom para extraer petróleo en 8.033 hectáreas aledañas al Lago de Tota. **

**Más de 2.000 campesinos llegaron de Betania, Tota, Pesca, Firavitoba, Iza y Sogamoso a participar de la Audiencia,** en la que también participaron organizaciones sociales, ambientales, la Gobernación de Boyacá, la Personería, la Defensoría del Pueblo y un representante de la multinacional. Le puede interesar: [210 cuerpos de agua cerca al](https://archivo.contagioradio.com/210-cuerpos-de-agua-cercanos-al-lago-de-tota-desaparecieron-por-actividad-petrolera/)lago de Tota[desaparecieron por actividad petrolera.](https://archivo.contagioradio.com/210-cuerpos-de-agua-cercanos-al-lago-de-tota-desaparecieron-por-actividad-petrolera/)

Patricia Corredor integrante del Colectivo por la Defensa de la Provincia de Sugamuxi, señaló que el espacio fue abierto para que “todos los que quisieran hablar pudieran decir sus argumentos (…) **se escuchó en la voz de 99% de los y las campesinas asistentes, que no quieren mas a la petrolera en el territorio”**. Le puede interesar: [Las incoherencias de Colombia después del acuerdo de parís frente al fracking.](https://archivo.contagioradio.com/las-incoherencias-de-colombia-despues-del-acuerdo-de-paris-frente-al-fracking/)

Corredor revela que **la empresa ha incumplido 3 condiciones estipuladas por la ANLA en la licencia ambiental otorgada en 2009**, 1. No se permite la explotación en pendientes superiores a 45º, 2. No se permite explotación en zonas de páramo y 3.No se permite actividad en zonas de recargas de acuíferos, **“al incumplirlas el ANLA esta obligada a revocar la licencia”** comenta Corredor.

La defensora ambiental denuncia que además de estos incumplimientos, las actividades de sísmica ocasionaron **la pérdida de sus casas a cientos de familias en la zona rural, y en Sogamoso más de 70 familias del barrio Asodea deberán desalojar sus casas** porque presentan agrietamientos.

Corredor, afirma que la audiencia dejó buenos frutos a las comunidades, y comenta que recibieron con alegría el que **la Gobernación de Boyacá junto a las alcaldías de Pesca y Tota respaldan las iniciativas de movilización social.**

Por otra parte indica que **la ANLA tiene 3 meses para emitir una respuesta sobre lo discutido en la Audiencia Pública,** “creemos que si hacen un pronunciamiento técnico, ese tiene que ser la revocatoria, para este caso es casi obligatorio”.

<iframe id="audio_13686233" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13686233_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
