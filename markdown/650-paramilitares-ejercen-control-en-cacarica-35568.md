Title: Cerca de 650 Paramilitares ejercen control territorial en Cacarica
Date: 2017-02-01 15:04
Category: DDHH, Nacional
Tags: cacarica, Desplazamiento, paramilitares
Slug: 650-paramilitares-ejercen-control-en-cacarica-35568
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Untitled-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia y Paz ] 

###### [1 Feb. 2017] 

Luego de más de 8 días de presencia paramilitar en la cuenca del rio Cacarica en el Bajo Atrato Chocoano, este martes cerca de 3 botes con 200 hombres de las Autodefensas Gaitanistas de Colombia (AGC) arribaron al caserío Bijao con regalos para los niños. Esta acción se da en el marco del **control paramilitar que está en la zona, quienes además con lista en mano han dicho asesinarán pobladores.**

Uno de los pobladores de Cacarica aseguró para Contagio Radio que los paramilitares han tenido asambleas con la comunidad y han dado las normas como que **las comunidades no pueden movilizarse luego de las 6 p.m**. “hay muchas limitaciones y han dado un mensaje claro de que van a tomarse todas las comunidades y que no han venido de paso sino para quedarse” manifestó el poblador.

Pese a que en la zona existe un retén militar, los uniformados no han hecho presencia en la comunidad “aquí no se ha aparecido nadie de la fuerza estatal, no ha habido respuesta alguna por parte del Estado **estamos ante un abandono total**” y añade que **serían 650** los** paramilitares en la zona **. Le puede interesar: [Comunidades del Cacarica sitiadas por presencia paramilitar](https://archivo.contagioradio.com/choco-paramilitares-gaitanistas/)

Según el poblador la Defensoría ha estado ausente del territorio y no se ha dirigido al lugar para revisar cuál es la situación de las familias, especialmente en la comunidad de Bijao y Puente América **“urge la presencia de la Defensora acompañada de otros integrantes como Ministerio del Interior, Cancillería y todos los entes** que tengan que ver con esta situación para ver cómo pueden minimizar los riesgos”.

**En 1997, las comunidades asentadas en la cuenca del rio Cacarica fueron víctimas de la Operación Génesis**, en la que los batallones de la Brigada 17 del Ejército, con sede en Carepa, Urabá antioqueño, al mando del entonces general Rito Alejo Del Río Rojas, de la mano de paramilitares ocasionaron el desplazamiento forzoso de las comunidades y la tortura y asesinato de Marino López Mena.

Por lo que el poblador concluye diciendo “**en la actualidad estamos haciendo memoria del 97,** nos duele mucho porque precisamente han escogido este mes de febrero, que habíamos decretado las comunidades como **el festival de la memoria, para hacer distintas actividades en las comunidades, pero el paramilitarismo nos está coartando las cosas”**.  Le puede interesar: [Mujeres de Cacarica: 20 años espantando la guerra](https://archivo.contagioradio.com/mujeres-de-cacarica-20-anos-espantando-la-guerra/)

<iframe id="audio_16778799" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16778799_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
