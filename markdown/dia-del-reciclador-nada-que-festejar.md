Title: Día del Reciclador, un oficio digno en condiciones indignas
Date: 2017-03-01 14:34
Category: Ambiente, Nacional
Tags: Bogotá, Dia del Reciclador, Esquema de Aseo, recicladores
Slug: dia-del-reciclador-nada-que-festejar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Recicladores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [01 Mar. 2017] 

Aunque en la actualidad hay un mayor reconocimiento y más normas y leyes que protegen el trabajo de los recicladores,  esto no se ve traducido en mejores condiciones de vida y de trabajo. Los **ingresos de los recicladores no superan los 3oo mil pesos y el actual sistema de aseo pretende que los recicladores** pasen de buscar la basura en unas canecas a hacerlo en containers.

Nohora Padilla integrante de la Asociación de Recicladores de Bogotá dice que se hace necesario que los gobiernos los escuchen, para que los programas que se crean vayan dirigidos a las personas que efectivamente lo necesitan, y no como en el caso de Bogotá “que **desde hace un poco de tiempo que le pueden pagar a los recicladores pero hay personas no recicladoras que se están llevando la mayor parte de esos recursos”.**

Esto a propósito de la nueva licitación que está llevando a cabo la Alcaldía de Bogotá en el tema de aseo, para el que se propone sean incluidos los recicladores y para quienes **adecuarían unos containers a donde será llevada la basura**. Le puede interesar: [Las razones de los recicladores para demandar licitación al servicio de aseo](https://archivo.contagioradio.com/recicladores-bogota-licitacion-aseo/)

**Licitación que ha preocupado a los recicladores dado que estos contenedores podrían deteriorar el trabajo de este gremio** “si introducen estos contenedores sin hacer una buena tarea de cultura ciudadana para la separación de la basura - desde donde los recicladores escogen el material-, no hay avance, porque debemos buscar el material en un contenedor que es de alto tamaño, el material se perdería” dice Nohora.

Para Nohora, con esta medida propuesta dentro del nuevo esquema de aseo de la capital no solo **se estaría afectando la situación de los recicladores, sino que se estaría hablando de agravar la situación de sostenibilidad ambiental de la ciudad** a raíz de la situación actual del relleno sanitario de Doña Juana. Le puede interesar: [Recicladores demandan licitación del servicio aseo de Bogotá](https://archivo.contagioradio.com/recicladores-demandan-licitacion-del-servicio-aseo-de-bogota/)

“Si mandamos todo lo que salga en la basura al relleno sanitario los pocos años que le quedan se van a cortar mucho más.  Por eso hemos hecho un llamado a la administración, diciéndoles que la idea es entendible pero injustificada en la medida que causa un gran impacto en el relleno y a los recicladores” relató Nohora

### **Cuál es la situación de los recicladores** 

**La mayor parte de los recicladores no logran ganarse la mitad de un salario básico**, es decir entre 300 mil y 400 mil pesos al mes y aunque hay un reducido número que alcanza al salario básico, ellos no cuentan con prestaciones.

“Ahí también encontramos una necesidad urgente de que **los Gobiernos en el sistema de aseo comiencen a mirar más el tema del trabajo de los recicladores y menos el negocio de los empresarios”** Le puede interesar: [Ciudadanos pagarán por reciclaje según nuevo esquema de aseo en Bogotá](https://archivo.contagioradio.com/ciudadanos-pagaran-por-reciclaje-segun-nuevo-esquema-de-aseo-en-bogota/)

Por último, Nohora como vocera de los recicladores pidió **a la ciudadanía una mayor colaboración en el tema del reciclaje “pedimos a las personas que en realidad comience a separar sus residuos pero que además se los entregue a los recicladores**, porque nosotros hacemos el trabajo desde hace mucho tiempo, lo hemos hecho en condiciones deplorables y creemos que tenemos derecho a que la comunidad nos mantenga su consideración”.

Bogotá es la ciudad que produce más basura al día en el país con 6.308 toneladas. Según cifras entregadas por el Ministerio de Vivienda existen, en ciudades capitales más de 30 mil recicladores y solo son  8 mil los que se encuentran formalizados.

<iframe id="audio_17302126" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17302126_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
