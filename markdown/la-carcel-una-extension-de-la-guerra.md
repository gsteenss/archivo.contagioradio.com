Title: La cárcel una extensión de la guerra
Date: 2019-11-18 15:49
Author: CtgAdm
Category: Expreso Libertad
Slug: la-carcel-una-extension-de-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/carcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal] 

 

Durante los más de 50 años de conflicto armado en Colombia, la cárcel ha representado un lugar de disputa para los diversos actores armados, que al contrario de otros escenarios no ha sido tenido en cuenta a la hora de buscar **verdad, justicia y** **reparación.**

<div style="text-align: justify;">

</div>

<div style="text-align: justify;">

En este programa del **Expreso Libertad** el profesor de sociología de la Universidad Nacional **Miguel Ángel Beltrán**, junto con el investigador **Miguel Pinzón**, hablaron sobre las afectaciones de la guerra a las cárceles y la impunidad que sierne sobre estos hechos.

</div>

<div>

</div>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2Fvl.417711228949394%2F417874342488364%2F%3Ftype%3D1&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

(Le puede interesar:S[in Olvido: Masacres en la Cárcel La Modelo](https://archivo.contagioradio.com/sin-olvido-masacres-en-la-carcel-la-modelo/))
