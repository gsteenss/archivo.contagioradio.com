Title: 15 millones de votos para vencer a los corruptos
Date: 2018-06-20 13:13
Category: Política
Tags: Angélica Lozano, anticorrupción, Claudia López, Consulta anticorrupción
Slug: consulta-anticorrupcion-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/DgJXk6PX4AEfEWN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Twitter 

###### 20 Jun 2018

En rueda de prensa realizada este miércoles el Ministro del Interior Guillermo Rivera explicó en detalle como se adelantará la **consulta anticorrupción** en Colombia, reglamentada mediante el Decreto 1028 de 18 de junio de 2018, convocada el domingo 26 de agosto por determinación presidencial.

Rivera aseguró que la iniciativa deberá alcanzar el umbral establecido en el 33% del censo electoral equivalentes a 12.000.261 ciudadanos quienes deberán contestar cada una de las 7 preguntas que componen la consulta. Para que se consideren aprobadas la mitad mas uno, osea 6.130.657 votantes, deberá haber marcado el sí en cada uno de los mandatos.

Rivera añadió que para esta consulta se aplican las mismas normas que para una elección ordinaria en materia de publicidad, encuestas y escrutinios, la campaña podrá iniciar hoy mismo, y también desde ya los ciudadanos se pueden inscribir ante el CNE como promotores del Sí o del No (Le puede interesar: ¿[Cómo se debe votar la Consulta Anticorrupción](https://archivo.contagioradio.com/como-se-debe-votar-la-consulta-anticorrupcion/)? )

El gobierno nacional y los gobiernos territoriales están en la obligación a promover la participación de las personas en la consulta según lo estipulado en la ley 1757 de 2015, por lo cual desde el Ministerio se  redactó una circular dirigida a servidores públicos de todos los niveles territoriales para recordarles tal compromiso, y pone a su servicio los instrumentos pedagógicos diseñados desde la dirección de democracia y participación de esa cartera.

**15 millones de votos por "7 veces sí"**

Por su parte Claudia López, promotora de la iniciativa ciudadana, recordó que es la "iniciativa de más respaldo ciudadano de la historia de Colombia" gracias a las más de 4 millones de firmas y más de 5 millones de cartas que enviaron al Congreso de la República pidiendo que fuera avalada, lo que se consiguió el pasado 5 de junio por unanimidad.

López celebró que el Presidente electo Iván Duque reiterara su compromiso frente a la consulta así como la invitación del ex candidato y próximo Senador Gustavo Petro a los ciudadanos a participar del mecanismo. "Todos estos hechos demuestran que al fin tenemos en Colombia una causa que nos une a todos: derrotar la corrupción"

Adicionalmente recordó que se llega a las urnas porque durante 8 ocasiones se le propuso al Congreso aprobar los mandatos que ahora componen la consulta y sistemáticamente los hundió "solo hasta que volvimos con 5 millones de ciudadanos a pedir permiso para ir a las urnas a votarlos logramos finalmente que el Congreso accediera a dar el aval"

Desde esa mirada crítica López aseguró que "la derrota de la corrupción se dará donde se tiene que dar en las urnas con el voto libre de los colombianos (...) estamos invitando desde hoy que arranca la campaña a 15 millones de votos 7 veces sí para aprobar los 7 mandatos de la Consulta anticorrupción"

Para la ex congresista los 7 mandatos son "indispensables para pegarle en el corazón a la corrupción de Colombia, para derrotar la corrupción que nos esta robando 50 billones de pesos al año" y recordó que la campaña no cuenta con ningún tipo de financiación pública, ni anticipos, ni reposición de votos de manera que "no es para enriquecer a nadie ni favorecer a nadie".

**Toda la ciudadanía es vocera de la consulta**

Por su parte Angélica Lozano, aseguró que al terminar la campaña presidencial "los ciudadanos de derecha, los de centro, los de izquierda, los abstencionistas, los que votan en blanco, todos y cada uno son voceros de estos 7 mandatos (...) esto es una causa común que nos une tenemos diferencias todas como ciudadanos y como políticos pero estamos de acuerdo en este marco".

"Para nosotros es un orgullo construir herramientas que nos unen como colombianos (...) aquí no hay voto en blanco aquí hay sí y hay no, y ambos son votos válidos y respetables de modo que vamos a sacar a Colombia adelante entre todos" puntualizó.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
