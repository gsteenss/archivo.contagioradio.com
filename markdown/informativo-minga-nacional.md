Title: Informativo Minga nacional
Date: 2016-05-31 17:13
Category: Paro Nacional
Tags: Paro Nacional
Slug: informativo-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Cumbre-Agraria.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cumbre Agraria 

###### [31 May 2016] 

Recuento en vivo de lo acontecido durante este miércoles con las movilizaciones que hacen parte de la Minga agropecuaria, indígena y popular en Colombia. Emisiones a las 6 y 8 p.m.

<iframe src="https://www.youtube.com/embed/KNzaGVG0IxY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
