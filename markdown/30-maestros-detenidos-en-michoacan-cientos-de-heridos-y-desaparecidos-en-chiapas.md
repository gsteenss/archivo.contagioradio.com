Title: 30 maestros detenidos en Michoacán, cientos de heridos y desaparecidos en Chiapas
Date: 2016-07-22 17:05
Category: El mundo
Tags: Coordinadora Nacional de trabajadores de la Educación, movilización maestros México, Reforma educativa en México
Slug: 30-maestros-detenidos-en-michoacan-cientos-de-heridos-y-desaparecidos-en-chiapas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Chiapas-maestros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Aquí Noticias ] 

###### [22 Julio 2016 ]

[Tras 2 meses de protestas en contra de la reforma educativa propuesta por el presidente mexicano Enrique Peña Nieto, este miércoles sobre las 2:30 de la tarde **fueron detenidos en Michoacán 30 profesores** al ser acusados de bloquear las vías del tren, argumento que es rechazado por la Coordinadora Nacional de Trabajadores de la Educación CNTE, que asegura que los docentes se encontraban en la carretera federal, exactamente en el tramo Lengua de Vaca, donde no hay presencia de ningún ferrocarril.]

Entretanto en Chiapas sobre el medio día un grupo paramilitar y de choque al servicio del gobierno municipal denominado ALMETRACH y en coordinación con la Policía, **desalojó violentamente a maestros, padres y madres de familia y organizaciones** que estaban realizando un bloqueo simbólico en la entrada de la autopista San Cristóbal-Tuxtla Gutiérrez. Los atacaron usando palos, piedras, varillas, cohetes y con disparos de arma de fuego. Se reporta un número indeterminado de personas gravemente heridas y algunas desaparecidas.

[Los profesores que están bajo disposición de la Fiscalía mexicana fueron trasladados hasta la ciudad de Morelia, Michoacán, tras ser capturados en la carretera de Zitácuaro, donde no hay redes ferroviarias. Mientras tanto, **miles de docentes continúan participando en los bloqueos de las redes** **ferroviarias** estatales de las ciudades de Maravatío, Yurécuaro, Uruapan, Morelia, Nueva Italia, Pátzcuaro y Lázaro Cárdenas.]

[Diversos sectores sociales y educativos de la región de Michoacán se unen a las protestas de la CNTE, entre ellos ocho escuelas normales e indígenas de la etnia purépecha, por lo que los **bloqueos en gran parte del estado mexicano se intensifican**. A la fecha, la Comisión Nacional de Derechos Humanos en México registra ocho personas fallecidas en Oaxaca y 2 en Nochixtlán.]

[A causa de estos lamentables acontecimientos, el anuncio de despido para más de 200 docentes y la [[falta de un buen diálogo sobre la reforma educativa](https://archivo.contagioradio.com/maestros-mexicanos-anuncian-que-las-movilizaciones-continuaran/)] impulsada por el gobierno de Peña Nieto, **la CNTE suspendido clases en 19.500 escuelas de educación básica**.]

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
