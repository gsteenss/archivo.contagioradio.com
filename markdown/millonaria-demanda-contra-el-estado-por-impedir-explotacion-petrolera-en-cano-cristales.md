Title: Millonaria demanda contra el Estado por impedir explotación petrolera en Caño Cristales
Date: 2017-08-11 12:40
Category: Ambiente, Voces de la Tierra
Tags: ANLA, Caño Cristales
Slug: millonaria-demanda-contra-el-estado-por-impedir-explotacion-petrolera-en-cano-cristales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Caño-Cristales-e1460593392319.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [10 Ago 2017] 

Más de **\$83.000 millones podría pagar el Estado colombiano a la multinacional Hupecol,** luego de que gracias a la presión social la ANLA decidiera revocara la licencia a la empresa para que realizara actividades de exploración y explotación de crudo de 150 pozos petroleros en medio de tres parques naturales entre los cuales se encuentra **Caño Cristales**. Una decisión que significó un respiro para las y los colombianos que veían en grave riesgo el denominado 'río más hermoso del mundo'.

Sin embargo, la compañía petrolera no se quedó quieta y en octubre de 2016 interpuso una demanda contra el Estado, según una investigación de El Espectador, argumentado que "la revocatoria del permiso generó **\$20.881 millones en pérdidas por las inversiones que ya se habían hecho para el proyecto, y \$62.177 millones** por lucro cesante, razón más que suficiente —en su criterio— para pedirle al Tribunal Administrativo de Cundinamarca que proteja sus intereses". (Le puede interesar:["Así afectará explotación petrolera a Caño Cristales"](https://archivo.contagioradio.com/presumo-que-hubo-torciditos-para-lograr-esa-licencia-lamacarena/))

En la demanda de 60 páginas radicada el 6 de octubre del año pasado, a la que tuvo acceso El Espectador, señala que el concepto de Cormacarena, mediante el cual decide revocar la licencia llegó por fuera del tiempo establecido por la ley, y que la ANLA tomó revocó ****la licencia con base en dos solicitudes de revocatoria que no tenían sustentos científicos.  ****(Le puede interesar:["Empresa Petrolera teñirá de negro las aguas de Caño Cristales")](https://archivo.contagioradio.com/empresa-petrolera-teniria-de-negro-cano-cristales/)

### **Amenaza latente** 

Apenas se revocó una licencia de un bloque petrolero. A**ún existen 12 licencias ambientales más para extraer petróleo y realizar actividades mineras. Asimismo, t**odavía hay cuatro bloques más que pertenecerían a las empresas **Hupecol, PetroNova, Hocol y Petrominerales. También hay 7 títulos mineros para Arenas Bituminosas y otros dos títulos mineros para extraer oro**, que se encontrarían en zona de amortiguamiento de las reservas naturales de esa parte del país.

[![consultas-populares](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2011/09/consultas-populares.png){.alignnone .size-full .wp-image-45110 width="1600" height="244"}](http://bit.ly/2vwVIpg%20)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
