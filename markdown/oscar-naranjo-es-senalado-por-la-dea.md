Title: DEA vincula al general (r) Óscar Naranjo con narcotráfico
Date: 2016-02-26 16:11
Category: Judicial, Nacional
Tags: Comunidad del Anillo, Oscar Naranjo, Policía Nacional
Slug: oscar-naranjo-es-senalado-por-la-dea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Oscar-Naranjo-e1456520386148.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Independent 

###### [26 Feb 2016] 

De acuerdo con una investigación realizada por la revista mexicana Proceso, el general (r) **Oscar Naranjo, fue vinculado con  el narcotráfico por la  Agencia Antidrogas de Estados Unidos, DEA.** Naranjo fue señalado de acudir al paramilitarismo como parte de su estrategia para combatir al crimen organizado.

Según los reportes que desarrolló la DEA y que habrían detenido el ascenso del general Luis Eduardo Martínez, los exdirectores de la policía Rosso José Serrano Cadena, José Roberto León Riaño y Óscar Naranjo, **habrían recibido sobornos derivados del narcotráfico a cambio de recibir “información privilegiada”,** como lo dijo uno de los reportes en marzo de 2011 en los que aparece la declaración del mayor de la Policía en retiro Byron Ernesto Ordoñez.

Naranjo ya había sido acusado de tener nexos vínculos con cárteles del narcotráfico, pero el exdirector de la policía se defendió diciendo que, esos señalamientos eran “una especie de “persecución en su contra”, afirmando que durante sus cinco años en la Policía Nacional dio  “certeros golpes” al narcotráfico, negando conocer las declaraciones del narcotraficante Juan Carlos Ramírez, conocido como ‘Chupeta’, quien lo nombró varias veces en sus testimonios.

En enero de 2014, **Naranjo fue acusado por el Partido de la Revolución Democrática, PRD, de favorecer la creación de grupos paramilitares en Michoacán.** Por lo que Alejandro Sánchez Camacho, quien era secretario general del PRD pidió una investigación contra el exasesor de Nieto, sosteniendo que había promovido el surgimiento de grupos paramilitares para enfrentar el narcotráfico.

Incluso, **Juan David Naranjo, hermano del general, fue detenido en 2006 en Alemania al haber intentado vender 35 kilogramos de coca** a dos policías encubiertos.

En Colombia el general (r) ha sido acusado por haber realizado detenciones ilegales cuando tuvo el cargo de comandante en Cali, mandando a encarcelar a 182 personas en un operativo. Por otra parte, Daniel Rendón Herrera, alias **“Don Mario”, ex jefe paramilitar, aseveró que en 2004 Óscar Naranjo se encontró con el narcotraficante y paramilitar Miguel Arroyave** para hablar del asesinato de un coronel de la policía colombiana.

Esta semana, la Procuraduría General de la Nación llamó al general a declarar, junto a otros miembros de la Policía Nacional, en el marco de las investigaciones que se adelantan respecto a la  presunta red de prostitución masculina al interior de la policía, conocida como la 'Comunidad del anillo'. Sumado a eso, Naranjo se encuentra bajo la lupa de la DEA por narcotráfico.
