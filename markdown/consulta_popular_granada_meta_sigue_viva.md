Title: "La consulta sigue viva": habitantes de Granada, Meta
Date: 2017-10-23 16:17
Category: Ambiente, Voces de la Tierra
Tags: consulta popular, Granada, Ministerio de hacienda, REgistraduría
Slug: consulta_popular_granada_meta_sigue_viva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/habitantes-granada-e1508780519775.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zoraya Ángel] 

###### [23 Oct 2017] 

Más de 3500 personas se movilizaron este domingo en  el municipio de Granada, Meta, ante las decisiones del gobierno nacional y la registraduría al informar que no era posible la realización de la consulta popular del 22 de octubre debido a la falta de recursos. Ante ello, la indignación de los habitantes se demostró en las calles con la consigna "La consulta sigue viva".

Según explica Aristides Rodríguez, vocero del Comité del NO del gremio agua y alimentos, faltando tres días la **Registradruíra y el Ministerior de Hacienda realizaron un documento fechado el 3 de octubre, pero entregado a la administración municipal el 17 de octubre, en el que se decía que la consulta popular no podría realizarse** por que no había posibilidad de financiarla.

Ante ese hecho, Rodríguez indica que si bien, no se logró ejecutar la consulta, se llevó acabo dicha marcha con el objetivo de "rechazar veementemente el atropello del Ministerio de Hacienda que de forma tramposa nos pretenden arrebatar una consulta propia de los pueblos".

Asimismo, la alcaldía ha anunciado que adelantará las medidas jurídicas necesarias, **como interponer una tutela**, para que se pueda garantizar la participación de la ciudadanía. (Le puede interesar: [Registraduría pide suspender consulta popular en Granada)](https://archivo.contagioradio.com/por-falta-de-presupuesto-se-suspende-consulta-popular-en-granada-meta/)

Teniendo en cuenta que son 17 los municipios de la región del Ariari, que buscan impedir el desarrollo de proyectos petroleros,  los comités del NO, y la comunidad en general trabajan este lunes en una hoja de ruta para hacer qu**e la consulta popular sea un hecho y se realice el próximo 5 de noviembre, como lo han establecido.**

### **Solo 58 mesas de votación** 

Por otra parte, denuncian que antes de informarse que sería suspendida la consulta, **la Registraduría había eliminado 110 mesas de votación reduciéndolas a 58 y** exigen se garanticen el derecho a la participación de los habitantes y sean habilitadas todas las mesas, como en su momento se hizo para la votación del plebiscito por la paz.

Mientras tanto las comunidades continúan en asamblea permanente y comunican que en los próximos días se llevarán a cabo, no solo las acciones jurídicas necesarias para que haya consulta, sino las actividades de movilización exigiendo que se destinen los recursos necesarios para que los pobladores puedan decidir sobre el futuro de sus territorios.

<iframe id="audio_21636224" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21636224_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
