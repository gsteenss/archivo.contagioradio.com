Title: Departamento de Sociología de la UN se solidariza con familia de Mateo Gutiérrez
Date: 2017-03-07 11:52
Category: DDHH, Nacional
Tags: Falsos Positivos Judiciales, Mateo Gutierrez
Slug: departamento-de-sociologia-de-la-un-se-solidariza-con-familia-de-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/facultad-de-sociología.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Libertad para Mateo ] 

###### [07 Mar 2017] 

Ya se cumple una semana tras el arresto de Mateo Gutiérrez, estudiante de Sociología de la Universidad Nacional y continúan sumándose los apoyos en defensa de su libertad.  **El Departamento de Sociología de la Universidad Nacional, a través de un comunicado de prensa, expresó su preocupación por la retención del estudiante** y envío un mensaje de solidaridad a la familia.

En el texto, el Departamento también hace un llamado al **“debido proceso y al respecto de las garantías jurídicas para Mateo”** y a los medios de información que han señalado desde un principio al estudiante como culpable. Le puede interesar: ["Se conforma red de solidaridad en defensa de Mateo Gutiérrez"](https://archivo.contagioradio.com/red-solidaridad-defensa-mateo-gutierrez/)

“Expresamos nuestra **preocupación frente a la opinión pública por las generalizaciones aparecidas en la prensa nacional** que, olvidando el principio fundamental de la presunción de inocencia, de manera sistemática hace asociaciones indebidas y sin ningún sustento entre nuestro Departamento, la lucha armada y otras prácticas anti-democráticas de expresión política" y agregó que rechazan **"manera rotunda y definitiva"** cualquier forma de resolución violenta de las contiendas políticas.

**El próximo viernes 10 de marzo, se desarrollará un plantón en frente de las instalaciones de los Juzgados de Paloquemado**, sobre las 4 de la tarde, para exigir la libertad del estudiante de 20 años. Le puede interesar: ["Mateo Gutiérrez León serían un falso positivo judicial"](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/)

Comunicado

*El Departamento de Sociología expresa su preocupación por la retención del estudiante Mateo Gutiérrez León y extiende su mensaje de solidaridad y apoyo a los padres. Al mismo tiempo, a nombre del Claustro de Profesores se hace un llamado al debido proceso y al respecto de todas las garantías jurídicas para Mateo.*

*Finalmente, expresamos nuestra preocupación frente a la opinión pública por las generalizaciones aparecidas en la prensa nacional que, olvidando el principio fundamental de la presunción de inocencia, de manera sistemática hace asociaciones indebidas y sin ningún sustento entre nuestro Departamento, la lucha armada y otras prácticas anti-democráticas de expresión política. El Departamento de Sociología rechaza de manera rotunda y definitiva cualquier forma de resolución violenta de las contiendas políticas.*

<div class="text_exposed_show">

*Andrea Lampis (Ph.D.)*

*Profesor Asociado*  
*Director Departamento de Sociología*  
*Facultad de Ciencias Humanas*  
*Universidad Nacional de Colombia*  
*alampis@unal.edu.co*  
*Tel. (57) 1 - 3165000 - ext. 16205*

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

</div>
