Title: Lo que nos deja el 27, un respiro
Date: 2019-10-28 15:58
Author: Camilo de las Casas
Category: Opinion
Tags: colombia, FARC, paz, Votaciones, votar
Slug: lo-que-nos-deja-el-27-un-respiro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/elecciones2019.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### *28 de octubre de 2019* 

En la breve historia que nos ha sido concedida **es un aliento constatar electoralmente lo que se venía percibiendo semanas atrás**: el rechazo a Uribe expresado de modo particular en los sectores jóvenes de 18 a 30 que se lanzaron a romper los esquemas de seguridad del superprotegido expresidente.

Las elecciones de ayer muestran los atisbos del nuevo país o por lo menos de un sector amplio de electores que hoy se mueven hacia asuntos que durante los últimos 30 años eran opacados por demonios creados como los terroristas de las **FARC** y el **ELN**. Ellos por supuesto, gracias a sus actuaciones cuestionables e incompresibles en su guerra de guerrillas, cuando afectaron a la población, aportaron a la industria mediática guerrerista perdiendo poco a poco sintonía con el país cambiante. Y, hay que escribirlo también, ese fantasma afectó los tímidos intentos y precarias expresiones políticas alternativas que también cayeron en dinámicas de exclusión, de corrupción, de retórica sin administración eficaces o sin suficiente claridad de sintonía ante los ciudadanos.

Le puede interesar: ([El derecho a la verdad es el de todas las verdades](https://archivo.contagioradio.com/el-derecho-a-la-verdad-es-el-de-todas-las-verdades/))

Lo llamado alternativo está hecho de muchos matices y aprendizajes de convivencia, coexistencia temporal, a veces efímera de sectores democráticos y de izquierda en sus múltiples fragmentaciones.

Los resultados de ayer reflejan un sector elector que combina los millennial y generaciones nacidas hace treinta años, que se asquean de la corrupción política, que están agotados con discursos que encubren cualquier atisbo de violencia o de autoritarismo, que se experimentan sensiblemente con el animalismo, con la protección del planeta, contra el machismo y ejercicios patriarcales, que se distancian de ideologizaciones de la vida cotidiana que quieren hechos, decisiones prácticas y concretas frente a libertades básicas que deben evitar perderse.

Así pocos hayan hablado del tema de la paz, la mayoría de los votantes estan cansados de la guerra en las ciudades como Bogotá, Cali, Medellín. En estas urbes resultó derrotado el uribismo. Y en ciudades de provincia en dónde ellos o los partidos tradicionales han mantenido el poder, también hay algún tipo de ruptura. Hay avances importantes.

#### Los votantes dejan de ser borregos lógicos. 

Excepciones evidentes en Barranquilla en donde el poder de la mafia política sigue dominando. Los 10 millones de Duque, que son de Uribe se hicieron pedazos. El asunto lejos de volver a la guerra, es del bolsillo, de la economía, de las apuestas que tienen los votantes de ciudades saludables, sostenibles, respetuosas de la diversidad y de los derechos.

Con Claudia López lejos de ganar Fajardo, como lo cuestionan muchos petristas, se expresa la **anticorrupción**, la diversidad, las personas hechas a pulso, la identidad de las nuevas sensibilidades ambientales, y en particular, la fuerza femenina, que puede ser crítica, que puede ser voraz, que puede ser desbocada y de alguna manera con rasgos de patriarcalismo y de caudillismo (femenino), apuestas acertadas en un tiempo en que la guerra ni la paz son atractivas y en que la justicia socio ambiental es conquistable en pasos a pasos. Y eso en sí mismo, es distante de ser cuestionable es un modo de ejercer la política con la que algunos se identifican, y que logra efectos prácticos de identificación con esos nuevos ciudadanos.

#### Por allí está pasando lo nuevo, lo viejo no atrae, no llama, no convoca ni evoca. 

Así se expresó el respaldo a Galán con su millón de votantes. Y por eso mismo, lo que hoy debe ser crisis en las iglesias fundamentalistas cristianas, dado que sus feligreses no le votaron a Uribe Turbay. Su poder está sobre algunas mente,s no todas. Los asuntos de las almas son de la libertad que se expresa en cada ser humano. Muchas personas de esas expresiones religiosas siguen en la iglesia pero votando en contra de lo que promovió su pastor, que era refrendar la corrupción asegurando un modelo único de familia.

Y he de escribir, contra mis propias lecturas que el petrismo en Bogotá, y otras partes del país, lograron significativos resultados ante una odiosa comparación con el uribismo que plantean los opinadores mediáticos. Nunca creí que Petro entre su imagen soberbia y en su rifirrafe con López, lograra mantener una base leal de votantes al respaldar al cuestionado Holman Morris. Esos ciudadanos de la Colombia Humana cerca de medio millón debe ser reconocida sin desprecio, y según actúen en el concejo y exista una democratización de las decisiones de esta expresión política, podrán crecer en un gran espectro de centro hacia la izquierda.

Las situaciones de conflictividad armada siguen sin ser lo dominante para los electores, tristemente, cuando estamos hablando del **derecho a la vida,** cuando todos sabemos que las armas hieren o matan, y desencadenan dolores, y desatinadas venganzas. Un hecho que debe ser leído para los futuros escenarios de paz con el ELN y de salida a las diversas conflictividades armadas. Estas aparentemente, en particular, con el narcotráfico siguieron cumpliendo un papel soterrado en muchos municipios en donde el control parainstitucional o heredero del paramilitarismo es evidente

#### Perdió el Uribismo, crece la posibilidad de una nueva apuesta política en Colombia. 

Difícilmente Duque va a enderezar el rumbo. Él se mantendrá con el fantasma de Uribe y en el servilismo a Trump. Seguirá en la cruzada hipócrita de la paz en un escenario latinoamericano de poderes ciudadanos en las calles en Chile, en Ecuador, en Argentina ante el modelo neoliberal y sus graves consecuencias sociales y ambientales, por el presente y las generaciones del porvenir. Así como otras expresiones ciudadanas contra la consolidación de poderes presidenciales como en Nicaragua y en Bolivia.

Nada es fórmula en Colombia. Colombia es Colombia. Se conocerán seguramente las practicas corruptas y de criminalidad en muchos pequeños municipios en dónde como expresan algunos ciudadanos*: “Ellos son poderosos, ellos son los que mandan”* y en las ciudades quizás en la calles los nuevos alientos que se animen a seguir apostando por atisbos de un nuevo país.
