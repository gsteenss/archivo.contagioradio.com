Title: En Bogotá " Se Puede Ser"
Date: 2015-04-12 20:15
Author: CtgAdm
Category: closet, LGBTI
Tags: Bogotá Humana, Closet Abierto, LGBTI, Ricardo Montenegro, Se puede ser, Sub Secretaría LGBTI
Slug: en-bogota-se-puede-ser
Status: published

<iframe src="http://www.ivoox.com/player_ek_4329776_2_1.html?data=lZifm5ybeo6ZmKiakpaJd6KplYqgo5mYcYarpJKfj4qbh46kjoqkpZKUcYarpJKwztTXqdWfosfWx9fYs4ynkZDRx5CxpdPu0JDQ0dOPlsrXwtfR0ZCxs8%2FoxtPSydfTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Closet Abierto 30 de Marzo] 

Se abre un espacio dentro del programa Closet Abierto para la Subdirección de asuntos LGBTI de Bogotá, con el abogado defensor de Derechos Humanos, Ricardo Montenegro, quien hace parte de esta organización y aboga por el bienestar de este grupo a menudo discriminado y vulnerado.

Partiendo de la política pública LGBTI en Bogotá Se Puede Ser, Ricardo Montenegro nos explica los beneficios del mismo y como elemento primordial, los cambios sociales que se han obtenido desde que esta política se propuso como campaña.

Más allá de la política pública, se abordan temas que afectan en gran medida a los sectores LGBTI y que la Subdirección se ha encargado de acompañar a lo largo de su proceso en toda clase de contextos, se aprovecha este espacio para visibilizar la labor de la organización e invitar a los oyentes a que hagan parte de la misma.
