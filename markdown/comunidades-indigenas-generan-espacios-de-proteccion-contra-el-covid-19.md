Title: Comunidades indígenas generan espacios de protección contra el COVID - 19
Date: 2020-03-27 07:03
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Comunidades Indígenas de Colombia, Covid-19
Slug: comunidades-indigenas-generan-espacios-de-proteccion-contra-el-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Indígenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Indígenas / CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante el nivel de vulnerabilidad que existe para las zonas rurales, los grupos étnicos y pueblos indígenas de Colombia, desde hace varios días se iniciaron acciones en los territorios para evitar qué la pandemia se pueda propagar en las comunidades, siendo la articulación, el trabajo espiritual y el aislamiento las herramientas principales de autoprotección para las 102 comunidades ancestrales que habitan en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la Organización Nacional Indígena de Colombia (ONIC), se invitó a adoptar las medidas de prevención por medio de la medicina tradicional, plantas propias y conocimientos ancestrales; el control territorial y la soberanía alimentaria. Una medida que se toma como prevención ante los **aún sin confirmar, dos primeros casos de Covid-19 entre las comunidades indígenas del Pueblo Yukpa asentado en el barrio El Escobar en Cúcuta**, **zona de frontera.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre este pueblo, la ONIC ha advertido que los pueblos indígenas ubicados en zona de frontera como los Yukpa, los Amorua y los Sicuanis se encuentran "en condiciones deplorables y en estado de mendicidad en las calles". [(Lea también: Los Amorúa, el pueblo indígena que sobrevive entre las basuras)](https://archivo.contagioradio.com/pueblo-amorua-el-pueblo-indigena-que-sobrevive-entre-las-basuras/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Resistiendo en medio del fuego y los grupos armados en la Sierra Nevada

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Es por ello que desde hace algunos días, los indígenas de la Sierra Nevada de Santa Marta, conformadas por los arhuacos, wiwas, kogis y kankuamos que juntos, suman más de 30.000 personas, se acogieron a la cuarentena decretada a nivel nacional, resaltando lo positivo que resultará detener las actividades humanas cotidianas en os beneficios que generará la suspensión de actividad humana a la flora y la fauna. [(Lea también: Nuevos incendios en Sierra Nevada afectan a más de 600 familias)](https://archivo.contagioradio.com/incendios-sierra-nevada/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La situación resulta difícil para una población que además de tener que combatir los continuos incendios que desde inicios de marzo, según las autoridades habrían consumido más de 900 hectáreas de bosque seco, también se encuentran en riesgo por la presencia de grupos armados en la zona como ‘Los Pachencas’, además de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y la reaparición del ELN en la zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se suman los años de lucha contra proyectos mineros que que según las autoridades indígenas, podrían acabar con la totalidad de la Sierra entre 5 a 10 años [(Le puede interesar: Las prácticas turísticas que más dañan los ecosistemas según Pueblos Indígenas de la Sierra)](https://archivo.contagioradio.com/las-practicas-turisticas-que-mas-danan-los-ecosistemas-segun-pueblos-indigenas-de-la-sierra/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La Guajira sin agua ni garantías

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A este llamado se han sumado organizaciones como la Asociación de Alaulayus y Cabildos Indígenas del Sur De La Guajira quienes piden a las comunidades Wayúu de los municipios de Barrancas, Hatonuevo y Distracción promuevan la prevención, contención y mitigación de los riesgos de la pandemia que hoy se encuentra en más de 170 países.  
  
Al respecto, Ever Heriberto Ojeda, representante Legal de la Asociación de Alaulayus y Cabildos Indígenas del Sur de la Guajira hizo un llamado al Gobierno naciona y departamental para al gobierno nacional, gobierno departamental resaltando la vulnerabilidad de las comunidades indígenas "ante el peligro de expansión de la pandemia COVID-19 por las condiciones sociales en las que vivimos", por lo que pidió se enfatice la atención de salud en los adultos mayores wayúu de cada municipio. [(Le puede interesar: Carbones del Cerrejón incumple sentencia que protege a comunidades y al Arroyo Bruno)](https://archivo.contagioradio.com/carbones-del-cerrejon-incumple-sentencia-que-protege-a-comunidades-y-al-arroyo-bruno/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La pandemia de la violencia contra indígenas en Cauca

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Dichas recomendaciones se dan en medio de un aislamiento preventivo obligatorio en condiciones precarias, pues el pueblo Wayúu no cuenta con agua, acceso a salud, alimentación, ni un plan especial diferencial étnico para atender al Covid-19. Desde la ONG, Nación Wayúu han advertido además que las comunidades Wayúu están obligadas a salir a los poblados más cercanos para poder realizar sus trueques.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En Cauca, donde el pasado 25 de marzo y en medio de la cuarentena se dieron hostigamientos de grupos armados en zonas cercanas a los municipios de Caldono, Toribio y Totoró, el [Consejo Regional Indígena del Cauca](https://twitter.com/CRIC_Cauca)también ha instaurado medidas de prevención como restringir el ingreso de personas que no pertenecen a las comunidades indígenas, evitar el ingreso de alimentos importados ni comprar en almacenes de cadena, dando prioridad a la soberanía alimentaria. Asímismo los trueques e intercambios deben realizarse en zonas donde no hayan multitudes, además han invitado a escuchar únicamente sus medios de comunicación por medio de los cuales se estará emitiendo información.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La situación tampoco es fácil, durante la cuarentena ya han sido asesinados dos líderes del pueblo Embera mientras permanecían en sus casas. Todo se da en medio de un contexto en el que los pueblos ancestrales conviven con las disidencias de las FARC, paramilitares y grupos como el Cartel de Sinaloa, además de una fuerte militarización del departamento donde según el CRIC, 22 indígenas fueron asesinados en este departamento en 2018 y **56 hasta noviembre de 2019.** [(Lea también: Acatando cuarentena en su hogar, asesinan a dos líderes indígenas en Valle del Cauca)](https://archivo.contagioradio.com/cuarentena-asesinan-indigenas-valle/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CRIC_Cauca/status/1241834045211848710","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CRIC\_Cauca/status/1241834045211848710

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Indígenas limitan la circulación

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte la Asociación de Autoridades Tradicionales y Cabildos U'Wa también acogieron las medidas decretadas por el Gobierno suspendiendo las actividades comunitarias, salidas y entradas de los resguardos del pueblo U'Wa en las 17 comunidades índigenas en Boyacá, Santander y Norte de Santander, limitando la circulación a la prestación de servicios de medicina tradicional, tratamientos medicinales culturales y adquisición de bienes, mercancías u objetos de primeros auxilios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras las autoridades del resguardo indígena Chaparral Barro Negro, han cerrado indefinidamente las vías de acceso al resguardo en los municipios de Hato Corozal, Sacama y Tamara en Casanare, otros, como los Pueblo Inga y Kamentzá hacen control territorial en el Valle de Sibundoy, Putumayo, acatando el aislamiento preventivo obligatorio, han manifestado que requieren víveres, tapabocas, guantes entre otros elementos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
