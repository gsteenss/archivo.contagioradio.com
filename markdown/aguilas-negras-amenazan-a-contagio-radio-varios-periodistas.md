Title: Águilas Negras amenazan a Contagio Radio y a periodistas de medios de comunicación
Date: 2020-11-20 14:21
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Águilas Negras, Libertad de Prensa, Movilización social, Panfletos
Slug: aguilas-negras-amenazan-a-contagio-radio-varios-periodistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Aguilas-Negras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Panfleto atribuido a Águilas Negras

<!-- /wp:paragraph -->

En la mañana de este 19 de noviembre se conoció una amenaza de muerte de las Águilas Negras a varios periodistas, congresistas, líderes políticos y el equipo periodístico de Contagio Radio.

<!-- wp:paragraph {"align":"justify"} -->

El panfleto llegó a la oficina de la [Agencia Reporteros Sin Fronteras,](https://twitter.com/RSF_esp) ubicada en el barrio Teusaquillo de la ciudad de Bogotá el día 18 de noviembre. Según lo reportado por ese medio, el panfleto fue dejado por debajo de la puerta de su sede en un sobre de manila sellado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las Águilas Negras han amenazado a diversos líderes sociales y de la movilización durante las últimas semanas, entre ellos **Iván Cepeda, Gustavo Petro, Gustavo Bolívar, Antonio Sanguino, los representantes de las comunidades indígenas Hermes Pete y Joe Sauca; los defensores de DD.HH. Luis Emil Sanabria y Luis Alfonso Castillo: los periodistas Juan Manuel Arango, Julián Martínez, José Luis Mayorga, William Parra, Ignacio Gómez, Jorge Enrique Botero, Hollman Morris, Guillermo Rico y al medio Prensa Rural**. [(Lea también: Los intereses tras las Águilas Negras en Colombia)](https://archivo.contagioradio.com/los-intereses-tras-las-aguilas-negras-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Contagio Radio ha sido víctima de perfilamientos, hostigamientos e interceptaciones en diversas operaciones de inteligencia ilegal. Ninguna de las actuaciones de persecución contra Contagio ha sido investigada con eficacia por parte de la Fiscalía General de la Nación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Contagio, que se inició en la década de los noventa ha concentrado sus capacidades técnicas y humanas para dar resonancia a voces territoriales y miradas críticas en relación con la democracia, los DD.HH, los derechos ambientales, los derechos de los animales, las mujeres y diversidades sexuales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma ha venido acompañando de manera particular el desarrollo del proceso de paz, ha realizado seguimientos en territorios remotos a los espacios abiertos por organizaciones locales en las enunciaciones extrajudiciales de verdad de responsables con víctimas. De la misma manera tiene información reservada sobre responsables de alto nivel en la comisión de crímenes de lesa humanidad en las últimas dos décadas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
