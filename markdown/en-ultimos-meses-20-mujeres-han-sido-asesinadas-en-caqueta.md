Title: En últimos meses 20 mujeres han sido asesinadas en Caquetá
Date: 2017-11-21 14:39
Category: Nacional, Paz
Tags: acuerdos de paz, Caquetá, ForumSYD, Misión de Verificación, misión internacional
Slug: en-ultimos-meses-20-mujeres-han-sido-asesinadas-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/caquetá-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  A la Orilla del Río] 

###### [21 Nov 2017] 

En el marco del desarrollo de la quinta Misión Internacional de Verificación y Monitoreo, que hace parte del Espacio de Cooperación para la Paz, se realiza en Caquetá, la primera sesión del **Consejo Territorial de Paz, Reconciliación, Convivencia y Derechos Humanos**. Allí, las mujeres han denunciado que no hay condiciones de seguridad y desarrollo para la población femenina del territorio.

Con la presencia del Alto Comisionado para la Paz, el Gobernador del Caquetá, la Alta Consejería presidencial para los Derechos Humanos, diversos colectivos y comunidades, la delegada de la Plataforma de Mujeres del Caquetá indicó que **en los últimos meses 20 mujeres han sido asesinadas.**

Con esto en mente, afirmó que es difícil garantizar la consecución de la paz, cuando en el departamento **hay mujeres que viven en la extrema pobreza** y no poseen mecanismos para ejercer actividades que las impulse a ejecutar acciones para el desarrollo. Como lo indican ellas, en Caquetá, “tiene más tierra una vaca que una mujer”. (Le puede interesar: ["La falta de implementación de los Acuerdos de Paz impiden la paz territorial"](https://archivo.contagioradio.com/la-falta-de-implementacion-de-los-acuerdos-de-paz-impide-la-paz-territorial/))

Adicionalmente, en esta sesión, el delegado de la Coordinación Departamental del Caquetá, denunció que en el departamento **son asesinadas dos personas semanalmente** en hechos relacionados con el sicariato. Por esto, argumentó que la inseguridad ha incrementado en todo el territorio. Sin embargo, el gobernador de Caquetá, Álvaro Pacheco, afirmó que Florencia lleva 32 días sin muertes violentas.

A estos problemas se suma la denuncia la denuncia de la organización ambiental La Otra Orilla, en donde su representante, Walter Ciro, manifestó que una de las grandes víctimas por la falta de compromiso con la implementación del Acuerdo de Paz ha sido el medio ambiente. La organización denunció que, tan solo en San Vicente del Caguán, han sido taladas **40 mil hectáreas de selva virgen.** (Le puede interesar:["Organizaciones internacionales apoyan Acuerdo de Paz en Colombia"](https://archivo.contagioradio.com/organizaciones-internacionales-apoyan-acuerdos-de-paz-en-colombia/))

Estas preocupaciones han sido manifestadas en este intercambio con las autoridades como parte del acompañamiento sostenido de organizaciones internacionales como ForumSYD. A lo largo de la semana, el Espacio de Cooperación para la Paz, trabajará con diferentes comunidades de Caquetá y Huila para verificar la implementación de los Acuerdos de Paz.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
