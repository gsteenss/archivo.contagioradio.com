Title: Operativo militar en Guapi generaría desplazamiento de comunidades
Date: 2015-05-22 12:55
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Cauca, Cese unilateral de fuego, COCOCAUCA, FARC, Guapi, Juan Manuel Santos, Orlando Pantoja
Slug: operativo-militar-en-guapi-cuaca-generaria-desplazamiento-de-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/desplazamiento-guapi-cauca-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: cococauca.org 

Según la información inicial entregada por la Asociación de **Consejos Comunitarios de la Costa Caucana, COCOCAUCA,** que tiene presencia en la región de **Guapi**, el operativo militar que se está realizando en la región y que dejó 26 guerrilleros de las FARC muertos, podría generar el **desplazamiento de cerca de 600 personas habitantes del municipio.**

Orlando Pantoja, integrante de COCOCAUCA, afirma que los **operativos militares** se están desarrollando desde las veredas de la parte alta del municipio hacia las zonas bajas y los centros poblados. Aunque no se tiene información precisa sobre las **consecuencias de los enfrentamientos para la población civil,** se teme que como en otras ocasiones, se generen graves efectos como y hasta muertes de civiles.

La región ha sido azotada por el desplazamiento forzado y solamente la semana pasada se logró el retorno de cerca de 800 personas que se vieron obligadas a salir por operaciones paramilitares. Lo complicado de la situación radica en la **presencia y el desarrollo de proyectos productivos de gran envergadura en la región.**

Tras el rompimiento del cese unilateral por parte de las FARC, que se mantuvo durante 5 meses y que logró la **reducción cercana al 90% de las acciones de la guerra** en esa región colombiana, la gran mayoría de la población afrodescendiente y todos los y las habitantes del departamento del Cauca afirman que crece de nuevo la zozobra y el terror por la fuerte presencia militar que desde hace varios meses se ha venido incrementando.
