Title: Rechazan nombramiento de brigadier general implicado en falsos positivos a Fuerza de Tarea
Date: 2019-01-10 09:44
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: bajo cauca antioqueño, Caso de Falsos Positivos, COEUROPA, Ejecuciones Extrajudiciales, Human Rights Watch
Slug: rechazan-nombramiento-brigadier-general-implicado-falsos-positivos-cauca
Status: published

###### Foto: Blu Radio 

###### 09 Ene 2019 

[La comunidad del Bajo Cauca antioqueño, junto a organizaciones defensoras de derechos humanos, rechazaron el nombramiento del General brigadier Edgar Alberto Rodríguez Sánchez como comandante de la Fuerza de Tarea Conjunto Aquiles, por la implicación del militar en **por lo menos 22 casos de los mal llamados "falsos positivos" o ejecuciones extrajudiciales. **]

El 28 de diciembre de 2018, el ministro de defensa Guillermo Botero anunció que el General brigadier fue asignado comandante de esta nueva fuerza de tarea, compuesto de 2.500 miembros de la Fuerza Pública, que tiene como objetivo desmantelar los grupos armados ilegales presentes en la subregión y combatir las economías ilegales.

[Rodríguez se encuentra bajo investigación de la Fiscalía General por 22 casos de ejecuciones extrajudiciales cometidos entre julio 2006 y diciembre 2007, mientras que se desempeñaba como Comandante del Batallón Magdalena de la Novena Brigada, ubicado en el Huila, según Human Rights Watch. ]

[Oscar Yesid Zapata, integrante del Nodo Antioquia de la Coordinación Colombia Europa Estados Unidos, manifestó que en cambio de mejorar la situación de seguridad y confianza de las comunidades en el Estado, la llegada de Rodríguez al Bajo Cauca podría “generar nuevos factores de riesgo” en la subregión donde ya se registra altos índices de asesinato de líderes sociales, desplazamiento y desaparición forzada.]

"Advertimos que con este nombramiento posiblemente se pueden incrementar y repetir crímenes de Estado contra la población; así mismo llamamos la atención de que las soluciones del gobierno proyectadas solo en vías militares incrementarán afectaciones contra la población civil," manifestó el comunicado de Nodo Antioquia.

### **Preocupaciones sobre recientes nombramiento y ascensos de militares**

A pesar de las serias denuncias que han realizado organizaciones defensoras de derechos humanos y víctimas, algunos militares de altos rangos, con investigaciones por ejecuciones extrajudiciales, han sido reconocidos recientemente a través de nombramientos y ascensos, hecho que provoca dudas sobre el compromiso del Estado con las víctimas y las garantías de no repetición.[(Le puede interesar:][[Víctimas rechazan candidatura de alto mando militar vinculado con ejecuciones extrajudiciales)]](https://archivo.contagioradio.com/victimas-rechazan-candidatura-de-alto-mando-militar-vinculado-en-ejecuciones-extrajudiciales/)

"El mensaje que está dando el gobierno es que probablemente en el Bajo Cauca y en otras regiones del país, con las nuevas cúpulas del país, podría verse nuevamente casos de falsos positivos," afirmó Zapata.

<iframe id="audio_31374745" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31374745_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
