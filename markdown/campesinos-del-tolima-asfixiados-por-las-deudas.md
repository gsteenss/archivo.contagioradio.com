Title: Campesinos del Tolima "asfixiados" por las deudas
Date: 2020-02-14 13:33
Author: ambiente y sociedad
Category: Comunidad, Nacional
Tags: bancos, campesinos, muertes
Slug: campesinos-del-tolima-asfixiados-por-las-deudas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-12-at-9.22.02-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: FelipeGiraldo*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2354748654626890%2F&amp;width=500&amp;show_text=false&amp;height=281&amp;appId" width="500" height="281" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Embargos, remates, abandonos, suicidios y enfermedades son las problemáticas que afrontan los campesinos y campesinas que solicitaron prestamos a bancos y otras entidades para desarrollar sus cultivos, y que debido condiciones climatológicas han perdido sus productos y su vez el medio para responder con las exigencias de sus prestamistas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 13 de febrero se presentaron en la Procuraduría General de la Nación 10 líderes campesinos en representación de cerca de 4.000 familias afectadas por estos "prestamos impagables", la mayoría de los departamentos de Tolima y Caldas. (Le puede interesar: <https://archivo.contagioradio.com/de-ruana-y-sombrero-campesinos-apagaron-incendio-en-paramo-de-sumapaz/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La deuda en todo el país según los campesinos puede oscilar en **un billón de pesos, teniendo en cuenta que según ellos y ellas gran parte de esta corresponde a los intereses** que les piden los bancos, teniendo en cuenta que el promedio de los prestamos solicitados oscilan entre los seis y quince millones de pesos máximo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo es difícil establecer la cifra exacta, debido a que muchos han acudido a prestamistas informales como son los gotagota o pagadiario. Los que se pueden registran corresponde en su mayoría al Banco Agrario de Colombia, y en su segundo lugar a entidades como Davivienda y Bancolombia que cuentas con planes de préstamo fácil para agricultores.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un banco indolente ante la realidad de lo campesinos

<!-- /wp:heading -->

<!-- wp:paragraph -->

El acoso por parte de los cobradores de los bancos parece ser el pan de cada día de quienes tienen la honrosa tarea de producir el 60 por ciento de los alimentos en Colombia. (Le puede interesa: <https://archivo.contagioradio.com/bosque-de-palma-de-cera-simbolo-nacional-nuevamente-en-riesgo/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Absalón Arias, lider campesino de Fresno, Tolima, e integrante de la veeduría ciudadana, respaldó esta afirmación, *"de 6:00 am a 10:00pm , lunes o domingos, nos llaman cobradores, nos tratan como delincuentes, cuando siempre hemos sido buena paga para los bancos"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó, *"a los bancos no les interesamos nosotros, les interesa es que paguemos, ignorando los factores naturales que puedan intervenir"*; según el campesino el valor del préstamo no es consecuente con los intereses de los bancos, *"en uno de los casos la deuda terminó ascendiendo a 70 millones de pesos, cuando el préstamo original no superaba los 20 millones".*

<!-- /wp:paragraph -->

<!-- wp:image {"id":24111,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Campesinos-cultivando-1024x576.jpg){.wp-image-24111}  

<figcaption>
Foto: Archivo de Contagio Radio

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:quote -->

> *"Hace algunos años el conflicto fue quien nos desplazó, despojó y amedrento, hoy son los bancos"*
>
> <cite>Absalón Arias</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Asimismo agregó que los prestamistas no son consientes de factores naturales, *"para un sembrado de aguacate el cual tarda casi 2 años en cosechar y dar ganancia, los bancos empiezan a cobrar desde el mes seis, siendo consientes que aún no hay aguacates y que muchas veces los cultivos se dañan no por descuido nuestros, sino por los climas extremos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último agregó, que cuando solicitan el prestamos muchos campesinos no tiene la finca a su nombre, aún así el banco acepta un documento llamado **"sana procesión"** que los acredita como poseedores, *"cuando no se puede pagar entonces viene la Fiscalía ha amenazarnos con que nos van a meter a la cárcel, porque ese documento no es legal. Entonces, si no lo es ¿por qué el banco lo acepta?"*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Causas de la insuficiencia económica campesina

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el Departamento Administrativo Nacional de Estadística (Dane) *"el 22.5 de los colombianos son pequeños agricultores y campesinos, afirmando que la brecha entre la población rural y urbana persiste y se agrava con la crisis del campesinado"*. (Le puede interesar: <https://www.justiciaypazcolombia.com/militares-ingresan-a-la-zona-humanitaria-civipaz/> )

<!-- /wp:paragraph -->

<!-- wp:image {"id":18806,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/campesinos.jpg){.wp-image-18806}  

<figcaption>
Foto: Archivo de Contagio Radio

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Según Nelly Cuadros, campesina de Chaparral, Tolima e integrante de la Asociación de Trabajadores Agropecuarios son tres obstáculos los que influyen en este proceso, el primero tiene que ver con las políticas de crédito, las cuales no son acordes a la realidad de los ciclos de cosecha.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El segundo es que no existe control por parte del Gobierno ante el constante aumento de los precios de insumos, equipos y maquinarias para la agricultura campesina, así como la cantidad de frutas y verduras de importación, que se venden mucho más económicos que los de los campesinos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y el tercero es el desconocimiento de la cultura campesina, *"nosotros tenemos palabra y si no pagamos es porqué en verdad no podemos, pero el Gobierno cree que nos enriquecemos, cuando lo que hacemos es sobrevivir con lo que podemos"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Un campesino sin tierra es un ave sin alas, un pez sin agua, si no las quitan básicamente nos matan en vida"*
>
> <cite>Absalón Arías</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### La deuda ambiental con los campesinos y campesinas

<!-- /wp:heading -->

<!-- wp:paragraph -->

La variabilidad climática de los últimos meses, la fuertes heladas, incendios y sequías, han afectado a un gran porcentaje de los agricultores colombianos dejándolos con perdidas innumerables, y si sustento para responder ante las bancos, y en muchos casos sin garantías para llevar un plato de comida a su familia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo señalaron que el uso de químicos y semillas modificadas por exigencia del Estado, han deteriorado la durabilidad de los cultivos, "*antes un cultivo de café duraba mucho más, la planta era resistente y entregaba fruto durante varios años, ahora es poco o nada lo que se puede recoger antes de que muera"*, señaló Nelly Cuadros .

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" Invito a las clases medias y bajas para que se pongan la mano por lo campesino de Colombia, que valoren nuestros esfuerzos y nos acompañen en nuestras luchas, seamos conscientes que hace algunos años productos como el café hicieron crecer la economía, hoy cuando hay crisis nadie nos escucha y solo recibimos olvido.*
>
> <cite> Absalón Arias, lider campesino de Fresno, Tolima </cite>

<!-- /wp:quote -->

<!-- wp:image {"id":37271,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Enfoque de Género](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/campesinas-1024x683.jpg){.wp-image-37271}  

<figcaption>
Enfoque de Género - El Semanario

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Ante la cantidad de frutas y verduras que exporta este Gobierno de otros países, y el olvido de la importancia de lo rural por parte de las zonas urbanas del país, la comisión de campesinos del Tolima y Antioquia que vinieron a Bogotá, exigieron:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Primero, **ratificación** por parte del Congreso y del Presidente de los **Derechos del Campesinado**, aprobados por las Naciones Unidas mediante declaración del 30 de octubre de 2018, y que hasta ahora no han tenido mayor aplicación en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Segundo, el diseño de una política agraria, que no solo beneficie a los grandes terratenientes, sino también a la pequeña economía campesina.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tercero, **seguros coherentes con la realidad**, según Absalón Arías, las pólizas que adquieren con sus prestamos, solo funcionan cuando los grandes productores tienen problemas en sus cosechas, *"nuestro seguro solo aplica, cuando hay fallecimiento o incapacidad extrema del deudor, de ahí en adelante no hay nada"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y por último señalaron que si no se registra algún tipo de acción se organizarían para una movilización ciudadana, *"los campesinas y campesinas nos organizaremos desde nuestras fincas hacia las ciudades para reclamar nuestros derechos y la garantía de nuestro trabajo".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
