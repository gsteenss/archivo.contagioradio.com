Title: Rodrigo Salazar, reconocido líder del Pueblo Awá fue asesinado en Llorente
Date: 2020-07-10 20:40
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinato de indígenas en Colombia, nariño
Slug: rodrigo-salazar-reconocido-lider-del-pueblo-awa-fue-asesinado-en-llorente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ech4LmoXoAEz2Xd.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Rodrigo Salazar, líder Awá / Comunicaciones Unipa

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En cercanías al corregimiento de Llorente cerca a Tumaco, Nariño, en horas de la mañana del pasado 9 de julio fue asesinado **Rodrigo Salazar, de 44 años quien se desempeñaba como gobernador suplente del Resguardo Indígena Awá de Piguambi Palangala** y quien era reconocido como líder de la Guardia en la región. Hasta el 9 de junio según cifras oficiales habían sido asesinados al menos 47 integrantes de los pueblos ancestrales en lo corrido del 2020, 14 de ellos en medio del aislamiento preventivo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque no existen muchos detalles con respecto a cómo fue asesinado Rodrigo, señalan que el ataque se dio mientras salía de su casa hacia una reunión cuando hombres armados lo abordaron y lo asesinaron. Su destino, era una audiencia virtual convocada por la Procuraduría Delegada para Asuntos Étnicos para hacer seguimiento al Capítulo Étnico acordado en el Acuerdo de Paz

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Rodrigo Salazar, quien además era asociado a la [UNIPA](https://twitter.com/UNIPAcomunica) **se desempeñó como consejero zonal de la Guardia Indígena entre 2012 y 2016 para el municipio de Tumaco**. Fue un líder que desde hace años había sido objeto de amenazas en defensa del territorio, dicho liderazgo fue una de las razones por la que también fue elegido este año como gobernador suplente del resguardo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Pueblo Awá vive en medio de la guerra

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, La **Unidad Indígena del Pueblo Awá (Unipa)** denuncia que en el corregimiento de Llorente se vienen presentando diversos hechos victimizantes entre ellos, cuatro asesinatos incluidos el de Rodrigo durante la pandemia. [(Le recomendamos leer: Asesinatos contra los pueblos indígenas son selectivos: Aida Quilcué)](https://archivo.contagioradio.com/asesinatos-contra-los-pueblos-indigenas-son-selectivos-aida-quilcue/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ocasiones anteriores, la Organización Nacional Indígena de Colombia, (ONIC) ha alertado que el pueblo Awá habita en medio de un conflicto en el cual participan “quince grupos al margen de la ley”, incluidas las disidencias de las FARC, bandas delincuenciales y el ELN, grupos que se disputan el territorio y el control sobre los cultivos de uso ilícito en Nariño. [(Lea también: Tras asesinato de indígena, Pueblo Awá exige garantías en medio de las balas y el Covid-19)](https://archivo.contagioradio.com/tras-asesinato-de-indigena-pueblo-awa-exige-garantias-en-medio-de-las-balas-y-el-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El corregimiento de Llorente en particular resulta de valor para los grupos armados ilegales que tienen a su disposición corredor estratégico para la producción y exportación de coca que es conducida **no solo a puertos naturales, sino a la frontera con Ecuador.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio del confinamiento no es la primera vez que la comunidad Awá es blanco de ataques, el pasado 26 de marzo, en el municipio de Barbacoas, fue asesinado **Wilder García, indígena Awá perteneciente al Resguardo de Tortugaña Telembí.** [(Lea también: Lilia García, secretaria del Cabildo Awá fue asesinada en Nariño)](https://archivo.contagioradio.com/lilia-garcia-secretaria-del-cabildo-awa-fue-asesinada-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
