Title: Con la Ciénaga inicia el 4to Festival de cine por los DDHH
Date: 2017-08-11 15:52
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, colombia, Festival de cine ddhh
Slug: festival-cine-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/la-cienaga-pelicula-colombiana-gana-premio-en-nueva-york-496410.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mago Films 

###### 11 Ago 2017 

Con la proyección de la película "Ciénaga, entre el mar y la tierra" del director colombiano Manolo Cruz, inicia la **4ta versión del Festival Internacional de Cine por los Derechos humanos de Bogotá**. Una ventana a las producciones argumentales y documentales de Colombia y el mundo que abordan la temática desde diferentes construcciones de realidad.

Entre el **11 y el 18 de agosto** el Festival presentará en su selección oficial un total de **70 producciones escogidas entre 738 enviadas desde 48 países del mundo**, en escenarios de Bogotá, Medellín, Cartagena y Barranquilla y los municipios de Soacha y Puerto Colombia. Le puede interesar: [Nuevos realizadores a relatar los derechos de los migrantes](https://archivo.contagioradio.com/nuevos-realizadores-ddhh-cine/).

Las películas seleccionadas por el **equipo curador del evento**, destacaron por su calidad técnica, estilo narrativo y aporte a la promoción y defensa de los derechos humanos desde el audiovisual. Como viene siendo costumbre, la presente edición ofrecerá una **agenda académica y cultural de acceso gratuito**.

Conversatorios sobre el **papel del cine en la memoria**, la construcción de paz, defensa de la vida de los líderes sociales y un homenaje al periodista Jaime Garzón, son algunos de los eventos especiales que se presentaran durante el Festival. Le puede interesar: [Una exposición busca dignificar la memoria de los líderes de la UP](https://archivo.contagioradio.com/memoria-up-exposicion/)

<iframe src="https://www.youtube.com/embed/nOYcX4uCD-I" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Adicionalmente a la presentación de la película inaugural, se realizará un c**onversatorio con Manolo Cruz, Vicky Hernández y Jorge Cao**, este viernes 11 de agosto en el Teatro México de la Universidad Central, Cl. 22 n.º 5-85, a partir de las **6:00 p.m**.

[Programación\_ IV Festival Internacional de cine por los Derechos Humanos de Bogotá](https://www.scribd.com/document/356095955/Programacio-n-IV-Festival-Internacional-de-cine-por-los-Derechos-Humanos-de-Bogota#from_embed "View Programación_ IV Festival Internacional de cine por los Derechos Humanos de Bogotá on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_73237" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/356095955/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-rKI8LO957ZXBDbiGDMcU&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.760539629005059"></iframe>

El Festival Internacional de Cine por los Derechos Humanos Bogotá es organizado por Impulsos Films en alianza con la Unidad para la Atención y Reparación Integral a las Víctimas y el Programa Estado de Derecho para Latinoamérica de la Fundación Konrad Adenauer y con la participación de la corte Interamericana de Derechos Humanos.
