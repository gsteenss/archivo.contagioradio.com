Title: Tras atentado fallece Miguel Ángel Alpala, líder indígena nariñense
Date: 2019-05-02 16:51
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Asesinato de indígenas, Chocó, nariño
Slug: tras-atentado-fallece-miguel-angel-alpala-lider-indigena-narinense
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Miguel-Ángel-Alpala.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @GustavoBolívar] 

En horas de la madrugada del pasado miércoles falleció el **líder indígena del Resguardo de Gran Cumbal del pueblo de Los Pastos, Miguel Ángel Alpala** en el Hospital Civil de Ipiales, desde el 16 de abril cuando ingresó en grave estado tras recibir cinco impactos de bala en inmediaciones de su hogar en la vereda Cuaical, municipio de Cumbal.

Miguel Ángel Alpala fue un activo integrante de **Autoridades Indígenas de Colombia (AICO)**, de la Comisión de Justicia de los pueblos Pastos y de la Escuela de Derecho Propio, fue un destacado defensor de derechos humanos y además participó en la Sexta Minga Organizacional y de Integración en el Resguardo Indígena de Colimba.

### **Asesinan a mujer indígena en Chocó** 

El pasado 1 de mayo también fue asesinada **Remedia Aizama Aizama, joven Embera- Cham**i, encargada de los alimentos de la comunidad de Campo Alegre, en San José del Palmar, Chocó, durante los hechos también resultó herida una menor de edad.

[**Deiner Palacios, personero de San José del Palma**r, señala que lo sucedido es materia de investigación pues lo que manifiesta la comunidad es que fue alguien encapuchado quien después de disparar salió del resguardo sin ser identificado.][(Le puede interesar: CRIC no permitirá que asesinato del comunero Deiner Yunda quede en la impunidad)](https://archivo.contagioradio.com/cric-exige-esclarecimiento-del-asesinato-del-comunero-deiner-yuda/)

[**"Podemos dar un parte de tranquilidad, las comunidades están un poco más receptivas y manifestaron que regresarán a su resguardo una vez entierren a la señora Remedia y resuelvan el tema de atención médica de la menor edad"** indicó Palacios refiriéndose al desplazamiento de cerca de 40 personas entre adultos y niños hacia la cabecera municipal que se dio durante el 1 de mayo. ]

<iframe id="audio_35294611" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35294611_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
