Title: Desde el Concejo rechazan posible incremento de tarifa del servicio de aseo
Date: 2017-08-18 16:31
Category: Economía, Nacional
Tags: Bogotá, recicladores, recolección de basuras, relleno doña juana
Slug: recoleccion-de-basura-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/basura-bogotá-e1503091849415.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Nacional de Colombia] 

###### [18 Ago 2017] 

Varios sectores políticos y sociales de la capital colombiana rechazaron la medida del Distrito de **aumentar la tarifa de aseo para los ciudadanos asociada a la recolección de las basuras**. Indicaron que es necesario una transformación inmediata del sistema que tiene a todos los capitalinos, y en especial a más de 137 barrios en el sur de la ciudad, inmersos en una crisis socio-ambiental.

Para el concejal por Bogotá, Manuel Sarmiento, el aumento en las tarifas no solo afectaría el bolsillo de los capitalinos, sino que **no soluciona el problema de recolección de desechos que tiene Bogotá desde hace tiempo**. Aseguró también que “la medida además de ser absurda y de carácter retardatario, beneficia a los negociantes y perpetúa un modelo de recolección obsoleto”. (Le puede interesar:["Peñalosa no da la cara": Comunidades en el sur de Bogotá"](https://archivo.contagioradio.com/penalosa-no-da-la-cara-comunidades-en-el-sur-de-bogota/))

Sarmiento recordó que el operador Centro de Gerenciamiento de Residuos (CGR), que realiza el trabajo de recolección de basuras y disposición en el relleno sanitario de Doña Juana, **“no ha cumplido con sus obligaciones y Peñalosa en vez de ponerlo en cintura, le aumenta la tarifa”.**

**¿Cómo solucionar el problema de la recolección de basuras?**

El concejal indicó que al problema de basuras en Bogotá **“siempre se le ha puesto pañitos de agua tibia”**  y asegura que es necesario que el Estado incentive el reciclaje como una política para que se beneficie también la población de recicladores, quienes quedarían excluidos de las políticas que comprenden la licitación de operadores que está en curso.

Adicionalmente, plantea que “**deben existir plantas de tratamiento que sustituyan el relleno sanitario Doña Juana** para producir energía, fertilizantes y abonos”. Sin embargo, pone de manifiesto que “Peñalosa está empeñado en mantener un modelo que favorezca a los negociantes y no a la ciudadanía”. (Le puede interesar: ["Prefiero mi basura en el patio de otro"](https://archivo.contagioradio.com/prefiero-mi-basura-en-el-patio-del-otro/))

**En el concejo de Bogotá realizarán control político**

El concejal fue enfático en que en el concejo de Bogotá realizarán un debate de control político **“para demostrar que se quiere favorecer a un operador que no ha cumplido con sus deberes”**. Allí, también discutirán la necesidad de que exista una política donde el Estado garantice la implementación de un nuevo modelo de recolección de basuras “donde se imponga una separación de basuras desde la fuente y se invierta en plantas de tratamiento de basuras”.

<iframe id="audio_20407925" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20407925_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
