Title: "Montoya diga la verdad" exigen las víctimas
Date: 2018-09-12 17:20
Author: AdminContagio
Category: DDHH, Movilización
Tags: Ejecuciones Extrajudiciales, General Montoya, JEP
Slug: montoya-diga-la-verdad-exigen-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/general_mario_montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [12 Sept 2018] 

Este 13 de septiembre se realizará la primera audiencia contra el General Mario Montoya en la Jurisdicción Especial para la Paz (JEP), investigado por su presunta responsabilidad en casos de ejecuciones extrajudiciales y la Operación Orión. Para las víctimas este espacio es de vital importancia en camino de esclarecer **lo qué realmente ocurrió durante esos hechos.**

En recientes declaraciones Andrés Garzón, abogado del General Montoya, aseguró que su cliente no se declarará culpable por los crímenes de los que es acusado ante la JEP y afirmó que ese mecanismo será utilizado para ratificar su inocencia.

Frente a esta posición las víctimas manifestaron que esperan que el tribunal preste atención frente a los hechos ocurridos en **la Operación Orión y todas las operaciones militares que se desarrollaron en el 2002**, cuando se inauguraba la política de la Seguridad Democrática. Según el abogado Luís Castillo, de la Corporación Jurídica Libertad, se dio la participación de Montoya y en el caso de la Comuna 13 tuvo el control total de estructuras paramilitares.

En ese sentido, las víctimas expresaron la necesidad de tener mayor participación en el proceso, porque en el Acuerdo de Paz se ratificó su centralidad; lo cual no se ve reflejado hasta ahora en las audiencias porque no se están re transmitiendo y otras víctimas en las regiones no están siendo convocadas a participar.

### **La audiencia de Montoya** 

Debido a las modificaciones que se le han hecho a la JEP, la audiencia de mañana tendrá como objeto verificar el sometimiento del General Montoya a la justicia transicional, pedirle que haga propuestas sobre cómo realizará su reparación y darle la palabra a las víctimas frente a qué opinan de esas formas de reparación que proponga el General.

Actualmente, Montoya solo ha manifestado que se someterá por el proceso de ejecuciones extrajudiciales a la JEP, sin embargo, **en ese Tribunal hay otros dos procesos en contra suya, hecho que también ha generado incertidumbre entre las víctimas** al no saber que pasará con esos casos. (Le puede interesar:["Los casos por los que deberá responder el General Montoya"](https://archivo.contagioradio.com/casos-responder-montoya-ante-jep/))

### **\#MontoyaDigaLaVerdad** 

Mañana, en Medellín se realizará una rueda de prensa en dónde habrá un intercambio de las opiniones de las víctimas y abogados con respecto a la audiencia. En Bogotá se está convocando a un plantón a las afueras de las instalaciones de la Jurisdicción Especial para la Paz, ubicada en la carrera 7 \#63-44.

<iframe id="audio_28549417" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28549417_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
