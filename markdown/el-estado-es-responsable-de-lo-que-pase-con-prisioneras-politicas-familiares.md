Title: El Estado es responsable de lo que pase con prisioneras políticas: Familiares
Date: 2020-04-01 22:28
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: #MovimientoCarcelario #Covid-19 #, carceles de colombia, Carceles de mujeres, Covid-19, Expreso Libertad, exprisioneras politicas, montaje judicial, Prisioneras políticas, situacion en las carceles de colombia
Slug: el-estado-es-responsable-de-lo-que-pase-con-prisioneras-politicas-familiares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/reclusas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Las tres prisioneras políticas, víctimas del montaje judicial conocido como caso Andino, fueron trasladadas de forma arbitraria a la cárcel de Coiba en Ibagué. Sus familiares aseguran que el Estado es responsable de los que pase con ellas en medio de la crisis del covid-19 y la falta de garantías a derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad**, Néstor Méndez, padre de Alejandra, una de las prisioneras políticas, manifiesta que hasta el momento no han logrado comunicarse directamente con ellas. Sin embargo han recibido información de que las mujeres estarían en calabozos de aislamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En vista de estos hechos y de la crisis carcelaria en torno a la pandemia del Covid-19, la Comisión de Justicia y Paz interpuso una serie de tutelas, que de acuerdo con la abogada Isabel Velasquez, permitan la atención urgente a esta situación y un conjunto de acciones que protejan la vida de la población privada de la libertad.

<!-- /wp:paragraph -->

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F818749251965187%2F&amp;width=600&amp;show_text=false&amp;height=338&amp;appId" width="600" height="338" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>

<!-- wp:paragraph -->

[Mas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
