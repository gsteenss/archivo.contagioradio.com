Title: Denuncian a Carlos Holmes Trujillo por crímenes de la Policía durante la noche del 9S
Date: 2020-12-15 17:34
Author: PracticasCR
Category: Actualidad, Nacional
Tags: 9S, Carlos Holmes Trujillo, denuncia, dhColombia
Slug: denuncian-a-carlos-holmes-trujillo-por-crimenes-de-la-policia-durante-la-noche-del-9s
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Carlos-Holmes-Trujillo-Policia.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Carlos Holmes Trujillo & Gral. Oscar Atehortua

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La **Asociación Red de Defensores y Defensoras de Derechos Humanos ([dhColombia](https://twitter.com/dhColombia?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor)) radicó una denuncia en contra del ministro de Defensa, Carlos Holmes Trujillo** y algunos altos mandos policiales solicitando judicializar a estos funcionarios por su presunta responsabilidad, por línea de mando, en los abusos y hechos delictivos en los que incurrió la Policía en el marco de las protestas del 9 y 10 de septiembre de este año.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la inacción de las autoridades para investigar los hechos acaecidos hace más de tres meses y la posibilidad de que estos queden en la impunidad, la organización **dhColombia** radicó una denuncia ante la Fiscalía General de la Nación en contra del ministro de Defensa, **Carlos Holmes Trujillo y varios mandos policiales como el director de la Policía Nacional, Oscar Atehortúa; el comandante encargado de la Policía Metropolitana de Bogotá, Luis Alfredo Sarmiento Tarazana;**  el comandante de la Policía de Cundinamarca, Necton Lincon Borja Miranda; el comandante de la Estación de Usaquén, Roberto Carlos Sánchez; y los comandantes de los CAI´s de Verbenal, Suba y Kennedy. (Le puede interesar: [Otra Mirada: ¿Dónde queda la responsabilidad policial?](https://archivo.contagioradio.com/otra-mirada-donde-queda-la-responsabilidad-policial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Según los denunciantes estos funcionarios serían responsables por las ejecuciones extrajudiciales colectivas (masacres), tortura, privación ilegal de la libertad y lesiones personales que se ejecutaron la noche del 9 de septiembre,** por ello las pretensiones principales de dhColombia es que se investiguen los hechos  y se sancione a los responsables.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El texto de la denuncia también da cuenta de la sistematicidad con la que opera la Fuerza Pública en este tipo de abusos y conductas delictivas, en relación con esto, reseñó el actuar recurrentemente violento con el que han operado sus agentes, especialmente en el marco de las movilizaciones sociales que han aumentado en afluencia y periodicidad desde noviembre del año pasado con las jornadas del Paro Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La organización trajo a colación el asesinato de Dilan Cruz a manos de un miembro del ESMAD, hecho que aún permanece en la impunidad pese a que el responsable fue individualizado. (Lea también: [Agresiones de la fuerza pública contra la protesta social han sido sistemáticas: Corte Suprema](https://archivo.contagioradio.com/corte-suprema-ordena-a-min-defensa-ofrecer-disculpas-por-exceso-en-el-paro-nacional/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, la denuncia señala que “*durante y después de las protestas \[del 9S\], medios de comunicación y organizaciones de defensa de derechos humanos reportaron violaciones graves, transgresiones flagrantes de los protocolos de acción en la protesta, agresiones contra periodistas y **una actitud general de uso excesivo de la fuerza por parte de la Policía Nacional en contra de quienes ejercían su derecho fundamental a protestar: personas asesinadas y heridas por arma de fuego, mala utilización de la figura de Traslado por Protección, lesiones personales, obstrucción al ejercicio periodístico, entre otras*****”.** (Lea también: [\[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/))

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/dhColombia/status/1336727427406569474","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/dhColombia/status/1336727427406569474

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

Como consecuencia de estas conductas, indica la denuncia, entre el 9 y 10 de septiembre **al menos 14 personas fueron asesinadas.** Adicionalmente, el 9 de septiembre se identificaron 248 civiles heridos en Bogotá, de quienes resaltan 68 presentaban lesiones de arma de fuego. Mientras que, para el 10 de septiembre, la Campaña Defender la Libertad, registró al menos 11 heridos y la Alcaldía de Bogotá documentó que al menos 8 lo habían sido con arma de fuego.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/DefenderLiberta/status/1336779241711669260","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DefenderLiberta/status/1336779241711669260

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph {"align":"justify"} -->

Por otra parte, la denuncia recopila testimonios de personas que resultaron gravemente heridas por cuenta del actuar desproporcionado de la Policía, entre ellos, los de **Manuel Antonio Fernández Acevedo** **quien perdió la movilidad en sus dos piernas por culpa de un disparo que recibió presuntamente por parte de un agente del CAI de Verbenal;** así como los de los hermanos Robert y Henry Valencia quienes fueron gravemente heridos estando en su propia casa; y el de Luisa Fernanda Tirado, una menor de edad que también resultó herida y quien ni siquiera se encontraba participando de las protestas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con base en todo lo recogido en la denuncia la organización dhColombia considera que **es “*altamente probable*” que el ministro de Defensa, el director de la Policía y los altos mandos de la institución referidos, “*resulten responsables de ejecuciones extrajudiciales colectivas, desapariciones forzadas, tortura, violencia sexual y lesiones personales agravadas,*** *por ser hechos cometidos por agentes del Estado contra ciudadanos en razón a sus opiniones, posiciones políticas o profesión y, en algunos casos, contra menores de edad o personas en incapacidad para defenderse*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**“*Este establecimiento de responsabilidad resulta toda vez que las personas referidas se constituyen como los sujetos activos que tienen la calidad y posición de garantes por ser la línea de mando de los agentes estatales que consumaron las conductas delictuales;*** *asimismo, se encontraban en la obligación constitucional y legal de evitar la producción de los resultados antijurídicos surgidos a partir de las conductas delictuales de los agentes que actuaban bajo su mando”* apunta la denuncia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Los hechos que aquí se ponen en su conocimiento, no hubieran ocurrido, si quienes, teniendo la posición de garantes, no hubieran incurrido en la ceguera intencional, deliberada y proterva, que permitió la repetición de crímenes que lesionan la conciencia ética de la humanidad”.
>
> <cite>Organización dhColombia</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
