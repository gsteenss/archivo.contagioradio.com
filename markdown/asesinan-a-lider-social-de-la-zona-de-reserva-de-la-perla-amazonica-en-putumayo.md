Title: Asesinan a líder social de la zona de reserva campesina de la Perla Amazónica, en Putumayo
Date: 2018-10-10 12:10
Author: AdminContagio
Category: DDHH, Nacional
Tags: Lider social, Zona de reserva Campesina del Putumayo
Slug: asesinan-a-lider-social-de-la-zona-de-reserva-de-la-perla-amazonica-en-putumayo
Status: published

###### [Foto: Contagio Radio] 

###### [10 Oct 2018] 

La Asociación de Desarrollo Integral Sostenible de La Perla Amazónica, **ADISPA, denunció el asesinato de Otto Valenzuela**, líder de esta organización. Los hechos se presentaron el pasado 8 de octubre, cuando integrantes de esa comunidad encontraron el cuerpo de Valenzuela con un impacto de bala en la vereda Frontera.

Yaneth Silva, integrante de ADISPA, señaló que durante la mañana, el líder social estuvo trabajando en la **Minga Comunitaria y posteriormente se retiró a su vivienda**. Allí, los vecinos de ese predio manifestaron que escucharon un disparo, pero no alertaron a las autoridades debido a que no imaginaron que se tratara de un acto de violencia.

De igual forma, Silva  expresó que cuando se le informó a la Policía de los hechos, le manifestaron que no podían acercarse al lugar porque estaba muy retirado, y le hicieron recomendaciones **"de ser muy cuidadosos con el tema de recoger cascos de balas"**, como material de investigación, delegando a los habitantes tareas que le competen tanto la Policía como a la Fiscalía.

Desde hace más de 3 meses, **Valenzuela había estado participando de las asambleas de ADISPA** y se desempeñaba como fiscal de la Junta de Acción comunal de la vereda Frontera, asimismo contribuyó en impulsar los planes de sustitución de cultivos de uso ilícito en el territorio. (Le puede interesar:["COCCAM denuncia masacre de líder social y su familia, en Bolívar Cauca"](https://archivo.contagioradio.com/coccam-denuncia-masacre-de-lider-social-y-su-familia-en-bolivar-cauca/))

Silva denunció que este hecho ha generado temor en las comunidades y desánimo en los demás líderes sociales que temen por sus vidas y agregó que este líder social no había sido amenazado, hecho por el cual se desconoce el motivo para llevar a cabo el asesinato.

<iframe id="audio_29252215" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29252215_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
