Title: Internos de la Tramacúa vuelven a huelga por sus derechos
Date: 2018-07-12 13:05
Category: DDHH, Nacional
Tags: Equipo Jurídico Pueblos, Huelga de hambre, La Tramacua, Palogordo
Slug: internos-tramacua-vuelven-a-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-12-a-las-12.41.03-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @equipojuridico] 

###### [12 Jul 2018] 

Internos de la cárcel de alta y mediana seguridad de Valledupar, conocida como al Tramacúa, iniciaron una nueva huelga de hambre, motivada por el incumplimiento de las peticiones hechas a las directivas de la institución penitenciaria tendientes a garantizar sus Derechos.

Según la información suministrada por el Equipo Jurídico Pueblos, los internos tomaron la decisión de iniciar una huelga, amparada en el art.56 de la constitución y en la sentencia T-2525 de 2014, **"por las violaciones a los Derechos Humanos, tratos crueles, degradantes e inhumanos"** a lo que están sometidos.

Los reclusos argumentan su protesta, iniciada el 10 de julio, por la **limitación en el acceso al agua por cortos periodos de tiempo durante el día, el hacinamiento, insalubridad** y falta de condiciones dignas de reclusión, demandas que han sido reiteradas en múltiples oportunidades. (Le puede interesar: ["Cerca de 1300 internos de cárcel la Tramacúa inician huelga de hambre"](https://archivo.contagioradio.com/internos-tramacua-huelga-de-hambre/))

### **En cárcel de Girón maltratan a familiares de reclusos** 

Así mismo, el equipo de defensores de Derechos Humanos, señaló en otro comunicado que en el centro penitenciario Palogordo de Girón, Santander, también se vulneran los derechos de los internos como de sus familiares, pues las condiciones para recibir visitas no son adecuadas.

En la denuncia se detalla que los visitantes de los reclusos deben someterse a tratos degradantes, particularment**e las mujeres, quienes deben soportar excesos en las requisas, "pues son tocadas en sus partes íntimas y obligadas a bajarse la ropa interior"**, así como restricciones en el ingreso de elementos sanitarios de cuidado íntimo.

Por estas razones, los internos de ambos centros de reclusión piden a las instituciones encargadas de velar por el cumplimiento de sus derechos, que se garantice el trato digno por parte de los funcionarios encargados de custodiar las cárceles del país, y que las organizaciones que defienden los Derechos Humanos los acompañen en su situación.

[Comunicado de Equipo Jurídico Pueblos sobre la Tramacúa](https://www.scribd.com/document/383735724/Comunicado-de-Equipo-Juridico-Pueblos-sobre-la-Tramacua#from_embed "View Comunicado de Equipo Jurídico Pueblos sobre la Tramacúa on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_8677" class="scribd_iframe_embed" title="Comunicado de Equipo Jurídico Pueblos sobre la Tramacúa" src="https://www.scribd.com/embeds/383735724/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Vm4nO3TLwmPuaSElgc2x&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.7018867924528303"></iframe>  
 

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
