Title: Palestina “ha escogido buscar justicia y no venganza”
Date: 2015-06-25 16:23
Category: DDHH, El mundo
Tags: Autoridad Palestina, Corte Penal Internacional, Fatou Bensouda, Iniciativa Nacional Palestina, Israel, Operación Margen Protector, Palestina, Riyad Maliki
Slug: palestina-ha-escogido-buscar-justicia-y-no-venganza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/palestina-CPI-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[asianews]

###### [25 Jun 2015] 

Este 25 de Junio, el ministro de **Relaciones Exteriores de la Autoridad Palestina, Riyad Maliki**, entregó documentación sobre crímenes de Israel al fiscal de la CPI, Fatou Bensouda. Según la agencia de noticias Ma’an. La documentación incluye crímenes cometidos desde Junio de 2014 hasta mayo de 2015.

En este margen de tiempo, que es el que Palestina puede demandar ante la **CPI,** está la ofensiva israelí en masa contra civiles palestinos que viven en el distrito de Hebrón en 2014, la **operación "Margen Protector"**, estadísticas sobre la invasión territorial a territorio palestino en los asentamientos, los prisioneros palestinos en las cárceles de Israel y las torturas a las que son sometidos.

"*El Estado de Palestina está encantado de cooperar con la Corte, incluido el hecho de proveer de información relevante*", aseguró Malki a la prensa y agregó que Palestina "ha escogido buscar justicia y no venganza" y que "va a cooperar" porque "ésta es el tipo de cooperación que buscamos, una cooperación positiva".

El secretario de la **Iniciativa Nacional Palestina**, Mustafa Barghouti, afirmó que el “objetivo es demostrar que los crímenes fueron cometidos y el de convencer al fiscal general para iniciar las investigaciones” y añade que están “*tratando de eliminar la inmunidad de Israel y sus líderes a medida que tratamos de alcanzar la justicia, proteger al pueblo palestino y asegurarnos que los criminales no eviten el castigo de la ley*”.

El informe entregado por Palestina se da a conocer solamente tres días después de que la Organización de las Naciones Unidas presentara un informe sobre los crímenes de guerra cometidos por Israel durante la operación "**Margen Protector**" que dejó más de 2200 palestinos muertos, entre ellos más de 1400 civiles y más de 500 niños.

**Palestina ingresó al grupo de estados miembros de la CPI el pasado mes de Abril** y desde allí se trazó el objetivo de impedir la impunidad en crímenes cometidos por Israel. Sin embargo, ese país ha rechazado responder a las preguntas formuladas por la fiscalía de la CPI por considerar ilegítima la demanda ya que, según autoridades de Israel, Palestina no es un Estado.
