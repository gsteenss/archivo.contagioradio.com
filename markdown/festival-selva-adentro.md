Title: Selva Adentro, una apuesta por el teatro para construir paz
Date: 2018-06-21 15:33
Category: Cultura
Tags: Chocó, Festival, teatro
Slug: festival-selva-adentro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/22338860_688910017986455_1387814283669079985_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Festival 

###### 21 jun 2018 

Después de su primera edición realizada entre el 1ro y el 8 de octubre de 2017, "Selva Adentro", primer Festival de artes escénicas realizado en el municipio de Carmen del Darien, vereda Brisas Caracolí, puntualmente en el ETCR Silver Vidal Mora, se prepara la segunda edición del evento.

Camilo Durango, uno de sus gestores, asegura que la idea inicial del evento fue recuperar algo de la biodiversidad perdida en la zona y ver como través del Festival podía entablarse un diálogo entre los excombatientes de las FARC y la gente de las comunidades, académicos y 14 agrupaciones teatrales profesionales que en sus obras abordaban de una manera u otra el conflicto armado colombiano.

"Lo que nosotros buscábamos era generar unos espacios donde se hicieran unos encuentros y hubieran unos reconocimientos, porque creemos que una de las grandes crisis  sociales que ha dejado la guerra es los procesos de inviduación, unas subjetividades muy marcadas en la competencia, en el aislamiento y en el construir unos juicios sobre el otro, sobre aquel que no se conoce" explica Durango.

Utilizar para tales propósitos las artes escénicas, se justifica en la necesidad de abrir espacios para recuperar la memoria "para pensarnos nuevas rutas en el fortalecimiento de los procesos democráticos e institucionales donde la palabra y el cuerpo aportara en esos procesos de escucha", potencialidades propias del teatro, la danza y otras manifestaciones artísticas.

<iframe src="https://www.youtube.com/embed/RGdR7Ld2bmU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Escuelas de Arte y Paz- Festival Selva Adentro 

Además del Festival de teatro, en la primera edición se hicieron unas escuelas de arte y paz "nos pensamos con la misma comunidad del ETCR y de otras cercanas, cuales eran los proyectos productivos que nos podíamos pensar para la región para hacer un fortalecimiento y como ese teatro podía convertirse en un espacio para la exploración escénica pero también para generar espacios de encuentro" llegando a construir de manera mancomunada un escenario ideal para el diálogo artístico y democrático.

Para la segunda edición, los organizadores esperan que el Festival logre congregar las expresiones artísticas surgidas desde las comunidades "queremos que sea un espacio más de teatro comunitario de los grupos de la región, también de los grupos que han surgido en los diferentes espacios territoriales, con invitación a otros grupos profesionales de teatro" buscan de alguna forma entregar el Festival a las comunidades para que sean ellos quienes en los próximos años lo sigan haciendo.

Aunque la gestión no es sencilla, teniendo en cuenta la poca financiación que se brinda para las expresiones artísticas y culturales en el país, Durango y el equipo del Festival busca que  los grupos de los muchachos que están por la región por las riveras del Atrato que tengan creaciones puedan llevarlas a este teatro, y se apropien del evento para así garantizar su permanencia en el tiempo. (Le puede interesar: ["Contra la corriente de aguas terrosas" el retrato de la esperanza en Colombia](https://archivo.contagioradio.com/contra-la-corriente-de-aguas-terrosas-la-historia-de-los-lideres-de-colombia/))

<iframe id="audio_26663534" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26663534_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
