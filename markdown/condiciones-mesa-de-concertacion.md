Title: Las tres condiciones de los estudiantes para la mesa de concertación con el Gobierno
Date: 2018-10-30 21:10
Author: AdminContagio
Category: Educación, Nacional
Tags: educacion, estudiantes, Mesa de negociación, Ministerio de Educación, Reunión
Slug: condiciones-mesa-de-concertacion
Status: published

###### [Foto: @Educacionbogota] 

###### [30 Oct 2018] 

Luego del primer encuentro entre estudiantes y Gobierno para discutir sobre la crisis en la educación pública, y el acuerdo que el presidente Duque firmó con rectores de algunas de las Instituciones de Educación Superior (IES) pública del país; el movimiento estudiantil afirmó que eran necesarias 3 condiciones para establecer una mesa de concertación que permitiera resolver la crisis del sector y levantar el paro nacional.

<iframe src="https://co.ivoox.com/es/player_ek_29752405_2_1.html?data=k56kl5eYdJahhpywj5WbaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncbTVz9nWw8zTb6iZpJiSpJjRqduZk6iYxsrQqcjVxdSYxsqPsMKftrOyp7iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Santiago Gómez, delegado de la UNEES que participó en la primera reunión con el viceministro de educación Luis Fernando Pérez, sostuvo que dicho encuentro fue negativo porque el Gobierno negó que hubiese una situación de crisis en la educación superior pública, pero luego, el mismo presidente hizo un acuerdo con los rectores de algunas IES públicas, en el que prometió más recursos para las instituciones; hecho que Gómez reconoció como un avance, pero calificó como una estrategia para dividir el movimiento estudiantil.

El delegado resaltó que el lunes se realizó la reunión del **Frente Amplio por la Educación**, del que participan la UNEES, ACRES, FENARES y ASPU, acordando la forma en que se deberían desarrollar las siguientes reuniones con representantes del Gobierno. Allí también se hizo evidente la disposición al diálogo con el Gobierno para resolver la crisis, pero la nueva reuniones tendría que realizarse en una mesa de concertación con capacidad decisoria.

En consecuencia, el integrante de la UNEES aseguró que la mesa de concertación debe tener condiciones de cumplimiento, por esto, un **garante internacional o nacional que pueda evaluar el desarrollo de los acuerdos sería necesario; otra condición sería la presencia de la ministra de educación, María Victoria Angulo; adicionalmente, profesores y estudiantes deberían tener voz y voto en el encuentro con el Gobierno**.

### **Paro Nacional de estudiantes sigue pese a ataques** 

<iframe src="https://co.ivoox.com/es/player_ek_29752460_2_1.html?data=k56kl5eYepGhhpywj5WaaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5ynca_dxNTZw9iPmMLhwt7ch5enb7biytvS1NjNqMLYjKjczsrLrdCfrsbm0dePqMafpNrbxs7Spc7V05KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Nicolás Tamayo, estudiante del Colegio Mayor de Cundinamarca, indicó que desde esa IES lamentaban que en el primer encuentro entre estudiantes y Gobierno no fueran convocados los docentes, y añadió que tampoco se vislumbra la intención de negociar por parte del Gobierno, porque mientras en su encuentro con los rectores se trató el tema de financiación, con los estudiantes se dialogó únicamente sobre levantar el paro.

Tamayo especificó que en el caso del Colegio Mayor, los ataques a la movilización estudiantil se han realizado desde la directiva de la Universidad y sus administrativos; pues mientras de forma pública se dice apoyar a los estudiantes, "en reuniones paralelas entre administrativos y directores han intentado torpedear el movimiento, y le han pedido a los representantes de facultades y programas, levantar el paro".

Pese a los intentos de sabotear las acciones del movimiento estudiantil, Tamayo dijo que continuarán buscando que se cumpla con el pliego local de exigencias, así como el de carácter nacional. Por su parte la UNEES sostuvo que el paro en todo el país seguirá, en tanto se logre llegar a acuerdos con el Gobierno en una mesa de concertación, **la cual sería inaugurada el próximo 1 de noviembre**.

> La ministra instala la mesa de negociación con el gobierno el jueves para tratar las exigencias conjuntas del frente amplio por la Educación Superior conformado por profesores y estudiantes, falta ver cómo va, pero por ahora es buena noticia, mañana a las calles [\#31PorLaEducacion](https://twitter.com/hashtag/31PorLaEducacion?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/bEFtHBwP6J](https://t.co/bEFtHBwP6J)
>
> — Daniela Alvarez Gallo (@\_Erbrow) [30 de octubre de 2018](https://twitter.com/_Erbrow/status/1057415564673667077?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
