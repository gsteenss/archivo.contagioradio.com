Title: No necesitas a Hidroituango para conectar tu iPhone
Date: 2018-07-23 06:00
Category: CENSAT, Opinion
Tags: Agua, ANLA, Hidroeléctricas, Hidroituango, Ministerio de Minas
Slug: no-necesitas-a-hidroituango-para-conectar-tu-iphone
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Ituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CENSAT 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 23 Jul 2018 

[La resolución 820 de la Autoridad Nacional de Licencias Ambientales ANLA emitida el 1 de Junio de 2018 impone la “]*[suspensión inmediata de todas las actividades regulares relacionadas con etapa de construcción, llenado y operación del embalse dentro de la ejecución del **proyecto Construcción y Operación Hidroeléctrico Pescadero – Ituango**]*[**”**. Esta resolución revive la polémica suscitada por  dicha megaobra, pues a pesar de la catástrofe social y ecológica que ha generado, las acciones emprendidas por Epm se han centrado en la salvaguarda del proyecto y no de las y los afectados.]

[A pesar de las crecientes denuncias, las represas suelen promocionarse como energía limpia, obviando así el sacrificio de los territorios destinados para tal fin, con todo lo que ello implica para las comunidades y la naturaleza. De otro lado se ha instaurado en la opinión pública un imaginario de escasez energética, generándose así unas condiciones de posibilidad por las cuales las hidroeléctricas se presentan como una opción no solo viable, sino deseable para suplir las crecientes necesidades energéticas. Desde hace años el Movimiento Ríos Vivos ha venido cuestionando ¿Para qué y para quién es la energía que se genera?, un interrogante que cobra más relevancia con la ampliación de la generación energética en el país. En ese sentido también nos preguntamos ¿qué tan real es que sin hidroeléctricas, incluyendo Hidroituango, no tendremos con que alumbrar nuestros hogares o disponer de electricidad continuamente?.]

[XM- operador del Sistema Interconectado Nacional (SIN) y administrador del Mercado de Energía Mayorista- registra en su sitio web que la capacidad efectiva neta del SIN para 2017 fue de 16,7GW, mientras la demanda de potencia máxima no llegó a 10GW (9,9 GW). Esta tendencia es de larga data, pues desde el apagón de 1992, la generación energética ha sido una prioridad en la agenda del país. Precisamente refiriéndose a la situación de emergencia en Hidroituango, la presidenta de la Asociación Colombiana de Generadores de Energía (Acolgen), Angela María Montoya afirmó en el diario El Tiempo en mayo de 2018, que “]*[sin Hidrotuango y bajo el escenario de alta demanda de la Unidad de Planeación Minero Energética UPME cruzado con la energía firme, hay excedentes de 7,656 GW hora año para diciembre del 2018]*[”.]

[Ahora, **con Hidroituango funcionando se adicionarían** 2,4 GW al SIN, además la UPME propone aumentar la generación hidroeléctrica del país en 5.6 veces, es decir más del 550%. Así lo afirmó Jorge Alberto Valencia -director general de la UPME- en el lanzamiento del Atlas hidroenergético en 2015:]*[“Actualmente contamos con una capacidad eléctrica instalada de 15 GW, de los cuales 10 GW corresponden a generación hidráulica. Con esta nueva herramienta (el atlas), se logró estimar que dicha cifra tiene un potencial de aprovechamiento de hasta 6 veces la capacidad actual del país, para un total de 56 GW sólo en proyectos a filo de agua”.]*

[Si tenemos en cuenta que, según XM, las mayores demandas energéticas están en dos actividades: las industrias manufactureras (41,7%) y las explotaciones de minas y canteras (24,8%) y los servicios sociales consumen menos del 8%, podemos prever que la energía que ofrecería Hidroituango no será precisamente para el consumo doméstico, más aun si observamos que el consumo energético per cápita en Colombia no es muy alto (0,0012 GWh – puesto 135 en ranking mundial de 217 países, según CIA World Factbook). Entonces ¿si la población colombiana no es gran consumidora de energía para qué tantas hidroeléctricas? ¿A dónde irán a parar los excedentes energéticos? ¿Quiénes se benefician con la expansión energética en Colombia y quiénes pierden?.]

[El listado de los grandes consumidores energéticos en Colombia actualizado por el Ministerio de Minas y Energía a junio de 2016 nos permite comprender hacia dónde prioritariamente se está encauzando la energía producida, dicho listado muestra que los mayores consumidores  son los sectores petrolero, minero, industrial, eléctrico, de construcción, y portuario entre otros. Los nombres de muchas de estas empresas o proyectos están asociados a graves conflictos socioambientales y algunos son tristemente célebres por devastación ambiental, abusos laborales y violación a derechos colectivos de las comunidades, entre otras afectaciones.]

![tabla opinion](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-22-a-las-21.07.46-762x603.png){.alignnone .size-medium .wp-image-54963 width="762" height="603"}Con este panorama tanto **Hidroituango como otras hidroeléctricas**, son no solo innecesarias para suplir la demanda energética nacional, que ya está cubierta incluso para los grandes consumidores energéticos, sino mezquinas en tanto los graves impactos son minimizados y gestionados desde una actitud negligente por parte de las empresas, en este caso EPM, la cual suele ir acompañada de una inoperatividad estatal. Las decisiones sobre Hidroituango no pueden seguirse tomando a partir del sofisma de que el país necesita esa energía, sin estudiar rigurosamente el destino de ésta y cotejarlo con los impactos ocasionados y los potenciales.

[**La tragedia humanitaria y ecológica desprendida de Hidroituango no se remonta a este año**, y a las cerca de 30.000 personas desplazadas desde hace tres meses; el proyecto ha sido criticado por impedir la participación social, impedir la búsqueda de verdad, afectar pueblos indígenas, mala praxis ambiental, vulneración al derecho al trabajo, incumplimiento de los protocolos de la ONU para desalojos, entre muchas otras. Continuar promoviendo las hidroeléctricas como energía limpia y necesaria impide ver con realismo los impactos generados por proyectos como éste que levantan muros tan altos como la injusticia social y ambiental que generan.    ]

[Finalmente, a partir de la crisis de Hidroituango seguimos insistiendo en la necesidad de continuar el diálogo nacional sobre la transición energética, como el realizado recientemente en Barrancabermeja por la Cumbre Agraria Campesina Étnica y Popular y por la Mesa Social Minero Energética y Ambiental que reafirmó que “]*[seguiremos caminando la palabra y fortaleciendo la movilización por el reconocimiento y respeto de las diversas formas de vida para la transición energética, por la construcción de un nuevo modelo energético que respete los gobiernos propios, las autoridades étnicas, que se articula con nuestros planes de vida, que nos reconozca como autoridades ambientales, que reconozca a la naturaleza como sujeto de derechos y los derechos de la madre tierra, el diálogo de saberes y la educación propia guiada por nuestras cosmovisiones y cosmogonías serán el camino más rápido de esta transición]*[”.]

**¡Que el Estado responda por algo, desmantelen Hidroituango!**

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radi
