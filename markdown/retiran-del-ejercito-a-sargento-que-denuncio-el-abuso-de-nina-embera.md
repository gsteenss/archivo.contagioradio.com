Title: Retiran del Ejército a sargento que denunció el abuso de niña Embera
Date: 2020-07-03 14:13
Author: CtgAdm
Category: Nacional
Slug: retiran-del-ejercito-a-sargento-que-denuncio-el-abuso-de-nina-embera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/EcAReMsXkAIOv6S.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-06-29-at-17.21.53.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego que el país conociera los hechos del abuso de la niña indígena en Risaralda por parte de 8 militares, este 1 de julio se conoció el comunicado del Comando Central del Ejército en el que se tomó la decisión de retirar al sargento **viceprimero Juan Carlos Díaz**, militar que comandaba a los uniformados que abusaron de la menor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El comando general  Eduardo  Zapateiro, señalo “Como comandante de los soldados involucrados en la violación, era su obligación moral, institucional y legal informar, como es el deber de cualquier funcionario, era lo mínimo que debía hacer. [Lea también: Piden a la JEP abrir caso sobre violencia sexual](https://archivo.contagioradio.com/pedimos-a-la-jep-que-se-abra-un-caso-de-violencia-sexual-humanas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tal responsabilidad es mayor máxime cuando era el comandante, pero prevenir era su responsabilidad particular, el mando y control significa que por su cargo los comandantes deben estar al frente de los procedimientos que ejecutan los militares que están a su mando” sin embargo, en redes algunas personas cuestionaron el retiro del militar, entre ellos la Presidenta Nacional de[l Moviumiento El Mais](https://twitter.com/movimientomais)con el siguiente mensaje:

<!-- /wp:paragraph -->

<!-- wp:html -->

> ATENCIÓN.  
>   
> Retiran del [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) al Sargento Viceprimero que denunció a los 7 violadores de la niña Embera.  
>   
> Protegen a los narcotraficantes, corruptos, violadores, genocidas.  
>   
> En este gobierno los buenos pierden, como el policía que se negó a un desalojo violento. [pic.twitter.com/aqTz3gjyZ6](https://t.co/aqTz3gjyZ6)
>
> — Martha Peralta Epieyú (@marthaperaltae) [July 3, 2020](https://twitter.com/marthaperaltae/status/1279041510110113798?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:image {"id":86238,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/EcAReMsXkAIOv6S.jpg){.wp-image-86238}  

<figcaption>
Comunicado sobre el retiro del militar

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Actuaciones del ejército frente al Sargento Diaz abre debate nacional

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con esta decisión por parte del ejército se abre un debate con dos aristas. Por una parte están quienes señalan que los militares o policías que se han atrevido a denunciar reciben traslados o retiros como parte de un castigo por el hecho de haber denunciado. Por otra parte, en este caso concreto, es acerca de la responsabilidad del sargento que debió prevenir el hecho. [Lea también. Hay políticas institucionales que promueven actuaciones criminales en las FFMM](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte la defensa del Sargento Diaz aseguró que está listo para asumir las investigaciones del caso, sin embargo aclaró que en todo momento se apegó tanto a los procedimientos de la le y la constitución, como al reglamento de las FFMM.

<!-- /wp:paragraph -->
