Title: Cuatro millones quinientos mil deberán votar "Sí" en plebiscito para la paz
Date: 2015-11-19 17:02
Category: Nacional, Paz
Tags: Delegación de paz de FARC, Delegación de paz del Gobierno, FARC, Iván Cepeda, paz, plebiscito por la paz, Polo Democrático Alternativo, proceso de paz
Slug: cuatro-millones-quinientos-mil-deberan-votar-si-en-plebiscito-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/radiomacondo.com_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:radiomacondo.fm 

###### [19 Nov 2015] 

Este miércoles fue aprobado, en su segundo debate, el proyecto de Ley de** plebiscito** para la paz, con el que se busca refrendar los acuerdos a los que llegue la delegación de paz de las FARC y el gobierno nacional.

Según lo aprobado, el **umbral de votación que se necesitaría para la aprobación sería del 13.5% de mayorías (es decir, **cuatro millones quinientos mil votos)**** pero no de participación mínima,  con el fin de impulsar la participación de las y los colombianos en las urnas.“No podrá entrar en vigencia nada para implementar la paz, si antes los colombianos no han votado a favor de los acuerdos de paz”, dijo el Ministro del Interior, Juan Fernando Cristo.

Por su parte, senadores como Iván Cepeda, del Polo Democrático Alternativo, aseguraron no estar de acuerdo con este proyecto de Ley. A través de twitter, Cepeda manifestó "No comparto proyecto de plebiscito: 1. No se acordó en La Habana. 2. Es inadmisible que votar por el derecho supremo a la paz tenga umbral".

**La reforma contempla que el gobierno deberá publicar y divulgar el contenido total del acuerdo final para la terminación del conflicto,** 30 días previo a la fecha del plebiscito nacional.

El proyecto ahora **pasa a las plenarias del Senado y Cámara**, para que se tome una decisión final frente a la iniciativa.
