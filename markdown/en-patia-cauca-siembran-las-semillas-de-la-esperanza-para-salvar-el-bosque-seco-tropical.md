Title: En Patía, Cauca, siembran las semillas de la esperanza para salvar el bosque seco tropical
Date: 2020-02-03 16:50
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Cauca, Mercaderes, Río Patía
Slug: en-patia-cauca-siembran-las-semillas-de-la-esperanza-para-salvar-el-bosque-seco-tropical
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/bbb9365d-911b-4435-b944-bb35b57798a7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Patía Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify","textColor":"very-dark-gray","fontSize":"normal"} -->

Durante este 1 y 2 de febrero, habitantes e integrantes del **Consejo Comunitario Conafros de Galindez, Patía en el Cauca** realizaron la siembra de al menos 40 árboles en el bosque seco tropical que colinda con la población y **el río Patía** como una forma de reivindicar un área de protección y recuperación de la memoria sociambiental, que viene siendo amenazada desde hace al menos quince años por proyectos hidroeléctricos y extractivos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tal como explica A**ndrés Caicedo, educador y habitante de Galindez**, pese a trabajar junto a la Agencia Nacional de Tierras para la entrega de espacios colectivos, por parte del Ministerio del Interior se ha negado la presencia de las comunidades en el territorio, **lo que ha derivado en la llegada de "proyectos de uso extractivo sobre la cuenca del río Guachicono**, afluente aledaño rico en materiales areníferos, al meno desde el 2004", afectando las dinámicas de las comunidades que luchan por la pervivencia cultural y étnica dentro del territorio.

<!-- /wp:paragraph -->

<!-- wp:image {"id":80181,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/2e2ae87f-d5d2-4192-b638-088942360d26-1024x682.jpg){.wp-image-80181}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify","textColor":"very-dark-gray","fontSize":"normal"} -->

Con el apoyo de la Comisión Intereclesial de Justicia y Paz, la jornada, contó con diversas expresiones artísticas y de diálogo en las que los mayores y mayoras rememoraron el estado del río Patía décadas atrás cuando el afluente era mucho más extenso, profundo y así mismo su población y biodiversidad era mayor, en contraste al estado actual del río.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hoy en día no se puede balsear porque el río está seco, uno antes iba a pescar para su almuerzo, hoy en día no se consigue nada" explica una de las habitantes de Galindez, población aledaña al Patía que participó en la siembra. [(Le puede interesar: En Mercaderes, Cauca, también quieren proteger la vida por encima de la Minería)](https://archivo.contagioradio.com/en-mercaderes-cauca-tambien-quieren-proteger-la-vida-por-encima-de-la-mineria/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":80182,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/04197838-0b87-49a5-9792-e2956201b01a-1024x682.jpg){.wp-image-80182}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

El bosque seco tropical, se diferencia de otros ecosistemas por presentar una fuerte estacionalidad de lluvias, característica que lo vuelve único en su tipo, por lo que solo esté presente en seis departamentos: **los valles interandinos de los ríos Cauca y Magdalena, Santander y Norte de Santander, el valle del Patía, Arauca y Vichada.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Andres Caicedo relata que las empresas como Ingeniería de Vías S.A han venido cambiando de razón social a lo largo de 16 años, haciendo difícil rastrearlas, sin embargo su accionar no solo ha deterioado el río Patia, sino que han permeado la población causando una división entre la comunidad y sus intereses, eso sin mencionar la deforestación y tala de árboles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las semillas llegan más allá del Patía

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La actividad fue acompañada a su vez por integrantes de comunidades del Cauca que también adelantan procesos similares en defensa del territorio y el agua como Inzá y Cajibío, **"compartir saberes nos permite conocer las experiencias de los diferentes municipios, es muy enriquecedor, me he dado cuenta que estas actividades no las realizan los adultos sino los jóvenes y eso lo podemos llevar a Inzá",** manifestó Luz Marina Cuchumbe, lideresa y representante del colectivo Sembradores de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Cuando hay esa apropiación del territorio se genera un espacio de unidad, estamos haciendo un espacio conjunto con tres comunidades y acoplando los municipios de de Mercaderes y El Patia, nos hemos dado cuenta que unidos somos más" reflexiona Andrea, maestro del grupo de danza Renacer que ha logrado convocar a las comunidades de El Pilón, Galindez y Palo Verde alrededor de la protección del bosque y sus afluentes. [(Lea también:¡No a la minería, sí a la vida! Comunidad de Mercaderes)](https://archivo.contagioradio.com/no-a-la-mineria-en-mercaderes/)

<!-- /wp:paragraph -->
