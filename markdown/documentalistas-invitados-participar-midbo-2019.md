Title: Documentalistas invitados a participar en MIDBO 2019
Date: 2019-07-01 12:10
Author: CtgAdm
Category: 24 Cuadros
Tags: Cine Colombiano, Documental, MIDBO
Slug: documentalistas-invitados-participar-midbo-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/midbo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MIDBO/Alados 

Con un homenaje póstumo al cineasta Jonas Mekas, vuelve la **Muestra Internacional de Documental de Bogotá – MIDBO**, un evento organizado por la Corporación ALADOS que **por más de veinte años se viene consolidando como una vitrina para el cine de lo real en Colombia y América Latina**.

En su versión 21 la Muestra abre su convocatoria para la recepción de **películas y obras audiovisuales en las categorías de Cine Documental y Documental Expandido**.  En medio de una búsqueda de autonomía e independencia, plantea otros modos de hacer, incluyendo los que se gestan desde las periferias: **los procesos colectivos y/o comunitarios,** que también han puesto a prueba narrativas, estéticas y modelos de producción audiovisual.

La convocatoria 2019 invita a la participación de diversas expresiones del audiovisual y el cine documental, entre las que se pueden mencionar: **documental de autor, de investigación, etnográfico, las expresiones audiovisuales comunitarias, étnicas, afro o de género.** Trabajos resultados de procesos de formación y producción alternativa o comunitaria, así como obras profesionales.

Por otro lado MIDBO cuenta con la convocatoria de exhibición, de la que pueden participar **películas directores y productores nacionales o extranjeros**, según la modalidad, con documentales f**inalizados a partir del 1° de enero de 2018**, de cualquier duración y formato, temática libre, las obras en idioma extranjero deben contar con subtítulos al español.

El calendario de inscripción estará abierto desde el **6 de junio al 20 de julio.** Los ganadores se publicaran el 5 de septiembre.

Para este año MIDBO parte de la obra y pensamiento del recientemente fallecido cineasta **Jonas Mekas, símbolo de la vanguardia norteamericana, se realizara del 29 de octubre al 7 de noviembre**. Para más información del evento y participación, puede consultar su página web [www.midbo.co](http://www.midbo.co/21/)
