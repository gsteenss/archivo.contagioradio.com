Title: 64 académicos de Colombia piden la renuncia del presidente de Ecopetrol
Date: 2016-04-20 17:00
Category: Nacional, Política
Tags: ANLA, Caño Cristales, Ecopetrol, Google, juan carlos echeverry, La Macarena
Slug: la-mala-racha-de-juan-carlos-echeverry-presidente-de-ecopetrol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/oscar-vanegas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [20 Abri 2016] 

Luego del debate entre Juan Carlos Echeverry, presidente Ecopetrol y el profesor Óscar Vanegas de la Universidad Industrial de Santander, al funcionario público no le ha ido muy bien y sus argumentos ante la Comisión Quinta de Cámara han sido rechazados y contra-argumentados en los últimos días.

Por un lado, **64 académicos de diferentes partes de Colombia enviaron una carta al presidente de Ecopetrol  solicitando su renuncia al cargo,** expresando su “profundo descontento y malestar frente a su actitud arrogante y despectiva para con la academia y la autoridad científica en temas socioambientales”, dice el manucristo.

Además, el documento evidencia un importante apoyo a la postura del profesor Vanegas, “Sus argumentos además de irrespetuosos frente al [[profesor Óscar Vanegas](https://archivo.contagioradio.com/presumo-que-hubo-torciditos-para-lograr-esa-licencia-lamacarena/)], demuestran **un gran desconocimiento de los verdaderos problemas ambientales, insensibilidad frente a los problemas sociales de las comunidades e irresponsabilidad total frente al cuidado de los recursos naturales de nuestro país**” y agregan que la presidencia de Ecopetrol, debe tener un criterio empresarial, pero también un sentido crítico y científico entendiendo que esta actividad petrolera puede generar graves impactos sociales y ambientales.

Por otra parte, Google también ha hecho quedar mal a Echeverry, en una búsqueda rápida su argumento ambiental y ficticio, aparece en el buscador con miles de resultados. Frente a los secuestros de humanos por extraterrestres, **existen 5.410 resultados, y sobre su argumento de [los acuíferos secados por la industria petrolera](https://archivo.contagioradio.com/no-es-posible-hacer-explotacion-petrolera-sin-danar-los-ecosistemas/), Google contabiliza 49.000** páginas donde se habla de ese tema.

Sumado a lo anterior, pese a que Echeverry aseguró contra viento y marea que no había tal daño a los acuíferos en la región de La Macarena, la [[ANLA se vio obligada a revocar la licencia ambiental](https://archivo.contagioradio.com/aun-hay-12-licencias-ambientales-mas-que-amenazan-la-macarena/)] que permitía la exploración y explotación petrolera de 150 pozos  cerca a Caño cristales.

[Piden Renuncia de Presidente de Ecopetrol](https://es.scribd.com/doc/309984215/Piden-Renuncia-de-Presidente-de-Ecopetrol "View Piden Renuncia de Presidente de Ecopetrol on Scribd")

<iframe id="doc_99852" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/309984215/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
