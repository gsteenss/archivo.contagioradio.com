Title: Restauran mural de Jaime Garzón vandalizado
Date: 2020-09-06 08:57
Author: CtgAdm
Category: Cultura, Nacional
Slug: restauran-mural-de-jaime-garzon-vandalizado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-06-at-7.58.00-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 5 de septiembre un grupo de **jóvenes integrantes del [colectivo Dexpierte](https://www.instagram.com/dexpierte_colectivo/?hl=es-la) y colectivo Movimiento Artístico Libre-MAL decidieron restaurar un mural en homenaje a Jaime Garzón** que había sido vandalizado por el grupo Defiende Colombia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Tati_Ramirez/status/1301583823000473603","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Tati\_Ramirez/status/1301583823000473603

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El mural de Jaime Garzón había sido realizado desde el pasado 3 de septiembre como una expresión artística de **apoyo a las exigencias de la comunidad estudiantil de la Universidad Nacional la cual completo 18 días en [huelga de hambre](https://archivo.contagioradio.com/matricula-cero-la-clave-para-garantizar-la-educacion-publica-superior/),** hasta el 28 de agosto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una expresión **de solidaridad que se extendió a las sedes de la Universidad nacional en Medellín y la Universidad Pedagógica** que también se encontraban en huelga de hambre exigiendo matricula cero y financiación para la universidad pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según los estudiantes, **la vandalización de las expresiones artísticas es una extensión de lo que es la censura  y  violación del derecho a la libertad de expresión por parte de grupos**, que desde el Gobierno, pero también desde algunos sectores minoritarios de la sociedad, se manifiestan en contra  del pensamiento crítico y diferente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con la restauración del mural de Jaime Garzón, los jóvenes aseguran que est**án revindicando, no solamente la memoria de Jaime Garzón sino también todas las exigencias que se le hacen al Gobierno en materia de condiciones de vida digna para todos y todas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo cual incluye, el d**erecho a la educación gratuita** y de calidad, el **derecho a la alimentación**, el **derecho a una vivienda digna**, derechos que si bien, según ellos, han sido  violentados desde hace mucho tiempo, son violencias que se exacerban en medio de la situación de pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De la misma manera, **se manifestaron en solidaridad con los campesinos y campesinas, indigenas, afrodescendientes, líderes y lideresas sociales que han sido víctimas de asesinatos completando más de 1000 personas**. Por lo cual, se exige el respaldo al cumplimiento del acuerdo de paz.    

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
