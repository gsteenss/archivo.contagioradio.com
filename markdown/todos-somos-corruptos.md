Title: "Todos somos corruptos"
Date: 2015-04-01 12:50
Author: CtgAdm
Category: Opinion, superandianda
Tags: Andres Felipe Arias, das, Maria Pilar Hurtado, Pretelt, superandianda
Slug: todos-somos-corruptos
Status: published

#### Por[ [Superandianda](https://archivo.contagioradio.com/superandianda/)] 

“Todos somos corruptos” podría ser una campaña política de indignación uribista, pero en la sociedad colombiana es la campaña -cultural- que se ha promovido, se mantiene y ha dado excelentes resultados.

En la democracia el aparato estatal de gobernantes y administradores de los bienes públicos son elegidos por el pueblo y para el pueblo, pero en nuestro país es diferente, ese aparato estatal es elegido por el pueblo corrupto para los corruptos, haciendo de este sistema electoral no más que el protocolo y festín donde se reparten los cargos y bienes del estado.

Todos lo sabemos, todos conocemos el sistema de compra y venta de conciencias, pero curiosamente cada vez que se destapa una olla podrida todos nos tapamos la nariz, los medios se escandalizan y los mismos ciudadanos que validaron el sistema ilegal con su voto se indignan; dos semanas después con algún partido de fútbol, novela o chisme de la farándula todo queda en el olvido, archivado en las páginas de la vergüenza nacional.

De la sociedad ilegal y la cultura mafiosa han nacido grandes producciones televisivas y también grandes comedias, es entonces cuando uribistas nos brindan el espectáculo de indignación con sus vestiduras rasgadas –como suelen hacerlo ante todos los escándalos que los acompaña- los ciudadanos de bien alzan su grito al cielo, la iglesia promete un futuro castigo divino y los medios de comunicación muestran su cotidiana sorpresa. Los episodios protagonizados por Andres Felipe Arias, Maria Pilar Hurtado y actualmente el magistrado Jorge Pretelt -sin nombrar toda la lista de ex-funcionarios procesados prófugos de la justicia- se han repetido y se seguirán repitiendo porque en el estado colombiano hemos adoptado a la trampa como nuestra cultura, de la que renegamos pero practicamos.

Al contrario de lo que nos dicen los noticieros la crisis en la justicia no llego con Pretelt, viene desde el momento que la jurisprudencia comenzó a venderse al mejor postor y cuando los aparatos del estado como el DAS comenzaron a servir no para la protección estatal sino para la protección de la mafia y de un régimen de corrupción que se legaliza a través de las ramas del poder público. Todo nos deja una gran comedia donde los ciudadanos juzgan al estado que ellos mismos eligen.

La campaña de cultura mafiosa no solo se desarrolla en el escenario político sino en la vida común de personas comunes, todo está diseñado para que lo seamos: hacer apología a los narcotraficantes volviéndolos héroes y crear historias de amor de los verdugos con las historias de las víctimas de la violencia, son aspectos que buscan justificar la ilegalidad de la que tanto nos quejamos: Hecha la ley hecha la trampa, la ley del más vivo, las leyes son para los más bobos, todos los políticos tienen que robar, aplaudir la trampa, enfrentarse al igual pero callarse ante el superior, pasar por encima de los demás porque para eso se paga, son algunas de las muchas razones que hacen validar el delito. Recordemos la frase “Cada pueblo tiene el gobierno que merece”.

Es tan fácil ser politiquero en Colombia que solo es cuestión de hacer una buena campaña que compre al ignorante y premie a quienes los consiguen, acostumbrados a que el servidor público trabaje por lujo y no por servicio nos ha vuelto incapaces de combatir el sistema electoral que no funciona para nosotros sino para el poder. Quizás llegue el día en que “todos somos corruptos” sea el nombre de un partido político y aceptemos que somos culpables del estado hipócrita y doble moral en que nos hemos convertido.
