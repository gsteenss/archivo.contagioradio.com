Title: La verdad de los desaparecidos no se satisface cuando se encuentran sus cuerpos: Francisco de Roux
Date: 2019-08-28 12:36
Author: CtgAdm
Category: Memoria, Paz
Tags: comision de la verdad, Desaparecidos en Colombia, Encuentros por la verdad, nariño, UBPD, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: la-verdad-de-los-desaparecidos-no-se-satisface-cuando-se-encuentran-sus-cuerpos-francisco-de-roux
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Francisco-de-Roux.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Francisco-de-Roux.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la Verdad ] 

[Francisco de Roux, presidente de la **Comisión por el Esclarecimiento de la Verdad (CEV)**, se refirió al Segundo Encuentro por la Verdad encaminado a homenajear y reconocer la labor de las familias y  mujeres que no han dejado de buscar a sus seres queridos quienes desaparecieron en el marco del conflicto, a veces, incluso hace más de 30 años.]

[Desde Pasto,  Nariño, el padre de Roux  indicó que para la CEV, el asumir este compromiso  junto a organismos como la JEP, la Unidad de Busqueda de Personas dadas por Desaparecidas y de la mano con las víctimas, es de gran importancia, pues en medio de esta tarea, son precisamente estas mujeres las que con su entrega "nos conmueve y  nos llama a todos a luchar por la dignidad humana", señaló. [(Lea también: Comisión de la Verdad expuso sus avances tras un año de trabajo)](https://archivo.contagioradio.com/comision-de-la-verdad-expuso-sus-avances-tras-un-ano-de-trabajo/)]

> **"El silencio y el miedo que tuvo Colombia a enfrentar estas cosas nos sumió a todos en una especie de desaparición de nuestra dignidad".**

"Aquí lo que hablan no son documentos ni pruebas escritas, es el dolor directo de la gente, mujeres que han sido estigmatizadas por sus luchas", afirma el padre, quien advierte que en muchos casos, ante la inoperancia del Estado, estas mismas personas que se han convertido en investigadores y buscadores también son sometidos a desaparición y que pese a ello son un símbolo de coraje y determinación.

\[caption id="attachment\_72764" align="aligncenter" width="1024"\]![Francisco de Roux](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Francisco-de-Roux-1024x505.jpeg){.size-large .wp-image-72764 width="1024" height="505"} Foto: Contagio Radio\[/caption\]

### [**El camino que se debe seguir para la no repetición **  
]

[El padre de Roux señala que no solo se trata de una conmemoración y que uno de los objetivos por alcanzar es convivir en una sociedad en diálogo consigo misma. [(Le puede interesar: La verdad debe ser un bien público, un derecho y un bien ineludible: Francisco De Roux)](https://archivo.contagioradio.com/verdad-debe-ser-bien-publico-derecho-bien-ineludible-francisco-de-roux/)]

"Esta verdad no se satisface únicamente cuando se encuentran los cuerpos o los huesos" asegura el presidente de la CEV al indicar que es necesaria una justicia basada en  la verdad y cimentada en el esfuerzo de esclarecer quienes fueron los responsables, dónde están, qué intereses los movían y quiénes les dieron las órdenes de cometer estos actos.

**Síguenos en Facebook:**  
<iframe id="audio_40608328" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40608328_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
