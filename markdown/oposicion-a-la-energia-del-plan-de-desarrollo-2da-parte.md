Title: Oposición a la energía del Plan de Desarrollo (2da parte)
Date: 2015-03-25 15:23
Author: CtgAdm
Category: Opinion
Tags: conflictos socio-ambientales, EPM, Megamineria, Plan de Desarrollo, PND
Slug: oposicion-a-la-energia-del-plan-de-desarrollo-2da-parte
Status: published

###### Foto: Isabel Zuleta 

Por**[Isabel Zuleta](https://archivo.contagioradio.com/isabel-cristina/)- [@ISAZULETA](http://bit.ly/1MvYsoI)**

¿Cuál es la importancia de la oposición a los mega proyectos minero energéticos? ¿Por qué el Plan de Desarrollo y la mercantilización de la naturaleza que impone no son una apuesta de paz?

Sin oposición no hay conflicto, sin denuncia no hay verdad, aunque la denuncia no asegure la verdad por lo menos nos acerca a ella. Sin conflicto una de las partes, la más poderosa, contará y escribirá su versión de la historia, de lo que allí había, de lo que quedo, de las “maravillas del desarrollo”.

En Colombia la oposición política ha sido imposible en la practica, los pocos arriesgados han tenido su escarmiento. La posibilidad de oponerse no esta en el imaginario colectivo, la asimetría de poder ha cimentado el miedo, pero el miedo se ha enfrentado a la muerte que representan las represas y la mega-minería. Hoy en Colombia hay conflictos socio-ambientales que buscan contar la otra versión de la historia, la del desastre ambiental, el despojo y las injusticias socio-ambientales generadas una visión del desarrollo.

El país tiene clara la deuda histórica con el campo y sus habitantes. Los procesos organizativos más fuertes de oposición están en la ruralidad, prueba de ello es la Cumbre Agraria Etnica y Popular en la que los sectores urbanos, tan individuales y dispersos, apenas se están organizando pues las problemáticas que los identifican están en construcción frente a una claridad histórica de los problemas del campo y un pliego unitario que resalta la ruralidad. La movilización y la protesta, a pesar de una sociedad cada vez menos rural, que reprime sin tregua, se destaca en Colombia por ser principalmente de indigenas, campesinos y campesinas que luchan por los derechos ligados a la tierra y al territorio, a la ancestralidad y la cultura, a la memoria y las víctimas, al agua y la vida. El 9 de abril el país vera esto en las calles de las principales ciudades, una apuesta por la paz con justicia social y ambiental.

La electrificación llegó hace un año a algunas veredas alejadas de Nechí Antiquia (San Pedro alto, medio, La trinidad, El Cedro), las comunidades con ingresos que no superan los \$200 mil mensuales pero con comida suficiente y sin quien compre la yuca que sobra, no tienen con que pagar una deuda que por familia oscila entre los 800 mil y los dos millones. EPM no recibe la yuca. Estrategia de cobro el acoso, llamadas a los hijos y esposas, todo el tiempo, cual banco agrario con los caficultores, el tormento del celular y la deuda impagable “*cuando llegan a la vereda los de EPM me dan ganas de esconderme. Nos sancionaron y duplicaron la deuda porque no cumplimos el convenio*”. La electrificación nunca llegó a decenas de veredas afectadas por represas en Colombia ¿Como se explica esto?

“*Después de escuchar las deudas y el daño que ocasionan las represas ya no quiero la energía para ver televisión en mi finca*” Líder de vereda sin electrificar

¿Esquemas de planeación diferenciados? EPM es monopolio, genera y cobra por adelantado, es juez, banco, tienda de agua y energía que vende a quien tenga con que pagarla ¿servicio público? no, servicios como privilegio para unos pocos, servicios para la exclusión social.

El Plan consolida la locomotora minero energética, de la misma manera que los planes de consolidación militar, ahora la milicia con un “enfoque de crecimiento verde” que no es otra cosa que seguir destruyendo, sacrificando comunidades y conservando si genera dinero; la acumulación de capital, no debe olvidarse, es lo primordial. Parte de este enfoque verde se sustenta en ver en la Licencia Ambiental, que no es social y que es lo mínimo, como un obstáculo y un riesgo.

Diagnostica el Plan suficiencia energética (sólo por cinco años, por lo que son “indispensable” Hidroituango, Espíritu Santo, Cañafisto, Porvenir II, 12 en el río Cauca,[ ]{.Apple-converted-space}25 en el río Magdalena, más de 100 proyectos de represas que se cocinan para el país, un inventario de proyectos para el “aprovechamiento” de[ **TODAS**]{.s1} las fuentes) y alto potencial exportador de energía por la abundancia de agua y carbon en Colombia, potencial que es “necesario” aumentar, alimentar y asegurar con las interconexiones con Panamá y la expansión del comercio energético con Ecuador, Venezuela, Perú y Chile.

A lo anterior se suma un largo rosario sobre la necesidad de llevar energía a cada rincón del país que justifique las grandes represas. Los altos costos de la energía (10 millones por usuario) pero al mismo tiempo la reducción de sobretasas a usuarios industriales (Ley 1430 de 2010) concluye con la reiterativa “necesidad” de más y más proyectos e incremento en ingresos por regalías. Con estos diagnosticos el Plan pretende generar una “necesidad” ficticia para el país y nos miente porque no habla de las necesidades industriales y menos de la energía para la megaminería. ¿a que costo la energía para todos? ¿Quienes son todos? ¿hasta que parte del mundo se entiende ese todos? ¿Energía para que, energía para quien?

Continuare en el intento de digestión de los demás exabruptos de este nefasto Plan
