Title: 439 indígenas han sido desplazados en el Chocó en los últimos dos meses
Date: 2018-05-22 13:02
Category: DDHH, Nacional
Tags: Chocó, desplazamiento de indígenas, embera, indígenas
Slug: 439-indigenas-han-sido-desplazados-en-el-choco-en-los-ultimos-dos-meses
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/indigenas-desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [22 May 2018] 

Continúan los desplazamientos de comunidades indígenas en el Chocó debido a la fuerte presencia de grupos armados como **paramilitares y estructuras guerrillas  del ELN**. Denuncias de organizaciones que defienden los derechos humanos indican que los desplazamientos se han realizado en medio de la inoperancia de la Fuerza Pública y el Estado colombiano.

De acuerdo con la Comisión Intereclesial de Justicia y Paz, desde abril, **439 personas que componen 115 familias** de las comunidades Embera Katio y que habitan en las inmediaciones del río Jiguamiandó, se han visto obligados al desplazamiento. Esto, producto de la fuerte presencia de grupos neoparamilitares y guerrilleros que buscan el empoderamiento del territorio.

la Comisión señaló que **no se han tenido en cuenta las disposiciones de la Comisión Interamericana de Derechos Humanos**, quien otorgó medidas cautelares a estas poblaciones. Afirmó que no han sido suficientes las acciones institucionales para prevenir este tipo de violencia que pone en riesgo la supervivencia de las comunidades indígenas. (Le puede interesar:["Respeto a la vivienda digna, exigen indígenas Nasa en Bogotá"](https://archivo.contagioradio.com/respeto-a-la-vivienda-digna-exigencia-de-los-indigenas-nasa-en-bogota/))

A esto se suma la ocupación ilegal de predios de las comunidades indígenas por parte de empresas que realizan operaciones **extractivas y que han generado contaminación** en los ríos con mercurio y la ampliación de la siembra de los cultivos de la hoja de coca, “sin que ninguna autoridad respalde las propuestas comunitarias de manera concreta y eficaz”.

### **ONU denunció desplazamiento masivo de comunidades indígenas en el Chocó** 

Asimismo la Oficina de las Naciones Unidas para la Coordinación de Asuntos Humanitarios, indicó que **más de 650 indígenas** han sido desplazados en ese departamento. Los desplazamientos se han presentado desde principios del mes de mayo con el ingreso de hombres armados a resguardos como el Pavesa Gello en el municipio del Bajo Baudó.

###### Reciba toda la información de Contagio Radio en [[su correo]

 
