Title: Tinguas tuvieron que cambiar humedales por charcos
Date: 2016-01-30 10:16
Category: Ambiente, Nacional
Tags: cambio climatico, Humedales, Tinguas
Slug: tinguas-tuvieron-que-cambiar-humedales-por-charcos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Tinguas2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  You tube 

###### [30 Ene 2016.] 

En días recientes las Tinguas, una de las 166 especies de aves que subsisten en los humedales de la Sabana de Bogotá, se les ha visto paseándose por las calles, jardines y parques de la capital buscando agua.

La Tingua bogotana es una de las aves endémicas del altiplano Cundiboyacence; de allí radica su gran importancia. Es un ave que vive asociada al agua, con un largo pico rojo y grandes patas del mismo color. Por encima, su plumaje es café con manchas oscuras, por debajo es gris y en cada ala tiene una gran mancha café rojiza.

Según expertos, que las Tinguas se estén viendo obligadas a beber agua de los huecos del pavimento puede responder a dos fenómenos: el primero, relacionado con el calentamiento global que ha recrudecido la temporada de sequía en nuestro país. El segundo, referente a la degradación ambiental y disminución de los Humedales, debido a la contaminación o relleno de los mismos para actividades agrícolas, mineras y de construcción.

Según la Unión Internacional para la Conservación de la Naturaleza y BirdLife International, la Tingua bogotana entra en la clasificación de especies amenazadas de extinción. Aunado a esto, es preocupante la falta de interés y compromiso con la protección y conservación de los humedales de la ciudad por parte del actual alcalde, Enrique Peñaloza, quién ha expresado la creación de un circuito público ecológico y la construcción de la avenida ALO que afecta los humedales Capellanía, Juan Amarillo y La Conejera.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
