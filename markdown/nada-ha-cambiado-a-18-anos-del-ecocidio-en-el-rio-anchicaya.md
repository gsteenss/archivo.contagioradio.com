Title: Nada ha cambiado a 18 años del "ecocidio" en el río Anchicayá
Date: 2019-12-12 15:57
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Agua, Cauca, Hidroelectrica, represa
Slug: nada-ha-cambiado-a-18-anos-del-ecocidio-en-el-rio-anchicaya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-11-at-9.44.10-AM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/rio-anchicaya.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@RiosVivosColom] 

El 11 de diciembre en el Congreso de la República se desarrolló la primera audiencia pública sobre el "ecocidio" ambiental del Río Anchicayá, cuando miles de litros de agua reposada fueron vertidos al cause del río, afectando a más de 500 personas de la comunidad en marzo del 2001. La audiencia giró en torno a discutir la **situación actual humanitaria de las comunidades afectadas por los vertimientos tóxicos** de la represa del bajo Anchicayá. (Le puede interesar: [¿Qué oculta EPM tras evacuación de empleados por filtraciones en Hidroituango?](https://archivo.contagioradio.com/que-oculta-epm-tras-evacuacion-de-empleados-por-filtraciones-en-hidroituango/))

La audiencia estuvo precedida por 30 representantes de la comunidad; delegados de la Defensoría del Pueblo, Contraloría General,  Procuraduría, Autoridad Nacional de Licencias Ambientales (ANLA), Ministerio de Salud, Alcaldía de Buenaventura y más de quienes junto a expertos **hicieron un recuento histórico del territorio, un informe sobre la situación socioeconómica de la Cuenca, las afectaciónes desde el rol femenino**, y  cerraron con una intervención cultural con poesía y canto sobre el Rió Anchicayá. (Le puede interesar:[Defendamos la paz: Tejido de esperanza para los territorios](https://archivo.contagioradio.com/defendamos-la-paz/)).

El caso, según el abogado German Ospina, apoderado de este hace más de 9 años, fue llevado a los tribunales desde el 2004  y ha dado vueltas durante años sin que las víctimas fuesen indemnizadas por los daños causados, *"Epsa lo que ha hecho es contratar a los mas reconocidos y costosos abogados del país, para poder dilatar el proceso, sin importar las prueba que nosotros hemos presentado"*; y agregó que **al no ver soluciones por parte del Gobierno, presentaron el caso ante la Corte Interamericana de Derechos Humanos.**

### "En Anchicayá los españoles decidieron abrir las compuertas, de manera indiscriminada durante 30 días envenenando el Río"

La represa del Bajo Anchicaya opera desde 1955, y es uno de los proyectos de la  Empresa de Energía del Pacífico (Epsa),  filial de Celsia perteneciente al Grupo Argos, *"Desde su construcción, el proyecto desplazó grandes grupos de afrocolombianos que habitaban allí, pero **el mayor daño fue el de 2001 cuando durante más de un mes arrojaron lodo putrefacto de más de 50 años de aguas represadas**" *

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2916426145058533%2F&amp;width=800&amp;show_text=false&amp;appId=1237871699583688&amp;height=413" width="800" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El 21 de marzo de 2001 la Empresa de Energía del Pacífico abrió las compuertas de la represa como parte de una tardía labor de mantenimiento que debió hacerse desde 1999, **liberando 500 mil metros cúbicos de sedimentos tóxicos,  causando la mortalidad de miles de peces, pérdida de cultivos  y la afectación** de 6.000 personas, "*Hoy los peces y el agua están contaminados  por encimobacterias, metilmercurio, entre otros elementos tóxicos, el Gobierno prometió proyectos de piscicultura  y apoyo en alimento y agua, acciones que aún no se han cumplido",* afirmó Juan Pablo Soler, integrante del Movimiento Ríos Vivos.

> *"**El río representaba la columna vertebral de la comunidad**, desde el 2001 todo se ha venido resquebrajado, entre esto la perdida de los encuentros espirituales, al igual que el agua nuestra comunidad necesita sanar, necesita ayuda y desintoxicación". Natividad Urrutia. *

Luego de la audiencia quedan diferentes compromisos, uno de ellos realizar jornadas de investigación en el territorio para determinar los daños ambientales vigentes y con esto respaldar las  solicitud de indemnización que se hizo a la empresa. Adicional, Soler resaltó que se creará la **Comisión General de Represas, un organismo que respalde a las comunidades** *"La idea es poder decirles que hay forma de generar energía sin que afecte la integridad de las comunidades y que no están solos en este proceso que tanto daño les ha generado".*

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45516401" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45516401_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
