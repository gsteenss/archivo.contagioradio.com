Title: El Abrazo de la serpiente "Somos campesinos produciendo orgánicos contra multinacionales"
Date: 2016-07-25 10:11
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombiano, el abrazo de la serpiente, Premios Platino
Slug: el-abrazo-de-la-serpiente-somos-campesinos-haciendo-cine-contra-multinacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Platino-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Premios Platino 

##### 25 Jul 2016 

La 3era edición de los Premios Platino al cine Iberoamericano fue para Colombia. Durante la gala que tuvo lugar en Punta del Este, Uruguay la noche de este domingo, la cinta "El abrazo de la Serpiente" consiguió 7 galardones en las categorías principales incluyendo los reconocimientos a mejor director y mejor película.

Además de los mencionados, la coproducción colombo-argentina-venezolana, obtuvo las estatuillas a mejor dirección de arte, fotografía, montaje, sonido y música original, confirmando de esa manera su favoritismo, tras ser la única película iberoamericana nominada al Oscar  para producciones de habla no inglesa en su más reciente edición.

“Comparto este premio con Colombia y la paz que parece al fin que está llegando y aquí estaremos los cineastas para contarlo” aseguró Ciro Guerra director de la cinta mientas recibía la distinción como mejor director por encima de grandes nombres como el chileno Pablo Larraín y el argentino Pablo Trapero.

Al recibir el premio como la mejor producción de 2015, la productora Cristina Gallego agradeció a los diferentes países y profesionales que hicieron posible vivir el "sueño amazónico" e hizo un llamado a las autoridades cinematográficas y a los gobiernos para que apoyen a crear hábitos de consumo del cine que se produce en los países iberoamericanos.

"Somos campesinos produciendo productos orgánicos contra multinacionales, lo que pasa es que el cine no le da cáncer a nadie" aseguró Gallegos entre el sentido aplauso de aprobación por parte del auditorio, en referencia a los bajos índices de asistencia y permanencia en salas de las películas nacionales frente a las producidas por las grandes Majors de Hollywood.

En el cierre de la presentación, las ovaciones fueron para Antonio Bolivar, actor indígena ocaina (Karamakate en su vejez), quien con un estruendoso ¡Viva Colombia! saludo a los asistentes y millones de televidentes que seguían la transmisión en directo. Con sus ya mas de 70 años de edad, el patriarca agradeció al equipo de producción en especial a su director y finalizó con un mensaje de paz para el país.
