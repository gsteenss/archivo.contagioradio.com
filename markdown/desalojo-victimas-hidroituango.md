Title: Alcaldía ordena desalojo de 22 familias víctimas de Hidroituango
Date: 2018-09-27 12:57
Author: AdminContagio
Category: DDHH, Nacional
Tags: EPM, Hidroituango, Ituango, Maria Jose PIzarro, Movimiento Ríos Vivos
Slug: desalojo-victimas-hidroituango
Status: published

###### [Foto: @RíosVivosColom] 

###### [27 Sep 2018] 

Tras la audiencia para resolver la querella interpuesta por la Alcaldía de Ituango, Antioquia, **se ordenó el desalojo de las 22 familias que se refugiaban en el Coliseo de ese municipio**, luego de haber sido afectadas por la emergencia de la hidroeléctrica; en medio de un proceso con irregularidades y sin el apoyo necesario para resolver la situación de desplazamiento vivida desde hace más de 5 meses.

La querella pedía que los damnificados por el desastre de Hidroituango fueran desalojados del Coliseo Jaidukamá porque con su permanencia en el sitio afectaban el derecho de los niños a recrearse, **sin tomar en cuenta el derecho de los damnificados a un espacio digno de vida, a la reparación por la emergencia y al trabajo.** (Le puede interesar:["Alertan posible desalojo de víctimas de Hidroituango albergadas en Coliseo"](https://archivo.contagioradio.com/alertan-posible-desalojo-victimas-hidroituango-albergadas-coliseo/))

La audiencia sin embargo, tuvo varias irregularidades que el Movimiento Ríos Vivos denunció, entre ellas que no tuvieron tiempo suficiente para poder acceder a una defensa técnica y que las pruebas que presentó el movimiento fueron desacreditadas. Isabel Zuleta, vocera del Movimiento afirmó que el Secretario de Gobierno dijo que las comunidades habían ingresado de forma ilegal al Coliseo **"cuando él mismo en compañia de la policía había acompañado a las familias a refugiarse cuando perdieron sus casas y su sitio de trabajo por la emergencia".**

<iframe src="https://co.ivoox.com/es/player_ek_28936988_2_1.html?data=k52mlZudfJmhhpywj5WZaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncarnwsfSzpC-uc3Z1caY25DQpYznytniw8jNaaSnhqeg0JDIqYzgwtiYxdTRuc_dxcbRx9iPqMbn0dHOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Adicionalmente, el Secretario presentó documentos del DAPARD (departamento administrativo encargado de atender desastres) afirmando que la emergencia había afectado el Municipio de Valdivia en adelante y no a Ituango; razones por la que se determinó el desalojo de las 22 familias refugiadas en el Coliseo. Por su parte, la comunidad apeló la decisión, esperando que haya una primacía de derechos para que se resuelva de fondo su situación, porque **su proyecto de vida, su trabajo, y sus hogares fueron "afectados por la emergencia de Hidroituango".**

### **EPM aceptó que pagó 70 mil millones para acelerar la construcción de la represa** 

Zuleta sostuvo que el hecho de que EPM haya aceptado que pagó para acelerar la construcción de Hidroituango es la certeza de que hay muchas mentiras **al rededor del proyecto,** y aseguró que si se hubiera investigado adecuadamente en su momento a la empresa, a día de hoy, la emergencia no habría ocurrido y no tendrían familias refugiadas o desplazadas en ningún municipio.(Le puede interesar:["Asesinan a Julian Areiza, sobrino de líder de Movimiento Ríos Vivos"](https://archivo.contagioradio.com/asesinan-julian-areiza-rios-vivos/))

> Plan de aceleración se dio por las fallas geológicas y por el compromiso con la CREG Los atrasos en las obras son reconocidos no obstante mintieron a la opinión pública una y otra vez diciendo que estaban al día en el cronograma de la obra [\#DebateHidroituango](https://twitter.com/hashtag/DebateHidroituango?src=hash&ref_src=twsrc%5Etfw)
>
> — Movimiento RíosVivos (@RiosVivosColom) [26 de septiembre de 2018](https://twitter.com/RiosVivosColom/status/1045084944584847360?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **EPM no responde las preguntas que se formulan sobre Hidroituango** 

<iframe src="https://co.ivoox.com/es/player_ek_28937573_2_1.html?data=k52mlZyZe5Shhpywj5WaaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKfq9Tgh6iXaaKtjLXW3MbWttCZk6iY1dTGtsafxtGYxsrGpdXZjMaYqs7IttDd1drO0MzTb8bijoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En el debate de control político sobre Hidroituango hubo dos realidades diferentes: una la que propusieron Federico Gutiérrez, alcalde de Medellín y Jorge Londoño, gerente de EPM, **en la que todo sobre el proyecto estaba bien;** y otra en la que los Congresistas Maria José Pizarro, David Racero y Fredy Muñoz **expusieron la grave situación económica y social que se vive cerca a la Hidroeléctrica.**

Pizarro sostuvo que hubo algo positivo en el debate y fue que todas las partes pudieron expresar sus opiniones de forma tranquila y respetuosa con los otros. No obstante, fue negativo que tanto Gutiérrez como Londoño se limitarán a elogiar los beneficios del Megaproyecto y sus acciones después de la emergencia; mientras la Representante a la Cámara denunció que Hidroituango revictimizó a cerca de 400 mil personas.

Estas son algunas de las preguntas que respondieron parcialmente o ignoraron tanto Gutiérrez como Londoño:

1.  - ¿Cuáles son las acciones que dentro de sus competencias (como Alcalde de Medellín) se realizaron en relación con la aprobación, seguimiento y control del proyecto Hidroituango, así como las labores para solucionar las diferentes problemáticas que se presentaron en torno al caso?
2.  Al director de EPM: Informe sí o no. ¿Se hubiera evitado la emergencia actual sí se hubiera terminado la presa antes de haber cerrado los túneles de desviación de los diseños iniciales?
3.  - Informar cuales fueron los criterios tenidos en cuenta para realizar el censo de afectados por el proyecto eléctrico de Hidroituango.
4.  En medios de comunicación informaron que en 3 años estaría el proyecto terminado, ¿informe a este congreso las razones que llevaron a estimar este tiempo? ¿Se mantiene dicha proyección? ¿Cuál es la proyección actual?
5.  En el primer comunicado de prensa del gerente de EPM, Jorge Londoño de la Cuesta, recién presentada la crisis, afirmó que se trataba de un hecho imprevisible de la naturaleza, en tal medida, entregue a esta Corporación (Cámara de Representantes) un informe de ¿por qué se llegó a esa afirmación y allegue el documento o estudios que la soportan? ¿Sigue manteniendo el gerente dicha posición al respecto, si o no?

EN el debate, los congresistas expresaron su interés de proteger a las víctimas de Hidroituango, así como a los líderes del Movimiento Ríos Vivos que están siendo amenazados en los municipios afectados por el Megaproyecto. Sin embargo, Pizarro sostuvo espera **que haya mejores respuestas por parte de Gutiérrez y Londoño, en el debate de control político que citará la Congresista Aída Avella en el senado.** (Le puede interesar: ["Ríos Vivos denuncia intimidaciones por parte de EPM y autoridades de Ituango"](https://archivo.contagioradio.com/rios-vivos-denuncia-ituango/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
