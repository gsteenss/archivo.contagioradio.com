Title: Acuerdo sobre víctimas acerca la posibilidad de la paz en Colombia
Date: 2015-12-14 16:56
Category: Nacional, Paz
Tags: cese bilateral, Conversaciones de paz de la habana, FARC, Juan Manuel Santos, jurisdicción especial para la paz, víctimas
Slug: acuerdo-de-victimas-en-proceso-de-paz
Status: published

###### Foto: pacifista 

###### [12 Dic 2015] 

La delegación de paz de las FARC y el gobierno nacional anunciaron que el próximo martes 13 de Diciembre se hará público el **acuerdo sobre el punto 4 de la agenda de conversaciones de paz, referente a las víctimas**. [En el acuerdo está incluida la Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/comunicado-conjunto-60-sobre-el-acuerdo-de-creacion-de-una-jurisdiccion-especial-para-la-paz/) que contempla la posibilidad de juzgar los crímenes cometidos por todos los actores en el marco del conflicto armado dando prioridad a la verdad y a los derechos de las víctimas.

A partir de las 10 am se realizará el acto en el salón de Protocolo “El Laguito”, acto en el que estarán presentes las víctimas del conflicto según lo anunciado por la propia delegación de paz del gobierno nacional y la delegación de las FARC.

Uno de los puntos importantes que contempla este acuerdo es el mencionado de la **Jurisdicción Especial para la Paz**, que contempla la creación de un tribunal que se encargue de recibir los procesos que presente la fiscalía o las organizaciones sociales y darle trámite a los[juicios en los que deberá prevalecer el derecho a la verdad](https://archivo.contagioradio.com/page/3/?s=jurisdicci%C3%B3n+especial+para+la+paz) y la justicia restaurativa como una posibilidad de garantía de no repetición.

Aunque este acuerdo ya se había dado a conocer en un comunicado de 10 puntos, [los 75 puntos que lo conforman guardan coherencia con lo anunciado el 23 de Septiembre](https://archivo.contagioradio.com/tribunal-especial-para-la-paz-tendra-7-salas-con-5-jueces-cada-una/) con algunos cambios que estarían enfocados en el juzgamiento a ex presidentes y a integrantes de las FFMM. **Según lo que se conoce hasta el momento, lo procesos contra los expresidentes los conocerá el tribunal pero los redirigirá a la Comisión de Acusaciones de la Cámara.**

Una de los puntos más esperanzadores del anuncio de este acuerdo es que el proceso de paz culminaría en 6 meses, luego de la publicación del acuerdo. Dentro de los plazos que se han establecido también resurge la posibilidad de un **Cese Bilateral de fuegos a partir del próximo 16 de Diciembre** como un “regalo de aguinaldos” para todos los colombianos.
