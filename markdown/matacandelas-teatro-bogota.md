Title: Matacandelas trae a Bogotá el Teatro del absurdo de Beckett
Date: 2017-05-30 17:41
Category: eventos
Tags: Beckett, Bogotá, teatro
Slug: matacandelas-teatro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/unnamed-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colectivo Matacandelas 

###### 30 May 2017 

Del 30 de mayo al 4 de junio el Colectivo Teatral Matacandelas de Medellín llega a la capital con su obra "Primer amor", una pieza inspirada en el cuento homónimo del escritor irlandés Samuel Beckett, figura clave del teatro del absurdo.

La compañía teatral paisa, adapta la narración escrita en 1945 en una puesta en escena que, a manera de monólogo, recoge algunos de los elementos representativos del teatro de Beckett como son los personajes marginales, "solitarios, desalojados del alma" como los califica Juan David Toro, protagonista de la obra.

Como recuerda el actor, desde hace por lo menos 10 años la compañía venia "coqueteándole" a la obra para poder montarla, reto que  el grupo cumplió en 2016 cuando logró estrenarla con éxito en el mes de octubre en Medellín para luego llevarla a Manizales, Río Sucio, Cali y ahora a Bogotá.

![matacandelas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/9.jpg){.alignnone .size-full .wp-image-41355 width="1200" height="800"}

La primera influencia estructural para construir la obra, parte de una puesta en escena del Teatro Fronterizo de España,  que fue presentada en Medellín hace algunos años, conservando algunos detalles de ese montaje sumado al "pinturazo" de Matacandelas  con su tradición artística de más de 37 años de existencia.

Los espectadores se encontrarán con "un ser humano que desnuda su alma, que escogió la otredad como lugar para habitar, para contar e imaginar su historia, desde su sensibilidad desde su postura ante la idea del mundo" invitación que hace el actor quién asegura que la obra "es un clamor por la existencia".

   
"Primer amor", se presentará en la Sala Seki Sano, ubicada en la Calle 12 \# 2- 65 Barrio La Candelaria, de Martes a Sábado: 7:30 P.M y el Domingo: 4:00 P.M  
<iframe id="audio_18990214" src="https://co.ivoox.com/es/player_ej_18990214_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**SINOPSIS:**

Ese viejo hombre que recuerda su vida, evoca el primer amor. Estuvo enamorado de una prostituta no monstruosa, al menos por cinco minutos: el tiempo que le tomó escribir el nombre de ella, Lulu,en una boñiga seca de vaca.

“Nada es más divertido que la desdicha, te lo aseguro...”. Esta idea, de un personaje de Beckett que vive en otra obra, sirve para decir que la historia que en esta va contando el proyecto de hombre negado en tercer debate es de una dolorosa tragedia, sí, pero que va surgiendo con un humor entre negro y sórdido.

Expulsado de la casa familiar tras la muerte del padre, él lleva su vida junto a un canal, de dos que tiene la ciudad, según comenta.

 
