Title: "Le exijo al gobierno que diga la verdad" Madre de Ayotzinapa
Date: 2015-01-28 20:04
Author: CtgAdm
Category: El mundo, Resistencias
Tags: Ayotzinapa, estudiantes, mexico, PGR
Slug: ayotzinapa-madre-de-estudiante
Status: published

###### Foto: politicasmedia.com 

##### [Madre de estudiante] <iframe src="http://www.ivoox.com/player_ek_4010763_2_1.html?data=lZWekpyad46ZmKiakpeJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLY08qYxsqPstDmzsbZy9jYpYzYxtjO0sbWqcTdxdSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Abogado de familiares] <iframe src="http://www.ivoox.com/player_ek_4010752_2_1.html?data=lZWekpyZdo6ZmKiakpqJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcPjyMbR0ZDKpc7dzc7O1MrXb8XZjNPc1NLFsMrn1cbgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Los familiares de los 43 jóvenes desaparecidos de Ayotzinapa en Iguala, Guerrero, el pasado 26 de Septiembre de 2014, **no creen en las declaraciones que entregó ayer el gobierno a través de la Procuraduría General**, en la que afirma que está comprobado que los estudiantes fueron asesinados, incinerados y sus restos botados al Río San Juan.

Según las versiones de los familiares, los funcionarios de la **PGR afirmaron que no estaban seguros de la muerte de los jóvenes**, pero en la entrega de la información al público, **contradijeron** lo que afirmaron inicialmente.

Una de las madres de los estudiantes, presente en la rueda de prensa, afirma que el Estado tiene a sus hijos y por ello exige que se los devuelvan con vida. **“Si este gobierno no nos puede resolver el problema, pues que dejen el puesto para que venga otro que si nos lo resuelva”**.

Por su parte, uno de los abogados de las familias, afirma que **no hay material probatorio concluyente para cerrar la investigación**. Además anunció que se presentará el caso ante la **oficina de desapariciones forzadas de las Naciones Unidas**.

Otra de las declaraciones que causó indignación fue la del presidente **Enrique Peña Nieto**, quién afirmó “**Estoy convencido de que este instante, este momento en la historia de México de pena**, tragedia y de dolor no puede dejarnos atrapados, **no podemos quedarnos ahí”**

A pesar de ello, los familiares re afirman que **seguirán en la búsqueda de los estudiantes** y que acudirán a todas las instancias necesarias, además, no descansarán hasta encontrarlos y por ello solicitan la **solidaridad de todos y todas** en el mundo.

**[Reproducimos el  comunicado de los familiares:]**[ ]

<!--more-->

### **CON PREMURA POLÍTICA PGR PRETENDA CERRAR LA INVESTIGACIÓN DE LOS HECHOS** 

<div style="text-align: justify; padding-left: 30px;">

</div>

<div style="text-align: justify; padding-left: 30px;">

Frente a lo dicho por la Procuraduría General de la República el día de hoy, las y los familiares de los 43 desaparecidos manifestamos:

1\. Que expresamos nuestro repudio a la manera en que el Gobierno Federal ha priorizado informar a los medios de comunicación antes que a las víctimas los avances del caso. Es preciso recordar que el Presidente de la República firmó una minuta comprometiéndose a que se informaría a los familiares antes que a los medios para evitar la revictimización, lo que se ha incumplido y se ha acentuado a las últimas semanas.

2\. Que la información dada a conocer el día de hoy los familiares no la conocíamos a profundidad, pues se ha incumplido el compromiso de proporcionarnos copias de todas las actuaciones que obren en los expedientes consignados, pese a que este también fue un compromiso asumido por el Presidente de la República Enrique Peña Nieto.

3\. Que no puede darse por concluida la investigación sobre la desaparición forzada de los estudiantes de la Normal Rural Raúl Isidro Burgos de Ayotzinapa debido a que:

a\) No puede darse por concluida la investigación porque no existe plena certeza científica sobre lo ocurrido en el basurero de Co cula. La PGR dio a conocer hoy que su hipótesis se sostiene en varios dictámenes de química, biología y otros. Dado que es más que conocido que las procuradurías mexicanas son especialistas en fabricar delitos y puesto que reconocidos científicos han expresado dudas sobre esta hipótesis, no podemos aceptar esos resultados hasta en tanto no realizan esos mismos peritajes expertos independientes con las más altas calificaciones. En ese sentido, reiteramos nuestra confianza en el Equipo Argentino de antropología forense y exigimos que se remuevan todos los obstáculos para que realicen su labor en condiciones óptimas.

b\) No puede darse por concluida la investigación porque la declaración de Felipe Rodríguez Salgado, contrario a lo señalado por la Procuraduría, no es determinante para esclarecer lo ocurrido en Cocula pues como se aceptó en la misma conferencia, dicha persona no declaró haber permanecido en ese lugar durante todo el tiempo que supuestamente duraron los hechos.

c\) No puede darse por concluida la investigación porque la información dada a conocer por la Procuraduría depende en exceso de declaraciones efectuadas ante ministerio público, por personas que fácilmente pudieron ser coaccionadas, pues es de todos conocido que en México la tortura es recurrente. Al respecto, se ha hecho pública información sobre la posible tortura de algunos inculpados sin que hasta ahora la Comisión Nacional de los Derechos Humanos haya intervenido para aclarar estas denuncias.

d\) No puede darse por concluida la investigación porque la Procuraduría ni hoy ni en ninguna otra conferencia ha aclarado cómo explica en su teoría del caso el cruento homicidio de Julio César Mondragón, cuyo joven cuerpo desollado fue encontrado en las inmediaciones de donde ocurrieron los hechos. ¿Cómo explica el señor Murillo este hecho hasta hoy no esclarecido?

e\) No puede darse por concluida la investigación porque el Estado mexicano, a cuatro meses de los hechos, se ha mostrado incapaz para detener a quienes según su hipótesis serían responsables. El Gobierno Federal no ha podido detener al máximo responsable de la corrupta policía de Iguala, Felipe Flores Velázquez, ni a su cómplice y subalterno Francisco Salgado Valladares. Tampoco ha sido capaz de detener a Gildardo Astudillo, alias el Cabo Gil, personaje a quien la propia PGR asigna gran relevancia en su versión de los hechos. También siguen prófugos once de las quince personas que según la procuraduría estuvieron en Cocula, lo mismo que el líder de Guerreros Unidos, Ángel Casarrubias Salgado El mochomo. Ante tanto prófugo, ¿Cómo pretende el Procurador cerrar la investigación?

f\) No puede darse por concluida la investigación porque la Procuraduría no ha logrado que exista un solo juicio penal iniciado por el delito de desaparición forzada de personas, siendo que esta es la figura jurídica adecuada para encuadrar los hechos dado que como todo México ha exclamad o, fue el estado el que desapareció a nuestros hijos. En tanto los juicios no se ventilen bajo las figuras legales pertinentes, no puede cerrarse el caso.

g\) No puede darse por concluida la investigación porque en el mismo expediente donde obran las declaraciones de los supuestos sicarios de Cocula, se encuentran también agregadas las declaraciones de otros sicarios de Iguala que confesaron haber atentado contra nuestros hijos no en Cocula sino en Pueblo Viejo y Cerro la Parota. ¿Cómo explica el señor Murillo que en un mismo expediente existan confesiones que se contraponen y son contradictorias entre sí?

h\) No se puede cerrar la investigación porque en sentido contrario a lo dicho en la conferencia de hoy, solo hay certeza de la muerte de uno de los estudiantes desaparecidos. Esta certeza no implica certeza sobre la muerte de otros ni sobre el lugar donde esto pudo haber ocurrido. En ese sentido, es un error jurídico decir que se ha consignado por homicidio, cuando en realidad de nuevo se consignó por secuestro con la agravante de privación de la vida, lo que técnicamente implica una diferencia relevante.

i\) No puede darse por concluida la investigación porque no se ha indagado la responsabilidad del Ejército, pese a que contrario a lo que hoy afirmó el Procurador, sí hay en el expediente indicios de su complicidad con la delincuencia organizada, pues el Policía Salvador Bravo Bárcenas afirmó ante el Ministerio Público que el Ejército sabía desde 2013 que los Guerreros Unidos controlaban la Policía de Cocula, pese a los cual las Fuerzas Armadas no investigaron a dichos delincuentes sino que antes bien les brindaron protección.

j\) No puede darse por concluida la investigación porque ni siquiera ha empezado el deslinde de responsabilidades sobre el entorno de corrupción política que desencadenó los hechos del 26 de septiembre. Está pendiente investigar a otros alcaldes, como el de Cocula, así como a otras autoridades del gobierno del estado de Guerrero.

Ante la falta de justicia y verdad en México, los familiares acudiremos a las instancias internacionales. Por ello, en breve una delegación de nosotros irá ante el Comité sobre la Desaparición Forzada de las Naciones Unidas a denunciar lo que ocurre en México.

Asimismo, no dejamos de lado que la Comisión Interamericana de Derechos Humanos ha designado un grupo de expertos que a la brevedad deberá comenzar a realizar su verificación técnica de la investigación realizada por México; esta revisión es indispensable pues sabemos que encontrarán múltiples irregularidades.

Al pueblo de México le pedimos que no nos dejen solos y que entiendan nuestra lucha. Frente a un Gobierno Federal que tiene prisa por cerrar el caso Ayotzinapa, reivindicamos nuestro derecho a dudar de autoridades que una y otra vez han fabricado expedientes para salir de crisis que muestran su ineficacia. Exigimos también respeto a nuestra dignidad pues los tiempos de las víctimas no son los tiempos de los políticos.

Hoy, en su conferencia de prensa la PGR afirmó que la desaparición de nuestros hijos era un “hecho atípico”. Pero en nuestro caminar estos cuatro meses, hemos constatado que la desaparición forzada es hoy en México una realidad generalizada a lo largo y ancho del país. Miente la Procuraduría cuando afirma que la desaparición de jóvenes y los narcogobiernos son atípicos, por el contrario hoy ese es el rostro herido de nuestra Nación. Hacia finales del año pasado, el propio Registro Nacional de Personas Desaparecidas ascendía a 26,000 personas; se trata de familias como lasd nuestras, atravedsadas por el dolor y la incertidumbre. Por eso, seguiremos luchando por justicia y verdad hasta que tengamos certeza plena sobre el paradero de nuestros hijos y hasta que transformemos México para que ninguna familia vuelva a vivir lo que nosotros estamos viviendo.

</div>

<div style="text-align: justify; padding-left: 30px;">

¡Vivos se los llevaron, vivos los queremos!  
¡Ayotzinapa Vive!

</div>
