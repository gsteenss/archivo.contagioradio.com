Title: 10 años sin Oscar Salas, 10 años de total impunidad
Date: 2016-03-09 14:38
Category: Nacional, Sin Olvido
Tags: Muerte Oscar Salas, Oscar Salas
Slug: 10-anos-sin-oscar-salas-10-anos-de-total-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/12795315_996951510375527_3715393434356968161_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Junio Ocho Memoria] 

<iframe src="http://co.ivoox.com/es/player_ek_10739302_2_1.html?data=kpWklZ6XdJOhhpywj5WWaZS1lJ6ah5yncZOhhpywj5WRaZi3jpWah5yncZKkjMaSpZiJhpLj1JDgy9OPk9TXwteYtcbQpdSZk6iYk5WPpYa3lIqvk9TXb8XZjNnc1sbQb8rh0drby8nFqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ana Benilda Angel] 

###### [9 Mar 2016] 

Este martes en el Centro de Memoria Histórica de la ciudad de Bogotá se llevó a cabo el foro ‘Desmonte del ESMAD como garantía de no repetición’, con el que se conmemoraron los 10 años de la muerte del estudiante de la Universidad Distrital, Oscar Salas Angel, tras ser atacado con una de las ‘recalzadas’ (cartuchos usados de gases lacrimógenos, rellenos de pólvora y canicas) fabricadas por efectivos del ESMAD, que fue propinada en su rostro y que le causó la excesiva pérdida de sangre que lo dejó sin vida durante una movilización en la Universidad Nacional.

El representante en el proceso judicial que libra la familia Salas, el abogado Luis Guillermo Perez, aseguró que pese a todos los esfuerzos jurídicos adelantados durante estos 10 años, hay "impunidad absoluta" en este crimen de Estado, catalogado así por tratarse del uso de un arma no convencional por parte de un funcionario público como lo es un integrante del ESMAD. Según afirmó Pérez "la investigación disciplinaria terminó siendo archivada y la investigación penal en La Fiscalía destruyó el material probatorio, incluidas las prendas de Oscar", pasando por alto incluso la declaración del efectivo Ricardo Angarita que confirma la responsabilidad de algunos oficiales de la Policía en la utilización de 'recalzadas' durante manifestaciones.

En el evento participaron familiares y amigos cercanos, entre ellos la madre del estudiante, Ana Benilda Angel quien aseguró que su hijo era un joven de 20 años nacido en el Libano, Tolima, municipio en el que había trabajado por la defensa de los derechos humanos y de los animales, en la alfabetización de comunidades indígenas y campesinas y en el tejido de mochilas y pulseras. "He tratado de rescatar lo que Oscar hacia y fui amenazada y sacada de Bogotá, dónde tenía a mis hijos estudiando y un empleo (...) estoy arraigada ahora en mi pueblo, desde allá trato de trabajar en lo que puedo por mi gente", aseguró Ana.

[![Oscar Salas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Oscar-Salas.jpg){.aligncenter .size-full .wp-image-21265 width="3456" height="2592"}](https://archivo.contagioradio.com/10-anos-sin-oscar-salas-10-anos-de-total-impunidad/oscar-salas/)

El estudiante de sociología de la Universidad Nacional y líder de la Asociación Colombiana de Estudiantes Universitarios, Omar Andrés Gómez, quien también participó del foro, fue enfático en afirmar que el movimiento estudiantil ha sido victimizado históricamente por cuenta del accionar militar y paramilitar que ha respondido a la "lógica de terrorismo de Estado que ha tratado la protesta social bajo la consideración del enemigo interno (...) una política que ha dejado múltiples violaciones a los derechos humanos". Según afirmó Gómez, es fundamental para la construcción de la paz la sanción de los crímenes de Estado cometidos por el ESMAD, no puede seguirse usando la violencia para acabar con la vida de los jóvenes en Colombia.

Mauricio Esguerra, integrante del 'Observatorio de Derechos Humanos de la Universidad Nacional', aseguró que la violencia contra líderes estudiantiles ha sido sistemática, "no ha parado y ha venido radicalizándose" tras las políticas determinadas en el marco del Plan Colombia, como el precedente inmediato para la conformación del ESMAD, un organismo cuyo accionar no es controlado por ningún ente institucional y que cometió las más graves violaciones a los derechos humanos durante el Gobierno de Uribe, bajo el pretexto de que las movilizaciones estudiantiles estaban infiltradas por la guerrilla. Esguerra aseveró que durante los últimos años esta persecusión y estigmatización se ha servido de herramientas judiciales.

Por su parte el Representante a la Cámara por Bogotá, Alirio Uribe, quien es proponente del debate para el desmonte del ESMAD, calificó como negativo el hecho de que "se estén asesinando jóvenes por un ejercicio arbitrario y criminal del uso de la fuerza por parte de la Policia Nacional en el marco de las protestas sociales", por lo que es necesario que se piense "una nueva polícia para el Posconflicto" y que el Ejército recupere su función constitucional de defender la soberanía nacional, teniendo en cuenta "la alta militarización de la sociedad colombiana" que lleva a definir cuál va a ser el rol de la fuerza pública en un eventual escenario de implementación de los acuerdos de paz.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
