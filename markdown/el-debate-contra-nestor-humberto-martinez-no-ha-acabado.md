Title: "Se quiere desviar la atención sobre las acusaciones a Néstor Humberto Martínez" Cepeda
Date: 2018-11-29 15:56
Author: AdminContagio
Category: Nacional, Política
Tags: Gustavo Petro, Nestor Humberto Martínez, Odebrecht
Slug: el-debate-contra-nestor-humberto-martinez-no-ha-acabado
Status: published

###### [Foto: El Espectador] 

###### [29 Nov 2018] 

El senador Iván Cepeda, del Polo Democrático señaló que pese a los intentos de los medios de información por desviar la atención sobre el Fiscal Néstor Humberto Martínez y **diluir su responsabilidad en el gigantesco escándalo de corrupción de los contratos de Odebretch** y del grupo Aval, persistirán en citar la continuación del debate de control político.

Para el senador, las acciones emprendidas en contra de Gustavo Petro, hacen parte de una campaña sistemática de persecución a la oposición que pretende apartar la mirada de las fuertes denuncias que se hicieron en el debate del pasado 27 de noviembre sobre los nexos del Fiscal General de la Nación y la corrupción de Odebrecht.

"Es evidente que se esta tendiendo una cortina de humo, con relación a un hecho que ocurrió hace 14 años" afirmó Cepeda, frente al vídeo en el que aparece Petro, y agregó que cuando este archivo fue presentado ante la plenaria, al senador de la Colombia Humana, se le negó el uso de la palabra, **impidiendo que rindiera declaraciones durante el debate y que se defendiera.**

### **El debate no ha terminado** 

Frente a qué pasará con el debate de control político a Martínez, Cepeda afirmó que el escenario del pasado 27 de noviembre **no se puede dar por terminado porque fue clausurado sin darse la oportunidad a los citantes de finalizarlo y concluirlo.**

El próximo martes 4 de diciembre, los 4 congresistas que propusieron el debate, radicaran una proposición pidiendo que continúe, exigiendo además que se respeten los argumentos e intervenciones que allí se presentan. (Le puede interesar:["Fiscal general fue amenzante porque sabe que lo desenmascaramos: Iván Cepeda"](https://archivo.contagioradio.com/fiscal-general-fue-amenazante-porque-sabe-que-lo-desenmascaramos-ivan-cepeda/))

###### Reciba toda la información de Contagio Radio en [[su correo]
