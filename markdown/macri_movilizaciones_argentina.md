Title: Macri ordena impedir el paso de la movlización de sindicatos en Argentina
Date: 2017-04-06 17:54
Category: El mundo, Otra Mirada
Tags: Argentina, Mauricio Macri
Slug: macri_movilizaciones_argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/952b60_63750745e3874b01acd3e5c6c4fc4d2f-mv2-e1491519047720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El sie7e 

###### [6 Abr 2017]

Las protestas de los movimientos sociales en Argentina vuelve con más fuerza. Esta vez, el gobierno de Mauricio Macri se enfrenta a la primera gran huelga de los sindicatos de ese país, convocada por la Confederación General del Trabajo, CGT.

El paro de este jueves evidencia el descontento de los sindicatos por las medidas del gobierno de Macri frente a la imposición de sus políticas económicas. Se trata de una jornada donde se unieron las dos otras grandes centrales, **las Central de Trabajadores de Argentina, CTA, y la CTA Autónoma,** junto a todas sus representaciones gremiales.

Sin taxis, buses, metro, ni aviones; así como sin colegios, universidades, ni bancos, amanecieron diferentes ciudades de Argentina prácticamente paralizadas, pues diversos sectores se sumaron a este primer paro nacional, que no se vivía desde que Mauricio Macri llegó a la Casa Rosada en diciembre de 2015.

Sin embargo, la represión contra la movilización social también se volvió a sentir, y el gobierno dio la orden de desplegar cerca de 2 mil agentes de seguridad para impedir el bloqueo de las autopistas y no dejarse ganar por "los mafiosos", como lo expresó el mandatario. **Tras la orden empezaron los choques entre los manifestantes y los fuerzas de seguridad, pues estos últimos empezaron a lanzar gases lacrimógenos y chorros de agua.  **

### Las razones 

De acuerdo con los gremios que se movilizan, las medidas económicas adoptadas por el actual gobierno argentino entre las que se cuentan el ajuste a las tarifas de **servicios públicos hasta en un 300% conocido como el “Tarifazo”** y la negociación con los denominados “fondos buitre” lograron que se pagara una deuda de 9.300 millones de dólares con tenedores de bonos especulativos. Asimismo, señalan que el gobierno ha provocado que la deuda argentina llegará a cifras históricas en 2016.

Los sindicatos aseguran que las recientes medidas han profundizado el desempleo, las importaciones, el cierre y recortes de fábricas, alzas en los servicios básicos y salarios pobres. De hecho, Jonathan Gilbert, corresponsal de The New York Times, ha afirmado de manera crítica que “los cambios económicos radicales de Macri agitan a la Argentina, acentuando las divisiones para las que quería construir puentes y llevando a algunos argentinos a dudar si el cambio será para mejor”.

Cabe resaltar también que recientemente el Gobierno pretende poner un techo en la negociación salarial de este año del 18% o 19% a los estatales y unos puntos más a los privados, en un momento en que la inflación del país no permite esas alzas.

Teniendo en cuenta ese panorama otros expertos como Sergio Palazzo, titular de La Bancaria, aseguran que **el presidente argentino “es el principal responsable” de la protesta por “no dar respuesta a los reclamos de los trabajadores”.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
