Title: Pescadores artesanales del Quimbo tendrán que ser indemnizados por Emgesa
Date: 2020-12-22 01:13
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: ecosidio, Hidroeléctrica El Quimbo, Pescadores, Pescadores artesanales
Slug: luego-de-6-anos-de-lucha-pescadores-artesanales-seran-indemnizados-por-la-multinacional-emgesa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/MG_3429-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Camila Lozano / Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 22 de diciembre y luego de mas de 6 años de lucha pescadores artesanales serán indemnizados por los daños que provocó la aceleración en las operaciones de la hidroeléctrica El Quimbo.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/Asoquimbo/status/1305657887646154752","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Asoquimbo/status/1305657887646154752

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

La reparación a los pescadores artesanales la tendrá que hacer la multinacional EMGESA, empresa del conglomerado internacional ENEL, junto al Ministerio de Medio Ambiente luego de los impactos generados por la anticipación de las operaciones realizadas durante el el 2010 tras el inicio de la construcción de la hidroeléctrica. ([Hidroeléctrica El Quimbo: «una amenaza potencial»](https://archivo.contagioradio.com/el-quimbo-amenaza-potencial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La demanda presentada en el año 2015 por la [Asociación Agropecuaria de Pescadores Artesanales de Yaguará](http://www.pitalitonoticias.com/2020/12/pescadores-artesanales-del-huila-le.html), daba cuenta de la preocupante reducción de las especies nativas que habitaban ese trayecto del Río Magdalena, afectadas directamente por la construcción y aceleración de la generación de energía de la represa, y con ello a cientos de pescadores artesanales y sus familias que realizaban esta práctica ancestral en en Neiva, departamento del Huila.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La acción fue ejecutada gracias a la respuesta del Tribunal Administrativo del Huila, que declaró como responsable el impacto sobre el río a la multinacional, y junto a esta a las autoridades nacionales y regionales** que en el momento *(periodo de 2010 a 2015)* , tendrían la responsabilidad del licenciamiento ambiental, la protección de las comunidades y la integridad del del Río y sus especies. ([Hidroeléctrica el Quimbo busca dividir para no compensar daños: Afectados](https://archivo.contagioradio.com/hidroelectrica-el-quimbo-busca-dividir-para-no-compensar-danos-afectados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La sentencia beneficiará directamente a 26 pescadores artesanales, y abre la posibilidad para que otros pescadores que no se hayan incluido en esta demanda puedan** presentar las pruebas que justifiquen los impactos de la represa en sus oficios y acceder del mismo modo a las indemnizaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El documento también da cuenta de la responsabilidad que tienen con las comunidades el Ministerio de Minas y Energía, la Autoridad Nacional de Licencias Ambientales (ANLA), la Autoridad Nacional de Acuicultura y Pesca (AUNAP), la Corporación Autónoma Regional del Alto Magdalena (CAM) , la Gobernación del Huila y la Alcaldía de Neiva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La acción del Tribunal en respuesta a la demanda incluye la protección de los Derechos Colectivos y las garantías a un ambiente sano que retorne la calidad de vida de los pescadores y personas afectadas ante el olvido de las reparaciones y responsabilidades producto del mega proyecto hidroeléctrico durante más de 10 años. ([La hidroeléctrica El Quimbo, con los mismos problemas de Hidroituango](https://archivo.contagioradio.com/el-quimbo-problemas-hidroituango/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
