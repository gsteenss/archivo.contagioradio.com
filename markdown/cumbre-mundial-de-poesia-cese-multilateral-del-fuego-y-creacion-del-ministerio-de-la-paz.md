Title: Cumbre Mundial de Poesía: "Cese multilateral del fuego y creación del Ministerio de la paz"
Date: 2015-07-26 08:07
Category: Cultura, El mundo, Paz
Tags: Cese al fuego, colombia, desescalar, FARC, Gobierno, Medellin, paz, pesia
Slug: cumbre-mundial-de-poesia-cese-multilateral-del-fuego-y-creacion-del-ministerio-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Poesia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Festival Internacional Poesia 

######  

###### [26 jul 2015] 

En el marco del 25 Festival Internacional de Poesía, que se desarrolló en Medellín del 11 al 18 de julio, se llevó a cabo también la II Cumbre Mundial de la poesía por la paz y la reconciliación de Colombia. En su declaración, poetas y poetisas de Colombia y el mundo hablaron del conflicto armado como un síntoma de la injusticia y el despojo, e hicieron llamado a desescalar el con conflicto mediante el cese al fuego pero también mediante el "desminado del lenguaje, propiciado por los medios de comunicación".

A continuación reproducimos la declaración completa.

**Declaración de la II Cumbre Mundial de la Poesía por la Paz y la Reconciliación de Colombia, en el marco del XXV Festival Internacional de Poesía de Medellín.**

*“Quien no puede vencer con las palabras, no debería vencer de ningún otro modo¨*

1\. Antecedentes del conflicto

Este conflicto que ha desgarrado al país por décadas es síntoma de viejas enfermedades: la injusticia y el despojo.

Así como la industria de la guerra ha minado la selva, los campos y las calles, ha hecho del lenguaje un fuerte y de la incultura guerrerista una lógica del mundo.

Por décadas se nos ha querido convencer que somos hijos de la violencia y del odio y que tal desmesura constituye nuestra identidad y destino. La incultura del odio ha creado una concepción de que la guerra resulta inevitable. El proceso de paz no es una dádiva sino una conquista propiciada por varias décadas desde el activismo de los movimientos sociales, campesinos, indígenas, afros, mujeres y estudiantes, en concierto con una gran mayoría de la población colombiana.

\[caption id="attachment\_11571" align="alignright" width="300"\][![Festival Internacional de Poesia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/poesia-3-300x200.jpg){.wp-image-11571 .size-medium width="300" height="200"}](https://archivo.contagioradio.com/cumbre-mundial-de-poesia-cese-multilateral-del-fuego-y-creacion-del-ministerio-de-la-paz/poesia-3/) Festival Internacional de Poesia\[/caption\]

La restitución de tierras debería suceder al unísono con la restitución de la paz, que también nos ha sido enajenada por tanto tiempo, y casi no la recordamos como un momento de nuestra historia. Quisiéramos que hubiera un desminado del lenguaje, propiciado por los medios de comunicación, y convertirlos en generadores de nuevos imaginarios de reconciliación, ajenos al belicismo verbal al que nos hemos ido acostumbrando.

2\. Desafíos de un presente: la poesía como imposible realizable

-La sostenida propaganda bélica no ha podido extinguir nuestra memoria y es posible volver a los orígenes de un nosotros oculto: el lugar donde resisten las espiritualidades que nos hermanan a unos y otros y a todos con la Madre Tierra, afirmando valores comunitarios.

A pesar del pesimismo mediático generado, no hay en Colombia un hombre ni una mujer que no alberguen el sueño de la reconciliación nacional, que no imaginen el reino de la justicia y que no añoren otro combate distinto al de las ideas. La paz tiene sus posibilidades reales en este acontecimiento que significa, en medio de un orden de muerte una cotidiana esperanza.

La poesía y el arte pueden transformar el dolor y la tragedia, vividas en memoria y fuerza para afirmar la vida y derrotar las argucias de la muerte. Disponernos a trasformar las hondas heridas que han producido en Colombia la injusticia y su despliegue bélico, nos compromete a todos a una reflexión sobre este malestar, pero también a reconocer lo que fuimos antes de contraerlo y de lo que, ya superado, podemos llegar a ser. Si afirmamos sin titubeos que la poesía es un imposible realizado, la paz debería ser un imposible realizable. Ese imposible realizable volverá a poner en nuestros ojos el país que no han dejado ser.

3\. Para hoy el porvenir

El presente es un futuro ya cumplido y por lo tanto no podemos dejarle la paz al porvenir. El porvenir es hoy.

Es preciso promover y materializar un cese multilateral del fuego hacia el silenciamiento definitivo de las armas de todos los actores del conflicto en nuestro país, para abrir anchas vías a acuerdos plenos que eliminen las causas y consecuencias de las guerras.

\[caption id="attachment\_11572" align="alignright" width="300"\][![Festival Internacional de Poesia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/poesia-5-300x220.jpg){.wp-image-11572 .size-medium width="300" height="220"}](https://archivo.contagioradio.com/cumbre-mundial-de-poesia-cese-multilateral-del-fuego-y-creacion-del-ministerio-de-la-paz/poesia-5/) Festival Internacional de Poesia\[/caption\]

El país necesita una nueva institucionalidad, un nuevo acuerdo social que garantice la justicia, un Estado de Derecho real. Proponemos desde esa nueva institucionalidad la creación de un Ministerio de la Paz. A manera de ejemplo señalamos algunos posibles puntos que le den base al mencionado Ministerio:

Garantizar y cuidar la memoria de este largo proceso y el devenir de una existencia como pueblos identificados con el diálogo y la convivencia.

Repensar las políticas como procedimientos indispensables para una verdad y una identidad común.

Hacer viable la justicia social para eliminar definitivamente las causas más evidentes del conflicto.

Proponer desde las diversas culturas que nos componen encuentros donde se valore la diferencia y la poética del diálogo.

Superar la condición de víctimas y victimarios impuesta por la lógica bélica y elaborar junto a las comunidades nuevas maneras de entendernos y afirmarnos como pueblos.  
Garantizar el libre acceso de las comunidades a los medios de comunicación.

Visibilizar y potenciar la función histórica de la poesía y las artes, para construir una paz profunda y verdadera.

#####  

##### II Cumbre Mundial de la Poesía por la Paz y la Reconciliación de Colombia. 

##### Medellín, Julio 17 de 2015. 
