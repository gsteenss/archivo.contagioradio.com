Title: Una nueva democracia ambiental para el posconflicto
Date: 2016-08-08 08:34
Category: Ambiente y Sociedad, Opinion
Tags: Ambiente, paz, posconflicto, proceso de paz
Slug: una-nueva-democracia-ambiental-para-el-posconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/paz-ambiental.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **[Natalia Gomez Peña - Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/)** 

###### 8 Ago 2016 

Como la gran mayoría de colombianos de mi generación, yo no conozco este país en paz. Desde que tengo uso de razón, la realidad de Colombia ha estado marcada por el conflicto armado. Solo ahora, luego de más de 50 años de violencia, vemos una luz de esperanza en el proceso de paz que está a punto de culminar en La Habana. A la par del proceso de paz, Colombia y otros 20 países de Latinoamérica y el Caribe participan desde el 2012 de una negociación regional transformadora que busca construir un instrumento internacional que contribuya a la cabal implementación de los pilares de la democracia ambiental contenidos en el Principio 10 de la Declaración de Rio de 1992: información, participación y justicia en asuntos ambientales. Este proyecto es una apuesta por el dialogo y la construcción de un mejor futuro para los niños y jóvenes de la región, se espera que transforme las vidas de millones de ciudadanos y contribuya a hacer frente a los retos que en materia ambiental enfrentan Latinoamérica y el Caribe.  

[La consolidación del desarrollo sostenible en América Latina y el Caribe demanda altos estándares en materia de acceso a la información, mecanismos de participación efectivos, y acciones para el acceso de todas las personas a la justicia ambiental. Como sociedad civil reconocemos la buena voluntad y el gran esfuerzo que han puesto todos los países participantes de la negociación para llevar adelante esta empresa, la cual llevará a cabo su cuarta reunión de negociación del próximo 9 al 12 de agosto en Santo Domingo, República Dominicana. Sin embargo, también hacemos un llamado para que se redoblen esfuerzos por alcanzar la meta propuesta, y que el resultado de este proceso verdaderamente fortalezca los estándares de gobernanza en nuestra región con la adopción de un Tratado Internacional.]

[Las negociaciones de paz en La Habana le están enseñando a Colombia que hay una forma diferente de hacer las cosas, que el diálogo es el camino para la resolución de los conflictos y que aún existe la esperanza para un país que por años ha sido dominado por la guerra. El mundo mira a Colombia con detenimiento, mientras los colombianos apostamos por la construcción de un nuevo país. De la misma manera, el proceso de negociación  regional sobre democracia ambiental es observado por el mundo entero,  y es muestra de que América Latina y el Caribe necesitan nuevas herramientas para la resolución de los conflictos ambientales y que es posible soñar con una región donde la protección de nuestro entorno y nuestros recursos no sea sinónimo de amenazas y muerte para nuestros defensores ambientales.]

[Es necesario que el instrumento regional sobre derechos de acceso asegure estándares regionales para la implementación de los derechos al acceso a la información, la participación y la justicia. Herramientas que fortalezcan los marcos legislativos nacionales; que incorporen mecanismos dinámicos para el fortalecimiento de capacidades y el intercambio de experiencias tanto de los Estados como del público; y  que suscriban un compromiso renovado con el desarrollo sostenible en la región.]

[Como sociedad civil, seguiremos atentos a la negociación y continuaremos pidiendo a Colombia y a los demás Estados reunidos que apoyen la adopción de un Tratado. Para nuestro país es especialmente importante que el posconflicto que esperamos con tanto anhelo contemple las medidas necesarias para la resolución de los conflictos ambientales, la prevención de la violencia que estos producen, la garantía de los derechos de las comunidades y se construya así una nueva Democracia Ambiental en Colombia.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
