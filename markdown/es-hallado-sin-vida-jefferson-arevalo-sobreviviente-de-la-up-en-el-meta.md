Title: Es hallado sin vida Jefferson Arévalo, sobreviviente de la UP en el Meta
Date: 2018-08-23 15:10
Category: DDHH, Nacional
Tags: Corporación Reiniciar, Meta, UP
Slug: es-hallado-sin-vida-jefferson-arevalo-sobreviviente-de-la-up-en-el-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Jefferson-Arévalo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Reiniciar 

###### 23 Ago 2018 

Luego de dos días de haber sido raptado por hombres encapuchados, el cuerpo de **Jefferson Andrés Arévalo**, hijo de una sobreviviente del genocidio de la Unión Patriótica e integrante de la Corporación Reiniciar, Capítulo Meta, **fue encontrado sin vida**.

De acuerdo con la información que  ha trascendido, **hombres armados irrumpieron en su vivienda ubicada en la vereda El Danubio, de Puerto Rico Meta** el pasado 21 de agosto. Mientras golpeaban y amarraban a Jefferson, torturaron, abusaron sexualmente a su esposa y estuvieron a punto de degollarla.

Jeferson Arévalo Robayo pertenecía a una reconocida familia de militantes y sobrevivientes de la Unión Patriotica en esa región. **Hace 15 años paramilitares habían asesinado a su mamá, la señora Luz Marina Robayo** quien hacia parte de ese movimiento político.

**Gladys Tirado, esposa de Arévalo, fue atendida por una ambulancia y trasladada al hospital de Granada, Meta,** según el reporte médico se encuentra en estado grave por las múltiples daños en su cuerpo, particularmente en sus órganos genitales.

A través de un comunicado el Centro Nacional de Memoria Histórica y la misma Corporación Reiniciar, habían manifestado su repudio ante estos hechos.
