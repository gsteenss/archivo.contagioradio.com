Title: Las razones por las que atentados de París afectaron a la COP21
Date: 2015-12-01 14:50
Category: Ambiente, Entrevistas
Tags: Calentamiento global, COP21, Cumbre climática, Cumbre por el cambio climatico 2015, Estado de excepci{on en Francia, Tom Kuchartz
Slug: las-razones-por-las-que-atentados-de-paris-afectaron-a-la-cop21
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/COP_21_detenida_París.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:cuartopoder.com 

###### [1 Dic 2015]

El gobierno de Francois Hollande **decretó estado de emergencia por los atentados que sufrió la capital el pasado 13 de noviembre**, que dejó más de 120 personas muertas, sin embargo ha permitido las conmemoraciones de estos atentados, partidos de fútbol, conciertos, y por el contrario **se ha mostrado implacable con las manifestaciones alrededor de la cumbre del cambio climático que se adelanta en la capital francesa**.

Hollande anunció la creación de 5.000 puestos adicionales en la policía y la gendarmería, 2.500 en la justicia y 1.000 en las aduanas, además de impulsar el financiamiento para la compra de equipos policiales, que según cifras oficiales descendieron un 17% entre 2007 y 2012.

Este aumento en la seguridad ha afectado las libertades civiles en el país galo y la COP21 ha sido el reflejo de esta situación. Muchas organizaciones están en desacuerdo con las negociaciones que se están dando en la cumbre, **los ciudadanos sienten que los gobernantes que están reunidos no los representan y las movilizaciones, como la del pasado 29 de noviembre para exigir más acción contra el cambio climático**, fueron reprimidas por las fuerzas policiales, dejando como resultado 100 personas detenidas.

Las movilizaciones han sido afectadas por el anuncio de Hollande tras los atentados, sin embargo, desde las organizaciones sociales ha hecho un **l**lamado a “en vez de declarar el estado de emergencia, nosotros **declaramos un estado de emergencia climático,** queremos desobedecer sus prohibiciones y sobre todo por las movilizaciones frente al cambio climático, también queremos poner una protesta contra los bombardeos en Siria y contra las guerras que están emprendiendo en Oriente Medio”, este último punto ligado al cambio climático ya que este conflicto, ha sido por recursos naturales, afirma Tom Kuchartz, miembro de ecologistas en acción.
