Title: "Neogociación Gobierno - ELN: Y sin embargo se mueve"
Date: 2015-09-28 11:37
Category: Nacional, Paz
Tags: alirio uribe, Angela Robledo, centro de memoria, Diálogos de paz en Colombia, ELN, Fase exploratoria ELN y Gobierno, justicia transicional en colombia, Negociación Gobierno ELN y sin embargo se mueve, Radio de derechos humanos, Senador Ivan Cepeda, Víctimas en Colombia, Víctor de Currea Lugo
Slug: neogociacion-gobierno-eln-y-sin-embargo-se-mueve
Status: published

###### Foto: Censat 

<iframe src="http://www.ivoox.com/player_ek_8666279_2_1.html?data=mZujmJebfY6ZmKiakp2Jd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmr8rcydTHrcLXyoqwlYqmd8%2BfqNTPy8rWstCfjpCyrrOJd6KfupDgy9OPqc7WwtfU0ZDXqYzh1srjx4qWdo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Víctor de Currea Lugo, Analista] 

###### [28 Sept 2015]

[El próximo jueves 1º de octubre a las 6:00 pm, se presentará en el  Centro de Memoria, Paz y Reconciliación el libro **Negociación Gobierno - ELN: Y SIN EMBARGO, SE MUEVE**, escrito por cerca de 29 autores, entre investigadores e integrantes de organizaciones de sociales y de derechos humanos y editado por el analista Víctor de Currea Lugo.]

[Currea sostiene que "A pesar del escepticismo de los enemigos de la paz, se adelanta el proceso con el ELN" en el que se contemplan puntos de discusión relacionados con el tema de **víctimas, participación política, democracia, transformaciones sociales necesarias para la consolidación de la paz y el fin del conflicto**. Temáticas en las que se concentran los artículos del libro.]

[De acuerdo con las investigaciones adelantas para la compilación del libro y las declaraciones de altos mandos del ELN en su último congreso, en el que ratificaron su compromiso con la paz, **se espera en las próximas semanas anuncios formales de instalación de la mesa de negociación**, luego de los tres años que ya cumple la fase exploratoria, afirma Currea.]

[El analista asegura que tal como lo ha indicado Nicolás Rodríguez, máximo comandante del ELN, **la agenda de negociación** propuesta entre el Gobierno y este grupo insurgente **resulta complementaria con la ya adelantada entre el gobierno colombiano y las FARC**, en materia de víctimas y justicia transicional, principalmente.]

[En relación con el acuerdo de justicia firmado la semana pasada en la mesa de diálogos de La Habana, Currea afirma que éste es un **acuerdo que logra satisfacer la tensión de la justicia** en términos de la coherencia con los derechos humanos y los pactos internacionales en la materia, una **"tensión entre paz y justicia enfrentada de una manera racional"**.]

[![Y sin embargo se mueve](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Y-sin-embargo-se-mueve.jpg){.aligncenter .size-full .wp-image-14811 width="1022" height="763"}](https://archivo.contagioradio.com/neogociacion-gobierno-eln-y-sin-embargo-se-mueve/y-sin-embargo-se-mueve/)
