Title: Minga Nacional se toma instalaciones de Ecopetrol en Bogotá
Date: 2016-06-02 11:08
Category: Paro Nacional
Tags: Ecopetrol, fracking, Minga, Paro Nacional
Slug: minga-nacional-se-toma-instalaciones-de-ecopetrol-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/ecopetrol-1-e1464883667743.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cumbre Agraria 

###### [2 Jun 2016] 

Sobre las 10 a.m, manifestantes de la Cumbre Agraria decidieron ingresar a las instalaciones de Ecopetrol en Bogotá, con el objetivo de exigir que no se adelanten acciones para implementar el fracking en Colombia, y también para que se proteja la naturaleza del modelo extractivista en el que se basa la economía colombiana.

A las afueras del edificio también se concentran personas que acompañan la Minga Nacional. Se reporta presencia de agentes de la Policía que ya amenazan con desalojar a los manifestantes de manera violenta.

<iframe src="http://co.ivoox.com/es/player_ej_11758108_2_1.html?data=kpakl52VdJmhhpywj5WWaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5yncaPmwt7O0JC2s8vV1JCajbfJt8rn1crbxc7Fb9Hj0drZw9ePcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Noticia en desarrollo...
