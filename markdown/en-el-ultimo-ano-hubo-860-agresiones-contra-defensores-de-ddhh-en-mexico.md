Title: En el último año hubo 860 agresiones contra defensores de DDHH en México
Date: 2015-08-31 14:07
Category: DDHH, El mundo, Política
Tags: Campaña Nacional contra desaparición forzada, comite cerezo, Derechos Humanos, Desaparición forzada en México, Día de las Víctimas de Desaparición Forzada, mexico
Slug: en-el-ultimo-ano-hubo-860-agresiones-contra-defensores-de-ddhh-en-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: radioamlo.org] 

<iframe src="http://www.ivoox.com/player_ek_7717054_2_1.html?data=mJyemZWZeI6ZmKialJmJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfxtGYh6iXaaO1zdnWz9SPpYa3lIqvk9SPrNbW0JClmJWPpcjmxtjW0dPJt4zX0NPh1MaPqMbaxtPg0dfJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Nadin Reyes, vocera de la campaña nacional contra la desaparición forzada] 

###### [31 Ago 2015] 

Entre entre 2014 y 2015 se han presentado** 860 casos de agresiones, hostigamientos, amenazas, detenciones arbitrarias, ejecuciones extrajudiciales y desapariciones forzadas contra defensores de DDHH** en México según el informe Defender los derechos humanos en México: la represión política, una práctica generalizada.

La Acción Urgente para la Defensa de los Derechos Humanos (ACUDDEH AC), el Comité Cerezo México y la Campaña Nacional Contra la Desaparición Forzada, presentaron el cuarto informe sobre Violaciones de derechos humanos (VDH) contra defensores de DDHH, para el periodo de junio de 2014 a mayo de 2015.

Entre agresiones, amenazas y hostigamientos el informe registró 330 casos que constituyen 488 eventos violatorios de los derechos humanos, que afectaron a 248 personas, 47 organizaciones y 35 comunidades. De las **459 detenciones arbitrarias** que fueron documentadas, 47 casos más que en el pasado informe, se tiene notificación de que **en 70 de ellas hubo tratos crueles, inhumanos, degradantes o de tortura**.

De los estados que continúan teniendo los mayores índices de casos Guerrero encabeza la lista con un registro de 202 casos, Distrito Federal con 140, Chiapas con 64 y Oaxaca con 55, Baja California 75 casos, Puebla y Quintana Roo con 62. Lugares en los que se llevan a cabo **extracciones de minerales por parte de transnacionales, sin el consentimiento de sus pobladores**.

Según Nadin Reyes, vocera de la campaña nacional contra la desaparición forzada, el **último año fue trágico para el movimiento social defensor de los derechos humanos** y que esta agudización se relaciona directamente con la **profundización de las políticas neoliberales** y como una **estrategia de control social y represión política por parte del Estado.**

El informe anota que **la cifra de personas privadas de la libertad por motivos políticos llega a 224** ; de las 67 ejecuciones extrajudiciales documentadas durante el gobierno de Felipe Calderón 2006-2012, se ha pasado a 47 en el periodo que lleva Peña Nieto, 22 de las cuales ocurrieron dentro del periodo de elaboración del informe y en lo que va del sexenio, **81 personas defensoras de los derechos humanos han sido víctimas de desaparición forzada**, grave violación que lejos de disminuir aumenta considerablemente.

Los casos documentados y las cifras arrojadas por el informe además de visibilizar las graves VDH que ocurren en México contra las personas que se atreven a organizarse y defender sus derechos, pretenden contribuir al fortalecimiento del trabajo de las organizaciones sociales y de defensa de los derechos humanos, posibilitándoles el acceso a más herramientas que les permitan disminuir el riesgo y los ataques a su ejercicio:

*“Quizá lo más importante es que, con este esfuerzo de documentación intentamos hacer un aporte, aunque modesto, para insistir en el hecho de que no podemos olvidar que las víctimas de las que da cuenta este informe no son sólo cifras: son hermanos, padres, hijas, estudiantes, amigos y compañeros… a los que otras personas estiman, esperan, extrañan o lloran”. Vea también [Sigue la persecución contra familiares de desaparecidos de Ayotzinapa](https://archivo.contagioradio.com/a-11-meses-persisten-indignacion-y-esperanza-por-ayotzinapa/)*
