Title: Sin implementación, ambiente va a seguir siendo víctima en el posconflicto
Date: 2017-11-28 16:58
Category: Ambiente, Nacional
Tags: acuerdos de paz, Ambiente, daño ambiental, reforma rural integral, santurbán
Slug: sin-implementacion-ambiente-va-a-seguir-siendo-victima-en-el-posconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ambiente.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Zona Cero] 

###### [28 Nov 2017] 

El daño ambiental en los ecosistemas de páramo, el aumento en la deforestación en diferentes territorios donde antes estaban las FARC, el incremento en pastizales y la poca inversión en producción agrícola, **son algunas de las preocupaciones de los defensores del ambiente** en lo que tiene que ver con el poco avance en materia de creación de normas que aterricen la Reforma Rural Integral.

Así lo manifestó Natalia Parra, que hace parte de la plataforma ALTO por la defensa de los animales y el ambiente. Afirma que, con la firma del Acuerdo de Paz, se pensó que “al no existir el fantasma de la guerra, **el tema ambiental podía escalar en la agenda pública** teniendo en cuenta la nueva relación que se da con el territorio en el posconflicto”.

Sin embargo, la falta de presencia del Estado y el incumplimiento de lo que se pactó en la Habana en materia rural ha demostrado que, “**el Estado no ha tenido la capacidad de cubrir y ejercer sus funciones**, hemos podido ver como en los municipios que vivieron de forma más dramática el conflicto, la deforestación ha aumentado y en lugares como Caquetá hay una hectárea por vaca”. (Le puede interesar: ["Especies en riesgo por decisión de MinAmbiente"](https://archivo.contagioradio.com/ministerio-de-ambiente/))

### **Si no se aplica la Reforma Rural Integral, va a ser difícil defender el ambiente en el posconflicto** 

Parra fue enfática en manifestar que en zonas de páramo como en Sumapaz se ha venido haciendo una campaña por sembrar frailejones para generar agua, pero **“está ingresando un turismo desmesurado** que supera la capacidad de resistencia del ecosistema que es muy frágil”. También indicó que las personas se están llevando los frailejones como regalos para sus familias y “se han visto camionetas 4x4 subidas en el páramo”.

Debido también a las dinámicas del conflicto armado, los defensores del ambiente han identificado que **más de 60 mil familias viven por encima de la línea de páramo** debido a que tuvieron que huir hacia la parte más alta de la montaña y esto afecta la conservación de los ecosistemas.

Por esto, para las organizaciones sociales es muy importante que se establezcan dinámicas para que **al campesino se le garantice nuevos escenarios productivos** y nuevas tierras para vivir. Afirmó que es importante que se empiece a implementar lo establecido en la Reforma Rural Integral que fortalece la producción agrícola en territorios adecuados y se protegen también los ecosistemas. (Le puede interesar: ["Los riesgos para el ambiente en el posconflicto"](https://archivo.contagioradio.com/los-riesgos-ambiente-posconflicto/))

### **Situación del Páramo se Santurbán es una contradicción** 

Parra señaló que la amenaza al páramo de Santurbán es inminente y no hay una concordancia con las afirmaciones del Ministerio de Ambiente y el Ministerio de Minas, “en la discusión interna del mismo gobierno, hemos notado que ciertos conceptos que presenta el Ministerio de Ambiente, **no tienen el mismo peso en la decisiones** cuando se encuentran con un Ministerio de Minas que está ligado a una forma de desarrollo arcaica”.

Desde los sectores ambientalistas y animalistas le han apostado a un Acuerdo de Paz “con la esperanza de que **se reconozca a los animales y al ambiente como víctimas**”. Si bien esto se ha hecho desde los discursos de diferentes instituciones, “no se ha defendido al ambiente desde la práctica y la aplicación y una gran muestra es el interés por intervenir Santurbán con los acuerdos que ha hecho el presidente con los árabes”.

###### Reciba toda la información de Contagio Radio en [[su correo]
