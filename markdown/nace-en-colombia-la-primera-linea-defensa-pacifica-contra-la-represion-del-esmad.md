Title: Nace en Colombia la Primera Línea, defensa pacífica contra la represión del ESMAD
Date: 2019-12-04 16:35
Author: CtgAdm
Category: Entrevistas, Paro Nacional
Tags: Chile, Desmonte del ESMAD, Movilización estudiantil, Paro Nacional
Slug: nace-en-colombia-la-primera-linea-defensa-pacifica-contra-la-represion-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Primera-Línea.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-04-at-2.00.50-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

Ante el homicidio del estudiante Dilan Cruz, cometido por el Escuadrón Móvil Antidisturbios (ESMAD) y los más de 300 casos de personas heridas durante las movilizaciones, dato aportado por la campaña Defender la Libertad;  ciudadanos y estudiantes decidieron crear la Primera Línea, una forma de defensa y resistencia pacífica frente a los ataques desmedidos de la Fuerza Pública en el marco de las movilizaciones que garantice además que los manifestantes puedan movilizarse sin temor a ser reprimidos.

La Primera Línea nace como una forma de emular el modelo creado en Chile donde, producto de la respuesta de la fuerza policial conocida como Pacos o Carabineros, más de 200 personas han perdido sus ojos o han sido heridos en la piel a lo largo de los más de 40 días de manifestación que se han vivido en el país austral. [(Lea también: La unión del pueblo y la persistencia: claves en la movilización de Chile)](https://archivo.contagioradio.com/la-union-del-pueblo-y-la-persistencia-claves-en-la-movilizacion-de-chile/)

Se trata de jóvenes que se ubican en puntos estratégicos de las marchas para impedir que los disparos de municiones de los carabineros y los gases lacrimógenos afecten al resto de la ciudadanía. Nao, uno de los integrantes de esta iniciativa  en Colombia, señala que no apoyan ningún tipo de acción ofensiva ni violenta, "nos encargamos de proteger a las personas pero también buscamos inspirarlas a que no corran cada vez que el ESMAD los ataca", explica.

Nao manifiesta que en medio de las movilizaciones utilizan escudos con los que protegen a los marchantes, de igual forma explica que acompañaran la marcha "bomberos" para apagar los gases y enfermeros para atender a las personas, **"es importante que la gente sepa en la marcha del 4 que la gente puede marchar segura, que pueden marchar tranquilos. Nosotros no vamos a ganar con fuerza, porque no somos militares ni policías, somos estudiantes y ganaremos con inteligencia"**, expresa.

> Hoy la primera línea y la minga indígena está protegiendo la marcha de La Nacional. A ellos les tocó hacer lo que al estado le quedó grande: proteger al pueblo. <https://t.co/KR1WXQeS5l>
>
> — ?????? (@JanderFranco) [December 4, 2019](https://twitter.com/JanderFranco/status/1202272852122189827?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

### Ganarle a la violencia con inteligencia 

El estudiante agrega que buscan que las marchas se desarrollen con más orden e inteligencia y que el rol de la primera línea permita de igual modo tender un puente con quienes promueven la protección de los derechos humanos en la movilización, "el paro tendrá más fuerza si las marchas son pacíficas, si las marchas consiguen sus objetivos, pero también si son seguras y se puede volver a casa después de asistir". [(Lea también:Las razones para pedir el desmonte del ESMAD a 20 años de su creación)](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/)

Añade, además, que el objetivo final del paro "**no es agarrarse con la Policía, ni solo ir a marchar; el objetivo del paro es presionar al Gobierno para que retroceda en políticas nocivas en términos de educación, trabajo, salud y derechos humanos"** por lo que considera esencial y estratégico el que la ciudadanía pueda movilizarse sin tener la zozobra de que pueden ser atacados por la Fuerza Pública; si se promueve esta iniciativa - agrega -, "la tensión que ya existe en la opinión pública" podría cambiar.

"Nosotros no ganaremos al ESMAD con fuerza, además hay una fuerza mediática que también queremos ganar, y lo vamos hacer protegiendo a la gente para que no haya heridos que no haya detenciones arbitrarias. Nosotros no queremos ningún choque con el ESMAD", explica, haciendo énfasis en que ninguno de los cerca de los 70 integrantes de los Escudos Azules está afiliada a ningún tipo de partido político o grupo institucional, "somos solo estudiantes, estamos financiándonos por nuestra cuenta".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45115324" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45115324_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
