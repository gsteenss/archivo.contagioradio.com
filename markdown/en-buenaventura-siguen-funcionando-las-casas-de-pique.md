Title: Las "casas de pique" no han sido desmontadas en Buenaventura
Date: 2015-01-30 18:11
Author: CtgAdm
Category: DDHH, Judicial, Paz
Tags: buenaventura, Casas de pique, Paramilitar
Slug: en-buenaventura-siguen-funcionando-las-casas-de-pique
Status: published

###### foto: contagioradio.com 

##### [Habitante de Buenaventura]<iframe src="http://www.ivoox.com/player_ek_4019981_2_1.html?data=lZWem56cdY6ZmKiak5eJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmMbn1c7a0dPNs4zYxpDVw8fNuMLi1cqYxsqPhtbZz8bjx9PYudPVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Habitantes de Buenaventura denuncian la persistencia de las** "casas de pique"**, lugares en los que grupos paramilitares descuartizan a hombres y mujeres, esto pese a la militarización ordenada en el 2014 por el Ministerio de Defensa. Según una de las habitantes del puerto **podría haber alrededor de 50 casas de pique en Buenaventura**.

A pesar de que el puerto se encuentra militarizado desde la comuna 1 a la 12, la presencia de grupos paramilitares es muy fuerte, y especialmente en los **barrios El Progreso y La Fortaleza, de la Comuna 10**, dónde persisten y funcionan las "casas de pique" señala la habitante. En Buenaventura hace presencia la Armada Nacional, "sin embargo los paramilitares transitan sin que nada pase...**Entre más días pasan, más se incrementan las casas de pique"**

El pasado 2 de enero, tres personas fueron trasladadas a una de las casas de pique, de las cuales dos fueron desmembradas y la tercera persona logró escapar, dirigiéndose al único lugar en el que se sentía seguro: el Espacio Humanitario Puente Nayero, en el que se concentran cerca de 300 familias. Según Nora en Buenaventura "el único espacio seguro es el espacio humanitario" que **también hace parte de la Red de Comunidades CONPAZ.**

La experiencia no es diferente para los jóvenes del territorio, que día a día denuncian los atropellos por parte de la Armada. El 29 de enero, los habitantes del Espacio Humanitario de Buenaventura denunciaron que el joven Luis Alberto Caicedo fue **violentado por el infante de Marina Mejía Gutiérrez**. El control **"no lo hacen con los paramilitares, sino con los muchachos del espacio humanitario".**

Cabe recordar que en el informe anual sobre violación a los Derechos Humanos, **Human Rights Watch alertó sobre la crítica situación del Puerto de Buenaventura**, que actualmente cuenta con la tasa más alta de desplazamiento forzado en Colombia, con **22.383 personas desplazadas entre enero y noviembre del 2014**.
