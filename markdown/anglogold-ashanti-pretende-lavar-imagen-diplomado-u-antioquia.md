Title: AngloGold Ashanti pretende lavar su imagen con diplomado de U. de Antioquia
Date: 2019-03-21 16:05
Category: Ambiente, Nacional
Tags: Anglogold Ashanti, Antioquia, Jericó, La Quebradona, Universidad de Antioquia
Slug: anglogold-ashanti-pretende-lavar-imagen-diplomado-u-antioquia
Status: published

###### [Foto: Melissa Sanchez A.] 

###### [21 Mar 2019] 

La Universidad de Antioquia reanudó un diplomado financiado por la multinacional minera AngloGold Ashanti en Jericó a pesar de que la institución educativa [se había comprometido a suspender este curso](http://%22Suspenden%20diplomado%20financiado%20por%20AngloGold%20Ashanti%20en%20la%20Universidad%20de%20Antioquia). Las comunidades reiteraron su oposición al diplomado pues según ellos, haría parte de una campaña de publicidad en favor del desarrollo de La Quebradona, un proyecto minero de metales en el municipio.

"AngloGold Ashanti está promocionando una cantidad de actividades con el objetivo de obtener la satisfacción de la población con su proyecto minero que **este año va presentar solicitud de licencia ambiental para explotar** una gran mina de oro y cobre en este territorio que jamás ha tenido minería y donde la población se opone rotundamente por los efectos ambientales y sociales que va a causar" afirmó José Fernando Jaramillo, coordinador de la Mesa Ambiental de Jericó.

El diplomado pretende entrenar líderes sociales en temas de desarrollo, autogestión y sostenibilidad, además la Universidad sostiene que preservarían su autonomía en el manejo de las materias. Sin embargo, Jaramillo indica que con esta iniciativa, AngloGold Ashanti realmente está **buscando lavar su imagen frente las comunidades**. "Detrás va a estar la publicidad de AngloGold Ashanti diciendo: Nosotros capacitamos a las comunidades, nosotros apoyamos el desarrollo", afirmó el coordinador. (Le puede interesar: "[Jericó, Antioquia le dice ¡no! a la minería por segunda ocasión](https://archivo.contagioradio.com/jerico-antioquia-le-dice-no-a-la-mineria-por-segunda-ocasion/)")

Cabe recordar que el año pasado el Concejo de Jericó prohibió la actividad de explotación de metales en el territorio a través de un acuerdo municipal, que luego fue respaldado por el Alcalde en enero pasado. A pesar de esto, AnglGold Ashanti continúa operaciones del proyecto La Quebradona, violando el acuerdo municipal y el acto administrativo firmado por el Alcalde. Jaramillo sostiene que la Universidad es bienvenida en Jericó para realizar un diplomado, pero sin la financiamiento de una empresa que pasa por alto de la voluntad del municipio.

El curso inicia este viernes, sin embargo, las comunidades esperan que la Universidad lo suspenda una vez más. "Esperamos que la rectoría entienda nuevamente que aquí en Jericó no queremos este diplomado, que rechazamos las actividades de la empresa minera y que **exigimos que la universidad pública cumpla realmente un papel social**, haciéndose al lado de las comunidades y ayudarlas en la protección del medio ambiente", puntualizó Jaramillo.

<iframe id="audio_33591655" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33591655_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
