Title: 127 heridos y 13 asesinadas en medio de represión  a protestas contra abusos policiales
Date: 2020-09-09 23:04
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Policía #CAI #Bogotá #FuerzaPública
Slug: mas-de-12-personas-heridas-y-dos-personas-asesinadas-en-medio-de-protestas-contra-accionar-de-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-09-at-11.01.54-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Localidades como Suba, Ciudad Bolívar, Bosa y Kennedy reportan hasta el momento 127 personas heridas y 13 personas asesinadas producto del accionar de la Fuerza Pública. Los hechos se registraron durante las protestas en rechazo al asesinato de [Javier Ordóñez, producto del abuso de violencia por parte de dos agentes de la Policía.](https://archivo.contagioradio.com/el-abogado-javier-ordonez-fue-asesinado-por-la-policia-afirman-sus-familiares/)

<!-- /wp:paragraph -->

<!-- wp:heading -->

Localidad Suba
--------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con organizaciones de derechos humanos, sobre las 8 de la noche, en cercanías al CAI de Suba Rincón, integrantes de la Fuerza Pública abrieron fuego contra los manifestantes, hiriendo a una persona. De acuerdo con los informes más recientes de la Policía, h**abría muerto**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el Comité Local de Derechos Humanos-Suba, denunció que la Fuerza Pública estaría agrediendo de forma física y [verbal defensores de derechos humanos y quitatándoles sus celulares.](https://www.facebook.com/contagioradio/videos/773622623388485)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, se reporta otra persona herida en el CAI de La Gaitana. Hasta el momento se reportan cinco personas retenidas.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Localidad de Kennedy
--------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la localidad de Kennedy, organizaciones defensoras de derechos humanos denunciaron las agresiones en contra de **Fabián Silvano Rodríguez,** quien se encontraba en la manifestación del Tintal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El jóven fue herido luego de la intervención de la Fuerza Pública, quién al parecer habría recibido un disparo en la cabeza. Silvano fue trasladado al Hospital de Kennedy y estaría en cuidado intensivos.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Localidad de Ciudad Bolívar
---------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la localidad de Ciudad Bolívar, las organizaciones defensoras de derechos humanos reportan hasta el momento dos personas heridas, ambas producto de impacto de gas lacrimógeno. Una de ellas recibió un impacto en la cabeza, mientras que la otra lo recibió en la mano, provocandole una fractura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, las organizaciones han señalado que hay detenciones por parte de la Fuerza Pública, sin embargo, hasta el momento se desconoce el número de personas que estarían en esta situación.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Localidad de Usaquén
--------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la localidad de Usaquén, en el barrio El Verbenal, se reportan tres personas heridas, identificadas como: Michael Sanchéz, quien recibió un impacto de bala en su pierna; Ferney Peralta, quien recibió un impacto de bala en su brazo izquierdo y Jaison Castillo, de quien se desconoce su estado de salud. Los tres fueron trasladados al hospital Simón Bolívar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Organizaciones defensoras de derechos humanos, también han señalado el asesinato de Ernesto Díaz, **quien habría recibido un impacto de bala en su cabeza.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Localidad de Bosa
-----------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la localidad de Bosa se reporta hasta el momento cinco personas heridas, organizaciones defensoras de derechos humanos están intentando establecer sus identidades y estado de salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta el momento la Fuerza Pública ha manifestado que 18 agentes estarían lesionados. De igual manera, aseguran que se presentan protestas por parte de la ciudadanía en un 90% en los alrededores de los CAI de la ciudad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo

<!-- /wp:paragraph -->
