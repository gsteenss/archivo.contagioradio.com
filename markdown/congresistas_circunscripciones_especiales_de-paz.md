Title: El último respiro que le queda a las 16 circunscripciones especiales de paz
Date: 2017-11-29 17:18
Category: Nacional, Paz
Tags: acuerdo de paz, Circunscripciones de paz, Conpaz
Slug: congresistas_circunscripciones_especiales_de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/victimas-la-chinita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Nov 2017] 

Agoniza la posibilidad de que las más de 8 millones de víctimas del conflicto armado en Colombia tengan alguna representación en el Congreso de la República. Se trata de una situación generada por congresistas de Cambio Radical, el Centro Democrático y los conservadores, como lo asegura el senador Guillermo García Realpe del Partido Liberal.

Frente a esta situación víctimas como Rodrigo Castillo, vocero de las Comunidades Construyendo Paz en los Territorios, CONPAZ, expresa que “**hay una preocupación profunda de parte de las víctimas del conflicto armado,** porque sin elementos de juicio el congreso determina hundir las circunscripciones bajo la excusa de que van a ser coptadas por personas que no sean víctimas”.

### **Circunscripciones como una forma de reparación de las víctimas del conflicto** 

Ante ese panorama, tanto Castillo como el senador García Realpe coinciden en señalar que precisamente esas curules garantizarían la participación política de las víctimas y en esa medida se abriría la posibilidad de que, teniendo representación en el congreso, se puedan generar medidas que transformen la situación actual de las comunidades y así puedan ser reparadas.

“Lo que plantea el acuerdo es que esas zonas han sido víctimas del conflicto y del abandono estatal, y **como mecanismos de reparación a las víctimas se crean esas circunscripciones, p**ero el congreso lo que determina es hundirlas, lo que es una forma de revictimizar a los habitantes de esas zonas”, manifiesta el líder de CONPAZ.

El integrante de la organización de víctimas agrega que dichas curules son un elemento esencial puesto que estas poblaciones nunca han tenido la oportunidad de contar con una representación directa en el congreso, por tanto, contarían con una voz que conozca de primera mano las problemáticas del territorio, y a partir de ese escenario se podría incidir para que se establezcan transformaciones reales en zonas como el pacífico sur.

### **Congresistas estigmatizas a comunidades del pacífico colombiano** 

El congresista denuncia que precisamente sobre las poblaciones del Litoral Pacífico, en el marco de la plenaria del Senado, este martes parlamentarios del **Centro Democrático, Cambio Radical y el Partido Conservador**, hicieron graves señalamientos que además de **revictimizar, estigmatizan a las comunidades** de esa región del país, pues afirmaron que en esas zonas solo hay coca y armas.

“Eso es una clara intención de estigmatizar nuestras comunidades que han dado ejemplo de enfrentar el delito y por eso han muerto muchos líderes que han promovido la sustitución de cultivos de uso ilícito”, indica el senador, quien agrega que los integrantes del partido Conservador y Cambio Radical quieren negar ese derecho alegando que en esas zonas no puede haber elecciones tranquilas, mientras que ellos sí pueden hacerse elegir como **congresistas en esos mismos lugares en  elecciones ordinarias departamentales y nacionales, “y ahí en cambio no tienen ningún problema”,** dice el parlamentario.

Ante tal situación Rodrigo Castillo informa que las víctimas y comunidades del pacífico se reúnen esta tarde para construir un documento conjunto en el que le exigen a los congresistas de Cámara y Senado aprobar las circunscripciones y que de esta manera se respete lo acordado en La Habana.

<iframe src="https://co.ivoox.com/es/player_ek_22358579_2_1.html?data=k5egl52Ze5qhhpywj5WZaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncbPjxdfWydSPh8Ln1c7ZztSJdqSf19TQx9fTb8XZjKi8sLWlno6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="https://co.ivoox.com/es/player_ek_22358629_2_1.html?data=k5egl52adpqhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncajpytHZx9fRs4y7wtfQh6iXaaK4wpC_x8bQtMnZhpewjajTssjmxtjW1dnFb7HV09nWxtSPkMrWxtfOzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
