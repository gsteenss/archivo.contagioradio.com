Title: "No fue una fuga masiva, fue un crimen de Estado"
Date: 2020-03-26 11:01
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: carcel la modelo, carceles de colombia, crisis carcelaria, Crisis carcelaria colombia, Expreso Libertad
Slug: no-fue-una-fuga-masiva-fue-un-crimen-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/unnamed-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego del asesinato de 23 reclusos de la cárcel La Modelo, la ministra de Justicia Cabello, aseguró que este hecho fue producto de una fuga masiva. Afirmación que fue rechazada por el movimiento carcelario quienes expresaron que el accionar del **INPEC**, ese 21 de marzo, provoco un crimen de Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el abogado Uldarico Florez y la ex prisionera política Liliany Obando, las manifestaciones del movimiento carcelario del pasado 21 de marzo, que se presentaron en más de 14 centros de reclusión obedecieron a un cansancio, temor y violación de derechos humanos históricos a los que han sido sometidas las personas privadas de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, exigen que se realicen las procedentes investigaciones frente al accionar desmedido por parte del INPEC, la noche del 21 de marzo que dejo como saldo el asesinato de 23 reclusos y más de 83 heridos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También aseguraron que debe investigarse la responsabilidad del Estado en esta crisis y el conjunto de acciones que no tomaron de forma pertinente para evitar que una situación como esta se presentará.

<!-- /wp:paragraph -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F211207026894419%2F&amp;width=600&amp;show_text=false&amp;height=338&amp;appId" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<!-- wp:paragraph -->

[Vea mas programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
