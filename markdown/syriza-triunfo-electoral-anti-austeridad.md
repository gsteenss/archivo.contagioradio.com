Title: Syriza: Triunfo electoral anti-austeridad
Date: 2015-01-30 12:42
Author: CtgAdm
Category: Cesar, Opinion
Tags: Alexis Tsipras, austeridad en grecia
Slug: syriza-triunfo-electoral-anti-austeridad
Status: published

###### Foto: DichoyHecho 

#### Por: [**[César Torres del Río]**](https://archivo.contagioradio.com/cesar-torres-del-rio/) 

El pasado 25 de enero la coalición político-social griega de izquierda Syriza obtuvo cerca del 37% de los votos y ganó el derecho a formar gobierno; fue un triunfo de la resistencia social y de las jornadas de movilización incluidas las más de 30 huelgas generales y las ocupaciones de las plazas. Alexis Tsipras, su principal dirigente, es ahora primer ministro. En adelante su principal obligación es la de implementar el Programa de Salónica - ciudad y puerto en el mar Egeo - cuyos aspectos principales, acogidos por Syriza como parte de su plataforma política, cubren restablecer el salario mínimo anterior a la crisis, recuperar los 13 salarios para los pensionados, restablecer los convenios colectivos, eliminar los impuestos inmobiliario (Enfia) y sobre la calefacción doméstica. Sobre el tapete también están los asuntos de la renegociación de la deuda y el rechazo a los “Memorandos” de la Troika (Unión Europea, Banco Central y FMI) que impusieron un plan neoliberal de austeridad insoportable.

Un “gobierno de izquierda” fue la consigna que desde el interior de Syriza y los distintos escenarios sociales de confrontación sociopolítica se proclamó a los millones de trabajador@s y sectores sociales: jóvenes, desempleados, jubilados, informales. Y es que en Grecia la confrontación entre las clases se evidenció claramente en el escenario electoral. Antonis Samaras, el primer ministro hasta el 25, fue derrotado; la socialdemocracia pagó su complicidad con las elites financieras, y la derecha y su extrema (Nueva Alborada) has sido desplazadas transitoriamente. En cuanto a los comunistas, fueron castigados por su sectarismo con Syriza aunque mantienen el registro electoral y su identidad como partido.

Dos aspectos hay que poner en relieve: el desplazamiento popular masivo hacia la izquierda (Syriza) y el papel que ocupa Syriza frente a la derecha. Mientras que en la Eurozona es ésta la que obtiene los beneficios económicos por la imposición de planes de ajuste y por la crisis, las ganancias políticas las recibe la coalición de Tsipras. De hecho Syriza se ha basado en la Esperanza anti-austeridad y ello produjo el cambio. Pero se enfrenta a un dilema de hierro: o mantiene al frente a Salónica o cede ante la Troika; y los previos discursos del ahora primer ministro y el del 25 de enero indican ya una tendencia: el 25 habló de diálogo “sincero” y de soluciones “ventajosas” pero nunca de deuda …  
En política se requiere firmeza programática. Y lo que hemos visto a menos de una semana del triunfo es que el gobierno constituido ha escogido por ahora constituir uno de “Unión Nacional” y no uno de “izquierda”. El nombramiento de Panos Kammenos, dirigente del partido nacionalista Griegos Independientes (ANEL), claramente de derecha, como ministro de defensa está indicando que debe haber tranquilidad monetaria y financiera en la Troika, y de paso comunica que la retirada de Grecia de la OTAN no es asunto prioritario en la agenda estatal.

Sí, la izquierda triunfó. Pero la concreción de Salónica únicamente puede residir en la movilización exigente de quienes llevaron a la coalición al poder. Una deriva del nuevo gobierno hacia la Unión Europea o una negociación que retrocediera ante el mandato anti-austeridad sería, además, un golpe que lo debilitaría. Un autogolpe.
