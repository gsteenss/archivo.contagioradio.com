Title: Plan de Golpe de Estado en Venezuela incluía bombardeos y magnicidio
Date: 2015-02-13 17:04
Author: CtgAdm
Category: El mundo, Política
Tags: Democracia en Venezuela, EEUU, Golpe de Estado, Juan Manuel Santos, MUD, Nicolas Maduro, Venezuela
Slug: descubren-plan-de-golpe-de-estado-en-venezuela-que-incluia-bombardeos-y-magnicidio
Status: published

###### Foto: taringa.net 

<iframe src="http://www.ivoox.com/player_ek_4079999_2_1.html?data=lZWkm56dfY6ZmKiak5eJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3Zy8bbxtfTb7TdzdvOjcnJb63VjLfOxs7Tb8XZzZDA19eRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandro Silva. La Radio del Sur] 

El presidente Nicolás Maduro anunció el desmantelamiento de un plan de golpe de Estado, que se estaba planificando desde el 2014 luego de la llamada “Fiesta mexicana” en la que participaron líderes de oposición venezolanos y figuras políticas del mundo. El **plan incluía el uso de un avión Tucano para bombardear objetivos específicos y el asesinato del presidente Nicolás Maduro.**

Según la información entregada por el propio presidente y corroborada con pruebas en mano por Diosdado Cabello, presidente de la Asamblea Nacional, y Jorge Rodríguez, presidente del PSUV, dentro del plan participaron tanto **militares como civiles que ya están detenidos y fueron quienes entregaron la información** detallada sobre los planes y los implicados e implicadas.

https://www.youtube.com/watch?v=fNP670ltwxM

En medio del informe que entrega el presidente, también agradeció a los mandos medios de las FFMM puesto que fueron ellos y ellas los que aplicaron las operaciones de inteligencia necesarias para desmantelar esa fase del golpe de Estado.

Otras informaciones apuntan a que hay algún tipo de relación de los elementos del plan de golpe con Colombia, por lo que Maduro afirmó que está intentando establecer comunicación con el presidente Juan Manuel Santos para discutir sobre **“información muy delicada” que tiene que ver con el territorio colombiano.**

Dentro de las fases del golpe de Estado también está la guerra económica que pretendía que el día del golpe se generaran disturbios en las calles, largas colas en los supermercados y un ataque desde algunos medios de información que estarían encargados de publicar el edicto por el cual se establecía un **gobierno de transición**.

Además el presidente Maduro afirmó que la ciudadanía y los integrantes de las FFMM estaban autorizadas para desplegar operativos cívico-militares **"13 de Abril"** en caso de presentarse un ataque armado en contra de la democracia en ese país, o un magnicidio por parte de los actores golpistas.

\[embed\]https://www.youtube.com/watch?v=0RK\_Vv6TrgE\[/embed\]
