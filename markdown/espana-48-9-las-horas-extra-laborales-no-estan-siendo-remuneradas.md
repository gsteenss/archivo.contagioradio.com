Title: En España el 48.9% de las horas extra laborales no están siendo remuneradas
Date: 2016-11-17 13:01
Category: Economía, El mundo
Tags: españa, Mariano Rajoy, SIndicatos
Slug: espana-48-9-las-horas-extra-laborales-no-estan-siendo-remuneradas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Sindicatos-Protestan-e1479405102395.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ok Diario 

###### [17 Nov 2017] 

Un total de **600.000 asalariados no están recibiendo el pago de las horas extra trabajadas** en España. Así lo evidencian los datos más recientes de la Encuesta de Población Activa (EPA) del tercer trimestre de este año.

Los principales sindicatos de trabajadores de España, la UGT y CCOO, denuncian que esta situación se desarrolla en el marco de la reforma laboral del año 2012, implementada por el gobierno de Mariano Rajoy, reelegido como presidente en días pasados. De acuerdo con ellos, esa **reforma ha facilitado la "explotación laboral" y el no pago de las horas extras.**

La encuesta señala que entre los meses de julio y septiembre se contabilizaron cerca de 5,3 millones de horas adicionales a la semana. Sin embargo, el 48,9% de esas horas, no fue retribuido, por parte de las empresas a los empleados.

Ante esta situación, los sindicatos buscan la manera de que el gobierno derogue la reforma laboral para que puedan **"recuperar los derechos laborales arrebatados a los trabajadores",** como ellos mismo lo han expresado. Y ese precisamente, será uno de los temas que hablarán Rajoy el próximo 24 de noviembre, fecha en la que esperan reunirse con el presidente.

Cabe recordar que pese a las buenas intenciones que mostraba Rajoy antes de ser elegido nuevamente como mandatario español, el día de la investidura, **advirtió que no estaba dispuesto a "destruir lo construido ni a liquidar las reformas",** lo cual viene a ser una negativa rotunda a la derogación de las reformas laborales vigentes, que hoy afectan los derechos de los asalariados, y que benefician únicamente al sector privado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
