Title: Gobierno de Honduras quiere desviar investigación del asesinato de Berta Cáceres
Date: 2016-03-08 12:45
Category: El mundo, Entrevistas
Tags: Berta Cáceres, Copinh, honduras
Slug: gobierno-de-honduras-quiere-desviar-investigacion-del-asesinato-de-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Berta-Ceceres-e1457458669271.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimerica 

<iframe src="http://co.ivoox.com/es/player_ek_10721525_2_1.html?data=kpWklJaZdpahhpywj5aZaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncajjw87S1NPTb8XZjK3c0MnZtsLnjNbiy8rWqYzYxtjjy8bWb8ri18rg1s7LpcTdhqigh6eXsozYxtGYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Tomás Membreño, COPINH] 

###### [8 Mar 2016]

El Consejo Cívico de Organizaciones Populares e Indígenas de Honduras, COPINH denuncia que el **gobierno está intentando desviar la investigación por el asesinato de la líder indígena Berta Cáceres tratando de responsabilizar a su ex esposo y al dirigente campesino de México** que la acompañaba, Gustavo Castro, con el objetivo de absolver la responsabilidad de la empresa que construye el proyecto hidroeléctrico Agua Zarca.

Es por eso, que desde el **COPINH, se ha exigido que el gobierno firme un convenio con la Comisión Interamericana de Derechos Humanos para crear una comisión internacional de investigadores independientes** que trabajen en el esclarecimiento del asesinado de la lideresa, debido a que aseguran que la justicia hondureña no brinda las garantías  para un proceso transparente y fiable.

“Ha habido muchos hostigamientos contra los miembros del COPINH, desde el Ministerio Público quieren que este asesinato quede en la impunidad con el propósito de ensuciar la imagen de la compañera Berta a nivel internacional”, dice Tomás Membreño, integrante de esa organización, y añade que “El Estado se está volviendo cómplice diciendo que era un crimen pasional para justificar el hecho, para que no cargue con esa presión a nivel internacional”.

De acuerdo con Membreño, los responsables están en el mismo congreso hondureño, entre ellos **Juan Hernández, congresista** que avaló la construcción de la hidroeléctrica, también nombra a la **vicepresidenta del Congreso, Gladis Aurora López,** quien tendría responsabilidad frente a las amenazas que le llegaban a Berta. Así mismo, menciona a **David Castillo representante de la empresa DESA y también Rigoberto Cuellar, Fiscal** general del Ministerio Público y quien  ha detenido las demandas que se han interpuesto por parte del COPINH.

Según Tomás, se trata de "una estrategia estatal de falso positivo. **Asesinar a nuestra compañera Berta, meternos presos, asaltar el COPINH y desaparecerlo”.** Para Memebreño, el Estado quiere acabar con su organización porque es una plataforma fortalecida que agrupa más de 200 comunidades que están dispuestas para movilizarse.

Así mismo, el COPINH ha exigió que se cancele de manera inmediata y definitiva las concesiones y operaciones de los proyectos hidroeléctricos, mineros y extractivistas en territorio Lenca, especialmente el proyecto Hidroeléctrico Agua Zarca en Río Blanco desarrollado por la empresa DESA.

Mientras tanto, ante la Comisión Interamericana de Derechos Humanos, este lunes se solicitó a  Honduras medidas cautelares para proteger a la familia de Berta. Por su parte, la Organización de las Naciones Unidas, **ONU, con sede en Perú destacó este lunes el papel de Berta Cáceres como la líder indígena.** "Fue una compañera muy importante, símbolo de nuestra organización, de nuestra Honduras, un símbolo mundial reconocida por su lucha en la defensa del territorio, el ambiente y del COPINH,  y la esperanza de las comunidades indígenas”, expresa Tomás Membreño.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
