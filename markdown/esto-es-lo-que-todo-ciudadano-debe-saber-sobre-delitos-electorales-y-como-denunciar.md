Title: Esto es lo que todo ciudadano debe saber sobre delitos electorales y cómo denunciar
Date: 2015-10-23 12:37
Category: Política
Tags: Consejo Nacional Electoral, Jornada electoral en Colombia 2015, Misión de Observación Electoral, Misión de Observación Electoral Bogotá, Pilas con el voto, Registraduría Nacional
Slug: esto-es-lo-que-todo-ciudadano-debe-saber-sobre-delitos-electorales-y-como-denunciar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Elecciones-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Santa Fe ] 

<iframe src="http://www.ivoox.com/player_ek_9143273_2_1.html?data=mpahlZebd46ZmKiak5iJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo0JDS1ZDQs4zl1sqY1tTIs4zXytrRw8nFstCfxcrPx5DXpcPZ05Dg0cfWqYzYxtHW1tTXb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Aura Rodríguez,  MOE] 

###### [23 Oct 2015 ] 

[El próximo domingo 25 de octubre se llevaran a cabo las elecciones regionales en Colombia y para evitar la comisión de delitos electorales la Misión de Observación Electoral ha dispuesto **3500 observadores a nivel nacional y 600 en Bogotá** quienes harán veeduría y control de esta jornada electoral.   ]

[Aura Rodríguez, encargada de la MOE para Bogotá, asegura que como delitos electorales se entienden todas aquellas **presiones ejercidas por actores de cualquier tipo para obligar a alguien a votar por algún candidato en específico**, éstas deben ser denunciadas ante los observadores electorales presentes en los puestos de votación.]

[Las mesas de votación estarán abiertas desde las 8 de la mañana y hasta las 4 de la tarde, sólo en este horario los votantes podrán ejercer su derecho a elegir, agregó Rodríguez, insistiendo en que los jurados y delegados de la Registraduría debidamente identificados como tal, deben asegurarse de que **para la apertura de las mesas las urnas estén completamente vacías**.  ]

[Así mismo, deberán fijarse en que al momento de votar los ciudadanos puedan hacerlo **libremente, sin ningún tipo de presión y en secreto**. Una vez sean las 4 de la tarde cada jurado debe anunciar en voz alta el número de personas que votaron en la mesa y éste debe corresponder al número de tarjetones que fueron entregados, los sobrantes tienen que ser destruidos antes de que las urnas sean abiertas para proceder con el conteo de los votos.]

[Pese a que el Consejo Nacional Electoral y la Registraduría han avanzado en el control de delitos electorales como la trashumancia, las **empresas criminales** que estarían detrás de estos **no han sido identificadas, denunciadas ni investigadas**, asevera Rodríguez.]

[El uso del mecanismo de **biometría** para la identificación con huella dactilar de cada ciudadano y que permitiría niveles de transparencia más elevados es todavía **marginal en Colombia**, según Rodríguez, la causa principal que han indicado varias autoridades es la falta de recursos económicos.]

[Aura Rodríguez concluye extendiendo la invitación a que cada ciudadano y ciudadana denuncie en [[Pilas con el voto](http://www.pilasconelvoto.com/)] **cualquier irregularidad** en esta jornada electoral. ]
