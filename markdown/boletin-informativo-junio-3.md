Title: Boletín informativo Junio 3
Date: 2015-06-03 19:12
Category: datos
Tags: Noticias del día, Noticias del día en Contagio Radio
Slug: boletin-informativo-junio-3
Status: published

[*Noticias del Día: *]

<iframe src="http://www.ivoox.com/player_ek_4591430_2_1.html?data=lZqmk5mXdI6ZmKiakpyJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZiRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

- El Proyecto de Ley Rosa Elvira Celi, que penaliza el feminicidio con hasta 50 años de prisión, fue aprobado este martes en último debate en plenaria de Cámara de Representantes, ahora solo queda esperar la conciliación en el Congreso y la firma del presidente Juan Manuel Santos para que sea nueva ley de la República de Colombia. Ángela María Robledo, señala los elementos de este ley.

-El próximo 4 de Junio se llevará a cabo una jornada de protección para la Universidad Nacional de Colombia, en reacción a un panfleto recibido el pasado 7 de Mayo en el que Águilas Negras amenazaban de muerte a los profesores Mario Hernández, ex candidato a la rectoría y Leopoldo Múnera, ex vicerrector de la sede de Bogotá, de igual manera hacia la profesora Piedad Ortega de la Universidad Pedagógica Nacional y a un grupo de estudiantes de la facultad de Ciencias Humanas. Habla Itayosara Rojas estudiante de la facultad de Sociología de la Universidad Nacional.

- Desde el año 2008 a la fecha han muerto 5.000 niños y niñas de la comunidad Wayú, en la Guajira, según datos de la Defensoría del Pueblo, citados por el consejero mayor de la Organización Nacional Indígena de Colombia, ONIC, Luis Fernando Arias quien asegura que la desnutrición es la principal causa de la desaparición de las comunidades indígenas en el país.

-Desde las 5:30 A.M. de este miércoles 3 de Junio, cerca de 600 indígenas de la Asociación de Cabildos Indígenas del Valle del Cauca y Región Pacífico ACIVRP, empezaron una movilización de vehículos en la vía que conduce de Buenaventura a Cali, por incumplimientos de la Administración Distrital de Buenaventura en acuerdos pactados previamente. Fernando Chirimia integrante de la Asociación de Cabildos del Valle del Cauca y Región Pacífica.

- La organización Ecologistas en Acción publica un cuaderno explicativo sobre las Zonas libres de Transgénicos en el que trata de despejar algunos mitos en torno a los Organismos Modificados Genéticamente, Habla Gabriela Vásquez, integrante del colectivo.

-Continuan las amenzas contra estudiantes de universidades públicas en el país, el turno ahora es para la Universidad Politecnico colombiano Jaime Isaza Cadavid de Medellín, donde circula un panfleto firmado por autodefensas gaitanistas. Habla Esteban Restrepo, estudiante amenazado.
