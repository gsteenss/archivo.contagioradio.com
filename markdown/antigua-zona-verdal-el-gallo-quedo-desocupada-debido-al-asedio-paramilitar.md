Title: Antigua Zona Veredal el Gallo quedó desocupada debido al asedio paramilitar
Date: 2017-10-03 12:37
Category: Nacional, Paz
Tags: el gallo, éxodo de ex combatientes, implemntación de los acuerdos de paz, paramilitares, zonas de reincorporación
Slug: antigua-zona-verdal-el-gallo-quedo-desocupada-debido-al-asedio-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/zona_veredal-Gallo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal] 

###### [3 Oct 2017] 

Las comunidades de la zona rural de Tierra Alta en el sur de Córdoba denunciaron el éxodo de 120 ciudadanos que se encontraban en proceso de reincorporación en el punto transitorio el Gallo. Dicen que los ex combatientes no tuvieron garantías de seguridad para continuar en el territorio, ahora **los campesinos temen correr la misma suerte.**

Según Andrés Chica, miembro de la Asociación Campesina para el Desarrollo del Alto Sinú, desde el año pasado **las organizaciones han denunciado la poca presencia del Estado** en zonas cerca del espacio de reincorporación. Adicionalmente, han dicho que los paramilitares han tomado el control del territorio y han puesto en riesgo la seguridad de la comunidad.

### **Paramilitares tienen el control y la Fuerza Pública es la única que no los ve** 

Debido a esto, y habiendo denunciado la falta de garantías para su seguridad, los **ex combatientes se fueron** para Urabá, Ituango y la zona entre Mutatá y Chigorodó donde ha estado presente la misión de las Naciones Unidas. (Le puede interesar: ["Implementación de los acuerdos no va por buen camino"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-no-va-por-buen-camino/))

Sin embargo, los campesinos que están en el territorio viven ante el **temor de que sean desalojados, amenazados** y violentados por los paramilitares, que según Chica, “son invisibles para la fuerza pública y para el Gobierno Nacional”. Dijo que “antes cuando estaban los integrantes de las FARC, el Gobierno y las autoridades ponían cuidado en la región, ahora que no están ¿qué va a pasar?”.

### **Paramilitarismo y erradicación forzada son los principales problemas de la comunidad** 

Ante el aumento de la presencia paramilitar, de la represión por cuenta de la erradicación forzada y de la **falta de cumplimiento de lo acordado en la Habana** por parte del Gobierno, la comunidad indicó que seguirán trabajando por defender a las personas y al territorio. (Le puede interesar:["Solo se ha cumplido el 18% de los acuerdos de la Habana"](https://archivo.contagioradio.com/solo-se-ha-cumplido-el-18-de-los-acuerdos-de-la-habana/))

Chica indicó que “las amenazas continúan en el territorio y estamos lejos de que el Gobierno Nacional cumpla por lo que la organización campesina tiene que seguir”. Dijo que la movilización social no pretende exigir que las FARC se queden en el territorio sino que buscan que el acuerdo se cumpla **“rodeando la implementación de un acuerdo que ya se firmó”.**

Finalmente, recordó que **a los campesinos no les han cumplido** con cosas como los Planes de Desarrollo Territorial que se pactaron y por el contrario, han sido víctimas de la represión en lo que tiene que ver con la erradicación forzada.

<iframe id="audio_21238690" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21238690_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
