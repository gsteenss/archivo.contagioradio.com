Title: Cuatro preocupaciones de la Corte Penal Internacional sobre cambios a la JEP
Date: 2018-11-02 08:40
Author: AdminContagio
Category: Nacional, Política
Tags: Corte Penal Internacional, JEP
Slug: cuatro-preocupaciones-corte-penal-internacional-cambios-jep
Status: published

###### Fotografía: @JEP\_Colombia 

###### 2 Nov 2018 

Durante su visita al país, el fiscal adjunto de la Corte Penal Internacional (CPI), James Stewart, expuso las diversas preocupaciones del tribunal frente al juzgamiento especial para miembros de la Fuerza Armada en una sala paralela a las salas de la JEP, un procedimiento que daría un trato especial a las fuerzas militares  y que lejos de contribuir al debido proceso penal, apartaría a los investigados de su responsabilidad.

### **Dilatación de procesos** 

Stewart se refirió con especial énfasis al artículo 75 de la JEP, el cual establece que los procesos en contra de las fuerzas militares comenzarán dentro un de un plazo de 18 meses, una condición que abrió la puerta a que aquellos que aún estuvieran interesados en declarar tuvieran el tiempo de hacerlo.

La creación de estas salas especiales y su posterior funcionamiento - de por sí postergado un año y medio - ocasionarían un mayor retraso en el esclarecimiento de casos que requieren atención inmediata. A los ojos de la Corte Penal Internacional tal dilatación de procesos resulta contraproducente pues derivaría en un retraso de la verdad y reparación a las víctimas, por tanto una mayor impunidad.

### **Exclusión de delitos** 

Una segunda preocupación deriva del segundo párrafo del artículo 11 de las reglas de la JEP que  exime de ser investigados delitos cometidos por agentes del Estado si durante el proceso estos accedieran a revelar información tal como planes criminales,  funcionamiento de las organizaciones criminales, sus redes de apoyo, las características del ataque y los patrones macro criminales.

### **Una sala autónoma y paralela para la Fuerza Armada** 

La propuesta de Acto Legislativo que buscaría crear salas especiales para las fuerzas militares implicaría un sistema independiente al que rige actualmente a la JEP incluyendo una selección autónoma de magistrados que afectarían la imparcialidad del organismo o incluso podría invalidar otras decisiones de otros órganos de la JEP, al tener el poder de ser una segunda instancia.

### **Concesión de beneficios** 

Una cuarta alerta deriva de el posible otorgamiento de  beneficios y conceder la libertad o una reducción de sentencias después de haber cumplido un sexto de la pena a miembros de las fuerzas militares sin estar directamente condicionados a esta jurisdicción ni haber cumplido con medidas como el aporte de toda la verdad y reconocimiento de sus delitos.

A pesar que en el Congreso aún continúa el debate alrededor de estas modificaciones, la Corte Penal Internacional ya ha lanzado una señal de alerta que pone bajo la lupa a la Jurisdicción Especial para la Paz y las acciones del congreso que las modificarían.

###### Reciba toda la información de Contagio Radio en [[su correo]
