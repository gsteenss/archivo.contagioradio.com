Title: "Consultas populares son constitucionales y vinculantes": Dejusticia
Date: 2017-03-27 15:32
Category: Ambiente, Nacional
Tags: Anglo Gold Ashanti, Cajamarca, Mineria
Slug: 38431-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comité Ambiental 

###### [27 Mar 2017] 

[En medio de la euforia de la población de Cajamarca, que tras una evidente votación por el NO expresó su rechazo absoluto a la minería en su territorio, desde el Ministerio de Minas y Energía, ha argumentado que la consulta popular no imposibilita que la empresa Anglo Gold Ashanti pueda explotar oro en ese territorio.]

[**“6100 personas no tiene la capacidad de romper el Estado social de derecho”,** dijo Germán Arce, Ministro de Minas, durante una entrevista esta mañana en Blu Radio. Su explicación es que  la consulta popular es un mecanismo de participación popular con incidencia política, mas no tiene la posibilidad de determinar si el proyecto técnicamente es viable, de tal manera que, **”la consulta no tiene la capacidad de afectar un procedimiento administrativo”.**]

[El ministro asegura que  los títulos de la compañía fueron entregados antes de conocerse la decisión de la comunidad, por lo que será la Agencia Nacional de Licencias Ambientales, ANLA, la que podría determinar si puede o no haber haber minería, de presentarse los proyectos.]

[Frente a estos argumentos por parte del gobierno nacional, César Rodríguez, director de Dejusticia, afirma que “la decisión de Cajamarca se aplica a títulos mineros existentes como **Anglo Gold, porque no tienen licencia ambiental y son meras expectativas**”. Es decir que "las consultas populares son constitucionales, vinculantes y sí tienen efectos sobre concesiones mineras", señala Dejusticia.]

[“La ley establece de forma  clara que si la votación en la consulta supera el umbral (un tercio del censo) la decisión que obtenga la mitad más uno de los votos válidos debe respetarse y materializarse. Si se cumple con esos requisitos, el Concejo Municipal está **obligado está obligado a tomar las medidas que se requieren para hacer efectiva la decisión del pueblo”**, explica Dejusticia.]

[Para Renzo García, vocero del Comité Ambiental del Tolima, el ministro está allanando el camino “para que se desconozca la democracia y la voluntad popular”. Según él, ese 98% del pueblo que dijo NO a la minería, no puede ser desacatado por el gobierno, teniendo en cuenta leyes como la 134 de 1994 y la 1757 de 2015, ratifican que las consultas populares son vinculantes.]

[En redes sociales, muchos colombianos se pronuncian este lunes exigiéndole al gobierno que acate la decisión del pueblo cajamarcuno, con \#CajamarcaSeRespeta, mientras los pobladores continúan celebrando el haber logrado defender la vocación agrícola de su territorio, que trabajó en las]**42 veredas para socializar y explicar por qué es dañino para el ambiente el proyecto La Colosa.**

[“Demostramos que lo último que se roban en este país es la esperanza, en medio del chantaje de la empresa, defendimos la vocación agropecuaria, defendimos el agua, el derecho a un ambiente sano”. Lo que viene ahora es que el concejo municipal asuma la decisión del pueblo, de no hacerlo el alcalde será el que deba adoptar vía decreto municipal la posición de los pobladores contra la minería. [(Le puede interesar: Cajamarca dijo NO a la minería)](https://archivo.contagioradio.com/si-se-pudo-pueblo-de-cajamarca/)]

<iframe id="audio_17793040" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17793040_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
