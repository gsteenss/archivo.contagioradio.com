Title: Indígenas buscan acuerdos sobre el proyecto Visión Amazonía 2020
Date: 2017-04-28 15:34
Category: Nacional
Tags: amazonas, CRIMA, vision amazonia
Slug: indigenas-buscan-acuerdos-sobre-el-proyecto-vision-amazonia-2020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/amazonas-e1478294687660.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo particular] 

###### [28 Abr. 2017] 

Una delegación del Consejo Regional Indígena del Medio Amazonas (CRIMA) llegó a Bogotá, para** realizar una serie de denuncias frente al proyecto Visión Amazonía 2020,** que busca reducir a cero la deforestación en la Amazonía colombiana para el año 2020, pero que afectaría culturalmente a las comunidades que allí habitan.

El CRIMA busca **llegar a unos acuerdos** sobre esta temática con el Gobierno Nacional, la Agencia Nacional de Tierras y las Embajadas que financian el proyecto, para poder establecer un proceso de consulta previa, en donde se decida si el proyecto afecta o no "la relación de los indígenas con la naturaleza y su ancestralidad".

Hernando Castro, integrante del CRIMA, afirmó que a ellos les afecta culturalmente porque el proyecto compromete** todos los sitios sagrados y los lugares de origen.** Asimismo, denuncia que **los procedimientos no están bien enfocados y se están tergiversando**, debido a que los bosques de los resguardos están siendo afectados, sin haber realizado una consulta previa con las comunidades. Le puede interesar:[El 12% de la Amazonía colombiana ha sido transformada.](https://archivo.contagioradio.com/12-la-amazonia-colombiana-ha-transformada/)

Por otro lado, según el Ministerio de Ambiente, Visión Amazonía “busca promover un nuevo modelo de desarrollo en la región **que permita mejorar las condiciones de vida de las poblaciones locales a la vez que mantiene la base natural** que sostiene la inmensa biodiversidad de la región y que sustenta la productividad de la región”. Le puede interesar: [El 17% de la selva amazónica ha sido destruida.](https://archivo.contagioradio.com/el-17-de-la-selva-amazonica-ha-sido-destruida/)

Respecto a esto, Castro afirmó que el Gobierno ha justificado su accionar, en cuanto a los procedimientos, con la ejecución de talleres informativos y de socialización, "como si estos procesos se pudieran considerar de consulta"[.]

### **Resultados parciales del encuentro con las instituciones** 

Castro señaló que, durante el encuentro en Bogotá, **con el Ministerio de Ambiente no se logró ningún acuerdo ni avance**, a pesar de que los delegados expusieron que el gobierno estaba ofreciendo estos espacios, sin haber realizado una consulta, teniendo en cuenta que éstos espacios son propiedad "privada y colectiva" de los indígenas cuando se constituyen los resguardos. Le puede interesar: [Indígenas de Putumayo ganan primera batalla contra proyecto petrolero](https://archivo.contagioradio.com/indigenas-de-putumayo-ganan-primera-batalla-contra-proyecto-petrolero/)

Entre la Agencia Nacional de Tierras** **y la Delegación del CRIMA, según Castro, se establecieron acuerdos en pro de tres resguardos, asunto que se había venido solicitando desde hace más de 10 años y no se habían obtenido avances. Asimismo, se espera seguir trabajando en declarar **territorios bajo el decreto 2333 para que se protejan, mientras se expide el procedimiento legal de titulación de tierras.**

En las reuniones sostenidas con las embajadas de Noruega, Reino Unido y Alemania, de acuerdo con Castro, **se habló de cómo éstas están perjudicando a las comunidades indígenas con el apoyo financiero que están haciendo a Visión Amazonía 2020**, sin consultar los procedimientos a nivel interno con los pueblos indígenas.

De igual forma, Castro afirmó que las Embajadas se comprometieron a reorientar y replantear los procedimientos que no **quedaron claros en la participación y en la construcción de los mega proyectos. **

<iframe id="audio_18401065" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18401065_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **¿Se necesita o no una consulta previa?** 

Julio Cesar López, presidente de la Organización Nacional de los Pueblos Indígenas de la Amazonia Colombiana (OPIAC), asegura que desde el 2012 se tomó la decisión de que **Visión Amazonia no necesitaba de una consulta previa en la Mesa Regional Amazónica,** que es la instancia de concertación del Gobierno con las comunidades.

Ante esto, Castro afirma que **“las leyes de Colombia dicen que las consultas se hacen a los pueblos, no a las organizaciones nacionales”.** Por lo cual, el vocero del CRIMA, sostiene que la Mesa Regional es un espacio de concertación, de coordinación y recomendación al Gobierno Nacional, pero no es un espacio de consulta, donde los dirigentes no son los legítimos para tomar esa decisión.

Se está a la espera de los resultados de las próximas reuniones programadas para seguir concretando puntos de comprensión y acuerdos sobre el proceso de la implementación del proyecto Visión Amazonía 2020, **en búsqueda de que los derechos de los pueblos ancestrales de la región no sean vulnerados.**

[Infografía Visión Amazonía 2020](https://www.scribd.com/document/346694775/Infografia-Vision-Amazonia-2020#from_embed "View Infografía Visión Amazonía 2020 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_55746" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/346694775/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-FvRSAvENJlhfz05CAfkn&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7136075949367089"></iframe>

###### Tomado de: DEDISE. Colectivo Derechos, Diversidad y Selva 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="ssba ssba-wrap">

</div>
