Title: Pese a la estigmatización avanzó la 'Peregrinación por el amor eficaz'
Date: 2016-02-15 12:01
Category: DDHH, Nacional
Tags: 50 años Camilo Torres, Conmemoración Camilo Torres, peregrinación por el amor eficaz
Slug: pese-a-la-estigmatizacion-avanzo-la-peregrinacion-por-el-amor-eficaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Peregrinación-Camilo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### <iframe src="http://www.ivoox.com/player_ek_10443537_2_1.html?data=kpWhlpiZd5ihhpywj5aWaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncbHZ1MqYw5DQpYzZ1NnWydLFuMruwsjWh6iXaaOnz5DO2MbSvoa3lIqvlZDQpYyZk5y9x9fJq9Pdz8bQy4qnd4a2lNOY0pKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Marylen Serna] 

###### [15 Feb 2016.]

Pese a la [[estigmatización, persecución y amenazas](https://archivo.contagioradio.com/denuncian-persecusion-y-estigmatizacion-a-peregrinacion-por-el-amor-eficaz/)] contra organizaciones sociales y líderes promotores de la ‘Peregrinación por el amor eficaz’, este fin de semana en Bucaramanga, Barrancabermeja y El Carmen de Chucurí se llevaron a cabo las actividades académicas y culturales programadas con el fin de [[rendir homenaje a la figura y memoria del sacerdote y sociólogo](https://archivo.contagioradio.com/camilo-torres-ejemplo-de-lucha-para-latinoamerica/)]Camilo Torres Restrepo.

De acuerdo con Marylen Serna, vocera del 'Congreso de los Pueblos', cerca de mil personas provenientes de distintas regiones del país y de diversas apuestas políticas y organizativas, se reunieron para conmemorar y reflexionar la **vigencia de la propuesta de transformación y unidad social** promovida por este religioso, "caminando por dónde él camino los últimos meses de su vida", pero haciendo **énfasis en su labor como profesor, sacerdote, sociólogo y luchador social**, y "no solamente en su opción por la lucha armada".

Según afirma Serna la estigmatización a la conmemoración incluyó visitas de autoridades militares y policiales a la familia que reside en el lugar en el que el sacerdote murió, con el fin de **obligarla a no vender el terreno solicitado con anterioridad para la instalación de un monumento** en honor a Camilo Torres. Por cuenta de las presiones, la familia teme por su vida, asegura la vocera, quien recuerda que en último [[Consejo de Seguridad](https://archivo.contagioradio.com/en-medio-de-estigmatizaciones-inicia-homenaje-a-camilo-torres/)] del Carmen de Chucurí se señaló que el acto era orquestado por miembros del ELN, en **desconocimiento de la trayectoria social e investigativa de quienes organizaron el espacio**.

Durante el recorrido de Barrancabermeja hasta El Carmen de Chucurí, las delegaciones se percataron de que se habían colocado cámaras en distintos lugares para registrar a quienes se movilizaban, asevera Marylen y agrega que el ambiente estuvo "supremamente pesado", por cuenta de los civiles que se unieron a la caravana, las **tachuelas que fueron botadas a la carretera y el bloqueo del ESMAD que impidió que la delegación avanzara a Patio Cemento** y llevó a que el acto simbólico se realizara en este punto de la peregrinación.

La vocera concluye afirmando que los señalamientos contra la jornada conmemorativa se relacionan con el **control que ejercen en la región los grupos paramilitares y las empresas multinacionales**, e insiste en que este acto político de reivindicación no fue programado "con la intención de generar confrontación ni riesgos sobre la comunidad", y califica de exitoso el **haber logrado hacer memoria del legado de Camilo  Torres**.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u) ]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
