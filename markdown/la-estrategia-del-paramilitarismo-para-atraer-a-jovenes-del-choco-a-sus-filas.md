Title: La estrategia del paramilitarismo para atraer a jóvenes del Chocó a sus filas
Date: 2019-05-13 17:12
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, cacarica, Chocó, Control Paramilitar
Slug: la-estrategia-del-paramilitarismo-para-atraer-a-jovenes-del-choco-a-sus-filas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Paramilitares....png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

A través de sus testimonios, jóvenes de la **Zona Humanitaria Nueva Vida en Cacarica, Chocó** denuncian cómo las Autodefensas Gaitanistas de Colombia (AGC), facción del paramilitarismo que hace presencia en la región, toma el control de las comunidades y busca constantemente la forma de seducir y ofrecer dinero a jovenes para que se unan a sus filas generando zozobra al interior de la comunidad.

Los relatos, enviados originalmente para que fueran escuchados por la **Comisión Interamericana de Derechos Humanos (CIDH)** revelan cómo los jóvenes son perseguidos por los grupos paramilitares, una situación que lleva a la mayoría de los integrantes más jóvenes de Nueva Vida a guardar silencio frente a lo ocurrido.

### Paramilitarismo afecta el tejido social 

"La comunidad ya no es la misma, muchos han decidido abandonar el territorio por miedo a la presencia del sector armado", agrega un joven, señalando cómo este temor ha ocasionado que la comunidad se encuentre dividida pues aquellos que denuncian no cuentan con el respaldo del resto de la comunidad como consecuencia del temor.

<iframe src="https://co.ivoox.com/es/player_ek_35761439_2_1.html?data=lJqkmJaYd5qhhpywj5WXaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncbXZ1NnWz9TSrdCfkpDX0dvJsozYxpDZw5DHs87pz87Rw8mPqc-fpMbQw9fNp8KZk6iYpc3Tp4a3lIqvlZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

"Muchas de nuestras jóvenes han sido seducidas por estos personajes, no queremos eso para nuestra juventud" expresa con preocupación una de las habitantes de la zona quien agrega que los líderes y lideresas de la zona han sido amenazados por defender los derechos de la población por lo que existe un miedo generalizado al interior de la comunidad. [(Lea también: CIDH estudia aumento de la violencia en el Chocó) ](https://archivo.contagioradio.com/cidh-estudia-aumento-de-la-violencia-en-el-choco/)

<iframe src="https://co.ivoox.com/es/player_ek_35762023_2_1.html?data=lJqkmJeUdpShhpywj5WXaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncbXZ1NnWz9TSrdCfk5DX0dvJsoy3wsjO1M7HpYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De este modo, los testimonios apelan a las diferentes organizaciones sociales y defensoras de derechos humanos para que se pronuncien y actúen ante el control del paramilitarismo que hoy, cerca del 70% de las comunidades del Chocó, son víctima. [(Le puede interesar: Alerta por fuertes operaciones Neoparamilitares en territorios de Curvaradó y Jiguamiandó)](https://archivo.contagioradio.com/operaciones-neoparamilitares-curvarado-jiguamiando/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
