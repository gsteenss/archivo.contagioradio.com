Title: Anglo Gold Ashanti desconoce acuerdo municipal que prohíbe la minería
Date: 2019-05-14 15:59
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Ambiente, Anglo Gold Ashanti, Explotación, Jericó, Mineria
Slug: anglo-gold-ashanti-desconoce-acuerdo-municipal-que-prohibe-la-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Anglo-Gold-Ashanti-violanco-acuerdoo-municipal-de-Jericó.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CorpoJuridicaLi] 

El pasado lunes 13 de mayo **empleados de Anglo Gold Ashanti**, en conjunto con miembros de la **Fuerza Pública** intentaron instalar plataformas de exploración minera en la vereda Vallecitos, del municipio de Jericó (Antioquia). Con esta acción, la empresa quiso pasar por encima del **Acuerdo municipal 10 de 2018**, que prohibía la explotación minera en el territorio. (Le puede interesar: ["Jericó, Antioquia le dice ¡No! a la minería por segunda ocasión"](https://archivo.contagioradio.com/jerico-antioquia-le-dice-no-a-la-mineria-por-segunda-ocasion/))

Fernando Jaramillo, coordinador de la Mesa Ambiental de Jericó, relató que los campesinos de la zona fueron advertidos sobre la presencia de la multinacional junto a efectivos de la Policía, el Ejército y el Escuadrón Móvil Anti Disturbios (ESMAD); fue entonces cuando los habitantes pidieron el acompañamiento del Alcalde de Jericó, quien solicitó a la empresa no violar el Acuerdo pactado en diciembre de 2018 y detener cualquier intento de explotación minera. Tras la intervención, la empresa aceptó detener sus actividades de exploración.

Sin embargo, como lo recuerda Jaramillo, **desde la fecha de aprobación del Acuerdo la empresa ha realizado distintas actividades "tendientes a terminar el estudio de impacto ambiental para pedir la licencia de exploración"** mediante la instalación de plataformas; pero siempre la respuesta campesina lo ha evitado. De acuerdo al ambientalista, esto ha sido así porque "esta multinacional, como otras, sabe que cuenta con el respaldo del Gobierno nacional, que favorece su presencia así se hayan rechazado sus actividades".

> Desde Jericó nos informan: MENSAJE Urgente. En este momento están llegando a la vereda Vallecitos fuerzas de la policía y a Palocabildo el Esmad. Igualmente, los campesinos con sus familias empiezan a llegar. Desconociendo acuerdo municipal que impide actividades mineras [pic.twitter.com/RB1XwjNrE7](https://t.co/RB1XwjNrE7)
>
> — CENSAT Agua Viva (@CensatAguaViva) [13 de mayo de 2019](https://twitter.com/CensatAguaViva/status/1127988960331030529?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Las multinacionales tienen respaldo del ejecutivo, ¿y del judicial?** 

Jaramillo recordó que el Acuerdo de diciembre es el segundo presentado y aprobado en Jericó, luego de que **en 2017 se aprobaran 12 acuerdos municipales que prohibían la minería y uno de ellos (el de Urrao) fuera reconocido por el Consejo de Estado**. A pesar del reconocimiento, el Tribunal Administrativo de Antioquia se ha negado a pronunciarse en su favor, situación que ha sido aprovechada por la empresa para cuestionar la legitimidad de los mismos. (Le puede interesar: ["Alcalde de Jericó exige a Anglo Gold suspender actividad minera"](https://archivo.contagioradio.com/alcalde-jerico-exige-anglogold-ashanti-suspender-actividad-minera/))

De hecho, el Coordinador de la Mesa Ambiental señaló que Anglo Gold Ashanti se comprometió a suspender sus actividades hasta que el Tribunal se haya pronunciado sobre el Acuerdo, y añadió que esperaban presiones hacía el tribunal para rechazar la medida. Pese a ello, sostuvo que el movimiento ambiental ya emprendió acciones legales ante la Fiscalía por el desconocimiento de la resolución municipal, están estudiando la incidencia en el esquema de ordenamiento territorial y seguirán en el proceso de movilización y unión del suroeste antioqueño en favor del ambiente.

> Conozca en este hilo los impactos a la montaña "Mama de Agua" que piensa explotar la multinacional sudafricana Anglo Gold Ashanti en [\#Jericó](https://twitter.com/hashtag/Jeric%C3%B3?src=hash&ref_src=twsrc%5Etfw) con impacto en todo el suroeste antioqueño, según art de El Colombiano. [@luisyepesb](https://twitter.com/luisyepesb?ref_src=twsrc%5Etfw) [@jgmarin1](https://twitter.com/jgmarin1?ref_src=twsrc%5Etfw) [@wilcheschaux](https://twitter.com/wilcheschaux?ref_src=twsrc%5Etfw) [@morrodeagua](https://twitter.com/morrodeagua?ref_src=twsrc%5Etfw) [@manuel\_rodb](https://twitter.com/manuel_rodb?ref_src=twsrc%5Etfw) [pic.twitter.com/4jmqK5SFs8](https://t.co/4jmqK5SFs8)
>
> — Lina María Velásquez (@Linavelasqueze) [13 de mayo de 2019](https://twitter.com/Linavelasqueze/status/1127911458388545538?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_35829352" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35829352_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
