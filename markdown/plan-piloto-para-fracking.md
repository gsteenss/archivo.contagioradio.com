Title: Plan piloto de fracking "es jugar con el ambiente y la salud de las comunidades"
Date: 2019-09-19 15:13
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Alianza Colombia Libre de Fracking, Fracking colombia, Magdalena Medio, pruebas piloto
Slug: plan-piloto-para-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/impactos-del-fracking-colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El Consejo de Estado anunció que la decisión de mantener la suspensión provisional de las normas que regulan la explotación de petroleo no impide la realización de pruebas piloto, información que por un lado, generó el pronunciamiento de grupos ambientalistas como Colombia Libre de Fracking, que señalaron que **“esta acción “piloto” es jugar con el ambiente y con la salud de las comunidades” **y por el otro la insistencia de la Ministra de Minas y Energía, María Fernanda Suárez que insistió en que "decirle no a los planes piloto de fracking es decirle no la ciencia".

### ¿Cómo serán estas pruebas piloto para el fracking? 

**Los planes piloto se darán inicialmente en el Magdalena Medio**, pero no se descartan exploraciones en la Cuenca del Cesar, Catatumbo, en Amagá Antioquia y en  Putumayo, donde se investigará la explotación de mantos de carbón y gas, adicionalmente, en la mira también está la Cordillera Oriental y municipios de la Sabana Boyacense, en los  límites con Santander, zona que aún no ha sido explotado. (Le puede interesar: [Debe suspenderse todo tipo de actividad relacionada al desarrollo de fracking en el país, no solo en Santander](https://archivo.contagioradio.com/debe-suspenderse-todo-tipo-de-actividad-relacionada-al-desarrollo-de-fracking-en-el-pais-no-solo-en-santander/))

**Este piloto consiste en realizar la actividad  de perforar verticalmente**, llegar a unos 3 km de dimisión, y luego perforar para inyectar agua con químico y así fragmentar la tierra. Oscar Sampayo,  integrante de la Alianza Colombia Libre de Fracking afirmó que es un experimento donde se va a jugar con la naturaleza y las comunidades, "y esto es lo que nos parece ilógico cuando en el mundo se han hecho múltiples investigaciones que demuestran que esto genera un daño terrible a la tierra”.

**Este  plan  piloto puede tardar de un año a  dos en todo el proceso de monitoreo, y en dentro de tres a cuatro años, se podrán conocer los resultados de las explotaciones**, "los daños se reflejan en los territorios desde que se encienda la primera maquinaria", señaló Sampayo. (Le puede interesar:[La lucha contra el fracking, la defensa de la vida](https://archivo.contagioradio.com/lucha-contra-fracking-defensa-vida/))

La posibilidad de hacer los proyectos piloto integrales de investigación no significa que el fracking haya quedado autorizado, de hecho, según Sampayo, pueden tardar de uno a dos años en iniciar las investigaciones, “el Consejo de Estado dice acatar una normativa de la Comisión  de expertos, y nosotros nos preguntamos si también acatará los otros ocho puntos  que la Comisión dijo que se tenían que realizar previo a los planes piloto”, de los cuales uno de los que más se exige , es  la **construcción de una línea base económica que incluya un licenciamiento social, que implique la participación de las comunidades evidenciando en donde se harán los planes,  para ver si aceptan o no los pilotos.**

> “queremos que en esta decisión se tengan en cuenta las voces de las comunidades, y no sea la imposición de unos dos o tres, tecnócratas y empresarios interesados en la explotación de estos territorios” - Sampayo

<iframe id="audio_41721496" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41721496_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
