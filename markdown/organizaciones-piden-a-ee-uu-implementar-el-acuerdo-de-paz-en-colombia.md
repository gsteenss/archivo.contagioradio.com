Title: Organizaciones piden a EE.UU implementar el acuerdo de paz en Colombia
Date: 2020-07-23 14:08
Author: PracticasCR
Category: Actualidad, Nacional
Tags: acuerdo de paz, Gobierno de EE.UU.
Slug: organizaciones-piden-a-ee-uu-implementar-el-acuerdo-de-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdos-de-paz-e1506448818344.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Laprensa.hn

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Seguido de las enmiendas presentadas en la Cámara de Representantes en Washington para condicionar la ayuda militar Norteamérica a Colombia, este 23 de julio, un grupo de organizaciones de los Estados Unidos y Colombia publicaron un informe titulado **Protejan la paz en Colombia** para instar al Gobierno de EE.UU a implementar el acuerdo de paz como un mensaje diplomático para Colombia. (Le puede interesar: [Congreso de EEUU condiciona ayuda militar a Colombia](https://archivo.contagioradio.com/congreso-de-eeuu-condiciona-ayuda-militar-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las organizaciones un indicador de la falta de compromiso por parte del gobierno de EEUU ha sido que después de la firma del acuerdo de paz en el 2016, más de 700 líderes y defensores de derechos humanos, así como 218 personas en proceso de reincorporación, según cifras de INDEPAZ, han sido asesinados en diferentes partes del país y **las acciones por parte del gobierno Duque han sido prácticamente nulas. **

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “...las acciones del Gobierno han sido insuficientes y no han protegido a las personas que arriesgaron sus vidas por la paz”.
>
> <cite>Lisa Haugaard, co-directora del Grupo de Trabajo sobre Asuntos Latinoamericanos (LAWG).</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Esta iniciativa, hace exigencias tales como que se **haga justicia en nombre de las víctimas del conflicto armado y que se proteja a los defensores de los derechos humanos poniendo fin a los abusos de las fuerzas armadas colombianas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, **piden implementar el capítulo étnico que se encuentra en el acuerdo**, pues según Ariel Palacios, representante del Consejo Nacional de Paz Afrocolombiano (CONPA), está paralizado en un 7% de su implementación, lo que ha contribuido a que el conflicto armado que está atentando contra las vidas de indígenas y afrocolombianos, crezca.   

<!-- /wp:paragraph -->

<!-- wp:heading {"textColor":"very-dark-gray"} -->

¿Y el plan de desmantelamiento de las estructuras paramilitares?  {#y-el-plan-de-desmantelamiento-de-las-estructuras-paramilitares .has-very-dark-gray-color .has-text-color}
-----------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Alberto Yepes, coordinador del Observatorio de Derechos Humanos y Derecho Humanitario de Coordinación Colombia  Europa  Estados Unidos (CCEEU) también indica que **ya que EE.UU no ha enviado un mensaje contundente y fuerte con respecto al desmonte del paramilitarismo, el gobierno de Colombia sigue considerando que  “el empleo de paramilitares en el conflicto interno sigue siendo útil** y por eso no han permitido que Comisión de Garantías de Seguridad haya implementado el Plan de desmantelamiento de estos grupos”.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que la Comisión de Garantías de Seguridad tiene como función hacer seguimiento de la política pública y criminal con respecto al desmantelamiento de organizaciones criminales que cometan homicidios y masacres atentando contra líderes, defensores derechos humanos y firmantes de la paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, Yepes agregó que reclamar el cumplimiento de este compromiso podría dar paso a la consolidación de la paz e implementar acciones para proteger a lideres y defensores de derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este informe es visto como de gran importancia, dado el nivel y alcance de las organizaciones y personas que lo elaboran, ente ellas Cristina Espinel y Julio Idrobo, Colombia Human Rights Committee, Washington, DC; Angel Patiño, Colombia Grassroots Support, New Jersey; Julio Bedoya, Movimiento por la Paz en Colombia, New York; y Elizabeth Castañeda, Arraigo, New York.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para leer el informe completo en inglés y español [AQUÍ](https://www.lawg.org/protect-colombias-peace/#)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
