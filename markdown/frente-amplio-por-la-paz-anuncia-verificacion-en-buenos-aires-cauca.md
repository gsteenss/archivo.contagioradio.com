Title: Frente Amplio por la Paz realiza verificación en Buenos Aires Cauca
Date: 2015-04-15 19:22
Author: CtgAdm
Category: Nacional, Paz
Tags: Aida Avella, Angela Maria Robledo, cese bilateral del fuego, cese unilateral de las FARC, Conversacioines de paz en Colombia, David Florez, FARC, Frente Amplio por la PAz, Iván Cepeda, Juan Manuel Santos, Lilia Solano, piedad cordoba
Slug: frente-amplio-por-la-paz-anuncia-verificacion-en-buenos-aires-cauca
Status: published

###### foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4360124_2_1.html?data=lZijkpaWeI6ZmKiak5WJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitPZz9nSjabRtM3d0JDd0dePsMKfscbnjcbSuc%2FXysaY2MrWrcfdxMbQy4qnd4a2lNOYx9OPhtbZz9Sah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rueda de Prensa Frente Amplio por la Paz] 

En una rueda de prensa ofrecida en la tarde de este miércoles en Bogotá, el **Frente Amplio por la Paz** anunció que el próximo jueves realizará una **visita de verificación en Buenos Aires, Cauca,** para establecer los hechos en los que murieron 11 integrantes de las FFMM y resultaron heridos 16.

Igualmente hicieron un llamado al gobierno nacional a **mantener la decisión de suspender los bombardeos** y no reanudarlos como anunció hoy el gobierno nacional. De la misma manera llamaron a la sociedad en general a r**espaldar las conversaciones de paz que se realizan en la Habana**.

La delegación del Frente Amplio estará integrada por Piedad Córdoba, Aida Avella, Iván Cepeda, Lilia Solano, David Flórez y se espera la confirmación de la presencia de Ángela María Robledo. Se espera que en la noche de este **jueves se tenga ya un informe de la situación que se presentó puesto que “todos los hechos no están esclarecidos aún”** afirma Aida Avella.

El Frente Amplio por la Paz es una de las organizaciones de la sociedad colombiana que está haciendo un seguimiento al **cese unilateral de fuego desde el pasado 20 de Diciembre** de 2014 y ha realizado tres informes en los que se ha dado cuenta de los beneficios de la decisión de las **FARC**. Desde el inicio de las conversaciones de paz han insistido en la necesidad de un **Cese Bilateral de Fuego** que brinde las condiciones para las conversaciones de paz y ambiente a la sociedad colombiana para respaldar y trabajar en la construcción de la paz con Justicia Social.
