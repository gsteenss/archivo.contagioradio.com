Title: Gobierno pone 'pico y placa' a visitas internacionales en los ETCR
Date: 2019-06-26 18:43
Author: CtgAdm
Category: Paz, Política
Tags: Cancillería, Comunidad Internacional, Embajadas, etcr
Slug: gobierno-pico-placa-visitas-etcr
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Gobierno-restringe-acceso-a-ETCR.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: @CarlosHolmesTru  
] 

Recientemente se hicieron públicos documentos de la Cancillería en los que se exige a las embajadas que quieran visitar los **Espacios Transitorios de Capacitación y Reincorporación (ETCR)** que presenten una solicitud con "por lo menos tres semanas de anticipación" ante el Ministerio de Relaciones Exteriores. Adicionalmente, se pide que la agenda de trabajo se construya en conjunto con representantes del Gobierno; y **se limitan las visitas a la segunda y cuarta semana del mes, entre miércoles y viernes.**

### **¿Por qué la Cancillería limita así las visitas internacionales a los ETCR?**

**El senador Iván Cepeda** manifestó que el hecho se podía interpretar como una censura de la Cancillería a la comunidad internacional para "visitar y verificar lo que ocurre en los ETCR", generando una política del silencio. El Senador recordó la denuncia recientemente publicada por la revista Semana según la cual, se estaría persiguiendo a militares que han declarado ante la Jurisdicción Especial para la Paz (JEP). (Le puede interesar: ["Crece la expectativa ante reubicación de 11 Espacios Territoriales de Capacitación y Reincorporación"](https://archivo.contagioradio.com/crece-la-expectativa-ante-reubicacion-de-11-espacios-territoriales-de-capacitacion-y-reincorporacion/))

En esa medida, Cepeda afirmó que el "Gobierno está intentando que no haya ojos y oidos para registar hechos que tengan relación con violaciones a los derechos humanos y cómo se está tratando el proceso de paz". Por su parte, **monseñor Darío Monsalve,** sostuvo que esta acción estaba dañando la relación con la comunidad internacional, por estas "actitudes sospechosas". (Le puede interesar: ["Actitud del Gobierno de Colombia es un deterioro de la diplomacia"](https://archivo.contagioradio.com/deterioro-de-diplomacia/))

### **"No se entiende el calculo que el Gobierno ha hecho, cuál es el beneficio"**

**Gustavo Gallón, abogado y director de la Corporación Colombiana de Juristas (CCJ),** expresó que no entendía el calculo que el Gobierno estaba haciendo al tomar esta medida porque no se entiende su beneficio; él recordó que la relación de las embajadas con los Espacios ha sido de aportar económica y políticamente al proceso de paz, en cambio, recibir estas restricciones podría generar un malestar en esa comunidad internacional que sigue apoyando el proceso mediante el envío de recursos económicos.

Por su parte Monseñor Darío Monsalve calificó la medida como un "pico y placa de la comunidad internacional", tratando de hacer el símil con la medida que evita congestiones vehiculares mediante la restricción a la circulación de automóviles; no obstante, tomando en cuenta el nivel de implementación del proceso de paz y el riesgo para la vida de excombatientes, no parece un problema que actores internacionales asistan a los ETCR para enterarse de primera mano sobre lo que allí ocurre.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
