Title: Indígenas Sikuani se oponen a ocupamiento de predios por empresa Poligrow
Date: 2018-04-10 15:02
Category: DDHH, Nacional
Tags: afectación a comunidades, indígenas sikuani, palma de aceite, Poligrow, Restitución de tierras, sikuani
Slug: indigenas-sikuani-se-oponen-a-ocupamiento-de-predios-por-empresa-poligrow
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/sikuani-e1523380800116.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de Justicia y Paz] 

###### [10 Abr 2018] 

La comunidad de indígenas Sikuani que habitan en el resguardo de Caño Ovejas en Mapiripán, Meta manifestaron su oposición al **ocupamiento y la siembra de palma de aceite** por parte de la empresa Poligrow en predios que hacen parte de una demanda de restitución de tierra. Diferentes organizaciones sociales han venido reiterando que esta empresa ha generado afectaciones al ambiente y en especial a las comunidades indígenas.

De acuerdo con un comunicado enviado por los Sikuani, los indígenas recibieron el documento sobre la evaluación de los **impactos socio ambientales** para poder desarrollar proyectos de plantación por parte de la empresa Poligrow. Allí, la empresa manifiesta las intenciones de realizar la siembra de palma en un área de 818,97 hectáreas de territorios indígenas.

### **Territorios son ancestrales para los indígenas Sikuani** 

Dentro de los predios que hacen parte de las intenciones de la empresa para la siembra de palma hacen parte La Cristalina, el Mojadero, el Yamú, el Bogante y el predio el Cazualito. Estas tierras han sido declaradas como territorios ancestrales y hacen parte de la **demanda de restitución de tierras** que lleva a cabo el Juez Segundo de Restitución de Villavicencio, razón por la cual los indígenas manifestaron su oposición a la ocupación y siembra por parte de la multinacional.

En reiteradas ocasiones, organizaciones que acompañan los procesos de restitución han asegurado que la empresa **ha prohibido a las comunidades indígenas** la práctica de actividades ancestrales. Entre estas se encuentran la visita a la laguna sagrada de las Toninas que ya fue adquirida por Poligrow para instalar plantas extractoras para el procesamiento de la palma africana. (Le puede interesar:["La cara oculta de Poligrow, empresa palmera en Mapiripán"](https://archivo.contagioradio.com/poligrow-palma-mapiripan/))

### **Grupos paramilitares hacen presencia en los territorios** 

A esta situación se suma las ya establecidas denuncias sobre la presencia de grupos paramilitares como las Autodefensas Gaitanistas de Colombia que están generando **afectaciones a las comunidades.** Desde hace varios años, las organizaciones sociales han manifestado que la Fuerza Pública ha permitido la presencia paramilitar generando afectaciones sobre los campesinos y los indígenas.

Teniendo de presente las afectaciones en las que estaría involucrada Poligrow, como lo son los **ocupamientos de territorios de mala fe** en predios que comprenden hectáreas adquiridas por la empresa y que son propiedad de familias campesinas, desde las organizaciones se le ha hecho un llamado al Gobierno Nacional donde han manifestado la importancia de hablar sobre las responsabilidades de las empresas en medio del conflicto armado.

### **Cultivos de palma de aceite afectan el medio ambiente** 

Además de las afectaciones a las comunidades, los ambientalistas han dejado en claro que este tipo de cultivos **dañan el medio ambiente** en la medida en que requieren de grandes cantidades de agua para su desarrollo. Estos cultivos podrían generar sequías que terminan por afectar la vida de diferentes especies de animales y las fuentes hídricas subterráneas de regiones como Mapiripán en el Meta.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
