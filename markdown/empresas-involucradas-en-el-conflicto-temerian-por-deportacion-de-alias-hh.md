Title: Empresas involucradas en el conflicto temerían por deportación de alias "HH"
Date: 2017-12-27 12:25
Category: DDHH, Nacional
Tags: Alias "HH", Everth Veloza, paramilitares, Paramilitarismo, víctimas
Slug: empresas-involucradas-en-el-conflicto-temerian-por-deportacion-de-alias-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/hh-y-empresas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Migración Colombia/Contagio Radio] 

###### [27 Dic 2017] 

Tras la deportación el pasado 26 de diciembre del ex jefe paramilitar José Éverth Veloza García, **alias “HH” a Colombia**, las víctimas han pedido que continúe contribuyendo con el esclarecimiento de la verdad de los más de 3 mil crímenes que le han sido atribuidos.

Indicaron que es necesario que se sepa **cuáles fueron los vínculos de terceros** en las actuaciones de los paramilitares y que es fundamental que esto este sepa en el marco del funcionamiento de la Comisión de la Verdad que busca esclarecer el conflicto armado del país.

Se tiene alguna información sobre algunas empresas, las bananeras, las palmeras y otras que operan en la región en la que los paramilitares **cometieron varios de sus crímenes**, pero falta mucha verdad en torno a la vinculación de esas empresas y las estructuras de HH, indicó Adriana Arboleda, abogada de la Corporación Jurídica Libertad. (Le puede interesar: ["Empresas bananeras responsables de crímenes de lesa humanidad"](https://archivo.contagioradio.com/empresas-bananeras-responsables-de-crimenes-de-lesa-humanidad/))

### **Empresas denunciadas que operaban en la región de influencia de "HH"** 

En diversos procesos judiciales, se ha incluido a empresas como **Chiquita Brands y su filial en Colombia Bananex**, como empresas que financiaron las actuaciones de los paramilitares de los bloques que comandó Veloza. A inicios de 2017, la Sala de Justicia y Paz  del Tribunal Superior de Bogotá, dio a conocer una lista con 57 empresas que habrían financiado estructuras paramilitares, 7 de ellas son bananeras y han sido mencionadas en diferentes testimonios y confesiones de desmovilizados paramilitares que se acogieron a la Ley 975 de Justicia y Paz.

De acuerdo con el portal Verdad Abierta, "Chiquita pagó a los paramilitares, entre 1997 y 2004, **más de 1,7 millones de dólares** no sólo por vacunas y sobornos extorsivos, sino también por 'seguridad activa'". (Le puede interesar: ["Los empresarios son los que están detrás de los asesinatos": líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

Además, la comercializadora **Banacol en el Urabá, realizó pagos a las "Convivir"**, que "fueron fachadas para canalizar recursos hacia las AUC". Sin embargo, en marzo de 2013, "un fiscal especializado de Medellín decidió precluir la investigación contra estas empresas, argumentando que los pagos a las Convivir se dieron dentro de un marco de buena fe y confianza generada por el respaldo estatal".

De igual forma, empresas como COINDEX, que está integrada por diferentes accionistas como Pacuare S.A., Agropecuaria El Tesoro y Agropecuaria Bananeras, esta última pertenece a la familia Argote, están relacionadas con las actuaciones del paramilitarismo. Así lo detalla el informe “Colombia: Banacol, empresa implicada en el paramilitarismo y acaparamiento de tierras en el Curvaradó y Jiguamiando”. A este grupo se le atribuye el **acaparamiento irregular de 1.236 hectáreas de territorios colectivos de comunidades negras. **(Le puede interesar: ["La macabra alianza entre paramilitares y empresas bananeras"](https://archivo.contagioradio.com/la-macabra-alianza-entre-los-paramilitares-y-las-empresas-bananeras/))

### **Se abre una nueva oportunidad de verdad para las víctimas** 

Debido a los aportes que ha hecho “HH” en el esclarecimiento de la verdad, las víctimas consideran que su llegada al país significa **una nueva puerta para conocer** y entender a fondo el fenómeno del paramilitarismo. Esperan que todavía haya voluntad por parte del paramilitar y las instituciones para esclarecer miles de crímenes y en especial las estructuras que estuvieron detrás de las actuaciones de esos grupos.

De acuerdo con  Arboleda, [**Ever**] **Veloza no debió ser extraditado** en la medida que estaba contribuyendo con el esclarecimiento de la verdad. Indicó que esto hizo parte de una estrategia para silenciar al paramilitar y que no revelara ciertos vínculos de terceros con el actuar de los paramilitares.

Además, indicó que, debido a que fue excluida la participación obligatoria de terceros en la Jurisdicción Especial de Paz, “es importante que en la Comisión de la Verdad los jefes paramilitares **cuenten los vínculos de la clase empresarial** y a qué obedeció la estrategia de terror”. Dijo que se debe conocer no solamente los crímenes sino también “de dónde provenían las ordenes y quiénes estuvieron detrás de las estrategias de despojos”.

Desde la Corporación Jurídica Libertad han intentado que se supere la **estrategia de impunidad** que ha habido y que se ve reflejada en decisiones recientes como la participación de terceros en la JEP. Por esto, “hay que generar condiciones para que “HH” pueda contar quienes financiaron la guerra en departamentos como Antioquia donde hay un manto de impunidad”. (Le puede interesar: ["20 empresas deberán devolver 53.821 héctareas de tierras despojadas"](https://archivo.contagioradio.com/20-empresas-deberan-devolver-53-821-hectareas-de-tierras-despojadas/))

### **No ha habido voluntad de la Fiscalía para avanzar en hechos revelado en Justicia y Paz** 

Arboleda enfatizó en que, si bien hay miles de expedientes de Justicia y Paz que han sido enviados a la Fiscalía para establecer la responsabilidad de terceros, **“aún falta que se digan muchas cosas”** y falta información sobre, por ejemplo, el grupo empresarial antioqueño y los empresarios bananeros.

Para ella, “es imposible que, de la estrategia paramilitar, que permeó tantos sectores de la sociedad, **hoy se tenga tan poca información** y no haya justicia con relación a la participación de terceros”. Para lograr esto, han pedido que haya garantías y voluntad por parte de la Fiscalía en la medida en que “todavía hay mucha información que pueden entregar y que va a ser muy útil en la Comisión de la verdad”.

### **El historial de "HH"** 

Alias “HH” **comandó el bloque Bananero en el Urabá Antioqueño y el Calima** del Valle del Cauca y sus alrededores. Desde este último bloque, “HH” extendió el grupo paramilitar a Buenaventura, la zona del pacífico colombiano hasta el Cauca y llegó a tener influencia en el Huila, en Quindío y en Antioquia.

El 24 de noviembre de 2004 **el Bloque Bananero se desmovilizó** y el 18 de diciembre del mismo año lo hizo el bloque Calima, esto en el marco del proceso de "desmovilización" de las estructuras paramilitares y la posterior aplicación de la ley 975.

Sin embargo, en el momento en que el paramilitar se encontraba brindando versión libre de los crímenes que se habían cometido bajo su mando, fue **extraditado en 2009 hacia Estados Unidos** por delitos relacionados con narcotráfico. Allí, cumplió una condena de 8 años y con dificultades continúo contribuyendo con información para encontrar fosas comunes y vínculos de políticos y miembros de la fuerza pública con los grupos paramilitares.

Al paramilitar, que se encuentra en el bunker de la Fiscalía en espera de la remisión por parte del INPEC para que comience a pagar la pena de 8 años que contempla el mecanismo de Justicia y Paz, **la Fiscalía le ha imputado 1.984 de homicidio** y desapariciones forzadas que ocurrieron en varios departamentos entre 1999 y 2004. Entre ellos se encuentra el asesinato y mutilación de 27 campesinos en la cuenca del Río Naya en abril de 2001.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
