Title: Piden respeto a la Corte Suprema por indagatoria a Álvaro Uribe
Date: 2019-10-07 18:09
Author: CtgAdm
Category: Judicial, Política
Tags: alvaro uribe velez, Corte Suprema de Justicia, Testigos falsos
Slug: piden-respeto-a-la-corte-suprema-por-indagatoria-a-alvaro-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/La-Cronología-del-caso-contra-Uribe.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/34146277812_d52c5f93f3_c.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Flickr Centro Democrático  
] 

El próximo martes 8 de octubre se desarrollará el llamado a indagatoria ante la Corte Suprema de Justicia (CSJ) del senador y expresidente Álvaro Uribe Vélez, en el marco del caso en su contra por presunta manipulación de testigos. Defensores de derechos humanos que han sido hostigados en los gobierno del ahora senador han señalado que **la indagatoria envía un mensaje importante que brinda credibilidad en la justicia, y podría ser la puerta que abra el camino al desarrollo de otras investigaciones contra el expresidente.**

### **En Contexto: Un llamado a indagatoria por testigos falsos** 

En 2011 el senador Iván Cepeda, por solicitud del mismo testigo, se entrevista con Pablo Hernán Sierra García (alias Alberto Guerrero), quien expresa que Uribe Vélez y su hermano crearon el Bloque Metro de las llamadas Autodefensas Campesinas de Córdoba y Urabá (ACCU). En ese mismo año, Cepeda denunció a Uribe por nexos con el paramilitarismo, y posteriormente se entrevistó con Juan Guillermo Monsalve, quién también solicitó la entrevista con Cepeda e integró el Bloque Metro de las Autodefensas Unidas de Colombia (AUC).

Monsalve confirmó lo señalado por Sierra García y denunció irregularidades cometidas por Santiago Uribe (hermano de Álvaro Uribe) en el marco de las elecciones presidenciales de 2002. En 2012, la defensa de Álvaro Uribe denunció al senador Cepeda por presunto  abuso de función pública al haberse entrevistado con ambos testigos; dicha denuncia se amplió en 2013 con nuevos testigos, y se sumó en 2014 a la acusación de que Cepeda había comprado testigos para inculpar a los hermanos Uribe Vélez. (Le puede interesar:["No es necesario pagar testigos contra Uribe "hay decenas" Iván Cepeda"](https://archivo.contagioradio.com/no-es-necesario-pagar-testigos-contra-uribe-hay-decenas-de-testimonio-ivan-cepeda/))

Luego de un proceso de cerca de 4 años, la Corte Suprema de Justicia determinó que las visitas del senador Cepeda a los testigos mencionados no fueron ilegales porque estaban entre sus competencia, así como que tampoco se habían presentado ofrecimientos de Cepeda a dichas personas. En su lugar, la Corte ordenó la apertura de un proceso contra Álvaro Uribe, porque en la ampliación de la denuncia hecha en 2013 se habría presentado la compra de testigos contra el senador Cepeda. (Le puede interesar:["Uribe a investigación por presunta manipulación de testigos contra Iván Cepeda"](https://archivo.contagioradio.com/uribe-testigos-cepeda/))

En el marco de este nuevo proceso, **la Corte tiene como evidencias más de 100 horas de grabación de llamadas, producto de interceptaciones realizadas contra terceros**, en las que Uribe estaría ofreciendo favores a testigos para incriminar a Cepeda. En agosto de este año, la CSJ cita a indagatoria el próximo 8 de octubre a Uribe Vélez por los delitos de fraude procesal y soborno para escuchar su versión de los hechos, mientras otros testigos acuden ante el alto tribunal entre el 3 de septiembre y el 9 de octubre. (Le puede interesar:["Corte Suprema abre investigación formal contra Álvaro Uribe y Álvaro Prada"](https://archivo.contagioradio.com/corte-suprema-abre-investigacion-formal-contra-alvaro-uribe-y-alvaro-prada/))

\[caption id="attachment\_74667" align="aligncenter" width="830"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/La-Cronología-del-caso-contra-Uribe-830x1024.jpg){.wp-image-74667 .size-large width="830" height="1024"} Foto: @disfrutaryganar\[/caption\]

### **¿Qué significa la indagatoria para quienes han *sufrid*o a Álvaro Uribe Vélez?**

**Soraya Gutiérrez, presidenta del Colectivo de Abogados José Alvear Restrepo (CAJAR)**, afirmó que ellos han sufrido a Álvaro Uribe Vélez desde su época como presidente, "porque fue un gobierno autoritario que usó toda su inteligencia de Estado para perseguir a los defensores de derechos humanos y sufrimos lo que significó su política de seguridad democrática". Por esa razón, resaltó como un hecho importante el que la justicia actúe "contra funcionarios de alto nivel", que habrían infringido las leyes.

Aunque Gutiérrez señaló que estos delitos por los que se investiga a Uribe son simbólicos, "en la medida en que no está siendo investigado por delitos más graves", el llamado de la Corte Suprema resulta importante porque **se trata de un funcionario de alto nivel que "usó su poder para desviar investigaciones, para sobornar personas**".  Al tiempo, la presidenta del CAJAR, resaltó que este evento podría desencadenar otros procesos, cuya investigación podría asumir la misma Corte.

### **Tres investigaciones que podrían tomar fuerza luego de indagatoria a Uribe** 

Alirio Uribe Muñoz, abogado integrante de la defensa del senador Cepeda, a lo largo de diferentes entrevistas que ha tenido con Contagio Radio ha recordado que este caso se remonta a uno más grave: la implicación de los hermanos Uribe Vélez en la creación del Bloque Metro. De igual forma, el Tribunal Superior de Antioquia tiene en curso una investigación contra Santiago Uribe Vélez por la presunta creación del grupo paramilitar "Los 12 Apóstoles", cuyos inicios se han rastreado en la hacienda "La Carolina", de la que eran dueños los hermanos Uribe Vélez. (Le puede interesar:["Caso de falsos testigos abriría la puerta a nuevas investigaciones contra Uribe"](https://archivo.contagioradio.com/caso-de-falsos-testigos-abriria-la-puerta-a-nuevas-investigaciones-contra-uribe/))

Otras denuncias realizadas por Cepeda ante la Corte Suprema están relacionadas con diferentes masacres ocurridas en Antioquia, mientras Álvaro Uribe ejercía como gobernador de ese departamento. Los hechos concretos se refieren a las masacres de El Aro y La Granja, así como el asesinato del defensor de derechos humanos José María Valle; casos por los que el Tribunal Superior de Medellín ha compulsado copias contra el expresidente. (Le puede interesar:["Compulsan copias contra Álvaro Uribe Vélez por dos masacres en Antioquia"](https://archivo.contagioradio.com/alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo/))

### **Uribe dice que su familia lo quiere ver en ya en la casa, ¿una forma de evitar la CSJ?** 

Luego que se conociera la intención de la Corte de avanzar en el caso, Uribe Vélez dijo que su familia le estaba pidiendo que se retirara de la política, un hecho que fue considerado por diferentes analistas como una forma para sustraerse de la competencia de la CSJ. En su momento, ocurrió lo mismo cuando el senador decidió renunciar a su curul en el senado, hecho del cual finalmente desistió. (Le puede interesar:["El historial de investigaciones contra Álvaro Uribe Vélez](https://archivo.contagioradio.com/el-historial-de-investigaciones-contra-alvaro-uribe-velez/)")

Según explicó la abogada Gutiérrez, el que Uribe Vélez renuncie a su curul no serviría para que la Corte pierda competencia en su caso porque "cuando él cometió los delitos gozaba de fuero constitucional", no obstante, declaró que ese sería un debate jurídico que tendría que ser resuelto. La directora del CAJAR concluyó que esperaba que la Corte Suprema mantuviera su competencia sobre el caso, y que la sociedad colombiana acompañe la independencia y decisiones que emita este poder judicial.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
