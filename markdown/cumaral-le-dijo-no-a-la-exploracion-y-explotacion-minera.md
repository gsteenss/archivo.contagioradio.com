Title: Cumaral le dijo no a la exploración y explotación minera en su territorio
Date: 2017-06-04 17:23
Category: Ambiente, Nacional
Tags: consulta popular, Cumaral, Meta
Slug: cumaral-le-dijo-no-a-la-exploracion-y-explotacion-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/18893124_1819588008369483_1357714469533752862_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [04 Jun 2017]

**Con 7.475 votos, Cumaral le dijo no a la exploración sísmica, perforaciones y producción de hidrocarburos en sus territorios**, la votación total fue de 7658 votos, de los cuales tan solo 183 eligieron el sí.

Las votaciones iniciaron desde muy temprano en la mañana y transcurrieron sin ningún tipo de inconveniente. En total fueron 22 las mesas instaladas en el municipio que tenía un censo electoral de **15.782 personas y se necesitaba un umbral de 6.000 votos para que la consulta popular fuese legítima. **

La pregunta que respondieron los participantes a las elecciones fue ¿Está usted de acuerdo, ciudadano cumaraleño, con que dentro de la jurisdicción del municipio de Cumaral (Meta) se ejecuten actividades de exploración sísmica, perforación exploratoria y producción de hidrocarburos?. Le puede interesar: ["Habitantes de Cumaral, Meta decidirán si quieren o no explotación petrolera en su territorio"](https://archivo.contagioradio.com/38866/)

Actualmente en Cumaral, hay un 80% del territorio que se habría destinado a exploración y explotación de petroleo y se encuentra cubierto con un 90% de Bloques petroleros ( (Condor, CP04, LLA 35, LLA 59 y LLA69) y el proceso de consulta surge tras el rechazó de la ciudadanía a  la actividad petrolera **Mansarovar Energy Colombia LTDA, o también conocido como el Proyecto Llanos 69 –LL69. **

Con esta decisión **Cumaral se suma a los municipios en Colombia como Cajamarca y Piedras del Río que le dicen no a la exploración y explotación minera en sus territorios**, con la finalidad de proteger el ambiente. Le puede interesar: ["Concejo municipal aprobó consulta popular minera en Carmen del Chucuri, Santander](https://archivo.contagioradio.com/concejo-municipal-aprobo-consulta-popular-en-carmen-de-chucuri-santander/)"

![cumaral8](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/cumaral8.jpg){.alignnone .size-full .wp-image-41634 width="806" height="465"}

######  Reciba toda la información de Contagio Radio en [[su correo]

 
