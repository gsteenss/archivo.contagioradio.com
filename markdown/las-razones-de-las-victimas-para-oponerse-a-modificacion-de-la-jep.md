Title: Las razones de las víctimas para oponerse a modificación de la JEP
Date: 2018-11-01 17:42
Author: AdminContagio
Category: DDHH, Nacional
Tags: JEP, MOVICE, Regla Operacional, víctimas
Slug: las-razones-de-las-victimas-para-oponerse-a-modificacion-de-la-jep
Status: published

###### [Foto: MOVICE] 

###### [19 Oct 2018] 

Ante la modificación que se propuso hacer desde el Congreso a la Jurisdicción Especial para la Paz (JEP), **agregando 14 magistrados más para juzgar exclusivamente a militares**, el Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), sostiene que se está afectando el derecho de las víctimas a la verdad y garantías de no repetición.

**César Vargas, integrante del MOVICE**, señaló que la modificación que se pretende no es extraña, puesto que se enmarca en un conjunto de acciones encabezadas por el Centro Democrático, en las que se incluye el rechazo del Proyecto de Ley que otorgaba curules en el Congreso para las víctimas y los ataques al Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR).

Vargas declaró que la aprobación en primer debate del Proyecto de Acto Legislativo Nº 4 de 2018, es un acuerdo de impunidad, y explicó que lo que estipula esta modificación al acto legislativo 01 de 2017 (con el que se creo la JEP), es adicionar 14 magistrados a la Jurisdicción, de tal forma que hayan 2 más en cada sala y sección que componen esta instancia.

El integrante del MOVICE criticó que en el Proyecto se manifieste la separación e independencia que tendrían los nuevos magistrados, respecto a los que ya integran la Jurisdicción, para juzgar a militares; **hecho que en lo operativo, se traduciría en la existencia de una sala exclusiva para uniformados compuesta por 14 miembros.**

Adicionalmente, el activista expuso que al ser uno de los criterios para elegir los nuevos magistrados su conocimiento en **reglas operacionales,** de tal forma que sus perfiles serian proclives al aparato legislativo militar;  rompen el principio de igualdad entre agentes del conflicto, y afectan el derecho a la verdad de las víctimas.

Un acontecimiento que Vargas calificó como extraño, fue la aprobación del Proyecto con 18 votos positivos, entre ellos, los de Roy Barreras, Gustavo Petro y el Partido Alianza Verde (Angélica Lozano e Iván Name), frente a un voto negativo (Alezander López del Polo) y un impedimento (Julián Gallo de FARC). Movimiento que según relata el integrante de MOVICE, fue orquestado por Álvaro Uribe mediante un discurso conciliador que terminó inclinando la balanza en favor de los uniformados.

Esta situación daría a entender que **el Proyecto tiene apoyo de diversos sectores, y contaría con el aval para salir adelante en los 7 debates que le restan.** Vargas afirmó que, por lo tanto, no tienen esperanzas de que algo cambie durante todo el proceso legislativo. (Le puede interesar: ["Organizaciones de DD.HH. y Corte Penal Internacional temen impunidad por modificaciones a la JEP"](https://archivo.contagioradio.com/organizaciones-de-dd-hh-y-la-corte-penal-internacional-temen-impunidad-por-modificaciones-a-la-jep/))

### **¿Cómo ese Proyecto afecta la verdad y las garantías de no repetición?** 

El integrante del MOVICE indicó que el trabajo de incidencia para apoyar todo el SIVJRNR va más allá que oponerse a esta modificación, y pasa por la construcción de **una verdad en la que se reconozca que hay crímenes de Estado,** y que los agentes del mismo son igualmente responsables, o más en algunos casos, que guerrillas o paramilitares.

Vargas dijo que las organizaciones de Derechos Humanos llamarán la atención a la oposición, así como al sistema internacional para garantizar el acceso a la justicia de las víctimas, y buscando que la Corte Penal Internacional tenga competencia para juzgar a los uniformados. (Le puede interesar: ["JEP debería dar el mismo tratamiento a todas las víctimas: Iván Cepeda"](https://archivo.contagioradio.com/jep-victimas-cepeda/))

También anunció que realizarán movilizaciones para defender el derecho a la verdad puesto que, "si el Estado no se puede reconocer como responsable en el conflicto armado, **difícilmente podemos pensar que ese Estado pueda hacer los cambios necesarios para alcanzar una paz estable y duradera"**.

<iframe id="audio_29779582" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29779582_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
