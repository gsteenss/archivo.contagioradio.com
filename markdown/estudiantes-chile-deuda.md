Title: Estudiantes chilenos exigen no más deuda por estudiar
Date: 2017-05-04 16:21
Category: El mundo, Otra Mirada
Tags: Chile, Confech, educacion, gratuidad
Slug: estudiantes-chile-deuda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/C-_MjD9XcAEsfHb.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El ciudadano 

###### 04 Abr 2017 

Bajo la consigna "Fin a la deuda", estudiantes universitarios de Chile se manifestaron frente a las instalaciones bancarias encargadas de entregar el Crédito con Aval del Estado (CAE) protesta justificada en el nivel de endeudamiento que acarrea el actual sistema, que trasciende en algunos casos los 20 años.

En capitales como Valparaiso, Concepción, Antofagasta y Santiago, ciudad donde los estudiantes se ubicaron frente a las instalaciones del BancoEstado, cerca al Palacio de la Moneda, algunos de ellos encadenándose mientras portaban carteles en los que indicaban la cantidad de dinero a la que corresponde el monto de su deuda.

La protesta que se desarrollo con total normalidad, se realizó también en otras entidades como el Banco de Chile de Concepción a la que llegaron 12 estudiantes con arengas y carteles protestando por la crisis de la educación que se viven en el país. Sin embargo desde la entidad bancaria los funcionarios argumentaron que no tiene vínculos con el CAE. Le puede interesar: [90 mil estudiantes se movilizan contra la reforma educativa en Chile](https://archivo.contagioradio.com/estudiantes-se-movilizan-en-chile/).

Desde la Confederación de estudiantes chilenos Confech, aseguran que cambiar esta situación es un tema de “ voluntades políticas ” y que esta demanda la “impulsarán con fuerza en las calles" en la próxima movilización convocada para el 9 de mayo. Adicionalmente desde la organización estudiantil afirman que vienen trabajando en una "propuesta técnica" que abordará la mejor manera de realizar la condonación que demandan.

Por su parte la ministra de educación Adriana Delpiano, aseguró que condonar este tipo de deudas “es imposible”  debido a que equivaldría a 13,8 veces los recursos destinados para la gratuidad en 2016. La funcionaria argumenta que la deuda estimada bordearía los \$8 mil 260 millones de dólares, es decir, el 3,26% del PIB del país.

“Es mucha plata, es muy complejo”, aseguró Delpiano añadiendo que el Gobierno sí se hará cargo del CAE “como tal, tenemos que cambiarlo ” a través de otro mecanismo. De acuerdo con la ministra el monto total de la deuda es mayor a toda la Reforma Tributaria que se hizo.
