Title: Tribunal de EEUU condena a Palestina a pagar 218 millones de dólares
Date: 2015-02-24 23:13
Author: CtgAdm
Category: Judicial, Otra Mirada
Tags: ANP, Atentados Jerusalén 2002 y 2004, EEUU condena a la ANP a pagar 218 millones de dolares, OLP, Palestina
Slug: tribunal-de-eeuu-condena-a-palestina-a-pagar-218-millones-de-dolares
Status: published

###### Foto:Minuto30.com 

Un juez de EEUU condenó por 25 cargos a la ANP (Autoridad Nacional Palestina) y a la **OLP (Organización para la liberación de Palestina), por los atentados en Jerusalén de 2002 y 2004**, en los que murieron ciudadanos estadounidenses.

Mes y medio ha durado el juicio contra las autoridades palestinas, que este lunes ha fallado en contra de ellas. La denuncia fue radicada por familiares víctimas de los atentados.

El abogado de la acusación **Kent Yolowitz responsabilizó a las autoridades palestinas de colaborar y proteger a los terroristas que mataron a un total de 33 personas e hirieron a 450**. A lo que el abogado palestino respondió que no tienen jurisprudencia para juzgar lo que sucede en Medio Oriente. Kent Yolowitz indicó a su homólogo palestino que “**si atentas contra un estadounidense la justicia antiterrorista caerá sobre tí”**

**Tanto la OLP como la ANP han negado la acusación tildándola de farsa, ya que el líder asesinado Yasser Arafat nunca colaboró con organizaciones terroristas o armadas**. Además la acusación coincide con la denuncia del estado Palestino contra Israel ante el TPI de La Haya por delitos de lesa humanidad en los territorios ocupados de Gaza y Cisjordania.

Hay que recordar que en la **última ocupación ilegal de Gaza por parte de Israel, murieron 2.000 civiles y hubo más de 10.000 heridos. Se cometieron crímenes de lesa humanidad**, que no fueron condenados debido al veto de EEUU y de los aliados de Israel ante Naciones Unidas.

**Tampoco Israel ha permitido entrar  los observadores de Derechos Humanos de la ONU para poder realizar el informe sobre los sucedido en el última ocupación**.

Por otro lado la organización de Derechos Humanos israelí "Yesh Din" de defensa de Palestina informó que el **92% de las denuncias de los palestinos contra Israel por la ocupación ni si quiera han llegado a su curso**.
