Title: 1.500 personas mueren en India por calor extremo y pésimas condiciones laborales
Date: 2015-05-29 16:03
Category: El mundo, Otra Mirada
Tags: Calor extremo en India, Condiciones laborales pésimas en India, Falla funcionamiento hospitales de la India, India no está preparada para ola de Calor, Insalubridad India, Medio millar de muertos en India por calor, Ola de calor en India
Slug: 1-500-personas-mueren-en-india-por-calor-extremo-y-pesimas-condiciones-laborales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/image55646454e73993_20817761.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:LosAndes.com.ar 

###### [29May.2015] 

**Al menos 1.500 personas han muerto en la India por la ola de calor extremo** que ha sacudido el país con temperaturas de entre 47 grados hasta los 52 grados centígrados. Las **pésimas condiciones laborales, el sistema sanitario insuficiente y la extrema pobreza** han llevado al país al colapso y a la catástrofe humanitaria.

En los **últimos 15 años, India no había vivido condiciones tan extremas como las del año actual,** pero, a pesar de que la ola de calor es inusual, esta no es la causa de la catástrofe humanitaria que vive el país. Todo apunta a que se debe a las dinámicas de cambio climático que está viviendo el planeta.

La **ausencia de alertas tempranas, de planificación y de previsión ante lo que, según los expertos, podía haber sido planificado para evitar riesgos** innecesarios, ha dejado a la población en una situación de extrema vulnerabilidad, siendo los estados de Andra Pradesh, Telangana y Odisha, los más afectados.

India tiene la **cifra más alta de pobreza extrema del mundo**, así lo confirma la ONU, donde miles de ciudadanos viven en condiciones de **máxima insalubridad, padeciendo hambre y enfermedades** que impiden su propia supervivencia.

La ONU también indica en su informes que el **30% de la población vive en la extrema pobreza** y solo en **2012 murieron 1,4 millones de niños de cinco años**, situando al país en el de mayor cantidad de niños muertos a los cinco años. El **60% de la población que defeca al aire libre vive en India**, y el **17% de muertes maternas a nivel mundial** se produce en el país Asiático.

Las **condiciones laborales actualmente en el país rozan la semiesclavitud** y no se han adaptado a la ola de calor. Según la Confederación Sindical Internacional **(CSI), India esta situada en el quinto grupo de países con peores condiciones laborales**. Siendo **el país con la mayor fuerza de trabajo del mundo con 516 millones** de personas. Por ejemplo, un obrero necesita trabajar porque, de lo contrario, ese día no cobra o no come, y así no puede mantener a su familia.

**UNICEF** calcula que **7,6 millones de personas tienen VIH y más del 80% ha desarrollado ya el SIDA.** Los informes apuntan a que la combinación entre pobreza extrema y falta de sistema sanitario, principalmente en temáticas de prevención, están provocando una crisis de contagios.

Testimonios como el del obrero Ijaz Ul Miyan  **"...Ayer me sentí mareado y vomité a causa del calor**. Algunos hombres están de baja a causa de las temperaturas...", atestiguan las condiciones en las que se trabaja.

Además, hay que añadir que debido al calor, **han fallado todo tipo de infraestructuras públicas como el tendido eléctrico o el alcantarillado**, aumentando las condiciones insalubres en las que ya vivían miles de personas.

Pero sin duda la peor situación es la del **sistema sanitario Indio**, que antes de la ola de calor era imposible de cubrir las necesidades de la población y **actualmente está totalmente colapsado ante las miles de urgencias** de niños y ancianos, así como de enfermos terminales.

Por último, el país vive en un **sistema de clases basado en las castas** que aún no han cesado. A pesar de su prohibición y de la lucha iniciada por Gandhi,  **llevan a la extrema pobreza a miles de ciudadanos sin posibilidad alguna de salir de ella,** siendo esta la principal causa de las muertes en una situación de emergencia como la presentada.
