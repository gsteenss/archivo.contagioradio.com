Title: La MOE ha registrado 228 hechos de violencia contra líderes sociales en últimos 8 meses
Date: 2019-06-16 10:27
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Alerta, Comicios electorales, elecciones, MOE
Slug: moe-228-hechos-violencia-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Elecciones-MOE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Nación  
] 

Según el más reciente informe de la **Misión de Observación Electoral (MOE)**, desde octubre de 2018 hasta mayo de 2019 **se han presentado 228 hechos de violencia contra líderes sociales**, políticos o comunales. En el 29% de los casos, el hecho culminó en asesinatos; cifras que podrían ser superiores a las últimas elecciones locales que tuvieron lugar en octubre de 2015.

**Diego Rubiano, investigador político de la MOE**, explicó que el informe es producto de una observación que inició desde octubre; momento desde el que han registrado **24 agresiones contra precandidatos**. En el mismo periodo de 2015 se documentaron apenas 15 agresiones, es decir, formas violentas de intimidar a quienes buscan participar activamente en política electoral.

**Las acciones violentas contra los líderes sociales ocurrieron en 111 municipios de 22 departamentos del país**; situación que preocupa a la Misión, pues a medida que se acercan los comicios, la cifra podría aumentar. (Le puede interesar: ["MOE pide compromiso de transparencia en elección de contralor"](https://archivo.contagioradio.com/moe-pide-compromiso-de-transparencia-en-eleccion-de-contralor/))

> Este es el mapa de asesinatos de defensor@s 2018. Ocurrieron en 26 departamentos y 96 municipios, y son más los lugares donde sufrieron otras agresiones. Los ataques contra defensores ocurren en gran parte de Colombia.  
> No solo los asesinatos hablan de la magnitud de la violencia [pic.twitter.com/cauorswEFJ](https://t.co/cauorswEFJ)
>
> — SomosDefensores (@SomosDef) [14 de junio de 2019](https://twitter.com/SomosDef/status/1139559039736659969?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Otras alertas de la MOE sobre el proceso electoral** 

En cuanto a la inscripción de cédulas, Rubiano resaltó que se han encontrado con una tasa de inscripción realmente baja para un proceso de carácter nacional: "Para unas elecciones ronda los 1oo inscritos por cada 1.000 habitantes y en este momento vamos en 21,5". No obstante, esperan que entre el 8 y 14 de julio se eleven los niveles; en cuyo caso, se debe prestar especial atención al eje petrolero del Meta y el eje carbonífero del cesar que tienen tásas históricamente altas de inscripciones.

La MOE también ha registrado **168 reportes sobre presuntas irregularidades electorales**, siendo la mayoría de ellas (58%), anomalías por publicidad extempóranea o masiva de candidatos. Adicionalmente, el Investigador alerto por presencia de actores armados en las regiones "que puede estar incidiendo en el proceso", siendo el Cauca uno de los departamentos más afectados por agresiones contra pre candidatos.

Rubiano sostuvo que el Gobierno Nacional es bastante receptivo para hablar sobre violencia de tipo política y las irregularidades electorales se están canalizando y envíando a los entes de control encargados; razón por la que espera **que se activen los mecanismos necesarios para prever la violencia contra líderes sociales y políticos, así como infracciones a las reglas electorales.** (Le puede interesar: ["MOE alerta sobre porcentajes atípicos de inscripción de cédulas en 28 municipios"](https://archivo.contagioradio.com/inscripcion_cedulas_elecciones_marzo_moe/))

<iframe id="audio_37113737" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37113737_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
