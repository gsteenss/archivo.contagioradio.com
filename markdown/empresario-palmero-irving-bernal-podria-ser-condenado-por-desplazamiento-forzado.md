Title: Empresario palmero Irving Bernal podría ser condenado por desplazamiento forzado
Date: 2015-02-06 21:18
Author: CtgAdm
Category: Comunidad, DDHH, Judicial
Tags: Desplazamiento forzado, Empresarios palmeros, Paramilitarismo
Slug: empresario-palmero-irving-bernal-podria-ser-condenado-por-desplazamiento-forzado
Status: published

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4049871_2_1.html?data=lZWhm52bdY6ZmKiak5aJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7k08rgw9fNs4zkwtfOz87QrdXV05C21NvNssifo8rf0MbQb8%2FjjMbQx9XYpYzXwtfU0diPtI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Manuel Garzón, Abogado de víctimas de desplazamiento] 

<iframe src="http://www.ivoox.com/player_ek_4049996_2_1.html?data=lZWhm56deo6ZmKiakpaJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7k08rgw9fNs4zkwtHax9fTb6rm187byZCmqdPiwtGY0tTItoa3lIqupsaPt8bmjMjc0MnJssLY0JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Edisson Zapata, Abogado de Irving Bernal] 

Esta semana inició en la ciudad de Medellín el alegato final, en el proceso jurídico que se adelanta en contra Irving Bernal, empresario palmero, ganadero y bananero del Urabá antioqueño. A pesar de haberse acogido sentencia anticipada en el año 2013, y reconocer cargos por concierto para delinquir y delitos ambientales **Bernal se niega a aceptar su responsabilidad en el desplazamiento de las comunidades** ancestrales y los Consejos Comunitarios de los territorios **de Curvaradó y Jiguamiandó**.

Para el abogado de las víctimas civiles, Manuel Garzón, integrante de la Comisión de Justicia y Paz, no reconocer que existe un modelo de despojo en Colombia, es una estrategia que utilizan de manera generalizada los victimarios paramilitares en los procesos jurídicos que se adelantan en su contra. "**El modelo de desarrollo implementado, un modelo depredador y criminal, basa su rentabilidad en la violación de los Derechos Humanos** de los y las habitantes de las regiones a las que llega", señaló Garzón.

Para el representante de las víctimas, también resulta preocupante la actitud por parte del victimario y su abogado, quienes en su alegato final justificaron la acción paramilitar presentando, como cortina de humo, la lucha contra la izquierda social y política en Colombia, declarando a víctimas como victimarios, señalando de manera puntual nombres de líderes y lideresas sociales, y atacando la función legítima de las organizaciones defensoras de Derechos Humanos.

El proceso jurídico continuaría el próximo 24 de febrero, con la presentación de la última parte del alegato por parte de la Defensa de Bernal.
