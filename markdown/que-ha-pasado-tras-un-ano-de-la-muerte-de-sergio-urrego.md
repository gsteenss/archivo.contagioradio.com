Title: ¿Qué ha pasado tras un año de la muerte de Sergio Urrego?
Date: 2015-08-06 14:18
Category: closet, LGBTI
Tags: Alba Reyes, Amanda Azucena Castillo, Colegio Gimnasio Castillo Campestre, Colombia Diversa, Corte Constitucional, Derechos Fundamentales, Derechos Humanos, Discriminación, Fiscalía General de la Nación, Homosexualidad, LGBTI, Ministerio de Educación, Sergio Urrego
Slug: que-ha-pasado-tras-un-ano-de-la-muerte-de-sergio-urrego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/20140912_154405.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [6 Ago 2015] 

El **5 de Agosto** se cumplió un año de la muerte del estudiante **Sergio Urrego Reyes**, quien decidió quitarse la vida tras estar condicionado por parte de las directivas de su colegio, **Gimnasio Castillo Campestre** por su orientación sexual, el estudiante fue víctima de discriminación y fue presionado para corregir sus “malas conductas”.

A lo largo de este año, su familia y en especial su mamá, **Alba Reyes**, se han dedicado a exigir justicia por medio de acciones de tutela en contra de entidades que no siguieron los debidos procesos para estudiar el caso del estudiante; actualmente se lleva a cabo un proceso penal en contra de las directivas del colegio por delitos de **discriminación, ocultamiento de pruebas y falsa denuncia**.

En el marco del proceso legal, un juez determinó que **Amanda Azucena Castillo**, quien en ese momento era la rectora del colegio, debía permanecer en casa por cárcel desde el mes de mayo hasta que se definiera el proceso judicial en su contra, que en caso de ser declarada culpable deberá cumplir una condena de diez años.

**Amanda** no aceptó su culpa en ninguna de las ocasiones que declaró, desde la primera entrevista aseguró que lo único que hizo la institución fue velar por los derechos del estudiante y que la causa de su suicidio había sido porque **Sergio** estaba en condiciones de abandono y porque siempre manifestó su “amor a la muerte”.

A pesar de que la justicia colombiana ha avanzado en el caso, **Alba Reyes**, de la mano de **Colombia Diversa** ha intervenido ante la **Corte Constitucional** para que la acción de tutela presentada en contra del Ministerio de Educación sea analizada; gracias a esto, la tutela está actualmente en trámite y proceso de revisión.

**Alba Reyes**, familia, amigos y amigas le rendirán un homenaje a **Sergio Urrego** para conmemorar un año de su muerte, será a través de música y vídeos que recordarán cómo fue la vida de Sergio, se llevará a cabo el día de hoy desde las **5:30 P.M.** con entrada libre en la **biblioteca Virgilio Barco**.
