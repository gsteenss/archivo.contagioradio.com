Title: Asesinato de Reclamante de tierras "pudo haberse evitado"
Date: 2017-01-30 16:04
Category: DDHH, Entrevistas
Tags: Autodefensas Gaitanistas, Reclamante de tierras
Slug: asesinato-de-reclamante-de-tierras-pudo-haberse-evitado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/audiencia-CIDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [30 Ene 2017] 

El asesinato de Porfirio Jaramillo, reclamante de tierras en la vereda Las Guacamayas, “pudo haberse evitado” de acuerdo con Julio León Correal, líder de esta comunidad, que junto con otras **9 personas, también reclamantes de tierras, han sido amenazados por grupos  identificados como paramilitares**, y que hoy le exigen al gobierno acciones inmediatas de protección por los hechos de violencia de los que han sido víctimas.

“Yo no sé qué tanto tienen esas tierras que tanto han luchado para sacarnos”

Para Julio León, detrás de estos actos está la presencia de empresarios y grupos al margen de la ley interesados en que no se restituyan las tierras “**porque son ellos los que están empoderados  y atacan a los campesinos”**, además afirmó que no es un hecho nuevo, ya que el Estado sabe que desde el 2007 campesinos habían tomado la iniciativa de regresar a sus tierras, sin recibir acompañamiento o garantías por parte de las instituciones correspondientes.

Desde ese momento las diferentes **denuncias que han hecho los campesinos han quedado en la impunidad**, de ninguna existen investigaciones que hayan dado con los responsables, tanto intelectuales como materiales, de las amenazas, hostigamientos y asesinatos en esta región. Le puede interesar:["Autodefensas Gaitanistas hostigan familias de la vereda Las Guacamayas, Turbo"](https://archivo.contagioradio.com/autodefensas-gaitanistas-hostigan-a-familias-de-la-vereda-guacamayas-turbo/)

De igual modo Julio León Correal expresó que el proceso de restitución de tierras ha sido muy lento **“Están entregando tierras solamente en la pantalla del televisor, si han restituído por ahí 5 fincas de 50 hectáreas es mucho”**

### **¿Quién era Porfirio Jaramillo?** 

Porfirio Jaramillo de 70 años, era un campesino que tenía sus tierras en la vereda Las Guacamayas, en Turbo, Antioquia. **Reclamante de tierras, se había convertido en un líder social para su comunidad**. Porfirio **ya había sufrido atentados**, uno de ellos lo llevo a desplazarse a Chigorodó para salvaguardarse por un tiempo, sin embargo, retornó a sus tierras tiempo después.

**Su caso fue revisado por la Unidad Nacional de Protección, sin dar algún resultado o medida de protección**, igualmente se presentó ante la Defensoría del Pueblo e incluso ante instituciones internacionales, sin que se produjera algún tipo de investigación frente a quienes estaban detrás de las amenazas.

**Fue secuestrado el pasado 29 de enero**, sobre la 1:30 am, cuando cuatro hombres fuertemente armados lo sacaron de su vivienda, su cuerpo se encontró en inmediación del mismo sector donde fue retenido. De otro lado la familia de Porfirio Jaramillo, salió de su hogar y recibió ayuda por parte de la comunidad para poder trasladarse a un sitio más seguro. Le puede interesar: ["117 líderes fueron asesinados durante el 2016"](https://archivo.contagioradio.com/117-lideres-fueron-asesinados-durante-2016/)

### **Exigencias de los Reclamantes de Tierras** 

Frente a esta situación los reclamantes de tierras **están exigiendo medidas de seguridad inmediata por parte de las autoridades correspondientes y garantías para poder vivir en sus tierras**, ya que también han denunciado la prohibición por parte de empresarios, del cultivo de maíz, la ganadería, el comercio y la falta de acciones de protección por parte de las Fuerzas Militares que no tienen una presencia permanente en este lugar.

“Nosotros lo que estamos pensando es ir a **preguntarle al Estado ¿qué pretende con nosotros?** porque no hay generación de empleo, no hay que comer. **Le pedimos al Estado que nos de seguridad, que nos ayuden a que podamos cultivar** y en nuestros proyectos para comercializar”

Sobre la restitución de tierras, los reclamantes le exigen al gobierno que se **“pellizque”** y acelere este proceso, porque se sienten en **“el olvido”**, de la misma manera que piden que se realicen las investigaciones correspondientes y que se devele finalmente quiénes son los que quieren sacarlos de sus tierras. Le puede interesar: ["Familias confinadas en sus fincas por ataque paramilitares"](https://archivo.contagioradio.com/familias-confinadas-en-sus-fincas-por-ataques-paramilitares-en-guacamayas/)

<iframe id="audio_16726556" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16726556_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
