Title: Daniel Rojas, líder indígena y promotor del deporte fue asesinado en Cauca
Date: 2019-05-15 12:35
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Asesinato de líderes indígenas, Caloto, Cauca
Slug: daniel-rojas-lider-indigena-y-promotor-del-deporte-fue-asesinado-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Daniel-Rojas-Líder-indígena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

En horas de la noche del pasado 14 de mayo, hombres armados asesinaron a **Daniel Eduardo Rojas Zambrano**, presidente de la Junta de Acción Comunal del resguardo indígena de López Adentro, municipio de Caloto, Cauca y destacado líder que promovió el deporte.

**Según Edwin Capaz, coordinador de DD.HH de la Asociación de Cabildos Indígenas del Norte del Cauca** **(ACIN)**, los hechos se presentaron cerca de las 7:20 de la noche cuando dos hombres armados habrían seguido el líder indígena hasta inmediaciones de su hogar, abriendo fuego e impactando cinco veces sobre su cuerpo provocando su muerte.

Las condiciones climáticas de fuertes lluvias complicaron la reacción por parte de la guardia indígena, lo que facilito la fuga de los responsables pese a que se acordonó el sector. La comunidad se encuentra ahora en busca de los autores para lograr esclarecer lo ocurrido. [(Lea también: Asesinan a Juliana Chirimuskai, menor misak de 15 años en Cauca)](https://archivo.contagioradio.com/asesinan-a-juliana-chirimuskai-menor-misak-de-15-anos-en-cauca/)

Daniel Rojas fue un destacado líder local, promotor del deporte y la cultura en la vereda de López Adentro, en particular incentivando la práctica del fútbol en los jóvenes, "había sido elegido presidente desde 2018 y en los últimos años también acompañó el ejercicio de control territorial en Caloto", indicó Capaz.

Las primeras indagaciones dan cuenta de que Daniel había denunciado en una asamblea comunitaria riesgos sobre su vida, adicionalmente la autoridad de López Adentro alertó sobre la presencia de hombres armados y amenazas escritas en el sector que, a pesar de no incluir el nombre del líder, iban dirigidas a otros líderes del resguardo.

Aunque aún no se ha podido determinar la responsabilidad en el asesinato de Daniel Rojas, en el sector operan las disidencias de las Farc, grupos paramilitares y se han conocido panfletos de las Águilas Negras, por ahora las investigaciones continúan.

<iframe id="audio_35889606" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35889606_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
