Title: ¡No más Feminicidios! paro de mujeres en Colombia
Date: 2017-03-08 17:03
Category: Mujer, Nacional
Tags: feminicidio en Colombia
Slug: no-mas-feminicidios-paro-mujeres-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Neiva-e1489007404193.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Mar 2017]

En ciudades como Bogotá, Bucaramanga, Medellín entre otras, las mujeres del país salieron a exigir que cesen los feminicidios y la impunidad frente a la violencia de género. Entre mascaras, arengas y pese al inclemente clima, las colombianas gritaron al unisono **!La solidaridad es nuestra arma!, ¡Sin nuestras vidas no importan que produzcan sin nosotras¡ y !Vivas nos queremos¡**

 \

###### Reciba toda la información de Contagio Radio en [[su correo]
