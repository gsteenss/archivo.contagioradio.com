Title: "Las cuarentenas abiertas de América Latina"
Date: 2020-04-23 14:56
Author: AdminContagio
Category: eventos
Tags: América Latina, cuarentena
Slug: las-cuarentenas-abiertas-de-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Image-2020-04-27-at-3.47.13-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: La Garganta Poderosa {#foto-de-la-garganta-poderosa .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Por: La Garganta Poderosa

<!-- /wp:heading -->

<!-- wp:paragraph -->

Recién, hace un ratito nomás, estábamos empezando a testear el golpe cívico militar en Bolivia, el oxígeno para la ilegítima deuda argentina, la fiebre por militarizar las calles uruguayas, los tapaojos para protestar contra Piñera, los delirios de Bolsonaro en las favelas, la temperatura del parlamento peruano, las fosas comunes en México, el rebrote del bloqueo a Cuba, la curva de las tarifas en Ecuador, el virus que mata líderes en Colombia, el aislamiento obligatorio de Venezuela, la letalidad del racismo, los guantes del capitalismo financiero y las vacunas apócrifas de la OEA, entre todas las trumpas que nos hermanan en una tierra sin coronas, donde la plaga del silencio mata personas, los favorecidos cierran sus puertas y nuestro pueblo trabaja.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¡las venas abiertas de la Patria Baja!

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por ahí agonizaban nuevas verdades, nunca prioridades en las inmunizadas portadas de la región, dramáticamente intubadas al mismo patrón, cuando de pronto cambió el universo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al reverso de la globalización, sin perder el control de la televisión, una pandemia tumbó al abrazo abriéndose paso, vomitando sus nichos y enterrando a los demás bichos que azotaban a la pobreza: el hambre, el desmonte, un alambre, ningún horizonte, la sequía, la discriminación, la hipocresía, la concentración, la megaminería, el mal menor, la policía con silenciador, la indiferencia, los pesticidas, **¡la convivencia con los femicidas!**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A lo amargo y a lo chancho del continente, hoy se extiende ruidosamente un entramado de asambleas populares que vienen hilando ideas desde distintos lugares, porque la pobreza es una sola, pero la entereza es una ola de convicciones, arrastrando durante siglos a millones que no sólo padecimos el colonialismo y la desgracia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **¡nacimos de la misma idiosincrasia!**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Burlando todas las barreras, vestidas de modas o erguidas como fronteras, nuestras comunidades villeras vienen tirando paredes por las redes con las delanteras de otras que también son escuelas, como las comunidades, las poblaciones, las chabolas, las barriadas, los cantegriles, los asentamientos,

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿hace cuánto tiempo comenzaron los aislamientos?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mal que les pese, América Latina crece sobre su dignidad, bajo techos de chapa, desafiando a la autoridad del mapa, porque nos unimos, porque rompimos el prisma, **¡porque ya no se aguanta!** Y porque compartimos la misma Garganta.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ARGENTINA

<!-- /wp:heading -->

<!-- wp:paragraph -->

Atrincherada sobre tierra arrasada, sin pan, trabajo, ni techo, el virus encontró buena parte de su laburo hecho.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recesión, desnutrición, familias desalojadas, policías desbocadas y deudas tan sagradas como fraguadas, ya infectaban al Estado, cuando nadie hubiera imaginado alguna limitación para la circulación comunitaria, sólo para evitar una eclosión funeraria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin dudas, el temprano decreto de la cuarentena, valió la pena y endeudó a la alegría: si alguien tiene que esperar, **¡que espere la economía!** Se salvaron vidas y se tomaron medidas importantes, pero son cada vez más desesperantes las filas de los merenderos, donde faltan los noteros y sobra la verdad: ya pasamos a disponibilidad, decenas de imágenes que desnudan a las Fuerzas de Seguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mucho, mucho control popular, donde nadie puede aislar a sus tenedores y parece que son tan peligrosos los comedores de la comunidad, tan ladrones y tan turbios, **¡que la Ciudad invirtió 53 millones en teatro antidisturbios!**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cuidarnos nunca implica callarnos las flaquezas, ni privarnos de aplaudir que por fin podamos discutir las grandes riquezas. Reglas vitales para los demás, como la restricción a los cortes de gas, no están cubriendo a las viviendas informales, porque no llegan las garrafas sociales, ni los sachets de agua potable en el kit de la vianda escolar. Ni eso, ni el Ingreso Familiar de Emergencia lograron descomprimir la situación: llega la urgencia, pero tarda la conexión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y aún faltan asilos para los ancianos menos pudientes que conviven con sus parientes o siguen laburando para morfar, codos con codos: ahora es cuándo, **¡La Patria es aislar a todos!**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### BOLIVIA

<!-- /wp:heading -->

<!-- wp:paragraph -->

El proceso alfabetizador, la plurinacionalidad, 25 mil kilómetros de ruta, la estabilidad monetaria, los hidrocarburos, el teleférico, La Paz libre de represión y el mayor desarrollo económico de la región, tal vez les molestaron a esos fiscales del carisma, la gente y la democracia que voltean una y otra vez.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### No les molestaba un presidente boliviano que hablara sólo en inglés.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Afuera Evo, arrancó un año nuevo y todo normal. Hoy, el 70% del comercio es totalmente informal, la situación social desborda por los costados y ni siquiera se informa el número de contagiados. Los hospitales están jodidos y los movimientos siguen tan perseguidos como las federaciones del Trópico que juntaron donaciones en el Alto Chaparé, pero fueron retenidas por el Ejército del sálvese quien pueda: la biblia, parece, quedó en la Casa de la Moneda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hay, sí, un bono de 400 Bolivianos para mayores de 65, viejos beneficiarios del salario social Juana Azurduy, madres recientes y personas con discapacidad, ¿buenísimo, no? Sólo que nadie lo cobró. Y entonces se creó una canasta familiar de 500 bolivianos, para padres de niños en escuela primaria, ¡qué lo parió! Ah, no, tampoco llegó

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### BRASIL

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ni el impeachment, ni las fake news, ni el law fare, mostraron un sesgo tan hostil hacia su propia gente:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El grupo de riesgo en Brasil es el Presidente.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Más de 20 mil contagiados como secuelas de su libre albedrío: sólo 69 testeados en las favelas de Río, 8 muertos. Y casi todos los medios tuertos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una joven indígena, Yanomami Alvanei Xirixana, acaba de fallecer a los 15 años, "por ese virus letal", **¡pasó 21 días sin hospital!** Abrazos, impotencia, ¿una nena? 1597 casos de violencia en cuarentena.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin aislamiento nacional, un juez del Supremo Tribunal Federal le prohibió a Bolsonaro incidir en cada departamento, pero él siguió abocado a difundir su tratamiento con hidroxicloroquina, totalmente descartado y desaconsejado por la medicina.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### "Uma gripezinha".

<!-- /wp:heading -->

<!-- wp:paragraph -->

A contramano del planeta, hasta su ministro Luis Henrique Mandetta prefirió confrontarlo, "porque tiene otra posición" y la oposición entera salió a respaldarlo, mientras Sao Pablo resiste a instancias de favelados que autogestionan ambulancias para los olvidados. A contraluz de sus funcionarios, Paraisópolis convocó a cientos de voluntarios para monitorear y coordinar, como en Monte Azul, otro experimento que conmueve: el Comité Popular de Enfrentamiento al COVID-19. Pues según el Instituto Data Favela, la pandemia que finalmente más duela tendrá como síntoma un dolor estomacal, porque hay un 55% del pueblo sin estabilidad laboral: 47% autónomos, 8% informales y muito futebol en todos los canales

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### CHILE

<!-- /wp:heading -->

<!-- wp:paragraph -->

El modelo más elogiado desde arriba, ya quedó tercero en la terapia intensiva de la región: 7 mil nuevos casos, sobre la misma constitución que ideó Pinochet, miles sin abrazos, miles sin Internet, miles sin ambulancia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si no pregúntenle a Valentina, vecina de Villa Francia, que perdió un embarazo a manos de los carabineros. O a todos esos compañeros criminalizados por la protesta del 18 de octubre, hoy hacinados en una celda que descubre sus carros de asalto, detrás del alambre: en pleno penal de Puente Alto, rige la huelga de hambre de los referentes más combativos, **¡entre varios casos positivos!** Y el Servicio de Menores en Chiguayante no tiene insumos, ni protocolo aislante, pero tiene 25 pibes enjaulados, todos contagiados. El transporte subió un 125% y la "ley que protege al empleo" puso todavía más feo el panorama de las poblaciones, alivianando las indemnizaciones, ante la plaga del despido y las suspensiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ni a los besos les pusieron frenos: las mascarillas cuestan 1000 pesos chilenos y la cuarentena se pierde en la espera para cobrar el seguro, mientras crece la hilera de la gente sin laburo. Y sí, además un 70% más de hombres denunciados por agresiones en sus propios hogares, para complementar las represiones que intentaron silenciar todas las manifestaciones. Pero nada, no se ve nada en los estrados, en los balcones, en los bazares: **460 contagiados de lesiones** **oculares**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### COLOMBIA

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Hace mucho tiempo que la información real dejó de circular: para la televisión internacional, Colombia es una serie de Pablo Escobar.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mejor no hablar de medio país hundido en la economía informal, ¡un 87,7% en la base del tejido social! Todos sepultados, los datos fuertes: 2473 contagiados y 80 nuevas muertes. Hasta al 27 de abril, una cuarentena sin opción y sin una sola medida de contención.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿La devolución del IVA para el estrato 0, 1 y 2 del sector trabajador? Aguarde en línea, por favor. ¿El auxilio familiar de 160 mil colombianos? Ya quedaron en manos de nombres inventados o copiados con antelación de programas que también fallaron por corrupción. ¿El fondo de emergencias con 14,8 billones de las arcas departamentales? Se inyectarán a los bancos, ¡siempre tan serviciales! Sin opción, los movimientos sociales conviven con la persecución de sus militantes y una cama de terapia cada 60 mil habitantes, aunque un 80% están ocupadas por otras enfermedades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hay municipalidades sin la más mínima disponibilidad vital, sin Unidad de Cuidados Intensivos en ningún hospital, así como hay 824 familias emberás que han sido desplazadas, obligadas por "enfrentamientos".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y entre tantos tratamientos medulares, los paramilitares avanzan por todas las vías, por todos lados: en sólo 14 días, 14 líderes asesinados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### CUBA

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aislada del egoísmo, históricamente confinada por el cinismo, Cuba se adelantó a la cura de la posverdad, cultivando la cultura de la solidaridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Justo ahí, donde la United Fruit sembraba su propia corona, hoy no hay ninguna persona encerrada en su egoísmo: hay 24 víctimas en un país condenado a vivir del turismo y el barcomenudeo, sobre la cuarentena crónica que impone un bloqueo, aun cuando no exista conmoción en el resto del planeta. Sin especulación, se amplió la provisión por libreta:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

un jabón de tocador, dos de lavar, un detergente, una lavandina, dos libras de chícharo y una pasta dental, más la canasta básica habitual. Y más inversión estatal para la producción de arroz, plátano, huevos, carne de cerdo y frijoles, pero también más remedios y más controles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Amén del tratamiento epidemiológico, se distribuyó un medicamento homeopático para elevar el sistema inmunológico y, aun con los comercios cerrados, todos los asalariados mantuvieron sus condiciones. Se sumaron subvenciones, se han congelado las deudas con el Estado y se ha garantizado la luz, el agua y el gas, reajustando presupuestos y acompañando las medidas sanitarias, con la reducción de impuestos y cuotas tributarias, **¡sin concesiones!** Ahora falta resolver cuestiones de la logística barrial, para evitar el aglomeramiento y garantizar el distanciamiento social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Las clases? Luchando, se vienen dando por televisión de acuerdo a la edad, desde la adaptación hasta la universidad, pero lo mejor del pueblo cubano sigue siendo esa mano siempre lista: 800 médicos en misión internacionalista, que hoy nos erizan la piel. **¡800 veces, Fidel!**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### ECUADOR

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ojo eh, ¡datos es-ti-ma-ti-vos! Con 7161 casos positivos y "unos 388" fallecidos, alguien podría sospechar de ciertas manipulaciones en la información, **¡pero llegaron las donaciones!** Son cajones de cartón.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde su atril, la alcaldesa de Guayaquil envía féretros reciclados, entre sanatorios colapsados y velatorios improvisados, ¡alegría para los oídos de la gente! Hay esperas de 3 días para ser atendidos "telefónicamente", porque sólo "hay disponibilidad" si empacás tu cuarentena y te vas a otra ciudad, cuidándote del virus que toma por asalto y te mata en el asfalto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lavándose las manos, levantaron 150 cuerpos humanos atacados por gérmenes no tan extraños: en estos dos años, echaron a 3 mil trabajadores de salud y ahora crece la industria del ataúd que no sirve, por supuesto, para enterrar ese 30% del presupuesto que un año atrás pudo evitar este avance.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Que en paz descanse.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gran parte de la enfermería y hasta el Hospital de la Policía tienen vetados los insumos elementales, como si el 43% de los contagiados no trabajara en los hospitales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ya renunciaron la ministra y el director del Seguro Social, porque los necios no valoraron sus maravillas: 300% de sobreprecios en las mascarillas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Las barriadas? Unos 5 millones de personas desempleadas, 62% precarizadas antes de abril y un bono social para 900 mil.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero eso sí, el último 23 de marzo priorizaron el pago al FMI, ¡326 millones de dólares están ahí! Y Lenín Moreno paseando por las Islas Galápagos, invocando marines y no precisamente de los mandarines, para cuidar a los demás. Que no vuelva. Nunca Más.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### MÉXICO

<!-- /wp:heading -->

<!-- wp:paragraph -->

"Para no dejarles un vacío de poder a los sectores más reaccionarios", López Obrador les lleno las calles a los virus más revolucionarios, que todavía no llegan a los barrios donde grita La Garganta, **¡pero coparon la playita en Semana Santa!** Ese turismo viral que nos pasea en ambulancia resultó fundamental para "la Jornada Nacional de Sana Distancia", que se incumplió estrictamente en todos lados. Y ya nos van llegando los resultados: 535 nuevos contagiados y otros 9 cuerpos velados, sólo en el escenario hospitalario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y no, en el barrio San Juan Bosco de Villa Guadalupe, Zapopan, no avanza el coronavirus, pero tampoco alcanza el pan, porque la demanda creció un 20% y al momento sigue congelada la economía informal, tan congelada como la mercadería del gobierno municipal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Previo a esta cuarentena tardía, México ya sufría diez femicidios por día, el 40% en su propio hogar. Pero esa curva también se comienza a empinar, porque aumentaron un 60% las denuncias de violencia intrafamiliar. Por lo demás, el sistema sanitario no da más, sigue faltando el equipamiento para el personal y esta semana debieron improvisar un bono extrasalarial, porque van 5014 infectados a nivel nacional: el primer caso llegó en febrero y 332 acaban de fallecer. **Un verdadero vacío de poder.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### PARAGUAY

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Paraguay no tiene dengue, el dengue tiene a Paraguay.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Un enemigo menos invisible, nuevas muertes que causan las crisis, "un enfrentamiento entre bandas", escribirían los peones de cipayos: 27 mil dengues contra 7 millones de paraguayos. Uno cada 259 del otro lado, ya cayó picado, 0,03 de cada diez. Ya murieron 53, pero los medios constituyen una gran tapia, en un país con apenas 700 camas de terapia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Itapúa, por ejemplo, hay "4" a disposición. Y nuestra olla popular en Encarnación acaba de aumentar un 33% sus comensales, gracias al aporte de otras organizaciones sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fueron insuficientes los bolsones municipales y un 80% de los trabajadores son independientes o informales, de modo que no hay paciencia, ni bono de emergencia, porque resultó tan compleja la inscripción que sólo se cobró en Asunción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No circula tan rápido como la represión, que ya dio imágenes de nuevas torturas, tan pero tan claras como oscuras. Hay una colección en video que comienza en Estrella y Montevideo, donde grabaron a un cuidacoches verdugueado y picaneado. Mala educación oficial, que hoy se presupone virtual, una gran idea que no aplica en todos los lugares: el 30% de nuestra asamblea no tiene celulares. Y del 70% que pudo comprarlo, casi nadie puede recargarlo, porque no puede laburar, ni cobrar, lógicamente. **¡Teléfono, presidente!**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### PERÚ

<!-- /wp:heading -->

<!-- wp:paragraph -->

Disuelto su parlamento y resueltas sus discusiones, para muestra valen 1.000.004 botones.

<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->

1.  A los pies del Cerro El Pino, en Lima, Maura escatima el agua para limpiar, porque no tiene cómo llegar al estanque y la bomba que llena el tanque funciona dos horas diarias, a veces, aunque la población diurna se duplicó en estos dos meses.
2.  Desde Cantagallo, Ronald grita que no hay cómo esperar a mayo: se fueron las changas, llegaron los flagelos, ya no puede ni vender caramelos y come una vez al día, porque no recibió todavía esos 380 soles del Subsidio de Emergencia atrasado, "que demuestra la presencia del Estado".
3.  Un asco: bajo el Cerro de Pasco, Marcos Castañeda gestiona desesperadamente una operación urgente para su hija Sheli Kiara. Necesitaba que viajara, pero están encerrados sin tratamiento, hacinados hasta el momento en una habitación, esperando alguna solución.
4.  Al igual que otras tres mujeres asesinadas en cuarentena, a Claudia Vera la balearon por buena frente al RENIEC, la misma entidad que le negó su identidad en el Distrito Independencia, donde reina la prepotencia del alcalde Marco Antonio Ramírez, un transfóbico, por donde lo mires.
5.  UN MILLÓN.Sí, un millón de pequeños agricultores se plantan a contramano del Gobierno peruano que, sin respuestas frente a todas sus protestas, decidió levantar el piso de su colaboración: ahora les da permiso para trasladar su producción.

<!-- /wp:list -->

<!-- wp:heading {"level":4} -->

#### URUGUAY

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Vamo' arriba,** ¿el coronavirus acecha? Todavía no tanto como la Derecha y su gobierno flamante, que no tuvo drama en patear la cuarentena para adelante, pero no pudo esperar sin elevar las tarifas en la adversidad: un 10,5% la electricidad, un 10,7% el agua y un 9,78% la telefonía, **¡más arriba no se podría!** Con la escuela suspendida, las viandas de comida se retiran por portería, a razón de 85 pesos por día, ¿cómo la ves? De propina, un bonito de 1200 por mes. Hasta ahí, ponele, todavía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero se complejiza la situación, escuchando a la asamblea del barrio Chón, donde Jeni debió hacer una fila de 40 personas para obtener los únicos alimentos que sus 4 hijos iban a comer, el día que su almuerzo fue cancelado: cuando lo retiró, estaba en mal estado

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

. Y sí, ahí quizá queda más claro por qué suena tan raro el [\#QuedateEnCasa](https://www.facebook.com/hashtag/quedateencasa?__cft__%5B0%5D=AZVEmPUPWdFjt15U-6CGflhyLhauLlvytyiLaf9YsxMEREr0VulcZ98kyGoTunBh8Lq_u4TI0uCBpdVbC9vxl-CoOLw_Lxzwd2HNJ_zErDBRM3XKVr0dMRXsEMxtU38OKBOqT3ddIogJHw3PKn7Q5y7BFCo9QYvITf1VfPh_2IulAg&__tn__=*NK-R), mientras la tele amasa sus ideas gourmet: Jaque lleva a sus hijos a la plaza, **¡para que hagan sus tareas con Internet!** Sobre los avatares sanitarios, el gobierno prometió 1000 tests diarios, que por ahora son 200.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras tanto, van 493 casos confirmados, 272 recuperados, 9 fallecidos y miles de carreros subsumidos en otro país, ese que habitan los compañeros del barrio Juntos de Nuevo París, donde Raquel llamó al médico por enésima vez, hasta que finalmente llegó, ¡3 días después! La revisaron y le diagnosticaron "coronavirus positivo, por prevención", sin hisopado, ni más confirmación que la intuición del doctor, ¿qué podría ser peor? A lo mejor contratar un astrólogo para que haga su trabajo, así no se nota tanto que les importamos un carajo, **¡vamo' abajo!**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### VENEZUELA

<!-- /wp:heading -->

<!-- wp:paragraph -->

Habitué del primer plano internacional, gracias a la moral que resguarda Derechos Humanos cuando los presuntos tiranos tienen posturas de izquierda, aunque las peores dictaduras les hayan importado siempre una mierda, hoy Venezuela tampoco goza de tal preocupación del afuera, **¡salvo que pinte votar una invasión extranjera!**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desabastecida hace años, ya no resultan extraños los sabotajes, ni los ultrajes perpetrados por distintas vertientes de la inseguridad. Y nuestro grito es un grito entero: hubo una masacre en el barrio 23 de Enero, que no se le puede endilgar a otros países, ni a la pandemia viral, ni a las guerras contra otros karmas. Se lo adjudican a Tres Raíces, un grupo paraestatal que interviene territorios con armas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Tolerancia cero para la especulación internacional. Y para toda represión estatal.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Detrás del telón, un malón de problemas nuevos, como el cartón de huevos que sufrió un aumento del 53% o el kilo de queso que trepó al pedestal y alcanzó al salario mínimo vital. De la realidad real, muchísimo aval para el Plan Nacional de Educación, mediante una buena conjugación de la televisión y los contenidos curriculares, con acceso a los sectores populares. América Latina la tiene jodida y esa Venezuela elegida para toda demonización no ha sido, ni será la excepción, pero ahora más que nunca necesitamos organización, para que ninguna solemne civilización se atribuya la categorización de ninguna rea barbarie. Al carajo la OEA, **¡aquí no se rinde nadie!**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Pagina en facebook de La Garganta Poderosa](https://www.facebook.com/gargantapodero/?ref=page_internal)

<!-- /wp:paragraph -->
