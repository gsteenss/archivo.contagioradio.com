Title: Las 5 dificultades que afronta la defensa de derechos humanos
Date: 2019-09-11 12:32
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Comunidad, Derechos Humanos, lideres sociales, territorio
Slug: dificultades-afronta-defensa-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Como parte de la conmemoración por el Día Nacional de los Derechos Humanos, la abogada **Jomary Ortegón, integrante del Colectivo de Abogados José Alvear Restrepo (CAJAR)** hizo una evaluación sobre el estado de los derechos humanos en el país. Ortegón presentó 5 preocupaciones del actual contexto nacional, pero también destacó las iniciativas territoriales de protección a los derechos.

### **Acuerdo de Paz, la aparición de nuevos grupos armados y la situación con el ELN  
** 

La defensora de derechos humanos inició señalando que el balance sobre la protección humanitaria es desalentador porque **“hace tres años estábamos suscribiendo el Acuerdo de Paz y muchos teníamos la esperanza de que eso generara mejores condiciones en términos de DD.HH. y así pasó”**; pero afirmó que aún coexisten situaciones graves que han ocurrido en el último año y la hacen pensar que la situación está muy delicada.

Una de esas situaciones es el retorno de excomandantes de la guerrilla de las FARC a la lucha armada y “los incumplimientos graves de este Gobierno al Acuerdo de Paz, que justamente se relacionan con esta decisión”. Un hecho que genera riesgos es el rompimiento de los diálogos de paz con el Ejército de Liberación Nacional (ELN), porque “nos avoca a una nueva situación de confrontación, a la que se suma la multiplicación de grupos armados sucesores del paramilitarismo, las disidencias y los grupos que no se acogieron al proceso de paz”.

### **Fallas en las políticas públicas relacionadas con los derechos humanos  
** 

En cuanto a las políticas públicas de protección de los derechos, la abogada criticó que el Plan Nacional de Desarrollo (PND) no incluya un capítulo significativo en materia de paz, asimismo, que programas como el Plan Nacional Integral de Sustitución (PNIS), los Planes de Desarrollo con Enfoque Territorial (PDET) y las instituciones del Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR) no cuenten con el presupuesto necesario para cumplir su función.

A ello se suma la crítica al Plan de Acción Oportuna (PAO) creado para proteger la vida de las y los líderes sociales, pero que, **según defensores de derechos humanos, tiene un énfasis muy militar.** (Le puede interesar: ["Cada tercer día es agredido un líder político"](https://archivo.contagioradio.com/tercer-dia-agredido-opositor-del-gobierno-actual/))

### **¿Interlocución, o no, con el Gobierno?**

Respecto a la interlocución que el Gobierno ha intentado mantener con las organizaciones sociales y defensoras de derechos, Ortegón manifestó que se podrían dar dos lecturas: Que es posible contribuir en la protección de los derechos humanos pese a una línea ‘guerrerista’ que ha expresado el Gobierno; o, que **la participación con el Gobierno en la creación de una política pública significaría la aprobación de esta línea hostil por parte de las organizaciones.**

### **Las propuestas para seguir defendiendo los derechos humanos  
** 

La abogada aseguró que los aspectos incumplidos del Acuerdo de Paz siguen siendo una oportunidad para mejorar la situación de derechos humanos, en ese sentido, haría falta voluntad estatal para convertir esta oportunidad en una realidad. Por otra parte, Ortegón destacó las propuestas de protección y autoprotección generadas por las comunidades para garantizarse sus derechos. (Le puede interesar: ["La MOE ha registrado 228 hechos de violencia contra líderes sociales en últimos 8 meses"](https://archivo.contagioradio.com/moe-228-hechos-violencia-lideres/))

**Síguenos en Facebook:**  
<iframe id="audio_41569661" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41569661_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
