Title: Memoria Televisiva
Date: 2015-09-10 16:29
Category: Opinion, superandianda
Tags: Crisis en la frontera Colombia Venezuela, Manipulacion mediatica, Medios empresariales, Memoria televisiva
Slug: memoria-televisiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/7840_967_544.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **Por [Superandianda ](https://archivo.contagioradio.com/superandianda/) - [@Superandianda](https://twitter.com/Superandianda)  **

###### [10 Sep 2015] 

[Parece ser que la mayor enfermedad que sufre Colombia no es la violencia, la inseguridad, la desigualdad y mucho menos la corrupción; la enfermedad más grave que sufre Colombia es la memoria televisiva, haciendola incapaz  de reconocer su historia si no es proyectada  por la pantalla del televisor.]

[La memoria televisiva es selectiva y su tarea es señalar solo los acontecimientos que le convienen  y archivar aquellos otros que nadie quisiera recordar, es indignarse ante una realidad que venden los grandes medios y ser indiferentes ante la realidad que vemos directamente y que no es publicada en televisión, siendo empleada como herramienta de olvido de los problemas que quieren ser borrados de la memoria social. Entiendo el dolor de patria de la mayoría de colombianos ante la reciente crisis en la frontera venezolana, no es nada agradable ver personas huyendo dejándolo todo, dejando la tierra donde por años vivieron, pero lo que personalmente me parece un absurdo es  seleccionar el dolor y clasificarlo. Es cruel pensar que el dolor que nos despierta el televisor no es el mismo dolor que debería despertar nuestra realidad social.]

[Nuestro país ha sufrido por décadas desplazamientos internos, poblaciones enteras que han tenido que dejarlo todo huyendo de la muerte y del abuso, las víctimas de la violencia han soportado la indiferencia del estado y han tenido que callarse cuando han sido amenazadas por contar sus historias; entonces, sin pretender restarle importancia a nuestros compatriotas de la frontera, sin ser indiferente ante la gran problemática con nuestro país hermano,  quiero señalar que muchos de los que hoy vuelven a Colombia sin nada, son los mismos que se habían ido desplazados de sus tierras. No le queda bien a Álvaro Uribe Vélez sembrar populismo regalando mercados si su gobierno fue el gobierno de los desplazamientos por la violencia y  el terror.  ]

[El nulo análisis sobre nuestra realidad ha hecho que Colombia sea un país lleno de odios y pasiones fundados con escasos argumentos,  perfectamente con la misma euforia que celebra  un partido de fútbol es capaz de indignarse por  la “realidad” que vende el programa de información, es esa manera de sentir sin conciencia y sin reflexiones profundas, lo que hace que reaccione frente a una noticia amarillista de RCN pero a la misma vez pueda tener el poder de ignorar la miseria que lo rodea.]

[Gracias a la memoria televisiva colombiana los verdugos de la clase vulnerable hoy son redentores, quienes nos gobiernan son los canales de televisión privados que derrochan su talento creativo para seguir manteniendo un país ignorante  sometido a la orden del día, creando héroes y villanos a su antojo, jactándose de sensacionalismo. Ahora resulta que debemos de odiar a todos los venezolanos, como si las personas fueran culpables de pertenecer aquí o allá. Tenemos que amar y rechazar lo que la pantalla diga, hoy nos dicen que se está cometiendo una violación de derechos humanos, las mismas violaciones que vivimos día a día internamente y que ningún medio privado se encarga de recordar.]

[Por mi parte repudio quienes fragmentan sus emociones, quienes son capaces de clasificarlas, sin importar la procedencia de las víctimas, todas deben existir en nuestra memoria, en nuestra memoria social, la verdadera memoria de un pueblo.]
