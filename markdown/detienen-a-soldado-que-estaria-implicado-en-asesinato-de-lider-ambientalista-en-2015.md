Title: Detienen soldado de la Brigada XVI por asesinato de líder social Daniel Abril
Date: 2017-06-08 13:59
Category: DDHH, Nacional
Tags: Ambientalistas. MOVICE, asesinato, Casanare, Daniel Abril
Slug: detienen-a-soldado-que-estaria-implicado-en-asesinato-de-lider-ambientalista-en-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Daniel-Abril.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [08 Jun. 2017] 

Luego de 2 años y 7 meses de haberse cometido el asesinato del líder defensor del ambiente Daniel Abril Fuentes en Casanare, este jueves se conoció la **captura de dos de los autores materiales del crimen, uno de ellos el soldado profesional de la Brigada XVI** Eliecer Anzueta Cero y el civil Jhonnever Tumay Tuay a quienes se les imputa el cargo de homicidio agravado. Le puede interesar: [Asesinato de Daniel Abril: impacto fuerte para movimiento social casanareño](https://archivo.contagioradio.com/asesinato-de-daniel-abril-impacto-fuerte-para-movimiento-social-casanareno/)

Según información de Prensa Libre Casanare, en dos operativos efectuados por el CTI y la Unidad Nacional de Derechos Humanos, se realizaron las detenciones de los dos hombres, quienes deberán rendir indagatoria para continuar con las investigaciones en el proceso abierto. Los familiares de Abril, saludan estas acciones, pero esperan se identifiquen los autores intelectuales del crimen.

### **¿Por qué asesinaron a Daniel Abril?** 

**Daniel Abril era un líder campesino, defensor del ambiente**, Presidente de la Junta de Acción Comunal de la Vereda “Los Chochos” en Casanare y de otras organizaciones sociales de Centro Oriente desde donde apoyaba las luchas campesinas de la región y del país.

**Era reconocido por su acérrima posición en contra de la industria petrolera** y de la presencia de esas empresas en Casanare, razón por la cual la comunidad cree que fue asesinado.

En el año 2014, Abril denunció que **había sido víctima de un intento de desaparición forzada por parte de agentes del Estado,** razón por la cual contaba con medidas de protección de la Unidad Nacional de Protección. Le puede interesar: [729 defensores de DDHH en Colombia han sido asesinados en los últimos 20 años](https://archivo.contagioradio.com/729-defensores-de-ddhh-en-colombia-han-sido-asesinados-en-los-ultimos-20-anos/)

Durante ese mismo periodo adelantó con organizaciones de Derechos Humanos **denuncias contra Corporinoquia por su falta de diligencia en su trabajo** y la complacencia del accionar de las petroleras en el Casanare.

Daniel Abril también hacia parte de plataformas de movilización social como la Voz de la Tierra, el MOVICE, el Congreso de los Pueblos, el Comité Cívico por los Derechos Humanos del Meta y trabajaba articuladamente con la Corporación COSPACC y la Corporación Claretiana Norman Pérez Bello.

### **En contexto asesinato de Daniel Abril** 

**El asesinato de Abril, sucedido el 13 de noviembre de 2015** mientras departía con otras personas en un establecimiento público en el municipio de Trinidad – Casanare, generó el repudió de organizaciones nacionales e internacionales que lo conocieron y fueron testigos de su trabajo. Le puede interesar: [CIDH rechaza asesinatos y amenazas a defensores de DDHH en Colombia](https://archivo.contagioradio.com/cidh-rechaza-asesinatos-y-amenazas-a-defensores-de-ddhh-en-colombia/)

“Fue un llanero valiente y noble, quitado del camino de los intereses de las multinacionales del petróleo, de los despojadores de tierra a quienes denunció, como a los corruptos del Estado a quienes señaló sin reservas”, manifestaba el Movimiento de Víctimas de Crímenes de Estado - MOVICE - para la fecha.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
