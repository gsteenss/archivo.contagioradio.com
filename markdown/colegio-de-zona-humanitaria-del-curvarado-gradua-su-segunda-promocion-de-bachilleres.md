Title: Colegio propio de las comunidades del Curvaradó gradúa su segunda promoción de bachilleres
Date: 2015-12-08 09:53
Category: Comunidad, Nacional
Tags: AFLICOC, Colegio de la AFLICOC, Comisión Intereclesial de Justicia y Paz, Comunicadores populares CONPAZ, Conversaciones de paz de la habana, Segunda promoción Colegio AFLICOC, Zonas Humanitarias
Slug: colegio-de-zona-humanitaria-del-curvarado-gradua-su-segunda-promocion-de-bachilleres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/IMG-20151207-WA0010.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunicadores CONPAZ- Curvaradó] 

###### [7 Dic 2015]

[15 estudiantes obtuvieron su título como bachilleres en la segunda promoción del Colegio de la Asociación de Familias de los Consejos Comunitarios de Curvaradó, Jiguamiandó, Pedeguita, Mansilla, Vigía del Curvaradó y la Larga-Tumaradó AFLICOC, una **experiencia de educación propia construida por las comunidades en proceso de retorno del Bajo Atrato chocoano**, desde el año 2013 y con el acompañamiento de la organización de derechos humanos 'Comisión Intereclesial de Justicia y Paz'.  ]

[**'Construyendo futuro desde la resistencia'** es la frase que identifica esta propuesta educativa que nace por las necesidades de las comunidades de **fortalecer sus saberes para retornar a sus territorios**, tiene como sede la Zona Humanitaria Las Camelias, situada en la cuenca del Rio Curvaradó. La ceremonía de graduación de este fin de semana contó con las **palabras de patriarcas, matriarcas y de los profesores** que han mostrado su apoyo durante todo este proceso de pedagogico,]

["Este es el **resultado de la lucha de años atrás**... hoy damos gracias a la Comision Intereclesial de Justicia y Paz, también a los profesores y profesoras que nos han dado su conocimiento. Esperamos nos sigan apoyando, para con ustedes **seguir construyendo paz y tener más logros en la educacion y en este proceso de resistencia**. Gracias de nuevo a los y las que nos han ayudado a obtener este logro de corazón".]

[Por: Comunicadores CONPAZ]
