Title: Las Mujeres tejen paz desde los territorios
Date: 2017-03-10 17:30
Category: Mujer, Nacional
Tags: construcción de paz, enfoque de género en los acuerdos de paz, mujeres rurales
Slug: las-mujeres-tejen-paz-desde-los-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mujeres-rurales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CINU] 

###### [10 Mar 2017] 

Participación política, formación para la participación, redes de mujeres en los territorios, fomento de proyectos productivos para la economía campesina, adjudicación y titulación de tierras, **fueron algunas de las demandas y apuestas que las mujeres rurales manifestaron en el Primer Encuentro de Mujeres Construyendo Paz desde los Territorios.**

En el marco del lanzamiento del nuevo espacio radial de Contagio Radio, Libertades Sonoras, un programa que trabajará temas de mujer y género, se realizó un encuentro con mujeres rurales de Chocó Antioquia, Cauca, Meta, Valle del Cauca y Putumayo, **en dicho espacio se socializó con más de 50 mujeres el enfoque de género de los acuerdos de paz y se plantearon problemáticas y retos para lograr su efectiva implementación.**

Respecto de las problemáticas, todas las mujeres coincidieron en que no es posible hablar de paz si no hay un desmonte real de las estructuras paramilitares, que siguen atemorizando a las comunidades, **“nos estan asesinando, nos siguen violando, siguen vulnerando nuestros derechos y el Estado se queda callado**”, manifestó Nidiria una mujer afro de la cuenca del Río Naya. ([Le puede interesar: Mujeres rurales siguen exigiendo garantía de sus derechos](https://archivo.contagioradio.com/mujeres-rurales-siguen-exigiendo-garantia-de-sus-derechos/))

Las mujeres señalaron que uno de los mayores retos es ganar espacios representativos dentro de la democracia colombiana, un lugar que históricamente ha sido negado a esta población. Aseguraron que para ello es fundamental crear y fortalecer estrategias para **visibilizar y comunicar las apuestas, las realidades y las preocupaciones, a través de escenarios como medios y emisoras comunitarias.**

Hablaron de la importancia de generar intercambios de saberes con mujeres de distintas regionas, **para impulsar proyectos productivos que contribuyan a la economía de sus comunidades y a su vez proporcionen mayor autonomía e independencia a estas mujeres.** En este punto también mencionaron que esto podría contribuir al punto de solución al problema de drogas ilícitas y a la sustitución de cultivos.

###  

Por último, recibieron con alegría el espacio del encuentro y resaltaron que es necesario dar continuidad a este tipo de procesos **“para que cada vez más, las muejres nos unamos para exigir nuestros derechos, cambiar pensamientos machistas en las comunidades** y contribuir a construir la paz en Colombia”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
