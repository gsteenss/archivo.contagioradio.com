Title: "Lo que está en juego es la independencia de la justicia en Colombia"
Date: 2015-07-28 11:16
Category: DDHH, Judicial
Tags: 13 jóvenes capturados, atentados Porvenir, congreso de los pueblos, Explosiones en Bogotá, Falsos Positivos Judiciales
Slug: hoy-se-decide-si-se-dicta-o-no-medida-de-aseguramiento-contra-los-13-jovenes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/los-13-jovenes-detenidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/David-Uribe.mp3"\]\[/audio\]

##### [David Uribe, abogado ] 

Desde las 9 de la mañana se realizará un cacerolazo frente a los juzgados de Paloquemao en apoyo a los 13 jóvenes capturados, debido a que hoy se decidirá si se dicta o no medida de aseguramiento en centro penitenciario mientras se adelanta el proceso de investigación, por las manifestaciones sociales del pasado 20 de mayo en la Universidad Nacional.

David Uribe, abogado de la bancada de la defensa, afirma que **“lo que está en juego en los juzgados de Paloquemao es la independencia de la justicia en el país”.** Según él, se espera que la juez sea independiente, autónoma y valore las pruebas que se han presentado desde la defensa, y las pocas que tiene la Fiscalía, para llegar a la conclusión de que no es necesario privarlos de la libertad.

En la audiencia se espera que no se dicte medida de aseguramiento teniendo en cuenta que los jóvenes no demuestran ningún peligro para la sociedad, siendo esta la misma que los está defendiendo, lo que el abogado califica como un argumento simbólico.

“Cuando se habla de peligro para la comunidad, de qué **comunidad se está hablando, si es la comunidad movilizada que está exigiendo la libertad; o la comunidad internacional, que está pidiendo garantías judiciales y la comunidad académica que ha respaldado el trabajo político y académico de algunas de las personas detenidas**… entonces la gran pregunta es ¿qué peligro para la sociedad es el que se está argumentando?”, dice David Uribe.

El abogado, señala que las pruebas que se tienen para justificar la medida de aseguramiento  han sido pruebas de último momento. Se trata de correos amenazantes a la vigilancia privada de la Universidad Nacional, que son “**coincidencialmente presentadas antes de la audiencia”**, lo que perjudica directamente a los jóvenes a quienes les sería imposible hacer este tipo de actuaciones estando privados de su libertad.

Cerca de **50 amigos y familiares acompañan a los 13 jóvenes en la audiencia y alrededor de 200 personas** están en el plantón a las afueras del complejo judicial apoyándolos.

Finalmente, el abogado resalta que los **allanamientos y capturas irregulares han sido legalizadas con decisiones arbitrarias e infundadas**. Mientras tanto, según la defensa, los jóvenes están reconfortados por el apoyo que han recibido desde distintos sectores del país, y “**continúan firmes para enfrentar este proceso judicial con dignidad y valentía”.**
