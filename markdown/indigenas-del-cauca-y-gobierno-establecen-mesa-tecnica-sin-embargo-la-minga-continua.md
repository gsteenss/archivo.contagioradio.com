Title: Indígenas y Gobierno establecen mesa técnica pero la Minga continúa
Date: 2019-03-22 15:31
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Cauca, CRIC, Minga en el Cauca
Slug: indigenas-del-cauca-y-gobierno-establecen-mesa-tecnica-sin-embargo-la-minga-continua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-21-at-16.51.36.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CRIC 

###### 22 Mar 2019

Este 21 de marzo las comunidades indígenas del Cauca que permanecen en Minga desde hace 12 días, establecieron una mesa técnica con el Gobierno representado por la **ministra del Interior, Nancy Patricia Gutiérrez. S**e espera poder organizar una metodología de acuerdos y preparar una posterior llegada del presidente Iván Duque al departamento,  hasta entonces, la Minga continúa.

Según **Giovanni Yule, dinamizador político del Consejo Regional Indígena del Cauca (CRIC)**  en la mesa se plantearán tres espacios, el primero **con relación a la garantía y el respeto de los DDHH en el marco de la protesta**, y poder establecer protocolos con el Gobierno y la Fuerza Pública que aseguren que no se atacará a la población civil que están en el ejercicio de la Minga.

El segundo punto abordará la implementación de políticas y resolución de las exigencias que han realizado los pueblos indígenas frente a **"derechos económicos, sociales y culturales, los cuales han sido sistemáticamente incumplidos"** explica Yule, quien agrega que un tercer punto estará ligado a la comisión política para coordinación del diálogo entre la Minga y el Gobierno.

Aunque desde el **CRIC** han planteado al Gobierno que el presidente Duque llegue el próximo lunes 25 de Marzo,  se desconoce cuál podría ser la fecha acordada, lo cierto es que la ministra Nancy Patricia Gutiérrez,  ya manifestó que por ahora se preparará la ruta para acordar la llegada del mandatario.[(Le puede interesar: Ausencia de diálogo ha dejado seis heridos y un muerto en Cauca) ](https://archivo.contagioradio.com/ausencia-dialogo-heridos-cauca/)

Yule es enfático en que las comunidades siempre han priorizado existido el diálogo por lo que esperan se establezca una conversación seria y respetuosa. Por esta razón "hasta que llegue el señor presidente la Minga permanecerá en los puntos establecidos"  buscando evitar también la dilación del acuerdo al que se podría llegar.

El dinamizador político expresó su apoyo a los familiares y las víctimas de la explosión ocurrida en Dagua, Valle del Cauca, "rechazamos lo que sucedió con nuestros hermanos indígenas en el Valle del Cauca, esperamos que los resultados de la investigación aclaren la situación". concluyó.

<iframe id="audio_33629321" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33629321_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
