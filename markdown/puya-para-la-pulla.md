Title: Puya para La Pulla
Date: 2017-08-16 11:09
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: medios colombianos, politica, Venezuela
Slug: puya-para-la-pulla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/puya-a-la-pulla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Montaje Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 16 Ago 2017 

*[“Hay que hacerse el imbécil para no saber que hay gato encerrado”]*[ denunciaba aceleradamente, como es costumbre, María Paulina Baena, conductora de La Pulla del diario El Espectador quien para su entrada del 10 de agosto de 2017 nos dio una opinión explicando:]*[Así se convirtió Venezuela en una dictadura.]*

[La desventaja de estar escribiendo  y no hablando en cortos superpuestos como pequeños fragmentos que le otorgan un sentido frenético, acelerado y medio embejucao como lo que se narra en La Pulla, es que aunque se puso de moda hablar golpeado, con tinte regañón y aceleradamente, es pertinente detenerse un momento ante todo el guión que lee esta mujer y desatar más pausadamente eso que no es pulla, sino una puya que pretende en 7 minutos definir cómo se convirtió Venezuela en una dictadura…]

[Sé también, que está de moda ser Youtuber, que eso de la historia y la política “¿cómo para qué?”...sé que hoy muchos periodistas no necesariamente son comunicólogos entonces, sale más provechoso hacer un buen show exprés (porque que mamera leer) de una denuncia coyuntural, antes que explicar críticamente así sea con una buena pulla lo que pasa en Venezuela.  ]

[La Pulla del 10 de Agosto, dio unos argumentos que cantan al unísono con los de todos los medios internacionales con agendas políticas bien particulares y hasta parecidas. Si bien dejó sobre la mesa varios argumentos, lo único que no reconoció, (tal vez por la agenda política del Grupo Santo Domingo, que está detrás de La Pulla) es que si usamos los argumentos con los que definió la dictadura en Venezuela, entonces Colombia se convirtió en una dictadura hace tiempo, y según lo narrado por María Paulina, queda la sensación de que sencillamente aquí en Colombia nos seguimos haciendo los imbéciles o nos siguen creyendo imbéciles y no precisamente con el tema de Venezuela…]

[Que Chávez escogió uno a uno a su gabinete, a sus delegados en los poderes y que a éstos no les importó ser idiotas útiles… Cabría preguntarnos aquí en Colombia ¿a dónde se vota por procurador, por director del antiguo DAS, por ministros, por contralor, fiscal anticorrupción, por director de la DIAN, por director del DANE, por director del DNP etc.? habría que preguntarnos si entonces, el caminito feliz de pasar de gerente de la Federación de Cafeteros o de gerente de la ANDI a ministro de defensa y luego a la embajada en Washington y luego a las precandidaturas presidenciales significa ser un idiota útil… ¡¡idiota útil ni siquiera ricostilla a quien le dieron un consulado en San Francisco solo por jugar bien al golf!!! Eso… idiotas útiles solo allá en Venezuela… como ño moñito.]

[La Pulla denuncia que cuando “]*[madurito necesitó que le aprobaran leyes se las aprobaron]*[” como si aquí en Colombia las gaseosas hubieran dejado de pagar impuestos al alza del azúcar como por arte de magia, o porque son tan deliciosas que no merecen el alza; como si aquí los que serían dueños de las EPS no hubiesen puesto plata para la ley 100, o los industriales para la ley 50, o entre oligarcas y terratenientes para la ley de justicia y paz de 2005; como si Odebretch no hubiese demostrado que estuvo siempre detrás de todo financiándolos a todos porque al final con todos ganaba, pues el poder es estático, la falsa democracia se vive en las redes sociales y con el favorcito de los medios masivos que solo ven demonios a un lado pero no en el otro.]

[Que Maduro dio contentillo \$\$\$\$ a los militares, como si aquí en Colombia los militares no cogieran bien el cuchillo y el tenedor, no estuvieran detrás de miles de escándalos o no les pidieran la baja a los que quieren estudiar para jefes de Estado, porque otro Pinilla no se lo aguantarían de nuevo… si ni siquiera los militares hicieron nada contra Samper, es porque aquí viven del “contentillo” mucho antes de que naciera María Paulina Baena.]

[Denuncia a los “chavistas y los grupos de gente bondadosa como los paramilitares en Venezuela”…  ¿y aquí? Hay que dar un paseíto por la escombrera en Medellín, sacar las actas del pacto de Ralito a ver si el paramilitarismo no existe, que La Pulla le dé un abrazo a  Villegas si quiere; un tipo que insiste en que aquí en Colombia no hay paramilitares, debe ser que lee mucho y por eso está tan gordo.  ]

[Que Chávez y los medios de comunicación… dejen a Chávez en paz y critiquen a Maduro que así sea cagándola se puede defender, aunque sea por respeto los muertos que muertos están, pero ni eso… Aunque sea por respeto, reconocer que entre más se quejan de la poquísima libertad de expresión, el mundo más odia a Maduro y hasta los portugueses saben más de Venezuela que de Portugal, cada vez más y más gente se expresa y se expresa en todas partes… dicen que no hay libertad de expresión pero aquí nadie habla cosas diferentes de Venezuela ¿será por arte de magia el consenso en el discurso?… no jodás.]

[Los medios informativos masivos tienen agenda política, por consiguiente no son imparciales… por tanto, como dijo Rafael Correa: ¿dónde dice que están por encima de la ley? ¿quién dijo que toda opinión es una verdad política? que viva la libertad de expresión y que vida la libertad de dudar de cualquier expresión si como la de la La Pulla para ésta ocasión lo amerita.  ¿O acaso los intereses del Grupo Santo Domingo no puyan posición editorial de La Pulla?  ]

[Le pregunta, en tono y performance desafiante, la conductora de la Pulla a Maduro (bueno, eso suponiendo que Maduro con tanto lío tiene tiempo para ver La Pulla)]*[“¿Cuál es el miedo de que la gente participe?”]*[ …  ¿y en Colombia? …… ¿¿¿¿¿¿¿¿¿¿¿¿será que aquí las más de 150 personas asesinadas entre 2016 y 2017 que hacen política sin sueldo acaso tenían miedo????????????????????]

[Por qué no hablamos de esta dictadura… ¿¿¿jueces comprados en Venezuela??? ¿acaso aquí no los compran y desde siempre? ¿militares untados en Venezuela? ¿y aquí en Colombia acaso no estuvieron detrás del asesinato de gente inocente a la que le pusieron ropa de guerrilleros con el fin de satisfacer a las masas que querían ganar una guerra sin lucharla y que adoran los análisis exprés de la política, con un tinte así super clase media al mejor estilo de La Pulla?  ]

[Políticos opositores encarcelados en Venezuela… ¿en Colombia acaso la rebelión no es un delito?? ¿y nuestros presos políticos? Claro, que Arizmendi no llame todos los días a las familias para preguntar cómo se sienten, no significa que no existan…. También los hay… ¿no le contaron eso a María Paulina cuando la invitaron a Caracol Radio? Ese Arizmendi es tremendo… tremendo jugador de golf.]

[Que el gobierno, los magistrados y  el CNE  de Venezuela funcionan confabulados…. Yo no sé si eso es un argumento o un chiste… en Colombia el procurador ambiental, el ministro de ambiente, el gerente de Ecopetrol (y sus extraterrestres) van de frente contra la democracia directa, contra las consultas mineras etc... posiciones tiránicas envueltas en discursos con bastante marketing y anglicismos no quitan la perversión de una democracia violada… y a una democracia violada ¿por qué no le llaman con tanto desespero “dictadura”?  ]

[Que Maduro no preguntó a nadie para cambiar la constitución, como si en Venezuela no se hubieran dado la mayor cantidad de procesos electorales frente a otros países de la región… (claro como perdían las elecciones no decían nada) es no más que gane el que no está en la agenda y de una gritan “¡fraude!” pero si gana el que está en la agenda gritan “¡democracia!” tal parece que no nos hacemos los imbéciles, sino que nos creen imbéciles… Ala, ¿y aquí en Colombia? ¿Uribe preguntó? Ah si, a su congreso de bolsillo, investigado, prófugo y preso en mansiones...]

[Denuncia a los matones de Maduro que mataron inocentes, pero ni una puya en La Pulla por las personas inocentes quemadas vivas por los opositores antichavistas.]

[Denuncia que la estrategia del gobierno venezolano ¡¡que su delirio!! es confundir... como si Juan Carlos Vélez, como si Uribe, como si Santos, como si los extraterrestre de Echeverry a través de RCN, Caracol, El espectador, El tiempo, Caracol Radio etc. etc. no hubiesen respondido a sus agendas políticas…. ¿acaso responder a una agenda política no es la siembra argumentativa de la defensa de un grupo de poder?]

[Resume la conductora de La Pulla, que cuando el poder queda en pocas manos, ¡¡esa!! ¡esa es la dictadura!... claro, individualizar en Maduro las desgracias de Venezuela es una gran agenda política de los medios (que se expresan como quieren) no obstante, en Colombia ni siquiera son capaces de individualizar a Uribe, y los candidatos políticos siguen distrayendo con que el problema es la corrupción, pues claro, “no se nota” aquí no hay crisis, hay escándalos, aquí no hay dictadores, hay amigos corruptos… todos juegan golf en el mismo club, pero al ser varios, pasan desapercibidos… dictador el vecino, imbéciles nosotros.  ]

[Sí. En Venezuela según los argumentos exprés de La Pulla: hay una dictadura.  Los mismos argumentos, son tan válidos y objetivamente más alarmantes en Colombia… si se asume la explicación exprés de La Pulla, entonces concluyamos coherentemente y dejemos de ser tan imbéciles, el gato no está encerrado: EN COLOMBIA HAY DICTADURA.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
