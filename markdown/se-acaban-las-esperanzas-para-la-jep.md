Title: ¿Se acaban las esperanzas para la JEP?
Date: 2018-06-26 16:29
Category: Nacional, Política
Tags: Camila Moreno, Centro Democrático, ICTJ, Iván Duque, JEP
Slug: se-acaban-las-esperanzas-para-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/ojo-a-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Ojo a la paz] 

###### [26 Jun 2018] 

Un nuevo obstáculo impediría que la JEP entre en funcionamiento luego que el presidente electo Iván Duque, afirmara que habrán modificaciones en esta Jurisdicción con penas proporcionales y que el Centro Democrático propusiera abrir una sala, al interior del Tribunal de paz, para los procesos de los militares. Transformaciones que de acuerdo a organizaciones defensoras de derechos humanos, **no solo acabarían con su espíritu, sino que tampoco garantizarían una verdad, justicia y reparación**.

Ambas modificaciones deberán pasar por el Congreso de la República y exigirían una reforma Constitucional, que tendría que volver a ser evaluada por la Corte  Constitucional y que podrían tardar incluso un año más, tiempo en que la JEP podría seguir operando como ahora, sin todas sus facultades. (Le puede interesar: ["Misión de la ONU pide remover obstáculos de las instituciones a la JEP"](https://archivo.contagioradio.com/mision-de-las-naciones-unidas-pidio-remover-obstaculos-de-las-instituciones-a-la-jep/))

### **¿Hay garantías para la JEP en el Congreso?**

De acuerdo con Camila Moreno, directora del Centro Internacional de Justicia Transicional en Colombia, cada vez la incertidumbres es más grande sobre las víctimas y personas que se acogieron para comparecer ante este Tribunal, tanto de la Fuerza Pública como de otros sectores, debido a "los palos en la rueda" que se le ponen a la JEP.

Frente a los acuerdos a los que se pueda llegar en el Congreso de la República, Moreno fue enfática en señalar que "no se trata solo la presión porque salgan normas que le permitan trabajar a la Jurisdicción, **sino que hay que garantizar que la JEP pueda desarrollar su trabajo tal y como se explicó en el modelo**".

### **Las propuestas del Centro Democrático** 

En relación a la propuesta que hizo el Centro Democrático de crear una nueva sala en la Jurisdicción que se encargue de procesar a los militares que se acojan a ella hay puntos encontrados. Por un lado el partido afirma que la necesidad de esto radica en la desconfianza que tienen algunos militares de acogerse a los jueces que conforman el Tribunal.

Del otro lado, el General Alberto Mejía, Comándante de las Fuerzas Militares, le pidió al Senado aprobar el reglamento de la JEP, tal y como fue presentado, **para proteger a los 2.159 soldados que se acogieron al mismo y que ya han recibido beneficios**.

En ese sentido, la directora de la ICTJ señaló que crear un procedimiento distinto para los miembros de la Fuerza Pública lo que podría generar es una ruptura del principio de cimetria en el tratamiento que "no le convendría a nadie". Además, recordó que la Corte Penal Internacional tiene sus ojos puestos sobre la JEP, ya que podría ser la última oportunidad que tiene Colombia para cumplir con su **obligación de investigar y sancionar a los altos mandos militares que estan bajo la lupa de la Fiscalía de la CPI**.

<iframe id="audio_26746756" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26746756_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
