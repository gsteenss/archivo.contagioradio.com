Title: Colombia inicia el 2016 con pasos hacia una crisis económica
Date: 2016-01-07 10:11
Category: Economía, Otra Mirada
Tags: ISAGEN, salario minimo, SIndicatos, Trabajadores
Slug: crisis-economica-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/economia-calles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Ene 7 2015 

Tras conocerse la cifra de inflación para el 2015, que según el DANE estuvo en 6.77% y con el reciente aumento del salario mínimo en Colombia de apenas el 7%, economistas e integrantes de las centrales obreras denuncian que habrá una pérdida de poder adquisitivo que arrastraría a Colombia a la agudización de la crisis económica. Además podría declararse ilegal el decreto del aumento dado que para los estratos 0, 1 y 2 la inflación superó el 7.2%.

### **Trabajadores de Colombia pierden poder adquisitivo** 

Varios economistas consultados por Contagio Radio afirman que la crisis económica recaería sobre los hombros de cerca de 8 millones de personas que devengan el salario mínimo y para aquellos supuestos beneficiados con la “ley del primer empleo” que permite el pago de salarios incluso por debajo del mínimo legal vigente.

Por otra parte el DANE  reveló el índice de precios al consumidor, el rubro que más aumentó fue alimentos, que se incrementó cerca de un 10%, seguido de vivienda y educación con un 5% aproximado cada uno, lo que indica que ese grupo de derechos es el más afectado por el fenómeno de la inflación.

Analistas afirman que la actitud del gobierno no es coherente con el déficit en Colombia puesto que se pretende vender ISAGEN para financiar las vías 4G lo que no representa una inversión con devolución en corto, mediano o largo plazo. Por otra parte se critica que se realicen aumentos salariales a congresistas lo cuales devengaran durante 2016 un monto superior a los 25 millones de pesos. Situación similar a la de los integrantes de las FFMM que durante el 2015 vieron un aumento salarial superior al del resto de trabajadores colombianos.

### **Precio internacional del petróleo y sus consecuencias en Colombia** 

La venta del petróleo financia gran parte del presupuesto nacional, representa el 20% del total del ingreso estatal, para 2015 se esperaba, sin déficit fiscal, que los precios internacionales del crudo se mantuvieran en 95 dólares por barril o incluso un pequeño porcentaje menor, sin embargo, el año cerró con un precio de 40 dólares y 2016 arranca con un record de 35 dólares barril, por fuera de cualquier proyección económica.

### **Más impuestos para la mayoría de los trabajadores** 

Aunque se afirma que el déficit estructural provocado por la caída de los precios del petróleo se va a solventar con el aumento de las deudas internacionales y no con el elevamiento de los impuestos, el gobierno de Colombia está optando por una solución de dos vías. Mayor endeudamiento con los fondos internacionales y una reforma tributaria que amplía la base de gravamen del IVA y lo aumenta al 19%, con lo cual supliría apenas un pequeño porcentaje del déficit.
