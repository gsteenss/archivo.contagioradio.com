Title: Exjefes paramilitares y de las FARC-EP piden ser escuchados por la Comisión de la Verdad
Date: 2020-11-29 10:26
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Carlos Antonio Moreno Tuberquia, Comisión de la Verdad, Hector Germán Buitrago, Martín Llanos, pastor alape, verdad
Slug: exjefes-paramilitares-farc-piden-ser-escuchados-comision-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Exfarc-y-exparamilitares-piden-ser-escuchados-en-la-Comision-de-la-Verdad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

El exjefe de las extintas FARC-EP, Pastor Alape, los exjefes paramilitares Héctor Germán Buitrago, alias “Martín Llanos", Nelson Buitrago y Héctor José Buitrago de las Autodefensas Campesinas del Casanare -ACC-, y Carlos Antonio Moreno Tuberquia exintegrante de las Autodefensas Unidas de Colombia -AUC- y del Clan del Golfo; **solicitaron a la Comisión de la Verdad -[CEV](https://comisiondelaverdad.co/)-, escuchar sus versiones en una audiencia pública con presencia de los medios de comunicación sobre las verdades que guardan respecto al conflicto armado en Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así lo confirmó Francisco de Roux, presidente de la Comisión de la Verdad quien señaló haber recibido cartas provenientes desde las cárceles de Cómbita en Boyacá y La Picota en Bogotá donde se encuentran recluidos los exparamilitares. (Lea también: [Exparamilitares dicen sí a la verdad](https://archivo.contagioradio.com/exparamilitares-dicen-si-a-la-verdad-para-reparar-a-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De Roux señaló que **la Comisión se encuentra sistematizando y organizando las versiones que llegan provenientes de personas que se situaron en distintas orillas del conflicto,** **contrastándolas para llegar a una comprensión integral de la verdad.** Asimismo señaló que la tarea de la Comisión es recopilar los relatos de las víctimas, los paramilitares, los excombatientes de FARC, las Fuerzas Militares y los empresarios para “*comprender qué fue lo que nos pasó y por qué nos pasó y qué intereses movían estas realidades tan duras que se vivieron*”. (Le puede interesar: [Rodrigo Londoño se comprometió con el «Movimiento Nacional por la Verdad»](https://archivo.contagioradio.com/rodrigo-londono-se-comprometio-con-el-movimiento-nacional-por-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Me ha sorprendido la cantidad de gente que quiere contribuir con la verdad; y no una verdad para acrecentar rupturas, señalamientos, estigmatizaciones, sino una verdad que nos ayude a comprender por qué \[terminamos en este conflicto\] y qué podemos hacer para salir de él”.
>
> <cite>Francisco de Roux, presidente de la Comisión de la Verdad</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otra parte, De Roux señaló que **la Comisión no es una entidad que sirva como una especie de puente para que los declarantes ingresen a la Jurisdicción Especial para la Paz -JEP-,** pero que, en aras de hacer una construcción integral de la verdad recibe las versiones, no solo de los responsables, sino de las víctimas y todas aquellas personas que puedan contribuir con dicha construcción.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La verdad de los paramilitares

<!-- /wp:heading -->

<!-- wp:paragraph -->

Héctor Germán Buitrago, quien lideró las Autodefensas Campesinas del Casanare -ACC-, también conocidas como “Los Buitragueños”, habló en nombre suyo y de su padre Héctor José Buitrago, alias “El Viejo” y su hermano Nelson Orlando Buitrago, alias “Caballo”, también comandantes del grupo;  expresando que accedieron al Sistema Integral de Verdad, Justicia, Reparación y No Repetición **–**SIVJRNR**–**, y que esperan comparecer ante la Jurisdicción Especial para la Paz **–**JEP**–**. No obstante, **hicieron hincapié en la importancia de que la verdad no solo se dé en las instancias de sometimiento, sino que sea un ejercicio «*****público, de cara a las víctimas y al alcance de todo el país*****».**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **«**\[Si la verdad\] no se conoce, si no llega  a las víctimas, si no se trasciende no puede haber reconciliación, ni mucho menos reparación.
>
> <cite>Aparte de la carta dirigida por los exjefes paramilitares Hector Germán, Hector José y Nelson Buitrago</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por su parte, Carlos Antonio Moreno Tuberquia busca dar a conocer en la Comisión de la Verdad, el exterminio del que, según él,  fueron víctimas muchos de los excombatientes que depusieron las armas en el Acuerdo de Santafe de Ralito, firmado por las AUC con el gobierno de Álvaro Uribe, para ello señaló que **entre los años 2005 y 2008 fueron «*asesinados más de 3.000 desmovilizados*» sin que el Estado hubiese investigado nada al respecto.** (Le puede interesar: [«Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> El proceso de Paz con las autodefensas hubiera sido exitoso, pero la extradición de los comandantes de las AUC para ponerles la mordaza y así impedirles el relato de la verdad al país en los tribunales de Justicia y Paz, junto con la persecución a los desmovilizados para asesinarnos y evitar que llegáramos a contar los hechos cómo sucedieron, fue el caldo de cultivo para que muchos nos reorganizáramos. \[…\] Cómo y por qué terminé en el Clan del Golfo es algo que quisiera explicar en detalle.
>
> <cite>Carlos Mario Guerra, exjefe paramilitar de las AUC y el Clan del Golfo</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Aún hay hechos del Conflicto que se desconocen

<!-- /wp:heading -->

<!-- wp:paragraph -->

**El gestor de paz, Álvaro Leyva, ha defendido que los exparamilitares puedan comparecer ante las instancias del Sistema Integral de Verdad, Justicia, Reparación y No Repetición -SIVJRNR-;** pues señala que la jurisdicción mediante la que se sometieron los paramilitares cuando pactaron su desmovilización con el Gobierno (Justicia y Paz), no fue el mecanismo idóneo para llegar a la verdad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Leyva señaló que **la Corte Interamericana de Derechos Humanos** **ha emitido** **4 pronunciamientos señalando que la justicia transicional de Justicia y Paz no fue propicia y que hubo obstáculos para llegar a la verdad.** Por eso insistió en que los exjefes paramilitares deben tener cabida en la JEP a la cual catalogó como una “*Justicia* *hibrida en la que el Estado no funge como parte*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto, señaló que hay que ser pacientes y dejar que las solicitudes de sometimientos de los exparamilitares agoten todas sus instancias ante esta Jurisdicción. **Leyva afirmó que aún faltan etapas por agotarse en el ingreso de los paramilitares al Sistema Integral como segunda instancia en la JEP y eventualmente la Corte Constitucional.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Álvaro Leyva aseguró que **el hecho que los paramilitares sean considerados por algunas instancias de la justicia nacional e internacional como una extensión del Estado y que recibieron mandatos del Estado, modifica todos los presupuestos objetivos para tramitar el sometimiento de los miembros de estas estructuras ante la JEP.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, saludó la idea de que las versiones sean públicas y difundidas, pues es la sociedad en general y no sólo las víctimas, la que tiene derecho a saber la verdad del conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “La verdad tiene que ser pública, la verdad no puede ser de confesionario”
>
> <cite>Álvaro Leyva, exministro y gestor de Paz</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
