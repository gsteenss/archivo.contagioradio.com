Title: Con tutelas, altos mandos militares quieren censurar mural sobre 'falsos positivos'
Date: 2019-10-29 14:18
Author: CtgAdm
Category: Nacional
Tags: Censura, Ejecuciones Extrajudiciales, Mario Montoya, Mural
Slug: con-demandas-altos-mandos-militares-quieren-censurar-mural-sobre-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/mural-e1572372443280.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Recientemente el Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE) dió a conocer que el brigadier general Marcos Evangelista Pinto y el general (r) Mario Montoya Uribe habrían presentado una acción de tutela en su contra, por la difusión de una imagen en la que se denuncian casos de ejecuciones extrajudiciales cometidos entre 2.000 y 2.010. La tutela señala que el MOVICE habría violado sus derechos a la honra, buen nombre y presunción de inocencia; pero para organizaciones sociales, la acción es un nuevo intento de censura contra el mural de la \#CampañaPorLaVerdad.

### **En Contexto: ¿Quién dió la orden?** 

El pasado 18 de octubre, en el marco de la \#CampañaPorLaVerdad a la que se han integrado diferentes organizaciones sociales, artistas pintaron en una pared cerca a la Escuela Militar de Cadetes José María Córdova en Bogotá, los rostros del coronel (r) Juan Carlos Barrera, el general Adolfo León Hernández, el general (r) Mario Montoya Uribe, el general y comandante del Ejército Nicacio de Jesús Martínez, y el brigadier general Marcos Evangelista Pinto. En el mural aparecen algunas cifras de ejecuciones extrajudiciales junto a la pregunta: ¿Quién dió la orden?.

> Pasando por encima incluso de procedimientos de la [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw) el [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) censuró el mural con los rostros de militares invomucrados con mal llamados [\#FalsosPositivos](https://twitter.com/hashtag/FalsosPositivos?src=hash&ref_src=twsrc%5Etfw) [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) Soliciamos intervención para proteger artistas y libre expresión. [pic.twitter.com/MEJcwToOH1](https://t.co/MEJcwToOH1)
>
> — ColectivoDeAbogad@s (@Ccajar) [October 19, 2019](https://twitter.com/Ccajar/status/1185367356752748545?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según denunciaron las organizaciones, los artistas que realizaban el mural fueron fotografiados en horas de la tarde y en la noche aparecieron integrantes del Ejército que pintaron de blanco la pared. En un hecho similar, pero replicado en Popayán, Cauca, uniformados pintaron de blanco un cartel a escala del mural, e impidieron la grabación del hecho. (Le puede interesar:["Con pintura, Ejército pretende ocultar responsabilidad de altos mandos en ‘Falsos Positivos’"](https://archivo.contagioradio.com/con-pintura-ejercito-pretende-ocultar-la-verdad-de-los-falsos-positivos/))

> En Popayán se replicó el Mural en la Semana de la Indignación, pero anoche fue censurado a altas horas de la noche por parte de miembros del Ejército Nacional. [\#ProhibidoOlvidar](https://twitter.com/hashtag/ProhibidoOlvidar?src=hash&ref_src=twsrc%5Etfw) Los uniformados también impidieron grabar video del momento en que lo borraban. [\#EjércitoCensuraMural](https://twitter.com/hashtag/Ej%C3%A9rcitoCensuraMural?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/HE5PpLTxOB](https://t.co/HE5PpLTxOB)
>
> — ColectivoDeAbogad@s (@Ccajar) [October 26, 2019](https://twitter.com/Ccajar/status/1188165161166819328?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### La tutela, otro intento de censura al mural

Sebastián Escobar, integrante del Colectivo de Abogados José Alvear Restrepo (CAJAR), explicó que la tutela se había presentado contra la imagen que circuló en redes sociales sobre lo que sería el mural, pero en la acción judicial no había una referencia contra el mural en sí mismo. Ante las tutelas de Montoya y Pinto, el Abogado manifestó que responderán cuestionando la procedencia de la acción de tutela contra particulares, como es el MOVICE, porque la misma tiene que estar caracterizada por una condición de subordinación o indefensión de la víctima respecto al presunto victimario.

Lo que la platoaforma buscará demostrar es que no existe tal situación de los altos mandos militares respecto del MOVICE, sino al contrario, porque hubo una indefensión de los artistas que intentaban pintar el mural respecto a la institución del Ejército, que se presentó con 20 hombres con armas largas en el lugar del dibujo. Adicionalmente, buscarán que se proteja el derecho a la libertad de expresión, consagrado en la Constitución y como un derecho humano, especialmente cuando se trata de hacer veeduría a personas que ocupan cargos de alto nivel, como en este caso.

Por último, Escobar afirmó que "ni el mural ni la imagen contenían un señalamiento penal", en su lugar, se refería a hechos concretos que están probados, son de conocimiento público y en ciertos casos cuentan con respaldo judicial. Entre los hechos está que entre 2.000 y 2.010 se cometieron más de 5 mil ejecuciones extra judiciales, es decir "que las personas que aparecen en el mural tenían condición de comandantes y que hombres bajo su mando cometieron estas actuaciones", afirmó el integrante del CAJAR.

### **Ejecuciones Extrajudiciales, ¿una violación a los derechos humanos del pasado?** 

El Abogado defensor de derechos humanos dijo que en la actualidad hay varios casos registrados de esta práctica, y recordó lo ocurrido con el excombatiente [Dimar Torres](https://archivo.contagioradio.com/ejercito-habria-torturado-asesinado-e-intentado-desaparecer-a-dimar-torres/), o las denuncias sobre el asesinato del jóven [Flower Jair Trompeta](https://archivo.contagioradio.com/comunidades-del-cauca-responsabilizan-al-ejercito-del-asesinato-de-flower-jair-trompeta/), presuntamente a manos del Ejército. De igual forma, señaló las denuncias hechas por el medio New York Times, sobre directrices que se estarían implementando en las Fuerzas Armadas para aumentar el número de bajas en combate.

En consecuencia, señaló que el registro de nuevos casos de ejecuciones extrajudiciales es una noticia lamentable para el país, "sea 1 caso o 500", y se esperaría que por parte del Ministerio de Justicia y el Ministerio de Defensa se presentara una modificación de las políticas, para que en lugar de "generar presiones para presentar resultados operaciones, se genere una política para ejercer los debidos controles" que eviten que esto se vuelva a presentar. (Le puede interesar: ["Fiscalía General de la Nación pedirá medida de aseguramiento contra el General (r) Mario Montoya"](https://archivo.contagioradio.com/fiscalia-general-de-la-nacion-pedira-medida-de-aseguramiento-contra-el-general-r-mario-montoya/))

> [\#CampañaPorLaVertad](https://twitter.com/hashtag/Campa%C3%B1aPorLaVertad?src=hash&ref_src=twsrc%5Etfw)  
> Con tutelas [@COL\_EJERCITO](https://twitter.com/COL_EJERCITO?ref_src=twsrc%5Etfw) quiere sacar de las redes el mural ¿Quién dio la orden? en donde aparece 5 militares colombianos bajo cuyas comandancias se cometieron ejecuciones extrajudiciales (Falsos positivos).  
> Firma contra la censura??<https://t.co/075l38XhKK> [pic.twitter.com/UAJQG3Q25K](https://t.co/UAJQG3Q25K)
>
> — Movice (@Movicecol) [October 29, 2019](https://twitter.com/Movicecol/status/1189235992093515777?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
**Síguenos en Facebook:**

</p>
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
