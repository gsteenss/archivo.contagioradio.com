Title: Crónica del primer sindicato de trabajadoras sexuales en Colombia
Date: 2016-01-23 10:00
Category: Mujer, Reportajes
Tags: SIndicatos, trabajadoras sexuales
Slug: cronica-del-primer-sindicato-de-trabajadoras-sexuales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/SINCICATO-TRABAJADORAS-SEXUALES.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: U.Nacional] 

#### **Crónica**  
<iframe src="http://www.ivoox.com/player_ek_10168922_2_1.html?data=kpWemJ2ddpOhhpywj5adaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5yncbXmwsfOzMbIs9PV1JDgx93Zpc3Z1JDhw9LGrYa3lIqum9OPuMrZz8rbjdjZqYa3lIqvk9TXaZO3jNXOycbSb8rhjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [22 Ene 2016]

Profesionales sanitarios, policías o periodistas son algunos de los grupos profesionales con los que han empezado a reunirse las integrantes del Sindicato Nacional de Mujeres Trabajadoras Sexuales de Colombia (Sintrasexco) para compartir sus quejas, reclamos y peticiones debido al "mal trato" que reciben por el hecho de dedicarse al oficio sexual.

"Que un Secretario de Salud te diga que no le concierne nuestra salud, que eso es obligación del dueño del local donde ejercemos nuestro trabajo demuestra claramente la discriminación que sufrimos en las consultas médicas por ser lo que somos", explica Fidelia Suárez, Presidenta de Sintrasexco.

Con voz dura y sin tapujos en la lengua, Suárez especifica las estigmatizaciones que sufren diariamente por ser trabajadoras sexuales: "que en este país la fuerza pública pueda exigirnos el carné de salud donde se especifique que no tenemos VIH, sífilis, frotis de garganta ni Hepatitis B, entre otras enfermedades, es algo que nos preocupa mucho por todo lo que nos genera", debido a la falta de reglamentación de este oficio.

El coordinador del Grupo de Sexualidad, Derechos Sexuales y Reproductivos del Ministerio de Salud y Protección Social de Colombia, Ricardo Luque, por su parte, respalda la afirmación de Suárez y resalta que "las trabajadoras sexuales usan muy bien el condón, por encima del 95%, lo cual hace que la prevención sea más eficaz y la prevalencia a tener enfermedades no sea tan alta".

Las mujeres que integran Asmubuli y Sintrasexco consideran que "el trabajo sexual no es indigno, indignas son las condiciones laborales en las que se realiza" y por eso su lucha por sus derechos como personas y como trabajadoras sexuales no descansará hasta ser un colectivo reconocido y no estigmatizado.

"Nosotras somos mujeres con muchas cualidades, capacidades y virtudes, no tienen por qué estigmatizarnos ni tampoco decirnos cómo tenemos que identificarnos. En mi cuerpo mando yo, en mi cuerpo decido yo".

Estos encuentros de sensibilización pretenden trasladarse a otras regiones del país y para el próximo mes de febrero se plantea realizar un Foro Nacional de Trabajadoras Sexuales "para saber qué pasa en otros departamentos", según Suárez.

##### **Silvia Arjona Martín - Periodísta** 
