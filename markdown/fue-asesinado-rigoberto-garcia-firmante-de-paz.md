Title: Fue asesinado Rigoberto García, firmante de paz
Date: 2020-04-18 14:43
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Antioquia, FARC
Slug: fue-asesinado-rigoberto-garcia-firmante-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Asesinan-a-Edwin-de-Jesús-Carrascal-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 17 de abril cerca de las 7:00 pm de la noche fue asesinado en el firmante de paz Rigoberto García; el hecho se presentó en el municipio de Urrao, en el departamento de Antioquia, suroeste colombiano.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1251525662961254402","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1251525662961254402

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según voceros del partido FARC, García fue abordado por hombres armados que ingresaron a su vivienda y le dispararon en varias ocasiones frente a sus dos hijas. (Le puede interesar:<https://archivo.contagioradio.com/puerta-a-puerta-armados-persiguen-a-excombatientes-y-lideres-en-argelia-cauca/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de su cuenta de twitter el partido FARC reportó el asesinato del ex combatiente y lamentó la muerte te esté en Antioquia, *"enemigos de la paz avanzan a sus anchas a pesar del confinamiento, dos hijas huérfanas \#DuquePareElExterminio".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el asesinato de Rigoberto García, **son 20 los y las firmantes de paz asesinados en lo corrido de este 2020**, y cerca de 200 desde la firma del acuerdo de paz en 2016; al igual que los territorios más riesgosos para excombatientes Cauca y nariño con cerca del 34% de los casos entre 2019 y 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte la [Comisión de la Verdad](https://archivo.contagioradio.com/la-historia-y-la-memoria-se-reconstruye-todos-los-dias-alejandro-castillejo-comisionado-de-la-verdad/)en medio de las diferentes agresiones a reincorporados, líderes y lideresas sociales en medio de este período de aislamiento señaló hace algunos días, *"no se tratan de asesinatos comunes sino de [asesinatos](https://www.justiciaypazcolombia.com/persisten-asesinatos-en-putumayo/)perpetrados por organizaciones criminales con vocación de control territorial".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo

<!-- /wp:paragraph -->
