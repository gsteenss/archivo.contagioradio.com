Title: Admitida otra demanda contra la ley de víctimas, una ley tramposa
Date: 2016-03-22 08:11
Category: Opinion, Ricardo
Tags: colombianos en exilio, Exilio, víctimas
Slug: una-ley-contra-las-victimas-en-el-exilio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/colombianos-en-el-exterior.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Laprensa 

#### [Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) - [@ferr\_es](https://twitter.com/ferr_es) 

El 28 de enero de 2016 fue admitida por la Corte Constitucional  otra demanda contra la ley de víctimas \[1\], esta vez por desconocer los derechos de quienes han optado al exilio para proteger sus vidas. Desde su aprobación, la “Ley de víctimas” contiene serios vacíos que menoscaban derechos y excluyen a diferentes colectivos de afectados por la guerra en Colombia.

**Las fallas más notorias y demandadas son:**

-   Las fechas desde las cuales se reconoce el hecho victimizante: el 1 de enero de 1985.

**Excluye a víctimas anteriores.**

-   Se restituyen tierras a quienes las perdieron desde el 10 de enero de 1991.

**De hecho, facilita legalizar la usurpación de tierras ocupadas antes de la fecha.**

-   Excluyó a las víctimas del narcotráfico, cuando es un componente central en el conflicto colombiano.
-   En cinco años (10 de junio de 2021) habrá terminado la vigencia de la ley.
-   Las cuantías son ridículas frente a la magnitud del daño.

Como si fuera poco, la ley de víctimas excluye maliciosamente la protección y reparación a  quienes debieron escapar de Colombia para proteger sus vidas.

El abogado Enan Arrieta Burgos presentó la demanda de constitucionalidad contra los artículos 1 de la Ley 387 de 1997 y el parágrafo 2 del artículo 60 de la Ley 1448 de 2011, la “Ley de víctimas”, en los cuales se excluye de reparación a las víctimas que han abandonado el territorio nacional. \[2\]

Señala el abogado Enán Arrieta que “el concepto de desplazamiento forzado se diferencia de otros fenómenos de migración forzada en la medida en que ésta se debe producir dentro del territorio nacional. Sin embargo, ello podría dar lugar a una omisión legislativa relativa (déficit de protección), pues, en términos normativos, para citar un ejemplo, derechos tales como el retorno se restringen a las personas desplazadas, sin cobijar a los refugiados, exiliados, asilados y, en general, a los migrantes que abandonaron el país como consecuencia del conflicto”.

Es evidente la mala fe de los gobernantes que ignoran la existencia del exilio colombiano. No todos salimos de Colombia como emigrantes por motivos económicos. Lejos del silencio, los exiliados promueven la defensa de los derechos humanos, llaman a la aplicación del DIH entre los actores armados y siempre promovieron la salida política, que por fin se está intentando.

Criminalizado, el exilio colombiano ha sido objetivo militar, político y judicial durante años. Ahora se abre la posibilidad de conocer esta parte de la historia  que nos ha sido negada y que debe ser recuperada y compilada en los procesos de memoria histórica.

El exilio colombiano está activo desde hace décadas y tiene demandas específicas de reparación que esta ley nunca consideró: retorno de exiliados con garantías, acompañamiento psicosocial, reinserción integral, reconocimiento de títulos, reparación por los años de exilio, reparaciones laborales, derechos pensionales y reparación por el daño en los proyectos de vida.

¿Qué decir del dolor de quienes no pudieron acompañar a sus seres queridos durante sus duelos familiares?  ¿Quién mide el dolor de quienes nunca pudieron despedir a sus padres y hermanos?

La muy tarada ley de víctimas tiene ahora nuevos problemas: las entidades que adelantan los trámites de reparación se han quedado sin presupuesto y ahora tienen menos personal. Adelantar trámites de reparación, en el actual contexto, es someterse a un nuevo proceso de y victimización.

------------------------------------------------------------------------

###### \[1\] [Ley de víctimas, 1448 de 2011](http://www.prosperidadsocial.gov.co/Documentos%20compartidos/Ley%201448%20de%202011.pdf). 

###### \[2\] Mediante auto del 28 de enero de 2016, mediantel el cual la Corte Constitucional admite la demanda promovida en contra de la expresión «dentro del territorio nacional », contenida en los artículos 1 de la Ley 387 de 1997 y el parágrafo 2 del artículo 60 de la Ley 1448 de 2011. Está radicada bajo el expediente D-11139. 
