Title: La evolución del derecho en el acuerdo final de paz en Colombia
Date: 2016-08-30 18:03
Category: Entrevistas, Judicial, Paz
Tags: acuerdo final, Conversaciones de paz de la habana, FARC, Jurisdicción Especial de Paz
Slug: la-evolucion-del-derecho-en-el-acuerdo-final-de-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/9649115806_053b451cf8_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Carmela Maria] 

###### [30 Ago 2016] 

Luego de la publicación de los borradores de los acuerdos y del acuerdo final, surgen varias discusiones, una de ellas la que tiene que ver con el punto de Justicia, es decir, la [Jurisdicción Especial de Paz](https://archivo.contagioradio.com/?s=jurisdicci%C3%B3n+especial+de+paz), que pone sobre la mesa el debate en torno a la evolución del derecho y la **justicia restaurativa versus la concepción de justicia tradicional** que se ha aplicado casi en todo el mundo.

Según [Gustavo Gallón](https://archivo.contagioradio.com/?s=gustavo+gallon), director de la Comisión Colombiana de Juristas, organización con estatus consultivo ante la ONU, **la concepción de justicia en el derecho internacional tiene 4 componentes, todos ellos presentes en el acuerdo sobre JEP**, y lo que se avanza es en torno a la concepción de justicia ligada a la pena privativa de la libertad y que tiene por fin último superar la impunidad.

En Colombia que una víctima tenga acceso a la justicia es casi una lotería, y el acuerdo firmado entre el gobierno y las FARC podría aportar en la superación de la impunidad. Según el abogado, el acuerdo es bueno y alcanza un nivel de detalle importante, **el trabajo estará en que se logre su implementación** y ese asunto es del resorte de la sociedad en general y de las organizaciones sociales y de derechos humanos para los próximos años.

Los cuatro elementos que debe tener la justicia según el derecho internacional son **reparación, garantías de no repetición, declaración judicial de responsabilidad y verdad**. Según Gallón el acuerdo se esfuerza en garantizar esos cuatro componentes, los cuales aportan en que se desmantele el manto de impunidad. En Colombia es prácticamente una lotería que una haya justicia, por eso hay que impulsar que los acuerdos se implementen, señala el jurista.

Respecto de las afirmaciones de **[José Miguel Vivanco](https://archivo.contagioradio.com/?s=vivanco)**, relator para las américas de HRW, Gallón afirma que las preocupaciones planteadas son legítimas y obedecen a preocupaciones basadas en el derecho internacional que establece la proporcionalidad de las penas con los crímenes cometidos, sin embargo **el acuerdo contiene 16 sanciones aplicables en zonas rurales y urbanas para quienes reconozcan verdad y más de 4 tipos de sanciones para quienes se nieguen al reconocimiento de sus delitos.**
