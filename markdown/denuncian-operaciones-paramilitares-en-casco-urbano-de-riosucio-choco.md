Title: Denuncian operaciones paramilitares en casco urbano de Riosucio, Chocó
Date: 2015-09-11 16:24
Category: DDHH, Nacional, Paz
Tags: Autodefensas Gaitanistas, Batallón Fluvial, Chocó, Comisión de Justicia y Paz Colombia, Consejos Comunitarios Bajo atrato, Fuerzas militares, Paramilitarismo, Paramilitarismo en Chocó, policia, Red CONPAZ, Riosucio, Truandó
Slug: denuncian-operaciones-paramilitares-en-casco-urbano-de-riosucio-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/paramilitares_gaitanistas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: eluniversal] 

###### [11 Sept 2015] 

[Desde el pasado 2 de septiembre hacen presencia por lo menos **200 paramilitares en la región del Bajo Atrato chocoano**. Según la denuncia de los pobladores los paramilitares ingresaron por el rio Truandó en 6 embarcaciones e instalaron retenes. ]

[Los paramilitares también han hostigado a los líderes de los Consejos Comunitarios Menores para que se reúnan con ellos y **en el 70% de las casas de Riosucio dejaron panfletos en los que indican que “quien no esté de acuerdo con ellos debe irse”**, todo ello en medio de una fuerte presencia militar y policial.]

[Los **panfletos fueron distribuidos en horas de la madrugada mientras policías y militares custodiaban las fiestas del candidato a la alcaldía** de apellido Luchin. Los paramilitares aseveran que se asentaran en los territorios pues buscan el desarrollo de la región y **“no están dispuestos a dormir en el monte”**, atentando contra la autonomía de las comunidades quienes, a pesar de haber denunciado los hechos ante la personería municipal, no cuentan con acompañamiento gubernamental.]

[El 5 de septiembre hubo un consejo de seguridad en el municipio, pero no sé conoce un pronunciamiento formal frente a la operación de control terrestre y fluvial que han desplegado los paramilitares. Los **Consejos Comunitarios exigen acciones concretas por parte del gobierno** y anuncian que se reunirán el próximo domingo para emitir un comunicado oficial sobre las acciones que adelantaran al respecto.    ]

[Conozca el panfleto que fue distribuido: ]

[![Gaitanistas Panfleto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Gaitanistas-Panfleto.jpg){.aligncenter .size-full .wp-image-13845 width="576" height="1024"}](https://archivo.contagioradio.com/denuncian-operaciones-paramilitares-en-casco-urbano-de-riosucio-choco/gaitanistas-panfleto/)
