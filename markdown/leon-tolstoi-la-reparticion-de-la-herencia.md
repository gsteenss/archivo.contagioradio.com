Title: León Tolstoi, "La repartición de la herencia"
Date: 2015-09-09 12:54
Category: Cultura, Viaje Literario
Tags: Cajón de cuentos Tolstoi, Cuentos en audio, La repartición de la herencia, León Tolstoi
Slug: leon-tolstoi-la-reparticion-de-la-herencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/2178209_Tolstoi-detalle.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Iluistración: Enrique Carceller Alcón ] 

<iframe src="http://www.ivoox.com/player_ek_8241411_2_1.html?data=mZehk5mVdY6ZmKiakpiJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKf08rdw9fYrcTdhqigh6eXsozYxpDZw5DMqdPZz8jWw5KPkMaZpJiSpJjSb7Xjzdjh0c6RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [La repartición de la herencia- León Tolstoi] 

###### [9 Sept 2015]

Liev Nikoláievich Tolstói, mejor conocido como León Tolstoi, nació en Yasnaia Poliana, el 9 de septiembre 1828, hijo del noble propietario y conde León Tolstoi y de la acaudalada princesa María Volkonski.

Tolstói viviría siempre escindido entre esos dos espacios simbólicos que son la gran urbe y el campo, pues si el primero representaba para él el deleite, el derroche y el lujo de quienes ambicionaban brillar en sociedad, el segundo, por el que sintió devoción, era el lugar del laborioso alumbramiento de sus preclaros sueños literarios.

Sus principales obras fueron "La guerra y la paz" y "Ana Karenina", aunqeu escribió más de 2000 cuartillas aproximadamente a lo largo de su vida.

Tolstoi Muere el 9 de noviembre de 1910 cuando escapa de su hogar en busca de la vida austera que siempre quiso tener. Compartimos con ustedes el microrrelato "La repartición de la Herencia" incluído en la publicación compilatoria "Cajón de cuentos" del autor.
