Title: Ahora podrá grabar y editar videos desde Twitter
Date: 2015-01-27 20:58
Author: CtgAdm
Category: datos, Uncategorized
Tags: tecnologia, twitter, video desde twitter
Slug: grabar-y-editar-videos-desde-twiitter
Status: published

Este 27 de enero, a través de un comunicado Twitter, la red de microblogging con más de  284 millones de usuarios, ha lanzado un nuevo complemento que permitirá a los twitteros grabar, editar y compartir sus videos.

La aplicación, en su primera etapa, estará activa para usuarios con Iphone, pero se espera que en poco tiempo esté disponible para sistemas Android.

El uso será muy similar al de hoy en día con las fotografías, pero con la diferencia que se podrá grabar  videos de 30 segundos, el que posteriormente se podrá editar y poner filtros.

Con este nuevo servicio de video y el anuncio de mensajes directos entre grupos, la red social empieza sus innovaciones para seguir cautivando a los usuarios en esta red social.

Esta aplicación permitirá un mayor acceso a las nuevas tecnologías por parte de los usuarios en cualquier país del mundo, favoreciendo también la inmediatez en la transmisión de información, que ha potenciado la movilización social y la solidaridad en muchos escenarios.

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

