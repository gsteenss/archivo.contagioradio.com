Title: Cambios de poder armado en comunidades agudiza crisis humanitaria: ECHO
Date: 2017-06-20 15:18
Category: DDHH, Entrevistas
Tags: ACNUR, desplazados, Desplazamiento forzado, ECHO
Slug: cambios-de-poder-armado-en-comunidades-agudiza-la-crisis-humanitaria-echo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [20 Jun. 2017] 

**En lo que va corrido del 2017 han sido desplazadas de manera forzada 7.371, según las cifras de ACNUR**, de los cuales la Costa Pacífica, Norte de Santander y Arauca presentan las situaciones más complejas. En total son 2.056 familias afectadas. Una tragedia humanitaria que según algunas personas e instituciones, podría agravarse si no se aplican los correctivos necesarios.

Para **Álvaro De Vicente, Jefe de la Oficina de América del Sur de Asistencia Humanitaria y Protección Civil de la Comisión Europea, ECHO,** también es una preocupación el ver cómo las consecuencias humanitarias en los territorios ya mencionados se mantienen, hay situaciones de desplazamiento y confinamiento.

“Todavía hay un número de personas muy importante en muchos lugares donde vemos que hay gente que está afectada, que sigue afectada tanto por desplazamiento como por restricción de acceso y movilidad” añadió de Vicente. Le puede interesar: [Nuevos desplazamientos forzados en Buenaventura](https://archivo.contagioradio.com/nuevos-desplazamientos-forzados-en-buenaventura/)

Si bien está oficina no se encarga de detallar quiénes son los actores responsables del desplazamiento, si aseguran que lo preocupante es que **hay algunas zonas en que la gente sigue afectada por la existencia de la violencia.**

### **Si no se atiende el desplazamiento interno será más difícil la paz** 

Según el jefe de ECHO, ha existido una coordinación con el Estado para atender esas realidades en los lugares que persisten graves situaciones humanitarias por el desplazamiento interno, pero que es necesario recordarle que debe ser “consciente de que estas necesidades que están en los territorios son persistentes y que **si se quiere la paz lo primero que hay que hacer es que la gente tenga cubiertas sus necesidades humanitarias”.**

### **Inestabilidad en el poder armado de las regiones genera problemas humanitarios** 

Para el caso particular de Colombia, las comunidades son más afectadas por problemas humanitarios cuando existe una inestabilidad o cuando hay cambios de poderes que ejercen armados en las comunidades “no tenemos que olvidar que hay muchos lugares en los que hay cambios de poderes a nivel con las comunidades y eso tiene consecuencias sobre la población”.

Para De Vicente las consecuencias pueden variar, primero **entrando un actor armado ejerce presión a través de la restricción de la movilidad**, o por medio de la extorsión o provocando el desplazamiento de las poblaciones. Le puede interesar: [Víctimas de desplazamiento exigen reparación integral](https://archivo.contagioradio.com/victimas-de-desplazamiento-exigen-reparacion-integral/)

### **Acceder a instrumentos legales para defender los derechos** 

Dice De Vicente que es de destacar que **Colombia cuenta con muchos instrumentos legales buenos para defender los derechos de las personas desplazadas,** por lo cual dice que ese sería el principal instrumento que vería para hacer valer los derechos de las comunidades. Le puede interesar: [Capturan a empresario Darío Montoya por desplazamiento en Curvaradó](https://archivo.contagioradio.com/capturan-a-empresario-ganadero-dario-montoya-por-desplazamiento-en-curvarado/)

“Lo importante es difundir cuáles son los derechos de estas personas afectadas por el conflicto, pero en especial las personas desplazadas para que puedan ser beneficiadas de las ayudas que el Estado les pueda brindar. Y si no pueden acceder, nosotros vamos a estar colaborando y articulando para complementar estas ayudas” recalca De Vicente.

<iframe id="audio_19376254" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19376254_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
