Title: Cómo la crisis climática podría ser el inicio de un cambio de paradigma
Date: 2016-08-25 14:50
Category: Ambiente y Sociedad, Opinion
Tags: Ambiente, cambio climatico, co2, naturaleza
Slug: como-la-crisis-climatica-podria-ser-el-inicio-de-un-cambio-de-paradigma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/cambio-climatico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Lyudmla Nesterenko 

#### [**Por: **Marisa Amorim Fonseca. Pasante Universidade Paris Descartes** - Ambiente y Sociedad **](https://archivo.contagioradio.com/margarita-florez/) 

###### 25 Ago 2016

La crisis ambiental que estamos viviendo es un hecho real y hoy muchos consideran como “extremamente probable” que la causa de este fenómeno sea la actividad humana. Informes científicos como los del Intergovernmental Panel on Climate Change (IPCC) están estudiando la incidencia del ser humano sobre este proceso a partir de análisis detallados. Además del cambio en la temperatura, los investigadores del IPCC han encontrado que, por ejemplo, en el último siglo el nivel del mar subió mucho más de lo que alcanzó en siglos anteriores.\[1\] Verà \[1\]

Desde hace casi 25 años, el objetivo de la comunidad internacional es evitar que la temperatura del mundo supere los 2 grados centígrados. En realidad, los 2 grados corresponden a un plan que fue elaborado a través de la información recolectada y su proyección a largo plazo. Más allá de esos 2 grados, el riesgo es que se desbocara el incremento de la temperatura, lo cual crearía una reacción en cadena que sería difícil de controlar.

### **Un sistema incompatible con las preocupaciones ambientales** 

Esta crisis climática permite cuestionarnos sobre nuestros hábitos de vida y particularmente en todo lo relacionado con el consumo y la producción. Nuestro sistema económico no funciona bien. Aunque se quiera mostrar distinto, el capitalismo “verde” sigue siendo muy parecido al tradicional, ambos generan una producción excesiva basada en el despilfarro de los recursos. Los Estados siguen, por ejemplo, siguen fundando su aparato productivo en los hidrocarburos, cuyas reservas se están acabando, y no saben cómo transformar el CO2 en energía. ¿Cómo podríamos tener un desarrollo sostenible en nuestra sociedad si nuestro sistema económico está estrechamente ligado a un sistema que utiliza energías fósiles? – *ver artículo 4 de declaración de Rio.*

La perennidad de un sistema limitado y destinado a desaparecer no puede ser considerada como sostenible. Las negociaciones de la COP 21, en diciembre 2015 en París, tenían por objetivo un acuerdo universal sobre el clima, vinculante, para no superar los 2 grados. Sin embargo, constatamos que, a pesar de la buena voluntad de algunos países, nada se dijo sobre el funcionamiento del sistema que provocó este cambio climático.

Para poder arreglar este problema es necesario cambiar de paradigma porque las soluciones que tenemos (o que pensamos tener) solo lo retrasan. En el libro «*Green capitalism: the God that failed*», Richard Smith muestra que la producción sostenible es posible pero no en este marco que fue definido por el capitalismo. En realidad, el capitalismo se basa sobre la creencia que el crecimiento es infinito, pero sabemos que los recursos son limitados.

Es necesario cambiar nuestra representación del mundo, salir de la espiral individualista y antropocéntrica. Porque entender el planeta como una sucesión de intereses individualistas nos impide tener una visión global y de largo plazo.

El aspecto no vinculante del derecho internacional hace de la aplicación de los textos internacionales un asunto particularmente delicado, como podemos ver en el caso de la Convención de las Naciones Unidas sobre el cambio climático de 1992. Este tratado tiene únicamente obligaciones generales y, aunque el Protocolo de Kioto permitió formular objetivos más precisos, la única obligación escrita en esta convención era el hecho de presentar el inventario de las emisiones. El rechazo de los Estados Unidos (que es uno de los principales contaminadores) muestra que es necesario convencer a los Estados de firmar los acuerdos para que las obligaciones sean efectivas.

### **¿Cómo cambiar de paradigma?** 

Cambiar de paradigma implica enfocarse en la cooperación internacional (el cambio climático afecta todos los Estados, en proporciones diferentes). Para eso, es necesario que se establezca una confianza entre los Estados. La reducción de las emisiones de CO2 tiene que estar acompañada de un cambio global, con una nueva trayectoria y una sociedad basada en el desarrollo (relacionado con las cualidades) y no en el crecimiento (que implica únicamente las cifras).

No existe una solución única y perfecta, al contrario, existen múltiples alternativas.  
Sobre la agricultura (que representó el 14% de las emisiones de gases en 2000) (*Climate analysis indicator tool*), una de las soluciones es privilegiar lo local y tratar de producir más cerca del consumidor, para evitar las emisiones de CO2 que genera el transporte de alimentos.

Además, algunos estudios evidenciaron que las pequeñas granjas son más productivas que las grandes. La explicación es muy simple: Las grandes empresas de producción, que utilizan cantidades impresionantes de productos químicos, acaban por dañar el suelo y contribuyen a su erosión y a tener pocos rendimientos.

Podemos también mencionar el desarrollo de la agroecología \[2\] que permite alimentar a las personas de forma sustentable. Por la complementariedad de las especies, el agricultor logra a tener mejores rendimientos como se puede ver en la Milpa\[3\].

Para la energía, que representa el 25% de las emisiones de gases de efecto invernadero, es importante dejar una parte de los hidrocarburos en el suelo, no explotarlos y desarrollar las energías renovables. Para los que piensan que estas alternativas no pueden asegurar la producción mundial de la energía, es necesario recordar que un cambio de nuestra manera de consumir y el mejoramiento del aislamiento de los edificios, por ejemplo, podría disminuir considerablemente nuestras necesidades en energía. La combinación entre la transición de energías fósiles a energías renovables y un cambio en la mentalidad sobre cómo entendemos la producción y el consumo es una de las soluciones. El despilfarro no solo existe en la alimentación sino también en la energía.

De hecho, lo que caracteriza nuestra sociedad actual es la sobre-producción. Lo vemos en la industria de la moda o de los celulares, que se establecen a partir de una creación perpetua de necesidades. Así, resolver el problema de la crisis climática implica poner en perspectiva varios elementos y combinarlos. El sistema capitalista no es el único sistema que existe y no puede ser compatible con una sociedad internacional que tiene como prioridad el desarrollo sostenible. Pero para adaptarse es necesario aceptar los cambios.

En los años 70, la noción de “decrecimiento” emergió cuando la población empezó a tener conciencia de los efectos nocivos del productivismo sobre el medio ambiente y las personas. Esta corriente de pensamiento preconiza un equilibrio entre las necesidades y los recursos. Privilegiando las producciones locales, la cooperación, la auto-sostenibilidad y los intercambios, o incluso la fabricación de productos más durables, es posible invertir la curva y convertir esta crisis en la oportunidad de cambiar nuestras perspectivas, nuestra manera de producir y consumir y, de forma más general, nuestra manera de representar el mundo.

------------------------------------------------------------------------

###### \[1\] <https://www.ipcc.ch/pdf/assessment-report/ar5/syr/SYR_AR5_FINAL_full_es.pdf> 

###### \[2\] <http://www.colibris-lemouvement.org/webzine/decryptage/permaculture-agroecologie-agriculture-bio-quelles-differences> 

###### \[3\] <http://www.biodiversidad.gob.mx/usos/alimentacion/milpa.html> 

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.](http://www.biodiversidad.gob.mx/usos/alimentacion/milpa.html) 
