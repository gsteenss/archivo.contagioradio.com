Title: "El gobierno nos trata como mendigas y nos manosea" Mujeres Afrodescendientes
Date: 2015-04-28 16:23
Author: CtgAdm
Category: Entrevistas, Mujer
Tags: Cauca, Derechos Humanos, Francia Márquez, Gobierno Nacional, Minería ilegal, Movilización de Mujeres Afrodescendientes por el Cuidado de la Vida y los Territorios Ancestrales, mujeres, Mujeres Afrodescendientes, Plan de acción integral, Unidad de Protección, UNP
Slug: mujeres-afrodescendientes-estan-cansadas-de-los-incumplimientos-del-gobierno
Status: published

##### Foto: radiomacondo.fm 

<iframe src="http://www.ivoox.com/player_ek_4420102_2_1.html?data=lZmfkpaUdo6ZmKiak5qJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdbextfS1ZDFqtPjxcrgxcrSqMrZz9nS1ZDJt9WZpJiSo5bSb8TVz9jOxsbXb8XZzZDW0MjZsdHgytKah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Francia Márquez, Movilización de Mujeres Afrodescendientes por el Cuidado de la Vida y los Territorios Ancestrales] 

[**“***Estamos bravas, nos sentimos cansadas, manoseadas por este Gobierno que no cumple con su palabra, con este Gobierno Nacional que nos falta al respeto y nos trata como personas de segunda, que nos trata como mendigas, este Gobierno Nacional que hace de la Paz un discurso, se le olvida que no hay Paz si no es capaz de cuidar de la Vida, si no pone la Vida de todos los seres por encima de los intereses transnacionales*”, dice el comunicado del colectivo **Movilización de Mujeres Afrodescendientes por el Cuidado de la Vida y los Territorios Ancestrales**, expresando su descontento con los constantes incumplimientos del gobierno.]

[**Francia Márquez**, una de las lideresas de esa organización, afirma que son  varios los puntos en los que el gobierno le ha incumplido a las mujeres afrodescendientes.  En base al fallo de la  Corte Constitucional, el gobierno nacional se había comprometido a implementar el **Auto 092 para la protección de las mujeres afrodescendientes**, también debía desarrollar un **plan de protección colectiva para las comunidades negras** del norte del Cauca, lo que implicaba **la realización de 41 consejos comunitarios.**]

[Así mismo, Márquez afirma que otro de los compromisos era la realización de un **Plan de Intervención Integral** para que las personas no tuvieran que someterse a trabajar en la minería ilegal y así, pudieran tener la oportunidad de volver a sus actividades ancestrales para sobrevivir.]

[Además, la **Unidad de Protección** había prometido medidas de seguridad colectivas debido a que las individuales no han sido suficientes, y la población sigue siendo objeto de constantes vulneraciones a sus derechos humanos.]

[Sin embargo, pese a la cantidad de acuerdos a los que se había llegado con el gobierno, **“hasta la fecha, en nada de eso se ha avanzado”**, expresa la lideresa afrodescendientes, quien cuenta seis reuniones con el gobierno en las cuales el movimiento asegura que los funcionarios del Estado tratan a las mujeres como si estuvieran pidiendo plata cuando **lo que necesitan son garantías,** dice Francia.]

[El colectivo por la vida y los territorios ancestrales, decidió levantarse de la mesa de negociación con el gobierno porque  aseguran que **"no son cuatro meses de incumplimiento, son años y años de un deber atrasado que no se quiere adelantar",** dice el comunicado de la Movilización de Mujeres Afrodescendientes. Es por eso que convocan tanto al pueblo negro como a toda la ciudadanía para que se movilicen en apoyo a las comunidades negras de todo el país que han sido blanco del  **racismo institucional y estructural.**]
