Title: Defensores de páramos proponen mesa de interlocución con el gobierno
Date: 2016-08-18 16:46
Category: Ambiente, Nacional
Tags: Ambiente, páramos, Tasco
Slug: defensores-de-paramos-proponen-mesa-de-interlocucion-con-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Tasco.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tasco Joven 

###### [18 Ago 2016] 

Defensores y defensoras de los páramos en Colombia, proponen la creación de un comité regional provisional que sirva para la conformación de una **Mesa de Interlocución con las instituciones gubernamentales,** donde se generen diálogo en torno al uso de los territorios por parte de las comunidades y las empresas. Esto, teniendo en cuenta las múltiples violaciones a los derechos humanos y del ambiente, cuando se ha puesto en marcha proyectos minero-energéticos en las diferentes regiones del país.

La propuesta surgió durante **el Tercer Encuentro Nacional de Defensores de Páramos "Territorios y Soberanía"**, donde se esperaba contar con la participación del Ministerio de Minas y Energía, de Ambiente, de Desarrollo Sostenible, de Agricultura, la Agencia Nacional Minera, la Defensoría del Pueblo, Procuraduría Regional de Boyacá, sin embargo ninguna de estas instituciones asistió.

El objetivo del evento que se desarrolló en Tasco, Boyacá, era discutir sobre la base de propuestas para la permanencia digna de las comunidades en los territorios y alternativas al modelo económico que impone políticas para la ampliación de la frontera extractiva y la mercantilización de la naturaleza.

Fue así, como integrantes de organizaciones ambientalistas de departamentos del **Tolima, Santander, Boyacá, Meta, Cundinamarca y Bogotá**, trabajaron durante esos tres días en el desarrollo de rutas de trabajo en el marco de la labor de defensa de los páramos. En ese sentido se identificaron las principales amenazas para la naturaleza y las propuestas de acuerdo al trabajo que se realiza en distintas partes de Colombia.

Una de las principales amenazas identificadas tiene que ver con la delimitación de los páramos. De acuerdo con la denuncia de las comunidades y organizaciones, esta **delimitación ha sido impuesta de forma unilateral sin tener en cuenta la participación de las comunidades** quienes aseguran que al fragmentar la montaña andina se está dando paso a la mercantilización de la naturaleza, por medio de la implementación de megaproyectos mineroenergéticos, que no tienen en cuenta las costumbres y actividades culturales y económicas de los pobladores.

También se concluyó que “la implementación de un modelo de economía verde en las altas montañas mediante mecanismos como el Pago por Servicios ambientales –PSA- y la Reducción de Emisiones por Deforestación y Degradación de los bosques –REDD+-, como mecanismos de mercantilización de la naturaleza poniendo un valor monetario a dinámicas ecológicas, inmersas en lógicas del mercado”.

Teniendo en cuenta lo anterior, y otra serie de amenazas identificadas durante el encuentro, se propuso que  través de una mesa de interlocución con el gobierno se planteará la importancia de que el **ordenamiento territorial sea construido de la mano de las comunidades y basado en las cuencas hidrográficas, y planes de vida comunitarios.**

Finalmente, se propuso fomentar figuras de ordenamiento territorial desde las comunidades como: **Zonas de Reserva de la Sociedad Civil, Zonas de Reserva Campesina y Territorios Agroalimentarios**, entre otros.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
