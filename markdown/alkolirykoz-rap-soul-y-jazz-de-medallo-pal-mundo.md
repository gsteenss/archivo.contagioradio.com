Title: Alkolirykoz, Rap, soul y jazz de Medallo pa`l mundo
Date: 2015-10-11 12:00
Category: Cultura
Tags: Alkolirykoz, Hip Hop, Jazz, Method Man, Rap de Medellín, Soul
Slug: alkolirykoz-rap-soul-y-jazz-de-medallo-pal-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Alkolirykoz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: traficoindependiente.wordpress.com 

###### [5 oct 2015] 

Una descarga de sonidos que combinan el jazz y el soul con el hip hop, se viene tomando la escena musical colombiana con "**Alcolirykoz**", una agrupación nacida en Medellín hace poco más de una década, integrada por **Gambeta, Fa-zeta y Kaztro**, quienes buscan en tarima conectarse con el público al cual nunca ha considerado un adorno, sino la parte vital de su espectáculo.

En 2007 los "Alcolirykoz" lanzan su primer EP títulado "**En letras Mayúsculas**". un disco demo compuesto por seis pistas, con el que llegaron por primera vez a oidos de melómanos y amantes de la música independiente, causando de que hablar, tanto por sus "beats" y "riffs" como por la irreverencia de sus letras.

Sería hasta el año 2010 en que el primer albúm completo de la banda llegaría a las tiendas: **“La revancha de los tímidos”**, seguido por el EP "**El despilfarro**" que los impulsaría a nivel nacional. En 2012 lanzan "**Viejas Recetas, remix y otras rarezas**", trabajo con el que consiguen consolidar su rap paisa.

Las invitaciones a grandes festivales no se hicieron esperar, incialmente como teloneros de **Method Man**, además de participar en tres ediciones del festival Altavoz de Medellín; en 2012 en el concierto Radiónica de Medellín, un año más tarde hicieron parte del cartel del Estereo Picnic  y en el de Bogotá  en 2014, entre otros festivales.

Con los "**Efectos secundarios**", su más reciente trabajo discográfico de 2014 que incluye temas como “Anestesia local 1” y “Anestesia local 2” los cuales narran una historia publicada por la revista Vice en un artículo del 4 de marzo de este año sobre la banda:

*“…en el primer episodio Kaztro narra la historia de un concierto que tuvo lugar en su barrio, el sueño cumplido, las calles que los vieron crecer ahora los veían gigantes sobre la tarima. Al terminar el toque, de camino a su casa, se detiene con un amigo para hacer un graffiti y otra historia se comenta. Esta vez llega Gambeta para repartir la segunda dosis de anestesia local y relatar el castigo impartido por el combo de turno al que no le pareció lo del graffiti y decidió reprenderlos a punta de puñal. Entre el dolor y el susto, Kaztro reaparece con un coro más letal que las heridas: "Mi seguridad social la llaman R.A.P / Jamás me podrás matar / Yo vivo en tu MP3". Esta secuencia, real, trazó la linea que debía seguir un disco que, a punta de rimas entrañables, se pasea con nostalgia y valentía por la vida con la única certeza de que en cualquier momento, por cualquier motivo, esta se acaba". *

En este 2015 los Alkolirykoz acompañaron a Manu Chao en la gira que adelantó en Colombia, en su gira por Colombia.

Esta agrupación **se puede escuchar en cualquier momento,** su rima cargada de sátira, jerga popular antioqueña y vivencias de calle, siempre difíciles para el joven con sueños de arte, no son nada sin un público receptivo y abierto a un sonido que en cualquier momento obliga a que se le enrede la lengua.
