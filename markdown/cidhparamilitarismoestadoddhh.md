Title: En CIDH evidencian tareas pendientes del Estado para el desmonte del paramilitarismo
Date: 2017-03-21 17:27
Category: Nacional
Tags: CIDH, DDHH, estado, Paramilitarismo
Slug: cidhparamilitarismoestadoddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/33189658950_928f0406c7_k.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH] 

###### [21 Mar 2017] 

Durante el periodo de sesiones 161 de la Comisión Interamericana de Derechos Humanos, CIDH, diversas organizaciones de DDHH denunciaron que hay falta de voluntad política por parte del Estado para desmontar el paramilitarismo, y que para ello es claro que hay tareas pendientes, unas establecidas en el acuerdo de paz y las recomendaciones que se han hecho en otros escenarios y también en la CIDH.

En el marco de esas líneas de acción, están las contempladas en el acuerdo para la terminación del conflicto que se firmó con las FARC EP a finales de 2016 y que estipulan la instalación y puesta en marcha de una unidad especial de la policía que se encargaría de investigar las operaciones criminales de los grupos sucesores del paramilitarismo, así como actuar para desmantelar dichas estructuras.

Otra de las tareas pendientes es crear una unidad especial de la Fiscalía, con independencia y autonomía que permita realizar las investigaciones correspondientes a los hechos y personas involucradas con la comisión de crímenes ligados a organizaciones sucesoras de los paramilitares. Le puede interesar: [Impunidad la mayor preocupación de organizaciones de DDHH con la JEP](https://archivo.contagioradio.com/impunidad-mayor-preocupacion-de-organizaciones-37994/).

Un tercer elemento es la reforma al sistema de alertas tempranas, que no ha funcionado casi en ninguna de las ocasiones en que se ha requerido con suficiencia que se aborden medidas de protección urgentes, por último se insiste en la realización de un pacto político que incluya a todos los actores de la sociedad y que tenga como objetivo que se aparte la violencia del ejercicio de la política.

### **El Estado no tiene voluntad para que haya fuerza pública para la paz  
** 

Hay exigencias de defensores de DDHH que tienen que ver con la reducción del gasto militar y del número de efectivos en la fuerza pública, dado que no estaría justificado un ejercito tan grande en momentos de pos conflicto, por otro lado la cancelación de la Justicia Penal Militar y la reformulación de la doctrina de seguridad que contiene el concepto del enemigo interno.

Además, aunque se ha dicho que habrá una reforma a la doctrina de seguridad, dicha reforma no se haría abiertamente y de cara al país, razón que ya despierta serias dudas en cuanto al alcance de este tipo de medidas que deberían estar orientadas a que se acabe la política del enemigo interno.

Según las organizaciones, esas serían tras situaciones que contribuirían de manera sustancial en la solución al problema del paramilitarismo ya que han generado impunidad, desborde del gasto militar e insuficiencia de los mecanismos de control interno de las FFMM.

### **Estado sigue negando sistemáticamente la existencia del paramilitarismo** 

Durante este periodo de audiencias de la CIDH las organizaciones de DDHH insistieron en señalar que para poder enfrentar el paramilitarismo primero hay que reconocerlo. Según señaló Franklin Castañeda, la excusa que ha usado el Estado, para no reconocer es que no se quiere dar estatus político, sin embargo, ya hay varias sentencias de la Corte Suprema de Justicia en que se infiere que por ningún motivo se concederá dicho estatus a unas organizaciones que nunca han enfrentado al Estado.

<div class="fb-video" data-href="https://www.facebook.com/contagioradio/videos/10154288800425812/" data-width="500" data-show-text="false">

> [Garantías de no repetición en el acuerdo de Paz](https://www.facebook.com/contagioradio/videos/10154288800425812/)\#ColombiaEnLaCIDH Garantías de no repetición en el acuerdo de Paz  
> Posted by [Contagio Radio](https://www.facebook.com/contagioradio/) on martes, 21 de marzo de 2017

</div>
