Title: Montería se queda sin pasaje preferencial a estudiantes
Date: 2017-01-11 12:56
Category: Educación, Nacional
Tags: aceu, Montería, Movimiento estudiantil
Slug: monteria-deja-sin-pasaje-preferencial-a-estudiantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/estudiantetarifa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Razón  ] 

###### [11 Enero 2017] 

A partir de este año queda **abolido el pasaje preferencial que beneficiaba a estudiantes de Montería, esta decisión se dio por la falta de acción del Consejo municipal**, que de acuerdo con Yuliana Montes, vocera de la ACEU Córdoba demuestra su poco interés por apoyar al estudiantado y afecta a la gran mayoría de alumnos de estratos 1, 2 y 3 quienes eran los que más usaban el transporte público.

**De \$1.800 pesos los estudiantes deberán pagar \$2.200 pesos**, esto debido a que no se gestaron los trámites necesarios para reanudar la contratación que permitía el descuento del 30% sobre el pasaje y que desde septiembre de 2016 se estaba desarrollando. Con este aumento los estudiantes deberán pagar **\$4.200 pesos diarios** y de acuerdo con Montes reducir otras necesidades básicas para poder estudiar, puesto que mensualmente deberán invertir **\$66.000 pesos para poder asistir a sus clases**.

Situación que para los estudiantes podría arreglarse si el Consejo Municipal **le otorgara facultades a la Alcaldía de Montería para reanudar el programa de tarifa diferencial**. Esto teniendo en cuenta que las familias no solo tienen un hijo y la inversión aumentaría, a esto Montes le suma el aumento en el IVA, que afecta directamente los ingresos de las familias.

“No podemos permitir que los estudiantes prioricen entre sus necesidades, que un estudiante **se quede sin ir estudiar o no pueda cumplir con sus condiciones mínimas de higiene para pagar el pasaje más caro de Colombia**”.

Al iniciar el calendario académico los estudiantes prepararán movilizaciones y plantones en frente del Consejo Municipal, a su vez, ya han dicho que como medidas **tendrán que caminar o usar más la bicicleta si quieren continuar asistiendo a clases.** Le puede ineresar: ["Estudiantes del SENA se movilizan en contra de Reforma Tributaria"](https://archivo.contagioradio.com/estudiantes-del-sena-se-movilizan-en-contra-de-reforma-tributaria/)

<iframe id="audio_16126620" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16126620_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
