Title: La Unidad de Búsqueda, una oportunidad para los familiares de desaparecidos
Date: 2018-05-30 13:44
Category: DDHH, Nacional
Tags: búsqueda de desaparecidos, semana del detenido desaparecido, víctimas de desparición
Slug: la-unidad-de-busqueda-una-oportunidad-para-los-familiares-de-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/desaparecidos-e1504032110792.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [30 May 2018] 

En el marco de la conmemoración de la semana del detenido desaparecido en Colombia, los familiares de las víctimas recordaron que es importante realizar acciones de **visibilización, memoria y dignificación** para recordarle a la sociedad la labor que tiene en la exigencia de la búsqueda de los desaparecidos y la verdad.

Por medio de diferentes actividades como marchas, plantones y actividades artísticas, los familiares de los desaparecidos **recuerdan en la última semana de mayo** a quienes no están, pero por quienes han librado una lucha incansable que no ha contado con las garantías suficientes en el país.

### **Estado no ha dado garantías para conocer la verdad y buscar a las personas desaparecidas** 

De acuerdo con Gloria Gómez, vocera de la Asociación de Familiares de Detenidos Desaparecidos, en el país **no hay garantías para la búsqueda de personas** dadas por desaparecidas como tampoco para conocer la verdad de los hechos que involucran a este fenómeno. (Le puede interesar:["Búsqueda de desparecidos en Hidroituango debe incluir a las víctimas"](https://archivo.contagioradio.com/busqueda-de-desaparecidos-en-hidroituango-debe-incluir-a-las-victimas-rios-vivos/))

Afirmó que “**sigue siendo un drama para los familiares** que aún no encuentran respuestas”. De igual forma, recordó que las acciones de dignificación y memoria, están dirigidas a crear un compromiso por parte de la sociedad para conocer la verdad, aplicar la justicia “pero sobre todo buscar y encontrar a nuestros desaparecidos”.

### **Familiares piden que se fortalezcan los mecanismos establecidos por el Acuerdo de Paz** 

Si bien el Acuerdo de Paz, creó la Unidad de Búsqueda de Personas dadas por Desaparecidas, Gómez afirmó que **no hay una voluntad política del Gobierno Nacional** “por fortalecer la Unidad” y así garantizar que se lleven a cabo los objetivos que se plantearon.

Sin embargo, recordó que las diferentes organizaciones de víctimas, continúan realizando acciones de incidencia para que “desde la comunidad internacional **se exija al Gobierno de Colombia** que se fortalezca la Unidad de Búsqueda de Personas Desaparecidas y la Comisión del Esclarecimiento de la Verdad”. (Le puede interesar:["Luz verde para funcionamiento de la Unidad de Búsqueda de Personas Desaparecidas"](https://archivo.contagioradio.com/luz-verde-para-funcionamiento-de-la-unidad-de-busqueda-de-desaparecidos/))

Desde ASFADDES, recordaron que en Colombia **aún están presentes obstáculos** ligados a la impunidad y la indiferencia. Por esto, recordó que el compromiso de las organizaciones y de los familiares se ha enfocado en mantener la realización de diferentes actividades que logren “comprometer a la sociedad en la búsqueda de una paz sin desaparecidos”.

<iframe id="audio_26264804" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26264804_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
