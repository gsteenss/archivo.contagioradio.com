Title: Presidenta de la JEP pide al Congreso no dilapidar Ley de procedimiento
Date: 2018-06-19 17:25
Category: Nacional, Paz
Tags: Corte Constitucional, Gobierno Nacional, JEP, paz, Senado
Slug: presidenta-de-la-jep-pide-al-congreso-no-dilapidar-ley-de-procedimiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/jep1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: JEP] 

###### [19 Jun 2018] 

**Patricia Linares, presidenta de la Jurisdicción Especial para la Paz** (JEP), manifestó su preocupación por el futuro cercano de dicho organismo, luego de que se aprobara en el Senado el aplazamiento de la Ley de Procedimiento que rige a esta justicia especial.

**A través de un comunicado, Linares expresó que la Ley de procedimiento es fundamental para poder avanzar** puesto que “hay temas que desde luego necesitan de esa norma de procedimiento” y el Acto Legislativo 01 de 2017, aunque importante, no es suficiente para garantizar todo el marco legal que se requiere para progresar.

**Esta reglamentación procedimental servirá para asegurar que se cumpla el debido proceso en cada caso que asuma la JEP**, y como indica Linares, con su aprobación, se destrabarían temas que la ciudadanía está esperando como los llamados a audiencias y la solicitud a testigos para comparecer ante esta justicia especial. (Le puede interesar: ["¿Qué ha pasado con la implementación de la JEP"](https://archivo.contagioradio.com/implementacion-jep/))

Aunque los congresistas que votaron a favor del aplazamiento de esta ley, señalaron que lo hicieron a la espera del pronunciamiento de la Corte Constitucional sobre la Ley Estatutaria de la JEP, la presidenta de este estrado desestimo ese argumento puesto que **“en el país, se han producido muchas normas de procedimiento y códigos sin que existan las correspondientes leyes estatutarias”**

### **Se avecinan nuevas disputas sobre la JEP** 

Tanto el presidente saliente, Juan Manuel Santos, como Rodrigo Rivera, Ministro de Interior y algunos congresistas se han pronunciado sobre el aplazamiento de la Ley de Procedimiento y han buscado que sea aprobada antes del fin de la presente legislatura. Sin embargo, en las ultimas horas, l**a plenaria del Senado optó por suspender la votación de este Proyecto de Ley y declarar informal la sesión.**

[Nota de Prensa - Declaraciones Patricia Linares, Presidenta de la JEP](https://www.scribd.com/document/382132448/Nota-de-Prensa-Declaraciones-Patricia-Linares-Presidenta-de-la-JEP#from_embed "View Nota de Prensa - Declaraciones Patricia Linares, Presidenta de la JEP on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_15263" class="scribd_iframe_embed" title="Nota de Prensa - Declaraciones Patricia Linares, Presidenta de la JEP " src="https://www.scribd.com/embeds/382132448/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-bLlX6Uel41YvWayxftaB&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7055599060297573"></iframe>  
<iframe id="audio_26641242" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26641242_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
