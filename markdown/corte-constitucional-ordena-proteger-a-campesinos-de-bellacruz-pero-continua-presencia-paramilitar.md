Title: Corte Constitucional ordena proteger a campesinos de Bellacruz, pero continúa presencia paramilitar
Date: 2015-07-27 13:19
Category: DDHH, Nacional
Tags: bellacruz, hacienda, Paramilitar, paramilitares
Slug: corte-constitucional-ordena-proteger-a-campesinos-de-bellacruz-pero-continua-presencia-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/campesinos2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: RadioMacondo 

#####  

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Adelfo-Rodríguez.mp3"\]\[/audio\]

##### [** [Adelfo Rodríguez, ASOCADAR]**] 

###### [27 jul 2015] 

A pesar de que el pasado 23 de julio, la Sala 5ta de Revisión de Tutelas de la Corte Constitucional ordenó “a las autoridades de Policía que se abstengan de llevar a cabo cualquier acto de hostigamiento en contra de tal población, y por el contrario, proteger la vida, integridad, debido proceso e intimidad de la población desplazada de la Hacienda Bellacruz”, los campesinos desplazados denuncian que los amedrantamientos en su contra, continúan.

[Las 260 familias que intentaron retornar a sus tierras el 1ro de julio de este año](https://archivo.contagioradio.com/esmad-agrede-a-20-personas-que-retornaron-a-sus-tierras-al-sur-del-cesar/), tras ser desplazadas en 1996 por grupos paramilitares, dicen recibir la noticia con alegría, pues consideran que están actuando en Derecho, intentado retomar sus prácticas de vida. Sin embargo, afirman que tras el desalojo del 2 de julio en el que fueron "brutalmente atropellados por la policía y el ESMAD, acompañados por civiles encapuchados", y después de que se emitiera el fallo de la Sala 5ta de la Corte, han "visto presencia de personas civiles armadas, al frente de la policía, que vienen a hostigarnos con palabras soeces, borrachos, hasta drogados", asegura Adelfo Rodríguez, de la Asociación de Campesinos Desplazados.

"Nosotros no estamos metiéndonos a parte privada, sino reclamando nuestra propia tierra para crear nuestros proyectos de vida.  Pero el gobierno en vez de proteger al pueblo campesino y trabajador, protege a multinacionales que ni siquiera son del país, que vienen con los megaproyectos que son es proyectos de muerte, que vienen a acabar con la vida de los colombianos y campesinos que estamos luchando aquí por un pedazo de tierra", asegura Adelfo.

Las familias desplazadas de la hacienda Bellacruz, en el departamento del Cesar, esperan que se haga efectivo el pronunciamiento de la Corte, para no sentir más "temor por nuestra vida y nuestra integridad, y que así tengamos la oportunidad nosotros de recuperar nuestras tierras para trabajar en ellas".

<div>

</div>
