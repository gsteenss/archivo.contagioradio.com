Title: Condenados empresarios palmeros de Curvaradó por paramilitarismo
Date: 2015-01-24 01:00
Author: CtgAdm
Category: Hablemos alguito
Tags: Curvarado, Palma, Paramilitarismo
Slug: condenados-empresarios-palmeros-de-curvarado-por-paramilitarismo
Status: published

**[Hablemos Alguito]**:

 <iframe src="http://www.ivoox.com/player_ek_3992320_2_1.html?data=lJ6mlJiWdI6ZmKiakpaJd6Klk4qgo5aVcYarpJKfj4qbh46kjoqkpZKUcYarpJK1w8fQqc7j1JCuzszZrdXjjqjc0MnJssLY0NiYx9LUtsbnwtfW0diPtMLgzsrf0diPqMafpNrfj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Por los delitos de **[concierto para delinquir agravado, desplazamiento forzado e invasión de áreas de especial importancia ecológica, fueron condenados a penas entre 7 y 10 años de prisión 16 empresarios]**, paramilitares y comisionistas de tierras. Entrevistamos a **Manuel Garzón**, representante de las víctimas quién nos explica y contextualiza el desplazamiento y la incursión paramilitar con el fin de despojar de sus tierras a los habitantes del lugar para poder lucrarse cultivando palma africana. Este es un fallo que recupera la memoria histórica, ratifica la versión de las víctimas y abre el camino para otras investigaciones por la multiplicidad de violaciones a los derechos humanos que se continúan violando en el territorio colectivo del Curvaradó. También escuchamos la voz de las víctimas, en este caso de Marta Martínez afectada por el desplazamiento, quién indignada nos cuenta como esta sentencia condena a los empresarios a pocos años de prisión y además no hay garantías sobre la restitución de las tierras y el posible retorno, añade a demás que es el estado colombiano quién debería realizar la reparación total. Por último escuchamos audios del juicio contra los empresarios paramilitares y de los testigos del caso
