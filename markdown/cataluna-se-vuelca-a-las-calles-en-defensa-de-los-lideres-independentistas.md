Title: Cataluña se vuelca a las calles en defensa de los líderes independentistas
Date: 2019-10-17 18:39
Author: CtgAdm
Category: Movilización, Política
Tags: 1-O, Cataluña, Catalunya, independentismo, presos politicos, Referéndum, Tsunami Democràtic
Slug: cataluna-se-vuelca-a-las-calles-en-defensa-de-los-lideres-independentistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/48912793226_95b7ea0e95_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [Oriol Daviu / @fotomovimiento](http://fotomovimiento.org/dia-3-de-respuesta-a-la-sentencia-del-proces-fotogaleria/)] 

El pasado lunes 14 de octubre se hizo pública la sentencia de los presos políticos catalanes que los condena a penas de hasta 13 años de cárcel por la organización del referéndum del 1 de octubre de 2017. Como respuesta, miles de personas salieron a las calles en Cataluña para protestar por esta sentencia condenatoria.

### **Cómo entender lo que ocurre en Cataluña** 

La mañana del 1 de octubre de 2017, millones de catalanes salieron a las calles con la intención de votar acerca del futuro de la nación, pese a las advertencias del gobierno español, que calificaba de ilegal esa consulta. La jornada estuvo cargada de una violencia y represión policial que marcó a la sociedad catalana.

Como precedente, el **9 de noviembre de 2014 **el gobierno de la Generalitat de Cataluña, presidido por Artur Mas, organizó una consulta popular en la que se preguntaba si querían que Cataluña se convirtiera en un estado y si, en caso afirmativo, querían que éste fuera un Estado independiente.

La participación en esa consulta fue de más de 2.3 millones de catalanes (la región tiene 7.5 millones de habitantes). El 80,91% respondió sí a ambas preguntas, el 10,02% sí-no y, finalmente, un 4,49% votó no. Esa consulta, pese a no ser vinculante, impulsó todavía más el movimiento independentista y en las siguientes elecciones, el bloque de partidos nacionalistas obtuvo mayoría absoluta.

En **enero de 2016**, Carles Puigdemont es investido como presidente de la Generalitat de Catalunya. Con su gobierno, se mantiene la “operación diálogo” iniciada con Mas, para negociar un referéndum pactado con el Estado español. Sin embargo, no se llega a un acuerdo y esto impulsa al gobierno catalán a convocar un referéndum de autodeterminación.

El **6 de septiembre de 2017**, el gobierno catalán firma el decreto de convocatoria del referéndum.

El **20 de septiembre de 2017**, días previos a la consulta, fue uno de los más tensos que vivió el movimiento. Esa mañana, la Guardia Civil y la Policía Nacional, desplazada desde otros puntos de España, realizaron una serie de registros en las consejerías del gobierno catalán y se procedió a varias detenciones. Paralelamente, se registraron imprentas y se incautaron millones de papeletas para el referéndum. Ante estos hechos, calificados de ataques a la democracia por parte del gobierno catalán, entidades y por una parte de la sociedad, se produjeron concentraciones espontáneas frente a las consejerías que estaban siendo registradas para expresar el rechazo ante tales hechos.

El **1 de octubre de 2017 **se procede a la celebración del referéndum. Desde las 5 de la madrugada, los ciudadanos empiezan a aglomerarse a las puertas de los colegios, tratando de impedir así su desalojo. Y junto a ellos, las urnas empiezan a llegar bajo un operativo clandestino: se escondieron en casas particulares, almacenes e, incluso, en iglesias. Quienes participaron en esa operación afirman que fue difícil mantenerla en secreto, pero que tenían un compromiso demasiado grande con la causa y que, para ellos, lo más importante era que ese uno de octubre los ciudadanos catalanes pudieran votar.

Muchos pensaban que ante esas aglomeraciones de personas en los colegios electorales podrían acogerse a la orden judicial que ordenaba impedir la votación pero “sin afectar la normal convivencia ciudadana”. Los Mossos d’Esquadra –la policía autonómica catalana–, trataron de mantener esa normalidad (hecho que abrió una investigación al mayor de los Mossos, José Luis Trapero, por desobediencia). No obstante, 6.000 efectivos de la Guardia Civil y Policía Nacional protagonizaron desalojos y un requisamiento del material electoral haciendo uso de la fuerza, causando más de 1.000 heridos.

 

> La Policía desaloja por la fuerza el instituto Pau Claris, de Barcelona. Te estamos contando todo el 1-O aquí ➡️ <https://t.co/HRmaBMf0BF> [pic.twitter.com/FfM1VP82HV](https://t.co/FfM1VP82HV)
>
> — eldiario.es (@eldiarioes) [October 1, 2017](https://twitter.com/eldiarioes/status/914427104414060544?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> La imagen de la derrota del Estado Español. [\#CatalanReferendum](https://twitter.com/hashtag/CatalanReferendum?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/XsxhhTbHa2](https://t.co/XsxhhTbHa2)
>
> — AbriendoLosOjos (@begiakirekiz) [October 1, 2017](https://twitter.com/begiakirekiz/status/914409804449411072?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
 

**16 de octubre de 2017**: la jueza de la Audiencia Nacional envía a prisión incondicional a Jordi Sánchez y a Jordi Cuixart, dos líderes de entidades sobiranistas –Assamblea Nacional Catalana y Òmnium Cultural respectivamente-. Se les acusa de liderar las concentraciones contra las consejerías del 20 de septiembre y su “capacidad de convocatoria”.

**27 de octubre de 2017**: tras el referéndum, el gobierno español aprueba la aplicación del artículo 155, lo que supone la suspensión de la autonomía de Cataluña.

El **2 de noviembre de 2017**, se ordena el ingreso en prisión sin fianza a ocho consejeros del gobierno de la Generalitat de Cataluña, acusados por sedición, rebelión y malversación.

### **La sentencia contra los líderes independentistas** 

El juicio contra los líderes independentistas, conocido como el juicio del “procés”, duró 8 meses y supuso una sentencia condenatoria por unanimidad por sedición y malversación a los presos políticos y activistas de las organizaciones cívicas.

La sentencia, pese a publicarse de manera oficial el pasado 14 de octubre fue filtrada a la prensa el día 11, lo cuál generó una crítica generalizada por parte de la población, ya que llegó antes al público que a los propios condenados.

Las penas impuestas suponen de 9 a 13 años de cárcel. Se les condena por sedición, malversación o desobediencia. Oriol Junqueras, vicepresidente de Cataluña entre enero de 2016 y octubre de 2017, es condenado a 13 años de prisión por los **delitos de sedición y malversación**. También por estos dos delitos son condenados Raül Romeva, Jordi Turull y Dolors Bassa, consejeros del Govern, a 12 años de cárcel.

Por el **delito de sedición **se condena a la expresidenta del Parlament de Catalunya, Carme Forcadell y al exconsejero Josep Rull a 11 años y 6 meses de cárcel. A Joaquim Forn, exconsejero de Interior, a 10 años y 6 meses.

A Jordi Sànchez y Jordi Cuixart, líderes de Assamblea Nacional Catalana (ANC) y Òmnium Cultural, dos entidades de la sociedad civil, reciben una pena de 9 años de cárcel por el **delito de sedición **por liderar y promover el movimiento independentista en las calles.

Asimismo, también se contempló la **condena por desobediencia**, con penas menores, a los exconsejeros Carles Mundó, Meritxell Borràs i Santi Vila a 1 año y 8 meses de inhabilitación.

### **Tensiones en las movilizaciones** 

El mismo **14 de octubre**, tras la publicación de la sentencia, miles de personas se concentran en las calles de distintas localidades catalanas. La principal movilización, organizada por Tsunami Democràtic, se lleva a cabo en el Aeropuerto de Barcelona, donde se desplazan cortando las principales vías. Se procede a cargas policiales que se saldan con más de 130 heridos, algunos de ellos periodistas que, pese a llevar un brazalete que los identifica, son agredidos por los agentes de policía.

 

> Primeras cargas de la [\#policía](https://twitter.com/hashtag/polic%C3%ADa?src=hash&ref_src=twsrc%5Etfw) [\#mossos](https://twitter.com/hashtag/mossos?src=hash&ref_src=twsrc%5Etfw) en el [\#aeroport](https://twitter.com/hashtag/aeroport?src=hash&ref_src=twsrc%5Etfw) del Prat Barcelona. Imposible desalojar a los cientos de personas que ahora lo ocupan y los miles que se dirigen hacia él. No podrán con un pueblo pacífico y democrático en marcha![\#Tsunamidemocratic](https://twitter.com/hashtag/Tsunamidemocratic?src=hash&ref_src=twsrc%5Etfw)[pic.twitter.com/aNZ1Z6xSXz](https://t.co/aNZ1Z6xSXz)
>
> — Manel Márquez (@manelmarquez) [October 14, 2019](https://twitter.com/manelmarquez/status/1183733686828785666?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Asimismo, uno de los heridos pierde un ojo tras el impacto de un proyectil policial. La última víctima que perdió ojo por una pelota de goma en Catalunya fue en las cargas policiales del referéndum del 1 de octubre de 2017. El uso de estos instrumentos está prohibido por ley en Catalunya desde 2014.

En los días posteriores,** 15, 16 y 17 de octubre**, las movilizaciones continúan, con fuertes enfrentamientos entre manifestantes y policía. El miércoles atienden a 96 personas, entre ellas, un joven que presenta un traumatismo craneoencefálico tras ser atropellado por una furgoneta de los Mossos d’Esquadra. Asimismo, se procede a la detención de  46 personas: 4 son enviadas a prisión sin fianza por desordenes públicos, atentados a la autoridad y daños y podrían enfrentar penas de hasta 9 años.

 

> Les Marxes per la Llibertat convocades per l’ANC i Òmnium arriben avui a Barcelona. Les cinc columnes van sortir dimecres de Tarragona, Tàrrega, Berga, Vic i Girona. Una sisena columna s'hi ha afegit avui des de Castelldefels [\#SentènciaProcés](https://twitter.com/hashtag/Sent%C3%A8nciaProc%C3%A9s?src=hash&ref_src=twsrc%5Etfw) [\#Play324](https://twitter.com/hashtag/Play324?src=hash&ref_src=twsrc%5Etfw) <https://t.co/6Chsv3nmw7> [pic.twitter.com/49I8F5rNeU](https://t.co/49I8F5rNeU)
>
> — 324.cat (@324cat) [October 18, 2019](https://twitter.com/324cat/status/1185134634310475777?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**18 de octubre**: comienza una multitudinaria huelga general en el centro de Barcelona, en la que ciudadanos se han sumado a los recién llegados con las Marchas por la Libertad, los cuales salieron el pasado martes desde distintos puntos de Cataluña recorriendo 100 kilómetros a pie hasta llegar a la capital. Según la Guardia Urbana, más de medio millón de personas se encuentran en las calles bajo el lema "Por los derechos y las libertades".

> Entra en Barcelona la primera de las cinco "Marchas por la Libertad" con miles de personas en un ambiente festivo en plena [\#VagaGeneral18O](https://twitter.com/hashtag/VagaGeneral18O?src=hash&ref_src=twsrc%5Etfw) <https://t.co/1aFTUzEcsn>
>
> — EFE Noticias (@EFEnoticias) [October 18, 2019](https://twitter.com/EFEnoticias/status/1185163961655873536?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Paralelamente, la Guardia civil, un cuerpo policial de naturaleza militar, ha empezado a clausurar, por orden de la Audiencia Nacional las páginas web de Tsunami Democràtic, la entidad que organiza las concentraciones como la del pasado lunes en el Aeropuerto de Barcelona, y se les investiga por el supuesto delito de terrorismo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
