Title: Así es el proceso de sanación de las indígenas tras las múltiples violencias
Date: 2020-06-30 23:52
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Exterminio Indígena, Mujer Indígena, Sabiduria Ancestral
Slug: asi-es-el-proceso-de-sanacion-de-las-indigenas-tras-las-multiples-violencias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/78429767_1596619583808287_1315592364445663232_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Mujeres indígenas/ Opiac

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A raíz de los recientes casos conocidos de abuso sexual por parte de integrantes del Ejército contra dos menores de edad indígenas de las comunidades embera Chamí en Puerto Rico, Risaralda y Nukak Maku en San José del Guaviare, las comunidades indígenas expresan que aunque sí existe una responsabilidad institucional en la restitución de los derechos de las menores, es necesario un enfoque étnico que **reconozca la participación activa de médicos tradicionales, un acompañamiento de autoridades indígenas y las dinámicas de cada pueblo, su diversidad lingüística y su ubicación geográfica.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**A propósito, Nazareth Cabrera, lideresa y sabedora de Araracuara** en la Amazonía colombiana afirma que aunque cada uno de los 115 pueblos indígenas del país tienen singularidades y diferencias, desde su territorio puede hablar con propiedad de la mujer uitota, que sin importar su edad y de admitir haber sido víctima de abuso, se somete a un tratamiento espiritual que busca reparar el daño "físico y emocional" a través de conjuros y baños con plantas **"porque nosotros estamos ligados y conectados con la madre naturaleza que nos da la fuerza para la recuperación de ese sentimiento que se tiene atrofiado",** explica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La lideresa explica que la curación no solo concierne a la menor de edad sino en general a toda su familia, "al señalar a una niña se le perjudica moral y psicológicamente por lo que se tiene que contrarrestar todo ese mal pensamiento" que afecta a la mente y al cuerpo y que a medida que la niña crece pueden afectar su realidad, por lo que es necesaria una una fortaleza espiritual, de la mano de sus seres más cercanos. [(Lea también: Agresión sexual del Ejército contra menor embera es un ataque a los 115 pueblos indígenas del país: ONIC)](https://archivo.contagioradio.com/agresion-sexual-del-ejercito-contra-menor-embera-es-un-ataque-a-los-115-pueblos-indigenas-del-pais-onic/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> La mujer es generadora de vida, es por eso que su cuerpo es sagrado
>
> <cite> - Nazareth Cabrera</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que que desde sus orígenes y los relatos de sus ancestras, la mujer indígena siempre ha sido víctima de abusos sexuales, "es una realidad escondidas del país y del Estado", fenómeno que se incrementó con el conflicto armado y la entrada y salida de grupos armados legales e ilegales de sus territorios. **Según cifras oficiales, la población indígena representan el 2,6 % de las 8.604.210 víctimas del conflicto armado en el país**; hechos victimizantes que a lo largo de la historia han producido un desarraigo de su territorio y por tanto de sus tradiciones, rituales y modo de vida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La mayor de las afectaciones a indígenas: una educación impuesta

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La sabedora señala que aunque no son ajenos a los cambios del mundo y su difusión a través de los medios de comunicación, la cultura ancestral predomina en s**us tradiciones a través de plantas medicinales, los conjuros, y los rezos, en particular para las mujeres quienes a través de la siembra como primera escuela,** no solo aprenden cómo sembrar sino otros conceptos como el respeto por el cuerpo, la sexualidad, representada a través de plantas y la unión de la naturaleza y el ambiente como hombre y mujer.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello señala que el mayor problema y quizá la raíz de las dificultades que se ven en la actualidad alrededor de los territorios proviene de cómo la civilización occidental y una idea errada de desarrollo "confunden a los niños y niñas indígenas, porque el sistema de educación nos obliga a no seguir con nuestra espiritualidad", dejando de lado las enseñanzas ancentrales de los indígenas que hoy son cerca de 1.900.000 personas que representan e 4,4 % de la población según el censo nacional de 2018. [(Le recomendamos leer: En Amazonas recurren a la medicina tradicional para protegerse el covid-19)](https://archivo.contagioradio.com/en-amazonas-recurren-a-la-medicina-tradicional-para-protegerse-del-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**La Organización Nacional Indígena de Colombia [(ONIC)](https://twitter.com/ONIC_Colombia) ha advertido que son 39 los pueblos indígenas que están en peligro de extinguirse física y culturalmente en el país, debido no solo a los factores que señala la lideres sino otros como el conflicto y la ausencia de acceso a la educación**, incluidos los embera chamí, como consecuencia del desplazamiento, la pobreza extrema, la discriminación, y el sobrevivir en medio del fuego cruzado de grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma los Nukak Maku, nomadas que habitan las selvas del Guaviare y mencionados en un nuevo caso de abuso sexual contra una menor de edad por parte del Ejército, desde el primer contacto regular en 1988, cerca del 50% de su población ha muerto y ha sido reducido a 200 familias que requieren atención en términos de salud, educación, vivienda y seguridad alimentaria.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
