Title: Comunidades buscan acuerdos humanitarios para acabar con asesinatos de líderes
Date: 2019-06-10 13:57
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Francia Marqués, lideres sociales
Slug: comunidades-acuerdos-humanitarios-acabar-asesinatos-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-10-at-1.19.48-PM-e1560192625393.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-10-at-1.19.49-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Comunidades negras, indígenas y mestizas del departamento del Cauca **estarían trabajando una serie de acuerdos humanitarios con los actores armados presentes en el territorio**, con el objetivo de definir  mecanismos que acaben con el asesinato de líderes sociales y ex combatientes de las FARC que asolan a los territorios. (Le puede interesar:["Atenta contra Francia Márquez y otros líderes de comunidades negras del Norte de Cauca"](https://archivo.contagioradio.com/atentan-contra-lideresa-francia-marquez/))

Según **Francia Márquez, lideresa del Proceso de Comunidades Negras (PCN)**, esta iniciativa se da en medio de una actitud irresponsable del Gobierno hacia los líderes sociales. Según ella**, si el gobierno no responde, son las comunidades las que debe asumir la tarea** puesto que no se puede permitir que siga la masacre de líderes y lideresas que completa cerca de 600 personas asesinadas desde la firma del acuerdo de paz con FARC en 2016.

Márquez también resaltó que **es necesario que la sociedad en general se manifieste, no solamente en las redes sociales, sino a través de acciones concretas de solidaridad**, de respuesta al Gobierno y de compromiso con la construcción de la paz. “Cuando me felicitan en la calle por mi lucha, me pregunto para mi misma ¿y la suya pa cuándo?” cuestionó la lideresa en medio de una intervención. (Le puede interesar:["Asesinan a Julián Quiñones, líder que denunció hechos de corrupción en Coveñas"](https://archivo.contagioradio.com/asesinan-julian-quinones-lider-denuncio-corrupcion-covenas/))

Este anuncio de la posibilidad de alcanzar acuerdos humanitarios regionales se dio en el marco de un evento citado por el Observatorio de Realidades Sociales, a cargo del **arzobispo de Cali, monseñor Dario de Jesús Monsalve**, en el que se invitó a toda la sociedad colombiana a ser escudo para los líderes sociales, y así defender el tejido social que es el que más estaría siendo destruido por los constantes ataques contras las personas en los territorios.  (Le puede interesar:["Con diálogos para la no repetición, Comisión para el Esclarecimiento de la Verdad promoverá defensa de líderes sociales"](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/))

\[caption id="attachment\_68622" align="aligncenter" width="517"\]![Comunidades hablando de Acuerdos Humanitarios por los líderes](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-10-at-1.19.48-PM-e1560192625393-300x170.jpeg){.wp-image-68622 width="517" height="293"} Encuentro del Observatorio de Realidades Sociales de la aquidiócesis de Cali\[/caption\]

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
