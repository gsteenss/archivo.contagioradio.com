Title: Por segunda vez suspenden consulta popular en Córdoba, Quindío por falta de recursos
Date: 2018-01-16 17:16
Category: Ambiente, Nacional
Tags: consulta popular, cordoba, Mauricio Cárdenas, Ministerio de hacienda, Quindío
Slug: consulta_popular_suspendida_cordoba_quindio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/5a05cc36c0e8c.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Laura Sepúlveda] 

###### [16 Ene 2018] 

Una vez más, deberá definirse una nueva fecha para la realización de la consulta popular en el municipio de Córdoba en el departamento del Quindío debido a la falta de respuesta por parte del Ministerio de Hacienda, que se negó a tramitar cerca de 20 millones de pesos que son los recursos necesarios para llevar a cabo dicho evento electoral.

La consulta, que **estaba programada para el próximo domingo 21 de enero, ha sido suspendida ya que, según el Ministro de Hacienda, Mauricio Cárdenas, dijo que** al gobierno no le corresponde costear dicho mecanismo de participación ciudadana, sino que se trata de una responsabilidad que tiene cada una de las entidades territoriales.

### **¿Cuánto cuesta la consulta?** 

Cabe recordar el análisis que hace el constitucionalista Luis Arturo Ramírez Roa, quien señala que según el presupuesto para el 2017, la Registraduría Nacional contaba con **un total de \$44'769.000.000 para la financiación de Asuntos electorales y procesos democráticos.** Además, de acuerdo con la Ley de participación ciudadana, la 1757 de 2015, había \$10.189.000.000 en el Fondo para la Participación Ciudadana, y \$1.252.000.000 en el ítem de Participación Ciudadana.

Es decir que había más de \$56.000.000.000 destinados para asuntos como las consultas populares, y precisamente, la primera fecha de la consulta era el 3 de diciembre. Además, la alcaldía de Córdoba ha dicho que según sus indagaciones, la consulta tendría un costo que oscilaría entre los 10 y 20 millones de pesos*. *Es decir que había dinero de sobra para esta, y otras consultas en los diferentes municipios del país.

No obstante, los ambientalistas ya tenían previsto que no hay voluntad de parte del gobierno para girar los recursos, por eso el Comité Ambiental del Quindío, interpuso una denuncia penal ante la Fiscalía General de la Nación, contra el Ministro de Hacienda, quien habría incurrido en **el delito de fraude a resolución judicial. De ser así, este funcionario público podría pagar hasta 6 meses de prisión, además de una multa entre 5 y 50 salarios mínimos legales vigentes.**

"Interpusimos esta denuncia ante la Fiscalía General de la Nación contra el ministro Cárdenas buscando, no solo que se le investigue y sancione por sus abiertas vulneraciones a la norma penal y constitucional, sino también para agotar los requisitos que nos permitirán accionar ante tribunales internacionales de ser necesario", dice el comunicado de los ambientalistas. (Le puede interesar: [Denuncian a Minhacienda por desacatar fallo del Tribunal del Quindío)](https://archivo.contagioradio.com/demanda_minhacienda_fallo_consulta_popular_cordoba_quindio/)

### **48 títulos mineros en la zona** 

En esa región hay alrededor de 48 títulos mineros otorgados, según indica el secretario de Agricultura, Desarrollo Rural y Medio Ambiente del Quindío. "**La gran preocupación que asiste  a esos títulos es que quien sea propietario de uno de ellos solo necesitaría una licencia ambiental **que la otorga la Agencia Nacional de Licencias Ambientales, Anla, y que permitiría que inmediatamente se empiece a hacer la explotación a gran escala y ese es un riesgo que hoy corremos en el departamento[**", dijo el secretario al diario Crónica del Quindío.**]

Por el momento, los pobladores siguen en pie hasta lograr que la consulta popular sea un hecho, y esperan que tanto la denuncia penal, como la movilización de la comunidad den resultado y puedan estipular una nueva fecha para la consulta.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
