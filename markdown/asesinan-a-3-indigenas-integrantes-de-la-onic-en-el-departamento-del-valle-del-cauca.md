Title: Asesinan a 3 indígenas integrantes de la ONIC en el departamento del Valle del Cauca
Date: 2018-05-29 16:40
Category: DDHH, Nacional
Tags: Chami, indígenas, Nasa, ONIC, Valle del Cauca
Slug: asesinan-a-3-indigenas-integrantes-de-la-onic-en-el-departamento-del-valle-del-cauca
Status: published

###### [Foto: Nasa] 

###### [29 May 2018] 

La Organización Nacional de Indígenas de Colombia denunció **el asesinato de dos de sus integrantes del pueblo Nasa y un indígena del pueblo Cham**i. Los tres hechos ocurrieron en el departamento del Valle del Cauca y hasta el momento la ONIC ha exigido medidas de protección a las diferentes autoridades para los resguardos indígenas que se encuentran en el territorio.

Los dos indígenas Nasa fueron identificados como Pablo Emilio Dagua y Adriana Montero, ambos eran compañeros sentimentales. De acuerdo con Oscar Montero, miembro de la Consejería de DD.HH Nasa, **los indígenas estaban desaparecidos desde el pasado sábado 26 de mayo y ayer sus cuerpos fueron encontrados** por la guardia indígena en el corregimiento de la Delfina, con signos de tortura.

El indígena Chamí fue identificado como Amilcar Yagarí Siagama, perteneciente al resguardo Garrapata; fue asesinado el día de ayer, en horas de la noche. Yagarí pertenecía a la Guardia Indígena, era ex gobernador de la comunidad **y actualmente se desempeñaba como vocero del territorio**. (Le puede interesar:[Indígenas Jiw, en el Meta, completan 5 meses sin recibir atención humanitaria"](https://archivo.contagioradio.com/indigenas-jiw-del-meta-completan-5-meses-sin-recibir-atencion-humanitaria/))

Frente a quiénes estarían detrás de estos hechos, Montero afirmó que no se tenía conocimiento de amenazas hacia ninguno de los indígenas. Sin embargo, señaló que las comunidades indígenas y **sus voceros han sido víctimas en diferentes ocasiones de estigmatización por parte de los grupos armados que operan en el territorio**.

Montero afirmaron que desde la firma del acuerdo de paz hasta la fecha, han sido asesinados 50 líderes indígenas, razón por la cual piden acompañamiento internacional y de organizaciones defensoras de derechos humanos, y de esta manera continuar en la defensa del territorio ancestral.

<iframe id="audio_26244617" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26244617_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
