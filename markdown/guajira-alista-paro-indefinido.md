Title: La Guajira alista paro cívico indefinido
Date: 2017-09-05 13:25
Category: DDHH, Nacional
Tags: Ambiente, crisis en la Guajira, Derechos Humanos, Guajira
Slug: guajira-alista-paro-indefinido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/guajira-paro.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CENSAT Agua Viva] 

###### [05 sept 2017] 

Las comunidades en el departamento de la Guajira definirán el **20 de septiembre la fecha para comenzar un paro cívico** en el cual pondrán en evidencia la crisis en la que está el departamento. Le exigirán al Gobierno Nacional la creación de políticas públicas para mitigar los impactos de la minería que derrumbó la vocación agrícola y comercial de un territorio que ha perdido, entre muchas cosas, la soberanía alimentaria.

De acuerdo con Felipe Rodríguez, presidente del Comité Cívico por la Dignidad de la Guajira, en 1980 la Guajira le aportaba al país, **en materia agrícola y comercial el 80% del PIB a la nación**. Cuando a finales del siglo pasado se impuso la vocación minera, se quebró el sector agrícola y hoy no llega a aportar el 7%. Dijo además que el 60% de la minería que se realiza en el país ocurre en los territorios guajiros.

Ante esto, anunció que **“el departamento ha perdido la capacidad de producir sus propios alimentos y las personas se están muriendo de hambre”**. Adicionalmente, los procesos extractivos “han aumentado la desigualdad social en los ingresos, el 60% de las personas que viven en la Guajira ganan menos de un salario mínimo porque la minería acabó con los empleos solo el 2.9% de los empleos los genera la minería y el 87% son trabajos informales”. (Le puede interesar: ["Costos ambientales de acción de Cerrejón en la Guajira sigue en aumento"](https://archivo.contagioradio.com/cerrejon-guajira-costos-ambientales/))

**A nivel ambiental la Guajira está destruida**

Rodríguez manifestó que ese departamento es **87% semidesértico y la minería ha destruido cerca de 12 mil hectáreas de bosque seco**. También dijo que hay 10 arroyos y 2 lagunas arruinadas dejando al territorio en un “estrés hídrico”.

La situación se agrava en la medida en que la Guajira tiene **172 mil hectáreas de tierra con títulos mineros,** esto equivale al 40% del territorio. De estos títulos el 70% se encuentran en zonas aledañas a la cuenca del río Ranchería que “es el que impide que el desierto se trague la Sierra Nevada de Santa Martha y la vida de una sociedad de más de un millón de personas”.

La riqueza que hay en este lugar, según Rodríguez, **“se ha convertido en la cereza de la geopolítica de los hidrocarburos”.** Esto pues allí se encuentra el 56% de las reservas probadas de carbón, el 35% de las reservas de gas del país y la capacidad de producir 11,200 barriles de petróleo. (Le puede interesar: ["El Cerrejón buscaría desalojar 27 familias para desviar el arroyo Bruno"](https://archivo.contagioradio.com/cerrejon-buscaria-desalojar-27-familias-wayuu-desviar-arroyo-bruno/))

**Guajiros están organizando procesos de unidad**

Ellos y ellas están enfocados en construir una unidad que les permita exigirle al gobierno la creación de políticas públicas que respeten su territorio. Han dicho que **no están en contra de la minería** pero creen que es necesario que estos procesos dejen una mayor renta, que no se destruyan los arroyos y los ríos y que existan procesos de diversificación de la producción minera.

Igualmente han dicho que es necesario **que se recupere la soberanía alimentaria por medio de la recuperación del sector agrícola** y que el Gobierno Nacional debe invertir en obras de acueducto para solucionar el problema del agua potable.

Finalmente, Rodríguez indicó que el 20 de septiembre realizarán un foro sobre la biodiversidad, el agua y el extractivismo. Allí decidirán las fechas del paro cívico indefinido que contará con **las experiencias que recojan del intercambio con manifestantes del paro de Buenaventura y Chocó.**

<iframe id="audio_20705617" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20705617_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
