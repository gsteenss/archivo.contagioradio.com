Title: El Estado, la prensa y los ciudadanos: una relación conflictiva
Date: 2015-07-09 12:06
Category: Opinion, Ricardo
Tags: Bacrim, Libertad de expresión, Libertad de Prensa, Periodistas
Slug: el-estado-la-prensa-y-los-ciudadanos-una-relacion-conflictiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/libertad-prensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto: Cronica Viva] 

#### **[Por [Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) - @ferr\_es]** 

***“La prensa no puede ser, en estos tiempos de creación, mero vehículo de noticias, ni menos sierva de intereses, ni mero desahogo de la exuberante y hojosa imaginación...”  ***José Martí:

El periodismo colombiano ha sufrido la misma suerte de la nación: un modelo de Estado gestor de violencia, con monopolios políticos, desigualdad social y polarización extrema. Dado que nunca hemos tenido paz, los periodistas sufrimos la presión activa de todos los actores en conflicto.  Así, cuando se escala el nivel de la guerra, los efectos se sienten en el ejercicio del periodismo, por pura inercia.

Tenemos que abandonar la vanidad del ***periodista estrella***. Es muy pretencioso exigir el blindaje absoluto al gremio, cuando el resto de la sociedad sigue bajo fuego. Nuestra suerte es la misma del ciudadano. Así de simple.

Así como el Estado es centralista, persiste el abismo entre la prensa “nacional” y la prensa de “provincias”, que por cierto es la que más muertos pone. Los sueldos, la relación con las autoridades, el trato con la fuerza pública, cambian drásticamente en la periferia.

El principal actor del conflicto armado sigue siendo el Estado: Medieval en su pensamiento, primitivo en su organización, excluyente en su modelo económico, brutal en su ejercicio del poder y muy tacaño en oportunidades. Estamos lejos de consolidar una identidad nacional, cuando el Estado ataca toda iniciativa comunitaria, destruye redes sociales, economías solidarias y criminaliza la protesta. Buscamos ***un contrato social solidario*** porque el Estado no cumple con la protección de la vida, honra y bienes de TODOS los compatriotas. Esto lo sabemos los periodistas en las provincias, lejos del poder.

Hay regiones donde las emisoras únicamente pueden moler música, para no incomodar a los poderes locales. Ocurre en casi todos los municipios del Magdalena medio, la costa Atlántica, el litoral Pacífico, en los llanos, las selvas del sur, donde la autoridad realmente está subordinada a otras fuerzas. Regiones donde sólo se escuchan las noticias “nacionales”, y toda la información local queda excluida.

¿Qué tipo de democracia tenemos en Colombia, cuando en la mayor parte de los municipios es imposible cuestionar al alcalde, y mucho menos denunciar los peculados y fraudes en la administración pública? ¿En cuantas regiones la Fuerzas Armadas y los grupos beligerantes le imponen al periodista el manual de estilo? Los periodistas son obligados a adjetivar cuando relatan situaciones de orden público, y sus titulares son alterados por los directores.

Los periodistas de Urabá y el Bajo Cauca, en Antioquia, siguen en el oficio pese a la muerte violenta de sus colegas. Viven en pueblos donde los alcaldes tienen evidentes relaciones con las estructuras paramilitares, los narcotraficantes y las  llamadas Bacrim. Luego de los funerales del último caído, reiteran las denuncias, piden protección, la “gran prensa nacional” se solidariza con ellos, pero la situación sigue invariable.  \[1\]

Lejos de la gran pantalla, los periodistas de la periferia viven en condiciones de precariedad y pobreza. Pero siguen ahí, defendiendo los derechos humanos, el erario público, a los campesinos y a las minorías.

Los periodistas de la periferia, como los ciudadanos de “provincia” tenemos el derecho a soñar con un nuevo modelo de Estado. Si se consolida un proceso de paz entre los actores del conflicto, queremos aportar en el **proceso de reconstrucción nacional**. En las emisoras, un campesino hablará de economía, un profesor indígena hará propuestas para un nuevo modelo cultural, los niños nos contarán sobre sus proyectos ecológicos y la siembra de un nuevo país. Posiblemente el humorista podrá reírse de las elites.

Soñamos. Mientras tanto, como notarios de la historia, los periodistas tendremos la memoria insumisa.

###### [\[1\] Segundo encuentro “Periodistas, daño, memoria y reparación”, Medellín, 12 y 13 de junio de 2015.] 
