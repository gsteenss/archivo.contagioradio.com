Title: En 2017 han sido asesinados 15 líderes sociales en el Cauca
Date: 2017-07-17 12:58
Category: DDHH, Nacional
Tags: Asesinatos a líderes comunitarios, Cauca, Defensores Derechos Humanos, paramilitares
Slug: en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/cauca-lideres-sociales-e1500314265502.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

###### [17 Jul 2017] 

Según la Red de Derechos Humanos del Suroccidente Colombiano, Francisco Isaías Cifuentes, en lo que va corridio del 2017 **han sido asesinados 15 líderes sociales en el Cauca,** haciendo de este departamento el más peligroso para la actividad de defensa de los derechos humanos y del ambiente.

La situación de seguridad de los líderes del Cauca se agrava cada día y según organizaciones de derechos humanos, hasta el momento **no se ha implementado ningún mecanismo que impida que líderes y lideresas sigan siendo asesinados y amenazados.** El caso más reciente fue el asesinato del líder afrodescendiente, Héctor William Mina, y la posterior intimidación de la que fue parte su hijo, Andrés Mina. (Le puede interesar: ["Asesinado en Cauca, Héctor William Mina, líder e integrante de Marcha Patriótica")](https://archivo.contagioradio.com/asesinado-hector-william-mina-marcha-patriotica/)

Los asesinatos y amenazas en el Cauca, según organizaciones que protegen los derechos humanos y el Ministerio del Interior, se presentan en su mayoría por **la oposición a las actividades de extracción minera.** De igual forma, la Red de Derechos Humanos estableció que en ocasiones los asesinatos se dan contra miembros de los mecanismos para la protección de la soberanía e independencia de los territorios como la Guardia Cimarrona.

Para Deivin Hurtado, miembro de esta Red de Derechos Humanos, “en el Cauca han ingresado grupos ilegales que quieren asegurar el control territorial”. Así mismo, afirmó que, en municipios como Caloto y Corinto, “**las amenazas a través de panfletos y grafittis dan cuenta de la presencia de grupos paramilitares** en la región pues sus mensajes son alusivos a las AUC". (Le puede interesar: ["Presidente de Sintrainagro en Guacarí fue asesinado en el Valle del Cauca"](https://archivo.contagioradio.com/presidente-de-sintrainagro-fue-asesinado-en-el-valle-del-cauca/))

Ante hechos como estos, las organizaciones sociales han rechazado afirmaciones como las de la Fiscalía General de la Nación quien asegura que la cifra de asesinatos de defensores de derechos humanos está en descenso.

### **Asesinato de William Mina es un golpe para las guardias indígenas y campesinas y Cimarronas** 

En el caso concreto del asesinato de Héctor Mina, Deivin Hurtado manifestó que “él venía adelantando una labor de protección de los derechos humanos de la población afrodescendiente”. En el momento de sus obras fúnebres su hijo, **Andrés Mina, fue intimidado por un hombre** que, según Hurtado, “se quedó mirándolo y le hizo señas de que tenía un arma”. Afortunadamente, Mina pudo adentrarse en la multitud de personas que se encontraban en el lugar. (Le puede interesar: ["Informe de Fiscalía sobre asesinatos a defensores de DDHH es otro "falso positivo"](https://archivo.contagioradio.com/informe-de-fiscalia-sobre-asesinatos-a-defensores-de-dd-hh-es-otro-falso-positivo/))

Para Hurtado, **este tipo de amenazas corresponden al establecimiento de grupos armados ilegales que quieren tener el control del territorio**. “Se cree que el asesinato puede tener relación con el hecho de que no se ha permitido el ingreso al territorio de los grupos ilegales que están reclamando el control”. Hurtado hizo énfasis que Guachené, donde ocurrió el crimen, es un corredor importante para llegar al norte del Cauca y al sur del Valle del Cauca.

<iframe id="audio_19843284" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19843284_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
