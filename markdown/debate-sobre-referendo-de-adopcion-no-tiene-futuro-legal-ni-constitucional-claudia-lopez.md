Title: "Debate sobre referendo de adopción no tiene futuro legal ni constitucional" Claudia López
Date: 2016-09-15 17:19
Category: Nacional
Tags: adopcion gay, parejas homosexuales, referendo adopción
Slug: debate-sobre-referendo-de-adopcion-no-tiene-futuro-legal-ni-constitucional-claudia-lopez
Status: published

###### [Foto:ColombiaGayPride] 

###### [15 de Sept de 2016] 

La comisión primera del Senado **aprobó en primer debate, el proyecto de ley de referendo que prohibiría la adopción por parte de parejas del mismo sexo** o de personas sin pareja como madres solteras o padres solteros. Sin embargo, para algunos sectores de la sociedad la discusión se abre más allá de la adopción y tiene alcances hasta los derechos de las minorías, los derechos fundamentales y el propio papel de la Corte Constitucional.

Aunque al proyecto le faltan 3 debates más en el Congreso de la República, ya está siendo objeto de debate. **Una de las razones es la posibilidad de que se encuentre viciado, puesto que en la recolección de firmas** se formuló una pregunta y en la sesión del congreso, realizada este 14 de septiembre, se formuló otra. Para algunos analistas el proyecto ya contiene vicios de forma y para otros, el cambio de pregunta no afectaría el proceso del trámite y la aprobación.

El constitucionalista y catedrático Rodolfo Arango explica que la Corte Constitucional ya ha legislado sobre la materia en varias ocasiones, como lo fue[la aprobación de la adopción](https://archivo.contagioradio.com/adopcion-igualitaria-no-puede-ser-llevada-a-consulta-ciudadana-abogado-german-rincon/)o cuando se **aprobó que las familias pueden estar constituidas por un hombre y una mujer o por parejas del mismo sexo, es decir, con igualdad de derechos**.

Por otra parte, resalta que es problemático el tema de este referendo, puesto que **“no se puede convocar a las mayorías para decidir sobre derechos fundamentales”**, se podría consultar en caso de una decisión sobre asuntos políticos o económicos que atañen a todos y todas pero no sobre los derechos de las minorías, en ese sentido afirma que["Sin duda alguna se está privando de derechos a una minoría constitucional"](https://archivo.contagioradio.com/referendo-contra-adopcion-igualitaria-no-solo-perjudicaria-parejas-homosexuales/).

En la misma vía, la senadora por el partido verde Claudia López, afirma que **el 40% de las familias en Colombia se conforman por madres solteras** y explica que el hecho de que el proyecto haya superado el primer debate es **"un total absurdo, que entre otras cosas se dio por favores políticos"**, motivo por el cual, considera que pese a que el referendo propuesto es inconstitucional y discriminatorio, lo que debe suceder es que se continúe con los debatesa partir de argumentos y educación.

El proyecto de ley de la senadora Vivian Morales, fue presentado el 21 de julio de este año,  y va en contravia la de la decisión de la Corte Constitucional, que en **el año 2015 expresó que da vía libre a la adopción igualitaria en Colombia**. De igual forma la Corte dejó  constancia de que durante el debate primaron las posiciones e**n favor de reconocer que las parejas homosexuales son tan idóneas como las heterosexuales para criar niños adoptados.**

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
