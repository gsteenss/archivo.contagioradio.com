Title: Se libra una guerra por tomar el control del Nudo de Paramillo
Date: 2019-04-15 22:10
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, Caparrapos, cordoba, Desplazamiento
Slug: guerra-control-nudo-paramillo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/maxresdefault.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tavo Radio] 

Este fin de semana se presentó un nuevo desplazamiento masivo en Córdoba, 589  indígenas y campesinos tuvieron que salir de sus territorios en Tierralta, para evitar quedar en medio de confrontaciones entre grupos armados ilegales. Se trata del segundo desplazamiento de tal magnitud que se produce en la región, que ya registra más de 2 mil personas afectadas, y sin la respuesta necesaria para solucionar su situación por parte de las entidades oficiales.

Como lo denunció la Oficina de Naciones Unidas para la Coordinación de Asuntos Humanitarios (OCHA) en Colombia, enfrentamientos en la parte alta del municipio de Tierralta causaron el desplazamiento de 589 personas de un resguardo indígena a otro. Según Arnobis de Jesús Zapata, integrante de la Asociación Campesina del Sur de Córdoba, en los enfrentamientos se vieron involucrados miembros de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y los Caparrapos.

> \[EHP\] Flash Update N° 1 - Desplazamiento masivo en Tierralta (Córdoba) - <https://t.co/MQPWMskXsw> [\#ConLosDesplazados](https://twitter.com/hashtag/ConLosDesplazados?src=hash&ref_src=twsrc%5Etfw) [\#PR20](https://twitter.com/hashtag/PR20?src=hash&ref_src=twsrc%5Etfw) [@GerardGomez59](https://twitter.com/GerardGomez59?ref_src=twsrc%5Etfw) [pic.twitter.com/RQ3IgB30Kp](https://t.co/RQ3IgB30Kp)
>
> — Ocha Colombia (@ochacolombia) [13 de abril de 2019](https://twitter.com/ochacolombia/status/1117199537658109952?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **¿Quién manda en el Parque Nacional Natural Paramillo?** 

Como lo han explicado varios habitantes de la región, en la frontera entre Antioquia y Córdoba (limites del Parque Nacional Natural Paramillo) operan al menos tres grupos diferentes: los Caparrapos, el Nuevo Frente 18 y las AGC. En la parte alta del territorio se encuentran los Caparrapos en unión con el Nuevo Frente 18, hombres que no se acogieron al proceso de paz; mientras que  las AGC operan en la parte baja del Departamento. (Le puede interesar: ["Cerca de 600 indígenas y campesinos fueron desplazados en Córdoba: ONU"](https://archivo.contagioradio.com/desplazan-600-indigenas-y-campesinos-en-tierraalta-onu/))

La disputa por la zona se debe a que se trata de una ruta que conecta al occidente con el Chocó y Urabá, al norte con la costa atlántica y al sur con el centro del país. Adicionalmente, es un lugar con presencia de cultivos de uso ilícito, mientras que el Nudo de Paramillo sirve como refugio (dadas las dificultades de acceso) para actores ilegales. De acuerdo a lo expresado por Zapata, los Caparrapos intentan ganar espacio en la región, mientras las AGC se defienden enviando sus 'fuerzas especiales' para mantener control sobre la zona; esta situación explica los constantes enfrentamientos que se presentan.[ ]

Entre tanto, según lo mencionado por el líder campesino, el Ejército se ha mantenido al margen de las confrontaciones, no ejerce control sobre la zonas y pareciera no estar interesado en hacerlo, pues las operaciones de la Fuerza Pública llegan hasta los corregimientos, pero no a las veredas en las cuales se presentan los enfrentamientos. (Le puede interesar: ["Campesinos de Córdoba resisten en el territorio pese a amenazas de grupos armados"](https://archivo.contagioradio.com/campesinos-cordoba-resisten-armados/))

### **Ayudas para los desplazados son insuficientes y no hay garantías de retorno** 

Zapata señaló que a Tierralta fueron enviadas ayudas por parte de entidades oficiales, pero son insuficientes y no solucionan la situación por la que se vieron obligados a desplazarse. Por otra parte, las más de 500 familias que tuvieron que refugiarse en el corregimiento de Juan José, municipio de Puerto Libertador, solamente han recibido atención por parte de la Alcaldía, y tanto los albergues como la comida brindados no alcanzan para atender su situación.

En ambos casos de desplazamiento las familias han decidido esperar para regresar a sus territorios, pues aguardan a que cesen los enfrentamientos para poder continuar con su vida; pero como lo recordó el integrante de la Asociación de Campesinos, en días recientes el General de la Séptima División del Ejército afirmó que ya habían tomado control en la circunscripción de Puerto Libertador, y cuando algunas familias regresaron a sus tierras fueron golpeadas y amenazadas.

Afortunadamente las familias agredidas pudieron escapar de sus captores y regresar al corregimiento de Juan José, pero pasado casi un mes desde su salida, aún no han recibido el reconocimiento en su condición de desplazamiento forzado por lo que no se presentan soluciones para ellos en el mediano y largo plazo. (Le puede interesar: ["Fuerza de Tarea Aquiles no evitó desplazamiento de más de 120 familias en Bajo Cauca"](https://archivo.contagioradio.com/fuerza-de-tarea-aquiles-no-evito-desplazamiento-de-mas-120-familias-en-bajo-cauca/))

<iframe id="audio_34519075" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34519075_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_34519252" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34519252_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
