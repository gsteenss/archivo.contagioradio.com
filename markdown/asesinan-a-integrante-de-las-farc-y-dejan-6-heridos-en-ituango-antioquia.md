Title: Asesinan a integrante de las FARC y dejan 6 heridos en Ituango, Antioquia
Date: 2017-11-12 12:34
Category: DDHH, Nacional
Tags: FARC, Ituango
Slug: asesinan-a-integrante-de-las-farc-y-dejan-6-heridos-en-ituango-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/dalider.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [12 Nov 2017] 

Habitantes de Ituango denunciaron que en la noche de ayer, sobre las 7:00pm se desarrolló una balacera en el sector La Peatonal, **en donde hasta el momento se confirma el asesinato de Daladier Ortíz, integrante de las FARC** que ya había sido beneficiado con la amnistía, de igual forma hasta el momento hay un registro de 6 personas heridas, una de ellas sería una mujer embarazada.

Los habitantes manifestaron que después de que se presentó la balacera, las autoridades intimidaron  e intentaron detener al defensor de derechos humanos **Oscar Arango, integrante de la plataforma Ríos Vivos,** que se encontraba en el lugar de los hechos y se disponía a hacer un reporte de lo mismo. Dalaider tenía 26 años y era beneficiaron de indulto, salió el pasado 22 de noviembre del año pasado e ingresó a la zona de transición en la vereda Santa Lucía.

Se presume que detrás del asesinato podrían estas estructuras paramilitares, ya que de acuerdo a la información dada por los habitantes, se había denunciado un plan de los paramilitares con la intención de reclutar a los milicianos de las FARC, **plan que habría comenzado con el asesinato de alias Molina, integrante de las FARC** que dirigía las entonces milicias de esta guerrilla en el territorio. Así mismo, señalaron que un paramilitar se “vanagloreaba” de haber asesinado a este hombre incluso delante de las autoridades.

Hasta el momento las 6 personas heridas ya fueron trasladadas a los diferentes centros hospitalarios, mientras que el joven fue trasladado a Medellín debido a la gravedad de sus heridas, también se está averiguando si el sería otro beneficiario de las amnistías. ([Le puede interesar: "Atentan contra dos integrantes de la FARC en Ricaurte Nariño"](https://archivo.contagioradio.com/atentan-contra-dos-integrantes-de-las-farc-en-ricaurte-narino/))

Noticia en desarollo ...
