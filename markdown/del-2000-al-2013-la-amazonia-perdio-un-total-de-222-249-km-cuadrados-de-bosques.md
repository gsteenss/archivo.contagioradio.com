Title: Del 2000 al 2013 la Amazonía perdió un total de 222.249 km cuadrados de bosques
Date: 2015-12-01 11:36
Category: Ambiente, Nacional
Tags: Amazonía colombiana, Calentamiento global, COP 21, París, Tala indiscriminada en Colombia
Slug: del-2000-al-2013-la-amazonia-perdio-un-total-de-222-249-km-cuadrados-de-bosques
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/amazonas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:semana.com 

###### [30 Nov 2105]

La Conferencia contra el calentamiento global COP 21 que se realiza en Le Bourget, al norte París tendrá en su agenda un tema de carácter urgente para el mundo y que a los latinoamericanos les compete defenderlo**: El Amazonas, el bosque más grande y uno de los más diversos del mundo, según un estudio publicado por la Red Amazónica de información Socioambiental Georreferenciada (RAISG)**, muestra que entre el 2000 y el 2013 esa región perdió un total de 222.249 kilómetros cuadrados, superficie equivalente al Reino Unido.

En este estudio también se evidencia que **la Amazonía colombiana perdió 7851 Km₂**, principalmente por la expansión de la frontera agrícola y la minería ilegal; **Venezuela perdió 3.3 % de Bosque**; **Surinam, Guyana y Guyana Francesa perdieron 3.2% de bosque en conjunto**; **Ecuador por su parte sufrió una deforestación del 10.7% de su bosque original**; **Perú perdió 9.1 % de  bosque amazónico**; **Bolivia perdió 10.000 km₂** de bosque y **Brasil 174000 Km₂**.

Estos datos evidencian una reducción vertiginosa de las selvas en tan solo 13 años debida en su mayoría a tres principales **causas como la ganadería, la agricultura mecanizada y la agricultura en pequeña escala**, sin embargo la explotación petrolera y la minería son hechos que también están arrasando bosques.

De esta forma después de los atentados de París los dirigentes del mundo han mantenido la firme voluntad de realizar la COP21 en donde se reunirán 195 comisiones nacionales, muchas presididas por los jefes de Estado, como Barack Obama, Dilma Rousseff y Juan Manuel Santos (quien estará dos días). **Los mandatarios discutirán en esta conferencia de las Naciones Unidas un acuerdo global con miras al año 2100 el cual limite el aumento de la temperatura al menos en 2 grados centígrados**.
