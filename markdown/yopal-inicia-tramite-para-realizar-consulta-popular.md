Title: Yopal inicia trámite para realizar consulta popular y prohibir actividades petroleras
Date: 2017-06-30 17:18
Category: Ambiente, Nacional
Tags: consulta popular, Yopal
Slug: yopal-inicia-tramite-para-realizar-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/yopal_petroleo1-e1498860896620.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pacific E&P 

###### [30 Jun 2017] 

Por iniciativa de la ciudadanía el municipio de Yopal inicia el proceso para llevar a cabo una consulta para saber si los habitantes avalan la realización de actividades petroleras en su territorio. **Sería la primera vez que una ciudad capital realiza una consulta por hidrocarburos.**

Este jueves ante la Registraduría Municipal, el Comité proconsulta popular de Yopal se inscribió. Dicho comité **espera recoger 20 mil firmas para que la Registraduría** avale el trámite de la consulta popular.

Según Luis Arturo Ramírez Roa, abogado y líder del Comité, esta iniciativa “**tiene por objeto aplicar el principio de precaución y prevención establecidos en el Tratado de Río de Janeiro y de Tokio** frente a la experiencia vivida por los casanareños y, en especial, por los yopaleños, durante más de 30 años de explotación petrolera”.

La pregunta que se le haría a los habitantes de Yopal sería: “¿Está usted de acuerdo ciudadana, ciudadano yopaleno, que en el municipio de Yopal, Casanare, se realicen actividades de sístima, fracking, exploración y explotación de hidrocarburos?”.

### Efectos de la actividad petrolera en Casanare 

Los líderes del comité pro consulta de Yopal buscan “rechazar mediante el mecanismo de consulta popular la forma como se ha entregado la riqueza a las multinacionales petroleras”, pues consideran que los contratos de concesión con las empresas  extranjeras “son totalmente leoninos para la economía del Estado colombiano,  donde la política central del gobierno desconoce lo establecido en los esquemas de ordenamiento territorial y los planes de ordenamiento territorial frente al uso del suelo”.

Para el abogado Arturo Ramírez, el ejecutivo “desconoce el diálogo y la autonomía territorial y la autonomía de las entidades descentralizadas”. **Además recordó la reciente situación vivida en el municipio de Nunchía “por la rotura de los tubos que llevan más de 30 años de servicio** y hoy están llenos de poros y causando daños ambientales irreparables”.

Según el consultor legal ambiental, la Agencia Nacional de Licencias Ambientales ni siquiera tiene una oficina en Yopal para atender de manera inmediata estas situaciones. “Esto hace que  los casanareños hayamos despertado y estemos llevando a cabo y trabajando varias consultas populares.”

Si la registaduría avala las firmas, el siguiente paso será ante el alcalde y el concejo municipal, luego pasará a revisión  constitucional al Tribunal Administrativo de Casanare.

###### Reciba toda la información de Contagio Radio en [[su correo]
