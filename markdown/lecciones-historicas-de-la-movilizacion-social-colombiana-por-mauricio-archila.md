Title: Lecciones históricas de la movilización social colombiana, por Mauricio Archila
Date: 2019-11-13 12:36
Author: CtgAdm
Category: Movilización, Paro Nacional
Tags: Mauricio Archila, Movilización, Movimiento social, Paro Nacional
Slug: lecciones-historicas-de-la-movilizacion-social-colombiana-por-mauricio-archila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/paro-21-de-noviembre.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/paro-21-noviembre-.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/paro-21-noviembre-1-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El próximo 21 de noviembre se realizará el paro nacional contra lo que sectores sociales han denominado el 'paquetazo' del presidente Duque. Trabajadores, profesores, estudiantes y líderes políticos se han unido a la movilización para manifestarse por el cumplimiento de los acuerdos pactados con el Gobierno, la mejora en el sistema pensional y la dignificación del trabajo.

En entrevista con Contagio Radio, **Mauricio Archila, profesor de la Universidad Nacional experto en la historia de los movimientos sociales en Colombia**, habló sobre grandes movilizaciones que se dieron en el siglo XX, el contexto actual que vive el país y las lecciones aprendidas de movilizaciones pasadas. (Le puede interesar:["Duro rechazo de docentes y estudiantes a señalamiento de Uribe contra profesor Archila"](https://archivo.contagioradio.com/48979-2/))

### **Contagio Radio: Se podría decir que el paro con los alcances más grandes en Colombia se dio en 1977, ¿qué tuvo esa movilización en particular en su convocatoria para ser tan relevante?** 

> [El paro cívico del 14 de septiembre de 1977 fue la movilización más grande de esa segunda parte del siglo XX]

**Mauricio Archila:** Para los analistas de la historia social y política del país contemporáneo, el paro cívico del 14 de septiembre de 1977 fue la movilización más grande de esa segunda parte del siglo XX, por supuesto, antes estuvo el Bogotazo y otras manifestaciones por allá en el 28. Pero en aras de la brevedad, sí, ese paro cívico fue muy notorio en ese periodo.

Fue convocado originalmente por las cuatro centrales obreras más algún sector del sindicalismo independiente, se sumaron organizaciones campesinas e indígenas. Varios factores influyeron para que tuviera tanto impacto, por un lado, aunque había habido paros nacionales, y no hay que olvidar la caída de Rojas en mayo del 57 fue un paro nacional, lo de septiembre del 77 se dió por una gran convergencia de distintos sectores. En un contexto bien interesante porque se trataba de un Presidente que en principio tenía un cierto discurso reformista, pues Alfonso López Michelsen venía del MRL (Movimiento Revolucionario Liberal), pero en la práctica el había mostrado que de reformista no tenía mucho.

Nada de reforma agraria, en términos de industrialización habló de Colombia como el Japón latinoamericano, y se habló de salario integral, cosas que están moviéndose también en la coyuntura actual. Además tuvo un manejo arrogante del conflicto: No recibió a las centrales, incluso trató de desconocer y minimizar la movilización. Un contexto cargado tanto de esperanzas reformistas frustradas, como de una economía boyante pero con signos de inequidad y de cierta crisis para los sectores sociales populares que pudo producir la gran movilización.

Yo creo que López no pensó que la movilización fuera tan grande, por eso lo sorprendió. Curiosamente, cuatro años después se intentó hacer otro paro, pero ya no todos los sectores convergieron y **el Estado había aprendido, entonces controló mucho la periferia de Bogotá que fue la que tuvo mucho más agitación**.

Hubo otros intentos de paro cívico pero no tuvieron el éxito de ese momento, y en el 89 y 90 hubo paros laborales, no cívicos propiamente, siempre convocados por las centrales sindicales pero apoyados por la ciudadanía y a veces, del susto de los transportadores, se terminaba favoreciendo el paro porque no sacaban los camiones.

### **CR: ¿Cómo leer el contexto actual con unos sindicatos llamando al paro, con Latinoamérica en medio de movimientos ciudadanos y como usted lo señalaba, con demandas similares a las del 77?** 

> [En Colombia alguna gente dice que aquí no se protesta, lo que no es del todo cierto, lo que pasa es que en Colombia tenemos el problema de la violencia...]

**MA:** Estamos ante un Gobierno que no pretende ser reformista y más bien quiere reversar muchas cosas, en un contexto en que se anuncian reformas pensionales, el Gobierno las retira aparentemente pero sigue el 'run run' de que se va a acabar el régimen de prima media, que sería terrible. Se habla de elementos de una reforma laboral hacia una flexibilización todavía más grande de la fuerza de trabajo, con propuestas tan negativas como la de recortar el salario a los menores de 25 años para que haya más empleo.

Está el tema particular con FECODE, está el tema estudiantil: los estudiantes aducen que si bien el Gobierno ha cumplido algo de lo pactado se ha quedado corto en la cuestión de ciencia y tecnología, y a ello se suma el famoso artículo 44 del Presupuesto General de la Nación para el siguiente año, que obliga a las universidades públicas a asumir los costos de las demandas que se hagan contra el Estado. También los indígenas están denunciando todo el problema de violencia y orden público, el ESMAD está muy cuestionado.

Esos son temas que se han agitado, que producen esta convergencia. El contexto latinoamericano me parece interesante, sobre todo lo de **Chile, es sorprendente porque es el país del aparente triunfo del neoliberalismo y resulta que eso estaba cocinando una profunda inequidad.** (Le puede interesar: ["La unión del pueblo y la persistencia: claves en la movilización de Chile"](https://archivo.contagioradio.com/la-union-del-pueblo-y-la-persistencia-claves-en-la-movilizacion-de-chile/))

Yo creo que en Colombia alguna gente dice que aquí no se protesta, lo que no es del todo cierto, lo que pasa es que en Colombia tenemos el problema de la violencia, no solo porque siguieron grupos paramilitares y brotan disidencias, pero sobre todo porque este Gobierno está empeñado en sacar su agenda de 'hacer trizas' el Acuerdo de Paz. Entonces, tenemos todavía un país donde hay más de 600 líderes sociales asesinados desde 2016, centenares de indígenas, más de 50 excombatientes, y eso para no hablar de la actuación del ESMAD en las protestas estudiantiles; se nota que hay un espíritu guerrerista desde el Gobierno que alienta a las Fuerzas Armadas y de Policía a un retorno a la lógica de guerra fría, anticomunista, con menos respeto a los derechos humanos, menos protocolos, menos inteligencia...

Yo creo que algunos saldremos a marchar, a participar en acciones espero yo que sean pacíficamente, pero pensando en que corremos riesgos. No es lo mismo marchar acá o en otros países de América Latina, la violencia es una espada de Damocles que pende sobre los líderes sociales, y más de uno lo piensa dos veces antes de movilizarse. Con todo y eso, hay un proceso de movilización que a su modo, y dentro de las coordenadas espacio temporales responde a esos reclamos que la población, con justicia, hace.

### **CR: ¿Qué lecciones vale la pena que aprenda el movimiento social de su propia historia en Colombia?** 

**MA**: No es fácil resumir unos rasgos sobre qué hacer,  porque además, no siempre tenemos buena memoria, incluso desde los movimientos sociales, más bien a veces se olvidan ciertas experiencias. Pero, parte de lo que uno trata de mostrar con sus trabajos académicos es que **si se lograra la convergencia de muchos sectores, el impacto podría ser más fuerte y más gente participaría.**

Para ello, yo soy partidario de que **las modalidades de protesta deberían ser lo más incluyentes posibles, lo menos antagónicas posibles**. Yo creo que el tropel no atrae, asusta. El entrar a chocar con el ESMAD o la Policía, cosas de esas, creo que asustan más de lo que convocan. Sin embargo, creo que hay una gran responsabilidad de la fuerza pública, a veces es más provocación de ellos que intención de los participantes de las protestas.

Yo creo que formas más lúdicas, carnavalescas, con performatividad, con actuación, con disfraces... mejor dicho, **abrazatones, mensajes que le lleguen a la ciudadanía incluyentes, son cosas que podrían tener un impacto más positivo** que de pronto entrar a tropelear de frente. Pero insisto, a veces es un desenlace que no se buscaba, pero ocurre.

Qué falta hacen los protocolos que en algún momento se firmaron en Bogotá y en los intentos de unos acuerdos a nivel nacional con defensores de derechos humanos, con integrantes de la Fiscalía, de la veeduría, de la Defensoría e incluso personas del Distrito que hacían mediación del conflicto y podrían dar pautas para un tratamiento menos belicoso y menos criminalizante de la protesta. Producir acuerdos, porque si el Estado en manos del Gobierno anterior logró hacer un acuerdo con su archienemigo que era la insurgencia de las FARC, por qué no hacerlo con gente de la sociedad civil que está desarmada para que puedan protestar pacíficamente.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44296398" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44296398_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
