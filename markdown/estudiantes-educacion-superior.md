Title: Estudiantes de la capital se reencuentran por la educación superior
Date: 2019-02-23 15:33
Author: ContagioRadio
Category: Educación, Nacional
Tags: educacion, estudiantes, marcha, UDEES, UNEES
Slug: estudiantes-educacion-superior
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/IMG_2956-e1543868468169-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Feb 2019] 

Durante 2018 el movimiento estudiantil logró meterse en la agenda del Gobierno Duque y establecer una Mesa de negociación que permitió un aumento de 5,6 billones de pesos para las Instituciones de Educación Superior (IES) públicas durante los próximos 4 años; sin embargo, los jóvenes reconocen que la financiación es solo la primera parte de una larga lucha para transformar el sistema educativo, por lo que iniciarán los encuentros ampliados para establecer los siguientes pasos a seguir en su lucha.

En el caso de Bogotá, el encuentro será durante este fin de semana (23 y 24 de febrero), en la Universidad Pedagógica Nacional**. El Encuentro Distrital de Estudiantes de Educación Superior (EDEES), será el lugar en que el movimiento estudiantil de la capital se organice para llevar propuestas al próximo encuentro amplio** que se realizará en la Universidad del Cauca, durante el mes de marzo. (Le puede interesar: [¿Por qué los estudiantes se seguirán movilizando en 2019?](https://archivo.contagioradio.com/estudiantes-movilizando-2019/))

### **De la financiación a la democracia universitaria** 

Tal como lo reconoce **Valentina Ávila, delegada de la Unión Nacional de Estudiantes de la Educación Superior (UNEES)** ante el Gobierno, las conversaciones del año pasado estuvieron enfocadas en el tema de financiación; y aunque fueron difíciles las concertaciones, **de nada vale inyectar mayores recursos a las IES si los estudiantes no tienen forma de incidir en las decisiones sobre la utilización de esos recursos** y la respectiva verificación de su gasto.

Adicionalmente, los jóvenes tendrán que evaluar el cumplimiento de los acuerdos alcanzados en el marco del Plan de Desarrollo Nacional del gobierno Duque, respecto al que Ávila señaló su inconformidad, en tanto no contiene todo lo pactado a finales de 2018. Durante el Encuentro también tratarán temas como el programa Generación E, las tasas de interés que emplea el ICETEX, las exigencias en materia de derechos humanos para la movilización y sobre su propia organización.

### **Gobierno y estudiantes reanudaron mesa de negociación** 

Durante este viernes, y en cumplimiento de los acuerdos pactados en diciembre de 2018, Gobierno y estudiantes se encontraron este viernes en la primera sesión de la Mesa de diálogo para la construcción de acuerdos para la educación superior; que será el espacio en el que se tramitarán las demandas del movimiento estudiantil, y se hará seguimiento de los puntos acordados. (Le puede interesar: ["Estudiantes vuelven a las calles por el desmonte del ESMAD"](https://archivo.contagioradio.com/desmonte-del-esmad/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
