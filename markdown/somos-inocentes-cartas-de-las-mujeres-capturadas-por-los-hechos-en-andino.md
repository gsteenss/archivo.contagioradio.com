Title: "Somos inocentes" Cartas de las mujeres capturadas por los hechos en Andino
Date: 2017-06-27 17:12
Category: Judicial
Tags: Andino, mujeres
Slug: somos-inocentes-cartas-de-las-mujeres-capturadas-por-los-hechos-en-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/carta-mujeres-andino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 27 Jun 2017 

Contagio Radio conoció algunas de las cartas escritas por tres de las mujeres capturadas durante el fin de semana y vinculadas con la explosión en el Centro Comercial Andino, a través de los escritos ellas reiteran  su inocencia, y manifiestan que estos hechos se deben a una presión mediática y política por dar resultados a la opinión pública. Ver:[ Perfiles de las cuatro mujeres capturadas y sindicadas como responsables por explosión en el Andino](https://archivo.contagioradio.com/mujeres-capturadas-explosion-andino/)

 \
