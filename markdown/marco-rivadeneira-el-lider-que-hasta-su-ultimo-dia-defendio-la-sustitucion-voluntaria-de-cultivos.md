Title: Las amenazas que enfrentan líderes como Marco Rivadeneira en Putumayo
Date: 2020-03-20 19:21
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: asesinato de líderes sociales, Marco Rivadeneira, PNIS, Putumayo
Slug: marco-rivadeneira-el-lider-que-hasta-su-ultimo-dia-defendio-la-sustitucion-voluntaria-de-cultivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Marco-Rivadeneira.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Marco Rivadeneira / @CospaccOficial

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones sociales, defensores de DD.HH. y comunidades campesinas recuerdan a Marco Rivadeneira, líder social asesinado el pasado 19 de marzo quien destacó por su gestión como promotor de la sustitución de cultivos de uso ilícito en Putumayo incluso antes de la firma del Acuerdo de Paz y de forma independiente al Programa Nacional Integral de Sustitución de cultivos de uso ilícito (PNIS). El líder fue a su vez presidente de la Asociación campesina de Puerto Asís, (Asocpuertoasis), de la que era su actual y un pionero en la concertación de diálogos con el Gobierno en dicho tema.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el 2019, Marco hacía parte de la Mesa de Concertación para la Transformación Productiva del Corredor Puerto Vega-Teteyé de Puerto Asís. Su trabajo consistía en visitar las veredas y escuchar las familias para llevar propuestas a la Mesa para acordar con el Gobierno, fue en medio de una de estas reuniones que el líder fue raptado y posteriormente asesinado. [(Le puede interesar: "La sustitución es cambiar una mata por otra, pero el Estado no lo está haciendo: campesinos de Putumayo ")](https://archivo.contagioradio.com/la-sustitucion-es-cambiar-una-mata-por-otra-pero-el-estado-no-lo-esta-haciendo-campesinos-de-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Su muerte es un argumento más para decir que no hay garantías para los defensores de DD.HH. y las comunidades, los territorio están totalmente a la deriva en manos de actores al margen de la ley en medio de una disputa por el territorio"** expresó la defensora de derechos, Yuri Quintero de la Red de DD.HH. del Putumayo quien trabajó junto a Marco y hoy advierte el temor que siente la población después de este asesinato.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Al parecer los dirigentes somos hoy la piedrita en el zapato para la persistencia de los cultivos de coca en zonas de frontera y zonas estratégicas"**. En el departamento de Putumayo conviven los frentes 1, 48, 49 y 62 de las disidencias de FARC que que desde 2018 llegaron desde Caquetá, para disputarse el control social y las rentas del narcotráfico con el grupo de La Constru que se conoce vive del narcotráfico, la explotación de la minería ilegal y la tala ilegal de madera.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Líderes como Marco no solo deben hacer frente a la presencia de grupos armados, también deben enfrentar los daños ambientales y sociales provocados por la operación de empresas como Amerisur Resources Plc y otras compañías extractivas, a menudo protegidas por la Fuerza Pública que defienden los intereses de los industrias petroleras.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A ello se suman la represión que viven las comunidades por parte de fuerzas policiales como el Escuadrón Movil Antidisturbios, que acompaña la erradicación forzada en **un departamento en el que están el 57,79 % del total de los inscritos al PNIS y donde** **según el Monitoreo de territorios afectados por cultivos ilícitos publicado en agosto de 2019 por las Naciones Unidas pasó de 41.382 a 38.170 hectáreas entre 2017 y 2018.** [(Lea también: Por daños ambientales en Putumayo congelan millonaria cifra a petrolera Amerisur)](https://archivo.contagioradio.com/por-danos-ambientales-en-putumayo-congelan-millonaria-cifra-a-petrolera-amerisur/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Marco Rivadeneira dio un realce a la labor del dirigente"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde 2002 jugó un papel muy importante en la defensa del territorio en contra de la expansión petrolera y lo que significa la construcción de Acsomayo y su posterior trabajo como representante legal de Asocpuertoasis, acompañando a las comunidades y familias que no se acogieron al programa de sustitución. [(Lea también: Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo")](https://archivo.contagioradio.com/se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Quienes lo conocieron y compartieron junto a él, como la [Asociación Minga](https://twitter.com/asociacionminga)lo recuerdan como un campesino conversador (...) convencido de que la situación de las comunidades debía mejorar". [(Le recomendamos leer: Marco Rivadeneira, líder campesino y defensor de DD.HH. asesinado en Putumayo)](https://archivo.contagioradio.com/marcos-rivadeneira-lider-campesino-y-defensor-de-dd-hh-asesinado-en-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Marco fortaleció los procesos en el municipio y aportó a la consolidcación de espacios de diálogo con entidades nacionales e incluso de corte internacional. - Yuri Quintero

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

El líder social fue vocero departamental del Proceso Nacional de Garantías desde el 2009 y delegado del Nodo Suroccidente de la plataforma de Derechos Humanos, Coordinación Colombia Europa Estados Unidos. además, fue uno de los pioneros del Congreso de los Pueblos en el Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Marco, de origen ecuatoriano, como relata Yuri permanece en la zona rural del departamento, por lo que l**a misma población se ofreció a llevar a su cuerpo hasta la carretera y transportarlo hasta a la cabecera municipal donde será velado** mientras su familia en Ecuador podría desplazarse hasta Puerto Asís para despedir de manera simbólica al líder social.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
