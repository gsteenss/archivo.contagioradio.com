Title: Desde la firma del Acuerdo de paz, han asesinado 224 firmantes
Date: 2020-08-21 12:25
Author: CtgAdm
Category: Expreso Libertad
Tags: #AcuerdoDePaz, #Asesinatos, #Expresolibertad, #Farc, #Paz
Slug: desde-la-firma-del-acuerdo-de-paz-han-asesinado-224-firmantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/bandera-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Uno de los mayores temores tras la firma del Acuerdo de Paz, era el posible genocidio que se podría repetir en el a historia de Colombia, en contra de las y los firmantes de este proceso. A la fecha, las cifras son alarmantes, **224 personas han sido asesinadas** y un desplazamiento forzado del ETCR de Ituango.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del Expreso Libertad, Camilo Fagua y Valentina Beltrán, integrantes del partido político FARC, señalan el gran riesgo en el que se encuentran las comunidades de reincorporación en las diferentes regiones del país y sus familiares.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_55516765" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_55516765_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

A la fecha, en Colombia han asesinado a 38 firmantes del acuerdo en el departamento del Cauca; 27 en Nariño,26 en Antioquia, 19 en Putumayo, 19 en Meta, 18 en Caqueta y 17 en Norte de Santander. (Le puede interesar: ["¡Que pare la matanza de reincorporados de FARC!"](https://archivo.contagioradio.com/que-pare-la-matanza-de-reincorporados-de-farc/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, además de los **224 asesinatos,** se han registrado 32 tentativas de homicidio, 12 desapariciones forzadas, 24 desplazamientos forzados y 236 amenazas. De acuerdo con Beltrán, Colombia no puede repetir la historia del genocidio de la Unión Patriótica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Razón por la cual, ambos integrantes aseguran que se necesita una **voluntad política urgente por parte del gobierno**. Como también manifiestan que la ciudadanía debe mantenerse firme en la exigencia de la construcción de paz.

<!-- /wp:paragraph -->
