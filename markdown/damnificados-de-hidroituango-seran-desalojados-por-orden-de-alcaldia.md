Title: Damnificados de Hidroituango serán desalojados por orden de Alcaldía
Date: 2019-05-08 18:09
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Empresas Públicas de Medellín, Hidroituango, Ituango, Movimiento Ríos Vivos
Slug: damnificados-de-hidroituango-seran-desalojados-por-orden-de-alcaldia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_7833-e1557267166226.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_7847.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_7900.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_7937.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-93.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

El Movimiento Ríos Vivos - Antioquia rechazó la decisión de la Alcaldía de Ituango de ordenar el desalojo de **18 personas** que fueron desplazadas por el proyecto hidroeléctrico Hidroituango el año pasado y que actualmente residen en el Coliseo Jaidukamá en este municipio de Antioquia.

A pesar de no contar con un ingreso estable para pagar un arriendo, las familias fueron informadas que tienen plazo **hasta el 14 de mayo** para abandonar el albergue temporal. Según la denuncia, la comunidad perdió sus viviendas, cultivos y pertenencias, incluso sus herramientas de trabajo, en las inundaciones del 28 de abril de 2018.

La administración del Alcalde Hernán Álvarez sostiene que para esta fecha las familias debieron haber encontrado trabajo y nueva vivienda, sin embargo la organización ambiental señala que ellos no han recibido apoyo socio-económico para lograr la restauración de sus derechos económicos. [(Le puede interesar: "[Desalojan familias afectadas por Hidroituango de albergues de EPM](https://archivo.contagioradio.com/desalojan-familias-afectadas-hidroituango-albergues-epm/)")]

Tal como lo indica Isabel Cristina Zuleta, integrante de Movimiento Ríos Vivos - Antioquia, un juzgado promiscuo del circuito de Ituango le dio razón a las comunidades en torno a que la administración debía garantizar a esta comunidad programas socio-económicos y vivienda.

Tras conocer la decisión de la Alcaldía de desalojar estas familias, la organización pidió al juzgado a abrir un incidente de desacato. Zuleta también indicó que la Procuraduría General encargó una agencia especial para investigar este caso y que la Defensoría del Pueblo se ha comprometido con visitar el albergue.

Zuleta también denuncia que la Alcaldía no ha cumplido su deber de garantizar la seguridad de los desplazados dado que algunas lideresas sociales que se refugiaron en el albergue tuvieron que huir a otro municipio tras recibir, repetidamente, amenazas de grupos paramilitares.

<iframe id="audio_35570219" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35570219_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
