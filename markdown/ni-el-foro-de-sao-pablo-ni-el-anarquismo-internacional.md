Title: Ni el Foro de São Pablo ni el anarquismo internacional
Date: 2019-11-19 19:06
Author: Foro Opina
Category: Opinion
Slug: ni-el-foro-de-sao-pablo-ni-el-anarquismo-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/protestas-consulado-chileno.jpg_57698320.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [**Foto de: Puntual.com**]**  
** 

El modelo de desarrollo imperante en América Latina y las democracias que nos gobiernan entraron en crisis y comienzan a hacer agua por todos los costados. No ha hecho falta una conspiración concertada por el **Foro de São Paulo** o el **anarquismo internacional** para que la ciudadanía recurra a la acción colectiva en las calles y exprese su descontento frente a decisiones públicas que precarizan su calidad de vida *(como Chile, que había tenido un nivel de vida mejor que el nuestro y que según expertos todos los países del continente debíamos copiar)* e imponen más límites a la democracia –como en el caso de Colombia en donde existen poderes sociales, políticos y económicos anquilosados y bien amarrados a su cuota de dominación que les garantiza su posición de privilegio.

Independiente de si los gobiernos son de centro, derecha o de izquierda, estamos presenciando un periodo de crisis de las democracias y del modelo de desarrollo bajo la égida del capital. El resultado en Colombia es inequidad y exclusión. Una sostenida desigualdad en la distribución de los ingresos y la riqueza que se perpetúa mediante la corrupción o la rampante apropiación privada de los recursos públicos con actividades lícitas o ilícitas (muchas veces van juntas), o en el reino de “todo vale” mediante el despojo, el desplazamiento, la criminalización y el asesinato de personas que lideran sus comunidades y hasta el exterminio de quienes se sitúan en la oposición política, y ni qué decir de las guerras aupadas por los apetitos insaciables de acumulación de riqueza y dominación.

Si a este coctel convulso se le suma el desprecio por la vida –entendida como la matriz de la existencia de todas las especies y seres en la madre tierra–, la intolerancia y el miedo como estrategia para ver enemigos a cada paso y con ello evitar que se fortalezcan lazos de solidaridad, convivencia en el respeto y construcción colectiva de un mundo donde “otros” y “otras” puedan convivir en la diversidad, lo más probable es que quienes han sido olvidados, violentados y excluidos de las promesas del desarrollo quieran interpelar en vivo y en directo a sus gobernantes, porque posiblemente no se sientan incluidos ni reconocidos.

La movilización social en Colombia se ha configurado en los años recientes como una de las expresiones más recurrentes de acción colectiva a la que se apela para hacer público el descontento, denunciar la **vulneración de derechos**, la inconformidad frente a los efectos del modelo económico, hacia decisiones gubernamentales inequitativas o por el incumplimiento de los acuerdos previamente pactados con las autoridades competentes en anteriores protestas. La ciudadanía recurre a la expresión pública y la manifestación colectiva como vía directa para centrar la atención sobre sus intereses y presionar a las autoridades para que sus propuestas y demandas se atiendan. En realidad, así se conquistaron los derechos a lo largo de la historia universal para las mujeres, el mundo laboral, las minorías, el ambiente, la cultura o la propia participación ciudadana, a los que podemos apelar para nuestra protección frente a los abusos de poder y de autoridad.

##### Protestar hoy en el país es válido y urgente. 

Ante un Congreso que le hace conejo a la consulta anticorrupción y un Presidente que prometió en campaña menos impuestos y mejores salarios, las razones de la ciudadanía, las organizaciones sociales, sindicales, de estudiantes y pueblos étnicos encuentran cada día más apoyos hasta de personajes públicos antes impensables para estas causas como la recientemente elegida reina de la belleza colombiana, que por su figuración mediática impactan por la contundencia del reconocimiento del derecho a protestar. Es visible el deseo de expresar la decepción, de hacerse oír, de encontrar solidaridades con quienes no conocen pero que entienden y comparten la justeza de que esos reclamos y demandas sean tenidas en cuenta. El sentir que se hace parte de una colectividad que tiene razones para alzar su voz en las calles es el medio que encuentran más idóneo para que los gobernantes las atiendan, es la forma de decir:

### *aquí estamos y estos son nuestros reclamos.* 

Una parte importante de la sociedad colombiana mantiene vivo el deseo de construir la paz, de reparar a las víctimas del conflicto armado, de conocer la verdad, las responsabilidades institucionales y personales, de cumplirle a los campesinos que le apostaron a la sustitución voluntaria y a los excombatientes que se comprometieron con la paz, para sanarnos como nación. Es inaplazable la adopción de las medidas pactadas en el Acuerdo de Paz que protegen y promueven la participación política y ciudadana, y actuar con contundencia para detener y judicializar los responsables de los asesinatos de líderes sociales e indígenas, defensores de derechos humanos y excombatientes de las **FARC**.

#### Esto es solo posible si comenzamos por cambiar el estado actual de cosas. 

En el paro nacional del 21 de noviembre los cambios están vinculados a los reclamos que allí se expresarán: las finanzas públicas no pueden sanearse a partir de las pensiones de los trabajadores de ingresos medios y bajos, del salario de los jóvenes ni precarizando más el ingreso de la población del campo; del aumento a las exenciones de impuestos al gran capital nacional e internacional y el incremento del IVA a los consumidores. Tampoco endosando a la población los costos de la corrupción en la que se ven implicados grandes emporios económicos, y menos ignorando lo acordado previamente con los educadores, trabajadores y estudiantes, los campesinos y los grupos étnicos en anteriores paros y movilizaciones.

 

(le puede interesar[:Garantizar la protesta social es función de un Estado democrático)](https://archivo.contagioradio.com/garantizar-la-protesta-social-es-funcion-de-un-estado-democratico/)

 

El 21 de noviembre se ha convocado una **protesta pacífica** que debe ser respetada y protegida por las autoridades y la fuerza pública. Protegerla de la intromisión de actores externos que incurren en actos de violencia para impedir que se escuchen las demandas y reclamos ciudadanos. Que se aíslen esos focos de violencia, pero sin disolver ni amedrentar al resto de manifestantes. Porque es deber de las autoridades además de proteger a quienes no participan en la protesta o a las edificaciones y mobiliario públicos, proteger y garantizar el derecho de quienes protestan.

 

**¡Se Vale Protestar!**

 
