Title: Crítica situación de derechos humanos en el Naya
Date: 2018-05-07 13:05
Category: DDHH, Movilización
Tags: afronayeros, amenazas a líderes sociales, desparición de afronayeros, grupos armados, Naya
Slug: critica-situacion-de-derechos-humanos-en-el-naya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/naya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Intereclesial de Justicia y Paz] 

###### [07 May 2018] 

El pasado sábado 5 de Mayo en el Río Naya, Valle del Cauca, el lider social de las comunidades del Naya, Iver Angulo Zamora, fue **retenido por hombres armados** a pesar de que se movilizaba protegido por una misión humanitaria de la que hacían parte la Defensoría del Pueblo y algunos integrantes de la comunidad. Los armados interceptaron la embarcación y se llevaron a Iver sin que hasta el momento se conozca algo de su paradero.

De acuerdo con la Comisión de Justicia y Paz, Iver estaba realizando labores de búsqueda de **Obdulio Angulo Zamora de 33 años, Hermes Angulo Zamora de 28 años y Simeón Ovale Angulo de 32 años,** desaparecidos desde el pasado 17 de abril en las mismas comunidades. Debido a ese trabajo había recibido amenazas y por ello se había pedido una misión para poner a salvo al afrodescendiente.

### **Grupos armados han desplazado a 15 familias** 

Cabe recordar que la comunidad de Juan Santo, donde habita el líder social, se encuentra en situación de desplazamiento. Desde abril, **50 personas han sido afectadas**, debido a la incursión de estructuras armadas desde el pasado 2 mayo. Ante esto, el Consejo Comunitario del Naya pidió a la Defensoría del Pueblo atender esta situación.

Ese 2 de mayo, la organización denunció que 15 hombres armados, al mando de alias “Chumbi”, “realizaron disparos, **saquearon viviendas** y preguntaron por el líder Iver Angulo Zamora, quien logró escapar”. Sin embargo, tres días después fue secuestrado y aún continúan las operaciones armadas criminales en el río Naya “con una evidente ineficacia de las Fuerzas Militares”. (Le puede interesar:["Todas reunidas no vamos a olvidar, vamos a reconciliarnos": Mujeres del Río Naya"](https://archivo.contagioradio.com/todas-reunidas-no-vamos-a-olvidar-vamos-a-reconciliarnos-mujeres-del-rio-naya/))

### **Tres afronayeros siguen desaparecidos desde el 17 de abril** 

A la crisis humanitaria se suma la desaparición de tres afronayeros desde el 17 de abril que son familiares del líder secuestrado. Ellos fueron vistos por última vez cuando se encontraban cruzando el corregimiento de Puerto Merizalde en cercanía a la desembocadura del río Naya y de los cuales aún se desconoce su paradero.

Adicionalmente, el 22 de abril fue **amenazada la esposa** de uno de los afronayeros desaparecidos, Nayibe Valencia, quien también habita en el territorio del Consejo Comunitario del Bajo Naya. La comunidad denunció que le hicieron una llamada al teléfono del servicio rural, instalado en la comunidad de La Bertola, donde un hombre le indicó que dejara de preguntar los por los hombres desaparecidos.

### **Control el tráfico de droga en el territorio** 

La Comisión Intereclesial de Justicia y Paz ha denunciado en diferentes oportunidades que las estructuras armadas ilegales “controlan el tráfico de droga por el río y permanecen en la parte baja sobre las bocanas de los ríos Naya, Yurumanguí y Micay”. Afirman que “se desconocen las acciones efectivas que realiza la **Fuerza Naval** de operaciones de control perimetral”como lo habían expresado en una reunión de Medidas Cautelares en Bogotá.

Con esta situación alertaron el riesgo en el que se encuentra la población en el Naya e indicaron que sólo ha habido respuesta por parte de la Defensoría del Pueblo y las medidas cautelares, otorgadas por la Comisión Interamericana de Derechos Humanos **“continúan siendo desconocidas”.** Por el momento se desconoce la existencia de un plan de búsqueda de las tres personas desaparecidas.

###### Reciba toda la información de Contagio Radio en [[su correo]
