Title: Tras una semana en cautiverio, fue liberada Andrea Ardila, lideresa de Saravena, Arauca
Date: 2019-11-07 16:57
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Arauca, Desaparición de líderes sociales, Saravena
Slug: andrea-ardila-lideresa-de-saravena-arauca-completa-una-semana-desaparecida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Saravena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corpoclaretiana] 

Desde el pasado 1 de noviembre, la Corporación Claretiana alertó sobre la desaparición de la defensora de derechos humanos, Andrea Katherine Ardila en Saravena, Arauca, integrante de la **Asociación Nacional de Usuarios Campesinos (ANUC),** pese al tiempo transcurrido, no se ha recibido ningún tipo de información de su paradero.

El 1 de noviembre cerca de las 10:30 de la mañana, 5 hombres encapuchados y fuertemente armados irrumpieron en la casa de la madre de Andrea en el barrio Universitario de este municipio, golpeando a la defensora de derechos y subiéndola a una camioneta Terios gris que se marchó con rumbo desconocido escoltada por una motocicleta Suzuki de cilindraje 125 color azul. [(Lea también: Líder social del Catatumbo, Celiar Martínez completa cinco días desaparecido)](https://archivo.contagioradio.com/lider-social-del-catatumbo-celiar-martinez-completa-cinco-dias-desaparecido/)

Andrea hace parte de la **Asociación Nacional de Usuarios Campesinos de Arauca, pertenece a la Asociación Amanecer de Mujeres por Arauca (AMAR)** y a la junta de acción comunal de este municipio.  [(Lea también: Diálogo por la verdad de líderes sociales asesinados llega a Arauca)](https://archivo.contagioradio.com/dialogo-por-la-verdad-de-lideres-sociales-asesinados-llega-a-arauca/)

Hernando Ardila, hermano de Andrea,  señala que aunque él sí fue víctima de intimidaciones hace cerca de tres años, por lo que se vio obligado a salir del municipio, su hermana nunca manifestó haber recibido amenazas. A su vez, resaltó que únicamente han sido las organizaciones de derechos humanos las que han acompañado la búsqueda.  [(Le puede interesar: Asesinan al gestor cultural y audiovisual, Mauricio Lezama en Arauca)](https://archivo.contagioradio.com/asesinan-al-gestor-cultural-y-audiovisual-mauricio-lezama-en-arauca/)

### **Actualización**

En horas de la noche de este 7 de noviembre se anunció que la lideresa regresó a su hogar y se encuentra actualmente a salvo con su familia, la noticia la dio a conocer Adriana Vera, madre de Andrea. La libración se habría dado en inmediaciones a Caño Negro, zona rural del municipio de Saravena, sin embargo  aún no se conocen los autores del secuestro y las razones por las que habrían retenido a la defensora de derechos.  Al respecto la familia ha preferido no dar declaraciones al respecto.

### Contexto de la violencia en Arauca 

Organizaciones sociales vienen alertando desde principios de 2019 que los asesinatos selectivos y las amenazas registradas se han agudizado en el pie de monte araucano y en las zonas rurales donde históricamente existe presencia del ELN y de hombres que pertenecerían a las disidencias del décimo frente de las FARC.

Según la Fundación Paz y Reconciliación,  ELN y "grupos postfarc" han llegado a un pacto de cooperación que se  refleja en que no se han registrado enfrentamientos entre ambas agrupaciones, sin embargo sí se ha conocido de la presencia de grupos paramilitares y hombres encapuchados sin distintivos **que estarían intimidando a la población y amenazando a líderes y lideresas sociales a través de panfletos y chats para que abandonen el territorio.**

Desde el levantamiento de la mesa de negociaciones entre el ELN y el Gobierno se ha registrado un aumento en los ataques contra la Fuerza Pública, con un total de 84 actos de violencias entre 2018 y mayo de 2019, mientras que en 2019 se han registrado 131 homicidios, una cifra que sigue en aumento con relación a los 160 notificados en 2018 y los 88 con los que cerró el 2017, revelando una difícil situación humanitaria que una vez más resalta la importancia de retomar una salida negociada al conflicto con el grupo guerrillero , con fuerte presencia en el departamento.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
