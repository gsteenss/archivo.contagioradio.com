Title: 1.000 km por la educación: Llegan los Caminantes de la Sierra a Bogotá
Date: 2019-01-23 15:42
Author: AdminContagio
Category: Educación, Movilización
Tags: Caminantes, estudiantes, Sierra, UNEES
Slug: caminantes-de-la-sierra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-23-a-las-2.43.57-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @crossiuris] 

###### [23 Ene 2019] 

Luego de caminar más de mil kilometros a lo largo de 48 días, llegan los Caminantes de la Sierra a Bogotá. Este grupo de estudiantes que rememora la Marcha del Hambre y se autodescribe como la braza que aviva las ganas de seguir luchando por una educación superior pública, digna y gratuita, **serán recibidos frente al ICETEX**, en el centro de la capital, y posteriormente desarrollarán un performance en la Plaza de Bolivar.

Los jóvenes **salieron de Santa Marta el pasado 6 de diciembre,** durante la conmemoración de la masacre de las bananeras, así como rindiendo un homenaje a los maestros que participaron en la **Marcha del Hambre**, una movilización que tuvo lugar por parte del magisterio el 24 de diciembre de 1966 para reclamar al Gobierno de Carlos Lleras Restrepo mejores condiciones laborales y de salario.

En esa ocasión, dadas las condiciones de las vías, los profesores debieron recorrer más de 1.600 km desde Santa Marta hasta la Capital y aunque las vías cambiaron y la presidencia está en otras manos, los Caminantes de la Sierra afirman que reivindican su lucha, y hoy completan 1.198 km recorridos por la educación superior. (Le puede interesar:["Activistas por la paz se dan cita este jueves para hablar sobre negociación con ELN"](https://archivo.contagioradio.com/activistas-paz-jueves/))

### **"Somos la braza que aviva el deseo de seguir luchando"** 

A final de 2018 diferentes grupos de estudiantes hicieron recorridos de hasta 600 km para apoyar el paro nacional estudiantil, y los Caminantes de la Sierra, motivados por estos grupos y buscando animar el movimiento estudiantil que se sabía, podía perder fuerza durante la temporada decembrina, optaron por alejarse de sus familias durante las fiestas para estar cerca de sus compañeros.

A la situación propia de la época se sumó la firma del acuerdo alcanzado entre estudiantes y Gobierno para inyectar más recursos a la base presupuestal de las Instituciones de Educación Superior Públicas, circunstancia con la que algunos creyeron resolver la mayoría de las reivindicaciones estudiantiles.

Por estas razones, los Caminantes quieren ser la braza que avive el deseo de seguir luchando por una educación digna y gratuita, que necesita más recursos, pero que también requiere **autonomía universitaria, reforma al ICETEX y garantías para el legítimo derecho a la protesta según han manifestado los líderes estudiantiles**.

(Le puede interesar:["Estudiantes vuelven a las calles por el desmonte del ESMAD"](https://archivo.contagioradio.com/desmonte-del-esmad/))

> ¡[\#BOGOTÁ](https://twitter.com/hashtag/BOGOT%C3%81?src=hash&ref_src=twsrc%5Etfw)! Acompaña hoy a los Caminantes de la Sierra en esta actividad. En conmemoración a nuestros líderes sociales y policías de la Escuela General Santander. [pic.twitter.com/Hc6pzJytBD](https://t.co/Hc6pzJytBD)
>
> — UNEES Magdalena (@UNEESMagdalena) [23 de enero de 2019](https://twitter.com/UNEESMagdalena/status/1088109536731377665?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_31725356" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31725356_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
