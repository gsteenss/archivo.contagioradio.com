Title: Denuncian plan criminal contra acompañantes de reclamantes de tierras en Urabá
Date: 2018-07-11 10:54
Category: DDHH, Nacional
Tags: lideres sociales, Reclamantes de tierra
Slug: denuncia-acompanantes-reclamantes-uraba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/tierras-curvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 11 Jul 2018 

Integrantes de la Fundación **Forjando Futuros**, el **Instituto Popular de Capacitación **IPC y la **Asociación de Reclamantes Tierra y Paz**, denunciaron un presunto plan criminal para atentar contra la vida de tres de sus directivos, destacados por su trabajo de acompañamiento y apoyo a reclamantes de tierras en el Urabá.

Según lo expuesto en un comunicado emitido el 7 de julio, reconocidos empresarios de los sectores ganadero, palmicultor y bananero de la zona, estarían fraguando un plan criminal en contra de **Gerardo Vega, director de Forjando Futuros; Carlos Yamil Páez, Fiscal de Tierra y Paz; y contra uno de los coordinadores del equipo de trabajo de restitución de tierras del IPC** que prefirió preservar su identidad.

En la denuncia, se vincula con nombre propio a los empresarios **Ángel Adriano Palacios Pino, José Arley Muñoz, Luis Fabio Moreno Ruiz Y Jaime Antonio Uribe Castrillón**, quienes según informaciones obtenidas por las organizaciones afectadas, habrían celebrado reuniones en una Finca ubicada en el kilómetro 40 por la vía Panamericana frente a la Finca el Trébol en el Municipio de Turbo, Antioquia.

La comunicación advierte que tales encuentros, tendrían como fin **establecer planes con el propósito de detener los procesos judiciales de restitución de tierras** que cursan contra ellos como despojadores. (Le puede interesar: [Asesinan a James Jiménez, líder de proceso de restitucion de tierras en Turbo](https://archivo.contagioradio.com/asesinan-a-james-jimenez-lider-de-proceso-de-restitucion-de-tierras-en-turbo/))

El 9 de julio, las organizaciones **radicaron la denuncia ante la Fiscalía General, seccional Antioquia**, y mientras su trámite tiene su curso, extienden un llamado al Estado colombiano para que acelere la investigación y garantice la protección de la vida de los dirigentes, y a la Comunidad Internacional para acompañar el proceso.

#### **Unidad de restitución de Tierras anuncia "barrido de solicitudes" en Uraba** 

La Unidad de Restitución de Tierras, Dirección Territorial Apartadó, anunció que **realizará el próximo 31 de julio un barrido total de las solicitudes** en Apartadó, Arboletes, Carepa, Chigorodó, Mutatá, Necoclí, San Pedro de Urabá y Turbo, considerando que, según sus cifras, ha sido alcanzado el 85% de atención a las reclamaciones relacionadas con microzonas en esos municipios.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
