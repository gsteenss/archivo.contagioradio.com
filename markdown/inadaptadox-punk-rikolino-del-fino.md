Title: Inadaptadox "Punk rikolino del fino"
Date: 2015-09-12 16:20
Category: Sonidos Urbanos
Tags: Inadaptadox, Música urbana Bogotá, Punk
Slug: inadaptadox-punk-rikolino-del-fino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/inadaptadox-e1442006469691.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_8323587_2_1.html?data=mZiflZqce46ZmKiakpaJd6Kkk4qgo5mccYarpJKfj4qbh46kjoqkpZKUcYarpJK20MbIpdHowsnc2pCJdpPE1tPYjdfNr9DgytPcjcnJsIzaytPch5eWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Inadaptadox, en "A la Calle"] 

Con un poco más de 1 año de haber iniciado a tocar juntos "Inadaptadox" agrupación bogotana de Punk, suben a la tarima, con energía, estilo y la inquietud propia de los jóvenes, elevando un grito de denuncia ante las desigualdades sociales que viven en su cotidianidad.

Sus inicios se remontan a mediados del 2014, encontrandosé por el interés común en los sonidos de bandas como The Misfits, Eskorbuto, Chite, Rxaxzxa, Sex pistols, Ramones, Ira y The Rabble.

\[caption id="attachment\_13885" align="aligncenter" width="960"\][![inadaptadox](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/inadaptadox1.jpg){.wp-image-13885 .size-full width="960" height="540"}](https://archivo.contagioradio.com/?attachment_id=13885) Inadaptadox en Contagio Radio\[/caption\]  
   
Inadaptadox son:  
\*Fabian Beltran - Guitarrista  
\*Jefferson Pajoy - Bajista  
\*Anderson Gonzalez - Baterista (Ampollas locas)  
\*Braulio Zorro - Vocal  
[Facebook.com/xNDRxSpunk   ](https://www.facebook.com/xNDRxSpunk/timeline)
