Title: Las razones de la Minga indígena en cifras
Date: 2017-10-31 16:56
Category: Movilización, Nacional
Tags: acuerdo de paz, asesinatos de lideres, Minga Indígena, paro
Slug: las-razones-de-la-minga-indigena-en-cifras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/minga-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Censat Agua Viva] 

###### [31 Oct 2017] 

El pliego de peticiones de la Minga que adelantan las comunidades indígenas desde este lunes, evidencia en cifras la grave crisis humanitaria, económica y política que atraviesan las diferentes etnias. De acuerdo con el pliego de peticiones, desde 1996 el gobierno ha suscrito **un total de 1392 compromisos**, de los cuales **el 63% corresponden a actos legislativos, administrativos y políticas públicas que tienen un promedio de 3% de cumplimiento,** así mismo el 37% restante corresponde a actas de sesiones que no superan un cumplimiento del 30%, mientras  que los acuerdos cumplidos son en su mayoría aquellos de gestión que no implican cambios sustanciales y estructurales.

### **Violencia contra líderes sociales** 

Según datos del movimiento social Cumbre Agraria Campesina y Popular desde el 1 de enero hasta el 1 octubre del año 2017 habían sido asesinados 1073 líderes sociales. Respecto a la población indígena, entre 1 noviembre 2016 al 31 de julio 2017, se reportaron 30 líderes y lideresas asesinadas, 6 desaparecidos, 115 amenazados, 3490 personas de Pueblos Indígenas en situación de desplazamiento, 9 con reclutamiento forzado que sumado a otros hechos representan **en total 4.508 casos de violaciones a los DDHH e infracciones al DIH que se han presentado en territorio indígena.**

Los departamentos con mayor afectación y con mayores riesgos de afectaciones se encuentran: Cauca, Tolima, Antioquia, Cesar, Chocó, Nariño, Quindío, Valle del Cauca, Meta, Guajira, Risaralda y Amazonas; y los pueblos más afectados son son: Wounaan, Eperara Siapidara, Nasa, Pijao, Embera Chamí, Awá, Embera Katío, Zenú, Jiw, Wayuú, Wiwa y Makuna.

Solo en octubre la situación se evidenció aún más crítica. En la masacre de Tumaco fueron asesinados dos indígenas del Pueblo Awá y hubo más de cincuenta heridos. Además por cuenta de la acción de ESMAD murió Efigenia Vásquez Astudillo, la líder indígena del pueblo Kokocuno; también se presentó el asesinato de dos indígenas el Choco del Pueblo Emberá Dovida en Chocó, dos indígenas Sikuanis en Meta y Vichada, dos líderes del Pueblo Nasa en el Cauca, y una Líder Pijao, para **un total de 10 muertes de indígenas solo en el último mes.**

### **Pueblos en riesgo de extinción** 

De acuerdo con la ONIC, en Colombia existen 102 Pueblos Indígenas, de los cuales, el 62,7% se encuentra en riesgo de extinción física y cultural. De ellos, la Corte Constitucional identificó 35 pueblos en riesgo por causa del conflicto armado. Además, en el transcurso del 2014 a 2016 en el marco del Proceso de Negociación de paz entre el Gobierno Nacional y las FARC –EP,  **fueron asesinados 58 indígenas, y ocurrieron 11.644 violaciones a los derechos humanos** e infracciones al DIH contra estos pueblos.

### **Mujeres indígenas** 

De acuerdo con el pliego, a la fecha **hay un incumplimiento del 86% de los acuerdos en materia de los de derechos de las mujeres indígenas**  en el PND 2014-2018. Es decir, que de los 51 acuerdos concertados, el nivel de cumplimiento al respecto llega apenas a un 14% y al igual que en otras políticas, las acciones que permite esa cifra, son de gestión y protocolización de los documentos propuestos por nuestras mujeres indígenas. Asimismo, las mujeres indígenas no cuentan con el Programa de Protección de los Derechos de las Mujeres Indígenas Desplazadas y en Riesgo de Desplazamiento, cuya creación la ordenó por la Corte Constitucional.

### **Incumplimiento del acuerdo de paz** 

Bajo el Gobierno Nacional quedó la definición de **las 86 normas necesarias para implementar el Acuerdo Final,** teniendo en cuenta la Consulta Previa; sin embargo, el Gobierno dijo que solo radicarían 46 normas, incumpliendo lo pactado. Además, el gobierno emitió más de 30 decretos con fuerza de ley sin consulta previa alguna, limitando la participación de los Pueblos Indígenas a vía intervención ciudadana ante La Corte Constitucional.

### **Derechos territoriales** 

Las exigencias de los indígenas señalan que se ha identificado 678 solicitudes de constitución, ampliación y saneamiento de resguardos, sin embargo no se evidencian la voluntad política del gobierno nacional de brindar la seguridad jurídica a los territorios colectivos, pues el 77% de los casos se encuentran a nivel de solicitud, 14% en trámite y el 9% detenidos.

Por otra parte, hay 148 territorios indígenas susceptibles a procesos de restitución de tierras por despojo en el marco del conflicto armado, de los cuales en 6 años del decreto ley 4633 de 2011 solo se han logrado 5 sentencias de restitución de derechos territoriales. A esto se suma que de **51 solicitudes presentadas por la declaración de territorios ancestrales, no se ha avanzado en ninguna,** ya que no ha existido una adecuación institucional, un presupuesto asignado, ni un sistema de información que posibilite su cumplimiento.

### **Derechos ambientales** 

De los 63,2 millones de hectáreas de bosque natural del país, el 57,3% se ubica en territorios de grupos étnicos, indígenas y Afrodescendientes. En territorios indígenas, la proporción de bosque natural es de 93,0% y el 5.3% tiene uso agropecuario.  Sin embargo por acción y omisión del gobierno, dichas áreas están siendo afectadas por la deforestación y degradación a causa de las economías legales e ilegales como cultivos de uso ilícito, industrias extractivas y agroindustriales, la ganadería extensiva, la extracción de madera, los monocultivos y obras de infraestructuras.

Para la minería, a la fecha **existen 396 títulos mineros vigentes en territorios indígenas y 927 solicitudes,** de las cuales la Agencia Nacional de Minería no reporta cuáles fueron consultados. En términos de áreas, se ha titulado 28.410.812 hectáreas a empresas mineras en zonas de resguardos indígenas. Por su parte, 27 resguardos se hallaban titulados en más del 50% de su área y 14 de ellos han sido titulados completamente.

En el tema de Hidrocarburos, a la fecha, la Agencia Nacional de Hidrocarburos, **reporta 419 áreas disponibles para hidrocarburos y 82 áreas en reserva para hidrocarburos,** que traslapan con territorios indígenas. En cuanto proyectos de infraestructura: a la fecha el Ministerio del Interior informa que se han consultado 101 obras de infraestructura, pero no se tiene información de cuantas obras se tienen proyectadas, los riesgo o potencialidades y en qué estado están el cumplimiento de los acuerdos consultivos con las comunidades indígenas.

<iframe id="doc_34617" class="scribd_iframe_embed" title="Pliego de Exigencias Minga Nacional Por La Vida 2017" src="https://www.scribd.com/embeds/363141719/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-2a0VM2jYukAdW5Jw8rKx&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
