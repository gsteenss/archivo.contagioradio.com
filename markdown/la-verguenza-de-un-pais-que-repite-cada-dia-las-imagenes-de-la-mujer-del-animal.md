Title: La vergüenza de un país que repite cada día las imágenes de La Mujer del Animal
Date: 2017-06-14 06:00
Category: Opinion
Tags: Activismo feminista, feminismo, mujeres, Violencia contra las mujeres
Slug: la-verguenza-de-un-pais-que-repite-cada-dia-las-imagenes-de-la-mujer-del-animal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/La-Mujer-del-Animal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto Tito Alexander Gómez/La mujer del animal 

#### [**Por Adriana Benjumea/[[@**AdrianaBenjume1**](https://twitter.com/AdrianaBenjume1) 

[Las escenas del director Víctor Gaviria que me tocan y conmueven hoy, quizás porque sus historias me evocan una Medellín vivida y padecida desde Rodrigo D No Futuro, Sumas y Restas y la Vendedora de Rosas, invaden mis sentidos con las imágenes de calles polvorientas, los rostros de la pobreza, los ranchos de tabla y las violencias cotidianas en La Mujer del Animal.]

[Mientras veo el]*[trailer]*[, un bus de  Santa Cruz, un rancho de tabla, un colchón viejo, la rabia y la aspereza de una violencia insoportable como la describe Gaviria, escucho a Natalia Polo, su protagonista, una actriz natural que narra lo insufrible que era leer el guión pero no más que escuchar la historia de Margarita, la mujer que había vivido “en carne propia” la historia que ella encarna como actriz protagonista, una vida de violencias sin treguas, como dice Víctor también, una historia sin redención, con odios desde el principio.  ]

[Las violencias en contra de las mujeres son una violación a los derechos humanos de graves dimensiones, que obstaculizan y perpetúan las exclusiones históricas padecidas por ellas. Así lo ha reconocido la Convención Interamericana para Prevenir Sancionar y Erradicar la Violencia contra las Mujeres -Convención de Belem do Pará-, al señalar en su preámbulo que “la violencia contra la mujer constituye una violación a los derechos humanos y las libertades fundamentales y limita total o parcialmente el reconocimiento, goce y ejercicio de tales derechos y libertades”(1).]

[Reconocimiento compartido por la Convención sobre la Eliminación de todas las formas de Violencia contra la Mujer CEDAW (por sus siglas en Inglés), la cual exige a los Estados parte que adopten las medidas adecuadas para eliminar la discriminación en contra de la mujer en todos los ámbitos (ONU,1979, art 2), entendiendo la violencia contra las mujeres como la máxima expresión de la discriminación y la desigualdad de las relaciones de poder entre hombres y mujeres y la fehaciente violación a los derechos humanos que ello representa (Comité Cedaw,1992, núm. 6 y7).]

[Ambos instrumentos han sido ratificados por el Estado Colombiano y de ellos se derivan mandatos de obligatorio cumplimiento. Sin embargo, estas obligaciones legales internacionales no han sido suficientes para que el país prevenga, investigue y sancione de forma eficiente la violencia contra las mujeres y las niñas.   ]

[El Instituto Nacional de Medicina Legal y Ciencias Forenses - INMLCF- registró 731 casos de homicidios de mujeres perpetrados por su expareja o pareja, siendo estos los principales agresores identificados en los casos de homicidio. En promedio esto significó 122 mujeres asesinadas por año, 10 por mes y 1 cada 3 días. (2) ]

[Entre 2004 y 2014, el INMLCF registró 181.093 mujeres víctimas de violencia sexual. En promedio, 16.463 mujeres por año, 46 por día, 2 por hora y una cada 32 minutos. Las niñas entre los 10 y los 14 años fueron las más afectadas por esta violencia.(3)  ]

[Amparo, nombre que recoge a Margarita, Sonia, Patricia, Gloria y muchas otras en distintos rincones de Colombia, no solo en los barrios empobrecidos de las grandes ciudades, sino mujeres en estratos socioeconómicos medios y altos, es víctima de múltiples formas de violencia mientras hay silencio y complicidad de una familia y una sociedad que, ante la desprotección del Estado, permite y calla con indolencia para proteger su propia vida.  ]

[Colombia es un país que se caracteriza por la amplia producción normativa pero existe una brecha entre lo que establece la Ley y su aplicación efectiva y materialización de la norma. Situación que se ve agravada en el caso de la violencia contra las mujeres. Aunque, formalmente se establecen garantías para el acceso a la justicia de las mujeres víctimas de violencia, en la práctica, estas se enfrentan a múltiples obstáculos relacionados con la consideración de estos delitos como de menor importancia, su naturalización y tolerancia social.]

[Los obstáculos para las mujeres y las niñas comienzan incluso antes de iniciar el recorrido de la denuncia; y, cuando esta puede darse, las mujeres se ven enfrentadas a funcionarios públicos, hombres y mujeres, que no han comprendido las barreras de acceso a las rutas de atención, entre ellas, la falta de autonomía económica, la desconfianza en la institucionalidad, los altos niveles de impunidad. La situación se agrava con los riesgos adicionales que asumen las mujeres al denunciar, como la posibilidad de sufrir revictimizaciones por parte de funcionarios y funcionarias públicas, así como las represalias que puedan tomar los agresores.]

[La Ley 1257 de 2008  y sus decretos reglamentarios no han sido suficientes para que Colombia avance de forma contundente en la paz que permita a las mujeres vivir una vida libre de violencias. Ante la ineficiencia de un Estado incapaz de dar respuesta a las mujeres suceden las violaciones y asesinatos de Rosa Elvira Cely, Yuliana Andrea Samboní, Claudia Johana Rodríguez, Gladys Janeth Quintero Jaramillo y muchas otras.]

[Múltiples Libardos, como el otro protagonista de la película en mención, someten a diario a mujeres y niñas al miedo y la violencia ante la complicidad de un Estado indolente, incapaz, que utiliza la democracia para decidir sobre nuestros cuerpos, pero no para garantizar una vida libre de violencias.]

[¿Cómo pueden dormir tranquilos ante tanta impunidad funcionarios y funcionarias públicas con responsabilidades en la prevención, investigación y sanción de la violencia contra las mujeres y las niñas mientras se tiene la certeza que cada noche en algún rincón del país se repite sin remedio alguna escena de La Mujer del Animal?.]

------------------------------------------------------------------------

###### (1) Convención Interamericana para Prevenir, Sancionar y Erradicar la violencia contra la Mujer- Convención de Belem do Pará. OEA, (1994). 

###### (2) [Plataforma Cinco Claves. Cartilla “Las mujeres en los Acuerdos de Paz”, 2016, p. 7 Disponible en: http://www.humanas.org.co/alfa/dat\_particular/ar/Las\_Mujeres\_en\_los\_Acuerdos.pdf.] 

###### (3) [ídem, p. 7] 

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
