Title: "Mujeres cocaleras son trabajadoras, no narcotráficantes"
Date: 2017-04-25 12:38
Category: Mujer, Nacional
Tags: acuerdos de paz, coca, hoja de coca, mujeres cocaleras
Slug: las-campesinas-cocaleras-son-mujeres-trabajadoras-no-narcotraficantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/campesinas-cocaleras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Plural  
] 

###### [25 Abr. 2017] 

La Corporación Humanas que trabaja con mujeres cocaleras, rechazó la intervención del Fiscal **Néstor Humberto Martínez en la Plenaria del Senado este 18 de Abril, en la que calificó como inconveniente el punto 6.1.9 del Acuerdo Final** de Paz, en el que se refiere al tratamiento diferenciado por los delitos relacionados con cultivos de uso ilicito, cuando los procesados sean campesinos no pertenecientes a organizaciones criminales.

Para el Fiscal, sería adecuado no tener un tratamiento diferenciado para las mujeres, porque, según él, este podría redundar en que **las mujeres encarceladas por esta práctica sean usadas "como carne de cañón por la industria de las drogas",** sin embargo para la Corporación Humanas el tratamiento penal diferenciado si es necesario y recuerda que dicho tratamiento hace parte de los acuerdos.

**Adriana Benjumea, directora de la Corporación Humanas** aseveró que se ha evidenciado que las mujeres realizan este tipo de prácticas porque son zonas que están desprotegidas del Estado.

"No se les ha garantizado la salud, la educación y la única posibilidad para muchas de estas mujeres en extrema pobreza para poder sacar a sus hijos e hijas adelante, ha sido la relación con el cultivo y allí han tenido relación con todo lo que es la producción de la coca, porque hay una ancestralidad pero también está la elaboración de la pasta, la cocaína" dijo Benjumea.

Las mujeres cocaleras exigen que se cumpla lo pactado en el Acuerdo de Paz en relación con la persecución penal "**lo que se está viendo es que hay un incumplimiento, que no se ha escuchado a las mujeres cocaleras.** Ellas no quieren que ningún acuerdo avance sin ellas. Por supuesto que quieren la sustitución, pero debe venir con garantías" relata Benjumea. Le puede interesar: [Familias Cocaleras ya tienen Plan de Sustitución de Cultivos](https://archivo.contagioradio.com/familias-cocaleras-ya-tienen-plan-de-sustitucion-de-cultivos/)

Dice la directora de la Corporación Humanas que lo que vienen denunciando desde hace mucho tiempo es que **las mujeres cocaleras no son narcotráficantes** "las mujeres cocaleras están dispuestas a viajar a Bogotá para reunirse con la comisión de implementación de los acuerdos, para hablar incluso con el Fiscal si se honra a recibirlas, porque **hay que dejar muy claro que ha habido un enfoque desde la Fiscalía General de la Nación desde el que se está criminalizando a las mujeres cocaleras**" aseveró.

Benjumea es enfática en manifestar que las mujeres no quieren una acuerdo en el que no estén presentes ellas "el enfoque de género pactado en los Acuerdos de Paz no puede ser negociable y que la paz será con las mujeres. **Todas las instituciones del Estado deben estar abocadas a cumplir".** Le puede interesar:["Comunidades del Putumayo insisten en plan de sustitución a pesar de erradicación forzada"](https://archivo.contagioradio.com/comunidades-del-putumayo-insisten-en-plan-de-sustitucion-a-pesar-de-erradicacion-forzad/)

En la actualidad las mujeres están siendo instrumentalizadas por estructuras delictivas y, manifiesta Benjumea que el Estado no las está protegiendo "en el Acuerdo dice que el Estado tendrá que **buscar alternativas a la persecución penal de estas mujeres y eso lo tiene que cumplir todas las partes**, el Ministerio de Justicia, la Fiscalía General y estamos abocando por eso". Le puede interesar: [Erradicación de cultivos de uso ilicito van en contra de Acuerdo de Paz](https://archivo.contagioradio.com/erradicacion-de-cultivos-hecha-por-el-gobierno-iria-en-contra-de-acuerdo-de-paz/)

En ese orden de ideas , asegura la Corporación Humanas, la persecución penal a mujeres instrumentalizadas por el narcotráfico, podría equipararse a culpar a trabajadores por aceptar laborar por debajo del salario mínimo antes que exigir a los empleadores respetar las normas laborales.

### **Mujeres cocaleras privadas de la libertad.** 

Según un informe de Prison Insider al 31 de enero de 2017 **del total de la población carcelaria, cerca de 7849 son mujeres**, es decir un 6.6% y comparadas con las cifras de hace 25 años, hoy en día la población femenina es 5.5 veces mayor.

Además lo más dramático de estas cifras es que estas mujeres han sido **condenadas en su mayoría – un 45 % de ellas - por delitos relacionados con drogas**. Le puede interesar: [La alarmante situación de las cárceles colombianas](https://archivo.contagioradio.com/39173/)

"Hay más hombres privados de la libertad que mujeres, pero más mujeres privadas de la libertad por drogas que hombres, es decir, corresponden al eslabón más debil de la cadena y estamos hablando de lo nefasto que es perseguir a estas mujeres, muchas de ellas cabeza de hogar (...) es un ejercicio desproporcionado del derecho, persiguiendo mujeres pobres" recalca Benjumea.

En la actualidad **si una persona es judicializada por narcotráfico la ley no permite acceder a ningún tipo de beneficio**, no admite la cárcel domiciliaria, ni disminución de la pena, razón por la cual quien incurre en este delito está condenado a pagar con la prisión de manera completa hasta que termine la pena.

### **Solicitudes de las mujeres cocaleras del país.** 

Los primeros beneficios que están llegando como parte de los Acuerdos de Paz, están siendo entregados solo a las mujeres recolectoras de la hoja de coca, razón por la que las mujeres trabajadoras afectadas continúan solicitando vías de acceso que en la actulidad se encuentran en estado deplorable o inexistentes, **mayores incentivos para el comercio rentable de otros cultivos, mayor apoyo para nuevas economías.** Le puede interesar: [Mujeres cocaleras de todo el país se reúnen para trabajar propuestas de sustitución](https://archivo.contagioradio.com/mujeres-cocaleras-de-todo-el-pais-se-reunen-para-trabajar-propuestas-de-sustitucion/)

![HUMANAS](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/HUMANAS.jpg){.alignnone .wp-image-39654 width="656" height="761"}

<iframe id="audio_18339324" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18339324_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
