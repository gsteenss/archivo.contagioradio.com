Title: “Nos reunimos para destacar la vida y no la muerte del genocidio” UP
Date: 2016-10-20 13:30
Category: Nacional, Paz
Tags: encuentro, genocidio, paz, Unión Patriótica, UP
Slug: nos-reunimos-para-destacar-la-vida-y-no-la-muerte-del-genocidio-up
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/UP-2016-e1476988487771.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo de Abogados] 

###### [20 Oct. 2016]

A partir del jueves 20 de octubre y hasta el sábado 22, Bogotá recibe el XI Encuentro conmemorativo del **Día Nacional por la Dignidad de las Víctimas de la UP**. Desde diversos lugares del país, sobrevivientes y familiares de las víctimas de la UP arribarán a la capital para dar apertura a este evento caminando hasta la Plaza de Bolívar con banderas blancas como señal de respaldo a los acuerdos de La Habana.

A propósito de esta iniciativa, que ha sido liderada por la Coordinación Nacional de Víctimas y Familiares y la Corporación REINICIAR  uno de sus abogados, Jorge Gómez, aseguró que e**ste evento pretende rendir un tributo a los más de 6000 militantes del movimiento que fueron asesinados y desaparecidos de manera sistemática** “no podemos olvidar que este exterminio se llevó a muchos dirigentes, alcaldes, concejales, diputados, senadores, representantes a la cámara, por eso queremos aprovechar estos tiempos de paz para decir que este genocidio no vuelva a ocurrir jamás” aseguró. Le puede interesar: [La Unión Patriótica quiere demostrar que hay otra forma de gobernar](https://archivo.contagioradio.com/?s=union+patriotica+)

Frente al tema de la paz, la Unión Patriótica ha manifestado su optimismo a lo pactado a través de los acuerdos de paz en los cuales, según ellos se respeta la vida “parte de los firmado en La Habana es precisamente garantizar la oposición política y ojalá que esto que le pasó a la UP no vuelva a ocurrir. **Esperamos que este acuerdo, que va a garantizar el ejercicio de la oposición, se cumpla, esa es una de las cosas que queremos recalcar en este momento en el que está el proceso de paz**” agregó. Le puede interesar: [Si no se acaba con el paramilitarismo “yo creo que se va a repetir el caso de la Unión Patriótica”](https://archivo.contagioradio.com/el-paramilitarismo-tiene-una-fuerza-tan-grande-que-yo-creo-que-se-va-a-repetir-el-caso-de-la-union-patriotica/)

**Las actividades que se tienen planeadas para este XI encuentro están encaminadas a visibilizar la situación que aconteció hace 30 años, además de reconocer  y dignificar la lucha de muchos que sobrevivieron.** Plantones, representaciones artísticas y la simulación de una Comisión de la Verdad para la cual han invitado a reconocidas personalidades del orden nacional e internacional hacen parte de la agenda“van a acompañarnos varias personas destacadas, que van a hacer de comisionados, para que los sobrevivientes de esta masacre nos hablen de los hechos por los que pasaron y de cierta manera de esa parte de la historia oscura de este país” añadió el abogado.

**El encuentro pretende además recordar a cientos de líderes que murieron en este genocidio** “queremos destacar no la muerte sino la vida, queremos recordar a estas personas que fueron ultimadas por el genocidio como personas vivas, líderes, que hicieron parte de la labor política que se adelantó en esa época en el país” puntualizó Gómez.

<iframe id="audio_13405619" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13405619_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio[[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
