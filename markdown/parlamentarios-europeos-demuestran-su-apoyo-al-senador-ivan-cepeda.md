Title: Parlamentarios europeos demuestran su apoyo al senador Iván Cepeda
Date: 2015-11-27 15:34
Category: Nacional, Política
Tags: 122 parlamentarios de trece partidos políticos británicos e irlandeses, Angus McNeil, encargado de Asuntos Exteriores del Partido Nacionalista Escocés, ex-Ministro de Defensa George Howarth, Iván Cepeda, líder de Sinn Fein Gerry Adams, Parlamentarios europeos demuestran su apoyo al Senador Iván Cepeda, Polo Democrático Alternativo
Slug: parlamentarios-europeos-demuestran-su-apoyo-al-senador-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/facetas_ivan_cepeda4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:eluniversal.com 

En medio de los señalamientos del Procurador Alejandro Ordoñez contra el senador del Polo Democrático Alternativo, Iván Cepeda, 122 parlamentarios de 13 partidos políticos británicos e irlandeses han manifestado su apoyo al congresista, a través de una carta en la que afirman que el senador “es uno de los más reconocidos defensores de derechos humanos”.

**“Sería muy preocupante que el Senador Cepeda estuviera siendo sancionado como resultado del debate en el Congreso sobre el ex –Presidente Uribe y supuestos vínculos con paramilitares",** afirma la declaración de los parlamentarios.

Adicionalmente, los 122 parlamentarios han solicitado que los cargos sean anulados, si se llegara a confirmar que el senador Iván Cepeda fuera sancionado **“enviaría un mensaje escalofriante sobre la posibilidad de encontrar la verdad detrás de los crímenes del conflicto armado” y que “podría significar un golpe significativo a la integridad de la democracia colombiana”,** dice la carta.

Varios de los firmantes son políticos reconocidos como el ex-Ministro de Defensa George Howarth; el líder de Sinn Fein, Gerry Adams; el encargado de Asuntos Exteriores del Partido Nacionalista Escocés, Angus McNeil; entre otros.
