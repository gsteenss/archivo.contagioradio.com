Title: Nobel de Paz debe contribuir a la unidad y a la refrendación de los acuerdos
Date: 2016-10-07 13:28
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Nobel de Paz, Plebiscito
Slug: nobel-de-paz-debe-contribuir-a-la-unidad-y-a-la-refrendacion-de-los-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/esquivel1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ElOnce] 

###### [7 Oct de 2016] 

**Este viernes fue otorgado a Juan Manuel Santos el premio Nobel de Paz por su gestión en los Acuerdos.** Frente a ello el maestro argentino Adolfo Pérez Esquivel, premio Nobel de Paz en 1980 por su compromiso con la defensa de la democracia y los Derechos Humanos en las dictaduras militares en América Latina, manifestó su opinión sobre este nombramiento y la situación política que atraviesa Colombia.

Esta mención debe [“](https://archivo.contagioradio.com/adolfo-perez-nobel-de-paz-celebro-anuncio-desde-la-habana/)**[contribuir a la paz y a la unidad](https://archivo.contagioradio.com/adolfo-perez-nobel-de-paz-celebro-anuncio-desde-la-habana/), para encontrar los nuevos caminos que tanto necesita Colombia** (...) la paz no se regala, se construye y eso requiere de paciencia y decisión de todo el pueblo no solo de Santos” afirma el Nobel de Paz argentino.

Señaló que si bien ganó el no, fue un empate, puesto que la polarización jugo un notable papel y la abstención fue de un 63%, además existieron zonas que no pudieron votar, por ejemplo **algunas regiones de la Costa Caribe, en donde las inundaciones hicieron imposible incluso constituir las mesas de votación**.

Para Perez Esquivel, es fundamental en este momento político llamar a la unidad del pueblo colombiano, **“no puede haber un paso atrás de los espacios ganados para una solución política”,** la revisión de los acuerdos no debe implicar la prolongación de la guerra. “Aquellos que votaron por el no, deben plantear propuestas para continuar la construcción de paz”, buscar caminos para terminar la violencia y **“transformar las armas en arados que contribuyan a la vida”.**

“Los acuerdos nunca son perfectos pero todos los acuerdos son perfectibles (…) la intervención del pueblo juega un papel primordial”, **[quienes han sufrido directamente las consecuencias de esta guerra](https://archivo.contagioradio.com/victimas-reciben-con-alegria-el-premio-nobel-de-paz-para-colombia/) de medio siglo, son el centro del proceso** y es “importante analizar la situación de las víctimas a través de la comisión de la verdad” indicó el Maestro Pérez Esquivel.

A su vez invitó a reflexionar sobre el futuro que le espera a los 7 millones de desplazados colombianos “cuando vuelvan a sus territorios que fueron entregados a las transnacionales” y la situación de los falsos positivos a manos de las fuerzas parapoliciales y paramilitares. **La construcción de Paz en Colombia es un proceso que requiere tiempo, paciencia y sobretodo mucha claridad conceptual.** “Espero que el nombramiento de santos contribuya a encontrar el camino que el pueblo colombiano merece que es vivir en paz y dignamente” concluye.

<iframe id="audio_13224787" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13224787_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
