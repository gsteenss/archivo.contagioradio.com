Title: Pólizas de nuevo Código de Policía provocan abandono y sacrificio de perros
Date: 2017-02-17 16:37
Category: Ambiente, Nacional
Tags: código de policía, Perros potencialmente amorosos
Slug: polizas-de-nuevo-codigo-de-policia-provocan-abandono-y-sacrificio-de-perros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/marcha-pitbull-medellin-efe-1200x600.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro] 

###### [17 Feb 2107] 

El nuevo **código de Policía está provocando el sacrificio y abandono de perros denominados como razas peligrosas**, debido a la falta de pedagogía sobre la adquisición de pólizas de seguro para los animales y las multas impuestas por el código si se infringe la ley en la forma de llevarlos por la calles.

En el Código se establece que los **dueños de las mascotas deben adquirir una póliza por riesgos, sin embargo esta todavía no ha sido reglamentada**, motivo por el cual y de acuerdo con la Federación de Aseguradoras Colombianas, no puede ser requerida por las autoridades. No obstante en el Código se establece una multa por **\$786.000** pesos de no tener la póliza.

Las pólizas pueden tener diferentes costos y formas de pago, de acuerdo con Giovanna Caballero, directora de la Fundación Bull Kan, hay pólizas desde los **\$170.000 pesos anuales o algunas mensuales por \$40.000 pesos, pero estas no cumplen** con todos los requisitos que exige el Código de Policía. La multa por llevar a los perros sin bozal es de \$186.000 pesos. Le puede interesar: ["70 animales envenenados en un mes, alarman a comunidades en Curiti, Santander"](https://archivo.contagioradio.com/70-animales-envenenados-en-un-mes-alarman-a-comunidades-en-curiti-santander/)

Giovanna Caballero, afirmó que la **desinformación en Bogotá ha generado un abandono de 20 perros y que se han encontrado con casos en donde los dueños prefieren sacrificar al animal** que obtener la póliza “nos hemos encontrado con maltrato y asesinato, han matado perros a tiros, a una perrita, en Copacabana, la amarraron a un costal con piedras y la ahogaron, en Meta incineraron a otro animal”

Además, agregó que no hay ningún estudio que demuestre que algunas razas son más violentas que otras “**según expertos en comportamiento animal, biólogos, genetistas, dicen que no existen razas potencialmente peligrosas,** lo que existen son tenedores irresponsables que no educan bien a su animal de compañía”.

<iframe id="audio_17079653" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17079653_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
