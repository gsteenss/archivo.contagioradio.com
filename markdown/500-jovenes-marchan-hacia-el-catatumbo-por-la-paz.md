Title: ¡500 jóvenes marchan hacia el Catatumbo por la paz!
Date: 2019-07-17 16:34
Author: CtgAdm
Category: Nacional, Paz
Tags: acuerdo de paz, Brigada, Catatumbo, Implementación
Slug: 500-jovenes-marchan-hacia-el-catatumbo-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Catatumbo-Contagio-Radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Los días 19, 20 Y 21 de julio 500 jóvenes de 11 países se movilizarán hacia la región del Catatumbo, Norte de Santander, para visibilizar la crisis humanitaria del territorio y exigir la continuidad e implementación de los Acuerdos de Paz, esto en el marco de la III Brigada Internacional Juvenil por la paz de Colombia.

Según Jhonny Marín, coordinador para América Latina y el Caribe de la Federación Mundial de la Juventud Democrática, esta movilización que cuenta con participación de jóvenes de países como Noruega, Bélgica, Uruguay, Venezuela y otros, es una muestra de solidaridad con el pueblo colombiano: “Son jóvenes que ven la necesidad de salir de la guerra… y entienden que la paz de Colombia es la paz del continente”.

En ese sentido, las anteriores Brigadas Internacionales se han movido en torno a temas de paz y derechos humanos, la primera se llevó acabo en el 2013 en la Macarena (Meta), donde se encontraba ubicada la fosa común más grande de Latinoamérica, la segunda sucedió en el 2015 en la región del Mango (Cundinamarca) por la defensa de los derechos humanos y el Derecho Internacional Humanitario.

Así, esta III Brigada en el Catatumbo tiene como objetivos, además de insistir al gobierno nacional en el proceso de implementación de los Acuerdos de Paz, y de las garantías para el ​ETCR (Espacios Territoriales de Capacitación y Reincorporación) allí ubicado; promover e insistir en el diálogo con otros actores armados que hacen presencia en la región, es decir, retomar la Mesa de Negociación con el ELN, y avanzar en los diálogos con el EPL.

En ese sentido, las diferentes organizaciones sociales han expresado el incumplimiento de la implementación del Acuerdo: “Han sido las distintas comunidades y las fuerzas sociales y campesinas que ha logrado avanzar en un acuerdo para evitar sobre todo un mayor derramamiento de sangre, organizaciones como ASCANCA, CISCA, ASOCUMONALES, entre otros”, mencionó Marín. (Le puede interesar: [José Arquímedes Moreno, tercer líder social asesinado en el Catatumbo en 2019)](https://archivo.contagioradio.com/lider-social-jose-arquimedes-moreno-fue-asesinajose-arquimedes-moreno-tercer-lider-social-asesinado-en-el-catatumbo-en-2019/)

Lo más importante con este tipo de iniciativas juveniles, que sobre todo nace de las organizaciones y el sentido por promoción de la paz, es precisamente rodear estos diferentes procesos sociales y hacer sentir a las comunidades, frente a la no actuación del Estado, que no están solos, aseguró el líder juvenil.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38606651" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38606651_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
