Title: Hidroeléctrica el Quimbo busca dividir para no compensar daños: Afectados
Date: 2020-02-27 10:43
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: ANLA, comunidades, El Quimbo, Huila
Slug: hidroelectrica-el-quimbo-busca-dividir-para-no-compensar-danos-afectados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Hidroeléctrica-El-Quimbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: CAS {#foto-cas .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->  
<iframe id="audio_48357374" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_48357374_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Este 26, 27 y 28 de febrero la empresa ENEL - EMGESA está convocando a comunidades afectadas por la hidroeléctrica el Quimbo, de la que es constructora, a unas reuniones de información sobre alternativas de compensación por los daños causados por la obra. Según la asociación de afectados por la represa, la reunión sería un nuevo intento de evadir los compromisos establecidos en la licencia ambiental del Proyecto.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/QChavarro/status/1232659761704902660","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/QChavarro/status/1232659761704902660

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### 10 años de incumplimientos a los afectados por El Quimbo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Jennifer Chavarro, integrante de ASOQUIMBO, explicó que pese a que han pasado 10 años desde que inició la construcción de la hidroeléctrica (en 2010) aún hay elementos importantes pendientes en términos de compensación y licencia ambiental. En cuanto a la licencia ambiental, se estipula que el Gobierno Nacional debía hacer la compra de 2.700 hectáreas, y ENEL-EMGESA adecuarlas con un distrito de riego por gravedad para restituir la actividad productiva de las comunidades afectadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Chavarro, este es un elemento importante que fue una de las razones por las que se negó en un primer momento el proyecto y cuando se concedió su licencia, apareció como una acción relevante que debía emprender la empresa. No obstante, desde 2013 la empresa ha insistido en señalar que no es posible técnicamente cumplir con el requerimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras los constructores de la Hidroeléctrica han afirmado que no hay tierras para realizar la compra de las mismas, las comunidades aseguran que en Huila hay 17 mil hectáreas para la compra y por lo menos 4 posibilidades de captación de aguas para el distrito de riego. (Le puede interesar: ["La hidroeléctrica El Quimbo, con los mismos problemas de Hidroituango"](https://archivo.contagioradio.com/el-quimbo-problemas-hidroituango/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La estrategia de la desesperación**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Chavarro aseguró que la reunión forma parte de una ["estrategia amañada"](https://www.asoquimbo.org/comunicados/la-estrategia-de-enel-emgesa-para-incumplir-la-obligacion-de-restituir-la-actividad-productiva-del-huila)de la empresa para lograr que las comunidades renuncien a sus tierras, acudiendo a la incertidumbre y desesperación, luego de 10 años en que no se han cumplido las obligaciones para mitigar el impacto de la obra. (Le puede interesar: ["Hidroeléctrica El Quimbo: "una amenaza potencial"](https://archivo.contagioradio.com/el-quimbo-amenaza-potencial/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así, la integrante de ASOQUIMBO sostiene que la empresa estaría acudiendo a cambiar la compra y adecuación de las tierras por beneficios que estarían cercanos a los 30 millones de pesos, pagados mediante ayudas en la producción, maquinaria o materiales. (Le puede interesar: ["Hidroeléctrica del Quimbo es un desastre ambiental y económico"](https://archivo.contagioradio.com/hidroelectrica-del-quimbo-es-un-desastre-ambiental-y-economico/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra parte de la estrategia de la empresa es la exclusión de los gobiernos locales y regionales, puesto que a las reuniones solo habría sido invitada la Autoridad Nacional de Licencias Ambientales (ANLA). Por el contrario, fue la Gobernación del Huila la que denunció la reunión, y pidió que se cumpla con las compensaciones que debe cumplir el Proyecto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, Chavarro sostuvo que pedían al Gobierno Nacional que se respete lo establecido en la licencia ambiental, y se cumpla a cabalidad con las condiciones allí expresadas en favor de los 6 municipios que están en la zona de influencia del Proyecto. (Le puede interesar: ["ANLA se subordina ante EMGESA: ASOQUIMBO"](https://archivo.contagioradio.com/anla-se-subordina-ante-emgesa-asoquimbo/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
