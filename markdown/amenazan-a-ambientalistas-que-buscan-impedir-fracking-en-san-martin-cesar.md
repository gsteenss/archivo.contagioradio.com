Title: Amenazan a ambientalistas que buscan impedir fracking en San Martín, Cesar
Date: 2016-09-14 12:54
Category: DDHH, Nacional
Tags: Ambiente, Amenazas, cesar, fracking, san martin
Slug: amenazan-a-ambientalistas-que-buscan-impedir-fracking-en-san-martin-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Cr3NHMrXEAAUhW8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CORDATEC 

###### [14 Sep 2016] 

Ante las acciones de resistencia por parte de la comunidad en San Martín, Cesar, acompañada por la Corporación Defensora del Agua, Territorio y Ecosistemas, CORDATEC, la organización denuncia que su vocero **Carlos Andrés Santiago, quien viene liderando las iniciativas de la comunidad en contra  del Fracking**, ahora es objeto de amenazas por parte de personas anónimas, así como directamente por parte de funcionarios de la empresa ConocoPhillips, que pretende adelantar actividades de fracturamiento hidráulico.

En los últimos días CORDATEC ha estado apoyando a los habitantes del corregimiento Cuatro Bocas en el plantón pacífico que desde el pasado 7 de septiembre se lleva a cabo para impedir el paso de maquinaria de la empresa para desarrollar **operaciones petroleras en el pozo PicoPlata 1, para las cuales no tiene permiso.**

Según señala la Corporación en un comunicado, “A raíz de nuestro trabajo y labor de denuncia el pasado domingo recibimos información **sobre unas acciones contra la vida e integridad de nuestro compañero Carlos Andrés Santiago** quien con valentía ha asumido la vocería de CORDATEC para denunciar en medios de comunicación y visibilizar en diferentes escenarios, los atropellos y abusos de los que hemos sido víctimas”.

Además, según cuenta Santiago, durante la jornada de resistencia para impedir que la empresa ingresara, el líder ambientalista habló por teléfono con el coordinador social de la ConocoPhillips, quien le dijo durante la conversación que la compañía los iba a demandar  y que **CORDATEC iba a tener que pagar por el dinero que han "perdido"** por el plantón que se desarrolla en Cuatro Bocas.

“No ha habido un ejercicio de diálogo, estamos en todo el derecho en defender el agua y nuestros ecosistemas, **seguiremos en Cuatro Bocas así nos intimiden y nos digan que no van a demandar,** con todo gusto nos veremos en los escenarios judiciales, pero vamos a defender nuestro territorio”, expresa Carlos Andrés Santiago ante esa amenaza.

La Corporación indica que van a interponer **una acción de tutela con una medida cautelar para que se detengan las actividades** en el pozo. Así mismo, se están presentando las denuncias antes los organismos correspondientes como la Fiscalía General de la Nación, la Defensoría del Pueblo, la Policía Nacional, y demás autoridades para que se investigue y proteja la vida de quienes buscan impedir los graves daños ambientales que podría causar el fracking en San Martín.

<iframe id="audio_12906111" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12906111_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
