Title: Israel reprime movilización de comunidad etíope contra el racismo
Date: 2015-05-04 15:21
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Disturbios Etíopes Israelistas, Etíopes Israel protestan, Israel, Racismo Etíopes Israel
Slug: israel-reprime-movilizacion-contra-el-racismo-es-institucionalizado
Status: published

###### Foto:Safed-tzfat.blogspot.com 

###### Entrevista con **Sergio Yani**, de la Agencia de Medios alternativos de Israel: 

<iframe src="http://www.ivoox.com/player_ek_4444051_2_1.html?data=lZmhlpWZdY6ZmKiakpuJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMrn1drfxM7Tt4zZz5C21dfFqc2f0dTfjdXWs9XZ1NnO1ZDIqYzgwpDQ0dLZssrYwsmYp9mJh5SZoqmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Después de la difusión de un video donde **policías blancos maltrataban** a un detenido **Etíope** en la ciudad de Holón, la comunidad se manifestó en contra del racismo institucional que padecen desde décadas.

Más de **30 heridos y dos detenidos** fue el balance final de las **protestas** que comenzaron en **Jerusalén Este** y tuvieron su mayor asistencia en **Tel-Aviv, la capital de Israel. **Después de los disturbios unas **3.000 personas intentaron acceder al edificio de la alcaldía.**

La comunidad etíope acusa al gobierno de Israel de perpetuar el racismo institucionalizado contra ellos, mediante la policía y la ubicación estratégica de más de 17.000 judíos etíopes en 17 guetos por todo el país. También acusan al ejecutivo de haberse olvidado de ellos y de la grave situación de exclusión social que están viviendo.

Según Avi, manifestante etíope "…**El 70 por ciento de los etíopes han sido concentrados en guetos en 17 localidades**, creando tensiones con el resto de la población, y desde que nos colocaron allí no se acuerdan más de nosotros…".

Para Sergio Yani, miembro de la Agencia de medios alternativos de Israel, la comunidad etíope ha **sido excluida históricamente** no solo por el estado sino por el seno de la sociedad por el mero hecho de ser africanos, además añade que es una **consecuencia de la propia conformación del estado Israelí** que ha discriminado desde su construcción a toda persona árabe o de origen africano.
