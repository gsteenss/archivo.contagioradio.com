Title: Si no se avanza en acuerdos habrá movilización en Agosto: Cumbre Agraria
Date: 2015-06-25 12:03
Category: Movilización, Nacional
Tags: ANZORC, CNA, Coordinador Nacional Agrario, Cumbre Agraria, Organización Nacional Indígena de Colombia, Proceso de Comunidades Negras, zonas de reserva campesina
Slug: si-no-se-avanza-en-acuerdos-habra-movilizacion-en-agosto-cumbre-agraria
Status: published

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4687519_2_1.html?data=lZulmZqVfY6ZmKiak5eJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8qfz9SY1cqPpdfVz9%2FOjcrSb8LX1srfxtTXb8nVw9eSpZiJhZKfztTjy9HNvsLXyoqwlYqmd8%2BfxtOYo8zTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [José Santo Caicedo. PCN] 

###### [25 Jun 2015]

Hoy las organizaciones de la **Cumbre Agraria** se reúnen con el presidente **Juan Manuel Santos para evaluar las razones sus incumplimientos** y dilaciones. Según José Santo Caicedo, vocero del **Proceso de Comunidades Negras, PCN**, es necesario que se establezcan mecanismos ciertos y eficaces para que se logre avanzar y así evitar una nueva etapa de movilizaciones.

El Paro Agrario se realizó en agosto de 2013, la instalación oficial de la mesa de diálogos entre la Cumbre Agraria y el gobierno nacional en mayo de 2014, casi un año después, y hasta el momento **no se ha iniciado la discusión de ninguno de los 8 puntos pactados del acuerdo inicial**. También se pactaron unos acuerdos iniciales de confianza entre las partes, como las **garantías de DDHH**, pero tampoco se ha podido avanzar en ello.

Frente al **Plan Nacional de Desarrollo** las comunidades plantean que fue una “cachetada” que aprobó el gobierno, puesto que **ninguna de las exigencias de los campesinos, negros e indígenas quedó incluida** en ese PND. Los rubros presupuestales serían una de las formas en que podrían acercarse las partes, pero tampoco se ha cumplido dice el PCN, así como tampoco se ha discutido uno de los primeros puntos que es el minero-energético.

Para el vocero del PCN, es necesario que se establezcan mecanismos como el nombramiento de una persona con capacidad de decisión que no dilate los diálogos y que no se vaya por las ramas. “El diálogo no hay que agotarlo” afirma el vocero, y agrega que también “hay que pasar del diálogo a la acción concreta” en referencia a la movilización de Dignidad Agropecuaria del pasado Lunes. Según Caicedo, “estamos alistando ya los sombreros, los bastones para a finales de agosto volver a las carreteras y a los barrios”.
