Title: "Colombia le dice al mundo que enfrentar el miedo y movilizarse si paga"
Date: 2016-09-26 15:33
Category: Entrevistas, Paz
Tags: Acuerdos de paz en Colombia, César Torres Del Río, Diálogos de paz en Colombia, proceso de paz Colombia
Slug: colombia-le-dice-al-mundo-que-enfrentar-el-miedo-y-movilizarse-si-paga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/carmela-maria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Carmela María ] 

###### [26 Sept 2016] 

La más reciente Encuesta de Opinión Nacional hecha por Invamer señala que el 67.7% de los colombianos votarán por el sí en el plebiscito y un 32.4% lo hará por el no; mientras que la encuesta Polimétrica indica que el 54% de los votantes respaldarán los acuerdos con el sí y el 34% los rechazará. Más allá de las diferencias porcentuales **lo importante es que la ciudadanía participe políticamente en todos los asuntos de su competencia**, porque no sólo está en juego la paz, la verdad, la garantía de no repetición y la reparación, sino la manera en la que participamos, como afirma el analista político Cesar Torres del Río.

Y es que este plebiscito marca un hito histórico, sí se tiene en cuenta que como lo señala el analista, el [[1° de diciembre de 1957](https://archivo.contagioradio.com/los-plebiscitos-de-1957-y-2016-1er-parte/)] la ciudadanía acudió a las urnas para votar lo que fue el primer plebiscito de la era moderna en Colombia, con el que se **aceptó la impunidad que habían pactado las élites liberal y conservadora, frente a la violencia estatal** de la que fueron víctimas los campesinos, trabajadores, mujeres y estudiantes. Por el contrario, el próximo 2 de octubre los ciudadanos tendrán la posibilidad de votar en un plebiscito que tiene en el centro a las víctimas y que sintetiza más de 60 años de lucha social contra este esquema de [[control hegemónico que fue aprobado](https://archivo.contagioradio.com/los-plebiscitos-de-1957-y-2016-2da-parte/)] en 1957.

Para el analista, si bien en el actual contexto mundial de guerra, impunidad y violaciones, están triunfando las fuerzas más poderosas del capital, en el terreno nacional es evidente que hay una **modificación sustancial en las condiciones de resistencia y de movilización popular y ciudadana**, que no implicará en el mediano plazo, transformaciones drásticas en la estructura del Estado y del régimen político, pues el acuerdo mismo es justamente una validación de las condiciones sociales políticas y económicas del actual statu quo, que no obstante permite un paso adelante en la garantía de los [[derechos que son demandados](https://archivo.contagioradio.com/incorporacion-del-enfoque-de-genero-un-hecho-historico-que-ningun-proceso-de-paz-ha-logrado/)] en aspectos como la identidad de género.

Si bien con el acuerdo de paz no se está hablando de un quiebre histórico, sí estamos ante un punto significativo de avance que da a entender al mundo que "movilizarse y enfrentar el miedo si paga", afirma Cesar Torres e insiste en que **el miedo está siendo usado como un arma política para desinformar a la sociedad** con argumentos como que con la implementación de los acuerdos la fuerza pública va a ser disminuida, que las pensiones se verán afectadas para financiar la desmovilización o que la religión podrá ser menoscabada en sus aspectos más esenciales; por lo que se hace necesario que la [[ciudadanía participe masivamente con el sí](https://archivo.contagioradio.com/el-respaldo-a-los-acuerdos-en-el-plebiscito-es-el-primer-paso-hacia-la-paz/)] para hacer frente.

El analista concluye asegurando que las experiencias de procesos de paz como el de Ruanda, Sudáfrica, Guatemala o El Salvador, brindan [[enseñanzas para la implementación](https://archivo.contagioradio.com/ninos-victimas-violencia-sexual-el-salvador-19742/)] de los acuerdos de paz en Colombia, en temas como la verdad, la justicia, la reparación y las garantías de no repetición. Pues si bien hubo movilización social, no fue lo suficientemente fuerte para que se develará la verdad histórica y judicial, ni para impedir que surgieran grupos armados posteriores, ni mucho menos para detener los altos índices de corrupción, problemáticas que **podrían evitarse sólo si se mantiene la movilización y participación ciudadana**.

<iframe id="audio_13067884" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13067884_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
