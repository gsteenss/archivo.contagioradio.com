Title: La carta de Carlos Velandia desde Paloquemao
Date: 2016-06-21 16:58
Category: Judicial, Nacional
Tags: Carlos Arturo Velandia, ELN, Proceso de conversaciones de paz
Slug: la-carta-de-carlos-velandia-desde-paloquemao
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/carlos-velandia-detenido-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elpais] 

###### [21 Jun 2016]

El ex integrante del ELN, [Carlos Velandia](https://archivo.contagioradio.com/?s=carlos+velandia), emitió una carta para compartir las impresiones acerca de su captura, en la que afirma que el proceso que se inició en el año 2000 **no fue notificado en ningún momento y que por ello se viola el debido proceso así como se afecta su derecho a la defensa.**

En 10 puntos el gestor de paz explica que estuvo detenido durante 20 años, que pagó su condena y luego se dedicó a luchar por la paz, por lo tanto este proceso le deja **"serias dudas" sobre los mecanismos de aplicación de justicia** y las implicaciones que puede tener el uso de los estrados judiciales con fines políticos.

En la carta también explica que no se amilanarán las apuestas de paz del [ELN](https://archivo.contagioradio.com/?s=ELN) ni tampoco los esfuerzos de muchas personas que se han comprometido con el proceso de conversaciones o la construcción de la paz. A continuación difundimos el texto completo.

*"Celdas de paso del CTI Fiscalía Paloquemado Bogotá*

*Algunas observaciones sobre mi detención:*

*1. Fui capturado y vencido en juicio en 1994, pague una condena de 20 años, estuve preso 10 años físicos. El proceso judicial y la pena fueron absoluta y totalmente ejecutoriados.*

*2. Hice dejación individual de armas desde mi salida en libertad el 8 de octubre de 2003.*

*3. He dedicado todos mis esfuerzos en construir caminos para la paz y la convivencia.*

*4. Todas mis actuaciones han sido públicas y visibles: me he entrevistado con presidentes de la república, con parlamentarios, con alcaldes y gobernadores. Con personas reconocidas de la sociedad civil, de las iglesias, de embajadas, con altos funcionarios del gobierno que trabajan por la paz, con ONG\`S de Derechos Humanos, con plataformas de paz.*

*He sido creador colectivo de centros de pensamiento para la paz y la democracia, he escrito numerosos artículos de opinión y análisis, he escrito tres libros y participado en la edición colectiva de otros tantos, dicté conferencias sobre paz, doy clases sobre este tema en colegios y sindicatos, participé en programas de opinión de radio y televisión sobre temas de paz. En fin, no tengo vida ni esfuerzo alguno que no sea dedicado a ayudar para que un día, por fortuna no muy lejano, Colombia y sus gentes puedan empezar a vivir en paz.*

*5. Mi captura, inexplicable, es una muy mala señal; muestra que la justicia ordinaria que me ha juzgado no está sostenida sobre bases jurídicas seguras; encuentro que la justicia puede ser usada con fines oscuros perversos.*

*6.  Es una mala señal para el proceso de paz, que puede generar dudas o frenar el ímpetu que tienen las partes comprometidas en el proceso de paz; o quizás es lo que buscan al abrirme un proceso que " ya es cosa juzgada".*

*7.  Desconozco las intensiones o las fuerzas que se mueven detrás de este "falso positivo judicial"; pero creo que si pretenden amilanar  a alguien con ello no lo van a lograr, porque yo estoy curado de espantos y continuaré con mis esfuerzos de paz hasta lograr el fin de la guerra y el comienzo de la construcción social y colectiva de la justicia social.*

*8. Amilanar a las partes? Se equivocaron de cabo a rabo; al Gobierno y a las Farc no las frena nada en interponer los mejores esfuerzos para lograr el acuerdo del fin del conflicto armado.*

*9. ¿Amilanar al ELN? nuevamente se equivocaron, los elenos tienen en su ADN político la impronta de cumplir las decisiones internas y cumplir los acuerdos que pacten. El ELN irá hasta el final en su esfuerzo por la paz y la justicia social.*

*10. Obviamente que esta circunstancia me golpea, también a mi entorno social y familiar; pero al igual que otros retos, asumo este como uno más en la larga lucha por la conquista de días mejores para Colombia y sus gentes.*

*Fraternalmente*

*CARLOS ARTURO VELANDIA*

*Celdas de paloquemado- 21 de Junio 2016"*
