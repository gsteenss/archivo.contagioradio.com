Title: "Duque no desconozca el camino ya andado" FARC
Date: 2018-06-18 10:41
Category: Paz, Política
Tags: acuerdos de paz, Colombia Humana, ELN, FARC, Iván Duque
Slug: duque-no-desconozca-el-camino-ya-andado-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Paz-e1499290199642.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [18 Jun 2018] 

En una rueda de prensa, el partido político FARC en cabeza de su presidente Rodrigo Londoño, dio a conocer sus reflexiones frente a la llegada de Iván Duque a la presidencia. En la misma invitaron al nuevo mandatario a reunirse con ellos para conversar sobre la implementación de los Acuerdos de La Habana y **para que no vaya a desconocer los avances que ya se han logrado en el país frente a la construcción de paz**.

En ese sentido instaron a Duque a que no desconozca el camino ya “andado” en materia de paz y “burlar” los compromisos adquiridos por el Estado frente a la sociedad colombiana y la comunidad internacional, debido a que este acto **solo lograría el retorno de los ciclos de violencia al país.** Razón por la cual piden que medie la sensatez para que exista una paz integral que lo conduzca a la reconciliación.

De igual forma, hicieron un llamado a la unidad de todos los sectores que creen en “la posibilidad de un futuro distinto” y que participaron de la campaña de la Colombia humana, **“solamente esa unidad transformada en organización y movilización podrá detener la tentación de los sectores más retardatarios** de la política tradicional de profundizar los odios”.

En el comunicado señalan que la campaña electoral permitió evidenciar que en Colombia está surgiendo “una alternativa política distinta a las que tradicionalmente han gobernado al país” y demostrar que hay un paso adelante en la “conciencia de un número muy grande de colombianas y colombianos **que, con su participación, asumen una posición por el cambio y la trasformación social**”.

Asimismo, rescataron el hecho de que, durante la primera y segunda vuelta, Colombia no vivió ningún incidente producto del conflicto armado debido tanto a la firma de los Acuerdos de paz, como al cese unilateral del ELN, hechos que demuestran el avance y las diferentes voluntades de diversos sectores políticos por construir paz. (Le puede interesar: ["Cese bilateral será tema central en el sexto ciclo de conversaciones"](https://archivo.contagioradio.com/cese-bilateral-sera-tema-central-en-el-sexto-ciclo-de-conversaciones/))

###### Reciba toda la información de Contagio Radio en [[su correo]
