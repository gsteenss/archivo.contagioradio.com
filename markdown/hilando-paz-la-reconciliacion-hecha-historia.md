Title: Hilando la Paz
Date: 2020-08-20 22:23
Author: CtgAdm
Category: DDHH, Especiales Contagio Radio
Tags: etcr, firmantes de paz, Hilando la Paz, Proyectos Productivos
Slug: hilando-paz-la-reconciliacion-hecha-historia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/06-Constructores-de-paz-El-sueno-de-vivienda-para-una-nueva-vida_Moment-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/07-Siembra-tu-arbol-por-la-paz_Moment-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/08-Los-paraisos-que-la-guerra-oculto_Moment-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/09-Los-futuros-medicos-de-la-paz_Moment-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/01-Hilando-Paz_Moment-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/02-Remando-por-la-paz_Moment-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/03-Heroes-y-heroinas-de-la-caficultura_Moment-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/04-La-casa-de-la-vida-Bioexpedicion-en-Anori_Moment-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/05-Arando-la-educacion_Moment-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-07-at-11.43.14-AM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-07-at-4.36.03-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-07-at-11.43.12-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**  
**

**HILANDO PAZ, LA RECONCILIACIÓN HECHA HISTORIA**

**Hoy en medio de un recrudecimiento del conflicto armado, el desistir de algunas personas que regresaron a las armas y la alarmante cifra de al menos 240 firmantes de la paz asesinados,  diferentes iniciativas de reconciliación y emprendimiento en todo el territorio nacional, se abren camino para demostrar su compromiso con la paz de Colombia.**

20 de agosto de 2020

<figure>
![Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Contagio-Radio.jpg){width="500" height="500" sizes="(max-width: 500px) 100vw, 500px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Contagio-Radio.jpg 500w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Contagio-Radio-300x300.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Contagio-Radio-150x150.jpg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Contagio-Radio-370x370.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Contagio-Radio-310x310.jpg 310w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Contagio-Radio-230x230.jpg 230w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Contagio-Radio-360x360.jpg 360w"}

</figure>
Por: Contagio Radio - Consejo Nacional de Reincorporación

#### -Reconciliarnos significa traer paz a las naciones, a la gente y a nuestra familia. Para poder reconciliarnos, necesitamos desarrollar el arte de la escucha profunda. -Nhat Han.

https://www.youtube.com/watch?v=AWXqgrDerxI&t=5shttps://www.youtube.com/watch?v=ElD0Iy6P5Qw&t=1shttps://www.youtube.com/watch?v=ZixJOC\_nYwQ&t=1s

TEJIENDO LA PAZ
---------------

REMANDO POR LA PAZ
------------------

HÉROES Y HEROÍNAS DE LA CAFICULTURA
-----------------------------------

https://www.youtube.com/watch?v=THhTICZNDDshttps://www.youtube.com/watch?v=J3LCMi3nQyghttps://www.youtube.com/watch?v=hH9aS77714c&t=1s

LA CASA DE LA VIDA
------------------

ARANDO LA EDUCACIÓN
-------------------

CONSTRUCTORES DE PAZ
--------------------

https://www.youtube.com/watch?v=9HzC8TjxjIshttps://www.youtube.com/watch?v=8BznUU1wWKQhttps://www.youtube.com/watch?v=un7Xc\_nRudE

SIEMBRA TU ÁRBOL POR LA PAZ
---------------------------

LOS PARAÍSOS QUE LA GUERRA OCULTÓ
---------------------------------

LOS FUTUROS MÉDICOS DE LA PAZ
-----------------------------

https://www.youtube.com/watch?v=CXB9OcsDEeI

CLUB DEPORTIVO PAZ Y RECONCILIACIÓN
-----------------------------------
