Title: En MIDBO 2018 el documental va más allá de las pantallas
Date: 2018-09-28 15:54
Author: AdminContagio
Category: 24 Cuadros
Tags: Bogotá, Documental, MIDBO
Slug: midbo-2018-documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Midbo-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MIDBO 2018 

###### 27 Sep 2018 

El documental en Colombia tiene su propio festival y es la **Muestra Documental Internacional de Bogotá MIDBO 2018**, que para esta edición propone una revisión de los procesos y las tendencias del documental en los últimos veinte años, considerando los giros, rupturas, y cambios de paradigma que se han dado en el panorama nacional e internacional, a través de sus tres componentes: la **Exhibición de películas, la exposición de obras de Documental expandido y el Encuentro Pensar lo Real**.

El primer evento tendrá lugar en el Centro Ático de La Universidad Javeriana con la selección **Documental Expandido**, que busca dar cuenta del alcance de la diversidad formas y exploraciones cinematográficas que proponen los autores al abordar la realidad, en las que se pone a prueba la narrativa tradicional del cine y se abren nuevas posibilidades de experimentación y de relación con el espectador. Las obras documentales presentadas son de diversos formatos, como instalaciones audiovisuales y sonoras, performances, acciones, proyectos transmedia, documentales interactivos, arte para la red y obras de realidad virtual.

La [programación](http://midbo.co/pdf/programa.pdf) de **exhibición** se realizará del 3 al 10 de octubre en Cine Tonalá, cinecolombia Avenida Chile, Sala Fundadores de la Universal Central, la Biblioteca Nacional, la Cinemateca Distrital, La Alianza Francesa y la Cinemateca alterna de la Universidad Nacional. Este año la Midbo presenta una selección de **95 obras documentales de largo y cortometraje de Colombia y otros 26 países**, las cuales brindan un panorama de la creación documental actual y de su evolución en estos 20 años; 50 de ellos seleccionados por convocatoria abierta en las **categorías Nacional, Miradas Emergentes y Otras Miradas**, también hace parte de la muestra Espejos al Volver de la Guerra y **Retrospectiva Mibdo 20 años**.

El tercer componente de la Mibdo es el **Encuentro Pensar lo Real**, que se plantea como un espacio de trabajo y pensamiento alrededor de las diferentes expresiones del documental desde sus transformaciones, rupturas y relaciones con otras prácticas artísticas y otras formas de narrar o abordar lo real. Un encuentro que busca reunir realizadores, realizadoras, artistas, académicos, estudiantes y demás agentes del campo documental con el propósito de compartir trabajos de investigación y procesos creativos, generar redes de colaboración y establecer estrategias de fortalecimiento al sector.

Además, este año Mibdo cuenta con un nutrido grupo de invitados nacionales e internaciones como Peter Metter de Canadá, Henri- Francois Imber de Francia, Maria Luisa Ortega de España, Jorge La Ferla de Argentina, Jean- Cosme Delaloye de Suiza, Atienne de France de Francia, Andres Denegris de Argentina y por Colombia, entre otros, estarán Andres Pedraza, Camilo Aguilera, Diana Cuellar, Oscar Campo, Sorany Marin, Víctor Palacio y la documentalista Wayu Leiqui Urina, que se hacen presentes con obras en exhibición, así como en charlas conversatorio y otros espacios de encuentro.

La Muestra Internacional Documental de Bogotá es organizada por la Corporación Colombiana de Documentalistas ALADOS, un evento enfocado en el cine y otras prácticas documentales contemporáneas, que a lo largo de los últimos 20 años, se ha consolidado, en Colombia y América Latina, como un espacio fundamental de exhibición, encuentro, diálogo y pensamiento alrededor del cine de lo real. (Le puede interesar: [Pepe Mujica desde la mirada de Kusturica](https://archivo.contagioradio.com/pepe-mujica-kusturica/))

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
