Title: Mercaderes Cauca le dice no a la minería
Date: 2017-01-26 18:08
Category: Ambiente, Nacional
Tags: Cauca, Minería de oro, Minería ilegal
Slug: mercaderes-cauca-le-dice-no-a-la-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/minería.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Panoramio] 

###### [26 Ene 2017] 

Comunidades campesinas del corregimiento La Esmeralda, en el municipio de Mercaderes Cauca, se movilizaron para rechazar un proyecto de exploración y explotación minera, que tramita actualmente el Grupo CI S.A.S ante el Consejo Municipal de Mercaderes. Los habitantes aseguran que de aprobarse dicho proyecto **pondrían en grave riesgo ecosistemas importantes para la producción de agua.**

Ya en 2015 la Coordinadora Campesina y Popular Mercadereña, había llevado ante entidades estatales casos que evidenciaban las afectaciones que dejó la minería ilegal presente en este municipio, denunciaron que a causa de esta actividad, **“el río San Bingo es sólo un recuerdo que habita en la comunidad”.**

El río San Bingo fue declarado por autoridades ambientales como uno de los primeros ríos en desaparecer en Colombia, no sólo por efectos del cambio climático, sino por el **vertimiento de mercurio y cianuro a manos de quienes realizaban minería ilegal** en este afluente.

La comunidad, también había llevado ante la Gobernación del Cauca el caso de las **360 hectáreas de bosque nativo**, ubicadas entre los municipios de Almaguer, Bolívar y Mercaderes, nutridas por el río San Bingo, **que al igual que el río desaparecieron.**

Hasta el momento las directivas del Grupo CI S.A.S, han dicho que el trámite no ha avanzado lo suficiente como para “prender las alarmas sobre minería (…) en caso de que sí se llegue a abrir una mina, en la vuelta de cinco o siete años, se haría con todos los patrones de sostenibilidad”.

Sin embargo, la comunidad de mercaderes ha manifestado que la movilización que convocó a unas 5.000 personas, continuará **“hasta que la empresa decida suspender las socializaciones y los trámites ante la ANLA”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
