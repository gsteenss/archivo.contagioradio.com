Title: Los retos al congreso del Partido Liberal
Date: 2017-09-21 15:14
Category: Nacional, Política
Tags: elecciones 2018, Parido Liberal
Slug: los-retos-al-congreso-del-partido-liberal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/SERPA-e1506024801838.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Frente] 

###### [21 Sept 2017] 

El partido Liberal tendría que decidir en algún mecanismo de consulta quién será el candidato o candidata que los represente para las próximas elecciones presidenciales de 2018, entre los nombres que **podrían ocupar esta representación están Humberto de La Calle, Vivian Morales y Juan Fernando Cristo**.

La senadora Vivian Morales planteo que se estaría buscando un acuerdo para “cercenar” su participación, y que no desistirá de la candidatura, en ese sentido el único mecanismo que le quedaría a Moreales sería la recolección de firmas. (Le puede interesar: ["Los partidos se convirtieron en empresas electorales"](https://archivo.contagioradio.com/46707/))

No obstante, Horacio Serpa, codirector del partido Liberal afirmó que “nadie quiere excluir a nadie” y que se está conversando sobre definiciones de lo que significa representar a esta instancia y **de la creación de unos compromisos que respeten y trabajen en procura de apuestas tan importantes como la paz**.

En ese sentido y para el caso específico de Vivian Morales, que se ha opuesto al proceso de paz, Serpa afirmó que lo que se está buscando es la construcción de un borrador de compromiso, que no es una imposición para ningún precandidato. (Le puede interesar: ["Acuerdos de paz podrían perder el Blindaje jurídico"](https://archivo.contagioradio.com/acuerdos-de-paz-podrian-perder-su-blindaje-juridico/))

Sobre a las candidaturas que se han lanzado vía recolección de firmas, Serpa afirmó que es un hombre de partido y que no cree conveniente este mecanismo para el fortalecimiento de este tipo de instituciones. De igual forma manifestó que el partido Liberal recibió con muy buenos ojos los últimos resultados de la gran encuesta, sin embargo, afirmó que falta **mucho trecho y que es necesario que se tengan definiciones ideológicas y programáticas modernas para fortalecer al liberalismo**.

“Tenemos que tener una actitud diáfana, a propósito de la corrupción y ser constructivos, saber representar bien los intereses de la juventud, de la mujer, en eso estamos” afirmó Serpa.

<iframe id="audio_21021425" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21021425_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
