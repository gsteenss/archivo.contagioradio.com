Title: Guatemala ha perdido más de 26 mil hectáreas de bosques
Date: 2016-12-06 20:31
Category: Ambiente, El mundo
Tags: Guatemala, incendios forestales
Slug: guatemala-ha-perdido-mas-26-mil-hectareas-bosques
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/incendios-forestales1-e1481074015633.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [CB24 Noticias Centroamérica]

###### [6 Dic 2016]

En Guatemala el cambio climático y las actividades ilícitas han hecho que el país haya perdido **26.273 hectáreas de bosques, debido a los 598 incendios forestales** entre octubre del 2015 y septiembre de este año, según informó el director del Sistema Nacional de Prevención y Control de Incendios Forestales, SIPECIF, Jimmy Navarro.

De acuerdo con Navarro, el departamento de Petén resultó ser el más perjudicado con 15.461, seguido de Quinché al oeste de Guatemala con 1.814 hectáreas; Zacapa al este con 543; Baja Verapaz al norte con 1.330 hectáreas; y por último Chiquimula al este con 1.240 hectáreas incendiadas.

Gran cantidad de los incendios forestales serían generados por la **quema de terrenos para la siembra y la ganadería extensiva**. Además el cambio climático generó largas temporadas de sequía y la incursión del narcotráfico también produjo varios de esos incendios que acabaron con miles de hectáreas de los bosques de Guatemala.

De acuerdo con denuncias de ambientalistas, el crimen organizado está utilizando territorios de la selva para la ganadería ilegal, también los usa para la **construcción de pistas de aterrizaje clandestinas, entre otras actividades ilícitas.**

Frente a esta situación, el Consejo Nacional de Áreas Protegidos presentó una campaña de prevención en la que se incluye el despliegue de 260 bomberos forestales y otras actividades para concienciar sobre la importancia de proteger los bosques.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
