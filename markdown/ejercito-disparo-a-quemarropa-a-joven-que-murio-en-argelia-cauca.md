Title: "Ejército disparó a quemarropa" a joven que murió en Argelia, Cauca
Date: 2015-11-20 13:33
Category: DDHH, Nacional
Tags: Argelia, Cacua, DDHH, DIH, Ejército Nacional, el mango, erradicación de coca, ESMAD, Ever Molano, FARC, Juan Manuel Santos, proceso de paz, Sinaí
Slug: ejercito-disparo-a-quemarropa-a-joven-que-murio-en-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Argelia-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Orlando Bolaños] 

<iframe src="http://www.ivoox.com/player_ek_9456609_2_1.html?data=mpmimJuUfY6ZmKiak5qJd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmps%2BSpZiJhZrmxM7h0ZDIrdTkwteSpZiJhpSfwpDe18rRpdPm0NXOh5eWb8Kfy9Tjx9OPtdbZjNLi1M6Jh5SZo5iYx9ORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Orlando Bolaños] 

###### [20 Nov 2015] 

**"La situación se complica minuto a minuto”** cuenta Orlando Bolaños, habitante del municipio de Argelia, Cauca, donde en la madrugada del viernes, nuevamente helicópertos de las fuerzas militares sobrevolaron la zona, generando pánico en los pobladores, que el jueves tuvieron que sufrir los ataques por parte de **más de 2 mil efectivos del ESMAD, la policía y el Ejército,** que dejaron como saldo 5 personas heridas y una muerta, según la información que se ha podido confirmar, aunque se cree que la cifra podría ser más alta.

Aunque a través de diversos medios el secretario de Gobierno del municipio caucano, Éver Molano dijo que desconocen de dónde provinieron los disparos que le causaron la muerte al joven campesino de 20 años Miller Bermeo Acosta, según la información de Bolaños, **"El Ejército es el que disparó, el problema ha sido con ellos, los pobladores lo saben",** expresa el habitante quien añade que los militares **“le han tirado plomo a la gente (…)La gente es testigo cómo el ejercito les disparó a quemarropa”**, refiriéndose al joven integrante de Marcha Patriótica asesinado y a otros heridos.

En el corregimiento del Sinaí, ya se tiene la orden de evacuación de niños, niñas y ancianos. Sin embargo, aunque las familias ven la clara necesidad de abandonar sus casas por la situación, el **bloqueo en las vías que mantiene la fuerza pública** ha impedido que las personas puedan desplazarse hacia las afueras del municipio.

**“A toda hora hay nuevos reportes de disparos, la gente no duerme, estamos angustiados, está totalmente bloqueado el municipio, no pueden ni desplazarse las ambulancias"**, relata Bolaños, a quien le preocupa que desde los altos mandos de la fuerza pública, se ha anunciado que la erradicación de coca continúa pese las consecuencias que han dejado, y teniendo en cuenta que los campesinos protestan porque este tipo de erradicaciones se habían suspendido por orden del presidente Juan Manuel Santos, con el fin de lograr un acuerdo consensuado frente al tema, ya que **los cultivos de coca son el único medio de sustento de cientos de familias de esa región.**

Por su parte, desde diversas organizaciones sociales y defensoras de DDHH, se emitió un comunicado por medio del cual se hace un llamado urgente al gobierno nacional para que **“cesen las violaciones de DDHH e infracciones al DIH en contra de la población civil** en el municipio de la Argelia y ordene el cese de la operación militar en dicho municipio de modo que se distensione y cese la situación”.

Así mismo señalan, “Hacemos un l**lamado a la guerrilla de las FARC-EP a que ante esta situación no se genere una ruptura del cese unilateral** en el departamento del Cauca como tampoco en otras zonas del país, a pesar del conocido copamiento de fuerzas militares y de paramilitares que se ha viene denunciado” [(Ver el comunicado completo)](http://justiciaypazcolombia.com/pronunciamiento-con-caracter)

Por el momento, los pobladores del municipio continúan esperando la presencia de autoridades nacionales y de organismos internacionales que logren detener esta situación pues como expresa el vocero de la comunidad “si las cosas continúan así en la tarde estaremos contando más muertos”.
