Title: Congresistas de EEUU le exigen a Santos proteger a líderes sociales
Date: 2017-12-22 13:38
Category: DDHH, Nacional
Tags: asesinato de líderes sociales, Congreso de Estados Unidos, Juan Manuel Santos, lideres sociales, protección a líderes sociales
Slug: congresistas-eeuu-exigen-proteger-a-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/líderes-rueda-de-prensa-2-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [22 Dic 2017] 

Siete congresistas de los Estados Unidos, le enviaron una carta al presidente Juan Manuel Santos manifiestando su **preocupación por la grave situación que viven los líderes sociales en el país**. Piden explicaciones sobre la ausencia en la implementación de las medidas necesarias para proteger la vida de los defensores y llaman la atención en torno a que esas medidas deben ser concertadas y no impuestas.

Los integrantes del Congressional Black Caucus le solicitaron al Gobierno Nacional que **re evalúe las medidas de protección** que se le están otorgando a los líderes sociales y que trabaje en conjunto con diferentes organizaciones sociales debido a que "el mundo está viendo como han venido incrementando el número de asesinatos de líderes de comunidades campesinas, indígenas y afro en el país".

Además, indicaron que es necesario que se realicen las acciones expuestas por el procurador **Fernando Carrillo Flórez**, quien el 14 de diciembre alertó sobre la situación que están viviendo las personas reclamantes de tierras y defensores de derechos humanos, por ejemplo, en Chocó. (Le puede interesar: ["32 líderes sociales han sido asesinados durante 2017 en Cauca"](https://archivo.contagioradio.com/aumenta-preocupacion-por-asesinato-de-lideres-sociales-en-el-cauca/))

### **Casos que resaltan los congresistas** 

Si bien los legisladores manifestaron su preocupación por la situación general que están viviendo los líderes sociales, hicieron énfasis en los asesinatos de **líderes afro colombianos** que hacen parte de los concejos comunitarios especialmente en la Larga Tumaradó en Chocó. Resaltan que la región del Bajo Atrato chocoano se registraron, este año, los asesinatos de Porfirio Jaramillo, Jesús Alberto Sánchez, Mario Castaño y Hernán Bedoya.

Además, alertaron sobre los obstáculos que tienen que enfrentar las personas que están amenazadas de muerte cuando requieren medidas de protección. Informan que ha habido casos en donde **la Unidad Nacional de Protección ha reducido los esquemas** de protección, aumentando el peligro para estas personas. “A Emigdio Pertuz, líder afrocolombiano, le redujeron las medidas de protección de un vehículo con escolta a un chaleco antibalas y un celular”, manifestaron. (Le puede interesar: ["MinDefensa pone en riesgo la vida de los líderes sociales: Somos Defensores"](https://archivo.contagioradio.com/min-defensa-expone-la-vida-de-lideres-sociales-somos-defensores/))

Indicaron también que no hay avances significativos en las diferentes investigaciones como en el caso de **Bernardo Cuero**. “han pasado seis meses desde que fue asesinado Cuero y aún no hay información disponible para clarificar este crimen”, “hay luz verde para que los perpetradores sigan cometiendo crímenes”.

### **Congresistas solicitan respuestas a 3 solicitudes fundamentales** 

De acuerdo con Gimena Sánchez, coordinador de WOLA, lo primero que solicitan los congresistas es que se establezca por qué a los líderes sociales, en especial los de Chocó, **no se les están dando las medidas de protección que solicitan**. “Estas medidas deben proteger las vidas y garantizar el trabajo comunitario” indican. Además, hacen referencia a los casos de Amauri Antonio Arteaga y Henry Chaverra Robledo, líderes de Belén de Bajirá en Chocó quienes han solicitado medidas de protección.

Como medida urgente, los congresistas exigieron que se fortalezcan las medidas de **autoprotección y protección colectiva** que garantizan los derechos a la vida, la integridad y la libertad. Para esto “el Gobierno debe tener un diálogo con las comunidades para fortalecer procesos como las guardias indígenas y cimarronas” atendiendo las características propias de cada región. (Le puede interesar: ["Víctimas piden renuncia del ministro de defensa por afirmaciones sobre asesinatos"](https://archivo.contagioradio.com/victimas-piden-renuncia-del-ministro-de-defensa-por-afirmaciones-sobre-asesinatos/))

“En las regiones que son muy calientes, no es apropiado que los líderes utilicen chalecos anti balas como medidas de protección”, afirmaron los congresistas. También llamaron a que **se tengan presentes la inclusión étnica y cultural** teniendo en cuenta el capítulo étnico de los acuerdos de paz. Debe haber un entrenamiento por parte de la UNP a los integrantes de las minorías étnicas para que sean contratados como guardaespaldas para brindar medidas de protección y confianza en las comunidades.

<iframe id="audio_22804164" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22804164_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
