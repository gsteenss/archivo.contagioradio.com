Title: "Apelación de Peñalosa solo busca desacreditar a la ETB": Sintratelefonos
Date: 2017-07-10 18:12
Category: Economía, Nacional
Slug: juzgado-adminsitrativo-tumba-la-venta-de-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/cacerolazo-etb-16.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [10 Jul 2017]

Tras la decisión del Juzgado cuarto, que definió la nulidad de la venta de ETB, Empresa de Telécomunicaciones de Bogotá**,** la admnistración de Peñalosa señaló que iniciará un proceso de apelación **que para Sintrateléfonos solo búsca generar más desprestigio sobre los ingresos de la empresa. **

El juzgado habría tomado una decisión definitiva en torno a una acción de tutela interpuesta en el mes de Mayo por la bancada del Polo ante el Concejo de Bogotá, Sintrateléfonos y ATELCA, que habrían presentado 3 demandas que el juzgado decidió unificar, y que **solicitaban la suspensión de la venta de la ETB**, en cuanto la etapa de enajenación de la empresa.

En ese momento el **juzgado decidió que se debía frenar la primera etapa de la venta de la empresa, como medida cautelar**, mientras se daba un pronunciamiento de fondo en torno la tutela interpuesta. [Lea también: Juzgado Administrativo frena temporalmente la venta de la Empresa de Teléfonos de Bogotá.](https://archivo.contagioradio.com/juez-ordena-suspension-temporal-la-venta-la-etb/)

De acuerdo con Leonardo Argüello integrante de Sintrateléfonos, el juzgado habría decidido proteger los derechos fundamentales de los trabajadores y de la ciudadanía en general, dado que la empresa reporta **ganancias que favorecen al sistema educativo del distrito** además de regular los costos por los servicios de telefonía fija, celular y señal de televisión digital. [Lea también: Asi fue el procedo para frenar la venta de la Empresa de Teléfonos de Bogota.](https://archivo.contagioradio.com/?s=etb)

Frente a la decisión de la Alcaldía de apelar a la decisión del Juzgado, Argüello argumentó que esta solo busca confundir a la ciudadanía sobre el presupuesto de la ETB, que ha sido víctima de los malos manejos administrativos de la empresa que ha venido "desacreditando sus servicios"**. Será el Tribunal Administrativo el que tome una decisión en derecho sobre la apelación y establecerá si mantiene o no la sentencia del Juzgado**.

<iframe id="audio_19753425" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19753425_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
