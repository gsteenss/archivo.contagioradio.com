Title: "Che" abre debate en la Universidad Nacional
Date: 2016-10-21 17:05
Category: Nacional
Tags: Che guevara, Movimiento estudiantil Colombia, Universidad Nacional
Slug: che-abre-debate-en-universidad-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/che.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Semana] 

###### [21 de Oct 2016] 

La pintura del **"Che" Guevara**, en la plaza central de la Universidad Nacional en Bogotá,  reabrió el debate frente a los símbolos que identifican estudiantes de esa institución, luego de haber sido borrado el 18 de octubre de la fachada del auditorio León de Greiff, lugar que ocupo intermitentemente durante 35 años y que con el pasar del tiempo **se ha convertido en una disputa entre la memoria y el cambio.**

### **Los argumentos en torno al "Che"** 

Por un lado los estudiantes que consideran importante que permanezca la imagen en la Plaza, aluden a la memoria, puesto que **este símbolo fue pintado por primera vez por los hermanos Sanjuán, líderes del movimiento estudiantil, desaparecidos el 8 de marzo de 1982 por el F-2**, a partir de ese momento pintar el **“Che”** se había convertido en un acto reivindicativo de la memoria de los estudiantes desaparecidos.

Por  otro lado, se encuentran los estudiantes que no entienden por qué está la figura del **“Che**” en la Plaza Santander y que no se ven identificados con esta imagen. Ellos aluden a que **otras figuras podrían ser más representativas o incluso  llegan a proponer que ninguna imagen debería estar en esta pared.**

Sin embargo, quienes borraron al **“Che”** aún no han presentado el motivo que los llevo a quitarlo. Por ahora de acuerdo con Susan Duque, estudiante de "la nacho", se debe abrir el debate y escuchar las diversas opiniones que hay, no solamente llenar de significado la pintura del “Che”, sino **pintar los rostros de quienes podrían representar al estudiantado**. Le puede interesar:["Estudiantes exigen mantener acuerdos de paz entre FARC y Gobierno"](https://archivo.contagioradio.com/estudiantes-exigen-mantener-acuerdos-de-paz-entre-farc-y-gobierno/)

<iframe id="audio_13422495" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13422495_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
