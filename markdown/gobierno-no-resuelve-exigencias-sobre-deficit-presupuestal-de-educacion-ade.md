Title: Gobierno no resuelve exigencias sobre déficit presupuestal de Educación: ADE
Date: 2017-06-08 12:46
Category: Educación, Nacional
Tags: Docentes movilización, Paro FECODE
Slug: gobierno-no-resuelve-exigencias-sobre-deficit-presupuestal-de-educacion-ade
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/grantomabogotacut-e1496772769832.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CUT] 

###### [08 Jun 2017] 

**“Vamos a empezar a radicalizar la lucha”** esa es la afirmación de William Agudelo presidente de la Asociación Distrital de Educadores, luego de que el presidente Santos manifestará que se descontarán los días de trabajo a los docentes que continúen en paro.

De acuerdo con Agudelo, el paso siguiente para la movilización de los docentes será “**buscar la manera de llevar a padres de familia y estudiantes a las movilizaciones, y optar por el bloqueo de vías"**. De igual forma expresó que las “amenazas” de Santos pretenden amedrentar a los maestros y que por el contrario lo que ha logrado es generar una manifestación mucho más fuerte y unida por parte de este gremio.

En la mesa de conversaciones entre FECODE y el Ministerio de Educación, el punto que ha impedido que se levante el paro, es el aumento del monto para el Sistema general de participaciones. Mientras que FECODE propone que se realice un aumento gradual del **4.2% a un término de 10 años, el Ministerio de Educación se mantiene en decir que ese aumento no garantiza la sostenibilidad fiscal del país**.

Sin embargo, FECODE ha sido enfática en afirmar que ese aumento del 4.2% permitiría superar el déficit de presupuesto para la educación que ronda los 700 mil millones de pesos. Le puede interesar:["Esperamos que Estado invierta en Educación los recursos que se necesitan"](https://archivo.contagioradio.com/fecode-acepta-a-procurador-general-como-facilitador-en-las-negociaciones/)

“El gobierno Nacional en estos momentos no está dando los dineros correspondientes para continuar en la formación de estudiantes, se **habla de una jornada única que no tiene alimentación para los estudiantes, sin infraestructura**, sin transporte ni salario para los educadores, estamos es en la pelea por el presupuesto” afirmo Agudelo. Le puede interesar: ["Hay propuestas pero el gobierno no tiene voluntad de negociar: FECODE"](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/)

Referente a la retoma de los diálogos con el Ministerio de Eduación, Agudelo señaló que consideran que **esa posibilidad con la ministra Yaneth Giha ya se agotó**, razón por la cual esperan que ahora sea el presidente Santos el que se siente en la mesa de diálogos y de soluciones a las exigencias de los docentes.

<iframe id="audio_19158281" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19158281_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
