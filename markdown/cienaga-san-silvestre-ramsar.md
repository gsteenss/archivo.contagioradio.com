Title: Ciénaga de San Silvestre podría ser parte de la lista Ramsar
Date: 2017-10-25 17:18
Category: Ambiente, Voces de la Tierra
Tags: Ciénaga de San silvestre, Ministerio de Ambiente
Slug: cienaga-san-silvestre-ramsar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Navegando-Río-Magdalena-desde-Mompox-hacia-Magangué.-Foto-Bunkerglo-e1508970335580.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [25 Oct 2017] 

La Ciénaga de San Silvestre, en Barrancabermeja, podría ser parte de la Lista Ramsar, un título que serviría para garantizar la protección de esta zona, afectada por la construcción de un Relleno Sanitario que la contamina con 1700 litros de lixiviados diarios y la extracción de agua por parte de ecopetrol. **Sin embargo deben darse medidas de protección integrales para acabar con el riesgo de manera eficaz, afirman ambientalistas.**

Oscar Sampayo, integrante del Grupo de Estudios Extractivos Ambientales del Magdalena Medio, expresó que lo más importante de que la Ciénaga haga parte de esta lista sería la protección contra las actividades de ECOPETROL que gastan el agua, y **que exista un compromiso real por parte del Ministro de Ambiente sobre aumentar la protección de las áreas naturales en Colombia.**

 “Valoramos que se haya manifestado establecer que la Ciénaga este bajo Ramsar, pero la cuestión politiquera es la que nos hace mirar la noticia con cautela” afirmó Sampayo y agregó que lastimosamente en otros lugares del mundo zonas en alto riesgo ya fueron declaradas dentro de esta lista y **continúan siendo explotadas o dañadas, como el caso de la Laguna de Ponte, en Campeche, México.**

Actualmente las especies que viven en la ciénaga hacen parte de la lista de los animales que se encuentran en vía de extinción, tal es el caso del jaguar, del manatí antillano, tortugas y otros. (Le puede interesar: ["Gobierno esta con las multinacionales al frenar consultas populares: R. García"](https://archivo.contagioradio.com/gobierno-esta-las-multinacionales-al-frenar-consultas-populares-r-garcia/))

### **Las amenazas sobre la Ciénaga San Silvestre** 

Sampayo señaló que sobre la Ciénaga de San Silvestre no solo hay graves afectaciones causadas por el relleno de basuras, además hay tres bloques petroleros, el Bloque Llanito, el Bloque Lizama y el Bloque Centro y actualmente hay un estudio para otorgar una licencia de exploración que está sobre el 70% de esta área protegida.

Esta Ciénga produce el agua que llega a Barrancabermeja y otros municipios de Santander y actualmente recibe 1.700 litros de lixiviados que van a llegar a todos los afluentes. **De igual forma Sampayo denuncio que el líquido transporta metales perjudiciales** para la salud de las personas y que finalmente estas terminan consumiendo el agua. (Le puede iteresar: ["Aprobada consilta popular en El Castillo, Meta"](https://archivo.contagioradio.com/aprobada-consulta-popular-en-el-castillo-meta/))

<iframe id="audio_21692152" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21692152_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
