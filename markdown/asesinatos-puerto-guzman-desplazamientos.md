Title: Asesinatos en Puerto Guzmán comienzan a generar desplazamientos forzados
Date: 2020-02-13 15:04
Author: CtgAdm
Category: DDHH, Nacional
Tags: Puerto Guzmán
Slug: asesinatos-puerto-guzman-desplazamientos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Puerto-Guzmán-desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto de referencia Puerto Guzmán : CRIC

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/wilmar-madronero-defensor-dd-hh-del-putumayo_md_47745240_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Wilmar Madroñero | defensor de DD.HH. del Putumayo

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

La tarde del 12 de febrero, organizaciones d'e DD.HH. de [Putumayo](https://www.justiciaypazcolombia.com/?s=putumayo)informaron sobre el hallazgo de los cuerpos sin vida de **Luis Alberto Parra Lozada, fiscal de la Junta de Acción Comunal (JAC) de la vereda Villa Fátima y su hijo Jáder Alberto Parra, coordinador del Comité de Obras de la JAC**. Los dos hombres habían sido secuestrados el pasado 10 de febrero por desconocidos que llegaron hasta su casa en Puerto Guzmán, municipio donde se ha conocido el asesinato de al menos ocho personas en lo que va corrido del 2020 fruto de disputas territoriales entre grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En la noche del lunes 10 de febrero, en horas de la noche hombres armados llegaron hasta la casa del fiscal de la Junta de Acción Comunal llevándoselo en contra de su voluntad junto a su hijo, Los sujetos dejaron a la esposa y madre de los dirigentes comunales, amarradas y se marcharon con padre e hijo con rumbo desconocido. [(Lea también: Hacer oposición en Putumayo nos cuesta la vida: diputado UP)](https://archivo.contagioradio.com/oposicion-putumayo-cuesta-vida/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante las siguientes horas, otros líderes de la zona adelantaron labores de búsqueda, sin embargo al medio día del miércoles 12, se informó que fueron hallados dos cuerpos que pertenecerían a Luis Alberto y su hijo Jader. Aunque por el momento no se conoce mayor información sobre los sucesos, organismos en defensa de DD.HH. se desplazarán al lugar de los hechos para recoger mayor información. [(Le recomendamos leer: Con asesinato del líder Yordan Tovar, continúa la violencia en Puerto Guzmán, Putumayo)](https://archivo.contagioradio.com/asesinato-lider-yordan-tovar-puerto-guzman-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las personas están empezando a abandonar sus hogares

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Wilmar Madroñero, defensor de DD.HH. del Putumayo, alerta que la situación compleja ocurrida desde inicio de año en la que cinco dirigentes han sido asesinados por diversas razones, y otros tres campesinos han sido víctimas del mismo destino, está generando un desplazamiento paulatino en las comunidades y continuas amenazas contra dirigentes que han tenido que salir del departamento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Para nosotros se trata de buscar la prevención para que el campesino siga trabajando y se quede en el territorio" afirma el defensor de DD.HH. quien estima que serían cerca de 15 o 20 familias que estarían abandonando Puerto Guzmán para dirigirse a Caquetá.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Puerto Guzmán, militarizado pero no a salvo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el mes de septiembre de 2019 a Puerto Guzmán arribó un nuevo actor armado ilegal autodenominado Mafia Sinaloa, que ha entrado en disputa territorial contra el Frente Carolina Ramírez, de las disidencias de las FARC-EP poniendo en riesgo las vidas de la población civil en el municipio donde a su vez hace fuerte presencia de tropas del Batallón Domingo Rico del Ejército Nacional y la Armada Nacional. [(Le puede interesar: La muerte se apodera de Puerto Guzmán, 8 asesinatos en dos semanas)](https://archivo.contagioradio.com/la-muerte-se-apodera-de-puerto-guzman-8-asesinatos-en-dos-semanas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Madroñero afirma que las FF.MM. hacen presencia en el departamento, resalta que no hay una situación de impacto que contrarreste la ola de violencia, **"uno no puede defender lo indefendible, hay una militarización en la zona pero están matando en las zonas y esa es la realidad".**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
