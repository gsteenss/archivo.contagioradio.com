Title: Senado le dice No al fracking pero todavía no se puede cantar victoria
Date: 2020-09-03 19:01
Author: AdminContagio
Category: Actualidad, Nacional
Tags: artículo 210, Ley de regalías, No al Fracking
Slug: senado-le-dice-no-al-fracking-pero-todavia-no-se-puede-cantar-victoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/WhatsApp-Image-2018-03-26-at-12.01.58-PM-1-e1544654159516.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Con 44 votos a favor y 39 en contra, la plenaria eliminó el artículo que para muchos, buscaba otorgar incentivos tributarios para las empresas que realizan actividades de extracción de hidrocarburos de yacimientos no convencionales a través de la técnica de fracking.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta decisión fue celebrada por diversos sectores como una forma de seguir protegiendo al país en la lucha contra el fracking. Por su parte, el representante a la Cámara, Wilmer Leal expresó a través de su twitter “esperamos que quede por la vida y el ambiente”.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Wilmerlealp/status/1301377666067632128","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Wilmerlealp/status/1301377666067632128

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Esta iniciativa había sido aprobada por la Cámara de Representantes el pasado 28 de agosto.** Sin embargo esta decisión fue cuestionada por varios sectores, ya que la propuesta burlaba la decisión en que se había suspendido esta actividad en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Anteriormente, Jorge Enrique Robledo se había [pronunciado, señalando](https://twitter.com/JERobledo/status/1295441975844450304) "este no es un proyecto de ley para determinar las condiciones de la producción de hidrocarburos, ni para determinar cuántas son las regalías; este es un proyecto de ley para distribuir regalías, y este artículo no trata de eso, sino de condiciones para los que extraigan hidrocarburos no convencionales, o sea, fracking". (Le puede interesar: [No al fracking: Deuda de campaña que Duque debe cumplir](https://archivo.contagioradio.com/no-al-fracking-deuda-de-campana-que-duque-debe-cumplir/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Senado y Cámara deben llegar a conciliación 
--------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Debido a que en Cámara se aprobó el artículo, en los próximos días este pasará a conciliación con dos textos presentados tanto por Cámara como por el Senado. Un grupo de conciliadores escogerá alguno de los dos textos y este entrará nuevamente a votación. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así, muchos aclaran que esta primera decisión no es una victoría y que la lucha no ha finalizado; "hay que ponerle mucha atención a la conciliación", expreso el senador Gustavo Petro a través de su twitter. (Le puede interesar: [NO al fracking dicen Concejales y Diputados del Magdalena Medio](https://archivo.contagioradio.com/no-al-fracking-dicen-concejales-y-diputados-del-magdalena-medio/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ColombiaNoFrack/status/1301575981451358211","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ColombiaNoFrack/status/1301575981451358211

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La senadora por el Partido Verde, Angélica Lozano, quien repetidas veces a expresado su compromiso a "No al Fracking", celebro la decisión y explicó a través de su twitter lo que seguía en el proceso.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AngelicaLozanoC/status/1301386913996308486","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AngelicaLozanoC/status/1301386913996308486

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?time_continue=5\u0026amp;v=Rgh1I2wehVU\u0026amp;feature=emb_title","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?time\_continue=5&v=Rgh1I2wehVU&feature=emb\_title

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->
