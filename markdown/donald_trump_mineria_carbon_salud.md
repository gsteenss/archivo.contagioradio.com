Title: Gobierno Trump impide investigación sobre efectos de la minería de carbón en la salud
Date: 2017-08-24 20:00
Category: Ambiente, El mundo
Tags: Donald Trump, Estados Unidos, Mineria
Slug: donald_trump_mineria_carbon_salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/trump_mineria-e1503622704446.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Getty 

###### [24 Ago 2017] 

El **Ministerio del Interior ha impedido que se siga trabajando en una investigación** que se estaba llevando a cabo sobre los impactos de la minería de carbón en la salud, lo que se ha calificado como una nueva arremetida del gobierno de Trump contra las políticas en defensa del ambiente o de la salud, versus políticas ambientales.

Se trata de una decisión que va en la misma vía de sus propuestas de campaña referentes a proteger el sector minero. El estudio lo adelantaba la Academia Nacional de Ciencias, Ingeniería y Medicina. Estaba financiado con recursos públicos y se había iniciado en el último año del gobierno de Barack Obama.

La investigación había iniciado como propuesta de la **comunidad del Estado de Virginia,** quienes habían solicitado que se verificara si existían o no riesgos para la salud asociados a la actividad minera de carbón a cielo abierto que se realiza en esa zona desde 1970.

### **La notificación** 

Para dicha investigación se contaba con un panel de 12 expertos que el pasado 18 de agosto recibieron una notificación desde el Ministerio del Interior, mediante la cual se ordenaba **detener el desarrollo del estudio debido a que era probable que no siguiera recibiendo financiación,** pues no se encontraba entre los planes del gobierno Trump.

“La Academia Nacional de Ciencias, Ingeniería y Medicina considera que este es un estudio necesario e importante y estamos atentos para continuar en cuanto el Ministerio termine la revisión de su presupuesto”, señalaron en un comunicado.

Para la Academia, resulta importante hacer el estudio, pues por cuenta de la actividad minera el ecosistema conformado por cientos de montañas, en que habitan las **comunidades Apalache los pobladores han presentado constantes enfermedades respiratorias, como el cáncer de pulmón** y que podrían ser producto de la contaminación causada por la mina, como lo relata The New YorK Times, y la revista *Science en 2010.*

“La evidencia científica del **severo impacto en el ambiente y en la salud humana es fuerte e irrefutable**”, había concluido Margaret Palmer, científica de la Universidad de Maryland, quien señalaba en los estudios que “Los efectos de esta práctica minera son irreparables y duraderos, además ninguna técnica de mitigación puede reparar los daños”.

Mientras la Asociación Nacional de Minería señala que está más que justificada la decisión, calificando la investigación como innecesaria,  el congresista Raúl M. Grijalva, considera que se trata de "**una estrategia para detener a la ciencia y para mantener al público en la oscuridad** respecto a los riesgos para salud que representa la industria minera. Es un favor que le hacen a esa industria, simple y llanamente”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
