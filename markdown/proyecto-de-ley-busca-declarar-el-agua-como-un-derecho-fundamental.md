Title: Congresistas buscan declarar el agua como derecho fundamental
Date: 2016-03-16 11:44
Category: Otra Mirada, Política
Tags: Congreso de la República, derecho al agua
Slug: proyecto-de-ley-busca-declarar-el-agua-como-un-derecho-fundamental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Agua-e1458146372649.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Taringa 

###### [16 Mar 2016]

Los senadores de la Alianza Verde, Jorge Prieto y Antonio Navarro Wolf junto a la congresista Sofía Gaviria del Partido Liberal, y el represente del Polo Democrático Alternativo Víctor Correa, presentan en el Congreso de la República un proyecto que tiene como objetivo declarar el uso y disfrute del agua como un derecho fundamental, lo que a su vez incluiría **la prohibición de todo tipo de actividad minera, ganadera y agrícola donde se puedan ver afectadas las fuentes hídricas del país.**

“El proyecto es una reforma constitucional que busca **incluir en la lista de derechos fundamentales el derecho fundamental al agua de manera autónoma,** es decir, que no está vinculado de forma no conexa a otros derechos, pero también busca dotar al recurso de mecanismos de exigibilidad o de garantía para su protección como lo serían las acciones de tutela y demás acciones constitucionales encaminadas a su protección”, explica el representante Correa.

Pese a que Colombia es el segundo país de América Latina con mayor cantidad de recursos hídricos, en estos momentos escasea el agua por cuenta del Fenómeno de El Niño, pero también por el poco control que hay por parte del Estado en el manejo del agua que tienen las empresas nacionales y las multinacionales en sus proyectos minero-energéticos.

Por ejemplo en Mapiripán, Meta, **[la empresa italiana palmera Poligrow gasta 1'.700.000 litros de agua diarios por plantación](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/)**. Así mismo, en Putumayo, la petrolera **Amerisur r**ealiza explotación de crudo en medio de un humedal que representa el 50% de la zona de reserva campesina Perla Amazónica,  afectando el recurso hídrico de la comunidad, **pues esta empresa bota agua industrial a los caños de los que se abastece la población.**

De acuerdo con Víctor Correa, la iniciativa define el  agua como “el derecho de todos a disponer de agua suficiente, salubre, aceptable, accesible y asequible para el uso personal y doméstico. El proyecto se construye como una forma de continuidad de procesos como el del referendo por el agua y todos aquellos que buscaban una defensa del recurso, que por su estrecha vinculación con la dignidad humana, no se podría considerar simplemente como un bien público, sino que trasciende esa dimensión para configurarse como un derecho desde una perspectiva social, económica y cultural”.

Por su parte, el senador Jorge Prieto, asegura que  “**el agua debe estar por encima de los interés del petróleo, la agroindustria y de cualquier otra actividad.** Si no se cuidan las fuentes hídricas inevitablemente nos quedaremos sin fuentes de agua y por eso es necesario cuidarlas para poder tener agua en el futuro”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
