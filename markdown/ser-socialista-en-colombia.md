Title: Ser socialista en Colombia
Date: 2017-10-20 10:17
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Cuba, Rusia, Socialistas
Slug: ser-socialista-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/mano-arriba-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo - Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

[Qué difícil es ser socialista en Colombia porque en principio, no existe un consenso sobre lo que significa ser socialista. Se supone que la academia tendría que resolver el enigma de lo que significa ser socialista, pero tal parece que ningún maestro luego de explicar qué significa socialismo se declara socialista.... envolviendo al estudiante en un mar de duda tremenda.]

[Las ideas sobre qué significa ser socialista, están sostenidas por un falso consenso... pues en Colombia la mayoría define socialismo “en negativo” y muchos pretenden mostrarse con una idea clara sobre el concepto siendo que sus fuentes provienen de lo que]*[más o menos]*[ se ha dicho en los medios de información o las redes sociales.]

[Ser socialista en Colombia es difícil porque en el país del liberalismo político, en el país del liberalismo económico, en el país de la libre expresión, considerarse socialista puede ser de una parte un atentado al “sentido común”, es decir a la estabilidad de la democracia como régimen. De otra parte, puede ser considerado como la peor estupidez que alguien se puede imaginar, pues al no conocer qué significa ser socialista, al estar definido siempre “en negativo”, automáticamente excluir al que no piensa igual, refuerza la tradición política en Colombia.]

[Ser socialista es un problema en Colombia porque antes de que cualquiera pretenda explicar las razones del por qué es socialista, las voces airadas diciendo “vete a Venezuela”, “corre a Cuba”, “vete a Corea del Norte”, emergen no como sugerencias, sino como acusaciones, sindicaciones en medio del más elemental de los problemas de ser socialista, que consiste en que quien lo rechaza, no sabe qué significa ser socialista, sino]*[más o menos]*[ cree saber qué es.]

[Sumado a lo anterior, en Colombia se ha configurado la peor falacia de todas: gente que se considera "a-política" ..., este status lo ha impuesto sin necesidad de la fuerza el régimen político colombiano, lo ha hecho básicamente naturalizando la idea de que la política lo único que hace es “estorbar a la economía" pues parte de tener “sentido común" implica no criticar el modelo económico, consumir todo lo que se pueda, y no andar hablando política.  Por tanto, el liberalismo convence a sus fieles, pero incautos seguidores, de que “hablar de lo político" se trata de hablar solo en torno al consenso de temas que proponen los medios masivos, o incluso a las estupideces que algunos funcionarios trinan por twitter.]

[Por tanto, el régimen político colombiano, por muy democrático que se autodenomine, se instala en el imaginario colectivo como un sistema neutro, como positivo, como objetivo..., tan neutro, tan objetivo que ni siquiera es capaz de definir a la derecha, sino que la llama centro democrático.]

[Ser socialista en Colombia es un problema porque un socialista tiene un propósito claro implícito en su práctica política: desmantelar la falsa neutralidad, desenmascarar la falsa objetividad de un régimen político que convence mostrándose neutral o “libre”. Por tanto, cuando se quiere desmantelar esa falsa neutralidad, entonces aquellos individuos que siempre celebran al régimen colombiano por ser libre, se ofenden al escuchar la diferencia o se ofenden cuando se les cuestiona la objetividad desde la que tranquilamente hablan, dando como resultado que cuando les faltan los argumentos, vuelven los litigios una ofensa personal, para, a como dé lugar, negar una razón objetiva y seguir subjetivizando su posición claramente ideologizada.  ]

[Ser socialista en Colombia es un problema porque a los que sindican de socialistas los tratan como dinosaurios; claaaro no importa que el neoliberalismo esté sembrado en los postulados de un conservador calvinista presbiteriano como Adam Smith que escribió incluso antes que Carlos Marx..., si quisiéramos apelar a la objetividad ¡dinosaurios todos!]

[Si quisiéramos apelar a la objetividad tendríamos que reconocer que el autoritarismo, el totalitarismo, las dictaduras, por lo general, las están definiendo los medios masivos de información; por tanto, el individuo promedio es una caja de resonancia que no se ve menos estúpida porque se diga "apolítico" si repite todo lo que le ponen a repetir.]

[Los medios masivos, de acuerdo a su tamaño de influencia, tienen el poder de conmover a la audiencia por una bomba terrorista que estalla en París, así como tienen el poder de no conmover a nadie con una bomba terrorista que estalla en Damasco; pueden hacer ver en una inmolación un acto cruel, pero en un bombardeo norteamericano un acto justo.]

[Ser socialista es un problema en Colombia porque ser socialista significa el negativo. Significa la oposición, la oposición a la tradición de los partidos políticos que quisieron convencer incautos en el 91 fundando nuevas empresas (partidos) que hoy están manejadas por las mismas clases políticas de siempre; ser socialista es la oposición a la tradición de mostrar un régimen político como neutral cuando en realidad nunca lo ha sido, ser socialista implica reconocer que los medios masivos tienen libertad de expresión pero no la única percepción política; ser socialista implica tener una fórmula política y económica que subvierte la forma política y económica que se ha venido manejando en Colombia desde siempre.]

[**Ser socialista** en Colombia significa que te opones a que las cosas sean definidas por la percepción y luchas porque sean definidas por lo que realmente son, pues de la percepción son dueños los medios masivos de información. Ser socialista en Colombia no significa fidelidad a un partido, significa que toda tu vida, todos tus actos, todo componente de tus propósitos están en contra del poder que domina sea el mundo o el país.]

[**Ser socialista** implica que toda tu moralidad subvierte las formas en que el liberalismo te acostumbró a odiar, a amar, a indignarte; ser socialista significa que logras ver más allá de los poderes dominantes que como una red controlan el pensamiento de las masas.]

[Por supuesto, ser socialista es un problema en Colombia porque cuando entiendes lo anterior, entonces luchas, y si luchas, puedes perder amigos por causa de tu pensamiento político, puedes enfrentarte a tu propia familia, a esa fuerza impresionante de un poder que al establecerse sin la fuerza se enquista en la biología misma, en la intimidad de las relaciones sociales.  ]

[**Ser socialista** es un problema, porque si el socialismo es lo negativo y el régimen democrático colombiano es lo positivo y lo neutro, entonces alguien que opine contra una protesta social será considerado "normal'', y alguien que opine a favor de la protesta entonces será considerado como "mamerto''..., si es así, entonces ¡mamertos todos! porque no se podría pasar por alto el hecho de que la mayoría de los problemas que se presentan por opiniones políticas emergen porque la gente opina sobre situaciones que no presencia, que no vive, sino que escucha o lee, cayendo en el error típico de pretender resolver desde el escritorio lo que tiene que resolverse en el territorio, cayendo en el absurdo de creer que libertad de expresión es lo mismo que una opinión política.  ]

[Ser socialista es darle prioridad al territorio, al contexto, a los actores sociales por encima de la percepción que ofrecen los medios sobre el problema. Ser socialista implica que el individualismo liberal debe ser definido como el eterno opuesto al colectivismo social sin que eso implique verdad absoluta, sino simplemente oposición existente.]

[Ser socialista no significa la búsqueda por la consolidación de sus tesis como la verdad revelada. Ser socialista en Colombia significa luchar contra la falsa neutralidad del régimen, significa luchar porque si a Venezuela con todos los errores que tiene, la comunidad internacional levanta la voz y la sindica de “dictadura’’, entonces con mayor razón Colombia con sus errores endémicos, su corrupción, el asesinato de personas inocentes y el fracaso de su modelo político, debería ser llamado “dictadura”.]

[Pero claro, el nominalismo no soporta el hecho de que a pesar de que en Colombia maten gente por razones políticas como en ningún otro país de América, finalmente la percepción dominante es que aquí somos libres y allá no, que aquí hay democracia y allá no; teniendo presente que ese “allá” se constituye siempre en la negación parmente de la alteridad en Colombia.]

[Ser socialista en Colombia es un camino difícil. No solo se trata de consolidar un partido, pues un partido más que por “ser socialista”, debería luchar contra la burocratización; no solo significa definir teóricamente qué es el socialismo, pues los intelectuales más que por “ser socialistas” deberían luchar contra la elitizacion de sus prácticas; ser socialista en Colombia no significa perder amigos por facebook, por twitter o por opiniones políticas, pues las opiniones deberían buscar ser sustentadas cara a cara,]**y así como mínimo reconocer que hablamos con otra persona, y no con lo que percibimos de esa otra persona.**

[Ser socialista en Colombia no se trata de tener una camiseta del Che, o criticar al que la tiene, se trata de pensarse como sujeto útil en la totalidad de la oposición al régimen, por tanto, estar dispuesto a escribir, a argumentar, a arengar, a batallar en una calle, a ir al campo, a ir al barrio, a ir al paro o apoyar a los que paran, a respirar por todos los poros la indignación por la barbarie que en unos lados es condenada pero en otros lados no; a reconocer la injusticia para que el alimento placentero que el liberalismo otorga no se convierta en la razón perezosa de no hacer nada y creer que porque nadie comprende entonces se justifica abandonar la lucha...]

[Sí, ser socialista en Colombia, significaría eso… hoy que el liberalismo y su falsa neutralidad es el poder que domina; es una lucha constante contra ese “sentido común”, significa ser lo negativo, lo que se opone, lo que ve el elemento digno donde otros ven “lo que toca”.]

[Ser socialista no es la respuesta a todos los problemas, de hecho, más parece una caja de pandora que una piedra filosofal, ser socialista significa que puedes morir, porque en Colombia el que atenta seriamente contra el “sentido común” lo matan y está comprobado;]**hoy avanza un exterminio en Colombia, solo que la percepción construida desde los medios masivos disminuye los grados de indignación.**

[Allí, batallando contra la percepción y promoviendo verdades territoriales, allí está la fuente del socialismo en Colombia, no obstante... que no se piense que un socialista es una figura modélica, no es un “súper humano”, no se trata de una razón para la composición del egocentrismo como sí del comunitarismo, de lo colectivo, de la alteridad, de la otredad,]**que lo único que pide hoy en Colombia, es existir como algo real, sin que le cueste tanto existir.**

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
