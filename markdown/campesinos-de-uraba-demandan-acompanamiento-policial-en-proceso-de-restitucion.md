Title: Campesinos de Urabá demandan acompañamiento policial en proceso de restitución
Date: 2019-05-14 18:07
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Restitución de tierras, uraba
Slug: campesinos-de-uraba-demandan-acompanamiento-policial-en-proceso-de-restitucion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Restitució-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Restituciónd de Tierras] 

Desde el 5 de abril de este año fue autorizada la restitución de 12 predios a 185 campesinos en la vereda de Guacamayas, de Turbo Antioquia, para el 14, 15 y 16 de mayo, pese a ofrecer su protección con antelación, la Policía suspendió el proceso de acompañamiento argumentando ausencia de garantías, paralizando así un proceso que para las víctimas ha tardado más de 25 años.

**Gerardo Vega, presidente de la organización Forjando Futuros**, en representación de las víctimas afirma que estaban preparados para partir hacia Guacamayas cuando fue cancelada la entrega, "l[a Policía debe garantizar el acompañamiento y la protección de la vida de los funcionarios públicos para que sean devueltas las tierras, además de garantizar la integridad de las víctimas que retornan" señalando que se trata de una situación que se ha vuelto repetitiva y que pone en duda la autoridad de la Fuerza Pública.]

> [@GerardoVegaMed](https://twitter.com/GerardoVegaMed?ref_src=twsrc%5Etfw) denuncia que la [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw) cancela el acompañamiento de la restitución de tierras de las familias de la vereda Guacamayas el día de mañana. ¿Quién controla Urabá?
>
> Esta diligencia estaba programada desde el 5 de abril del 2019 por un juzgado. [@URestitucion](https://twitter.com/URestitucion?ref_src=twsrc%5Etfw) [pic.twitter.com/vLyQkD1Guj](https://t.co/vLyQkD1Guj)
>
> — Forjando Futuros (@forjandofuturos) [13 de mayo de 2019](https://twitter.com/forjandofuturos/status/1128085388097204224?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### Responden las autoridades 

Por su parte,  la Unidad de Restitución de Tierras manifestó frente a las inquietudes generadas a raíz de este suceso, que el acompañamiento se desarrollará según información entregada por la Policía Nacional los **días miércoles 15, jueves 16 y viernes 17 de mayo de 2019.** [(Le puede interesar: Crece preocupación en Urabá por garantías de seguridad para restitución de tierras)](https://archivo.contagioradio.com/crece-preocupacion-en-uraba-por-garantias-de-seguridad-en-restitucion-de-tierras/)

### Existen sectores que se oponen a la restitución

Urabá - según el director de Forjando Futuros - es la región con mayor dificultad en términos de restitución, "asesinan animales, envenenan los cultivos, corren los cercos, amenazan, desplazan gente" agrega, afirmando que en la región se han asesinado a 20 líderes sociales reclamantes de tierras.

Desde la organización también denuncian la creación de un **"Ejército anti restitución"**, a menudo compuesto por bandas criminales o grupos paramilitares patrocinados por sectores legales e ilegales que históricamente han hecho presencia en la región como son los empresarios bananeros, ganaderos o palmicultores.

Pese a que Vega señala que es necesario que las autoridades lleguen hasta la raíz de este problema, es decir a quienes financian y patrocinan dichos grupos, no han existido avances efectivos en los resultados de las investigaciones enfocadas en estas organizaciones criminales.

Según datos aportados por el director de Forjando Futuros, en el Urabá existen **cerca de 6.300 solicitudes de restitución de las que solo 300 casos han sido resueltos** mientras a nivel nacional solo han sido otorgadas  330.000 hectáreas de las más de 6 millones de las que deben ser restituir, un proceso, según Vega que ha sido débil y que no está incluido en el actual plan nacional de desarrollo.

<iframe id="audio_35829441" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35829441_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
