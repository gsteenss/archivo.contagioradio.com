Title: En jornada de #Agroindignados comunidades indígenas exigen el cese de la estigmatización
Date: 2015-09-04 15:58
Author: AdminContagio
Category: Movilización, Nacional, Resistencias
Tags: Agroindignados, Cumbre Agraria Étnica y Popular, Gobierno, Movilización, ONIC
Slug: en-jornada-de-agroindignados-comunidades-indigenas-exigen-el-cese-de-la-estigmatizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ONIC_cumbre_agraria_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pacifista.co 

###### [Sep 4 2015] 

En el marco de la cumbre agraria se realizó la audiencia pública "**pueblos indígenas conflicto armado y paz**" que se desarrolló en el Congreso de la República y giró en torno al tratamiento del conflicto por parte de los medios de comunicación, a la justicia y al territorio.

Frente a los medios de comunicación se  hizo una crítica sobre el tratamiento de los medios empresariales a las comunidades a las cuales se les afecta y  estigmatiza a sus miembros, con programas como el de [séptimo día ](https://archivo.contagioradio.com/mas-de-4-mil-agresiones-contra-pueblos-indigenas-en-colombia-durante-2014/) y el cubrimiento a la [toma pacífica del ministerio de agricultura](https://archivo.contagioradio.com/agroindignados-se-tomaron-pacificamente-ministerio-de-agricultura/).

Otro de los temas fue la brecha tan amplia que existe entre la justicia indígena y la justicia institucional, puesto que la institucionalizada no representa los intereses, usos y costumbres de las comunidades indígenas.

Este foro se realizó en el marco de la cumbre agraria, étnica y popular que llegó a Bogotá con la intención de que se cumplan los acuerdos que el gobierno hizo en el 2013 y al día de hoy continúa incumpliendo.
