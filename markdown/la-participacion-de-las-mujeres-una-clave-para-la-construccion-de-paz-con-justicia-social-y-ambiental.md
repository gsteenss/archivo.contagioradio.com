Title: La participación de las mujeres: una clave para la construcción de paz con justicia social y ambiental
Date: 2017-03-03 08:31
Category: CENSAT, Opinion
Tags: Ambiente, Berta Cáceres, mujeres
Slug: la-participacion-de-las-mujeres-una-clave-para-la-construccion-de-paz-con-justicia-social-y-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mujeres-el-ambiente-y-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CENSAT 

###### 3 Mar 2017

#### **[Por: CENSAT Agua Viva /Amigos de la Tierra Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

#### *Es una incoherencia cosmogónica defender el territorio tierra* 

#### *si allí hay mujeres maltratadas, violentadas por el patriarcado* 

#### Lorena Cabnal[\[1\]](#_ftn1)

Este marzo se tiñe de fuerza y resistencia, pues, sumado al día internacional de las luchas de las mujeres, conmemoramos un año desde la siembra de Berta Cáceres, y hoy su legado en defensa de las aguas libres, los territorios soberanos y los liderazgos de las mujeres continúan fortaleciéndose en numerosos territorios.

De la mano del Consejo Cívico de Organizaciones Populares e Indígenas de Honduras –COPINH- Berta enfrentó el sistema capitalista-patriarcal, y aportó con su tenacidad a diversos procesos organizativos más allá de las fronteras. A pesar de esta importante labor, Berta fue criminalizada, por distintos actores, y posteriormente asesinada el 3 de marzo de 2016.

Su actuar se enmarcó en una infatigable defensa de la naturaleza y en enfrentar el modelo de dominación actual, donde los cuerpos de las mujeres y los bienes comunes son  convertidos en objetos del mercado. La lucha de Berta es un llamado a que nuestros cuestionamientos a los modelos de desarrollo basados en el extractivismo y las desigualdades sociales pasen también por un cuestionamiento de los roles de género, de la identificación y transformación de las prácticas patriarcales que impiden una participación equitativa y que deforman la construcción de poder popular.

Como Berta, en los diversos caminos de defensa de la naturaleza las mujeres hemos jugado históricamente un papel fundamental, pues a pesar de la profundización del extractivismo y la violencia que éste implica, nuestras iniciativas de resistencia se han mantenido y fortalecido. En nuestros procesos las mujeres hemos cuestionado el modelo de desarrollo, las relaciones de poder configuradas a partir del género y hemos construido propuestas de defensa territorial con base en el reconocimiento de las mujeres como sujetos políticos que históricamente hemos aportado a grandes transformaciones.

Situaciones de criminalización, como las que tuvo que pasar Berta en su camino, las enfrentan también lideresas colombianas, ya que el extractivismo implica la profundización del patriarcado, que se traduce en conductas violentas que reprimen y obstaculizan su participación activa en los espacios de toma de decisiones e incidencia política. Esta  situación se hace evidente en tiempos recientes, pues hemos sido testigos del asesinato de varias mujeres reconocidas por su trabajo para la construcción de paz, la defensa del ambiente y de los derechos humanos: María Fabiola Jiménez, Martha Pipicano, Cecilia Coicué, Adelinda Gómez, entre otras.

Ahora bien, frente a este panorama, es pertinente mencionar que también han habido esfuerzos de distintos sectores para consolidar propuestas de participación, donde las voces de las mujeres son escuchadas y tenidas en cuenta, donde se coincide con lo que afirmaba Camilo Torres Restrepo: “Insistamos en lo que nos une y prescindamos de lo que nos separa”. Entre estas numerosas iniciativas se incluyen propuestas organizativas como Fuerza de Mujeres Wayúu, Vicaría del Sur, Movimiento Ríos Vivos, Colectivo por la protección de la Provincia de Sugamuxi, AMMUCALE (Asociación Municipal de Mujeres Campesinas del Municipio de Lebrija), Movilización Mujeres Afrodescendientes por el Cuidado de la Vida y los Territorios Ancestrales (Cauca), Cinturón Occidental Ambiental, Ruta pacífica de las mujeres, COCOMACIA (Consejo Comunitario Mayor de la Asociación Campesina Integral del Atrato), el proceso de mujeres del Comité de Integración del Macizo Colombiano, y  muchas otras iniciativas históricas que desde la organización comunitaria y la articulación local y regional han dado pasos importantes en la incidencia política de y para las mujeres, y que se han enfrentado a los poderes hegemónicos a partir de la fuerza de su liderazgo y su trabajo de base.

Con respecto a lo anterior, no sobra examinar: ¿cómo la construcción de paz con justicia social y ambiental genera transformaciones en nuestras relaciones como seres humanos y con la naturaleza?. Esta pregunta, más que respuestas concretas genera retos que no sólo tienen el gobierno, las Fuerzas Armadas Revolucionarias de Colombia (FARC) y el Ejército de Liberación Nacional (ELN), sino que tenemos cada una de las organizaciones que defendemos la naturaleza y los tejidos territoriales.

Así, hacemos un llamado a que como Berta, María Fabiola, Martha, Cecilia y muchas otras personas seamos conscientes de que las luchas de las mujeres no sólo implican un cambio referente la igualdad de géneros, si no que es una apuesta por construir una sociedad armónica, incluyente, lejos de la escisión entre naturaleza y cultura, donde todas y todos tengamos voz y participación.

Finalmente, invitamos a que del 2 al 8 de marzo nos movilicemos para denunciar a los responsables de la violencia contra las mujeres y nuestras comunidades. Que desde nuestros movimientos, organizaciones y a título personal, acompañemos al COPINH, y multipliquemos su caminar.

###### [\[1\]](#_ftnref1)

#### **[Leer más columnas de opinión de  CENSAT](https://archivo.contagioradio.com/censat-agua-viva/) **

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
