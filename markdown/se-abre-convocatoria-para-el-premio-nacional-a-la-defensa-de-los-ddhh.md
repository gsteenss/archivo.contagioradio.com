Title: Se abre convocatoria para el Premio Nacional a la Defensa de los DDHH
Date: 2016-04-07 16:52
Category: DDHH, Nacional
Tags: Centro Memoria Histórica, Derechos Humanos, Diakonia, paz
Slug: se-abre-convocatoria-para-el-premio-nacional-a-la-defensa-de-los-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/premio-ddhh.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [7 Abr 2016] 

**Desde el primero de abril hasta el 20 de mayo,** está abierta la convocatoria para la postulación de organizaciones, procesos y personas que han dedicado su vida a la defensa de los derechos humanos en Colombia.

Se trata del Premio Nacional a la Defensa de los DDHH, **que cada 9 de septiembre desde el año 2012, la ONG de Cooperación de Suecia Diakonia** – Programa Colombia entrega a las personas y organizaciones que trabajan día a día en la construcción de la paz y una sociedad democrática. Un premio que busca visibilizar las luchas de cientos de personas, que algunas veces invisibles, trabajan por la defensa y promoción de los DDHH en Colombia.

En la versión pasada, Francia Márquez, lideresa afro de la vereda Yolombo en el municipio de Buenos Aires en Cauca; Mujeres Caminando por la Verdad, colectivo de mujeres víctimas de la operación Orión; OS-PACC, sobrevivientes del exterminio de la Asociación Departamental de Usuarios Campesinos de Casanare; Fabiola Lalinde, madre de Fernando Lalinde, desaparecido en el año 1984; Ocomacia, organización que trabaja por visibilizar, denunciar y acompañar a las comunidades del Medio Atrato; fueron galardonados por su trabajo.

Las categoría para este año son: **defensor y defensora de derechos humanos del año, reconocimiento de toda una vida al servicio y proceso social colectivo y ONG´s.**

Algunos de los jurados son reconocidos por su trabajo investigativo frente a la defensa DDHH, como Mario Morales, reconocido docente de la Universidad Javeriana de la facultad de  Comunicación Social; Amanda Romeo, defensora de derechos humanos; y Martha Nubia Bello académica investigadora del Centro de Memoria Histórica.

“**Para nosotros la construcción de la paz es un horizonte de largo plazo, que se hace y en la medida en que la agenda de derechos humanos continúa vigente y se da realización a todos los derechos de manera integral a cargo del Estado**”, expresa Nancy Arevalo, del Programa Colombia de Diakonia.

Para mayor información pueden visitar la [Página del Premio Nacional para defensores y defensoras de DDHH.](http://www.premiodefensorescolombia.org/convocatoria-2016/)

<iframe src="http://co.ivoox.com/es/player_ej_11084704_2_1.html?data=kpadmpmbdJWhhpywj5WcaZS1kpyah5yncZOhhpywj5WRaZi3jpWah5ynca%2FVz8jmjabWqdfVzdSSlKiPqMbgjLXf0czWpc7VjKjcztTRpsrVjMnSjanNpczjz87OkJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
