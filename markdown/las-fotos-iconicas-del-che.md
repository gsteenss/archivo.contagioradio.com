Title: Las fotos icónicas del Che Guevara
Date: 2015-10-09 10:20
Category: Cultura, El mundo
Tags: 47 años muerte Che Guevara, Alberto Korda, Ernesto Che Guevara, Fotografías Ernesto Che Guevara, Liborio Noval
Slug: las-fotos-iconicas-del-che
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Che-Guevara-Camera-Rolex.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [9 Oct 2015] 

Aunque Ernesto Guevara de la Serna, al ver un fotógrafo apuntandole con su lente decía "no gastes película", fueron varios los que lograron capturar imágenes que hoy constituyen una amplia galería que ha servido para inmortalizar la figura del emblemático líder de la revolución cubana.

Fotografías como la obtenida por Alberto Korda, tomada el 5 de marzo de 1960 mientras el "Ché" observaba el cortejo fúnebre de los muertos en el atentado terrorista al barco La Coubre, conocida como "Guerrillero Heroico", utilizada y reconocida como ícono universal del pensamiento rebelde.

\

Liborio Noval, otro cubano, fotografió al Che en sus tiempos de Ministro, pero pero también en el trabajo voluntario y en su entorno familiar, mientras que el fotógrafo suizo René Burri, es el autor de otra de las emblemáticas fotografías,  la famosa foto del Che Guevara fumando un puro.

Tanto Noval como Korda y el resto del grupo de profesionales no fueron simples documentalistas de la revolución; también se sentían parte y protagonistas del proceso y militaron activamente en su causa, por eso los detractores del castrismo los consideraron siempre simples propagandistas.
