Title: Ante renuncia de secretario de JEP las víctimas de Estado insisten en proteger archivos del DAS
Date: 2018-04-03 15:29
Category: Nacional, Paz
Tags: das, DIH, MOVICE, vícitmas
Slug: ante-renuncia-de-secretario-de-jep-las-victimas-de-estado-insisten-en-proteger-archivos-del-das
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/victimas-movice-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [03 Abrl de 2018] 

Luego de conocerse la noticia de la renuncia de Nestor Raúl Correa, actual Secretario Ejecutivo de la JEP, y quien ejercería hasta el próximo 30 de Abril, las organizaciones que hacen parte del Movimiento de Víctimas de Crímenes de Estado, **pidieron que se mantengan las medidas cautelares dictadas por esa oficina para que salvaguardar los archivos del extinto DAS**.

Según las organizaciones de víctimas es necesario que se mantengan las medidas cautelares, puesto que en esos archivos reposa material de inteligencia, reportes de seguimientos y otras informaciones que tienen que ver con la acción estatal contra defensores de Derechos Humanos y líderes políticos de varias generaciones **que sufrieron la persecución a través de Departamento Administrativo de Seguridad, DAS**. (Le puede interesar: ["JEP otorga medidas a archivos de inteligencia del DAS](https://archivo.contagioradio.com/jep-otorga-medidas-cauteles-a-archivos-de-inteligencia-del-das/)")

Para el MOVICE “La medida asumida por la SE-JEP es pertinente toda vez que actúa sobre los riesgos que pesan sobre dichos archivos1, si se atiende a la posibilidad de que exista destrucción, alteración u ocultamiento de documentos con vocación probatoria” así mismo señalan que la medida cautelar está dentro de las facultades asignadas a la Secretaría con el acto legislativo 01 de 2017 en su artículo 7.

El MOVICE le pidió también a la Presidencia de la JEP “que adopte las medidas necesarias para el cumplimiento del auto preferido por la SE-JEP, y que con carácter urgente se tomen decisiones similares para la proteccion de todos los archivos que contengan información que **contribuyan al esclarecimiento de la verdad de violaciones a los DH e infracciones al DIH.**”

Aunque la renuncia de Correa podría afectar las decisiones tomadas que no se han implementado, la salvaguarda de los archivos y las demás medidas que se hayan tomado desde la secretaría tendrán que ser garantizadas por la presidencia de la institución que fue creada para buscar la superación de la impunidad y la garantía de todos los derechos de todas las víctimas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
