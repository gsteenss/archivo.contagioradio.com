Title: Estructuras de poder minimizan y legitiman hechos como el ocurrido con menor en San Agustín
Date: 2020-11-18 18:20
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Ejército Nacional, Huila, San Agustín
Slug: dos-menores-de-edad-fueron-asesinados-por-el-ejercito-en-san-agustin-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Ejercito-asesina-dos-menores-en-San-Agustin-Huila.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este domingo **dos menores de edad de 14 y 16 años fueron asesinados, presuntamente por un miembro del Ejército Nacional,** en medio de un control militar impuesto para el cumplimiento de algunas medidas restrictivas relacionadas con el Covid-19 en la vereda “El Palmar de Obando” del municipio de San Agustín, Huila. (Le puede interesar: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marthaperaltae/status/1328194423096811520","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1328194423096811520

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la información suministrada, **uno de los miembros del Ejército que integraba el control accionó su arma de fuego en contra de los menores que se movilizaban en una motocicleta,** luego de que estos omitieran una señal de pare, ocasionándoles graves heridas que posteriormente les causaron la muerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las víctimas se identificaron como **Emerson Alejandro Dussan Puentes de 16 años**, quien falleció cuando era trasladado al hospital “San Antonio” en Pitalito y **Joselino Irua Delgado de 14 años**, quien murió en el centro asistencial; ambos se desempeñaban en labores agrícolas como la recolección de café.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El uniformado que habría disparado el arma, pertenece al **Batallón de Infantería No. 27 Magdalena** y según confirmó el Mayor General Luis Mauricio Ospina, Comandante de la Quinta División del Ejército, se trata de un soldado profesional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Óscar Prieto, integrante del Observatorio Surcolombiano de Derechos Humanos, Paz y Territorio -[OBSURDH](https://www.obsurdh.org.co/)-, señaló que **este Batallón es el que más casos de presuntas ejecuciones extrajudiciales tiene en el Huila.** Para Prieto este tipo de casos son producto de **una práctica sistemática y generalizada** en la que se ha instruido a los miembros de la Fuerza Pública para considerar a la población civil como un enemigo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el integrante de OBSURDH, señaló que en este caso hay un agravante y es el hecho de que San Agustín no es una zona en la que haya presencia de actores armados, por lo que es injustificable el proceder de la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente al caso, el Comando de la Novena Brigada del Ejército Nacional emitió un comunicado en el que confirmó el hecho señalando que “*por el resultado de la reacción de uno de nuestros soldados, orgánico del Batallón de Infantería N.º 27 Magdalena, dos menores de edad resultaron heridos, siendo de inmediato trasladados a un centro asistencial donde lamentablemente fallecen* ”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El ministro de Defensa, Carlos Holmes Trujillo, se dirigió a Neiva para atender la situación y señaló que el soldado presuntamente responsable había sido retirado de sus funciones y puesto a disposición de las autoridades judiciales. Asimismo, tras la muerte de los menores a manos del soldado, desde el Ministerio de Defensa y el Comando General de las Fuerzas Militares se ordenó un “*reentrenamiento*” en el uso de las armas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/cuervoji/status/1328752486077829120","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/cuervoji/status/1328752486077829120

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### El discurso que legitima hechos como el de San Agustín

<!-- /wp:heading -->

<!-- wp:paragraph -->

En septiembre de este año **también fue asesinada Juliana Giraldo en medio de una intervención del Ejército Nacional** en la que un uniformado abrió fuego en contra del vehículo en el que se movilizaba con su pareja y dos personas allegadas. Este hecho generó indignación y puso en entredicho los métodos tanto de reclutamiento como de formación de los miembros de la Fuerza Pública. (Lea también: [Ejército asesinó a Juliana Giraldo, mujer transgénero en Miranda Cauca](https://archivo.contagioradio.com/juliana-giraldo-mujer-trans-es-asesinada-por-el-ejercito-en-miranda-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Óscar Prieto **el discurso que se maneja desde las autoridades civiles, legitima en algún punto este tipo de actuaciones, ya que se minimiza la gravedad de los hechos** en los que se ven involucrados miembros de la Fuerza Pública y por ello las investigaciones no cuentan con una rigurosidad que permita identificar y sancionar a los responsables, razón por la cual “*la impunidad supera el 90%*” en estos casos.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “En la medida en que las estructuras de poder legitimen este tipo de acciones, \[los miembros de la Fuerza Pública\] se sienten cada vez con mayores posibilidades de salir impunes”.
>
> <cite>Óscar Prieto, integrante de OBSURDH</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
