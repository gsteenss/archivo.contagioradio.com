Title: El legado de la Guarachera de Cuba sigue vivo
Date: 2016-02-04 16:02
Category: En clave de son
Tags: Celia Cruz, Premio póstumo a Celia Cruz, Salsa y son cubano
Slug: el-legado-de-la-guarachera-de-cuba-sigue-vivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/celia-e1454619812265.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Ilustrración: Ernesto Molina 

###### [4 Ene 2016] 

Celia Caridad Cruz Alfonso, o simplemente Celia Cruz, será homenajeada de manera póstuma por parte de la Academia de la grabación con el "Premio al Logro Vitalicio", por su contribución al mundo de la música y a la cultura de la industria, durante su trayectoria profesional que abarcó casi 6 décadas.

La artista fallecida en New Jersey el 16 de Julio de 2003, añadirá un reconocimiento más a la larga lista de distinciones entre las que cuenta tres Grammys y cuatro Grammys latinos, entregados por la misma Academia, y 23 discos de oro por ventas de sus producciones discográficas.

En el marco de la conmemoración de los 90 años del nacimiento de la "Guarachera de Cuba", el pasado 21 de Octubre nuestro espacio "En Clave de son" realizó un especial en su honor, con algunos de los temas inmortalizados en su voz y estilo. Un homenaje de dos horas que compartimos con ustedes.

<iframe src="http://www.ivoox.com/player_ek_10318670_2_1.html?data=kpWgk52ae5Ghhpywj5WWaZS1k5qSlaaWe46ZmKialJKJe6ShkZKSmaiRdI6ZmKiap9OPp83V18qYxsqPl9DihpewjajJsMrVjKjf19%2BRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
