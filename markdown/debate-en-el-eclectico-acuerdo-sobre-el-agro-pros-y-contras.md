Title: Debate en el Ecléctico: Acuerdo sobre el agro, pros y contras
Date: 2016-09-05 15:32
Category: El Eclectico
Tags: Acuerdos de La Habana, proceso de paz, Reforma agraria integral
Slug: debate-en-el-eclectico-acuerdo-sobre-el-agro-pros-y-contras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/FOTO9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 5 Sep 2016 

El debate en el Ecléctico sobre la **Reforma Rural Integral**, contemplado en el acuerdo de paz firmado entre el Gobierno y las Farc, contó con la participación de **Ángela Ospina de Nicholls**, vicepresidenta del Partido Conservador y exasesora del Incoder, quien aseguró que no incluye nada nuevo ni diferente a la legislación agraria que ya existía. Para ella, lo único que significa este acuerdo es el reconocimiento de que el Estado colombiano ha fallado en implementar las medidas que ya había tomado en el pasado para solucionar el problema del agro.

Por el contrario, **Jaime Pérez**, quien trabaja en la oficina del Alto Comisionado para la Paz y contribuyó a la redacción del acuerdo, interpretó la Reforma Rural Integral como la oportunidad que tiene el país para poner en marcha las medidas que pretenden implementar una política agraria que le sirva al país para aumentar la productividad del agro y reducir la brecha entre el campo y la ciudad. Además, aseguró que esta reforma no incluye únicamente aspectos de aumento de productividad agraria sino que el hecho de que sea integral implica que se intervendrán otros aspectos importantes que necesita el campo colombiano para ser productivo y que están en mora, como la infraestructura vial y la tecnificación.

Por su parte, **Daniel Arce**, coordinador del comité ideológico del grupo UNICD y miembro coordinador del centro democrático de la Universidad del Rosario, cuestionó que la redacción de este acuerdo agrario no cierra la puerta para que se ponga en riesgo la propiedad privada. Manifestó la preocupación del Centro Democrático relacionada con el establecimiento de una jurisdicción agraria, pues, según Arce, “no está claro por ejemplo quiénes van a ser los que se van a encargar de determinar la extinción de dominio, de la que también se habla en el acuerdo”.

<iframe src="https://co.ivoox.com/es/player_ej_12788959_2_1.html?data=kpekmp2deZqhhpywj5WVaZS1kZmSlaaUeI6ZmKialJKJe6ShkZKSmaiRdI6ZmKiao8jZqdPY0JDg0cfWqYzZzZDOydfTaZO3jNXf0diPvYzX0NPh1MbXaZO3jMnSxMbYqYzZz5CyzpCpp82hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
