Title: Víctimas se darán cita en la Fiscalía para tejer la memoria
Date: 2016-11-01 16:24
Category: Movilización, Nacional
Tags: Fiscalía, memoria, MOVICE, Movilización, víctimas
Slug: victimas-se-daran-cita-en-la-fiscalia-para-tejer-la-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/victimas-movice-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [1 Nov de Oct.] 

Este 2 de noviembre el **Movimiento de Víctimas de Crímenes de Estado** – Movice - junto con otras organizaciones sociales y de derechos humanos han convocado a una **Jornada Nacional de Movilización en la que exigirán a la Fiscalía Nacional, celeridad, en investigaciones contra agentes estatales por violación de derechos humanos.**

Así mismo, diversos lugares del país se unirán a este llamado para exigir que los puntos de justicia y víctimas que fueron pactados en La Habana sean mantenidos. Zorayda Hernández, integrante del Movice, afirmó que **las victimas le han dicho al país de diversas formas incluyendo movilizaciones y actos simbólicos que “aquí estamos, existimos y seguiremos luchando por nuestros derechos”.**

Según Zorayda, esta será una jornada para hacer y reconstruir la memoria, pero también para tejer dignidades de cientos de familias que aún siguen esperando justicia y verdad en los casos de sus familiares perdidos por el conflicto armado en Colombia.

Según el Movice, realizarán esta movilización para mostrarle a Colombia y recordarle al Estado que ha tenido una “grave” responsabilidad en el conflicto armado **“le vamos a decir a la Fiscalía que ahí tenemos los casos de nuestros familiares, de nuestros amigos que están en completa impunidad,** por los cuales no ha habido ningún tipo de avance” y argumenta que “esta debería ser una de las instituciones que debería ser escudriñadas por la Comisión de la Verdad propuesta en La Habana, ellos deberían encontrar esa complicidad del aparato judicial encontrando todos los crímenes que se han cometido”.

Y concluye diciendo que **como movimiento de víctimas no están de acuerdo con las propuestas que han planteado en materia de justicia los promotores del no en el plebiscito** “porque los que están planteando el no, lo que quieren es la impunidad absoluta de quienes han estado comprometidos con los crímenes en este país – empresarios, fuerzas militares, organismos de seguridad del estado

 La cita en Bogotá será a las 10:00 a.m. en la Fiscalía General de la Nación y la invitación es a hacer presencia con fotografías de familiares para recordarlos y exigir justicia en sus casos. En todo el país la convocatoria es en las diversas seccionales de la Fiscalía. Le puede interesar: [“El sí es una posibilidad de encontrar nuestros desaparecidos”: Movice](https://archivo.contagioradio.com/el-si-es-una-posibilidad-de-encontrar-nuestros-desaparecidos-movice/)

<iframe id="audio_13571728" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13571728_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
