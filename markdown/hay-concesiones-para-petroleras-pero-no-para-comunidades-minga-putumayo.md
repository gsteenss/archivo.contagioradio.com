Title: Hay concesiones para petroleras pero no para comunidades: Minga Putumayo
Date: 2019-06-14 13:20
Author: CtgAdm
Category: Ambiente, DDHH
Tags: agencia Nacional de Tierras, Minga
Slug: hay-concesiones-para-petroleras-pero-no-para-comunidades-minga-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/Minga-Indigena-Paro-Nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vice] 

Dos meses después de la finalización de la Minga Nacional, los movimientos indígenas del Putumayo decidieron bloquear la vía que conduce de Mocoa a la capital del Huila dado que el Gobierno sigue incumpliendo los compromisos pactados, entre ellos en torno al **reconocimiento  de derechos colectivos, territoriales y a la Madre Tierra**.

Según Gilberto Mochicón, integrante Nasa de la Guardia Indígena del Putumayo, el Gobierno fue citado a una mesa de diálogos el pasado 14 de mayo, fecha que fue aplazada por parte de los funcionarios para el 13 de junio sin consultar a los delegados de la Minga. Sin embargo, el **Gobierno no llegó a la reunión que había convocado para este miércoles**. (Le puede interesar: "[La Minga vive y cumplió con su palabra, ahora solo falta la del Gobierno](https://archivo.contagioradio.com/la-minga-cumplio-con-su-palabra-ahora-solo-falta-la-del-gobierno/)")

A pesar de que las comunidades Nasa fijaron el jueves como la nueva fecha para el encuentro, "los delegados del Ministerio del Interior nos dicen que no pueden hacer presencia el día de hoy porque solamente tenían tiempo el día de ayer", afirmó Mochicón. Además, los funcionarios comunicaron que los delegados de la Minga podrían acordar una nueva fecha con el Gobierno a través de correo electrónico.

### **Puntos de la mesa de diálogos**

Según el integrante, el punto más importante es el territorio. "Estamos exigiendo la presencia de la Agencia Nacional de Tierras para mirar el tema territorial porque para las transnacionales petroleras sí hay concesiones de tierras, pero para las comunidades indígenas, campesinas y negras del Putumayo no hay estas posibilidades". Puntualmente, piden la constitución de sus resguardos, demanda que se suma a otros importantes, como la de la salud, vivienda, educación, entre otros.

### **La Minga del Putumayo sigue viva**

Frente estos hechos, la comunidad Nasa del Putumayo decidió bloquear nuevamente la vía que conduce de Mocoa a Pitalito, Huila hasta que los funcionarios del Gobierno se reúnan con la Minga para firmar el acuerdo de la nueva reunión que pactarán.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
