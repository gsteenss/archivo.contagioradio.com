Title: Unas velas, el poder de lo nuevo
Date: 2018-07-07 17:42
Category: Camilo, Opinion
Tags: colombia, Empresarios, lideres sociales, Pax, persecución estatal
Slug: unas-velas-el-poder-de-lo-nuevo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/velaton-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### Por **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### 7 Jul 2018 

30 o 127, uno solo, un solo asesinado es un escándalo. La violencia multiforme que combina amenazas, estigmatizaciones, seguimientos legales e ilegales,  interceptaciones, y culmina con el asesinato de lideresas y líderes, defensores y promotores de paz está mostrando la dolorosa transición de la guerra a la Pax Neoliberal.

**El acicate es variopinto pero quien más rentabilizó esa Pax ha sido el uribismo que generó un sentimiento de adoración a la muerte como si fuera vida**.

Los aderezos son visibles. Los incumplimientos del Acuerdo del Teatro Colón para la terminación del conflicto armado con las FARC; los re incorporados en espera de respuestas de gobierno, y otros, en la desesperanza, ya distantes de las Espacios Territoriales; los planes de Desarrollo con Enfoque Territorial en pañales; el desmonte de estructuras herederas del paramilitarismo y de otro tipo de criminalidad en el papel, y finalmente, una mesa de conversaciones con el ELN con poca estimulación.

Da igual uno o cien, recordando la expresión del periodista asesinado, Guillermo Cano. El asunto no son la cifras. El asunto es que \#NosEstánMatando.  (Ver: ¿[Hay sistematicidad en asesinatos a líderes sociales en Colombia](http://¿Hay%20sistematicidad%20en%20asesinatos%20a%20líderes%20sociales%20en%20Colombia?)?)

**La transición de esa Pax Neoliberal refleja el país de regiones en su especificidad de deudas sociales y ambientales históricas**. Así como el entrecruce de viejas violencias socio políticas, nuevas criminalidades y las de la racionalidad de la "justicia" por cuenta propia.

Los caídos son lideresas o líderes asesinados en la vieja técnica de sicariato que incluye a reclamantes de tierras en zonas de operación empresarial despojadora para asegurar agronegocios de palma, banano, coca, infraestructura. (Bajo Atrato, Catatumbo, Ituango) convertidos en blanco militar de estructuras paramilitares empresariales y neoparamilitares.

**Otros caídos son los líderes que promueven la sustitución de las siembras de hoja de coca que enfrentan grupos armados** (legales e ilegales) que se benefician del tráfico de cocaína sea por los peajes que cobran o porque ellos envían su propia mercancía valiéndose de sus relaciones con los invisibles: los empresarios de alto riesgo.

Otros de los caídos son aquellos que han promovido la paz afectando intereses de aquellos que se han beneficiado de la guerra, que conocen verdades que deben ser silenciadas u ocultadas. Algunos de ellos corresponden a familiares de exguerrilleros o incluso a ex combatientes que saben de la persecución estatal.

Otros caen por la decisión que toman quiénes tienen diferencias o tensiones no políticas  con los líderes o lideresas  haciendo uso de la violencia.

Y otros, de los más recientes asesinados, son aquellos que han ejercido un liderazgo social que salta hacia lo político en lo electoral a través de la real oposición política.

Intentar trazar los rasgos de los móviles en nada exculpa los equívocos silencios del presidente Santos, los salidas en falso de su ministro de Defensa en los que se banaliza la muerte violenta o se justifica.

**Si los asesinatos de los líderes tienen un patrón o una sistematicidad más allá de los enfoques jurídicos penales hay que abordarlos en un contexto más amplio.**

Los asesinatos son los desastrosos costos de una Pax Neoliberal en la que coinciden el santismo y el  uribismo al pretender sustraer a la histórica clase política de sus responsabilidades en la que todo ha valido desde la corrupción y la mentira hasta el paramilitarismo.

Y,  por supuesto  una acuñada ausencia de voluntad politica para cumplir la palabra y enfrentar la podredumbre que corroe la propia clase política, a un sector empresarial y a sectores milltares y policiales que les han servido para sortear el cuestionamiento de su autoridad como legitima.

La sistematicidad es de raíces históricas de responsabilidad, insisto, más allá de la calificación penal. Un solo asesinato de un defensor es gravísimo, muchos más demuestran la gravedad.

Muchos de los violentos están de fiesta. Ellos están pescando en río revuelto para asegurar sus intereses, otros están aprovechando para difuminar la responsabilidad estatal y de la clase dirigente.

**La velatón nacional e internacional** como expresión ciudadana de indignación es diciente de lo nuevo con independencia del motivo de los que son sicarios o de los que están detrás de los sicarios.

**La velatón es  la ética de la vida más allá y entre la muerte violenta.** Ahí se está expresando el nuevo país, el sentido de nación que toma distancia del silencio, de la banalización y de la naturalización de la muerte violenta.

Allí en los velas que danzan entre el viento de la tarde o de la noche están almas solitarias que se suman, en ellas están los resortes y horizontes de la paz que hace y nace de la sociedad.La sociedad que entre la Pax Neoliberal va abriendo pasos a lo nuevo, ahí el poder consciente de la democracia profunda.

Nada es igual al 78 con el poder de la Triple AAA desde el Cantón Norte. Tampoco es el tiempo de los masetos ni de las ACCU o de las AUC es el tiempo de las disonancias con el poder autoritario adobado de democracia. Es el tiempo en que la infinita adoración a la guerra se está resquebrajando. Es el tiempo donde el amor es por la vida, el movimiento del alma de la nación empieza a ser leal a su origen.

En esas almas esta la sociedad que tiene la posibilidad de terminar la noche terror por días y noches de alegría, en donde nos muramos de viejos o por el destino de la propia naturaleza finita. Unas velas, miles de velas, el poder de lo nuevo.

#### **[Leer más columnas Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
