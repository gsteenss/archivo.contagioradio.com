Title: Guardias del INPEC destruyeron medicamento que mantiene con vida al preso José Ángel Parra
Date: 2016-05-20 16:47
Category: DDHH, Nacional
Tags: crisis carcelaria, INPEC
Slug: guardias-del-inpec-destruyeron-medicamento-que-mantiene-con-vida-al-preso-jose-angel-parra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ICRC 

###### [20 May 2016]

Desde la Fundación Comité de Solidaridad con Presos Políticos, se denunció este jueves que al interno **José Ángel Parra** recluido en el centro penitenciario ERON - Picota, la guardia del INPEC **le destruyó sus pertenencias incluido su medicamento para tratar la leucemia en fase terminal que sufre.**

Según la denuncia, la guardia del INPEC ordenó a todos los internos del patio cuatro salir de sus celdas para una inspección general. Cuando ingresaron a la celda de Parra, dañaron todas sus pertenencias, su colchoneta fue picada, sus elementos de aseo fueron pisoteados y todas sus cosas fueron tirados al piso, con ellas los **600 miligramos de  Imatinib, sin la cual el recluso puede empezar a sufrir de** fiebre, desaliento, hematomas e incluso convulsiones como sucedió ayer.

Cabe recordar que el 30 de enero de 2015 la Corte Interamericana de Derechos Humanos, le exigió al Estado colombiano, proteger y garantizar la vida del prisionero político de las FARC EP, José Ángel Parra, sin embargo, al interno no se le ha garantizado la atención en salud, pues en reptidas ocasiones ha durado semanas sin recibir sus medicamentos.

Cabe recordar, que pese a que el nuevo Ministro de Justicia, Jorge Eduardo Londoño, **decretó nuevamente emergencia carcelaria en 74 prisiones del país, la situación de los más de cinco mil presos que sufren enfermedades como VIH, cáncer, diabetes y problemas psiquiátricos**, no mejora, y en cambio continúa agudizándose la crisis pues ya ni siquiera se cuenta con ibuprofeno en los centros carcelarios, pero además las arbitrariedades por parte de los guardias son constantes.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
