Title: "Falsos Positivos" son crímenes de Lesa Humanidad
Date: 2017-04-04 17:43
Category: DDHH, Entrevistas
Tags: Falsos positivos Colombia
Slug: falsos-positivos-son-crimenes-de-lesa-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Aqrchivo Particular] 

###### [04 Abril 2017] 

Fueron 10 años los que tuvieron que luchar las familias víctimas de los mal llamado Falsos Positivos para que condenaran a 21 militares por la desaparición, concierto para delinquir y homicidio agravado de 5 jóvenes. **La Jueza Primera Especializada de Cundinamarca dio penas entre 37 y 52 años por los delitos de desaparición forzada y homicidio y señaló estos hechos como crímenes de lesa humanidad.**

La Jueza estableció que debido a la **sistematicidad de los 5 casos presentados y a características como que eran jóvenes de entre 20 y 25 años, de sectores marginados**, en este caso Soacha, se podía declarar los hechos como de lesa humanidad y además indicó que como acto de reparación el Estado deberá hacer una petición de perdón público. Le puede interesar: ["Por décima vez aplazan audiencia por Falsos Positivos de Soacha"](https://archivo.contagioradio.com/por-decima-vez-aplazan-audiencia-por-falsos-positivos-de-soacha/)

La abogada Carolina Daza, del Colectivo de Abogados José Alvear Restrepo, expresó que “**gran parte de la responsabilidad de este fallo, ha sido de las madres y de su persistencia**, desde el principio cuando los hechos se dieron ellas pusieron este tema en la opinión pública y eso presiono a que las investigaciones avanzaran pese a todos los obstáculos”.

La representante de las familias, afirmó que durante todo este proceso se “hizo énfasis en que las víctimas **no eran ningunos delincuentes, sino que eran personas que reunían ciertas características y condiciones que los convirtió en objetivos de estas empresas criminales**.

Sin embargo, Daza también explica que hay incertidumbre frente a la posibilidad de encontrar verdad en los casos de los Falsos Positivos, debido a que podrían entrar a la Jurisdicción Especial de Paz, **sin que esta esté en vigencia, situación que para la abogada puede generar más años de espera para las familias**. Le puede interesar: ["Juez noveno de garantías "se burla" de las víctimas de ejecuciones extrajudiciales"](https://archivo.contagioradio.com/juez-noveno-de-garantias-se-burla-de-las-victimas-de-ejecuciones-extrajudiciales/)

Estos hechos se llevaron a cabo en enero del año 2008, cuando los 5 jóvenes fueron reclutados en Soacha y aparecieron tiempo después en Ocaña como dados de baja en combate, las víctimas son **Víctor Fernando Gómez, Julio Cesar Mesa y Jhonatan Orlando Soto Diego Alberto Tamayo, Jader Andrés Palacio Bustamante**.

### **Lista de 21 militares** 

Medardo de Jesus Ríos  
Luis Alirio López  
Ferneny Grijalva  
Géiner Fuertes  
Pedro Johan Hernández  
Manuel Ángel Zorrilla  
Juan Gabriel Espinoza  
José Orlando González  
Kevis Alberto Jiménez  
Richard Armando Jojoa  
Mauricio Cuniche  
Nixon Arturo Cubides  
José Adolfo Fernández  
Gabriel de Jesús Amado (coronel)  
Ricardo Gonzales Gómez  
Eider Andrés Guerrero  
John Anderson Díaz  
Juan Ramón Marín  
Ricardo Coronado Martínez  
Janer Ediel Duque  
Henrry Mauricio Blanco

<iframe id="audio_17983304" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17983304_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
