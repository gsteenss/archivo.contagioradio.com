Title: El periodismo y la desaparición forzada, un debate pendiente
Date: 2016-02-16 08:22
Category: Opinion
Tags: desaparecidos, dialogos de paz, periodismo, proceso de paz
Slug: el-periodismo-y-la-desaparicion-forzada-un-debate-pendiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/periodistas1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [foto: proclamadelcauca] 

#### [**César Augusto Muñoz  - ASFADDES – PECT**] 

###### [16 feb 2016] 

Hace unos días se comunicó conmigo una periodista de uno de los programas del canal Citytv. Ella estaba interesada en contactar familiares de personas desaparecidas para un especial que están realizando sobre este delito. La solicitud fue clara: “necesito contactar a través de ASFADDES a familiares de personas que hayan sido desaparecidas por la guerrilla de las FARC, para que me den su testimonio para el programa”.

La respuesta a esta solicitud fue la siguiente contra-pregunta ¿Por qué realizar un especial sobre este delito con víctimas de una sola de las partes en conflicto, sin tener en cuenta que el Centro Nacional de Memoria Histórica en el informe que publicó en el año 2014, confirmó que la principal responsabilidad en estos hechos la tienen agentes del Estado en complicidad con grupos de paramilitares y narcotraficantes? (Ver los cuatro informes sobre [desaparición forzada realizado por el CNMH](http://www.centrodememoriahistorica.gov.co/informes/informes-2014/desaparicion-forzada), 2014 )

Ante la pregunta la periodista sólo atino a responder que era parte de la exigencia editorial del programa, teniendo en cuenta el contexto de los diálogos de paz. De esta manera, salvo su responsabilidad personal.

Esta experiencia reciente me permite poner en contexto el debate en torno a la relación entre periodismo y desaparición forzada, en un momento en el que un sector importante de organizaciones sociales y entidades vienen discutiendo el desarrollo y la puesta en marcha del acuerdo 62 de la mesa de Negociación entre las FARC EP y el Gobierno Nacional para la búsqueda e identificación de todas las personas dadas por desaparecidas.

En ese sentido, considero importante preguntar ¿Cuál va ser la responsabilidad que van asumir los medios de comunicación masivos, los periodistas, editores y empresarios dueños de estos medios, frente a la búsqueda, localización e identificación de personas desaparecidas? En una eventual, comisión de la verdad ¿Asumirán los medios de comunicación, la responsabilidad histórica que les compete por hacer parte de los mecanismos sociales que permitieron y avalaron este tipo de prácticas atroces? Y los periodista de a píe ¿Seguirán como autómatas realizando tareas para editores politizados, sin ningún tipo de cuestionamientos?

El debate es mucho más complejo que estas preguntas y tiene sentido hoy más que nunca asumirlo con la altura que requiere, porque los familiares sobrevivientes que han estado organizados desde hace más de 34 años, tienen claro que el reconocimiento por parte de la sociedad de este delito, es un paso fundamental en el avance de los mecanismos de prevención.

En ese camino, es primordial el papel de los medios de comunicación liderados por periodistas dispuestos a realizar investigaciones serias, con tiempos suficientes para la realización y logrando una difusión masiva en horarios “prime time”. La premisa de ASFADDES como organización de sobrevivientes es clara: las sociedades que reconocen la existencia de los desaparecidos, conocen la problemática y entienden los mecanismos de prevención, no permiten que estos hechos se vuelvan a repetir y en cambio exigen a las autoridades legales o ilegales responsables de las acciones, la aparición con vida de estas personas. De esa manera, los verdugos no tienen eco en la sociedad y no les queda más salida que detener sus acciones violentas.

Por esta razón, es clave asumir un diálogo franco y directo entre sobrevivientes, periodistas, editores y empresarios dueños de medios de comunicación, para así avanzar en una estrategia de comunicación amplia y masiva que se convierta en un mecanismo social de prevención de este delito.
