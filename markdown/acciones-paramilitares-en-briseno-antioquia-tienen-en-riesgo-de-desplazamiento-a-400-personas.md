Title: Acciones paramilitares en Briceño, Antioquia tienen en riesgo de desplazamiento a 400 personas
Date: 2016-01-29 13:05
Category: DDHH, Nacional
Tags: desminado humanitario, Desplazamiento forzado, paramilitares, proceso de paz
Slug: acciones-paramilitares-en-briseno-antioquia-tienen-en-riesgo-de-desplazamiento-a-400-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/paramilitarismo-e1454090118583.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimercia 

###### [29 Ene 2016.] 

En el municipio de Briceño, Antioquia, la comunidad denuncia que en los últimos meses han sido víctimas de la **estigmatización, asesinatos, y desplazamiento por cuenta de acciones paramilitares, del denominado Clan Úsuga.**

En zonas aledañas a la vereda El Orejón, las personas se encuentran en situación de confinamiento por la presencia paramilitar en la zona, los habitantes evitan salir por miedo a ser asesinados, ya que en el pueblo se han difundido panfletos donde amenazan de muerte a los pobladores. Además, en la vereda La América, los hermanos Moreno, líderes campesinos del lugar, fueron asesinados por miembros del Clan Úsuga.

Aunque las autoridades habían asegurado que tomarían las medidas necesarias para hacerle frente al grupo neoparamilitar, el día de los asesinatos de los tres hermano, el Movimiento Ríos Vivos de Antioquia preguntó al Ejército por las razones por las cuales no estaba en esos lugares donde sucedieron los hechos, los militares respondieron que había sido por instrucción de un alto mando militar.

De acuerdo con un comunicado publicado del Movimiento, los asesinatos de estos líderes y otros campesinos, han ocurrido en circunstancias poco claras, donde se supone que hay presencia de la fuerza pública y quienes ya conocen los sitios y la forma como opera los paramilitares.

“Muchos de los campesinos no duermen en sus casas por miedo a incursiones paramilitares y **se preparan para un desplazamiento masivo cercano a las 400 personas** de no resolverse la angustia que están viviendo. Ya empiezan a **escasear los víveres para algunas familias** y otras han salido por peligrosos terrenos que no han sido descontaminados de minas antipersonales, en absoluto sigilo ingresan al pueblo evitando ser vistos y reconocidos como de estas veredas”, afirma el comunicado.

Ante este panorama, la primera pregunta que asalta a los habitantes, tiene que ver con la decisión de haber escogido ese lugar para el desarrollo del plan piloto de [desminado humanitario](https://archivo.contagioradio.com/?s=desminado+humanitari)acordado en el proceso de paz que avanza en la Habana, pues de acuerdo al comunicado publicado, la comunidad no fue la que tomó esa decisión, “lo que es cierto es que **los terrenos desminados son, en su mayoría, propiedad de Empresas Públicas de Medellín –Grupo EPM para [Hidroituango](https://archivo.contagioradio.com/?s=hidroituango) y de Continental GOLD LTD** para la segunda mina más grande del departamento”, y añaden que pese a las múltiples expectativas que el gobierno les creó sobre los beneficios del desminado, “este ha avanzado con una intervención social desordenada, casi nula, sin resultados” y además poca seguridad para la comunidad.

Los pobladores hacen un llamado a las instituciones estatales, organizaciones sociales e incluso a las FARC EP para que "se abstengan de dar declaraciones o realizar acciones que aumenten el riesgo de la población" que se encuentra en medio del conflicto, como sucede en Briceño, Antioquia.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
