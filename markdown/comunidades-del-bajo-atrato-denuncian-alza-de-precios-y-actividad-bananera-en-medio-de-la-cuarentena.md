Title: Comunidades del Bajo Atrato denuncian alza de precios y actividad bananera en medio de la cuarentena
Date: 2020-04-07 16:36
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Bajo Atrato, Chocó, Pede
Slug: comunidades-del-bajo-atrato-denuncian-alza-de-precios-y-actividad-bananera-en-medio-de-la-cuarentena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/informe-platano083116_153.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Bajo Atrato/ Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque desde el 30 de marzo en la zona, que corresponde a los territorios colectivos de Curbaradó y Pedeguita y Mancilla en el Bajo Atrato, se realizan controles para regular el ingreso de personas en medio de la cuarentena, defensores de DD.HH. denuncian que además de existir un sobrecosto en los insumos, numeroso personal ingresa a las plantaciones bananeras del sector, sin conocerse si existen protocolos para controlar la propagación del Covid-19 en una zona donde comunidades afro e indígenas están expuestas a enfermedades como el dengue y el paludismo, al control militar y paramilitar y al accionar de empresarios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Actividad bananera no se detiene pese a aislamiento

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La comunidad denunció que existe mucha actividad al ingreso a Caño Manso, en las zonas de Nueva Unión y Pijao, señalan que muchas personas estarían ingresando a las plantaciones de plátano a hacer la recolecta, incluído el ingreso de personal del que se desconoce qué vínculo tienen con empresas o con Baldoyno Mosquera, representante legal del Territorio Colectivo de Pedeguita y Mancilla.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"La gente va sin ningún tipo de protección, los camiones siguen ingresando, y la gente sigue manipulando elementos para hacer embarques" r**elata el defensor, ya existen tres casos de Covid-19 confirmados en el municipio aledaño de Apartadó y al menos 180 casos en Medellín hasta la fecha, pese a ello, las personas siguen llegando**. [(Lea también: Gobierno debe dialogar con Cuba para enfrentar la pandemia)](https://archivo.contagioradio.com/gobierno-debe-dialogar-con-cuba-para-enfrentar-la-pandemia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Relatan las comunidades que mientras salen a aprovisionarse a los caseríos de Brisas y Belén de Bajirá, han evidenciado que existe además un alza en los productos: una especulación de precios que ha llevado a que una libra de arroz cueste cerca de 3.000 pesos. Los habitantes de los territorios colectivos solicitan que se hagan los controles respectivos a los precios frente al caso en especial en la cuarentena.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No es solo el Covid-19, actores armados y paludismo amenazan al Bajo Atrato

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la comunidad ha acogido las indicaciones de autocuidado, existe temor frente a la pandemia por lo que señala, es necesaria una mayor pedagogía. La preocupación es aún mayor al evaluar los sistemas de salud de la zona en los dos hospitales de Belén de Bajirá, los que no cuentan con la infraestructura que requiere una enfermedad de esta magnitud. [(Le recomendamos leer: Campesinos cumplen cuarentena mientras Ejército erradica forzadamente en Cauca)](https://archivo.contagioradio.com/campesinos-cumplen-cuarentena-mientras-ejercito-erradica-forzadamente-en-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El defensor de DD.HH. advierte que de finalizarse el aislamiento en todo el país el próximo 13 de abril, sería una decisión poco prudente por parte del [Gobierno](https://twitter.com/infopresidencia), "En Bajo Atrato contamos con una población muy vulnerable, antes las bajas defensas que tiene la gente, no creo que su sistema biológico les permita resistir una pandemia como lo es el Covid-19". [(Le puede interesar: Aislamiento inteligente debe estar basado en la verdad y no en sub registros)](https://archivo.contagioradio.com/aislamiento-inteligente-debe-estar-basado-en-la-verdad-y-no-en-sub-registros/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Concluye que el temor no solo es hacia la pandemia y su expansión en el territorio colombiano, las comunidades deben enfrentar a su vez a los actores armados en el territorio, que aunque pareciera, han reducido sus actividades, continúan los controles y su estadía en el territorio, algo que varía según las dinámicas de cada territorio. [(Le puede interesar: Control paramilitar de AGC se extiende desde frontera con Panamá, Bajo y Medio Atrato)](https://archivo.contagioradio.com/no-es-solo-bojaya-bajo-atrato-y-dabeiba-blanco-de-operaciones-paramilitares/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El defensor lanzó una alerta final, está vez relacionada con la comunidad embera del Resguardo Jurada en Jiguamiandó que permanece confinada no solo por el Covid-19 sino también por los actores armados y donde se está presentando un brote de paludismo y posiblemente de dengue donde habrían 25 a 30 personas, entre ellas 10 menores con esta enfermedad viral y requieren de la atención debida, con el puesto de atención médica más cercano a cerca de tres horas, solicitan al Gobierno realizar los protocolos necesarios para tratar a las personas afectadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
  

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
