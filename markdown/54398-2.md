Title: Los salvavidas de la JEP
Date: 2018-06-29 15:23
Category: Nacional, Política
Tags: Centro Democrático, Iván Duque, JEP
Slug: 54398-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/1521051977_757465_1521052414_noticia_normal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [29 Jun 2018] 

Diferentes mecanismos se han activado para la protección de la Jurisdicción Especial para la Paz y la función que tiene de generar procesos de verdad justicia y reparación en lo que fue el conflicto armado del país. De acuerdo con Patricia Linares, presidenta de este Tribunal, las modificaciones serían **anticonstitucionales y el senador Iván Cepeda asegura que incluso otras podrían ser discriminatorias como lo es retirar la sigla LGTB** de los Acuerdos.

Estas propuestas que hasta ahora fueron aprobadas en el Senado de la República, deberán ser revisadas por la comisión conjunta de Cámara y Senado, posteriormente pasará a la Corte Constitucional. (Le puede interesar: ["Aún se puede defender el espíritu de la JEP"](https://archivo.contagioradio.com/aun-se-puede-defender-la-jep/))

### **Listos los mecanismos para proteger la JEP** 

<iframe src="https://co.ivoox.com/es/player_ek_26805534_2_1.html?data=k5ulkpqZd5Whhpywj5WbaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVhpewjdjTptPZjNHc1ZDRqcTVz87gz9TXb9HV08aY0tfTuMbbxteYzsaPjqbEjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según el senador del Polo Democrático, hay una primera demanda que se interpondrá por tres cargos: en primer lugar, **la distorsión que esas modificaciones hacen a los Acuerdos de Paz**, en segundo lugar, está el hecho de que una ley ordinaria no puede modificar la Constitución y en tercer lugar, está la violación del principio de consecutividad en el debate parlamentario, que significa que modificaciones como las que se hicieron no pueden realizarse en la plenaria del Senado, sin haber pasado por la Cámara de Representantes.

“Hay suficientes argumentos para que la Corte Constitucional, tanto por vicios de forma como de fondo, declare inexequible estos artículos que han sido objeto del debate” afirmó Cepeda y argumentó que **tampoco se paralizarán los procesos de quienes ya se habían acogido a la JEP**, debido a que ese organismo tiene todas las facultades para seguir con los casos.

En ese sentido, envió un mensaje de tranquilidad para las víctimas del conflicto armado, de igual forma, Patricia Linares señaló que ya son **2.000 los integrantes de la Fuerza Pública que se han sometido a esa instancia**.

Puntualmente frente al trabajo que realizará la bancada de oposición en el Congreso para la defensa de los Acuerdos de paz, Cepeda anunció que el próximo 20 de julio, se presentará un conjunto de proyectos de ley y actos legislativos que responden a los programas que defendieron Coalición Colombia y Colombia Humana o que fueron hundidos en el **Fast Track como las circunscripciones de paz y la reforma agraria**.

### **¿Un ataque a la constitucionalidad de la JEP?** 

Para Patricia Linares, las normas incorporadas sobre extradición y la creación de la nueva sala de juzgamiento a militares, tienen que ver directamente con la estructura de la Jurisdicción que ya están definidas en el acto legislativo 001, en ese sentido, para hacerlas realidad tendrán que **ser sometidas a una reforma constitucional y luego a la sanción presidencial, proceso que podría tardar 18 meses.**

<iframe src="https://co.ivoox.com/es/player_ek_26805473_2_1.html?data=k5ulkpqYe5Shhpywj5WcaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5yncbPVzs7f0ZCmqcvV08bb0YqWh4zVw9TUw8nTb9qfxcrTx9PXs9OfxcqYxsrWqcTc0NiYytrRpc_j1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Ramiro Bejarano, abogado defensor de derechos humanos expresó que las reformas que pretende hacer el Centro Democrático buscan acabar con la JEP e impedir que las personas que se hayan sometido a esta desde FARC, no puedan ejercer como parlamentarios hasta tanto no hayan purgado cárcel, para lo cual pedirían que se realizara en Colombia un referendo político.

Sin embargo, aseguró que la intensión más grande es impedir que la JEP descubra las relaciones que **hay entre el accionar de la Fuerza Pública, políticos y terceros en el marco del conflicto armado**. (Le puede interesar:[¿Se acaban las esperanzas para la JEP?](https://archivo.contagioradio.com/se-acaban-las-esperanzas-para-la-jep/)")

Además, manifestó que es “ridículo” que por un lado el presidente Santos denuncie y se oponga a las modificaciones que le pretenden hacer, mientras que por el otro se declara impedido para ejercer el poder que se le confiere de objetarla, acción que evitaría este debate.

###### Reciba toda la información de Contagio Radio en [[su correo]
