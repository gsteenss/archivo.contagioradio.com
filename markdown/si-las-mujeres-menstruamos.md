Title: Sí, las mujeres menstruamos
Date: 2018-10-05 07:00
Author: AdminContagio
Category: Opinion
Tags: feminismo, Menstruación, Menstruación libre de impuestos, mujeres
Slug: si-las-mujeres-menstruamos
Status: published

###### Foto: /claudiacarrillo 

#### **Por: [Ángela Galvis Ardila]** 

###### 5 Oct 2018

[Recuerdo tener 15 o 16 años y haberle pedido el favor a mi hermano menor de ir a comprarme un paquete de toallas higiénicas. Tenía un dolor menstrual casi incapacitante e ir hasta la tienda se me dificultaba, además de que, en mi familia nos acostumbramos a mandar a hacer favores al hermano o hermana menor. Estando en posición fetal apretándome el vientre para sentir un poco de alivio, escuché a mi hermano gritar mi nombre desde afuera. Al verme asomada en la ventana del segundo piso, gritó: “que hay dos marcas de toallas higiénicas, que cuál quiere”. ¡Sentí desfallecer! Vi gente a su alrededor, pero no quise detenerme a ver quiénes eran, solo deseé que la tierra se abriera y me tragara de un bocado. Atiné a responderle “las que sean” y me escondí de un salto. Ese día, sin saber el porqué, me avergoncé de mi naturaleza de mujer, gracias a que mi mandadero en un ataque de ingenuidad había pronunciado dos palabras malditas para decirlas en público: ¡toallas higiénicas! Malditas porque estaban relacionadas con un suceso que, aunque natural, tenía una carga enorme de tabú que me dictaba a mantener en secreto.]

[Secreto que me hacía recurrir a toda clase de eufemismos para no llamarlo por su nombre: “tengo eso”; “estoy en esos días”; “me bajó”, obvio, siempre y cuando tuviera muchísima confianza con mi interlocutora -sí, interlocutora-, porque si eran hombres y pares de mi adolescencia, ni a bate me hubiera atrevido tan siquiera a hacer sugerencia del tema. Uno de los miedos más tenebrosos de mi juventud era que mi ropa se viera manchada de sangre, pero de sangre que expulsara mi vagina, porque si era sangre de mi nariz o por una herida en mi cuerpo, esa no importaba, al contrario, esa -yo sabía- conseguía la atención y solidaridad de los demás. La otra, la que me empezó a llegar mes a mes, causaba burla, pena ajena, asco, hasta indignación, un completo estigma corporal.]

[Entendí que, si no quería causar incomodidad y de paso repulsión, siempre que hablara de ese fluido tan natural, lo tendría que hacer bajito, con discreción. Por eso, no fueron pocas las veces que antes de entrar a comprar el paquete de toallas higiénicas, crucé los dedos para que quien me atendiera fuera mujer porque si era hombre, además de decirlo casi entre los dientes, me pondría tan roja casi como el sangrado producto de mi biología femenina.]

[Empecé a darme cuenta de que todo confabulaba para que fuera así. Si tenía la menstruación y no quería saber del mundo, no podía responder -so pena de pasar por imprudente y hasta escatológica- con un escueto “tengo la menstruación”, lo correcto era decir “estoy indispuesta” o falsear la realidad e inventarme un dolor de cabeza, o de estómago o de muelas. Y es que hasta la publicidad hizo lo propio, pues en las propagandas televisivas de toallas higiénicas mostraban -y lo siguen haciendo- un líquido de color azul que se entiende es sangre. ¡Sangre azul! ¡Háganme el favor! El mensaje era y sigue siendo claro: la sangre producto del ciclo menstrual es de esconder, es vergonzosa, hay que hacerla ver menos asquerosa.]

[Y no estoy diciendo que tengamos que andar con un letrero o gritando que tenemos la menstruación. No. O que merezcamos una medalla por menstruar. Menos. O que sintamos orgullo por andar dejando huellas en la ropa o en el lugar en donde estemos. Tampoco. Simple y llanamente asumirlo como lo que es, un proceso natural femenino cíclico en el que el recubrimiento del útero es expulsado por la no fecundación del óvulo.]

[Hoy en día sigo con muchas taras en mi cabeza y en mi comportamiento debido a esa construcción cultural que en muchas oportunidades nos impide a las mujeres a habitar lo público. Por ejemplo, aún si estoy en un espacio distinto a mi casa y tengo que ir al baño a cambiarme de toalla higiénica, la saco y hago maromas para esconderla cual si estuviera llevando cocaína y me dispusiera a esnifarla. Sigo con el miedo al señalamiento porque mi ropa se manche, como si no fuera un accidente más, sino como si estuviera cometiendo un verdadero pecado contra la humanidad, pero por fortuna, para mí y mis miedos, ya entro sin ningún temor y compro las toallas higiénicas sin siquiera inmutarme, esté quien esté, porque sí, ya lo digo sin sonrojarme: las mujeres menstruamos.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
