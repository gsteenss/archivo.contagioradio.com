Title: En completa normalidad se cumplió jornada electoral en Venezuela
Date: 2015-12-06 10:08
Category: El mundo, Política
Tags: Brasil, CELAC, Elecciones Venezuela, Mercosur, Nicolas Maduro, Parlamento Venezuela, PSUV, unasur, Venezuela
Slug: en-completa-normalidad-avanza-la-jornada-electoral-en-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/elecciones_6D_Venezuela_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elestimulo 

###### [6 Dic 2015]

Este 6 de diciembre, los venezolanos y venezolanas eligen la conformación del parlamento de ese país con 167 diputados y diputadas. Más de 19 millones de personas podrán acceder a las urnas y expresar su respaldo a los candidatos a través del sistema electoral electrónico que se ubica dentro de los más seguros del mundo.

Esta jornada ha estado enmarcada por una muy fuerte presión política internacional que incluso orienta a la opinión internacional hacia una denuncia de “fraude” incluso cuando los mismos integrantes de la oposición, en reuniones privadas con veedores internacionales como los veedores de la UNASUR, avalan el sistema, pero públicamente lo descalifican, generando un clima desfavorable a las elecciones que han caracterizado a ese país como uno de los países en que mayor participación hay.

Los resultados de estas elecciones son el centro de la atención del mundo dado que la oposición al gobierno de Nicolás Maduro, ha anunciado que esta jornada es una especia de “plebiscito” al chavismo, de igual manera lo hizo en la elección general y las elecciones regionales en las que triunfó el gobierno con amplias mayorías.

Según diversos analistas el interés de las fuerzas de oposición sería ganar mayoría parlamentaria para iniciar un juicio parlamentario a Nicolás Maduro, al mismo estilo de Paraguay con el expresidente Lugo y Brasil que recién inicia este proceso contra la presidenta Dilma Roussef.

Diversos medios internacionales han señalado que la escasez de algunos productos y la falta de dinero en efectivo en bancos y cajeros automáticos, así como un supuesto amplio descontento social van a marcar las elecciones de este Domingo, intentando impulsar las voces de fraude que integrantes de la MUD han venido ambientando.
