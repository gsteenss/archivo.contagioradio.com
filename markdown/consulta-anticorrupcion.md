Title: Aprobada la consulta anticorrupción con 84 votos a favor y 0 en contra
Date: 2018-06-05 19:53
Category: Nacional, Política
Tags: alirio uribe, Claudia López, Consulta anticorrupción
Slug: consulta-anticorrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/consulta-anticorrupcion.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Prensa HN] 

###### [05 Jun 2018]

Con 84  votos  a favor y 0 en contra  fue votada la Consulta Anticorrupción que busca frenar las conductas corruptas de los Congresistas.

La consulta busca siete acciones concretas entra las que se encuentras: Reducir el salario a los congresistas y altos funcionarios del Estado, cárcel para corruptos y se les prohibirá volver a contratar con el Estado, Contratación transparente obligatoria en todo el país., Presupuestos públicos con participación ciudadana,  congresistas deben rendir cuentas de su asistencia, votación y gestión, hacer públicas las propiedades e ingresos injustificados de políticos y extinguirles dominio,  no más atornillados en el poder: máximo 3 periodos en corporaciones públicas.

De acuerdo con el representante a la Cámara Alirio Uribe, la idea es que la consulta Anticorrupción se alcanzaría a realizar en las elecciones a segunda vuelta por la presidencia de Colombia. "Nos permitiría ahorrar muchísimos recursos para unas elecciones" afirmó Uribe.

Para Uribe, los puntos críticos de esta consulta se enfocan en la intensión de "acabar con una clase política que viene de la corrupción" y que se ha visto reflejada en diferentes hechos vergonzosos para el país como el caso Odebrecht, Reficar, la Ruta del Sol, Interbolsa, el carrusel de la contratación en Bogotá, Saludcoop, entre otros. (Le puede interesar: "

En ese mismo sentido manifestó la importancia de acabar con prácticas como el pago de "coimas" para que se firmen contratos o los intercambios de favores para respaldar proyectos políticos. "Estamos viendo con ocasión del as elecciones como 5 ex presidentes que han estado en escándalos de corrupción de alinean en la campaña de Iván Duque, como el 90% del Congreso hoy se alinea, también, en función de evitar cualquier posibilidad de que hayan candidatos diferentes" afirmó el representante.

El siguiente paso lo tendrá que dar el **Presidente Juan Manuel Santos, **quien tendrá que fijar la fecha de votación en la que deben participar cerca del 30 por ciento del censo electoral, un cantidad cercana a 11 millones de personas.

<iframe id="audio_26371118" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26371118_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

###### 

 
