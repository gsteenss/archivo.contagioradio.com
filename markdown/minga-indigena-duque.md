Title: La minga indígena que reclama a Duque cumplir los acuerdos
Date: 2019-03-12 12:52
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, Duque, Minga, Ministra
Slug: minga-indigena-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Minga-Cauca.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Minga-Indígena-de-Cauca-e1562876410634.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contra Portada] 

###### [12 Mar 2019] 

Durante este martes los integrantes de los diferentes pueblos indígenas del suroccidente de Colombia, agrupados en el **Consejo Regional Indígena del Cauca (CRIC)**, esperan la llegada del presidente Iván Duque, en el marco de la minga indígena instalada desde el lunes, con la que **exigen el cumplimiento de los acuerdos alcanzados anteriormente con el Estado colombiano**.

Según información del CRIC, son **cerca de 15 mil integrantes de los pueblos indígenas de Cauca, Valle del Cauca, Nariño, Huila y Caldas que participan en la "Minga** Social por la Defensa de la Vida, el Territorio, la Democracia, la Justicia y la Paz"; la reunión se realiza en el Resguardo Indígena de Las Mercedes, municipio de Caldono (Cauca), hasta dónde esperan que llegue el Presidente para comprometerse en el cumplimiento de los acuerdos de Estado pactados en el pasado, y que hasta el momento han sido implementados.

<iframe src="https://co.ivoox.com/es/player_ek_33319822_2_1.html?data=lJigk56cdpOhhpywj5WbaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5ynca_Zzdjc0JC0pcTpxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo al **exgorbernador del Resguardo Las Mercedes, Nelson Pacue**, con la minga están reclamando su derecho a vivir en paz, a transitar libremente por su territorio ancestral, a la garantía de acceso a servicios como salud y educación.  (Le puede interesar: ["Asesinan al comunero indígena Alexander Cunda en Miranda, Cauca"](https://archivo.contagioradio.com/asesinan-el-lider-indigena-alexander-cunda-en-miranda-cauca/))

### **Duque tiene tiempo para Venezuela, pero no para atender a Colombia** 

El Presidente anunció que delegaría a Nancy Patricia Gutiérrez, ministra de interior, para atender los reclamos de los indígenas; no obstante, el CRIC afirmó que requerían la presencia de Duque en la zona, porque como explica Pacue, en el pasado se ha visto que los ministros no tienen la capacidad de solucionar los problemas que les son presentados. (Le puede interesar: ["Campesino fue víctima de la brutalidad del ESMAD en Cauca"](https://archivo.contagioradio.com/campesino-fue-victima-de-la-brutalidad-policial-del-esmad-en-el-cauca/))

Por esta razón, advierte el líder indígena, que están preparados para esperar lo que sea necesario para encontrarse personalmente con el Presidente, y recuerda que **"así como Duque saca tiempo para reunirse con los venezolanos, que lo haga con sus compatriotas"**. (Le puede interesar: ["Asesinan a Dilio Corpus Guetio, miembro de la guardia campesina en Cauca"](https://archivo.contagioradio.com/dilio-corpus-guetio/))

Adicionalmente, el CRIC ha afirmado que en caso de que Duque no asista al encuentro con los pueblos indígenas, se verán obligados a realizar obstrucciones sobre las vía Panamericana para que el Gobierno los escuche. Por su parte, la Federación Nacional Sindical Unitaria Agropecuaria (FENSUAGRO) está evaluando su posible participación en la minga, uniéndose al movimiento indígena.

### **Actualización, miércoles 13 de Marzo** 

Aunque el movimiento indígena envío el 9 de agosto una misiva al presidente Iván Duque para acordar un encuentro y abordar los temas planteados por la Minga, y a menos de 20 días repitió el ejercicio, este martes al Cauca solo llegaron ministros. Una hora después de su llegada, y tras comunicarse con el Presidente, anunciaron que Duque tenía la agenda ocupada, y no asistiría al llamado.

### **El Plan Nacional de Desarrollo y la articulación nacional** 

Según Giovani Yule, dinamizador del sistema de gobierno propio del CRIC, el Gobierno Nacional prometió recursos por cerca de 10 billones de pesos en el Plan Nacional de Desarrollo (PND) para garantizar los derechos de los diferentes pueblos indígenas que habitan en el país; sin embargo, Yule afirma que el proyecto de PND presentado al Congreso incumple con ese acuerdo, y reduce un 70% los recursos pactados.

<iframe src="https://co.ivoox.com/es/player_ek_33351013_2_1.html?data=lJigl5aUdZShhpywj5WcaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncajd0NvO0M6PndbgxoqfpZDIrc_Vzs7nw8nTtoznytjhx9LFb8XZjMzcxM7Jts_jjMnSzpCnlqq3joqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para enfrentar este incumplimiento, así como todos los artículos contrarios a la mayoría de la población nacional planteados por el PND, el movimiento indígena se está articulando con otros sectores sociales entre los que se encuentran ambientalistas, campesinos y estudiantes de todo el país; y han anunciado que están preparados para mantener su movilización hasta que logren el cumplimiento de los Acuerdos pactados con el Estado.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
