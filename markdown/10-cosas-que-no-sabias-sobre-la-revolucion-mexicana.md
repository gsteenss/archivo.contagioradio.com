Title: 10 cosas que no sabías sobre la revolución mexicana
Date: 2015-11-20 17:23
Category: El mundo, Movilización
Tags: Datos de la revolución mexicana, Emiliano Zapata, mexico, Porfirio Díaz, Revolución mexicana
Slug: 10-cosas-que-no-sabias-sobre-la-revolucion-mexicana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Las-10-cosas-que-no-sabías-sobre-la-Revolución-Mexicana1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [20 Nov 2015]

[Este 20 de noviembre se cumple el aniversario número 105 de un hecho político y social que determinó el rumbo del México moderno. La Revolución Mexicana fue la respuesta de una población inconforme, que a diferencia de la economía y la industria extranjera en la región, se veía sumida en la pobreza y falta de oportunidades.]

\
