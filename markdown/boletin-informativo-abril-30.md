Title: BOLETÍN INFORMATIVO ABRIL 30
Date: 2015-04-30 16:36
Author: CtgAdm
Category: datos
Tags: agente Naranja, angel molano, Bogotá, caravana de juristas, Escuadrón Antidisturbios, fecode, Frente Nacional de Liberación de Vietnam, Fundación Nicolás Neira, Gina Díaz, Guillermo García Realpe, Humedal La Conejera, Luis Alberto Grubert, maestros y maestras, Marta Lucia Zamora, Ministerio de Eduación, Napalm, nicolas neira, ONU, Partido Liberal, política pública de protección animal, Sangaris
Slug: boletin-informativo-abril-30
Status: published

[Noticias del día:]

<iframe src="http://www.ivoox.com/player_ek_4431843_2_1.html?data=lZmgk52Yd46ZmKiakpuJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjabGtsrgjJidj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-En una apuesta por la reconstrucción de la memoria, y el fortalecimiento de la lucha contra la brutalidad policial en Colombia, se conmemorarán los 10 años del asesinato de **Nicolás Neira** este 1ro de mayo en la carrera 7ma con calle 28, en el mismo lugar en el que fuera golpeado por el **Escuadrón Antidisturbios**. Habla **Angel Molano**, de la **Fundación Nicolás Neira**.

-Soldados Franceses habrían abusado sexualmente de 12 niños en la república **Centro Africana**, en el marco de la operación **Sangaris** entre 2013 y 2014. Los militares abusaron de los niños a quienes les ofrecían alimentación o dinero a cambio de permitir la violación. Según el informe de la **ONU**, filtrado por uno de sus trabajadores, los responsables del abuso serían 12 soldados que ya estarían identificados.

-**La Caravana de Juristas** presentó un informe que evalúa las condiciones de **DDHH** y la normatividad que protege a las y los abogados defensores de Derechos Humanos en Colombia, tras la visita realizada por más de 60 abogados y juristas del mundo. El informe afirma que en el territorio nacional (Bogotá, Bucaramanga, Cartagena, Medellín, Buenaventura, Cali y Pasto, entre otros), persisten los asesinatos, amenazas y hostigamientos hacia abogados y abogadas defensoras de Derechos Humanos. Habla la **Caravana de Juristas**.

-Hoy se conmemoran 40 años de la salida del ejercito de **Estados Unidos** derrotado por el **Frente Nacional de liberación de Vietnam**. La Operación militar que se prolongó por varios años dejó cerca de 4 millones de víctimas fatales, y secuelas en más de 6000 personas como consecuencia del uso del **Napalm** y el **agente Naranja.**

-**La política pública de protección animal** para el plan nacional de desarrollo 2014-2018, propuesta por el senador del **Partido liberal**, **Guillermo García Realpe**, fue aprobada en plenaria de cámara de representantes y por primera vez se incluye la defensa animal en un plan de desarrollo. Habla el senador **Guillermo García Realpe**, ponente de la iniciativa.

-Defensores y defensoras del **humedal La Conejera**, se encuentran preocupados por las últimas declaraciones de la secretaria general del distrito, **Marta Lucia Zamora**, por las cuales se criminaliza la protesta social. Habla **Gina Díaz,** defensora del humedal.

-La próxima semana se tiene previsto que cerca de 50000 **maestros y maestras** del país se tomen **Bogotá** con la instalación de campamentos en diversas partes de la ciudad pero que tendrán un sitio base en el **Ministerio de Eduación**. La tarde de este jueves 30 de Abril, iniciarían actividades con caravanas y marchas de antorchas en más de 400 municipios del país. **Luis Alberto Grubert**, presidente de **Fecode.**
