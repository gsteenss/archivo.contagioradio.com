Title: Cuatro heridos deja ataque de ESMAD a movilización por la Salud en Medellín
Date: 2015-08-20 14:47
Category: Movilización, Nacional
Tags: crisis de la salud, Derecho a la salud, EPS's, IPS Universitaria, Juan Manuel Santos, Ley 100, Ministerio de Salud
Slug: cuatro-heridos-deja-ataque-de-esmad-a-movilizacion-por-la-salud-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Crisis_salud_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: opinionysalud 

<iframe src="http://www.ivoox.com/player_ek_7212456_2_1.html?data=mJeelJmZeo6ZmKiak5iJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9bV1dfcjc3JtsrY0NiYxsrOpYzV1cbe18qPqMafpri6o6mPpYzh0NvWzs7epcTdhqigh6eXsozk0Neah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Marín, IPS Universidad de Antioquia] 

###### [20 Ago 2015] 

**J**[**uan Edgar Marín**, abogado y actual gerente de contratación de la IPS Universitaria de la Universidad de Antioquia, quien se encontraba junto a médicos, trabajadores, estudiantes y usuarios del sector de la salud, marchando pacíficamente en la ciudad de Medellín, fue agredido por el ESMAD.]

[La movilización que comenzó sobre las 10 de la mañana en el Parque de los Deseos, se encontró con un bloqueo por parte del Escuadrón Móvil Antidisturbios al llegar a la avenida Oriental  al que respondió de forma pacífica levantando las manos. No obstante el ESMAD resolvió luego de escasos 3 minutos **disolver la movilización disparando y lanzando gases lacrimógenos, que lesionaron gravemente a Juan Edgar Marín y a otros 3 integrantes de la marcha**.]

[Las lesiones de Marín se registran en su rostro y oído izquierdo y aunque actualmente no tiene un dictamen por parte de medicina legal, espera notificar formalmente al alcalde de Medellín y a la procuraduría, así como **entablar una demanda disciplinaria contra el comandante del ESMAD,** por la vulneración de los derechos  a la libre expresión y movilización.]

\[caption id="attachment\_12474" align="alignleft" width="207"\][![foto contagioradio.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Salud-Medellín.jpg){.wp-image-12474 width="207" height="148"}](https://archivo.contagioradio.com/cuatro-heridos-deja-ataque-de-esmad-a-movilizacion-por-la-salud-en-medellin/salud-medellin/) foto contagioradio.com\[/caption\]

[La movilización pretendía llamar la atención frente al actual deterioro en los servicios de salud de alta complejidad que presta la **IPS Universitaria** a usuarios del régimen subsidiado, en diferentes regiones del país como Santander, San Andrés y Antioquia, a causa de los \$320.000 millones que el Estado le adeuda.]

[Esta situación, que afecta tanto a proveedores como a trabajadores, a quienes se les retrasan los pagos por periodos de mínimo 2 meses, ha llevado a la **restricción de servicios médicos de alta complejidad** en detrimento de la atención hospitalaria y el derecho a la salud de cientos de usuarios.]

[Pese a las respuestas violentas por parte de las autoridades la Mesa Nacional por el Derecho a la Salud anuncia que **las jornadas de movilización continuarán** para seguir exigeindo el pago de la cartera hospitalaria y la reestructuración del actual sistema de salud en Colombia.]
