Title: Estudiantes logran compromiso de Min Salud para salvar el Hospital Universitario del Valle
Date: 2015-10-22 16:32
Category: Movilización, Nacional
Tags: Alejandro Gaviria, crisis de la salud, EPS's, HUV, Ley 100, Ministerio de Salud, Movilzación estudiantil, Red hospitalaria del Valle, Universidad del Valle
Slug: estudiantes-logran-compromiso-de-min-salud-para-salvar-el-hospital-universitario-del-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/SAlud-HUV-1024x576.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:contagioradio 

<iframe src="http://www.ivoox.com/player_ek_9132089_2_1.html?data=mpaglJWcfY6ZmKiak5mJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcriytjhx9fNs4zYxpDAw9HZqIznxpDQ0dLUttDhxtnSjdjTsNbXytTbw9ePp9Pd1M7gjcnJsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Natalia Lucio, U. del Valle] 

###### 22 oct 2015

Después de la jornada de movilizaciones y plantones que se hicieron en Bogotá por parte de la comunidad universitaria de la Universidad del Valle en Bogotá **exigiendo al ministro de salud, Alejandro Gaviria, el** rescate del único hospital de tercer nivel del Valle del Cauca, el HUV, se  **concretó un acuerdo para salvar la entidad.**

Natalia Lucio, integrante del equipo técnico de la representación al consejo superior de la Universidad del Valle, indica que **los estudiantes de las distintas sedes de la Universidad continuarán en cese de actividades** para que en las próximas elecciones para rector se pueda dar una elección directa, ya que este es también miembro de la junta directiva del HUV y a estado de acuerdo con intervenir el hospital, lo cual “da pie para una “intervención liquidatoria”.

Según los estudiantes la Universidad del Valle debe comprometerse a buscar “focos de gestión” para encontrar fondos para el hospital, sin privatización y además para que se modifique el acuerdo de estatuto general, que incluye entes externos a la universidad en la elección del rector, “*hemos trabajado con conciencia y compromiso*” en esta crisis que vive el hospital del Valle, afirma Lucio.

**Estos son los acuerdos con el ministro de Salud Alejandro Gaviria:**

1-El ministro Alejandro Gaviria descartó la liquidación o cierre del hospital y se comprometió a defender su carácter universitario y público.

2-Las partes acordaron la instalación de una mesa que iniciará durante la primera semana de noviembre, en la que el Ministerio de salud presentará un informe detallado de la cartera por parte de las entidades promotoras de salud (EPS) y de las Direcciones Territoriales de Salud (DTS) como respuesta al llamado de la comunidad universitaria sobre las deudas del hospital.

3-El ministerio de salud y de protección social continuará apoyando las gestiones de recuperación de la cartera a través de figuras como la compra de cartera que beneficie al HUV y brindará asistencia técnica en caso de que sea necesario.

4- Frente a la opción de intervención los estudiantes manifiestan su rechazo frente a esta figura, no obstante el Ministerio propone discutir de manera amplia los pros y contras de la misma, como una salida a las problemáticas del hospital. En la mesa se analizarán otras alternativas para salir de la crisis.

5-Esta mesa tendrá un carácter permanente : su prioridad al inicio será trabajar en una salida conjunta para el salvamento del HUV y posteriormente en la agenda se definirán otros temas de la red pública hospitalaria del Valle del Cauca.
