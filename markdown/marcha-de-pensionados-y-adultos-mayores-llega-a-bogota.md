Title: Marcha de pensionados y adultos mayores llega a Bogotá
Date: 2015-06-18 16:39
Category: Movilización, Nacional
Tags: Alianza Nacional de Pensionados, Marcha de pensionados Cali- Bogotá, Proyecto de Ley 183 de 2014, Robinson Emilio Masso pensionados, Situación crítica de Adultos Mayores en Colombia
Slug: marcha-de-pensionados-y-adultos-mayores-llega-a-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: alianzanacionaldepensionados.com] 

##### <iframe src="http://www.ivoox.com/player_ek_4659516_2_1.html?data=lZuim5qVeo6ZmKiak5aJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMbi1M7c0MbIs9Sf2pDOxtrQuNDnjNLO29TWqdSfzdHSycbSb8Tjz5Dg15DRpdPXycaYxsqPtI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [Robinson Emilio Masso, Organizador de la marcha] 

#### [18, jun, 2015] 

La **marcha por la Unidad y Dignidad del Pensionado Colombiano**, que partió el pasado 8 de junio desde la ciudad de Cali, llegó en la mañana de este jueves a Bogotá, con el objetivo de **presentar un pliego petitorio** al presidente Juan Manuel Santos ante la **critica situación** que atraviesa el **sector pensional**, así como los **adultos mayores** del país ([Pensionados marchan desde Cali hacia Bogotá](https://archivo.contagioradio.com/pensionados-marcharan-desde-cali-hasta-bogota/)).

Durante el recorrido por **15 municipios de 15 departamentos** del país hacia la Capital de la República, los marchantes encontraron eco de sus **inconformidades** en otros pensionados y adultos de la tercera edad, que al igual que ellos padecen por el **incumplimiento de las promesas** hechas en campaña por el primer mandatario de los colombianos.

De acuerdo con **Robinson Emilio Masso**, presidente de la **Corporación de jubilados y pensionados del Cali** y miembro de C**omité ejecutivo de la Alianza Nacional de pensionados** de Colombia, la manifestación es motivada, entre otras razones, por el incumplimiento en la **reducción de los aportes en salud** del **12% al 4%** del ingreso de la respectiva mesada pensional, incluído en el **Proyecto de Ley 183** de 2014,  que fue archivado el día de ayer en el Congreso de la República.

El líder de la marcha manifiesta además que el **85% de los pensionados en Colombia, devengan menos de 2 salarios mínimos** y los cerca de **5 millones de adultos mayores** reciben subsidios que están entre los **110.000 y 170.000** pesos cada dos meses de manera desigual en las regiones; condiciones que **"**no compensan el mínimo vital, que nos reivindiquen bienestar y calidad de vida**"** agrega Masso.

Los pensionados también reclaman el **incremento anual de las mesadas** con el porcentaje más favorable, entre el incremento del IPC, y el **incremento del salario mínimo**, la **no** aplicación de **cuotas moderadoras** a los beneficiarios, pero sobre todo que se instale una **mesa de concertación con el gobierno naciona**l, en la que se discutan estos y otros temas que los afectan en su cotidianidad.

La agenda de los manifestantes inició hoy en horas de la mañana con una marcha hacia la plaza de Bolívar en el centro de la ciudad y una rueda de prensa para exponer las problemáticas ante los medios, después del medio día un cacelorazo en el Ministerio de Hacienda por parte de un grupo de 600 pensionados como reclamo ante la negligencia de la cartera ministerial.

Las actividades continuarán el viernes en horas de la mañana, con una audiencia pública en el Congreso de la República, donde se espera contar con la presencia de las delegaciones participantes, algunos Congresistas y Senadores que viene acompañando el proceso, y será televisado por el Canal del Congreso. En horas de la tarde, los manifestantes esperan sentarse a dialogar con los Ministros de Educación, Hacienda, Justicia, Trabajo y Salud o con sus representantes y resolver de alguna manera el pliego de peticiones presentado.

La organización de la manifestación **aspira a convocar cerca de 5 mil pensionados**, parte de ellos provenientes de las delegaciones de Cali, Barranquilla, Santa Marta, Putumayo, Villavicencio, Santanderes y Antioquia se unen en la capital a los marchantes quienes esperan además que ciudadanos y ciudadanas bogotanos se unan de manera solidaria a la causa, por medio de una firmatón que ayude a respaldar el pliego petitorio presentado al gobierno nacional.
