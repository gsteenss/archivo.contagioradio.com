Title: "Siempreviva", representación de aquellos que nunca regresaron del Palacio
Date: 2015-10-01 16:35
Author: AdminContagio
Category: 24 Cuadros
Tags: Andrés Parra, CMO Producciones, Desaparecidos palacio de justicia, Enrique Carriazo, Klych López director, Laura García, Laura Ramos, Película Siempreviva, Siempre Viva pelicula
Slug: siempreviva-representacion-de-aquellos-que-nunca-regresaron-del-palacio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/unnamed-5.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [30 Sep 2015] 

Los acontecimientos relacionados con la toma y posterior retoma del Palacio de Justicia, ocurrida entre el 6 y 7 de noviembre de 1985, han sido objeto de múltiples investigaciones y debates, a partir de las cuales se han publicado informes, libros, novelas, documentales, películas, obras teatrales y performáticas.

Cerca de cumplirse 30 años, las diferentes movilizaciones de familiares de los desaparecidos, y de aquellos que se suman a la cruzada por encontrar un poco de verdad en un mar de dilaciones y mentiras, aparecen una vez más en el panorama nacional, en un ejercicio de memoria al que la cultura y el arte, como manifestaciones políticas que son, no pueden estar al margen.

Es precisamente la "Siempreviva", reconocida creación teatral de Miguel Torres estrenada en 1994 ambientada en el antes, durante y después de la toma, la que inspira la producción homónima que desde hoy podrán, y deberían, ver todos los colombianos y colombianas en las salas de cine, para entender por qué "este no es un país serio".

Producida por CMO, (compañía reconocida, entre otras, por producciones como "La promesa" que aborda el tema de la trata de personas), la versión cinematográfica "Siempreviva" se gesta en la mente de dos experimentadas productoras, Clara María Ochoa y Ana Piñeres, y el importante trabajo de Manuel Arias en la adaptación del guión. (Lea tambien "[Siempreviva" una película ambientada en la toma del Palacio de Justicia](https://archivo.contagioradio.com/siempreviva-una-pelicula-ambientada-en-la-toma-del-palacio-de-justicia/)")

Para Klych López, director de la cinta, Siempreviva "es el sentir de esas personas que tienen un ser que no se sabe si esta vivo o muerto", una producción, su primera en cuanto a cine se refiere,  que le  representó grandes retos y responsabilidades a nivel profesional partiendo del hecho de "tomar un texto tan grande como el de Miguel Torres, y lo que significo para el teatro colombiano".

La película recurre a un falso plano secuencia, utilizando pequeñas transiciones para denotar el paso del tiempo, recurso que de acuerdo con el director, refleja lo que vivió el país durante esas horas "pensamos que por el tema, por la forma narrativa, por la sensibilidad y todo lo que dice Siempreviva, deberíamos respetar el tiempo real" teniendo en cuenta que "lo que le llego a los colombianos fueron veintipico de horas ametralladas de información, no habia manera de parar". (Vea [el tráiler completo de Siempreviva](https://archivo.contagioradio.com/siempreviva-anuncia-fechas-de-estreno-y-presenta-trailer-oficial/))

El título de la cinta, hace referencia a Julieta (Andrea Gómez), joven estudiante de derecho, que lucha por sacar adelante a su familia, encontrando la oportunidad de trabajar como cajera en la cafetería del Palacio de Justicia; una figura que representa a todos los desaparecidos, y que con su ausencia marca un antes y un después para los demás personajes de la cinta.

\[caption id="attachment\_15104" align="aligncenter" width="547"\]![Siempre viva](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Siempre-viva.png){.wp-image-15104 width="547" height="307"} Andrés Parra, Laura Ramos y Enrique Carriazo en "Siempreviva"\[/caption\]

Vale la pena resaltar la interpretación sincera y desgarrada que hace Laura García, en representación de esas madres que se levantan día tras día con la convicción de encontrar a sus hijos, con un hilo de esperanza al que se aferran frente a la indiferencia gubernamental y el olvido colectivo, de un país cuya memoria es de corto plazo.

Andrés Parra (Sergio) y Enrique Carriazo (Don Carlos), se roban en varios pasajes el protagonismo de la película, en un juego entre lo cómico y lo dramático partiendo de la historia personal de cada personaje, y con un discurso claro frente a la manera en que debería resolverse el conflicto en Colombia, reflejo de la polarización que ha sido constante en la historia nacional.

[Siempreviva ofrece actuaciones veraces, comprometidas y desgarradoras, propias de intérpretes comprometidos en lo que hacen. En la película hay diálogos tan vigentes que hasta asustan, porque bien podría decirse que estamos en una nación que se ha quedado estancada en muchas cosas y ha dejado de solucionar problemas solo para disfrazarlos.]

[2015 ha sido un año rico en buenas producciones cinematográficas y aunque para algunos no es decoroso comparar, la ópera prima de Klych López está al nivel de películas que nos han representado en festivales este año. Siempreviva es un largometraje merecedor de la atención del público, participando activamente  bien sea riendo, llorando o recordando.]
