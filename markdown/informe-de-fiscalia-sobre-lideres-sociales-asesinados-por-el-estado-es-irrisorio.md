Title: Informe de Fiscalía sobre líderes sociales asesinados por el Estado es irrisorio
Date: 2019-01-14 16:59
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: colombia, Fiscalía General de la Nación, Jurisdicción Espacial de Paz, lideres sociales
Slug: informe-de-fiscalia-sobre-lideres-sociales-asesinados-por-el-estado-es-irrisorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/crimenes-de-lideres-sociales-por-el-estado-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Ene 2019] 

La Fiscalía General de la Nación hizo entrega oficial a la Jurisdicción Especial para la Paz de un informe que contiene **184 investigaciones de asesinatos a líderes sociales por parte del Estado, presentadas desde 1985 hasta el 2016.** Hecho que para Franklin Castañeda, presidente del Comité de Solidaridad Presos Políticos, es un avance, pero demuestra el irrisorio trabajo que ha desempeñado esa institución en el marco del conflicto armado y  la tarea de acabar con la impunidad.

Castañeda aseguró que si se analiza el periodo de tiempo en el cual se circunscribe el informe, se podría afirmar que son más de 5 mil las personas asesinadas, si se tiene en cuenta a víctimas de la Unión Patriótica y ejecuciones Extrajudiciales "basta recordar que en el **año 2002 más de 1.200** personas que se presume tenían algún tipo de liderazgo, fueron asesinadas en Colombia". (Le puede interesar:["Tutela ordena al Estado cumplirle a vícitmas de Operación Génesis"](https://archivo.contagioradio.com/fallo-ordena-al-estado-cumplir-con-la-reparacion-a-victimas-de-operacion-genesis/))

En ese mismo sentido, señaló que la entrega de tan pocas investigaciones en el informe evidencia el accionar de la Fiscalía durante este tiempo, en el que **se ha establecido un 99% de impunidad en el esclarecimiento de los asesinatos hasta el 2016**, además cuestionó las labores del ente investigador a la hora de encontrar a los autores  intelectuales de estos homicidios, hecho que ha impedido constatar los intereses tras los asesinatos, la sistematicidad en las acciones violentas y el papel que tuvo la Fuerza Pública.

### **La responsabilidad del Estado en la conformación del paramilitarismo** 

El informe entregado por la Fiscalía menciona que hay 1.602 personas judicializadas como presuntos responsables de los asesinatos, de las cuales **484 corresponden a agentes del Estado, mientras que 444 a integrantes de grupos de autodefensa**, situación que para Castañeda es producto de la falta de reconocimiento por parte del Estado de su responsabilidad en el surgimiento y desarrollo de las estructuras paramilitares.

"Cuando hablamos de 444 casos de homicidios a líderes sociales bajo responsabilidad de grupos paramilitares, estamos hablando realmente de una responsabilidad del Estado, no solo porque no protegió a los líderes, sino porque está demostrado que el Estado creó los grupos paramilitares, que fue cómplice en el desarrollo de sus acciones y  que en el caso de homicidios selectivos, se evidenció que los paramilitares contaron con información de inteligencia estatal" afirmó Castañeda.

### **La JEP: una esperanza para la verdad** 

Castañeda manifestó que espera que la Jurisdicción Especial para la Paz logre completar este informe entregado por la Fiscalía, con el trabajo que han venido desarrollando organizaciones defensoras de derechos humanos, para abrir un gran caso que permita develar la relación entre el **Estado y los homicidios a sindicalistas y líderes sociales en el país. **

<iframe id="audio_31474074" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31474074_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
