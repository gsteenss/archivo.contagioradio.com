Title: Organizaciones defienden el enfoque étnico en acuerdo de paz
Date: 2017-09-20 13:31
Category: Nacional
Tags: acuerdo de paz, Comisión Étnica
Slug: gobierno_enfoque_etnico_acuerdos_de_paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/pueblos-etnicos-e1504805616561.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [20 Sep 2017] 

[“El gobierno ha sido negligente en la inclusión del enfoque étnico en la implementación de los acuerdos”, así lo denuncian desde la Comisión Étnica para la implementación del acuerdo de paz entre el gobierno y las FARC. De acuerdo con Charo Mina, integrante del proceso de Comunidades Negras, no se ha evidenciado la voluntad política por parte del gobierno ni de la Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Final (CSIVI) para hacer efectivo ese enfoque.]

[Son dos los factores críticos. Mina explica que la **Instancia Especial de Alto Nivel con Pueblos Étnicos para el Seguimiento de la Implementación del Acuerdo Final** que no está cumpliendo con velar para que transversalmente se tenga en cuenta a los pueblos indígenas y afro. Dicha instancia tiene la responsabilidad de interlocutar con la CSIVI, para orientar y monitorear la implementación, sin embargo no ha funcionado.]

[El otro tema al que no se le ha dado cumplimiento tiene que ver con el plan de implentación del acuerdo de paz. Charo Mina señaló que es importante porque es la ruta para que una vez se ponga en marcha lo pactado, también se cuente con indicadores de resultados que permitan evaluar como se implementa el acuerdo en los territorios étnicos.]

**"Hay una intensión de exclusión de ignorar la presencia y legitimidad de la Instancia y con ello a las comunidades étnicas, y**a que los pueblos seguimos siendo una amenaza para las intensiones extractivista del gobierno sobre los territorios", expresa la integrante del PCN.

### **¿Cómo debería funcionar?** 

La CSIVI debe llamar a la instancia de alto nivel para tener la orientación de cómo proceder y garantizar instancias de participación de los pueblos étnicos. El problema es que en el primer periodo de Fast track, nunca llegó a las organizaciones indígenas y afrodescendientes  las leyes y decretos del gobierno, que además tenían serías consecuencias para el pueblo afrodescendiente respecto a temas como la consulta previa y otros temas.

### **La Respuesta del gobierno** 

[Ante la situación, y después de muchas solicitudes para que hubiera una respuesta sobre tal situación de incumplimiento, **apenas este 13 de septiembre las organizaciones étnicas lograron una reunión con la CSIVI y el gobierno.** Allí se estableció que se debe incluir los indicadores étnicos en el plan de implementación, para que de esa manera se garantice el funcionamiento de la instancia, fluya la comunicación con la CSIVI y así se garantice la participación de las personas en las regiones para discutir los indicadores.]

El ministro del posconflicto, Rafael Pardo será quien deba garantizar los recursos para que la instancia se reúna y la CSIVI responda a las solicitudes de las organizaciones étnicas. Con respecto al plan marco de implementación se espera que se amplíe la participación y se **incluyan los indicadores étnicos con respectivos sus recursos que serían definidos el próximo 10 de octubre.** No obstante, los pueblos afro e indígenas afirman que si el gobierno incumple con lo pactado planearán una jornada de movilizaciones.

<iframe id="audio_21001535" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21001535_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
