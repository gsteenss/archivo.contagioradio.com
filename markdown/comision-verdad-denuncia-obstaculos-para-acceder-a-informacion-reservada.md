Title: Información de inteligencia y contrainteligencia no ha sido entregada a la Comisión de la Verdad
Date: 2020-10-02 10:02
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Comisión de la Verdad, víctimas
Slug: comision-verdad-denuncia-obstaculos-para-acceder-a-informacion-reservada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/48417385526_11ab065890_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de la rendición de cuentas que realiza la Comisión por el Esclarecimiento de la Verdad, el comisionado Alejandro Valencia expresó la preocupación que existe ante la demora de varias instituciones para atender las solicitudes de acceso a información radicadas por el [organismo](https://comisiondelaverdad.co/) a poco más de 14 meses que finalice su mandato.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Valencia resaltó la facultad que tiene la Comisión para **acceder a información pública y reservada, incluidos documentos de inteligencia y contrainteligencia sin que se le pueda oponer reserva alguna,** en especial a información que pueda encontrarse en instituciones con relación a posibles violaciones a los DD.HH, infracciones al Derecho Internacional Humanitario y así garantizar los derechos de las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Nos resta un escaso semestre para recibir los informes que voluntariamente entreguen instituciones, víctimas y grupos de interés, recaudar información publica y analizar las versiones que serían las que respalden nuestro informe final", recordó el comisionado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aseguró que la falta de entrega de datos conlleva a diversos obstáculos como también lo alertó la Corte Constitucional con relación a la carencia de información oficial; tal como se vio en otros mandatos de Comisiones de la Verdad en el mundo. Dichos registros oficiales del Estado no solo "representan el derecho que tienen las víctimas de conocer la verdad de los hechos del conflicto armado, además representan fuentes primarias de información y contrastación".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El acceso a la información es vital para que las víctimas conozcan la verdad

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Algunas de las dificultades puestas en conocimiento a la Procuraduría son la oposición de reserva de información por parte de organismos que custodian información reservada "llama la atención que organismos de inteligencia dilaten la respuesta de solicitudes de acceso para responder que la información pública a su cargo es reservada", contrariando lo que ordena la Corte Constitucional.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComisionVerdadC/status/1311331133028732928","type":"rich","providerNameSlug":"twitter"} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComisionVerdadC/status/1311331133028732928

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Además, señala que existe una falta de congruencia entre la información solicitada y la que es recibida, "se requiere información precisa sin embargo han sido encontradas omisiones a las indicaciones que ha pedido la Comisión de la Verdad", explica el comisionado Valencia. [(Le puede interesar: Leyner Palacios representará el legado de las víctimas ante la Comisión de la Verdad)](https://archivo.contagioradio.com/leyner-palacios-representara-el-legado-de-las-victimas-ante-la-comision-de-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otros problemas obedecen a respuestas elusivas, señalando que una vez vencido los plazos legales se le informa a la Comisión que debe dirigirse a otra autoridad o que se ha dado traslado a otra dependencia de la información o "que ha juicio del funcionario no resulta conveniente entregar la información".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El más preocupante de dichos obstáculos es la inexistencia de archivos de derechos humanos, "de manera puntual por parte de organizaciones que tienen como función la protección de los ciudadanos" que han expresado de forma verbal cómo determinados archivos que dan cuenta de violaciones de DD.HH han sido destruidas o incineradas "por ordenes de directivas, como práctica institucionalizada o que fueron destruidos en medio del conflicto armado".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El comisionado destacó que aunque a la fecha se han recibido, alrededor de 41 informes de instituciones tales como Fiscalía General de la Nación y las Fuerzas Militares, aún hay instituciones que no han facilitado el acceso a la información por lo que "se deberán iniciar las acciones administrativas y legales pertinentes para hacer valer los derechos de las víctimas".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
