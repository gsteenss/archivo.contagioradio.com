Title: EPM reanuda ilegalmente obras en Hidroituango
Date: 2016-03-10 17:00
Category: Ambiente, Nacional
Tags: ANLA, EPM, ESMAD, Hidroituango
Slug: epm-incumple-orden-de-la-anla-sobre-suspension-en-obras-de-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/hidroituango-e1454622622131.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Movimiento Ríos Vivos 

<iframe src="http://co.ivoox.com/es/player_ek_10755120_2_1.html?data=kpWkl5qVdpGhhpywj5WYaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncabErpDW0MjZsdHgxpDc1MnJsozYxpDZw5Clkq21jNjcxNfJb9Tp1NXS0NjNaaSnhqeg0JDIqYzjw9fOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Isabel Cristina Zuleta 

###### [10 Mar 2016]

Este jueves el **Movimiento Ríos Vivos de Antioquia denunció actividades de ingreso de combustible, carro tanques, maquinaria y agua en la obra** del[proyecto hidroeléctrico Hidroituango](https://archivo.contagioradio.com/?s=hidroituango)de Empresa Públicas de Medellín, EPM, pese a que el pasado 15 de febrero del 2016 la Agencia Nacional de Licencias Ambientales, ANLA, ordenó la suspensión del proyecto.

De acuerdo con Isabel Cristina Zuleta, vocera del movimiento, campesinos de la comunidad empezaron a notar varias irregularidades, como la entrada de personal y maquinaria, así pudieron constatar que se trataba de una contratación nueva, que pasa por encima de la orden de la ANLA, y con la que se pretende continuar la obra.

La suspensión se dio a través de la [Resolución n° 0027](https://archivo.contagioradio.com/anla-suspende-actividades-de-hidroituango-por-danos-socio-ambientales/) de la ANLA que ordenaba, como medida preventiva, una suspensión de 3 meses en los cuales se debía anular cualquier actividad en Puerto Valdivia, con el fin de que se reconfigure la planeación a nivel ambiental, teniendo en cuenta que la empresa incumplió las obligaciones establecidas en la licencia ambiental.

Según la denuncia de la vocera del Movimiento Ríos Vivos de Antioquia, **ya se organizaron varios escuadrones del ESMAD puesto que la comunidad **se muestra indignada con el restablecimiento de las obras.

El Movimiento solicitó una respuesta por parte de EPM y a la respectiva constructora, al igual que la espera de alguna declaración por parte de ANLA, pues la comunidad no conoce ninguna resolución que reanude el desarrollo de la obra. **“Lo único que les dijeron a los compañeros en terreno, es que tenían que trabajar por encima de quien fuera, no podían perder más dinero… no podían perder más tiempo”,** dice Isabel Cristina Zuleta.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
