Title: Empresa Riopaila quiere la patente de la Panela: paneleros
Date: 2020-08-20 16:17
Author: AdminContagio
Category: Actualidad, Nacional
Tags: campesinos, Coordinador Nacional Agrario, paneleros, Riopaila, soberanía alimentaria
Slug: empresa-riopaila-quiere-la-patente-de-la-panela-paneleros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Paneleros-denuncian-abusos-de-Riopaila.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Por medio del Coordinador Nacional Agrario -[CNA](https://www.cna-colombia.org/ingeniero-de-riopaila-quiere-poner-a-pagar-a-los-paneleros-por-el-proceso-de-producir-panela/)-, **productores paneleros denunciaron que a través de una patente el ingeniero Jorge Enrique González Ulloa de la industria Riopaila, estaría buscando beneficiarse económicamente de la producción que desde hace muchos años vienen realizando miles de campesinos y campesinas en Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hace algunos días el ingeniero González Ulloa, anunció la certificación y patente por parte de  autoridades en Estados Unidos de un método, que según él, «*procesa la caña de azúcar maximizando la preservación de policosanoles durante la producción de un producto que baja el colesterol».* (Le puede interesar: [Paneleros irán a Paro Nacional el próximo 28 de junio](https://archivo.contagioradio.com/paneleros-iran-a-paro-nacional-el-proximo-28-de-junio/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noraldo Nariño, secretario general del CNA, señaló que más allá de la terminología técnica, **el método reseñado no es otra cosa que lo que tradicionalmente han venido haciendo los campesinos en Colombia para obtener la panela durante todos estos años**, y que lo único que se quiere con la inscripción de la patente es «*apropiarse individualmente de un proceso que ha sido perfeccionado colectivamente,* *para lucrarse*».

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Lo único nuevo es que con esta patente los grandes monopolios e industriales, buscan que los pequeños productores de panela tengan que pagarles por el derecho de propiedad que adquieren con la privatización del proceso»
>
> <cite>Noraldo Nariño, Secretario General del Coordinador Nacional Agrario</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según el CNA, **la patente perjudicaría a más de 350 mil familias paneleras en el país, afectando más de 1´700.000 empleos directos** y perjudicando gravemente la economía campesina de pequeños y medianos productores en más de 29 departamentos a nivel nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Noraldo Nariño, afirmó que la implementación de esta patente a nivel nacional «*afectaría esa soberanía alimentaria por la que tanto han luchado los campesinos*» y vulneraría los derechos del campesinado colombiano como «*se ha hecho históricamente con la aprobación de tratados de libre comercio*». (Le puede interesar: ["En Colombia estamos importando hasta la papa"](https://archivo.contagioradio.com/en-colombia-estamos-importando-hasta-la-papa/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los paneleros también denuncian el acaparamiento de tierras baldías por parte de Riopaila

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el Coordinador Nacional Agrario, **«*la industria Riopaila estuvo involucrada en el año 2013 en el acaparamiento ilegal de más de 42 mil hectáreas de tierra en el departamento del Vichada*** *buscando violar la normatividad colombiana que por ley debe entregar terrenos baldíos de la nación a las comunidades campesinas sin tierra o con tierra insuficiente*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia, citada por el CNA, la empresa azucarera, constituyó 27 sociedades por acciones simplificadas -S.A.S-, para la compra de las 42 mil hectáreas; a las cuales les prestó el dinero para comprar las tierras; y a su vez, según se afirma en la denuncia, dichas empresas le arrendaron los terrenos a Riopaila por el término de 30 años y por el mismo valor en el que se adquirieron. (Lea también: [Cerca de 19 empresas son las que han acaparado más tierras en Colombia](https://archivo.contagioradio.com/cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
