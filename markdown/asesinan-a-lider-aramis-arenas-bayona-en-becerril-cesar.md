Title: Asesinan a líder Aramis Arenas Bayona en Becerril, Cesar
Date: 2020-05-19 20:15
Author: CtgAdm
Category: Actualidad, DDHH
Tags: cesar, Defensor de DD.HH., Líder Social
Slug: asesinan-a-lider-aramis-arenas-bayona-en-becerril-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Lider-Aramis-Arenas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Cortesía {#foto-cortesía .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este martes 19 de mayo, organizaciones sociales lamentan el asesinato del líder Aramis Arenas Bayona en su finca, corregimiento de Esatdos Unidos en el Becerril, Cesar. De acuerdo a la Fundación Redprodepaz, Aramis era uno de los líderes más emblemáticos del proceso que impulsaba un proyecto de turismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De hecho, en su finca ubicada en la vereda Canada, del corregimiento de Estados Unidos, venía trabajando en las adecuaciones para ponerla al servicio del turismo. (Le puede interesar: ["Freddy Angarita y Enrique Oramas, nuevos líderes que pierde Colombia por la violencia"](https://archivo.contagioradio.com/freddy-angarita-y-enrique-oramas-nuevos-lideres-que-pierde-colombia-por-la-violencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La [Mesa Departamental](https://comosoc.org/mesa-ddhh-cesar-rechaza-asesinato-aramis-arenas-bayona/) de Derechos Humanos y Territorios del Cesar señala que Aramis "desde el año 2016 venia defendiendo la autonomía territorial, alzando la voz de protesta en contra de la judicializaciones de líderes campesinos". En un comunicado, la Organización pidió celeridad en la investigación de su caso, y se solidarizó con la familia del líder.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Más de 100 líderes sociales asesinados en 2020

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado domingo se confirmaba el homicidio de Enrique Oramas, líder ambientalista que trabajaba por proteger los farallones de Cali de la minería ilegal. Según los registros de [INDEPAZ](http://www.indepaz.org.co/paz-al-liderazgo-social/) Oramas fue el líder número 100 asesinado en lo corrido de 2020, y con los homicidios de Freddy Angarita, [Emerito Digno](https://archivo.contagioradio.com/ejercito-habria-asesinado-a-emerito-digno-buendia-ascamcat/), Alirio García y Aramis Arenas la crifra aumentaría a 104.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La cuarentena nacional se decretó el pasado 24 de marzo, y desde esa fecha hasta ahora, 27 líderes sociales o defensores de derechos humanos han perdido la vida en medio de hechos de violencia. En Cesar este año se tiene registros del asesinato de Henry Wilson Cuello Villareal, el pasado 10 de enero en Chiriguana.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78979} /-->
