Title: ¿Por qué se suspendió la audiencia contra Jesús Santrich?
Date: 2018-12-03 15:20
Author: AdminContagio
Category: Nacional, Política
Tags: Jesús Santrich
Slug: por-que-se-suspendio-la-audiencia-contra-jesus-santrich
Status: published

###### Foto: Contagio Radio 

###### 03 Dic 2018 

La audiencia en la que se definiría la pérdida de investidura de[ Jesús Santrich][ ]y que estaba programada para este lunes 3 de diciembre nunca tuvo lugar pues la Fiscalía no autorizó el traslado del dirigente político de FARC desde la cárcel La Picota hasta el lugar donde se realizaría la audiencia, por tal motivo el **Consejo de Estado** decidió aplazarla.

Por su parte, desde el ente investigador sostienen que el traslado no pudo realizarse por motivos de seguridad, razón por la cual el **INPEC** no transportó a Santrich. A pesar de ello, la Fiscalía dice haber entregado a tiempo la autorización para que compareciera de forma virtual, lo cual tampoco fue posible.

A través de un audio difundido en las últimas horas, **Jesús Santrich expresó que tiene plena confianza en los integrantes del Consejo de Estado,** quienes "harán la valoración acertada de las circunstancias" y que se presentará ante el organismo como un revolucionario en proceso de reincorporación comprometido con la reconciliación. **[(Le puede interesar Captura de Jesús Santrich es un montaje: Iván Márquez)](https://bit.ly/2AKtAly)**

De igual forma señaló que desde que fue capturado se cometieron en su contra "numerosos atropellos" de los cuales dará cuenta durante la audiencia, manifestando que las evidencias presentadas "se han ido derrumbando, queda demostrado que el fiscal no es un acusador sino un perseguidor y le está haciendo un terrible daño a la paz de Colombia”.

Santrich fue firme al aclarar que nunca "reconocerá culpabilidades sobre hechos que no ha cometido" a cambio de una curul en la Cámara de Representantes o de la no extradición, probabilidad en la que fue enfático, no sucederá. **Se estima que la audiencia tendría lugar el próximo 21 de enero de 2019.**

###### Reciba toda la información de Contagio Radio en [[su correo]
