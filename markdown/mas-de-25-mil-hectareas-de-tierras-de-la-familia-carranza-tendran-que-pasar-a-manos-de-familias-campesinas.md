Title: Más de 25 mil hectáreas de tierras de la familia Carranza tendrán que pasar a manos de familias campesinas
Date: 2014-12-26 15:51
Author: CtgAdm
Category: Otra Mirada
Slug: mas-de-25-mil-hectareas-de-tierras-de-la-familia-carranza-tendran-que-pasar-a-manos-de-familias-campesinas
Status: published

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsTry4ke.mp3)  
La Corporación Jurídica Yira Castro y la comunidad de El Porvenir de Puerto Gaitán, emprendieron una acción jurídica para recuperar **25.930 hectáreas, que correspondían a 27 adjudicaciones que se hicieron ilegalmente, quebrantando la Ley Agraria de 1992**. El INCODER expidió la resolución 64-23 en que se establecía que las hectareas fueron adquiridas por Víctor Carranza, quien utilizando a personas de su entorno (como trabajadores de sus haciendas), se hizo a la adjudicación de propiedades que debían parar en manos de campesinos pobres o sin tierras, y posteriormente las englobó a través de empresas. Ante la no apelación de la resolución, el 30 de octubre se puso en firme esa decisión.  
Para el Senador Iván Cepeda, si bien esto representa** un adelanto en la batalla por la restitución de tierras adquiridas por el paramilitarismo, las acciones especulativas y el testaferrato, esta es apenas una pequeña parte de la gigantesca tarea en contra del despojo de tierras**. “Estos avances podrían desplomarse con la Ley de Baldíos que promueve el gobierno” enfatizó Cepeda Castro.
