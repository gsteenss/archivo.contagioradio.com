Title: Último foro abordará los puntos más álgidos del proceso de paz
Date: 2016-01-28 17:04
Category: Nacional, Paz
Tags: FARC, ONU, Paramilitarismo, proceso de paz, Universidad Nacional
Slug: foro-sobre-proceso-de-paz-universidad-nacional-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/foro-universidad-nacional-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Primicia Diario 

<iframe src="http://www.ivoox.com/player_ek_10235083_2_1.html?data=kpWflZqUfJShhpywj5aWaZS1lZaah5yncZOhhpywj5WRaZi3jpWah5yncYa3lIqmo9HYrc7jjMvc1NSPpcPj08nO1Iqnd4a1kpDZ0diPtNbi1dTgjdKJh5SZopbgjYqnd4a1ktHUy8nTt4zYxtGY0tfTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carlos Medina Gallego] 

###### [28 Ene 2016.] 

**Por solicitud de la delegación de paz de las FARC al gobierno nacional, el próximo 8 y 9 de febrero** la Oficina de la Organización de Naciones Unidas en Colombia de la mano del Centro de Pensamiento y Seguimiento al Diálogo de Paz de la Universidad Nacional, realizarán ** **un foro frente a **los puntos 3 “Fin del Conflicto” y 6 “Implementación, Verificación y Refrendación”** de la Agenda del proceso de paz.

La iniciativa se consolida, luego de que el pasado 22 de enero, la Mesa de Conversaciones del Gobierno Nacional y las FARC-EP que sesiona en La Habana, emitiera un comunicado en el que le solicitan la realización de un último foro, teniendo en cuenta el momento final que atraviesa el [proceso de paz,](https://archivo.contagioradio.com/?s=proceso+de+paz) en el que se encuentran en discusión los puntos “**más complejos en términos jurídicos, técnicos y políticos”,** señala Carlos Medina Gallego, miembro del Centro de Pensamiento y Seguimiento al Diálogo de Paz de la Universidad Nacional.

El profesor Medina, indica que el foro tiene como objetivo que la sociedad civil, plataformas, organizaciones sociales, políticas y de Derechos humanos, pueda aportar elementos sustanciales para nutrir los puntos más álgidos que quedan por desarrollar en la Habana

De acuerdo al comunicado conjunto de la ONU y el Centro de Pensamiento y Seguimiento al Diálogo de Paz de la Universidad Nacional, los siguientes serán los puntos a tratar en el foro que se realizará en el **Hotel Tequendama.**

### **Fin del conflicto** 

Proceso integral y simultáneo que implica:

1.  Cese al fuego y de hostilidades bilateral y definitivo.
2.  Dejación de las armas. Reincorporación de las FARC-EP a la vida civil -en lo económico, social y político-, de acuerdo a sus intereses.
3.  El Gobierno Nacional coordinará la revisión de la situación de las personas privadas, procesadas o condenadas por pertenecer o colaborar con las FARC-EP.
4.  En forma paralela, el Gobierno Nacional intensificará el combate para acabar la organizaciones criminales y sus redes de apoyo, esto incluye la lucha contra la corrupción y la impunidad, en particular contra cualquier organización responsable de homicidios y masacres o que atente contra defensores de derechos humanos, movimientos sociales o movimientos políticos.
5.  El Gobierno Nacional revisará y hará las reformas y los ajustes institucionales necesarios para hacer frente a los retos de la construcción de la paz.
6.  Garantías de seguridad.
7.  En el marco de lo establecido en el punto 5 (Víctimas) de este acuerdo se esclarecerá, entre otros, el fenómeno del paramilitiarismo.

### **Implementación, verificación y refrendación** 

La firma del Acuerdo Final da inicio a la implementación de todos los puntos acordados.

1.  Mecanismos de implementación y verificación
2.  Sistema de implementación, con especial importancia a las regiones
3.  Comisiones de seguimiento y verificación
4.  Mecanismos de resolución de diferencias

Estos mecanismos tendrán capacidad y poder de ejecución y estarán confirmadas por representante de las partes y de la sociedad según el caso.

2.  Acompañamiento internacional
3.  Cronograma
4.  Presupuesto
5.  Herramienta de difusión y comunicación
6.  Mecanismo de refrendación de los acuerdos

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
