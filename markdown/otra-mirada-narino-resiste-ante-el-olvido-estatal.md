Title: Otra Mirada: Nariño resiste ante el olvido estatal
Date: 2020-08-10 16:59
Author: PracticasCR
Category: Nacional, Otra Mirada, Programas
Tags: Comunidad indígena Awá, Crisis de DD.HH., nariño
Slug: otra-mirada-narino-resiste-ante-el-olvido-estatal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Nariño.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Tomado de twitter- JuanEdgardoPai

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nariño es uno de los departamentos que enfrenta grandes problemas de asesinatos contra defensores y líderes de derechos humanos, comunidades indígenas, así como masacres y el fuego cruzado entre diferentes grupos armados que dejan en medio a la comunidad campesina y que evidencian el olvido por parte del Gobierno. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Indepaz además del Cauca, **Nariño es otra de las regiones donde más se atenta contra la vida de los integrantes de las comunidades indígenas y es el departamento donde se presentan constantes operaciones de erradicación forzada.** (Le puede interesar: [Pueblo indígena Awá fue víctima de una nueva masacre](https://archivo.contagioradio.com/pueblo-indigena-awa-fue-victima-de-una-nueva-masacre/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los participantes de este encuentro fueron Leonardo González, coordinador de proyectos INDEPAZ, Nancy Liliana Villota, abogada defensora de derechos humanos, Hermógenes Rodríguez, coordinador del programa de sustitución de cultivos de uso ilícito en Tumaco y fundador de la organización Campesina Asoporca y Rider Pai Nastacuas, consejero mayor de la Organización del pueblo Awá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los expertos explican cuál es la situación actual en el departamento para las distintas comunidades campesinas, indígenas, afro, los mecanismos de represión por parte de la fuerza pública y el olvido del gobierno, además de cuáles son las dinámicas organizativas que desarrollan las comunidades aún en medio de daños, violaciones de DDHH o infracciones al DIH. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También comentan cuáles son los riesgos que representan ser un líder y cómo pese a ellos se sigue ejerciendo el ejercicio de liderar y defender los derechos de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, **siendo la comunidad indígena Awá una de las más afectadas, con la cifra de 9 asesinatos contra integrantes de la comunidad en 4 meses** según la UNIPA, el consejero mayor comparte qué es lo que viene viviendo la comunidad Awá al tener que dejar costumbres y abandonar los territorios que les pertenecen y de igual manera, cuáles han sido las respuestas del gobierno frente al ataque al pueblo Awá por parte de grupos armados. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, exigen al gobierno y a las instituciones, cumplir a las comunidades y lo pactado en el acuerdo de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 6 de agosto: [Otra Mirada: y¿y el Gobierno de Colombia a quién escucha?](https://www.facebook.com/contagioradio/videos/929409430803394)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo [Haga clik aquí](https://www.facebook.com/contagioradio/videos/899527523868967)

<!-- /wp:paragraph -->
