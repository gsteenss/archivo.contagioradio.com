Title: Arranca verificación tripartita al cese bilateral de fuego en Colombia
Date: 2016-08-31 17:46
Category: Nacional, Paz
Tags: acuerdo final, Conversaciones de paz de la habana, FARC, Juan Manuel Santos, ONU
Slug: arranca-la-verificacion-tripartita-al-cese-bilateral-de-fuego-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/verificación-onu-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: onu colombia] 

###### [31 Ago 2016]

Mediante un comunicado del Gobierno, las FARC y la Organización de Naciones Unidas se informó que **arrancó el proceso de verificación del cese bilateral de fuego con la etapa de capacitación a más de 80 personas**, observadores internacionales, delegados y delegadas de 8 países que hacen parte de la CELAC. Las personas serán capacitadas por integrantes de las FARC y del gobierno así como de la ONU.

En el comunicado conocido hace pocas horas, resaltan que es un hecho histórico que después de 50 años de confrontación están unidas las partes en confrontación para capacitar a los observadores internacionales provenientes de Argentina, Bolivia, Chile, El Salvador, Guatemala, México, Paraguay y Uruguay.

La capacitación constará de ejercicios teórico prácticos sobre la **metodología de verificación, aspectos logísticos, de seguridad, de los procedimientos operativos para las Zonas Veredales Transitorias de Normalización** y los Puntos Transitorios de Normalización, y temas de género, entre otros. Los integrantes de esta delegación estarán concentrados en la ciudad de Popayán hasta el próximo 6 de septiembre.

##### *COMUNICADO*

##### *Popayán, Cauca.  31 de agosto de 2016*

##### *El Gobierno Nacional, las FARC-EP y la Misión de la Organización de las Naciones Unidas (ONU) en Colombia, informamos que hoy empezó la primera fase de capacitación del grupo de mujeres y hombres que harán parte del mecanismo tripartito encargado del monitoreo y la verificación del Cese al Fuego y de Hostilidades Bilateral y Definitivo, en el marco de la etapa preparatoria a la activación de este mecanismo.*

##### *Resaltamos como un hecho histórico que durante una semana partes enfrentadas durante más de 50 años, se reúnan con el acompañamiento de la Misión de la ONU, para capacitar a 80 integrantes que liderarán el monitoreo y la verificación conjuntos a nivel nacional y regional.*

##### *Entre las mujeres y hombres observadores internacionales de la Misión, están delegados de ocho países de la Comunidad de Estados Latinoamericanos y Caribeños (CELAC): Argentina, Bolivia, Chile, El Salvador, Guatemala, México, Paraguay y Uruguay.*

##### *La capacitación, que se extenderá hasta el próximo 6 de septiembre en Popayán, es realizada por instructores del Gobierno Nacional, las FARC-EP  y la Misión de la ONU. Es un ejercicio teórico-práctico que comprende varios aspectos del Acuerdo Final, en especial el Cese al Fuego y de Hostilidades Bilateral y Definitivo y la Dejación de las Armas, y los protocolos que regularán la actividad del Mecanismo de Monitoreo y Verificación. *

##### *En las varias sesiones de estudio y trabajo se incluye la metodología de verificación, aspectos logísticos, de seguridad, de los procedimientos operativos para las Zonas Veredales Transitorias de Normalización y los Puntos Transitorios de Normalización, y temas de género, entre otros aspectos. La capacitación comprenderá además ejercicios prácticos para reforzar los conocimientos adquiridos.*

##### *Este primer nivel de capacitación constituye un paso importante hacia la construcción de una paz estable y duradera. Es no solo el inicio de la materialización de los acuerdos alcanzados en La Habana, sino también del firme compromiso de los integrantes del mecanismo para consolidar un proceso confiable y transparente de monitoreo y verificación, que dé plenas garantías a las comunidades y la sociedad en general.*
