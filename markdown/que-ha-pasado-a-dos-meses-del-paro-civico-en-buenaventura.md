Title: ¿Qué ha pasado a dos meses del Paro Cívico en Buenaventura?
Date: 2017-08-01 17:19
Category: Movilización, Nacional
Tags: Acuerdos con Buenaventura, buenaventura, paro, Paro Cívico en Buenaventura
Slug: que-ha-pasado-a-dos-meses-del-paro-civico-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/4103147Paro-Buenaventura-EFE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [01 Ago. 2017] 

El Comité Ejecutivo del Paro Cívico de Buenaventura entregó algunos avances que han logrado con el Gobierno Nacional luego de más de 2 meses de haber levantado el paro, los que han permitido avanzar en el proceso de búsqueda de mejores condiciones de vida para los Bonaverenses.

Hasta la fecha todos los comités creados se encuentran trabajando de manera intensa, pero uno de **los primeros logros es haber alcanzado la creación del proyecto de ley para declarar el patrimonio autónomo para Buenaventura** que fue radicado el 26 de julio en el Senado de la República. Le puede interesar: [Así participaron las mujeres en el Paro Cívico de Buenaventura](https://archivo.contagioradio.com/las-mujeres-en-el-paro-civico-de-buenaventura/)

“Aspiramos y esperamos que todos los senadores lo respalden y se logre el objetivo que es a través de ese patrimonio autónomo de Buenaventura desarrollar el plan de desarrollo durante 10 años que contiene todo el trabajo que se viene haciendo en las diferentes mesas de acuerdo a los 8 ejes temáticos que nos planteamos durante los 22 días de paro cívico” aseguró **María Miyela Riascos integrante del Comité del Paro Cívico de Buenaventura.**

### **No ha sido fácil llegar a acuerdos con el Gobierno** 

Antes de lograr alcanzar la creación del proyecto de ley fueron varios los debates que se dieron entre el Gobierno Nacional y el Comité del Paro Cívico con base en las propuestas presentadas por las partes, que dieron como resultado el texto final conocido.

**“Los dos tuvimos que ceder en varias cosas para tener una tercera propuesta** que mínimamente diera participación a las partes sin perder el objetivo por el cual el pueblo de Buenaventura salió a las calles a manifestarse”.

### **¿Para qué un Patrimonio Autónomo para Buenaventura?** 

Este fondo permitirá que los cambios solicitados por las comunidades se den, puesto que **en éste se destinan \$1.6 billones de pesos que serán invertidos en los próximos 3 años**, así como un plan de desarrollo para ese distrito que se ejecutará en los próximos 10 años.

“Queremos poder trabajar para generar ingresos, generación de empleo. Esperamos poder avanzar en ese proceso. No pedimos nada descabellado, queremos que se reivindique el Estado colombiano”.

### **El tema de salud no ha avanzado, sino ha retrocedido** 

Asegura Riascos que luego del Paro Cívico en Buenaventura se ha vuelto más complejo el tema de la salud, puesto que han cerrado varios centros de atención como Comfamar, Profamilia y la Clinica Santa Sofía está colapsada. En contexto: [Así va el cumplimiento de los acuerdos del Gobierno con Buenaventura](https://archivo.contagioradio.com/asi-va-el-cumplimiento-del-acuerdo-del-gobierno-con-buenaventura/)

“Desde la mesa de salud se ha pedido **al Gobierno Nacional y local que declare Buenaventura en emergencia de salud** y lo que han hecho es plantear algunas iniciativas de las que no estamos convencidos todavía de ellas, pero evaluaremos”.

### **Bonaverenses siguen soñando con abrir la llave de las casas y tener agua** 

Dice la lideresa que la mesa que está trabajando el tema de agua, saneamiento y alcantarillado se ha dedicado de lleno a trabajar para poder encontrar soluciones a las problemáticas que se viven en estos puntos en Buenaventura.

“Como dice una adulta mayor **finalmente yo veré eso materializado cuando yo abra la llave de mi casa y me caiga el agua.** Entonces en lo concreto es allí donde se ven los resultados, pero también sabemos que tiene que surtirse todo un proceso y unos tiempos pertinentes para ver resultados, pero ya hemos tenido avances importantes”.

### **Falta que el Gobierno responda por el uso desmedido de la fuerza** 

A la fecha, el Comité del Paro Cívico manifiesta que se encuentra a la espera de **un informe en el que se dé cuenta de los graves hechos de violaciones de derechos humanos** que se dieron en Buenaventura en el marco del paro dado que todo lo que la comunidad dijo sucedió y no ha habido responsables sancionados. Le puede interesar: [Denuncian que Paramilitarismo y daños ambientales van de la mano en Buenaventura](https://archivo.contagioradio.com/43782/)

“A hoy tenemos casos en los que mujeres y hombres que fueron impactados por bala y que tienen la bala en sus cuerpos y tienen su vida en riesgo y todavía se están negando esos hechos. No estamos dispuestos a que todas las violaciones de DD.HH. queden impunes. Estamos solicitando un evento en el que el Gobierno reconozca lo que se denunció y en ese sentido no se nos estigmatice. **A la fecha ya estamos siendo amenazados y seguidos por miembros de la Sijin y de personas no identificadas”.**

<iframe id="audio_20130320" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20130320_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]{.s1}
