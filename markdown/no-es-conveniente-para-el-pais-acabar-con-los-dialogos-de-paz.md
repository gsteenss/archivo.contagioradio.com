Title: "No es conveniente para el país acabar con los diálogos de paz"
Date: 2015-04-15 11:48
Author: CtgAdm
Category: Nacional, Paz
Tags: Cauca, Cese al fuego, cese bilateral, ejercito, FARC, habana, la esperanza, paz, Santos
Slug: no-es-conveniente-para-el-pais-acabar-con-los-dialogos-de-paz
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4357788_2_1.html?data=lZiimZycfI6ZmKiak5eJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fa08rb1sbRrcbi1dSYpaa5h6Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Medina Gallego, Grupo de Seguridad y Defensa] 

#####  

Hechos presentado entre el Ejercito y la guerrilla de las FARC, aún sin esclarecer, en la madrugada del miércoles 15 de abril en La Esperanza, corregimiento de Timba en el municipio de Buenos Aires en el Cauca, que dejaron como resultado 10 militares muertos, han vuelto a poner en debate público las condiciones en las cuales se adelantan los diálogos de paz entre ambos actores.

Para el investigador Carlos Medina Gallego, del Grupo de Seguridad y Defensa de la Universidad Nacional, es lamentable, aunque normal, que este tipo de choques se presenten cuando "una fuerza que decide no operar recibe acciones de una fuerza que mantiene el mandato constitucional de atacarlas". Frente a este aspecto se pronunció el Comandante de las FARC Pastor Alape, quien aseguró en rueda de prensa que "esta guerrilla lleva 4 meses rehuyendo combates", refiriéndose a los casi 4 meses que cumple el cese al fuego unilateral decretado por las FARC desde el 20 de diciembre del 2014.

Si hasta el momento, el gobierno nacional y comandantes de las fuerzas armadas han aceptado y reconocido que el cese al fuego unilateral ha surtido un efecto positivo, es necesario que se implementen con mayor fuerza las decisiones en torno al cese bilateral de fuegos, como el cese de los bombardeos, en lugar de regresar a la situación de conflicto de meses anteriores, asegura Medina Gallego.

El investigador concuerda con la propuesta presentada por el Frente Amplio por la Paz, que sugiere que el cese bilateral de fuegos inicie de manera experimental "en aquellas zonas donde el conflicto ha tenido mayor intensidad, especialmente en el sur occidente colombiano, los departamentos del Cauca, Nariño y Valle del Cauca", donde la presencia de muchos actores armados genera incidentes que afectan fuertemente a comunidades indígenas, campesinas y negras.

Según afirma, la mesa de conversaciones ha madurado lo suficiente para que este tipo de incidentes puedan ser abordados con racionalidad, y no se produzcan más incertidumbres", máxime en un momento en que "se está llegando a puntos muy críticos en materia de víctimas, justicia y terminación del conflicto".

"No es conveniente para el país, ni para el gobierno, ni para el proceso" acabar con los diálogos de paz. El presidente Santos ha dicho en los últimos días que "si había necesidad de correrse, se corría" del proceso, sin embargo para el investigador, el presidente "ya no tiene el mandato para incurrir en ese tipo de decisiones. Él llego a la presidencia en su segundo mandato porque Colombia le apuesta a la paz del país. Acabamos de tener una ratificación de la voluntad nacional de paz a través de la marcha del 9 de abril"

En este sentido, es necesario que la opinión pública considere de manera crítica la intervención de los medios de información en el conflicto, pues considera el investigador que a través de titulares y micrófonos "han jugado un papel de carboneros de la guerra".

"Es necesario construir una estrategia comunicativa para la paz, y los medios, sobre todo los medios privados, no están colaborando en ese sentido". La opinión pública debe entender que "estos hechos que se vienen produciendo son parte de las dinámicas de un conflicto como el nuestro, enrarecido, degradado, con múltiples actores, y con responsabilidades mayores, y es importante que los medios asuman una postura a favor de la paz".

Finalmente, anuncia Medina Gallego, "cada momento trae su angustia", y si al país le generan preocupación los operativos militares, sería necesario prestarle atención a este punto, para que "se vaya clarificando como se hace para que no se sigan muriendo ni miembros del ejercito ni de la guerrilla", ni los campesinas y la sociedad civil en medio de los diálogos de paz.
