Title: El PND emprobecería los colombianos: Congresistas de Bancada Alternativa
Date: 2019-03-20 18:18
Category: Entrevistas, Política
Tags: Bancada Alternativa, Plan Nacional de Desarrollo, Presidente Ivan Duque
Slug: pnd-emprobeceria-los-colombianos-congresistas-bancada-alternativa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-48.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

###### [20 Mar 2019] 

Este lunes, congresistas de la Bancada Alternativa radicaron una ponencia negativa al Plan Nacional de Desarrollo (PND) por medio del cual denunciaron que la ruta de acción que plantea el gobierno del Presidente Iván Duque **favorece "los grandes capitales y las multinacionales" a costo de los ciudadanos, el ambiente y la paz**.

Así lo comunicaron los firmantes de la ponencia, el senador[ **Wilson Arias** (Polo Democrático Alternativo), la senadora **Aída Avella** (Unión Patriótica), el senador **Gustavo Bolívar**, el representante **David Racero** (Lista Decentes), el representante **Carlos Carreño** y el representante **Jairo Cala** (Fuerza Alternativa del Común).]

[El senador Arias indicó que en el PND fueron introducidos artículos que facilitarían el proceso para el **otorgamiento de la licencia ambiental y que plantean el uso de la técnica de fracturación hidráulica**, conocida como fracking. Este respaldo al modelo extractivista vulnera en particular los derechos de los sectores populares y la clase media, tal como ha sucedido con megaproyectos como Hidroituango, señaló el congresista. (Le puede interesar: "[Desalojan familias afectadas por Hidroituango de albergues de EPM](https://archivo.contagioradio.com/desalojan-familias-afectadas-hidroituango-albergues-epm/)")]

Arias también resaltó que **las bases del PND desconocen los acuerdos pactados con los maestros y estudiantes universitarios.** Por un lado, pretende seguir financiando la educación por medio del crédito tal como lo había planteado el gobierno previo; mientras tanto, propone **"**[**quitarle autonomía financiera y de gobierno al SENA,** presentando un rencauchado de unificación y botando a la basura algo ganado por los trabajadores desde 1957”, aseguró.]

[Frente al tema de pensiones en el Plan de Desarrollo, la senadora Avella advierte que **el Gobierno está haciendo la reforma pensional por la puerta de atrás**, ya que incorpora las peticiones que venían presentando los fondos privados Porvenir y Protección, los cuales manejan 191 billones de pesos de los ahorros de los colombianos, mientras que, como indica la Senadora, **"los campesinos que trabajan de sol a sol no tienen ni el derecho a una pensión digna y ni manera de cotizar”**.]

[Por su parte, los congresistas de la FARC Carlos Carreño y Jairo Cala resaltaron que el PND **no incorporaría el capítulo específico para la implementación del Acuerdo de Paz** en los términos definidos en el CONPES 3932. “A través del PND, el Gobierno Nacional busca mediante el desconocimiento de la Paz como un derecho, reactivar un nuevo ciclo de violencia que impida la materialización de los distintos puntos que componen el Acuerdo Final", señalaron los representantes.]

Finalmente, el senador Bolívar indicó que **el PND propone políticas económicas que serían perjudiciales para la salud de los colombianos** dado que[ “incentiva el modelo de negocio que proviene de la Ley 100, en detrimento de los recursos de salud pública, plantea subsidios a privados, exige a los hospitales rentabilidad financiera en lugar de rentabilidad social e incluye el riesgo de salvar financieramente a las EPS".]

El debate en el Congreso sobre el proyecto de ley [227 Senado y 311 Cámara, mediante el cual se expide el Plan de Desarrollo, inició en las comisiones económicas esta semana. **Dicho proyecto de ley deberá ser aprobado el próximo 6 de mayo**.]

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
