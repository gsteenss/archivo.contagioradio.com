Title: Senadores ceden su curul a líderes sociales por un día
Date: 2019-05-21 17:14
Author: CtgAdm
Category: Líderes sociales, Política
Tags: lideres sociales, Senado de la República
Slug: senadores-ceden-su-curul-a-lideres-sociales-por-un-dia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Líderes-sociales-pacto.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Líderes-sociales-pacto-senado.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @IvanCepedaCas] 

Trece líderes sociales de Cauca, Arauca,  Bolívar, Córdoba y Amazonia sesionaron este martes en  la Comisión II del Senado exponiendo los desafíos que implica ejercer su trabajo en las regiones. Al concluir la sesión los congresistas que cedieron su curul, y suscribieron un pacto de protección por los líderes y lideresas que defienden los derechos humanos y del territorio.

### Senadores por un día 

Durante la sesión, presidida por la lideresa **Mayerlis Angarita**, los representantes de diversas regiones del país se refirieron por cerca de tres horas a las amenazas y circunstancias de inseguridad que rodean su labor, los factores de violencia en el territorio y la débil presencia estatal para garantizar su protección. [(Lea también: Lideresa social Mayerlis Angarita sobrevive a atentado en Barranquilla)](https://archivo.contagioradio.com/lideresa-social-mayerlis-angarita-sobrevive-a-atentado-en-barranquilla/)

Algunos de los líderes presentes fueron Edgar Velasco, Tatiana Galera, María Arías, Robinson Mejía, Maerli Robles, Daniely Estupiñan, César Cerón, Jhoe Sauca, Jhon Noriega,  "realmente fue una sesión muy emotiva, cedimos 13 curules a igual número de líderes sociales, quienes presentaron las circunstancias excepcionales y difíciles en las que deben adelantar su trabajo" afirmó el senador del Partido Verde, Antonio Sanguino, uno de los integrantes de la Comisión.

### Un pacto por los líderes sociales 

Al concluir la sesión, los senadores presentes firmaron un pacto compuesto por cuatro puntos principales enfocados en trabajar por la protección de los líderes, luchar contra su estigmatización y demandar al Estado la protección de los mismos.

Según Sanguino, este pacto es novedoso en tanto que incorpora a partidos como Cambio Radical, el Partido de la U y el Partido Liberal, fuerzas políticas que no venían trabajando "con la suficiente potencia frente a la gravedad en materia de asesinato de líderes sociales, es importante que hayan firmado ese pacto".

Algunos de los senadores que firmaron este compromiso fueron José Luis Pérez, Ema Claudia Castellanos, Berner León Zambrano de la U, Iván Cepeda Castro del Polo Democrático, Feliciano Valencia de  Mais y Antonio Sanguino del Partido Verde, a la espera de que los demás integrantes de la Comisión, entre ellos miembros del Centro Democrático, Partido Conservador y Mira firmen el acuerdo.

> Congresistas de la [\#ComisionII](https://twitter.com/hashtag/ComisionII?src=hash&ref_src=twsrc%5Etfw) de [@SenadoGovCo](https://twitter.com/SenadoGovCo?ref_src=twsrc%5Etfw) suscribimos un Pacto de Protección de los Líderes Sociales en el que nos comprometemos a no estigmatizarlos, denunciar las amenazas en su contra y exhortar al Estado a protegerlos integralmente. [\#UnLiderEnMiLugar](https://twitter.com/hashtag/UnLiderEnMiLugar?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/wh5SIjfV4X](https://t.co/wh5SIjfV4X)
>
> — Antonio Sanguino Senador (@AntonioSanguino) [21 de mayo de 2019](https://twitter.com/AntonioSanguino/status/1130897990829432832?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
