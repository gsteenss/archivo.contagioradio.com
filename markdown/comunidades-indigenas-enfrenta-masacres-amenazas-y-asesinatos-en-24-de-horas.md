Title: Comunidades indígenas enfrentan masacres, amenazas y asesinatos en menos de 24 de horas
Date: 2020-12-06 12:16
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Amenazas, Asesinatos, Cauca, comunidades indígenas, masacre
Slug: comunidades-indigenas-enfrenta-masacres-amenazas-y-asesinatos-en-24-de-horas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/diseños-para-notas-2.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/diseños-para-notas-3.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*Foto: ACIN*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este fin de semana, entre el 5 y 6 de diciembre **los líderes y comunidades indígenas del Cauca fueron víctimas de diferentes acciones armadas que acabaron con la vida de 7 integrantes de su comunidad entre ellos 2 líderes** sociales, al tiempo que defensores de Derechos Humanos y pueblos recibieron amenazas en su contra.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CapazMauricio1/status/1335571024977534977","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CapazMauricio1/status/1335571024977534977

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por medio de un comunicado el Consejo Regional Indígena del Cauca -C**RIC Nacional-** , denunció pese al aumenten de los asesinatos y masacres, de campesinos, afros y comuneros indígenas en el Cauca a manos de ***"grupos armados que tienen como combustible el narcotráfio"*, las acciones del Gobierno son limitadas e insuficientes para generar acciones de fondo.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"La muerte asecha a nuestros Territorios y el Gobierno de Ivan Duque solo hace consejos comunitarios y aumenta el pie de Fuerza militar***"
>
> <cite>C**RIC Nacional**</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### 4 personas son masacradas en el municipio de Santander de Quilichao, **Cauca**

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/jorgeeliecersa/status/1335436169992167424?s=20","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/jorgeeliecersa/status/1335436169992167424?s=20

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este 6 de diciembre sobre la 1:00 am en medio de las veredas San Pedro y Gualandai, en cercanías al resguardo indígena de Munchique los tigres,  
municipio de Santander de Quilichao, norte del Cauca, ingresaron ***"hombres encapuchados que dispararon indiscriminadamente contra seis personas quienes se encontraban departiendo en una casa familiar"***.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo el [CRIC Nacional](https://www.cric-colombia.org/portal/proyecto-politico/cumplimiento-de-mandatos/cric-nacional/) señaló que en esta nueva masacre perdieron la vida cuatro comuneros, **dos de ellos fallecieron en el lugar de los hechos, y las otras dos personas en un centro asistencial al que fueron remitidas**, asimismo indicaron que una mujer aún de identidad desconocida resultó herida. ([Es asesinado Guildon Solís, líder afro del Cauca](https://archivo.contagioradio.com/es-asesinado-guildon-solis-lider-afro-del-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acción que aumenta el miedo, la incertidumbre y el riesgo de desplazamiento forzado ante la falta de garantías de permanencia en el territorio de las comunidades indígenas inmersas en el conflicto. Según el Instituto de estudios para el Desarrollo y la Paz -Indepaz- , **son 309 personas las que han muerto en 77 masacres perpetradas en el año, 12 cometidas en el Cauca.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En la masacre perdió la vida Carlos Escue líder social juvenil de las comunidades indígenas

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la masacre perpetrada por hombres armados en zona rural de Santander de Quilichao, perdió la vida **Carlos Escue, quien se desempeñó  
como coordinador de Jóvenes en el resguardo** Munchique los tigres, en donde lideraba acciones de control territorial, al tiempo que se desempeñaba como músico. ([Es asesinado el Mayor indígena Miguel Tapi en Chocó](https://archivo.contagioradio.com/es-asesinado-el-mayor-indigena-miguel-tapi-en-choco/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":93787,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/diseños-para-notas-2.jpg){.wp-image-93787}

</figure>
<!-- /wp:image -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CricDdhh/status/1335428849744703488?s=20","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CricDdhh/status/1335428849744703488?s=20

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Es asesinado Juan Carlos Petins comunero indígena del resguardo Nega Cxhab - Belalcázar

<!-- /wp:heading -->

<!-- wp:paragraph -->

A la secuencia de hechos de vulneración a la vida de los y las líderesas del Cauca se suma el asesinato del comunero **Juan Carlos Petins en Páez-Belalcázar**, reconocido vocero de la comunidad indígena.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho ocurrió sobre 11:00 de la mañana , **comerciante y comunero del resguardo indígena de Nega Çxhab-Belalcázar**, según versiones de la comunidad indígena Petins se dedicaba a ofrecer servicios en redes de comunicaciones de internet.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Su muerte se da luego de recibir múltiples impactos de bala con arma de fuego a la altura del pecho**, esto según información suministrada por voceros y voceras de la Asociación de Autoridades Ancestrales Nasa.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93788,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/diseños-para-notas-3.jpg){.wp-image-93788}

</figure>
<!-- /wp:image -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1335315150199533570?s=20","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1335315150199533570?s=20

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A este asesinato también se suma el de **Eduardo Pino Julicue, hijo de Luz Eida Jilicue exconsejera de la ACIN**, con ellos son casi 300 los y las líderes sociales asesinados durante el 2020 en Colombia. ([Radiografia de la violencia contra los lideres asesinados en colombia](http://www.indepaz.org.co/radiografia-de-la-violencia-contra-los-lideres-asesinados-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Amenazan a las comunidades indígenas y sus líderes

<!-- /wp:heading -->

<!-- wp:paragraph -->

***"A los recientes y atroces hechos donde asesinan a nuestros comuneros, se le suman las amenazas constantes** de grupos armados al servicio del narcotráfico"*, indica el CRIC Nacional, quienes señalan que por medio de panfletos intimidantes señalan a los líderes y lideresas, de ***"afectar sus intereses con nuestras políticas de control territorial".***

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1335557878220992512","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1335557878220992512

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Al tiempo desde el CRIC, denuncian la política de seguridad del Gobierno Nacional en cabeza del Presidente Ivan Duque Márquez puesto que para ellos y ellas ***"no garantiza la pervivencia de los pueblos ancestrales, y por el contrario, atenta contra las comunidades, los sectores sociales, las familias desde la colectividad".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez alertan, *"frente a la realidad palpable del país, **vemos con  
preocupación que los actores armados continúan asesinando y desplazando comunidades, generando miedo y zozobra, por lo cual es deber del Gobierno Nacional en cabeza del dar la cara y responder ante la situación de violencia**, muerte e estigmatización"*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"El Gobierno debe garantizar la defensa de la vida, la paz, el territorio  
> y la democracia, **los consejos de seguridad del Gobierno de Ivan Duque son simplemente el aumento de la Guerra"***
>
> <cite>CRIC Nacional</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

  
Finalmente hacemos un llamado a los Organismos Nacionales e Internacionales defensores y garantes de los DD. HH y el DIH, a la Defensoría del Pueblo, a la Procuraduría General de la Nación, para que **en una acción urgente hagan seguimiento al gobierno nacional y lo llamen a dar garantías a la vida de las comunidades indígenas del Cauca.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
