Title: Comisionada Michelle Bachelet reitera respaldo al Sistema de Justicia Transicional
Date: 2020-10-29 13:25
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Alta Comisionada de las Naciones Unidas para los Derechos Humanos, comision de la verdad, JEP, Sistema de Justicia Transicional, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: comisionada-michelle-bachelet-reitera-respaldo-al-sistema-de-justicia-transicional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Sistema-de-Justicia-Transicional-sostiene-reunion-con-la-Alta-Comisionada-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves 23 de octubre el Sistema de Justicia Transicional sostuvo una reunión con la Alta Comisionada para los Derechos Humanos de Naciones Unidas, Michelle Bachelet para presentar avances en la implementación del acuerdo de paz.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1319337035426037762","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1319337035426037762

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En la reunión participaron la directora general de la [Unidad de Búsqueda de Personas dadas por Desaparecidas](https://www.ubpdbusquedadesaparecidos.co/), Luz Marina Monzón, la presidenta de la [Jurisdicción Especial para la Paz](https://www.jep.gov.co/Paginas/Inicio.aspx), Patricia Linares Prieto, y el presidente de la [Comisión para el Esclarecimiento de la Verdad](https://comisiondelaverdad.co/), Francisco de Roux.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante la reunión, los mecanismos del Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR) informaron sobre los avances que se han realizado en los casos que cada uno adelanta, aún en medio de la pandemia. ([Le puede interesar: A la JEP llegan nuevo presidente y vicepresidenta](https://archivo.contagioradio.com/jep-elige-nuevos-presidente-y-vicepresidenta/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los mecanismos expresaron preocupación por la violencia que viene aumentando en los territorios y que ha costado la vida de líderes sociales y defensores de derechos humanos, firmantes de paz, jóvenes, indígenas y campesinos, además de la no aprobación de recursos adicionales para la JEP en el presupuesto general del 2021, que de acuerdo con la jurisdicción, son fundamentales para la atención y protección a víctimas. [(Le puede interesar:](https://archivo.contagioradio.com/presupuesto-del-2021-se-puede-convertir-en-un-nuevo-ataque-a-la-jep/)[Presupuesto Nacional 2021: Antesala de reforma tributaria y ataque a la paz](https://archivo.contagioradio.com/presupuesto-nacional-2021-antesala-de-reforma-tributaria-y-ataque-a-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, la comisionada reiteró su respaldo y el de la ONU al trabajo de **estos tres entes que son «la base para poner final al conflicto y sus contribuciones son precisas para garantizar los derechos de las víctimas». Asimismo, hizo un llamado para que se garantice la continuidad en las labores de los mecanismos con autonomía e independencia.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONUHumanRights/status/1319310373120581632","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONUHumanRights/status/1319310373120581632

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading -->

Mecanismos presentan avances en enfoque de género
-------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ese mismo jueves en horas de la tarde, los tres mecanismos sostuvieron una reunión con Åsa Regnér, directora ejecutiva adjunta de la Oficina de ONU Mujeres, para presentar avances en la implementación del enfoque de género.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComisionVerdadC/status/1319373125667196938","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComisionVerdadC/status/1319373125667196938

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En este encuentro se destacó la labor realizada para recibir los testimonios de mujeres que fueron victimas de violencia sexual en el marco del conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
