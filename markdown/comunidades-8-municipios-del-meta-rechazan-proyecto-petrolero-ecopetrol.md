Title: Comunidades de 8 municipios del Meta rechazan proyecto petrolero de Ecopetrol
Date: 2017-04-07 09:43
Category: Ambiente, Nacional
Tags: Ecopetrol, Meta
Slug: comunidades-8-municipios-del-meta-rechazan-proyecto-petrolero-ecopetrol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/petroleo1-e1468449932490.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Confidencial Colombia 

###### [7 Abr 2017] 

Las comunidades de los municipios de **Guamal, Castilla, San Martín, Cubarral, El Dorado, El Castillo, Granada y Acacias, evidencian su total rechazo al proyecto petrolera de Ecopetrol** en el departamento del Meta. Así lo hicieron frente a instituciones como la Gobernación del Meta, el Ministerio de Minas y Energías, el Ministerio del Interior, la Agencia Nacional de Hidrocarburos, la Agencia Nacional de Licencias Ambientales, la Procuraduría General de la Nación, la Defensoría del Pueblo, Cormacarena, la administración pública del municipio de Guamal, y la empresa Ecopetrol.

Se trata del  **Proyecto petrolero “Área exploratoria CPO9” a cargo de Ecopetrol** que cuenta con licencia ambiental otorgada para la fase de exploración mediante la cual el Ministerio de Ambiente y le autoriza la construcción de diez plataformas con hasta tres pozos por cada una de las plataformas impactando 8  municipios del Meta.

La preocupación se centra en el tema ambiental teniendo en cuenta que el proyecto petrolero afectaría el Pie de Monte Llanero, que **es una zona de amortigüamiento del páramo de Sumapaz y además es el lugar de nacimiento de diversas fuentes hídricas,** lo que configura esa área como zona de recarga hídrica de la Orinoquia colombiana.

La comunidad campesina ha realizado dos movilizaciones pacíficas y se ha declarado en asamblea permanente desde el mes de febrero, manifestando que no están dispuestas a permitir la construcción de la plataforma Trogon en su territorio, pues además habría amenaza latente de **desplazamiento forzado, perdida del tejido social y de los lazos familiares y de la economía campesina** que se basa en la producción de cacao, cítricos y producción de leche y carne vacuna. A su vez, la población ha manifestado que ya hay amenazas contra algunos líderes del proceso.

La comunidad denuncia que **“la actitud de la empresa Ecopetrol, en cabeza del Señor Eduardo Uribe, fue displicente y arrogante,** al señalar que el proyecto se llevará a cabo sin tener presente las consideraciones de la comunidad. Hecho que demuestra la nula importancia que le da la empresa a la participación ciudadana”. [(Le puede interesar: Cormacarena y gobernación advierten sobre riesgos de proyecto petrolero de Ecopetrol)](https://archivo.contagioradio.com/cormacarena-gobernacion-del-meta-senalan-riesgos-actividades-ecopetrol-guamal/)

“La comunidad de Guamal, manifiesta y denuncia que el escenario realizado con las instituciones lejos de ser un espacio de diálogo, para que fuesen escuchadas y transformadas eficazmente las irregularidades y afectaciones a derechos fundamentales y colectivos de la comunidad por la plataforma Trogon 1, se trató de una **jornada de propaganda política y de sabotaje a la resistencia pacífica de la comunidad al proyecto petrolero”**, expresa la comunidad.[(Le puede interesar: Proyecto petrolero acabaría con río Humedea)](https://archivo.contagioradio.com/proyecto-petrolero-acabaria-con-el-rio-humadea-en-el-meta/)

<iframe id="audio_18023885" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18023885_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
