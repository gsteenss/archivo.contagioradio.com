Title: Cada hora mueren en el mundo 1200 personas por falta de acceso a medicamentos
Date: 2017-07-26 10:09
Category: Mision Salud, Opinion
Tags: Acceso a medicamentos
Slug: cada-hora-mueren-en-el-mundo-1200-personas-por-falta-de-acceso-a-medicamentos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/medicamentos3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Federico Parra/Getty 

#### **Por Germán Holguín Zamorano - [Director General de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

###### 26 Jul 2017 

Este dato, proveniente de la OMS, es sobrecogedor. Significa que mientras usted lee esta nota, alrededor de 100 seres humanos dejarán de existir en nuestro planeta, por no tener acceso a medicinas necesarias.

[Pero no crea que este es un problema exclusivo de los países más pobres, como Sierra Leona, Afganistán o Haití. No. Porque el principal origen de la falta de medicinas no está en la pobreza sino en las patentes farmacéuticas, que otorgan a las farmacéuticas multinacionales el privilegio del monopolio en la producción y comercialización de medicamentos, lo que les permite cobrar por sus productos precios abusivos, inalcanzables para cualquier sistema de salud y para quienes deben pagar los productos de su bolsillo, como es común en países en desarrollo.  ]

[Por eso América Latina no es excepción en medio de este drama. Allí, 125 millones de personas carecen de acceso permanente a los servicios básicos de salud, en buena parte debido a los altos precios de las medicinas derivados de las patentes, lo que se traduce en cerca de 80 muertes evitables cada hora.]

[Tampoco es excepción Colombia, donde según datos oficiales, a pesar de haberse logrado una cobertura casi universal del sistema de salud,  la tercera parte de los medicamentos que se prescriben no llegan a los pacientes, con las consecuencias inherentes en términos de enfermedad y pérdida de vidas.  ]

[Para enfrentar, entre otros, este problema, fruto de la ambición humana, durante la  VIII Conferencia Mundial de Promoción de] [la Salud (2013), 120 países aprobaron la][[“Declaración de Helsinki sobre Salud en Todas las Políticas]](http://apps.who.int/gb/ebwha/pdf_files/wha67/a67_r12-en.pdf)[”, que hace un llamado a todos los sectores gubernamentales a reconocer que la salud es una obligación fundamental del gobierno de cada país frente a su ciudadanía y que su cumplimiento no es responsabilidad exclusiva del sector salud sino de todos los sectores, y recomienda a éstos promover políticas para el mejoramiento de la salud y abstenerse de políticas y prácticas que puedan afectarla negativamente.]

[La Declaración reconoce que los gobiernos tienen una serie de prioridades, como el comercio y la política exterior, en las que la salud y la equidad no obtienen prioridad automáticamente, y les hace un llamado a que aseguren que las consideraciones de salud se tengan en cuenta de forma transparente en la formulación de políticas.]

[Es claro que si los países honraran esta Declaración, el problema de la pérdida de vidas humanas por falta de acceso a medicinas no sería tan grave pues los gobiernos impedirían el patentamiento de medicamentos prioritarios para la salud pública o tomarían las medidas necesarias para asegurar la utilización plena de las salvaguardas de la salud pública consagradas en la normativa internacional para controlar los abusos de los titulares de las patentes, entre las cuales ocupan lugar preferencial las licencias obligatorias, cuya finalidad es permitir el ingreso al mercado de versiones genéricas de medicamentos patentados para bajar los altos precios de monopolio a niveles de competencia y mejorar el acceso.]

[En el caso colombiano, es seguro que el Presidente y su Ministra de Comercio se hubieran abstenido de expedir el Decreto 670 de 2017, que elimina la posibilidad de controlar los precios de medicamentos declarados de interés público y hace casi imposible en la práctica el ejercicio del derecho a otorgar licencias obligatorias, con lo cual ignoraron olímpicamente la Declaración de Helsinki, a la que el país adhirió en su momento, y colocaron los intereses comerciales de un puñado de multinacionales farmacéuticas sobre el derecho fundamental a la salud y la vida de  los colombianos.]

[Es nuestra esperanza que algún día usted pueda releer este artículo con la tranquilidad de saber que mientras lo hace no se está registrando en el país ni en la región ni en el mundo ni una sola muerte evitable.]

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
