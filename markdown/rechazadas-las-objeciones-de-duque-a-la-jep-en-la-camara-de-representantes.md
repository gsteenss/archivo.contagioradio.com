Title: Objeciones de Duque a la JEP son rechazadas en la Cámara de Representantes
Date: 2019-04-09 01:35
Author: CtgAdm
Category: Nacional
Tags: JEP, Ley estatutaria
Slug: rechazadas-las-objeciones-de-duque-a-la-jep-en-la-camara-de-representantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ausentismo-camara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Luego de cuatro horas de debate se rechazó en la plenaria de la Cámara de Representantes las objeciones a la ley estatutaria radicadas por el Presidente Duque, **las votaciones quedaron 110 en contra y 44 a favor.**

> Salvamos la JEP!!!! Salvamos La Paz!!!!! Rechazadas las objeciones de Duque!! ?????????????? [pic.twitter.com/QrZ4EdF084](https://t.co/QrZ4EdF084)
>
> — Katherine Miranda (@MirandaBogota) [9 de abril de 2019](https://twitter.com/MirandaBogota/status/1115409840032112645?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>

