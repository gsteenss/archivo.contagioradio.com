Title: Chuzadas de la Fiscalía pretendían afectar el Proceso de Paz
Date: 2020-12-02 10:27
Author: AdminContagio
Category: Actualidad, Paz
Tags: colombia, paz
Slug: se-confirma-nueva-accion-para-afectar-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/160623_07_FirmaAcuerdosPazFarc_1800-e1467294057494.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En medio de una acción que pretendía afectar el acuerdo de paz un reportaje da cuenta como ex presidente Juan Manuel santos fue intervenido telefónicamente durante la fiscalía de Néstor Humberto Martínez como una acción en donde se pretendía organizar montajes judiciales *"para tratar de mezclar de mala fe el proceso de paz en el narcotráfico"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El artículo periodístico fue publicado por Gonzalo Guillén y Julián Martínez en el medio [La Nueva Prensa](https://www.lanuevaprensa.com.co/component/k2/juan-manuel-santos-fue-chuzado-durante-la-fiscalia-de-nestor-humberto-martinez), allí los periodistas indican que durante la la fiscalía de Martínez se habían interceptado *"al menos un teléfono del entonces presidente Juan Manuel santos", bajo la supuesta autorización"del juzgado décimo penal municipal con función de control de garantías y a instancias de un presunto agente de la DEA americana"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta acción, según los periodistas, tiene como único objetivo vincular el proceso de paz con acciones del narcotráfico por medio de la utilización de falsos testimonios que pretendían involucrar al vicepresidente, al ministro de defensa y el mismo presidente quién es entonces negociaron el proceso de paz con las Farc.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(la intervención del teléfono se llevó a cabo desde el 14 de noviembre de 2017 bajo la autorización de la fiscal 03 de la dirección de fiscalía antinarcóticos y lavado de activos especializada Luz marina tapia)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"A santos naranjo y pardo los intervinieron en el mismo proceso confuso que la fiscalía de Martínez Neira armó con el propósito de entregarlos a la justicia de Estados Unidos para solicitar las extradiciones por narcotráfico de los negociadores de paz "*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Dentro de la supuesto pedido de la DEA se dispuso la intervención de cerca de 29 teléfonos más de los cuales solamente se pudieron identificar la titularidad de 9, según el medio en una acción clara de un plan ilegal de espionaje.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los cerca de 24 mil audios que hacen parte del expediente de la Corte Suprema de Justicia de **Seuxis Paucias Hernández mejor conocido como Jesús Santrich,** acusado de enviar 10 kilos de cocaína a los Estados Unidos. tras la firma del Acuerdo de la Habana; permitirían identificar si todo hizo parte de **una operación de entrampamiento en la que habrían participado agentes de la DEA con el aval de la Fiscalía General de la Nación. **No solo ocultando información a la Jurisdicción Especial de Paz (JEP) que adelantaba el proceso del jefe político de FARC, sino hiriendo directamente al Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Le puede interesar: [DEA y Fiscalía habrían consumado un concierto para delinquir y afectar el proceso de paz: E Santiago](https://archivo.contagioradio.com/entrampamiento-mas-que-una-persecucion-a-santrich-un-ataque-al-acuerdo-de-pz/))

<!-- /wp:paragraph -->
