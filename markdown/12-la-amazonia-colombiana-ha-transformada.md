Title: El 12% de la Amazonía colombiana ha sido transformada
Date: 2016-12-19 20:36
Category: Ambiente, Nacional
Tags: amazonas, proyectos mineros
Slug: 12-la-amazonia-colombiana-ha-transformada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Rio-amazonas-e1482197399239.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Viajes Mollar 

###### [19 Dic 2016] 

“En 100 años se ha destruido el equivalente a **5 millones de canchas de fútbol en la Amazonía colombiana”, es la conclusión de un informe publicado por** el Instituto Sinchi y Parques Nacionales.

Ese dato indica que el **12% del Amazonas ha sido transformado**. Esa información se obtuvo gracias a los mapas a **escala 1:100.000** en los que están representados 483.074 km2 que conforman ese territorio, del cual han sido identificados 1.042 unidades ecosistémicas, que corresponden a 381 ecosistemas acuáticos y 661 terrestres.

Con el mapa se alerta sobre la baja **tasa de recuperación de los bosques.** Entre 2012 y 2014 se registró 445.000 hectáreas de bosques transformadas, y para ese mismo  tiempo sólo el 10% se restauró, es decir 43.200 hectáreas.

Cabe recordar que de acuerdo con ‘Amazonia Viva’, informe de WWF (Fondo Mundial para la Naturaleza) **la ganadería extensiva, la minería,  los proyectos hidroeléctricos y la construcción de vías de trasporte,** son las principales amenazas que actualmente tienen en riesgo una de las selvas más importantes del mundo. [(Le puede interesar: El 17% de la selva amazónica ha sido destruida)](https://archivo.contagioradio.com/el-17-de-la-selva-amazonica-ha-sido-destruida/)

Por otra parte, la Revista Science señala que en todo el territorio amazónico hay **800 autorizaciones para el desarrollo de proyectos mineros, sumado a eso, existen 6.800 solicitudes en proceso de revisión. Además se indica que hay otros 20 proyectos de construcción de carreteras.** [(Le puede interesar: Los ríos más biodiversos del mundo se encuentran en peligro)](https://archivo.contagioradio.com/biodiversidad-de-los-rios-en-peligro-por-represas/)

Según el mapa publicado en octubre, Caquetá, Putumayo, Guaviare y Meta, son los departamentos que mayor transformación han presentado. Lugares donde el gobierno ha promovido especialmente actividades petroleras y mineras que afectan derechos de las comunidades y con ello el ambiente sano.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
