Title: Versos por la paz y la reconciliación de Colombia
Date: 2015-07-10 15:45
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cultura por la paz de Colombia, Segunda Cumbre mundial de poesía por la paz y reconciliación de Colombia, XXV Festival Internacional de poesía de Medellín
Slug: versos-por-la-paz-y-la-reconciliacion-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/cumbre-mundial-de-poetas-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: movimientofilometajara.blogspot.com 

Desde el 14 hasta el 16 de Julio, en el marco del XXV Festival Internacional de poesía de Medellín, se realizará la segunda edición de la **Cumbre mundial de poesía por la paz y reconciliación de Colombia**, un espacio de expresión que presenta a la **poesía, el arte y la cultura como elementos vitales de reconstrucción de una cultura herida por la guerra**.

Al rededor de **90 poetas y poetizas**, provenientes de **40 países del mundo**, compartirán propuestas surgidas a partir de sus conocimientos y experiencias de vida, marcadas por fuertes conflictos sociales y políticos en algunos casos, que permitan abrir **caminos para la paz y la reconciliación**, con procesos creativos de reconstrucción de memoria que ayuden a sanar heridas y desaparecer odios.

Serán **29 actividades** de corte pedagógico, poético y artistico, entorno a los tres ejes temáticos del evento: Los trabajos de la poesía y el arte en la creación de la paz, para un nuevo lenguaje y una nueva cultura; la poesía y el arte: defensa de la tierra y de los seres vivientes; y La poesía y lo imposible realizable: Acciones globales para transformar la vida.

Las intervenciones de académicos, defensores de DDHH, politólogos, ambientalistas y gestores culturales invitados a la cumbre, servirán para enriquecer el diálogo y las conclusiones resultantes, utilizando como referentes algunas experiencias de procesos de paz, en países como Suráfrica, Irlanda, El Salvador y el País vasco.

Como complemento a las delegaciones originarias de Afganistán, Australia, Chile, China, Cuba, Egipto, El Salvador, España, Francia, Estados Unidos, Guatemala, Grecia, México, Nigeria, Noruega, Palestina y Panamá entre otras, estarán las **organizaciones de derechos humanos**, nacionales y extranjeras, estudiantiles, raízales e indígenas; que nutrirán con sus voces y propuestas al desarrollo del evento.

La segunda Cumbre mundial de poesía por la paz y reconciliación de Colombia, llega 12 años después de su primera edición, en el contexto de las conversaciones que se adelantan en la Habana entre el Gobierno colombiano y la guerrilla de las FARC E-P, y a 8 años del Encuentro Nacional de Poesía y Arte por la Paz, punto de partida para la creación de un movimiento nacional de poetas y artistas por la paz.

[Ver programación completa ](http://www.festivaldepoesiademedellin.org/es/Festival/25/News/ProgramacionIICumbrePoesiaPaz.jpg)
