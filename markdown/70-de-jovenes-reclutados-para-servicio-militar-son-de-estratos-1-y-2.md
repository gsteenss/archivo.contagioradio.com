Title: 70% de jóvenes reclutados para servicio militar son de estratos 1 y 2
Date: 2015-11-23 17:15
Category: Nacional
Tags: ACOOC, alirio uribe, batallones mineroenergéticos, batidas ilegales, Batidas militares, cpdh, Derechos Humanos, Maria Ángela RObledo, ministerio de defensa, objeción de conciencia, proceso de paz
Slug: 70-de-jovenes-reclutados-para-servicio-militar-son-de-estratos-1-y-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/CPDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CPDH 

###### [23 Nov 2015]

El Comité Permanente por la Defensa de los Derechos Humanos (CPDH), socializó el informe *Batidas Militares y Servicio Militar Obligatorio en Colombia: Entre la conciencia y el Modelo de Desarrollo*** **en el que se evidencia que pese a las sentencias de la Corte Constitucional, **persisten las detenciones arbitrarias con fines de reclutamiento, más conocidas como “batidas”.**

El informe presentado en el Centro de Memoria Histórica, Paz y Reconciliación, registra del 2006 al 2014, **88 batidas en Bogotá, de ellas cerca del 70% de los jóvenes que son reclutados hacen parte de estratos uno y dos, de localidades como Usme, Tunjuelito, Sumapaz , Ciudad Bolívar y en el municipio de Soacha. **

Según información de organizaciones defensoras de derechos humanos, en 2013 hubo 32 batidas ilegales, en 2014 se registraron 58 y en lo que va del 2015 ya hay 71. Estas cifras contrastan con los datos entregados por Ministerio de Defensa, que dice que en el 2013 se registraron 20, en 2014 hubo 34 y en lo que va del 2015 van 24. Sin embargo, ambas fuentes finalmente demuestran que los jóvenes que no cuentan con recursos para pagar la tarjeta militar siguen temiendo ser reclutados en las calles de la ciudad.

**En su informe, el CPDH evidencia que las detenciones arbitrarias están ligadas a un modelo de batallones minero energéticos** que se han impulsado desde el año 2001. **Estas unidades militares tienen la función de cuidar las actividades de extracción minero energética** de las multinacionales, de manera que “el discurso de la seguridad sirve de complemento a la inversión extranjera directa”, señala el Comité.

Por otra parte, se denuncia que las autoridades militares continúan manejando una serie de cuotas de reclutamiento que encubren asegurando que se trata de “proyecciones estadísticas”, pero que obedecen a órdenes **del Ministerio de Defensa, por ejemplo, la cuota para este año es 106.161 jóvenes.**

Julián Ovalle, integrante de la Acción Colectiva de Objetores y Objetoras de conciencia (ACOOC), indicó **que existe una “resistencia por parte de la institución”** para brindar las facilidades a las personas que quieren acogerse a esta medida. Este **año se han presentado la mayor cantidad de casos en Bogotá (24), seguido por Villavicencio (23) y por Cali (21)**, sin embargo generalmente este derecho no se concede pues desde el Ministerio de Defensa se argumenta que muchas veces los jóvenes "no acreditan las condiciones exigidas por la Corte Constitucional".

Desde el Ministerio de Defensa se** registra que en lo corrido del 2015 han muerto 13 jóvenes militares,** muchos de ellos en algún momento fueron objeto de estas batidas ilegales y finalmente encontraron la muerte mientras prestaban sus servicios.

http://issuu.com/comitepermanenteddhh/docs/informe\_batidas/1
