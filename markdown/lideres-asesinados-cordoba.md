Title: Desde 2016 hasta la fecha, 28 líderes han sido asesinados en Córdoba
Date: 2019-02-12 17:58
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: cordoba, lideres sociales
Slug: lideres-asesinados-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/lideres-sociales-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Feb 2019] 

Integrantes de organizaciones sociales que trabajan en Córdoba vienen denunciando que allí la guerra nunca se fue y se ha reciclado, lo que se ha traducido en un incremento de la violencia contra líderes y habitantes de la zona. Mientras eso ocurre los cordobeses denuncian que los gobiernos locales y nacional están desinteresados por las situaciones que se presentan en la región.

Andrés Chica, presidente de la Organización Cordoberia, recuerda que de los 28 líderes que han sido asesinados en Córdoba después de la firma del Acuerdo de Paz hasta la fecha, 20 han ocurrido en el sur del Departamento. Allí se han presentado más de 200 solicitudes de protección para líderes sociales ante la Unidad Nacional de Protección (UNP), y desde julio de 2017 se han desplazado forzadamente más de 1600 personas, entre ellos, 14 líderes reconocidos.

Por su parte, la Defensoría del Pueblo ha emitido 14 alertas tempranas sobre el Sur de Córdoba, entre las que se encuentran 5 sobre San José de Uré y 3 de Tierra Alta, en las que se evidencian la guerra entre grupos armados ilegales que se sustentan de negocios ilícitos, la llegada de dineros provenientes de carteles mexicanos y los problemas relacionados con el cultivo, siembra, producción y trasporte de Coca.

Pese a estas advertencias provenientes de la Defensoría, Chica sostiene que los Gobernantes miran con desidia a los líderes sociales, y si ellos no pertenecen a su partido político, no se preocupan por su seguridad. Adicionalmente, el presidente de Cordoberia afirma que Sandra Patricia Devia, gobernadora de Córdoba, no cita a la Mesa de Garantías para líderes si las partes civiles que tienen lugar en este espacio no le suplican que lo haga.

### **Cordobeses: Entre el fuego de los ilegales y la ineficacia del Estado** 

El pasado fin de semana el presidente Duque dijo, desde Coveñas, en el marco de un Taller Construyendo País, que era necesario que los habitantes de Córdoba denunciaran las situaciones que sufren, y aseguró que el Estado no tenía la capacidad para proteger a todos los líderes sociales del país.

Sin embargo, Chica asevera que las personas no se sienten seguras para denunciar ante las autoridades o la Mesa de Garantías, porque los actores ilegales que actúan en el territorio tienen amenazados a todos los habitantes; adicionalmente, resalta que la mayoría de los líderes asesinados están relacionados con la implementación de los Programas de Desarrollo con Enfoque Territorial (PDET) y el Programa Nacional Integral de Sustitución (PNIS) de cultivos de uso ilícito, porque el Gobierno se ha desentendido de estos planes.

Y mientras el Gobierno incumple con las promesas en inversión para los territorios, y los líderes intentan encontrar alternativas de desarrollo económico y vida digna, la respuesta estatal sigue limitada al enfoque militar, que poco ha servido para resolver la crisis vivida en Córdoba durante los últimos años.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
