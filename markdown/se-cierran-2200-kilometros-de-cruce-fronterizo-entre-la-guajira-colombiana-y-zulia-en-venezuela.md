Title: Se cierran 2200 kilómetros de cruce fronterizo entre la Guajira Colombiana y Zulia en Venezuela
Date: 2015-09-08 16:32
Category: El mundo, Política
Tags: Cierre fronterizo, Crisis con venezuela, Deportados colombianos, Frontera Paraguachón, Juan Manuel Santos, Nicolas Maduro
Slug: se-cierran-2200-kilometros-de-cruce-fronterizo-entre-la-guajira-colombiana-y-zulia-en-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/paraguachon1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Viendo.me 

###### [08 Sep 2015] 

El gobierno venezolano amplió el cierre fronterizo con Colombia a lo largo de **2200 kilómetros en el cruce de Paraguachón**, donde el Estado de Zulia en Venezuela, conecta con la Guajira colombiana. Para este cierre Maduro **ordenó movilizar 3.000 funcionarios de la Fuerza Armada** para proteger el paso fronterizo.

Según explica el presidente de Venezuela, Nicolás Maduro, la decisión fue tomada en el marco de la **Operación de Liberación del Pueblo, **para combatir el paramilitarismo y contrabando de alimentos y combustible. Además aseguró que para tomar esta decisión el Presidente venezolano consultó con las comunidades wayú de La Guajira, quienes además le propusieron una serie de medidas para tomar en la frontera.

Así mismo se decretó un estado de excepción en tres municipios los municipios de Mara, Guajira y Almirante Padilla, a los el gobierno venezolano, denominó como zona número 03, y autorizó la libre circulación de las comunidades indígenas por el área fronteriza.

Este anuncio se dio después de que el presidente **Juan Manuel Santos, reiterara su disposición de reunirse con su homólogo venezolano** con la mediación del presidente de Uruguay, Tabaré Vásquez, quien se ofreció a facilitar el diálogo.
