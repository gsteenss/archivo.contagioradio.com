Title: La Cumbre Agraria se toma Bogotá del 31 al 5 de Septiembre
Date: 2015-08-26 08:06
Category: Movilización, Nacional
Tags: Cumbre Agraria Étnica y Popular, Dignidad campesina, movilización campesina, Paro agrario 2013
Slug: la-cumbre-agraria-se-toma-bogota-del-31-al-5-de-septiembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/image1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: diarioadn 

<iframe src="http://www.ivoox.com/player_ek_7601966_2_1.html?data=mJudk56aeo6ZmKiak5uJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8rixNSYz87Qb8TVztXS1c7Ss9Sf1MqY1tTRpdOZpJiSo5bSb6PjyNThh6iXaaKljNnfw9iPqNDnjJeYw4qnd4a2kpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Cesar Jerez,  ANZORC / Francisco Alvarado, Dignidad Agropecuaria] 

###### [25 Ago 2015] 

Desde este lunes se adelantan movilizaciones campesinas en distintas regiones del país denunciando públicamente el incumplimiento del gobierno nacional frente a los acuerdos pactados para la finalización del Paro Agrario de 2013, así como el anunciado **recorte presupuestal en gastos de inversión social para 2016.**

Tras dos años de negociaciones entre el gobierno de Juan Manuel Santos y organizaciones campesinas colombianas como la Cumbre Agraria Étnica y Popular y Dignidad Campesina, no **ha habido acciones concretas que proyecten mejoras en el sector rural en Colombia**, por el contrario el Ministerio de Hacienda ha anunciado recorte en gastos de inversión social para el 2016 y actualmente cursan proyectos de ley como las llamadas Zonas de Interés de Desarrollo Rural Económico y Social, ZIDRES, que habilitarían zonas rurales para agronegocios e inversión extranjera, yendo en contravía de las exigencias del sector agrario en Colombia y atentando contra el medio ambiente.

El principal incumplimiento tiene que ver con uno de los acuerdos que se pactaron entre el gobierno y las organizaciones campesinas, tras el Paro Agrario de 2013 y que tenía que ver con una inyección cercana a \$1000.000.000 para cada una de las regiones que luego fue negada por el gobierno, anunciado que esa era una suma que se repartiría entre todas las regiones.

Cesar Jerez, vocero de la Asociación Nacional de Zonas de Reserva Campesina, ANZORC, anuncia que cerca 5000 campesinos llegaran a Bogotá para convocar una asamblea entre el 30 de agosto y el 5 de septiembre, con el objetivo de **llamar la atención del sector urbano sobre las problemáticas del campo colombiano**. En el marco de movilizaciones locales y regionales que denunciaran entre otras, la **falta de voluntad política del gobierno**, principalmente del actual ministro de agricultura, Aurelio Iragorri Valencia, luego de dos años de interlocución; demostrando así que existe un **movimiento rural activo capaz de modificar la desigual correlación de fuerzas**.

Así mismo, Jerez denuncia que la actual resolución del INCODER a través de la cual se establece que las tierras de la región de La Macarena pertenecen a las FARC y que por tanto, las comunidades campesinas allí presentes son testaferras, es una nueva estrategia de **expropiación, estigmatización y criminalización del campesinado colombiano**. Así que se espera que estas movilizaciones llamen la atención de la sociedad sobre la estrategia criminal y mediática del Estado contra el movimiento campesino en Colombia y a su vez, posibiliten una mayor coordinación que obligue al gobierno nacional a cumplir con los acuerdos pactados.

Por su parte, Francisco Alvarado, vocero de Dignidad Agropecuaria, afirma que en departamentos como Cauca, Antioquia y Boyacá, se están comenzando las movilizaciones frente al incumplimiento del gobierno nacional,  pues de los 5.2 billones aprobados para el sector agrario en Colombia no sea han visto mayores resultados, los créditos han sido mínimos y restrictivos,  los insumos han subido en un 45% y los abonos han doblado su precio. Sumado a la grave situación de los cafeteros a quienes les fueron asignados recursos a través del Programa de Protección del Ingreso Cafetero,  para aliviar precio bajo del café y que no les han sido entregados pues el programa ya no está en marcha y pese a que el dólar esta caro el café sigue vendiéndose muy barato.
