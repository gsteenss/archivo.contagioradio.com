Title: En huelga de hambre internos de La Dorada y Girón
Date: 2016-05-02 15:18
Category: DDHH, Nacional
Tags: carcel la dorada, carcel palo gordo, cárceles colombia, huelga carceles colombia
Slug: en-huelga-de-hambre-internos-de-la-dorada-y-giron
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Huelga-hambre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Panorama San Jorge ] 

###### [2 Mayo 2016 ]

Desde este domingo los detenidos de los patios 5 y 6 del establecimiento penitenciario y carcelario de alta seguridad de Doña Juana en La Dorada, se declararon **en huelga de hambre ante la falta de atención médica** que padecen desde hace un mes y que ha puesto en alto riesgo su integridad personal.

De acuerdo con el Movimiento Nacional Carcelario, los detenidos han decidido no recibir ni consumir ninguna clase de alimentos en respuesta a la violación sistemática del derecho a la salud que han venido enfrentando, como muestra de la **crisis general en materia de salud que enfrenta la población reclusa en toda Colombia**.

Esta huelga se suma a la que días atrás iniciaron internos de la cárcel de 'Palo Gordo' de Girón, Santander, con el fin de llamar la atención de las autoridades nacionales para que den una solución definitiva a esta **grave problemática que cada día cobra vidas. **

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

<div>

</div>
