Title: Asesinan al comunero Iván Mejía, víctima de la crisis humanitaria en el Cauca
Date: 2019-08-27 10:26
Author: CtgAdm
Category: DDHH
Tags: asesinato, Asesinato a líderes, CRIC, indígenas cauca, ONIC
Slug: muere-lider-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Contagio Radio] 

Cerca de las 6:40 de la tarde de este 26 de agosto fue asesinado el **comunero indígena Iván Mejía**, en el Resguardo de López Adentro, en el sector Bocatoma corregimiento del Palo en Caloto Cauca. Hasta el pasado 13 de agosto el estimado de indígenas asesinados durante el gobierno Duque llegaba a  97 personas, cifra que se eleva con la muerte de Iván.

Información aportada por miembros del Consejo Regional Indígena del Cauca (CRIC), como Joe Sauca coordinador de Derechos Humanos, indican que varios impactos de arma de fuego, fueron la causa de la muerte. Aún se desconoce la identidad de los agresores, así mismo  allegados a la víctima informan que este no había recibido amenazas o sentencia alguna en los últimos meses. [(Lea también: Plan pistola en marcha contra líderes indígenas: ACIN)](https://archivo.contagioradio.com/plan-pistola-en-marcha-contra-lideres-indigenas/)

Según el Congreso de los pueblos con este asesinato ** ya son 7 fallecidos  y 30 atentados a la comunidad indígena en lo corrido del mes de agosto**. [(Le puede interesar: Durante gobierno Duque han sido asesinados 97 comuneros indígenas: ONIC)](https://archivo.contagioradio.com/onic-gobierno-duque-97-indigenas-asesinados/)

Noticia en desarrollo.

**Síguenos en Facebook:**  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
