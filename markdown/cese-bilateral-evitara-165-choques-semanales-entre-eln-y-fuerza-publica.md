Title: Cese bilateral evitará 165 choques semanales entre ELN y Fuerza Pública
Date: 2017-09-11 21:00
Category: Nacional, Paz
Tags: ELN, Enfrentamientos, Fuerzas militares, paramilitares
Slug: cese-bilateral-evitara-165-choques-semanales-entre-eln-y-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/cese-bilateral-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: Archivo] 

###### [11 Sept 2017]

Recientes cifras entregadas por el Ejército y que fueron citadas en una publicación del ELN indican que se presentan 165 choques semanales entre Fuerza Pública y ELN y a ellas deben sumarse los choques con paramilitares. Sin embargo, el cese bilateral pactado para el primero de Octubre tiene el reto de reducir estos incidentes a cero y aclimatar las conversaciones de paz, así como facilitar el desarrollo del punto sobre participación.

Según la revista Voces de esa insurgencia "(...) la situación se muestra en toda su dimensión y gravedad (...) las estadísticas oficiales ponen en evidencia que **el conflicto armado sigue siendo una realidad en nuestro país"** resalta el escrito. Asegura el ELN que los hechos reportados en las últimas semanas han sido atribuidos y reportados por el Frente de Guerra Capitán Omar G y el frente de Guerra Oriental. Le puede interesar: [Cese bilateral entre ELN y gobierno Nacional es Histórico: Álvaro Villarraga](https://archivo.contagioradio.com/cese-bilateral-entre-eln-y-gobierno-nacional-es-hstorico-alvaro-villarraga/)

En la publicación se hace énfasis en que en varios territorios y  en algunas de las operaciones habría complicidad de la Fuerza Pública, "Si a esos registros se suman los enfrentamientos de nuestras fuerzas con grupos paramilitares (aunque actúan en contubernio con el Estado en la mayor parte de los casos, no contabilizan en esa estadística), la situación se muestra en toda su dimensión y gravedad." Le puede interesar: [Enfrentamientos entre paramilitares y ELN afectan comunidades en Chocó](https://archivo.contagioradio.com/46439/)

Además resaltan que el estado de las confrontaciones es grave y de importante magnitud, así no se reseñe en los medios de comunicación "la contundencia de las estadísticas oficiales pone en evidencia que **el conflicto armado sigue siendo una realidad en nuestro país**." afirman en la publicación. Le puede interesar: [Iglesia y ONU serán veedoras del cese bilateral entre el gobierno y ELN](https://archivo.contagioradio.com/la-iglesia-seria-veedora-del-cese-bilateral-entre-el-gobierno-y-el-eln/)

### **El cese al fuego representará un alivio** 

En todo caso, como lo señalan diversos analistas consultados por Contagio Radio, el cese bilateral que comenzaría a regir el próximo 1 de Octubre representará un alivio para las comunidades que se ven enfrentadas al conflicto casi a diario, un alivio para las conversaciones de paz que se desarrollan en Quito y una oportunidad para que la participación, uno de los puntos de la agenda de conversaciones, se desarrolle de la mejor y mas eficaz manera posible.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
