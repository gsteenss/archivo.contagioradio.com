Title: "Gobierno debe dejar de enfrentarnos": campesinos Cauca
Date: 2016-09-24 13:33
Category: Movilización, Nacional
Tags: efectos de la minería, Gran mineria en el Cauca, Indígenas del Cauca
Slug: gobierno-debe-dejar-de-enfrentarnos-campesinos-cauca
Status: published

###### [Foto: Archivo Contagio Radio ] 

###### [24 Sept 2016] 

Tras cuatro días de movilización, las comunidades campesinas del Cauca lograron llegar a acuerdos con el Gobierno nacional, en cabeza de los Ministerios de Agricultura, Interior, Ambiente, Minas y Cultura, quienes **se comprometieron a trabajar de manera permanente para solucionar las problemáticas** **que enfrentan los pobladores** en términos territoriales, de educación, salud, vivienda y trabajo, principalmente.

De acuerdo con Nilsón Liz, presidente de la Asociación Nacional de Usuarios Campesinos del Cauca, a la mesa de concertación que viene funcionando con MinAgricultura se articularán los Ministerios de Cultura, Interior, Ambiente y Minas quienes sesionarán permanentemente con las comunidades para tratar los problemas de propiedad territorial, educación, salud, vivienda y trabajo, partiendo del **reconocimiento de los campesinos como sujetos de derechos**.

El líder asegura que uno de los problemas más álgidos a los que se enfrentan es la disputa territorial a la que el Estado colombiano ha llevado a las comunidades indígenas, afro y campesinas, con la compra de predios para resguardos indígenas, en territorios de pobladores campesinos o afrodescendientes; por lo que **le exigen al Gobierno nacional que nos los enfrente** y encuentre [[solución a estas disputas sin vulnerar ninguno de sus derechos](https://archivo.contagioradio.com/18-mil-comuneros-indigenas-de-la-acin-se-mantienen-en-minga-en-el-cauca/)].

Otra de las problemáticas territoriales a las que se enfrentan las comunidades son los [[impactos sociales](https://archivo.contagioradio.com/asesinan-a-tres-ambientalistas-en-cauca/)] y ambientales que ha traído la minería legal e ilegal, que según refiere el líder, ha implicado cambios drásticos en los ecosistemas y en las costumbres campesinas, pues resulta más rentable emplearse sacando y vendiendo minerales, que sembrando cultivos de pancoger, por lo que demandan la consolidación de **estrategias de subsistencia en las que se preserve el ambiente y la cultura campesina**.

"Creemos que este acuerdo tendrá que partir de una buena voluntad y en ese sentido le hemos exigido al Gobierno nacional que, **sí no quiere ver más movilizaciones y paros, defina una clara postura de compromiso y de cumplimiento**. Así mismo le hemos dicho que [[la próxima movilización o paro](https://archivo.contagioradio.com/la-comunidad-y-los-medios-regionales-saben-que-la-fuerza-publica-mato-a-los-indigenas-onic/)] que hagamos no va a ser en las mismas condiciones", concluye el líder.

<iframe id="audio_13064630" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13064630_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
