Title: De enero a julio de 2018 se registraron 121 amenazas a periodistas: FLIP
Date: 2018-08-03 14:15
Category: DDHH, Nacional
Tags: FLIP, Ministro de Interior, Nancy Patricia Gutiérrez, Periodistas amenazados
Slug: 121-amenazas-periodistas-flip
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/violencia-prensa-e1501178542597.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [3 Ago 2018] 

Ante la creciente ola de amenazas contra periodistas en el país; directores de diferentes entes de control nacional y representantes tanto del gobierno Santos como del entrante gobierno, rechazaron en un comunicado los hechos de violencia y se comprometieron a tomar medidas suficientes para garantizar el ejercicio de la prensa libre en el país.

El pronunciamiento, surgió tras la reunión sobre la libertad de prensa y el derecho a la información en Colombia, convocada por la Asociación Colombiana de Medios de Información (AMI) y la Fundación para la Libertad de Prensa (FLIP). (Le puede interesar: ["Alerta por incremento de amenazas contra periodistas en Colombia"](https://archivo.contagioradio.com/incrementan-amenazas-periodistas/))

A dicho encuentro asistieron el Fiscal General, Néstor Humberto Martínez; el Procurador  Fernando Carrillo Flórez; el Defensor del Pueblo, Carlos Alfonso Negret; el Ministro del Interior, Guillermo Rivera; el Director de la Unidad Nacional de Protección, Diego Mora y la vocera de la comisión de empalme del gobierno Duque, Nancy Patricia Gutiérrez.

Para Pedro Vaca, director de la FLIP, el balance de la reunión fue positivo, dado que permitió al Estado en su conjunto manifestarse frente a las intimidaciones que sufren los periodistas. Sin embargo, alertó que pese a los avances en investigaciones que anunció el Fiscal, se debe mejorar la atención a las víctimas de las amenazas, específicamente en las regiones, y habilitar unas rutas de atención para reporteros en riesgo.

### **"121 amenazas han recibido los periodistas hasta julio de 2018, 8 menos que en todo el 2017": Pedro Vaca** 

Según Vaca, en Colombia se siguen presentando agresiones graves contra la prensa, que hacen parte de una agresión generalizada contra los periodistas por parte de agentes de la fuerza pública y actores armados ilegales, con especial énfasis en los sectores rurales del país.

A esos ataques, se suma el incentivo que están recibiendo los ciudadanos para atacar a la prensa libre que se encarga de narrar las diferencias, y que podría terminar llevándolos a promover la censura; las instancias de protección para periodistas amenazados que existen en el país, que según Vaca "son lentas, reactivas, caras y deben ser reconsideradas en el modelo actual"; y el hecho de que el Estado, como garante de la libertad de prensa debe establecer una política de protección que brinde mejores condiciones para periodistas y líderes.

Adicionalmente, el Contexto electoral del presente año ha sido un generador de violencias contra periodistas, hecho que se refleja en las cifras presentadas por Vaca según las cuales en 2014 se presentaron 65 amenazas a comunicadores, 129 en 2017, mientras que en apenas 7 meses de 2018, ya se han denunciado 121 amenazas.[ ]

<iframe id="doc_50247" class="scribd_iframe_embed" title="Comunicado de autoridades frente al aumento en amenazas a periodistas" src="https://www.scribd.com/embeds/385389921/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-sQnAdqE1HdyJP4t8dYqy&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.707221350078493"></iframe>  
<iframe id="audio_27580670" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27580670_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
