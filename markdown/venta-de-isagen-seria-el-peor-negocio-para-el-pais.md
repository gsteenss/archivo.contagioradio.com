Title: Venta de Isagen "sería el peor negocio para el país"
Date: 2015-09-11 16:43
Category: Economía, Entrevistas, Política
Tags: Consejo de Estado, Crisis fiscal en Colombia, ISAGEN, Patrimonio de la nación, RECALCA, Soberanía Energética, Venta de Isagen
Slug: venta-de-isagen-seria-el-peor-negocio-para-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Isagen.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cmi] 

###### [11 Sept 2015] 

[Los magistrados de la Sección Cuarta del Consejo de Estado aprobaron la ponencia de Hugo Bastidas para continuar con la **venta de la electrificadora Isagen, en oposición a la demanda de trabajadores y extrabajadores** que argumentan que **esta venta genera graves riesgos económicos para el patrimonio de la nación**.]

[Mario Valencia, analista de la Red Colombiana de Acción Frente al Comercio RECALCA, asegura que **la venta de Isagen atenta contra la soberanía energética del país y agudiza la actual crisis fiscal por la que atraviesa la nación** “esta decisión es una muestra de que los intereses económicos privados primaron, los únicos beneficiados serán los inversionistas extranjeros que aprovechen la ganga”.]

[“La enajenación de Isagen representa la **imposibilidad de regulación de los costos de la energía en el mercado**, pues al dejar de ser una empresa pública el Estado colombiano ya no tiene competencia para impedir alzas como ocurre con Electricaribe”, agrega Valencia.]

[Hace dos años la empresa estaba avaluada en 5 billones de pesos y pese a que **en estos momentos tiene un mejor manejo técnico y administrativo**, adelanta proyectos de energía térmica y eólica, **el gobierno de Juan Manuel Santos ha decidido devaluarla** en 1.600 millones de dólares que serán destinados para la construcción de vías 4G, aun cuando organismos como la Contraloría han afirmado la inviabilidad del proyecto.]

Vea también: [Con venta de Isagen Estado dejaría de recibir 225 mil millones de pesos anuales](https://archivo.contagioradio.com/con-venta-de-isagen-estado-dejaria-de-recibir-225-mil-millones-de-pesos-anuales/)
