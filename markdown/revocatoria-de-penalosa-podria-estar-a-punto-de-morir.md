Title: Revocatoria de Peñalosa podría estar a punto de morir
Date: 2018-04-24 15:02
Category: Movilización, Nacional
Tags: Consejo Nacional Electoral, Peñalosa, revocatoria
Slug: revocatoria-de-penalosa-podria-estar-a-punto-de-morir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/marcha_de_las_antorchas_para_revocar_a_penalosa_foto_blu_radio_2.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blu Radio] 

###### [24 Abr 2018] 

El partido Polo Democrático interpuso un incidente de desacato contra el Consejo Nacional Electoral por su falta de pronunciamiento e incumplimiento al fallo del Tribunal Administrativo de Cundinamarca, en donde se solicitaba emitir un concepto frente a los montos usados por los comités de Revocatoria. **Este incumplimiento podría significar que definitivamente ya no quede tiempo en el calendario electoral para realizar la revocatoria**.

De acuerdo con Carlos Carrillo, integrante del comité “Unidos Revocaremos a Peñalosa”, esta falta de respuesta del CNE demuestra que no está actuando en derecho, **“si ellos tuvieran las pruebas de que el comité violó los topes, las habrían mostrado**”. Asimismo, aseguró que todos los obstáculos y las demoras que uso esta autoridad, evidencian un intento “a toda costa de defender a Enrique Peñalosa”.

Situación que podría estar dando frutos para el Alcalde, debido a que los tiempos electorales se han acortado y de no haber un fallo pronto sobre el desacato, ya no se podría realizar la revocatoria. “Dentro de un mes, la alcaldía de Peñalosa podría estar salvada” afirmó Carrillo y reiteró que se tiene el tiempo exacto para activar este mecanismo.

### **“La ciudadanía debe exigir la renuncia de Peñalosa”: Carrillo** 

Frente a este escenario, Carrillo afirmó que la ciudadanía debe movilizarse, “no podemos permitir que este tipo de abusos nos bajen la moral”. De igual forma aseguró que hasta el 31 de diciembre de 2019, se tendrán muchas actividades e iniciativas manifestando su oposición al gobierno de Peñalosa. (Le puede interesar: ["Nuevo round entre promotores a la revocatoria a Peñalosa y el CNE"](https://archivo.contagioradio.com/nuevo-round-entre-promotores-de-la-revocatoria-a-penalosa-y-el-cne/))

<iframe id="audio_25601866" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25601866_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
