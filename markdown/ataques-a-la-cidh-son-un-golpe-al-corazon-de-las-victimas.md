Title: Ataques a la CIDH son un golpe al corazón de las víctimas
Date: 2020-08-27 22:54
Author: AdminContagio
Category: Actualidad, DDHH
Tags: CIDH
Slug: ataques-a-la-cidh-son-un-golpe-al-corazon-de-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/12038203_946776042035673_4514528148313693581_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: CIDH

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la decisión del **Secretario General de la Organización de Estados Americanos (OEA), Luis Almagro,** de no avanzar en el proceso de renovación del mandato de su Secretario Ejecutivo, **Paulo Abrão**, elegido en 2016; la [Comisión Interamericana de Derechos Humanos](https://twitter.com/CIDH) (CIDH) ha expresado su [rechazo](http://www.oas.org/es/cidh/prensa/comunicados/2020/202.asp) ante lo que consideran un golpe a la autonomía de una institución que durante más de 60 años ha promovido la veeduría y la defensa de los derechos humanos en la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la Comisión Interamericana decidió por unanimidad renovar el mandato a su Secretario Ejecutivo para el periodo 2020-2024 en enero de 2020, el pasado 15 de agosto el Secretario General de la OEA comunicó a la CIDH, sin consulta previa, su determinación unilateral de abstenerse “de avanzar en el proceso de nombramiento del Secretario Ejecutivo”, hecho que a los ojos de la Comisión "constituye un franco desconocimiento de su independencia y autonomía".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La **vicepresidente de la Comisión, Antonia Urrejola** resalta que no se trata de una cuestión de reputación ni de defender en particular a Paulo Abrão, sino que es un debate que va mucho más allá y que define la independencia y autonomía de la CIDH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Explica además que el rol del secretario ejecutivo, es transversal para el trabajo de los siete comisionados que constituyen la organización y que no tienen su sede en Washington sino que trabaja cada uno desde su país por lo que resulta el funcionario de mayor confianza pues participa en las reuniones, debates y da instrucciones precisas a los demás integrantes de la Comisión en el continente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, **Javier Galindo, coordinador de litigio internacional de la Comisión Colombiana de Juristas** recalca que Luis Almagro debe consultar este tipo de decisiones con los comisionados e informar los motivos de su decisión, procedimiento que no se ha realizado por lo que estaría viciado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Aunque el secretario genial designa al secretario ejecutivo, no se trata de la designación, se trata de renovar su mandato", proceso que se realizó en enero de 2020 y que durante ese lapso no recibió cuestionamiento alguno por parte del Secretario General.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La independecia de la CIDH ante posturas políticas y financieras

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque Urrejola señala que no tiene argumentos para conocer las razones que motivan a Almagro, en el contexto en el que se encuentra el continente supone que se trata de una decisión política y no administrativa y la injerencia que pueden tener algunos Estados, que no siempre han expresado estar satisfechos con la labor de la Comisión.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Un ejemplo de ello fue la carta firmada en 2019 por cinco países incluidos **Argentina, Colombia, Paraguay, Brasil y Chile** quienes se dirigieron a la Secretaría Ejecutiva de la CIDH para hacer cuestionamientos sobre sus facultades y destacando su autonomía como naciones por encima de la de la Comisión. [(Le puede interesar: Radican en la CIDH alerta contra avance del fracking)](https://archivo.contagioradio.com/ante-cidh-se-radica-alerta-en-contra-del-avance-del-fracking/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sumado a esta situación política, la vicepresidente de la Comisión alerta cómo se viene poniendo en duda el cumplimiento del Acuerdo de Cancún firmado en 2018 en relación con la duplicación del presupuesto aprobado por la Asamblea General de la OEA y que de nos ser cumplido debilitaría la incidencia de sus funciones "poniendo en duda la independencia de la Comisión pues los Estados pueden usar el tema financiero" para castigar o aportar a la CIDH.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La CIDH es más necesaria que nunca en Colombia y el continente

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para **Gina Romero, directora ejecutiva de la Red Latinoamericana y del Caribe para la Democracia**, lo que viene sucediendo al interior de la institución, mina la credibilidad y confianza del sistema, "no se habla de la defensa de una persona sino de la solidez y la capacidad de una institución para hacer cumplir su reglamento interno" sin dejar de lado, su capacidad de ser coherente con sus propios principios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La comisionada Urrejola agrega que en paralelo a esta situación se encuentra la contingencia del Covid-19, pandemia que ha generado una mayor crisis de DD.HH. en el continente y que ha significado un esfuerzo adicional para la CIDH exigiendo el cumplimento de políticas públicas, participación de organizaciones en derechos a la salud y transparencia de información, crisis que prepara al continente para un resurgir de la movilización social ante la ausencia de garantías para la ciudadanía.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Es un momento muy sensible para la gente en el actual contexto y en la post-pandemia vamos a ver mayores manifestaciones y un mayor uso desproporcionado de la fuerza (...) nos va poner en una situación de tensión con los Estados, si ya estábamos en una mayor efervescencia social lo que viene va a ser peor".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante ese escenario, **Gina Romero,** resaltó la importancia de instituciones como la CIDH que velan por la protección de los derechos de la población ante acciones del gobierno que no serán reconocidas o analizadas por los mandatarios de forma diferente.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio del contexto colombiano, **Bayron Góngora, director de la Corporación Jurídica Libertad** señala lo delicado que resulta atestiguar cómo el Gobierno ha llevado a sus ex funcionarios a dirigir los organismos de control e investigación como la Fiscalía y la Procuraduría y lo peligroso que resulta para la división de poderes y donde más se requiere una veeduría. [(Lea también: CIDH condena al Estado en el caso de Gustavo Petro)](https://archivo.contagioradio.com/cidh-condena-al-estado-por-la-destitucion-en-la-alcaldia-de-gustavo-petro/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"No podemos perder el foco de que en Colombia está la implementación del Acuerdo de Paz y la CIDH le ha hecho un seguimiento, además de las agresiones contra líderes y excombatientes a las que también se ha hecho seguido". [(Le puede interesar: Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes)](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre la crisis humanitaria que vive el país, la comisionada Urrejola expresó su preocupación y la de Comisión, que permanece al tanto de los numerosos ataques contra defensores, las masacres y las denuncias de violencias sexuales por parte del Ejercito en regiones donde "vemos que el vacío del Estado en pandemia se ha agudizado".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las víctimas las verdaderas afectadas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Galindo resalta las múltiples peticiones y casos que recibe año tras año la CIDH que no han sido resueltas por la falta de personal y que ahora, mientras se prolongue esta disyuntiva se verán más retrasadas y por tanto tardarán más en ser llevadas a la Corte Interamericana, que ha manifestado tener capacidad de recibir más casos de lo que están recibiendo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Muchas de las víctimas claman que estos procesos se aceleren pero entre una etapa y la otra pasan años en que se quedan estancados", explica el abogado quien resalta cómo no existirá celeridad en los procesos ante esta eventualidad. [(Lea también: ONU y CIDH estarían preparando pronunciamientos sobre situación de DDHH en Colombia: CEJIL)](https://archivo.contagioradio.com/onu-cidh-cejil-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, el director de la Corporación Jurídica Libertad exalta el rol de el sistema interamericano como "una esperanza para los pueblos que no encuentran en los sistemas judiciales internos la posibilidad de obtener justicia", tomando como ejemplo los casos de poblaciones del Chocó que ahora representan y que en 1997 fueron víctimas de agresiones por parte del Ejército y grupos paramilitares que actuaban en conjunto "es impresionante cómo las instituciones colombianas no hicieron absolutamente nada, ni la Fiscalía, ni la Procuraduría, nadie escuchó la voz de las víctimas pidiendo protección".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El experto en litigio en internacional complementa la idea, afirmando que la esperanza de las víctimas es mayor en el sistema interamericano pues ya no confían en las instituciones nacionales, incluso si el proceso puede tardar años, acuden a esta instancia en la búsqueda de verdad y de una resolución real. **"Cuando una víctima acude a un tercero y se da a la pelea de hacer un proceso es porque está parada en la idea de que va a recibir justicia y esa esperanza no existiría sino hubiera unos terceros que acogen la defensa de su caso"**, concluye Gina Romero

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
