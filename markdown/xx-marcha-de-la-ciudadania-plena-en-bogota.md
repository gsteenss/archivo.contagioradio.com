Title: El domingo 28 de Junio se realizará la XX marcha de la ciudadanía plena en Bogotá
Date: 2015-06-27 09:51
Category: LGBTI, Nacional
Tags: Closet Abierto, Colombia libre de prejuicios, Derechos Humanos, LGBTI, Marcha del orgullo gay, Marcha por la ciudadanía plena
Slug: xx-marcha-de-la-ciudadania-plena-en-bogota
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4694925_2_1.html?data=lZumlp6WeY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRlsrXwtfR0ZCxs8%2FoxtPSydfTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ricardo Montenegro] 

El próximo **28 de Junio** se celebra el día internacional de orgullo gay que gira entorno a toda la comunidad **LGBTI**, conmemorando los disturbios del **Stonewall** en **Nueva York**, **Estados Unidos de 1969**, que dan inicio al movimiento de liberación homosexual.

Cada año se realizan una serie de eventos a nivel nacional e internacional con el objetivo de visibilizar a la comunidad **LGBTI** y promover la tolerancia e igualdad, los eventos más relevantes son las marchas que se realizan en las ciudades más importantes de cada país. En Bogotá se realizará la **XX marcha por la ciudadanía plena**, bajo el lema: “**Colombia  libre de prejuicios**”.

Este domingo se reunirán las organizaciones y colectivos más importantes, se espera también que se acerquen personas que no hagan parte de la comunidad **LGBTI** para que conozcan este movimiento social, el punto de partida será el **Parque Nacional** a partir de las **2:00 p.m.**, la movilización avanzará por la carrera séptima hasta la **Plaza de Bolivar**, donde se llevarán a cabo una serie de actividades en pro de la diversidad sexual.

**Ricardo Montenegro** de la **Subdirección LGBT** exttiende la invitación a la marcha: "Queremos una Colombia libre de prejuicios, todos y todas están cordialmente invitados a acompañarnos, a conocernos y conmemorar un aniversario más del 28 de Junio de 1969, fecha donde el movimiento social **LGBT** logra por primera vez el reconocimiento de derechos y el reconocimiento de que somos ciudadanos de primer grado".
