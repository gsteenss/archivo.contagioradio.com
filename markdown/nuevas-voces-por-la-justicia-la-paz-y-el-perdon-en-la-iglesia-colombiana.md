Title: Nuevas voces por la justicia, la paz y el perdón en la iglesia católica colombiana
Date: 2017-02-13 06:00
Category: Abilio, Opinion
Tags: iglesia católica, paz, Perdon, Teologia
Slug: nuevas-voces-por-la-justicia-la-paz-y-el-perdon-en-la-iglesia-colombiana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/la-paz-y-el-perdón-en-la-iglesia-colombiana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Tomadas de internet 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 13 Feb 2017

[Empiezan a trascender en la vida nacional signos esperanzadores de una iglesia católica colombiana que eleva su voz a  favor de las víctimas, la justicia y apoyo a los procesos de transformación social. Desde la invisibilidad de la hormiga  en el compromiso evangélico y social las palabras de algunos  obispos llaman las cosas por su nombre. Lo hicieron tomando postura ante la tibia orientación a la  feligresía en el plebiscito de refrendación de los acuerdos entre el gobierno y las FARC-EP, donde se impuso el no, por escaso margen, tras  las mentiras que los enemigos del acuerdo  expandieron como pólvora por todo el país. También, en parte este mismo grupo,  dentro de los que se encuentran **Darío Monsalve, arzobispo de Cali,  Oscar Alberto Sánchez,  obispo de Tibú, Juan Barreto,  obispo de Quibdó , se ha comprometido en la facilitación  de los diálogos entre el gobierno  y el ELN**.]

[Llama la atención las posturas evangélicas asumida [por el obispo de Jericó Noel  Londoño y otros obispos frente a la minería](https://www.cec.org.co/sites/default/files/ComPrensa-Mineria%2806-07-2016%29.pdf). Así mismo la del obispo de Apartadó  Alberto Torres, quien con  voz profética rompió  la historia de complicidades que por décadas vinculaba a sectores de su diócesis con el paramilitarismo que tantos crímenes y despojos ha provocado en la población de Urabá, el Bajo Atrato  y Córdoba. En su reciente [comunicado](https://www.cec.org.co/sites/default/files/Comunicado-prensa-dioc-Apartado.pdf)) (ver:  habla de la legítima participación en política de nuevos movimientos y partidos que se constituyen tras el acuerdo de paz y de los crímenes que en razón de esa opción se vienen perpetrando en la región y en el país; del aumento del paramilitarismo en el área y llama al gobierno a que renuncie a los  eufemismos  para calificar estas estructuras “lo que todos reconocemos como paramilitarismo”;  del silenciamiento a los reclamantes de tierras; del control de zonas  para el narcotráfico y que se aprestan para la minería ilegal.]

[En este contexto se hizo pública, también,   una carta de pedido de perdón firmado 400 católicos colombianos y 84 internacionales,  entre los que se encuentran  taxistas, amas de casa, empleadas del servicio domestico, teólogos y teólogas, sacerdotes, religiosas y profesionales de diversos oficios, y al que se siguen sumando virtualmente decenas de católicas de Colombia y del mundo. Da cuenta  de la necesidad de expresiones claras, directas que alivien  el peso que  miembros de la iglesia, comprometidos con la vida,  cargan. Sobre sus hombros recae  un pasado y un presente de apoyo a la violencia: respaldo al partido conservador contra los liberales; persecución al partido comunista; bendición a las armas de militares implicados en crímenes atroces como los “falsos positivos”;  negación de sacramentos a quienes pensaban diferente;  compromiso de sacerdotes con el paramilitarismo.(  ][[Lea el resumen de la carta aquí.]](https://docs.google.com/document/d/1Dht7jPZve7Xo9wXhq6baRIJRbw-CsNAsIKB8kelvS_I/edit?usp=sharing)[ ][[Lea la carta completa aquí).]](https://docs.google.com/document/d/1olMGOcNAil0HnGMtq7UMIfiNrVRmLVV6RsgAna7anks/edit?usp=sharing)

[Además de pedir perdón directamente a Dios y a las víctimas por todas esas graves responsabilidades,  invitan fraternal, humilde  y respetuosamente a la Conferencia Episcopal Colombiana  a leer algún texto de pedido de perdón antes de la Semana Santa de éste año, también a celebrar un acto público de perdón en el que se invite  miembros del partido comunista y liberales ofendidos históricamente. A su vez escriben al papa Francisco la solicitud de cerrar  la diócesis castrense desde donde se han bendecido y exculpado  tantos crímenes de militares,  y piden al arzobispo de Bogotá sacar los restos del conquistador Gonzalo Jiménez de Quesada de la Catedral primada, en razón de los cientos de crímenes de indígenas y el saqueo de oro que propició. Este símbolo radical de rectificación de comportamientos de los cristianos haría mucho bien a la conciencia colectiva de la nación y podría animar a otras instituciones y personas a hacer su pedido de perdón.]

Estos signos de esperanza  encuentran en el papa Francisco razones  para su afianzamiento, a pesar de las oposiciones que su postura, apegada al Evangelio ha provocado en la curia vaticana,  como las que seguramente sobrevendrán en Colombia  ante las  decisiones audaces y evangélicas que se vienen tomando por parte de sectores del laicado, religiosas, sacerdotes y algunos obispos.

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

#### 
