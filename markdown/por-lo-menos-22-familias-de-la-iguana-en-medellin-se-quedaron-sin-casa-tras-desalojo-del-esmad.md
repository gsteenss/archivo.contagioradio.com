Title: Por lo menos 22 familias de “La Iguaná” en Medellín se quedaron sin casa tras desalojo del ESMAD
Date: 2017-03-29 17:47
Category: DDHH, Nacional
Tags: La Iguaná, Medellin
Slug: por-lo-menos-22-familias-de-la-iguana-en-medellin-se-quedaron-sin-casa-tras-desalojo-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/IMG_20170328_205227741.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CPDH] 

###### [29 Mar 2017] 

Por lo menos **22 familias, desplazadas por la guerra de sus tierras en el Chocó y Urabá que se alojaron hace varios años en el sector conocido como “la Iguaná” en Medellín**, se quedaron nuevamente sin casa, esta vez por orden de la alcaldía de esa ciudad, que argumentó que la presencia de los desplazados era un factor más que aumentaba los riesgos de la actual crisis ambiental que afronta el Valle de Aburrá.

Según Diego Orjuela, integrante del Comité Permanente para la defensa de los DDHH, si bien el asentamiento presentaba algunos de los riesgos señalados, el **procedimiento del desalojo fue desconociendo la condición de víctimas de esas personas**, no se hicieron presentes las organizaciones que podían colaborar, no se anunció el desalojo ni se les brindó ninguna posibilidad de re-ubicación.

Ante esa situación, algunos estudiantes de la Universidad de Antioquia, decidieron facilitar las instalaciones del coliseo, colindante con el predio, para que por lo menos las personas, entre las que hay bebes, niños y adultos mayores pudieran pasar la noche, **sin embargo en horas de la mañana el rector decidió dar una nueva orden de desalojo. **Le puede interesar: ["Familias afectadas por Hidroituango exigen la suspensión del proyecto"](https://archivo.contagioradio.com/familias-afectadas-hidroituango-exigen-la-suspension-del-proyecto/)

A pesar de la extrema condición de pobreza y abandono las familias no recibieron la atención humanitaria de emergencia, sin embargo, los mismos estudiantes de la Universidad de Antioquia decidieron brindar su apoyo y van a tener la posibilidad de pagar un arriendo en casas de la ciudad, mientras que **se establece un lugar para que puedan rehacer sus vidas con la dignidad a la que tienen derecho y con la atención debida** por parte del gobierno.

<iframe id="audio_17861854" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17861854_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
