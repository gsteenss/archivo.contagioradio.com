Title: Recuperar su territorio le está costando la vida a indígenas en Cauca
Date: 2018-12-21 12:33
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: ACIN, Cauca, Fuerza Pública, lideres sociales, ONIC, paramilitares
Slug: nos-estan-asesinando-llamado-de-comunidades-indigenas-del-cauca
Status: published

###### [Foto: Contagio Radio] 

###### [21 Dic 2018] 

Con el intento de asesinato a Germán Valencia, hermano del senador Feliciano Valencia, son 4 los hechos de violencia registrados en el Cauca durante el mes de diciembre. Acciones que para las autoridades indígenas hacen parte **de un plan de estructuras armadas para recuperar el control territorial del departamento**, sin que se tome alguna medida que garantice la seguridad de las comunidades por parte del Gobierno Nacional.

En palabras de Luis Acosta, coordinador de la Guardia Indígena, el intento de homicidio en contra de Valencia, es producto de la intención que han manifestado los distintos Cabildos por recuperar el control territorial y sacar a los armados de estos lugares. (Le puede interesar[: "No hay tregua para indígenas y líderes sociales en el Cauca"](https://archivo.contagioradio.com/no-tregua-indigenas-lideres-sociales-cauca/))

Tal decisión, fue tomada por las comunidades tras el asesinato de Edwin Dagua, autoridad indígena y gobernador del Cabildo de Huellas, ocurrido el pasado 7 de diciembre a manos de hombres armados; homicidio ocurrido a pesar de que en el mes de  julio, la ONIC solicitó al Estado su protección y la de otros líderes que recibieron panfletos amenazantes de las** Águilas Negras.** Además Dagua había sido amedrentado por diversos grupos, por sus mandatos en la comunidad y por cuestiones relacionadas con el control territorial.

A este hecho se suma el asesinato del líder indígena Gilberto Antonio Zuluaga, cometido el pasado 9 de diciembre en el corregimiento de los Andes, cuando un hombre le disparo en la cabeza. Zuluaga hacia parte de la Asociación de Trabajadores Campesinos de la** Zona de Reservas Campesinas de Corinto (ASTRAZONAC)**, además, era integrante de la Guardia Campesina y de la Coordinación Social y Política Marcha Patriótica.

### **Hay un plan para acabar con nosotros** 

Acosta, señaló que previo a los homicidios de ambos líderes, en el territorio aparecieron panfletos firmados por un grupo que se identifica como Águilas Negras, anunciando que acabarían con la vida de autoridades indígenas. Además, dos días antes del atentado contra Germán Valencia, **se conoció un panfleto que ofrece recompensas por asesinar a integrantes de la Guardia Indígena y Cabildos. **

El Coordinador de la Guardia Indígena, también denunció que pese a que en el Cauca hay una alta presencia de Fuerza Pública, ha sido la Guardia la encargada de proteger a las comunidades e incluso realizar requisas en las que han logrado incautar elementos robados y otros derivados del tráfico de estupefacientes. (Le puede interesar:["Paramilitares ofrecen recompensas por asesinar líderes indígenas"](https://archivo.contagioradio.com/panfletos-firmados-por-aguilas-negras-anuncian-recompensas-a-quienes-asesinen-a-lideres-indigenas/))

<iframe id="audio_30964426" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30964426_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
