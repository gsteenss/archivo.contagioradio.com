Title: En Santander recalcaron que "Santurbán no está en venta"
Date: 2017-11-16 13:45
Category: Ambiente, Nacional
Tags: delimitación páramo de Santurbán, empresa minesa, Minesa, Páramo de Santurbán
Slug: en-santander-recalcaron-que-santurban-no-esta-en-venta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DOr8Qg0XkAEgTOw-e1510857171884.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ADN Bucaramanga] 

###### [16 Nov 2017] 

Ante la firma del convenio entre Emiratos Arabes Unidos y Juan Manuel Santos y teniendo en cuenta las declaraciones del Ministro de Minas y Energía, Germán Arce, quien afirmó que el proyecto minero de Minesa no va a tener afectaciones para el páramo de Santurbán, las organizaciones en defensa del ecosistema recordaron que **existe un fallo de la Corte Constitucional** que ordenó una nueva delimitación por lo que el proyecto no se puede desarrollar.

Para protestar, los defensores del páramo llegaron a Bogotá y le hicieron saber al presidente de la República que "el páramo de Santurbán no está en venta". También en la capital, asistieron al Congreso de la República donde recordaron que Colombia posee el **50% de los páramos del mundo** y que deben ser defendidos.

### **Proyecto minero afectaría el total del ecosistema del páramo** 

De acuerdo con Mayerli López, integrante del Comité por la Defensa del Páramo de Santurbán, “la Corte Constitucional tumbó la delimitación que se hizo del páramo **por que no hubo participación de las comunidades**, por lo que en este momento no sabemos qué es páramo y qué no es”. (Le puede interesar:["Corte Constitucional da la razón a defensores del páramo de Santurbán"](https://archivo.contagioradio.com/corte-constitucional-da-la-razon-a-defensores-del-paramo-de-santurban/))

Además, para ella, la delimitación **se había hecho a favor de los proyectos mineros** y especialmente el de Minesa. Afirmó que, “no se puede hablar de coger la alta montaña y trazar una línea imaginaria para decir desde dónde hay páramo para explotar lo que quieran”. Dijo que es necesario tener en cuenta que el páramo, el sub páramo y el bosque alto andino son ecosistemas relacionados que dependen cada uno entre sí.

Por lo tanto, recordó que **“si se llega a afectar alguno de ellos, simplemente el ecosistema pierde su función principal** que es la de regular el agua”. Por esto, calificó como irresponsable las decisiones del Gobierno Nacional para ejecutar este tipo de proyectos que afectan el ecosistema de Santurbán de manera general.

### **ANLA busca incluir a Bucaramanga como zona de influencia del proyecto de Minesa** 

El Comité del páramo indicó que, tras haberse reunido con la Agencia Nacional de Licencias Ambientales, esta última manifestó que es necesario reconocer a **Bucaramanga como zona de influencia del proyecto de Minesa**. Por esto, “le va a solicitar información adicional a la empresa para incluir a Bucaramanga”.

Los defensores del páramo han manifestado en diferentes ocasiones que los habitantes de la capital de Santander **se ven directamente afectados por la ejecución del proyecto** minero. López fue enfática en que “ese proyecto se va a hacer arriba de las bocatomas del acueducto metropolitano de Bucaramanga”. (Le puede interesar: ["Masiva marcha en Santander exige que se proteja el Páramo de Santurbán"](https://archivo.contagioradio.com/movilizacion_santurban_proyecto_minero/))

Esto quiere decir que  "va a afectar el recurso hídrico **de más de 2 millones de personas** del nororiente colombiano”. Ante esto, exigirán que se realice una audiencia pública en Bucaramanga y le harán saber a la ANLA que es improcedente continuar con los procesos de licencias cuando ya existe un fallo que tumbó la delimitación existente y que exige un nuevo proceso.

### **Ante falta de delimitación del páramo, el proyecto no se puede desarrollar** 

López manifestó que hay **un plazo de un año** para que se desarrolle la nueva delimitación que esta vez deberá incluir las opiniones de las comunidades. Dijo que hasta que esto suceda, “es peligroso que la ANLA le dé la licencia ambiental a Minesa porque no hay una delimitación establecida”.

Finalmente, los defensores del páramo de Santurbán indicaron que la nueva delimitación **debe incluir las zonas donde se ubica el proyecto minero** en la medida que hace parte del ecosistema total. Además, se deberá tener en cuenta a las comunidades que están siendo afectadas. Por esto, han exigido que se debe negar la licencia de cualquier proyecto que busque hacer minería para proteger todo el páramo de Santurbán.

<iframe id="audio_22118249" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22118249_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
