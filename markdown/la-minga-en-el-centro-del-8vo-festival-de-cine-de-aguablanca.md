Title: La minga en el centro del 8vo Festival de Cine de Aguablanca
Date: 2016-07-05 17:42
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: @FESDAcine, Cine y video comunitario en Colombia, colectivo mejoda, Festival de cine Aguablanca
Slug: la-minga-en-el-centro-del-8vo-festival-de-cine-de-aguablanca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/FESDA-e1467753650388.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: FESDA 

###### [Jul 05 2016] 

La experiencia de "ser y hacer minga" será lo que  vivirán los participantes del **8vo Festival Nacional de Cine y video comunitario del Distrito de Aguablanca**, FESDA 2016, un espacio para la creación colectiva audiovisual surgida desde las comunidades que residen en barrios vulnerables de la ciudad de Cali.

Desde su nacimiento en 2008, el colectivo Mejoda como organizador del festival promueve la exhibición y la formación en el tema audiovisual comunitario, como una herramienta para contar los "sueños y realidades" de quienes han cargado históricamente con la estigmatización social de los lugares donde residen.

Para esta edición, desde el lema "Minga construyamos nuestras historias" se extiende la invitación tanto a caleños como visitantes a unirse a esta iniciativa que tiene como objetivo incitar a la movilización y a la organización de las comunidades para que construyan su propia verdad desde el territorio que habitan.

En su octava edición, FESDA promete ser una fiesta cultural y cinematográfica que reune proyecciones, talleres, laboratorios, conversatorios y actividades sociales y culturales de integración desde lo comunitario. La convocatoria para la selección oficial, estará abierta hasta el próximo 30 de agosto en el sitio www.fesdacine.co , sitio donde encontrarán las bases de participación.
