Title: Con telares, víctimas de violencia sexual exigieron procesos de reparación a la JEP
Date: 2019-05-22 16:15
Author: CtgAdm
Category: Judicial, Paz
Tags: Arrópame con tu esperanza, JEP, Mesa de Víctimas, víctimas de violencia sexual
Slug: con-telares-victimas-de-violencia-sexual-exigieron-procesos-de-reparacion-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Telares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

**La red 'Entretenzando la Confianza**',  que acoge a 200 municipios, más de 200 organizaciones y cerca de 600 personas, que han sido víctimas de violencia sexual en Colombia en el marco del conflicto armado, lanzó su campaña 'Arrópame con Tu Esperanza", que a partir del tejido de telares que plasman las historias vividas en la guerra, busca hacer más fuerte la voz de las víctimas y garantizar la no repetición de estos hechos.

Con las colchas, expresó **Yolanda Perea, integrante de la Mesa Nacional de Víctimas y lideresa de Riosucio, Chocó**, las víctimas pueden "desahogarse y plasmar todas esas ideas de reconicliación, de resiliencia, de dolor.  Buscamos abrazar a los colombianos en torno a la no repetición y esperamos que Colombia nos abrace en un tono reconciliador".

La lideresa explicó que esta campaña,  que se presentó durante una audiencia pública en el Congreso, tiene como fin que las personas que han tenido que padecer los impactos de la guerra en sus cuerpos, sean escuchadas y así, llevar sus historias hasta organismos como la JEP, para que sus derechos sean reestablecidos, pues los avances han sido pocos en términos de reparación.

"Desde el 2 de agosto, la Mesa de Víctimas entregó 2.000 casos de violencia sexual a la JEP, seguimos trabajando en los territorios, seguimos recogiendo información", enfatizó Perea, que a su vez le exigió al organismo tomar cartas en el asunto y abrir el caso 008 para víctimas de violencia sexual. [(Lea también: Informe revela que hay 15076 víctimas de violencia sexual en Colombia)](https://archivo.contagioradio.com/victimas-de-violencia-sexual-colombia/)

### Las historias tras los telares 

Yolanda afirmó que no existen garantías económicas al interior del colectivo, por lo que el trabajo se dificulta, sumado a la situación de que gran parte de las integrantes del grupo son madres cabeza de familia, "la mayoría tenemos hijos y es ahí donde entra el miedo cuando somos amenazadas, aquí lo que se busca no son carros, ni chalecos, sino prevenir toda esa violencia", expresó.

La lideresa explicó que son colchas construidas por hombres y mujeres a la vez, **"una sola colcha tiene muchas voces, muchas lágrimas, mucho amor y mucha memoria, eso es lo que buscamos que el país sepa".**

De igual forma, Perea hizo una invitación a la ciudadanía a participar en el plantón que se tiene pensado para el 25 de mayo, Día de la Dignidad de las Víctimas de Violencia Sexual, en cada uno de los 200 municipios en los que trabaja 'Entretenzando la Confianza'.

Finalmente en noviembre, la red espera reunir todas las voces de los territorios alrededor de un mismo telar,  **"esto es lo que conlleva ser víctima de esa violencia que deja marcas en tu cuerpo, en tu territorio, en tu conciencia, en tu familia y  es ahí donde trabajamos entorno a la no repetición y prevención de otras violencias"**.

<iframe id="audio_36248557" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36248557_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
