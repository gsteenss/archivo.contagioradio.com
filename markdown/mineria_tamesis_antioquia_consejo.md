Title: Concejo de Támesis, Antioquia impide actividades mineras
Date: 2017-05-30 16:51
Category: Ambiente, Voces de la Tierra
Tags: consultas populares, Mineria
Slug: mineria_tamesis_antioquia_consejo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-e1457467593828.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Macondo 

###### [30 May 2017] 

Con el objetivo de proteger el patrimonio natural y cultural, el municipio de Támesis se convierte en el primero de Antioquia en prohibir la minería. Así lo decidió el Concejo de ese municipio en su segundo debate por votación unánime.

La decisión se toma también en el marco de la definición del Plan de Ordenamiento Territorial en el cual se prohíbe la exploración y explotación de minería de metales. Asimismo, se acoge una sentencia de la Corte Constitucional que le da facultades a alcaldes y entidades territoriales para impedir el desarrollo de proyectos mineros.

Se trata de una medida basada en estudios sobre las condiciones geográficas, ambientales y culturales. En esa medida, se encontró que los títulos mineros que existen sobre 7.600 hectáreas en Támesis y el corregimiento Palocabildo, de Jericó, van en contravía de la vocación agrícola de estos lugares, donde ya se realizan actividades exploratorias mineras. [(Le puede interesar: Lista la consulta popular en Pijao)](https://archivo.contagioradio.com/lista-la-consulta-popular-de-mineria-en-pijao-quindio/)

Tanto el Concejo como la comunidad esperan que la medida signifique la prohibición de la actividad minera en esos territorios, de nos ser así, aseguran que empezarán la tarea de tramitar una consulta popular para que sean los habitantes de Támesis los que le digan 'no' a la entrada de empresas mineras. [(Le puede interesar: Cajamarca le dijo no a la minería)](https://archivo.contagioradio.com/si-se-pudo-pueblo-de-cajamarca/)

<iframe id="audio_19009006" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19009006_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
