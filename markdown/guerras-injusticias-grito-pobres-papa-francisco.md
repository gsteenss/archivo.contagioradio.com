Title: No hemos despertado ante guerras, injusticias, ni escuchado el grito de los pobres: Papa Francisco
Date: 2020-03-28 18:03
Author: CtgAdm
Category: Actualidad, El mundo
Tags: Papa Francisco
Slug: guerras-injusticias-grito-pobres-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/EUInOdFX0AAYDTZ.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Día-3.3-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/3.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/4.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/2.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Papa Francisco/ VenusMedia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante la oración que el papa Francisco presidió en la Basílica de San Pedro, el [Sumo pontífice](https://twitter.com/Pontifex_es) pidió por el bienestar de la población a nivel mundial y las personas que han sido afectadas por la pandemia del Covid-19, a su vez advirtió que la humanidad se ha dejado absorber por la rutina y el ego, sin embargo, resaltó que es una oportunidad para descubrir que todos pertenecemos al mismo planeta por lo que invitó a trabajar a todos de la mano.

<!-- /wp:paragraph -->

<!-- wp:image {"id":82575,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/3-1024x1024.png){.wp-image-82575}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

El papa hizo alusión a la pandemia a la que describió como "una tormenta inesperada y furiosa" que demostró cómo toda la humanidad comparte la misma barca, "todos frágiles y desorientados; pero, al mismo tiempo, importantes y necesarios, todos llamados a remar juntos, todos necesitados de confortarnos mutuamente. En esta barca, estamos todos". [(Lea también: Más de 300 organizaciones y personalidades piden al Pápa interceder por la paz de Colombia)](https://archivo.contagioradio.com/organizaciones-piden-papa-interceder-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

 Durante su reflexión Francisco resaltó la labor de las **"personas comunes —corrientemente olvidadas— que no aparecen en portadas de diarios y de revistas"** y que hoy están decidiendo el destino del mundo "médicos, enfermeros y enfermeras, encargados de reponer los productos en los supermercados, limpiadoras, cuidadoras, maestros, transportistas, fuerzas de seguridad, voluntarios, sacerdotes, religiosas y tantos pero tantos otros que comprendieron que nadie se salva solo". [(Le puede interesar: Organizaciones sociales piden al Papa que intervenga en negociación con ELN)](https://archivo.contagioradio.com/papa-negociacion-eln/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":82577,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/4-1024x1024.png){.wp-image-82577}

</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### La tempestad desenmascara nuestra vulnerabilidad: Francisco

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Advirtió que la humanidad se ha dejado absorber por la material y la prisa, "no nos hemos despertado ante guerras e injusticias del mundo, no hemos escuchado el grito de los pobres y de nuestro planeta gravemente enfermo".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente señaló que en medio del aislamiento **"donde estamos sufriendo la falta de los afectos y de los encuentros, experimentando la carencia de tantas cosas"** es tiempo para motivar espacios que generen hospitalidad, fraternidad y solidaridad.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
