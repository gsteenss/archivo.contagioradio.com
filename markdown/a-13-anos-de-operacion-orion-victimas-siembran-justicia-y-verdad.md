Title: A 13 años de Operación Orión, víctimas siembran justicia y verdad
Date: 2015-10-16 14:41
Category: DDHH, Entrevistas
Tags: alvaro uribe velez, Comuna 13, crímenes de estado, das, General Mario Montoya, IV Brigada del Ejército, La escombrera, Medellin, Mujeres Caminando por la Verdad, operación orion, paramilitarismo en Colombia, Sala de Justicia y Paz del Tribunal de Medellín, semana de la memoria, Terrígenos
Slug: a-13-anos-de-operacion-orion-victimas-siembran-justicia-y-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/12166013_10153513260955020_1595001438_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Corporación Jurídica Libertad] 

<iframe src="http://www.ivoox.com/player_ek_9026543_2_1.html?data=mpWfmJqYd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhYyllJDOh6iXaaOl0NiYxsqPk9HZ08bQy4qnd4a2lNOYsdfNaaSnhqeg0IqWh4zqhqigh6aop9XdzsbgjdjNqc7W08bbjc%2FZt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luz Helena Galeano, Mujeres Caminando por la Verdad] 

<iframe src="http://www.ivoox.com/player_ek_9026583_2_1.html?data=mpWfmJqcd46ZmKiak5aJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmoa3lIqupsjYrc7V1JDgy8rRptPVz5DX19jYrcTdwpDmjdvJtsXVxZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Wilmar Botina, Agroarte] 

###### [16 Oct 2015 ]

[Hoy se conmemoran 13 años de la **Operación Orión** en la Comuna 13 de Medellín, diversas organizaciones la califican como la operación de mayor envergadura a nivel urbano ejecutada por tropas de la IV Brigada del Ejército bajo el mando del General Mario Montoya, en la presidencia de Álvaro Uribe Vélez. Esta dijó como resultado cerca de **650 violaciones a los derechos humanos**, **92 desapariciones forzadas** y más de 200 personas detenidas, de las que sólo 20 llegaron a juicio y 3 cuentan con condena.]

[Wilmar Botina, integrante de Agroarte y participante de la apuesta artística colectiva ‘Cuerpos gramaticales’, asegura que esta acción simbólica involucra más de **300 personas que se siembran** de la cintura hacia abajo para rememorar "aquello que nos paso hace 13 años, así como las violencias que nos han atacado, recuperando la memoria, protestando y resistiendo… **somos plantas que nacemos en un territorio y nos resistimos a ser cortadas, a ser olvidadas**".  ]

["Las raíces nos permiten construir", indica Botina y estos actos de ‘Catarsis colectiva’ tejen redes para la reclamación de derechos, "Ésta es **una siembra por todo lo que nos ha acontecido en el país, para que no vuelva a suceder**… es una resistencia desde nuestros territorios, desde nuestros cuerpos". ]

[En relación con la decisión de la Sala de Justicia y Paz del Tribunal de Medellín de compulsar copias al ex presidente y ahora senador Uribe, Botina afirma que las víctimas están a la expectativa, esperan que se haga justicia.]

[Luz Helena Galeano, vocera de la organización Mujeres Caminando por la Verdad, comenta que esta conmemoración cuenta con actos de memoria en homenaje a las víctimas, foros sobre la justicia en el marco de la construcción de paz y la presentación del **primer informe de la Fiscalía** sobre los avances de las **excavaciones en La Escombrera**.]

[Galeano denuncia aun existe una montaña conocida como **Terrígenos** que pese a las exigencias de la comunidad para que sea cerrada, se continúan arrojando desechos sobre cerca de 300 cuerpos enterrados, **"allí más de 300 volquetas a diario sepultan la verdad"**. De acuerdo con ella, en Terrígenos persisten intereses políticos y económicos asociados a estructuras paramilitares.]

["Nos han tildado de guerrilleras por nuestras exigencias" afirma Galeano, pese a ello continúan reclamando justicia y haciendo veeduría continúa al proceso de excavación en La Escombrera, en la que no se han hallado hasta ahora cuerpos. Además de la Fiscalía hay un equipo forense independiente que hace las veces de fuente de verificación, según explica Luz Helana.]
