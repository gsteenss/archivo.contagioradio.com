Title: Gobernación de Antioquia atenderá a víctimas de Hidroituango
Date: 2016-05-12 14:51
Category: Ambiente, Nacional
Tags: Ambiente, Antioquia, Hidroituango, Movimiento Ríos Vivos
Slug: gobernacion-de-antioquia-atendera-a-victimas-de-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Vícrtimas-de-Hidroituango-e1463081947920.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Debate Hidroituango 

###### [12 May 2016]

La Gobernación de Antioquia firmó un decreto para atender los conflictos que se han desarrollado alrededor del proyecto hidroeléctrico Hidroituango que lleva a cabo Empresas Públicas de Medellín, de acuerdo con la solicitud del Movimiento Ríos Vivos para que sean atendidas las **más de 20 mil víctimas de la construcción de la hidroeléctrica más grande Colombia.**

Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, cuenta que hace más de 5 años se venía solicitando la creación de “un espacio serio” para las solución de los conflictos socio-ambientales generados por los megaproyectos minero energéticos en Antioquia, y específicamente sobre los que se han desarrollado por cuenta de Hidroituango. Pero solo tras la fuerte presión de la movilización realizada el pasado 14 de marzo en defensa del agua y la vida, se logró que el gobernador de Antioquia “por fin aceptara que se creara una mesa de diálogo y solución de esos conflictos”.

En anteriores ocasiones cuando se la población podía reunirse con,**la gobernación y EPM, estos se levantaban de la mesa de manera arbitraria,** por eso fue necesaria la exigencia de un decreto departamental que asegurara que las partes estén presentes para dar respuestas.

Es así, como **el próximo 14 de mayo las víctimas por este megaproyecto de todos los municipios podrán dejar sus denuncias en las alcaldías,** para que luego sea elegido un representante por cada municipio afectado por estos megaproyectos, para que haga parte de la Mesa de Diálogo para el Análisis y Búsqueda de Soluciones, donde también habrá delegados de la academia, del Movimiento Ríos Vivos, EPM y la gobernación.

Mientras las comunidades esperan que se active ese espacio de discusión, presentarán una denuncia ante las autoridades ambientales, teniendo en cuenta que pese a las la[resolución 027 del 15 de enero de la Agencia Nacional de Licencias Ambientales](https://archivo.contagioradio.com/anla-suspende-actividades-de-hidroituango-por-danos-socio-ambientales/), en la que se suspende las obras de la hidroeléctrica, los pobladores han visto que en la noche [la construcción continúa con total normalidad,](https://archivo.contagioradio.com/epm-incumple-orden-de-la-anla-sobre-suspension-en-obras-de-hidroituango/) engañado a las comunidades y cometiendo un delito ambiental, pues no se han cumplido las medidas de la resolución, como lo explica Zuleta.

Por el momento los habitantes impulsarán el proceso de creación de la mesa para que desde las autoridades gubernamentales se reparen los derechos ciudadanos y ambientales.

<iframe src="http://co.ivoox.com/es/player_ej_11508662_2_1.html?data=kpaikp2aepOhhpywj5WbaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncarnwsfSzpCncozO1tHS1saPcYzGhqigh6aos9Sft87j0diRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
