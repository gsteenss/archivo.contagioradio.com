Title: Equipo Jurídico Pueblos denuncia montaje judicial
Date: 2018-11-22 16:34
Author: AdminContagio
Category: DDHH, Nacional
Tags: Equipo Jurídico Pueblos, Gloria silva, montajes judiciales
Slug: equipo-juridico-pueblos-denuncia-montaje-judicial
Status: published

###### [Foto: Contagio Radio] 

###### [22 Nov 2018] 

El Equipo Jurídico Pueblos denunció que se estaría realizando un **montaje judicial en contra de algunos de sus abogados y defensores de derechos humanos, ordenado por fiscales especializados**. Gloria Silva, integrante de esta organización, afirma que la información la hicieron llegar fuentes de la misma entidad, indicando que hay un "plan" para relacionarlos con estructuras armadas.

En una de las comunicaciones que llegaron a la organización se les alertaba de la siguiente forma “tengan cuidado porque no hace falta sino que aparezca información contra ustedes en memorias USB incautadas en operativos contra comandantes guerrilleros”, hecho que según el Equipo Jurídico, serviría para relacionarlos con estructuras como el ELN.

Desde la organización aseguran que la fuente les indicó que **“los están buscando para advertirles que le bajen a las denuncias”** y que “deben tener especial cuidado con dos fiscales” quienes estarían detrás de eso montajes judiciales.

Información similar se filtró a través de otra fuente de la misma entidad, que manifestó que se está pretendiendo vincular a la abogada Silva con la insurgencia del Eln, acción que coincide con las múltiples denuncias que ha hecho la defensora de derechos humanos **sobre seguimientos ilegales en su contra**, así como la interceptación de sus comunicaciones y el reciente robo de su computador portátil.

**Implantación de pruebas al Equipo Jurídico Pueblos**

La organización aseguró en un comunicado de prensa, que al consultar a un especialista sobre la posibilidad de implantar archivos en sus dispositivos, les indicó  que **"es viable y que difícilmente pueden ser detectadas tales alteraciones",** hecho que incrementa la incertidumbre ante las aseveraciones realizadas por las fuente de alta confiabilidad.

De igual forma, señalaron que no se descarta el uso de supuestos desertores, "cuyas versiones suelen usarse acomodando las acusaciones a cambio de estímulos económicos, jurídicos, e incluso bajo presión". (Le puede interesar: ["Existe presión y vulneración a la autonomía judicial con el caso Andino"](https://archivo.contagioradio.com/presion-vulneracion-judicial-caso-andino/))

<iframe id="audio_30270195" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30270195_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
