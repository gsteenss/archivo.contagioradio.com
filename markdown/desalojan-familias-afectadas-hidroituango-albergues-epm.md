Title: Desalojan familias afectadas por Hidroituango de albergues de EPM
Date: 2019-03-01 15:08
Category: Ambiente, Comunidad, Nacional
Tags: Antioquia, Familias desplazadas por Hidroituango, Fernando Carillo, Hidroituango, Movimientos Ríos Vivos, Procuraduría General de la Nación
Slug: desalojan-familias-afectadas-hidroituango-albergues-epm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DdstP13V4AM4wWu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos - Antioquia] 

###### [28 Feb 2019] 

El Movimiento Ríos Vivos - Antiquia denunció que el pasado 27 de febrero, entre 25 y 30 familias fueron desalojadas por la Policía de las oficinas de Empresas Públicas de Medellín (EPM) en Sabanalarga, Antioquia. Las personas se encontraban desde hace 10 meses albergadas en este lugar luego de ser desplazadas por las inundaciones producidas por Hidroituango el año pasado.

Ángela Agudelo, integrante de Movimientos Ríos Vivos - Antioquia, es unas de las afectadas por este hecho. De acuerdo con ella, la empresa habría aprovechado que la mayoría de las personas, que estaban viviendo en las oficinas, se encontraban en una audiencia pública, convocada por la Procuraduría y Contraloría en Medellín, para hacer el desalojo. De hecho, Agudelo sostiene que **la decisión de llevar acabo esta acción se pudo dar como consecuencia de las declaraciones que dieron esta comunidad en contra de EPM** durante la audiencia.

Estas familias habían regresado al albergue después de trabajar en fincas recolectando café durante la temporada de cosecha. Agudelo sostiene que las condiciones de la vivienda no eran aptas para las personas, pues dormían en el piso por la falta de colchones y no contaban con ayudas de EPM. Sin embargo, Agudelo afirma que lo que les espera será más difícil, dado que las familias **tendrán que buscar sustento mientras que viven en la calle, con niños y ancianos.**

"Estamos desprotegidos, hemos hablado con EPM, hemos hablado con el Alcalde del municipio," dijo Agudelo. "No hay solución para nosotros." (Le puede interesar: "[Alcaldía ordena desalojo de 22 familias víctimas de Hidroituango](https://archivo.contagioradio.com/desalojo-victimas-hidroituango/)")

Actualmente, la comunidad afectada no puede entrar a las oficinas ni sentarse al frente de ellas. Algunos de ellos se han plantado en la acera del albergue. No obstante, la policía les piden que se retiren porque, según ellos no pueden estar ahí. Además, Movimiento Ríos Vivos sostiene que **una camioneta desconocida se llevó las pertenencias de las comunidades**.

Frente estos hechos, el movimiento ambiental le exige a la Policía, EPM y a la administración de Sabanalarga que **les garantice una vivienda** para estas familias "ante la angustia permanente de no tener fuentes de vida y subsistencia, de no tener en dónde habitar."

### **La audiencia con la Procuraduría** 

En la audiencia de "[vigilancia y seguimiento preventivo" al megaproyecto Hidroituango, el Procurador Fernando Carillo anunció que presentará ante un juez una acción popular, es decir, una **acción jurídica que protege los derechos de una comunidad antes de que sean vulnerados**. Ante esta declaración, Agudelo manifestó que "ojala se diera porque la verdad la estamos necesitando."]

Aún así, la víctima de Hidroituango manifiesta tener poca credibilidad en esas ayudas debido a que "ya nos han alegrado mucho con palabras, pero nunca se han visto los hechos,". Pese a ello, espera que con las investigaciones que adelanta la Procuraduría se pueda esclarecer la verdad detrás de la construcción del proyecto y que los funcionarios respondan por las afectaciones de Hidroituango.

La Procuraduría está investigando[ al alcalde Federico Gutiérrez, al gobernador de Antioquia Luis Pérez, el ex-gobernador Sergio Fajardo y el ex-alcalde Aníbal Gaviria, por presuntas irregularidades en el control y vigilancia al proyecto. Además, se abrió indagaciones preliminares en contra de la Gobernación de Antioquia, el Instituto para el Desarrollo de Antioquia, EPM, la Sociedad Hidroeléctrica de Ituango y la Autoridad Nacional de Licencias Ambientales.]

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
