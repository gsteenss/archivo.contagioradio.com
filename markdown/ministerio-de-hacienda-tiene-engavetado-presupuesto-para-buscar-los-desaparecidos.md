Title: Min. Hacienda tiene “engavetado” presupuesto para buscar los desaparecidos
Date: 2018-08-30 09:22
Category: DDHH, Nacional
Tags: desparecidos, Luz Marina Monzon, Ministerio de hacienda
Slug: ministerio-de-hacienda-tiene-engavetado-presupuesto-para-buscar-los-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/e-e1472582019512.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### [30 Ago 2018]

Aunque antes de terminar su mandato Juan Manuel Santos firmó el decreto que le daba vía libre a la Unidad de Búsqueda de personas dadas por Desaparecidas, UBPD, es necesario que el Ministerio de Hacienda firme una resolución para que se destine el presupuesto, inicialmente, para el 30% del total que requiere el organismo.

Según el presupuesto que se definió en el diseño institucional para la puesta en marcha de la Unidad, es necesario que este año se destinen por lo menos 7 mil millones de pesos, que se utilizarían para crear 156 cargos, instalar las oficinas regionales, recopilar la información existente y definir una ruta a la que puedan acceder las víctimas.

La directora de la Unidad, Luz Marina Monzón, afirmó que para arrancar en firme con el trabajo es necesario contar por lo menos con esa parte del presupuesto y por ello el Ministerio de Hacienda debe firmar la resolución para que el primer dinero se consigne a la Unidad y así poder contratar parte de la planta de personal que hace falta, para una Institución que se plantea descentralizada y con énfasis en las regiones.

Adicionalmente, Monzón señaló que el mandato de la organización es compatible con la reciente solicitud del Movimiento de Víctimas de Crímenes de Estado en que se pide a la Jurisdicción de Paz dictar medidas cautelares a favor de 16 territorios en los que podrían hallarse cuerpos de personas desaparecidas para facilitar su búsqueda, como es el caso de la llamada "Escombrera" en Medellín en donde se prevé hay cerca de 150 cuerpos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
