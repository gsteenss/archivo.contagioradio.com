Title: En condiciones precarias directivas pretenden reanudar semestre en la Universidad del Tolima
Date: 2016-03-15 13:58
Category: Educación, Nacional
Tags: aspu tolima, jorge gantiva, u del tolima, universidad del tolima
Slug: en-condiciones-precarias-directivas-pretenden-reanudar-semestre-en-la-universidad-del-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/U-Tolima.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASPU Tolima ] 

<iframe src="http://co.ivoox.com/es/player_ek_10814498_2_1.html?data=kpWlk5mYfZmhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncabijMjc0MnNp8rjz8rgjdXWqcTV087O1ZDIrdPZxNnW2MbXb9HmxtnS0MnJsozmxsbb18nFtoyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jorge Gantiva, ASPU Tolima] 

###### [15 Mar 2016]

Tras un [[largo periodo de parálisis](https://archivo.contagioradio.com/profesores-y-estudiantes-exigen-renuncia-del-rector-de-la-universidad-del-tolima/)] producto de la [[crisis estructural](https://archivo.contagioradio.com/la-burocracia-le-cuesta-mas-de-10-mil-millones-anuales-a-la-universidad-del-tolima/)] que vive la Universidad del Tolima, el Consejo Superior de la institución decretó el inicio del semestre académico en "condiciones precarias y muy limitadas", según afirma Jorge Gantiva, integrante de la Asociación Sindical de Profesores Universitarios ASPU, debido a que **aún no han sido contratados los maestros catedráticos** que conforman el 85% de la planta docente, no se han pagado los salarios ni las primas correspondientes al mes de diciembre y **se determinó un recorte presupuestal de \$20 mil millones**.

De acuerdo con Gantiva esta apertura del semestre académico se da en condiciones poco adecuadas que afectan "la calidad de la docencia y la dignidad de los trabajadores" y que son consecuencia de la "administración mediocre, comprometida con la corrupción y el clientelismo" que **en un mes puede llevar a la universidad al colapso**. Razón por la que la planta docente decidió continuar en asamblea permanente exigiendo una reforma integral de la institución.

Como asevera Gantiva los mayores afectados con la "incapacidad política" del actual rector José Hermán Muñoz son los estudiantes, quienes el pasado lunes decidieron manifestarse contra el **recorte del 70% de los recursos para el Centro Cultural Universitario**, varios de ellos se desnudaron en las instalaciones universitarias para llamar la atención frente a las **enormes limitaciones a las que se enfrenta este escenario** que ha contribuido ampliamente con propuestas teatrales, musicales y literarias en el devenir académico de la institución.

La asamblea permanente en la que los profesores decidieron continuar busca presionar el inicio de **investigaciones formales por parte de la Procuraduría y la Fiscalía sobre las denuncias de corrupción contra la actual administración**, y que se espera lleven a la salida del cargo del rector Muñoz y a la restructuración de la Universidad "sobre la base de la participación, la democracia y el respeto de los derechos", como asegura Gantiva.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/)]
