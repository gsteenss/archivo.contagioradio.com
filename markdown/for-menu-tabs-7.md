Title: For Menu Tabs #7 - Economia
Date: 2019-01-18 04:25
Author: AdminContagio
Slug: for-menu-tabs-7
Status: published

[](https://archivo.contagioradio.com/menos-impuestos-para-las-empresas-menos-salario-para-los-trabajadores/)  

###### [Menos impuestos para las empresas, menos salario para los trabajadores](https://archivo.contagioradio.com/menos-impuestos-para-las-empresas-menos-salario-para-los-trabajadores/)

[<time datetime="2018-12-21T17:47:19+00:00" title="2018-12-21T17:47:19+00:00">diciembre 21, 2018</time>](https://archivo.contagioradio.com/2018/12/21/)  
[](https://archivo.contagioradio.com/aumento-salario-minimo/)  

###### [Trabajadores buscan que aumento de salario mínimo sea de doble dígito](https://archivo.contagioradio.com/aumento-salario-minimo/)

[<time datetime="2018-12-11T18:25:50+00:00" title="2018-12-11T18:25:50+00:00">diciembre 11, 2018</time>](https://archivo.contagioradio.com/2018/12/11/)  
[](https://archivo.contagioradio.com/hueco-fiscal-hacienda/)  

###### [Hueco fiscal se lo invento el ministro de Hacienda: Eduardo Sarmiento](https://archivo.contagioradio.com/hueco-fiscal-hacienda/)

[<time datetime="2018-11-27T15:13:09+00:00" title="2018-11-27T15:13:09+00:00">noviembre 27, 2018</time>](https://archivo.contagioradio.com/2018/11/27/)  
[](https://archivo.contagioradio.com/aumento-iva/)  

###### [Aumento del IVA: Única propuesta del Gobierno que puso de acuerdo al Congreso](https://archivo.contagioradio.com/aumento-iva/)

[<time datetime="2018-11-21T13:38:00+00:00" title="2018-11-21T13:38:00+00:00">noviembre 21, 2018</time>](https://archivo.contagioradio.com/2018/11/21/)
