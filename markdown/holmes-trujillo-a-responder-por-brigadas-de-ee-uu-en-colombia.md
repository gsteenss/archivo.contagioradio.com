Title: Holmes Trujillo a responder por Brigadas de EE.UU. en Colombia
Date: 2020-10-15 12:30
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Brigadas de Estados Unidos, Carlos Holmes Trujillo, Procuraduría
Slug: holmes-trujillo-a-responder-por-brigadas-de-ee-uu-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Holmes-Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Twitter Carlos Holmes Trujillo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El ministro de Defensa, Carlos Holmes Trujillo fue notificado el pasado 13 de octubre fue notificado por la Procuraduría General para que en un plazo de cinco días entregue toda la información con relación a las acciones y presencia que ha realizado la **Brigada de Asistencia Fuerza de Seguridad de los Estados Unidos (SFAB, por sus siglas en inglés)** en el territorio colombiano y la situación de seguridad que vive el país”. Los senadores Iván Cepeda y Antonio Sanguino ya habían enviado un derecho de petición con diferentes inquietudes al respecto sin embargo, no han recibido respuesta desde la cartera de Defensa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde su llegada en junio de este año, defensores de DD.HH alertaron en primer lugar que según el artículo 173 de la Constitución es requisito que cualquier presencia de tropas extranjeras en territorio colombiano sea debatida y autorizada de forma previa por el Senado. [(Lea también: Congreso no ha autorizado tropas estadounidenses y pide rectificar a Mindefensa)](https://archivo.contagioradio.com/congreso-no-ha-autorizado-tropas-estadounidenses-y-pide-rectificar-a-mindefensa/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Congresistas además han alertado que tras examinar el manual de operaciones de dichas Fuerzas Especiales, se puede concluir que dentro de las capacidad que tendría está unidad se permitiría la asociación y asesoría a grupos ilegales en el país. ***«menciona en forma explícita la función de asesorar milicias no gubernamentales y socios irregulares»***, lo que según senadores como Iván Cepeda, se podría interpretar como **dar permiso a estas fuerzas internacionales para que se asocien con grupos ilegales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, según la información brindada por la embajada de Estados Unidos en Bogotá, la Brigada de Asistencia de Fuerza de Seguridad es "una unidad especializada del Ejército formada para asesorar y ayudar en operaciones en naciones aliadas".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Holmes Trujillo, un funcionario que ha apelado al desacato

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según Holmes Trujillo, el Gobierno recibió una carta con el visto bueno de 69 congresistas, que equivaldría a más de la mayoría absoluta del Senado, para que se reanudaran las tareas de la Brigada. Al respecto, el expresidente del Senado Lidio García, pidió rectificar la información difundida por este, en la que aseguraba que el Congreso había autorizado la presencia de tropas estadounidenses en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Horas después de que García hubiese solicitado la rectificación oficial; el ministro Carlos Holmes Trujillo respondió que no se rectificaría. Además expresó que las tropas de EE.UU. están activas desde el pasado 20 de julio, violando una orden judicial del Tribunal Administrativo de Cundinamarca que suspendía las actividades de los 53 soldados que componen esta brigada. [(Le puede interesar: Episodio de las Brigadas militares estadounidenses es ejemplo de la política exterior colombiana)](https://archivo.contagioradio.com/episodio-de-las-brigadas-militares-estadounidenses-es-ejemplo-de-la-politica-exterior-colombiana/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido, la Procuraduría Auxiliar para Asuntos Constitucionales, agregó en su declaración que el ministro debe entregar la información en el tiempo oportuno y de forma que resuelvas las inquietudes de los senadores, **de lo contrario al no contestar las peticiones en "los términos legalmente establecidos", constituiría una falta disciplinaria.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esto a propósito de que el ministro se había negado a acatar el fallo de la Corte Suprema de Justicia de ofrecer disculpas públicas al país por los abusos y la violencia ejercidos por la Fuerza Pública en el marco de las manifestaciones sociales del 9 y 10 de septiembre argumentando que ya lo había hecho, lo que lo llevó a disculparse durante el debate de control político en el Congreso el pasado 7 de octubre. [(Le puede interesar: Desacato del Gobierno a la Corte podría acarrear arresto)](https://archivo.contagioradio.com/desacato-del-gobierno-a-la-corte-podria-acarrear-arresto/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1316071447291473920","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1316071447291473920

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

“Para que el ministro Holmes Trujillo cumpla con sus deberes lo único que funciona es recurrir a estrados judiciales o en este caso a la Procuraduría", expresó el senador Iván Cepeda, agregando que pareciera que el ministro no quiere entregar dicha información; "es increíble que haya que proceder de esta manera para que los funcionarios cumplan con sus más elementales deberes".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Defensores de DD.HH. como Alirio Uribe Muñoz han advertido que "**esta actitud del Gobierno es una práctica recurrente en el sentido de cuestionar y desacatar las órdenes judiciales**". [](https://archivo.contagioradio.com/operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso/)[(Lea también: "Brigadas de EE.UU. en Colombia tiene potestad para asesorar grupos irregulares"](https://archivo.contagioradio.com/brigadas-de-ee-uu-que-llegaron-a-colombia-tienen-potestad-para-asesorar-grupos-irregulares/))</a>

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
