Title: Más de 70 familias desplazadas por combates y sin atención estatal en Juradó, Chocó
Date: 2019-09-12 13:36
Author: CtgAdm
Category: Comunidad, Nacional
Tags: AGC, Chocó, ELN, Juradó
Slug: mas-de-70-familias-afectadas-por-combates-entre-grupos-ilegales-en-jurado-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Juradó-e1568313352480.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Regalías Gov.] 

Desde el pasado 9 de septiembre los habitantes de Juradó, Chocó, han sido víctimas de acciones armadas a cargo de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y el Ejército de Liberación Nacional (ELN), que los han confinado, retenido y dejado en medio de combates. Mientras tanto, denuncian que la fuerza pública que opera en el territorio no ha hecho lo necesario para proteger sus vidas, y asegurar que este tipo de acciones no se repitan.

### **Las denuncias de la comunidad**

Edison Mena, Concejal de Juradó, denunció que el pasado 9 de septiembre en la zona de Punta Ardita, las AGC retuvieron una comunidad; posteriormente, el 1 de septiembre cerca del medio día se presentó un enfrentamiento con fusiles y granadas entre miembros de este grupo y el ELN, en el barrio conocido como las Maderas, en Juradó. Ante esta situación, la comunidad informó que solo auxiliares de la policía hicieron presencia, es decir, integrantes de la Fuerza Pública que no pueden repeler este tipo de ataques.

Según Mena, la gente tiene miedo porque están "a la merced de dios", entre unos grupos armados que entran al territorio "como Pedro por su casa", y una fuerza pública que no actúa con la misma contundencia que lo hacía cuando allí operaba las FARC. En ese sentido, sentenció que era necesario que la base militar establecida en Juradó trabaje por generar condiciones de confianza y seguridad para la población. (Le puede interesar: ["¿Está intimidando la Fuerza Pública a jóvenes indígenas en Chocó?"](https://archivo.contagioradio.com/esta-intimidando-la-fuerza-publica-a-jovenes-indigenas-en-jurado-choco/))

### **Confinamientos, desplazamientos y miedo en Juradó**

Luego de los combates se han presentado situaciones de desplazamiento desde Las Maderas, porque como dice Mena, si de día combaten como lo hicieron el pasado martes, "cómo será en las horas de la noche". Por este flagelo se han visto afectadas unas 70 familias, que se suman al confinamiento sufrido por diferentes comunidades indígenas, producto de la siembra de minas. Una práctica que utilizan los grupos armados para controlar el territorio. (Le puede interesar: ["Paramilitares de las AGC sitian a 800 habitantes de J. Chocó"](https://archivo.contagioradio.com/paramilitares-de-las-agc-sitian-a-800-habitantes-de-jurado-choco/))

**Síguenos en Facebook:**  
<iframe id="audio_41568879" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_41568879_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
