Title: Activistas del BDS interrumpen participación de empresa israelí en Paris
Date: 2017-06-29 12:43
Category: Onda Palestina
Tags: BDS, francia, Israel, Palestina
Slug: bds-francia-palestina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:BDS France 

###### 28 Jun 2017 

El Pasado 24 de junio activistas que apoyan el BDS en Francia hicieron una acción no violenta **interrumpiendo la tranquilidad del stand de la empresa israelí Elbit Systems**, en el marco del Salón de la Aeronáutica y del Espacio que se llevaba a cabo **en Paris**.

En la acción participaron personas pertenecientes a la **organización antimilitarista ATTAC**, al sindicato Solidario y al grupo Unión de Judíos Franceses por la Paz. De forma organizada se extendieron en el piso portando cada una camisetas alusivas a la campaña de Boicot, Desinversiones y Sanciones (BDS), extendieron pancartas y procedieron a leer un comunicado en el que denunciaban a la empresa. Tras la llegada de la seguridad del evento, fueron retirados de la feria.

La acción se centró en denunciar que **Elbyt Systens es la empresa israelí que fabrica el 85% de los drones que se utilizan para vigilar, y bombardear en algunos casos, Gaza y Cisjordania**. Además, esta empresa también facilita tecnología para la vigilancia del muro ilegal que encierra a Cisjordanía. Es decir, es una de las empresas responsables del sufrimiento de los y las palestinas.

Este llamado es una de las líneas más importantes que desde Palestina se ha solicitado a los ciudadanos del mundo, para que **presionen a sus gobiernos a parar la compra y venta de armas**, es decir todo comercio militar con Israel; el embargo militar a esta nación es una prioridad en este momento debido a que la industria de la guerra que garantiza la violación de los derechos de los y las palestinas.

Esta y otras noticias del movimiento BDS, acompañadas de información sobre la situación en Palestina, cultura y música pueden escucharlas en esta emisión de [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/).

<iframe id="audio_19543459" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19543459_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
