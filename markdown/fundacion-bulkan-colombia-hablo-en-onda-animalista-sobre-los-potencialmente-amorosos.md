Title: Fundación Bulkan Colombia habló en Onda Animalista sobre los "potencialmente amorosos"
Date: 2016-01-22 17:10
Category: Voces de la Tierra
Slug: fundacion-bulkan-colombia-hablo-en-onda-animalista-sobre-los-potencialmente-amorosos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/gabriel-galindo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

<iframe src="http://www.ivoox.com/player_ek_10169137_2_1.html?data=kpWemJ6Vd5ihhpywj5maaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncafpz8nOxc6Jh5SZo5jbjafZsMzVz5Cw0dHTscPdwpDVw8fQaaSnhqegjcrSb7DixcaYo9PNscLgytjhw5DXs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [22 Ene 2016] 

En Onda Animalista, contamos con la presencia de Giovanna Caballero y Silvana Benitez, de la Fundación BKC (Bulkan Colombia), quienes desde hace 8 años trabajan promoviendo la tenencia responsable de animales de compañía, específicamente con las razas denominadas como "potencialmente peligrosas", como se consagra enSer pioneros en Educación y tenencia responsable de los caninos, especialmente los descritos en la ley 746 de 2002.

**"Potencialmente amorosos"**, es la forma como la fundación prefiere decirles, ya que estos animales pueden ser igual de amorosos que cualquier otro perro, que haya sido bien educado por su familia.

Esta fundación realiza cada año una marcha que busca generar una nueva percepción de estas razas, que, juntos a sus tenedores, son víctimas de  discriminación y maltrato por la estigmatización a la que son sometidos en la sociedad.

La Fund[acion BKC busca la sensibilización de la problemática actual, en cuanto a Tenencia y Reproducción Responsable, en busca del reconocimiento y de acciones que vinculen de manera activa y dinámica al canino a la sociedad, según su funcionalidad, para así brindarle espacios sanos de recreación y esparcimiento.]{.text_exposed_show}

Si deseas mayor información o estas interesado en colaborar con esta causa, puedes comunicarte a los teléfonos 3184215055 - 3123519663 o en el facebook, [Fundación BKC](https://www.facebook.com/OfficialFundacionBKC/timeline).
