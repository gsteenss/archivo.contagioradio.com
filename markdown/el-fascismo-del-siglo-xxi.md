Title: El fascismo del siglo XXI
Date: 2019-05-14 10:03
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Alvaro Uribe, ex uribista, expresidente, Humberto Eco
Slug: el-fascismo-del-siglo-xxi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/34146277542_20101131f2_b.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-1-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Neil Palmer 

[No pocos años permanecerá trinando un expresidente sin darse cuenta el daño que causa, y el impulso tan serio que aporta a la violencia.]

[17 años de uribismo, con sus traidores y desentendidos, con su nobel de paz ex uribista, ex ministro de guerra del expresidente de la guerra, que nunca pagaría un día de cárcel porque entonces ¿a quién invitaría la reina?]

[Nunca es tarde para entender que dejarse traspasar por la realidad, es la esencia de Nuestra América ¿por qué nos niegan? ¿por qué lo más fácil para algunos políticos y candidatos de todos los colores, es desautorizar la realidad de los pueblos que viven más allá de ese recalcitrante institucionalismo o la prefiguración de los partidos?]

[A sangre y fuego se han ganado su país… ¿y el nuestro? ¿es en serio? ¿tendremos que pelearlo con sangre y pasión? No es justo, porque al final la sangre siempre será sangre. No somos hermanos, pero somos humanos, y preparando nuestro argumento, ya habían cerrado las ventanillas para radicarles el hecho de que estábamos llenos de alegría.]

[El fascismo del siglo XXI tiene lencería discursiva y negra, fúnebre, pero lleva un traje con lentejuelas democráticas sumado a frescos y variados neologismos que atraen a estúpidos con título o sin título. Que incluso, sirven de epígrafe a todos los planes de la barbarie y convenciendo a los más incautos que resultan anunciando en las calles: “plomo es lo que viene”.]

[El fascismo del siglo XXI ha convertido al institucionalismo en principio de autoridad … ¡es infalible!... Esa es su característica más ontológica. De ahí para abajo, los intereses económicos, la tierra, la banca entre otros, son fieles derivaciones sujetas a dicho principio que los conserva, que los renueva constantemente, que los mantiene en el poder.]

[Qué más se puede hablar del fascismo del siglo XXI, no creo que merezca tanta prosa, pues está muriendo gente, y aparte de todo, los intereses casi “metafísicos” de las BACRIM refuerzan la idea de que las víctimas son culpables de su destino. Se está llamando “sicarios” a los parlamentarios en vez de atender dialógicamente una controversia.]

[Humberto Eco advertía de algunas señales que indican sobre la aparición del fascismo como lo son,]**el culto a la tradición**[ (preguntémosle a Ordoñez),]**el rechazo del modernismo**[ (preguntémosle a Cabal),]**oposición sistemática a la crítica**[ (miremos la persecución jurídica a la oposición),]**miedo a la diferencia**[ (pregúntele a los uribistas que gritan a homosexuales en centros comerciales yo gritan “te pelamos” a un muchacho etc.),]**alianza con clases frustradas**[ (miserables sin finca pudieron ver cómo otros sí volvieron a sus fincas, o también miserables arribistas con tarjeta de crédito confían en las migajas culturales de sus amos),]**apelación a una amenaza constante**[(ahora llamada “disidencia”),]**búsqueda de culpables externos**[ (la eterna Venezuela),]**utilización de un líder carismático**[ (¡Ave Uribe! emperador de Colombia),]**uso de vocabulario sencillo basado en tópicos**[ (miremos al mamarracho y poco educado Guaidó con su “cuando cese la usurpación” y a Duque con su “cerco diplomático” o su “economía naranja”)… ¿huele a fascismo? Desde Eco se podría decir que sí.]

[A esos fascistas del siglo XXI un solo mensaje: nosotros, el otro país, existimos y si no contribuimos a que duerman tranquilos, aplaudidos en su afán de neutralizar la política, de solicitar desde el baile de las ballenas la no polarización para que nada cambie y todo siga igual; en nombre de Morfeo que está de nuestro lado, junto con Virtus y Areté, les pedimos disculpas.]

[Para una América Latina sumida en la injusticia, esa, Nuestra América que quieren negar, que quieren callar, no hay realidad separada del misterio, por ello nuestros muertos descansan en ataúdes de guadua y concha nácar, por eso gritamos en plural la frase de Ortega y Gasset ¡“]*[no nos podemos salvar, si no salvamos también nuestras circunstancias]*[”!]

------------------------------------------------------------------------

<div class="entry-content">

<div class="has-content-area" title="Las licencias obligatorias, una herramienta de política pública que salva vidas" data-url="https://archivo.contagioradio.com/las-licencias-obligatorias-una-herramienta-de-politica-publica-que-salva-vidas/" data-title="Las licencias obligatorias, una herramienta de política pública que salva vidas">

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

</div>

</div>
