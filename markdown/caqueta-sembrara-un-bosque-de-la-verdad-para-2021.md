Title: Caquetá sembrará un bosque de la verdad para 2021
Date: 2019-07-12 16:44
Author: CtgAdm
Category: Nacional, Paz
Tags: Caquetá, Casas de la Verdad, comision de la verdad
Slug: caqueta-sembrara-un-bosque-de-la-verdad-para-2021
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/florencia2.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/minas-nororiente1_4d33eb5e80598f3da72c9a42ffe9fe59-e1562967804309.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ComisionVerdadC] 

Continúa el trabajo de la Comisión para el Esclarecimiento de la Verdad, esta vez en el marco de la inauguración de la **Casa de la Verdad en Florencia, Caquetá** a la que asistieron cerca de 300 personas incluidas víctimas y sobrevivientes del conflicto provenientes de los 16 municipios del departamento. Durante el evento también estuvieron presentes la comisionada Marta Ruíz, además de autoridades eclesiásticas como el padre Javier Giraldo, y funcionarios militares y administrativos del territorio.

**Fernando Cruz, coordinador de esta casa de la verdad,** expresó "queremos que sea un lugar dinámico donde podamos escuchar a la gente, a las víctimas y a los responsables que tengan compromiso con la sociedad y que quieran ayudarnos a salir de este laberinto" aseguró Cruz quien espera que la casa de la verdad de Florencia se convierta en un referente para otros procesos de reconciliación.

Durante la inauguración se realizó la iniciativa Heleva, la que consistió en escribir sus verdades sobre el conflicto en unos papeles que serán encapsulados y en 2021, año en que finaliza el trabajo de la Comisión, serán dadas a conocer para luego sembrarlas  en un bosque de Caquetá.

### Cómo funcionará el trabajo de la Comisión en Caquetá 

Particularmente en el departamento de Caquetá, la Comisión dividirá el trabajo en cinco zonas: **Bajo Caguán, Norte, Centro, Sur y Bajo Caquetá**, donde recolectarán diversos testimonios para luego articularlos y sumarlos a los de los otros territorios. [(Lea también: Comisión de la Verdad lista para tejer nuevos relatos de memoria en Medellín)](https://archivo.contagioradio.com/comision-de-la-verdad-lista-para-tejer-nuevos-relatos-de-memoria-en-medellin/)

Frente a los recortes de presupuesto, el coordinador señaló, que esta alerta es un llamado para que la Comisión se fortalezca y dé un buen uso a los recursos que le fueron asignados y para continuar creando alianzas con instituciones que permitan siga desarrollándose el trabajo de la entidad.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38372618" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38372618_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
