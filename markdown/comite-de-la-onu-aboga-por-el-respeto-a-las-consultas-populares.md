Title: Comité de la ONU exije por el respeto a las consultas populares
Date: 2017-10-11 12:09
Category: DDHH, Nacional
Tags: consultas populares, Implementación de los Acuerdos de paz, ONU
Slug: comite-de-la-onu-aboga-por-el-respeto-a-las-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2Orillas] 

###### [10 Oct 2017] 

El Comité de Derechos Económicos, Sociales y Culturales de la ONU dio su informe sobre el sexto periodo sobre la situación de derechos humanos en Colombia, en el que manifestó su preocupación por la **falta de acción de las instituciones gubernamentales en adoptar las consultas populares en los territorios** del país contra la minería y la voluntad de las personas expresada en este mecanismo.

De igual forma manifestaron la **urgencia por la implementación de los Acuerdos de Paz**, firmados entre el Gobierno y las FARC, la sitaución de los defensores de derechos humanos y el estado de vulneración de los derechos de la población afro, indígena y de las mujeres.

### **Explotación de Recursos Naturales**

En este punto el Comité de la ONU manifestó que le preocupa que no se tenga en cuenta el resultado de las consultas populares por las autoridades correspondientes, **y que a pesar de los resultados los proyectos continúen en los territorios**. (Le puede interesar: ["Consultas populares en Santander son demandadas por Min. Minas"](https://archivo.contagioradio.com/ministerio_minas_consulta_popular_sucre_santander/))

“Preocupa, además, los daños que tienen estas actividades en el medio ambiente, incluyendo la deforestación y el impacto negativo en el goce **efectivos de los derechos culturales, económicos y sociales”**, señala el texto del informe.

Por tal razón, el comité recomienda al gobierno que tome las medidas necesarias para asegurar que los resultados de las consultas populares sean debidamente valorados y **tengan validez ante las autoridades correspondientes y que su implementación se lleve a cabo de forma concertada con las comunidades**.

### **La situación de los Defensores de Derechos Humanos** 

En el informe el Comité señaló que continúa la preocupación por la persistencia e incluso aumento de casos de hostigamientos, agresión y atentados contra la vida de defensores de derechos humanos, lo que configura **“un obstáculo para la construcción de paz en el país”**.

Razón por la cual le recomiendan al gobierno que investigue de forma exhaustiva y eficaz todas las denuncias de actos de violencia, amenazas y atentados contra la vida de defensores de derechos humanos. De igual manera, expresó que es importante que se **continúen con los esfuerzos para asegurar el funcionamiento de la Unidad Nacional de Protección para prevenir situaciones de violencia**.

Además, le manifestó al gobierno la importancia de generar campañas comunicativas que expliquen y den importancia a la labor que cumplen los defensores de derechos humanos al interior de la sociedad. (Le puede interesar: ["Policía sigue actuando de forma "criminal" en Tumaco"](https://archivo.contagioradio.com/policia-sigue-actuando-de-manera-criminal-en-tumaco/))

###  **Derechos de las minorías** 

De igual forma en el informé el Comité realiza recomendaciones sobre la falta de cumplimiento de los acuerdos de paz con las comunidades afro e indígenas en la realización de las consultas previas, razón por la cual asegura que debe hacerse un proceso amplio de consulta previa y **que aumente sus esfuerzos por eliminar las condiciones y actitudes que perpetúan la discriminación.**

También solicitan en el informe se ponga en marcha planes y medidas que reduzcan la brecha de género laboral y **que garantice el acceso a las mujeres al goce pleno de sus derechos al interior de las empresas**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
