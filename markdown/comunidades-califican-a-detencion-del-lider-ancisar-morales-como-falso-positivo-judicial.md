Title: Comunidades califican detención del líder Ancísar Morales como "falso positivo judicial"
Date: 2019-04-25 13:46
Author: CtgAdm
Category: Judicial, Líderes sociales
Tags: Antioquia, falso positivo judicial, Fiscalía general, Movimiento Social por la Vida y la Defensa del Territorio
Slug: comunidades-califican-a-detencion-del-lider-ancisar-morales-como-falso-positivo-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-74.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Facebook] 

Comunidades del oriente de Antioquia rechazaron la detención de **Ancísar de Jesús Morales Zuluaga**, campesino, líder social y concejal del municipio San Francisco, quien el pasado 8 de marzo fue capturado por el presunto tráfico de estupefacientes y la utilización de menores para la comisión de ilícitos.

De acuerdo a la Fiscalía General y el Cuerpo Técnico de Investigación (CTI), Morales habría pagado entre \$50.000 y \$60.000 diarios a un adolescente de 17 años de edad para distribuir marihuana entre la población local, lo cual fue confirmado por el menor, quien también sostuvo que Morales lo amenazó con un arma de fuego después de que la mercancía ilícita bajo su responsabilidad fuese incautada por la policía.

Según el juez de segunda instancia, existen "suficientes materiales probatorios para inferir de manera razonable" que Morales pudo cometer estos crímenes, pues tuvo acceso a  una interceptación telefónica que supuestamente compromete al líder social en eventuales actividades relacionadas con el tráfico de narcóticos.

Además de trabajar como concejal, Morales se destaca como miembro de la Junta Directiva de la Asociación Campesina de Antioquia y su labor en el [Movimiento Social Por la Vida y la Defensa del Territorio](MOVETE), que se ha manifestado en contra del represamiento del Río Samaná por proyectos hidroeléctricas. Adicionalmente, el líder había denunciado actividad criminal en el municipio.

La **Corporación Jurídica Libertad**, organización que representa a Morales, afirmó en un comunicado que la detención del líder social hace parte de un montaje judicial, incluso señaló que **uno de los declarantes que testificó en su contra había sido denunciado por Morales ante la Fuerza Pública del municipio por la venta de estupefacientes**.

La Corporación también rechazó la decisión de la Fiscalía de imputar al líder por el delito de tenencia de armas de fuego, dado que la escopeta que le encontraron en su residencia en la vereda Boquerón se utiliza en el territorio para cazar en zona de bosque. (Le puede interesar: "[Como persecución política califican detención de líder Wayuú Javier Rojas](https://archivo.contagioradio.com/persecucion-politica-califican-detencion-lider-wayuu-javier-rojas/)")

"Estas son graves acusaciones contra un defensor de derechos humanos, que proviene de una familia humilde, víctima de crímenes de Estado (ejecución extrajudicial), y que se ha destacado por ser un líder social, comunal y campesino que ha ayudado a emprender programas dirigidos hacia esta población", declaran en el comunicado en el que exigen la liberación del líder.

"Contrario a lo expresado por el juez, Ancísar de Jesús Morales, igual que otros líderes sociales en el país, cumple una labor fundamental para el bienestar de la comunidad, y hoy es víctima de falsas acusaciones, en las que **tanto personas como instituciones son utilizadas por criminales para desviar la atención y quitar del medio a quienes denuncian abiertamente sus actividades ilícitas**, y así poder tener el camino libre a sus nefastos intereses en los territorios", continúa el texto.

Carlos Olaya, integrante del MOVETE, coincide con las afirmaciones de la Corporación y agrega que la detención se trata de un "falso positivo judicial", que ha generado entre las comunidades indignación pero también "incertidumbres y miedos para la participación social y para los políticos de carácter alternativo".

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
