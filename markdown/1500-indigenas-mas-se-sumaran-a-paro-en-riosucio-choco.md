Title: 1500 indígenas se sumarán a paro de desplazados en Riosucio, Chocó
Date: 2017-05-08 16:12
Category: DDHH, Entrevistas
Tags: afrodescendientes, indígenas, paro, Riosucio
Slug: 1500-indigenas-mas-se-sumaran-a-paro-en-riosucio-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Minga-Riosucio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:AMA Alternativos] 

###### [08 May. 2017] 

Luego de 8 días de paro que adelantan cerca de 3 mil afrodescendientes e indígenas **en Riosucio, departamento de Chocó, la situación humanitaria y de seguridad continúa siendo preocupante** dado que en los albergues no cuentan con las condiciones necesarias, el agua potable escasea, las lluvias los han inundado, los niños tienen problemas de salud y la atención ha sido casi nula.

Uno de los integrantes de este paro manifiesta que, si bien han tenido unos primeros acercamientos con el Gobernador del Chocó, con el senador del Movimiento Alternativo Indígena, Luis Evelis Andrade y con la Alcaldía, estos solo han sido para entregar el pliego de peticiones.

“Hasta el momento lo que hemos podido hacer es entregar el pliego de peticiones que tenemos con nuestras comunidades. **En este momento uno de los mayores problemas que se vive en nuestro territorio es el campo minado,** no hay presencia del Ejército” puntualiza el poblador. Le puede interesar: [600 personas del Río Truandó en Chocó fueron desplazadas por presencia paramilitar](https://archivo.contagioradio.com/38661/)

Además, dice que otra de las preocupaciones es que **al intentar combatir a las estructuras del ELN que se encuentran en la zona, el minado en sus territorios se incremente,** afectando la permanencia y la vida de sus comunidades. Le puede interesar: [Enfrentamientos entre paramilitares y ELN desplazan 300 familias en Chocó](https://archivo.contagioradio.com/desplazamiento-choco-enfrentamientos/)

### **"Vivimos la guerra del 97 y no la queremos vivir más"** 

“Nosotros sentimos que nos están atacando, **hemos sido desplazados 5 veces, hemos estado confinados, nuestros campos minados,** el reclutamiento de nuestros niños y ahora nos responden con dos camionados de antimotines del ESMAD” relata el líder. Le puede interesar: [Confinadas comunidades indígenas y afros en el Litoral de San Juan Chocó](https://archivo.contagioradio.com/san-juan-choco-paramilitares/)

Aseguró el poblador que de no encontrar respuestas de las instituciones del estado y de sus funcionarios tendrían un plan B “nosotros hemos dicho que **vivimos la guerra del 97 y no la queremos vivir más** y está vez la estamos viviendo más peligrosa y más mortal. Haremos lo que sea necesario, habrá un consejo de seguridad y allá estaremos”.

Así mismo, se podrían sumar en los próximos días cerca de 1500 indígenas más “para que nos escuchen, porque nosotros estamos sufriendo y no podemos descartar el paro porque el Gobernador vino o porque el Senador vino” recalcó el integrante del paro. Le puede interesar: [En Chocó, indígenas y afrodescendientes marchan contra el paramilitarismo](https://archivo.contagioradio.com/en-choco-indigenas-y-afrodescendientes-marchan-contra-el-paramilitarismo/)

Las habitantes del departamento del Chocó, dice el poblador, han apoyado en gran medida este paro que pretende mejoras para las condiciones de vida de los afrodescendientes e indígenas “nosotros hemos detenido lanchas o transportes porque estamos en paro, entonces **hay mucha gente preocupada, pero también hay mucha gente que se nos ha unido** porque sabemos que sabemos los incumplimientos con los habitantes del Chocó”.

<iframe id="audio_18564853" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18564853_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
