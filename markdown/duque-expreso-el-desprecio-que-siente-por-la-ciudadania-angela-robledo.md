Title: Duque expresó "el desprecio que siente por la ciudadanía", Ángela Robledo
Date: 2020-09-17 17:59
Author: AdminContagio
Category: Actualidad, Política
Tags: Iván Duque, Policía Nacional
Slug: duque-expreso-el-desprecio-que-siente-por-la-ciudadania-angela-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Duque-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Iván Duque / Cortesía

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Iván Duque visitó tres de los CAI destruidos en Bogotá durante las manifestaciones en respuesta a la violencia policial y el asesinato del abogado Javier Ordoñez. Mientras ambos funcionarios insisten en relegar la responsabilidad de los hechos violentos a actores armados, estigmatizando a organizaciones sociales, desde el Congreso y la ciudadanía exigen, asuman su responsabilidad política.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, la representante Ángela María Robledo tildó el hecho de "desconcertante", ratificando "el desprecio que siente por la ciudadanía", agregando que si bien, el presidente quiere expresar su apoyo a la Policía en medio de la crisis que vive la institución, el mandatario "está ciego, sordo y mudo" al no reconocer la situación que está viviendo el país. [(Lea también: Tribunal ordena a Duque responder a comunidades que exigen cese al fuego en sus territorios)](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para la congresista, llama la atención que la visita se haya dado en la noche, cuando "no aparece en el escenario público como debe hacerlo" para responder las preguntas que debe asumir como comandante en jefe de la Policía Nacional. [(le recomendamos leer: \[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que al incluso usar su uniforme, es una forma de **"revictimizar a una población herida"**, en especial a las familias de los 14 personas que perdieron la vida la noche del pasado 9 de agosto, los heridos e incluso a la misma Policia, pues Duque debió acudir al acto de reconciliación propuesto por la Alcaldía de Bogotá para "reconocer su responsabilidad política en lo ocurrido". [(Lea también: Mejorar imagen de Duque con recursos para la paz: una muestra de insensibilidad y desconexión hacia el país)](https://archivo.contagioradio.com/mejorar-imagen-de-duque-con-recursos-para-la-paz-una-muestra-de-insensibilidad-y-desconexion-hacia-el-pais/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "¿En manos de quién estamos?"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La visita del presidente Duque a los CAI de la Policía se dió en medio de un debate político contra el ministro de Defensa, Carlos Holmes Trujillo. Pese a que el funcionario reconoció en la noche de este martes que la muerte del abogado Javier Ordoñez en Bogotá fue responsabilidad de la Policía, este no asistió de forma presencia a su citación a la Cámara de Representantes, haciéndolo de forma virtual acompañado de la Cúpula militar y de la Policía.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/mindefensa/status/1306343758712262656","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/mindefensa/status/1306343758712262656

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

En el Senado son varios los congresistas que han exigido la renuncia de Holmes Trujillo a su cargo en medio de un contexto en el que son casi 60 las masacres ocurridas en solo 2020, y en el que en fueron asesinadas 14 personas durante las protestas contra el abuso policial, además de evidenciarse el uso de armas de fuego por parte de miembros de la Policía que participaron en dichas acciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Legisladores también expresaron que lo ocurrido en Bogotá fue un hecho que demostró la necesidad de una reforma integral a la Policía. Acerca del debate de control político al gabinete de Defensa, la representante señaló que es necesario buscar justiciaa, verdad y no solo responsabilidades sociales sino políticas. [(Lea también: Cinco normativas que deben cambiarse para frenar delitos de la Policía)](https://archivo.contagioradio.com/cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "A quien le resulta funciona la violencia es al mismo gobierno, aquí hay que retomar el espíritu de lo que significó salir en 2018 y en 2019 donde la mayoría de las movilizaciones fueron pacificas, esa indignación ha que mantenerla y es una forma de presionar medias a corta y mediano plazo a la Policía
>
> <cite>Ángela María Robledo </cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

La representante agrega que el Código de Policía lanzado hace unos años fue un momento en el que "se contribuyó a quebrar ese aprecio que tenia la ciudadanía por la Policía y sus actuaciones, se rompió la confianza y recuperarla será una tarea muy grande". [(Le puede interesar: Hay una ruptura entre el poder y la autoridad: Dario Monsalve)](https://archivo.contagioradio.com/hay-una-ruptura-entre-el-poder-y-la-autoridad-dario-monsalve/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No es un problema únicamente en la Policía

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Angela María Robledo señala que lo que viene sucediendo con el escalamiento de las masacres, los asesinatos de liderazgos sociales y los recientes actos de violencia ocurridos en Bogotá **son una alerta del deterioro de la esperanza que existía en el país con el Acuerdo de Paz,** cuya firma "despertó emociones de perspectivas de presente de futuro, de ponerse en el lugar del otro y de romper con la máquina de la guerra", sin embargo, el incremento en el conflicto ha permitido el regreso de doctrinas como la del enemigo interno.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Lo que paso con Guillermo Botero parecía que era lo peor, un bombardeo donde se sabía que había menores de edad, ahora ocurre esto, debemos seguir resistiendo desde el Congreso y exigir a Duque que respete la división de poderes y la constitución", expresó la representante con relación al antecesor de [Holmes Trujillo](https://twitter.com/CarlosHolmesTru).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente al Ministerio de Defensa, también cuestionando por los sucesos de interceptaciones ilegales, directrices que desde el Ejército podrían convertirse en un nuevo episodio de ejecuciones extrajudiciales y el ataque a un campamento armado en el que se sabía, habían menores de edad reclutados; cabe señalar que ninguno de estos cuestionamientos han sido resueltos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La representante señaló que es necesario seguir reivindicando la exigencia de los derechos y proteger de manera especial a la población joven, que ha sido la que ha sido criminalizada por las declaraciones del ministro Holmes Trujillo y del comisionado de Paz, Miguel Ceballos. [(Le puede interesar: ELN niega vinculación a manifestaciones y reitera voluntad de diálogo)](https://archivo.contagioradio.com/eln-niega-vinculacion-a-manifestaciones-y-reitera-voluntad-de-dialogo/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
