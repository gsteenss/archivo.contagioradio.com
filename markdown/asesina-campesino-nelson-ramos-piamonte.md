Title: Asesinan a Nelson Ramos integrante de ASIMTRACAMPIC en Piamonte, Cauca
Date: 2020-10-15 12:42
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Asesinato de campesinos, ASIMTRACAMPIC
Slug: asesina-campesino-nelson-ramos-piamonte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Nelson-Ramos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Villano Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La **Asociación Municipal de Trabajadores Campesinos de Piamonte Cauca (ASIMTRACAMPIC)** filial de FENSUAGRO denunció el asesinato del campesino **Nelson Ramos Barrera** en la vereda Yapurá del corregimiento de Yapurá, del municipio de Piamonte, Cauca el pasado 13 de octubre.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La organización campesina denuncia que los hechos ocurrieron a la 1:00 pm cuando hombres pertenecientes a un grupo armado ilegal que no fue especificado pero que señalan, opera en la zona, llegaron hasta la vereda reclamando a sus habitantes hacer presencia en el polideportivo.  
  
En medio de este accionar, los hombres decidieron llevarse a Nelson Ramos Barrera como una muestra del control territorial que ejercen para posteriormente asesinarlo frente a su familia y la comunidad que mostraron resistencia cuando intentaron llevárselo. [(Lea también: Masacre en Piamonte, un reflejo de la persecución a campesinos)](https://archivo.contagioradio.com/masacre-en-piamonte-reflejo-persecucion-campesinos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, los habitantes de la zona, afirman que durante el tiempo en que el grupo armado hizo presencia en el polideportivo, un helicóptero sobrevoló la zona, y agregan que la Fuerza Púbica tenía **"pleno conocimiento que los integrantes de la asociación estaban amenazados desde el 5 de marzo por los grupos armado que operan en la región"**, por lo que exigen le brinden su apoyo a las entidades que cumplen una labor humanitaria en la zona.  
  
**ASIMTRACAMPIC** expresó su preocupación pues Nelson Ramos y su familia son integrantes de la organización y temen que el grupo armado atente también contra los familiares del campesino o en general de los habitantes de la región que también están afiliados a la organización.  
  
Dirigentes de **ASINTRACAMPIC han alertado que la organización ha sido declarada como objetivo militar por grupos como el Cartel de Sinaloa, ‘La Constru’ o las disidencias de las FARC,** además de las denuncias que ha hecho la [Comisión de Justicia Paz ](https://twitter.com/Justiciaypazcol)sobre grupos como ‘La Mafia’ quienes también delinquen en el municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para abril de este año, los integrantes de la Asociación de Trabajadores Campesinos de Piamonte asesinados llegaban a cinco, registro que asciende hoy con el asesinato de Nelson Ramos. [(Le puede interesar: Campaña "Defendamos la Vida" seguirá respaldando el trabajo de los líderes sociales)](https://archivo.contagioradio.com/campana-defendamos-la-vida-seguira-respaldando-el-trabajo-de-los-lideres-sociales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

ASINTRACAMPIC **viene trabajando por la conservación de 1.670 hectáreas de bosque primario junto a cerca de 130 familias y al menos 945 que participan de la sustitución voluntaria de cultivos de uso ilícito**. Pese a la presencia de actores armados que se disputan el territorio y la ausencia del Estado que hasta el momento no ha entregado los proyectos productivos, sus integrantes continúan en su función defendiendo los derechos ambientales y de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
