Title: 77 personas asesinadas entre enero y julio en Soacha
Date: 2016-07-22 12:40
Category: DDHH, Nacional
Tags: asesinatos altos de cazuca, asesinatos en Soacha, homicidios en soacha
Slug: 77-personas-asesinadas-entre-enero-y-julio-en-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Soacha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pasión y Vida ] 

###### [22 Julio 2016 ] 

Entre enero y mayo de este año se registraron 58 homicidios en Soacha, 26 de ellos en Altos de Cazucá, una de las comunas a las que ha llegado gran parte de las 10.640 personas en condición de desplazamiento que arribaron al municipio durante los últimos 5 años, y que se enfrentan a los indices de pobreza e indigencia que afronta la población, 53,8% y 20,4%, respectivamente. Pese a las alertas tempranas emitidas por la Defensoría el pasado mes de junio, por el asesinato selectivo de 16 pobladores, la situación no parece mejorar, **este viernes fueron asesinados tres habitantes en el barrio El Progreso**.

"Las versiones de la comunidad del sector argumentan que en horas de la noche algunos hombres que se movilizaban en una camioneta y en un vehículo, pasaron disparando, asesinaron inicialmente a una de las víctimas, las otras dos lograron escapar pero cinco cuadras después fueron ultimadas a balazos", afirma Ariel González, director del medio local Periodismo Público, quien agrega que **la versión de la Policía es que estos asesinatos "obedecen a retaliaciones entre bandas del microtráfico** (...) justamente cuando reportan la captura de 16 delincuentes correspondientes a la banda de los Micho que ha tenido el control total de la venta de estupefacientes en este sector".

De acuerdo con González la comunidades tienen miedo, incluso varios de sus líderes han pedido protección porque están siendo blanco de amenazas y agresiones por parte de los miembros de estas bandas que se han tomado el control total de la zona, ante la ausencia estatal. Lo cierto es que los habitantes se sienten desprotegidos y dicen que no es posible que a pesar de las [[advertencias de la Defensoría](https://archivo.contagioradio.com/16-asesinatos-selectivos-entre-mayo-y-junio-alarman-a-habitantes-de-soacha/)] sigan ocurriendo asesinatos, así mismo consideran insuficientes **las capturas porque no cortan el problema de raíz que radica en que el Estado no hace presencia** garantizando empleo, educación y salud.

"La respuesta de las autoridades es tomarse ciertas zonas (...) pero en el fondo son pañitos de agua tibia porque no hay presencia como debe ser, **la solución no es simplemente militarizar y capturar a las bandas sino que el Estado intervenga**,** **que mire la problemática social y comience a intervenir en el tema de cobertura en salud, en servicios públicos, en educación y en empleo, pero eso no se ha hecho (...) uno no ve que haya intervención que haya oportunidades, programas sociales, creo que en eso se está fallando (...) es una responsabilidad del municipio, el departamento y el país", concluye González.

<iframe src="http://co.ivoox.com/es/player_ej_12307847_2_1.html?data=kpegkpyceJihhpywj5aWaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncaLmysrZjazTstuZpJiSo5bQqduZk6iYssrWrdDYytja0ZC0aaSnhqeuxNHNp9Chhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) ] 
