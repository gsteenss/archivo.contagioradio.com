Title: Asesinan a Unaldo Castillo y Yoimar Jiménez excombatientes de FARC en Antioquia
Date: 2020-07-29 12:16
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Excombatientes de FARC, Exterminio sistemático, Partido FARC, Unaldo Castillo, Yoimar Jiménez
Slug: asesinan-a-unaldo-castillo-y-yoimar-jimenez-excombatientes-de-farc-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Asesinados-los-excombatientes-de-FARC-Unaldo-Castillo-y-Yoimar-Jiménez..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes, hombres armados asesinaron al excombatiente de FARC en proceso de reincorporación Unaldo Castillo y Yoimar Jiménez, integrante de la Unidad Nacional de Protección -UNP- quien también era firmante de paz. (Le puede interesar: [Siguen los asesinatos contra líderes y excombatientes en medio de la pandemia](https://archivo.contagioradio.com/siguen-los-asesinatos-contra-lideres-y-excombatientes-en-medio-de-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se presentó en  la vereda Sabanas, zona rural del municipio de Urrao, Antioquia; donde **hombres armados atentaron en contra del esquema de seguridad del también excombatiente José Ignacio Sánchez**, promotor de las actividades de la Comisión Especial de la Verdad en el noroccidente del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según versiones preliminares, el atentado estaba dirigido contra José Ignacio, pero en él resultaron **asesinados Unaldo Castillo y Yoimar Jiménez,** este último, integraba el esquema de protección del excombatiente en calidad de escolta de la UNP.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComunesANT/status/1288310393060888581","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComunesANT/status/1288310393060888581

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El [Partido FARC](https://twitter.com/PartidoFARC) denunció el hecho, al tiempo que **alertó que en Antioquia, han sido asesinados al menos 25 excombatientes en lo corrido del año**, siendo esta una de las regiones más afectadas por hechos violentos en contra de exintegrantes del extinto grupo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, hicieron un llamado urgente al Gobierno Nacional para que «*cumpla con la implementación integral del Acuerdo de Paz, haga presencia en los territorios y brinde verdaderas garantías para la vida y la construcción de paz en el país*». (Lea también: [«Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También, pidieron a la comunidad internacional, la ONU, los países acompañantes del proceso de paz, organizaciones defensoras de derechos humanos y en general a la sociedad civil, **rodear el Acuerdo de Paz firmado entre el Gobierno del expresidente Juan Manuel Santos y las FARC.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los asesinatos de Unaldo Castillo y Yoimar Jiménez una muestra más de las violencias contra los excombatientes de FARC

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cabe recordar, que hace unos días se registró, justamente en Antioquia, el **desplazamiento forzado de los excombatientes que adelantaban su proceso de reinserción a la vida civil en el Espacio Territorial de Capacitación y Reincorporación -ETCR- “Santa Lucia” en Ituango**, hacia Mutatá por causa de las violencias y amenazas a las que estaban siendo sometidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Partido FARC, **son 222 los excombatientes asesinados** desde que se firmara el Acuerdo de Paz en el año 2016.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1288328600954052609","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1288328600954052609

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Todos estos hechos, han llevado a que el Partido FARC señale que están siendo víctimas de un **«exterminio sistemático»**, razón por la cual, han elevado varias solicitudes para que les sean decretadas medidas cautelares de protección a su vida e integridad física; ante instituciones como la Jurisdicción Especial para la Paz e incluso ante organismos internacionales como la Comisión Interamericana de Derechos Humanos. (Le puede interesar: [Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
