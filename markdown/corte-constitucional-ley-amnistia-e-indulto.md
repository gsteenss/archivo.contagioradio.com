Title: Corte Constitucional restringe Ley de Amnistía e indulto
Date: 2018-03-01 20:43
Category: Entrevistas, Nacional, Paz
Tags: acuerdo de paz, Corte Constitucional, Corte Penal Inteernacional, Ley de Amnistía e Indulto
Slug: corte-constitucional-ley-amnistia-e-indulto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Corte-constitucional-e1470436473989.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: www.tnnpoliticas.com] 

###### [1 Mar 2018] 

La Corte Constitucional concluyó que la Ley de Amnistía e Indulto acordada en La Habana y ya firmada por la presidencia, es acorde a la Constitución y por ello le dio vía libre a dicha Ley. No obstante, hizo algunas precisiones que podrían representar cierto nivel incertidumbre para quienes han decidido dejar la armas y reintegrarse a la sociedad civil.

Uno de los cambios que ha decidido hacer la Corte Constitucional y que más llama la atención tiene que ver con que será el Congreso el que podrá definir cuándo un excombatiente incumple, y cuáles serán las consecuencias por ello.  "En principio las libertades son definitivas pero **el Congreso de la República podrá determinar cuáles son las consecuencias de incumplir las condicionalidades del sistema",** señala el comunicado de la Corte.

Lo anterior quiere decir que en dado caso de que el legislativo determine si un integrante de la FARC incumplió con contribuir con la verdad, esto podría conllevar a la pérdida de los beneficios de amnistía y la libertad.

También se hicieron cambios sobre **el delito de reclutamiento de menores de 15 años, pues este no será amnistiable.** El alto tribunal ha establecido esa condición si dicha actividad fue cometida después del 25 de julio del 2005, teniendo en cuenta los compromisos que adquirió el país luego de esa fecha. Siendo así, esos casos quedarían en manos de la Justicia Especial de Paz.

Asimismo, otro de los temas que dejaría en la cuerda floja a los excombatientes, tiene  que ver con que los crímenes de guerra no serán admnistiables. **La Corte Penal Internacional había dicho eso, pero en referencia a los crímenes cometidos por agentes estatales**, sin embargo lo que ha hecho la Corte es admitir esa condición para todos las partes que participaron en el conflicto.

"Crear la categoría graves crímenes de guerra sistemáticos generaba una falta de claridad sobre lo que ya está definido en el derecho internacional", explicó la magistrada Diana Fajardo, ponente de la decisión.

Finalmente, cabe resaltar que pese a las recomendaciones de la Corte Penal, **los beneficios para los funcionarios del Estado que hayan comedido crímenes en el marco del conflicto armado se mantuvieron intactos.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]{.s1}
