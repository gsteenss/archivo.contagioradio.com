Title: Las artes escénicas tejen redes en la Plataforma Conecta
Date: 2017-10-23 17:19
Category: eventos
Tags: Bogotá, conecta, teatro
Slug: plataforma-conecta-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/image001-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Idartes 

###### 23 Oct 2017 

Del 23 al 25 de octubre se realiza en Bogotá la segunda versión de Plataforma Conecta, una iniciativa que busca vincular experiencias del campo teatral y las artes vivas, con entidades del sector público y privado para construir redes y alianzas a través de un espacio de visibilización y encuentro.

En esta versión, participan quince proyectos culturales relacionados con: arte, ciencia y tecnología, arte para la transformación social, y emprendimiento cultural, líneas de trabajo transversales del Idartes. Cada día se desarrollará una línea de acción, a partir de dos espacios: un pitch o muestras de experiencia y un diálogo entre el sector oficial, privado y demás asistentes de la plataforma con los artistas invitados para ampliar sus horizontes.

Tras un trabajo continuo de gestión y desarrollo de la Plataforma para esta versión 2017 se espera vincular a más de 200 representantes, superando la cifra del año anterior, de Ministerios, embajadas, universidades, medios de comunicación y empresarios para que conozcan las experiencias, productos y metodologías de : Radio Bestial, Sin Diploma: creadores en compañía, Medea 73, ConcuerpoYmov, N.N, La Compañía de Teatro La Maldita Vanidad, La Navaja Ockhan, Sensósfera, La Asociación Gapomaro, Casa Abierta (ASAB), Kathalaya Colombia, Fundación Mulato, El otro Trapo, La Sala, La Tienda Teatral y Soy Teatro, los cuales fueron seleccionados a partir de una evaluación minuciosa del equipo organizador.

Las formas de emprendimiento que pueden tener los artistas, las posibilidades de transformar y transformarse a través del arte, y los caminos estéticos que exploran en la triada arte, ciencia y tecnología, son diversos y se componen de variadas y múltiples acciones que los han llevado por años, en no pocos casos, a sentar su pertinencia social, a instaurar sus preguntas de investigación y a encontrar en su quehacer su fuente económica de vida.

Estos emprendedores y los nuevos de la escena, son fuente de conocimiento y pueden ayudar al sector a identificar experiencias exitosas para aprender, intercambiar ideas, generar conocimiento y replicar acciones. La Plataforma Conecta busca hacer visible estas prácticas y generar un espacio para el diálogo y la conexión, que pueda redundar en el mejoramiento de las mismas, siguiendo el eje de Igualdad de calidad de vida del Plan de Desarrollo “Bogotá Mejor para Todos”, en lo que tiene que ver con las oportunidades para el desarrollo a través de la cultura, la recreación y el deporte.

Es por eso, que la Plataforma Conecta está dirigida a gestores culturales, productores, directores artísticos, actores, curadores, programadores, dramaturgos, investigadores, maquillistas, técnicos, vestuaristas, directores de arte, administradores, empresarios y comunicadores relacionados con el sector teatral y de las artes vivas de la ciudad.

La Plataforma Conecta se llevará a cabo el lunes 23, martes 24 y miércoles 25 de octubre desde las 6:00 p.m. en la Sede Teusaquillo de Facultad de Artes la ASAB de la Universidad Distrital, ubicada en la carrera 15 Bis No. 39 - 39. Entrada libre. Inquietudes o mayor información en artedramatico@idartes.gov.co

[Programación plataforma conecta](https://www.scribd.com/document/362411158/Programacion-plataforma-conecta#from_embed "View Programación plataforma conecta on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_97537" class="scribd_iframe_embed" title="Programación plataforma conecta" src="https://www.scribd.com/embeds/362411158/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-bIopJCyuV5kPGFhLcbJf&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
