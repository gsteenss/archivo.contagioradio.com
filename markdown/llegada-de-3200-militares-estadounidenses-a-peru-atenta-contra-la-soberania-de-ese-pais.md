Title: Llegada de 3200 militares estadounidenses a Perú atenta contra la soberanía de ese país
Date: 2015-09-03 12:04
Category: El mundo, Movilización, Política
Tags: Bases estadounidences, George Washington, Impunidad de militares de EEUU, Injerencia de Estados Unidos, Ollanta Humala, Perú, porta aviones
Slug: llegada-de-3200-militares-estadounidenses-a-peru-atenta-contra-la-soberania-de-ese-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/portaviones-peru.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Taringa 

<iframe src="http://www.ivoox.com/player_ek_7810920_2_1.html?data=mJ2ekp6WdI6ZmKiakp6Jd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkM3ZyMbRw5DIqYynk5WdjdLNsMrowtfS1ZDJt9XVxdTi0M7Iqc%2FnxtiYw5DUqdOZpJiSm6aPpdXZz9mah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carlos Romaniville, Movimiento Comunitario Alfa y Omega.] 

###### [3 Sept 2015] 

El pasado 1 de septiembre organizaciones sociales salieron a movilizarse debido a la **llegada del porta aviones estadounidense USS George Washington con 3.200 militares,** situación que “atenta contra la soberanía del pueblo peruano, lo que genera un malestar tremendo en el país”, asegura Carlos Romaniville, quien hace parte del Movimiento Comunitario Alfa y Omega.

Para el activista, aunque EEUU dice llegar a Perú para combatir el narcotráfico y el terrorismo, lo cierto es que el país norte americano “lo que hace es sembrar terror y muerte”, dice Romaninville  quien agrega que los soldados estadounidenses llegan a su país para adiestrar a los militares peruanos y empezar a **usar técnicas en contra del pueblo y específicamente hacia la movilización social,** teniendo en cuenta que **en Perú existen más de 250 conflictos.**

La llegada de George Washington generó  indignación  y protestas en ciudades como Arequipa, Lima y Huancayo, donde los ciudadanos habían salido el pasado 19 de agosto para protestar por el **incremento de militares que actualmente ya cuenta con una cifra de 4 mil militares estadounidenses.**

El portaaviones llegó al  puerto limeño del Callao, al surgir una cooperación militar bilateral entre la Administración del presidente Ollanta Humala y la de Estados Unidos, ya que el Congreso de Perú había autorizado en enero de este año el ingreso y la permanencia de efectivos estadounidenses en territorio peruano.

**Actualmente hay silencio mediático **por lo que muchos peruanos aun no conocen esa situación. Así mismo, a los peruanos les preocupa la impunidad con la que cuentan los militares de Estados Unidos cuando cometen crímenes en otros países como ha sucedido en Colombia.
