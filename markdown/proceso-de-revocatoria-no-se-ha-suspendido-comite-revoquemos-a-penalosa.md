Title: “Proceso de revocatoria no se ha suspendido”: Comité Revoquemos a Peñalosa
Date: 2017-06-15 12:16
Category: Nacional, Política
Tags: CNE, Enrique Peñalosa, revocatoria, revocatoria Peñalosa
Slug: proceso-de-revocatoria-no-se-ha-suspendido-comite-revoquemos-a-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/laestampida4-e1497546759598.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patrótica] 

###### [15 Jun 2017]

Ante la devolución de las cuentas al Comité Unidos Revoquemos a Peñalosa por parte del Consejo Nacional Electoral CNE, los promotores de la iniciativa aseguraron que **no hay irregularidades en las cuentas y que el proceso de revocatoria tampoco se suspende.**

Diego Pinto, miembro del Comité, afirmó que “algunos medios de información están diciendo que el Consejo Nacional Electoral nos devolvió las cuentas porque hay irregularidades y eso no es así, simplemente **hicieron observaciones naturales frente a temas financieros**”. (Le puede interesar: "[CNE debe respetar constitución": Comité de Revocatoria a Peñalosa](https://archivo.contagioradio.com/la-revocatoria-a-penalosa-continua/)")

Pinto aseguró además que, “hay un descuadre de cuentas por 50 mil pesos que no significa ninguna irregularidad”. El Comité fue enfático en establecer que **esta devolución de cuentas es un acto normal en todo proceso financiero**. “Debemos justificar en que se utilizaron los recursos, garantizar que los donantes de la recolección de las firmas tengan las actas y recopilar información adicional”, afirmó Pinto.

**Proceso de revocatoria no ha sido suspendido**

Para el Comité de revocatoria, “el proceso ha sido interesante y hemos contado con el **apoyo de varios sectores de la sociedad que concuerdan con que Peñalosa ha tenido un mal desempeño**”. Aseguraron además que el CNE ha actuado en contravía del proceso de la revocatoria de Enrique Peñalosa porque “tiene intereses de frenarlo”. (Le puede interesar:"[Con movilizaciones y acciones legales defenderán revocatoria contra Peñalosa](https://archivo.contagioradio.com/mas-de-600-mil-firmas-expresan-insatisfaccion-hacia-gobierno-de-penalosa/)")

De igual forma establecieron que el la revocatoria continúa su proceso normal donde la Registraduría tiene plazo **hasta hoy para verificar y contar las firmas que fueron recolectadas**. Seguidamente ellos y ellas continuarán con las actividades que tienen planeadas para convocar a los capitalinos a las urnas.

<iframe id="audio_19287035" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19287035_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
