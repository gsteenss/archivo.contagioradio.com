Title: Educadores de Palestina califican como ejemplar el Paro de profesores en Colombia
Date: 2017-06-21 15:09
Category: Onda Palestina
Tags: educacion, fecode, Palestina, Paro de maestros
Slug: educadores-palestina-fecode
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Escuela-palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 21 Jun 2017 

En la noche del pasado viernes 16 de junio se anunció el **acuerdo entre el gobierno nacional y el sindicato de educadores FECODE** que dio término a una larga huelga que llevaron a cabo los y las profesoras en Colombia. El acuerdo se dio luego de las movilizaciones que se sintieron por todo el país.

Todas las marchas fueron multitudinarias, pero además **con mucha creatividad**: antorchas, clases en la calle, lectura masiva de libros, letreros con mensajes muy interesantes, arengas con mucha fuerza, batucadas, baile y abrazos para el ESMAD.

**Aunque se dijo en varios medios de comunicación que este paro se hacía por salarios, otra fue la realidad:** se hizo para exigir financiación de la educación pública. En 1993 se había decretado que el 46% de los ingresos corrientes debían ir para salud y educación pero en el 2001 se transformó en solo el 26%. De ahí el origen de la precariedad en infraestructura, alimentación, número de docentes, y en general de la educación pública en Colombia.

Las fuertes movilizaciones de educadores, que se escucharon en todo el mundo, **motivaron al Sindicato General de Profesores Palestinos a enviar un comunicado de solidaridad con FECODE**, en el cual afirmaron que: “La educación es un arma liberadora. Las movilizaciones de profesores/as como las de ustedes son el mejor ejemplo que le podemos dar a nuestros/as estudiantes, especialmente en medio de restricciones y opresión. Ustedes le están enseñando a la sociedad colombiana y a nosotros/as lo que significa un real compromiso con la educación”.

Esta declaración tiene un valor particular, ya que **proviene de una comunidad educativa que ha tenido que sufrir la limitación de sus derechos bajo la ocupación militar israelí**. Solamente en 2016 el Ministerio de Educación Palestino documentó ataques israelíes contra 89.799 estudiantes palestinos y 5.528 profesores.

### **Las alarmantes cifras contra el derecho a la educación en Palestina** 

El ministerio documentó que **26 estudiantes palestinos fueron asesinados** por las fuerzas de ocupación israelíes, **1.810 estudiantes y 101 instructores o empleados resultaron heridos**, **198 estudiantes y profesores fueron detenidos**, hubo 1**62 ataques de contra escuelas palestinas** y las operaciones en los puestos de control israelís condujeron a la **suspensión de 4.878 clases**.

Por esta razón decidimos hacer esta emisión de [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/)dedicada a hablar de la educación en medio del apartheid que sufren los palestinos, pero también recogiendo **análisis sobre lo que ha significado para el magisterio colombiano el gesto de solidaridad producido desde un territorio tan lejano**. Al final, la lucha es la misma: por los derechos para que los niños puedan desarrollar con calidad su formación educativa. Les invitamos a escuchar el programa completo a continuación.

<iframe id="audio_19395383" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19395383_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
