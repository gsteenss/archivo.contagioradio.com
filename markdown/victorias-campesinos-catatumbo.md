Title: Las victorias de los campesinos del Catatumbo
Date: 2017-11-05 12:05
Category: Movilización, Nacional
Slug: victorias-campesinos-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/catatumbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: areacucuta] 

###### [05 Nv 2017]

En el acuerdo que se logró en la madrugada de hoy entre los campesinos del Catatumbo y el gobierno nacional, uno de los puntos que se consideran claves es que se reactiva la discusión sobre la **sustitución de cultivos de uso ilícito y uno de los temas en la mesa es la implementación del punto sobre reforma rural integral** que es parte del acuerdo con las FARC y que tiene carácter regional en algunos de sus puntos.

El acuerdo que contempla IV puntos gruesos también incluye las garantías de seguridad para la región entre las que se encuentran las garantías para que los manifestantes vuelvan a sus territorios, así como evaluar la actuación de la Unidad Nacional de Protección en todo el territorio y el gobierno nacional tiene la tarea de convocar una reunión con las instituciones y el acompañamiento de la ONU para verificar estos avances.

Además en la concreción de las instituciones que tendrán parte en la Mesa de Interlocución y Acuerdos, los campesinos propusieron que haga parte de ella la CSVI de las FARC “dadas sus competencias de impulso, verificación en la implementación del acuerdo de paz”. El gobierno deberá hacer las consultas correspondientes para lograr ese acompañamiento.

### **Reforma Rural Integral y Plan de Sustitución de Cultivos de Uso Ilícito** 

En este punto el gobierno se comprometió a la realización de un plan de formalización de la tierra en coordinación con la Agencia Nacional de Tierras, una primera fase que contemplará los municipios de Tibu, Zulia, Sardinata, Zona Rural de Cúcuta, la Playa, Ábrego, Hacarí, zona rural de San Calixto, Teorema, Carmen y Convención, municipios que serán sometidos a un análisis y priorización conjunta.

En materia de sustitución de cultivos de uso ilícito se instalará una mesa de planificación para la implementación del PNIS en la región se firmará un acuerdo de voluntades de  inclusión a este programa, se concertará el cronograma y antes del 17 de diciembre se deberían firmar los acuerdos. Un modelo piloto se propone en la región de Caño Indio en el que se avanzaría en los proyectos comunitarios alternativos.

### **La agenda de la MIA Catatumbo** 

Según reza el acuerdo la próxima reunión se realizaría a finales de Noviembre e incluye la entrega del pliego de exigencias del campesinado. La agenda también considera la posibilidad de realizar un segundo encuentro social en el catatumbo en febrero de 2018 al que asistan todas las instituciones que han realizado proyectos en la región y han aportado al desarrollo equitativo.

La agenda también incluye la exigencia de la aprobación de varias obras. Por ejemplo se contempla la construcción de acueductos de la Campana-El Tarra y La Trinidad – Convención los cuales deberán tener estudios, diseños y presupuestos aprobados durante 2017. Además se regula la construcción del anillo vial para la paz en los municipios de Hacarí, San Calixto y El Tarra.

###### Reciba toda la información de Contagio Radio en [[su correo]
