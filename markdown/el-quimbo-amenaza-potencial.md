Title: Hidroeléctrica El Quimbo: "una amenaza potencial"
Date: 2019-08-13 11:46
Author: CtgAdm
Category: Ambiente, DDHH
Tags: Garzón, Hidroelectrica, Huila, Muro, Quimbo
Slug: el-quimbo-amenaza-potencial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/El-Quimbo-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Avispa Midia  
] 

Tras la audiencia realizada el pasado vienres 9 de agosto en el Consejo de Estado sobre la hidroeléctrica El Quimbo, nuevas informaciones confirmarían las irregularidades denunciadas por ambientalistas sobre la obra. En el evento, el Consejo aceptó la demanda contra el Proyecto y decretó una toma de pruebas que serán entregadas el 11 de diciembre para decidir de fondo sobre la hidroeléctrica; entre las pruebas podría estar el testimonio de un geógrafo que participó en la construcción, y afirma que por la obra, en Huila **"nunca se podrá dormir tranquilo"**.

### **En Contexto: La audiencia que llegó 5 años tarde**

El Quimbo es la primera Hidroeléctrica construída totalmente por el sector privado, según la Universidad Nacional, es el tercer proyecto de generación de energía más grande del país tras Hidrosogamoso e Hidroituango. Su construcción obligó a inundar más de 8 mil hectáreas de seis municipios en Huila (Garzón, Gigante, El Agrado, Paicol, Tesalia y Altamira); y según la misma Universidad, **afectó "las dinámicas bióticas de la zona"**, en tanto la vida acuática disminuyó por el represamiento del río Magdalena, al tiempo que la inundación de la zona significó el daño de ecosistemas de "bosques riparios, secos y premontanos".

Según información oficial, la Hidroeléctrica surtiría el 8% de la demanda de energía del país pero dada su capacidad de generación, el restante sería exportado a centroamérica y Ecuador. El Proyecto vio la luz en 2009, cuando el Ministerio de Ambiente y la Agencia Nacional de Licencias Ambientales (ANLA) otorgaron las licencias para iniciar la construcción, lo que permitió iniciar obras en 2010 y poner en marcha la operación en 2015.

Un año antes, en 2014, integrantes de la **Asociación**[ **de Afectados por el Proyecto Hidroeléctrico El Quimbo (ASOQUIMBO) y  el Centro de Estudios para la justicia social (Tierra Digna)** interpusieron una tutela ante el Consejo de Estado, pidiendo una medida cautelar para detener las obras. (Le puede interesar: ["La hidroeléctrica con los mismos problemas de Hidroituango"](https://archivo.contagioradio.com/el-quimbo-problemas-hidroituango/))  
]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

Aunque la demanda se radicó en 2014, solo 5 años después el Consejo determinó que la medida cautelar que se pedía no tenía validez porque la hidroeléctrica ya estaba construída; sin embargo, resolvió la realización de una audiencia sobre el tema que fue aplazada en dos ocasiones pero que se desarrolló el pasado viernes. En la audiencia participaron integrantes de la Asociación, Tierra Digna, abogados del consultorio jurídico de la Universidad Surcolombiana y representantes de distintas instituciones relacionadas con el Proyecto.

### **"La audiencia logró las expectativas que teníamos"**

**Miller Dusan, integrante de ASOQUIMBO y profesor de la Universidad Surcolombiana**, declaró que la audiencia "logró las expectativas que teníamos" porque solo había dos escenarios posibles: Que el Consejo de Estado negara la validez jurídico de los cargos presentados, o, lo que en este caso ocurrrió, que se reconocieran los mismos y se diera seguimiento al proceso.

Como explicó Dusan, los tres argumentos presentados para pedir la medida cautelar apuntaban a que el proyecto "había sido declarado inviable desde 1995", realizó una sustracción irregular del área donde se hizo el embalse, "que es protector de la Amazonía", y que no se hicieron los estudios de vulnerabilidad, sismicidad e impacto social que debieron hacerse, y en el tiempo que se requería para prevenir cualquier tipo de daño causado por el Proyecto.

El profesor destacó que esta **"es la primera demanda de nulidad de un proyecto de interés nacional estratégico"**, y resulta importante porque allí entran en juego todos los poderes: político, jurídico, económico y social. El Consejo también decretó que se realizarían pruebas, incluyendo la toma de testimonios, que serán presentadas el próximo 11 de diciembre; fecha en la que las partes en disputa recibiran los resultados, emitirán su concepto y finalmente, el alto tribunal definirá lo que ocurra con El Quimbo.

### **"El Quimbo es una amenaza potencial para la vida"**

El mismo 9 de agosto, mientras se desarrollaba la audiencia en el Consejo de Estado, una concentración se tomó la plaza principal de Garzón, Huila, para manifestarse contra El Quimbo. Allí participaron el Movimiento Rios Vivos Colombia y otras organizaciones ambientales. Para Dusan, el hecho fue relevante porque se mostró toda la fuerza del movimiento que trabaja por impulsar la transición energética de mano de las comunidades, y así superar la generación de energía mediante hidroeléctricas.

Un día después, el Diario del Huila publicó una entrevista con el **geómetra italiano, Alexander Arpino,** que participó en la construcción del proyecto durante cuatro años e hizo serias denuncias sobre el mismo. Arpino fue parte de la obra entre 2010 y y 2014 trabajando para el consorcio Impregilo, que asu vez, estaba a las órdenes de la interventoría realizada por **INGETEC (misma firma interventora de Hidroituango)**.

Según referencia el geómetra para el Diario, "desde el principio de la obra esta es una represa que nació mal, y cuando las cosas nacen mal, toda la vida serán mal. Nunca se podrá dormir tranquilo”. Ello, porque la base del muro de contención, o talud, se hizo sin los debidos requerimientos técnicos;  adicionalmente, las fallas en el estudio de suelos significó que la base del muro cediera constantemente. Y **mientras él advertía los problemas, dijo que INGETEC se encargaba de minimizarlos** mediante el cambio de los planos de construcción, y brindando soluciones parciales que no respondían a las necesidades técnicas reales.

A las denuncias por irregularidades en la construcción misma del Proyecto, Arpino sumó acusaciones por corrupción en las compras de materiales de la obra. Por todo lo dicho por el geómetra, así como las denuncias ya presentadas por las organizaciones sociales y ambientales sobre la hidroeléctrica, **el profesor Dusan concluyó que "El Quimbo es una amenaza potencial contra la vida".**

<iframe id="audio_39960482" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_39960482_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
