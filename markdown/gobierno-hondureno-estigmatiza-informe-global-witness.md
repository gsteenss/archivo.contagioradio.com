Title: Gobierno hondureño estigmatiza informe de Global Witness
Date: 2017-02-06 17:56
Category: DDHH, El mundo
Tags: asesinato Berta Cáceres, Global Witness, honduras
Slug: gobierno-hondureno-estigmatiza-informe-global-witness
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Justicia-Berta-Cáceres_AFP-e1466625702189.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### 6 Feb 2016 

Por medio de un comunicado, el Centro por la Justicia y el Derecho Internacional, CEJIL, aseguró apoyar el trabajo de la organización internacional Global Witness y rechazó “la **persecución y la campaña de desacreditación por parte del Gobierno hondureño**”, frente a las conclusiones que hace la ONG, mediante sus estudios en los que asegura que ese país es el más peligroso para defender la vida del planeta, teniendo en cuenta el crimen contra la lideresa indígena y ambientalista Berta Cáceres.

En el informe “Honduras: El lugar más peligroso para defender el planeta” Global Witness denuncia los más de 120 asesinatos de defensores y defensoras de la naturaleza que han ocurrido en el país desde 2010, y que, según las investigaciones y denuncias de las comunidades, se ha tratado de crímenes en los que **aparecen como responsables “las élites ricas y poderosas del país, entre ellas algunos miembros de la clase política**”, dice el informe.

La situación es tal, que el CEJIL denuncia también que la semana pasada, B**illy Kyte, delegado de Global Witness, tuvo que ser escoltado por la Alta Comisionada de Derechos Humanos, Silvia Lavagnoli,** de la Organización de Naciones Unidas, ONU, luego de comparecer en el programa de televisión Frente a Frente. Según lo señalado, allí Kyte, evitó posibles agresiones de la gente que lo esperaba fuera de la Corporación Televicentro.

La campaña de criminalización contra la ONG que ha emprendido el gobierno hondureño, incluso ha hecho se propusiera que Ministerio Público **detuviera a Kyte por el contenido del informe.** Asimismo, se ha indicado que la campaña de desprestigio ha incluido ataques contra organizaciones sociales hondureñas como el COPINH, CEPRODEH Y MILPAH.

“El Estado de Honduras debe reconocer el rol que juegan estas personas en la democracia y abstenerse de hacer comentarios estigmatizantes. Ante la contundencia del informe lo procedente es investigar de forma inmediata los hechos que se denuncian y no criminalizar a las personas que hacen dichas denuncias”, expresa el CEJIL.

Marcia Aguiluz, directora del Programa para Centroamérica y México de CEJIL, hizo un llamada de atención al Estado Hondureño, y **aprovechó para solicitar avances respecto a la investigación del asesinato de la lideresa Berta Cáceres,** pues la familia no ha podido acceder a la investigación, y tampoco se sabe quiénes fueron los autores intelectuales de hecho.

###### Reciba toda la información de Contagio Radio en [[su correo]
