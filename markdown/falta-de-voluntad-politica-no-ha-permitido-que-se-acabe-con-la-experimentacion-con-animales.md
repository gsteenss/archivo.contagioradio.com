Title: Falta de voluntad política no ha permitido que se acabe con la experimentación con animales
Date: 2015-02-03 18:47
Author: CtgAdm
Category: Animales, Voces de la Tierra
Slug: falta-de-voluntad-politica-no-ha-permitido-que-se-acabe-con-la-experimentacion-con-animales
Status: published

##### [Foto: www.petalatino.com]

<iframe src="http://www.ivoox.com/player_ek_4033834_2_1.html?data=lZWglZ2XeI6ZmKiakp2Jd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRicXpwtfR0ZC0qYa3lIqvk8aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Eduardo Peña] 

La sección Cuarta del Consejo de Estado invalidó una decisión que se había tomado en noviembre de 2013 en la que **se prohibía al científico Manuel Elkin Patarroyo, cazar y usar monos del Amazonas** para sus investigaciones sobre la vacuna contra la Malaria.

El fallo, que no permitía experimentación con animales monos, fue emitido por el Tribunal Administrativo de Cundinamarca, y cancelaba definitivamente los permisos otorgados a la Fidic, argumentando que antes y durante estos procesos los animales son víctimas de constantes torturas y maltratos.

Frente a esta decisión, Eduardo Peña, coordinador de campañas de Animal Defenders International, dijo que **las organizaciones animalistas esperarán la decisión de la sección quinta del Consejo de Estado**. Además,  la ADI  fortalecerá su campaña y realizarán un plantón el próximo miércoles 11 de febrero a las 11 de la mañana frente al Palacio de Justicia.

El Coordinador de campañas de la ADI, afirma que en Colombia se han trabajado otras alternativas para acabar con la experimentación animal, sin embargo, asegura que el problema es **“la falta de voluntad política y administrativa de los gobiernos para implementar otros métodos, -y añade-, Colciencias podría incentivar este tipo de investigaciones… estamos muy quedados”.**

Cabe recordar que el año pasado, en Brasil, se reconocieron 17 métodos alternativos para la no experimentación con animales en los laboratorios, debido a que existen múltiples estudios e investigaciones con cámaras ocultas donde se evidencia como son torturados los animales usados con fines científicos.
