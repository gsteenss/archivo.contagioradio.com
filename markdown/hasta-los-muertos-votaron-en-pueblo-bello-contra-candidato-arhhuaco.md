Title: Hasta los muertos votaron en Pueblo Bello, Cesar
Date: 2015-10-30 16:18
Category: Uncategorized
Tags: arhuacos, Comunidad Arhuaca, Consejo Nacional Electoral, Denuncia electoral, Elecciones en Pueblo Bello, Fraude electoral, Gnneco Cerchar, MOE, Partido MAIS
Slug: hasta-los-muertos-votaron-en-pueblo-bello-contra-candidato-arhhuaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ELECCIONES.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpilon 

<iframe src="http://www.ivoox.com/player_ek_9223001_2_1.html?data=mpeflZWUdY6ZmKiakp2Jd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmqcbg1saPsNDnjNLix9fYs9Sf19Thw9fTsozZz5C918rGsNCfo8rZztSJdpOfxNTb1tfFb8TVz8mah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rodolfo Vega,  MAIS] 

###### [30 oct 2015]

Después de una semana de las elecciones en Pueblo Bello, César, la comunidad Arhuaca que representa el 80% de la población de este municipio **sigue a la espera de que el voto de la comunidad por Saúl Tobías Mindiola, candidato perteneciente a la comunidad indígena, sea respetado** y sea designado como alcalde, esto en busca de proteger, defender sus territorios sagrados y demostrar que los indígenas también pueden gobernar.

Rodolfo Vega, presidente del partido Movimiento Alternativo Indígena y Social, MAIS, indica que “a la fecha el parte de l**o que está sucediendo en el municipio es bastante lamentable**”, ya que inicialmente “es un imposible que hoy esté ganando otro candidato”, y que se tiene conocimiento de “una serie de irregularidades en la registraduría” y así se “imponen las maquinarias políticas del Cesar”.

Certificados electorales de personas fallecidas, transhumancia, porque, si bien se excluyeron muchas cédulas, al final se les permitió votar y además hay manipulación de las bolsas que llegaron de algunos corregimientos y del mismo casco urbano del municipio, son las pruebas que respaldan las denuncias de MAIS.

son **las irregularidades que la comunidad ha encontrado en las elecciones a alcalde**, además que “se demostró la parcialidad del registrador municipal”, de lo cual hay evidencias que lo “pone **la casa Gnecco del César**”, por lo cual no hay garantías ya que “patrocinan al otro candidato que a la vez e patrocinado por la misma administración actual de Pueblo Bello”, afirma Vega.

Así los habitantes de Pueblo Bello con la “dignidad en alto” **están en proceso de presentar las pruebas que tienen ante los entes judiciales** “para reclamar lo que el pueblo ganó”, sin embargo, Rodolfo Vega, afirma que  por parte del Consejo Nacional Electoral (CNE), su respuesta frente al caso es que “no pueden pronunciarse, que todo es en los espacios de escrutinio”.

Vega afirma que Juan Francisco Villazon Tafur, candidato del partido de la U, y virtual favorecido por las irregularidades denunciadas. es el candidato del alcalde actual y es respaldado por la "*Casa Gneccco Cerchar*", una de las familias que durante años ha ostentado el poder en esa región y que también ha sido acusada y condenada por constreñimiento al elector y actos administrativos irregulares en sus posiciones en la alcaldía de Santa Marta y la gobernación del Cesar.

Es por esto que **la comunidad pide que la ONU, el Consejo Nacional Electoral y a la misma Registraduría** que se brinden las garantías políticas y jurídicas necesarias “de imparcialidad sobre los funcionarios locales que permitan hacer una elección transparente en este municipio”, afirma el presidente del partido MAIS.
