Title: En el Bajo Cauca persiste el reclutamiento forzado de niños
Date: 2019-12-29 21:28
Author: CtgAdm
Category: DDHH, Nacional
Tags: Bajo Cauca, Niñas, niños, reclutamiento forzado, Tarazá
Slug: en-bajo-cauca-persiste-el-reclutamiento-forzado-de-ninos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/reclutamiento-forzado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

A través de redes sociales se denuncia que hombres armados han llegado a algunas veredas de **Tarazá y Caucana** en el **Bajo Cauca** para censar y reclutar a niños, fuentes cercanas a Contagio Radio han manifestado que el reclutamiento en esta parte del país ha sido constante pero que las denuncias recientes no han sido confirmadas por líderes o población de la zona.

> Urgente:  
> Hace unas días apareció sobre el río Nechí una cabeza cortada, al lado un aviso de los Caparrapos. Hoy me llaman campesinos y envían fotos: en El Doce municipio de Taraza, y en La Caucana hay grupo de 20 hombres armados censando niños y se van a llevar a varios de ellos
>
> — Ariel Ávila (@ArielAnaliza) [December 29, 2019](https://twitter.com/ArielAnaliza/status/1211308277319000064?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según **Oscar Yesid Zapata, de la Fundación Sumapaz** en el departamento de Antioquia, la organización no ha conocido esta denuncia concreta difundida en redes, pero si han conocido del caso sucedido el 27 de diciembre en una vereda del corregimiento de La Caucana donde la estructura paramilitar de *Los Caparrapos* quiso reclutar a un joven, pero debido a la presión de la comunidad el joven fue dejado en libertad. Ver: [Alerta en el Bajo Cauca por el asesinato de tres mineros y el reclutamiento de menores](https://archivo.contagioradio.com/alerta-en-el-bajo-cauca-por-el-asesinato-de-tres-mineros-y-el-reclutamiento-de-menores/)

La Defensoria del Pueblo ha advertido de varios hechos de violencia generalizada en el Bajo Cauca, zona en la que se han presentado enfrentamientos entre ***Los Caparrapos*** *y* las autodenominadas *Autodefensas Gaitanistas de Colombia*, grupos que se disputan el control territorial, debido a la alta presencia del cultivos de coca y al ser un punto clave para las rutas del narcotráfico con fácil conexión con otros departamentos que limitan con Antioquia. Ver: [En el Bajo Cauca Antioqueño la violencia se está tomando el poder](https://archivo.contagioradio.com/bajo-cauca-antioqueno-violencia-gobierno/)

### **Alertas tempranas en el Bajo Cauca** 

Los hechos de violencia que han sucedido en el Bajo Cauca han sido advertidos en alertas tempranas de la **Defensoria del Pueblo** entre ellas 009 - 027 - 031 de 2018 y la 003 y 020 de 2019 donde han informado los constantes hechos de violencia que suceden en esta parte de Colombia y que han sido inadvertidos por el gobierno nacional porque no han tomado control de la zona, ni ha protegido a la comunidad, indica Zapata. Ver: [El líder social Humberto Londoño, es asesinado en Tarazá Bajo Cauca](https://archivo.contagioradio.com/el-lider-social-humberto-londono-es-asesinado-en-taraza-bajo-cauca/)

A pesar de ser una de las zonas más militarizadas del país y donde se desarrolla la Operación Aquiles y la Operación Agamenón II, las estructuras paramilitares mantienen el control del territorio

### **El Estado, un ausente en el Bajo Cauca** 

Durante años se han mantenido el reclutamiento forzado contra niñas, niños y adolescentes en el Bajo Cauca, y esto, según la fundación Sumapaz se debe a la ausencia del Estado en esta zona del país donde la pobreza y la baja inversión social facilita las condiciones para que los grupos armados se lleven a los niños y niñas a formar parte de las estructuras armadas. A esto se suma la falta de infraestructura adecuada para colegios y escuelas, la alta tasa de deserción escolar y la nula de protección de los derechos de la niñez.

Según el informe [“Una guerra sin edad”](http://www.centrodememoriahistorica.gov.co/descargas/informes2018/una_guerra-sin-edad.pdf). del Centro Nacional de Memoria Histórica, el Bajo Cauca esta dentro de las 20 regiones con mayor reclutamiento de niños y niñas.

Para finalizar, el defensor de derechos humanos indicó que el gobierno debería general un plan estratégico que tenga por objetivo las comunidades y no la militarización, de otra parte manifestó que durante 2019 el Bajo Cauca fue el lugar donde se asesinó al mayor número de líderes sociales, con un total de 11 personas, según información de observatorio de la Fundación Sumapaz y Fundación Jurídica Libertad.

### **Panfletos en Tarazá** 

La población de Tarazá ha denunciado durante los últimos días la circulación de un panfleto del grupo paramilitar Los Caparrapos que prohíbe a los motociclistas el uso casco en el pueblo, e igualmente hablar con personas foráneas y hacerse en esquinas y en lugares oscuros. Conozca más sobre esta información: [Mediante panfleto que circula en Tarazá, ilegales prohíben el tránsito con cascos](https://analisisurbano.org/mediante-panfleto-que-circula-en-taraza-ilegales-prohiben-el-transito-con-cascos-en-el-pueblo/58479/) en el pueblo

-   Ver: [Grupos ilegales se disputan el Bajo Cauca Antioqueño ante la ausencia del Estado](https://archivo.contagioradio.com/grupos-ilegales-se-disputan-el-bajo-cauca-antioqueno-ante-la-ausencia-del-estado/)

