Title: Habitantes de Argelia, Cauca, se cansaron de ser "trinchera" de la policía
Date: 2015-06-23 13:06
Category: Otra Mirada, Paz
Tags: Argelia, Cauca, Cese al fuego, el mango, el plateado, Frente Amplio por la PAz, paz, policia, puerto rico
Slug: la-poblacion-de-argelia-cauca-se-canso-de-ser-trinchera-de-la-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Indígenas-del-pueblo-Nasa-desmontan-una-trinchera-del-ejército.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La pluma 

<iframe src="http://www.ivoox.com/player_ek_4678669_2_1.html?data=lZukmpuafY6ZmKiakp6Jd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLWytnO0NnJt4zYxpCu1MzJsMrVhpewjajFucTVhpewjdjJb8TVz9jO1NTSb8XZjNjS1JDYtsrixM2ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Orlando, Habitante de El Mango] 

###### [23 Jun 2015] 

En un acto de resistencia y desobediencia civil, los habitantes del **municipio El Mango**, en Argelia, Cauca, decidieron **sacar la estación de policía del casco urbano del corregimiento**. Afirman que se han recrudecido los enfrentamientos entre la guerrilla y las fuerzas militares, y que hay **hechos de guerra que ponen en riesgo la vida de la población civil.**

Los pobladores de El Mango cuentan que durante el **cese al fuego** unilateral decretado por las FARC "*se pudo volver a dormir tranquilo y las cosas estaban en calma, pero como eso se acabó, esto es un infierno de nuevo: cilindros, plomo, zozobra, es muy duro esto que se está viviendo. Debido a todo este problema, la gente tomó la decisión última de que la policía salga de aquí*", asegura Orlando (Contagio Radio se reserva el nombre completo del habitante por su seguridad).

Es por esto que el lunes 22 de junio, de manera pacífica los habitantes del corregimiento ingresaron a la estación. Hablaron con los policías que se encontraban allí y les aseguraron que no iban a agredir a nadie, pero que debían irse del lugar. "*Al principio no querían dejar entrar la gente, hubo empujones, palabras cruzadas, y luego poco a poco se fue ingresando. Se habló mucho. Lo que no queríamos era lastimar a nadie. Se trató al máximo de que eso no fuera a pasar*". Durante el día se acercaron también habitantes de El Plateado, Puerto Rico, Cinaí y La Belleza, para acompañar el desalojo, que empezó a hacerse efectivo este martes en horas de la tarde.

Esta no es la primera vez que la comunidad de Argelia intenta sacar a la fuerza pública del corregimiento. Sin embargo, en ocasiones anteriores, la policía les pedía no acercarse a menos de 10 metros de la estación, y posteriormente los dispersaba con gases lacrimógeno. "*Nos sacaron a plomo y gases, entonces la decisión ahora fue hacerlo desde adentro*".

Los habitantes aseguran que, lejos de lo que afirman las fuerzas militares, las estaciones no se encuentran en la zona para proteger a los habitantes. "*Más protegidos se sienten ellos \[la policía\] con la gente ahí, porque saben que no les van a hacer nada. Ellos no vienen a cuidar a nadie, ellos vienen es a cuidarse de los ataques, y  por eso se escudan dentro de nosotros mismos*".

<div>

Durante la noche del lunes, después de la toma, aviones Fantasma del Ejercito sobrevolaron el municipio. Los habitantes presumen que su intensión era sembrar temor. "*Como aquí ha pasado tantas veces que caen los antimotines, la decisión es no salirse de ahí, porque han pasado tantas cosas, tantos atropellos, tantos sustos, tanta angustia*".

La población está alerta ante la posibilidad de que el Ejercito responda con fuerza, pero la decisión está tomada. "Esperemos que no se presenten incidentes", finaliza Orlando.

Durante las horas de la tarde de este martes, los pobladores de los corregimientos han realizado una caravana para acompañar a los efectivos policiales hasta el casco urbano de Argelia y han denunciado que durante todo el recorrido se han visto los ametrallamientos desde naves militares que no dejan de atemorizar a la población civil.

</div>
