Title: Tres miembros de la Colombia Humana han sido asesinados en menos de dos meses
Date: 2018-07-05 15:52
Category: DDHH, Nacional
Tags: Ana María Cortés, Colombia Humana, Gustavo Petro, Jorge Rojas, lideres sociales
Slug: tres-miembros-de-la-colombia-humana-han-sido-asesinados-en-menos-de-dos-meses
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/WhatsApp-Image-2018-07-05-at-3.45.50-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Casanare Humana] 

###### [05 Jul 2018] 

Con María Cortés, son tres las personas integrantes de la Colombia Humana que han sido asesinadas en el país. De acuerdo con Jorge Rojas, integrante de ese movimiento, podría ser el inicio de una “operación exterminio” que tiene como objetivo “**recuperar las zonas en donde perdieron las elecciones quienes están al servicio de la guerra** y quieren hacer trizas el acuerdo de paz”.

Rojas enfatizó en que estos actos de violencia se vienen dando en medio de una situación crítica para el país como lo es el cambio de gobierno, en donde por un lado esta Santos con el mandato que termina y al que “poco le importa lo que está ocurriendo”, y el entrante que no tiene responsabilidad **pero que tampoco tiene interés en pronunciarse frente a estos asesinatos”.**

### **Los líderes de la Colombia Humana están en riesgo** 

Gabriel Muñoz Muñoz, Evelia Atencia y María Cortés son los tres integrantes de la Colombia Humana que han sido asesinados desde que ese movimiento inició sus actividades políticas. Para Rojas, los territorios en donde fueron asesinados comparten características en común, la primera que los territorios han sido golpeados fuertemente por el conflicto armado, la segunda que eran lugares en donde el movimiento social o los liderazgos eran muy fuertes y la tercera, que son escenarios en donde gano la propuesta política de la Colombia Humana.

María Cortés, la víctimas más reciente de la violencia contra este movimiento, fue asesinada ayer cuando se encontraba en una cafetería en el municipio de Cáceres, Antioquia. Cortés era la encargada de coordinar la campaña electoral de la Colombia humana y previamente **fue líder social del territorio, además desempeñó labores de ayuda a las víctimas de Hidroituango**.

Evelia Atencia era docente en Maicao, Guajira. Fue asesinada el pasado 23 de junio por dos hombres que se movilizaban en una moto, mientras ella se encontraba en el barrio La Floresta. **Evelia era integrante del Sindicato de Trabajadores en esta región y acompañaba las labores de campaña de la Colombia Humana**.

Gabriel Muñoz Muñoz fue asesinado el 26 de mayo, en la vereda Las Águilas en el departamento del Huila, cuando hombres armados lo abordaron antes de ingresar a su vivienda y le dispararon. Muñoz era el **coordinador de la campaña de Colombia Humana en el municipio La Argentina y estaba habilitado como testigo electoral**.

A esos hechos se suman la aparición de panfletos en regiones del Cauca y Tolima, en donde se amenaza de muerte a quienes apoyaron la campaña de Gustavo Petro y Ángela María Robledo a la presidencia de Colombia. (Le puede interesar: [“Defensores de DDHH del Tolima aminazados ante intención de voto por Gustavo Petro"](https://archivo.contagioradio.com/defensores-de-ddhh-del-tolima-amenazados-ante-intencion-de-voto-por-gustavo-petro/))

Ante esta situación, el dirigente del movimiento Colombia Humana, Gustavo Petro, convocó a la ciudadanía a una movilización el próximo **7 de agosto, durante la posesión del presidente Iván Duque** para denunciar no solo los actos de violencia en contra de los integrantes de esta plataforma, sino de todos los líderes y defensores de derechos humanos que han sido asesinados en el país.

<iframe id="audio_26913458" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26913458_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
