Title: Movimientos Sociales de Haití y Brasil se alian en contra de la Minustah
Date: 2015-05-26 14:07
Category: El mundo, Otra Mirada
Tags: Brote de cólera cascos azules Pakistán, Cascos azules de Bolivia abandonan Haití, Minustah, Movimientos sociales de Haití se movilizan en contra de los cascos azules
Slug: movimientos-sociales-de-haiti-y-brasil-se-alian-en-contra-de-la-minustah
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Sin-título2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:JorgeBarrena 

Todo tipo de **movimientos sociales y sindicales de Brasil y Haití** se dieron cita el 22 y 23 de Mayo en Sao Paulo para iniciar una **Campaña en Solidaridad con Haití y para exigir el fin de la misión de Naciones Unidas en la isla.**

En los encuentros se decidió **luchar contra la militarización** del país que está **impidiendo la autodeterminación de los Haitianos y el libre desarrollo**, además de servir a **intereses de carácter imperialistas** basados en la posición geoestratégica de la nación.

Actualmente, la **misión la lidera Brasil** y se **renueva anualmente cada mes de Octubre. E**s por ello que se ha iniciado la Campaña en contra de que la misión continúe y así poder presionar de cara a la próxima reunión de los miembros de Naciones Unidas.

Desde **2004,** Naciones Unidas hace presencia con la Misión de estabilización de Naciones Unidas en Haití **MINUSTAH** , con el objetivo de **estabilizar el país políticamente y de permitir unas elecciones libres.**

En el mismo, año **EEUU apoyó el derrocamiento** del presidente electo democráticamente Jean-Bertrand Aristide. Proveniente de la Teología de la Liberación y cercano a gobiernos y posturas políticas como las de Cuba y Venezuela, se convirtió en un aliado incomodo para el imperialismo Norteamericano.

En **2010 Haití fue sacudido por un terremoto que devastó al país** dejando un rastro de 316.000 muertos y 1,5 millones de personas sin hogar. Actualmente es conocido como la  catástrofe humanitaria más grave de la historia.

Fue entonces cuando la MINUSTAH engordó su contingente para trabajar en labores humanitarias, convirtiéndose en una de las más **polémicas** misiones humanitarias de la ONU debido al contagio de **Cólera por parte de los Cascos Azules Pakistaníes y a la represión ejercida por la misión** en contra de las protestas por la gestión de la catástrofe de la población civil.

Actualmente, Haití es el **país más pobre de América y más del 70% de la población vive en condiciones de extrema pobreza; **la inestabilidad política está a la orden del día y toda la nación está militarizada. Su futuro depende de las ayudas humanitarias de la comunidad internacional, debido a que su economía quedó gravemente afectada en el terremoto.
