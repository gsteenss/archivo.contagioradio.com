Title: Aumenta a 179 el número de heridos y a 145 los detenidos en la Minga Nacional
Date: 2016-06-04 19:13
Category: Movilización, Paro Nacional
Tags: Balance de Derechos Humanos, Minga Nacional
Slug: aumenta-a-152-el-numero-de-heridos-y-a-145-los-detenidos-en-la-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/la-delfina-buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congresodelospueblos] 

###### [04 Jun 2016] 

El balance de la Minga muestra unas cifras muy desalentadoras que, según los voceros de la Cumbre Agraria, dan cuenta de que el gobierno sigue mintiendo cuando habla de respeto a la protesta social. **Tan solo en el quinto día de la movilización las cifras son de 152 personas heridas, 152 detenidos y 3 asesinados.**

En cuanto a las personas detenidas la Cumbre Agraria desglosa las cifras y el recuento es que son 122 hombres, 4 mujeres y 15 menores de edad. De los 145 detenidos, la gran mayoría hace parte de un grupo de personas detenidas en Berlin, Santander el pasado 3 de Junio. Del total de las personas capturadas masivamente **solamente han sido judicializadas 3 de ellas** con cargos que tienen que ver con el bloqueo de  las vías. Sus abogados temen que las acusaciones sean por terrorismo.

En cuanto a otros tipos de agresiones la Cumbre Agraria asegura  que**[hubo 8 amenazas, 21 acciones de inteligencia ilegal y 14 atentados](https://archivo.contagioradio.com/un-indigena-nasa-muerto-y-otro-mas-herido-en-la-minga-agraria-en-cauca/)** contra los participantes de la Minga en todo el territorio nacional. Sin embargo la cifra podría aumentar debido a que se conoció una orden del ministerio de defensa para atacar a los manifestantes que se encuentran en la vía panamericana en el departamento de Cauca y Nariño.
