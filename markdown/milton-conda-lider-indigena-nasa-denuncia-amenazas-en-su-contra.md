Title: "En medio del miedo la comunidad indígena en vez de retroceder se ha reforzado", Milton Conda
Date: 2020-06-04 18:05
Author: CtgAdm
Category: Actualidad, Otra Mirada
Slug: milton-conda-lider-indigena-nasa-denuncia-amenazas-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/milton-conda.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/milton-conda-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/amenaza-milton-conda.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En medio del aislamiento preventivo en todo el país, la situación en los territorios empeora, continúan las [amenaza](https://archivo.contagioradio.com/hermes-loaiza-secretario-jac-de-pueblo-nuevo-fue-asesinado-en-florida-valle/)s contra líderes y defensores, especialmente quienes defienden los territorios indígenas, y **a pesar del riesgo constantes por su vida, no detienen sus luchas y al contrario las fortalecen.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Milton Conda, delegado por parte de las organizaciones indígenas, para la Comisión Nacional de Territorios Indígenas,** defensor de derechos humanos y de la tierra, al sur del Valle del [Cauca](https://archivo.contagioradio.com/ejercito-y-narcotrafico-verdugos-de-la-sustitucion-de-la-coca/), ha denunciado más de 3 amenazas en su contra en los últimos meses.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante años Conda ha denunciado las afectaciones sociales culturales y ambientales generadas por los grupos armados, así como la defensa de la madre tierra en contra de las políticas extractivistas, especialmente del **mega proyecto de la transversal Pacífico Orinoquia .**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pasado 19 de mayo, reportó la llegada de un panfleto amenazante de parte las disidencias de Farc, exactamente la autodenominada columna móvil Dagoberto Ramos; en el que señalaban que harían tareas de **"limpieza social", contra todas las personas que no dejan progresar el desarrollo del megaroyecto.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":85058,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/amenaza-milton-conda.jpg){.wp-image-85058}  

<figcaption>
*En el panfleto señalan: "al señor Milton Conda, delegado de la Comisión Nacional de Tierras de la ONIC, (...) que deje de joder con los controles territoriales (...) y que deje de hablar de defender la tierra (...) le manifestamos que se quede callado o se vaya del territorio de lo contrario que se atenga a las consecuencias, porque lo declaramos objetivo militar"*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Ante las amenazas y acciones que se han propagado hacia los pueblos indígenas Conda señaló, *"**históricamente las comunidades indígenas se han encargado de la organización territorial**, así como de la armonía en el país, esto ha permitido la protección de territorios sagrados".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Hemos hecho una lucha históricamente por un buen vivir que contempla defender el ambiente, los paramos, la madre tierra; **acciones que nos han puesto en riesgo, porque es algo que no gusta a los interventores internacionales,** que tienen intereses en nuestros territorios"
>
> <cite>**Milton Conda | Comisión Nacional de Territorios Indígenas**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Frente a los intereses Estatales que cada día se vuelven mas obvios, el líder señalo, ***"es preocupante que el desarrollo ponga encima la vida de las comunidades indígenas y de los territorios sagrados*** y donde nos dan dos opciones, callamos y nos vamos, o nos silencian a bala".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Lo que detiene a otros, a los indígenas los hace movilizarse aún más: Milton Conda

<!-- /wp:heading -->

<!-- wp:paragraph -->

Milton Conda, destacó qué **en medio de la pandemia se han venido intensificando las agresiones** ***"por parte no solamente de los grupos ilegales sino también de los actores armados** del Estado en contra de las comunidades indígenas que defienden el territorio"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y añadió que a pesar de esto **la Guardia Indígena a intensificado su presencia con 1200 puestos indígenas de control,** **"un esfuerzo por parte de los integrantes de la comunidad por la defensa de la vida, y en contra de las acciones violentas".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Poniendo en evidencia que a pesar de las amenazas y las diferentes acciones que ponen en riesgo la vida de quienes están en estos controles territoriales, **la comunidad [indígena](https://www.cric-colombia.org/portal/kiwe-thegnas-la-lucha-que-nunca-descansa/) en vez de retroceder se ha reforzado,** *"siendo pie de fuerza en contra de las acciones que arremeten contra la estabilidad de la comunidad y de la madre tierra".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Le puede interesar ver: Otra Mirada así es la situación del movimiento social desde los territorios en medio de la pandemia*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F270404227485635%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
