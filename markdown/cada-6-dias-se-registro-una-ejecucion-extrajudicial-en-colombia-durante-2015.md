Title: Ejecuciones extrajudiciales persisten en Colombia
Date: 2016-09-07 13:12
Category: DDHH, Nacional
Tags: crímenes de estado, Ejecuciones Extrajudiciales, ejercito, falsos positivos, policia
Slug: cada-6-dias-se-registro-una-ejecucion-extrajudicial-en-colombia-durante-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11692702_1151561871525593_4408124755510560296_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pulzo 

###### [7 Sept 2016]

Durante el 2015 se presentaron **65 ejecuciones extrajudiciales en Colombia, es decir que una persona cada 6 días fue asesinada a causa de violencia estatal ilegítima**, así lo concluye el más reciente informe de la Coordinación Colombia - Europa - Estados Unidos, (CCEEU)  en el que además constatan nuevas modalidades de encubrir y llevar a cabo estos crímenes de Estado.

El informe analiza esos 65 casos, de los cuales el **30% aparecen directamente vinculados a la situación de conflicto armado, mientras que el 70% se presentaron por fuera de este contexto**. En ese sentido, 16 casos corresponden a víctimas que fueron presentadas como “falsos positivos” y 39 a otras modalidades de ejecuciones, “que de no ser adecuadamente controladas y sancionadas, podrían convertirse en prácticas sistemáticas en la nueva etapa del post conflicto”, dice el informe.

Los **jóvenes que viven de cerca conflictos sociales y ambientales pertenecientes a comunidades indígenas y campesinas** siguen siendo las principales víctimas de estos crímenes. En el departamento de Bolívar se registraron 9 casos de ejecuciones extrajudiciales, en Atlántico 6, Antioquia 6, Bogotá 5, Cauca 6, y Caquetá 4.

Sin embargo, esta realidad analizada y hallada por la CCEEU, continúa sin ser reconocida por el Ministerio de Defensa, que ha expresado que no conocen nuevos casos de ejecuciones extrajudiciales perpetradas por agentes estatales, frente a ellos la Coordinación, afirma que “los nuevos casos de ejecuciones extrajudiciales permiten evidenciar modalidades que buscan ocultar o encubrir la responsabilidad de los perpetradores tanto en la policía como en el ejército”.

“A diferencia de las ejecuciones extrajudiciales de la época de la Seguridad Democrática el Ejército ya no es la entidad responsable de la mayoría de estas ejecuciones (registra 23 víctimas de las cuales 13 corresponden a la modalidad de falsos positivos) sino **la Policía Nacional, con 43 víctimas** (de las cuales 5 corresponden a la modalidad de falsos positivos)”, señala el informe.

Frente a esas cifras y conclusiones la CCEEU, presentó una serie de recomendaciones al Estado Colombiano para superar las fallas sistémicas que han ocasionado y siguen propiciando estos crímenes estatales, en ese sentido se insta a la necesidad de **transformar la Fuerza Pública** “para avanzar en la garantía de una paz sin nuevas víctimas de falsos positivos ni de otras modalidades encubiertas de ejecuciones extrajudiciales”, indica el informe.

<iframe id="doc_56825" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/323273173/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
