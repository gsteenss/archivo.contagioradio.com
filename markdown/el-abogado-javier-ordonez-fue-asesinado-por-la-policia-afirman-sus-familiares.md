Title: El abogado Javier Ordóñez fue asesinado por la Policía afirman sus familiares
Date: 2020-09-09 14:40
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Abuso policial, Dilan Cruz, Javier Ordóñez, Pistola taser
Slug: el-abogado-javier-ordonez-fue-asesinado-por-la-policia-afirman-sus-familiares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Policias-agreden-a-Javier-Ordonez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la madrugada de este miércoles se conoció un **caso de abuso policial en el que dos uniformados, sometieron a Javier Ordóñez, propinándole descargas eléctricas con una pistola de electrochoque y lo redujeron con la técnica de estrangulamiento,** pese a que el ciudadano estaba neutralizado y suplicaba a los uniformados para que pararan la agresión. (Lea también: ["No puedo respirar”](https://archivo.contagioradio.com/gerorge-floyd-respirar/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ReynaldVillalba/status/1303742383893422080","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ReynaldVillalba/status/1303742383893422080

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según versiones, Javier Ordóñez se encontraba departiendo con unos amigos y fue requerido por la Policía por presuntamente estar ingiriendo licor  en espacio público.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una de las personas que acompañaba al abogado en el momento del procedimiento, señaló que luego del maltrato que se evidenció en el video, **los policías lo trasladaron a un CAI cercano, donde lo habrían seguido golpeando y minutos después fue conducido a un centro médico donde «*falleció por los golpes*»,** según el reporte médico que recibió el testigo quien era amigo de Ordóñez.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Javier Ordóñez, era padre de dos hijos de 10 y 14 años, y según Elvia Bermúdez, tía del abogado, fue asesinado por la Policía.** «*Le estaban pegando en el suelo y poniendo corriente, luego lo subieron a una patrulla y dijeron que se lo llevaban para la URI de La Granja*» señaló la tía de la víctima; quien agregó que a su sobrino «*lo mató la Policía*». (Lea también: [Abuso de la Fuerza Pública ha dejado 30 homicidios en medio de la movilización social en cuarentena](https://archivo.contagioradio.com/abuso-de-la-fuerza-publica-ha-dejado-30-homicidios-en-medio-de-la-movilizacion-social-en-cuarentena/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El uso de armamento «no letal» por parte de la Fuerza Pública

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las armas taser con las que fue sometido Javier Ordóñez, pese a ser categorizadas como «no letales» pueden generar la muerte con una utilización inadecuada o excesiva.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1303702369713221632","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1303702369713221632

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según expertos médicos, un contacto prolongado con el arma puede producir asfixia por contracción sostenida de los músculos de la respiración que puede llevar a paros cardiorrespiratorios. Asimismo, descargas  que excedan la intensidad que el cuerpo tolera, pueden hacer que el corazón altere su ritmo y se produzca una fibrilación ventricular llegando a causar en algunos casos la muerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En relación con la utilización de estas armas se ha pronunciado el Comité contra la Tortura de la Organización de Naciones Unidad -ONU-, que mediante un [informe](https://www.refworld.org/cgi-bin/texis/vtx/rwmain/opendocpdf.pdf?reldoc=y&docid=492fb69e2) señaló que **«*el uso de esas armas provoca un dolor intenso, constituye una forma de tortura, y en algunos casos, puede incluso causar la muerte*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este hecho, **revive el asesinato del joven Dilan Cruz a manos de un agente del ESMAD en el mes de noviembre del año pasado, hecho sobre el cual aún no hay condenas, ni responsables, pese a que el uniformado que accionó el arma que hirió de muerte a Dilan; fue plenamente individualizado.** Este caso fue recientemente sometido a la Justicia Penal Militar, lo que según varios sectores será garantía de que el hecho quede en la impunidad. (Le puede interesar: [Caso de Dilan Cruz es enviado a la Justicia Penal Militar](https://archivo.contagioradio.com/caso-de-dilan-cruz-es-enviado-a-la-justicia-penal-militar/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/sandraborda/status/1303724108606251009","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/sandraborda/status/1303724108606251009

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
