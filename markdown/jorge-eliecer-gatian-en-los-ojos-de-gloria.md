Title: "La memoria de mi papá es muy revolucionaria": Gloria Gaitan hija de Jorge Eliecer Gaitán
Date: 2018-04-09 16:50
Category: Entrevistas, Política
Tags: 9 de abril, Día de las dignidad de las víctimas, Jorge Eliecer Gaita
Slug: jorge-eliecer-gatian-en-los-ojos-de-gloria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/gloria-gaitan-e1460486016682.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 Abr 2016] 

Mi padre fue un “luchador por cambiar la democracia representativa por la democracia directa”, relata Gloria Gaitán, hija de Jorge Eliecer Gaitán, caudillo liberal, y referente político, asesinado el 9 de Abril de 1948. Gloria relata que **en su casa había que levantarse a las 6 de la mañana, “llueva o truene”, hacer gimnasia y comer alimentos integrales** para cuidar la salud. Además **estaba prohíbo mentir.**

Jorge Eliecer entraba a la plaza de toros y todo el público se paraba a aplaudir y la banda tocaba el himno nacional en su versión porro, “la versión de los gaitanistas (…) cuando terminaba la corrida sacaban en hombros al torero y a mi papá”, **relata Gloria que creció entre las “salas de la resistencia” y la plaza pública al lado de Jorge y de Amparo** “Ella, la noche anterior soñó que lo mataban” por eso lo invitó a encuadernar la constitución y tomarse el poder, conversa, antes **rememorar cómo vivió ella el 9 de Abril.**

Jorge, esposo de Amparo, hijo de la profesora Manuela Ayala y del librero liberal Eliécer Gaitán Otálora, **de familia intelectual “por suerte”, pero “inmensamente pobres”**, como dice su hija, sigue vivo en la memoria de las víctimas del Estado que hoy conmemoran  el dolor de cientos y miles de muertes, pero también celebran los cientos y miles de vidas entregadas por pensar y trabajar por una Colombia distinta.

Gloria Gaitán afirma que **la memoria de su papá es muy revolucionaria, pero la memoria verdadera, no la del caudilló y el ícoino,** no la memoria de las estatuas que son "sarcófagos ideológicos" sino sus ideas y sus construcciones legales e ideológicas. Esa es la memoria que se debe retratar, afirma.

Cuando asesinaron a Jorge Eliecer Gaitán “cada uno sintió que lo habían herido a sí mismo”, **ahora 68 años después la herida sigue abierta y la memoria sigue viva.**

### **Primera Parte de entrevista especial con Gloria Gaitán** 

\[embed\]https://www.youtube.com/watch?v=nurK8T3UisI\[/embed\]

### **Segunda Parte** 

\[embed\]https://www.youtube.com/watch?v=bBezksD4LF8\[/embed\]  
 
