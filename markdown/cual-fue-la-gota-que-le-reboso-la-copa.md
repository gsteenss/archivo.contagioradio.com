Title: ¿Cuál fue la gota que le rebosó la copa?
Date: 2019-10-08 17:03
Author: A quien corresponde
Category: A quien corresponda, Opinion
Tags: Cristianos, familia, Medios de información, redes sociales, Religión, Sociedad
Slug: cual-fue-la-gota-que-le-reboso-la-copa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***No hagas a nadie lo que no quieres que te hagan.***

(Tobías 4, 15)

Bogotá, 30 de septiembre de 2019

 

*El amor no se impone por la fuerza, la autoridad o el poder.*

*El mensaje de Jesucristo es armonía, verdad, justicia y amor. *

 

Estimado

**Hermano en la fe**

Cristianas, cristianos, personas interesadas

Cordial saludo,

Te recuerdo que decidí escribir cuando se volvió imposible hablar con argumentos sobre temas relacionados con la religión y la política.

Lo hice públicamente porque conozco a muchas personas que vivían situaciones parecidas con familiares, amigos y compañeros de trabajo.

Espero estar logrando el objetivo de exponerte mis ideas con argumentos, lenguaje comprensivo, claro y sin agresiones para ayudar a pensar y analizar antes de reaccionar pasional y agresivamente con personas de derecha, de extrema derecha, de centro, de izquierda o de extrema izquierda al abordar temas relacionados con la religión y la política.

He recibido comunicaciones positivas de las 10 primeras cartas escritas, tanto de personas jóvenes y adultos, mujeres y hombres, creyentes y no creyentes, con poco estudio y profesionales; unas valoran las reflexiones y las agradecen, otras preguntan *“¿quién lo dejo sin posibilidad de dialogo?”* o *“¿cuál fue la gota que rebosó la copa”?,* hay quienes hacen referencia a los diálogos generados por las cartas o comparten que “*a mí también me ha pasado”* o “*eso pasa en parte de mi familia”.*

#### 

#### *T*ambién hay quienes aportaron reflexiones como esta: 

*“A veces como seres humanos carecemos de la capacidad de comprender perspectivas que difieran con las propias, nos han enseñado a defender a capa y espada nuestros sistemas de creencias e ideologías y a invalidar cualquier opinión que no encaje en tales posiciones. Si tan sólo nos detuviéramos a reflexionar que somos construcciones sociales y que básicamente cada persona está insertada en un sistema social particular y que por ende piensa y siente de esa forma sería el primer paso para que sea posible el dialogo y la escucha en las relaciones humanas. Mientras tanto espero que sus palabras sigan siendo semillas de reflexión y cuestionamiento, y que algún día germinen en acciones de cambio”. *

Si bien la discusión descrita en la primera carta fue la “*gota que rebosó la copa”,* hubo hechos relacionados con la religión, que fueron **gotas que poco a poco llenaron la copa,** no solo de personas de tu entorno político y religioso sino a nivel general.

 

#### Comparto algunas “gotas”: 

-   La gota de la inundación de las redes sociales y medios de información de noticas falsas con apariencia verdadera.  Verdades a medias que llegan de diversas formas (textos, videos, fotos, memes…) sin posibilidad de verificar o cuestionar mentiras, que son asumidas como verdaderas por muchísimas personas, entre ellas cristianas. La gota de ver “normales” las informaciones sin confirmar, sin contrastar fuentes; de repetir “versiones oficiales” sin contexto y difundidas por la radio, los periódicos, la televisión y las redes sociales. “Gotas” que son peligrosa porque *“La divulgación de noticias falsas desemboca en una banalización de la mentira y, por ende, en la relativización de la verdad”.*   ([le puede interesar:  LA ERA DE LA POSVERDAD:realidad vs. percepción](https://www.revista-uno.com/wp-content/uploads/2017/03/UNO_27.pdf))

<!-- -->

-   La gota de la falta costumbre para dar y pedir argumentos, para relacionar los hechos y noticias de hoy con los de ayer, para identificar las relaciones del poder y los intereses (económicos, políticos, religiosos, personales…) de quienes difunden informaciones o las silencian de acuerdo a los intereses particulares, para ver los mismos hechos en diferentes medios y preguntarnos por la idoneidad e imparcialidad de quienes nos informan.

<!-- -->

-   El uso de mentiras y verdades a medias para defender personas, grupos religiosos y políticos y movimientos sociales o para atacarlos si son “de los contrarios”. La “gota” de la descalificación o adulación de funcionarios e instituciones públicas y privadas de acuerdo a los propios intereses, creando un ambiente social que borra los límites de lo bueno y lo malo y “devalúa” la verdad, la justicia, honestidad y el bien común.

<!-- -->

-    Una sociedad que legitima y justifica a los mentirosos porque presentan la mentira atrayente, agradable o conveniente; que no le importa la realidad “real” sino la apariencia y percepción de ella; que coloca lo religioso a su “servicio” mientras predica lo contrario. La “gota” de la sociedad de la posverdad que ***“consiste en la relativización de la veracidad, en la banalización de la objetividad de los datos y en la supremacía del discurso emotivo”.***  2  

    ###### 

**gotas que rebosaron mi copa:**

-   Ver y escuchar a personas creyentes, cristianas católicas y evangélicas defendiendo, apasionadamente y a veces de forma inconsciente, una sociedad con intereses, mentalidad y actitudes contradictorias con el proyecto de Jesús de Nazaret.
-   Ver personas buenas, de diversas iglesias, haciendo afirmaciones contundentes a favor de quienes mueven este tipo de sociedad; ver creyentes “envenenados” por sus líderes y por formadores de opinión contra personas que no conocen, no han visto y no conoce las razones para pensar como piensan y actuar como actúan.
-   Ver y sentir que el nombre de Dios era “degradado” o maltratado ante personas serias y respetuosas que pedían argumentos y razones de decisiones políticas y religiosas sobre realidades que afectaban a toda la sociedad y que a cambio recibieron insultos, descalificaciones y condenas de quienes se autoproclaman defensores de la fe, la familia, la “moral” y los “valores tradiciones”.

*Estas y otras fueron **“gotas que rebosaron mi copa”** y me llevaron a escribir las cartas que estoy escribiendo. *

([ver mas:Le escribo ante la imposibilidad de hablar : La gota que rebosó la copa](https://archivo.contagioradio.com/le-escribo-ante-la-imposibilidad-de-hablar-cordialmente/))

Fraternalmente, su hermano en la fe,

1.  Alberto Franco, CSsR, J&P
2.  *&lt;&lt; Cf. Ibídem.&gt;&gt;*

<francoalberto9@gmail.com>

 
