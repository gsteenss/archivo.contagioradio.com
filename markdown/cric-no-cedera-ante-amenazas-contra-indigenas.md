Title: CRIC no cederá ante amenazas contra indígenas
Date: 2019-10-04 16:09
Author: CtgAdm
Category: DDHH, Nacional
Tags: Amenazas contra indígenas, Cauca, CRIC
Slug: cric-no-cedera-ante-amenazas-contra-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/CRIC-guardia-indígena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

Desde el Consejo Regional Indígena del Cauca (CRIC) se han emitido una serie de alertas sobre las continuas intimidaciones y hechos victimizantes a los que se están viendo expuestos los diversos resguardos del departamento; las más recientes, relacionadas a un interés de actores armados en liberar a personas vinculadas con actos delictivos y que han sido detenidas por la Guardia indígena al "desarmonizar el territorio". Pese a las amenazas, el CRIC  manifestó que no cederán ante la presión ejercida.

El consejero mayor del CRIC, Hermes Pete explica que está situación, ocurrida en Tierradentro, se remite a la captura de una persona identificada como integrante de las disidencias de las FARC, quien fue apresado por la Guardia Indígena y sentenciado por la asamblea general a ser enviado a un centro para rehabilitación, tras haber desarmonizado el territorio.

A raíz de eso, en la región comenzaron a escucharse rumores de que esta persona debería ser entregada, o de otro modo, "procederán a ajustar cuentas con las autoridades, guardias y comuneros en la zona”, amenaza que llegó  a través de una llamada que recibió el consejero Pete cerca de las 10:00 pm del pasado 2 de octubre.

### CRIC afirma que deben acompañar a su guardia indígena 

Un caso similar se presentó el mismo 2 de octubre, cuando el coordinador indígena José Albeiro Camayo Guetio habría sido retenido y torturado por hombres que, según algunas versiones iniciales, buscaban liberar a personas detenidas por la Guardia Indígena. [(Le puede interesar: Tortura contra coordinador indígena José Camayo retrata la aguda crisis del Cauca)](https://archivo.contagioradio.com/tortura-contra-coordinador-indigena-jose-camayo-retrata-la-aguda-crisis-del-cauca/)

> ### Hay que tomar medidas en el asunto, este no es un cuento, nos están matando 

Para el gobernador, las constantes amenazas y atentados obedecen a un plan de estigmatización, promovido en ocasiones por fuerzas políticas como es el caso de la senadora María Fernanda Cabal, y a lo que los cabildos describen como una inoperancia del Gobierno, que responde con la militarización del departamento. **"Son más de 2.000 soldados en el territorio pero no pasa nada, todo se sigue agudizando más"**, expresó Pete. [(Lea también: El CRIC tramitará denuncia formal contra María Fernanda Cabal)](https://archivo.contagioradio.com/cric-denuncia-contra-maria-fernanda-cabal/)

Pese a la enorme presencia de fuerza pública en los territorios, la ausencia de políticas de inclusión por parte del Estado y la proliferación de grupos armados, desde el CRIC advierten que "pase lo que pase, no van a echar pie atrás, no pueden venir a desarmonizar el territorio". Por su parte el consejero agrega que las comunidades deben rodear a la guardia indígena, seguir en el ejercicio de control y acudir a la fortaleza que hasta entonces han demostrado.

<iframe id="audio_42763064" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_42763064_4_1.html?c1=ff6600"></iframe>

**Síguenos en Facebook:**

 

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
