Title: "El problema de la infancia y la adolescencia en Colombia es estructural"
Date: 2015-02-11 22:27
Author: CtgAdm
Category: Comunidad, DDHH, Economía
Tags: Angela Maria Robledo, COALICO, Día de las manos rojas, Niños en el conflicto
Slug: el-problema-de-la-infancia-y-la-adolescencia-en-colombia-es-estructural
Status: published

###### Foto: Carmela María 

##### <iframe src="http://www.ivoox.com/player_ek_4070640_2_1.html?data=lZWkkpuYdI6ZmKiakp6Jd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNDnjNPWh6iXaaOl0NiY25DSrYa3lIqvk8bXb8XZjKjcztTRpsrVjNPSxcrXrdXVz5DS1ZCuudToysjWw5C3cYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Ángela María Robledo, Representante a la Cámara] 

El salón Luis Guillermo Vélez del Congreso de la república reunirá el jueves 12 de febrero a 14 organizaciones que trabajan con y para niños, niñas y jóvenes en el **"Día de las manos rojas: vamos a reconstruir el amor"**, para escuchar sus análisis y propuestas para evitar que esta población sea involucrada y afectada por la guerra.

El evento se realiza en el marco de los recientes escándalos por la violación a los Derechos Humanos de niños y niñas en toda Colombia, desde el **asesinato de 4 menores en el Caquetá hasta la violación de un grupo de hermanos en el sur de Bogotá**; organizaciones que trabajan con y para esta población buscan evidenciar que el debate es mucho más profundo de lo que -en algunas oportunidades- permite ver la coyuntura, y recordar que, como demostraron las **muertes por hambre de niños y niñas en la Guajira, en el Cauca y en el Chocó hace menos de 6 meses**, el problema es estructural.

La reflexión, según lo planteado por la Representante a la Cámara Angela María Robledo, deberá **superar el marco de la discusión sobre el reclutamiento forzado** de menores, para debatir las necesarias **garantías integrales y reales de los Derechos de los niños y las niñas,** reconociendo que "hay otras condiciones y formas de victimización directa o indirecta: el que no puedan terminar sus estudios -por ejemplo- es un problema fundamental".

Para la representante a la cámara, la falta de oportunidades es una de las necesidades que más siente esta población. **"La demanda de los jóvenes en Colombia es no tener que elegir entre el fusil del Ejercito y el fusil de los ejércitos irregulares"**.

<div>

El tema debería ser discutido en la mesa de Diálogos de Paz de La Habana, pues no debería repetirse lo que ocurrió en el proceso de **desmovilización del paramilitarismo**, en que el ex-jefe paramilitar Ernesto Baez aseguró que "**El tema nunca se trató**: Uribe nunca permitió que se hablara porque complicaba las negociaciones".

De esta manera, pese a lo indignante que resultan hechos como los divulgados en los últimos días sobre violencia contra la infancia, **Colombia no puede acostumbrarse a judicializar temas que requieren política social**, y resolver todo con "Palomino, el Fiscal y la Directora del ICBF en una mesa hablando de judicializar. Por supuesto debe haber procesos de investigación contundentes, eficaces y pertinentes, porque eso es justicia jurídica. Pero aquí lo que los niños y niñas necesitan es Justicia Social"

La convocatoria para el **"Día de las manos rojas**" enfatiza en que "lo fundamental es salir de esta guerra, para no seguir contando niños muertos, niños reclutados, niños violados; sino niños con sus familias, en sus escuelas disfrutando de una vida digna a lo que tienen derecho".

</div>
