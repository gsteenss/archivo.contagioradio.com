Title: Denuncian que cinco campesinos fueron detenidos en el nordeste antioqueño
Date: 2020-06-22 14:28
Author: CtgAdm
Category: Actualidad, Nacional
Slug: capturas-nordeste-antioqueno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Foto-de-Cahucopona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Foto: Cahucopana

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización Cahucopana denunció este lunes que por lo menos cinco campesinos han sido detenidos en operativos en los que participó el ESMAD, la policía y el ejército en varios municipios del nordeste antioqueño. Además denunciaron que en los operativos un campesino más resultó herido por los disparos de las FFMM.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la organización de DDHH, el operativo se desarrolló desde las 2:00 am con allanamientos y capturas masivas simultáneas en la vereda El Carmen, caserío Rancho Quemado y el corregimiento de Carrizal, jurisdicción del municipio de Remedios (Antioquía), resultando en la captura de tres líderes comunales y dos campesinos, sindicados por cargos de concierto para delinquir agravado y porte ilegal de armas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Capturas en el nordeste antioqueño llenan a las comunidades de temor

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los campesinos capturados son Luis Felipe Argumero Álvarez, Antonio Rúa Ospina, Fernando Francisco Osorio, Luis Fernando Osorio Franco y Jesus Osorio Narváez. Algunos de ellos habrían sido transportados en helicóptero y otros sacados de sus viviendas por las montañas sin que la comunidad se percatara de los hechos. Según la denuncia “hasta el momento se desconoce el lugar al que fueron llevados los capturados después de su traslado en helicóptero desde el corregimiento”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente denunciaron que “en el caserío Rancho Quemado, el operativo de allanamientos y capturas inició con disparos por parte del Ejército Nacional que dejó una persona herida, el cual fue atendido por los soldados” sin embargo se desconoce el nombre y el estado de salud de la persona que resultó herida.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PrensaRural/status/1275091731025985536","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PrensaRural/status/1275091731025985536

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Exigen respeto por el debido proceso a favor de campesinos capturados

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por su parte CAHUCOPANA denunció que son necesarias las garantías al debido proceso para los campesinos detenidos quienes también son identificados como líderes comunales. También expresaron su preocupación por la militarización y el uso de las tropas que deben estar destinadas a la seguridad de los ETCR pero que están sirviendo como “apoyo logístico” para este tipo de capturas y por último manifestaron que las comunidades viven en permanente zozobra por este tipo de acciones en contra de personas reconocidas de las juntas de acción comunal. [Le puede interesar: Violencia contra las JAC se incrementa antes de las elecciones](https://archivo.contagioradio.com/violencia-politica-contra-juntas-de-accion-comunal-aumentaria-al-acercarse-elecciones-regionales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas son algunas de las imágenes que se han compartido por parte de CAHUCOPANA

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CAHUCOPANA/status/1275105512841973766","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CAHUCOPANA/status/1275105512841973766

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Ya se han presentado falsos positivos judiciales en el nordeste antioqueño

<!-- /wp:heading -->

<!-- wp:paragraph -->

Información aportada por la organización Cahucopana señala que la campesina Meily Bernal, capturada el 10 de febrero, en la vereda Panamá Nueve \#Remedios, fue liberada, pues según el Juzgado Primero Penal de Control de Garantías de Barrancabermeja, se le imputaron cargos por el delito de porte ilegal de armas (por tener una escopeta en su finca), llegando a la decisión de no imponerle medida de aseguramiento intramural.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La señora Bernal tiene 32 años, es madre de 3 hijos, agricultora, ama de casa y desde hace varios años ha estado afiliada a la Junta de Acción Comunal de su vereda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte las comunidades manifestaron su apoyo a los campesino detenidos y aseguran que este tipo de acciones por parte de las FFMM y la policía se constituyen en ataques a las organizaciones sociales presentes en la zona.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CAHUCOPANA/status/1275174954636623875","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CAHUCOPANA/status/1275174954636623875

</div>

</figure>
<!-- /wp:core-embed/twitter -->
