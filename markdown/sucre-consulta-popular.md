Title: Sucre también dice NO a la minería en Santander
Date: 2017-10-01 17:35
Category: Nacional
Tags: Consulta minera, Santander, Sucre
Slug: sucre-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/mapiot.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 1 Oct 2017 

Con el total de las 15 mesas instaladas informadas, los resultados de la Consulta popular en el municipio de Sucre Santander, fueron contundentes, 3016 personas, equivalentes al 98,21% de los votantes, dijeron NO al desarrollo de actividades de explotación minera y petrolera en su territorio.

De acuerdo con los resultados de la Registraduría Nacional del Estado civil, un total de 3.071 personas se presentaron a ejercer su derecho al voto de un potencial de 5.853 sufragantes. Los votos para el SI tan solo correspondieron al 1, 07% con 33 votos, uniéndose así a los municipios de Santander que antes habian tomado la misma decisión.

Durante los comisios de registraron 7 votos nulos y 15 votos no marcados, en una jornada tranquila donde la comunidad desde tempranas horas había registrado superar el umbral de 1951 votos. Le puede interesar: [Sucre, Santander busca detener actividades petroleras y mineras en su territorio](https://archivo.contagioradio.com/sucre-santander-busca-detener-actividades-mineras-y-petroleras-este-domingo/).

   
[![DLFYlDTXcAAlIq0](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DLFYlDTXcAAlIq0.jpg){.alignnone .size-full .wp-image-47422 .aligncenter width="584" height="349"}](sucre)  
 
