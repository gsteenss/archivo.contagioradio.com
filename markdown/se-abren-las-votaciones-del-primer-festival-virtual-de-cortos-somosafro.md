Title: Se abren las votaciones del Primer Festival Virtual de Cortos “SomosAfro”
Date: 2015-04-03 10:38
Author: CtgAdm
Category: 24 Cuadros, Nacional
Tags: Cine alternativo, Cinemateca Distrital, Festival de cine somos afro, Festival de Cine y Derechos Humanos, Somos Afro
Slug: se-abren-las-votaciones-del-primer-festival-virtual-de-cortos-somosafro
Status: published

*26 producciones en competencia hacen parte de la iniciativa que pretende relatar la experiencia de los afroamericanos en el continente.*

Desde ya y hasta el 1ro de Mayo, la comunidad América Latina y el Caribe podrá escoger con su voto la producción ganadora del Primer Festival Virtual de cortos “SomosAfro”,  propuesta que busca dar cuenta de la identidad afro americana presente en gran parte del continente, las fortalezas y propuestas sociales, culturales, políticas y económicas de sus comunidades, con diversas miradas desde lo audiovisual.

Un total de 181 propuestas fueron enviadas desde diferentes países de América Latina, de las cuales fueron preseleccionadas 26 producciones que reflejan las distintas formas de vida, aspiraciones y contextos de afro-descendientes de países como Chile, Uruguay, Aruba, Brasil y Colombia, con especial participación de estos últimos por tener una mayor ascendencia de raíces africanas.

Por Colombia hacen parte de la competencia los cortometrajes “Afrika” de Manuel Alejandro, “Bajamar” de Yaisia Rodríguez, “Currulao” de Tonio Hecker, “El abuso” de Leitner Smith, “El islam en Buenaventura” de Mercedes Vigón,”Matachindé” de Victor Palacios, “Nala y el brillo de su herencia” de Margarita Maquilón, “Ombligados en Jurubirá“ de Juan Camilo Garcia y Juan Manuel Vásquez y “Villas del progreso” de Daniela Reyes y  Alejandra Vanegas.

Concluidas las votaciones el viernes 1ro de Mayo, los 15 trabajos con mayor votación pasarán a la sección final, donde un jurado experto seleccionará al ganador que se llevará, además del reconocimiento, un premio de 10.000 dólares.

Los interesados en seguir el Festival pueden ver los documentales y votar por sus favoritos en [www.festival.somosafro.org/festival](http://www.festival.somosafro.org/festival) y seguir el concurso a través del hashtag \#HistoriasPorContar. El público también podrá ganar un premio, subiendo fotografías ingeniosas que den cuenta de cómo y en dónde viven el festival, la imagen con el mayor número de “Me gusta” recibirá una GoPro Hero 3+.

El festival es parte de la iniciativa SomosAfro.Org, promovida por el Laboratorio de Ideas del Banco Interamericano de Desarrollo y la Asociación Nacional de Alcaldes y Gobernadores de Municipios y Departamentos con Población Afrodescendiente – AmunAfro.
