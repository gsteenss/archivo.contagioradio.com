Title: Las mujeres integrantes de FARC avanzan en su reincorporación
Date: 2019-08-20 10:31
Author: CtgAdm
Category: Entrevistas, Mujer
Tags: acuerdo de paz, Ex comba, FARC, mujeres, reincorporación
Slug: mujeres-farc-avanzan-reincorporacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-19-at-12.28.35-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-21-at-5.37.24-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto: @mujeresfarc] 

Mujeres integrantes del Partido FARC se reunieron para hablar de cómo avanza la implementación de los Acuerdos de Paz y el enfoque de género. Hasta el momento sus mayores preocupaciones se ven reflejadas en los incumplimientos en puntos como **tierras, participación política, salud, educación y seguridad para quienes suscribieron este acuerdo. **

**Las tierras para la paz**

De acuerdo con Victoria Sandino, senadora de este partido, una de las principales exigencias de las mujeres que participaron en el evento, tiene que ver con la entrega de tierras. Debido a que este hecho a frenado la puesta en marcha de algunos  proyectos productivos tal y como se había pactado en La Habana, y genera una incertidumbre frente a las garantías para la reincorporación.

Sumado a este hecho, las mujeres también se refirieron a los múltiples obstáculos que han tenido que afrontar los integrantes de FARC a la hora de presentar los proyectos productivos. En ese sentido, Sandino manifestó que no hay una designación presupuestal lo suficientemente amplia que aseguré la puesta en marcha de estos emprendimientos.

Hasta el momento, la senadora expresó que tan **solo hay 29 proyecto colectivos avalados por el gobierno que no recogen "ni siquiera a dos mil mujeres y hombres"** que están en el proceso de reincorporación.

**"Nos están matando"**

Desde la firma del Acuerdo de Paz, el partido político FARC ha señalado que 141 de sus integrantes han sido asesinados, entre quienes se encuentran dos mujeres. Esta situación para las integrantes de esta organización, que se encuentran en su camino a la reincorporación social, no solo deja una estela de temor**, sino también de desconfianza hacia un gobierno que para ellas no ha emprendido ninguna acción por salvaguardar sus vidas. **

Según Victoria Sandino, la falta de medidas de seguridad tanto en los Espacios Territoriales de Capacitación y Reincorporación, como en las ciudades "es un de esas dificultades que las mujeres deben padecer en los territorios", razón por la cuál durante el evento, se hizo un llamado tanto al gobierno como a las distintos países garantes y organizaciones sociales, a tomar acciones prontas frente a los asesinatos y las investigaciones tanto de los autores materiales como intelectuales.

**Enfoque de género**

Uno de las grandes hazañas de estos Acuerdos, es la introducción de más de 100 artículos y transversales sobre el enfoque de género en la implementación de los mismos. Sin embargo, según la plataforma GPAZ, si bien se ha generado un 77% en el desarrollo normativo, hay un 40% de las medidas que no tienen ningún avance operativo, **al que se suma un 53% que debería tener mayores progresos. **

En el caso de las mujeres de FARC, Sandino aseguró que uno de los puntos en donde más avances han tenido es el de participación política, debido a que ha aumentado la **postulación de mujeres a cargos de elección popular desde este partido con 119 candidaturas. **

De igual forma señaló que uno de los logros más fuertes ha sido el empoderamiento de las mujeres en los territorios que ha generado un mayor grado de organización interno, "desde la firma del Acuerdo de Paz, las mujeres hemos buscado un escenario para encontrarnos. Lo hemos a partir de la comisión Nacional Género y Diversidad y eso nos ha permitido hacer un acompañamiento a nuestras integrantes".

No obstante a estos avances, Sandino señaló que durante el encuentro las mujeres manifestaron las dificultades que han afrontado a la hora de exigir derechos como el de la salud a las entidades prestadoras de este servicio, ya que no son atendidas con un enfoque diferencial y en donde las más perjudicadas siguen siendo las mujeres que habitan en la ruralidad. (Le puede interesar:["Mujeres en América inician un camino por abolir las cárceles"](https://archivo.contagioradio.com/mujeres-en-america-inician-un-camino-por-abolir-las-carceles/))

**Los retos que se vienen paras las mujeres y su camino en la reincorporación**

La senadora del partido FARC aseveró que dentro de los retos que tendrán que afrontar las mujeres en los próximos meses para caminar hacia la reincorporación será hacer seguimiento y exigencias a la institucionalidad para que "implemente el Acuerdo de paz, cumpla lo pactado y lo haga con el enfoque de género", es decir que "nos den el papel protagónico que nosotras hemos luchado y merecemos".

Finalmente, otros de los retos que afrontan las mujeres será exigir al gobierno el enfoque diferencial en derechos como salud y educación, de forma tal que no tengan que ser víctimas del actual sistema de salud del país, y que tengan verdaderas oportunidades para ingresar al mercado laboral desde sus intereses y no desde ofertas educativas que reproducen los roles sociales.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

 
