Title: A 20 años de la masacre de Tibú, madres del Catatumbo resisten con la esperanza de encontrar verdad
Date: 2019-05-29 18:10
Author: CtgAdm
Category: Memoria
Tags: Ana Rosa Castiblanco, Catatumbo, masacre, tibu
Slug: madres-catatumbo-masacre-tibu-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/D7wgSCeX4AAZrk3.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/D7wGOxQXkAYCirE.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### **Foto: ASCAMCAT** 

**Dos décadas después del inicio de la masacre de las Carboneras en Tibú, Norte de Santander**, cuando la "caravana de la muerte" de los paramilitares ingresó al Catatumbo en connivencia con el Ejército Nacional, las madres de la región aseguran que a pesar de todo lo vivido "no permitiremos que la historia se repita dos veces, pues **ya fue suficiente dolor y tristeza que las madres del Catatumbo debimos soportar por el maltrato de nuestros hijos, nuestros esposos, nuestros padres, nuestro territorio**"

En un comunicado, manifestaron que al conmemorar la fecha "**renace la esperanza de seguir construyendo un Catatumbo mejor para nuestras futuras generaciones,** las que se encargaran de ser guardianes permanentes de la paz, la justicia y buscarán la verdad de lo sucedido", y enviaron un mensaje a quienes se han auspiciado este tipo de acciones en su territorio "estamos con la mejor disposición de seguir trabajando y protegiendo nuestro nuestro Catatumbo y sobre todo de **construir un territorio de paz sin secuelas de lo sucedido** extendiendo nuestro mensaje acogedor hacia ustedes para que hagan su aporte a la construcción de paz en el Catatumbo mediante el reconocimiento de la verdad.

Así mismo extendieron un llamado a todos los campesinos y campesinas de la región para que "**apoyar la implementación del acuerdo paz para la terminación del conflicto y la construcción de una paz con justicia social para el Catatumbo,** implementación que se debe realizar de la mano de la comunidad y más aún en tan adverso momento en que se refleja la falta de voluntad política del Gobierno Nacional como una estrategia para hacer ver que el proceso fue un fracaso"

#### **Sobre los hechos que rodearon la masacre**

El 29 de mayo de 1999 inició un episodio más de terror para la población de Tibú en el Catatumbo, Norte de Santander. Aquel sábado en 1999 200 paramilitares viajaron desde Urabá y del sur de Cesar en camiones para incursionar en la subregión del Catatumbo. Por órdenes de Vicente y Carlos Castaño arribaron a la zona para quitarle el control territorial a al ELN y a las FARC. Al llegar, se da la primera masacre en Tibú, donde, asesinaron a 18 personas.

Luego, sobre la carretera que conecta el casco urbano de Tibú con el de la Gabarra, los paramilitares instalaron un retén reteniendo a 60 vehículos. Allí asesinaron entre cinco y ocho personas en frente de sus familiares obligando a que abandonaran los cuerpos sobre la vía. Es así como se consolida el Bloque Catatumbo al mando de Salvatore Mancuso, con complicidad del Coronel Gustavo Matamoros.

Desde entonces, se realizaron varias masacres perpetradas por este bloque entre mayo y agosto de 1999 en el Norte de Santander. Debidó a la ola de violencia los pobladores catatumberos tuvieron que migrar hacia Venezuela y otras partes del país.

En 2004 se vinculó también como complicidad por parte de la Fuerza Pública a el Mayor Mauricio Llorente Chávez por la participación de los crímenes ocurridos en la región. A pesar de que el Catatumbo ha sido un escenario de desmovilización en dos ocasiones por parte del EPL y del Bloque Catatumbo, las estructuras armadas no se han desmontado debido a la ausencia del Estado y el abuso por parte de la Policía y el Ejército ha agravado la situación de violencia y de derechos humanos en la zona.

 

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/D7wgSCeX4AAZrk3-213x300.jpg){.aligncenter .wp-image-68094 width="494" height="696"}

###### Reciba toda la información de Contagio Radio en [[su correo]
