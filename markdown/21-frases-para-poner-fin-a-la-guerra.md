Title: 21 Frases para poner fin a la guerra
Date: 2016-09-29 12:16
Category: Otra Mirada, Paz
Tags: colombia, Fin del conflicto, Frases, guerra, paz
Slug: 21-frases-para-poner-fin-a-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/21.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: Contagio radio 

###### [29 Sep 2016] 

En Febrero de 1991, el docente y escritor payanes **Gustavo Wilchez Chaux**, compiló algunas frases de líderes mundiales, intelectuales, filosofos y artistas entre otros, sobre las causas y consecuencias de la guerra para la humanidad. En momentos en que Colombia esta cerca de alcanzar una salida negociada al conflicto armado, recordamos esos 21 pensamientos, con el ánimo de aportar a la reflexión personal sobre la posibilidad que se abre de dedecir democráticamente si continuar o dar un paso hacia su culminación.

**1**. Preferiría la paz más injusta a la más justa de las guerras. Cicerón  
**2.** Cuando los ricos se hacen la guerra, son los pobres los que mueren. Jean Paul Sartre  
**3.** El hombre tiene que establecer un final para la guerra. Si no, ésta establecerá un fin para la humanidad. John Fitzgerald Kennedy  
**4**. Las guerras seguirán mientras el color de la piel siga siendo más importante que el de los ojos. Bob Marley  
**5.** No sé con qué armas se luchará en la Tercera Guerra Mundial, pero sí sé con cuáles lo harán en la Cuarta Guerra Mundial: palos y mazas. Albert Einstein  
**6.** Una nación que gasta más dinero en armamento militar que en programas sociales se acerca a la muerte espiritual. Martin Luther King  
**7**. La guerra no es más que un asesinato en masa, y el asesinato no es progreso. Alphonse de Lamartine  
**8.** Si ha de haber conflictos que sea mientras yo viva, que mi hijo pueda vivir en paz. Thomas Paine  
**9.** La guerra es una masacre entre gentes que no se conocen, para provecho de gentes que sí se conocen pero que no se masacran. Paul Valéry  
**10.** La guerra vuelve estúpido al vencedor y rencoroso al vencido. Friedrich Nietzsche  
**11.** Inteligencia militar son dos términos contradictorios. Gray Marx  
**12.** Ningún hombre es tan tonto como para desear la guerra y no la paz; pues en la paz los hijos llevan a sus padres a la tumba, en la guerra son los padres quienes llevan a los hijos a la tumba. Heródoto de Halicarnaso  
**13**. La guerra es un mal que deshonra al género humano. Fénelon  
**14**. Para hacer la Paz se necesitan dos; pero para hacer la guerra basta con uno sólo. Arthur Neville Chamberlain  
**15.** Se tardan veinte o más años de Paz para hacer a un hombre y bastan veinte segundos de guerra para destruirlo. Balduino I  
**16.** La guerra es una invención de la mente humana y la mente humana también puede inventar la Paz. Winston Churchill  
**17.** La guerra es la salida cobarde a los problemas de la paz. Thomas Mann  
**18.**Un estado en guerra sólo sirve como excusa para la tiranía doméstica. Aleksandr Solzhenitsin  
**19.** El gran Cartago lideró tres guerras: después de la primera seguía teniendo poder; después de la segunda seguía siendo habitable; después de la tercera... ya no se encuentra en el mapa. Albert Camus  
**20.** El único medio de vencer en una guerra es evitarla. George Catlett Marshall  
**21.** Cada día en Colombia hay mas vidas truncadas, nuevos huérfanos, nuevos horrores, nuevas soledades. En este mismo instante hay en Colombia petróleo crudo tiñendo la vida del color de la muerte. Hay bosques ardiendo. Hay niños que tiemblan cuando ladran los perros. Hay una orgía incontenible de violencia y de muerte. Pero también, en este mismo instante, hay esperanza, hay deseo, hay voluntad de paz, hay confianza. Hay vida, el reto es defenderla, facilitarla, compartirla, mejorarla. El reto es que nuestros hijos hereden nuestras esperanzas, no nuestros horrores.
