Title: Fue asesinado Joaquín Jaramillo, fiscal de Junta de Acción Comunal en Sonsón, Antioquia
Date: 2019-03-20 14:53
Category: DDHH, Nacional
Tags: Antioquia, asesinato de líderes sociales, juntas de acción comunal
Slug: fue-asesinado-joaquin-jaramillo-fiscal-de-junta-de-accion-comunal-en-sonson-antioquia
Status: published

###### Foto: Archivo 

###### 20 Mar 2019 

En la noche del pasado 19 de marzo, fue asesinado con múltiples disparos de arma de fuego, **el fiscal de la Junta de Acción Comunal de la vereda Piedras Blancas, Joaquín Emilio Jaramillo** mientras se encontraba en su vivienda en el corregimiento de San Miguel, municipio de Sonsón, Antioquia.

**Óscar Yesid Zapata, integrante de la Coordinación Colombia Europa Estados Unidos** explica que el caso ya fue asumido por la dirección de derechos humanos de la Fiscalía de Antioquia, a la que han solicitado agilice la investigación para que la muerte de Joaquín no pase a ser un caso más en la impunidad.

El integrante de Coeuropa aclara que la labor de Joaquín Jaramillo, como la de todos los fiscales dentro de la Juntas de Acción Comunal estaba relacionada a funciones de control, aportando a la consolidación de la participación comunitaria. [(Le puede interesar: Guardia campesino que acudió a llamado de auxilio fue asesinado en Corinto, Cauca) ](https://archivo.contagioradio.com/asesinan-guardia-campesina-corinto/)

Según el **Observatorio de DD.HH de la Fundación Sumapaz y la Corporación Jurídica Libertad, se han registrado seis  homicidios contra líderes defensores de DD.HH.** en Antioquia en lo que va corrido del año, cifras que junto a la del año pasado en el que se registraron 31 homicidios revelan un panorama preocupante.

A su vez, explica que aunque el municipio de Sonsón no se ha caracterizado por ser una zona marcada por el conflicto, es evidente el incremento de "fenómenos de violencia y formas de control político y territorial" en una región donde grupos paramilitares como el clan Isaza utilizan el paso del **Magdalena Medio** para desarrollar sus actividades de microtráfico.

Se desconoce si el líder había recibido amenazas en su contra, sin embargo, con su muerte y los recientes ataques contra líderes sociales y comunales en diferentes regiones del país, Zapata asegura que muchos dirigentes de las comunidades han preferido abandonar su labor ante la falta de garantías.

<iframe id="audio_33554781" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33554781_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
