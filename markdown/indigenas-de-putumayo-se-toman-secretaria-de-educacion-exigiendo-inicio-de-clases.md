Title: 132 niños indígenas están sin clase por incumplimientos de la Secretaría de Educación
Date: 2016-05-20 18:04
Category: Educación, Otra Mirada
Tags: educacion, indígenas, Pueblo Nasa, Putumayo
Slug: indigenas-de-putumayo-se-toman-secretaria-de-educacion-exigiendo-inicio-de-clases
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Putumayo-Nasa-e1463785355735.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cajar 

###### [20 May 2016] 

38 Cabildos indígenas del Pueblo Nasa, departamento del Putumayo, decidieron tomarse pacíficamente la Secretaría de Educación, cansados de los constantes incumplimientos del gobierno departamental que en este momento **no ha contratado 11 profesores que hacen falta para que 132 niños y niñas indígenas puedan iniciar sus clases.**

En total, son 11 las comunidades en las que a la fecha no se ha podido dar inicio a las clases, pero además, se denuncia que los pequeños **no están recibiendo los refrigerios y comida necesaria por cuenta del robo de esos recursos, no hay transporte escolar, entre otras exigencias.**

De acuerdo con Oscar Pisso, consejero del Pueblo Nasa, en los últimos años se ha tenido un diálogo constante con la Secretaría, pero esta entidad ha incumplido lo pactado y no se han nombrado los docentes necesarios. Por tal razón, **han asegurado que no se retirarán de la entidad hasta que no se haya nombrado a los profesores** y no se haya establecido una hoja de ruta para atender las demás exigencias.

<iframe src="http://co.ivoox.com/es/player_ej_11635428_2_1.html?data=kpajlZqYdpmhhpywj5aXaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncbDnxMbfjbXNt9TjjJKYpdTSt8bextfcjcnJsIzE1srPztSPksLnwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
