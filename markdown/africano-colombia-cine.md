Title: Inicia la muestra itinerante de cine Africano en Colombia
Date: 2017-05-03 10:20
Author: AdminContagio
Category: 24 Cuadros
Tags: cine africano, colombia, MUICA
Slug: africano-colombia-cine
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/africano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Something Necesary 

###### 25 Abr 2017 

Con una programación de **20 películas** entre documentales, ficciones y cortometrajes, del **3 de mayo al 10 de junio** se realiza en cuatro ciudades de Colombia la **Segunda Muestra Itinerante de Cine Africano MUICA**. Un escenario ideal para reconocer el valor e influencia que tiene la herencia del continente negro para la cultura del país.

En espacios académicos, barriales y alternativos de **Bogotá, Cali, Cartagena y Providencia**, se exhibirán las producciones que van desde el thriller, pasando por el drama histórico, la ciencia ficción y el falso documental, abriendo **una ventana a la diversidad cultural africana, con sus realidades sociales y expresiones artísticas**.

La MUICA estará compuesta por tres secciones principales: **Hecho en África**, selección principal de películas realizadas por cineastas africanos; **Otras Miradas**, compuesta por títulos que abordan temáticas africanas dirigidos por realizadores no africanos; y **Diáspora**, que incluye historias que giran en torno a la afrodescendencia en el mundo.

Las proyecciones que tendrán lugar en **Bogotá del 3 al 10 de mayo, Cali del 10 al 16, Cartagena del 24 al 31 y Providencia del 2 al 10 de junio**, serán presentadas por miembros del equipo de producción y programación, y algunas de ellas contarán con la presencia de expertos locales que estimularán la discusión en torno a temas relacionados con el contenido de las películas.

![muica](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/unnamed-1.png){.size-full .wp-image-39963 .aligncenter width="800" height="317"}

Como invitado especial, la muestra presentará al director camerunés **Jean-Pierre Bekolo** quien presentará sus producciones **Las Sangrientas y El Presidente**, dos películas que lo han consolidado como una de las voces de vanguardia en la escena cinematográfica africana.

De la [programación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Programación-general-MUICA-2017.jpg) hace parte también el **Taller de cine africano "África ReXiste"** que tendrá lugar las ciudades de Cali y Providencia, con una exploración por los antecedentes del cine en África. El 21 de mayo de manera simultanea en Bogotá, Cali y Cartagena, se proyectará la película colombiana **Marímbula**, de Diana Kuellar, que sigue a dos jóvenes afrocolombianos que aspiran a encontrarse con sus raíces y viajan a Senegal, presentación que servirá para conmemorar el día de la abolición de la esclavitud en Colombia.

Angela Ramírez, del equipo organizador de la MUICA, conversó con Contagio Radio sobre la programación y proceso de curaduría de la muestra y dejó algunos recomendados para ver desde este miércoles 3 de mayo.

<iframe id="audio_18480013" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18480013_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
