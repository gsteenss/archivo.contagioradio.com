Title: Luego de 18 meses, las lideresas Sara Quiñones y Tulia Valencia recobran la libertad
Date: 2019-07-29 12:37
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Altomira, Falsos Positivos Judiciales, lideresas, Tumaco
Slug: luego-de-18-meses-las-lideresas-sara-quinones-y-tulia-valencia-recobran-la-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Afro Colombian Solidarity Network ] 

[Las lideresas Sara Quiñones y Tulia Valencia del Consejo Comunitario Alto Mira en Tumaco, fueron puestas en libertad, hace un par de semanas, tras un año y medio en la cárcel, tras haber sido víctimas, de acuerdo con organizaciones sociales, de un 'un falso positivo judicial'. Según Charo Mina, Coordinadora de Proceso de Comunidades Negras de Colombia, el proceso jurídico contra ellas continuará y tendrán que comparecer hasta que se determine su libertad total. ]

[Mina, afirma que las razón por las que  las lideresas fueron obligadas a permanecer en una cárcel de máxima seguridad fue "ser acusadas como miembros de el Ejército de Liberación Nacional (ELN)”. Además “por una serie de decisiones de carácter político y argumentadas bajo tecnicidades jurídicas", las mujeres permanecieron ese tiempo en detención intramural, cuando podrían haber estado en sus hogares con restricciones.](Le puede interesar:["Falsos positivos judiciales contra lideresas sociales"](https://archivo.contagioradio.com/falsos-positivos-judiciales-contra-lideresas-sociales/))

### **Representación y  acompañamiento para las lideresas** 

[Según Mina debieron recurrir a varias solicitudes, además de un camino complejo para lograr que Quiñones y Valencia salieran de la cárcel. Por el momento están siendo representadas por Adelmo Carabalí, uno de los abogados del Proceso de Comunidades Negras de Colombia (CNC). ]

[Las lideresas están solicitando a la Procuraduría General de la Nación que se realice una investigación frente al caso en su contra, aunque Mira menciona que “pedir  esta medida a la PGN podría interferir con el proceso”.  Por su parte Sara Quiñones está bajo la unidad de protección, goza de medidas cautelares de la Comisión Interamericana de Derechos Humanos. ]

[Mina Añadió que “ambas lideresas cuentan con el apoyo de la comunidad  y se encuentran en un proceso de ajuste ,además de la recuperación física y emocional” que les generó este hecho al estar separadas de sus familias y actividades políticas. (Le puede interesar: ["Dirección Nacional de Inteligencia estaría detrás de falsos positivos judiciales contra oposición"](https://archivo.contagioradio.com/direccion-nacional-de-inteligencia-contra-oposicion/))]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
