Title: Gobierno está "repartiendo la pobreza" pero no aumenta presupuesto: FECODE
Date: 2017-05-30 12:50
Category: Educación, Nacional
Tags: educacion, fecode, Paro de maestros
Slug: gobierno-esta-repartiendo-la-pobreza-pero-no-aumenta-presupuesto-fecode
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/FECODE-FOTO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 may 2017] 

El acercamiento entre el gobierno nacional, en cabeza de la Ministra de Educación Yaneth Giha, y el gremio de los educadores públicos del país, Fecode, es cada vez más difícil. Para el gobierno, los maestros se están movilizando porque quieren únicamente aumentos en sus salarios. Sin embargo, los educadores han manifestado en varias ocasiones que el **objetivo principal de la movilización es financiar el déficit presupuestal de la educación pública.**

Este martes se reanudan las conversaciones entre maestros y gobierno y se espera que se dialogue sobre temas que según Over Dorado, miembro de la junta directiva de Fecode, **“son responsabilidad del Estado y no la han querido asumir”**. Los temas a discutir se enfocan en la bonificación especial de los educadores y educadoras, la nivelación salarial, las primas extra legales y las brechas de la educación en materia de financiación a través del sistema general de particiones. Le puede interesar: ["Estas serán las rutas de movilización de la Gran Marcha por la Educación de Fecode"](https://archivo.contagioradio.com/estas-seran-las-rutas-de-movilizacion-de-la-gran-marcha-por-la-educacion-de-fecode/)

<iframe src="http://co.ivoox.com/es/player_ek_18980167_2_1.html?data=kp2mmpWVepihhpywj5aWaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncbDqxteYptTWpcXjj5KYqKqnk6W5joqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Bonificación Especial** 

Sobre el tema de la bonificación especial, Fecode ha establecido que “el gobierno ha propuesto un monto muy bajo que **solo reparte pobreza, porque sigue diciendo que no hay plata”.** Según Over Dorado, los maestros han propuesto un monto diferente a los 15 días de remuneración salarial que reciben por bonificación salarial, el cual han tenido que negociar en diferentes ocasiones.

### **Nivelación Salarial** 

Ante la nivelación salarial, el gobierno ha sostenido el argumento de que no tiene dinero para aumentar salarios hasta 2020 y 2021. Por esto y para Over Dorado, **“la economía se está moviendo por lo que tiene que haber cambios”.** Frente al tema de las primas extra legales, el Consejo de Estado estableció que este tipo de primas son ilegales e inconstitucionales. Para Fecode, este concepto afecta al 25% de los salarios de educadores y educadoras del país.

### **Financiación de la Educación** 

Finalmente, ante la financiación de la educación, las partes no se han puesto de acuerdo porque según Over Dorado, **“los maestros no hemos recibido una propuesta eficaz”.** Fecode continúa exigiendo mejoras en el sistema nacional de partición para que se solvente el problema de las brechas de cobertura, acceso a educación y la jornada única.

### **Se puede bajar el presupuesto de la guerra para invertir en Educación** 

Para los educadores el gobierno debe revisar la inversión que se hace para temas de guerra, temas de endeudamiento externo y reservas internacionales. Over Dorado afirma que “el gobierno puede bajar el pago de los impuestos de la deuda externa y puede utilizar los recursos que tiene para las “vacas flacas” e invertirlos en educación”. Le puede interesar: ["Hay propuestas pero el gobierno no tiene voluntad para negociar: Fecode"](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/)

En materia de impuestos a la guerra, los educadores han afirmado que, en épocas de posconflicto, **las inversiones en seguridad deben disminuir** para darle prioridad a la educación.

**Advertencia de secretarías sobre situación de Educación en el país**

<iframe src="http://co.ivoox.com/es/player_ek_18980225_2_1.html?data=kp2mmpWWdpahhpywj5aWaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKfxcrZjajFts7Zz5C3y9KJh5SZop7bx9-JdqSftMrQ1MrYpdPdwpDRx5CpqNbXwsjWh6iXaaOnz5DRx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Secretarios y secretarias de nueve departamentos y municipios señalaron la falta de recursos para cubrir la planta educativa de las instituciones, la falta de infraestructura y la ausencia de condiciones para establecer la jornada única, que **no tiene ambientes de aprendizaje dignos ni suficientes maestros.** Le puede interesar: ["Secretarías de educación ya habían advertido grave situación de educación en Colombia"](https://archivo.contagioradio.com/secretarias_educacion_paro_educadores/)

Según María del Carmen Jiménez, Secretaria de Educación Departamental del Huila, “las secretarías de educación están comprometidas con la educación de calidad como derecho fundamental y por esto **le estamos pidiendo al gobierno que piense en la financiación de la educación** para mejorar las condiciones de la misma”.

De igual manera, las secretarías han respaldado el paro de los educadores pues afirman ellas que “los maestros y maestras están **reclamando la dignificación de su profesión** docente con mejores salarios, prestaciones sociales y de salud”.

Se espera que, para mañana, 31 de mayo, los docentes del país realicen una gran movilización **en 5 capitales del país.** Se realizarán marchas en Cali, Bogotá, Barraquilla, Bucaramanga y Medellín.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
