Title: Condena de CIDH a Colombia es una sentencia con rostro de mujer
Date: 2017-01-11 17:09
Category: DDHH, Entrevistas
Tags: Comuna 13, Condena, mujeres, operación orion, paz, Sentencia
Slug: condena-con-rostro-de-mujer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Mujeres-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alfa Noticias] 

###### [11 Enero 2017] 

**“Colombia es responsable por no prevenir el asesinato de la defensora de derechos humanos Ana Teresa Yarce y de su detención ilegal y arbitraria junto con la de Socorro Mosquera, Mery Naranjo, Luz Dary Ospina y Miriam Rúa, todas defensoras de la Comuna 13**”, así lo aseguró en su reciente sentencia la Corte Interamericana de Derechos Humanos que hizo pública el pasado 10 de Enero.

Dicha decisión en el *Caso de Ana Teresa Yarce y otras – Comuna 13* en Medellín, es la **primera condena proferida contra Colombia por violaciones de derechos humanos cometidas contra mujeres lideresas comunitarias** y a su vez, es la primera condena de carácter internacional por los hechos atroces ocurridos en la Comuna 13.

Para María Victoría Fallon directora del Grupo Interdisciplinario por los Derechos Humanos – GIDH “**la decisión de la Corte hace un énfasis especial en la protección y garantía que deben tener las mujeres lideresas colombianas para poder desarrollar sus labores**, esto  no lo había tratado la Corte antes en relación con un caso para Colombia, es una sentencia sin precedentes”.

Pese al intento del Estado colombiano de una posible “revictimización” de las víctimas de la Comuna 13 a través de diversas actuaciones, como solicitar a la Corte no estudiar el caso por estar en un proceso de paz o que se abstuviera de ordenar reparaciones y remitiera a las víctimas a hacer la solicitud, estas no fueron escuchadas por el ente internacional.

“Esto hubiera sido una carga adicional para las víctimas, además que la ley de víctimas no tiene la capacidad para reparar como la tiene un órgano judicial” agregó Fallon.

**En esta sentencia**, la Corte se encarga de analizar los hechos que dieron lugar a la Operación Orión en todo su contexto. Además **da cuenta de violaciones de derechos humanos a lideresas comunitarias y hace énfasis en protección a la mujer, defensoras de derechos humanos.**

Así mimo, **ordena al Estado colombiano que reconozca responsabilidad y pida perdón en un acto público y en materia judicial dice que se deben continuar con las investigaciones para clarificar varios aspectos que continúan sin estar suficientemente claros. **

Asegura Fallon que en ese sentido “esperan que avancen las **investigaciones que hay en contra del General Mario Montoya y Leonardo Gallego así como del presidente de la República de ese momento hoy Senador, el señor Álvaro Uribe Vélez”. **Le puede interesar: [Exigen a Álvaro Uribe respeto por la dignidad de las víctimas de la Comuna 13](https://archivo.contagioradio.com/exigen-a-alvaro-uribe-respeto-por-la-dignidad-de-las-victimas-de-la-comuna-13/)

De acuerdo con la sentencia, **la Corte consideró que todos los hechos de violencia cometidos contra las cinco mujeres, se dieron por su condición de lideresas comunitarias y en un contexto de violencia contra las mujeres defensoras de derechos humanos** que se tradujo en una situación de riesgo para ellas, sin que el Estado hubiera hecho algo para evitarlo.

**¿Quiénes son las mujeres defensoras de derechos humanos?**

**Ana, Luz, Miriam, Mery y Socorro, vivían para la época de 2002 en la Comuna 13, todas tuvieron participación en la Asociación de Mujeres de las Independencias AMI,** organización vinculada al trabajo social de mujeres, así como en las Juntas de Acción Comunal menos la señora Rúa, que sólo se desempeñó en esta última entidad.

En el marco de su actividad como defensoras de derechos humanos, estas 5 mujeres, así como sus familias, se vieron afectadas por hechos vinculados a la actuación de personas relacionadas con grupos armados ilegales.

**Myriam Rúa Figueroa nació en Medellín**, el 18 de marzo de 1961, es socióloga de la Universidad Autónoma Latinoamericana y **para la época de los hechos era Presidenta de la JAC Barrio Nuevo**. Vivía en una casa de su propiedad, ubicada en Barrio Nuevo de la Comuna 13, con su compañero y sus tres hijas. Asimismo, Myriam participó en la creación de un grupo femenino para trabajar por la comunidad.

**Luz Ospina Bastidas nació en Medellín**, el 16 de septiembre de 1960. **Era Directora Ejecutiva de la Asociación de Mujeres de las Independencias y vivía en una casa de su propiedad con su esposo, sus hijas e hijo.**

**María del Socorro Mosquera Londoño nació en Medellín**, el 15 de julio de 1954. En la época de los hechos **se desempeñaba como Presidenta y representante legal de la AMI** y tenía a su cargo, al menos, el cuidado de sus dos hijos menores de edad.

**Mery del Socorro Naranjo Jiménez nació en Medellín,** el 1 de marzo de 1960, es costurera y diseñadora de disfraces. **Era Presidenta de la JAC del Barrio Independencias III de la Comuna 13 y antes del año 2002, ocupó el cargo de Fiscal de la JAC** y actualmente es integrante activa de la AMI. Tenía a su cargo el cuidado de sus cuatro hijos e hijas, con quienes vivía.

**Ana Teresa Yarce nació en Colombia el 15 de noviembre de 1959 y fue asesinada el 6 de octubre de 2004. Para la época de los hechos actuaba como Fiscal de la JAC del Barrio La Independencia** de la Comuna 13. También participó en la AMI y era la fontanera del barrio encargada del acueducto veredal. Tenía a su cargo el cuidado de cuatro niños con los cuales convivía.

Ana Yarce, Mery Mosquera y Mery Naranjo fueron detenidas en 2002, por haber sido señaladas como colaboradoras de la guerrilla, siendo liberadas 9 días después por no existir elementos suficientes que comprobaran dicha acusación en su contra. Le puede interesar: [Entre los escombros de la Comuna 13 florece la dignidad](https://archivo.contagioradio.com/a-13-anos-de-impunidad-florece-la-dignidad-entre-los-escombros-de-la-comuna-13/)

**La Operación Orión.**

La Operación Orión, fue llevada a cabo del 16 al 22 de octubre de 2002. En dichos operativos participaron de manera conjunta integrantes del Ejército Nacional, de la Policía Nacional, del Departamento Administrativo de Seguridad (DAS), de la Fuerza Aérea Colombiana (FAC), del Cuerpo Técnico de Investigaciones (CTI) y de la Fiscalía General de la Nación. Le puede interesar: [Fallas en búsqueda de desaparecidos de Comuna 13 se presentan ante la CIDH](https://archivo.contagioradio.com/fallas-en-busqueda-de-desaparecidos-de-comuna-13-se-presentan-ante-la-cidh/)

**El 16 de octubre de 2002, comienza la Operación Orión con la orden del entonces Presidente de Colombia Álvaro Uribe Vélez**, **y ha sido considerada como** reza en el informe “La huella invisible de la guerra: Desplazamiento forzado en la Comuna 13” del Grupo de Memoria Histórica de la Comisión Nacional de Reparación y Reconciliación, como  **"la acción armada de mayor envergadura que ha tenido lugar en un territorio urbano y en el marco del conflicto armado en el país”.**

<iframe id="audio_16143546" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16143546_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Sentencia Corte Interamericana de DH Caso Ana Teresa Yarce.](https://www.scribd.com/document/336310926/Sentencia-Corte-Interamericana-de-DH-Caso-Ana-Teresa-Yarce#from_embed "View Sentencia Corte Interamericana de DH Caso Ana Teresa Yarce. on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_6057" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/336310926/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-IGosAMzYdkfyRdG5TOpg&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
