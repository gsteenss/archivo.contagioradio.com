Title: Inicia jornada de movilizaciones de organizaciones sindicales
Date: 2016-09-15 13:28
Category: Movilización, Nacional
Tags: Movilización Bogotá, paro, Sindicalismo en Colombia
Slug: inicia-jornada-de-movilizaciones-de-organizaciones-sindicales
Status: published

###### [Foto: Contagio Radio ] 

###### [15 Sept 2016] 

"No nos queda alternativa diferente que la huelga el paro y la movilización", asegura Luis Alejandro Pedraza, presidente de la Central Unitaria de Trabajadores CUT, a propósito de la jornada de protestas que inició este miércoles y confluirá en un **gran paro cívico distrital programado para el próximo 27 de octubre**, con el que se busca presionar a los gobiernos nacional, departamental, municipal y local para el cumplimiento de los pliegos petitorios que han sido presentados por sindicatos de la salud, la educación, los servicios públicos y el sector mineroenergético.

Por su parte la Unión Sindical Obrera USO, anunció que entrara en huelga general con el fin de presionar a Ecopetrol para que no continúe vendiendo la empresa, afirma Pedraza y agrega que desde sectores como la educación también habrán **movilizaciones en rechazo a la privatización de los colegios públicos**. En los hospitales estatales la [[situación también es precaria](https://archivo.contagioradio.com/40-recorte-presupuestal-bogota-hospitales/)], principalmente porque no hay inversión, desarrollo tecnológico, ni garantías laborales.

Para los sindicatos es necesaria esta jornada debido a la falta de respuestas del Gobierno al pliego de demandas que fue presentado por diversas organizaciones sociales, pues **ni el presidente Juan Manuel Santos ni los ministros han presentado soluciones a las exigencias**. Las [[movilizaciones serán permanentes](https://archivo.contagioradio.com/sindicatos-rechazan-reforma-tributaria-y-convocan-a-paro-general/)] hasta que el mandatario y las carteras se pronuncien con acciones efectivas, y se espera que la ciudadanía en su conjunto las apoye "porque en la medida en que desaparezcan los servicios públicos y las garantías del Estado quienes se benefician son los grandes capitales que están detrás de los grandes negocios".

El próximo martes desde las 8 de la mañana iniciará una movilización que partirá desde la plaza Eduardo Umaña Mendoza para llegar a la Registraduría Distrital, en dónde serán radicadas las firmas del cabildo abierto por la [[No venta de la ETB](https://archivo.contagioradio.com/venta-de-etb-sera-demandada-por-sindicatos/)]. Los días miércoles a las 9 de la mañana y jueves a las 5 de la tarde, se llevarán a cabo los **ciclos de cine liderados por los sindicatos** de la Unión Nacional de Empleados Bancarios y Sintratelefonos.

El SENA, Notariado y Registro, junto con la Unión Sindical Obrera, la eaab y el magisterio distrital, han convocado asambleas permanentes; los trabajadores de la DIAN y del sector Financiero preparan un foro para discutir las [[consecuencias de la Reforma Tributaria](https://archivo.contagioradio.com/reforma-tributaria-desaparecera-el-94-de-las-organizaciones-sociales-del-pais/)] y **para mediados del mes de octubre se plantea la realización de cuatro asambleas distritales**, la primera de ellas en Kennedy, la segunda en Teusaquillo, la tercera en Suba y la cuarta en centro oriente.

<iframe id="audio_12921507" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12921507_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
