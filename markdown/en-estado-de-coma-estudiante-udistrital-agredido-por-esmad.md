Title: En estado de coma estudiante UDistrital agredido por ESMAD
Date: 2016-04-22 13:55
Category: DDHH, Nacional
Tags: ESMAD, estudiante UDistrital en coma, Universidad Distrital
Slug: en-estado-de-coma-estudiante-udistrital-agredido-por-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ESMAD.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Red Colombia ORG ] 

###### [22 Abril 2016] 

Este jueves en medio de una toma cultural de la Universidad Distrital, Sede Tecnológica, un estudiante fue agredido en la cabeza por el ESMAD, actualmente **se encuentra en el Hospital del Tunal en estado de coma**, mientras que otros diez están heridos.

"Cuando llegó el ESMAD lanzó gases y balas de pintura, luego empezaron a volar piedras y más gases durante una hora", afirma un estudiante, y agrega que el "pupitrazo cultural" se convocó para denunciar las **problemáticas internas de la Universidad**.

Entre ellas la [[elección antidemocrática del rector](https://archivo.contagioradio.com/consejo-superior-impide-eleccion-democratica-del-nuevo-rector-de-u-distrital/)] que promueve el Consejo Superior, quién además busca **desconocer la reforma universitaria elaborada desde hace dos años por trabajadores, estudiantes y profesores**.

La administración de la Universidad no se ha pronunciado al respecto, hasta el momento sólo han comunicado el cierre de la Facultad durante este viernes y sábado.

<iframe src="http://co.ivoox.com/es/player_ej_11269897_2_1.html?data=kpafmJ6cfZihhpywj5WbaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncabn1drRy8bSuMafttPW2MrWt8rYwsmYps7XuNPd1cbZj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]

 
