Title: Informe revela que hay 15076 víctimas de violencia sexual en Colombia
Date: 2017-11-09 17:18
Category: Mujer, Nacional
Tags: conflicto armado, guerra, mujeres
Slug: victimas-de-violencia-sexual-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/violencia-contra-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [09 Nov 2017]

El Observatorio de Memoria y Conflicto del Centro Nacional de Memoria Histórica presentará el próximo 24 de Noviembre su informe “La guerra inscrita en el cuerpo. Informe nacional sobre violencia sexual en el conflicto armado” En él se entrecruzan las historias de 227 mujeres que hacen parte del amplio espectro de la violencia sexual **en el marco del conflicto armado en Colombia y que arroja la cifra de 15076 víctimas**.

Según el Centro Nacional de Memoria Histórica, el 91.6% de las víctimas de estos delitos contra la libertad y la integridad sexual "han sido niñas, adolescentes y mujeres adultas". Además se especifica que existe un sub registro y poca información sobre esos delitos y un importante porcentaje de casos en los **que se desconocen los perpetradores que hace que las cifras no sean presentadas de manera concluyente.**

### **Los principales perpetradores de la violencia sexual han sido los paramilitares** 

Entre otras cifras el informe resalta, frente a los perpetradores que “**los paramilitares han sido responsables de 4.837 casos, 32%, las guerrillas han sido responsables de 4.722 casos, 31,5 %, Agentes del Estado** han sido responsables de por lo menos 206 casos registrados y los grupos armados posdesmovilización GAPD son responsables de 950 casos.” Y en 3.973 se desconocen los responsables.

El informe recoge cifras hasta el 20 de Septiembre de 2017 y pretende, según el comunicado de prensa, no solamente desenmascarar la verdad de una violencia que “ha sido silenciada” sino interpelar al conjunto de la sociedad colombiana para que cese la estigmatización de las víctimas y permitir que se visibilicen sus estrategias para superar esa victimización. (Le puede interesar:["ABC de la despenalización del aborto en Colombia"](https://archivo.contagioradio.com/despenalizacion-aborto-en-colombia/))

### **Las alternativas para intentar restaurar el daño de la violencia sexual** 

Según el CNMH, una de las preguntas que plantea esta investigación y que debe hacerse a toda la sociedad colombiana, también de cara a un proceso de post acuerdos es **¿qué vamos a hacer para que esto no vuelva a suceder?** Por ello presenta algunas de las alternativas que han apropiado las víctimas para superar las secuelas que este tipo de violencias deja en los cuerpos de las mujeres y en general en el tejido social.

Por ejemplo, señala que algunas de las mujeres optaron por enfrentar a sus victimarios acudiendo a la denuncia pública, otras han decidido seguir adelante en la defensa de sus territorios con la idea de que las jóvenes y niñas de sus comunidades no sean también víctimas **de los paramilitares o los actores armados que operan en sus territorios, entre otras.**

Si bien, solamente hasta el 24 de noviembre se conocerá el informe completo, quedan planteadas tanto las preguntas como las alternativas de respuesta a una situación que es abrumadora y que debe estar en la primera línea de la recién creada Comisión de la Verdad, no solamente para que se esclarezcan estos crímenes sino también para que se abra el camino de la No Repetición.

<iframe id="audio_22059230" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22059230_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
