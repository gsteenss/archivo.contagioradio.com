Title: ¿Puede Colombia reducir el pago de la deuda externa, en el PND?
Date: 2015-03-03 19:31
Author: CtgAdm
Category: Economía, Nacional
Tags: alirio uribe, Angela Maria Robledo, Deuda Externa en Colombia, Iván Cepeda, Plan Nacional de Desarrollo Colombia 2014-2018
Slug: puede-colombia-reducir-el-pago-de-la-deuda-externa-en-el-pnd
Status: published

###### Foto: Imagen Audiencia Pública 

<iframe src="http://www.ivoox.com/player_ek_4159905_2_1.html?data=lZaim56UeY6ZmKiakpiJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqezstrJqMafpNTZ0dLGrcKf08rR18jNtozZzZDdw8zTb8XZjNHOjcnJucXVjMrl1srWssKZk6iYx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ángela María Robledo, Representante a la Cámara] 

La alianza de representantes a la Cámara y Senado, conformada por **Ángela María Robledo, Alirio Uribe, Alberto Castilla, Víctor Correa e Iván Cepeda**, en colaboración con la Plataforma Ciudadana por la Auditoría de la Deuda Pública, están convocando para el próximo **jueves 5 de marzo a la Audiencia Pública "Por una Paz sin Deuda con los Derechos"** para discutir la posibilidad de Colombia de no pagar la deuda pública.

**Colombia gasta entre el 25% y el 39% de su PIB en el pago de la deuda externa** ¿Sería posible tomar alternativas como ha hecho Grecia, Ecuador o Argentina, de llevar al mínimo ese pago para destinar el presupuesto a inversión social, como salud, educación, y desarrollo agrario?

El proyecto de Plan Nacional de Desarrollo 2014-2018 presentado por el gobierno de Juan Manuel Santos al Congreso, proyecta el gasto de al rededor 200 billones en el pago de la deuda pública, que contrasta con la inversión de menos de 25 billones que tendría, por ejemplo, la educación.

Para el grupo de parlamentarios, esta proyección del Plan Nacional de Desarrollo contrarresta con la intensión de construir paz con justicia social en Colombia, y "la pregunta de fondo es por la visión de paz que expresa el articulado radicado en el Congreso".

Para la Representante Ángela María Robledo, esta audiencia se plantea como la continuidad de la Audiencia Pública **"Plan Nacional de Desarrollo, Derechos sociales y postconflicto"** realizada el pasado 19 de febrero, en la que se evidenció que el principio de sostenibilidad fiscal y el pago de la deuda externa asfixiarían la garantía de los derechos de los colombianos y colombianas; los representantes buscan establecer, de la mano de académicos y líderes sociales, si es posible virar en el modelo neoclásico de la economía que prioriza el gasto en la deuda pública sobre el buen vivir de la sociedad.

La Audiencia se llevará a cabo en el **auditorio Luis Guillermo Vélez del Congreso, desde las 8:30 de la mañana.**
