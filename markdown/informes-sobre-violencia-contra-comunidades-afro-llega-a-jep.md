Title: Informes de violencia contra comunidades afro son entregados a la JEP
Date: 2019-05-24 16:04
Author: CtgAdm
Category: DDHH, Judicial
Tags: afrocolombianas, AFRODES, jurisdicción especial para la paz
Slug: informes-sobre-violencia-contra-comunidades-afro-llega-a-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-109.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/violencia-en-el-naya.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión Justicia y Paz] 

Cinco organizaciones afrocolombianos entregaron una serie de informes preliminares al Sistema Integral de Verdad, Justicia, Reparación y No Repetición el pasado 24 de mayo que detallan como estas comunidades fueron victimadas de manera diferencial en el marco del conflicto armado.

Estos documentos incluyen información sobre graves violaciones de derechos humanos como el desplazamiento forzado, asesinato selectivos, violencia sexual y el reclutamiento de jóvenes así como las afectaciones que sostuvieron estas comunidades a su cultura, territorio y sus procesos organizativos.

Luz Marina Becerra, coordinadora del movimiento COMADRE de Afrodes, indicó que las comunidades esperan que las entidades del Sistema Integral, como la Jurisdicción Especial para la Paz (JEP), la Unidad de Búsqueda y la Comisión de la Verdad, adelanten sus respectivas investigaciones **teniendo en cuenta un enfoque étnico, racial y de género.**

\[caption id="attachment\_67791" align="aligncenter" width="436"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-109-300x197.png){.wp-image-67791 width="436" height="286"} Luz Marina Becerra- AFRODES\[/caption\]

> "Gran parte de las graves violaciones de los derechos humanos de que hemos sido víctimas el pueblo negro en Colombia obedece al racismo estructural", dijo la lideresa

"Si comparamos las cifras de población afrocolombiana desplazadas con la cifra de población afrocolombiana que aparece en el censo nacional, esto muestra claramente el impacto desproporcionado y diferencial dentro el conflicto armado en nuestras comunidades", afirmó la coordinadora.

Según la última encuesta del DANE, hay más de 4.311.000 afrocolombianos, es decir, la población negra constituye el 10,5% de la población total del país. Sin embargo, como Becerra indicó, hay aproximadamente 2.000.000 de afrocolombianos desplazados, un 30 % del total número de personas desplazadas. (Le puede interesar: "[Comunidad afro denunciará ante la CIDH violaciones a los DDHH](https://archivo.contagioradio.com/afro-cidh-violaciones-dd-hh/)")

"**Esto significa que la mitad de nuestra población hoy se encuentra desplazada**, desarraigada de sus territorios, llegando a las gran ciudades para engrosar las largas filas de miseria a la indiferencia del Estado y la falta de implementación de políticas públicas con enfoque diferencial", sostuvo Becerra.

La lideresa concluyó que la justicia transicional [genera una gran esperanza para las comunidades afrocolombianas, en particular las mujeres víctimas de la violencia sexual, para que se avance con las investigaciones y cese la impunidad.]"**Confiamos en la JEP y por eso necesitamos que esta institución pueda seguir avanzando**", afirmó.

<iframe id="audio_36310114" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36310114_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
