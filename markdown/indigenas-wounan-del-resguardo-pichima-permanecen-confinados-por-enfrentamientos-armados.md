Title: 500 indígenas Wounaan del resguardo Pichimá permanecen confinados por enfrentamientos armados
Date: 2019-06-02 16:30
Author: CtgAdm
Category: Comunidad, Nacional
Tags: combates, fuego cruzado, indígenas wounan, litoral san juan
Slug: indigenas-wounan-del-resguardo-pichima-permanecen-confinados-por-enfrentamientos-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-03-at-11.41.22-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

La tarde de este domingo 2 de junio, comunidades **indígenas Wounaan del resguardo Pichimá Quebrada**, en al norte del municipio litoral San Juan, Chocó, alertaron sobre **enfrentamientos armados desde las 11 a.m. que han sometido al confinamiento por lo menos a 500 indígenas** que tratan de resguardarse en sus viviendas.

Primeras informaciones advierten que se trataría de **combates entre disidencias de las FARC con otro grupo armado de la región, posiblemente los Urabeños**. Una disputa que podría generar un desplazamiento en el corto plazo.

Adicionalmente se conoció que la **Fuerza Naval del Pacífico** se encuentra movilizando tropas hacia la zona, sin embargo, este anuncio atemoriza a las comunidades pues el hecho representaría mayor represión contra ellos y ellas dada la **actuación ineficaz de la fuerza pública contra los grupos criminales que operan en el territorio**.

El resguardo compuesto por 700 personas, 120 familias en promedio, hace un llamado al gobierno regional y nacional, y entidades competentes para mitigar la situación ocurrida **en cercanías del colegio La Unión de Pichimá y en el sitio Cementerio sagrado**. Así mismo aseguran que no cuentan con medios de comunicación directa con el exterior de la comunidad.

En la tarde del lunes, ya con la presencia de fuerzas armadas se registraron los primeros desplazamientos hacia el casco urbano del municipio de Litoral San Juan, a donde las comunidades se movilizan en busca de salvaguardar sus vidas.

###### ![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/WhatsApp-Image-2019-06-03-at-11.41.22-AM-246x300.jpeg)

###### Reciba toda la información de Contagio Radio en [[su correo]
