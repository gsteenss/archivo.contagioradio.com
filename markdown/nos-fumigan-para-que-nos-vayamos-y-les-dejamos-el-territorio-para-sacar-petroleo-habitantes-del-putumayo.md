Title: "Nos fumigan para que nos vayamos y les dejamos el territorio para sacar petroleo" Habitantes del Putumayo
Date: 2014-12-26 16:05
Author: CtgAdm
Category: Otra Mirada
Slug: nos-fumigan-para-que-nos-vayamos-y-les-dejamos-el-territorio-para-sacar-petroleo-habitantes-del-putumayo
Status: published

Crédito foto: radiomacondo.fm

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsQdqPl5.mp3)

A las fumigaciones que el gobierno colombiano reiniciara en el Putumayo el 4 de noviembre, violando los acuerdos alcanzados en la mesa de negociación entre el Estado y las organizaciones sociales de este departamento, **se suma este 11 de noviembre la presentación del proyecto de de la petrolera Amerisur, para construir una vía en el departamento que comunique la plataforma petroletra para la explotación en el Putumayo**.

La comunidad de Monteverde ha expedido un comunicado en el que se opone a cualquier forma de actividad petrolera en el sector, y denuncian que el tema de "Hidrocarburos y medio ambiente" fue uno de los discutidos en la mesa de negociación con el gobierno, en que las comunidades ya habían expresado su rechazo a este tipo de iniciativas.

Los habitantes de la región denuncian que justo en el momento en que se estaba llevando a cabo la reunión de socialización del proyecto por parte de la petrolera Amerisur, pasaron varias veces aviones fumigadores. "Nos fumigan a ver si nos vamos y les dejamos el territorio para que hagan lo que les de la gana", enfatizaron los habitantes del Putumayo.
