Title: Las conquistas de Verónica Mendoza y el Frente Amplio en Perú
Date: 2016-04-12 13:24
Category: El mundo, Otra Mirada
Tags: Elecciones Peru, Frente Amplio Perú, Verónica Mendoza
Slug: las-conquistas-de-veronica-mendoza-y-el-frente-amplio-en-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Vero-e1460484736944.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Miguel Cha 

###### [12 Abr 2016] 

Rodeada por un importante número de seguidores, equipo de trabajo y familia, la candidata presidencial **Verónica Mendoza** expresó su satisfacción por lo que significa lo alcanzado tras las elecciones del domingo: **“**El Frente Amplio es el frente del pueblo ha nacido para quedarse, ha nacido para hacer historia como ya estamos haciendo historia hasta el momento con el esfuerzo de todos y cada uno de ustedes. ¡Aquí seguimos de pie!**”**.

El entusiasmo de la joven aspirante cusqueña de 35 años de edad no es para menos. Con más del 95 % de las actas procesadas, la votación conseguida cercana al **19% para una campaña austera e independiente** y los **22 escaños alcanzados por el Frente Amplio en el Congreso de la República**, representan la mejor participación de la organización política en sus 30 años de existencia en el Perú.

La periodista Paloma Duarte, quien ha seguido de cerca todo el proceso electoral, asegura que **la figura de Mendoza “ha logrado consolidar a una izquierda joven y renovada”** que no ha necesitado de alianzas políticas y que no ha perdido durante la campaña la convicción por los cambios profundos que pretenden alcanzar desde la izquierda democrática.

Así mismo, la comunicadora asegura que la elección de los congresistas por el Frente Amplio es relevante al ser **en su mayoría integrantes de movimientos sociales en las regiones que representan** y en la capital del país donde alcanzarían 4 curules con la socióloga **Marisa Glave** del movimiento Tierra y libertad, **Indira Huilca** hija del ex dirigente sindical Pedro Huilca asesinado durante el gobierno Fujimori, **Manuel Dammert** quien trabaja en la defensa de los recursos particularmente los minerales,  y **Martina Portocarrero** cantante folclórica popular.

De cara a la [segunda vuelta del próximo 5 de junio](https://archivo.contagioradio.com/entre-fujimori-y-kuczinsky-se-definiria-la-presidencia-en-peru/) entre los candidatos Keiko Fujimori de ‘Fuerza Popular’ y Pedro Pablo Kuczinsky (PPK) de ‘Peruanos por el Kambio’, la periodista considera que a los votantes no les queda otra alternativa que **elegir entre la “ultra derecha y la ultra derecha”**; recordando el apoyo brindado por parte de Kuczinsky a la candidatura fujimorista de 2011, frente al actual presidente Ollanta Humala.

Finalmente, ante la posibilidad de establecer **una alianza entre la izquierda y PPK** para la segunda vuelta, Duarte **no la considera una opción cercana por el perfil y antecedentes del candidato**, y expresa que la discusión se centra en el cómo hacer para que cualquiera de los dos aspirantes “por lo menos tengan la intención de hacer compromisos públicos para **mantener los derechos mínimos**” refiriéndose a la preocupación de lo que vendría en esa materia durante los próximos 5 años y **el nivel represivo que ambos candidatos encarnan**.

##### [Paloma Duarte, periodista] 

<iframe src="http://co.ivoox.com/es/player_ej_11141480_2_1.html?data=kpaelpaYfJGhhpywj5WXaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncbHVzdTaw5CoucLm1cqSlKiPtMbmytTRy9jYpYzExteSpZiJhqKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
