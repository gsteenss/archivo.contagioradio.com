Title: "Marielas Traum" una novela sobre la resistencia en Cacarica
Date: 2015-03-13 21:18
Author: CtgAdm
Category: Hablemos alguito
Tags: Alexandra Huk, Kolko, Marielas Traum, Novela sobre las comunidades de Cacarica
Slug: marielas-traum-una-novela-sobre-la-resistencia-en-cacarica
Status: published

###### Foto:Contagioradio.com 

###### **Hablemos Alguito:** 

<iframe src="http://www.ivoox.com/player_ek_4212580_2_1.html?data=lZeelJqcdI6ZmKiakpaJd6KkkYqgo5eXcYarpJKfj4qbh46kjoqkpZKUcYarpJKy0NnWqdfd1NnOjcjTsozgwpDO19nTtsKfxcrZjdHNptPjjLLO1M7JsMLnjLnfw9rRcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Traum** tiene dos significados en alemán **sueño o pesadilla**. De esta manera la autora del libro ha querido expresar los dos sentimientos que han prevalecido en la historia de la resistencia de las comunidades del Cacarica en el Chocó colombiano.

**Sueño**, porque es el sentimiento de las personas que han sido desplazadas y que quieren **retornar y continuar con sus vidas de antes de que tuvieran que huir**. Pero también la pesadilla que fue la masacre, ver cómo asesinaron a sus seres más queridos, cómo los han despojado de lo único que tiene un campesino en esta vida, es decir su tierra.

Conversando con Alexandra Huk, autora del libro, escuchamos muchas anécdotas que nos crean una imagen de lo que sucedió el fatídico día en el que tuvieron que salir. Sin embargo no es lo único ante lo que nos encontramos con su lectura. El choque entre mundos, de una persona que nunca ha visto a un extranjero pasando por otra persona que viene de un mundo totalmente distinto y se encuentra con una realidad totalmente distinta.

La autora pretende **visibilizar sus experiencias vividas como acompañante internacional de las organizaciones de DDHH de Colombia,  PBI y Justicia y Paz** en las comunidades de **Cacarica afectadas por la operación "Génesis"**, perpetrada el ejército colombiano con el apoyo de grupos paramilitares, donde fueron desplazadas más de 3.500 personas.

Usando el formato novelístico Alexandra, relata con otros personajes la cotidianidad de la vida en las comunidades y las consecuencias del desplazamiento sobre ellas. Una historia de llantos, alegrías y esperanza, cargada de cultura e idiosincrasia chocoana.

El libro está escrito en alemán, a la espera de ser traducido al español. Según la autora el **objetivo principal** de la novela es que en **otras partes del mundo se conozca a modo de memoria histórica una de las peores etapas del terrorismo de estado colombiano**.
