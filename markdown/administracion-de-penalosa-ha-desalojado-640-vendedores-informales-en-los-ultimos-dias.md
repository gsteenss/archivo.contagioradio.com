Title: Administración de Peñalosa ha desalojado 640 vendedores informales
Date: 2016-04-28 14:14
Category: Economía, Nacional
Tags: Alcaldía de Bogotá, Enrique Peñalosa, Vendedores informales
Slug: administracion-de-penalosa-ha-desalojado-640-vendedores-informales-en-los-ultimos-dias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/vendedores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Inti Asprilla 

###### 28 abr 2016 

El pasado 26 de abril, 230 vendedores informales de la calle 72 fueron desalojados por orden de la Alcaldía de Bogotá, además, días atrás en la carrera séptima fueron desplazados 130, y otros 280 fueron retirados de su lugar de trabajo en la calle 19 con carrera cuarta, para un total **640 personas desalojadas sin ningún tipo de solución de empleo por parte del distrito,** como lo denuncia Jhon Rivera, vocero de los vendedores informales de la ciudad.

“**El alcalde miente a la ciudadanía, a los medios y a los jueces cuando dice que hemos recibido algún tipo de solución,** pero la solución que tiene el distrito es escondernos en lugares aislados de la ciudad donde no entran ni las moscas a comprar”, dice Rivera, quien cuenta que los puntos que ha escogido la Alcaldía para reubicar a los vendedores son: calle 200 cerca al cementerio Jardines La Inmaculada; Usme; y la bodega 22 en San Andresito de la 38, “un lugar que no ha dado resultados”, expresa.

Según cuenta el vocero de los vendedores informales, **el 50% de ellos son adultos mayores, y además el 90% están reportados en Data Crédito,** por lo que es muy difícil conseguir algún tipo de crédito o un empleo, lo que ha llevado  al desespero de los vendedores que se ven obligados a buscar comida en las basuras, incluso, uno de ellos intentó suicidarse en días pasados.

Alicia, cabeza de hogar con tres hijos y un nieto, se encuentra en la misma situación. Ella ganó una tutela en la que se obliga al distrito a reubicarla en un lugar para que pueda trabajar, sin embargo, sus derechos no han sido restituidos y en cambio **va a tener que someterse a un sorteo para lograr obtener un puesto de trabajo en los lugares que ha estipulado la Alcaldía**, “No hemos podido  llevar el diario para la casa, estoy endeudada con el banco, no sé qué hacer”, dice Alicia.

**Las soluciones que se han ofrecido ya llevan 17 años,** y no son garantía para los vendedores, es por ello, que piden que se abra una mesa de trabajo y concertación entre la Alcaldía y los vendedores ambulantes, de manera que se pueda establecer una política pública encaminada al emprendimiento de las personas que viven de las ventas ambulantes y no se continúe con “**una política de atropello como sucedió en la anterior administración de Enrique Peñalosa”.**

Para dar a conocer esas exigencias, los vendedores llevan movilizándose desde inicios de este año, y para el 28 de abril realizaron un acto simbólico en la calle 71 con carrera 11. Además, para el **próximo 1 de mayo se tiene planeado la realización de un plantón a las 9 de la mañana frente al Planetario de Bogotá.**

<iframe src="http://co.ivoox.com/es/player_ej_11342028_2_1.html?data=kpaglpeUdpmhhpywj5aXaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncbfZz8nSxtTWqdSfwsvSxdnFqNDnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
