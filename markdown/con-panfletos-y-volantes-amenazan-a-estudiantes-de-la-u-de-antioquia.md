Title: Con panfletos y carteles amenazan a estudiantes de la U. de Antioquia
Date: 2019-01-23 17:10
Author: AdminContagio
Category: DDHH, Educación
Tags: Amenazas, estudiantes, Universidad de Antioquia
Slug: con-panfletos-y-volantes-amenazan-a-estudiantes-de-la-u-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Amenazas-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Aguacate humano - Com. Universitaria] 

###### [23 Ene 2018] 

Luego de la jornada de protestas realizadas en la Universidad de Antioquia en contra del levantamiento de la mesa de negociaciones entre Gobierno y ELN, cerca al claustro universitario aparecieron carteles amenazantes contra los estudiantes de la Institución en los que se afirma la existencia la relación de jóvenes con grupos armados al margen de la Ley.

En un comunicado firmado por diferentes organizaciones defensoras de derechos humanos, se sostiene que el pasado 22 de enero, el gobernador de Antioquia Luis Pérez Gutiérrez declaró en medios de comunicación empresariales que habría celulas del ELN al interior del recinto universitario, aseveración que convierte en objeto del conflicto a los estudiantes de la Universidad.

Posterior a esta declaración, este miércoles fueron repartidos volantes y pegados carteles amenazantes con frases como **“Muerte al comunista, no más guerrilleros en la UdeA”,** sobre la calle Barranquilla en inmediaciones de la Universidad. La autodenominada **Brigada de seguridad Anticomunista,** es la agrupación que se adjudica la elaboración de los mensajes, amenazando **a quienes aseguran hacen parte del Partido Comunista o a posibles milicianos o guerrilleros**.

Sobre uno de los puentes peatonales de la vía apareció un cartel donde podía leerse  **“No queremos más guerrilleros en la UdeA… Les llegó su hora”**. Por su parte, las autoridades han asegurado que adelantarán las investigaciones sobre los autores de las amenazas, pero los estudiantes han pedido que se active el Sistema de Alertas Tempranas (SAT) de la Defensoría del Pueblo y se alerte a las autoridades civiles, policivas y militares sobre este hecho para evitar que se repita nuevamente.

[Alerta Temprana Universidad de Antioquia](https://www.scribd.com/document/398160157/Alerta-Temprana-Universidad-de-Antioquia#from_embed "View Alerta Temprana Universidad de Antioquia on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_76262" class="scribd_iframe_embed" title="Alerta Temprana Universidad de Antioquia" src="https://es.scribd.com/embeds/398160157/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-BjZbGtAwLSGnOK3qsmv5&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
