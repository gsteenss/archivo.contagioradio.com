Title: ONU y Celac participarán en la verificación del Cese al fuego y dejación de armas
Date: 2016-01-19 15:11
Category: Nacional, Paz
Tags: Cese al fuego y dejación de armas, Diálogos de La Habana, Proceso de paz en Colombia
Slug: onu-y-celac-verificaran-cese-al-fuego-y-dejacion-de-armas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/paises-garantes-paz-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [19 Ene 2016]

Los equipos negociadores del Gobierno y las FARC EP anunciaron este martes la creación de un organismo tripartito, con participación de la Organización de las Naciones Unidas ONU y los países miembros de la Comunidad de Estados Latinoamericanos y Caribeños CELAC, encargado de verificar el cese al fuego y la dejación de las armas como parte del proceso de paz.

La tarea que iniciará labores por 12 mese a partir de la firma del acuerdo, será la de verificar el cese de hostilidades en el marco del conflicto y la dejación de las armas, decisión que corrobora el compromiso de ratificar los acuerdos que se alcancen en la mesa de negociación.

El texto del comunicado conjunto leído por el garante cubano Rodolfo Benitez es el siguiente:

*Comunicado Conjunto \# 65*  
*La Habana, 19 de enero de 2016*

*El Gobierno de la República de Colombia y las Fuerzas Armadas Revolucionarias de Colombia, Ejercito Del Pueblo, FARC-EP:*

*Reiteran su compromiso con las negociaciones para lograr un Acuerdo Final para la Terminación del Conflicto y la Construcción de una Paz Estable y Duradera (Acuerdo Final), incluido un acuerdo sobre el cese al fuego y de hostilidades bilateral y definitivo y la dejación de las armas.*

*Así mismo, reiteran su compromiso con la implementación de todos los acuerdos contenidos en el Acuerdo Final y la puesta en marcha de mecanismos eficaces de monitoreo y verificación, con acompañamiento internacional, que garanticen el pleno cumplimiento de los compromisos adquiridos.*

*Hemos decidido crear un mecanismo tripartito de monitoreo y verificación del acuerdo sobre el cese al fuego y de hostilidades bilateral y definitivo y la dejación de las armas, que genere confianza y de garantías para su cumplimiento, conformado por el Gobierno de Colombia, por las FARC-EP y por un componente internacional, quien preside y coordina el mecanismo en todas sus instancias, dirime controversias, realiza recomendaciones y presenta informes, y que iniciará sus labores una vez se haya llegado a ese acuerdo. Respecto a la dejación de las armas el mismo componente internacional la verificará en los términos y con las debidas garantías que se establecerán en los protocolos del acuerdo.*

*Hemos acordado que ese componente internacional será una misión política de la ONU integrada por observadores de países miembros de la CELAC.*

*Con ese propósito, hemos decidido solicitar al Consejo de Seguridad de la ONU la creación desde ya de esa misión política con observadores no armados por un período de 12 meses, prorrogables a petición del Gobierno Nacional y las FARC-EP, y así como a los países miembros de la Comunidad de Estados Latinoamericanos y Caribeños, CELAC, su disponibilidad para contribuir en dicha misión que será conformada por Naciones Unidas.*

*Así mismo, solicitan que la Misión inicie los preparativos necesarios, en estrecha coordinación y colaboración con el Gobierno de Colombia y las FARC-EP, para su despliegue. Los observadores internacionales gozarán de plenas garantías de seguridad.*

*Agradecemos a las Naciones Unidas y a la CELAC la disposición para apoyar a Colombia en la búsqueda de la paz. *
