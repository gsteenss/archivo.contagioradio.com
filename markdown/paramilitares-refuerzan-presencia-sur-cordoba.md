Title: Paramilitares refuerzan su presencia en el Sur de Córdoba
Date: 2016-12-16 14:04
Category: DDHH, Nacional
Tags: amenazas a lideresas campesinas, amenazas paramilitares, Asociación de Campesinos del Sur de Córdoba, Autodefensas Gaitanistas de Colombia, Desplazamiento forzado
Slug: paramilitares-refuerzan-presencia-sur-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Paramilitares_Sur-de-Córdoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RadioMacondo] 

###### [16 Dic 2016] 

La Asociación de Campesinos del Sur de Córdoba ASCSUCOR, denunció a través de un comunicado el desplazamiento forzado de la lideresa campesina Petrona Isabel Polo, debido a amenazas de las Autodefensas Gaitanistas. La Asociación también denuncia que **el abandono estatal ha generado un recrudecimiento del paramilitarismo en sus territorios.**

Andrés Chica vocero de la Asociación Campesina, manifestó que la mayor preocupación de la comunidad es que “con la ausencia de instituciones estatales y el pre agrupamiento de las FARC en el territorio se ha aumentado la presencia de paramilitares, **la semana pasada pasaron de ser 160 hombres a 400 paramilitares que andan atemorizando y rondando** por Tierralta, Tierradentro, Puerto Libertador y Montelibano”.

Chica también aseguró que Petrona Isabel quien es miembro del Comité de Mujeres de ASCSUCOR, “fue extorsionada y amenazada en su propia casa por alias ‘el pollo’ quien le cobró una vacuna de 3 millones, esta humilde campesina de Tierradentro le dijo que no tenía como dar ese dinero y **el hombre procedió a decirle que debía abandonar su casa y sus labores organizativas si no quería perder la vida”.**

Días después fue introducido bajo la puerta un **panfleto intimidatorio que insta al reclutamiento y lesiona la seguridad de los integrantes** de la Asociación Campesina.

\[caption id="attachment\_33854" align="alignnone" width="960"\]![Sur de Córdoba](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Panfleto-Juan-Jose-18_12_16.jpeg){.size-full .wp-image-33854 width="960" height="1280"} Panfleto de las AGC intimidando a integrantes de ASCSUCOR\[/caption\]

### **Los paramilitares nunca se han ido de Córdoba** 

“La comunidad ha podido corroborar la presencia y accionar de alias Otoniel, alias El Indio y alias El Pollo (…) a alias El Pollo **lo capturó la Fiscalía y al día siguiente fue liberado por la misma fiscalía seccional de Montelibano a pesar que hay denuncias y pruebas”** puntualizó Chica.

De igual modo el integrante de ASCSUCOR reveló que “la misma comunidad también comprobó que los paramilitares se hacían pasar por guerrilleros del ELN para cobrar doble vacuna (…) **como asociación campesina desvirtuamos las afirmaciones de la Defensoría y la Fiscalía sobre la presencia del ELN en el territorio”.**

Andrés Chica enunció que “los paramilitares nunca han salido de Córdoba, por el contrario, se han fortalecido y todo ante los ojos del ejército y las instituciones del Estado, el campesinado está sólo y ¿quien va a contrarrestar a esos grupos? Hacemos un llamado a la Ficalía, a la Defensoría y a MinDefensa **para que de respuesta a las denuncias que hemos puesto sobre todos estos hechos”.**

Por último, la Asociación señaló que los principales responsables de hostigamientos y muertes subsiguientes a las denuncias, serían Juan Manuel Santos y Diego Fernando Mora, director de la Unidad Nacional de Protección, además **manifestaron que de no obtener prontas respuestas y soluciones a la situación de seguridad retomarían las movilizaciones y bloqueos de vías principales. **

<iframe id="audio_15079458" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15079458_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
