Title: La firma del nuevo acuerdo, una esperanza para las víctimas
Date: 2016-11-24 14:52
Category: Otra Mirada, Paz
Tags: Conpaz, firma del acuerdo, implementacion acuerdos, Víctimas Colombia
Slug: la-firma-del-nuevo-acuerdo-una-nueva-esperanza-para-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Nov 2016] 

Frente a la firma del nuevo acuerdo de paz entre las FARC-EP y el Gobierno Nacional, víctimas de la organización Comunidades Construyendo Paz CONPAZ, manifestaron que con este suceso histórico **muchos anhelos de las** **víctimas se pueden materializar, a pesar de las negativas y el pesimismo de algunos sectores del país.**

Rodrigo Castillo integrante de CONPAZ, aseguró que reciben con alegría la firma de los acuerdos y que el reto más grande que está por delante es **“lograr que toda la población y las víctimas conozcan el contenido de los acuerdos** y las modificaciones que tuvo, **que lo apropiemos como un instrumento de paz”.**

Por otra parte Luz Marina Cuchumbe lideresa campesina de Inzá Cauca, también integrante de CONPAZ y a quien una ejecución extrajudicial del Ejército en 2008 le arrebató a su hija de 17 años, señaló que la firma de los acuerdos significa **“el comienzo de la paz que todo colombiano quiere haya sido víctima o no,** porque Colombia necesita esa paz para las futuras generaciones”. Le puede interesar: [Este es el discurso del presidente santos en firma de Acuerdo de Paz.](https://archivo.contagioradio.com/este-discurso-del-presidente-santos-firma-acuerdo-paz/)

### **Una nueva esperanza para las víctimas** 

Luz cuenta que este nuevo paso es fundamental pues “no queremos volver a las épocas donde estábamos atemorizados porque campesino que hablaba campesino que era muerto”. Resalta que **este deber ser un verdadero compromiso de las partes porque “después de la petición de perdón en 2012 se presentaron otros hechos victimizantes** y lo volvimos a ver en días pasados con los líderes asesinados”.

**“Seguiré cultivando los sueños de mi hija”** fue la frase que Luz Marina le dijo al general Óscar Naranjo en 2012, cuando gracias a su ardua labor de denuncia por el asesinato de su hija y el de otros jóvenes, **en 2012 el Gobierno realiza un acto de petición de perdón en compañía de las Fuerzas Militares y las FARC-EP en el municipio.**

Por último la lideresa afirma que si bien “a muchas víctimas nos da miedo denunciar, el dolor debe movernos, debe hacernos hablar pues **es lo que asegura nuestro derecho a la verdad, a saber lo que pasó y por qué lo hicieron** (…) las víctimas esperamos que **se ilumine la mente y el corazón de quienes han hecho tanto daño en Colombia”.**

<iframe id="audio_13959845" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13959845_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_13959832" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13959832_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
