Title: Dirigente de la USO es víctima de atentando en Barrancabermeja
Date: 2016-09-14 17:27
Category: DDHH, Nacional
Tags: colombia, Estigmatización, movimientos sindicales, Sindicales
Slug: dirigente-de-la-uso-es-victima-de-atentando-en-barrancabermeja
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/moir.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Moir] 

###### [14 de Sept de 2016] 

El 13 de septiembre en horas de la tarde, el vehículo asignado a **Alex Castro**, dirigente de la Unión Sindical Obrera en Barrancabermeja, Santander, **fue interceptado y atacado con armas de fuego por parte de desconocidos,** el sindicalista logró salir ileso del atentado debido a la rápida reacción de su esquema de seguridad y de las personas que se encontraban con él.

Alex Castro estaba en el corregimiento El Centro, en el poso 2235 de la firma contratista "Erazo Valencia", **en una reunión con trabajadores de esta empresa,** sobre el medio día, cuando se desencadenó el ataque.

Anteriormente Castro [ya había sido víctima, en dos ocasiones, de atentados sicariales](https://archivo.contagioradio.com/colombia-es-uno-de-los-10-paises-mas-peligrosos-para-los-sindicalistas/), sin que hasta el momento se produzcan resultados en cuanto a individualización o captura de los responsables. Según la información entregada por la USO, las autoridades de Policía se encuentran adelantando investigaciones frente a los atacantes.

De acuerdo con el comunicado de prensa de la Unión Sindical de Trabajadores, otros dirigentes de la organización **han sido objetos de amenazas de muerte en los últimos días por parte de desconocidos**, tal es el caso de Rodolfo Vecino y Jhon Alexader Rodríguez.

La USO exige a  las autoridades que **esclarezcan prontamente estos sucesos** y al gobierno Nacional que otorgue garantías para desarrollar el[libre ejercicio de la libertad sindical](https://archivo.contagioradio.com/roban-base-de-datos-con-informacion-sobre-asesinatos-de-sindicalistas-en-colombia/), así como el desmonte de las estructuras criminales que han victimizado durante décadas a los líderes obreros en Colombia.

Y es que el caso de Alex Castro no ha sido el único, durante el conflicto armado en Colombia, la defensa de los derechos laborales fue estigmatizada y vivió políticas de exterminío, así lo han denunciado organizaciones como la USO, la CUT, entre otras; muestra de ello son los **más de  3.000 dirigentes sindicales y líderes obreros que han sido asesinados  en los últimos 30 años.** Por este motivo el gobierno Nacional expidió el [decreto 624 de 2016, en donde reconoce la victimización de la actividad sindical](https://archivo.contagioradio.com/decreto-reconoce-a-trabajadores-como-victimas-del-conflicto-armado/) y por lo tanto toma acciones reparadoras frente a estas organizaciones y sus miembros.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
