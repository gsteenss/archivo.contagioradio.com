Title: Por lo menos seis mil personas en condición de calle siguen sin poder resguardarse del Covid-19
Date: 2020-04-13 20:57
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Bogotá, Habitantes de calle
Slug: por-lo-menos-seis-mil-personas-en-condicion-de-calle-siguen-sin-poder-resguardarse-del-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Ciudadanos-en-condición-de-calle.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @AndresLM2018 {#foto-andreslm2018 .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según alertó el Fray Gabriel Gutiérrez, integrante de la Fundación Callejeros de la Misericordia, Bogotá tiene una capacidad máxima estimada para brindar atención a tres mil habitantes de calle, lo que deja sin lugar a más de 6 mil personas. Sin embargo, esta población es solo una de las más vulnerables en medio de la crisis generada por la pandemia del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **De la crisis de salud a la crisis humanitaria**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el censo del DANE, en Bogotá hay 9.538 ciudadanos en condición de calle, y de acuerdo al Fray, en Colombia la cifra alcanzaría las 40 mil personas. El religioso sostuvo que en Colombia no hay capacidad para atender estas personas, porque no hay una política pública para ello, y por lo tanto todos los esfuerzos locales probablemente no serán suficientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ese sería el caso de Bogotá, cuya tasa de atención en centros de atención diurna y nocturna según cálculos de Gutiérrez alcanzaría como máximo las 3 mil personas. Además, señaló que si se toma en cuenta la acción de instituciones distritales y organizaciones sociales, lo máximo que estas personas llegan a recibir en la calle es un plato de comida al día. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el Fray mencionó que el consumo de drogas por parte de algunos habitantes de calle ha sido aprovechado por los expendios en medio de esta cuarentena, elevando los precios de la llamada bicha de dos mil hasta a nueve mil pesos. Por otra parte, cuestionó que la Alcaldía no tenga programas como el de los Centros de Atención Médica a Drogodependientes (CAMAD), lo que puede ser un problema, en tanto no se trata el síndrome de abstinencia. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No se trata solamente de ciudadanos habitantes de calle

<!-- /wp:heading -->

<!-- wp:paragraph -->

Fray sostuvo que lo que se está viviendo en Bogotá no se reduce únicamente a personas en condición de calle: se trata de cachivacheros (vendedores de cosas usadas) que no tienen con qué pagar un lugar de vivienda, ciudadanos venezolanos que vivían del día a día y como ellos, grupos de personas que se sostienen con el ‘rebusque’. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También recordó que el distrito intentó abrir centros de acogida para la población migrante y refugiada de Venezuela pero en algunos barrios los vecinos se resistieron a recibir a estas personas. A esta expresión de aporofobia se suman las denuncias que Gutiérrez ha recibido sobre golpizas por parte de la Policía a quienes no pueden cumplir la cuarentena y deben pasarla en la calle, lo que para él es una muestra de que los proyectos sociales, económicos y políticos están perdidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para recuperarlos, aseguró que se debe asumir un enfoque de derechos para proteger a las poblaciones vulnerables, además de acudir a la solidaridad de las personas para apoyar a quienes lo necesitan. (Le puede interesar: ["La crisis económica no se resuelve con limosnas: Alirio Uribe Muñóz"](https://archivo.contagioradio.com/la-crisis-economica-no-se-resuelve-con-limosnas-alirio-uribe-munoz/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
