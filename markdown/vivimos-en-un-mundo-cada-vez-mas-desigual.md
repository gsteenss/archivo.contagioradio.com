Title: "Vivimos en un mundo cada vez más desigual": OXFAM
Date: 2016-01-18 12:46
Category: Economía, Otra Mirada
Tags: Desigualdad en el mundo, Oxfam, Paraisos fiscales
Slug: vivimos-en-un-mundo-cada-vez-mas-desigual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/neglected-world-hunger.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [18 Ene 2016]

De acuerdo con cifras difundidas por la ONG británica OXFAM en su informe “Una economía al servicio del 1%", la desigualdad en el mundo va en incremento. En la actualidad **los más ricos del mundo poseen tanto como la mitad más pobre de la población mundial**, situación agravada por la evasión impositiva y la proliferación de paraísos fiscales.

En el informe presentado este lunes, en vísperas de la realización del Foro Económico de Davos (Suiza), se pone de manifiesto que **el patrimonio acumulado por el 1% de las personas más ricas del mundo superó en 2015 al del 99% restante**, situación que la ONG había previsto ocurriría en 2016 y no un año antes como sucedió según registra el documento.

De 388 personas que en 2010 poseían tanto capital como la mitad más pobre de la población mundial, **en los últimos doce meses pasaron a ser 62**, cálculo que permite concluir a los investigadores que la brecha entre la franja de los más ricos y la del resto de la población se ha profundizado “de manera espectacular”.

Las consecuencias nefastas que para el destino de la humanidad tienen los datos presentados, permiten que la ONG recomiende una serie de acciones, que deben **empezar con la lucha contra la evasión**, en medio de la que denominan “era de los paraísos fiscales”; a los que 9 de cada 10 empresas, socias del Foro económico mundial, están vinculadas.

Con la evasión fiscal, los estados dejan de percibir ingresos que podrían ser invertidos en salud, educación, por lo que la organización insta a establecer políticas efectivas de control tributario, que permitan en el mediano plazo incrementar la inversión social y mejorar los salarios de quienes ganan menos.
