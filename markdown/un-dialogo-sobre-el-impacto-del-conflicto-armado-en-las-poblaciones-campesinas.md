Title: Un diálogo sobre el impacto del conflicto armado en las poblaciones campesinas
Date: 2019-12-11 15:38
Author: CtgAdm
Category: Nacional, Paz
Tags: Campesinado colombiano, comision de la verdad, Encuentros por la verdad
Slug: un-dialogo-sobre-el-impacto-del-conflicto-armado-en-las-poblaciones-campesinas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/16917738189_5d26821a6b_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural] 

Estos 12  y 13 de diciembre, en Cabrera, Cundinamarca, la Comisión de la Verdad, encabezará un nuevo encuentro destinado a escuchar y reconocer testimonios de comunidades campesinas, víctimas de numerosas afectaciones, incluidas desplazamiento forzado, despojo de tierras y persecución política. Este también será un escenario que rendirá homenaje al periodista Alfredo Molano, Comisionado de la Verdad que dedicó gran parte de su trabajo a narrar la historia del campesinado en Colombia.

Según el Registro Único de Víctimas, de los 8,6 millones de desplazados forzados, el 87% son personas de zonas rurales de las que cerca de 6 millones de personas son de origen campesino, una estadística que permite evidenciar las múltiples afectaciones que ha sufrido esta población a lo largo de más de 50 años en los que la tierra se ha convertido en un botín de guerra y una forma de ejercer control en las regiones.

### Afrontar el despojo y el desplazamiento contra las comunidades campesinas

Además del desplazamiento forzado, también se abordarán temas como el despojo de tierras, afectación que según el Centro Nacional de Memoria Histórica oscila en las 5.504.517 hectáreas despojadas entre la década de 1980 y la primera década de los 2000, cifras que contrastan con las 322.000 hectáreas restituidas tras 8 años de firmada la Ley de Víctimas. [(Le puede interesar: Organizaciones sociales proponen reforma a Ley de Víctimas y Restitución de Tierras) ](https://archivo.contagioradio.com/organizaciones-sociales-proponen-reforma-a-ley-de-victimas-y-restitucion-de-tierras/)

Finalmente, el dialogo se enfocará en la persecución a organizaciones campesinas que han sido estigmatizadas, por grupos paramilitares, guerrilleros y la Fuerza Púbica quienes históricamente las han señalado, encarcelada y asesinado a sus integrantes por defender la tierra, ejercer un liderazgo en su región o proteger los derechos de la población.

Según el Centro de Investigación y Educación Popular (CINEP), tan solo en 2018, 98 líderes cívicos y comunales fueron víctimas de violaciones de DD.HH, mientras que en lo corrido del 2019 han sido asesinados 155 líderes sociales de los que 72 eran personas campesinas o venían  desarrollando procesos de restitución de cultivos.

Para la comisionada Martha Ruíz, este escenario busca "cerrar la brecha entre el campo y la ciudad" y **representa un espacio para seguir en el proceso de establecer responsabilidades de actores en el conflicto.** La Comisión de la Verdad, resalta además que este es la oportunidad de reconocer las luchas y transformaciones que han abanderado las poblaciones campesinas a lo largo de la historia y simboliza  "un llamado a la conciencia nacional para reconocer la dignidad campesina", agrega el comisionado Saul Franco.

Al respecto, **Sandra Saenz Sotomonte de la Plataforma de Incidencia Política de Mujeres Rurales Colombianas**, afirma que se trata de un encuentro en donde la verdad la contarán "quien la ha vivido, ayudando fortalecer y reinvidicar la importancia del tejido social, al ratificar la importancia de ejercer los liderazgos sociales", desarrollando mecanismos de resistencia frente al conflicto.

### Un homenaje a Alfredo Molano, un hombre que caminó junto a los campesinos

Además, el Encuentro por la Verdad rendirá tributo al comisionado Alfredo Molano, quien falleció el pasado 31 de octubre y quien venía trabajando junto a la Comisión de la Verdad en el proceso de esclarecimiento del conflicto y recolección de testimonios de las regiones Orinoquía y Amazonía. "Queremos que la presencia de Alfredo esté en todo el evento, pues si alguien supo reconocer la voz del campesino como protagonista del conflicto, fue él", expresó la comisionada Martha Ruíz. [(Le puede interesar: Alfredo Molano un caminante de la verdad)](https://archivo.contagioradio.com/alfredo-molano-un-caminante-de-la-verdad/)

El comisionado Molano, consideraba el Sumapaz, región que vivirá este encuentro como  un lugar donde se gestaron "luchas agrarias, violencias múltiples y conflictos que fueron la antesala del surgimiento de las FARC", por lo que resulta el lugar ideal para que se escuchen los testimonios que pueden permitir la construcción de una verdad colectiva y se resalte el rol del campesino en la historia de Colombia.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
