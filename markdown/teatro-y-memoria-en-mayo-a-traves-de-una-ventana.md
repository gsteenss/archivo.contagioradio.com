Title: Teatro por la memoria "Mayo a través de una ventana"
Date: 2015-11-10 16:04
Category: Cultura, eventos
Tags: Centro de memoria historica paz y reconciliación, Colectivo 16 de mayo, masacre barrancabermeja 1998, Teatro memoria histórica
Slug: teatro-y-memoria-en-mayo-a-traves-de-una-ventana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/article.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [10 nov 2015] 

Como parte de la "Gira Internacional para la construcción de la memoria histórica" se presenta en Bogotá la obra "**Mayo a través de una ventana**", montaje que reivindica la resistencia, búsqueda de justicia, verdad y reparación de los habitantes de Barrancabermeja masacrados por grupos paramilitares, el 16 de mayo de 1998.

La puesta en escena del colectivo "16 de Mayo", integrado por padres, familiares y amigos de las víctimas, busca a través del arte y la cultura para la paz, hacer pedagogía social de la memoria colectiva, trabajando de manera conjunta con habitantes de la comuna 7 y otras zonas del Magdalena medio golpeadas por el conflicto armado colombiano.

El testimonio teatral, gestado por el trabajo realizado desde 2008 en asocio con el Centro Cultural horizonte, combina estética de la dignidad, música, danza, dramaturgia corporal y títeres.

**Sobre la masacre.**

El 16 de Mayo de 1998, un comando armado de las Autodefensas Unidas de Santander y sur del César, irrumpió en el sector de los barrios populares de El Campin y María Eugenia (comuna 7), asesinando a siete pobladores y desapareciendo forzadamente a veinticinco personas más.

**Fecha:**Martes 10 de noviembre

**Lugar:** Centro de Memoria histórica Paz y Reconciliación, Carrera 19 B \#24-86

**Hora:** 6:00 p.m

Entrada libre (hasta completar el aforo)

   
 
