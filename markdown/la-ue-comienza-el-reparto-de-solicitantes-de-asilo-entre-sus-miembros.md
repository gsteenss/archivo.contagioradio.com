Title: La UE comienza el reparto de solicitantes de asilo entre sus miembros
Date: 2015-05-28 16:04
Category: El mundo, Otra Mirada
Tags: Inmigrantes ilegales, Reparto de solicitantes de asilo UE, Solicitantes de asilo Eritreos UE, Solicitantes de asilo Sirios en UE, Solicitantes de asilo UE, Unión europea
Slug: la-ue-comienza-el-reparto-de-solicitantes-de-asilo-entre-sus-miembros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/inmigrantes_lampedusa_getty.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Zoomnews.es 

###### [28May.2015] 

Este miércoles, la UE ha puesto en funcionamiento la medida presentada la semana pasada para repartir un total de **40.000 solicitantes de asilo pertenecientes a Siria y a Eritrea**, que actualmente están provocando una **crisis humanitaria** en Grecia e Italia.

Con la nueva medida se pretende aliviar la **emergencia humanitaria que están viviendo Grecia e Italia** principalmente, aunque también España, pero en este caso la procedencia de los inmigrantes es distinta, cuando en el caso de Grecia e Italia proceden de la guerra de Siria, países que conforman la frontera con Europa por donde entran los inmigrantes de Asia.

En el caso de **España,** la procedencia es Subsahariana y directamente relacionada con conflictos armados como los de **Congo, Somalia o Ruanda** a través del flujo migratorio en las ciudades colonialistas, situadas en Marruecos, de **Ceuta y Melilla.**

En total son **40.000 inmigrantes** a repartir entre los países miembros de la UE. El **reparto responde a las capacidades de cada país** como el PIB, las capacidades para aceptar refugiados políticos, la tasa de desempleo o la tasa de solicitantes de asilo existente antes de la medida.

\[caption id="attachment\_9344" align="aligncenter" width="560"\][![Elpais.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1432724517_589486_1432748321_sumario_normal.png){.size-full .wp-image-9344 width="560" height="789"}](https://archivo.contagioradio.com/la-ue-comienza-el-reparto-de-solicitantes-de-asilo-entre-sus-miembros/1432724517_589486_1432748321_sumario_normal/) Elpais.com\[/caption\]

En total, la UE cuenta con un presupuesto de **240 millones de euros para el programa que duraría dos años** y tendría como objetivo principal los potenciales solicitantes de asilo de Siria y de **Eritrea, este último país castigado por un servicio militar indefinido.**

Además, el programa pretende también **re-ubicar a 20.000 solicitantes de asilo político de países no comunitarios** como sería el caso de Turquía. Esta última medida no ha sido bien acogida por la mayor parte de estados miembros.

Por último, es importante remarcar que los **solicitantes de asilo político** tienen que pasar un **periodo de prueba** y tienen que ser revisado su status para que de esta forma les sea aprobada la solicitud **convirtiéndose así en refugiados**.
