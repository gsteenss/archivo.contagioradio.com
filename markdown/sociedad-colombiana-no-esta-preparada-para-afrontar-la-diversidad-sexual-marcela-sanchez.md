Title: "Sociedad colombiana no está preparada para afrontar la diversidad sexual": Marcela Sánchez
Date: 2016-08-10 15:52
Category: Entrevistas, LGBTI
Tags: Colombia Diversa, Corte Constitucional, Diversidad sexual, Sergio Urrego
Slug: sociedad-colombiana-no-esta-preparada-para-afrontar-la-diversidad-sexual-marcela-sanchez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/LGBTI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carmela María 

###### [10 Ago 2016] 

Hoy en Colombia se realizan marchas en las que los organizadores invitan a defender “la familia original”. Todo ello tras la polémica divulgación de unas cartillas endilgadas al Ministerio de Educación. Una situación que para organizaciones defensoras de los derechos humanos, no se debería centrarse en esas publicaciones sino en el derecho de todas y todos a liberar la escuela de la violencia y la discriminación.

“Adoctrinar a nuestros hijos en la ideología de género”, "disolver la familia, corromper la niñez y quitarles la pureza", y “colonización homosexual" son algunos de los comentarios que se han usado por parte de sectores conservadores para atacar la posibilidad de que en Colombia se empiece a dar verdaderas clases sobre educación sexual, como lo ordenó un fallo **de la Corte Constitucional, a través del cual se considera que la población LGBTI, ha sido “históricamente discriminada y desprotegida por el Estado”.**

Frente a esta nueva situación que enfrenta la comunidad LGBTI y el Ministerio de Educación, Marcela Sánchez, directora de Colombia Diversa, asegura que ese tipo de mensajes, como hablar de **“ideología de género” provienen de una estrategia para manipular y desinformar a partir** de “una vertiente religiosa que critica visiones sociales, antropológicas y sociales de la sexualidad”, pues, la perspectiva de género – como debe llamarse -  se trata de una corriente académica y teórica, de cual, parten cientos de estudios, pregrados, diplomados, maestrías; explica Sánchez.

Esa situación de polémica, es la que enfrenta apenas la primera fase de una estrategia implementada por el Ministerio de Educación que busca fortalecer la educación sexual, para que casos como el de Sergio Urrego nunca más se vuelvan a repetir. “No es un capricho del Ministerio, responde a una Ley y una orden de la Corte Constitucional”, dice la directora de Colombia Diversa, una de las organizaciones que asesora al Ministerio en el desarrollo de este proceso.

Respecto al tema de las cartillas, Marcela Sánchez, asegura que es falso que ese material se haya distribuido en los colegios pues la cartilla, ‘Ambientes escolares libres de discriminación’, apenas está en proceso de construcción y retroalimentación para que esta sea usada como material pedagógico con el fin de capacitar las secretarías de educación. Allí **no solo se hablará de diversidad sexual, si no temas de convivencia y derechos humanos,** además,** **Esta primera fase, implica la revisión de los cerca de 20 mil manuales de convivencia.

La segunda etapa para dar cumplimiento a la orden de la Corte, será la capacitación de los docentes con materiales pedagógicos; y finalmente la tercera fase, comprenderá la promoción de la participación estudiantil y al creación de espacios de comité de convivencia ciudadana.

“Si las personas están saliendo hoy a marchar es porque no conocen los manuales de convivencia, muchos de los hijos de quienes marchan pueden estar siendo víctima de acoso escolar”, dice Sánchez, quien finalmente, concluye pidiéndole a los padres de familia que “Lean, pregunten en sus colegios, y  no se dejen llevar por lo que la gente”, pues los **padres de familia pueden ser parte de la construcción de los manuales de convivencia de los colegios de sus hijos.**

<iframe src="http://co.ivoox.com/es/player_ej_12504838_2_1.html?data=kpeikpmcd5mhhpywj5abaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5ynca7V08jSzsaPl4a3lIquk9PHrMbuhpewjajTsNDhw87OjcnNusbm1Maah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
