Title: Maestros argentinos se unen a movilizaciones contra los tarifazos
Date: 2016-07-27 16:44
Category: El mundo
Tags: Gobierno Macri, huelga docentes argentina, tarifazos argentina
Slug: maestros-argentinos-se-unen-a-movilizaciones-contra-los-tarifazos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Tarifazos.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [27 Julio 2016] 

"También forma parte de la formación de un docente poder ir a un teatro, poder ir a cine, o poder comprarse un libro, pero hoy día los docentes de Argentina llegan arañando a fin de mes", afirma Mariano de Nardis y agrega que además de recortes salariales, persecuciones y constantes despidos, los maestros se han tenido que enfrentar a **alzas en las tarifas de los servicios públicos de hasta 500%**, por lo que han decidido articular sus movilizaciones a los festivales contra los tarifazos que distintos sectores sociales están promoviendo realizar en agosto de este año.

De acuerdo con Nardis, en Argentina se está viviendo un cambio de etapa política con repercusiones negativas para las condiciones de vida de los ciudadanos. Recientemente se ha puesto en marcha una oleada de despidos en los sectores estatal, educativo e industrial, que han implicado, entre otras, una **reducción en las garantías logradas por los sindicatos, quienes exigen la reapertura de las paritarias** que son los espacios en los que tienen la posibilidad de negociar los salarios.

Actualmente los docentes de esta nación se enfrentan a recortes salariales, condiciones laborales inadecuadas y constantes procesos judiciales en su contra, tanto a nivel nacional como provincial. Situación que se agudiza con las **disminuciones de los presupuestos para el sostenimiento de las instituciones educativas y los comedores escolares** que "recuerdan las peores épocas del país", como asegura Nardis, por lo que adelantan una convocatoria de huelga en distintas provincias para presionar soluciones a [[la crisis que enfrentan](https://archivo.contagioradio.com/maestros-argentinos-en-paro-por-48-horas-exigen-alzas-salariales/)].

<iframe src="http://co.ivoox.com/es/player_ej_12358710_2_1.html?data=kpegl52bdZGhhpywj5abaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5ynca7V087O0NSPqMafr8bfxs7XaZO3jLjW0MnNp8Lgytjhw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
