Title: Las cárceles y su doble revictimización a las mujeres
Date: 2020-03-16 11:14
Author: AdminContagio
Category: Expreso Libertad
Tags: carcel de mujeres, carceles, Expreso Libertad, INPEC, movimiento carcelario, Mujeres y cárceles, revictimización, Sistema Penitenciario
Slug: las-carceles-y-su-doble-revictimizacion-a-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/495749_710417.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este **8 de marzo** se conmemoró en el mundo las luchas de las mujeres por el fin de las violencias basadas en género, las desigualdades salariales y la construcción de una sociedad feminista. Ha estos hechos se suman las apuestas de las prisioneras políticas y de las mujeres que hacen parte del **Movimiento Carcelario**, como Rocío Barrera, Hermana de César barrera, víctima del caso Andino, que en los micrófonos del **Expreso Libertad** habló sobre las revictimizaciones a las mujeres en el sistema penitenciario. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Rocío asegura que las mujeres no solo deben padecer violaciones a derechos humanos estando dentro de la cárcel, en razón de ser mujer, es decir, producto de la ausencia de un enfoque de género que responda las necesidades y roles de género, sino que también las mujeres que son visitantes deben vivir distintas afectaciones cuando ingresan a las cárceles, producto de los abusos de poder ejercidos por guardianes del **INPEC. **  
Violaciones que para Roció son producto de un sistema penitenciario que no debería existir porque no responde a su principal labor que es la resocialización, y que por el contrario atenta contra la vida de las personas, legitima la corrupción y permite abusos constantes.

<!-- /wp:paragraph -->

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F606075323576418%2F&amp;width=600&amp;show_text=false&amp;height=338&amp;appId" width="600" height="338" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>

<!-- wp:paragraph -->

Ver mas de [Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
