Title: JEP abre incidente contra gerente de EPM
Date: 2020-08-22 12:21
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Empresas Públicas de Medellín, EPM, Hidroituango, JEP, Movimiento Ríos Vivos
Slug: jep-abre-incidente-contra-gerente-de-epm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Gerente-de-EPM-requerido-por-JEP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Jurisdicción Especial para la Paz -JEP-, abrió este jueves un incidente de medidas correccionales contra el gerente de Empresas Públicas de Medellín -EPM-, Álvaro Guillermo Rendón; por el «*incumplimiento*» de las órdenes dadas para proteger 16 zonas donde habría cuerpos de víctimas del conflicto armado. (Le puede interesar: [UBPD pide protección para sitio de inhumación en El Copey (Cesar)](https://archivo.contagioradio.com/ubpd-pide-proteccion-para-sitio-de-inhumacion-en-el-copey-cesar/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1296564213855522816","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1296564213855522816

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La Sección de Ausencia de Reconocimiento de **la JEP, ordenó a EPM entregar información sobre el traslado de los cementerios de Barbacoas, La Fortuna y Orobajo, ubicados en la zona de influencia del proyecto Hidroituango, donde se estima, fueron exhumados 349 cadáveres.** (Lea también:[Manejo erróneo de cementerios por Covid-19 pondría en riesgo memoria histórica del conflicto](https://archivo.contagioradio.com/manejo-erroneo-de-cementerios-por-covid-19-pondria-en-riesgo-memoria-historica-del-conflicto/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Los cuerpos de los cementerios que estaban en las áreas del proyecto Hidroituango, fueron exhumados y trasladados, en un proceso que, según la JEP, está lleno de irregularidades.** Esta Jurisdicción busca corroborar que en las zonas de inundación del proyecto hidroeléctrico, puede haber fosas y otros sitios de inhumación donde hay cuerpos de personas que fueron asesinadas en medio del conflicto armado, muchas de las cuales, fueron desaparecidas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las víctimas señalan que en los seis municipios de influencia directa del proyecto; Briceño, Peque, Sabanalarga, Ituango, Toledo y Valdivia; hay centenares de desaparecidos y que el eje en torno al cañón del río Cauca, ha sido una de las zonas más afectadas por la violencia, a lo largo de varias décadas. Por otro lado, se asegura  que muchas veces bajaban los cadáveres con el caudal del agua y tenían que ser enterrados en las playas de la ribera del río.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto, **el Centro Nacional de Memoria Histórica ha registrado desde 1982 hasta 2016, cerca de 600 casos de desaparición forzada en esos seis municipios que hacen parte del área de influencia de Hidroituango.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Movimiento Ríos Vivos denuncia las malas prácticas de EPM

<!-- /wp:heading -->

<!-- wp:paragraph -->

El [Movimiento Ríos Vivos](https://riosvivoscolombia.org/), que agrupa muchas de las víctimas y afectados por Hidroituango, basado en el auto que profirió la JEP, aseguró que **«*EPM* *inundó y desapareció a los muertos, y borró parte de la verdad del conflicto armado en Colombia*»**. Además, afirmó que EPM, mintió sobre un supuesto aval que le había concedido la Fiscalía para hacer el traslado de los cuerpos que se encontraban en los cementerios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ríos Vivos ha venido denunciando esta y otras irregularidades, incluso antes de que se empezara a construir el proyecto hidroeléctrico, y la Jurisdicción de Paz, está validando estas denuncias. (Lea también: [«Ituango en una encrucijada de pobreza y violencia»](https://archivo.contagioradio.com/ituango-en-una-encrucijada-de-pobreza-y-violencia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RiosVivosColom/status/1296625340647903235","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RiosVivosColom/status/1296625340647903235

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Isabel Zuleta, quien está a la cabeza de este Movimiento, ha sido recurrente en elevar esta y otras denuncias que hay en torno al proyecto; como por ejemplo **las presiones que, según ella, ha recibido la comunidad por parte de EPM en los procesos de negociación para la indemnización por los daños sufridos con la construcción y el colapso de la represa.** Adicionalmente, el desinterés, que según Zuleta, han tenido todos los gobernantes locales y regionales; y los órganos de control como la Procuraduría para escuchar a las comunidades.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
