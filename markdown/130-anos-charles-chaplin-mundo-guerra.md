Title: 130 años del nacimiento de Chaplin y el mundo sigue en guerra
Date: 2019-04-16 13:31
Author: AdminContagio
Category: 24 Cuadros
Tags: discurso Chaplin, El gran dictador, Segunda Guerra Mundial
Slug: 130-anos-charles-chaplin-mundo-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/c9b3072d-e1444942954706.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: United Artists 

A 130 años del nacimiento de Charles Spencer Chaplin, conocido artísticamente como Charlie Chaplin,  recordamos el discurso que incluyó en el cierre de su película "El gran dictador", la cinta en que por primera vez, Charlot, el vagabundo creado en 1914 por su genio incontenible, se dirigió al mundo en propia voz, con un mensaje contundente.

Tras resistirse durante varios años a ingresar al mundo de las películas habladas, en auge desde el estreno de "The jazz singer" de 1927, incorporando únicamente algunos sonidos balbuceados en producciones como "Tiempos Modernos" de 1936, Chaplin decidió que era el momento perfecto para que el vagabundo hablara, pronunciándose en contra de la ambición desmedida y un llamado a la igualdad entre los seres humanos.

Hoy sus palabras cobran especial vigencia con frases como **"Más que máquinas necesitamos más humanidad. Más que inteligencia, tener bondad y dulzura"** o  **"El odio pasará y caerán los dictadores, y el poder que se le quitó al pueblo se le reintegrará al pueblo, y, así, mientras el Hombre exista, la libertad no perecerá".**

<iframe src="https://www.youtube.com/embed/3cFTJ9q5ztk" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Discurso Charles Chaplin (Subtitulado) 

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
