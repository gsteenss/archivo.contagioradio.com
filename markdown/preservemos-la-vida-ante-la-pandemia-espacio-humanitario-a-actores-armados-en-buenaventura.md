Title: Preservemos la vida ante la pandemia: Espacio humanitario a actores armados en Buenaventura
Date: 2020-04-08 11:20
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: amenazas contra líderes sociales, buenaventura
Slug: preservemos-la-vida-ante-la-pandemia-espacio-humanitario-a-actores-armados-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/espacio-humanitario.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Espacio HUmanitario/Gabriel Galindo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Orlando Castillo, líder del espacio humanitario Puente Nayero en Buenaventura, Valle del Cauca, la violencia que se perpetúa contra los líderazgos sociales y la población, además de las precarias condiciones sanitarias para atender la emergencia del Covid-19 aumentan las probabilidades de que la población quede expuesta ante la pandemia, por lo que exigen al Estado colombiano existan garantías para proteger a los habitantes del municipio.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Castillo expresa que los bonaverenses guardaban la esperanza que el Covid-19 no llegara hasta su municipio sin embargo, un primer caso confirmado ha puesto en alerta a la población que en medio de un olvido estatal, altas cifras de desempleo y pobreza, podría complejizar aún más la situación de la región con la llegada de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El líder social resalta que cerca del 80% de la población carece de ingresos regulares, la mayoría de la gente vive del rebusque o del 'día a día, es decir, no existen condiciones para vivir en la cuarentena. **Para 2018, la posición ocupacional de trabajador por cuenta propia en Buenaventura concentró el 49,7% de la población mientras la de 'obrero' o empleado particular ocupó un 36,7%. A su vez, la tasa de desempleo se ubicó en 18,4%.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En materia de salud, resalta que Buenaventura no tiene las condiciones de sanidad para mitigar este tipo de pandemia", no existe agua para la población y no se cuenta con hospitales de tercer nivel que puedan hacer frente a la pandemia. Asegura además que el Gobierno tampoco ha dado las garantías al alcalde Víctor Vidal para generar condiciones de seguridad alimentaria tanto en lo urbano como lo rural para que la población pueda resguardarse. [(Le puede interesar: Del paro cívico a gobernar Buenaventura, los retos que asume Hugo Vidal)](https://archivo.contagioradio.com/del-paro-civico-a-gobernar-buenaventura-los-retos-que-asume-hugo-vidal/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Violencia en el puerto de Buenaventura avanza incluso en aislamiento

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El líder resalta que **en menos de 24 horas han sido asesinadas dos personas, uno de estos casos, el del joven Duván Ballesteros quien fue asesinado a tan solo 100 metros del Espacio Humanitario Puente Nayero**. A esta situación se suman [las amenazas que han surgido](https://www.justiciaypazcolombia.com/amenazan-a-lideres-y-lideresas-para-imponer-microtrafico/) por parte de jóvenes que, enviados por estructuras criminales herederas del paramilitarismo, buscan amedrentar a los líderes y lideresas del espacio que se oponen a la vinculación de menores de edad a redes de **microtráfico**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Advierte además que existen controles en lugares como Punto Icaco, con movimiento, presencia y control de actores armados en el territorio, "ni siquiera la pandemia ha podido originar que estos actores se mantengan resguardados, los asesinatos dan cuenta de ello". [(Le puede interesar: Comunidades del Bajo Atrato denuncian alza de precios y actividad bananera en medio de la cuarentena)](https://archivo.contagioradio.com/comunidades-del-bajo-atrato-denuncian-alza-de-precios-y-actividad-bananera-en-medio-de-la-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Este es un momento crucial en el mundo en el que hacemos un llamado en todos los actores armados, dediquémonos a preservar lo más valioso que es la vida y salir de esta pandemia que afecta al mundo", afirma el líder social quien considera que la situación puede mejorar y prevenirse si desde el Gobierno comienzan a implementarse todos los acuerdos que hacen parte del pliego de acuerdos del paro cívico. [(Lea también: Atentado contra Orlando Castillo, reflejo de la guerra en Buenaventura)](https://archivo.contagioradio.com/atentado-contra-orlando-castillo-reflejo-de-la-guerra-en-buenaventura/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
