Title: Continúa huelga de Pilotos por "soberbia" de directivos de Avianca
Date: 2017-09-27 13:02
Category: Nacional
Tags: ACDAC, Avianca, Huelga de pilotos
Slug: continua-huelga-de-pilotos-por-soberbia-de-directivos-de-avianca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DKmPaWSWAAoR06c-e1506535314979.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CUT] 

###### [27 Sept 2017] 

Luego de que en la madrugada del 27 de septiembre de 2017, **la empresa Avianca se levantara de la mesa de negociación con los pilotos** que hacen parte de la Asociación Colombiana de Aviadores Civiles, estos últimos manifestaron que no hay voluntad de la empresa para llegar a acuerdos. Le hicieron un llamado a que “cese la actitud de soberbia” y continúen las negociaciones.

En rueda de prensa y a través de un comunicado, los pilotos agremiados indicaron que Avianca, después de haber planteado una propuesta como respuesta a las exigencias del sindicato, **no les dio la posibilidad de discutirla en la asamblea**.

Manifestaron que “pocos minutos después de las 12 de la media noche y aún en el marco de las conversaciones adelantadas en el Ministerio, **la administración en actitud de soberbia decidió levantarse de la mesa** y retirar la propuesta, aduciendo órdenes de la presidencia de la compañía”. (Le puede interesar: ["Estas son las exigencias de los pilotos de Avianca"](https://archivo.contagioradio.com/estas-son-las-exigencias-de-los-pilotos-de-avianca/))

###  

Además, indicaron que “varios de los preacuerdos alcanzados durante los días de negociación en el Ministerio del Trabajo **fueron modificados**”. Por esto, rechazaron la actitud de Avianca en cuanto “demuestra la clara falta de voluntad y la dilación de este proceso de negociación para el que los pilotos mantuvimos total disposición.”

Dijeron que en repetidas ocasiones ha habido “un comportamiento soberbio e **irrespetuoso que se ha evidenciado en las declaraciones públicas** de Hernán Rincón y Germán Efromovich tanto hacia los pilotos como hacia los demás trabajadores”. Hicieron alusión a que “el video de amenazas del señor Efromovich ha generado enorme malestar en los trabajadores”.

### **Huelga no es ilegal** 

El Tribunal Superior de Bogotá, **inadmitió la demanda de Avianca** que buscaba el suspender el paro colectivo de los pilotos. Así mismo, le dio un plazo de 5 días a la aerolinea para subsanar la demanda.

El abogado Carlos Roncancio explicó que la **demanda no reunió los requisitos formales** del código de procedimiento laboral y de la seguridad social. Dijo que se Avianca no subsana la demanda, en sus falencias de forma en el plazo establecido, ésta será rechazada en definitiva por el Tribunal.

### **Peticiones de los pilotos se han ridiculizado en los medios de comunicación** 

Los aviadores indicaron que en medio de la huelga, **ha habido desinformación y “ridiculización”** con respecto a las exigencias que demandan. Han manifestado que su objetivo es trabajar porque se defiendan los derechos laborales de los pilotos.

El presidente de ACDAC afirmó que Avianca **“ha manipulado la verdad a través de los medios de comunicación”** y ha modificado la propuesta y algunos de los 17 pre acuerdos que se habían negociado y calificaron la acción como una reacción con “un espíritu de sabotaje y falta de respeto”.

Manifestó que la mesa de diálogo va a continuar con la presencia de la Ministra de Trabajo Griselda Restrepo **esperando que Avianca se vuelva a sentar a negociar**. Dijo que la “Asociación de pilotos no se va a levantar de la mesa por la responsabilidad que hemos asumido”.

Finalmente, **reiteraron su disposición a continuar con las negociaciones en la mesa**, esperando proteger los derechos laborales que, según ellos, “han sido violados en múltiples ocasiones por una empresa que ha sido sancionada varias veces por el Ministerio del Trabajo”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
