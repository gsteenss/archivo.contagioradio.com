Title: Asesinan a Ezequiel Rangel líder de ASCAMCAT en Norte de Santander
Date: 2017-07-18 10:17
Category: DDHH, Nacional
Tags: ASCAMCAT, Ezequiel Rangel, marcha patriotica
Slug: asesinan-a-ezequiel-rangel-lider-de-ascamcat-en-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/lideres-sociales-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  ASCAMCAT] 

###### [18 Jul 2017] 

Según la denuncia de ASCAMCAT, fue asesinado Ezequiel Rangel, líder del Norte de Santander, su cuerpo fue hallado sin vida este 17 de Julio, con varios impactos de arma de fuego, en cercanías a la vereda Vegas del Águila, en el municipio Del Carmen. Con **Rangel son dos los líderes sociales de Marcha Patriótica asesinados en menos de una semana y 52 líderes asesinados.**

Según testimonios de personas cercanas, el pasado domingo, Ezequiel salió sobre las 5:30 de la tarde de su vivienda en el corregimiento de Guamalito, sobre las 10:00 pm diferentes personas expresaron que era conducido en una motocicleta en la vía que conecta con el corregimiento de Santa Inés. (Le puede interesar:["Asesinado en Cauca, Héctor William Mina, líder integrante de Marcha Patriótica"](https://archivo.contagioradio.com/asesinado-hector-william-mina-marcha-patriotica/))

Sobre las 5:00 am del 17 de julio, la familia de Ezequiel recibió una llamada en donde les comunicaban que el cuerpo del líder social **había sido encontrado en la vereda Vegas de Águila con varios impactos de arma de fuego**. (Le puede interesar: ["En 2017 han sido asesinados 15 líderes sociales en el Cauca"](https://archivo.contagioradio.com/en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca/))

Rangel era, padre de dos menores de edad, el coordinador del comité veredal de la Asociación Campesina del Catatumbo (ASCAMCAT) y pertenecía a la plataforma social Marcha Patriótica, además, de acuerdo con Olga Quintero, líderesa de ASCAMCAT, **Ezequiel se encargaba de generar espacios importantes de interacción en la comunidad a la que pertenecía**.

### **La estigmatización a ASCAMCAT por parte de Medios de Información** 

Quintero manifestó que, pese a que no se conoce que el líder social haya recibido amenazas de algún tipo, este acto se suma a las intimidaciones hechas por grupos autodenominados como los Urabeños y la estigmatización de la cual ha sido víctima la organización **ASCAMCAT en diferentes medios de información**, que señalan que esta plataforma pertenece a las FARC-EP.

“La estigmatización circula en decir que somos integrantes de las FARC-EP, señalando que hemos robado recursos en el marco del acuerdo de la MIA Catatumbo, haciendo comentarios tendenciosos que nos están haciendo daño, lo que va a ocurrir y hemos venido dejando constancia es que nos van a ocasionar la muerte o la cárcel” manifestó Quintero.

Además, agregó que serían aproximadamente 20 artículos de medios como el Diario La Opinión o la Silla Vacía Santanderiana, los que han realizado este tipo de aseveraciones.

### **La Fuerza Pública en el Catatumbo** 

En los últimos meses al Catatumbo llegaron, de acuerdo con Quintero, 3.000 integrantes de la Fuerza Pública que se suman a la militarización que ya había sido ordenada por Min Defensa, sin embargo, la líder social **aseguró que hasta el momento no se conoce que haya acciones contra paramilitares**.

Por esta razón hizo un llamado al gobierno Nacional y al presidente Santos para que se brinden las garantías a líderes sociales y defensores de derechos humanos en el ejercicio de sus actividades **“En este país no se puede convertir en delito ser defensor de derechos humanos**, necesitamos que el gobierno se porte serio con esta situación que se está presentando, no somos cifras somos personas”.

<iframe id="audio_19864326" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19864326_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
