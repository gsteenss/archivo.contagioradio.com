Title: Estos son los retos de implementación del acuerdo de paz
Date: 2016-11-24 14:59
Category: Nacional, Paz
Tags: #AcuerdosYA, acuerdo farc y gobierno, implementacion acuerdos
Slug: estos-son-los-retos-de-implementacion-del-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/presidencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia] 

###### [24 Nov 2016] 

Tras la firma del nuevo Acuerdo de Paz, denominado “Acuerdo de paz del Teatro Colón”, muchos son los restos que se vienen tanto para el movimiento social, como para las FARC-EP y el gobierno del presidente Juan Manuel Santos. **Entre ellos está la construcción de una ruta que permita que se implementen los acuerdos a corto, mediano y largo plazo.**

De acuerdo con el profesor y analista político Carlos Medina, el principal resto que deberá asumir el movimiento y las organizaciones sociales, partidos políticos y la sociedad civil en general consiste en pensar en la necesidad de colocar **un nuevo gobierno que recoja todo el esfuerzo que se ha hecho e implementarlo a fondo**; “sería adverso no para los acuerdos, sino para la paz de Colombia que llegara un gobierno que estuviera en contra de lo acordado”. Le puede interesar:["CONPAZ exige voluntad política al gobierno para implementación de acuerdos"](https://archivo.contagioradio.com/conpaz-exige-voluntad-politica-para-implementacion-de-acuerdos/)

Frente a las tareas que debe cumplir el gobierno actual, Medina afirmó que en principio le queda un año y medio para procesar legal e institucionalmente los acuerdo e ir creando las condiciones para su implementación futura, que tardará aproximadamente 4 generaciones dependiendo de la celeridad de las fuerzas políticas. Por ende, **debe dejarse en una buena ruta para la implementación y generar toda la institucionalidad que se requiere para que no se presenten inconvenientes en lo acordado.**

Sobre las FARC-EP, Carlos Medina aseguró que el discurso que pronunciaron en la firma del nuevo acuerdo, deja sentir el optimismo y el compromiso de ellos por construir una paz estable y duradera, además de evidenciar el a**gradecimiento hacia la sociedad civil que salió a las calles a defender los acuerdos** y que deberá mantenerse en esta fase de implementación.

Para finalizar Medina enfatizó que existen unas urgencias que deben generarse, una de ellas es expedir una **ley de amnistía en general que le permita a los guerrilleros avanzar a las zonas veredales** y en donde el ente protagonista será el congreso que deberá generar la transición hacia el inicio de la implementación de los acuerdos.

<iframe id="audio_13960174" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13960174_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
