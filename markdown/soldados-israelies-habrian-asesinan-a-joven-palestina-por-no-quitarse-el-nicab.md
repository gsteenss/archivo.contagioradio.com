Title: Soldados Israelíes habrían asesinado a joven Palestina por no quitarse el Nicab
Date: 2015-09-25 16:59
Category: El mundo, Política
Tags: Autoridad nacional palestina, Conflicto en Medio oriente, Israel, Palestina
Slug: soldados-israelies-habrian-asesinan-a-joven-palestina-por-no-quitarse-el-nicab
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:diariodeurgencia.com 

###### [25 sep 2015]

En Palestina Jadil Hashlamoun recibió disparos de las **fuerzas de Defensa de Israel ubicadas en Cisjordania**, la joven permaneció en la calle sin recibir ayuda por más de 30 minutos, hasta que murió, "ya que los soldados no permitieron que los médicos se le acercaran a prestarle ayuda", indicó el díario The times of Israel. La joven de 18 años fue **asesinada** por **negarse a levantar su velo.**

El ejercito Israel indica que la joven era una “terrorista”, pero fotos difundidas por los activistas del grupo de **Jóvenes contra Asentamientos** aseguran que Hadil Hashlamoun "*no hizo ningún intento de apuñalar a ningún soldado*" y agregaron que "*probablemente la joven fuera asesinada tras negarse a levantarse el velo*".

La **Autoridad Nacional Palestina (ANP)** pidió en una declaración a la comunidad internacional que ejerzan presión sobre Israel para poner fin a una “*política de ocupación basada en el asesinato fácil de palestinos sin rendición de cuentas".*
