Title: Falsos positivos judiciales contra lideresas sociales
Date: 2019-04-17 09:59
Author: CtgAdm
Category: Expreso Libertad
Tags: Alto Mira y Frontera, Lideresas Sociales, prisioneros políticos
Slug: falsos-positivos-judiciales-contra-lideresas-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/acsn1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Afro Colombian Solidarity Network 

**Sara Quiñonez y Tulia Maris Valencia**, son dos lideresas del Consejo Comunitario de Alto Mira y Frontera en Tumaco, departamento de Nariño, que cumplen un año de estar en condición de reclusas por **delitos que según su comunidad no existen** y que por el contrario, podría ser **su labor en la defensa de los derechos humanos** el motivo para que se encuentren detenidas.

En este programa del Expreso Libertad, conversamos con **Charo Mina**, **lideresa del Proceso de Comunidades Negras (PCN)**, que además de denunciar el falso positivo judicial en contra de Quiñonez y Valencia, relató **la gran fuerza que ha adquirido el proceso social y la comunidad en torno al apoyo de sus mujeres**, y de la lucha para proteger sus territorios de actores armados, multinacionales y el abandono estatal.

<iframe id="audio_34596761" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34596761_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
