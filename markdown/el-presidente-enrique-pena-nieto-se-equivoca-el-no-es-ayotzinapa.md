Title: "Enrique Peña Nieto se equivoca, él no es Ayotzinapa"
Date: 2015-01-09 15:47
Author: CtgAdm
Category: El mundo, Movilización
Tags: Ayotzinapa, Desaparición forzada, Destacado, Enrique Peña Nieto
Slug: el-presidente-enrique-pena-nieto-se-equivoca-el-no-es-ayotzinapa
Status: published

###### Foto: México.cnn.com 

**Entrevista a Ada Chávez**

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsngiS3U.mp3)

Luego del anuncio de las medidas que tomará el gobierno mejicano y que estarían apuntando a resolver la crisis que se desató en ese país por el caso de los 43 estudiantes desaparecidos de la Normal Rural de Ayotzinapa, diversos sectores sociales, de Derechos Humanos y analistas han confluido en afirmar **que ninguna de las medidas anunciadas podrá responder eficazmente a la violencia estructural y estatal que se ha tomado a ese país centro americano.**

Según explica Ada Chávez, sub directora del portal Desinformémonos, varios analistas afirman que el presidente Peña Nieto se equivocó al afirmar que el también hace parte de “Todos Somos Ayotzinapa” pues además de negar la existencia de la práctica de la desaparición forzada, también niega que la indignación sea por el caso Ayotzinapa y niega la responsabilidad estatal, afirmando que son solo algunos infiltrados en gobiernos municipales los que cometen actos delictivos. Adicionalmente la periodista resalta que se mantiene la exigencia social de la renuncia del presidente mejicano.
