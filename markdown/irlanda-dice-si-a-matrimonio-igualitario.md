Title: Irlanda dice SÍ a matrimonio igualitario
Date: 2015-05-26 14:06
Category: closet, LGBTI
Tags: Closet Abierto, Derechos Humanos, Homosexualidad, Irlanda, Leyes, LGBTI, matrimonio igualitario, Referéndum
Slug: irlanda-dice-si-a-matrimonio-igualitario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1506605_999252930094421_3324958977351834009_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Amnistía Internacional 

###### [25 May 2015]

El viernes pasado se llevó a cabo el referéndum que decidió el futuro de las parejas del mismo sexo en Iralanda. En una votación histórica se dio el sí al matrimonio homosexual, con resultados de 62,1% a favor y 37,9% en contra, con picos de 70% en ciudades como Dublín, capital irlandesa.

Irlanda se convirtió en el primer país en decir sí al matrimonio para personas del mismo sexo por medio de un referéndum. Desde ahora es uno de los países que vela por los derechos de la comunidad LGBTI y fomenta la igualdad.

Aodhan O'Riordain ministro para la igualdad y uno de los promotores del referéndum, se expresó al respecto: "Hoy estoy orgulloso de ser irlandés". A nivel religioso, esta decisión influirá considerablemente, pues la iglesia era de los pocos oponentes a la aprobación de esta ley para conservar el modelo tradicional de familia.

La aprobación del matrimonio para personas del mismo sexo significa un avance definitivo, pues Irlanda es uno de los países más avanzados en materia de derechos, hasta el momento la comunidad LGBTI goza de matrimonio, adopción conjunta, adopción consentida, unión civil, leyes anti-discriminación, protección laboral y legalidad de la homosexualidad.
