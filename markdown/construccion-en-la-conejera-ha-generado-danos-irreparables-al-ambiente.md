Title: Construcción en La Conejera ha generado daños irreparables al ambiente
Date: 2015-05-08 12:32
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Bogotá, Gabriel Vallejo, Humedal, La Conejera, licencias ambientales, Ministerio de Ambiente, ministro de Ambiente, Praga Servicios Inmobiliarios S.A, Secretaría de Ambiente, Suba
Slug: construccion-en-la-conejera-ha-generado-danos-irreparables-al-ambiente
Status: published

##### Foto: [poliradio.poligran.edu.co]

**“Se presentó daño ambiental irreparable en la fauna y la flora”,** es la conclusión de la **Fiscalía General de la Nación**, que tras un año de investigación presentó un informe preliminar sobre las afectaciones del la construcción del proyecto **Reserva del Fontanar,** en el Humedal La Conejera.

De acuerdo al informe y los elementos probatorios que se tuvieron en cuenta, Praga Servicios Inmobiliarios S.A, generó un **daño ambiental irreparable en los recursos hídricos, el suelo, la fauna y la flora,** debido a que la construcción invadió el espacio natural, poniendo en riesgo el ecosistema, emisión de gases y desplazamiento de aves nativas.

El informe presentado por la Fiscalía **contrasta con el realizado por la Secretaria de Ambiente de Bogotá,** donde se concluía que la construcción no significaría ningún daño ambiental. Es por eso, que  se estudiarán los dos informes y posiblemente se necesitará una nueva investigación por parte de una universidad para tomar la decisión definitiva.

El grupo de investigadores estuvo conformado por dos fiscales de medio ambiente, tres ingenieros y dos químicos. Así mismo, los investigadores van a revisar los permisos del proyecto residencial.

Mientras tanto, Gabriel Vallejo, **ministro del Medio Ambiente, indicó que no daría ninguna declaración sobre el estudio de la fiscalía, por su condición de alcalde ad hoc,** “Yo una vez más quiero hacer claridad. Lo primero es que a mí me eligieron como alcalde ad-hoc, para definir si el procedimiento desde el punto de vista de la licencia urbanística que otorgó el curador, cumplió con todos los requisitos de sus mediciones y demás el Consejo de Veedores, del cual yo formaba parte, como presidente tomó la determinación de que esa licencia desde ese punto se otorgó con todos los requisitos y a partir de ese momento lo expresa la Ley, por lo cual no puedo pronunciarme".

Tras conocerse los resultados de la investigación, **los propietarios de la constructora y los funcionarios públicos que concedieron las licencias, serán llamados a declarar**.

 
