Title: 14 hechos de violencia contra los DD.HH. de la Minga en Cauca
Date: 2019-04-03 16:25
Author: CtgAdm
Category: Comunidad, DDHH, Nacional
Tags: Abuso de fuerza ESMAD, Comunidades Indígenas en Colombia, DD.HH, Minga Nacional
Slug: como-avanza-la-minga-en-la-defensa-de-dd-hh-de-los-pueblos-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-03-at-4.23.57-PM-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @edwincapaz] 

###### [3 Abr 2019] 

Después de conocerse sobre la muerte de **Breiner Yunda, joven comunero de 20 años quien fue asesinado el pasado 2 de abril** en medio de una agresión del ESMAD en contra de la casa de habitación en el marco de la Minga Nacional por la vida en El Cairo, Cajibio en el departamento del Cauca, son varias las denuncias en torno a las agresiones y la situación de DDHH en la protesta.

**Según Joe Sauca, coordinador de derechos humanos del CRIC**, no se ha establecido aún si la herida de muerte de Breiner fue ocasionada por un arma de fuego como se había señalado desde un principio o si fue herido por el proyectil de alguna recalzada, por lo que aclara que prefieren esperar un pronunciamiento oficial de Medicina Legal, pero que lo **sí es seguro es la presencia de la Fuerza Pública en el lugar y que fueron atacados mientras se encontraban en una vivienda.**

Por su parte, **el minguero Walter Florez** quien también se encontraba presente durante la arremetida, fue herido en el brazo izquierdo y trasladado al Hospital de Piendamó donde se encuentra fuera de peligro, además sería un testigo crucial del asesinato de Breiner Yunda, "el dolor es de todos nosotros, al igual que el luto; pero también de rabia porque nosotros hemos tenido intención de diálogo, y este es el resultado de la inoperancia", agrega.

Estos son algunos de los hechos que han marcado estos 24 días de Minga en los que el uso de la fuerza contra las comunidades se ha visto evidenciado:

**Recuento de sucesos**

**11 de marzo:** Cuando transitaban sobre la vía panamericana para dirigirse al punto de concentración de la minga, dos vehículos que transportaban comuneros indígenas, fueron retenidos por agentes de la Policía de Carreteras.

**12 de marzo:** Se presentan confrontaciones con el ESMAD, como resultado son heridos dos indígenas, uno de ellos es remitido al hospital Santander de Quilichao y el otro atendido por el CRIC.

**13 de marzo:** La vía que comunica a Morales con Suárez donde los mingueros están instalados es militarizada por el ESMAD, a su vez, la policía del Cauca anuncia que da dos horas para despejar la vía.

En el transcurso del día, integrantes del ESMAD, mediante el uso de armas letales y no letales (papas bombas) ataca a comuneros indígenas asentados en tres puntos sobre la vía panamericana, se impide el traslado de los heridos.

En la vereda El Cairo denuncian arremetida del Ejércitoy el ESMAD, ingresando de forma a los campamentos, luego de este ataque, se suspende la energía en la vereda El Cairo.

**14 de marzo:** Varias personas fueron atacadas por parte del ESMAD con gases lacrimógenos lanzados directamente al rostro de las personas, fueron heridos dos mingueros.

**16 Marzo:** Poblaciones denuncian ataques del ESMAD a campesinos que se encontraban concentrados en el Cairo-Cajibio. completando cuatro días de agresiones por parte de la fuerza publica a los manifestantes.

**17 Marzo:** A eso de las 11:15pm, un  helicóptero de la Policía sobrevoló y lanzó bengalas en el lugar de concentración de El Pital y Monterilla causando pánico entre los mingueros.

**19 Marzo:** En el sector la Agustina el ESMAD dispara con armas de fuego de largo alcance contra tres mingueros dejándolos heridos.

Sobre la vía Panamericana, a la altura del sector de La Agustina, seis comuneros indígenas  son heridos con arma de fuego, causados por ráfagas de fusil, en los sucesos también fallece el patrullero Boris Alexander Benítez.

**20 Marzo:** Comunidades campesinas en Minga Social en El Cairo, Cajibio alertan sobres hostigamientos en horas de la noche de hombres vestidos de negro y sonidos de artefactos explosivos

**21 Marzo:** Sobre la vía del corregimiento de Asnazú municipio de Suárez continúan las confrontaciones con el ESMAD en inmediaciones de la concentración de campesinos, indígenas y afros.

**24 Marzo: **En horas de la mañana la fuerza pública arremete contra la comunidad de Asnazú, municipio de Suárez Cauca, se reporta la utilización de armas de fuego por parte del Ejército

**28 Marzo: **Comunidad de Cerro Tijeras describe que un grupo de personas afrodescendientes que portaba armas, persiguió y disparó contra mingueros  que se movilizaban en el sector de Asnazu, dos comuneros fueron heridos.

**31 Marzo:** En horas de la tade, varias unidades del ESMAD arremeten contra la comunidad de Guanacas,  Tierradentro.

**2 Abril:** Comunidades campesinas que se concentran en la vereda de La Pajosa, denuncian ataques  de personal del ESMAD, Ejército, y civiles armados, uso de gas lacrimógeno contra la comunidad campesina del sector.

Es asesinado Deiner Yunda Camayo, minguero del resguardo indígena de Jebala Totoró, mientras participaba de la minga en el sitio de El Cairo en inmediaciones de la vía Panamericana.

**3 Abril:** A pesar de establecerse que prevalecerían los diálogos  y se daría cese a las agresiones,  indicando que desde las 3:00 am la Fuerza Pública está arremetiendo contra las comunidades en otro sector de la minga en Suárez.

**Reporte de las comunidades **[**indígenas**]

Sobre los continuos embates del ESMAD contra los mingueros, el coordinador señala que **"este es el resultado de un Gobierno que no quiere abrir la puerta a un camino que busca soluciones"** reafirmando que es necesario el reconocimiento de la Minga como una forma de protesta pacífica. [(Lea también: Asesinan a minguero Breiner Yunda tras 22 días de resistencia en Cauca) ](https://archivo.contagioradio.com/asesinan-a-minguero-breiner-yundaque-tras-22-dias-de-resistencia-en-cauca/)

El más reciente reporte realizado por el CRIC da cuenta de una persona fallecida y 50 heridos  de los cuales 15 han sido afectados por recalzadas y tiros de fusil en el marco de la Minga, "la ministra (Nancy Patricia Gutiérrez) nos decía que ellos estaban en la vía de la legalidad, entonces es como si ellos tuvieran permiso para dispararnos".

Asímismo, siete indígenas han sido judicializados, "hay otros que están en proceso que están en El Cairo” asegura Sauca, determinarán cómo avanzar en este proceso, “entendemos que hay algunos que han generado desarmonía” por los que la jurisdicción indígena será la encargada de juzgarlos.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
