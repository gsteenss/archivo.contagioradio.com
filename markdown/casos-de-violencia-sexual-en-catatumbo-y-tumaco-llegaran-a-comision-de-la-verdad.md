Title: Casos de violencia sexual en Catatumbo y Tumaco llegarán a Comisión de la Verdad
Date: 2018-11-28 16:37
Author: AdminContagio
Category: DDHH, Mujer
Tags: Catatumbo, colombia, mujeres, Tumaco, violencia sexual
Slug: casos-de-violencia-sexual-en-catatumbo-y-tumaco-llegaran-a-comision-de-la-verdad
Status: published

###### [Foto: Caracol Radio] 

###### [28 Nov 2018] 

“Yo pienso: estoy muerta, como muerta en vida, que para volver a estar con mi esposo me siento destruida doctora, totalmente, el cuerpo sucio, derrotada; pero con ganas de salir adelante, de luchar a pesar de todo esto", este fragmento hace parte de los 35 relatos que recopiló la Corporación Humanas en los informes sobre violencia sexual en el marco del conflicto armado en el Catatumbo y Túmaco. **Un capítulo que llegará a la Comisión de la Verdad para contar la guerra en los cuerpos de las mujeres.**

Para Adriana Benjumea, directora de la organización, estos documentos tienen dos grandes expectativas: la primera es que las voces de las mujeres sean escuchadas ante la Comisión de la Verdad, para que "el país conozca lo que significó la guerra en sus cuerpos y en sus vidas, y no solo lo que hicieron los agresores de todos los grupos armados, sino además, **como ellas han pasado años viviendo con esas violaciones, sin contar con una atención Estatal**".

Razón por la cual el informe sobre violencia sexual en la región del Catatumbo refleja los daños físicos, sexuales y psicológicos provocados a las mujeres en el marco del conflicto armado, mientras que el de Tumaco, además de estos hechos, **intenta reflexionar frente a las afectaciones de forma colectiva en sus comunidades afro, negras y palenqueras.**

[La segunda expectativa sobre los textos, es que también pretenden evidenciar la falta de acciones por parte del Estado y la responsabilidad que tuvo en estos hechos, ante su incapacidad para evitar que las mujeres fuesen víctimas de violencia sexual y la revictimización que sufrieron posteriormente por parte de la institucionalidad. ]

"Muchas de las mujeres llegaron a centros de salud, a hospitales y jamás se les pregunto por qué tenían esas lesiones, las preguntas que se hicieron se referían a su sexualidad nunca al daño y ellas han tenido que vivir con cicatrices, glándulas mamarias destrozadas" afirma Benjumea, y agrega que derechos como la interrupción legal del aborto en la causal de violación ha sido negada en reiteradas ocasiones a estas mujeres.

Se espera que otros casos de mujeres víctimas de violencia sexual de los departamentos de César y Putumayo también logren ser presentados como informes y ser relatados ante la Comisión de la Verdad. (Le puede interesar: ["FDIM preocupada por implementación del enfoque de género en los Acuerdos de Paz")](https://archivo.contagioradio.com/fdim-preocupada-por-implementacion-de-enfoque-de-genero-en-acuerdos-de-paz/)

### **Las mujeres tienen esperanza en la Paz** 

De acuerdo con Benjumea, la firma del Acuerdo de Paz y el funcionamiento de la Jurisdicción Especial para la Paz, son dos escenarios que han generado esperanza de transformación social para las mujeres, **[espacios a los] que según la directora de Humanas, debe sumarse el diálogo entre la guerrilla del ELN y el gobierno Nacional.**

En ese sentido, Benjumea señala que comprometerse con la paz, es asegurar un fin del conflicto armado con todos los sectores y garantizar que hechos como la violencia sexual no vuelvan a ser padecidos por ninguna persona en el país.

<iframe id="audio_30404419" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30404419_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
