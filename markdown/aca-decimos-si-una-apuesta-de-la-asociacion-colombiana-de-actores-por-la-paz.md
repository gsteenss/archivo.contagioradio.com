Title: ACA decimos sí: una apuesta de la Asociación Colombiana de Actores por la paz
Date: 2016-09-16 16:07
Category: Paz
Tags: AcadecimosSi, Aristas por la paz
Slug: aca-decimos-si-una-apuesta-de-la-asociacion-colombiana-de-actores-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/wrado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: wradio] 

###### [16 Sept 2016] 

Con la refrendación de los acuerdos de paz de la Habana, son muchas las historias que ayudarán a esbozar la Colombia que el conflicto armado a disgregado y los escenarios del arte servirán como espacio para conocerlas. Este es el compromiso que **han adquirido las y los artistas del país, que han expresado su apoyo al sí en el plebiscito por la paz desde la Asociación Colombiana de Actores (ACA).**

Por este motivo, el 15 de septiembre se lanzó la iniciativa **"Acá decimos sí"**, una apuesta desde la ACA que con frases como **“triunfando la paz, el territorio de nuestra nación podrá convertirse en escenario de reconciliación”** se compromete con el futuro del país desde el lenguaje del arte.

Con la etiqueta **\#AcadecimosSi** miembros de este gremio, ratifican que[un país en paz tiene posibilidades para abrir caminos hacía la cultura, la alegría y la vida.](https://archivo.contagioradio.com/festival-de-mujeres-en-escena-por-la-paz-25-anos/) Artistas como Nórida Rodríguez, Julio Correal, Diana Ángel, entre otros, se sumaron a la tendencia con sus mensajes por la paz. Muchas son las iniciativas que han acompañado esta campaña, entre ellas la canción titulada **"Adiós a la guerra"** que se ha convertido en un himno del sí a la paz.

Producciones como “La Siempreviva” de Miguel Torres, “Silencio en el Paraíso”  de Colbert García y la obra teatral “Soma Mnemosine” de Patricia Ariza, entre otras, son experiencias que, desde el arte, [han conectado la violencia con el relato de país y la construcción de memoria](https://archivo.contagioradio.com/somos-diferentes-con-el-mismo-derecho-a-vivir-en-paz/), de igual forma han servido para **evidenciar la necesidad de explotar otro lenguajes culturales para dar a conocer el drama del conflicto armado.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
