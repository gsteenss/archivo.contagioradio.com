Title: Gobierno continúa sin atender precaria situación de indígenas Wounaan desplazados
Date: 2016-07-11 17:54
Category: DDHH, Nacional
Tags: bombardeos, Desplazamiento, ejercito, indígenas
Slug: gobierno-continua-sin-atender-precaria-situacion-de-indigenas-wounaan-desplazados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/3a1156b6-95ba-4ecc-b860-35594a3e70a4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Bernardino Dura 

###### [11 Jul 2016] 

El gobierno nacional no ha atendido la situación en la que viven 476 indígenas Embera Wounaan del resguardo Pichimá en Chocó, que **por cuenta de los continuos bombardeos del Ejército terminaron en condición de desplazamiento.**

**De acuerdo con Bernardino Dura, líder indígena, se trata de** 94 familias que llevan casi tres meses viviendo en albergues del casco urbano del municipio y que son insuficientes, para atender la proliferación de epidemias originadas por el consumo de agua contaminada.

Es por eso que la comunidad demanda de las autoridades municipales, departamentales y nacionales, acciones concretas que permitan mejorar sus actuales condiciones en términos de seguridad, salud y educación, debido a que aseguran sentirse acorralados por la fuerza pública, además señalan que el hacinamiento en el único albergue es preocupante sobre todo por la gran cantidad de niños que hay.

**“La Secretaria de Salud debe darnos soluciones, pero las instituciones estatales nos han abandonado, además necesitamos que cesen los bombardeos para regresar a nuestro territorio",** denuncia el líder indígena, quien agrega que algunos pobladores junto con autoridades municipales, regresaron a los resguardos para verificar la situación y **evidenciaron que los bombardeos aún continúan**.

<iframe src="http://co.ivoox.com/es/player_ej_12223528_2_1.html?data=kpeflJiZdpmhhpywj5aWaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaPZ09PO1MnNstCfpdrfw4qWh4zghqigh6aoqMbmjLzc19PFpc-hhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
