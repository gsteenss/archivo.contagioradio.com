Title: Uribe ha elegido la vía de la presión mediática y política contra el poder judicial: Iván Cepeda
Date: 2019-08-23 08:23
Author: CtgAdm
Category: Judicial, Política
Tags: Alvaro Uribe, Corte Suprema de Justicia, Iván Cepeda
Slug: uribe-ha-elegido-la-via-de-la-presion-mediatica-y-politica-contra-el-poder-judicial-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Cepeda-Uribe.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ivancepedacastro.com/ Twitter ] 

Respecto a la indagatoria que tendrá lugar el próximo 8 y 9 de octubre en contra del senador Álvaro Uribe por presentación de falsos testigos y sus continuas declaraciones en medios y en la plenaria del Congreso apelando al estado de opinión y su inocencia, el senador del Polo Democrático, Iván Cepeda señala que más allá del proceso en contra del expresidente, lo que está de por medio en esta indagatoria es la independencia del poder judicial.

Esto con referencia a la voluntad que se ha expresado desde el Centro Democrático, de acabar con entidades judiciales como la Corte Suprema de Justicia, la Corte Constitucional y la JEP y crear un nuevo tribunal, "para este sector, la solución ya no es litigar ante los jueces y demostrar su inocencia sino cortar el árbol de raíz", añade Cepeda.

### "Le pido al senador Uribe que enfrente su responsabilidad en sede judicial" 

Señala que sin importar el desenlace de esta indagatoria, **la decisión de llamar a Uribe, marca un precedente, llamando a la justicia a un expresidente por primera vez,** razón por la que el dirigente del Centro Democrático, ha decidido hacer una campaña mediática, para "intentar invertir la situación que hay y presentar todo como un montaje judicial y así apelar al estado de opinión y generar un ambiente propicio a su favor".

Advierte que además de esta gira mediática y la intervención realizada por Álvaro Uribe en el Senado durante los últimos días,  también es probable que se vean en el Congreso iniciativas y propuestas que busquen "acabar con la independencia judicial".

Aunque admite que no fue una decisión fácil el privarse de asistir a la diligencia, afirma que lo hacen en coherencia con lo que él y sus abogados han afirmado, **"no vamos a generar una presión ante la Corte, ni generar ningún tipo de motivo para que se diga que estamos intimidando a la contraparte"** afirma. haciendo la salvedad que continuarán litigando el proceso.

"Aquí debe brillar la verdad y eso es lo que vamos a seguir  haciendo en estos procedimientos, creemos que es fundamental que haya justicia, verdad y reparación" agrega el senador del Polo Democrático.  [(Lea también: "Uribe esta haciendo una tenebrosa campaña contra la Corte Suprema" Iván Cepeda)](https://archivo.contagioradio.com/uribe-esta-haciendo-una-tenebrosa-campana-contra-la-corte-suprema-ivan-cepeda/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_40561945" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40561945_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
