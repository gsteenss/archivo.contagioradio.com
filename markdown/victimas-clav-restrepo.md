Title: Víctimas que se tomaron el CLAV logran creación de mesa de participación
Date: 2017-12-12 15:05
Category: DDHH, Nacional
Tags: Bogotá, centro de atención a víctimas, clav, víctimas del conflicto armado
Slug: victimas-clav-restrepo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DQ2zAfsW0AAD1TU-e1513097437470.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UPP Bogotá] 

###### [12 Dic 2017] 

Las víctimas del conflicto armado que se encontraban protestando en el Centro Local de Atención de Víctimas, CLAV, en el barrio el Restrepo de Bogotá para que se garanticen sus derechos, anunciaron que **acordaron una hoja de ruta** para trabajar en el diálogo con las instituciones sobre los temas consignados en el pliego de peticiones. Con esto, se buscará concretar el desarrollo de la mesa de participación.

Esta mañana, las víctimas habían denunciado que no se les permitió el ingreso de comida y frazadas en el momento que ingresaron de manera pacífica al edificio del CLAV.  Además, indicaron que las autoridades locales **los han amenazado con enviar al ESMAD** para que se retiren de la protesta pacífica. (Le puede interesar: ["Víctimas de Ciudad Bolívar se toman edificio  redignificar"](https://archivo.contagioradio.com/victimas-de-ciudad-bolivar-realizan-toman-de-edificio-para-la-atencion/))

### **Estos fueron los acuerdos a los que llegaron ** 

En el acta de reunión, se constata que la Alta Consejería para los Derechos de las Víctimas, la Paz y la Reconciliación, la Personería Delegada para la Protección  de las Víctimas, la Personería Local de Rafael Uribe, la Policía Nacional, los gestores de Convivencia de la Secretaría de Seguridad y los delegados de las víctimas y defensores de derechos humanos, acordaron la creación de una hoja de ruta que tenga en cuenta las exigencias que hicieron públicas las víctimas.

Allí, acordaron **discutir el pliego de peticiones**, crear una mesa de participación que cuente con la presencia de garantes de la comunidad internacional y la participación efectiva de las víctimas que hacen parte de la Mesa de Víctimas de Ciudad Bolívar. Además, dejaron constancia de que requieren un garante en la mesa de trabajo que haga parte de la Comisión Intereclesial de Justicia y Paz. Por otro lado, acordaron el traslado de las personas a Ciudad Bolívar donde se instalará la mesa de negociaciones.

### **Representante Alirio Uribe pidió instalación de mesa de concertación** 

Peña indicó que la Policía intentó entrar al edificio por la fuerza por lo que las víctimas han dejado en claro que se trata de una protesta pacífica para dar a conocer una serie de problemas que las instituciones locales y nacionales **no han atendido**. Es por esto que el día de ayer habían solicitado la presencia de funcionarios de la Unidad de Víctimas, la Alta Consejería para los derechos humanos, la secretaría de Habitat y la Alcaldía de Bogotá; funcionarios que no han hecho presencia en el sitio aún.

Por su parte, el representante a la Cámara por Bogotá Alirio Uribe, hizo presencia en el lugar para dialogar con las víctimas. Tras el encuentro, el representante exigió que se establezca una mesa de concertación con las autoridades y **denunció que no pudo entrar a las instalaciones** pues los funcionarios no se lo permitieron. Por esto, le pidió al alcalde de Bogotá que realice las diligencias necesarias para que las víctimas sean escuchadas y atendidas en el centro de atención local. (Le puede interesar: ["Víctimas conmemoran los 20 años de la operación Septiembre Negro"](https://archivo.contagioradio.com/spetiembre_negro_operacion_victimas_memoria/))

### **Víctimas llevan meses preparándose para exigir sus derechos** 

De acuerdo con Yaneth Peña, vocera de las víctimas del conflicto armado que se están en el edifico del CLAV, para lograr establecer una mesa de negociación y poder reclamar sus derechos, las víctimas que viven en Ciudad Bolívar han venido desarrollando un **proceso de capacitación con diferentes asesores** en la medida en que, como lo afirma Yaneth Peña, “las instituciones están acostumbradas a jugar con la ignorancia del pueblo, pues en su mayoría somos personas campesinas que desconocemos cosas de nuestros derechos”.

Finalmente, las víctimas indicaron que van a continuar con la protesta pacífica y afirmaron que las autoridades requieren que la instalación de la mesa se haga en un ligar diferente al CLAV. **Esperan llegar a un acuerdo** donde se garantice unas condiciones de vida digna que cumplan con la reparación colectiva que se debe realizar a las víctimas.

![clav comunicado 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/clav-comunicado-1.jpeg){.alignnone .size-full .wp-image-49911 width="960" height="1280"} ![clav comunicado 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/clav-comunicado-2.jpeg){.alignnone .size-full .wp-image-49912 width="960" height="1280"} ![clav comunicado 3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/clav-comunicado-3.jpeg){.alignnone .size-full .wp-image-49913 width="960" height="1280"}

<iframe id="audio_22607736" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22607736_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
