Title: 5 Rostros de Simón Bolívar en el cine
Date: 2017-07-24 17:06
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Simón Bolívar
Slug: bolivar-cine-aniversario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Boliavr.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 

###### 24 Jul 2017 

A propósito del natalicio 234 de Simón Bolívar, varias son las ocasiones en las que el cine ha puesto sus ojos en la figura del libertador de América. Aunque la mayoría de las versiones que existen fueron realizadas en Venezuela, patria natal de Bolívar, directores y actores internacionales han estado interesados en llevar a la pantalla su vida y luchas.

Desde "Simón bolívar" película mexicana del año 1942 hasta "El libertador" de 2014, varios proyectos, algunos estrenados otros inconclusos como los propuestos por directores de renombre como Lester Cowan o Dino de Laurentiis y famosos actores como Charlton Heston, Laurence Olivier o Gregory Peck para encarnar al libertador de 5 naciones.

En su aniversario, recordamos 5 de las producciones que pasaron por las salas, algunas con mejores críticas  que otras, pero todas con una mirada distinta de "el hombre de las dificultades" ( Le puede interesar: [Daupará, muestra de cine y video indígena abre sus convocatorias](https://archivo.contagioradio.com/daupara-cine-indigena/))

**Bolívar soy yo (2002)**

<iframe src="https://www.youtube.com/embed/Ev4j7u_apVg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Comedia colombiana dirigida por Jorge Alí Triana y protagonizada por Robinson Díaz, que está inspirada en la vida de Pedro Montoya, un actor risaraldense que durante los años ochenta encarno al personaje del libertador en la serie Televisiva "Revivamos nuestra historia", dirigida por el mismo Triana, quien para los espectadores empezó a convertirse en la imagen viva de Bolívar.

**Bolívar el héroe (2003)**

<iframe src="https://www.youtube.com/embed/sxx7RTE39vA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Con bastantes críticas por su estilo y caracterizaciones, Bolívar el héroe, es una apuesta poco convencional de animación producida en Colombia. Bajo la dirección y producción de Guillermo Rincón, la película cuenta la vida de Bolívar y su lucha por liberar a los pueblos, con una estética y narrativa similar a la utilizada por el animé japonés.

**Bolívar el hombre de las dificultades (2013)**

<iframe src="https://www.youtube.com/embed/JMpuTpJxrUM" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La cinta coproducida entre Venezuela y España esta enmarcada en los acontecimientos que rodearon la vida de Bolívar durante los años 1815 y 1816, años en que estuvo en el destierro, mezclando drama, romance y aventura. La producción, dirigida por Luis Alberto Lamata y protagonizada por Roque Valero, inicia con la partida del recién nombrado libertador hacia Cartagena como fugitivo, su paso por Jamaica y Haití y su persistencia en la idea de conseguir la libertad para los pueblos de América ante la adversidad.

**Diario de Bucaramanga (2013)**

<iframe src="https://www.youtube.com/embed/7XX7ldJlbj4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La cinta esta basada en el diario del general francés Luis Perú de Lacroix, escrito durante su época como edecán de Bolívar durante el año 1828, año en el que el libertador y su Estado Mayor se encontraba en la población colombiana de Bucaramanga mientras esperaba noticias decisivas sobre la Convención de Ocaña, en la cual debe producirse una nueva Constitución para Colombia. La producción es dirigida por Carlos Fung y protagonizada por Simón Pestana, Patrick Delmas y Luis Mesa.

**El libertador (2014)**

<iframe src="https://www.youtube.com/embed/X4hmftmmp6A" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Es la cinta con mayor presupuesto que se ha realizado sobre la vida de Bolívar. La dirección estuvo a cargo de Alberto Arvelo y es protagonizada por Édgar Ramírez (quien también es el Productor Ejecutivo) y con música del maestro Gustavo Dudamel. La cinta fluctúa entre episodios de un joven e idealista Bolívar con los acontecimientos posteriores a la victoria criolla sobre las fuerzas del imperio español, en una historia que combina amor, guerra y traición.
