Title: El fracaso de la política exterior de Duque a 18 meses  de su Presidencia
Date: 2020-02-19 18:22
Author: CtgAdm
Category: Nacional
Tags: Gobierno Duque, Noruega
Slug: el-fracaso-de-la-politica-exterior-de-duque-a-18-meses-de-su-presidencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Duque-Internacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/antonio-sanguino-sobre-politica-exterior-duque_md_48221583_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Antonio Sanguino | integrante del Partido Verde

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Iván Duque: Presidencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El nuevo informe presentado por el senador [Antonio Sanguino](https://twitter.com/AntonioSanguino) del Partido Verde detalla los primeros 18 meses de la política exterior del gobierno Duque, caracterizada por un rechazo al diálogo y un retorno a prácticas del pasado en términos de seguridad y lucha contra las drogas centrando su atención más en la situación en Venezuela que en la prevalencia del Acuerdo de Paz, lo que ha llevado a un distanciamiento con los países garantes como Cuba y Noruega, o con Naciones Unidas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**El informe titulado "A un año del Fracaso del 23F, Radiografía a la Política Exterior del Gobierno de Iván Duque"** cubre el periodo comprendido entre el 7 de agosto de 2018 y el 31 de enero de 2020 y según Sanguino, evidencia cómo la política exterior "ha sufrido un retroceso pasando de una diplomacia para la paz a una política en favor de la guerra y la lucha contra el terrorismo", siendo, "una muestra de la terquedad de un Gobierno desprovisto de ideas en los escenarios regionales y globales".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Venezuela, único tema de la agenda exterior del Gobierno Duque

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La responsabilidad no solo recae sobre Iván Duque, durante la estancia de Carlos Holmes Trujillo en la Cancillería hasta noviembre de 2019, en sus múltiples giras solo hizo referencia a temas ligados a la crisis venezolana, sin informar sobre demás cuestiones de interés para las relaciones bilaterales de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"A ratos, Carlos Holmes Trujillo parecía el canciller de Juan Guaidó, presidente interino de Venezuela y, a otros, el vocero del Centro Democrático"** afirma Sanguino en su informe, al mostrar en el informe que cuando el canciller no se refería a la situación del país vecino, se refería a modificaciones al Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A este problema diplomático se suma la salida de Colombia de la Unión de Naciones Suramericanas, (Unasur) el 27 de agosto de 2018, por considerar al organismo “una caja de resonancia de Venezuela”. Finalmente en enero de 2019 se concretó el rompimiento de relaciones. [(Le puede interesar: A 16 años del Embrujo Autoritario llega Iván, el aprendiz)](https://archivo.contagioradio.com/a-16-anos-del-embrujo-autoritario-llega-ivan-el-aprendiz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En su informe, Sanguino señala que la participación de Colombia en otras organizaciones, como el Grupo de Lima, se centró en consolidar el "cerco diplomático" contra Venezuela, una decisión que además de acercarlo a las políticas del gobierno de Donald Trump fue reforzada con **el retorno de la fumigación con glifosato y la constante advertencia sobre la presencia de grupos como el ELN o disidencias de las FARC** en Venezuela, para apelar a políticas de seguridad más no de diálogo. [Desconocimiento e incumplimiento del Gobierno con la sustitución de cultivos de uso ilícito](https://archivo.contagioradio.com/coccam-exige-garantias-para-las-familias-que-trabajan-por-la-sustitucion-de-cultivos/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "El balance es negativo y desastroso y está sumado a la mermelada con la que el Gobierno ha repartido los cargos en su coalición de gobierno sin que las personas designadas para esos cargos cuenten con la competencias para representar al país en escenarios internacionales".

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Las falencias resaltadas por Sanguino, contrastan con información recopilada por distintos medios que revelan cómo en tan solo 11 meses de gobierno, Duque viajó 18 veces al extranjero, con un costo para los colombianos de 1.500 millones de pesos; de seguir con esta tendencia podría ser el presidente que más gaste en salidas durante su Gobierno.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Deterioro de las relaciones con la ONU y los países garantes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, la insistencia en la reforma del Acuerdo de Paz centrada en el aumento de cultivos de uso ilícito, la necesidad de continuar la lucha contra las drogas y dar prioridad a la llamada paz con legalidad, puso a Colombia en curso de colisión con los países garantes Cuba y Noruega y así como Naciones Unidas, que advirtieron sobre las consecuencias de no avanzar en la implementación o detener el avance de la ley estatutaria de la Justicia Especial para la Paz (JEP), una de las columnas del Acuerdo. [(Lea también: Duque y su estrategia internacional para desajustar el proceso de paz)](https://archivo.contagioradio.com/duque-y-su-estrategia-internacional-para-desajustar-el-proceso-de-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tal como han advertido organizaciones sociales el doble mandato de asesoría y observación de la Oficina del Alto Comisionado de Derechos Humanos de la ONU, ha generado malestar en el gobierno Duque, lo que quedó demostrado al plantear desde la casa de Nariño una reforma que limitara la verificación y avalara la prohibición de los pronunciamientos públicos sin autorización previa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Preocupó además, durante octubre de 2019 la no renovación del mandato en el país de la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos, pese a que **la misma fue firmada** hasta 2022. Expertos han advertido que la lentitud en estos trámites denotan una incomodidad del Gobierno ante una supervisión de la comunidad internacional. [(Lea también: Al Gobierno colombiano le irrita la vigilancia internacional: Alberto Yepes)](https://archivo.contagioradio.com/al-gobierno-colombiano-le-irrita-la-vigilancia-internacional-alberto-yepes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A ello se suman los frecuentes cuestionamientos del presidente Duque e integrantes de su gabinete, quienes insisten en la solicitud de extradición desde La Habana a los negociadores del ELN – lo que violaría el protocolo establecido en caso de rompimiento de la mesa de diálogo, una situación que han advertido académicos, **ha logrado un escalonamiento de las tensiones con el país insular;** hecho que se evidenció cuando Colombia se abstuvo de votar en contra del bloqueo de Cuba, cambiando históricamente de posición ante el mundo. [(Recomendamos leer: No se puede menosprecia el rol de Cuba en los procesos de paz del mundo](https://archivo.contagioradio.com/no-se-puede-menosprecia-el-rol-de-cuba-en-los-procesos-de-paz-del-mundo/)[)](https://archivo.contagioradio.com/academicos-del-mundo-piden-a-la-onu-exigir-cumplimiento-de-protocolos-con-el-eln/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
