Title: Prisionera víctima de caso Andino está en aislamiento por posible contagio de Covid 19
Date: 2020-05-12 19:29
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Andino, #Cárcel, #Picaleña, #PrisioneraPolítica
Slug: prisionera-victima-de-caso-andino-esta-en-aislamiento-por-posible-contagio-de-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/lina.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 11 de mayo, las prisioneras políticas, Alejandra Méndez y Lizeth Rodríguez, víctimas del montaje judicial conocido como caso Andino, alertaron sobre el aislamiento de su compañera y también prisionera política **Lina Jiménez** por posible contagio de Covid 19. Los hechos se habrían producido durante un traslado arbitrario del que fueron sujeto las tres mujeres a la cárcel de Coiba-Picaleña en Ibagué.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con Alejandra, Lina presenta **síntomas como fiebre y afectaciones respiratorias**. Razón por la cual, el 11 de mayo fue aislada a la espera de que le den los resultados de la prueba de Covid 19. Sin embargo, al resto de mujeres que se encontraban con ella, aún no les practican la prueba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las mujeres fueron trasladadas desde el pasado 22 de marzo de la Cárcel del Buen Pastor a la cárcel de Picaleña en Ibagué. Durante el trayecto, las prisioneras políticas denunciaron que **ninguno de los guardia usó tapabocas o elementos de bioseguridad para evitar contagios**. Asimismo, afirman que este es el único contacto que han tenido con agentes externos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente en la cárcel de Picaleña se registran 12 casos positivos del Covid 19. **Diez de ellos son guardias del INPEC**, y los otros dos son trabajadores del centro de reclusión. Según las mujeres, la prisionera política Lina Jiménez habría tenido contacto con esos guardias. (Le puede interesar: ["Mujeres detenidas en cárcel Picaleña exigen protocolos contra Covid 19"](https://archivo.contagioradio.com/mujeres-detenidas-en-carcel-picalena-exigen-protocolos-contra-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las alarmas ya estaba encendidas en Picaleña
--------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 8 de abril del presente año, un informe de la Personería de Ibagué evidenció que en este centro de reclusión no se cuentan con protocolos para el control de la pandemia. (Le puede interesar: ["Jueces fallan a favor de población reclusa en 3 cárceles"](https://archivo.contagioradio.com/jueces-fallan-a-favor-de-poblacion-reclusa-en-3-carceles/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, el pasado 20 de abril el Juez de Familia de la ciudad de Ibagué ordena al INPEC proteger el derecho al agua. Entre las medidas que solicita se encuentra dar solución al abastecimiento del agua de forma continua y permanente. Asimismo, **insta a las autoridades correspondientes a cumplir con los protocolos** y lineamientos para el control del Covid 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, la población reclusa denuncia que ninguna de las medidas falladas a su favor, se han puesto en marcha. También expresan que con los resultados positivos de los guardias del INPEC es urgente que **la prueba del Covid se le realice a toda la población carcelaria** para evitar un foco de propagación. (Le puede interesar: " «[Rumbo a un genocidio carcelario](https://www.justiciaypazcolombia.com/rumbo-a-un-genocidio-carcelario/)«)

<!-- /wp:paragraph -->
