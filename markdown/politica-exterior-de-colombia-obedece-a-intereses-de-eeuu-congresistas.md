Title: Política exterior de Colombia obedece a intereses de EEUU: Congresistas
Date: 2020-06-03 14:05
Author: CtgAdm
Category: Actualidad, El mundo
Tags: Donald Trump, Gobierno Duque, Miguel Ceballos, Venezuela
Slug: politica-exterior-de-colombia-obedece-a-intereses-de-eeuu-congresistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/180925_02_ReunionBilateral_1800.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/eeuu.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Presidencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante debate en el Congreso, se puso bajo la lupa la forma en que el gobierno Duque ha abordado la política exterior durante dos años de administración, evidenciando la forma en que el país se ha sometido a las decisiones del gobierno de Estados Unidos

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El más reciente episodio tiene que ver con el envío de tropas de EEUU territorio colombiano sin previa autorización, además de la deuda que existe hacia Cuba, país garante de las negociaciones de paz con las FARC y el ELN y al que se ha puesto en una situación que ha dificultado la relación entre ambas naciones.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Política exterior colombiana pone a prueba la paciencia de Cuba

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que el Alto Comisionado para la Paz, Miguel Ceballos ha responsabilizado a Cuba de la no entrega y extradición de los negociadores del ELN que permanecen en la isla pese a los protocolos internacionales que blindan la negociación y que en recientes declaraciones ha apoyado la decisión del Departamento de Estado de EE.UU. de incluir a Cuba en la lista de países que no respaldan la lucha contra el terrorismo. [(Lea también: Arrogancia del Gobierno no le permite ver oportunidades políticas con el ELN: De Currea-Lugo)](https://archivo.contagioradio.com/arrogancia-del-gobierno-no-le-permite-ver-oportunidades-politicas-con-el-eln-de-currealugo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, el senador Iván Cepeda señala que **la política exterior colombiana ha estado sometida al ala más radical de la extrema derecha de la Casa Blanca,** "es una política de entrega total de [Donald Trump](https://twitter.com/potus) y una posición muy agresiva con relación al proceso de paz en Colombia y a despertar un conflicto armado con Venezuela y a lesionar de la manera más grande a Cuba",

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por tal motivo los senadores Iván Cepeda y Antonio Sanguino interpondrán una queja disciplinaria contra el Alto Comisionado de Paz a quien pidieron su renuncia hace unas semanas, pues señalan no le ha cumplido a la implementación del Acuerdo de Paz ni con las responsabilidades que le corresponden a su cargo. [(Le puede interesar: Miguel Ceballos no le cumple a la paz sino que "promueve el conflicto armado": Cepeda)](https://archivo.contagioradio.com/miguel-ceballos-no-le-cumple-a-la-paz-sino-que-promueve-el-conflicto-armado-cepeda/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “No ha escatimado energías para eludir con pretextos y maromas legales la implementación del Acuerdo de Paz, ha desconocido los valiosos acuerdos a los que se llegó después de arduas negociaciones que duraron años con el ELN y ha burlado los acuerdos con los países garantes”. - Iván Cepeda

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que **se trata de un un rasgo que no solo caracteriza a funcionarios como Miguel Ceballos sino de otras figuras como Carlos Holmes Trujillo y la administración Duque en general**. Por su parte, expresa que Cuba ha hecho acopio de paciencia y tolerancia con relación a "las acciones hostiles que ha emprendido el Gobierno colombiano contra Cuba", entre ellas la negación de un protocolo que obliga a cumplir compromisos de índole internacional o o hechos históricos cómo abstenerse en la votación en la ONU que condena el bloqueo de Estados Unidos hacia el país insular.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Decisiones de los gobiernos fortalecen el descontento y la movilización social

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Cepeda advierte que a raíz de las decisiones que están tomando los líderes mundiales, se verá el surgimiento de movilizaciones sociales más robustas, como ha ocurrido en EE.UU. a raíz de un acto de violencia racial y que en Colombia donde "se ve una mayor indignación con relación a las medidas que ha tomado el Gobierno", **el Paro nacional del 21 de noviembre de 2019 será tan solo un preámbulo del descontento e indignación de la población.** [(Lea también: George Floyd y los tres detonantes de las protestas en EE.UU.)](https://archivo.contagioradio.com/george-floyd-y-los-tres-detonantes-de-las-protestas-en-ee-uu/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la llegada de una brigada de Asistencia de Fuerza de Seguridad perteneciente al Ejército de los EE.UU, el senador considera que se trata de una acción parte de una intervención militar en Colombia, de la que él, junto a otros 50 congresistas aún esperan una respuesta del Gobierno para conocer el propósito de su llegada al país. [(Lea también: Operaciones militares de EEUU en Colombia deben ser autorizadas por el Congreso)](https://archivo.contagioradio.com/operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto y con la estadía de estas tropas durante al menos los próximos cuatro meses, señala que se debe explicar al país por qué el anuncio de la llegada de las tropas extranjeras lo hizo la embajada de EE.UU. y no el Gobierno, haciendo caso omiso al artículo 173 de la Constitución, por lo que esperan en el menor tiempo posible considerar a los integrantes de esta brigada personas no gratas para que puedan regresar a Norteamérica.  [(Lea también: Es momento de redoblar los esfuerzos por la paz: Iván Cepeda)](https://archivo.contagioradio.com/redoblar-esfuerzos-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El senador concluye que este tipo de accionar por parte del gobierno Duque no solo pone en tensión a toda la región de Latinoamérica y aumenta la tensión con el gobierno venezolano sino que configura un escenario de confrontación con otras naciones como Rusia y China [(Lea también: No se puede menosprecia el rol de Cuba en los procesos de paz del mundo)](https://archivo.contagioradio.com/no-se-puede-menosprecia-el-rol-de-cuba-en-los-procesos-de-paz-del-mundo/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
