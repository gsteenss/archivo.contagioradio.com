Title: Listos los 78 preseleccionados para integrar la JEP
Date: 2017-09-18 18:01
Category: Nacional, Paz
Tags: justicia especial para la paz, magistrados
Slug: jep-magistrados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [18 Sep 2017] 

Ya están listos los nombres de los 78 aspirantes preseleccionados por el Comité de Escogencia, que aspiran a ser magistrados del Tribunal para la Paz y de las salas justicia de la Jurisdicción Especial para la Paz. **De 2.328 postulados, 40 serán entrevistados para el Tribunal y 38, para las salas de justicia.**

En la lista se encuentran personas como el académico Rodolfo Arango,  uno de los artífices de la JEP, Yesid Reyes, así como el exmagistrado y expresidente de la Corte Constitucional, Eduardo Cifuentes. En cambio no aparecieron personajes como Martha Lucía Zamora, exfiscal general, o Mónica Cifuentes, asesora jurídica del Comisionado para la Paz y la exprocuradora para asuntos de Infancia de Alejandro Ordónez.

En el comunicado del Comité de Escogencia, se informa que se hará pública la grabación de las entrevistas de los(as) aspirantes al cargo, y el próximo 26 de septiembre de 2017,  será publicado el listado de las personas seleccionadas.

<iframe id="doc_7074" class="scribd_iframe_embed" title="COMUNICADO 16 LISTA Entrevistas Tribunal y Salas JEP_ (002)" src="https://www.scribd.com/embeds/359266171/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-TppArKi7mp5fiWMqXyVI&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
