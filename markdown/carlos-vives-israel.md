Title: En solidaridad con Palestina piden a Carlos Vives no cantar en Israel
Date: 2018-07-17 08:58
Category: Otra Mirada
Tags: Apartheid Israel, BDS Colombia, Palestina
Slug: carlos-vives-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Carlos-Vives-e1531835578610.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Frank Micelotta/Invision/AP 

###### 17 Jul 2018 

Desde el movimiento Boicot Desinversiones y Sanciones a Israel (BDS) en Colombia, iniciaron una campaña en la que **invitan al artista samario Carlos Vives a solidarizarse con el pueblo palestino y no presentarse en Israel**, como parte del su gira Vives Tour, el próximo 31 de julio.

Los activistas por los derechos de la comunidad palestina, le envian un mensaje al cantante vallenato en el que le dicen "**si quieres que la paz trabaje por la justicia, no cantes ante la colonización, apartheid y genocidio en Palestina**, no ayudes a Tel Aviv a lavar sus crímenes contra el pueblo palestino y la humanidad".

Como parte de su campaña, el movimiento le recuerda a Vives que Israel es el país que cometer más violaciones contra los Derechos humanos en el mundo, y que **cantar allí "es apoyar al Apartheid y dar la espalda a toda la gente que están a muy pocos kilómetros del escenario**, que sufren la ocupación militar, la limpieza étnica y una sistemática y permanente violación a sus derechos humanos".

Otra de las organizaciones que se ha sumado al llamado es la **Asociación cultural Colombo Árabe de Cartagena**, quienes redactaron un comunicado dirigido al artista "nosotros te queremos pedir que te unas al clamor nuestro y ayudar a parar la limpieza étnica y romper el apartheid del que es víctima miles de palestinos en los territorios ocupados ilegalmente"

"Tu voz es importante y sería un orgullo profundo para nosotros como colombianos y como parte del mismo género humano **que tu conciencia, que tus principios, y tus convicciones humanitarias profundas, nuevamente se pongan de lado y al servicio del más débil**, esta vez, al pueblo palestino" señalan en la misiva (Le puede interesar: [Piden a Messi y Argentina no jugar amistoso en Israel](https://archivo.contagioradio.com/messi-amistoso-israel/))

###### Reciba toda la información de Contagio Radio en [[su correo]
