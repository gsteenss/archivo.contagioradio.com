Title: Alcaldes y gobernadores respaldan el carácter humanitario de los acuerdos de paz
Date: 2016-10-05 12:34
Category: Nacional, Paz
Tags: acuerdos de paz ya, plebiscito por la paz, zonas transitorias
Slug: alcaldes-y-gobernadores-respaldan-el-caracter-humanitario-de-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Comunidades-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidades de Paz] 

###### [5 de Oct 2016] 

Alcaldes y gobernadores de 19 departamentos donde ganó el sí en el  plebiscito por la paz, se reunirán para establecer una agenda de iniciativas que permitan el **avance en la construcción de paz en sus territorios, así como respaldar el cese bilateral y la protección e implementación de los acuerdos de la Habana.**

Del encuentro se espera que salga un **llamado a todas las fuerzas políticas e institucionales del país para que tengan sensatez** y finalmente logren finiquitar la guerra en Colombia. De igual modo, buscan solidarizar a todos los sectores de la sociedad para que se **movilicen en torno a la protección de los acuerdos de paz.**

El anunció de ayer, por parte del presidente Santos sobre el fin del cese bilateral, el próximo 31 de octubre, también ha **alertado a estas poblaciones, ya que muchos de sus territorios tenían zonas transitorias** en donde se ubicarían guerrilleros de las FARC-EP y en donde ya se habían establecido compromisos con las comunidades para que el tránsito se diera de forma tranquila y sin riesgos.

Para el alcalde de Puerto Asís Omar Guevara, el respaldo a la paz, que dio su comunidad en el plebiscito, significó cumplir con una decisión de carácter humanitario “lo que está en juego aquí son los [millones de colombianos que han vivido las consecuencias directas de este conflicto armado”](https://archivo.contagioradio.com/granadas-de-mortero-del-ejercito-ponen-en-riesgo-la-vida-de-campesinos-en-puerto-asis/), de igual forma afirmó que “Si nosotros los que hemos vivido directa e indirectamente las consecuencias de la guerra, perdonamos y todo lo demás, **por qué el resto de colombianos no lo pueden hacer”.**

Municipios como [Apartado, Turbo, Cartagena del Chaira, entre otros, han vivido el conflicto armado con mayor intensidad](https://archivo.contagioradio.com/en-los-lugares-mas-afectados-por-la-guerra-gano-el-si/). Para estas comunidades y territorios, desde que inició el proceso de paz  y se dio la declaración del cese al fuego bilateral, los cambios en la cotidianidad han sido visibles, esto debido a que **la dinámica del conflicto armado disminuyó, permitiéndoles habitar en la tranquilidad.**

<iframe id="audio_13192504" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13192504_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
