Title: Gobierno de Grecia promete cerrar los Centros de Internamiento de Inmigrantes CIE's
Date: 2015-02-24 18:11
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: Centro de Internamiento de Inmigrantes Grecia, Grecia, Grecia quiere cerrarCIE'S, Inmigración Grecia, Syriza quiere cerrar CIE
Slug: gobierno-de-grecia-promete-cerrar-los-centros-de-internamiento-de-inmigrantes-cies
Status: published

###### Foto:Guerrerosdelteclado.wordpress.com 

A través de un comunicado del Ministro de Protección Ciudadana el gobierno de **Syriza** declaró el sábado su intención de cerrar los Centros de Internamiento de Inmigrantes, después de que un inmigrante paquistaní se suicidara.

Tras haber visitado el centro de Amygdaleza, el más cercano a la capital, el ministro de protección ciudadana **condenó el suicidio del joven inmigrante** y declaró como centros donde se violan sistemáticamente los derechos humanos por el simple hecho de ser inmigrante.

Durante toda la semana se han presentado protestas en los CIE'S, pero especialmente en el ubicado en Atenas, capital griega, que fue construido para albergar 1000 internos y que hoy está al doble de su capacidad.

Organizaciones de derechos humanos han expresado al nuevo gobierno que debe de cumplir lo prometido y cerrar los centros, a lo que **ha respondido el ejecutivo que así será pero que necesitan" tiempo para buscar una solución**".

La solución mas probable es la **conversión de dichos de los CIE'S en centros de acogida para inmigrantes ilegales en situación de exclusión**.

A pesar de las declaraciones del gobierno organizaciones sociales de defensa de los inmigrantes ilegales **temen que no se pueda acometer la nueva reforma debido a su socio en el gobierno, el partido derechista y antiinmigración "Griegos Independientes"**, con los que sería necesario su apoyo parlamentario.

Los **CIE'S** se construyeron como **parte de la política de la UE para la inmigración en la frontera sur, es decir en España, Grecia, e Italia, países receptores de la gran mayoría de inmigrantes ilegales** que llegan en situaciones de grave riesgo para su salud a travesando el mar mediterráneo en barcas totalmente precarias.

Además de ser centros en situación de alegalidad con respecto a las constituciones de dichos países, no cumplen con los derechos fundamentales de las personas migrantes al penar una práctica que no se puede considerar como delito. A esta situación hay que añadir las **constantes violaciones de DDHH en su interior denunciadas por distintas campañas que demandan su cierre inmediato**.
