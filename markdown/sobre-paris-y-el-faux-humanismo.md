Title: Sobre París y el “faux” humanismo
Date: 2015-11-17 15:26
Category: Opinion
Tags: ataques en francia, atentado en Paris, Beirut, Charlie Hebdo, gobierno frances, isis, Mateo Córdoba
Slug: sobre-paris-y-el-faux-humanismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/francia-atentados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: 20 minutos] 

#### **[Mateo Córdoba - [@DurkheimVive  ](https://twitter.com/DurkheimVive)]** 

###### 17 Nov 2015 

[Pareciera ser que todo intento por comprender y poner en sus justas proporciones lo sucedido en París implicaría ineludiblemente justificar la masacre, pues claro,  en medio de la muerte cualquier asomo de]*[mea culpa]*[ occidental supone asumir yerros que ni siquiera en función del humanismo que profesamos estaremos dispuestos a reconocer. Podríamos entrar a preguntarnos ¿Qué tan insolidaria es la solidaridad selectiva? ¿Qué tan coherente es el rechazo al terror solo en cuanto ponemos víctimas cercanas? El problema evidentemente no es el siempre viral y filantrópico aliento que se hace manifiesto cada que el terrorismo golpea a la puerta de nuestro venerado oeste. El asunto de fondo es, fundamentalmente, lo que me permitiré llamar “humanismo mocho”.]

Y si, es una discusión que al parecer se dio en vísperas del ataque al Charlie Hebdó en el mes de enero. Que si todos éramos Charlie o no, ser o no ser era la cuestión. Debemos entender que el tránsito de las dolencias ante el terrorismo está determinado por un puñado de ingredientes no menores. La mezquindad de las manifestaciones de dolor ante el terrorismo se devela cuando comprendemos que la nobleza no está en el millón de formas en que rechazamos un atentado sino, más bien, en la única forma de rechazar los millones de atentados. Ante la tragedia en París, Obama comparecía ante la comunidad internacional alegando que el ataque no era contra París, que el atentado había sido contra la humanidad. A partir de esto es que se empieza a caer la máscara del falso internacionalismo, pues parece ser que esos “valores universales” que han sido vilipendiados en Paris son los mismos que se vienen deshonrando hace décadas en Medio Oriente. No sobra insistir en que el fondo de este artículo no es hacer una analogía barata y anacrónica entre militarismos y relaciones internacionales enfiladas a la miseria de los pueblos árabes, pues lo que ha quedado en evidencia es, de nuevo, un cerco mediático que supone directamente un cerco emocional tras lo cual aparece el comodín de la desdicha ante tan cruento cruce de los límites de los cabales democráticos y humanitarios, ejes retóricos de la vida occidental.

Seguramente no debamos saber ubicar a Beirut (donde un día antes al atentado de Paris ISIS asesinó a 41 personas) en el mapa, ni mucho menos estamos comprometidos con rastrear las coordenadas históricas de la invasión israelí en Palestina, que seguramente no repara mucho en esos “valores universales” que conforme vamos revisando su puesta en práctica se vuelven más abstractos. No hace falta que hablemos de Siria y ni qué decir de Irak. De igual manera, nos cuesta comprender en donde queda Chocó o el Cauca, parecen realidades equivalentemente lejanas y ajenas para el colombiano de a pie, así como para la infraestructura institucional.

[Quizás, y a fin de cuentas, nuestras reacciones ante la barbarie están señaladas por aquel entre-lugar del complejo de inferioridad y el arribismo. Claro, somos lo suficientemente fraternales como para sentir como nuestra la tragedia parisina, aún cuando insistan en vociferar ruines personajes como María Fernanda Cabal (Ver trinos en @MariaFdaCabal). Tal vez nos llena de temor sospechar que el fundamentalismo religioso esté a la vuelta de la esquina tras lo sucedido en Francia. Además, como buenos faltos de autoestima debemos indignarnos por cualquier agravio contra los patrones del mundo, aunque se nos esté quemando la casa de puertas hacia adentro. Pese a lo anterior, el arribismo brota cuando banalizamos las bombas que caen a diario en territorio árabe, también contra la humanidad sin que Obama dedique unas palabras de aliento a las víctimas ni CNN cubra por más de 3 minutos el drama de los inocentes, quienes siempre resultan asumiendo el peso de la guerra que no libran. Naturalizamos la miseria de los pueblos que desde el suelo asiático suplican por verdaderos votos de fraternidad a nivel internacional que asuman como suya la tragedia, que allí es pan de cada día. Y ni hablar de la indolencia frente a nuestros propios muertos, seguramente es más fácil encontrar]*[wallpapers]*[ de luto para Paris que motivos para poner en cuestión la guerra que nos estalla los oídos a fuerza de fusil.]

En últimas, el “humanismo mocho” se olvida de ser humanismo cuando –por mocho– se hace selectivo.

*[Twitter:]*[ ]
