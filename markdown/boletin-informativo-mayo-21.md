Title: Boletín informativo Mayo 21
Date: 2015-05-21 17:30
Author: CtgAdm
Category: datos
Tags: Actualidad informativa, ANAFRO, Anglo Gold Ashanti, contagio radio, Día de la Afrocolombianidad, mujeres, Negociaciones Habana, Noticias del día, Sucre Cauca, Unidad Nacional de Victimas, Vida y Territorio"
Slug: boletin-informativo-mayo-21
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4529714_2_1.html?data=lZqfm5yVeI6ZmKiakpyJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfk5aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Inicia el **ciclo 37 de negociaciones entre el Gobierno y guerrilla de las FARC** en La Habana, con cambios en el equipo que lidera Humberto de la Calle; **la incorporación de la canciller Maria Ángela Holguín y el empresario Gonzalo Restrep**o. Habla el analista y columnista **Luis Eduardo Celis**.

-**En un 80% el municipio de Sucre** en el departamento del Cauca, **tiene solicitudes de títulos mineros por parte de compañías** entre las que se encuentra **Anglo Gold Ashanti** para la extraccion de **oro y material de rio** entre otros, situación que podría afectar negativamente las principales fuentes hídricas de la población. Habla **Carlos Chilito del Colectivo "Bienandantes"** de Sucre Cauca.

-Representaciones de **6 comunidades de Bolívar, Sucre y Santander,** en las que se han incumplido los acuerdos **se tomaron ayer la sede en Bogotá de la Unidad Nacional de Víctimas**, con el objetivo de entregar un **pliego de peticiones y negociar el cumplimiento integro** de lo que por derecho les corresponde. Habla **Fidel Serpa** en representación de los manifestantes.

-Los **acueductos comunitarios de Sucre**, Cauca, hacen una apuesta por **continuar funcionando sin faltar a la ley 142**, regulada por la **Superintendencia de Servicios Públicos**, que está orientada a la **empresarización en la prestación de este tipo de servicios**, legislación que incluye artículos que dificilmente este tipo de acueductos pueden cumplir. Habla **Gener Hoyos de la Asociación campesina "Bienandantes"**.

-En el marco del **Día Nacional de la Afrocolombianidad,** que se celebra en todo el país hoy 21 de Mayo, **disitintas expresiones culturales, lúdicas, y académicas** se pueden ver en varias regiones del pais donde hay presencia de la etnia como parte de los Retos para el **decenio 2015-2025**. Habla **Jimmy Viera,** miembro de la **Autoridad Nacional Afro ANAFRO**.

-**Leidy Correa** representante del **Colectivo de mujeres "Mujeres, Vida y Territorio" **nos cuenta que el colectivo surge de la **"Escuela política, cultura y paz de Sucre"** en 2012, como expresión de las distintas colectividades de mujeres del municipio. El grupo tiene como objetivo luchar por la defensa y el respeto de los DDHH de las mujeres en Sucre, brindando asistencia, acompañamiento y visibilización de las distintas problemáticas que las afectan.
