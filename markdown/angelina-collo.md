Title: Mujer indígena Angelina Collo fue asesinada en Cauca
Date: 2020-10-17 15:18
Author: AdminContagio
Category: Actualidad, Nacional
Slug: angelina-collo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Angelina-Collo-Paez-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Angelina Collo Tumbo, mujer indígena de 65 años **fue asesinada **en zona rural de Páez, Cauca.** L**a víctima,** fue abordada en su vivienda y atacada con arma blanca en repetidas ocasiones. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Steven Collo, gobernador del Resguardo de Belalcázar, afirmo que**la principal hipótesis alrededor del hecho gira entorno a disputas por tierras,** por lo que avanzan en el proceso investigativo para capturar cuanto antes al responsable, de quien ya tienen indicios. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ahora las comunidades indígenas miembros de la Minga del Suroccidente pasan por Ibagué y esperan llegar a Bogotá en los próximos días con el fin de exigir al gobierno garantías de seguridad para sus territorios. [Lea también: Violencia se vive en la mayoría de los departamentos del país](https://archivo.contagioradio.com/violencia-contra-lideres-sociales-se-concentro-en-29-de-los-32-departamentos/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Sigue la violencia contra las mujeres y las indígenas en Cauca

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el último año el departamento del Cauca y en concreto las comunidades indígenas han sido las principales víctimas de la violencia. En días pasados también se presentó el asesinato de [Erlin Undagama](https://archivo.contagioradio.com/asesinan-a-erlin-undagama-docente-embera-del-choco/) a manos de grupos armados que no han sido controlados a pesar de la fuerte militarización del departamento

<!-- /wp:paragraph -->
