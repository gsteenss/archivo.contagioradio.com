Title: Muestra audiovisual "La imagen de la memoria" se toma Medellín
Date: 2015-05-10 10:40
Author: CtgAdm
Category: 24 Cuadros
Tags: Alejandro Cock Peláez, Asociación Campesina de Antioquia, Cortometraje Leidy, Festival de Cine de Cannes, La imagen de la memoria, Mark Griego, Marmato, Medellin, Memoria y Territorio, muestra internacional de cine en Medellín, Palma de Oro, retrospectiva, Santiago Herrera, Simón Mesa
Slug: muestra-audiovisual-la-imagen-de-la-memoria-se-toma-medellin
Status: published

Del 20 al 24 de Mayo, **Medellín** vivirá la 5ta muestra audiovisual "**La imagen de la memoria**", un espacio colaborativo y de participación para construir y difundir miradas propias sobre lo que vivimos o soñamos, con imágenes y sonidos que cuenten nuestras historias y memorias.

Los antioqueños y antioqueñas podrán disfrutar de manera gratuita con las más de **60 proyecciones** que se tomarán diferentes espacios de la ciudad tales como Teatros, Casas de la cultura, Cine Clubes, Corporaciones y Colectivos que abren sus puerta a las producciones provenientes de Colombia y varias partes del mundo, en diversos formatos audiovisuales.

<iframe src="https://www.youtube.com/embed/59GPM_sJ5r0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

De la muestra central hacen parte, entre otras, "**Leidy**", cortometraje del director **Simón Mesa**, ganador de la **Palma de Oro** en el **Festival de Cine de Cannes** en 2014; "**Marmato**" largometraje documental dirigido por **Mark Griego**, que retrata la situación de un pueblo de minería aurífera artesanal en Colombia, y la serie documental "**Memoria y Territorio** " de la Escuela de Creación Documental de la **Asociación Campesina de Antioquia**. (Ver muestra central http://bit.ly/1zKs8ft)

Dentro de la **selección nacional** se combinan las miradas a realidades colombianas en relación con temáticas como las problemáticas y movilizaciones campesinas y raízales, las tradiciones y manifestaciones culturales de las regiones, género y equidad, las diferentes violencias en el entorno urbano, los Derechos humanos, ambiente y conservación entre otros, abordados en más de 20 producciones realizadas en diferentes puntos de la geografía nacional. (Ver programación completa http://bit.ly/1JwcJQb )

En cuanto a la **muestra internacional**, países como Perú, Ecuador, España, Argentina, Portugal, Francia, Brasil, Estados Unidos, y Nicaragua entre otros, aportan producciones donde las guerras populares y las víctimas de los conflictos, las luchas de movimientos y comunidades por sus derechos durante periodos de represión, las explotaciones ambientales, expresiones artísticas como agentes de cambio, se abordan desde la visión particular de los protagonistas y sus experiencias de vida y la manera en que entran a ser parte de la historia de todos. (Ver programación completa http://bit.ly/1Fa87Bt)

La programación que incluye **conversatorios** entre realizadores y el público asistente, contará además con una **retrospectiva** del trabajo de de **Santiago Herrera**, Documentalista, Magister en Artes visuales, Profesor universitario y realizador independiente y un homenaje póstumo al documentalista, maestro y realizador local **Alejandro Cock Peláez**, quién falleció en Medellín en la noche del jueves 30 de abril de 2015.
