Title: Protección animal es incluida en el PND 2014 - 2018
Date: 2015-04-30 13:37
Author: CtgAdm
Category: Animales, Nacional
Tags: Animales, Guillermo García Realpe, Juan Carlos Losada, Mineria, Partido Liberal, Plan Nacional de Desarrollo, protección animal
Slug: proteccion-animal-es-incluida-en-el-pnd-2014-2018
Status: published

##### Foto: [Contagio Radio]

Por iniciativa del senador del Partido Liberal, Guillermo García Realpe, por primera vez la protección animal se incluirá en el Plan Nacional de Desarrollo “Todos por un nuevo país 2014 - 2018”.

La Plenaria de la Cámara aprobó el nuevo artículo número 252, que busca promover una política pública en defensa de los animales, que debe ser responsabilidad de las entidades territoriales, quienes deberán vigilar, controlar y fomentar el **respeto por los animales y con ellos su cuidado e integridad.**

*El Gobierno Nacional promoverá políticas públicas y acciones gubernamentales en las cuales se fomenten, promulguen y difundan los derechos de los animales y/o la protección animal. Para tal efecto, en coordinación con las organizaciones sociales de defensa de los animales, diseñará una política en la cual se establecerán los conceptos, competencias institucionales, condiciones, aspectos, limitaciones y especificaciones sobre el cuidado animal en cuanto a la reproducción, tenencia, adopción, producción, distribución y comercialización de animales domésticos no aptos para reproducirse. Las entidades territoriales y descentralizadas del Estado se encargarán de vigilar, controlar y fomentar el respeto por los animales y su integridad física y anímica. Adicionalmente, las organizaciones sociales de defensa de los animales participarán de manera coordinada con las entidades nacionales y territoriales para la difusión de las políticas a que se refiere el presente artículo.*

El Plan Nacional de Desarrollo abre cada vez más el camino a la minería lo que ocasionaría graves impactos a los hábitats de los animales, sin embargo, según el representante a la cámara Juan Carlos Losada, **se trata de un avance en la lucha animalista,** teniendo en cuenta que la iniciativa del senador Realpe, incluiría tanto a animales domésticos como silvestres, **“se tuvo una derrota en el tema ambiental, pero se obtiene una victoria teniendo en cuenta que por primera vez el tema de protección animal es incluido en el Plan Nacional de Desarrollo**, lo que le da un componente político para darle voz a los que no tienen voz”.

 Los entes regionales, municipios, gobernaciones y alcaldías deberán formular políticas de bienestar para los animales, lo que da herramientas para exigir al gobierno nacional que la política pública se genere y se cumpla.
