Title: Este fin de semana se realizará el Encuentro de la Mesa Ecuménica por la paz
Date: 2016-11-11 16:47
Category: Nacional, Paz
Tags: Mesa Ecuménica por la paz, Movimiento social
Slug: programese-con-el-encuentro-de-la-mesa-ecumenica-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Mesa-Ecuménica-e1476131458378.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [11 Nov 2016] 

**Durante este fin de semana se llevará a cabo en la Universidad Industrial de Santander el Encuentro de la Mesa Ecuménica por la paz,** una plataforma que reúnen diferentes expresiones religiosas, con el fin de fortalecer la perspectiva de fe en torno a un compromiso social y político y a la construcción de la propuesta de veedurías éticas ciudadanas por la paz sobre los acuerdos de La Habana

Esta propuesta iría en caminada a generar la veeduría sobre la participación social y el sobre el punto de víctimas, para poder **visibilizar las víctima del sector cristiano que por sus creencias o actividades al interior de las comunidades fueron desaparecidos**, torturados, exiliados o vulnerados sus derechos.

Frente a la participación que han tenido algunas iglesias que pertenecen a los sectores del No, Sol Ángela Hoyos, miembro de la Mesa Ecuménica por la paz, afirma que “los acuerdos deben impulsarse como están pactados desde un inicio en La Habana y **si se modifican debe ser en trabajo con las comunidades y el movimiento social** y no bajo los intereses de los poseedores de tierras o el monopolio de los medios de comunicación”.

La metodología del encuentro girará en torno a cuatro mesas: “Memoria y Víctimas”, “Veedurías éticas ciudadanas por la paz”, “Ecumenismo paz y relacionamiento social” y “Familiares de víctimas cristianas”. Se espera que participen aproximadamente 200 personas.

La Mesa Ecuménica  realiza este encuentro anualmente con la finalidad de **constituir este espacio como un actor social que pueda incidir en los procesos de paz** al interior del país. Al Encuentro asistirán personas de diferentes regiones del país. Le puede interesar:["Mesa Ecuménica lanza propuesta en respaldo a los acuerdos de paz"](https://archivo.contagioradio.com/mesa-ecumenica-lanza-propuesta-en-respaldo-a-los-acuerdos-de-paz/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
