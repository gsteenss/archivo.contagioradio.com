Title: Desacato del Gobierno a la Corte podría acarrear arresto
Date: 2020-09-24 14:45
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Carlos Holmes Trujillo, Corte Suprema de Justicia, Descato del Gobierno, Iván Duque, Protesta social
Slug: desacato-del-gobierno-a-la-corte-podria-acarrear-arresto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Desacato-del-Gobierno-a-la-Corte-Suprema.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Este jueves, el Ministro de Defensa, Carlos Holmes Trujillo se negó a ofrecer disculpas públicas al país por los abusos y la violencia ejercidos por la Fuerza Pública en el marco de las manifestaciones sociales,** argumentando que ya lo había hecho el pasado 11 de septiembre. Cabe recordar que la Corte Suprema de Justicia había ordenado expresamente al Ministro emitir un comunicado excusándose; **por lo que varios sectores han considerado esta actuación como un desacato del Gobierno al mandato de la Corte.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/mindefensa\/status\/1309230469972721665","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/mindefensa/status/1309230469972721665

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Contagio Radio, consultó al abogado y defensor de DD.HH., Alirio Uribe para indagar sobre las posibles consecuencias de que el Gobierno pueda ser declarado en desacato y señaló que en ese caso **podría proceder una orden de arresto en contra del Ministro, por incumplir la orden judicial impartida. Según el [articulo 52](http://www.secretariasenado.gov.co/senado/basedoc/decreto_2591_1991_pr001.html#52) del Decreto 2591 de 1991 dicho arresto, podría ser de hasta de seis meses y también sería aplicable una multa hasta de 20 salarios mínimos mensuales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para el abogado, Alirio Uribe, **esta actitud del Gobierno es una práctica recurrente en el sentido de cuestionar y desacatar las órdenes judiciales.** Señaló que lo ha hecho ya, con la medida de aseguramiento en contra del expresidente Álvaro Uribe, contra los fallos que prohíben la aspersión aérea con glifosato y la erradicación de cultivos de uso ilícito de manera violenta, y ahora nuevamente con la decisión de la Corte que ampara la protesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Según el jurista Alirio Uribe, ninguno de los argumentos que ha invocado el Gobierno para no acatar la orden es válido.** Por una parte, el Gobierno ha señalado que esperará que la Corte Constitucional entre a revisar y a pronunciarse sobre el fallo, sugiriendo que no acepta su contenido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, según el abogado Alirio Uribe, las partes en la tutela no están facultadas legalmente para solicitar la revisión de la Corte Constitucional como recurso y esta decisión solo le compete a ese Alto Tribunal. Agregó que **aun cuando la Corte decidiera revisar eventualmente el fallo, esto no exime al Gobierno de acatarlo, mientras esta se pronuncia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, en una declaración ante un medio de comunicación, el Presidente Duque, señaló que el fallo tenía que ser revisado, pues según él, la tutela tenía un principio de subsidiaridad que no fue atendido por los tutelantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En contraste, Alirio Uribe, quien actuó como accionante en la tutela, señaló que esta era el medio idóneo ante la violación de un derecho fundamental como lo es la protesta el cual ha sido «*atacado y destruido*» por parte del Gobierno a través de «*acciones ilegales desplegadas por la Fuerza Pública»* y que por ello no requería el agotamiento de ningún otro recurso jurídico.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desacato del Gobierno es un “pésimo precedente”

<!-- /wp:heading -->

<!-- wp:paragraph -->

Camilo Gónzalez Posso, director del Instituto para el Desarrollo y la Paz -[Indepaz](http://www.indepaz.org.co/mindefensa-o-acata-la-orden-de-la-corte-o-debe-renunciar/)-; fue enfatico en señalar que el ministro de Defensa estaba en claro desacato ante la orden emitida por la Corte Suprema y que eso era un *«pésimo precedente pues es un mensaje que desintitucionaliza»*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> «El desacato \[del Gobierno\] es un mensaje a la ciudadanía de que las instituciones y la ley son para los de ruana*»*
>
> <cite>Camilo Gónzalez Posso, director de Indepaz.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Adicionalmente, señaló que esa posición del Gobierno no tenía un sustento sólido, ni política, ni jurídicamente hablando, exponiendo muchos de los argumentos que expresó el abogado Alirio Uribe, y agregó que **esa postura generaba una presión y un choque de poderes en contra de la Corte Suprema de Justicia.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https:\/\/www.youtube.com\/watch?v=HAHCFLNhHFk","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=HAHCFLNhHFk

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->
