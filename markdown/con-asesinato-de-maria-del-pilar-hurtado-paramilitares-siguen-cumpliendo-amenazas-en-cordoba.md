Title: Con asesinato de María del Pilar Hurtado, paramilitares siguen cumpliendo amenazas en Córdoba
Date: 2019-06-21 11:24
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, cordoba, Panfletos amenazantes, Tierralta
Slug: con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/María-del-Pilar.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Panfleto.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Archivo particular] 

La Fundación Social Cordoberxia denunció que este 21 de junio en **Tierralta, Córdoba** fue asesinada María Del Pilar Hurtado Montaño de 34 años, quien fue una de las personas  declaradas como objetivo militar en un panfleto amenazante difundido por las **Autodefensas Gaitanistas de Colombia  (AGC)** a inicios del mes de junio

Los hechos sucedieron en tempranas horas de la mañana cuando María del Pilar se disponía a iniciar su trabajo como recicladora cuando fue atacada en la calle 2 del sector La Esperanza del barrio 9 de Agosto. La mujer madre de cuatro hijos a pesar de no ser incluida con su nombre en el panfleto, aparecía bajo el peyorativo término de  "la gorda hpta mujer del Chatarrero".  [(Le puede interesar: No se quedó en amenazas panfleto contra ocupantes de predios en Tierralta, Córdoba)](https://archivo.contagioradio.com/panfleto-contra-ocupantes-predios-tierralta-cordoba/)

\[caption id="attachment\_69446" align="aligncenter" width="547"\]![Panfleto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Panfleto-547x1024.jpeg){.wp-image-69446 .size-large width="547" height="1024"} Foto: Cordoberxia\[/caption\]

### Los predios que vinculan a la Alcaldía de Tierralta

Cordoberxia denunció a inicios de junio la aparición de este panfleto, en el que se amenazaba con la muerte a ocupantes de tierra por vías de hechos, así como a líderes sociales y políticos en Tierralta Córdoba. Una de las amenazas se cumplió el pasado 3 de junio contra uno de los ocupantes de tierras, que resultó muerto tras recibir disparos por parte de hombres armados que lo atacaron mientras que otro resultó herido.

Sin los recursos para adquirir una propiedad, muchas familias han tenido que recurrir a la invasión para establecer sus lugares de vivienda. Tal como explicó Andrés Chica, integrante de Cordoberxia, las personas fueron amenazadas por haber ocupado **uno de los predios, propiedad del papá de Fabio Otero, alcalde de Tierralta.**

### Alcaldía no reconoce que María del Pilar había sido amenazada

A través de un comunicado la Alcaldía asegura que María del Pilar no está vinculada a los nombres que aparecen en el panfleto,  desvirtuando las denuncias hechas por Cordoberxia, asegurando que su esposo afirmó que ella no era la mujer que señalaban en el panfleto. Desde la fundación que su compañero sentimental "estaba y está amedrentando, presionado por los autores intelectuales y materiales del crímen".

**La Defensoría del Pueblo** se manifestó al respecto indicando, que a pesar de ser mencionados en el comunicado,  a través de su defensor comunitario, tales afirmaciones provenientes de la Alcaldía no corresponden a la realidad.

> Esta información de la Alcaldía de Tierralta donde se desvirtúa la condición de líder social de María del Pilar Hurtado y menciona a [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) como Institución que respalda esa afirmación, no corresponde a la realidad pues no estuvimos presentes en la declaración. [pic.twitter.com/OlDlELSNhF](https://t.co/OlDlELSNhF)
>
> — Defensoría delPueblo (@DefensoriaCol) [June 22, 2019](https://twitter.com/DefensoriaCol/status/1142492765592457217?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
La fundación reiteró su llamado al Gobierno para que Tierralta sea denominada "Zona Estratégica de Intervención integral (ZEII), como parte parte del **Plan de Acción Oportuna  (PAO)** de prevención y protección para defensores de derechos humanos, líderes sociales, comunales y periodistas y detener la escalada de violencia, amenazas, y desplazamientos forzados en este municipio.[(Lea también:Amenazan a Paola Jaraba, defensora de derechos humanos en Tierralta, Córdoba)](https://archivo.contagioradio.com/amenazan-paola-jaraba-defensora-derechos-humanos-tierralta/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
