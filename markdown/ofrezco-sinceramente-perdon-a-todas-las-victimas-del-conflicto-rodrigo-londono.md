Title: "Ofrezco sinceramente perdón a todas las víctimas del conflicto": Rodrigo Londoño
Date: 2016-09-27 14:55
Category: Nacional, Paz
Tags: acuerdo de paz, Conversaciones de paz con las FARC, delegación FARC, Diálogos de La Habana
Slug: ofrezco-sinceramente-perdon-a-todas-las-victimas-del-conflicto-rodrigo-londono
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/CtXG0IdUsAQR_MU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Delegación de Paz FARC-EP ] 

###### [27 Sept 2016] 

"En nombre de las FARC-EP, **ofrezco sinceramente perdón a todas las víctimas del conflicto**, por todo el dolor que hayamos podido ocasionar en esta guerra", fue tal vez la frase más emotiva que los 2500 asistentes a la firma del acuerdo de paz, pudieron oír de parte de Rodrigo Londoño Echeverry, quien durante media hora se dirigió a un auditorio que con alegría y esperanza asistía al cierre de un capítulo trágico de la historia nacional.

El máximo comandante de las FARC-EP dirigió sus palabras a la sociedad colombiana, el "pueblo bondadoso que siempre soñó con este día (...) que **nunca abandonó la esperanza de poder construir la patria del futuro**, donde las nuevas generaciones (...) puedan vivir en paz, democracia y dignidad". A quienes habitan los cinturones de miseria de la ciudad de Cartagena, Londoño extendió su mano y abrazo desde el corazón, y les aseguró que ellos junto al pueblo colombiano "serán artífices de la siembra de la paz que apenas empieza".

Durante su intervención hizo un reconocimiento a los fundadores de las FARC-EP, Manuel Marulanda Vélez y Jacobo Arenas, e hizo mención de la perseverancia que caracterizó al comandante Alfonso Cano, para luego reafirmar que en la reciente Conferencia Nacional Guerrillera **todo el movimiento aprobó los acuerdos de La Habana y la conformación de un nuevo partido político** como "paso definitivo de la forma de lucha clandestina y alzamiento armado, a la forma de lucha abierta, legal, hacia la expansión de la democracia".

"Que nadie dude que vamos hacia la política sin armas. Preparémonos todos para desarmar las mentes y los corazones", aseguró el máximo comandante en su discurso, ante los ojos de los miles de colombianos que lo veían en distintas plazas del país, a quienes les insistió en que debían jugar un papel de garantes, teniendo en cuenta que **la clave de los acuerdos es justamente la implementación**, en la que se debe garantizar que lo que quedó escrito se materialice y cobre vida en la realidad. "Nosotros vamos a cumplir, y esperamos que el gobierno cumpla", agregó.

Londoño insistió en la satisfacción del equipo negociador al saber que el proceso de paz en Colombia "es ya **un referente para la solución de conflictos en el mundo**" y llamó la atención de los gobiernos israelí y palestino para que encuentren una solución política al [[conflicto que se vive día a día](https://archivo.contagioradio.com/gaza-se-ha-convertido-en-una-carcel-a-orillas-del-mar-mediterraneo/)] en los territorios ocupados, en este sentido, se refirió a Siria y a su anhelo de que "se silencien las bombas y el horror de una guerra que victimiza a un pueblo, que lo destierra y lo obliga a lanzarse al mar en barcazas inseguras para buscar refugio en países que también los rechazan, y los reprimen".

A quienes también hizo referencia el máximo comandante fue a los niños y las niñas que mueren de hambre, desnutrición o enfermedades incurables en territorios como [[La Guajira](https://archivo.contagioradio.com/comunidades-de-la-guajira-siembran-esperanza-y-construyen-paz/)], dónde las comunidades también esperan que la implementación de los acuerdos de paz incluya la **disminución del presupuesto público destinado a la guerra para que haya aumento en la inversión social**, en regiones que reclaman [[mayor atención estatal como el Chocó](https://archivo.contagioradio.com/levantaremos-el-paro-cuando-hayan-compromisos-serios-pueblo-chocoano/)].

Con este acuerdo pactado se aspira a poner punto final a la larga historia de luchas y enfrentamientos continuos que han desangrado a Colombia, afirmó Londoño, quien agrego que "sólo un pueblo que ha vivido entre el espanto y los padecimientos de una y otra guerra, durante tantas décadas, podía **tejer pacientemente los sueños de paz y justicia social**, sin perder nunca la esperanza de verlas coronadas por sendas distintas a la confrontación armada, mediante la reconciliación y el perdón".

En su discurso el máximo comandante de las FARC-EP insistió en que parte fundamental de la consolidación de la paz depende de que cesen "la persecución, la represión, la muerte y el [[accionar paramilitar](https://archivo.contagioradio.com/ante-la-cidh-se-exige-al-gobierno-enfrentar-fenomeno-paramilitar/)], que aún persisten, así como múltiples causas del conflicto", porque "los pueblos, víctimas iniciales y finales de todas las violencias, **son a la vez los primeros en soñar y desear la paz y la convivencia** arrebatadas".

<iframe id="audio_13082894" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13082894_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
