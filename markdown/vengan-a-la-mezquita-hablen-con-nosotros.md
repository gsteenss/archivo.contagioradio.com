Title: “Vengan a la mezquita, hablen con nosotros”
Date: 2017-03-27 09:00
Category: El mundo, Otra Mirada
Tags: Islam, Mezquitas, Musulmanes, Observatorio Vasco
Slug: vengan-a-la-mezquita-hablen-con-nosotros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mezquitas-país-vasco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:El Periódico 

*Actualmente en el País Vasco existen 58 mezquitas, según el Observatorio del Pluralismo Religioso del Ministerio de Justicia*

#### **[Por Mónica Lozano]  
** 

Los pies desnudos pisan una alfombra roja hasta llegar al fondo del templo, suben por las escaleras, y pasan al baño. Son cinco las veces las que deben lavarse. Primero las manos hasta los codos, luego la cara. El cabello se echa hacia adelante y se bañan la parte de atrás de la cabeza. Luego se pasan el agua sobre el cabello, y finalmente se refrescan los pies. Este es el ritual sagrado que cada hombre o mujer musulmana debe hacer para poder orar en cualquier mezquita, como lo explica casi pedagógicamente, Hanan El Yousfi  vicepresidenta de la comunidad islámica de Rekalde, donde se ha inaugurado la quinta mezquita que existe en Bilbao.

“Vivo en Amezola. Iba a muchas mezquitas, una en Bilbao y otras en Barakaldo y Portugalete. Esta nos viene muy bien, estamos muy contentos, además hay un espacio muy grande. Es un trabajo muy significativo para nosotros”, expresa Hanan El Yousfi de 31 años. Ella llegó a España hace 10. Estudió en Granada Traducción e Interpretación, y luego se trasladó hace tres años a Bilbao. Casada con otro marroquí, tienen dos niños, a los que les hablan en todos los idiomas que han aprendido, incluido el euskera.

El Yousfi  antes debía desplazarse más de 4 kilómetros para ir a la mezquita, el tercer lugar que más frecuenta un musulmán después de la casa y el sitio de trabajo. Su esposo, Abdel Aziz, presidente de la comunidad islámica de Rekalde lo tenía más difícil. Obligatoriamente los hombres deben asistir cinco veces al día. “Debemos rezar al alba, al mediodía, por la tarde, tras la puesta de sol y en la noche. Algunos solían desplazarse en metro a las mezquitas del centro”, dice Aziz, quien hoy  celebra que se hayan finalizado todos los trámites, y ahora la comunidad musulmana de Rekalde y los alrededores pueda tener su lugar sagrado más cerca de casa.

Esa apertura de la mezquita, no solo sirve para que los musulmanes de la zona tengan un lugar más cerca para practicar su religión. También se trata de un espacio que busca generar un diálogo con la comunidad que no conoce el Islam. “La mezquita está muy bien para hacer nuestras actividades, pero también para abrirnos y darnos a conocer a los demás. Es un centro abierto a todos,  por eso en el futuro próximo se organizarán unas jornadas abiertas a todos los vecinos” indica Hanan El Yousfi.

El número de musulmanes que viven en España superó en 2016 los 1,9 millones, 31.235 más que en 2015. Según datos del Observatorio del Pluralismo Religioso del Ministerio de Justicia, la comunidad musulmana del País Vasco, está conformada por más de 50.000 personas. En su mayoría son de Marruecos, Argelia y Pakistán. Con la nueva mezquita inaugurada en Rekalde, son 58 las que se encuentran en Euskadi. Hay 14 mezquitas en Álava, 26 en Bizkaia y 18 en Gipuzkoa. Cuando en 2012 apenas se contaban  32 en total.

Hanan considera que la libertad de culto religioso en España y especialmente en Euskadi se  respeta gracias a la Ley, sin embargo, no siempre ha sido así por parte de la sociedad vasca. Más de una vez la islamofobia se ha tomado a Euskadi.

En 2011, [vecinos de Basurto salieron a las calles de su barrio a protestar contra la apertura de la mezquita de Pablo Alzola](http://www.elcorreo.com/vizcaya/v/20110519/vizcaya/vecinos-basurto-echan-otra-20110519.html) y un centro socio cultural musulmán. “¡Mezkita no! Queramos que se nos escuche”, gritaba la comunidad. Esa presión social logró su cometido y no fue posible la inauguración del templo. Hace un año en Vitoria nuevamente se evidenció la islamofobia. En el barrio de Zabalgana, apareció con manchas de sangre de cerdo en el exterior y con dos cabezas del mismo animal, la entrada del que iba a ser un nuevo centro de culto para los musulmanes. Ese tipo de actos, están prohibidos por el Corán.

\[caption id="attachment\_37911" align="alignnone" width="800"\]![mezquitas país vasco 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mezquitas-país-vasco-4.jpg){.wp-image-37911 .size-full width="800" height="532"} Foto: my heritage\[/caption\]

### **¿Por qué la islamofobia?** 

De acuerdo con Gorka Moreno, director del Observatorio Vasco de Inmigración Ikuspegi, hay un menor grado de simpatía hacia quién es más diferente en términos culturales, religiosos, socioeconómicos y educativos. “A nivel europeo sí que se detecta una ola de islamofobia”. No obstante, admite que aunque se han detectado casos, se puede decir que no es un patrón general de la sociedad vasca. Eso sí, reconoce que desde el concepto de terrorismo se ha construido una idea errónea del Islam. “A la inmensa mayoría de la población cuando se le habla de Islam lo primero que le viene a la mente es terrorismo. Lógicamente ese binomio es diabólico porque sabemos a ciencia cierta que la inmensa mayoría de la población musulmana es totalmente contraria a la violencia y al terrorismo. De hecho, quienes más perjudicados salen son ellos, quienes muchas veces acaban tildados como terroristas”, explica Moreno.

Frente a esa situación los medios de comunicación tienen gran parte de la responsabilidad porque han hecho ver la inmigración como un problema. Así lo ve Adil El Guarrah, presidente de la mezquita Attawhid, de corriente salafista, catalogada como una de las líneas del islam más radicales, de las cuales en Euskadi, solo hay dos: una en Vitoria y otra en Barakaldo. “El salafista es el que más obedece a la autoridad. Pero, hay ciertas personas que trabajan en los medios que tienen algo en contra de nosotros. No tenemos nada que esconder. Somos más claros que el agua, pero somos atacados por lo que sale en el periódico. Lo que sale no se contrasta con lo que se vive en la realidad” y adicionalmente explica, “Estamos en contra de Bin Laden y el Daesh. Son los perros del infierno porque interpretan de su cerebro. Mohamed dijo que no hay que matar. Hemos venido a buscar trabajo y la vida. Esta gente nos ha dado trabajo ¿Por qué tenemos que hacerles daño?”.

![mezquitas país vasco 5](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mezquitas-país-vasco-5.jpg){.alignnone .size-full .wp-image-37912 width="800" height="532"}

### **Los retos** 

En torno al islam hay muchos estereotipos a los que hay que hacerle frente. “Tratarlos con normalidad”, es el principal reto que visibiliza Gorka Urrutia, director del Instituto de Derechos Humanos de la Universidad de Deusto. Para él los musulmanes deben ser tratados con la misma naturalidad que hay hacia los católicos. Una tarea que se resuelve desde dos frentes: el mediático y el comunitario.

Urrutia asegura que lo que debe haber es un tratamiento informativo más adecuado y recuerda que los casos de fraude de RGI (Renta de Garantía de Ingresos), y otros temas polémicos que se están sacando relacionados con el islam, provocan una percepción negativa de la población frente a la comunidad musulmana.

Por su parte, el director del Observatorio Vasco de Inmigración Ikuspegi, señala que se está trabajando en muchos municipios con la estrategia antirumores en lugares como Bilbao, Vitoria y Getxo. “Esto supone que desde el ámbito social se pongan en entredicho algunos de esos prejuicios y se trabaje por la convivencia y la integración”.

Para la vicepresidenta de la comunidad islámica de Rekalde, es simple, “Les digo que vengan a conocernos de cerca. Que no escuchen a los medios de comunicación porque muchas veces informan muy mal. Vengan acá a la mezquita, hablen con nosotros y pregúntenos”. Asimismo, manifiesta que la comunidad musulmana también tiene que trabajar para acercarse más a los demás y no caer en el victimismo.

El acto de inauguración de la mezquita de Rekalde terminó con mensaje por parte de Aziz Messaoudi, presidente de la Federación Islámica del País Vasco, “el mensaje es que trabajemos por humanizar la sociedad… estas  cosas no nos van a vencer. Ni los terroristas, ni los fanatistas, ni nadie. Entre toda la sociedad podemos enfrentar este tipo de situaciones”.
