Title: Salvatore Mancuso reafirma su compromiso con las víctimas
Date: 2020-08-13 11:20
Author: CtgAdm
Category: Actualidad, Paz
Tags: MOVICE, Paramilitarismo, Salvatore Mancuso, verdad y reparación, víctimas
Slug: salvatore-mancuso-reafirma-su-compromiso-con-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Audio-2020-08-13-at-10.56.00-AM-online-audio-converter.com_.mp3" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Por medio de un audio conocido este 13 de agosto el líder paramilitar Salavatore Mancuso, afirmó que las [víctimas](https://archivo.contagioradio.com/jep-nego-en-segunda-instancia-tutela-presentada-por-el-coronel-r-plazas-acevedo/)pueden estar seguras del aporte que hará para la reconstrucción de la verdad en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde la cárcel de Irwin, en la que esta recluido el exjefe paramilitar de las Autodefensas Unidas de Colombia, envió un audio en el que **expresa a las autoridades judiciales de Colombia y a las víctimas, que reafirma su compromiso con la verdad , la justicia y las garantías de no repetición** en relación con el *"conflicto colombiano en el cual tuve participación".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***"A las víctimas el conflicto colombiano tengan por seguro que jamas las voy a defraudar"***, fue la expresión dicha por Mancuso; además presentó una solicitud de perdón y resaltó que esta comprometido con las garantías de no repetición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta acción se da luego de que víctimas del caso 004 dirigieran varias cartas a la presidencia de la Jurisdicción Especial para la Paz, hacia algunos de los magistrados de las diferentes salas, desde hace 15 días sin recibir respuesta alguna.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En parte de las comunicaciones conocidas por Contagio Radio las víctimas expresan su objeción ante la decisión de rechazo tomada por la sala de reconocimiento.

<!-- /wp:paragraph -->

<!-- wp:audio {"id":88211} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Audio-2020-08-13-at-10.56.00-AM-online-audio-converter.com_.mp3">
</audio>
  

<figcaption>
Escuche el audio completo de Salvatore Mancuso

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
