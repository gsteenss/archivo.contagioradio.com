Title: Debate en El ecléctico: "Elección del nuevo Fiscal general de la nación"
Date: 2016-07-26 11:30
Category: El Eclectico
Tags: Elección fiscal, Fiscalía General de la Nación, Nestor Humberto Martínez
Slug: debate-en-el-eclectico-eleccion-del-nuevo-fiscal-general-de-la-nacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/nestor-humberto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: politika.com 

##### [18 Jul 2016] 

En este debate nos acompañaron Sthefany Zamora, abogada e integrante del grupo de derechos humanos de Juventud Rebelde, Nicolás Sánchez, estudiante de derecho y columnista de El Ecléctico, Julián Daza, estudiante de Gobierno y Relaciones Internacionales e investigador en temas de gobernanza y políticas públicas, Alejandro Matta, abogado y profesor universitario y miembro de la Unidad de Trabajo Legislativa del representante a la cámara, por el Polo Democrático, Víctor Correa, y Francisco Bernate, abogado penalista y doctor en derecho, profesor de la Universidad del Rosario y abogado litigante.

Respecto de los posibles y numerosos impedimentos que el nuevo Fiscal General de la Nación, Néstor Humberto Martínez, podría tener debido a sus cercanas relaciones con el poder, algunos panelistas coincidieron en afirmar que efectivamente existen y son una limitante para un adecuado trabajo de investigación de la Fiscalía. Otros, como el abogado Bernate, afirmaron que no consideran que estas relaciones constituyan impedimentos para su trabajo a la cabeza del ente acusador.

Ademas, los panelistas discutieron acerca del proceso por el cual se escogió al nuevo fiscal, el concurso público de méritos y la conformación de la terna, proceso que muchos estuvieron de acuerdo en decir que se trató de una pantomima dado que ya todo el país sabía quién sería el elegido para el cargo. Adicionalmente, también hablamos sobre la declaración de Néstor Humberto Martínez frente a la Corte Suprema de Justicia de revisar la criminalidad del delito de violencia intrafamiliar.

Les invitamos a escuchar el debate completo a continuación y a sacar sus propias conclusiones, recuerden seguirnos en [@[Contagioradio1]{.u-linkComplex-target}](https://twitter.com/Contagioradio1){.ProfileHeaderCard-screennameLink .u-linkComplex .js-nav} y [@[Eclectico\_co]{.u-linkComplex-target}](https://twitter.com/Eclectico_co){.ProfileHeaderCard-screennameLink .u-linkComplex .js-nav}

<iframe src="http://co.ivoox.com/es/player_ej_12342805_2_1.html?data=kpeglpecdJahhpywj5WVaZS1kZ6SlaaVeI6ZmKialJKJe6ShkZKSmaiRdI6ZmKiah5eWic3ZxMjWh6iXaaOnz5Db18ras4zaytjQw9GJdpOhjKnSxMbYqYzZz5DSzpDJp83ZxNnWxdSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
