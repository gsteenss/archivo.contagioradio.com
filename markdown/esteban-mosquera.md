Title: Esteban Mosquera, víctima del ESMAD en Cauca en el marco del paro estudiantil
Date: 2018-12-14 14:31
Author: AdminContagio
Category: Educación, Nacional
Tags: Esteban Mosquera, Popayán, UNEES, Universidad del Cauca
Slug: esteban-mosquera
Status: published

###### [Foto: Esteban Mosquera] 

###### [14 Dic 2018] 

El estudiante Esteban Mosquera fue víctma del ESMAD, cuando se encontraba en las instalaciones de la Universidad del Cauca, en Popayán y fue alcanzado por una granada aturdidora que **lo golpeó en el ojo izquierdo, provocando un daño irremediable, lanzada por uno de los integrantes de la Fuerza Pública.**

Mosquera, de 25 años y quien se encontraba cursado el pregrado en música de esa institución, estaba con el conjunto del movimiento estudiantil en el campamento por la educación, en el marco del paro nacional, cuando se les informó de la fuerte arremetida por parte del ESMAD, e incluso de un posible desalojo realizado por el Ejército.

En el momento del impactó una de sus compañeras fue quien lo ayudó y fue trasladado al hospital de San José, en donde, tras realizarle una cirugía quirúrgica, se le informó a la familia del muchacho la perdida de su ojo.  (Le puede interesar: ["96 Personas heridas por represión del ESMAD en el marco del paro estudiantil en Cauca"](https://archivo.contagioradio.com/movimiento-estudiantil-del-cauca-uno-de-los-mas-golpeados-por-abusos-de-fuersa-del-esmad/))

Una de sus familiares publicó en redes sociales una sentida carta en donde le expresa su afecto y admiración a Esteban y le recalca la importancia de seguir en el estudio y composición de la música.

*MI NIÑO LINDO*

Que despertar tan amargo, que tristeza tan honda- Como quisiera que fuese tan solo una mas de las noticias falsas que cada día leo en la confrontación irracional en la que está la sociedad colombiana.

Recuerdo el día que naciste. Como te encontré despierto y sonriendo mientras tu mamá descansaba. Te cogí en brazos y te acerqué a mi corazón.  
Habías llegado sin avisar, irrumpiendo en la vida con esa sonrisa dulce y tranquila.  
No ha sido fácil. Un corazón sensible y lleno de ganas de que el mundo sea música.  
No puedo parar de llorar, de sentir como se me dobla el alma, de que seas tu, herido en medio de la violencia que no quiere cesar.

Ahora, mientras leo a quienes te insultan y dicen que te lo buscaste por estar en el movimiento de estudiantes que utiliza el conflicto, y a quienes desean utilizarte contra el gobierno, solo siento el fastidio que he sentido siempre hacia el fanatismo, la ignorancia querida y esas ideas retorcidas de quienes son tan mezquinos que solo piensan a través de sus vísceras y de sus complejos, de sus resentimientos o de sus odios.

Ahora mientras hablaba por teléfono y me preguntaban como vamos decírselo a tu abuelita, yo no sabía que decir. Porque le dolerá muchísimo. Ella siempre ha adorado a sus ovejitas rebeldes. Al fin y al cabo ella lo ha sido en cierto sentido.  
Pero mas me preocupas tu. Que esa violencia que has sentido en tu cuerpo y sus consecuencias físicas, no te dañe esa música que estás buscando y le de su lugar al caos.

Todas esas conversaciones que hemos tenido, nuestra complicidad, y conociendo tus ganas de buscar sentido a la vida mas allá de la rigidez social y cultural, tu horror ante la injusticia, tu humanidad, tienen que continuar perseverando en una manera constructiva. En esos pasos hacia adelante donde a pesar de la diferencia y de las resistencias se van dando los pasos que hacen un camino.

No tengas miedo a seguir el sendero que lleva al claro del bosque.  
Es oscuro, y si te das cuenta, hay fuerzas a ambos lados queriendo hacerte desistir, para que te quedes con ellos y no continúes.  
Porque ellos se rindieron a buscar la luz.

Eso es el fanatismo. El ser humano que decidió que ya lo sabía todo y que el mundo ha de ser a su manera a como de lugar. El ser caído. Que dejó morir su alma de niño. La curiosidad, las ganas de desvelar los misterios y de aprender. De dejarse sorprender por la realidad. y de verla en el encanto de la melodía auténtica, no falsificada para reescribir la historia en pentagramas que degeneran en marchas de guerra disfrazadas de armonía. O en himnos para alentar soldados a aniquilar al otro.

La música nunca ha sido una cuestión de poder.  
Mas una de las mas maravillosas expresiones humanas para elevar los sentimientos de estos seres que lo tenemos todo: LA VIDA. la única posibilidad.  
Y EL AMOR, la única respuesta.

Tus primitos que te adoran se fueron llorando al colegio porque están super orgullosos de tener un primo grande y siempre te recuerdan con alegría y les duele que estés pasando por esto.

Y yo pues que te puedo decir Mi Estebitan, que te quiero muchísimo, y que ese cariño inmenso no depende de nada porque te quiero así. Como eres.

Todo irá bien.

###### Reciba toda la información de Contagio Radio en [[su correo]
