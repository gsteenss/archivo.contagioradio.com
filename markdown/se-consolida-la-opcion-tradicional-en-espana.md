Title: Se consolida la opción tradicional en España
Date: 2016-06-27 15:23
Category: El mundo, Entrevistas
Tags: #UnidosPodemos, Elecciones España
Slug: se-consolida-la-opcion-tradicional-en-espana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/26j.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Okdiario.com] 

###### [27 jun 2016]

El pasado 26 de junio se realizaron en España las elecciones generales al congreso en donde se esperaba una reconfiguración del escenario político. Sin embargo, pese a que el Partido Popular se posicionó como la primera fuerza electoral, **no alcanzó la mayoría absoluta**, hecho que deja a la expectativa las colaciones electorales que se puedan dar en un posible tercer escenario de votación.

Diferentes encuestas previas a las elecciones, auguraban un panorama político esperanzador para la alianza Unidos Podemos, que terminó como [la tercera fuerza política con 71 escaños, dejando en segundo lugar al Partido Socialista Obrero Español con 85 escaños, y el Partido Popular en primer lugar con 137.](https://archivo.contagioradio.com/podemos-es-la-segunda-fuerza-politica-por-encima-del-psoe-en-espana/)

José Ronsero, vocal Vecino del Distrito Ahora Madrid, asegura que "la obtención de los 71 escaños de la alianza Unidos Podemos afirma la fuerza que se ha venido consolidando desde el inicio de Podemos, pero las elecciones del **26J fueron un fracaso frente a las expectativas que se tenían de las votaciones de Unidos Podemos, **en donde se esperaba una mayor participación ciudadana".

Para estas elecciones, **Unidos Podemos perdió un millón docientos de votos** que según Rosnero, se  transforman en **la campaña de desprestigio "Antipodemos" generada por el PSOE y el PP**, que en conjunto tenían la apuesta de frenar a Unidos Podemos. A su vez, expone que otro de los motivos de pérdida de votos pudo estar relacionada **con el miedo y el tradicionalismo de la población,** que pese a los múltiples escándalos en los que se ha visto envuelto el PP, continúa votando por la estructura actual.

De otro lado, el líder de Unidos Podemos, **Pablo Iglesias, informó que el día de ayer envío un mensaje al secretario general del PSOE** (Partido Socialista Obrero Español) Pedro Sánchez, para comenzar el diálogo entre las formaciones políticas, propuesta que podría convertirse en **una salida desde la sumatoria de escaños para obtener un gobierno de cambio o progreso.** Sin embargo, Rosnero índica que este acercamiento también ha generado que muchos votantes de izquierdas más radicales no acudan a las urnas, debido a que no sienten que hayan sido consultados, ocasionando una ruptura de la confianza entre las bases y la finalidad primordial de Podemos.

Con este panorama, se espera que España viva una tercera votación en donde los partidos deberán analizar cuáles serán sus apuestas y qué estarán dispuestos a ceder, para obtener la mayoría absoluta en las próximas elecciones.

[![podemos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/podemos.jpg){.aligncenter .size-full .wp-image-25786 width="999" height="372"}](https://archivo.contagioradio.com/se-consolida-la-opcion-tradicional-en-espana/podemos/)

<iframe src="http://co.ivoox.com/es/player_ej_12043951_2_1.html?data=kpedlpideZKhhpywj5aZaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncavj1IqwlYqlfYzG0NPgx9fTaZO3jNvcxcbQb7fZxM7b0ZDIqc2fpc7g1tfNuNCfos3c1MaPkcLY087Rj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

 
