Title: OtraMirada: Así va la lucha contra los Femicidios desde los territorio
Date: 2020-06-25 20:53
Author: PracticasCR
Category: Actualidad, Otra Mirada, Otra Mirada, Programas
Tags: femicidio, feminicidios, patriarcado, Rosa Elvira Cely, Violencia contra las mujeres
Slug: otramirada-lucha-contra-feminicidios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Oaxaca tres punto cero

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El incremento de casos de femicidio y violencia contra mujeres, niñas y jóvenes en la época de pandemia ha sido un tema preocupante para varios sectores de la sociedad y los medios de comunicación no podemos ser ajenos a esta realidad. Durante el periodo de confinamiento han registrado 76 feminicidios y 99 en lo corrido del año.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para desarrollar este tema invitamos a Yaneth Suárez, sobreviviente de tentativa de feminicidio y Adriana Arandia Cely, activista y hermana de Rosa Elvira Cely; para el análisis contamos con la voz de Diana Salcedo López directora de la Liga Internacional de Mujeres por la Paz y la Libertad Limpal y Jani Silva, representante de la Zona de Reserva Campesina de la Perla Amazónica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este espacio se pudo conocer cuál ha sido el camino de muchas de ellas para transformar sus vidas y las de otras mujeres y su visión acerca del incrementando del femicidio y otros tipos de violencias durante el aislamiento. También aportaron en la reflexión para el desmonte del sistema patriarcal y derribar esos muros que no solo están presentes en las zonas rurales y que no distinguen raza, nacionalidad o edad. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, explican cómo se vive desde los territorios esta lucha y cómo se ha fortalecido la defensa de las mujeres desde las organizaciones y explican el trabajo que desarrollan para transformar las instituciones del Estado para garantizar la vida de todas las mujeres y que las denuncias tengan una ruta de justicia justa. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, es un espacio para dar esperanza a víctimas, familiares y a toda la comunidad, pues estas reflexiones evidencian que se están dando los cambios en todos los niveles para que la violencia contra la mujer sea superada. (Si desea saber sobre nuestros programas anteriores: [la cadena perpetua desde](https://archivo.contagioradio.com/la-cadena-perpetua-desde-la-optica-constitucional/) [la](https://archivo.contagioradio.com/la-cadena-perpetua-desde-la-optica-constitucional/) [optica constitucional](https://archivo.contagioradio.com/la-cadena-perpetua-desde-la-optica-constitucional/) )

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/473187150189431/?__xts__[0]=68.ARCqeq5w2CtjV98l7QLVPKnfJI-7by4axc_iIo6prJdBJEi1A5Gilmbay7d0VkHSWOhwsgDREwfIhhTCB0K9E7ORbFMrT8CyJYedC_tO8A0kAbvYvW2DbtBbHNs_4_YvMJ-a0IvThwL1y0AdhFlo6cjyqEJHEYAQTT4tzZYMFQ0e8aqT6cgDSKsF35mS1vRnzvMkTIUNpv-cEBI9qhAzGGwXX7hWNzIX-5uYCmmrPAyKZ4N9TBekrLU3YESDFn7Hb7ddvQEYrhZQR9Cto2hPzvM5EcWPGhVqNROt1lPtQQYnPDrnjUje3cLcNnOHv6cxLzw8xcqE7-no834KR0hFi8DitJA\u0026amp;__tn__=-R","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/473187150189431/?\_\_xts\_\_\[0\]=68.ARCqeq5w2CtjV98l7QLVPKnfJI-7by4axc\_iIo6prJdBJEi1A5Gilmbay7d0VkHSWOhwsgDREwfIhhTCB0K9E7ORbFMrT8CyJYedC\_tO8A0kAbvYvW2DbtBbHNs\_4\_YvMJ-a0IvThwL1y0AdhFlo6cjyqEJHEYAQTT4tzZYMFQ0e8aqT6cgDSKsF35mS1vRnzvMkTIUNpv-cEBI9qhAzGGwXX7hWNzIX-5uYCmmrPAyKZ4N9TBekrLU3YESDFn7Hb7ddvQEYrhZQR9Cto2hPzvM5EcWPGhVqNROt1lPtQQYnPDrnjUje3cLcNnOHv6cxLzw8xcqE7-no834KR0hFi8DitJA&\_\_tn\_\_=-R

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
