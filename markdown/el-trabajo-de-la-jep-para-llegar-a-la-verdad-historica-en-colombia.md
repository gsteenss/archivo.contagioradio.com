Title: El trabajo de la JEP para llegar a la verdad histórica en Colombia
Date: 2020-10-05 18:36
Author: AdminContagio
Category: Actualidad, Nacional
Slug: el-trabajo-de-la-jep-para-llegar-a-la-verdad-historica-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/jep.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 3 de octubre el país encabeza de la Jurisdicción Especial para la Paz (JEP), conoció la aceptación de responsabilidad de los homicidios de,[Álvaro Gómez Hurtado](https://www.jep.gov.co/Sala-de-Prensa/Paginas/Farc-asume-responsabilidad-en-homicidio-de-%C3%81lvaro-G%C3%B3mez-Hurtado-y-en-otros-cinco-casos.aspx); Hernando Pizarro León Gómez; José Fedor Rey; Jesús Antonio Bejarano; Fernando Landazábal Reyes, y Pablo Emilio Guarín, por parte de las FARC-EP.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/TimoFARC/status/1312483934077288448","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/TimoFARC/status/1312483934077288448

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Un hecho que compromete en el esclarecimiento de la verdad a[8 de los ex jefes de la FARC-EP](https://archivo.contagioradio.com/farc-ep-reconoce-ante-la-jep-la-verdad-sobre-6-homicidios-entre-ellos-el-de-alvaro-gomez-hurtado/), y en donde por medio de una carta de reconocimiento de estos hechos le entrega a la JEP, la responsabilidad de buscar una ruta que permita por medio de la justicia esclarecer estos delitos cometidos en el marco del conflicto armado colombiano, **correspondiente a una verdad histórica con múltiples responsables reconocidos por la opinión pública durante años y que hoy apunta a la extinta guerrilla de las FARC.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de los retos que surgen alrededor del tratamiento que le dará estas confesiones la JEP, también se levantan incógnitas de cómo funcionará esta información, que además no corresponde a ninguno de los siete macro casos que ha abierto la JEP, y además, **evidencia una verdad temprana, al no haber ningún proceso judicial en marcha por parte de la jurisdicción que obligara a los ex guerrilleros a contar la verdad.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una verdad que también fue reconocida por la Comisión de la Verdad, quienes **acogieron la determinación del partido FARC de aceptar responsabilidades y contribuir a la verdad "como un acto de ética pública**", y se comprometieron a establecer una comunicación inmediata con los familiares de las 6 víctimas señaladas en esta confesión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señalando que en su trabajo del ***"esclarecimiento de la verdad histórica, escucharán a los miembros de la extinta guerrilla para contrarrestar sus versiones con las de otras personas y entidades",*** que también quieran aportar a contribuir al esclarecimiento de estas verdades, en donde algunos casos ya superan más de dos décadas de haberse cometido como es el caso de Álvaro Gómez Hurtado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo respaldaron el lugar que tiene la JEP, en su ***"ejercicio de la justicia transicional y ASUME su rol extrajudicial en la complementariedad bajo el cual fue diseñado el sistema".***

<!-- /wp:paragraph -->

<!-- wp:heading -->

La verdad que hoy enfrenta la JEP
---------------------------------

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DePacotilla/status/1313176281794703361","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DePacotilla/status/1313176281794703361

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Y mientras cada vez se abre más el debate en contra de la decisión del presidente de la Comisión de la Verdad, Francisco De Roux, de aceptar la verdad entregada por los ex militantes de FARC-EP, **defensores de la paz y del acuerdo como el excongresista Álvaro Leyva indican que pese a su cercanía con algunas de las víctimas de estos seis asesinatos** es necesario reconocer la verdad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"En la puerta de la universidad Sergio Arboleda, al lado del carro parcialmente destrozado por las balas que le quitaron la vida, viendo su sangre que manchaba el asiento trasero, lloré. Lloré a Álvaro, lloré a un hombre recto a un historiador, a un talante…"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por medio de una carta enviada desde España Leyva indica, ***"llegó la hora de la verdad, llegó la hora de que la expongan todos los que participaron directa o indirectamente en el conflicto armado nacional",*** y agregó, *"colombianos: no más. El estado y sectores de la sociedad aún cabalgan cómodamente en el lomo de las mentiras que generaron la confrontación armada pero que son inconscientes en el escenario brindado al país por el acuerdo de paz"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y enfatizó diciendo que pese a que muchos han pretendido soslayar o maltratar el acuerdo, el esclarecimiento de la verdad de los hechos del conflicto por *"difíciles e incómodos"* que sean son el elemento necesario para la construcción de la paz y la reconciliación.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"El camino a la verdad puede ser tortuoso y triste pero hay que transmitirlo además abre la trocha que no sabré de conducir a la paz integral".**

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y pensé que son muchas las versiones las que confluyen en las razones por las que los ex comandantes de FARC decidieron entregar esa verdad temprana al sistema integral de verdad justicia y reparación, por ahora sólo quedan dos caminos, por un lado que estos seis procesos continúen en la justicia ordinaria, o que se adopten y respeten la competencia y las medidas que serán entregadas por la JEP en un proceso de justicia restaurativa.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
