Title: ¿Cuándo podrá vivir en paz la comunidad de San José de Apartadó?
Date: 2015-09-30 14:06
Category: Carolina, Opinion
Tags: Comunidad de Paz de San José de Apartadó, Control paramilitar en San José de Apartadó, Paramilitares asesinan a campesino Ernesto Guzmán
Slug: cuando-podra-vivir-en-paz-la-comunidad-de-san-jose-de-apartado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/comunidad-de-paz-e1443639750934.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nasaasin.org 

###### [29 Sep 2015] 

#### **[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) - ** [**@E\_vinna**](https://twitter.com/E_Vinna) 

[El pasado 21 de septiembre cinco niños quedaron sin padre a causa de las balas de los paramilitares. El hecho ocurrió en el corregimiento de San José, en el municipio de Apartadó, Antioquia. El padre se llamaba Ernesto Guzmán y era un humilde campesino de la región que vendía sus productos a la Comunidad de paz de San José de Apartadó e incluso tenía a familiares viviendo en esta emblemática colectividad que se constituyó como un territorio de vida y paz desde marzo de 1997.]

[El asesinato del señor Ernesto Guzmán a manos de paramilitares, como lo indican la comunidad y los vecinos, es una prueba más de la preocupante situación de seguridad de esta población. Una situación que es constantemente denunciada, que ha sido evidenciada nacional e internacionalmente y que ha cobrado la vida de cientos de hombres, mujeres, ancianos y niños, como ocurrió el 21 de febrero de 2005, cuando militares y paramilitares perpetraron la atroz masacre de San José de Apartadó.]

[El control paramilitar de esta zona no cesó con la supuesta desmovilización producto de la “ley de Justicia y paz”. Aunque se les trate de cambiar el nombre a “Bandas emergentes” o “Bandas criminales”, los campesinos de la zona saben que son exactamente los mismos paramilitares y que se llamen como se llamen siguen aterrorizando a la región bajo la mirada cómplice de la fuerza pública. Históricamente las fuerzas militares en esta región no sólo han sido permisivas de la acción paramilitar contra la Comunidad de Paz, sino que contribuyen a la estigmatización de sus integrantes tildándolos constantemente como guerrilleros e incluso participando directamente en acciones en su contra.]

[Pocas comunidades en Colombia son tan conocidas como la Comunidad de Paz de San José de Apartadó y las razones de su reconocimiento nacional e internacional son dos: primero, porque a lo largo de estos 18 años de trabajo han permanecido como una propuesta de vida en el territorio y como una apuesta ejemplar de dignidad y resistencia. Y la segunda razón es porque han soportado con entereza masacres, asesinatos, violencia, amenazas e incluso operativos conjuntos de militares y paramilitares que han ocasionado varios desplazamientos. Cada una de estas violaciones a sus derechos ha sido denunciada con valentía pero ninguna ha tenido una respuesta satisfactoria por parte del Estado.]

[Seguimos instando al Gobierno a actuar inmediatamente para desmontar a los grupos paramilitares y sobre todo ponerle punto final a las macabras alianzas que tienen éstos con las fuerzas armadas del Estado. El compromiso del Gobierno debe ser real, porque la firma de un acuerdo con las Farc en La Habana no tendrá ningún efecto en una población como la de la Comunidad de San José de Apartadó si no se trabaja con entereza y efectividad en las garantías de no repetición.]

[¿Hasta cuándo? Se pregunta constantemente la comunidad, “¿Hasta cuándo continuará el Gobierno Nacional en esa casería salvaje y agresiva de todos estos 18 años contra nuestra Comunidad de Paz, pisoteando las sentencias y las medidas cautelares y provisionales de la Comisión y la Corte Interamericana de Derechos Humanos?” sostienen en uno de sus recientes comunicados.]

[La Comunidad de San José de Apartadó tiene como símbolo de sus muertos una pared construida con piedras, cada una de estas piedras tiene el nombre de los integrantes de la comunidad que han sido asesinados. Deseamos y exigimos que la de Ernesto Guzmán sea la última piedra en ese doloroso mural. Debemos actuar para que el mural no se convierta en una gran muralla.]
