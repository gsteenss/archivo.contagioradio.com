Title: Joven indígena le ganó la pelea al servicio militar obligatorio
Date: 2016-02-05 17:30
Category: DDHH, Nacional
Tags: Daniel Morera Pamo, Reclutamiento ilegal
Slug: joven-indigena-le-gano-la-pelea-al-servicio-militar-obligatorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Indígena-servicio-militar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Proyectos Comunidad ] 

###### [5 Feb 2016 ]

<iframe src="http://www.ivoox.com/player_ek_10333193_2_1.html?data=kpWglZiVfZShhpywj5WdaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncavj18rbjc7SqIa3lIqupszJssKfzcqYycbSaaSnhqegjdHFb9HZzcrOjcbQb9TZ09vWxc7Tb87dzc7hw9ePs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

20 días estuvo retenido ilegalmente en el Batallón Mecanizado Silva Plazas de Duitama el joven indígena Daniel Morera Pamo, perteneciente al resguardo Palma Alta de Natagaima, Tolima, quien fue **obligado a raparse, cambiar su ropa habitual por un camuflado y botas, y recibir instrucción militar**, pese a que de acuerdo con la Ley 48 de 1993 “los miembros de comunidades indígenas están exentos en todo tiempo de prestar el servicio militar”.

Según Flor Pomo, madre del joven, Daniel se dirigió el pasado 9 de enero al Distrito Militar de Bogotá, “pensando en que ese día le definirían los trámites de la libreta militar, pues ya había hecho las vueltas en Chaparral, Tolima”, sin embargo, los militares invalidaron lo que el joven ya había hecho, le realizaron algunos exámenes médicos y **le condujeron contra su voluntad a la ciudad de Duitama**.

Familiares de Daniel hicieron llegar al Batallón los certificados de pertenencia indígena del joven, pero los militares hicieron caso omiso y de acuerdo con Flor Pomo, ellos “decían que estaba en trámite su salida, que estaban verificando los documentos, si existía o no el resguardo, pero **no daban respuesta inmediata, estaban resueltos a dejarlo a la fuerza**” y fue hasta el pasado 1 de febrero que lo dejaron en libertad.

La madre del joven asevera que pese a que el joven no fue agredido físicamente, ella está considerando entablar “una demanda para que esto no vuelva a suceder más adelante con otros muchachos”, pues "**algunos de ellos van con miedo, muchos no desean quedarse allá**" y "no es justo que los lleven a la fuerza".
