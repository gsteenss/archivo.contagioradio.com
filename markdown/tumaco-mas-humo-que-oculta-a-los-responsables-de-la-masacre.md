Title: Tumaco: más humo que oculta a los responsables de la masacre
Date: 2017-10-17 08:29
Category: Abilio, Opinion
Tags: Masacre de Tumaco, Tandil
Slug: tumaco-mas-humo-que-oculta-a-los-responsables-de-la-masacre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/tumaco-versiones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 16 Oct 2017 

[Amanecimos con una nueva versión de organismos del Estado, sobre  los hechos de la masacre de Tandil,  Tumaco del 5 de octubre,  donde un número aún indeterminado de campesinos cultivadores de hoja de coca –al menos 7- otros heridos, dicen que 50 y algunos desaparecidos por comprobar, fueron víctimas  luego de participar en una protesta para evitar la erradicación forzada de cultivos de hoja de coca por parte de la policía antinarcóticos y el ejército.]

[Caracol radio al medio día de ayer presentó como primicia que conoció el informe de balísticas según el cual los 6 cuerpos hallados habían sido impactados con proyectiles en la espalda  cuya trayectoria era de “abajo hacia arriba”, que se trataba efectivamente de armas usadas por la fuerza pública, más no era seguro  que fueran disparadas  por agentes oficiales dado que se habían perdido, previamente, 14 fusiles, como lo estaba investigando la Fiscalía.(Ver:][[http://caracol.com.co/radio/2017/10/15/judicial/1508085986\_804810.html]](http://caracol.com.co/radio/2017/10/15/judicial/1508085986_804810.html)[). Nada dicen  del cuerpo de  un indígena que testigos  y fotografías  señalan que fue rematado por la espalda por parte de un policial. (][[http://www.semana.com/nacion/articulo/masacre-en-tumaco-sobrevivientes-hablan-de-lo-que-paso/543409]](http://www.semana.com/nacion/articulo/masacre-en-tumaco-sobrevivientes-hablan-de-lo-que-paso/543409)[  y ver:][[http://alc-noticias.net/es/2017/10/15/tumaco-dialogo-intereclesial-por-la-paz-saca-a-la-luz-lo-verificado-en-territorio/]](http://alc-noticias.net/es/2017/10/15/tumaco-dialogo-intereclesial-por-la-paz-saca-a-la-luz-lo-verificado-en-territorio/)[ ).]

[Pareciera que se trata de otro chorro de humo como los que sucesivamente  se han esparcido sobre la opinión ante estos gravísimos hechos. Primero, el propio presidente de la República rodeado por su vicepresidente  y toda la cúpula militar y de policía se apresuró a encubrir la mal herida verdad  de los hechos, afirmando que se trataba de un ataque de la disidencia de las FARC que había atacado con cilindros bombas (][[https://www.telesurtv.net/news/Santos-responsabiliza-a-bandas-por-masacre-en-Tumaco-Colombia-20171006-0057.html]](https://www.telesurtv.net/news/Santos-responsabiliza-a-bandas-por-masacre-en-Tumaco-Colombia-20171006-0057.html)[) . No dice a quienes:  ¿A los campesinos? ¿A los policías? La versión  se desvaneció como el humo al conocer los videos, fotografías  grabadas por los campesinos y sus testimonios que dieron cuenta de disparos de la policía. Pero más aún,  Medicina legal  al examinar seis cuerpos de las víctimas estableció fueron  proyectiles de alta velocidad los que  les había causado la muerte.]

[Adicionalmente, ni un solo policía apreció afectado y en la escena de los hechos no aparecía rasgo alguno de explosión,  más sí árboles con las marcas de los disparos, a pesar de que los más gruesos, que seguramente albergaban proyectiles de la policía, habían sido cortados, alterando la escena del crimen.]

[Ya la Defensoría del Pueblo había  señalado que  de acuerdo a los testimonios y la observación superficial de la escena de los hechos, los presuntos responsables eran  policías (Ver:][[http://www.elpais.com.co/colombia/fuerza-publica-seria-participe-de-muerte-de-campesinos-en-tumaco-defensoria.html]](http://www.elpais.com.co/colombia/fuerza-publica-seria-participe-de-muerte-de-campesinos-en-tumaco-defensoria.html)[). Así mismo la misión que verificaba en la zona el 8 de octubre, que dicho sea de paso fue atacada con disparos policiales,  integrada por la Misión de Verificación Onu, ONU Derechos Humanos, MAPP OEA, funcionarios de la gobernación de Nariño y organismos de derechos humanos, estableció  lo mismo.]

[Ante una nueva humareda para intentar ocultar la verdad, ahora del ataque a la misión de verificación,  el Ministro  de Defesa y la policía emitieron  comunicaciones en las que afirmaban que la misión de verificación no había informado a las “autoridades” de su presencia en la zona y que había pretendido ingresar al área de seguridad de manera forzada (][[https://www.radionacional.co/noticia/masacre-tumaco/se-necesita-coordinacion-poder-visitar-estos-sitios-mindefensa-ataque-a]](https://www.radionacional.co/noticia/masacre-tumaco/se-necesita-coordinacion-poder-visitar-estos-sitios-mindefensa-ataque-a)[) .  Esta nueva mentira debió ser desvirtuada  por los tres organismos internacionales mediante un comunicado donde enfatizan el conocimiento que el gobierno tenía de dicha misión. (Ver:][[https://www.elespectador.com/noticias/nacional/la-onu-y-la-oea-rechazan-ataque-mision-humanitaria-en-tumaco-articulo-717342]](https://www.elespectador.com/noticias/nacional/la-onu-y-la-oea-rechazan-ataque-mision-humanitaria-en-tumaco-articulo-717342)[).  Por este lado, tampoco fue posible evadir la responsabilidad sobre el ataque a la misión de verificación, por lo que el vicepresidente Oscar Naranjo presentó excusas.]

[Ya se le había dicho al presidente Naranjo  en reunión del 9 de octubre  que en los terribles hechos de Tandil, Tumaco, la verdad había resultado gravemente herida y la confianza en la institucionalidad se acababa de desvanecer.]

[Valeria, una lideresa  de la región, al conocer  la nueva versión que surge sobre la  perdida de fusiles y los disparos con trayectoria “de abajo hacia arriba” se preguntó   ¿por qué ni un solo policía resultó herido?  Y también cabe la pregunta de  Noticias Uno en su emisión del  16 de octubre, ¿si tal pérdida de fusiles fue en el mes de abril, por qué hasta ahora se habla del asunto?]

[Valeria quien fue la  única campesina que pudo ingresar a la reunión con el  vicepresidente Naranjo   en Tumaco ya le había dicho,   mientras lloraba, como presintiendo esta nueva extraña versión de los hechos:]

[ “Me parece muy humillante y nos duele mucho que mientras nosotros estamos enterrando a nuestra gente, salgan los comunicados de la policía diciendo que fueron cilindros bomba de un grupo armado al margen de la ley cuando no fue así. Le pedimos encarecidamente que nos ayude a esclarecer y a sacar a la luz la verdad y que si nos van a traer un plan de sustitución, si nos van a remover de allí, que nos presenten proyectos y oportunidades de vivir. No balas, no masacre, no muertos, nos más sangre. Hace tres días enterramos al compañero indígena al que remataron, y era del Ecuador, solamente llegó a la vereda del Azúcar a trabajar, y era desgarrador escuchar los familiares diciendo: por qué la policía colombiana mata nuestra gente, si cuando ustedes van al Ecuador nuestros policías no los matan. ¿Por qué la gente colombiana nos asesina? Era tanto su dolor y su impotencia que el expresaba eso".]

[Ojalá  la respuesta al  clamor de Valeria   por parte del vicepresidente Naranjo, a nombre del Estado, no sea más humo para pretender ocultar la verdad de esta primera, y esperamos única,  masacre del pos acuerdo.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio 
