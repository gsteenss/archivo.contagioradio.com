Title: FECODE se prepara para el próximo paro definido el 9 y 10 de mayo
Date: 2018-04-23 12:39
Category: Educación, Nacional
Tags: fecode, Ministerio de Educación, Yaneth Giha
Slug: fecode-llama-a-paro-definido-los-proximos-9-y-10-de-mayo
Status: published

###### [Foto: Archivo Partícular ] 

###### [19 Abr 2018]

La Federación Colombiana de Docentes, FECODE, anunció que los próximo 9 y 10 de mayo se realizaran actividades para denunciar el incumplimiento del Ministerio de Educación a los maestros en puntos como la financiación de la Educación y garantías de salud para los docentes. De acuerdo con Over Dorado, integrante de esta organización, **en esos temas ni siquiera ha existido un 5% de avance en las conversaciones**.

Para Dorado las alertas sobre la crisis que afronta la Educación continúan encendidas debido a que no ha existido ninguna propuesta desde el Ministerio de Educación sobre asignar más recursos a este rubro. "La propuesta del ministerio es repartir la pobreza" manifestó el integrante de FECODE y agregó que para el 2018 la desfinanciación llegará a superar los dos billones de pesos. (Le puede interesar:["Gobierno esta repartiendo la pobreza pero no aumenta el presupuesto: FECODE"](https://archivo.contagioradio.com/gobierno-esta-repartiendo-la-pobreza-pero-no-aumenta-presupuesto-fecode/))

### **Una vez más el Ministerio le incumple a los profesores **

Sobre los acuerdos de red de Salud que se habían pactado, Dorado manifestó que la red de prestación del servicio de Salud, que fue asignada por el Estado, **no cumple con los requisitos necesarios para atender a los docentes ni a sus familias**.

"Se vienen presentando situaciones complejas, no se están dando las citas generales, tampoco las de odontología, las citas de especialistas ni se diga, en las enfermedades de alto costo no está surtiendo en los términos acordados", señaló Dorado. Circunstancia que para FECODE, **es producto de la falta de organización y articulación entre el Gobierno, los contratistas y la fiduprevisora**.

Referente al Sistema de la financiación de la Educación y la reforma estructural del sistema de participación, los docentes solicitaban un mayor recurso destinado a este sector. Sin embargo, Dorado afirmó que las soluciones que da el Ministerio de Educación son de forma y no fondo, **"en el cuerdo que se firmó nosotros planteábamos que hacían falta recursos para la Educación y había que aumentarlos**, sin quitarle a los demás sectores".

Frente a esa situación FECODE propuso una progresividad en la financiación a la Educación, "no estamso diciendo que se pase del 25% de financiación al 45 o 47%, estamos diciendo que anualmente se suba el porcentaje entre 6 o 7%".

De otro lado, la Ministra de Educación Yaneth Giha, aseguró que es "absurdo" que FECODE vuelva a hacer un llamado a paro, debido a que han cumplido con todos los acuerdos del año pasado. No obstante para Over Dorado, el hecho de que se hayan reunido más de 11 veces no significa necesariamente que haya existido una voluntad política por parte del Ministerios para avanzar en los compromisos.

### **El paro ** 

El paro tendrá un carácter definido y se realizará los días 9 y 10 de mayo, el primer día se harán asambleas en las diferentes seccionales de FECODE en todo el país, mientras que el segundo se llevarán a cabo movilizaciones en 5 ciudades: Bogotá, Cali, Medellín, Barranquilla y Bucaramanga.

<iframe id="audio_25575687" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25575687_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
