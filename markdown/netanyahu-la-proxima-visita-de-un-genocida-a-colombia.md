Title: Netanyahu: La próxima visita de un genocida a Colombia
Date: 2017-09-05 09:15
Category: Opinion, Ricardo
Tags: Israel, Netanyahu, Yair Klein
Slug: netanyahu-la-proxima-visita-de-un-genocida-a-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Netanyahu-en-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [IsraeliPM]

#### **[Ricardo Ferrer Espinosa - @ferrer\_espinos](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/)** 

###### 5 Sep 2017 

En pocos días Benjamín Netanyahu estará de visita en Colombia [\[1\].] ¿Se reunirá con las miles de víctimas de los mercenarios y paramilitares que Israel entrenó?.

Ahora que se negoció el fin del conflicto con las FARC, la visita es inoportuna y dolorosa. Las relaciones entre Israel y Colombia van mal, si las miramos desde una visión civil.

Desde la invasión española, parte del pueblo hebreo ha encontrado aquí su refugio. Los apellidos sefardíes destacan hoy en la política, literatura y la economía. Durante la Segunda guerra Mundial la comunidad civil colombiana les acogió y protegió.

**En 1948, Colombia dio su voto para la creación del Estado de Israel**[. Desde entonces se unen los judíos de la Díaspora, residentes en Colombia, con los Sabra \[]**צבר**[\], los nacidos en Israel. Son miles las familias colombo – israelíes.   ]

**Desde 1982, se envió el Batallón Colombia a la frontera entre Israel y Egipto,**[ como parte de los acuerdos de Camp David. Allí, por malas artes, los militares colombianos aprendieron el concepto de la Haganá \[2\], Autodefensa, ההֲגָנָה , que aplicado en Colombia derivó en las Autodefensas Unidas de Colombia. El paramilitar Carlos Castaño confesó en sus libros que en  Israel aprendió, entre otras cosas, algunas técnicas de interrogatorio (eufemismo para la tortura). Igualmente está documentado el envío de un avión cargado de armas, desde Israel, para las AUC.]

[Existe acopio testimonial sobre acciones paramilitares que se gestaron desde la base colombiana en el Sinaí. La misión debería ser retirada porque Colombia ha roto la neutralidad ante Egipto y la doctrina militar que irradia rompe el marco del DIH. Será un buen debate para el Congreso \[3\].    ]

**Desde 1986 Colombia envía a Israel miles de toneladas de carbón del Cerrejón**[. A cambio,  Colombia recibe equipo y tecnología militar. El carbón se ha convertido por esta vía en combustible para la guerra. \[4\]. Lo notable es que coincide el inicio de las exportaciones de carbón a Israel, con el inicio de las escuelas de mercenarios en toda Colombia. El saldo para Colombia, que nunca se menciona, son las viudas y huérfanos. ¿Qué le hicimos los colombianos a Israel, para merecer este trato?]

[En el año 2002 Yair Klein fue condenado por un tribunal de Manizales, por el entrenamiento ilegal de militares y paramilitares colombianos.  \[5\].  ]

La acción de mercenarios en Colombia ha sido intensa y la respaldaron activamente empresas multinacionales. Ante el fenómeno, los gobiernos hicieron la vista gorda, adaptaron la legislación en contra del interés nacional y las Fuerzas Armadas se ufanan de sus nuevos socios e instructores.

[Durante los últimos años se habla de que “]***Colombia es el Israel de Sur América”*** [frase que llena de orgullo al gobierno de Santos, llena de muertos los campos colombianos y alarma a los países vecinos \[6\].]

**En ese contexto viene Benjamín Netanyahu.** [Si termina la guerra en Colombia, peligra la venta de armas. ¿Será que la tecnología militar será sustituida por tecnología agrícola? El modelo de los Kibutz sería un gran aporte para el campo colombiano en el post conflicto. La sociedad civil de Israel nos pueden aportar bastante en la reconstrucción nacional. Pero ese sueño solo es posible si hay cambios severos en los gobiernos de Israel y de Colombia. Y es imposible con Netanyahu, el genocida de Gaza.]

------------------------------------------------------------------------

###### [\[1\]] **[Netanyahu escogió a Colombia para primera visita de un primer ministro israelí a A. Latina. 19 agosto de 2015.](http://www.pulzo.com/mundo/392891-netanyahu-escogio-colombia-para-primera-visita-de-un-primer-ministro-israeli-latina)**

###### [\[2\]] **En:** [**https://es.wikipedia.org/**](https://es.wikipedia.org/) ** **[ver la palabra] **Haganá**

###### [\[3\]] **[Sinaí Misión de paz. Ejército de Colombia.](http://www.ejercito.mil.co/index.php?idcategoria=74086)** 

###### [“El Batallón Colombia N3, \[desde\] abril de 1982 como integrante de la MFO, ocupa el área central de la zona C y establece seguridad perimétrica en campo norte, para observar y reportar las violaciones al tratado de paz entre Egipto e Israel, al igual que proteger las instalaciones del cuartel general de la MFO...”] 

###### [\[4\]] **[Mas carbón colombiano a Israel](http://www.eltiempo.com/archivo/documento/MAM-25069)- **[El Tiempo, 28 de noviembre de 1990 ] 

###### [\[5\] ]**Yair Gal Klein.**[ En mi blog:][[http://mercenariosencolombia.blogspot.com]](http://mercenariosencolombia.blogspot.com) 

###### [[http://mercenariosencolombia.blogspot.com/search/label/Yair%20Gal%20Klein]](http://mercenariosencolombia.blogspot.com/search/label/Yair%20Gal%20Klein) 

###### [Nuestra guerra ajena. Germán Castro Caicedo. Planeta. 2014.](http://www.casadellibro.com/ebook-nuestra-guerra-ajena-ebook/9789584241795/2399115) 

###### [\[6\]] [[Israel y Colombia, una relación preocupante. 8 de agosto de 2010](http://www.peaceobservatory.org/1056319512/israel-y-colombia-una-relacion-preocupante).] 

###### [[http://www.peaceobservatory.org]](http://www.peaceobservatory.org) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
