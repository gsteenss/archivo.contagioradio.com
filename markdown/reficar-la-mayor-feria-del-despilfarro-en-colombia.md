Title: Reficar la mayor feria del despilfarro en Colombia
Date: 2016-05-05 17:16
Category: Economía, Nacional
Tags: CBI EMPRESA, corrupcion reficar, Ecopetrol, reficar, refineria cartagena
Slug: reficar-la-mayor-feria-del-despilfarro-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Reficar-infografía.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cable Noticias ] 

###### [5 Mayo 2016]

De acuerdo con la Contraloría General de la República, entre noviembre de 2007 y octubre de 2015 en la ampliación de la Refinería de Cartagena se registraron sobrecostos de por lo menos US\$4 mil millones, por cuenta de **onerosas adiciones en contratos, altos costos de materiales, reparación de maquinaría** **y elevación del costo de mano de obra**.

Sí bien la **mano de obra en la instalación de tubería incrementó 127%**, pasando de un valor inicial de US\$ 39,4 millones a uno final de US\$89,7 millones, este aumento **no significó mejores condiciones laborales** para los 34 mil trabajadores que fueron contratados por la firma CB&I, según denuncia Edwin Castaño, integrante de la Unión Sindical Obrera, USO.

Castaño afirma que **mientras un empleado colombiano ganaba \$3.500.000 por conducir una grúa, a un trabajador extranjero por hacer lo mismo le pagaban \$18.000.000**. Los técnicos de otros países tenían derecho a 40 días de vacaciones y eran alojados en los mejores hoteles de Bocagrande, en cambio los empleados de otras regiones de Colombia contaban con 23 días de vacaciones y debían hospedarse de a 4 personas en un sólo cuarto.

Por si fuera poco, recientemente MinTrabajo aprobó la suspensión de los contratos que la CB&I había suscrito con **347 empleados, que actualmente sufren enfermedades laborales, como problemas lumbares y de articulaciones, distintos tipos de cáncer o amputaciones**. Esta decisión reversa la negativa que el Ministerio había comunicado en agosto de 2015, y que fue apelada por la CB&I argumentando que la finalización de la obra le impedía contar con los recursos financieros para cumplir.

Frente a esta vulneración, los empleados decidieron movilizarse en la ciudad de Cartagena el pasado martes, llamando la atención de Clara López, actual ministra de trabajo, con quien pactaron un encuentro en el que esperan soluciones reales a la crisis, **que no sean suspendidos los contratos, que la compañía CB&I pague los bonos estipulados en la convención colectiva**, que las aseguradoras de riesgos profesionales y los fondos de pensiones brinden el acompañamiento debido y que Ecopetrol se manifieste.

Sin embargo, estas violaciones a los derechos laborales no son recientes, según Castaño al iniciar la obra, se construyó una malla para impedir que los trabajadores tuvieran contacto con el sindicato que, frente a los **bajos salarios y la falta de seguridad social denunciada por los empleados**, decidió impulsar una huelga en octubre de 2009, que finalizó con un alza salarial por debajo de lo exigido en la convención colectiva, negociada por Ecopetrol y no por CB&I.

Otros de los hechos violatorios se relacionan con el **asesinato del líder Aury Sara Marrugo**; el **desplazamiento forzado del sindicalista Rodolfo Vecino** amenazas contra sus hijos y un atentado que también puso en peligro la vida de su esposa; el **atentado del que fueron víctimas Rafael Cabarcas y su escolta**, y el despido del 70% de la junta subdirectiva de la USO, todo ello en Cartagena, cuando la obra apenas iniciaba.

Pero las irregularidades no sólo fueron de orden laboral. Según la Contraloría los nueve contratos para el **alquiler de grúas incrementaron 70,2%, de un presupuesto de \$129.771 millones**, terminaron costando \$220.952 millones, lo más irrisorio es que muchas de estas máquinas duraron años parqueadas sin ser usadas.

En Reficar aún quedan **97 contenedores sin abrir, 5 bodegas de gran volumen llenas de material**, 4 reactores, más otra cantidad no establecida de hornos y vasijas que se están pudriendo en un muelle cercano, cuando debían estar en la refinería de Barranca. En suma más de **US\$400 millones gastados en materiales que nunca se usaron**. Sin incluir las 250 toneladas de materiales que reposan en Houston, Texas.

La ampliación de la refinería se planteó con el objetivo de superar el desabastecimiento de energía en Colombia, pasar de refinar 80 mil barriles diarios a 165 mil; sin embargo a 2016 esta meta no se ha logrado. **Actualmente se refinan 140 mil barriles de petróleo diarios con algunas limitaciones**. La producción de Diesel no supera los 22 barriles, su importación hoy es mayor a las cifras reportadas años atrás, no más en 2015 se gastaron por lo menos \$3.3 billones y \$892.ooo millones en importación de gasolina extra, que fue vendida como corriente.

Pese a que a Reficar el Ministerio de Ambiente le otorgó una licencia para construir una banda cerrada que transportara directamente a los buques el material residual, en Puerto Mamonal las **corrientes de aire están llevando estos residuos a las empresas que fabrican atún Van Camp's** en la que varias operarias resultan con las manos tiznadas de coque, sustancia resultante de la refinación y con alto contenido cancerígeno.

[![INFOGRAFIA REFICAR CONTAGIO RADIO](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/INFOGRAFIA-REFICAR-CONTAGIO-RADIO.png){.aligncenter .size-full .wp-image-23647 width="1522" height="1109"}](https://archivo.contagioradio.com/reficar-la-mayor-feria-del-despilfarro-en-colombia/infografia-reficar-contagio-radio/)

<iframe src="http://co.ivoox.com/es/player_ej_11424936_2_1.html?data=kpahlJmdd5ehhpywj5eXaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncabY2M7bjajFt9XVhqigh6eVs4yhjLrAsZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
