Title: Peñalosa insiste en urbanizar la reserva más protegida y estudiada del país
Date: 2016-02-17 13:47
Category: Ambiente, Entrevistas
Tags: Enrique Peñalosa, Reserva Thomas Van der Hammen
Slug: penalosa-insiste-en-urbanizar-reserva-van-der-hemmen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Manuel-b.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: U. Andes 

###### [17 Feb 2016] 

En el marco de las actividades que se vienen realizando, para defender la Reserva Thomas Van der Hammen, este miércoles se llevó a cabo un foro en la universidad de los Andes, en el que se evidenció la importancia ecológica de la reserva y los riesgos que corre con el proyecto de urbanización que se ha planteado desde la alcaldía.

El foro: “**el futuro de la Reserva Forestal Thomas Van Der Hammen”**, contó con la participación de Julio Carrizosa, María Jimena Duzán, Manuel Rodríguez Becerra, Fernando Viviescas, Néstor Franco, Gonzalo Andrade, Francisco Cruz, y además el alcalde de Bogotá, Enrique Peñalosa quien presentó su plan de urbanización teniendo en cuenta el aumento de la densidad de la población en la ciudad.

Tras la presentación de las riquezas ambientales que tiene la reserva, Peñalosa expuso su propuesta de urbanizar el borde norte de la capital, para crear el proyecto **“Ciudad Paz”.** Allí, evidenció su claro interés en expandir la ciudad hacia una zona que de acuerdo a Manuel Rodríguez,  presidente del Foro Nacional Ambiental y exministro de Ambiente, "es un ecosistema único en el planeta".

Para el científico, las afirmaciones de Peñalosa “Son verdades a medias y un alcalde no tiene derecho a hablarle a la ciudad con verdades a medias, no me parece serio que distorsione un trabajo de 20 años por su obsesión de urbanizar el norte de la ciudad. Bogotá no va a depender de que se urbanicen esas 1.395 hectáreas”, y añadió, “**Que no se confunda a la opinión pública, los parques que anuncia el señor Peñalosa no son equivalentes a la Reserva Van der Hammen”.**

Por su parte, expertos como Julio Carrizosa, le recomendaron al alcalde hacer Ciudad paz en el centro ampliado, teniendo en cuenta que se le haría un grave daño al sistema de ordenamiento territorial del país. Carrizosa, sostuvo no entender por qué Enrique Peñalosa insiste en construir sobre las 1395 hectáreas si dice que necesita 18 mil, en cambio se generaría un grave problema ambiental para la ciudad.

Los científicos y ambientalistas se demostraron preocupados por las intenciones del alcalde, pues ese proyecto implicaría “arrasar con la reserva” que “sería uno de los lugares más maravillosos para recreación”, y el proyecto más ambiciosos de América Latina.

“**La ciencia hay que respetarla y la ciencia no puede ser manoseada, la ciencia ha dicho que esa reserva tiene todo el valor”,** señaló el presidente del Foro Nacional Ambiental, quien aseguró que durante el foro se evidenció que el alcalde "detesta y menosprecia la reserva Thomas Van der Hammen”, y concluyó que **“Peñalosa pasaría a la historia como uno de los alcaldes que le habría hecho más daño a la ciudad".**
