Title: José Arcila, líder campesino fue asesinado en Corinto
Date: 2020-07-26 18:10
Author: CtgAdm
Category: Actualidad, DDHH
Slug: jose-arcila-lider-campesino-fue-asesinado-en-corinto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/lider-campesino-asesinado.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 26 de julio en el corregimiento El Jaguar, vereda vereda La Cominera, la comunidad denunció el asesinato del líder agrario **José Gustavo Arcila Rivera.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Justiciaypazcol/status/1287514035014041601?s=19","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1287514035014041601?s=19

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El líder José Arcila , hacía parte de la Asociación Campesina del municipio de Corinto Cauca, **integraba el comité de tierras y además pertenecía a la guardia campesina del territorio**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según testigos, su muerte se dio a manos de un hombre armado que ingreso a su finca y le disparó, posteriormente fue llevado al hospital más cercano, pero **debido a la gravedad de sus heridas murió en el transcurso del recorrido.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho es motivo de investigación y aún se **desconoce sí José Arcila, tenía amenazas en su contra**, a pesar de ello las organizaciones defensoras de Derechos Humanos en el departamento del Cauca, exigieron garantías a la vida de los y las defensoras en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho más reciente registrado en Corinto, Cauca fue el 29 de mayo en donde la Asociación de Cabildos Indígenas del Norte del Cauca, reportó el asesinato de dos médicos tradicionales en el Resguardo Indígena de Páez. (Le puede interar: [Asesinan a una pareja de kiwe thë’j (médicos ancestrales) en Corinto, Cauca](https://archivo.contagioradio.com/asesina-a-una-pareja-de-kiwe-thej-medicos-ancestrales-en-corinto-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo en el informe mas reciente de Indepaz, sobre las agresiones a liderazgos en el periodo de 2016 a julio del 2020, señalan número más alto de asesinaros está en el departamento del **[Cauca](https://archivo.contagioradio.com/los-castigos-brutales-con-los-que-grupos-armados-instauran-su-propio-regimen-contra-el-covid-19/)**, con **226 casos**, seguido por [Antioquia](https://archivo.contagioradio.com/esperamos-que-en-mutata-encontremos-la-paz-excombatientes-de-ituango/) (133), [Nariño](https://archivo.contagioradio.com/la-saga-del-elefante/) (84), [Valle del Cauca](https://archivo.contagioradio.com/consejo-de-seguridad-de-la-onu-llama-a-un-cese-al-fuego-global/) (74) y [Putumayo](https://www.justiciaypazcolombia.com/plantar-arboles-como-una-accion-politica-de-amor-por-el-territorio/) (60).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organización también destacó que el número más alto de homicidios a personas líderes sociales y defensoras de Derechos Humanos, se encuentra en sector campesino, con 342 casos en los último 4 año. (También puede leer: [971 defensores de DD.HH. y líderes han sido asesinados desde la firma del Acuerdo de paz](https://archivo.contagioradio.com/971-defensores-de-dd-hh-y-lideres-han-sido-asesinados-desde-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
