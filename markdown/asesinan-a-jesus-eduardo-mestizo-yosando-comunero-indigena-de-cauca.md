Title: Asesinan a Jesús Eduardo Mestizo Yosando, comunero indígena de Toribío, Cauca
Date: 2019-11-04 14:12
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, indigena, tacueyo, Toribío
Slug: asesinan-a-jesus-eduardo-mestizo-yosando-comunero-indigena-de-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Fotos-editadas-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

En la noche del pasado domingo 3 de noviembre, el Consejo Regional Indígena del Cauca (CRIC) denunció el asesinato del comunero indígena y defensor de derechos humanos Jesús Eduardo Mestizo Yosando, en la vereda Loma Linda del municipio de Toribío, Cauca. El CRIC señaló que el mismo domingo en la noche, Arbey Noscué, coordinador de la guardia indígena de Tacueyó , sufrió un atentado en la vereda La Playa; y un guardia indígena recibió un impacto de bala en Corinto, cerca de la sede del cabildo.

### **Jesús Eduardo Mestizo Yosandó, 11 indígenas han sido asesinados esta semana**

Según la información del CRIC, el asesinato de Eduardo Mestizo ocurrió sobre las 7 de la noche, en momentos en que se disponía a viajar. El defensor de derechos humanos estaba con su esposa e hijo, cuando advirtió la presencia de extraños en la zona no obstante, Mestizo decide emprender su viaje y tras salir de su casa, en la vereda Loma Linda, dos hombres atentan contra su vida. El Consejo señaló que los hombres arrastraron el cuerpo de Mestizo hasta otro lugar, donde lo despojaron de su maleta, documentos de identidad y los celulares.

Jesús Eduardo Mestizo era comunero indígena, socio fundador de la Asociación Indígena Avelino Ui, de la cual aún era integrante, y era participante activo de los espacios de decisión comunitaria en el territorio. La Asociación de la cual es socio fundador hace parte del Proceso de Unidad Popular del Suroccidente Colombiano (PUPSOC) y de la coordinación social de Marcha Patriótica. (Le puede interesar:["No se detiene el exterminio hacia los pueblos indígenas"](https://archivo.contagioradio.com/no-se-detiene-el-exterminio-hacia-los-pueblos-indigenas/))

### **Persiste grave situación en materia de derechos humanos en Cauca**

El pasado sábado 2 de noviembre, el CRIC lamentaba la muerte de [Alex Vitonás Casamachín](https://archivo.contagioradio.com/alex-vitonas-casamachin-indigena-asesinado-en-toribio-cauca/), jóven nasa en la misma vereda donde fue asesinato Mestizo. Aunque Vitonás no tenía labores de liderazgo, su asesinato advertía nuevamente sobre una agudización de la crisis en materia de derechos humanos en el territorio. A ambos asesinatos se sumó el atentado contra el coordinador de la guardia indígena de Tacueyó Arbey Noscué, en la vereda La Playa, hecho del que logró salir ileso.

De igual forma, el CRIC denunció que en Corinto, un guardia indígena recibió un impacto de bala cerca del cabildo de la zona, en momentos en que una patrulla de Policía se encontraba a una cuadra de distancia. Por estas razones, y ante la militarización del territorio, la Organización indígena cuestionó el enfoque militarista con que se está tratando la problemática que atraviesa el territorio, y la efectividad de las Fuerzas Armadas en su misión de proteger la vida de los y las ciudadanas.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fcric.colombia%2Fposts%2F2025484274220614&amp;width=500" width="500" height="290" frameborder="0" scrolling="no"></iframe>

### **Una caravana humanitaria en solidaridad con Tacueyó**

Este domingo, el arzobispo de Cali, monseñor Darío de Jesús Monsalve viajó a Corinto para encontrarse con el arzobispo de Popayán, monseñor Luis José Rueda para realizar una minga "por la paz y por la vida en Tacueyó, Cauca". Al llegar a la zona del[atentado que cobró la vida de cinco integrantes](https://archivo.contagioradio.com/autoridades-indigenas-responsabilizan-a-duque-de-masacre-en-tacueyo/) de los pueblos indígenas, realizaron una ceremonia pidiendo que estos hechos no se vuelvan a repetir. (Le puede interesar: ["Militarizar más al Cauca es una propuesta 'desatinada y arrogante': ACIN"](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/))

> [\#MingaDeDuelo](https://twitter.com/hashtag/MingaDeDuelo?src=hash&ref_src=twsrc%5Etfw) | Desde Tacueyó, nos unimos en oración con las víctimas de la masacre perpetuada contra cinco indígenas en este territorio, para que estos actos atroces que vienen ocurriendo no se vuelvan a repetir. [@PaSocial\_Cali](https://twitter.com/PaSocial_Cali?ref_src=twsrc%5Etfw) [@CRPacifico](https://twitter.com/CRPacifico?ref_src=twsrc%5Etfw) [@OArquidiocesis](https://twitter.com/OArquidiocesis?ref_src=twsrc%5Etfw) [@arzobispodecali](https://twitter.com/arzobispodecali?ref_src=twsrc%5Etfw) [pic.twitter.com/UThXUksJZz](https://t.co/UThXUksJZz)
>
> — Arquidiócesis Cali (@Arqui\_Cali) [November 4, 2019](https://twitter.com/Arqui_Cali/status/1191401786101424130?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> [\#ImagenDelDía](https://twitter.com/hashtag/ImagenDelD%C3%ADa?src=hash&ref_src=twsrc%5Etfw) Hasta el lugar donde fue cometida la masacre de Tacueyó llegaron para honrar la vida de las mujeres y los hombres de los pueblos indígenas que han sido asesinados. [\#NoEstánSolos](https://twitter.com/hashtag/NoEst%C3%A1nSolos?src=hash&ref_src=twsrc%5Etfw) [\#MingaDeDuelo](https://twitter.com/hashtag/MingaDeDuelo?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/eI3zld6qeO](https://t.co/eI3zld6qeO)
>
> — Contagio Radio (@Contagioradio1) [November 4, 2019](https://twitter.com/Contagioradio1/status/1191421096668680192?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
**Síguenos en Facebook:**

</p>
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
