Title: TLC Colombia-Israel afecta derechos del pueblo Palestino
Date: 2017-06-07 14:42
Category: Onda Palestina
Tags: alirio uribe, Apartheid, colombia, Israel, TLC
Slug: israel-palestina-tlc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/tlc-colombia-israel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: GPO] 

###### [06 Jun 2017] 

En entrevista con Onda Palestina, el representante a la cámara por Bogotá Alirio Uribe afirmó que **el Tratado de Libre Comercio (TLC) entre Israel y Colombia**, que acaba de ser aprobado por el congreso, no solo lesiona gravemente la economía sino que **estaría violando las leyes internacionales a las que está suscrito Colombia.**

El representante advierte que este tratado viola el **artículo 93 de la Constitución Nacional** donde se ratifica que nuestro país respeta el tratado de Derechos Económicos, Sociales y Culturales que obliga que el Estado colombiano **respete la autodeterminación de los pueblos, en este caso del pueblo Palestino**.

En la intervención que hizo en el congreso la semana pasada, Uribe dejo claro que uno de los problemas más grandes es que en el tratado **no hace una clara distinción del territorio israelí**, ya que solo se dice que el tratado aplica a los territorios donde se mantiene el régimen tarifario de Israel. Eso en la práctica significa no solo Israel sino los Territorios Ocupados Palestinos, ya que el Estado ocupante mantiene el control de las fronteras de ellos.

Finalmente el representante Uribe afirma que el tratado **lesiona la economía colombiana** ya que se repite lo que con otros tratados ha pasado: realizar una competencia de iguales con países que tienen mayor capacidad tecnológica. Esto lo que genera es un **progresivo proceso de desindustrializacion en el país** y un aumento en la importación, y dependencia, de productos del mercado externo.

En esta emisión de [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/) tendremos además información sobre vi**olaciones de derechos en Gaza y Cisjordania**, las acciones de la campaña BDS en Chile y Europa y la segunda parte de nuestro informe del **TLC Colombia Israel**. También tendremos el perfil de la chef palestina Joudie Kalla y la mejor música de palestina.

<iframe id="audio_19135829" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19135829_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
