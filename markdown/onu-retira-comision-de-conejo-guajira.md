Title: ONU y FARC retiran comisión de Conejo, Guajira
Date: 2017-01-05 12:13
Category: Nacional, Paz
Tags: Conejo, FARC, Gobierno Nacional, Guajira, ONU
Slug: onu-retira-comision-de-conejo-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Colombia-Peace.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Herald] 

###### [5 Enero  2017] 

La Misión de la ONU en Colombia dio a conocer este jueves, una breve comunicación en la que señala que han retirado de la misión a 3 observadores y 1 supervisor de su organización, como medida de investigación de los hechos ocurridos el pasado 31 de diciembre, en el Conejo**. Por otro lado las FARC-EP anunción que hará lo mismo hasta tanto no conocer más detalles sobre el despido de los observadores.**

En el comunicado conocido a través de su página web la ONU, manifiestan que **luego de realizar las investigaciones del caso** sobre las circunstancias en las cuales observadores de esta organización participaron en el festejo del 31 de diciembre del 2016 en un Punto de Pre agrupamiento Temporal de las FARC-EP  **"la Misión de la ONU en Colombia ha tomado la decisión de separar de su servicio a tres observadores presentes en la ocasión y a su supervisor directo".**

La anterior decisión e investigación se da luego de la petición enviada en una carta por la embajadora de Colombia en la ONU, **María Emma Mejía**, al secretario general adjunto para Asuntos Políticos del organismo, Jeffrey Feltman, en la que solicitaron se aplicaran "correctivos".

De igual modo, **aseguraron que este hecho no significará la afectación de la Misión en verificar los compromisos de las partes** "reiteramos nuestra determinación de verificar con total imparcialidad los compromisos de las partes sobre el cese al fuego y de hostilidades y la dejación de armas" concluyen.  Le puede interesar: ["Bailando es como se acaban las guerras" comunidad de Conejo](https://archivo.contagioradio.com/bailando-es-como-se-acaban-las-guerras-comunidades-de-conejo/).

Así mismo, en un comunicado **las FARC-EP aseguró que retiran su componente del mecanismo de Monitoreo y Verificacion local del PTN de Conejo, La Guajira, hasta tanto la ONU no clarifique el despido de su personal.  
**

Y agregan "hacerle caso a un fragmento de video apócrifo descontextualizado donde se cataloga a las FARC-EP de terroristas  es altamente ofensivo para todos los asistentes a ese acto". Le puede interesar: [Entregan alimentos descompuestos a guerrilleros de las FARC en zonas de preagrupamiento](https://archivo.contagioradio.com/entregan-alimentos-descompuestos-guerrilleros-las-farc-zonas-preagrupamiento/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
