Title: Asesinatos de mujeres en Atlántico aumentaron un 45% en 2016
Date: 2017-01-08 18:15
Category: Mujer, Nacional
Tags: Atlántico, feminicidios, mujeres
Slug: asesinatos-mujeres-atlantico-aumentaron-45-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/feminicidios-e1483917108810.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: desastre.mx 

###### [8 Ene 2017] 

El departamento del Atlántico cerró el 2016 con 51 casos  de asesinatos de mujeres lo que significa  un aumento del 45% en relación con el 2015. **De ellos 12 fueron catalogados como feminicidios.**

Así lo habían alertado organizaciones defensoras de los derechos de las mujeres de esa región, quienes señalaron que durante las fiestas de fin de año la violencia machista, principalmente porque esta zona del país culturalmente sigue un patrón patriarcal, que empeora con el consumo de alcohol.

Pese a ese llamado, la última mujer asesinada fue **Yarelis María Arévalo Fontalvo**, quien el pasado 28 de diciembre, tras recibir una llamada en el barrio Rebolo, el cuerpo de la joven, de 19 años, fue encontrado en una calle del sector la Bendición de Dios con dos tiros en la cabeza que le habría propinado un parrillero de una moto.

Frente a esta situación, Sandra Vásquez Hernández, secretaria de La Mujer y Equidad de Género de la Gobernación, manifiesta que con la apertura de la oficina de la Mujer en los 22 municipios del Departamento **se fortalecerá la ruta de atención y emprenderá una campaña más fuerte en contra de las violencias contra las mujeres.**

Cabe recalcar que luego de un año de haberse promulgado la Ley Rosa Elvira Cely, a nivel nacional las condenas en los procesos abiertos por feminicidio no han sido resultados, pese a que en promedio cada día cuatro mujeres son víctimas de asesinatos en Colombia, cuyo **90% de los casos se encuentran en la total impunidad.**

Según el Instituto  de Medicina Legal **2016 tuvo un registro de más de 27 mil casos de abuso sexual** contra las mujeres en Colombia, y el número de feminicidios también aumentó, mientras en 2015 fueron 670 y **en 2016 la cifra llegó a 731.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
