Title: Los riesgos del 'Fast Track' ambiental
Date: 2017-02-22 15:16
Category: Ambiente, Nacional
Tags: Fast Track, Implementación de los Acuerdos de paz, Minambiente, Sistema Ambiental Nacional
Slug: los-riesgos-del-fast-track-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/ambiente.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ZonaCero] 

###### [22 Feb 2017] 

Menos autonomía para las Corporaciones Autónomas Regionales, modificaciones a la Ley General Ambiental para crear dos entidades nuevas y transformar la Agencia Nacional de Licencias Ambientales, una ley de pago por servicios ambientales, una ley de cambio climático y una reforma a la Ley que rige las Reservas Forestales, son propuestas que el Ministerio de Ambiente quiere poner dentro del debate del Fast Track

El Ministro Luis Gilberto Murillo ha dicho que su cartera planea realizar el envío de estas propuestas de forma progresiva dentro de los seis meses que dura el Fast Track, para la **“instrumentación legislativa de los Acuerdos de Paz en términos ambientales” y “modernizar el Sistema Ambiental Nacional”.**

### Cambios en las entidades de control y licencias 

Hasta el momento las CAR, han tenido autonomía para autorizaciones, estudios de impacto ambiental, concesiones a proyectos de extracción de recursos y delimitación de zonas de reservas o de especial cuidado, frente a ello la iniciativa de MinAmbiente busca reformar los artículos 150 y 189 de la Constitución, para **agregar a las funciones del Presidente “la de ejercer la inspección y vigilancia de la gestión ambiental,** incluido el uso de sus recursos” y cambiar la estructura administrativa y ejecutiva de las CAR.

Respecto a las nuevas entidades y cambio en la ANLA, proponen modificar la Ley 99 1993 para crear la Unidad de Planificación Ambiental, que coordinaría y ordenaría la **información para la toma de decisiones en temas ambientales** y la Unidad Administrativa Especial de Mares, cuyas funciones serían ordenar y administrar el medio marino, **“vigilar, controlar y hacer cumplir las normas para preservar la protección de tales ecosistemas”.**

### Cambio Climático, Servicios Ambientales y Reservas Forestales 

Sobre el Pago por Servicios Ambientales, que ya existe en Colombia, se ordenaría la normatividad, que no existe hasta el momento y se le asignaría recursos a la iniciativa que ha sido criticada por varias organizaciones ambientales y comunidades, puesto que en la práctica no se orienta la iniciativa hacia la conservación.

Se crearía un Sistema Nacional de Cambio Climático, el cual priorizará las medidas del país tanto en mitigación de gases de efecto invernadero **“como en la adaptación a los ya inevitables efectos del fenómeno global”**, la ley crearía el Sistema y asignaría recursos para su funcionamiento.

También se harán modificaciones a la Ley 2 de 1959, la cual organiza las Reservas Forestales, MinAmbiente buscaría incluir en la Ley de Tierras **“medidas que permitirían hacer titulaciones dentro de las Reservas para cumplir con lo acordado con las Farc”.**

Por último, frente a este anuncio, **organizaciones ambientales han advertido que harán seguimiento y veeduría a estos proyectos de ley**, para garantizar que no se hagan en contravía a los Acuerdos de La Habana, los derechos de las comunidades y el derecho ambiental.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
