Title: Madrid protege: una iniciativa para salvar vidas
Date: 2019-07-25 13:31
Author: CtgAdm
Category: Entrevistas, Líderes sociales
Tags: defensores de derechos humanos, lideres sociales, Madrid Protege, Mundubat
Slug: madrid-protege-vidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/lideres-sociales-en-españa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

El pasado miércoles fueron presentados algunos de los resultados del **programa Madrid Protege, iniciativa del Ayuntamiento de Madrid que acoge temporalmente en España a líderes y lideresas sociales** con un riesgo medio contra sus vidas. Las primeras participantes del programa fueron lideresas de Valle del Cauca, Cauca y Nariño; quienes tuvieron la oportunidad de crear redes para su propia protección y la de sus comunidades.

### **En 2018 en Colombia fueron asesinados el 35% de los defensores de DD.HH. del mundo**

El **concejal Ignacio Muri,** que fue invitado a la presentación del Programa, indicó que Colombia fue elegida para desarrollar este proyecto en vista de que el país fue declarado zona de alerta especial en materia de derechos humanos por la relatora para este aspecto de las Naciones Unidas, Michelle Bachelet. A ello se suma que en 2018, de los 321 asesinatos de defensores y defensoras de derechos humanos en el mundo, 110 fueron en Colombia (Le puede interesar: ["Mientras Duque daba un discurso en el Congreso, 3 l´dieres fueron asesinados en Colombia"](https://archivo.contagioradio.com/duque-lideres-asesinados-colombia/))

Como relató Muri, a ello se sumó la preocupación ante el aumento de un 60% en los asesinatos contra líderes y lideresas después de la firma del Acuerdo de Paz, respecto a 2015. Ello motivó a que el Programa propusiera cuatro objetivos esenciales para apoyar el trabajo de los y las defensoras en Colombia: Protección temporal, formación, visibilización de su trabajo y la creación de redes de alianzas. (Le puede interesar: ["Durante tres meses Madrid acogerá a defensores de derechos humanos amenazados"](https://archivo.contagioradio.com/durante-3-meses-madrid-acogera-a-lideres-sociales-amenazados/))

### **Los cuatro objetivos del programa Madrid Protege  
**

**Raúl Rojas, Coordinador del Programa Madrid Protege**, resaltó que "es ejemplo para todas las sociedades que quieren construir paz y democracia el gran número de líderes y lideresas sociales que siguen haciendo frente todos los días" en los territorios a los problemas que enfrentan sus comunidades. Por eso, explicó que el objetivo del Programa es crear un tejido a largo plazo para que sus participantes cuenten con herramientas para seguir desarrollando su trabajo.

En ese sentido, el programa plantea cuatro objetivos. El primero la protección inmediata de los y las lideresas escogidas para tener una estadía de tres meses en diferentes lugares de España. Las elegidas tienen un riesgo medio, de tal forma que el programa de protección temporal sirvió para reducir el riesgo contra sus vidas, pero no las apartó mucho tiempo de su comunidad; permitiendo así que continúen unidas a sus comunidades y sus luchas.

El segundo objetivo plantea la evaluación de necesidades de formación, para que las líderes escogidas regresen a sus comunidades con más herramientas para cumplir sus objetivos. Sobre ello, Rojas aclaró que ellas recibieron formación en protección para sí mismas y sus comunidades; en comunicación política; y en redes sociales, para comunicar su trabajo. (Le puede interesar: ["Asciende a 10 la cifra de líderes del Paro Cívico de Buenaventura amenazados"](https://archivo.contagioradio.com/asciende-a-10-la-cifra-de-lideres-del-paro-civico-de-buenaventura-amenazados/))

En cuanto al tercer objetivo, el Director del[Programa](https://www.madridprotege.org/) afirmó que buscaron sensibilizar y visibilizar a la sociedad española sobre la situación en Colombia mediante una agenda de encuentros con medios de comunicación ibéricos. (Le puede interesar: ["Comunidad internacional está cargando, en solitario, la implementación del Acuerdo de Paz"](https://archivo.contagioradio.com/comunidad-internacional-acuerdo-paz/))

Y en el último punto, el más importe, Rojas destacó que buscaron que las lideresas tuvieran una agenda de incidencia social y política  para crear una red de alianzas. Así, las defensoras de derechos humanos **tendrán toda una 'red' de contactos en la comunidad internacional a la que pueden acudir si sus comunidades o sus vidas presentan algún riesgo.**

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38994047" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38994047_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
