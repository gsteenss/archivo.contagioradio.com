Title: Cinco argumentos de la ciudadanía para modificar la ley TIC
Date: 2019-08-23 11:54
Author: CtgAdm
Category: Entrevistas, Política
Tags: FLIP fundación para libertad de Prensa, Fundación Karisma, LeyTIC, MinTIC
Slug: ley-tic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/ley-tic.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@Coljuristas] 

En un trabajo mancomunado  de  la **Fundación Karisma**, la **Fundación para  la Libertad de Prensa**  (FLIP) y la **Comisión Colombiana de Juristas** (CCJ), se radicó en la Corte Constitucional una demanda en contra de la Ley 1978 de 2019, mas conocida como **Ley TIC.**  La demanda busca defender la libertad de expresión de los ciudadanos, su **derecho a la libre conectividad** y el consumo de contenido.

El documento presentado se hace bajo el uso  de la demanda por inconstitucionalidad, un acto que permite  la modificación de algunas leyes bajo la demanda ciudadana; fundamentando esto con diferentes artículos de la **Constitución** que **destacan la libertad de expresión y el estado de derecho y democracia**. Uno de los puntos que destacó  la CCJ  dentro de la demanda es el uso del espectro de electromagnético. (?)..

### Puntos de la demanda 

El primero de los argumentos se refiere a la **autonomía,** es decir, permitir un control equilibrado de la **Comisión de Regulación de Comunicaciones (CRC)**, donde todo el manejo de las comunicaciones se pueda ejecutar según las diferentes necesidades, lo cual a su vez genera los **punto dos y tres**, que hacen referencia al control que tendrían el **Ministerio de las TIC** y la **rama ejecutiva** , sobre el contenido, los canales de difusión y los recursos. (Le puede interesar:[Priorizar al sector privado, censura y control estatal: los cuestionamientos a la ley TIC](https://archivo.contagioradio.com/priorizar-al-sector-privado-censura-y-control-estatal-los-cuestionamientos-a-la-ley-tic/))

El **punto cuatro** hace referencia a las **redes comunitarias** las cuales se verían afectadas por impuestos y normativas que alejarían a muchas comunidades rurales de abrir sus redes de comunicación; finalmente el punto quinto para hacer oposición a la Ley TIC,  aborda una consulta previa donde se incluyan comunidades afro, indigenas y raizales dentro del espectro electromagnético y no se pase por alto su participación como sucedió en el pasado.

### **Consecuencias de la Ley TIC** 

Para **Carolina Botero, Directora de la Fundación Karisma** , "el trasladar todo a una sola rama de  poder en este caso la  ejecutiva, genera la creación de un súper poder, que a su  vez crea un desbalance", por ello se debe apelar y solicitar una distribución de funciones dentro de la Ley TIC, donde se permita la participación de diferentes voceros ciudadanos y esto se vea reflejado en contenido distinto y no a favor de propaganda política.

Así mismo Botero recalca que una ley es a fin de cuentas un marco, y propone las *reglas de juego,* y si no se tienen claras y justas para todas las partes, el que se verá afectado es el que tenga menos poder, en este caso los ciudadanos, lo que compromete directamente la libertad de expresión.

Para Botero esta demanda sobre la Ley TIC radica en proyectarse, "no estamos diciendo que todos mañana vayan a ser censurados, sino que queremos generar un marco jurídico fuerte, con controles y peso, y así evitar en un futuro que el poder  absoluto recaiga sobre los mismo".

El ideal de esta demanda es pensar en el futuro, evitar el cierre de entidades como la  **Autoridad Nacional de Televisión** (ANTV), e impedir el monopolio mediático donde el contenido se rija bajo un solo interés; los documentos ya están radicados y será la Corte Constitucional quien tendrá que estudiar y dar aprobación a la demanda.

<iframe id="audio_40562019" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40562019_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
