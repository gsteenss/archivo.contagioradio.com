Title: En la Universidad Distrital ganó la inconformidad con el voto en blanco
Date: 2016-09-02 15:20
Category: Educación, Nacional
Tags: crisis universidad pública Colombia, paro universidad distital, Universidad Distrital
Slug: en-vilo-la-rectoria-de-la-udistrital-tras-el-triunfo-del-voto-en-blanco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Elecciones-UDistrital.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UDistrital ] 

###### [2 Sept 2016 ] 

En los próximos días el Consejo Superior de la Universidad Distrital deberá definir el **plan de acción frente a la interinidad** en la que queda la rectoría de la institución, luego de que seis de los nueve integrantes del máximo órgano de dirección votaran por el voto en blanco que también había obtenido la mayoría de sufragios en las elecciones celebradas el pasado 23 de agosto, con la participación de estudiantes, docentes, administrativos y egresados.

Según Johana Mendoza, representante estudiantil ante el Consejo Superior, **ninguno de los ocho candidatos respondía a los intereses de la comunidad universitaria**, por lo que la mayoría de los estudiantes acordaron favorecer el voto en blanco en las elecciones en las que pese a que el potencial de votantes era de 82.000 personas sólo votaron 13.380.

Mendoza asegura que haber logrado el proceso de elecciones es la victoria resultante de los dos meses en los que estuvieron [[en el paro](https://archivo.contagioradio.com/tras-60-dias-de-paro-estudiantes-de-la-udistrital-vuelven-a-clases/)]en el que [[falleció el estudiante Miguel Ángel Barbosa](https://archivo.contagioradio.com/dictamen-de-medicina-legal-confirmaria-que-miguel-angel-barbosa-fue-asesinado/)], tras ser agredido por miembros del ESMAD. Pese a que no se logró que fuera derogado el [[acuerdo 001](https://archivo.contagioradio.com/movimiento-estudiantil-de-la-udistrital-no-cedera-ante-presiones-del-consejo-superior/)], que contempla la elección antidemocrática del rector, lo que los estudiantes esperan es que sea **aprobada la Constituyente universitaria que desde hace 28 años se viene presentando ante el Consejo Superior**.

En la Universidad Distrital estudian cerca de 16 mil estudiantes con un presupuesto anual de \$285 mil millones, 70% de transferencia directa del distrito, financiación que ha sido **objeto de malos manejos por parte de las últimas administraciones**, que han dejado al [[claustro con un déficit](https://archivo.contagioradio.com/se-agudiza-la-crisis-en-las-universidades-publicas-colombianas/)] de más de \$25 mil millones.

<iframe src="http://co.ivoox.com/es/player_ej_12759168_2_1.html?data=kpekl56Vepmhhpywj5aUaZS1kZ6ah5yncZOhhpywj5WRaZi3jpWah5yncavjycbbw5Cxqc_Y0N_Oh5enb7afpc7g1tfNuMLgjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio [[en su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio ](http://bit.ly/1ICYhVU)]] 
