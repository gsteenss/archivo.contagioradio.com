Title: Con panfletos amenazan a líderes estudiantiles en Universidad de Antioquia
Date: 2019-05-21 14:53
Author: CtgAdm
Category: DDHH, Estudiantes
Tags: Aguilas Negras, Amenazas, Panfletos, Universidad de Antioquia
Slug: con-panfletos-amenazan-a-lideres-estudiantiles-en-universidad-de-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-1-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Desde el pasado 20 de mayo aparecieron panfletos amenazantes provenientes del Bloque Universidad de Antioquia Águilas Negras, en el campus de la Institución; en los que se menciona a seis líderes estudiantiles,  exigiéndoles no matricularse nuevamente. Para las organizaciones estudiantiles, la aparición de este panfleto resulta preocupante porque están los nombres con apellidos de estudiantes, y se presenta en un contexto de estigmatización al movimiento social.

Cristian Guzmán, vocero nacional de la Unión Nacional de Estudiantes de Educación Superior (UNEES), explicó que los panfletos que aparecieron en todo el campus de la Universidad tienen los nombres completos de los representantes estudiantiles; "entonces significa que se tomaron el esfuerzo de saber quiénes son los que están liderando" los movimientos en la Institución. (Le puede interesar: ["¿Es la Brigada Nacional 18 el fortalecimiento del paramilitarismo urbano en Medellín?"](https://archivo.contagioradio.com/es-la-brigada-nacional-18-el-fortalecimiento-del-paramilitarismo-urbano-en-medellin/))

Adicionalmente, Guzmán recordó que recientemente el gobernador de Antioquia, Luis Pérez, pidió apoyo a la Universidad para desestructurar "células terroristas" al interior de las universidades; siguiendo la línea marcada por el ex fiscal Néstor Humberto Martínez, cuando señaló la presunta presencia de grupos terroristas en los espacios académicos. El panfleto señala que los líderes estarían relacionados también con grupos guerrilleros, lo que agravaría la estigmatización y persecución contra el movimiento social.

### **La comunidad estudiantil responde con unión, las directivas** 

Las organizaciones estudiantiles entienden que la amenaza no es solo para uno de los líderes, sino que busca desarticular al movimiento con el miedo; por eso este martes se reunirán los diferentes movimientos de las universidades en el Valle de Aburrá para analizar la situación y responder a ella. Por su parte, los profesores hicieron un llamado a la unión y apoyo entre colectivos para enfrentar a los violentos.

Sin embargo, el líder estudiantil sostuvo que las directivas de la Universidad se limitaron a decir que la amenaza era un tema que correspondía a las autoridades de la ciudad; mientras que estas no se han referido a la situación. (Le puede interesar: ["Con panfletos y carteles amenazan a estudiantes de la U. de Antioquia"](https://archivo.contagioradio.com/con-panfletos-y-volantes-amenazan-a-estudiantes-de-la-u-de-antioquia/)) y contempló una posible reunión nacional para el próximo mes de junio en la que se decidirían mecanismos para hacer frente a estas acciones.

<iframe id="doc_23586" class="scribd_iframe_embed" title="Denuncia sobre Panfletos en la Universidad de Antioquia" src="https://es.scribd.com/embeds/410992814/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-TEq1TivsL7aJs0QmKB89&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.707221350078493"></iframe>

<iframe id="audio_36160331" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36160331_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
