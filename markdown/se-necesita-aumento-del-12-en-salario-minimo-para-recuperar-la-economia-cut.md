Title: Se necesita aumento del 12% en salario mínimo para recuperar la economía: CUT
Date: 2017-12-06 08:38
Category: Economía, Nacional
Tags: CUT, Fabio Arias, salario minimo
Slug: se-necesita-aumento-del-12-en-salario-minimo-para-recuperar-la-economia-cut
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/grantomabogotacut5-e1496778380732.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [05 Dic 2017] 

Inicio el debate sobre el ajuste del salario mínimo entre el gobierno y sectores de la sociedad, en donde ya se han esbozado diferentes propuestas; como reajustar el salario de acuerdo a las regiones y su productividad, para la Central Unitaria de Trabajadores el aumento debe ser del **12% para que se salde la deuda que hay con los trabajadores del país en términos de las brechas salariales y los altos costos de vida**.

### **Salario de acuerdo a la producción del departamento** 

Esta primera propuesta para Fabio Arias, integrante de la Central Unitaria de Trabajadores, es retroceder a los años 80 en donde había un salario para la ruralidad y otro urbano que después de luchas de años se logró unificar.

“Hacer una diferenciación sobre una condición mínima, en donde siempre hay un sector más desprotegido que otro, nos parece que no es bueno ni positivo para el país, y volver a retrotraer esa discusión al 2017, **más de 30 años después, es un absurdo desde el punto de vista económico y social**” afirmó Arias.

Además, señaló que esa propuesta proviene de la OCDE, con un claro interés de disminuir los ingresos de los trabajadores, una situación que para Arias es “inaceptable”. (Le puede interesar:["Salario mínimo cada vez más mínimo en Colombia"](https://archivo.contagioradio.com/salario-minimo-cada-vez-mas-minimo-en-colombia/))

### **Aumento del salario mínimo sobre la inflación** 

El año pasado el salario mínimo estuvo apenas por encima de la inflación, esta podría ser el mismo método que se establezca este año para el alza, sin embargo, ya hay una primera propuesta de un aumento del 4.5%. Por su parte el consejo gremial propone que sea mucho más alto, de lo contrario el consumo de las familias colombianas va a seguir disminuyendo y que para que se equilibre **con la inflación tendría que ser aumentado en un 12% que se ha venido acumulando en el tiempo**.

"Una familia mínimo debe tener el equivalente de dos salarios mínimos, entonces estamos resueltos a que en esta familias los papas deban trabajar siempre, eso implica que no hay vida, el salario mínimo de **Colombia es uno de los más bajos en Latinoamérica**" señaló Arias.

### **La fuerza de los sindicatos para el 2018** 

Arias manifestó que el contexto que ahora se debe tener en cuenta es si hay fuerza desde los sindicatos para afrontar la batalla por el reajuste salarial y el inició de las campañas electorales, “**la cancha para hacer definiciones políticas está agotada, y ahora todo el mundo está interesado en sus campañas electorales”.**

De igual forma expresó que la elección del 2018 a la presidencia y Congreso también definirá si se continúa o no con regresiones a los derechos laborales, “los trabajadores tenemos que pensar que en esta coyuntura política no podemos estar celebrando los mismos con las mismas, sobre todo las acciones de personas como Vargas Lleras y el Uribismo”, en ese sentido señaló que, **durante los 8 años de Uribe en el poder, no se alcanzaron mejoras significativas para los trabajadores**.

<iframe id="audio_22506178" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22506178_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
