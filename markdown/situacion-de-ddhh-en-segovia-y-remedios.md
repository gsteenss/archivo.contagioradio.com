Title: Informe ratifica grave situación de DDHH en Segovia y Remedios, Antioquia
Date: 2017-08-18 15:20
Category: DDHH, Nacional
Tags: Cahucopana, ESMAD, paro minero, Remedios, Segovia
Slug: situacion-de-ddhh-en-segovia-y-remedios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/20800058_1293732307404404_1432483080181276138_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad de Segovia] 

###### [18 Ago. 2017] 

La **Corporación Acción Humanitaria por la Convivencia y la Paz del Nordeste Antioqueño (Cahucopana)** dio a conocer un informe de DD.HH. en el que muestran cómo en los 30 días de Paro que completan los integrantes de la Mesa Minera en Segovia  y Remedios en Antioquia, se han violado de manera sistemática los derechos humanos y se ha criminalizado la protesta.

### **Violaciones a los derechos humanos** 

Según Cahucopana, basados en la información entregada por la Mesa Minera, se conocieron casos en los que la Fuerza Pública, en cabeza del Escuadrón Móvil Antidisturbios – ESMAD – han arribado a la región por orden del Gobernador de Antioquia, quienes **fuertemente armados, atrincherados en los techos de las casas, con gases lacrimógenos han atacado a la población.**

“Hasta el día 14 de agosto se habían registrado once heridos en Segovia y dieciséis heridos en Remedios. El día 15 de agosto, los cuerpos de salud y la Mesa Minera informan que, hasta 4 p.m., se presentaron cinco heridos leves, con **lesiones ocasionadas por balas de cristal, balas de goma y con material de piedra caliza,** entre ellos el señor Jorge Cañas, quien resultó lesionado en el pómulo y detrás de la oreja” manifiestan en el informe.

### **Detenciones arbitrarias** 

En el informe se registra que el 14 de agosto se realizaron 3 capturas por parte de la Policía Nacional “sin estar con ningún tipo de artefacto no permitido o realizando actividad alguna que se encuentre tipificada en la normatividad penal”. Le puede interesar: ["Mineros de Remedios y Segovia cumpletan un mes en paro"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-completan-un-mes-en-paro/)

Además, **la comunidad ha denunciado que integrantes del ESMAD han ofrecido beneficios penales a las personas que son detenidas** y dinero a los jóvenes de la comunidad “para que proporcionen información sobre los encargados de las marchas y las protestas.

### **Dos personas han sido asesinadas en un mes de Paro** 

**Además de los heridos, durante este paro han sido asesinados 2 jovenes,** Hernán Tobón Meneses en el municipio de Segovia, quien perdió su vida delante de su padre, luego de que dos hombres encapuchados que se movilizaba en una moto le dispararan.

Otro de los jóvenes que fue asesinado es Brandon Ochoa de 18 años de edad, que según la comunidad fue asesinado por francotiradores que se atrincheran en las casas del lugar.

### **Estigmatización al derecho a la protesta** 

En el informe, dice Cahucopana que la institucionalidad estatal municipal, departamental y nacional, los ha culpado de incitar a la protesta y a la violencia, incluso en espacios formales donde se han reunido a dialogar para solucionar las problemáticas que los llevaron a entrar en paro. Le puede interesar: [Continúa paro de mineros en Remedios y Segovia pese a la represión](https://archivo.contagioradio.com/continua-paro-de-mineros-en-remedios-y-segovia-pese-a-la-represion/)

“En espacios de interlocución entre la Alcaldía de Remedios y la Mesa Minera, **la institucionalidad solo le presentó a esta última las supuestas infracciones y errores** que se han cometido durante los días que van de manifestación, sin proponer ningún tipo de solución ni reflejar voluntad para llegar a las vías del diálogo y la concertación”.

### **Exigencias de la mesa minera** 

En el informe Cahucopana dice que la mesa minera de los municipios de Remedios y Segovia exige al Estado y a sus instituciones de nivel municipal, departamental y nacional, retirar de las cabeceras municipales a los miembros del ESMAD. Le puede interesar: [Policía usa francotiradores para reprimir mineros en Remedios y Segovia](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-denuncian-presencia-de-francotiradores-en-el-paro/)

Además, **exigen al ESMAD no utilizar los hospitales, colegios, coliseos y demás espacios públicos, ni las casas de la comunidad para refugiarse** y llevar a cabo los ataques en contra de los manifestantes. Ni usar a la población como escudos humanos.

Por último, hacen un llamado al Estado colombiano a cumplir con los tratados internacionales en materia de Derechos Humanos “dando cumplimiento a las obligaciones estatales en el marco del Derecho Internacional de los Derechos Humanos, invitándolos finalmente a buscar una salida concertada y basada en el diálogo”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
