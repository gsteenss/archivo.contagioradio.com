Title: Desplazamiento y acaparamiento: el precio del aguacate en Caldas
Date: 2020-11-19 20:53
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: aguacate hass, Caldas, Monocultivos
Slug: __trashed-5
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/image.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/7f5d0aeb-db96-413f-a5ce-a17e484482a6.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/WhatsApp-Image-2020-11-18-at-4.21.14-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: aguacate hass / Archivo particular

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Comunidades de los municipios de **Pacora, Aranzazu, Villa María, Aguadas, Salamina y Manizales**. denuncian que el incremento de producción de aguacate hass en zonas productoras de agua, de conservación ambiental y de alta fragilidad ecosistémica está alterando las condiciones del territorio y ocasionando afectaciones sobre la población por desplazamiento, acaparamiento de tierras y su extranjerización.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**José Humberto Gallego de la facultad de ciencias agropecuarias y director del Jardín Botánico de la [Universidad de Caldas](https://www.ucaldas.edu.co/portal/)** explica que desde el 2018 se viene implementando en el sector de Caldas, Risaralda, Quindío y Antioquia una propuesta de cultivos a gran escala apoyada por las gobernaciones y la Secretaría de Agricultura "fomentando el agroextractivismo, los monocultivos y una propuesta agroexportadora". [(Lea también: En Colombia una comunidad campesina ha luchado más de 40 años por el derecho a la tierra)](https://archivo.contagioradio.com/el-garzal-una-comunidad-campesina-ha-luchado-mas-de-40-anos-por-el-derecho-a-la-tierra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Resultado de esta propuesta, se han implementado grandes cantidades de cultivos de aguacate Hass en espacios altamente frágiles en zonas por encima de los 2.000 metros sobre el nivel del mar a lo largo de al menos 2.000 hectáreas; lo que no solo ha generado alteraciones sobre los causes de agua, sino acaparamiento de tierras, desplazamientos y conflictos con relación a grandes latifundios en zonas históricamente cafeteras que han pertenecido a pequeños productores.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras la producción de un solo kilo de aguacate requiere cerca de 2.000 litros de agua según la organización holandesa Footprint Network; **Carolina Ocampo, representante de las comunidades campesinas de Neira, Caldas** relata que pese a ser un territorio rico en recursos hídrico, son al menos 1.000 familias las perjudicadas a raíz de las operaciones que realiza la empresa Wakate al realizar carreteras al interior de las zonas que compraron y que contaminan el agua con la tierra que se remueve y con pesticidas, afectando, tan solo en Neira a 17 veredas y 3 sectores.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Lo que estamos viendo es una intención de acaparamiento y de extranjerización y es que hablar de 14 empresas que se apoderaron de más de 14.000 hectáreas de esta zona ya dice muchas cosas" afirma el docente. Algunas de estas empresas son Camposol, o Wacate que exportan principalmente a países como **Reino Unido, España, Bélgica y Países Bajos junto a otros como Estados Unidos, Aruba, Costa Rica, Chile, Ecuador, Panamá y Argentina.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Se ha evidenciado un manejo ambiental pésimo de parte de esta empresa, y estamos hablando de casi 2.000 hectáreas donde se hace un cambio del uso del suelo muy brusco, deforestan, tumban árboles, además hacen carreteras muy largas para sacar sus productos sin tener en cuenta cuencas e hilos hidrográficos que alimentan y abastecen a la comunidad", relata la habitante de Neira.

<!-- /wp:paragraph -->

<!-- wp:image {"id":93094,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/7f5d0aeb-db96-413f-a5ce-a17e484482a6-1024x768.jpg){.wp-image-93094}  

<figcaption>
Neira, Caldas y zonas de producción de aguacate hass

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

**Paula Valencia, campesina y comerciante de Neira, Caldas** explica que las afectaciones no solo afectan el consumo del agua de comunidades y animales que habitan en la región sino que el mismo proceso de producción de café se ve detenido pues el agua sucia impide limpiar la cosecha

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Revista Forbes, hasta mayo de 2020, Colombia ha exportado 30.344 toneladas al exterior con un valor de \$61 millones de dólares. "Dicen que pagan supuestamente mejor, pero no logro entender el beneficio para el campesino y sí hay una mayor ganancia no veo muy lógico elegir más dinero a costa de nuestra agua", cuestiona Valencia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Estamos bajo un modelo que violenta las condiciones dignas del campo colombiano"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El profesor advierte que este modelo de producción es auspiciado por la misma institucionalidad, "vemos con el Ministerio de Agricultura, las mismas Corporaciones Autónomas Regionales (CAR) están a favor de lo que ellos llaman desarrollo con propuestas netamente extractivistas". [(Le recomendamos leer: Aguacate hass ¿una amenaza para el paisaje cafetero?)](https://archivo.contagioradio.com/aguacate-hass-amenaza-paisaje-cafetero/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Carolina expresa que otra de sus preocupaciones es el desinterés que se ha visto no solo en las instituciones sino en la misma comunidad pese a los llamados que se han hecho sobre lo que pasaría con las casi 5.000 personas que podrían quedar sin agua: "las personas dicen pero entonces por qué la gente vende las tierras que están comprando diferentes multinacionales, pero venden porque el campesino está desamparado, estamos en un país en el que prefieren importar, que lograr que los campesinos produzcan y vendan a un buen precio"

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Gallego denuncia que incluso en algunos de estos municipios ya no se pueden recorrer los senderos y las mismas empresas se reservan el derecho de admisión a la zona, de igual forma, el desplazamiento en las veredas se da porque a raíz de la venta de predios, estas ya están quedando solas, mientras los pocos que se quedan se sienten aislados y con presión social por no vender.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a la tierra hay dos temas, el campesino que no quiere vender y el que está esperando a que sus tierras se valoricen más, (...) las afectaciones son muy parecidas en todos los municipios, se rompe el tejido social, los que trabajan el aguacate ven mal a las personas que están haciendo veeduría y hay muchas personas que se van del territorio y se rompe ese tejido", explica **Federico Ospina, campesino afectado por esta misma problemática del municipio de Salamina.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Paula señala que no solo se trata del problema ambiental, en la actualidad muchas personas tienen miedo a las represalias que puedan ocurrir contra aquellos que defienden su territorio, además del rompimiento del tejido social y la diferencia de opiniones que existe frente al tema y que temen pueda ser igual al escenario de otras regiones del continente como en Michuacán, México donde se han llegado a quemar amplias zonas de bosque para que su producción aumente impactando la vida silvestre de la zona en 690 hectáreas entre 2000 y 2010.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una situación similar se ve en Chile donde desde la década de los 90 se ve un deterioro en las fuentes hídricas, en lugares como la provincia de Petorca en la región de Valparaíso afectando a cerca de 400.000 personas, cabe señalar que entre 2017-2018, el país produjo cerca de 225.000 toneladas de esta fruta en una tierra en la que existen comunidades con una carencia de agua potable cercana al 80% según el informe “Sequía en Chile: la brecha más profunda”. [(Lea también: Interéses empresariales, los principales deforestadores de Colombia)](https://archivo.contagioradio.com/intereses-empresariales-los-principales-deforestadores-de-colombia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La solución no es dejar de cosechar aguacate"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El profesor José Humberto Gallego explica que las comunidades han acudido a la **resolución 0464 del diciembre de 2017** que establece la necesidad de fortalecer los sistemas productivos de agricultura familiar con un enfoque territorial que mejore su sostenibilidad y el buen vivir de la población rural para hacer frente a las problemáticas que vive la región

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Resalta además que al interior de las veredas se ha logrado la creación de veedurías ciudadanías, e incluso se ha logrado que algunas de las empresas desenterrara lo que ya había sido plantado. Pese a ello y a las iniciativas que se han generado desde las bases comunitarias y campesinas, estas no han sido lo suficientemente visibilizadas para generar incidencia política y dar cuenta de cómo la situación en el sector rural no ha mejorado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de esta situación, los campesinos coinciden en que la solución no es dejar de vender aguacate, sino que exista una verdadera igualdad entre empresas y los pequeños productores de la región. [(Le puede interesar: "Van por nuestras tierras": Informe entregado a la JEP sobre despojos en Urabá)](https://archivo.contagioradio.com/van-por-nuestras-tierras-informe/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":93099,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Comunidades de municipios de Caldas denuncian afectaciones al agua derivada del accionar de empresas de aguacate hass en la región](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/WhatsApp-Image-2020-11-18-at-4.21.14-PM-1024x576.jpeg){.wp-image-93099}  

<figcaption>
Movilización en contra de la contaminación del agua en Neira

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
