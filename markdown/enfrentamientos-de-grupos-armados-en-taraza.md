Title: Aumenta control por parte de grupos ilegales en Tarazá Antioquia
Date: 2019-09-30 17:34
Author: CtgAdm
Category: DDHH, Nacional
Tags: Antioquia, grupos ilegales, heridos, muertes, Tarazá
Slug: enfrentamientos-de-grupos-armados-en-taraza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Diseño-sin-título-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivopersonal] 

 

Habitantes del corregimiento La Caucana, sector rural del municipio de Tarazá denunciaron este lunes un enfrentamiento entre hombres armados, que dejaron un saldo de 6 personas heridas y 3 fallecidas. Las víctimas fatales  fueron identificadas como Alberto Armando Sanchez  promotor rural de 21 años, Fernando Salcedo técnico agropecuario de 27 años, y Cristian Sánchez  también de 27 años. Todos trabajaban como técnicos del programa Nacional de Sustitución de cultivos de uso ilícito, y hacían parte de la Asociación de Caucateros del municipio.

(Le  puede interesar:[Antioquia, segundo departamento más peligroso para la defensa de DD.HH.](https://archivo.contagioradio.com/antioquia-segundo-departamento-mas-peligroso-para-la-defensa-de-dd-hh/))

Según Oscar Yesid Zapata, integrante de la Coordinación Colombia Europa Estados Unidos en el nodo Antioquia, en Tarazá las personas involucradas en la sustitución de cultivos de uso ilícito se vuelven blancos de  grupos armados como *Los Caparros* o las *Autodefensas Gaitanistas de Colombia*, "no es coincidencia los ataques a estas personas, esto es algo sistemático y aún  más cuando hay una intención permanente por parte de estos grupos, hacia el  narcotráfico  como fuente de financiación y otras formas de control territorial", afirmó Zapata.

Por otro lado, Zapata afirmó que esta es una zona de constantes enfrentamientos entre grupos al margen de la ley, debido  a que es  un punto clave para las rutas del narcotráfico y esta es una zona de fácil conexión con otros departamentos que limitan con  Antioquia; así mismo Zapata resaltó que aquellos lugares donde no se ven ataques es  porque están protegidas, ya sea por pactos informales o porque son lugares donde se están consolidando estructuras de poder , como lo es el  Valle de Aburrá.

Algunos de los heridos se recuperan en hospitales del Bajo Cauca, entre ellos una mujer y una menor de 10 años, el resto de heridos fueron remitidos a centros de salud en Medellín, Zapata por último resaltó que estos hechos ocurrieron en zona rural apartada de municipio de Tarazá,  por lo tanto el acceso de autoridades y atención medica ha sido difícil.

(Le puede interesar:[Colombia pierde a un líder &\#171;innato&\#187;: Asesinan a Norberto Jaramillo en Tarazá](https://archivo.contagioradio.com/asesinan-norberto-jaramillo-taraza/))

 

 

<iframe id="audio_42417916" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42417916_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
