Title: Salario mínimo en Colombia aumentó $30.723 para el 2021
Date: 2020-12-29 08:51
Author: CtgAdm
Category: Actualidad, Economía, Nacional
Slug: salario-mnimo-aumento-treintamil-pesos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Salario-mínimo-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

[foto: archivo]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El anuncio del aumento del salario mínimo lo hizo el presidente Duque en la mañana de este 29 de Diciembre. La cifra que estaba en \$877.803 llegó a los \$908.526 para un aumento de \$30.723 lo que equivale al 3.5%. Centrales obreras se pronunciaron con preocupación pues el 2021 puede ser un año muy difícil para los trabajadores formales teniendo en cuenta los estragos del COVID 19 en la economía familiar de los colombianos que verán incrementados sus ingresos en cerca de \$1.000 diarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde que se iniciaron las negociaciones por el SMLV entre empresarios y centrales obreras y siendo fiel a sus políticas de gobierno, Duque se había mantenido más cerca de la postura de empresarios que ofrecían un aumento del 2.7%, mientras que los trabajadores representados en centrales obreras como la CGT o la CUT habían manifestado que para sostener la economía y garantizar el poder adquisitivo de las familias el [aumento debería rondar el %14](https://archivo.contagioradio.com/un-aumento-decente-del-salario-minimo-tendria-que-ser-del-14/).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Salario mínimo no cubre aumentos en costos de servicios esenciales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las grandes preocupaciones de las familias colombianas, expresadas en las movilizaciones sociales de este año que termina es la debilidad de su economía, dado que los servicios básicos como salud, educación, agua, luz y gas, muchos de ellos en manos de privados casi siempre tienen un aumento anual cercano al 5%, sin contar los servicios de transporte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente la informalidad en Colombia, que según el DANE, en el trimestre de agosto a octubre estuvo en el 47%. Esto significa que un aumento en el salario mínimo formal no beneficia a cerca de la mitad de la población pues las reglas de la economía informal son diferentes y no dependen de la oficialidad sino de las dinámicas del poder adquisitivo de la gente de a pie.[Lea también: Aumento del SMLV fue una cachetada para los trabajadores](https://archivo.contagioradio.com/cachetada-del-gobierno-a-trabajadores-con-aumento-del-6-al-salario-minimo-en-2020/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Salario mínimo y desempleo, una relación poco favorable para la gran mayoría de la gente

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Dane informó que el desempleo en Colombia durante octubre de 2020 fue del 14,7%, siendo la última medición oficial, y teniendo en cuenta que esta es solamente la cifra de trabajos formales y no cobija a la población que vive en la informalidad, esta tasa es realmente preocupante pues es muy alta y no mide las cifras de la desocupación informal que evidentemente fue víctima del COVID 19 y las medidas de aislamiento obligatorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ya desde el congreso se han escucha algunas voces de protesta como la del Senador Gustavo Bolívar quien afirmó que es crítica la brecha entre los altos salarios, entre ellos el de congresistas, y el salario mínimo.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/GustavoBolivar/status/1343897311399862272?s=20","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/GustavoBolivar/status/1343897311399862272?s=20

</div>

</figure>
<!-- /wp:embed -->
