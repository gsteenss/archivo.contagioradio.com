Title: Reivindicaciones por la libertad femenina que inspiraron a Violeta Parra
Date: 2019-02-06 15:51
Author: AdminContagio
Category: Cultura
Tags: Chile, feminismo, nueva canción, Violeta Parra
Slug: violeta-parra-liberacion-femenina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Violeta-Parra-990x683-900x600-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La tercera 

###### 5 Feb 2019 

A sus 49 años la artista chilena Violeta Parra decidió partir de este mundo, dejando un grandes himnos para el folclore latinoamericano como "Gracias a la Vida" y otras poderosas composiciones musicales convertidas en grandes obras políticas, en las que incluso abordaba temas adelantados para su época.

Violeta, una mujer profundamente ligada a la política y consiente de las situaciones sociales de su nación, transgredió desde sus canciones los estereotipos de su época y roles establecidos para las mujeres. Gracias a algunas de sus canciones se cimentaron las bases de la Nueva Canción Chilena, el movimiento musical que emergió en torno a la construcción de la Unidad Popular liderado por Salvador Allende.

La vida de esta hija de campesinos y trabajadores cargó las penas de cualquier mujer en una sociedad chilena injusta, en la que tuvo pasar por la precariedad, la miseria, el desamor y la enfermedad, las cuales le hicieron entender como es vivir en un país que no valora a las mujeres ni a los artistas.

Al cumplirse 52 años de su muerte recordamos algunas de sus composiciones sobre la reinvidicación del rol de la mujer.

**La petaquita**

Habla de la poca importancia que tiene para ella el matrimonio: “Todas las niñas tienen/ en el vestido/ un letrero que dice/ quiero marido/ dicen que le hace/ pero no le hace/ lo que nunca he tenido/ falta no me hace”.En ella Violeta Parra nos entrega una imagen, casi ridícula, de los hombres y mujeres de la sociedad de su época.

<iframe src="https://www.youtube.com/embed/rikCctOX5MA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Atención mozos solteros**

Del año 1959, esta canción inicia con una referencia al dinero: “Atención, mozos solteros/ lo que les voy a explicar:/ no porque tengan dinero/ digan «Me quiero casar»”, para pasar inmediatamente a mostrar que la institución familiar es opresiva: “No digan «Voy a gozar/ con mi esposa verdadera»,/ ¡son muy duras las cadenas,/ las que se van a amarrar!”.

<iframe src="https://www.youtube.com/embed/Uj60meBndLc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**La Juana Rosa**

Escrita en 1953, mucho antes de las liberaciones femeninas de los años sesenta, es una sátira de las mamas que quieren casar pronto a sus hijas y de la belleza y coquetería como “propias de la mujer”: “tenís que andar buenamoza/ por si pica el moscardón”, o por la posiblidad de enfrentar la soledad "Tenís veinticinco, Rosita/ay, Rosa/ vai pa’ solterona”.

<iframe src="https://www.youtube.com/embed/DtG7rv0EoXU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Yo también quiero casarme**

Compuesta en año 1959 en la que Violeta Parra va en contra de la imposición del novio y del matrimonio por parte de la sociedad a las mujeres chilenas de aquella época, “por eso a mí la prudencia me aconseja no casarme… mejor será señores que me quede sin casar, y no caer en la trampa por toda una eternidad”.

https://www.youtube.com/watch?v=w1Ol0RjPWFs

**El joven para casarse**

En esta canción lanzada en 1957 la artista chilena habla sobre el machismo y como la moneda es ofrecida a cambio de favores sexuales “la niña que quiere a un joven se destina a padecer / andará de boca en boca si no se casa con él”.

<iframe src="https://www.youtube.com/embed/-6FPv6mU-2M" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
