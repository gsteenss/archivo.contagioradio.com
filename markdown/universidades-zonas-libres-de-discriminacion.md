Title: Universidades, zonas "libres" de discriminación
Date: 2015-06-02 16:11
Category: closet, LGBTI
Tags: Clip DS, Closet Abierto, Derechos Humanos, Homosexualidad, ISOS Tadeo, Javeriana, LGBTI, Stonewall, UD es igual, Universidad Distrital, Universidad Jorge Tadeo Lozano, Universidad Pedagógica Nacional, Universidades
Slug: universidades-zonas-libres-de-discriminacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/DSC0087.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

###### <iframe src="http://www.ivoox.com/player_ek_4585402_2_1.html?data=lZqll5mUdo6ZmKiakpaJd6KpmIqgo5WbcYarpJKfj4qbh46kjoqkpZKUcYarpJKejcnJb6vpz87cj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Programa 1ro de Junio] 

Las universidades en Colombia buscan fomentar espacios libres de discriminación por medio de grupos que apoyan a las minorías dentro de las aulas de clase. El aporte de la juventud al desarrollo de una sociedad guiada desde la academia es un elemento clave para abrir la mentalidad de un país que todavía se torna reacio hacia la homosexualidad.

Closet Abierto se centra en resaltar el trabajo de estos grupos universitarios en Bogotá, los cuales se han encargado de realizar una labor de importancia para sus propias instituciones por medio de conversatorios, cine foros, talleres de apoyo, acompañamiento psicológico, espacios de esparcimiento, que fomentan la diversidad sexual y la igualdad.

Stonewall de la Universidad Javeriana, ISOS Tadeo de la Universidad Jorge Tadeo Lozano, Clip DS de la Universidad Pedagógica, UD es igual de la Universidad Distrital y Grupo Griis de la Universidad del Rosario, son los grupos que hablan acerca de su surgimiento y apoyo a la comunidad LGBTI, tanto por su parte como por parte de las universidades a las que pertenecen, enfatizando en la colaboración de docentes que han aportado desde su profesión al avance de la sociedad en materia de derechos.
