Title: Asamblea de Venezuela no tiene facultades para declarar al presidente en abandono del cargo
Date: 2017-01-10 15:13
Category: El mundo, Política
Slug: asamblea-de-venezuela-no-tiene-facultades-para-declarar-al-presidente-en-abandono-del-cargo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/venezuela-asamblea.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: larepublica.ec] 

###### [10 Ene 2017]

Tanto el Tribunal Supremo de Justicia de Venezuela, como los integrantes de los partidos oficialistas como el PSUV han coincidido en que **ni la Asamblea Nacional tiene facultades para declarar a Nicolás Maduro en “abandono del cargo”, ni se cumplen las condiciones** para que el presidente sea sujeto de esa declaratoria, pues no se cumplen los requisitos constitucionales para tal fin.

Eustoquio Contreras, diputado oficialista, afirma que la decisión que tomó la Asamblea demuestra que hay una **división profunda en la llamada “Mesa de Unidad Democrática”** hasta el punto en que hubo 3 abstenciones por parte de congresistas de esa colectividad. Que haya 3 abstenciones indica que no están de acuerdo en la ruta que pretenden seguir, afirmó.

Según diversos medios de información los artículos 232, 233 y 234 de la Constitución de 1999, especifican las razones por las cuales se puede declarar el abandono del cargo y son, muerte, enfermedad incapacitante por más de 90 días sin autorización de prórroga por parte de la Asamblea o incapacidad física o mental certificada por el Tribunal Supremo de Justicia; **en el caso de Nicolás Maduro ninguna de esas condiciones se cumple.**

Además existe la situación de desacato en la que se encuentran desde la elección, pues tres de las cururles fueron demandadas y resueltas a favor de los demandantes , en los que se ordena a la Asamblea la anulación de esas investiduras. Esta orden no se ha cumplido, razón por la cual el **TSJ de Venezuela declaró nulas todas las decisiones que se tomen en ese escenario.**

Contreras afirmó que la mayoría de oposición en la Asamblea les da la posibilidad de hacer oposición política, pero no han aprovechado la oportunidad para mejorar la situación del país o hacer contrapeso político, **sino que se han desenmascarado con una serie de iniciativas que lo único que buscan es ubicarlos a ellos en el poder**. ([También le puede interesar [Tensión e](https://archivo.contagioradio.com/continua-la-tension-en-venezuela-en-medio-de-las-conversaciones/)n Asamblea Nacional])

Por otra parte, el diputado resaltó que **es necesario que se haga una evaluación profunda de las políticas de gobierno y de la llamada “Revolución Bolivariana”** que condujeron al gobierno a la situación actual y que podrían significar una nueva derrota en las urnas de cara a las elecciones de alcaldes y gobernadores que se debería realizar este año, y también en la posibilidad de que se convoque un referendo revocatorio. ([Lea también Oposición violenta es neutralizada](https://archivo.contagioradio.com/organismos-de-seguridad-de-venezuela-han-neutralizado-amenazas-de-violencia-en-las-calles/))

<iframe id="audio_16070398" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16070398_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
