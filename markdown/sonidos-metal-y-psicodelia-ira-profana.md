Title: Sonidos Metal y Psicodelia, "Ira Profana"
Date: 2015-09-28 15:57
Category: Sonidos Urbanos
Tags: Diego Paez vocalista Ira Profana, ira profana banda bogotana de metal, Ira profana en Contagio radio, Stoner Thrash Metal
Slug: sonidos-metal-y-psicodelia-ira-profana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ira-profana-e1443460513807.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_8664098_2_1.html?data=mZujlpWdfI6ZmKiakpaJd6KklYqgo5WbcYarpJKfj4qbh46kjoqkpZKUcYarpJK21MaPlNPjx8bbw4qWh4zH1dTbx9ePmMnmwtjVjbLJuMLgjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [28 Sep 2015]

"**Ira Profana**" es una banda bogotana de Stoner Thrash Metal, fuertemente influenciada por sonidos doom, black, death metal y rock psicodélico entre otros géneros musicales. Creada en 2012, la agrupación busca crear nuevos conceptos a partir de la experimentación con sonidos oscuros, combinados con líricas con las que fijan su posición critica ante la sociedad.

Antiguos integrantes de "Demonic Throne", conforman la alineación actual de la banda: Deimer Beltran en el bajo,  Ruben Beltran en la guitarra, Nicolas Cantor en la batería y Diego Paez en la voz principal, y es precisamente este ultimo quien nos acompaño en "A la calle", para compartir parte de su experiencia, trayectoria, influencias y proyectos de "**Ira Profana**" en la escena bogotana.

<iframe src="https://www.youtube.com/embed/_nbwICYox3I" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
[Facebook.com/IraProfana](https://www.facebook.com/IraProfana?fref=ts)
