Title: Why the National Strike in Colombia? Answers in brief!
Date: 2019-04-24 20:07
Author: CtgAdm
Category: English
Tags: Strike
Slug: why-the-national-strike-in-colombia-answers-in-brief
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/paro-nacional-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Why the National strike?**

Different social sectors will protest across the streets to demand the accomplishment of the agreements, the adjustment of the National Development Plan, the defense of public education and finally rejecting the murder of leaders and social leaders.

**[Who is going to participate?]{lang="EN"}**

The mobilization has been joined by labour, political, social, peasants, indigenous, Afro, students movements.

**How long will this protest last?**

The protest will begin on April 25th in different departments of Colombia but it could be extended more days.

**In which cities will be the protest?**

**Bogotá**, from CAD stop to Bolivar Square 9 a.m.

-   Pedagógica: Dario Square 8 a.m.
-   La Santa María Square 9 a.m.

**Medellín:**

-   Los Deseos Park 9 a.m.

**Yopal:**

-   El Resurgimiento Park 8 a.m.

**Bucaramanga:**

-   San Pio Park , 9 AM

**Popayán:**

-    Santo Domingo Cloister 9 a.m

**Santa Marta:**

-   San Miguel Park 8 a.m

**Cucuta: **

-   Transportation Center 8 a.m

**Cali:**

-   Glorieta de Sameco  9 a.m

 
