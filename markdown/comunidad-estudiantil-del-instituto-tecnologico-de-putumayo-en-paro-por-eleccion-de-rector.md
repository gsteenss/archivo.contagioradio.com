Title: Comunidad Estudiantil del Instituto tecnológico de Putumayo en paro por elección de rector
Date: 2018-09-03 12:52
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, Putumayo, rector
Slug: comunidad-estudiantil-del-instituto-tecnologico-de-putumayo-en-paro-por-eleccion-de-rector
Status: published

###### [Foto: John Willian Vargas] 

###### [03 Sept 2018]

Desde el pasado 28 de agosto, los estudiantes del Instituto Tecnológico de Putumayo entraron en paro producto de su inconformidad con la tardía elección del Rector de la institución hecha por el Consejo Superior. La comunidad estudiantil afirma que se está **vulnerando su derecho a ser escuchados** y dicen que su voto no está siendo reconocido.

A pesar de haber realizado las votaciones reglamentarias, en la que participan tanto estudiantes como docentes, y tener una victoria contundente de la docente y ex rectora Marisol Gonzales, el Consejo Superior aún no ha manifestado quién estará a cargo de la rectoría, debido a que Alberto Fajardo, docente de cátedra y anteriormente Jefe de Control Interno de la Alcaldía Municipal de Mocoa Putumayo, cuenta con el respaldo de la mitad del CSU, y según la comunidad académica, **habrían intereses desde la gobernación del departamento para que sea nombrado rector.**

Los estudiantes del instituto afirman que la Rectoría se ha convertido en un conflicto de intereses y se niegan a dejar que la institución tenga **nexos con los partidos o tendencias políticas,** debido a que jamás han estado relacionados con ningún tema de este tipo y ha sido beneficioso para la organización.

De otro lado, los docentes de esta institución denunciaron haber sido víctimas de amenazas, a través de una abogada independiente, quien les ha enviado oficios advirtiendo a los profesores y a los cuatro miembros del Consejo que apoyan la reelección de Gonzales,** que se enfrentarían a acciones judiciales y laborales en caso de no apoyar la decisión de posicionar a Fajardo como rector.** (Le puede interesar: [En el día del maestro, docentes exigen que se salde crisis de la educación](https://archivo.contagioradio.com/en-el-dia-del-maestro-docentes-exigen-que-se-salde-crisis-de-la-educacion/))

Actualmente, la candidata Marisol Gonzales se encuentra **inhabilitada de cualquier cargo público durante los próximos tres meses** a causa de un fallo realizado por la Fiscalía donde se le involucra en asuntos relacionados con dineros ilegales y desvío de recursos, acusaciones que según la comunidad de Instituto Tecnológico de Putumayo serían falsas, y solo representarían una columna de humo para ayudar al candidato del Gobierno Alberto Fajardo posicionarse como rector.

### **Los estudiantes del  Putumayo se movilizan ** 

En protesta por los hechos, los alumnos informaron sobre el** cese a las actividades académicas como medio de movilización pacífica** para que se respete su derecho a ser tomados en cuenta y hacer válida la votación contundente de la comunidad académica.

Además, realizaron una marcha para pedir transparencia en las decisiones educativas, a la que se sumaron estudiantes de colegios locales y padres de familia con el fin de **visibilizar otras problemáticas relacionadas con las gestiones académicas adelantadas por el PAE**. (Le puede interesar: [En Septiembre se realizará el segundo encuentro de estudiantes de educación superior](https://archivo.contagioradio.com/en-septiembre-se-realizara-el-segundo-encuentro-de-estudiantes-de-educacion-superior/))

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).] 
