Title: Balance del primer día de paro del Sur - Tunjuelo Bogotá
Date: 2017-09-27 15:27
Category: Movilización, Nacional
Tags: Paro desde el Sur Tunjuelo, relleno doña juana
Slug: balance-el-primer-dia-de-paro-del-sur-tunjuelo-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/mochuelos-e1506526775637.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nc Noticias] 

###### [27 Sept de 2017] 

Sin respuestas por parte de la administración de Enrique Peñalosa finalizó el primer día de paro de los habitantes del Sur - Tunjuelo de Bogotá. Las marchas que se encontraron en la Autopista Sur, transcurrieron sin alteraciones, sin embargo, sobre las 3 de la tarde los ciudadanos denunciaron ataques del ESMAD, dejando como resultado una persona detenida, de igual modo ** 5 estudiantes de la Universidad Nacional habrían resultado heridos, durante confrontaciones con la Fuerza Pública.**

Desde tempranas horas de la mañana, cientos de personas comenzaron a juntarse en cada uno de los puntos de movilización que se habían citado para la hora cero del Paro Sur-Tunjuelo, **convocado en más de 130 barrios, 4 localidades y que fue apoyado por estudiantes, campesinos, organizaciones sociales** y miles de ciudadanos que marcharon por las calles del Sur de Bogotá.

### **Fuerza Pública** 

Los ciudadanos desde el inicio de la jornada denunciaron la fuerte presencia de la Fuerza Pública en cada uno de los puntos de movilización. No obstante las marchas trascurrieron pacíficamente. Sin embargo, **otro fue el desenlace en las universidades públicas que acompañaron la movilización desde sus centros educativos**.

La Universidad Pedagógica realizó bloqueos sobre las doce de día y posteriormente la reacción del Escuadrón Móvil Antidisturbios, provocó enfrentamientos, hasta el momento no se tiene información de algún estudiante o ciudadano herido. (Le puede interesar: ["Comité del Paro Sur-Tunjuelo denuncian estigmatización a líderes"](https://archivo.contagioradio.com/comite-del-paro-sur-tujuelo-denuncia-estigmatizacion-a-lideres/))

En la Universidad Nacional, los estudiantes, en un primer momento salieron a bloquear la Calle 26, posteriormente el ESMAD los desalojo y salieron a manifestarse por la calle 45, en donde agentes del ESMAD los desalojaron con gases lacrimógenos, desatando confrontaciones. Hasta el momento los estudiantes **denuncian que 5 personas habrían sido heridas, dos de ellas de gravedad.**

De igual forma sobre las 3 de la tarde los habitantes **que se movilizaban por la localidad de Usme manifestaron que fueron agredidos por el ESMAD**, al parecer habría una persona detenida. (Le puede interesar: ["ESMAD también arremetió contra manifestantes en el Relleno Doña Juana"](https://archivo.contagioradio.com/esmad-continua-arremetida-contra-manifestantes-en-el-relleno-dona-juana/))

Otra de las denuncias tiene que ver con la retención de una profesora en el barrio Usme. Se trata de **Mónica Patricia Sánchez, docente del colegio Miguel de Cervantes Saavedra**. De acuerdo con las denuncias que ha hecho la comunidad, Sánchez fue agredida por "el oficial 5028", y aún se desconoce el paradero de la profesora.

### **Sin diálogo con la administración de Peñalosa** 

Sobre las 12 del mediodía, iniciaron las diferentes asambleas comunitarias, que estuvieron a la espera de enviados por parte de la administración distrital de Enrique Peñalosa, sin embargo no hubo presencia de ninguno para poder entablar el diálogo y las mesas de conversación en donde se trataría el pliego de exigencias compuesto por 4 puntos: el cierre del Relleno Sanitario Doña Juana, la militarización de la vida juvenil, el acceso a la educación y las garantías sociales, que se respete la recolección de firmas y el proceso de la revocatoria del alcalde Peñalosa y que se den mejorías urgentes en el sistema de transporte en el Sur.

El día de mañana los ciudadanos continuaran con diferentes acciones en el marco del Paro y exigirle a la Alcaldía de Enrique Peñalosa respuestas urgentes a las problemáticas que los aquejan.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
