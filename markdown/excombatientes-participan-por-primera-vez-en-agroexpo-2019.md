Title: Excombatientes participan por primera vez en Agroexpo 2019
Date: 2019-07-13 17:31
Author: CtgAdm
Category: eventos, Paz
Tags: Agroexpo, ECOMÚN, excombatientes, Proyectos Productivos
Slug: excombatientes-participan-por-primera-vez-en-agroexpo-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/D_R9MdMW4AUCwKB.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CoopReinco] 

Del 11 al 21 de julio, 15 excombatientes integrantes de la organización Economías Sociales del Común (ECOMUN) provenientes de diferentes lugares del país, participarán en Agroexpo 2019 para compartir los resultados de la gran variedad de proyectos productivos que han emprendido en sus regiones, como parte de su proceso de reincorporación y  emprendimiento económico.

### ECOMUN llega a Agroexpo

ECOMUN, organización que nació como fruto del Acuerdo de La Habana, ha sido una herramienta que ha permitido la reincorporación colectiva y económica de cerca de 13.000 excombatientes desde su creación en 2017, agrupando 135 formas asociativas alrededor del país.

Mojarras, carne y leche de búfalo, sacha inchi, miel, procesados de fruta, confecciones, servicios de turismo y café son algunos de los productos que ofrecerán los excombatientes que han venido de A**ntioquia, Guaviare, Amazonía, Tolima, Meta, Cauca y Arauca para trabajar en Corferias**

Aunque en esta edición de Agroexpo solo hay ocho cooperativas de Ecomún, los excombatientes presentes en Bogotá **destacan las iniciativas ganaderas, textiles y artesanas que sus compañeros vienen desarrollando en el campo**, "estamos construyendo y sé que vamos a ser capaces de sacar estos proyectos de Ecomún adelante, pero tenemos que irnos y trabajar a los territorios" asegura una de las integrantes de Ecomún.

> Los proyectos productivos de las cooperativas de excombatientes se reflejarán en Agroexpo 2019 a través de la partipacion de ECOMUN. Corferias 11 al 21 de Julio  
> Con [\#FondoEuropeoParaLaPaz](https://twitter.com/hashtag/FondoEuropeoParaLaPaz?src=hash&ref_src=twsrc%5Etfw) [@AECIDColombia](https://twitter.com/AECIDColombia?ref_src=twsrc%5Etfw) [@Gob\_eus](https://twitter.com/Gob_eus?ref_src=twsrc%5Etfw) [@ThinkupLKS](https://twitter.com/ThinkupLKS?ref_src=twsrc%5Etfw) [@alecop](https://twitter.com/alecop?ref_src=twsrc%5Etfw) [pic.twitter.com/TLXx0VZBWB](https://t.co/TLXx0VZBWB)
>
> — ReincoCoop (@CoopReinco) [June 21, 2019](https://twitter.com/CoopReinco/status/1142189637257105419?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
