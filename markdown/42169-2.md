Title: Dejación de armas de las FARC –EP marca un cambio en la historia del país
Date: 2017-06-13 14:22
Category: Nacional, Paz
Tags: acuerdo de paz, FARC-EP
Slug: 42169-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/entrega-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Naciones Unidas] 

###### [13 Jun 2017] 

Con la entrega de armas que se realiza hoy en las zonas veredales, las FARC-EP completan el 60% del material armamentístico que ahora estará en las manos de la ONU y el Mecanismo Tripartito de Verificación. Este paso para Andrés Paris, integrante de las FARC-EP, **es la muestra de la voluntad que tienen las y los guerrilleros por construir paz en Colombia y asumir la política a partir del debate de las ideas**.

La siguiente etapa para culminar la entrega total de armas, será la evacuación del material explosivo que ya está ubicado en coordenadas específicas y en conocimiento tanto de la ONU como del mecanismo tripartito indicó Andrés Paris. Le puede interesar: ["FARC inicia de dejación de armas sin pesar ni llanto"](https://archivo.contagioradio.com/el-20-de-junio-las-farc-habra-entregado-todas-sus-armas/)

El integrante de las FARC-EP señaló que este momento re significa el uso de las armas porque ahora serán el símbolo del establecimiento del fin del conflicto **“el fusil en esta etapa simboliza la voluntad del insurgente de pasar a una actividad política abierta**, en donde el Estado cumple el conjunto de los compromisos y las FARC el compromiso de la dejación de armas”.

Por otra parte, ya se entregó el certificado de dejación de armas al primer grupo de integrantes de las FARC que hacen parte del grupo de **305 personas que se están entrenando para ser parte de los esquemas de protección** al servicio del partido político que surgirá luego del congreso que esa insurgencia realizará el próximo mes de Agosto.

### **¿El 2018 un año de cambios para la política del país?** 

Sobre la implementación de los acuerdos de Paz, Paris manifestó que mientras las FARC-EP han cumplido a cabalidad con su parte, el gobierno no ha respondido en un 80% de lo pactado, situación que genera incertidumbre al interior de la guerrilla, pero que aspira a que se transforme en las elecciones del 2018. Le puede interesar: ["Físcal debe dejar de tratar a las FARC como si no hubiesen firmado la paz: Andres Paris"](https://archivo.contagioradio.com/farc/)

“Los colombianos hacia el proceso electoral del 2018 deben contemplar bien cuáles son los sectores de la guerra y cuales los de la reconciliación” afirmó Paris y agregó que si bien es cierto que hay una desilusión por el incumplimiento del Gobierno y crea problemas internos, **lo fundamental es que el conjunto de la organización se encuentra bajo la intención de no continuar con la guerra.**

### **Las FARC-EP y su partido político** 

En Agosto las FARC-EP dará a conocer los lineamientos con los que se fundará su partido político, en ese sentido Paris, aseguró que se está avanzado en conversaciones con cada uno de los sectores político que ha manifestado **su voluntad de lanzar candidatos a las elecciones del 2018, como lo ha sido Gustavo Petro y más recientemente Piedad Córdoba**.

la discución fundamental es dirijida a incorporar las grandes demandas de la nación, a construirt un programa de transdformación democrática y pacífica de colombia, el nuevo partido que se va a transformar agrupa a sus viejos militantes, si claro que se agitan las discuciones somos un partido que vuelve a las formas horizontales, pero estas no van a terminar en divisiones sino en una convergencia democrática que logre en el 2018 colocar un gobierno un presiodetne que retome los acuerdos de la habana y los hga su gobierno.

**“La discusión fundamental está dirigida a incorporar las grandes demandas de la nación, a construir un programa de transformación democrática y pacífica”** expresó Paris e indicó que frente a la conformación del partido y las tendencias que deberá reunir aspira a que se junten a partir de una dirección horizontal.

“la discución fundamental es dirijida a incorporar las grandes demandas de la nación, a construirt un programa de transdformación democrática y pacífica de colombia, el nuevo partido que se va a transformar agrupa a sus viejos militantes, si claro que se agitan las discuciones somos un partido que vuelve a las formas horizontales, pero estas no van a terminar en divisiones sino en una convergencia democrática que logre en el 2018 colocar un gobierno un presiodetne que retome los acuerdos de la habana y los hga su gobierno.

<iframe id="audio_19247104" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19247104_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
