Title: ESMAD desaloja a campesinos de Sabanalarga en Antioquia
Date: 2018-02-12 10:12
Category: DDHH, Nacional
Tags: Antioquia, desalojo de campesinos, Hidroituango, Movimiento Ríos Vivos
Slug: esmad-desaloja-a-campesinos-de-sabanalarga-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/DV1R3HZXkAA8Y8A-e1523649866886.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [12 Feb 2018] 

Sin reubicación, sin claridad en el plan de manejo ambiental y sin posibilidad de buscar a los **desaparecidos** que se encuentran en el sector de Portachuelo en el Cañón del Río Cauca en Antioquia, y en medio de un paro armado del ELN, el ESMAD, está desalojando de cerca de 400 familias para que avance la tala de bosque y puesta en marcha del proyecto hidroituango.

Han dicho las comunidades y el Movimiento Ríos Vivos que esto implica **riesgos para su integridad física** y para el territorio que se ha visto amenazado por la tala de árboles para la puesta en marcha del proyecto hidroeléctrico Hidroituango.

De acuerdo con Isabel Cristina Zuleta, integrante y vocera del Movimiento Ríos Vivos, “más de **40 hombres del ESMAD** están desalojando a los campesinos en Sabanalarga”. Allí, la empresa EPM, encargada del desarrollo del proyecto hidroeléctrico, ha realizado la tala del bosque tropical desde hace dos semanas.

Zuleta enfatizó en que hubo una persona que resultó herida por el desprendimiento de unas rocas y “ha habido **amenazas por parte de la empresa contratista Refocosta** contra el líder Guillermo Wiles del Movimiento Ríos Vivos”. Para ese movimiento es grave que se realice la tala por encima de las casas de los campesinos y “el ESMAD está desalojando para que la empresa continúe talando el bosque”. (Le puede interesar: ["Memoria y resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

### **400 personas están siendo desalojadas de sus territorios** 

La denuncia la realizaron diferentes comunidades quienes aseguran que son **400 personas las que han sido desalojadas** de su vivienda y que se encuentran a lo largo del Cañón del Río Cauca, que será inundado en junio de 2018 como parte del proyecto Hidroituango. Zuleta manifestó que, “la zona donde se está realizando la tala está afectando a 13 familias, pero todos los cañoneros van a ser afectados por la tala que inició sin reubicación y con amenazas de los funcionarios”.

Si bien las Fuerza Pública había manifestado que el desalojo se realiza como medida de seguridad teniendo en cuenta el paro armado del ELN, Ríos Vivos indica que la única excusa **“es que la empresa tiene que trabajar”**. Han pedido que se deje de nombrar el tema del ELN “que no tiene ningún sentido, no hemos visto en la historia de este municipio presencia de ese grupo armado”.

Ante esto, le han pedido claridad a la empresa, puesto que el desalojo “es una orden de la Policía que llegó por una querella que interpuso EPM y la empresa Refocosta porque ellos necesitan seguir talando el bosque y **necesitan sacar a la gente** que les está estorbando”. (Le puede interesar: ["En zona de Hidroituango habría más de 2 mil personas desaparecidas"](https://archivo.contagioradio.com/en-el-canon-del-rio-cauca-afectado-por-hidroituango-hay-mas-de-2-mil-personas-desaparecidas/))

### **Tala del bosque afecta la búsqueda de personas desaparecidas** 

El Movimiento Ríos Vivos ha venido estableciendo diferentes alertas sobre la importancia que tiene el bosque tropical en esa zona, no solamente por la diversidad de la flora y fauna sino porque “hay personas que creen que **sus familiares desaparecidos se encuentran allí** y le han dicho a Refocosta que no tale los árboles porque ellos quieren buscarlos”.

Estas alertas fueron puestas ante la Fiscalía General de la Nación, pero “nadie los ha escuchado porque lo más importante para ellos es que **avance la tala y avance la obra**”. Zuleta manifestó que la empresa EPM “no entiende este tipo de razones y ha procedido con la tala del bosque con el que se pierden señales fundamentales para poder encontrar a estas personas”. (Le puede interesar:["62 masacres en los 12 municipios donde se desarrolla el proyecto Hidroituango"](https://archivo.contagioradio.com/62-masacres-los-12-municipios-donde-se-desarrolla-proyecto-hidroituango/))

Finalmente, Zuleta manifestó que, según los permisos ambientales que tiene el proyecto, **“se tendrían que talar 4.500 hectáreas** que hacen parte de la inundación”. Ante esto y teniendo en cuenta la fase en la que se encuentra el proyecto, “hasta el momento han sido taladas 5% de esas hectáreas”.

<iframe id="audio_23734948" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23734948_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div class="text_exposed_show" style="text-align: justify;">

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

</div>

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
