Title: Así es el control paramilitar en San José de Apartadó
Date: 2017-04-18 12:52
Category: DDHH, Entrevistas
Tags: Autodefensas Gaitanistas de Colombia, Paramilitarismo, San josé de Apartadó
Slug: paramilitares-controlan-veredas-de-san-jose-de-apartado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Comunidad-de-Paz-San-Jose.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de prensa IPC] 

###### [18 Abr 2017] 

La comunidad de paz de San José de Apartadó denunció que se está viviendo una arremetida paramilitar sobre el territorio, de manera evidente y sin que las autoridades actúen. Además afirman que hay una base de operaciones en la Vereda Nuevo Antioquia, con cerca de **600 hombres y desde la que se coordina el control territorial en la región.**

La presencia de este grupo autodenominado como Autodefensas Gaitanistas de Colombia se ha reportado en 3 veredas en específico: en la vereda Mulatos, en la vereda Playa Larga y en la vereda Arenas Bajas ubicadas entre **3 o 4 horas del casco urbano de San José de Apartado. **Le puede interesar: ["419 violaciones a los DD.HH. en 5 meses a manos de paramilitares"](https://archivo.contagioradio.com/419-violaciones-a-los-dd-hh-en-5-meses-a-manos-de-paramilitares/)

### **Control paramilitar en San José de Apartado** 

En la vereda Playa Larga, del corregimiento de Nuevo Antioquia, la comunidad ha señalado que hay una base paramilitar que se encarga de “regular” todas las acciones en la zona y tendría unos 600 hombres armados. **Desde esa base se coordinan las operaciones de escuadrones entre 30 y 40 paramilitares que recorren las veredas** sin ningún tipo de control.

En Arenas Bajas, la comunidad expresó que los paramilitares han construido en este lugar una cancha de futbol **“para recrearse”, además de tener una presencia permanente en este lugar. **Le puede interesar: ["Paramilitares amenazan integrantes de la comunidad de Paz de San José de Apartadó"](https://archivo.contagioradio.com/paramilitares-amenazan-integrantes-de-la-comunidad-de-paz/)

La situación de la vereda Multados no es muy diferente, los habitantes denunciaron que, desde el viernes santo, un grupo de paramilitares llegó al lugar y se mantienen allí de forma permanente, impidiendo que las personas tengan **celulares en las manos para tomas fotografías o grabaciones, restringiendo la movilización e intimidando a la población**.

Además, denunciaron que muy cerca de este lugar hay una estación de Policía que “no hace nada” lo que para ellos evidencia una complicidad con el accionar de estos grupos y adicionalmente en algunas incursiones a las veredas, los paramilitares **se han identificado, en principio como integrantes del Batallón Voltígeros de la Brigada XVII con sede en Carepa.**

### **Cobro de Vacunas e imposición de reglas** 

Referente a las **vacunas, las Autodefensas Giatanistas de Colombia, estarían cobrando entre \$50.000 y \$60.000** pesos por cada animal que se venda y a los negocios que hay en estas veredas, de igual forma, expresaron que estos grupos estarían imponiendo normas y estatutos al interior de las poblaciones amenazando a las personas diciendo que si no están de acuerdo deben abandonar el territorio.

Además, los pobladores señalaron que las **insignias de AGC, pasamontañas y pañoletas que les cubren los rostros** y el tipo de armas que usan los identifican como paramilitares. Le puede interesar. ["Asedio de Paramilitares contra comunidad de paz de San José de Apartadó"](https://archivo.contagioradio.com/paramilitares-atacan-a-la-comunidad-de-paz-de-san-jose-de-apartado/)

Sobre la actuación de las autoridades, un poblador, indicó que la Gobernación y Alcaldía tienen pleno conocimiento de esta situación, sin embargo, han dicho que “la comunidad se inventa todo”, frente a la Fuerza Pública, el habitante expresó que el **Batallón Voltígeros de Carepa, de la Brigada XVII, pese a tener conocimiento de estas incursiones paramilitares no han actuado.**

Los riesgos para la comunidad de San José de Apartado es someterse a un nuevo éxodo de desplazamiento, como lo vivieron desde la década de los 90 y a la **continuación de un ciclo de violencia con un actor del conflicto armado que ha tenido una fuerte presencia en esta región.**

<iframe id="audio_18210447" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18210447_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
