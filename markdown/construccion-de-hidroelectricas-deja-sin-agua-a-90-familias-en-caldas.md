Title: Micro-hidroeléctricas en Caldas han secado 19 fuentes de agua
Date: 2016-02-20 10:00
Category: DDHH, Nacional
Tags: Agua, Caldas, Corpocaldas, Fenómeno de El Niño, Hidroeléctricas
Slug: construccion-de-hidroelectricas-deja-sin-agua-a-90-familias-en-caldas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Hidroelectricas-Caldas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

<iframe src="http://www.ivoox.com/player_ek_10503235_2_1.html?data=kpWikpiWd5ahhpywj5aWaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncaTjz9jh1NrHp8qZpJiSpJjSb8XZjM3WxtfTqc2ZpJiSo57HuNPdxMbgjcnJrsKf1M7bjcbLucKfwpCmkpDKpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [**Albeiro Atehortúa**] 

###### [20 Feb 2016] 

Las construcciones de proyectos hidroeléctricos por parte de la empresa Unión Temporal Grupo Constructor Centrales Hidroeléctricas del Oriente de Caldas - UT GC CHOC en el corregimiento de Bolivia, **han provocado que 19 quebradas se hayan secado, dejando sin agua a 90 familias de la zona.**

De acuerdo con la denuncia del líder campesino, Albeiro Atehortúa, se trata de un proyecto hidroeléctrico en que la empresa  **UT GC Choc ha construido un túnel de 5 kilómetros, generando la filtración del agua.** “El agua llega desde arriba de la montaña y con explosivos se generó el túnel lo que provoca que el agua se desaparezca por las vibraciones de la tierra”.

La situación ha provocado que la empresa deba proveer agua en carrotanques a las familias afectadas. Sin embargo, según el líder campesino, el agua no es suficiente y aunque la comunidad ha logrado establecer reuniones con la alcaldía municipal, la gobernación, la Procuraduría Ambiental de Manizales,  CORPOCALDAS, la Defensoría del Pueblo e incluso el Ministro de Ambiente, Gabriel Vallejo, **ninguna autoridad ha puesto atención a esta problemática que tiene preocupada a la población de Bolivia.**

Según Atehortúa, en el 2014 se logró demostrar a CORPOCALDAS que la quebrada la Balastrera  había desaparecido por una filtración en el túnel de 70 litros de agua por segundo. En ese momento la empresa y la autoridad ambiental reconocieron que se había presentado la filtración, pero la respuesta fue la realización de un estudio “amañado a la empresa”, que concluyó que el secamiento de los manantiales estaba asociado a factores climáticos, específicamente **lo excusaron con el fenómeno de El Niño**, por lo que CORPOCALDAS levantó la medida cautelar y el proyecto siguió su curso.

Sin embargo, la comunidad ha demostrado por medio del Comité de Cafeteros, que cuenta con un sistema de medición de aguas lluvias, que aun cuando en el 2009 la región vivió el verano más intenso que en 2015, ninguna quebrada se secó. Teniendo en cuenta que la empresa hace presencia en la región desde el año 2011 y a inicios del  2014 se comenzó la construcción del túnel y con ella, se fue acabando el agua en la quebrada.

“CORPOCALDAS, ha dicho que es el fenómeno de El Niño y que el túnel no tiene nada que ver, que cuando vuelvan las lluvias se van a recuperar las quebradas, pero hasta ahora no ha pasado, **es una zona donde llueve mucho, y ninguna quebrada se había secado antes”,** dice el campesino.

En la licencia ambiental se da permiso a la empresa de utilizar 17 litros por segundo en el túnel, pero los pobladores aseveran que en visita con CORPOCALDAS se ha evidenciado que esos niveles siempre los sobrepasa la compañía. **“Tienen una filtración de más de 100 litros por segundo, el agua de las quebradas está allá”,** sostiene Atehortúa.

Al oriente de Caldas,  la **UT GC CHOC planea llevar a cabo 11 proyectos hidroeléctricos.** En el corregimiento de Montebonito la empresa ya cuenta con licencia ambiental y en el resto de lugares está en proceso de obtener la licencia,  “la gente está muy preocupada y están conformando algunas veedurías para ver qué se puede hacer ”.

La comunidad señala que será imposible recuperar las quebradas, y han exigido que se instale el servicio de agua suficiente para las familias y fincas damnificas, además piden una indemnización para las personas afectadas por el daño causado en las propiedades debido a que **“las tierras van a  quedar áridas e improductivas”**, denuncia el líder campesino.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
