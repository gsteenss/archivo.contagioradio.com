Title: Es asesinado Euloquio Pascal líder Awá, en Nariño
Date: 2020-10-09 17:04
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: asesinato, Awá, indígena, Líder Social
Slug: es-asesinado-euloquio-pascal-lider-awa-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/lider-social.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Archivo Contagio Radio*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este viernes 9 de octubre la comunidad Awá denunció el asesinato del líder indígena, **Euloquio Pascal Rodríguez , en el resguardo La Brava, zona rural de Tumaco,** departamento de [Nariño](https://www.justiciaypazcolombia.com/narino-dos-masacres-por-resolver/).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CNTI_Indigena/status/1314559015997837312","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CNTI\_Indigena/status/1314559015997837312

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Euliquio Pasca, tenía 42 años de edad y hacia aparte del Resguardo Mayor de esta comunidad indígena Awá, según fuentes regionales el hecho en horas de la noche de este jueves, c**uando hombres armados ingresaron al resguardo, en donde se encontraba líder tomando un baño en una quebrada ubicada en su hogar**. allí le dispararon ocasionando su muerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posterior a ello integrantes de la comunidad que escucharon los disparos se dirigieron al sitio de los hechos, y reportaron el asesinato al **Gobernador suplente del resguardo, quién ordenó que el líder fuera traslado de La Brava a la vereda Guanapí**, en donde la SIJÍN realizó el levantamiento del cuerpo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo autoridades locales señalaron que realizarán investigaciones para esclarecer este nuevo asesinato, de un líder indígena que según personas cercanas no tenia amenazas en contra. ([Ana Lucía Bisbicús, lideresa indígena Awá es asesinada en Nariño](https://archivo.contagioradio.com/ana-lucia-bisbicus-lideresa-indigena-awa-es-asesinada-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
La Comisión Nacional de Territorios Indígenas, señaló que con el asesinato de Euloquio Pascal **son 17 los integrantes del pueblo Awá, asesinados en los últimos 6 meses en el departamento de Nariño**. ([El Pueblo Awá lanza un S.O.S contra la guerra en Nariño](https://archivo.contagioradio.com/el-pueblo-awa-lanza-un-s-o-s-contra-la-guerra-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicional señalan que en esta zona rural de Nariño, operan grupos armados como Los Contadores, el Frente Oliver Sinisterra y las Guerrillas Unidad del Pacífico. [Otra Mirada: Se reactiva la movilización social en Colombia](https://archivo.contagioradio.com/otra-mirada-se-reactiva-la-movilizacion-social-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
