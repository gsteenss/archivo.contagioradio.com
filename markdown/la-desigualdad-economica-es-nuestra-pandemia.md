Title: La desigualdad económica es nuestra pandemia
Date: 2020-04-12 16:33
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Colombianos, Covid-19, desigualdad económica, desigualdad en Colombia, pandemia, Pobreza en Colombia
Slug: la-desigualdad-economica-es-nuestra-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/coronavirus-Chile-América-Latina-desigualdad-inequidad-covid-19-Bolivia-min.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#656c6f"} -->

###### Foto de: [IADB](https://www.iadb.org/) 

<!-- /wp:heading -->

<!-- wp:heading {"level":5,"customTextColor":"#525657"} -->

##### Por Jhonathan Orozco 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ahora la pandemia nos tiene ocupados sofocando incendios aquí y allá, pero en medio del humo empiezan a calar reflexiones que quizá se aclaren y se debatan con más fuerza el día que salgamos de esta crisis. Una de ellas tiene que ver con las vergonzosas desigualdades sociales y las consecuencias que ahora están generando en la vida y salud de las personas por cuenta de las medidas tomadas para contener el virus del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las autoridades recomiendan lavarse las manos: pero de los 8.000 millones de seres humanos que vivimos en el mundo, 2.100 millones carecen de acceso a agua potable y otros 4.500 millones a[saneamiento básico](https://www.who.int/es/news-room/detail/12-07-2017-2-1-billion-people-lack-safe-drinking-water-at-home-more-than-twice-as-many-lack-safe-sanitation).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esa cifra en Colombia alcanza 3,6 millones en falta de acceso a acueducto y 5,6 millones a alcantarillado.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Del resto de la población que sí tiene acceso a esos servicios, nos enteramos que había un millón de hogares [sin conexión al servicio de agua.](http://www.minvivienda.gov.co/sala-de-prensa/noticias/2020/marzo/gobierno-nacional-anuncia-nuevas-medidas-desde-el-sector-de-agua-potable-para-prevenir-el-coronavirus) Colocándole poquito, eso vienen siendo otras 3 millones de personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las autoridades dicen que en caso de contraer el virus, debemos quedarnos en las casas mientras se pasa y no avanza a una condición grave, como lo sería el caso de personas con malas condiciones físicas o sistemas inmunes vulnerables. P[ero 2.000 millones de personas en el mundo no tienen acceso regular a alimentos saludables, nutritivos y suficientes](https://news.un.org/es/story/2019/07/1459231).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5,"customTextColor":"#154c64"} -->

##### [En Colombia pasan hambre unas 2,4 millones de personas](https://www.elespectador.com/economia/en-colombia-24-millones-de-personas-padecen-hambre-onu-articulo-871095). 

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Las autoridades dicen  que debemos confinarnos en nuestras casas:](https://lasillavacia.com/quedate-casa-significa-cosas-muy-diferentes-segun-casa-76080) pero 15.5 millones de hogares en Colombia tienen materiales precarios de construcción, son espacios pequeños que generan hacinamiento y les falta de servicios públicos, lo que incide en la propagación de enfermedades pulmonares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todos estamos de acuerdo en que  
ahora mismo debemos llevar comida y agua potable a las personas, reconectar  
servicios, subsidiar servicios públicos y congelar créditos bancarios. Algo que  
hace tres meses sería un escándalo, ¿por qué?, ¿acaso estos problemas no  
existían antes de la pandemia? Voy a aventurar una explicación acerca de por  
qué la gente está de acuerdo con esta suerte de Estado de Bienestar ahora, pero  
hace unos meses no.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En condiciones “normales”, las personas juzgan que en general el Estado no debe proporcionar ayudas porque eso genera vagos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La gente debe trabajar y labrarse su propio destino. El pobre es pobre porque quiere. Si no tiene agua potable, alimentación o vivienda digna es porque, a pesar de tener la libertad de trabajar y esforzase, no lo hace. El dinero está inventado. Y entonces sacan su mejor arma: “*Es que yo conozco a alguien que nació en la miseria, trabajó duro y ahora tiene plata. ¿Si ella pudo, por qué no los otros*?” Suena lógico. La  gente tiene libertad para hacer lo que quiere, si quiere tener buenas condiciones de vida, debe esforzarse. El pobre, seguramente no se esfuerza lo suficiente.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Esa es la base de la ética moderna: la autonomía humana.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En lo religioso, la gente merece las consecuencias de sus actos porque Dios les ha dado el libre albedrío para elegir entre el bien y el mal. Si escogieron el mal, no vengan a quejarse. Ya en el plano de los sistemas sociales y legales, el tema funciona más o menos parecido. El capitalismo se basa en la premisa de que el ser humano es autónomo, toma sus propias decisiones y puede definirse un destino. Lo que tenga en su vida es producto de lo que hace con esa libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora, como la pandemia ha anulado la autonomía de muchas personas  porque no pueden salir a trabajar, entonces no es su culpa que no tenga cómo garantizar sus condiciones materiales de existencia. En ese sentido, no puede tenerse por justo que alguien aguante hambre o le corten sus servicios públicos cuando no su culpa que no tenga dinero.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Pero esto es una completa farsa, la libertad para avanzar socialmente es un mito que condena a las personas a pedalear en una bicicleta estática donde, por más que se esfuercen, el paisaje alrededor va a seguir siendo el mismo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para que tenga lógica el mito de  
la libertad, un niño que nace en Chocó, si se esfuerza por salir adelante en la  
misma medida que un niño que nace en Bogotá, al cabo de 25 años debieron y  
deberían tener más o menos lo mismo: haber recibido buena alimentación, haber  
ido a la escuela, tener una familia, haber tenido un computador con acceso a  
internet y, finalmente, ambos tener un título profesional y su primer empleo,  
por ejemplo. Pero eso no es cierto. Las condiciones sociales, económicas y  
ambientales son más adversas en Chocó que en Bogotá (guardando las salvedades)  
y eso determina la vida de las personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Está comprobado que una madre que no tiene acceso a alimentación adecuada o su hijo sufre de desnutrición durante los primeros 1000 días, ese niño está condenado a [no tener un buen desarrollo cognitivo durante el resto  de su vida](https://elpais.com/elpais/2016/10/07/planeta_futuro/1475833790_799620.html). Aunque no  existen superhumanos, competir en la vida con personas que sí tuvieron buena alimentación sería más o menos como estar en una carrera donde no importa lo mucho que te prepares, el otro va a correr más rápido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Está comprobado que en Colombia, [si una persona vive en la miseria, le costará 11 generaciones lograr subir un escaño en la pirámide social.](https://www.bbc.com/mundo/noticias-45022393)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### A lo anterior hay que agregar un dato:

<!-- /wp:heading -->

<!-- wp:paragraph -->

Actualmente en el mundo existe suficiente riqueza para alimentar y dar condiciones materiales dignas para cada uno de los 8.000 millones de seres humanos que vivimos en él. ¿Por qué siguen existiendo, entonces, personas que no tienen hogar, agua potable, saneamiento básico, alimentación adecuada y suficiente, y acceso a educación? Porque existe un sistema económico que funciona parecido a las medidas de prevención y contención de una pandemia en el sentido que limita la libertad para lograr tener condiciones dignas de existencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No importa lo mucho que alguien se esfuerce: gran parte de su destino dependerá de cosas que no están dentro de su órbita de decisión: el país, la familia y el lugar donde nace. Si la madre aguantó o no hambre antes de concebirla; si tuvo cómo darle buena alimentación durante los primeros mil días. Si en la casa hubo agua, alcantarillado y buenas condiciones de construcción. Si tuvo un pc y acceso a internet.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Debemos eliminar la pandemia de las desigualdades económicas que limitan el potencial de los seres humanos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cada persona debería tener las condiciones para desarrollar su potencial tanto como ella quisiera, y no, tanto como pudiera, conforme a las condiciones económicas, sociales y ambientales en que le toque nacer.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
