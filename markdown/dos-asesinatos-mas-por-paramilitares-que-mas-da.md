Title: Dos asesinatos más por paramilitares ¿qué más da?
Date: 2015-07-26 10:18
Category: Camilo, Opinion
Tags: Alto Comisionado de Paz, Asesinatos, Bacrim, buenaventura, Christian Aragón, Despojo para la competitividad, Paramilitarismo
Slug: dos-asesinatos-mas-por-paramilitares-que-mas-da
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/espacio-humanitario.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

**[[Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas)**

###### [26 Jul 2015] 

A menos de 150 metros de unidades de la Infantería de Marina fueron asesinados, tuiteo @justiciaypazcol sobre el atentado que le costó la vida a dos niños o adolescentes en Buenaventura, Christian Aragón y Sol Mina,  el pasado 19 de julio, a manos de paramilitares.  
Una coincidencia, letal y fatal como el asesinato hace un año de Óscar Hernández Torres, otro afrocolombiano de menos de 17 años,  asesinado en las narices de la Infantería de Marina y de la Policía.

Por lo menos estos tres asesinatos coinciden, en que había presencia de la fuerza pública, lo que ya sugiere omisión y por su repetición y contextos similares, así como sus propósitos: comisión por omisión. Es otro elemento de indicación de la existencia del paramilitarismo.

Mientras Santos continúa negando su existencia, y se arropa en las palabras de su predecesor, llamándolas BACRIM, lo cierto es que en Buenaventura, en 88 barrios de 120,  hay presencia armada con control social de estas estructuras armadas.

¿Cómo se explica esta situación en medio de una altisima presencia militar  y policial? Las posibles respuestas son pocas. O las BACRIM, grupo que controla más del 70% del municipio de los barrios son un Estado real que tolera la presencia institucional o a la inversa. Si es la segunda hipótesis esto permite comprender porque persiste allí la parapolítica con nuevas formas, y cómo no, una paraeconomía, la más oculta. No me refiero al microtráfico, ni a la extorsión ni al control del trabajo sexual en el puerto más importante de Colombia.

No, lo más grueso es la inversión privada nacional e internacional que se mantiene, supervive y se acrecienta en medio de ese terror cotidiano que padecen los afrocolombianos. Es como se ha titulado recientemente un documento de Mundubat y la Comisión de Justicia y Paz: [Despojo para la competitividad](http://justiciaypazcolombia.com/Buenaventura-El-despojo-para-la).

Detrás de las violaciones graves y sistemáticas de dh se oculta el.proyecto económico que con el Plan Maestro elaborado en dos ocasiones por una compañía del país Vasco como Esteyco definió como salida rentable para el municipio y el país, la construcción de un Muelle Turístico, justo sobre los barrios de Bajamar, en donde se encuentran, entre otros barrios, La Playita, en donde hay operaciones de control paramilitar desde por lo menos hace 7 años.

Tal Plan Maestro contempla una reubicación en el barrio San Antonio. En este proyecto de vivienda se desconocen los principios del derecho internacional sobre reubicación, y los de habitat y viviendas dignas. Todo  perfectamente es coincidente. El inversor con o sin consentimiento se beneficia del paramilitarismo.

No son tan ingenuos e inocentes los inversionistas. En uno de los negocios portuarios està la familia Parody, uno de sus integrantes, Gina, hoy es Ministra de Educación, en el gobierno que dice buscar la paz. ¿Desconoce lo qué pasa o mo quiere reconocerlo? O podemos tildar de ingenuo a Camilo Gómez, abogado de la multinacional con matriz en Cataluña, que es una de las ejecutoras del proyecto TCB en un barrio controlado por paramilitares y donde se produjo entre otras, la masacre de 12 jóvenes hace ya una década. Imposible que un ex Alto Comisionado de Paz no haya conocido qué ha pasado en Buenaventura y no sepa que sigue pasando para advertir por lo menos de la eventual mala imagen que se puede generar contra la compañía por invertir en esas condiciones de violencia.

Ese despojo para la competitividad,  para la minería y extracción petrolera, o los monocultivos o las obras de infraestructura, es lo que está detrás de las BACRIM, grupos crecientes al lado del Estado para asegurar territorios para la inversión. Por eso mismo se conversa con las FARC EP y se.pretende hacerlo con el.ELN, para asegurar negocios, no la vida fisica y digna de los nacionales.

Hay que decirlo, así no se crea, a los dirigentes colombianos, no les importan ni Christian ni Ángel, son empobrecidos, son negros, y sus vidas  no  valen, vale su suelo o el lugar de su palafito; su vida vale por lo que ha habitado, su ser no existe, ni su vida bioĺógica
