Title: ANLA aprueba licencias ambientales en el único bosque de niebla de Tolima
Date: 2018-09-07 17:33
Author: AdminContagio
Category: Ambiente, Nacional
Tags: ANLA, Bosque de Galilea, Cundinamarca, Tolima
Slug: anla-licencia-bosque-niebla
Status: published

###### [Foto: @MongabayLatam] 

###### [7 Sept 2018] 

Lo que esta ocurriendo en el Bosque de Galilea, único de niebla subsistente en el Tolima, es una 'captura corporativa' por cuenta de dos licencias ambientales otorgadas por la **ANLA** en el lugar; que, según **Julián Viña**, promotor de la Consulta Popular del Municipio de Piedras, ocurre cuando una empresa contrata al Ejército Nacional para evitar el acceso a un determinado lugar.

Viña aseguró que la máxima autoridad ambiental otorgó licencia para hacer explotación en el Bosque, "con miras a futuro de realizar explotación de Yacimientos No Convencionales", es decir, mediante 'fracking'. Según el ambientalista, mientras la empresa realiza los estudios en territorio contrató con el Ejército para realizar la **captura corporativa**, y en este momento, los habitantes de la zona no pueden ingresar al Bosque.

### **¿Por qué es importante este ecosistema?** 

Para el ambientalista sería grave que se produjera una explotación de hidrocarburos en "uno de los reductos de bosque de niebla que quedan en el país"; y confirmó que intervenirlo sería "nefasto para Colombia y para el mundo", porque **significaría dañar el corredor natural que conecta el Páramo de Sumapaz con la región de la Amazonía.** (Le puede interesar: ["Licencias, concesiones y empresas deberían retirarse de Cumaral"](https://archivo.contagioradio.com/licencias-concesiones-y-empresas-deberian-retirarse-de-cumaral/))

Adicionalmente, del interior del bosque surge el río que alimenta la Represa de Prado, en Tolima, y sus cuerpos de agua alimentan los acueductos de Villarica, Dolores, Cunday, Prado, Icononzo, Purificación en Tolima, y otros municipios en Cundinamarca. (Le puede interesar:["No a la minería en Cajamarca: Cortolima"](https://archivo.contagioradio.com/no-mineria-en-cajamarca-cortolima/))

### **¿Cómo proteger al Bosque de Galilea?** 

Viña sostuvo que las ganancias y regalías producto del extractivismo, son mínimas en contra prestación con los daños sociales y ambientales que se generan en las regiones. Sin embargo, resaltó que hay un despertar a nivel nacional, y el movimiento ambiental seguirá haciendo  marchas carnavales y movilizaciones para defender el territorio.

El ambientalista sostuvo que el despertar del movimiento ha sido liderado en buena parte desde el Tolima, por ejemplo, con las consultas populares que iniciaron en el municipio de Piedras, y que **se han realizado en otros 8 municipios y 18 más que están en curso;** por lo tanto, aseguró que la lucha ambiental se mantendrá con los recursos legales que les garantiza la constitución.

<iframe id="audio_28416786" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28416786_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
