Title: Aumento del IVA: Única propuesta del Gobierno que puso de acuerdo al Congreso
Date: 2018-11-21 13:38
Author: AdminContagio
Category: Economía, Nacional
Tags: economia, Katherine Miranda, Ley de Financiación
Slug: aumento-iva
Status: published

###### [Foto: @IvanDuque] 

###### [21 Nov 2018] 

Entre todos los Proyectos de Ley que ha propuesto el presidente Duque en sus primeros 100 días de mandato, solo uno ha logrado poner de acuerdo al Congreso: La Ley de financiamiento. Lamentablemente para el Gobierno, **todas las bancadas del Congreso están de acuerdo en que la Ley es nefasta para la clase media y popular**, y por lo tanto, debe modificarse antes de ser aprobada.

Según la **Representante a la Cámara, Katherine Miranda**, las principales modificaciones para aprobar la Ley de Financiación confluyen en invertir el sector del cual se obtendrán los recursos, de esta forma, se buscaría que la reforma tocara el bolsillo de los más ricos: empresarios, corporaciones mineras y de hidrocarburos, en lugar de ‘echar mano’ de los ingresos de la clase media, las pensiones o la renta.

Miranda especificó que, en la propuesta tal como la presentó el Gobierno, **80% de los recursos se obtendrían gravando la canasta familiar**, buscando recaudar cerca de 19 billones de pesos; cifra que es por sí misma alta, dado que **el déficit a cubrir por esta reforma es de 11, 5 billones** de pesos.  Por estas razones, ya hay radicadas en el Congreso 25 proposiciones para encontrar otra forma de obtener los recursos, evitando tocar el bolsillo de los colombianos.

### **Con solo dos propuestas, se podrían recaudar 6 billones de pesos anuales** 

Una de las propuestas de Miranda es **gravar las grandes utilidades de empresas mineras y de hidrocarburos**, teniendo un impuesto entre el 7 y 10%; tasa que equivaldría a un recaudo anual de entre **3,5 y 5 billones** de pesos. Adicionalmente, se podrían **gravar las utilidades de los más ricos accionistas del país**, dado que actualmente esos dineros no se toca, pero que si llegarán a tener un impuesto similar al de renta (del 37%), se podría conseguir **1 billón de pesos anual**.

 Al tiempo que se toman estas medidas, **podrían revisarse las exenciones de impuestos que tienen muchas empresas**, actualmente contabilizadas en 225. Gracias a estas libertades en impuestos, **el país deja de percibir 13 billones al año** (únicamente en las exenciones por renta), con el argumento de que ese dinero lo re-invierten las organizaciones para crear puestos de trabajo, pero bien ha probado el estudio económico y el caso colombiano, que eso no es así.

A ello se suma una de las grandes promesas incumplidas de la campaña Duque para la presidencia: Reducir en 50% la evasión de impuestos, y lucha contra la corrupción; rubros que si se tuvieran en cuenta, **solo en corrupción, representarían para el país 50 billones de pesos al año**,  pero que como lo denunció la Representante, son temas que están “cojos” en materia legislativa por parte del Gobierno.

La Congresista concluyó señalando que **el debate sobre esta Ley iniciará el próximo miércoles**, en un debate de comisiones conjuntas; y para que entre en funcionamiento para el 2019, debería culminar su tramite legislativo antes del 31 de diciembre. (Le puede interesar: ["La reforma tributaria de Duque: Los ricos más ricos, y los pobres más pobres"](https://archivo.contagioradio.com/reforma-tributaria-duque-ricos-pobres/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
