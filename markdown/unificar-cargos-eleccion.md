Title: ¿Qué implica unificar los periodos de alcaldes, congresistas y presidente?
Date: 2018-10-17 17:30
Author: AdminContagio
Category: Nacional, Política
Tags: Alcaldes y Gobernadores, MOE, Unificación de Periodos
Slug: unificar-cargos-eleccion
Status: published

###### [Foto: Contagio Radio] 

###### [17 Oct 2018] 

Recientemente fue aprobado por la Cámara de Representantes el Proyecto de Acto Legislativo con el que se pretende unificar el periodo de Concejales, Alcaldes y Gobernadores, con el de Congreso y Presidente. El proyecto fue objetado por diferentes sectores, porque **incluye un 'mico', que extendería el periodo de los actuales gobernantes municipales y regionales hasta 2022.**

**Camilo Mancera, integrante de la Misión de Observación Electoral (MOE)** sostuvo que la discusión que se está llevando a cabo en el Congreso tiene que ver con la unificación de los periodos y la extensión de los mismos; recordando que la Constitución de 1991 contempló la separación de los periodos para evitar que **"la burocracia invadiera los procesos electorales"**, lo que ocurriría porque al unir los comicios electorales, una sola fuerza política podría llevarse la mayoría de los votos, causando un desbalance de las ramas del poder.

Adicionalmente, aquel que tenga en su poder la administración del estado **podría poner en funcionamiento una maquinaria política que le permitiera ganar alcaldías, gobernaciones y congreso** para su partido o movimiento; hecho que se traduciría en una falla para que opere el equilibrio de poderes. (Le puede interesar: ["MOE pide compromiso de transparencia en elección de Contralor"](https://archivo.contagioradio.com/moe-pide-compromiso-de-transparencia-en-eleccion-de-contralor/))

Mancera reconoció que unificar las elecciones y extender el periodo de los cargos tendría como consecuencia una mejora en la gobernabilidad, porque indudablemente sería más fácil poner en marcha un plan de gobierno si se tiene tiempo y funcionarios públicos (alcaldes, gobernadores y senadores) favorables al mismo. (Le puede interesar: ["99 mil millones de pesos gastó Peñalosa en publicidad entre 2016 y 2017"](https://archivo.contagioradio.com/99-mil-millones-penalosa-publicidad/))

No obstante, hay hechos que para el integrante de la MOE escapan de la discusión como los efectos colaterales de una decisión como esta: por ejemplo, en el caso del Congreso que postula distintas cortes, y entes de control (procuraduría, contraloría, fiscalía, etc.)  al unificarse los periodos con el presidente, **podría elegir personas favorables a sus intereses,** modificando el sistema de pesos y contra pesos que establece la Constitución colombiana.

Mancera afirmó que **un desequilibrio tal de poderes ocurrió en 2006, con la reelección presidencial, y es un hecho que no debe repetirse.** Por lo tanto, la discusión tendría que llevarse a la ciudadanía, entender sus posibles efectos y ventajas, y tomar las discusiones de forma seria, no solo aprobando el Proyecto como ocurrió en la Comisión Primera de la Cámara de Representantes.

<iframe id="audio_29405732" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29405732_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
