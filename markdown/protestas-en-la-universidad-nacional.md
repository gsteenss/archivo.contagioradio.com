Title: Protestas en la Universidad Nacional
Date: 2015-03-17 21:11
Author: CtgAdm
Category: Fotografia, Movilización
Tags: calle 26, ESMAD, protestas nacional
Slug: protestas-en-la-universidad-nacional
Status: published

Estudiantes de la Universidad Nacional protestaron el día de hoy  17 de marzo en la ciudad de Bogotá en apoyo a los indígenas del Cauca que se encuentran en movilización hace más de 15 días y en conmemoración del asesinato del líder social Carlos Pedraza, igualmente por las elecciones a director de la universidad que se desarrollarán esta semana. Las protestan se extendieron desde las 12 p.m hasta pasado las 4 p.m con cierres momentáneos de la calle 26.

\
