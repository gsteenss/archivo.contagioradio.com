Title: Universidad Nacional se moviliza contra asesinato de Andrés Camilo Ortiz
Date: 2018-06-21 11:01
Category: DDHH, Movilización
Tags: Alejandra Lerma, marcha, Movilización, Universidad Nacional
Slug: universidad-nacional-se-moviliza-contra-asesinato-de-andres-camilo-ortiz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Jun 2018] 

El pasado viernes 15 de Junio, luego de que Andrés, en compañía de algunos amigos, desistieran de ingresar sin pagar a la estación de Transmilenio de la calle 142, fueron perseguidos por un integrante de la Policía. Como se puede apreciar en los videos difundidos por redes sociales, e**l uniformado después de abandonar un taxi e interceptar a los jóvenes, acciona su arma de fuego causando la muerte al estudiante de Contaduría de la Universidad nacional.**

<iframe style="-webkit-backface-visibility: hidden; -webkit-transform: scale(1);" src="//gifs.com/embed/mQz1Gr" width="480px" height="332.07547169811323px" frameborder="0" scrolling="no"></iframe>  
<iframe style="-webkit-backface-visibility: hidden; -webkit-transform: scale(1);" src="//gifs.com/embed/KZogzY" width="480px" height="336.15141955835963px" frameborder="0" scrolling="no"></iframe>

Por esta razón, tanto la Universidad, como la Facultad de Ciencias Económicas se han pronunciado invitando a la movilización propuesta por la representación estudiantil, para acompañar a la familia de Ortiz  con velas y camisas blancas. **Esta movilización se llevara a cabo este jueves a las 2 de la tarde, y avanzará desde la Universidad Nacional hasta la estación de Transmilenio en la calle 142.**

Por su parte, el representante de Contaduría de la Universidad Nacional, indicó que también pretenden hacer un llamado de atención a la Fiscalía y Procuraduría para que investiguen el caso, y se esclarezcan los hechos que resultaron en la muerte de Andrés Camilo. Esto, dado que los videos difundidos por redes sociales evidencian que “no hubo un debido proceso, porque el policía llego en un taxi, sólo”, y agregó, que se trata tanto del joven que fue asesinado como de sus acompañantes, quienes fueron intimidados. (Le puede interesar: ["Imputado Coronel de la Policía por asesinato de Nicolas Neira"](https://archivo.contagioradio.com/imputado-coronel-de-la-policia-por-asesinato-de-nicolas-neira/))

En las últimas horas, Jairo García, secretario de seguridad de Bogotá, indicó que el uniformado involucrado en los hechos fue separado del cargo, y el CTI ya asumió el caso. **Sin embargo, no se conocen pronunciamientos de parte de la Policía Nacional o avances en la investigación.**

<iframe id="audio_26661592" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26661592_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
