Title: Asou´wa denuncia amenazas contra líder y defensor de DDHH
Date: 2017-04-06 12:32
Category: DDHH, Nacional
Tags: Amenazas a defensores de Derechos Humanos, asou'wa, colombia, Nación Uwa
Slug: amenazas-defensor-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/images-cms-image-000037350.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Confidencial Colombia 

###### 06 Abr 2017 

Integrantes de la Nación U´wa de los departamentos de Santander, Boyacá y Norte de Santander, denunciaron a través de un comunicado las **amenazas en contra del líder y defensor de los derechos humanos José Murillo Tobo**, proferidas el día de 2 de abril del presente año.

De acuerdo con la información suministrada por Asou´wa, las amenazas **llegaron sobre las 8 de la noche a través de un mensaje al teléfono de Murillo Tobo**, por parte de un grupo que se identificó como **Águilas Negras**. El defensor se ha destacado por su trabajo en Arauca y otras poblaciones del Centro Oriente colombiano.

En esa misma fecha, el líder estuvo participando en la reunión de diálogo intercultural, en l**a sede de Asou¨wa en el municipio de Cubará**, en el que estuvieron presentes funcionarios del gobierno como el Ministro de Ambiente, el Viceministro para la participación e igualdad de derechos y el Gobernador de Boyacá, con avances positivos sobre los [acuerdos alcanzados con la comunidad](https://archivo.contagioradio.com/nacion-uwa-llega-a-un-acuerdo-con-gobirno-nacional/), **por la defensa de la vida, el territorio y los derechos de las comunidades indígenas**.

La organización hace un llamado de exigencia al gobierno nacional para que **se garantice la vida de sus líderes y liderezas,** fortaleciendo sus esquemas de seguridad, y a los mecanismos judiciales para que **investiguen pronta y efectivamente estas formas de intimidación **y a las organizaciones de derechos humanos para que estén atentos a este tipo de hechos.

El llamado enviado desde la Nación U´wa ha sido atendido por d**iferentes organizaciones, quienes en una carta se solidarizaron** levantando su voz contra tales amenazas, asegurando que "no será posible una paz en Colombia si no protegemos la vida y tenemos plenas garantías para la participación social" y extendiendo el pedido de protección al gobierno Santos.

[Apoyo a José Murillo y Mov Social en Colombia](https://www.scribd.com/document/344280579/Apoyo-a-Jose-Murillo-y-Mov-Social-en-Colombia#from_embed "View Apoyo a José Murillo y Mov Social en Colombia on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_98694" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/344280579/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-7aYPB4WN4RRMgcmQAV2k&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
