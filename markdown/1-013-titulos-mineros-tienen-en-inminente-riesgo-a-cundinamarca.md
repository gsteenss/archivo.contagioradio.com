Title: 1.013 títulos mineros tienen en inminente riesgo a Cundinamarca
Date: 2016-07-08 15:05
Category: Ambiente, Nacional
Tags: efectos de la minería, impactos minería, Minería en Colombia, Minería en Cundinamarca
Slug: 1-013-titulos-mineros-tienen-en-inminente-riesgo-a-cundinamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Minería-300x240.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CundiTV] 

###### [8 jul 2016]

Tras denuncias de las comunidades, la Defensoría del Pueblo realizó visitas de verificación a los municipios de Soacha, Sibaté y Choachí, y constató **graves afectaciones a ecosistemas estratégicos, déficit en las fuentes de agua, incumplimientos en la aplicación de los planes de manejo ambiental**, cambios en la vocación de los suelos, daños en la infraestructura habitacional y ruptura del tejido social, a causa de la explotación minera que afecta al departamento de Cundinamarca.

La información recopilada confirma que "Cundinamarca cuenta con 1.013 títulos mineros vigentes, 1.062 solicitudes de contrato de concesión y casi 270 solicitudes de legalización", lo que permitiría concluir que el departamento contará con **un total de 2.344 títulos en una extensión aproximada de 912.846 hectáreas** y con vigencia de hasta treinta años. Según la Agencia Nacional Minera, "los municipios cundinamarqueses con más títulos mineros son Nemocón con 96, Soacha con 60 y Lenguazaque con 55, mientras que los de menos son Yacopí con 50, Guaduas con 45 y Ubalá con 40.

En veredas como [[San Jorge](https://archivo.contagioradio.com/vamos-por-la-consulta-popular-vereda-san-jorgue-de-suacha/)], El Peñón y San Miguel, la actividad minera se realiza a los 3.250 y 2.800 metros sobre el nivel del mar, en ecosistemas ambientalmente estratégicos, y que deberían tener mayor protección por sus características de páramo, como ocurre en la zona rural de Choachí, en la que la **minería se lleva a cabo en inmediaciones del páramo de Cruz Verde y el Parque Nacional Natural del Sumapaz**.

Para la Defensoría del Pueblo son impactantes las cifras y problemáticas identificadas, y mucho más cuando la minería ocurre en el marco de la **ausencia institucional para realizar controles estrictos** que permitan, entre otras, reparar las áreas ya intervenidas y a las comunidades por los efectos devastadores que las han perjudicado.

https://www.youtube.com/watch?v=NVROEMzgMD8

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
