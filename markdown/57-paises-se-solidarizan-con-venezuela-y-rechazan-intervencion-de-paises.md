Title: 57 países se solidarizan con Venezuela y rechazan intenciones de intervención
Date: 2017-08-04 15:49
Category: El mundo
Tags: Asamblea Nacional Consituyente, Nicolas Maduro, Venezuela
Slug: 57-paises-se-solidarizan-con-venezuela-y-rechazan-intervencion-de-paises
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Venezuela-Maduro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: albaciudad.org] 

###### [04 Ago. 2017] 

En el Consejo de Derechos Humanos que se reunió en Ginebra, Suiza, cerca de 57 estados han firmado una declaración conjunta en la que respetan a Venezuela como Estado soberano, apoyan las acciones del presidente Nicolás Maduro de convocar el diálogo y la paz en ese país, e instan a las naciones a no intervenir en sus asuntos internos.

En la declaración conjunta que contiene ocho puntos dicen que es necesario así mismo considerar que “**es al pueblo venezolano a quien compete, exclusivamente, determinar su futuro sin injerencias** externas”. Le puede interesar: [Con triunfo de la Constituyente en Venezuela "fracasó el saboteo" Atilio Borón](https://archivo.contagioradio.com/44468/)

Razón por la cual, dicen estas 57 Naciones consideran importante **recordar a la comunidad internacional que “debe fomentar las capacidades y proporcionar ayuda** técnica tomando como base la solicitud del País concernido, para tratar los desafíos de derechos humanos del país”. Le puede interesar: [Venezuela rechazó informe de la OEA](https://archivo.contagioradio.com/37789/)

### **“Saludamos los esfuerzos realizados para buscar el diálogo”** 

En la misiva **celebran los esfuerzos realizados en pro del diálogo político y la paz por la Unión de Naciones Suramericanas (UNASUR)** y los ex presidentes José Luis Rodríguez Zapatero, de España; Martín Torrijos de Panamá; y Leonel Fernández, de República Dominicana, junto con el Enviado Especial de la Santa Sede.

Dicen además que **“apoyamos la incorporación de países de América Latina y el Caribe al fomento del diálogo político en Venezuela**, a saber: El Salvador, Nicaragua, República Dominicana, Uruguay y los Estados miembros de la CARICOM”. Le puede interesar:[ "Venezuela respeta los poderes constitucionales y desvirtúa golpe de Estado"](https://archivo.contagioradio.com/venezuela-respeta-los-poderes-constitucionales-y-desvirtua-golpe-de-estado/)

Dentro de los países que firmaron esta declaración de apoyo y llamado se encuentran Rusia, China, India, Sudáfrica, Irán, Vietnam, Argelia, Egipto, Jordania, Siria, Ecuador, Bolivia, Cuba, Nicaragua, Burundi, Zimbabue, Myanmar, Timor Leste, Tayikistán, Omán, República Democrática del Congo, Nigeria, Guinea Ecuatorial, Mauritania, Mozambique, Togo y Venezuela.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
