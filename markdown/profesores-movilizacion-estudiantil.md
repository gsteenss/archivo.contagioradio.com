Title: Profesores se suman a la gran movilización estudiantil del 10 de octubre
Date: 2018-10-08 15:10
Author: AdminContagio
Category: Educación, Movilización
Tags: Déficit, educacion, marcha, profesor, universidad
Slug: profesores-movilizacion-estudiantil
Status: published

###### [Foto: Contagio Radio] 

###### [8 Oct 2018] 

**El déficit presupuestario que impediría a las Instituciones de Educación Superior (IES) finalizar el año es de 1,4 billones**, situación que se suma a a la crisis en infraestructura y a la deuda histórica con las Universidades, calculada en 13 billones de pesos. Estos hechos llevaron a la movilización estudiantil del próximo miércoles 10 de octubre, que también acompañarán los profesores.

**Según Pedro Hernández, presidente de la Asociación Sindical de Profesores Universitarios (ASPU),** todo el movimiento actual lo ha generado el déficit financiero de todas las IES porque "el Gobierno ha mantenido un crecimiento del 1% anual en promedio del presupuesto para la educación, mientras el número de estudiantes matriculados desde 2.000 a la fecha ha subido más del 130%".

Hernández sostuvo que por esta relación desigual entre la cobertura de la educación y su financiación, **la inversión del Estado por estudiante se ha reducido de 10 a 4 millones de pesos anuales;** todo ello, mientras la estructura física de los centros de educación se mantiene igual, o sufre deterioros en algunos casos. Situación que se traduce en una grave afectación para la calidad de la educación, porque "hay estudiantes que toman clases desde las ventanas".

Los profesores tampoco escapan de la crisis, pues como lo refirió el director de ASPU, **entre el 60 y el 70% de los profesores no tienen estabilidad laboral**: "Tienen contratos durante solo 8 meses al año, lo que significa que las familias de los docentes viven 4 meses sin seguridad social". (Le puede interesar: ["La plata del Estado no se puede invertir solamente en carreteras"](https://archivo.contagioradio.com/la-plata-del-estado-no-se-puede-invertir-solamente-en-carreteras-profesores/))

### **La Universidad de Antioquia tienen 120 mil millones de déficit** 

El profesor afirmó que mientras **la Universidad Nacional tiene un déficit de 60 mil millones, la Universidad de Antioquia la duplica;** por lo que ambas instituciones no podrían cubrir los últimos meses de salario del año y mucho menos las prestaciones sociales de los docentes. (Le puede interesar: ["Universidad Nacional necesita presupuesto para seguir siendo pública"](https://archivo.contagioradio.com/presupuesto-un-regalo-urgente-para-la-u-nacional-en-sus-150-anos/))

Para palear esos gastos, Hernández recordó que las Instituciones en otras ocasiones han pedido adelantos de las vigencias futuras que les corresponden, pero **en esta ocasión, habría que pedir 3 adelantos del presupuesto, haciendo que el 25%  del dinero destinado para el 2019 sea usado para cubrir la deuda del 2018.** Esto significaría que las Universidades terminarían paradas a mitad del siguiente año por falta de recursos.

### **Plata si hay, voluntad...** 

Hernández aseveró que en 2016, cuando se aprobó la Ley 1819 de reforma tributaria se incluyeron los artículos 102, 142 y 184, con los cuales se ordenaba que las IES recibieran con destinación específica. Los artículos se referían al IVA social, el impuesto a la renta y un impuesto a las cooperativas que **recaudarían un total de 1,4 billones de pesos.** Sin embargo, el Profesor declaró que esos recursos no han llegado a las Universidades, sino que se han destinado para cubrir el programa Ser Pilo Paga (SPP).

Adicionalmente, el docente aseguró que gracias a la bonanza petrolera, p**or cada dólar que aumenta el barril de petróleo se estima que entran entre 350 y 450 mil millones de pesos anuales a Colombia**; dinero con el que se podría garantizar la finalización de SPP e incrementar los presupuestos de inversión para solventar la crisis de la Educación Superior Pública. (Le puede interesar: ["Estamos 'Ad portas' de un gran paro estudiantil en Colombia"](https://archivo.contagioradio.com/gran-paro-estudiantil/))

### **La educación superior pública está en cuidados intensivos** 

Hernández sostuvo que el trabajo de los estudiantes y los profesores es como el de los médicos: Diagnosticar y prevenir las enfermedades, en lugar de intentar curarlas cuando están en la fase terminal. No obstante, **en la Ley de Presupuesto General de la Nación no se ha respondido al diagnóstico**, y en caso de no ser atendidos, se verían obligados a paralizar totalmente las actividades en las universidades.

Para sacar a la educación superior del estado en que se encuentra, el movimiento estudiantil presentó un pliego de peticiones con el que piden mayor presupuesto para la educación e investigación. (Le puede interesar: ["Las 10 exigencias del movimiento estudiantil al Gobierno"](https://archivo.contagioradio.com/exigencias-movimiento-estudiantil/))

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
