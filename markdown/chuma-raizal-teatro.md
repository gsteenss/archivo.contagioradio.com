Title: Chuma Raizal, la cosmogonía Yoruba en el teatro
Date: 2017-10-06 17:34
Category: eventos
Tags: Bogotá, teatro
Slug: chuma-raizal-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/CHUMA-RAIZAL_BR-min.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Q¨anil 

###### 5 Oct 2017

La Corporación Colombiana de Teatro presenta "Chuma Raizal", una obra del grupo Q´ anil Danza Contemporánea, bajo la dirección de Nicolás Maldonado Guerra, que se presenta en la Sala Seki Sano de Bogotá.

Esta pieza teatral se orienta en la recuperación de la cosmogonía Yoruba, las manifestaciones musicales, dancísticas afro y de la reflexión sobre la herencia cultural de nuestro tiempo.

Chuma Raizal se divide en 5 actos, donde se integran elementos míticos sobre la creación del mundo Yoruba, trama narrativa de 5 personajes, quienes sugieren preguntas sobre la imperfección humana, la sexualidad, las espiritualidades y la individualidad contemporánea.

Chuma Raizal tiene una duración de 40 minutos y se presentara los días 5, 6 y 7 octubre a las 7:30 p.m, la boletería tiene un costo de 12.000 para estudiante y 20.000 para público general en la taquilla del teatro.

<iframe src="https://www.youtube.com/embed/OmyOfDA3Wfc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Si desea saber más información puede consultar la página oficial de la La Corporación Colombiana de Teatro.
