Title: Alfredo Molano un caminante de la verdad
Date: 2019-10-31 12:32
Author: CtgAdm
Category: Nacional, Paz
Tags: Alfredo Molano, comision de la verdad, periodismo en colombia
Slug: alfredo-molano-un-caminante-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Alfredo-Molano.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Comisión de la Verdad] 

En horas de la madrugada de este 31 de octubre y tras hacer frente a una larga lucha contra el cáncer, falleció a sus 75 años Alfredo Molano Bravo, periodista, sociólogo y comisionado de la verdad. Un hombre que durante más de 50 años recorrió las zonas rurales del país en búsqueda de historias que recopiladas e**n su extensa bibliografía permitió desentrañar el surgimiento del conflicto armado en Colombia.**

A través de su obra, Molano no solo dibujó una cartografía de la reivindicación, luchas campesinas y el extenso conflicto que se ha vivido en Colombia por el territorio, estos mismos relatos lo llevaron incluso a ser objeto de amenazas del narcotráfico y el paramilitarismo.  [(Le puede interesar: “La condición básica de no repetición es que el Gobierno enfrente al paramilitarismo”)](https://archivo.contagioradio.com/la-condicion-basica-de-no-repeticion-es-que-el-gobierno-enfrente-al-paramilitarismo/)

Su amplia trayectoria como investigador  y cronista le permitió dar a conocer mediante sus escritos la violencia, el desplazamiento forzado y las problemáticas que se viven en la Colombia rural, conocimiento que supo compartir y analizar y que lo postuló para ser parte de la Comisión por el Esclarecimiento de la Verdad, r**ecogiendo los testimonios de la Orinoquía y la Amazonía.**

"A lo largo de su vida y durante su tiempo de trabajo con la Comisión de la Verdad, Alfredo recorrió el país hasta sus rincones más profundos e hizo aportes invaluables, críticos y valientes a la comprensión y solución de los problemas sociales, económicos  políticos del país" manifestó el padre Francisco de Roux, director del lugar al que el maestro Molano dedicó sus últimos años de vida.

A su vez, el padre de Roux en nombre de los comisionados y comisionadas,  afirmó que la Comisión seguirá trabajando por el esclarecimiento del conflicto armado y así **"poner las bases para la construcción de un país en paz  con justicia como lo soñó Alfredo".** [(Lea también: Hay que abandonar los escritorios, estar cerca del mundo: Alfredo Molano)](https://archivo.contagioradio.com/hay-que-abandonar-los-escritorios-estar-cerca-del-mundo-alfredo-molano/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
