Title: Organizaciones piden incluir la presencia de la ONU en acuerdos de paz
Date: 2016-02-08 12:23
Category: Nacional, Paz
Tags: CCEEUU, Conversaciones de paz de la habana, Oidhaco, WOLA
Slug: organizaciones-piden-incluir-la-presencia-de-la-onu-en-acuerdos-de-paz
Status: published

###### Foto: elpais. 

###### 08 Feb 2016

Un grupo de 28 organizaciones con presencia en diversos países, pidió a la mesa de conversaciones de paz de la Habana, dejar en firme, como un acuerdo, la **presencia de la oficina de las Naciones Unidas para los Derechos Humanos en Colombia**, luego de la firma de los acuerdos como una garantía para el monitoreo de la situación de DDHH, que podría agravarse una vez firmados los acuerdos.

Las organizaciones afirman que los años posteriores a la firma de un acuerdo pueden ser “años difíciles en lo relativo a los registros de violencia y de violaciones a los derechos humanos”  y por ello es importante **la presencia de la oficina y con un mandato claro en cuanto a asesoramiento técnico, vigilancia de la situación de DDHH en Colombia y también la presentación de un informe anual en la asamblea de ONU en Ginebra.**

Otro de los puntos que resaltan las organizaciones gira en torno a la presencia de [organizaciones internacionales de Derechos Humanos](https://archivo.contagioradio.com/?s=onu) y en ese sentido solicitan que el gobierno colombiano respalde la acción de ese tipo de organizaciones y facilite tanto su presencia como su labor “respalde y ofrezca todas las garantías a estas organizaciones internacionales para ejercer su labor de prevención y protección a comunidades y organizaciones en Colombia.”

Entre las organización que hacen la petición se encuentran ABColombia, ACAT France, ask, ASOC-Katío, Associació Catalana per la Pau, Broederlijk Denle, Caravana Internacional de Juristas, CEAR Euskadi, Christian-Aid, Colombia Solidarity Campaign, CCEEU, CVSC, CEAR-PV, CEDSALA, Coordinadora de ONGD del Principado de Asturias, DKA Austria, kfb Austria, Fokus, Forum Syd, Fundación Sueca para los Derechos Humanos, Intermon Oxfam, kolko - derechos humanos por Colombia, Latin America Working Group, Misereor, Mundubat, OIDHACO, Pachakuti, PBI Colombia, Taula Catalana por la Paz y los Derechos Humanos en Colombia y WOLA.
