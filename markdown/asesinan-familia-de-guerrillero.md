Title: Asesinan en Putumayo a familiares de guerrillero de las FARC
Date: 2017-05-15 13:03
Category: Nacional, Paz
Tags: asesinato, Familiares, FARC, Putumayo
Slug: asesinan-familia-de-guerrillero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Zona-Veredal-Brisas-Curvarado-7.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [15 May. 2017]

En el departamento de Putumayo, se conoció del **asesinato de dos familiares** del guerrillero de las FARC conocido como Fabián García, así lo aseguraron las personas de la Vereda Teteyé del municipio de Puerto Asis.

**Fabián es integrante de la Zona Veredal Transitoria de Normalización La Pradera,** ubicado en ese departamento. Le puede interesar: [En 2017 han sido asesinados 41 líderes sociales](https://archivo.contagioradio.com/en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales/)

Este no es el primer caso que se presenta, **el pasado 16 de abril el Estado Mayor de las FARC aseguró que en las últimas semanas han sido asesinados** en Nariño, Luis Alberto Ortiz además beneficiario de la Ley Amnistía. Le puede interesar: [José Yatacúe, integrante de las FARC-EP fue asesinado en Toribio, Cauca](https://archivo.contagioradio.com/jose-huber-yatacue/)

**En Chocó fueron asesinados los hermanos Dalmiro Cárdenas y Anselmo Cárdenas,** hermanos del guerrillero de las Farc, Robinson Victorio recluido en la Cárcel de Chiquinquirá. Le puede interesar: [Asesinado guerrillero de las FARC que había sido beneficiado con ley de amnistía](https://archivo.contagioradio.com/asesinado-guerrillero-de-las-farc-que-habia-sido-beneficiado-con-ley-de-amnistia/)

**Para el 27 de abril se denunció el ataque contra la familia de otro integrante de esa guerrilla**, también beneficiado con el indulto en Tarazá Antioquia. Tres familiares del guerrillero Carlos que se encuentra en Remedios murieron durante el ataque[**, entre los que se encontraba una menor de 14 años. **]

Al finalizar esta nota, no se conoce ningún pronunciamiento por parte del Mecanismo de Monitoreo sobre los hechos sucedidos, ni de la guerrilla de las FARC.

Noticia en Desarrollo…
