Title: Corte Penal Internacional podrá juzgar crímenes contra la naturaleza
Date: 2016-09-27 17:15
Category: Ambiente, Judicial
Tags: Ambiente, Corte Penal Internacional, guerra, lesa humanidad
Slug: corte-penal-internacional-podra-juzgar-crimenes-contra-la-naturaleza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/corte_penal_internacional_-e1475014678188.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona cero 

###### [27 Sep 2016] 

Los delitos ambientales ahora serán juzgados por la Corte Penal Internacional, el organismo que juzga los casos de crímenes de guerra y de lesa humanidad. Así lo informó esa Corte mediante un documento en el que se explica que los gobiernos y personas que cometan algún delito  contra la naturaleza serán juzgados por la legislación internacional.

El documento señala que “El impacto de los crímenes puede evaluarse a la luz de, entre otras cosas, el aumento de la vulnerabilidad de las víctimas, el terror causado en consecuencia, o el daño social, económico o medioambiental causado a las comunidades afectadas. En este contexto, la Oficina prestará especial consideración a la persecución de los crímenes comprometidos a través del Estatuto de Roma o **que tengan como resultado la destrucción del medio ambiente, la explotación ilegal de los recursos naturales o el despojo ilegal de las tierras**”.

La medida que ha sido celebrada las organizaciones sociales y ambientalistas, explica que el acaparamiento de tierras o la explotación ilegal de los recursos naturales podrán ser juzgados por el ese tribunal internacional que ha tomado esa decisión debido a que ese tipo de crímenes conllevan impactos negativos sobre las comunidades, y por ende se pueden juzgar como crímenes contra la humanidad.

Frente a este tema, es importante recordar que los asesinatos contra defensores del ambiente en el mundo continúan incrementando, y durante el 2015, la cifra llegó a su punto más alto. De acuerdo con el más reciente informe de la** **ONG, Global Witness **al menos 185 activistas fueron asesinados durante el año pasado, es decir tres por semana, de los cuales el 66% de las víctimas son latinoamericanas, además esa organización **afirma que la impunidad de estos casos es alarmante pues llega a un 90% del total de los casos.

Los defensores de la tierra, los bosques, los ríos y los animales fueron asesinados en su mayoría por la minería (en 42 casos), la agroindustria (20), la tala (15) y los proyectos hidroeléctricos (15). Frente a eso, Margarita Flores, directora de la Asociación Ambiente y Sociedad, resalta que “**El ambientalismo ejercido como una causa en defensa del ambiente constituye ahora un motivo de muerte,** en vez de resaltarlo como un ejercicio de ciudadanía y defensa de derechos humanos y ambientales”.

Además frente a la problemática de tierras, Gillian Caldwell, director ejecutivo de Global Witness, indica que “Expulsar a las comunidades de sus tierras y destrozar el medio ambiente se ha convertido en una forma aceptada de hacer negocios en muchos países pobres, pero ricos en recursos. La decisión de la CPI muestra que la impunidad está llegando a su fin. Los directivos de las empresas y los políticos cómplices del empoderamiento violento de la tierra, arrasando bosques tropicales o envenenado los recursos de agua, pronto podrían verse sometidos a juicio en La Haya junto a criminales de guerra y dictadores”.

Sin embargo, por el momento **la CPI sólo puede juzgar a personas físicas,** pues actualmente no existen instancias jurídicas internacionales ante las que denunciar a las empresas por los casos de violaciones de derechos humanos o al ambiente.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
