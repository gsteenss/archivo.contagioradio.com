Title: Tras 16 años Estado pedirá perdón por masacre y desplazamiento del pueblo Wiwa
Date: 2018-07-16 12:48
Category: DDHH, Nacional
Tags: #NoSirvoALaGuerra, 2 mil personas masacre Nigeria Charlie Hebdo, indígenas
Slug: estado-perdon-wiwa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/pueblo-wiwa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CAJAR 

###### 16 Jul 2018 

El próximo 19 de julio, el Estado colombiano representado por el Ministro de Justicia, realizará un **acto de pedido de perdón y reconocimiento de responsabilidad por la masacre y el desplazamiento de una comunidad del pueblo Wiwa** del sur de la Guajira, cometida por grupos paramilitares en el año 2002.

El fallo proferido en segunda instancia por el Tribunal Administrativo de la Guajira el 29 de marzo de 2016, **reconoció el asesinato de 16 integrantes de la comunidad de la vereda "El Limón"**, corregimiento de Caracolí Sabanas de Manuela, en San Juan del Cesar **y el desplazamiento de cientos más, por la incursión de por lo menos 200 hombres armados**.

Según los testimonios aportados en las investigaciones frente al caso, e**l 1ro de septiembre de 2002 durante la incursión los hombres del Bloque Norte de las Autodefensas Unidas de Colombia** (AUC), asesinaron niños, adolescentes e indígenas, **quemaron 15 viviendas, torturaron y desaparecieron**. También se informó sobre rockets y cilindros arrojados en contra de la población civil.

Gracias al material probatorio, **el tribunal declaró la responsabilidad de la nación**, particularmente del Ministerio de Defensa por el desplazamiento forzado, **al desconocer sus obligaciones de garantía y protección al pueblo Wiwa**, permitiendo la violación de sus derechos fundamentales como población objeto de protección especial por tratarse de comunidades indígenas (Le puede interesar: [121 familias se encuentran confinadas por paramilitares: Pueblos indígenas](https://archivo.contagioradio.com/121-familias-se-encuentran-confinadas-por-paramilitares-pueblos-indigenas/))

En el  falló se reconoce que la **fuerza pública tenía posibilidades de intervenir en el desarrollo de los hechos**, al tener conocimiento sobre los actos de violencia que podrían ocurrir; advertidos por las denuncias que la comunidad y la Defensoría del Pueblo habían realizado por medio del Sistema de Alertas Tempranas durante el año 2002.

Aemás, en el curso de la investigación se escucharon las declaraciones de desmovilizados de las autodefensas, como José Luis Angúlo, quien **admitió que el ejército escoltó a los paramilitares, no les opusieron resistencia y facilitaron su huida tras la masacre**; versión que coincide con la de José Gregorio Álvarez de las AUC y con testimonios de las víctimas quienes **aseguraron que en la zona había presencia permanente de efectivos militares**.

El fallo del Tribunal, hace parte de las decisiones tomadas frente a hechos de violencia y graves violaciones a los derechos humanos ocurridas en la región, donde s**e concluye que el Estado incurrió en una falla al no proveer la suficiente seguridad a la comunidad de "El Limón"**.

###### **Reciba toda la información de Contagio Radio en [[su correo]
