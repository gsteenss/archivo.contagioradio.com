Title: Cuatro indígenas Embera Chamí completan 5 días desaparecidos
Date: 2017-03-13 13:16
Category: DDHH, Nacional
Tags: desaparición, Embera Chami, indígenas
Slug: cuatro-indigenas-embera-chami-completan-5-dias-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/indigenas-desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Ma. 2017] 

Desde este 8 de marzo **se encuentran desaparecidos 4 indígenas Embera Chamí, dentro de los cuales hay dos menores de edad**, todos integrantes de la comunidad de la Esperanza, en el municipio de Alcalá en el Valle del Cauca, así lo denunciaron las Autoridades indígenas de esta comunidad.

Hasta el momento se desconocen los móviles de esta desaparición, pero las organizaciones han denunciado que en la zona desde hace más de 6 meses se vienen dando **amenazas de muerte por parte de Paramilitares a líderes indígenas del Norte del Cauca y del Valle.**

Los hechos sucedieron cuando **5 miembros de esta comunidad salieron de sus casas para dirigirse a la finca el Edén, sitio donde los indígenas habitualmente realizan labores de pesca.** Los 2 adultos y 3 menores de edad caminaron hacia este lugar que queda aproximadamente a 20 minutos de la cabecera municipal. Al llegar al lugar uno de los menores optó por regresarse a su comunidad.

Los 4 indígenas restantes continuaron su labor de pesca en el sitio, según lo relato a su comunidad el menor que se regresó antes “manifestó el niño que se quedaron pescando en el lago, en la finca el Eden” dice el comunicado. Le puede interesar: [Asesinado Eder Cuetia Conda, líder defensor de derechos humanos de Cauca](https://archivo.contagioradio.com/asesinado-eider-cuetia-lider-de-derechos-humanos-36907/)

La autoridad indígena en compañía de la guardia realizó una inspección al lugar donde fueron vistos por última vez lo 4 indígenas, **hallando en el sitio “evidencias como objetos o pertenencias que ellos llevaban como anzuelos,** el tarro donde llevaban las lombrices y el flotador, tirados en el suelo” reza la comunicación.

**Pese a las denuncias que han sido instauradas por la desaparición de estos 4 indígenas** ante la Fiscalía General de la Nación de Cartago, Valle, **hasta el momento no ha habido respuesta.** Razón por la cual las autoridades indígenas y la guardia se han declarado en Asamblea Permanente en el sitio, hasta tanto que no haya claridad al respecto.

Así mismo, han **exigido al Gobierno nacional medidas de protección urgentes para la comunidad**, así como una respuesta eficaz por parte de la Fiscalía General de la Nación para que pueda investigar y dar con el paradero de los 4 indígenas y de los autores intelectuales y materiales de este hecho. Le puede interesar:[ Situación de derechos humanos en Colombia "una preocupación mundial"](https://archivo.contagioradio.com/situacion-de-derechos-humanos-una-preocupacion-mundial/)

Además, han instado a las organizaciones internacionales y de la sociedad civil aúnen voces de **exigencia al Estado colombiano para que realicen las acciones necesarias para garantizar y proteger los Derechos de los Pueblos Indígenas** y la misión humanitaria que se encuentra en asamblea permanente.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
