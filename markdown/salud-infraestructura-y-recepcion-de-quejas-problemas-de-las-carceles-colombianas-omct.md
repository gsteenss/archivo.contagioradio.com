Title: Persiste la tortura en establecimientos carcelarios de Colombia
Date: 2015-11-13 18:06
Category: DDHH, Nacional
Tags: Cárceles colombianas, CAT, Crisis carcelaria en Colombia, Hacinamiento en Colombia, Organización Mundial Contra la Tortura
Slug: salud-infraestructura-y-recepcion-de-quejas-problemas-de-las-carceles-colombianas-omct
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/elsalmonurbanologspot.com_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:elsalmonurbanologspot.com 

<iframe src="http://www.ivoox.com/player_ek_9418699_2_1.html?data=mpmempudfY6ZmKialJWJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMbm1M7g1sqPsMKf1dTf1trWpYzZz5DS1dnFps3ZxM7ay8rSuNDnjMjO1MjJsMLmytTgjcnJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Helena Solá] 

###### [13 Nov 2015]

La Misión Internacional de la Organización Mundial Contra la Tortura (OMCT) **visitó desde el 9 de noviembre hasta el 13 de noviembre los centros carcelarios del país para hacer seguimiento a una serie de recomendaciones** que dio el Comité Contra la Tortura de Naciones Unidas (CAT) frente a la distintas formas de tortura en el país.

La OMCT visitó** la cárcel **Eron Picota en Bogotá y San Isidro en Popayán. Helena Solá, coordinadora de la OMCT, indicó que se **ha instado a Colombia a tomar medidas de forma prioritaria**, en temas puntuales como **el derecho a la salud de las personas privadas de la libertad, que es un tema crítico** y urgente, "un ejemplo de ello es el caso de personas que  están a punto de perder miembros por falta de atención y negligencia del sistema", señala Solá.

Una de las recomendaciones  está  dirigida a **asegurar que exista un sistema de salud accesible cuando los presos lo requieran**, además se recomienda la implementación de la L**ey 1709 de 2014** por la cual se obliga a las cárceles a tener sus propios centros médicos con atención no solo para temas ambulatorios, sino para afectaciones graves de salud, y en donde también se puedan realizar cirugías básicas y de nivel medio.

Según el **INPEC el 56% de las cárceles del país pr**esenta altos niveles de hacinamiento, hecho que se ha denunciado desde tiempo atrás ante distintas autoridades y medios de comunicación.

Solá manifiesta que en los centros carcelarios existe la “necesidad de tener mecanismos eficaces de investigación que permitan revertir la impunidad” ya que **no tienen un sistema de recepción de quejas adecuado** y estas son recibidas por la policía judicial, lo que genera que los casos de abuso no lleguen a la Fiscalía. Los presos denuncian “supuestas represalias” cuando se hacen las denuncias, tema que se debe investigar más a fondo por las autoridades.

Helena Solá asegura que pese a que la OMCT no tiene una “capacidad punitiva” ya que su función es de supervisión, **Colombia debe presentar en mayo del 2016 un informe en que se evidencie si se atendieron o no las recomendaciones hechas por las Naciones Unidas** en materia de salud, infraestructura y sistema de recepción de quejas en las cárceles.
