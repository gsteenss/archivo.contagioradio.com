Title: 413 mujeres fueron víctimas de feminicidio en los últimos 3 años
Date: 2017-05-22 18:32
Category: Mujer, Nacional
Tags: asesinatos de mujeres, feminicidio, feminismo, mujeres
Slug: 413-mujeres-han-sido-victimas-de-feminicidio-en-colombia-en-los-ultimos-3-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/feminicidios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [22 May. 2017] 

En los últimos 3 años se han conocido de 413 casos de feminicidios en Colombia o de tentativa de feminicidio, de los cuales **276 mujeres se vieron enfrentadas a crímenes de odio por su condición de ser mujer.** Así lo manifestó La Fiscalía General de la Nación en un reciente informe, en el que además añadieron que solo en lo que va corrido del 2017 se conocieron 50 casos.

Adicionalmente, asegura el Instituto de Medicina Legal que durante el año 2016 más de **3 mil mujeres sufrieron violencia de género,** lo que las deja aún más expuestas a ser víctimas de feminicidio. Le puede interesar: ["Tenemos un Estado fracasado, habla de paz y permite violencia contra mujeres y niñas"](https://archivo.contagioradio.com/tenemos-un-estado-fracasado-que-habla-de-paz-y-permite-la-violencia-contra-mujeres-y-ninas/)

Para la **jurista, feminista y poeta Isabel Agatón, estas cifras dan cuenta de una realidad que se está empezando a conocer** y recuerda como hace 2 años se aprobó la Ley Rosa Elvira Celis, reconociendo que las mujeres son asesinadas “fundamentalmente” en el ámbito de las relaciones de pareja.

“Cuando ellas deciden terminar una relación de pareja o divorciarse o cuando demandan al agresor o progenitor por violencia intrafamiliar o por alimentos, por ejemplo. Pero también se hace necesario reconocer que, **si bien ese es el principal escenario, no es el único”** añadió Agatón.

### **La ley no soluciona todo** 

Según este informe, en materia jurídica de **52 procesos abiertos por feminicidio, solo el 12% han dado como resultado una condena** y otros 122 se encuentran en etapa de juicio, para los restantes 122 apenas se han formulado cargos contra los feminicidas. Le puede interesar: [Colombia se disputa segundo lugar en Feminicidios en América Latina](https://archivo.contagioradio.com/colombia-se-disputa-segundo-lugar-en-feminicidios-en-america-latina/)

“La ley cumple un papel, pero **no se le puede atribuir a la ley y a las leyes toda la responsabilidad de la prevención del feminicidio** y de las violencias contra las mujeres ni otras formas de discriminación, dado que es un fenómeno cultural, producto de las sociedades patriarcales” añadió Agatón.

### **Insistir en trabajar en lo cultural** 

Para la jurista y poeta feminista no hay que olvidar que un conjunto de hombres se han sentido históricamente dueños de la libertad, del cuerpo y de la autonomía de las mujeres, por lo cual **es necesario hacer ejercicios de deconstrucción de esos mensajes e insistir en posicionar el mensaje de que hay que comprometerse con la vida de las mujeres.**

“Hay que comprometerse con la autonomía, con la libertad de las mujeres y eso es una responsabilidad de todas las instituciones y de la sociedad, empezando por la formación que se les está dando a las niñas y niños desde el colegio, para que no reproduzcan mensajes que se ven en el cine, en las novelas, en la música, que es lo que está sucediendo” manifiesta Agatón. Le puede interesar: [La música reivindica los derechos de las mujeres](https://archivo.contagioradio.com/el-feminismo-en-la-musica/)

### **Más educación para evitar feminicidios** 

Para Agatón, una de las primeras herramientas a las que puede acudir la sociedad para prevenir los micromachismos, violencias o desenlaces fatales, es **formar a las mujeres desde pequeñas como mujeres libres** “que amen la libertad y la autodeterminación, tanto que sean capaces de advertir cualquier expresión disfrazada de cariño como un mecanismo para ejercer el poder y el control sobre las mujeres”.

Manifiesta la feminista que “se nos ha enseñado – a las mujeres - a través del tiempo que los celos son una expresión de amor, es que la quiere tanto que la recoge en la oficina, en el colegio, me revisa el celular y esta es una forma equivocada de ver el amor y las relaciones que estamos construyendo no solo con hombres sino con todas las personas, **el poder no es muestra de amor”.**

<iframe id="audio_18834780" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18834780_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="ssba ssba-wrap">

</div>
