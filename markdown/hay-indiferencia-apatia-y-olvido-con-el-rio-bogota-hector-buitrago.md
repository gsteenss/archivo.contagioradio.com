Title: "Hay indiferencia, apatía y olvido hacia el río Bogotá" Héctor Buitrago
Date: 2015-05-12 15:56
Author: CtgAdm
Category: Ambiente, Otra Mirada
Tags: 12 de mayo, ambientalistas, Aterciopelados, Bogotá, contaminación, Héctor Buitrago, Humedal La Conejera, río Bogotá
Slug: hay-indiferencia-apatia-y-olvido-con-el-rio-bogota-hector-buitrago
Status: published

###### [Foto: www.revistalima.com.ar] 

<iframe src="http://www.ivoox.com/player_ek_4484609_2_1.html?data=lZmllpuUfY6ZmKiak5WJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmqcbmjc7SqMraxtfS0MjNpYampJDO0sbYaaSnhqaxw5Ddb9Dg187R0ZDHs8%2BfxtGY1Iqnd4a1pdSYpNTLs9WZpJiSo5aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Héctor Buitrago, Aterciopelados] 

Este martes 12 de mayo se celebra el día del Río Bogotá, que de acuerdo a la Contraloría General de la República **necesita aproximadamente \$9.3 billones de pesos para recuperarse,** lo que quiere decir que se registra un **47% más** de lo que se había presupuestado en el documento Conpes de 2004, cuando se tomó la decisión de descontaminar el río más importante de Bogotá.

Así mismo, en el Plan Nacional de Desarrollo 2014 – 2018 “Todos por un nuevo país”, **no existe explícitamente algún presupuesto destinado** al apoyo de la gestión integral del río, ni para la descontaminación del río Bogotá.

Con base en este panorama, **Héctor Buitrago, guitarrista de Aterciopelados** se ha dedicado a crear consciencia en las personas sobre el deterioro de la naturaleza y el abandono de las fuentes hídricas, "hay muchas personas que están trabajando por el río Bogotá, pero todavía no somos los que deberíamos ser”, y agrega que “**hay indiferencia, apatía y olvido con el tema de nuestro río”**, es por eso que según él, se debe empezar un proceso de reconexión espiritual con el agua.

Precisamente buscando esa conexión, nace Canto al agua, después de que Héctor navegara  por las sucias aguas del río Bogotá, aguantándose el olor pútrido y el aspecto desagradable del río que atraviesa la capital. Canto al Agua, que hace parte de un llamado de Héctor Buitrago a la **meditación colectiva, para sanar el agua desde la música,** es por eso que en el 2008, el líder de Aterciopelados crea la canción “Río”  dedicada al río Bogotá.

\[embed\]https://www.youtube.com/watch?v=S6L9wccThyA\[/embed\]

A través de esta iniciativa y Canto al Agua, Héctor hace un llamado a la ciudadanía para que sean  veedores de cómo se van a manejar los recursos para recuperar el río, y todas las actividades institucionales en torno a la fuente hídrica más importante de la ciudad, ya que como dice Buitrago, el río Bogotá es “la sangre de nuestra ciudad”.
