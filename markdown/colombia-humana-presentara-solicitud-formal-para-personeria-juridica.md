Title: Colombia Humana presentará solicitud formal para obtener su personería jurídica
Date: 2018-09-25 12:31
Author: AdminContagio
Category: Movilización, Política
Tags: CNE, Colombia Humana, Consejo Nacional Electoral, Jorge Rojas
Slug: colombia-humana-presentara-solicitud-formal-para-personeria-juridica
Status: published

###### [Foto: Twitter Gustavo Petro] 

###### [25 Sept 2018] 

Luego de la asamblea fundacional que se realizó el pasado 22 de septiembre, la organización política Colombia Humana hará la solicitud formal para adquirir la personería jurídica la próxima semana. Jorge Rojas, integrante de esa colectividad manifestó que, si bien ya el **Consejo Nacional Electoral falló contra este hecho, la decisión fue errada porque aún no se había demandado el reconocimiento político**.

Durante la jornada del fin de semana, **4.600 personas se registraron como nuevos militantes de la Colombia humana**, se realizó la aprobación de los estatutos, se escogió un comité de ética y una dirección provisional, hechos que para Rojas  demuestran la realidad política de esta nueva organización que hace urgente una realidad jurídica.

Frente al llamado que hizo la Procuraduría para que se reconozca la personería jurídica de la Colombia Humana, Rojas señaló que hay dos poderosas razones para otorgarla: la primera es la reforma al equilibrio de poderes que habilita la posibilidad de que los candidatos que obtienen y que en la segunda vuelta electoral presidencial, acceden a unas curules en el Congreso de la República.

La segunda razón es que de acuerdo al Estatuto de Oposición y su artículo 24, esa representación de quienes lleguen al Congreso, se hará en nombre de la oposición, situación que para Rojas es absurda, debido a que **"Gustavo Petro llega al Senado en virtud de más de 8 millones de votos, pero no tiene un partido con personería jurídica**, cuando en Colombia no se puede llegar al senado si no hay un partido reconocido por el CNE".

Rojas aseguró que, con la solicitud formal que hará Colombia Humana, ante el nuevo Consejo Nacional Electoral, se espera este falle en favor del concepto contenido en el Estatuto de oposición y no primen las intenciones políticas que ha tenido esa entidad en sus decisiones.  (Le puede interesar:["Los 3 mecanismos con los que la Colombia Humana defenderá su personería jurídica"](https://archivo.contagioradio.com/los-3-mecanismos-con-los-que-la-colombia-humana-defendera-su-personeria-juridica/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
