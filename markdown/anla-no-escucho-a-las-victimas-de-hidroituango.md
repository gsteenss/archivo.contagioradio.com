Title: ANLA no escuchó a las víctimas de Hidroituango
Date: 2018-01-29 15:47
Category: DDHH, Nacional
Tags: ANLA, Hidroelectrica, Hidroituango, licencias ambientales, víctimas de hidroituango
Slug: anla-no-escucho-a-las-victimas-de-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/DUa2YdpXUAckk-o-e1517256627798.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movimiento Ríos Vivos] 

###### [29 Ene 2018] 

El Movimiento Ríos Vivos denunció que la Agencia Nacional de Licencias Ambientales no permitió la continuación de** la audiencia pública sobre Hidroituango** que se llevaba acabo del 25 de enero en el municipio de Santa Fe de Antioquia. Indicaron que la audiencia se había programado desde marzo del año pasado para hacerle seguimiento ambiental al proyecto y para expresar el rechazo a la modificación número 24 de la licencia ambiental.

Además, denunciaron que hubo un trato vejatorio contra más de mil campesinos, barqueros y demás afectados por el mega proyecto hidroeléctrico Hidroituango pues no fueron escuchados quienes han sido despojadas de sus territorios por parte de la empresa EPM.

### **Inscripciones a la audiencia se habrían perdido ** 

De acuerdo con la denuncia, se perdieron **560 de las 680 inscripciones** que se habían hecho a través de Corantioquia de personas que deseaban intervenir en la audiencia.  Indicaron que en la reunión hubo condiciones precarios, con malos olores y las personas ya estaban exhaustas pues pasaron más de 18 horas porque la ANLA no aceptó la solicitud de que la audiencia fuera aplazada.

Isabel Cristina Zuleta, integrante del Movimiento Ríos Vivos, indicó que a las 2:00 a.m del viernes 26 de enero la agencia decidió terminar la reunión “argumentando que no había logística y sus representantes dicen que esperaban 120 personas y fueron algo más de mil”. Dijo que **“fue evidente el afán por terminar la audiencia por parte de la ANLA”** en la medida que las personas fueron llamadas de manera muy rápida y "hasta la directora de la Agencia se estaba quedando dormida". (Le puede interesar: ["En zona de Hidroituango habría más de 2 mil personas desaparecidas"](https://archivo.contagioradio.com/en-el-canon-del-rio-cauca-afectado-por-hidroituango-hay-mas-de-2-mil-personas-desaparecidas/))

### **ANLA no permitió que se realizara audiencia de seguimiento a Hidroituango** 

Zuleta recalcó que la ANLA estaba dispuesta a llevar a cabo únicamente la audiencia pública de la modificación de la licencia ambiental más **no la de seguimiento** que era donde los afectados se querían pronunciar para dar a conocer las afectaciones que han tenido.

Ante esto, Zuleta reiteró que lo que quiere hacer la ANLA es **“reducir las obligaciones del retiro de la capa vegetal** con todos los impactos que esto conlleva”. Esto, teniendo en cuenta que la licencia ambiental prevee la inundación de 4.500 hectáreas **sin retirar el material denominado biomasa** donde se encuentran todo tipo de especies vegetales y diferentes especies animales. Esto, para Ríos Vivos supondría un desastre ambiental. (Le puede interesar: ["Memoria y resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

Esta inundación está prevista para realizarse en junio de 2018 y la licencia ambiental que la regula ha tenido 23 modificaciones. Explicó que, a pesar de que el Ministerio de Ambiente le dijo a la ANLA que debía hacerse la audiencia de seguimiento al proyecto más no la de evaluación del mismo y esta fue la que realizó la Agencia **“por presión de la empresa EPM”.**

Adicionalmente, Zuleta denunció que la empresa ha ofrecido plata en varias ocasiones y **“la ANLA se los permitió”**. Dijo que este dinero fue ofrecido a las administraciones municipales “a cambio de que se modificara la audiencia diciendo que quedaba mejor invertida la plata en otras necesidades de los municipios que en remover la capa vegetal”.

### **Ríos Vivos interpuso una tutela para defender su derecho a la participación** 

Ante lo sucedido, el movimiento que ha venido defendiendo los derechos del territorio y las comunidades redactaron una tutela por el derecho a la participación que fue radicada en el Juzgado Promiscuo Municipal de Santa Fe de Antioquia. Ahora **esperan la decisión del juez** teniendo en cuenta que la ANLA se paró de la mesa y no dio una nueva fecha para la continuación de la audiencia. (Le puede interesar:["62 masacres en los 12 municipios donde se desarrolla el proyecto Hidroituango"](https://archivo.contagioradio.com/62-masacres-los-12-municipios-donde-se-desarrolla-proyecto-hidroituango/))

Finalmente, las comunidades manifestaron su **rechazo e indignación frente a este tipo de hechos** debido a que en repetidas ocasiones han manifestado su posición frente a la construcción del proyecto que afecta la posibilidad de encontrar los cuerpos de cientos de personas que están desaparecidas y cuyos cuerpos se piensan que están en las riberas del río Cauca, lugar que será inundado.

<iframe id="audio_23428072" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23428072_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
