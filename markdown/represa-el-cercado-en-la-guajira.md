Title: Piden abrir compuertas de represa “El Cercado” en la Guajira
Date: 2017-04-25 16:25
Category: DDHH, Nacional
Tags: arroyo Bruno, Guajira, río Ranchería, Wayuu
Slug: represa-el-cercado-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Diseño-sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: semana/wikimedia] 

###### [25 Abr 2017] 

A través de una carta enviada a la primera dama de la Nación, María Clemencia Rodríguez de Santos, más de 90 firmantes, entre organizaciones y personas, tanto en Colombia como en el mundo, **pidieron que se abran las compuertas de la represa “El Cercado”** que contienen las aguas del Río Ranchería en la Guajira, como un mecanismo para recuperar la mayor y casi única fuente hídrica del departamento.

En la misiva resaltan que el papel de la Primera Dama es velar por los intereses de las mujeres, los niños y niñas en todo el país, por eso sería necesario que se destapone el Río Ranchería, puesto que esta medida contribuiría en la **mitigación del impacto ambiental que produce la empresa Cerrejón, sobre la vida de la tierra en el departamento** y la supervivencia de las comunidades indígenas y negras de la región.

También apelan a la Primera Dama como mujer y madre “Contamos con su apoyo, no sólo como la esposa de un mandatario, sino como la mujer dadora de vida, que tiene la sensibilidad de entender el dolor de las madres que están sufriendo esta situación” y reafirmaron que “**confiamos en su importante contribución para poder detener la desaparición de las etnias vivas del país**”.

Ya en 2016 la Corte Constitucional ordenó que el Estado garantice el abastecimiento de agua para los habitantes de la Guajira y que se abrieran las compuertas del “Cercado”, a pesar de ello, hasta el momento no se ha cumplido dicha orden y por ello en la comunicación también se recuerda que la apertura de la represa es un asunto de supervivencia de comunidades negras e indígenas.

### **La represa “el Cercado” y la sed de la guajira** 

La represa el “Cercado” fue puesta en funcionamiento en 2001 y tiene una capacidad de almacenamiento es de 198 millones de metros cúbicos, su profundidad máxima es de 110 metros, manteniendo el nivel de agua en 98 metros. En época de sequía la longitud de entrada del agua es de 7 metros, pero en época de lluvias podría abarcar los 20 metros, es decir que la capacidad de acaparamiento de agua podría superar los 20 metros cúbicos por segundo.

Una de las justificaciones para la construcción de la represa es que abastecería 9 distritos de riego y favorecería las actividades de monocultivos de palma de aceite y la minería de Carbón, sin embargo, **la realidad es que su utilidad solo ha favorecido a la empresa privada, concretamente a Carbones del Cerrejón**, según lo denuncian constantemente las comunidades que habitan el departamento.

Según el PNUD, **el consumo de agua por persona al día en La Guajira es de 0,7 litros,** mientras que, de acuerdo con la comunidad**, la mina El Cerrejón asegura necesitar diariamente 17 millones de litros extraídos del Río Ranchería** para disminuir el polvo en las vías de transporte. ([Lea también: Empresa cerrejón usa 17 millones de litros de agua al día](https://archivo.contagioradio.com/mina-de-carbon-del-cerrejon-usa-diariamente-17-millones-de-litros-de-agua/))

### **Las consecuencias del represamiento y de la actividad minera** 

La UNICEF ha denunciado que en los últimos **6 años han muerto 5 mil niños del pueblo Wayúu por desnutrición**, sumado a eso, de acuerdo con las comunidades 26 fuentes de agua se han secado, entre ellas, los arroyos: Aguas Blancas,  La Puente, Cerrejoncito, Araña de Gato, Bartolito,  Morrocon, la chercha, el Sequión, La Trampa, Pupurema, Tabaco, La Puente, Taurina, Chivo feliz, Palomino, La Quebrada, Ceino, Puente Negro.

Asimismo, se han secado las lagunas del Co, Buzu, Fermin, Roche, Garrapatero, El Chivato, El Burro y El Ejemplo, muchas de ellas eran lugares sagrados pues no solo abastecían de agua a las familias, sino que además, alrededor de ellas las comunidades se congregaban para tomar decisiones o realizar rituales ancestrales. ([Lea también: Con el Arroyo Bruno serían 27 las fuentes de agua desaparecidas en la Guajira](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/))

<iframe id="audio_18340258" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18340258_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
