Title: Así se vivió el 1 de mayo en Bogotá
Date: 2015-05-01 19:18
Author: CtgAdm
Category: Fotografia, Galerias, Nacional
Tags: 1 de mayo, Bogotá, fotos, nicolas neira, polo
Slug: asi-se-vivio-el-1-de-mayo-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/1-de-mayo-7-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

En Bogotá miles de personas de partidos políticos, grupos juveniles, sindicatos, movimientos sociales, salieron a las calles a marchar conmemorando el 1 de mayo, día internacional del trabajo. La marcha transcurrió entre música, cantos y eventos de memoria a lo largo de la carrera 7, la marcha llegó hacia las 2 p.m a la Plaza de Bolívar con mayor tranquilidad.

\

###### [Fotos por contagioradio.com ] 
