Title: ELN responde a carta enviada por CONPAZ sobre acuerdo de justicia
Date: 2015-10-06 12:56
Category: Nacional, Paz
Tags: Conpaz, Diálogos de paz en la Habana, ELN Conversaciones de paz, FARC Diálogos de paz, jurisdicción especial para la paz, Radio de derechos humanos
Slug: eln-responde-a-carta-enviada-por-conpaz-sobre-acuerdo-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/gabino-contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: archivo 

###### [06 Oct 2015] 

La [carta enviada por la **Red de Comunidades Construyendo Paz desde los Territorios CONPAZ**](https://comunidadesconpaz.wordpress.com/2015/09/24/jurisdiccion-especial-para-la-paz-algunas-de-nuestras-primeras-preguntas-e-inquietudes-en-esperanza/) el pasado 24 de septiembre al presidente de la República Juan Manuel Santos y a Timoleón Jiménez y Nicolás Rodríguez Bautista, comandantes máximos de las FARC y el ELN respectivamente, sobre el acuerdo de Justicia firmado en La Habana, fue respondida a través de la cuenta de Twitter @ELN\_RANPAL.

La guerrilla del ELN afirma que no ha discutido con el gobierno colombiano la Jurisdicción Especial para la Paz JEP, pues las **dos delegaciones se han centrado en la construcción de la Agenda de los Diálogos** que esperan iniciar en las próximas semanas.

Insisten en que la participación de la sociedad, sobre todo la excluida del poder y de las decisiones sobre los destinos del país, es definitiva para la construcción de los acuerdos en el marco del proceso de paz.

Para la concreción de la verdad, la justicia, la reparación y la no repetición el “Tema de víctimas tiene especial preponderancia”, no obstante “*Si derechos como  Verdad, Justicia, Reparación, etc., son competencia sólo de la insurgencia y gobierno, el tema de víctimas no se resolverá*”, por lo que “Se requiere participación organizada y protagónica de la sociedad desde sus organizaciones”.

En referencia a la armonización de los dos procesos de conversaciones el ELN aseguró que “*Es complicado armonizar los dos procesos, pues mientras el proceso de La Habana se acelera, el nuestro apenas va para la fase pública*”; sin embargo, no han diseñado “Un diálogo por separado, sencillamente asumimos esa realidad como un reto más en aras de la paz” y consideran que “Colocar plazos perentorios al acuerdo final, sin haber concluido las discusiones y definido acuerdos específicos, se vuelve camisa de fuerza”.

“En materia de acuerdos y compromisos adquiridos, en Colombia se violan flagrante y sistemáticamente; esta realidad no debe repetirse” aseguró el ELN. Con relación a los acuerdos pactados en La Habana “Escuchamos decir al comandante Iván Márquez que en varios asuntos el gobierno estaba tergiversando lo firmado” y “*Sobre el tema de JEP se requiere tener la documentación completa, antes de emitir más opiniones*”.

“*La paz es una sola, los procesos se complementan y todo aquello que en sana discusión se acuerde, será mucho más sólido si hay consenso*” aseguró el ELN, además están “Atentos a recoger y estudiar todas las experiencia en esa materia, con los compañeros de las FARC, así lo hemos conversado”, “Paz no es “borrón y cuenta nueva”, es reconocer en qué se falló, desandar equívocos y generar consenso, es decir es inclusión, es democracia”.[ Vea también: ][[Víctimas piden desmantelar el paramilitarismo como garantía de la JEP](https://archivo.contagioradio.com/dudas-y-cuestionamientos-sobre-jurisdiccion-especial-para-la-paz/)]
