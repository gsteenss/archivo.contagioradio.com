Title: Incursión armada de AGC en comunidad indígena de Jiguamiandó
Date: 2018-11-29 17:32
Author: AdminContagio
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, indígenas, Jiguamiandó
Slug: incursion-armada-de-agc-en-comunidad-indigena-de-jiguamiando
Status: published

###### [Foto: Comisión Justicia y Paz ] 

###### [29 Noz 2018]

La comunidad del Caserío de Puerto Lleras, en el territorio Colectivo de Jiguamiandó, municipio del Carmen del Daríen, denunció el pasado 26 de noviembre, la incursión armada de aproximadamente **40 hombres que se identificaron como integrantes de las Autodefensas Gaitanistas de Colombia**, provocando miedo en los habitantes y amenazando a la guardia indígena.

De acuerdo con el relato de uno de los habitantes del lugar, los hombres llegaron vestidos con camuflados, portando armas largas y amenazaron a la guardia indígena de la comunidad, **señalándoles que si tomaban alguna acción habrían represalia**s. Los 40 hombres permanecieron en la comunidad hasta el día siguiente, posteriormente se retiraron del la zona.

Las personas que viven en el caserío manifestaron su preocupación y señalaron que ya informaron de esta situación a las instituciones correspondientes, razón por la cual están a la espera de que se tomen medidas que garanticen la protección de quienes conviven allí. (Le puede interesar: ["Autodefensas Gaitanistas de Colombia extorsionan a integrantes de la Zona de Biodiversidad"](https://archivo.contagioradio.com/autodefensas-gaitanistas-de-colombia-extorsionan-a-integrantes-de-zona-de-biodiversidad/))

A estos hechos se suman las denuncias de las comunidades del Bajo Atrato, que a lo largo de este año han venido informado sobre la presencia de grupos **armados como las Autodefensas Gaitanistas de Colombia, que han aumentado el control en la región. **

###### Reciba toda la información de Contagio Radio en [[su correo]

###### 
