Title: "No podemos darle espacio a quienes desde ya tratan de sabotear los acuerdos": Carlos Lozano
Date: 2016-06-24 15:54
Category: Nacional, Paz
Tags: acuerdo cese al fuego farc y gobierno, Diálogos de La Habana, dialogos de paz, Mesa de conversaciones de paz de la habana
Slug: no-podemos-darle-espacio-a-quienes-desde-ya-tratan-de-sabotear-los-acuerdos-carlos-lozano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/proceso-de-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [24 Junio 2016 ] 

"Prácticamente estamos en la última etapa del proceso, en este momento ya esto no tiene reversa" sólo se espera que se den tramite a los asuntos pendientes "**sin afanes, sin tiempos fatales, sin presiones innecesarias y sobretodo, ya en el mejor ambiente** porque es en las condiciones del cese al fuego bilateral y definitivo", afirma Carlos Lozano, director del Semanario Voz, a propósito del más reciente acuerdo firmado en La Habana.

Para Lozano los 180 días acordados para que las FARC se acaben como fuerza armada y se transformen en una organización política es el tiempo suficiente y pese a que en ciertas regiones las comunidades sienten preocupación por la instalación de las zonas en las que se concentrarían los excombatientes, él asegura que esa angustia depende de la actitud del gobierno de turno de cada región frente al proceso de paz; sin embargo, las condiciones ya están definidas favorablemente en el acuerdo pactado.

Lozano asegura que "aquí hay problema de fondo, y es la renuencia del Gobierno frente a una reforma estructural, un claro ejemplo de ello es **el Código de Policía que está en el espíritu no de la paz, no de la convivencia, no de la reconciliación sino de la represión y la coerción** (...) eso ocurre también con todo el estamento militar y la concepción de seguridad nacional del enemigo interno que persiste (...) esa va a ser una tarea más larga, uno de los objetivos de la Asamblea Nacional Constituyente, y debe ser el empeño de todos los sectores democráticos para el posacuerdo."

"Lo más importante que debemos entender los sectores democráticos y de izquierdas, es que **hay rodear de apoyo los acuerdos, no podemos dejarle el espacio a quienes desde ya están tratando de sabotear**, no solamente desde la extrema derecha sino dentro del Estado como el caso del Procurador General de la Nación y los famosos enemigos internos dentro del Gobierno, que tratan de cerrarle el paso a esta paz estable y duradera", concluye Lozano.

<iframe id="audio_12018842" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12018842_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
