Title: Plazas de Colombia y el mundo encienden una luz por los líderes sociales
Date: 2018-07-06 09:46
Category: Movilización, Nacional
Tags: #NosEstánMatando, lideres sociales, Movilización social, Velatón
Slug: plazas-colombia-encienden-luz-por-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/VELAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EldelaBarca] 

###### [06 Jul 2018] 

En rechazo a los asesinatos en contra de líderes y lideresas sociales en el país, **la ciudadanía en general hizo un llamado para que en todas las plazas de Colombia, se encienda una luz a las 6 de la tarde**. Más de 100 ciudades del mundo también se sumarán a esta movilización por la protección de quienes buscan construir paz en sus territorios.

**La demanda de está 'velatón' es exigir al gobierno entrante como al saliente pronunciamiento, condena y acciones concretas para que no se siga asesinando a líderes sociales.** Igualmente, se pedirá que se protejan los líderes que están siendo amenazados y las comunidades a las cuales pertenecen.

**La concentración en Bogotá se llevará a cabo en la Plaza de Bolivar,** de la misma forma, se llevará a cabo la jornada de protesta en las principales plazas del país de más de 50 ciudades. Entre las ciudades del exterior que se han unido a la jornada están París, Nueva York, Ciudad de México, Montevideo, Buenos Aires, Quito, Santiago de Chile y Rio de Janeiro.

![WhatsApp Image 2018-07-04 at 6.35.46 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/WhatsApp-Image-2018-07-04-at-6.35.46-PM-800x741.jpeg){.alignnone .wp-image-54522 width="303" height="281"}![Captura de pantalla 2018-07-06 a la(s) 8.59.10 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-06-a-las-8.59.10-a.m.-525x528.png){.alignnone .wp-image-54518 width="280" height="282"}

![Captura de pantalla 2018-07-06 a la(s) 9.00.20 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-06-a-las-9.00.20-a.m.-504x332.png){.wp-image-54520 .alignleft width="289" height="190"}

![WhatsApp Image 2018-07-04 at 11.37.22 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/WhatsApp-Image-2018-07-04-at-11.37.22-AM-720x638.jpeg){.alignnone .wp-image-54524 width="260" height="230"}

![WhatsApp Image 2018-07-04 at 8.28.08 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/WhatsApp-Image-2018-07-04-at-8.28.08-PM-566x800.jpeg){.wp-image-54523 .aligncenter width="201" height="284"}

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
