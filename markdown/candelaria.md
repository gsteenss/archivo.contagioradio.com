Title: 'Candelaria' una historia de amor otoñal a ritmo de son cubano
Date: 2018-08-24 17:29
Author: AdminContagio
Category: 24 Cuadros
Tags: Candelaria, Cine Colombiano, hendrix
Slug: candelaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Candelaria-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Aldo Dalmazzo 

###### 24 Ago 2018 

Luego de ser estrenada en Francia y de su paso por salas internacionales, desde este 23 de agosto llega a Colombia la película 'Candelaria', tercer largometraje del director colombiano Jhonny Hendrix Hinestroza (Chocó 2014, y Saudó 2016). Una cinta que logra mostrar la belleza, ternura, humor y sensualidad de un amor otoñal.

Candelaria es una tragicomedia en la que sus protagonistas, una pareja de adultos mayores viven en la monotonía de la Cuba de los años 90, hasta que Candelaria (Verónica Lynn) encuentra en su lugar de trabajo, un hotel de la Habana, una cámara de video con la que ella y su esposo Víctor Hugo (Alden Knight) redescubren su sexualidad.

Para ella y su esposo este objeto se convierte en primera instancia en un dinero extra para su familia, pero termina siendo un objeto preciado que cambiará sus vidas por completo. A través de este objeto prohibido en una isla caracterizada por los bloqueos económicos, el tabaco y el ron,  los dos protagonistas empiezan un nuevo capítulo donde afloran nuevos sentimientos, tras 70 años de compartirlo todo.

Su director Jhonny Hendrix Hinestroza, asegura que decidió hacer una película sobre un par de adultos mayores por varias razones "la primera es que no le temo a las arrugas, las arrugas son lo más bello porque hablan de ti, dicen lo que has vivido. Dos, porque solamente ese amor que dura hasta esos años puede ser puro, no le creo a ningún otro y tres porque es un homenaje a ese amor que ha existido en mi vida y que de una u otra forma he soñado que sea así de bien".

<iframe src="https://www.youtube.com/embed/gpvAi9nv9yg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Rodada en Cuba a finales de 2016, 'Candelaria' recibió recientemente el premio en la categoría mejor película, en el la categoría Colombia en Cinta, del Festival Internacional de Cine de Santander y el Galardón al Gran Premio del Público en la edición del Festival de Cine Latinoamérica de Toulouse.

###### Encuentre más información sobre Cine en [24 Cuadros ](https://archivo.contagioradio.com/programas/24-cuadros/)y los sábados a partir de las 11 a.m. en Contagio Radio 
