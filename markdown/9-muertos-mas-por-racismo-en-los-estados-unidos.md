Title: 9 muertos más por racismo en los Estados Unidos
Date: 2015-06-18 11:02
Category: Comunidad, El mundo
Tags: carolina del sur, EEUU, Estados Unidos, ferguson, masacre, racismo
Slug: 9-muertos-mas-por-racismo-en-los-estados-unidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/EEUU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AP 

###### [18 jun 2015] 

El miércoles 17 de junio se presentó una masacre en los Estados Unidos, en la que Dylann Roof, un joven de 21 años de edad, asesinó a 9 afroamericanos que se encontraban en la iglesia Ame Emmanuel, una de las más tradicionales en Charleston, Carolina del Sur.

Según afirman las fuentes, el jóven estadounidense habría permanecido 1 hora en el oficio antes de levantarse, gritar "He venido a matar negros", y arremeter contra las personas asistentes, dejando como un resultado fatal a 6 mujeres y 3 hombres.

El presidente de la oficina de Charleston de la Asociación Nacional para el Progreso de las Personas de Color, declaró que el objetivo de Roof no era únicamente el de asesinar, sino también generar temor y zozobra en la comunidad, pues permitió que una mujer escapara del lugar diciendo "Te voy a salvar para que cuentes lo que pasó".

Este crimen representa un atentado más contra la comunidad negra en los Estados Unidos, que ha visto en los últimos meses ataques de tipo racista en su contra, y total impunidad por parte de los organismos competentes de capturar y judicializar a los responsables. Sin embargo, según afirman las autoridades, hace más de 2 décadas no se presentaba un caso de esta magnitud. Cabe recordar que en los últimos meses la policía estadounidense ha estado implicada en el asesinato de jóvenes afroamericanos en Cleveland, Ferguson, Baltimore, y Carolina del Norte, casos que continúan en la impunidad.

La comunidad afroamericana de los Estados Unidos denuncia que este tipo de hechos responden a la campaña racista que han emprendido políticos y empresarios estadounidenses, y que la justicia no se aplica de manera igualitaria.

En un experimento realizado por jóvenes estadounidenses se evidencia el trato diferencial que tiene la policía con hombres blancos y hombres negros, cuando portan un arma por las calles.  
\[embed\]https://www.youtube.com/watch?v=BKGZnB41\_e4\[/embed\]
