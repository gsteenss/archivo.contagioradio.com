Title: Organizaciones de mujeres pedirán cese bilateral del fuego el 9A
Date: 2015-04-07 16:45
Author: CtgAdm
Category: Mujer, Nacional
Tags: 9 de abri, 9A, Adriana Benjumea, Centro de Memoria Histórica, Corporación Humanas, Movilización, Mujeres por la paz, paz, proceso de paz, Secretaría de la Mujer, Sub comisión de género, Subcomisión de género
Slug: organizaciones-de-mujeres-pediran-cese-bilateral-del-fuego-el-9a
Status: published

#####  Foto: [primiciadiario.com]

<iframe src="http://www.ivoox.com/player_ek_4320785_2_1.html?data=lZifkpyceY6ZmKiakpqJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMLnjNLizMrWqdSf1cbaxM6Jh5SZop7bjdjJb87j187Zy9%2FFtoa3lIquk9OPqc2fhpegm6aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Adriana Benjumea, Corporación Humanas] 

Varias organizaciones de mujeres, civiles e institucionales, también harán parte de la gran movilización del próximo 9 de abril. De acuerdo a Adriana Benjumea, directora de la Corporación Humanas “este jueves es una **oportunidad para seguir respaldando el proceso de paz,** un proceso en el que se reconoce a las víctimas y tienen un lugar fundamental”.

Las organizaciones de mujeres, se encontrarán desde las **nueve de la mañana en el Centro de Memoria Histórica,** y con pancartas y camiseras elevarán sus voces en apoyo al proceso de paz y las víctimas.

La Corporación Humanas, que trabaja por el acceso a la justicia de las mujeres víctimas de violencia sexual en el contexto del conflicto armado, se unirá con la Plataforma de **Mujeres por la Paz** y con la Secretaría de la Mujer, para hacer **exigirle al gobierno el cese bilateral al fuego y para que se tenga en cuenta el papel de las mujeres en los puntos de negociació**n de la Habana, a partir de la subcomisión de género que allí se encuentra.

Según Benjumea, las organizaciones de mujeres han dicho que todas las alternativas para que el proceso de paz avance, incluyendo la labor de las mujeres, son fundamentales para que se llegue a un acuerdo entre las FARC-EP y el gobierno Santos.
