Title: Unidad de Búsqueda tendrá que encontrar a más de 50.000 desaparecidos
Date: 2017-06-01 16:21
Category: DDHH, Nacional
Tags: acuerdo de paz, Unidad de Búsqueda de Desaparecidos
Slug: unidad-de-busqueda-tendra-que-encontrar-a-mas-de-50-000-desaparecidos-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [01 Jun 2017] 

Organizaciones defensoras de derechos humanos, víctimas y políticos expresaron su respaldo a la creación de la Unidad de Búsqueda de Personas dadas por Desaparecidas que tendrá como labor principal encontrar a los más de **50.000 desaparecidos forzadamente que habría en el país.**

De acuerdo con el Sistema de Información de Red de Desaparecidos Cadáveres (SIRDEC) aún no se ha podido establecer el total de personas desaparecidas forzadamente durante el conflicto armado en Colombia, **pero señalaron que la cifra oscilaría entre las 30.000 y las 60.000 personas**. Le puede interesar:["Víctimas de la desaparición forzada de Latinoamérica desarrollan encuentro en Colombia"](https://archivo.contagioradio.com/el-primer-encuentro-de-la/)

Las víctimas y organizaciones sociales expresaron, a través de un comunicado de prensa, que esta Unidad “**es un avance importante en la satisfacción de los derechos de las víctimas a conocer el paradero de sus familiares** y en el cumplimiento de las obligaciones internacionales de Estado en la materia, exigidas continuamente por el movimiento social”. Le puede interesar: ["Estado no mostró voluntad política en la CIDH frente a desaparición forzada: CCJ"](https://archivo.contagioradio.com/estado-no-muestra-voluntad-politica-en-el-tema-de-desaparicion-forzada/)

Además, agregaron que la desaparición forzada requiere de medidas urgentes y necesarias, en vista de la posible destrucción de pruebas por parte de los perpetradores, razón por la cual manifestaron **la importancia de adelantar acciones humanitarias para lograr el esclarecimiento de los casos y el conocimiento de la verdad**.

Esta Unidad de Búsqueda de Desaparecidos hace parte del Decreto Ley 589 de 2017 firmado el pasado 30 de mayo por el presidente Juan Manuel Santos, sin embargo, solo entrará en vigencia una vez la Corte Constirucional lo apruebe, por este motivo las víctimas le han solicitado a **la Corte que lo declare lo más pronto posible constitucional para que entre en vigencia de inmediato.**

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
