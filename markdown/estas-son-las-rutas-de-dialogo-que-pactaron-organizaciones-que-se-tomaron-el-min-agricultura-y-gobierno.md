Title: Estas son las rutas de diálogo que pactaron organizaciones que se tomaron el Min. Agricultura y gobierno
Date: 2017-10-20 16:28
Category: Movilización, Nacional
Tags: Jornadas de indignación, Paro Nacional
Slug: estas-son-las-rutas-de-dialogo-que-pactaron-organizaciones-que-se-tomaron-el-min-agricultura-y-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DMhQ9oYW4AE5MIy.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso de los Pueblos] 

###### [20 Oct 2017] 

Las organizaciones que el día de ayer ingresaron al Ministerio de Agricultura, lograron establecer una **mesa de conversaciones con el Gobierno Nacional para avanzar en la negociación de algunos de los puntos y reivindicaciones**, tras el acompañamiento de organizaciones defensoras de DD.HH que no permitieron el accionar violento por parte del ESMAD y verificaron las denuncias sobre persecución por parte de la SIJIN a las personas que se encontraban en el lugar.

De acuerdo con Edgar Mujica, vocero de Congreso de los Pueblos, mientras que los integrantes de diferentes organizaciones del movimiento social se encontraban en las instalaciones del Ministerio de Agricultura, se realizaron varias amenazas por parte del ESMAD de intentar un desalojo, sin embargo, **la misión de verificación de la ONU y otras organizaciones impidieron que este hecho sucediera y permitieron que se instalara la mesa de conversación.**

Además, le pidieron a estas organizaciones garantes que le pidieran **el material fotográfico y de video que tomo la SIJIN** a las personas que se encontraban en la sede del Ministerio de Agricultura, debido a que luego podrían ser utilizadas para hacer montajes judiciales. (Le puede interesar:["Asesinan a Liliana Astríd Ramírez, docente y lidereza en Tolima"](https://archivo.contagioradio.com/asesinan-liliana-astrid-ramirez-tolima/))

### **Los avances en la mesa de conversación** 

Mojica señaló que en la mesa se establecieron 4 bloques de temáticas, **el primero de ellos son la falta de garantías para las comunidades**, en el que se expuso la situación en los territorios y se acordaron rutas específicas para casos muy concretos de líderes amenazados en estos lugares. A su vez se expuso la reparamilitarización que están viviendo muchas zonas del país, y la poca acción por parte del gobierno nacional para desestructuras estos grupos e investigar quienes están detrás de la creación de los mismos.

El segundo bloque es el de **tierras, territorios y territorialidades,** en donde se expuso la problemática de los territorios afros en el sur del Cauca y el Magdalena Centro, allí se concertó una ruta de negociación directamente desde las regiones. (Le puede interesar: ["Así será el paro Nacional de este lunes"](https://archivo.contagioradio.com/23-de-octubre-hora-cero-para-el-paro-nacional/))

El tercer bloque consistió en la**s problemáticas minero energéticas** en donde se acordó una reunión con el Ministerio de Minas y la Agencia Minera, a finales de octubre, de cara a realizar el diálogo nacional Minero energético.

El cuarto bloque trato los puntos sobre los acuerdos que aún no cumple el gobierno, sin embargo, sobre este no se alcanzó ningún avance debido a la falta de tiempo para continuar en la mesa de conversación.

<iframe id="audio_21590094" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21590094_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
