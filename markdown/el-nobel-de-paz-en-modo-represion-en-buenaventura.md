Title: El Nobel de paz “en modo” represión en Buenaventura
Date: 2017-06-03 09:57
Category: Abilio, Opinion
Tags: buenaventura, ESMAD, Presidente Santos
Slug: el-nobel-de-paz-en-modo-represion-en-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/premio-de-paz-en-Buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 3 Jul 2017 

#### **Primero el dios mercado.** 

[Eran tolerables las protestas en Buenaventura hasta que los ciudadanos se cansaron de observar que las protestas no eran atendidas y que  las mercancías eran tratadas con sumo cuidado al descender de los barcos y al ascender a los camiones y viceversa, como si fueran divinas. Tomaron la decisión de afectar su circulación, pues tanta comodidad para las cosas era  ofensiva para  ciudadanos sin agua, sin luz, sin vivienda,  sin salud,  sin empleo.]

Lo sagrado no se toca, respondió el poder  estatal y ordenó atacar a los potenciales manifestantes antes de que salieran a manifestar, aprovechándose de la oscuridad y del sueño reparador de fuerzas. En otros lugares se llama  guerra preventiva, asegurar que  el dios mercado pueda circular, así sea necesario sacrificar para su satisfacción el sueño de las mujeres, de los hombres, de los animales, pero también su salud y si fuera necesario su vida. Ya ha pasado en el puerto. Muy buenos estudios   han mostrado que su ampliación y la de  el malecón, financiado por accionistas extranjeros, se construye en áreas donde antes vivía gente que fue desplazada o en los lugares en que se entronizaron las barbaras casas de pique en la que desmembraron   a mujeres y hombres.

Para  rendirle culto a ese dios, valen los sacrificios. Pareciera que se reanimara, que se llenara de más fuerza y poder, que sus sacerdotes se enorgullecieran del humo de las granadas y de la sangre con la que se le abre camino para que  pueda circular  sin interferencias. Las protestas son sólo obstrucciones a la libre circulación del mercado fácil de resolver. Así como para la basura se utilizan escobas, para los que se atraviesan en el camino de las mercancías  está la fuerza de choque de la policía – El Esmad- , los helicópteros que lanzan bombas aturdidoras y la presencia aterradora de la marina. No conmueve el llanto de las niñas y niños, el clamor de sus mamás.

“Son demasiados, demasiados, atacando a la comunidad -dice una mujer  llorando- fumigando desde arriba, solamente  con el propósito de sacar la carga del muelle, que la población no les importamos nada. Por favor necesitamos la solidaridad del mundo entero. No dejen que nos maten”.

[El nobel de paz, luz en la calle y sombra en la casa,  es el  primero que en Colombia ha recibido ese premio, el mismo al que en en Asís, Italia le entregaron la lampara del santo de la paz, del pobre Francisco.  El es el jefe de esa policía y de ese ejercito que reprime a los bonaverenses. El no sabe del Dios de Francisco, él adora  al dios mercado. El  tiene planes que la realidad le fue cambiando. Quería convertir a Buenaventura en el epicentro del Plan Pacífico, aquella área  de libre comercio para la mitad mundo   del que el puerto es uno de sus  estandartes. Ese atrio extendido para su dios, preparado con detalles por medio de sacrificios humanos,  de repente empezó a ser visto por defensores de derechos,  humanos del mundo y medios de información como el epicentro del horror y la barbarie tras los hallazgos de las casas de pique, que primero las “autoridades” querían tapar, como si se pudiera ocultar  el sol con un dedo, y luego a fuerza de desvergüenza debieron reconocer.]

La gran cumbre de presidentes de los países del grupo del pacífico, debió trasladarse de Buenaventura a Cartagena, donde la pobreza no se viera. La sangre de los sacrificios del  dios  mercado no se puedo lavar bien y  esa mancha gritó al cielo, así como ocurrió con la sangre  de Abel luego de haber sido asesinado por su hermano Caín, en la metáfora bíblica.

Dentro de un mes se realizará en Cali otra cumbre de la Alianza Pacífico de la que hace parte Colombia con tres países más  y cuarenta y nueve países   observadores. El nobel de paz recibirá de Chile  la presidencia protempore. El principal objetivo de la Alianza, ha dicho  el gobierno del nobel, “ es conformar un área de integración profunda que impulse un mayor crecimiento, desarrollo y competitividad de las economías participantes, mediante la búsqueda progresiva de la libre circulación de bienes, servicios, capitales y personas”. Ya había dicho que la paz facilitaría esos negocios.  Nadie se puede oponer a la libre circulación del mercado, el dios de éste mundo. Si así ocurriere, lloverá represión y muerte, a pesar de que  su máximo profeta  en Colombia  sea un premio nobel de paz.

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
