Title: De la mano de organizaciones y víctimas comienza Plan de Búsqueda de Desaparecidos
Date: 2019-09-30 17:34
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Sistema Integral de Justicia, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: de-la-mano-de-organizaciones-y-victimas-comienza-plan-de-busqueda-de-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Búsqueda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @DesaparecidosHE] 

###### [ ] 

La Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD), dio inicio al primer encuentro del Plan Nacional de Búsqueda, un diálogo que garantizará la participación de víctimas, organizaciones de DD.HH. e instituciones para establecer una ruta estratégica en la búsqueda humanitaria y extrajudicial de los **más de 97.000 casos de personas que según Medicina Legal**, desaparecieron en el marco del conflicto armado.

Según Luz Marina Monzón, directora de la entidad, estos diálogos surgen como respuesta a la necesidad de  destacar aportes y exponer problemáticas que permitan realizar una búsqueda con un enfoque diferencial, por lo que esta iniciativa apunta a "un intercambio de objetivos,  para poder consolidarlos y reforzarlos".

### La búsqueda debe ser diferencial

Por tal motivo han sido programados un total de seis encuentros con familiares de víctimas de desaparición forzada, mujeres, población LGBTI, afrodescendientes e indígenas. Sumados a estos encuentros, también se realizará una reunión con familiares de víctimas de secuestro y familiares de integrantes de la Fuerza Pública en el exilio y finalmente un encuentro con entidades estatales e instituciones implicadas en la búsqueda de personas desaparecidas.

Por su parte, para colectivos como la Asociación de Familiares de Detenidos Desaparecidos (ASFADDES) este proceso será una forma de fortalecer los procesos  de búsqueda que a su vez **"permitirán que podamos transitar y caminar para aliviar y cicatrizar las heridas que genera la impunidad y la ausencia de respuestas"**.

Los encuentros, señala Monzón, serán de carácter privado, esto debido a que la UBPD quiere garantizar la seguridad y respeto de quienes han puesto "su esfuerzo, esperanza y compromiso en la construcción de este trabajo". [Le puede interesar: "Colombia merece una paz con verdad y sin desaparecidos", ASFADDES)](https://archivo.contagioradio.com/colombia-merece-una-paz-con-verdad-y-sin-desaparecidos-asfaddes/)

<iframe id="audio_42418001" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_42418001_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
