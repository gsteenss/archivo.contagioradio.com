Title: Cifras demuestran que gobierno Duque ha hecho muy poco por la paz
Date: 2019-08-08 17:52
Author: CtgAdm
Category: Paz, Política
Tags: acuerdo de paz, Congresistas, Implementación, Iván Cepeda
Slug: gobierno-duque-poco-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Duque-e1565303834276.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia de la República  
] 

Congresistas de la oposición emitieron un documento titulado "¿En qué va el Acuerdo de Paz a un año del gobierno Duque?" en el que presentan los retos que ha enfrentado la implementación en este primer año de Gobierno. El texto fue posible gracias a información entregada por fuentes oficiales, y allí se revela que "**el 57% de las normas requeridas para la implementación del Acuerdo aún están pendientes de trámite en el Congreso**".

**Iván Cepeda, senador de la oposición**, declaró que la creación del informe estuvo liderada por la representante Juanita Goebertus y contó con la participación de parlamentarios de diferentes partidos políticos. Su objetivo fue hacer un balance riguroso de la implementación con base en información que el mismo Gobierno ha entregado, "entonces no son datos subjetivos ni caprichosos de los congresistas". (Le puede interesar: ["Gobierno Duque cumple un año obstaculizando la paz y la garantía de DD.HH."](https://archivo.contagioradio.com/gobierno-duque-cumple-un-ano-obstaculizando-la-paz-y-la-garantia-de-ddhh/))

El informe recuerda que según "el Profesor John Paul Lederach del Instituto Kroc, el **50% de los países vuelven a la guerra durante los primeros 5 años después de la firma de un acuerdo de paz**"; de allí la importancia de verificar que la implementación del Acuerdo se haga con total disposición. No obstante, Cepeda indicó que han visto una narrativa contradictoria sobre este tema de parte del presidente Duque: Por una parte, hace declaraciones internacionales diciendo que implementará lo acordado, "pero las cifras entregadas por este informe muestran que hay una visión muy limitada de lo que es el Acuerdo de Paz".

### **No hay dinero para la implementación del Acuerdo de Paz** 

De acuerdo al Plan Marco de Implementación y el Marco Fiscal de Mediano Plazo de 2018, implementar el Acuerdo de Paz costaría 139,1 billones de pesos en un plazo de 15 años; el informe destaca que en el Plan Plurianual de Inversiones para este cuatreniuo, "el Gobierno incluyó 37,1 billones que deberán ser destinados a la implementación del Acuerdo de Paz" (un 26% del total necesario). Sin embargo, solo el 65% de los recursos saldrían del Presupuesto General de la Nación y el Sistema General de Participaciones.

El resultado de ello, es que **el restante 35% se obtendría del Sistema General de Regalías, recursos propios de los territorios, la contribución de privados y la cooperación internacional**. Adicionalmente, Cepeda afirmó que tras estudiar el presupuesto presentado para 2020, se dieron cuenta que al menos el 41% de los proyectos de paz son, en realidad, programas de política social general lo que "hace que se diluya la responsabilidad del Gobierno de manera muy importante".

### **Punto a punto, las críticas a la implementación del Acuerdo de Paz** 

Sobre el primer punto, que tiene que ver con la Reforma Rural Integral, los congresistas afirmaron que solo se está avanzando en un 8,7% de la meta anual para cumplir con la meta establecida en el Acuerdo para dotar y formalizar 10 millones de hectáreas a través del Fondo de Tierras. A ello se suma que la inversión al año en los Planes de Desarrollo con Enfoque Territorial (PDET) **es 1,2% de lo que se debería estar invirtiendo para cumplir lo pactado.** (Le puede interesar: ["Presidente Duque, fumigando no se acaba con el narcotráfico: lideresa Maydany Salcedo"](https://archivo.contagioradio.com/presidente-duque-fumigando-no-se-acaba-con-el-narcotrafico-lideresa-maydany-salcedo/))

Respecto al segundo punto, sobre participación política, el informe destaca que las 16 Circunscripciones Transitorias Especiales de Paz no han recibido el impulso necesario para convertirse en una realidad; de igual forma, critican que tampoco fue el caso de una reforma política 'robusta' como la propuesta en el Acuerdo.

Sobre la reincorporación y seguridad territorial, expusieron que el **"83% de los excombatientes siguen sin hacer parte de ningún proyecto productivo**"; situación que se suma al **asesinato de 56 excombatientes de agosto de 2018 a julio de 2019.**

En cuanto al cuarto punto, "Solución al problema de las drogas", el informe resalta que de las 99.097 familias vinculadas al Programa Nacional Integral de Sustitución (PNIS), el 94% cumplió con la erradicación concertada, pero **"solo el 0,7% han tenido acceso a proyectos productivos"**. Al incumplimiento en los pagos del programa, y el acompañamiento técnico se suma la intención del Gobierno de volver al uso de Glifosato para erradicar cultivos de uso ilícito, lo que implicaría "potenciales riesgos para la salud y al medio ambiente".

Los congresistas presentaron sus reparos en cuanto al desarrollo de la garantía de derechos para las víctimas, reseñando "un marco de enorme polarización, ataques jurídicos y desfinanciación" a la Jurisdicción Especial para la Paz (JEP), la Comisión para el Esclarecimiento de la Verdad (CEV) y la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD). También llamaron la atención sobre el proceso de reparación a las víctimas, al indicar que "**al ritmo que vamos nos tomaría más de 62 años culminar el proceso de reparación**".

Por último, en el documento se expresa el poco avance que ha tenido el componente de implementación, verificación y refrendación, porque **"no se ha hecho oficial una decisión sobre la continuidad del componente de verificación internacional"**. Asimismo, en cuanto al desarrollo del enfoque étnico, destacaron que el "4% de las disposiciones con enfoque étnico presentaban una implementación completa, 9% una implementación intermedia, 41% una implementación mínima, y 46 % no habían iniciado su ejecución".

### **"Hay un despertar de la conciencia en la población sobre estos asuntos"** 

El senador Cepeda manifestó que sí creen que se ha avanzado en algunos elementos, pero ello se debe a "la voluntad de Farc, que está haciendo su parte y ayudando a la implementación del Acuerdo", pero también a la bancada 'pro paz'  que está en el Congreso y a la ciudadanía. En esa medida, el líder político opinó que hay signos que muestran que cada vez más las personas e instituciones se vinculan seriamente a la construcción de la paz en Colombia, lo que significa que hay "un despertar de la conciencia en la población sobre estos asuntos".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_39649140" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39649140_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
