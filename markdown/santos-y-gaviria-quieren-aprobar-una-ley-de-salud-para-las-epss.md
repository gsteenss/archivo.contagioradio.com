Title: Santos y Gaviria quieren aprobar una Ley de salud para las EPS's
Date: 2015-01-27 21:20
Author: CtgAdm
Category: DDHH, Política
Tags: Jorge Robledo, Jose David Name, Ley estatutaria, Ministro Alejandro Gaviria, Salud, Santos
Slug: santos-y-gaviria-quieren-aprobar-una-ley-de-salud-para-las-epss
Status: published

##### Foto:lapatria.com 

###### [Jorge Robledo]

<iframe src="http://www.ivoox.com/player_ek_4006828_2_1.html?data=lZWdmJ2WfI6ZmKiakpiJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjtDmyMqYtNTGsMbY0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Mario Hernández]  
<iframe src="http://www.ivoox.com/player_ek_4006912_2_1.html?data=lZWdmJ6Vdo6ZmKiak5WJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLmytSYqsrWssLixcrnh5encYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El senador Jorge Robledo, denunció que el presidente Juan Manuel Santos y el Ministro de Salud, Alejandro Gaviria, **habían aprobado una ley estatutaria de salud, contraria a los derechos de los ciudadanos y que dejaba sin dientes a la tutela,** se trataba más de una ley en favor de las EPS.

Cuando la ley pasó a manos de la Corte Constitucional, se le realizaron profundos cambios y se “**le quito varias de las peores cosas que el ministro Gaviria le había introducido”,** dijo el congresista, quien afirma que la Corte logró mantener la tutela y los derechos ciudadanos.

Ahora existe una demora del Ministro y el Presidente para sancionar la Ley, es por eso que La Mesa Nacional por el Derecho a la Salud, está interponiendo una acción legal al presidente del Congreso, Jose David Name, para que se apruebe la Ley tal y cómo la dejó la Corte Constitucional, y de esta manera, Name, “no actué como alcahuete del Ministro Gaviria”, señala Jorge Robledo.

Pese a que Gaviria aseguró que no hay “desgano del Gobierno Nacional" para sancionar la ley,  sino hay una revisión por un error textual, el senador Robledo, afirma que el gobierno no aprueba la Ley porque **el ministro “tiene montada una conspiración para cambiarla nuevamente (…) porque no le sirve a los negociantes de la salud.”**, algo que sería inconstitucional, más cuando el ministro y el presidente tienen el deber de respetar la constitución, señala.

De esta manera, la nueva Ley estatutaria de salud no sería la que aprobó el "santismo" en el Congreso, sino la que estableció la Corte Constitucional, y el presidente Santos o el presidente del Congreso, son quienes deberían sancionar la ley, en lugar de violar la Constitución, de acuerdo a las palabras del senador Robledo.
