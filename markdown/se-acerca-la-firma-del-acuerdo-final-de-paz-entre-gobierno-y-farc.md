Title: Se acerca la firma del acuerdo final de paz entre gobierno y FARC
Date: 2016-08-24 11:30
Category: Paz
Tags: acuerdo final, FARC, Gobierno, paz, proceso de paz
Slug: se-acerca-la-firma-del-acuerdo-final-de-paz-entre-gobierno-y-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/CqleIyGXEAEMYca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Equipo Paz Gobierno 

###### [24 Ago 2016] 

Crece el optimismo por los anuncios en torno al acuerdo final entre el gobierno y las FARC. Ayer se culminó la etapa de conversaciones, lo que no significa que haya finalizado el proceso.

Después de las **3 de la tarde el presidente de Colombia, Juan Manuel Santos anunciaría la terminación del texto final de los acuerdos**, la fecha de la firma del acuerdo final y convocará a los colombianos a expresarse en el plebiscito frente a si están o no de acuerdo con lo pactado en La Habana. Una vez se vote el plebiscito aprobando los acuerdos, se iniciaría la concentración de los integrantes de las FARC en las zonas campamentarias ya acordadas.

“**Es un gran día para Colombia después de años de conversaciones y esfuerzos se culmina de manera exitosa la negociación del contenido de la agenda de paz y** con eso se pone el piso sólido para la construcción de la paz”, expresa el senador Iván Cepeda, desde La Habana.

Para que el acuerdo se materialice, faltaría también la realización de la décima conferencia nacional de las FARC donde “**el grupo guerrillero se transformará políticamente en una organización política”,** explica Carlos Gallego, profesor de la Universidad Nacional e integrante del Centro de Pensamiento y Seguimiento al Diálogo de Paz, quien también celebra lo logrado afirmando que “El acuerdo es racional para un Estado Social de Derecho y liberal, abriendo la puerta para impulsar los cambios constitucionales para que haya una paz verdadera”.

El profesor Medina, indica que en su visita a comunidades como El Orejón en Antioquia y Santa Helena en el Meta,  donde se adelantan las labores del desminado humanitario, pudo evidenciar que ahora los pobladores respiran un ambiente de tranquilidad, “Están felices, dicen que pueden salir tranquilos al campo, esperan la salud, educación, vías. **La gente se llenó de felicidad al ver sentados en la misma mesa a guerrilleros y soldados que están trabajando en el desminado”.**

Por su parte, León Valencia, director de la Fundación Paz y Reconciliación, señala, “Estábamos atados al siglo XX por esta guerra de más de 50 años”, ahora el reto que se acerca es la votación por el SI al plebiscito, además se debe trabajar para que se brinden verdaderas garantías para los guerrilleros en su reintegro a la vida civil; así mismo es indispensable que de lado y lado se cumplan los acuerdos y  la integración la sociedad por medio de la confianza, para “**desminar el odio, el miedo, el terror, y poder construir un nuevo futuro”,** expresa Carlos Medina, teniendo en cuenta que el gobierno ha incumplido con el 87% de los acuerdos que ha firmado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
