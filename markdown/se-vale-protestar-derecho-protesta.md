Title: “Se vale protestar”, la campaña que busca educar a la Policía sobre el derecho a la protesta
Date: 2019-04-25 18:01
Author: CtgAdm
Category: Cultura, Movilización
Tags: 25 de abril, marcha, Paro Nacional, Refugio Humanitario
Slug: se-vale-protestar-derecho-protesta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Promo-general-Garantizar-Protesta-Decálogo-DLPEP-1.mp3" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[  
especial paro nacional  
](https://archivo.contagioradio.com/paro-nacional-6/)  
<audio src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Promo-general-Garantizar-Protesta-Decálogo-DLPEP-1.mp3" preload="none" controls="controls"></audio>

###### Foto: Contagio Radio 

En medio de un Paro Nacional, una reciente Minga indígena y las constantes medidas antipopulares del gobierno que han despertado a indignación de muchos sectores de la población colombiana, nace la campaña “Se vale protestar” una herramienta pedagógica dirigida a la fuerza pública, las instituciones y la ciudadanía para que se garantice el derecho a la protesta y se entienda como un ejercicio válido y legal. (Le puede interesar: ["El 28 de abril abre las puertas el refugio humanitario por la vida de los líderes sociales"](https://archivo.contagioradio.com/refugio-humanitario-vida-lideres-sociales/))

Con una serie de 10 piezas audio, videos, dibujos con plastilina entre otros, 16 organizaciones, colectivos sociales, plataformas, defensoras de derechos humanos de diferentes regiones del país, que han contribuido a la generación de garantías para el ejercicio del derecho a la protesta en Colombia, buscan que con acciones de pedagogía y difusión, se propicie un ambiente favorable entorno a la protesta y movilización social, generando valoraciones positivas en la ciudadanía en general, en las entidades y en los servidores públicos.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fposts%2F10156158883345812&amp;width=500" width="500" height="669" frameborder="0" scrolling="no"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
