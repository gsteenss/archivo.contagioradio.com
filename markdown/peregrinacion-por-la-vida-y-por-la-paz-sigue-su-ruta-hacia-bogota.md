Title: Así avanza la peregrinación por la Vida y por la Paz
Date: 2020-10-29 15:07
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Excombatientes de FARC, Partido FARC, Peregrinación por la vida y por la paz, reincorporación farc
Slug: peregrinacion-por-la-vida-y-por-la-paz-sigue-su-ruta-hacia-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Peregrinacion-por-la-vida-y-por-la-paz-FARC-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Asesinatos-de-los-excombatientes-de-FARC.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Excombatientes de FARC en Norte de Santander con sus delegaciones de Cúcuta, Tibú, San Calixto y otros municipios del Catatumbo; se movilizan para **sumarse a la** **Peregrinación por la Vida y por la Paz en la que se exige el respeto y garantía a la vida de los excombatientes y el cumplimiento de los Acuerdos de Paz.** **(Le puede interesar:** [Caravana Humanitaria del Cañón del Micay: “un canto por la vida y la paz del territorio”](https://archivo.contagioradio.com/caravana-humanitaria-del-canon-del-micay-un-canto-por-la-vida-y-la-paz-del-territorio/)**)**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1321834098130911238","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1321834098130911238

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JuniorMaldo0/status/1321490707325112320","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JuniorMaldo0/status/1321490707325112320

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**La peregrinación inició el pasado 21 de octubre** en el municipio de Mesetas, pasando por Granada y Acacías en el departamento del Meta y arribaron a la ciudad de Villavicencio el pasado viernes 23 para participar en una programación de actividades que incluía el **“Acto simbólico en defensa de la vida y la construcción de paz en el Meta”** en la Plaza de Los Libertadores.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1320337595612696577","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1320337595612696577

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Varias delegaciones de excombatientes de diversas regiones del país han emprendido su camino para sumarse a la **movilización que tiene programado arribar a la ciudad de Bogotá** **el próximo 1° de noviembre para seguir “*exigiendo al Gobierno garantías de vida y cumplimiento con el Acuerdo de Paz*”.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“En nuestro peregrinar por la vida y por la paz reflexionamos sobre la paz y el impacto del conflicto en la región. Reiteramos nuestro compromiso y extendemos nuestra solicitud de perdón a las víctimas. Por paz para la no repetición.  Por Paz como único camino”*
>
> <cite>Rodrigo Londoño, director del Partido FARC</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Antioquia, fue otra de las regiones desde donde partieron hoy los excombatientes con rumbo a Bogotá, para exigir no solo el respeto a la vida de los firmantes, sino la de los líderes y lideresas sociales y la de las personas defensoras de Derechos Humanos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CNRFARC/status/1321802157859971073","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CNRFARC/status/1321802157859971073

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La movilización ha tenido acogida no solo por de los exintegrantes de FARC, sino por **diversos sectores de la sociedad colombiana quienes se han sumado a la movilización para hacer eco de los pedidos que se elevan ante el Gobierno Nacional;** gesto que ha sido agradecido por las diversas delegaciones que se han sumado a la [Peregrinación por la Vida y por la Paz](https://partidofarc.com.co/farc/2020/10/29/octavo-dia-de-la-peregrinacion-por-la-paz/).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AbadColorado/status/1321830245079699456","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AbadColorado/status/1321830245079699456

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Peregrinación por la Vida y por La Paz avanza en memoria a las y los 236 excombatientes asesinados

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Desde 2016, año en el que se firmó el Acuerdo de Paz entre el Estado colombiano y el extinto grupo armado de las FARC-EP; han sido asesinados 236 excombatientes, cinco fueron mujeres.** Asimismo 154 de estas personas fueron asesinadas en el periodo de gobierno del presidente Iván Duque. (Lea también: [«Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SandinoVictoria/status/1320870311529746441","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SandinoVictoria/status/1320870311529746441

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Solo en el presente año 2020 han sido asesinados 53 firmantes y 18 de estos casos sus restos se encuentran aún desaparecidos**. **Los cinco departamentos más afectados por este tipo de hechos son, Cauca con 38; Nariño con 28; Antioquia con 25; Caquetá con 22 y Meta con 21.** (Le puede interesar: [Dos excombatientes de FARC son asesinados en Cauca y Caquetá](https://archivo.contagioradio.com/dos-excombatientes-de-farc-son-asesinados-en-cauca-y-caqueta/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":92026,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Asesinatos-de-los-excombatientes-de-FARC.jpeg){.wp-image-92026}  

<figcaption>
Fuente: Partido FARC

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
