Title: Por una 'Menstruación Libre de Impuestos' no a la Reforma Tributaria
Date: 2016-12-09 14:51
Category: Mujer, Otra Mirada
Tags: Desigualdad de género, Menstruación libre de impuestos, Reforma tributaria, Violencia contra las mujeres en Colombia
Slug: por-una-menstruacion-libre-de-impuestos-no-a-la-reforma-tributaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/iva-a-toallas-higienicas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 Dic 2016 ] 

El equipo de Género y Justicia Económica de la Red por la Justicia Tributaria, inició una campaña llamada Menstruación Libre de impuestos, la cual denuncia que las mujeres serán directamente afectadas con el **aumento del IVA en 19% sobre productos de primera necesidad como toallas higiénicas, tampones y productos de aseo personal.**

Natalia Moreno investigadora del equipo de género comentó que “el IVA debe ser sólo a productos de lujo, pero las toallas y tampones no lo son, la menstruación es biológica no opcional y con el proyecto, **el gobierno nos dice que por ser mujeres y menstruar tenemos que pagar el impuesto”**, por ello la campaña busca concretar un plan de acción colectivo con organizaciones y movimientos de mujeres para tumbar la propuesta de Reforma gubernamental.

### **¿Cómo afecta la reforma el bolsillo de las mujeres?** 

“En promedio, una mujer usa 5 toallas en un día durante 5 días que dura aproximadamente la menstruación, serían 25 toallas cada 13 periodos que tienen las mujeres al año, es decir 325 toallas que a \$500 cada una, da un total de \$162.500. En Colombia hay 13 millones de mujeres en el período reproductivo que demandan dichos artículos lo que representa un aporte de 2,2 billones de pesos al recaudo del IVA de 16% y **con la reforma el aporte aumentaría alrededor de medio billón de pesos, una cifra inmensa e inadmisible”** señaló Moreno.

De igual modo, Natalia Moreno manifestó que la medida propuesta por el gobierno afectará sobre todo a mujeres de poblaciones vulnerables, pues “estamos en un país donde la brecha salarial entre hombres y mujeres es del 20%, donde más del 30% de los hogares tienen madres cabeza de familia, **donde el desempleo femenino supera el 13%, más del 21% entre las mujeres jóvenes y la mayor parte del trabajo es informal”.**

La investigadora indicó que en el primer debate de la reforma tributaria en el Congreso, el equipo de género expuso sus argumentos con cifras y otras observaciones a través del senador Jorge Robledo, pero **no obtuvieron respaldo y el proyecto para evitar el gravamen a los artículos de cuidado femenino, se cayó.**

Por otra parte, Moreno afirmó que “diferentes organizaciones de mujeres a nivel barrial de Bogotá y otros lugares del país se están uniendo a la iniciativa, se espera que se fortalezca la campaña, para que la próxima semana **en el segundo debate de la reforma tributaria ante el Congreso se logre mayor respaldo y pueda tumbarse la medida tan nociva del gobierno”.**

<iframe id="audio_14807379" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14807379_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
