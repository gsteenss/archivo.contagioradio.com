Title: Inicia paro de docentes en el departamento del Cauca
Date: 2019-02-25 12:06
Category: DDHH, Educación
Tags: 15 palestinos han fallecido por disparos israelíes en Gaza., Cauca, Docentes, Plan Nacional de Desarrollo
Slug: inicia-paro-de-docentes-en-el-departamento-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/asoinca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asoinca] 

###### [25 Feb 2019] 

La comunidad docente que hace parte de la Asociación de institutores y trabajadores de la Educación del Cauca, (ASOINCA), manifestaron a través de un comunicado de prensa que a partir de este 25 de febrero inician un cese de actividades producto del riesgo que afrontan líderes y lideresas sociales en el territorio, múltiples incumplimientos en acuerdos pactados previamente con el gobierno nacional y en contra de algunos puntos del Plan Nacional de Desarrollo de Iván Duque.

### **No hay garantías para docentes del Cauca** 

Según Fernando Vargas, presidente de ASOINCA, el sistema de salud que atiende a los docentes del departamento sigue siendo insuficiente, "hemos enterrado, por el pésimo servicio que prestan los particulares, a muchos educadores, a hijos de educadores y no vamos a permitir que esto siga sucediendo".

Asimismo, frente al Plan Nacional de Desarrollo, el docente expresó que la idea de profundizar la virtualización de la educación, retira el papel de los educadores en lugares en donde son vitales, "una cosa es aprender desde un apartado y otra cosa es tener la orientación y el seguimiento de un orientador" afirmó.

Igualmente, sobre a la implementación de la jornada única, Vargas aseveró que esa propuesta no va a resolver la brecha que existe entre el campo y la ciudad a la hora de acceder al derecho a la educación.

Sumado a ello, muchos campesinos del departamento,  hacen parte de acuerdos firmados con el gobierno para avanzar en la implementación de proyectos productivos, en los que solo existe un cumplimiento del 30% y acceder a mejoras en las viviendas, en los que no hay ningún avance.

En vista de la falta de garantías por parte del Estado a la ciudadanía, se conformó la Mesa de Derechos Humanos del Cauca, que desde hace 8 días ya envío el pliego de exigencias al gobierno, y que buscará lograr un espacio de concertación para poner fin estos hechos. (Le puede interesar[: "1800 docentes del Cauca viajan a Bogotá para continuar con paro indefinido"](https://archivo.contagioradio.com/1800-docentes-del-cauca-viajan-a-bogota-para-continuar-paro-indefinido/))

<iframe id="audio_32860734" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32860734_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
