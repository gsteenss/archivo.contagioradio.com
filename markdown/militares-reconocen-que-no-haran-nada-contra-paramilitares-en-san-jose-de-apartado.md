Title: Militares reconocen que “no harán nada” contra paramilitares en San José de Apartadó
Date: 2016-10-13 15:50
Category: DDHH, Nacional
Tags: Comunidad de Paz de San José de Apartadó, Fuerzas militares, Paramilitarismo
Slug: militares-reconocen-que-no-haran-nada-contra-paramilitares-en-san-jose-de-apartado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/San_José-apartado-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [13 Oct 2016]

La Comunidad de Paz de San José de Apratadó denunció que integrantes de las Fuerzas Militares afirmaron que no van a hacer nada contra los paramilitares que están acampados en las veredas Arenas Bajas y Arenas Altas desde hace más de 30 días, y que además “tergiversarán” todas las denuncias que se realicen desde la comunidad. Los paramilitares también afirmaron que su objetivo es quedarse en el territorio.

Según la denuncia de la Comunidad de Paz de San José de Apartadó, tanto los militares como los paramilitares están operando de manera similar porque construyen sus campamentos en medio de los cultivos de Pan Coger de las familias, dañan las siembras y profieren amenazas en contra de los habitantes de las veredas. **Los casos se han presentado en las veredas “la unión”, “arenas altas” y “arenas bajas”** ubicadas en zona rural. (Lea también: [Paramilitares atemorizan a comunidad de paz](https://archivo.contagioradio.com/comunidad-de-san-jose-de-apartado-atemorizada-por-presencia-de-grupo-paramilitar/))

Uno de los episodios más preocupantes para los integrantes de la comunidad de paz y los acompañantes internacionales, se dio cuando **se verificó directamente que los paramilitares están acampados en la vereda Arenas Bajas** y ante la solicitud de la comunidad para retirarse afirmaron que llegaron para quedarse “reiteran que no se van a ir de la zona, pues su principal objetivo es quedarse en los territorios” afirma la denuncia.

Otra de las preocupaciones gira en torno a la inacción por parte de los militares, puesto que los campesinos señalaron claramente que al solicitar al ejército el combate a los paramilitares **respondieron que están enterados pero que no realizarán ninguna acción y que van a desmentir lo que se afirme desde la comunidad**.

Según el relato de la comunidad los militares afirman que “ellos son conscientes de la presencia paramilitar en la región, pero que ellos no van a hacer nada, pues toda denuncia que realice esa HP de Comunidad de Paz, la van a tergiversar ante la opinión pública y comunidad internacional como *“montajes de la Comunidad de Paz”.* (Lea también: [Cuándo podrán vivir en paz los habitantes de la comunidad de paz](https://archivo.contagioradio.com/cuando-podra-vivir-en-paz-la-comunidad-de-san-jose-de-apartado/)*)*

Finalmente la Comunidad de Paz hace un llamado a la solidaridad de las organizaciones sociales tanto en Colombia como en el mundo, para que se exija coherencia por parte del Estado que en un momento de construcción de paz debería ser consecuente con las necesidades de las comunidades “**para que exijan una mínima coherencia a un Estado que hace continuas promesas de paz, mientras actúa en contravía de su discurso**.”
