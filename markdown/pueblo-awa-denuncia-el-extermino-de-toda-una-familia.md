Title: Pueblo AWÁ denuncia el exterminio de toda una familia
Date: 2017-10-20 14:33
Category: DDHH, Nacional
Tags: Asesinatos de indígenas, indígenas, indígenas Awá, pueblo Awá
Slug: pueblo-awa-denuncia-el-extermino-de-toda-una-familia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/pueblo-awa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unidad Indígena  del Pueblo Awá] 

###### [20 Oct 2017] 

De acuerdo con un comunicado del pueblo Awá, a las 7:30 am del 16 de octubre de 2017, **fueron asesinados Alirio Taicus Sabala y su hijo Geovanny Sabala Quistial**. Los asesinos fueron dos hombres “que se movilizaban en un carro Renault 9 los cuales al percatarse de la presencia de los dos indígenas, bajaron de su vehículo y sin mediar palabras dispararon ráfagas con los fusiles que cargaban”.

Los indígenas afirmaron que el exterminio de la familia de Taicus Sabala **“se viene perpetuando desde hace un año”**. El 2016 fueron asesinados dos hijos del señor Alirio Taiucus, Lorena Sabala Quistial y Richard Sabala Quistial, en el corregimiento de Guayacana.

Tras estos asesinatos, **quedaron 3 niños pequeños en total desamparo** pues estaban al cuidado del señor Alirio Taicus. Ante esto, el pueblo Awá manifestó su preocupación por “la ruptura dentro del tejido social de esta familia y del pueblo Awá en general”. (Le puede interesar: ["Indígenas Awá denuncian presencia de grupos armados en su territorio"](https://archivo.contagioradio.com/indigienas-awa-denuncian-presencia-de-grupos-armados-en-sus-territorios/))

Afirmaron que se trata de un hecho que **“acelera el exterminio físico, cultural y espiritual”** del pueblo Ancenstral “como lo contempla la Corte Constitucional, donde los Awá, al igual que 33 pueblos indígenas de Colombia, se encuentran en riesgo de desaparición”.

### **Presencia de grupos armados está afectando a las comunidades** 

Los Awá argumentaron que las comunidades de Nariño se han visto profundamente afectados por **“una guerra que libran varios grupos armados legales e ilegales**, que buscan tener el control territorial de la zona”. Indicaron que el Gobierno Nacional ha hecho caso omiso a esta situación y ha sido incapaz de cumplir los acuerdos que buscan la paz.

Adicionalmente, **rechazaron el asesinato del líder afrocolombiano** José Jair Cortés e indicaron que los hechos no son aislados pues hacen parte “de una serie de acciones de exterminio físico y cultural de las comunidades afro, indígenas y campesinas”. (Le puede interesar: ["Comunidades Awá en Tumaco: En medio del abandono estatal y la militarización"](https://archivo.contagioradio.com/comunidades_awa_tumaco/))

Finalmente, **denunciaron la falta de garantías por parte del Estado** colombiano “en el cumplimiento de los acuerdos pactados con las comunidades y el acatamiento del mandado de la Corte constitucional frente a la garantía de la vida y pervivencia del pueblo Awá”. Instaron a la comunidad internacional a asumir un rol de veedor “frente a las graves condiciones que asechan a nuestro pueblo y comunidades”.

[Comunicado 010 Awa](https://www.scribd.com/document/362151579/Comunicado-010-Awa#from_embed "View Comunicado 010 Awa on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_92339" class="scribd_iframe_embed" title="Comunicado 010 Awa" src="https://www.scribd.com/embeds/362151579/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-HR4vtkBfvrBwfsEkQTlz&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
