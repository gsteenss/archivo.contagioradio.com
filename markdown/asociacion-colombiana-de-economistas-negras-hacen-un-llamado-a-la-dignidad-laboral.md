Title: Nace Asociación Colombiana de Economias Negras
Date: 2020-07-26 08:51
Author: CtgAdm
Category: yoreporto
Slug: asociacion-colombiana-de-economistas-negras-hacen-un-llamado-a-la-dignidad-laboral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/DANE2_opt-700x525-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/El-día-de-la-magia.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:block {"ref":88544} /-->

<!-- wp:paragraph -->

En el día de la conmemoración de la **Mujer Afrolatina, Afrocaribeña y de la Diáspora Africana, mujeres negras, economistas y colombianas,** queremos continuar con los legados de emancipación desde un enfoque poco tocado hasta ahora; la visibilización, incidencia para la producción y análisis crítico de la información estadística y desagregada por sexo y pertenencia étnico racial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A nivel internacional, organismos como la Organización de los Estados Americanos (OEA), la Comisión Económica Para America Latina y el Caribe (CEPAL) el Comité para la Eliminación de la Discriminación Racial (CERD) y el Comité para la Eliminación de la Discriminación contra las Mujeres (CEDAW) **durante las últimas dos décadas han insistido en sus recomendaciones al estado colombiano sobre importancia de la desagregación** estadística para incrementar la efectividad de las medidas de politica pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Teniendo en cuenta esto, pero también los altos subregistros, la ausencia de rigor para que que exista representatividad étnica en las muestras de las principales encuestas nacionales, la ausencia de casillas para el reconocimiento étnico en los registros administrativos y de los sistemas de información institucionales, el desinterés alrededor de potenciar la construcción de variables, indicadores que retomen sentidos de organizar y vivir la vida desde compresiones culturales y territoriales; que mujeres negras, economistas y colombianas, de procedencias tan diversas como como Antioquia, Bogotá y Caquetá lanzamos hoy 25 de julio la Asociación de Economistas Negras Mano Cambiada, inspiradas y comprometidas en nuestra práctica ancestral “Mano Cambiada” que enseña desde la práctica que la utilidad individual no es posible sin propiciar la sostenibilidad de la utilidad colectiva.  

<!-- /wp:paragraph -->

<!-- wp:image {"id":87308,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/El-día-de-la-magia-1024x768.jpg){.wp-image-87308}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

**Conozcan la declaración sobre las Economias Negras**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como **Asociación Colombiana de Economistas Negras** nos inscribimos desde el movimiento social y político de mujeres negras y del movimiento social [afrocolombiano](https://archivo.contagioradio.com/otra-mirada-voces-contra-el-racismo-estructural/)y afrodiaspórico. En ese sentido, **ponemos nuestro trabajo a disposición de las luchas que se han dado y se darán** en el camino construido para la emancipación y la dignidad del pueblo que hacemos parte, y desde aquí, contribuir a [realidades económicamente](https://www.dane.gov.co/index.php/estadisticas-por-tema/demografia-y-poblacion/censo-nacional-de-poblacion-y-vivenda-2018/informacion-tecnica)más justas y dignas en Colombia y en el Mundo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La supremacía blanca ha conducido que en la economía exista una sobrerrepresentación de hombres blancos que han impuesto su forma de ver y analizar el mundo. **Rechazamos los supuestos de la teoría económica dominante (mainstream), entre ellos, los que consideran a los individuos homogéneos y optimizadores centrados en su utilidad personal**, que pueden tomar decisiones con información perfecta y completa e interactúan en un mercado indiferente entre los oferentes y demandantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Rechazamos el supuesto donde la tierra es comprendida exclusivamente como factor o recurso de producción. Encontramos un fracaso de los paradigmas, en tanto que niegan las relaciones sociales basadas en lo colectivo y la heterogeneidad humana, por tanto **asumimos la responsabilidad de repensar la teoría y la práctica de la economía para aprender y retomar** de forma crítica las comprensiones raciales, territoriales y culturales existentes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Es además innegable la baja participación de profesionales afrodescendientes en este campo y como la brecha se amplía por género**. Nos constituimos por tanto, en la primera asociación de economistas integrada por mujeres negras con carácter totalmente independiente en Colombia, orientadas por los **principios de solidaridad, cooperación, articulación, acompañamiento, compromiso por la emancipación económica**, contrahegemonía técnica, horizontalidad y  rigurosidad para la pertinencia. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Inicialmente somos tres asociadas *-esperamos ser más-* interesadas en la generación de in-formación, investigación y análisis crítico, de las situaciones socioeconómicas del pueblo negro en Colombia, particularmente de las mujeres negras en nuestras identidades diversas para el goce de derechos humanos y colectivos, en perspectiva de autodeterminación. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Somos una asociación independiente, autónoma y sin ánimo de lucro, cuyo destino, contenido y temas de publicación, son elegidos internamente.** Tenemos  la disponibilidad para recibir apoyo financiero que respalde y contribuya a nuestros propósitos. **Queremos trabajar principalmente con organizaciones de mujeres negras, construyendo de forma colectiva y retroalimentada** respuestas transformadoras de diferentes contextos y situaciones. También estamos interesadas en establecer articulaciones con organizaciones e instituciones que gestionen información y quieran comprometerse con la reducción de los sesgos análiticos propiciados por el racismo, el patriarcado y el clasismo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Consideramos de vital importancia contrarrestar el racismo estadístico y la falta de información desagregada por población étnica-racial y de género, una política que aunque necesaria ha sido ignorada por el Estado Colombiano o peor aún ha significado un “genocidio estadístico” como se pudo observar con el último censo nacional de población y vivienda. **Contrarrestar el racismo estadístico es una condición para fortalecer la toma de decisiones y la pertinencia en la formulación de las políticas públicas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Reconocemos que la tecnocracia ha sido un instrumento del racismo estructural del Estado, por tanto, el movimiento negro y de mujeres negras necesitan y merece más y mejores herramientas para negociar, debatir y defender nuestros derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esperamos consolidar un grupo amplio y unido que participe de los debates académicos y sociales en medio del respeto y la argumentación constructiva, los cuales nos permitirán seguir avanzando en la lucha por nuestra dignidad y por la eliminación de los sistemas de opresión que nos violentan como mujeresnegras.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
