Title: BOLETÍN INFORMATIVO ABRIL 28
Date: 2015-04-28 15:59
Author: CtgAdm
Category: datos
Slug: boletin-informativo-abril-28
Status: published

[Noticias del día:]

<iframe src="http://www.ivoox.com/player_ek_4420422_2_1.html?data=lZmfkpmWdo6ZmKiakpqJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjabGtsrgjJelj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-El **Ministerio de Salud** decidió acoger el reporte de la **Organización Mundial de Salud (OMS**), donde se concluye que el glifosato puede causar cáncer. Sin embargo el **Ministerio de Defensa** afirmó que las fumigaciones con glifosato continuarán hasta “nueva orden”. **Esaín Calderón**, médico y abogado de **Semillas de Montealegre**, Huila, habla sobre los efectos del herbicida.

-**La Movilización de Mujeres Afrodescendientes por el Cuidado de la Vida y los Territorios Ancestrales,** afirman estar cansadas de los constantes incumplimientos del Gobierno Nacional, habla **Francia Marquez**, integrante de la organización.

-Las organizaciones que hacen parte de la **Cumbre Agraria** aseguran que el Gobierno Nacional ha incumplido con la mayoría de los acuerdos pactados para mitigar la crisis del agro, es por eso que han solicitado una cita con el Presidente Juan Manuel Santos, para encontrar una solución. Habla **César Jerez**, representante de las **Zonas de Reserva Campesina**.

-El profesor de la **Universidad Nacional** **Luis Fernando Wolf**, de 64 años de edad, conocido por su compromiso activo en la lucha por los Derechos Sociales, fue asesinado en la mañana del miércoles 27 de abril en Medellín en inmediaciones de la Universidad Nacional Sede Medellín. **Oscar Zapata**, Presidente de la **Asociación de Profesores de la Universidad Nacional de Colombia**, recuerda la labor social del docente.

**-Boone Pickens**; el ex presidente **George HW Bush** y su familia; **Li Ka-shing** de Hong Kong; **Manuel V. Pangilinan**, y multimillonarios filipinos entre otros, estarían comprando terrenos donde hay enormes e importantes fuentes de agua, según un informe de la revista digital **Conexión México**, lo que afirma cada vez más la idea de que el derecho al agua está dejando de existir, debido a su la privatización de este recurso vital.

-Este lunes miles de maestros, trabajadores de la salud y estudiantes se tomaron las calles de Bogotá para exigirle al gobierno mejore las condiciones del magisterio, Así mismo, **Fecode**, notificó que no admiten más como interlocutora a la Ministra de Educación **Gina Parody** y pidieron un diálogo directo con el presidente de la República, **Juan Manuel Santos.**
