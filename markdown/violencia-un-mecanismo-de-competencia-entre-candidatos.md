Title: Violencia, un mecanismo de competencia entre candidatos
Date: 2019-09-16 17:50
Author: CtgAdm
Category: Comunidad, Política
Tags: Antioquia, Cauca, La Guajira, Norte de Santander, violencia electoral, Violencia Política
Slug: violencia-un-mecanismo-de-competencia-entre-candidatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Violencia-Electoral.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

Según la **Fundación Paz y Reconciliació**n (Pares), la violencia electoral que se está ejerciendo como un mecanismo de competencia política, revela un recrudecimiento de las acciones violentas contra candidatos y precandidatos, pues de los 20 asesinatos ocurridos durante los 10 meses del calendario electoral, 7 han ocurrido en los últimos dos meses, lo que apunta a que esta tendencia siga en ascenso conforme se acercan los comicios locales.

Desde el 27 de octubre de 2018, la violencia electoral se ha presentado en 24 departamentos, es decir el 75% de todo el territorio nacional, sin embargo el fenómeno se ha concentrado en departamentos como **Cauca, Valle del Cauca,  La Guajira, Tolima, y Antioquia**. Para Daniela Gómez, coordinadora de Gobernabilidad y Democracia de Paz en Pares,  este fenómeno está vinculado a algo que va más allá de los actores armados y es un mecanismo de competencia electoral usado entre contendores políticos, algo que se evidencia en departamentos como Tolima donde hay un gran número de amenazas, pese a ser un departamento que no tiene una alta presencia de actores al margen de la ley.

> Estamos volviendo a la política por medio de las armas

De igual forma, desde Pares señalan que el incremento en los asesinatos, atentados y desapariciones forzadas podría estar vinculado a la etapa en la que se encuentran las elecciones y donde ya existen candidatos oficiales que son más fáciles de identificar y que ejercen un liderazgo mucho más notorio frente a los votantes. [(Le puede interesar: Tres medidas para frenar la violencia política según la MOE)](https://archivo.contagioradio.com/tres-medidas-para-frenar-la-violencia-politica-segun-la-moe/)

El asesinato de la candidata a la **alcaldía de Suárez, Cauca, Karina García**, de **Yeison Obando,** aspirante al consejo municipal, y otras cuatro personas el pasado 1 de septiembre sumado al de **Orley García, candidato del Centro Democrático** a la alcaldía de Toledo, Antioquia, y al del **candidato por el Partido Conservador a la Alcaldía de Tibú, Bernardo Betancurt** quien fue víctima de impactos de bala mientras se encontraba en un acto de campaña en La Gabarra, en Tibú, Norte de Santander son reflejo de esta tendencia en ascenso.

### Pese a los pactos de no violencia electoral incrementan las amenazas y homicidios 

La coordinadora señala que las dinámicas electorales difieren de las nacionales, por lo que los pactos de no violencia no determinan una forma de prevención frente a las amenazas, asesinatos y demás actos de intimidación, "el candidato de Tíbú había firmado un pacto de no violencia, sin embargo fue asesinado", resalta. [(Lea también: La importancia de firmar un pacto por la no violencia en época electoral)](https://archivo.contagioradio.com/pacto-por-la-no-violencia-electoral/)

Gómez advierte que en este punto a tan solo unas semanas de las elecciones, no se puede realizar un cambio estructural para evitar estos mecanismos violentos sino que es necesario, reajustar el presupuesto y reforzar la seguridad de los candidatos, priorizando las zonas de mayor riesgo.  [(Lea también: Comunidades del Cauca no van a dejar que el terror se apodere de sus territorios) ](https://archivo.contagioradio.com/comunidades-cauca-no-terror/)

Su informe también revela que las coaliciones de Gobierno y los partidos políticos de oposición son los sectores más afectados por esta tendencia, pues tienden "a ser los sectores que están en ascenso o tomando fuerza desde las pasadas elecciones", sin embargo, destaca Gómez, si se miran los ataques de forma individual, el más afectado ha sido el Partido de la U, lo que revela que personas pertenecientes a partidos independientes también han sido víctimas de violencia electoral.

Aunque destacan que la violencia podría estar vinculada al accionar de los mismos candidatos, el informe señala a su vez que los responsables de los actos de violencia electoral siguen sin ser identificados, algo que aplica para el 71% de las víctimas, mientras que el 29% restante corresponde a la autoría de grupos como Las Águilas Negras, las autodenominadas Autodefensas Gaitanistas de Colombia, el ELN, Muerte Enemigos de la Patria (MEP), Caparrapos, Fuerza Pública y disidencias de las FARC.

<iframe id="audio_41571792" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41571792_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
