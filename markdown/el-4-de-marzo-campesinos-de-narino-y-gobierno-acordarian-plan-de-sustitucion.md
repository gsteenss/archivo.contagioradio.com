Title: El 4 de Marzo campesinos de Nariño y Gobierno acordarían plan de sustitución
Date: 2017-02-27 17:41
Category: DDHH, Nacional
Tags: cocaleros, Cultivos de uso ilícito
Slug: el-4-de-marzo-campesinos-de-narino-y-gobierno-acordarian-plan-de-sustitucion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/COLP_132549.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La FM] 

###### [27 Feb. 2017] 

Este lunes se conoció de un **acuerdo preliminar entre campesinos y el gobierno de Nariño. El acuerdo se dio luego de seis días de que los campesinos protestaran en la vía Tumaco - Pasto** para exigirle al gobierno frene la erradicación forzada de cultivos en sus territorios y cumpla con lo pactado en el punto cuatro de los Acuerdos de Paz de La Habana y los programas de sustitución de cultivos de uso ilícito.

Este primer acuerdo contempló el **desbloqueo de la vía hasta el próximo 4 de marzo cuando arribaría una delegación del Gobierno Nacional a la zona**, quien se reuniría con las comunidades para llegar a acuerdos sobre la forma como debe ser tratado el tema de sustitución de cultivos ilícitos. Le puede interesar: [6 Campesinos heridos  en manifestaciones contra erradicación forzada](https://archivo.contagioradio.com/campesinos-exigen-presencia-del-gobierno-en-tumaco/)

El encuentro se dio en la vereda Pulgande, en el kilómetro 53 de la vía al mar, en el que el Gobernador de Nariño, Camilo Romero y representantes de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana, COCCAM, junto con voceros del Gobierno, dialogaron y recalcaron la **importancia de Plan Nacional Integral de Sustitución de Cultivos Ilícitos que se desprende de los diálogos de paz de La Habana, Cuba, entre el Gobierno y la guerrilla de las Farc.**

Según la COCAM, **estos planes deben estar acordes y responder a las necesidades de las familias de Nariño.** Por el momento, los campesinos adelantan un censo para establecer cuántas familias dependen de los cultivos ilícitos y construir su plan de sustitución.

Cabe recordar que según la ONU, **Nariño es el departamento que tiene más cultivos de uso ilícito** en el territorio nacional con cerca de 30 mil hectáreas y en Tumaco hay 16 mil hectáreas de estos cultivos. Le puede interesar: [ESMAD atacó a campesinos que protestaban contra erradicación forzada](https://archivo.contagioradio.com/campesinos-exigen-al-gobierno-frenar-erradicaciones-forzadas/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
