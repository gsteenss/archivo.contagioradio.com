Title: Sigue la policía en la Universidad Nacional
Date: 2015-10-29 12:14
Category: Educación, Nacional
Tags: Derecho a la eduación, Ignacio Mantilla, Militarización de la Universidad, Universidad Nacional, Universidad pública
Slug: por-orden-de-rectoria-universidad-nacional-amanecio-con-presencia-policial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/UN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RevistaHekatombe 

<iframe src="http://www.ivoox.com/player_ek_9207211_2_1.html?data=mpedmZeVdY6ZmKiakp2Jd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNDmjNTfxsrSb8XZjNfSxdnTtoa3lIqupsaPmc%2Fd18rf1c7IpcWfr8bQy9TSpc2fwtLO0MrHrYa3lIqvlZDHcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mateo Córdoba, estudiante UNAL] 

###### [29 oct 2015]

El día de hoy la **Universidad Nacional sede Bogotá amaneció con las puertas cerradas y con presencia de la policía** que está realizando requisas de los bolsos por orden de las directivas que según indican en un comunicado es para “garantizar el normal funcionamiento de la sede” y para proteger a la “comunidad universitaria”.

Mateo Córdoba, estudiante de sociología de la Universidad Nacional, indica que el cierre se da con la excusa de evitar la [**celebración del "Aquelarre"**] que se venía realizando todos los años al interior de la universidad a finales de octubre que ha dejado de realizarse por órdenes "arbitrarias" de las directivas,  lo cual es una “excusa para cerrar y militarizar la universidad”, afirma Córdoba.

Además, según explica el estudiante de último semestre de Sociología, se impide la entrada de las personas que viven de las ventas informales al interior de la Universidad y se imponen medidas que van generando un ambiente de privatización de la educación pública.

Estas medidas de cierre **podrían alargarse hasta el día de mañana y han acarreado problemas para el desarrollo de las actividades académicas**, hoy habían “muy pocos estudiantes, hubo gente que no pudo entrar para el parcial”, indica el estudiante,  además que la policía estaba efectuando requisas en la entrada, lo cual también ha impedido el ingreso de muchos, pese a que hay clases en la jornada habitual.

[![12047179\_1498097583851328\_4399616414667765847\_n (3)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/12047179_1498097583851328_4399616414667765847_n-3.jpg){.aligncenter .wp-image-16445 width="512" height="376"}](https://archivo.contagioradio.com/por-orden-de-rectoria-universidad-nacional-amanecio-con-presencia-policial/12047179_1498097583851328_4399616414667765847_n-3/)

Por otra parte Córdoba denuncia que el lunes pasado **las huertas que habían hecho los estudiantes** de Ciencias humanas, de agronomía, artes plásticas y otras facultades y carreras, las cuales estaban trabajando desde hace un año, **las directivas las destruyeron** y justificaron que “la ley dice que no se puede sembrar cualquier cosa”, arbitrariedades  a las cuales ya están acostumbrados “ya no se hacen extrañas”, afirma Córdoba, pero que están llegando a límites “intolerables”, contra las actividades que proponen los estudiantes.

Desde que **Ignacio Mantilla obtuvo la rectoría en el año 2012** “cualquier argumento es base para no dejar entrar” a la universidad, con lo cual se ha impuesto un “cerco" al estudiantado para efectuar actividades culturales, académicas y administrativas. Frente a estos hechos los estudiantes han buscado mediante actividades como plantones y marchas al interior de la universidad y han querido **generar diálogo con el rector** pero que “generalmente nos dejan plantados”.

<div class="fb-video" data-allowfullscreen="1" data-href="/Juan.Esteban.Restrepo.M/videos/vb.688471747/10153332113226748/?type=3">

</div>
