Title: Claudia López y Antanas Mockus se unen a Gustavo Petro
Date: 2018-06-08 11:56
Category: Paz, Política
Tags: Antanas Mockus, Claudia López, Colombia Humana, Gustavo Petro, presidenciales
Slug: claudia-lopez-y-antanas-mockus-se-unen-a-gustavo-petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/mockus.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [08 Jun 2018] 

A la candidatura de Gustavo Petro se suman los integrantes del Partido Verde, **Claudia López y Antanas Mockus.** Esto tras haberse reunido con el candidato presidencial en donde le pidieron que se comprometa con los puntos programáticos de ese partido. En la plaza del Voto Nacional los senadores recordaron que su respaldo a Petro hace parte de un voto libre y consciente.

El candidato presidencial se comprometió entre algunas a cosas a defender la división de poderes que rige la democracia en Colombia, se comprometió a defender el **Estado Social de Derecho**, las libertades de los ciudadanos y ratificó que no va a expropiarle nada a nadie. Por el contrario, enfatizó en que va a impulsar el emprendimiento y la formalización de la economía por medio del trabajo digno.

### **"Un gobierno de Duque, implica una tiranía del Uribismo"** 

Por su parte, Claudia López recordó la oportunidad que tiene Colombia en las urnas para "**derrotar a las maquinarias políticas** tradicionales para tener un gobierno de la ciudadania y un gobierno del cambio". Afirmó que "Colombia está a una X de generar un cambio donde se le quite el poder a la política tradicional como la parapolítica y la mermelada".  (Le puede interesar:["Pastores y pastoras de algunas iglesias anuncian apoyo a Gustavo Petro"](https://archivo.contagioradio.com/pastores-y-pastoras-de-algunas-iglesias-anuncian-apoyo-a-gustavo-petro/))

Recordó que "no sería justo ni respetuoso decir que si no es con uno no es con nadie". Esto haciendo énfasis **en los votos en blanco** que aún tiene suficientes electores. La senadora indicó que un gobierno de Iván Duque, implica "que el Uribismo controle las tres ramas del poder y estaríamos frente a una tiranía".

La fórmula vicepresidencial, Ángela María Robledo, **le agradeció a Mockus y López** por el apoyo al proyecto político. Mockus afirmó que es necesario que la sociedad colombiana confíe en un gobierno "que respeta la independencia" y proteje los derechos de las personas.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
