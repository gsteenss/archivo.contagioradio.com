Title: El Estado nos dio la espalda, Cuba acogió a nuestros hijos: Militares por la Reconciliación
Date: 2020-02-17 18:56
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Comité de Reconciliación, Fuerzas militares
Slug: cuba-acogio-nuestros-hijos-militares-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Mayor-Maldonado.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Mayor-Maldonado-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Reconcliación.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Cuba: Comité de Reconciliación

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La [Fundación Militares por la Reconciliación](http://Dentro%20de%20las%20personas%20beneficiadas%20por%20las%20becas%20otorgadas%20por%20Cuba,%20no%20solo%20hay%20excombatientes,%20también%20están%20hijos%20de%20integrantes%20de%20la%20Fuerza%20Pública) respondió a las declaraciones de la senadora María Fernanda Cabal, quien criticó las becas otorgadas por Cuba a jóvenes excombatientes para acceder a la educación superior**, aclarando que no solo los reincorporados fueron beneficiados con la posibilidad de estudiar medicina en Cuba**, sino que dentro de este grupo también existen hijos de los militares quienes no habían podido formarse como profesionales en la salud en Colombia y que ahora en el extranjero pueden cumplir este sueño.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A nombre de los 9.102 militares que hacen parte de esta organización, el mayor (r) **César Maldonado,** director de la colectividad señaló que las becas también fueron destinadas **a hijos de integrantes de la Fuerza Pública a quienes ni el Estado nunca incluyó en programas de reincorporación** y a quienes el Congreso tampoco prestó atención.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Es una verdadera tragedia humana. Sus hijos quedaron en la miseria, sin posibilidades de acceder a la educación superior",** afirma el militar al referirse a los cerca de 3.000 integrantes de las Fuerzas Militares quienes después de someterse a la Jurisdicción Especial para la Paz (JEP) y recuperar la libertad, no han podido ubicarse laboralmente debido a sus antecedentes penales. [(Lea también: ¿Por qué militares piden que se abra una agencia de reincorporación para ellos?)](https://archivo.contagioradio.com/militares-piden-que-se-abra-una-agencia-de-reincorporacion-para-ellos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Hemos estado abandonados por el mismo Estado, encontramos en la isla la posibilidad de que nuestros hijos puedan hacer la carrera de medicina y puedan servir al país"** explica, refiriéndose a los 20 jóvenes, hijos de integrantes de la Fuerza Pública que fueron beneficiados con las becas otorgadas por el país donante.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"No solo se trata de ayudar a los excombatientes, Cuba quieren poner ese granito de arena en la reconciliación que pueda consolidar este proceso de paz" afirma en respuesta a la senadora del Centro Democrática. Se espera que otros 15 jóvenes hijos de integrantes de lasa FF.MM. hagan parte de ese grupo en La Habana y cumplan una meta que en Colombia parece casi imposible de realizar para un sector menos favorecido de la población. [(Lea también: Fuerte respuesta de jóvenes becados en Cuba a María Fernanda Cabal)](https://archivo.contagioradio.com/fuerte-respuesta-de-jovenes-becados-en-cuba-a-maria-fernanda-cabal/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":80832,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Mayor-Maldonado-1-790x1024.jpeg){.wp-image-80832}

</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
