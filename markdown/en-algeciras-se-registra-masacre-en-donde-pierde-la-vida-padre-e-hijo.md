Title: Padre e hijo, víctimas de nueva masacre en Algeciras
Date: 2020-09-23 09:25
Author: CtgAdm
Category: Actualidad, Nacional
Tags: Algeciras, Huila, masacre, violencia
Slug: en-algeciras-se-registra-masacre-en-donde-pierde-la-vida-padre-e-hijo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-23-at-10.43.34-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*Foto: Pares*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este 22 de septiembre en horas de la noche se registró **una nueva [masacre](https://archivo.contagioradio.com/asesinan-a-joven-de-16-anos-y-a-comunera-indigena-en-cauca-y-narino/)en zona rural del municipio de Algeciras, departamento del Huila en donde fueron asesinadas tres personas,** dos de ellas hacían parte del mismo núcleo familiar.

<!-- /wp:paragraph -->

<!-- wp:core-embed/instagram {"url":"https://www.instagram.com/p/CFfAYZoJ9Ax/?igshid=9a91ofn2x648","type":"rich","providerNameSlug":"instagram","className":""} -->

<figure class="wp-block-embed-instagram wp-block-embed is-type-rich is-provider-instagram">
<div class="wp-block-embed__wrapper">

https://www.instagram.com/p/CFfAYZoJ9Ax/?igshid=9a91ofn2x648

</div>

</figure>
<!-- /wp:core-embed/instagram -->

<!-- wp:paragraph {"align":"justify"} -->

Según el Centro de Pensamiento sobre el Conflicto Armado, Paz y Postconflicto ([Pares](https://pares.com.co/2020/09/17/estamos-profundamente-preocupados-por-la-paz-de-colombia-onu/)), hombres armados ingresaron a una vivienda en la vereda Quebradon Sur y **asesinaron a Jimmy Betancourt Ortiz de 43 años y a Alejandro Betancourt de 17 años, padre e hijo. La tercera víctima era un colaborador de la finca, identificado como Camilo Rayo de 18 años.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marthaperaltae/status/1308762335314665477","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1308762335314665477

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

La comunidad de Algeciras, Huila ha vivido diversos hechos de violencia en los últimos meses: **el pasado 16 de julio seis hombres armados asesinaron a cuatro personas e hirieron a dos más**, hecho que género el desplazamiento forzado de los familiares de las víctimas, quienes denuncian que los hechos continúan en la impunidad y pese haber salido el municipio siguen recibiendo amenazas de los agresores.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según analistas desde los 90 y comienzos de 2000, Algeciras se posicionó como el corredor clave en la ruta del narcotráfico que comunica al departamento del Huila con el del Cauca y Tolima; **abriendo así las puertas del narcotráfico de cocaína al Pacífico colombiano y de allí a otros países.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A esta situación conocida y denunciada por defensores de Derechos Humanos y la comunidad del Huila, se suman los cinco excombatientes de Farc asesinados durante el 2020, **así como los tres líderes sociales que perdieron su vida también en Algeciras.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
