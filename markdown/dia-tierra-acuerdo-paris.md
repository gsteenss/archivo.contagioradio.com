Title: Ecologistas en Acción cuestiona acuerdo de París en el Día de la Tierra
Date: 2016-04-22 17:14
Category: Ambiente, Nacional
Tags: cambio climatico, contaminación, COP 21, Día de la tierra, París
Slug: dia-tierra-acuerdo-paris
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/COP-211.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: imp[actoevangelistico.net]

###### [22 Abr 2016]

Este 22 de abril se conmemora a nivel mundial el Día de la Tierra, fecha en la que más de 170 países firman en la sede de la ONU en New York, el acuerdo alcanzado durante la Conferencia de las Partes en París, COP 21, a finales de 2015. **Un acuerdo muy criticado por las organizaciones sociales y ambientalistas que lo consideran insuficiente** para generar verdaderos cambios frente al cambio climático.

Como **“inhumano, engañoso y esquizofrénico”**, lo señaló en su momento, Tom Kucharz, integrante de Ecologistas en Acción. Para él, este acuerdo acepta implícitamente que la temperatura del mundo pueda aumentar en los próximos años entre 2,5 ºC y 3,7 ºC, jugando a favor de los intereses del modelo económico actual que implica “la muerte, el desplazamiento y el sufrimiento de millones de personas”, expresa Kucharz.

Asimismo, aseguró que se trata de un pacto “inhumano”, debido a que “**festeja las limosnas que se dan a los países más pobres para financiar las medidas de adaptación al cambio climático con el Fondo Verde para el Clima”,** en cambio, es injusto porque impide anclar el derecho a compensación por daños y pérdidas, pues Estados Unidos y la Unión Europea lograron incluir una demanda con la cual los países más afectados por el aumento de la temperatura deberán renunciar su derecho legal a demandar a otros países, como EE.UU, responsables de la contaminación en el mundo.

En ese marco, este 22 de abril se señalan cifras alarmantes que evidencian la grave situación actual que vive el planeta tierra y por lo cual se verían como insuficientes las medidas que se tomarán para disminuir los niveles de contaminación en la capa de ozono.

De acuerdo con la organización World Wildlife Fund for Nature, WWF, una tercera parte de las especies animales del planeta podrían extinguirse sino se actúa desde ya para frenar el cambio climático; según un estudio publicado en la revista Nature, cerca de 3,3 millones de personas mueren anualmente en el mundo por enfermedades a causa de la contaminación en el aire; y otro estudio publicado en la revista Science, asegura que dos tercios de la población mundial experimenta una grave escasez de agua al menos un mes al año; además para el  2050 los mares y océanos tendrán más desechos plásticos que peces, como lo afirmó una investigación de la revista Science con apoyo de la fundación de la navegadora Ellen MacArthur.

Es decir que la situación requiere un mayor compromiso, no solo de los gobiernos, sino de la totalidad de la población del planeta. “**Se ha demostrado que la sociedad civil no debe esperar nada de sus gobiernos”**, sostiene Tom Kucharz, de manera que la movilización social, traducida no solo en salir a las calles sino en generar verdaderos cambios a través del empoderamiento desde cada cotidianidad y cada comunidad es la verdadera esperanza que hoy existe si realmente se quiere salvar el planeta.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
