Title: Bombas, montajes y garantías de no repetición
Date: 2015-07-13 14:22
Category: Nicole, Opinion
Tags: congreso de los pueblos, Movimiento social, Petardos en bogota
Slug: bombas-montajes-y-garantias-de-no-repeticion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Captura-de-pantalla-2015-07-13-a-las-13.09.33.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por [Nicole Jullian](https://archivo.contagioradio.com/nicole-jullian/) **] 

###### [13 Jul 2015] 

De las 13 personas detenidas por su presunta participación en la activación de artefactos explosivos en la ciudad de Bogotá ya son 8 los imputados. Esta semana debiera resolverse la situación judicial de los 5 detenidos. Entre los diferentes delitos que les imputa la Fiscalía figuran el de terrorismo y el de rebelión. El ente acusador dice tener pruebas contundentes.

Nadie pone en duda que las bombas no hayan sido colocadas y algunas de ellas detonadas. El punto es determinar con base en una investigación seria quién está detrás de estas acciones antes de asignar responsabilidades basadas en meras corazonadas. Es inaceptable que a los detenidos se les acuse de entrada de ser terroristas, de pertenecer a células urbanas del ELN y en palabras del Presidente Santos de ser responsables de los petardos en Bogotá. Es sumamente grave que no se les respete su derecho a la presunción de inocencia ... mirando esta situación no puedo dejar de pensar en las pobres garantías de no repetición que anda prometiendo el gobierno ...

En Colombia han habido montajes emblemáticos en torno a la detonación de bombas. ¿A caso ya se habían olvidado de la seguidilla de bombas que fueron detonadas y otras que „lograron ser desactivadas“ en los días previos y posteriores a la poseción de mando del ex-presidente Uribe en agosto del 2006? Me refiero, por ejemplo, al taxi bomba en el centro comercial Caracas, en el sur de la ciudad, y al carro bomba ubicado en el noroccidente, muy cerca de la Escuela de Cadetes, donde murió una persona y alrededor de 15 militares quedaron heridos.

Mientras Santos (en ese momento en su posición de Ministro de Defensa) ofrecía 1.000 millones de pesos por información veraz, el mayor Javier Efrén Hermida Benavídez y el capitán Luis Eduardo Barrero (adscritos a la Regional de Inteligencia Militar del Ejército) cobraban su dinero por haber „montado“ un clima de zozobra antes de que asumiera Uribe. Al poco tiempo después el general Mario Montoya pedía (por orden de Santos) disculpas públicas al país por el montaje y el fiscal general Mario Iguarán confirmaba que el supuesto atentado del centro comercial Caracas fue un montaje en el que intervinieron dos oficiales del Ejército. Lo absurdo de esta telenovela judicial es que la fiscalía le imputó a los dos militares el delito de transporte ilegal de explosivos y estafa agravada. Iguarán adujo incluso en su momento que "no se trató de un acto terrorista, tampoco de una acción de inteligencia militar, fue un grosero montaje en el que intervinieron estos dos oficiales".

No estoy de acuerdo con lo publicado en El Espectador este pasado sábado: *„Quienes se oponen a las detenciones sienten que se está reviviendo la persecución histórica a los activistas de izquierda con el fin de encontrar chivos expiatorios que tranquilicen a los ciudadanos“.* Aquí no se trata de evaluar sentimientos subjetivos, sino de confirmar con base en hechos que los métodos de persecución política en contra del robusto movimiento social y de derechos humanos colombiano siempre ha existido, naturalmente con más o menos intensidad dependiendo de los falsos logros militares y policiales que se quieran presentar. Son graves acusaciones y la prensa una vez más apoyó la lógica terrorista del estado colombiano. Para sorpresa de muchos incluso Hollman Morris apareció orquestando las acusaciones contra los 13 detenidos.

Quedo preocupada por la actuación de Hollman Morris y por la fantasiosa realidad en la que vive el consejo editorial de El Espectador. Pero aún más me preocupa el hecho de que para algunas organizaciones el caso de Paola Salgado sea el caso „más preocupante“ dentro de los 13 detenidos. Es un claro error valorar la política de persecución del Estado colombiano dependiendo de quién es el perseguido o la perseguida. Los montajes, los falsos positivos judiciales, la permanente violación de  derechos y la persecución política son hechos que definen la esencia del Estado colombiano; el cumplimiento de las garantías de no repetición pasa entonces inevitablemente por el desmantelamiento total de la actual estructura estatal, aún así suene esto una locura. Y es que no hay de otra!

El movimiento social y de derechos humanos colombiano cae una y otra vez en el error de afirmar que unas víctimas son más importantes que otras, siendo que sabe que en este campo de batalla el enemigo es el mismo y que sus acciones represivas repercuten sin distinción sobre todo aquel que piense distinto.
