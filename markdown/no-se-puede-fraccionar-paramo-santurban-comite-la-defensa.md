Title: "No se puede fraccionar el Páramo de Santurbán": Comité para la Defensa
Date: 2019-03-27 06:47
Category: Ambiente, Entrevistas
Tags: Corte Constitucional, Ministerio de Ambiente y Desarrollo Sostenible, Páramo de Santurbán
Slug: no-se-puede-fraccionar-paramo-santurban-comite-la-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Paramo-de-Santurban-e1472756604235.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Macondo] 

###### [26 Mar 2019] 

El Ministerio de Ambiente y Desarrollo Sostenible tiene hasta el **16 de julio** para realizar una nueva delimitación del Páramo de Santurbán después de que la Corte Constitucional tumbara la primera por no consultar con las comunidades involucradas. Sin embargo, las comunidades denuncian que sus sentires en este nuevo proceso de demarcación están siendo ignorados por el Gobierno.

Así lo afirmó Mayerly López, vocera del **Comité por la Defensa del Páramo de Santurbán**, quien indica que si bien el Ministro de Ambiente, Ricardo Lozano, realizó reuniones con las comunidades en tres municipios en[ Santander y Norte de Santander, aún no ha convocado una mesa de trabajo con las comunidades o una reunión con el Comité, uno de los accionantes de la tutela en contra de la primera delimitación.]

"Lo mínimo que debió haber hecho a estas alturas es que el Ministro nos hayan escuchado, ya se haya pronunciado sobre la propuesta alternativa de delimitación que nosotros le planteamos, pero por ahora, **hay un silencio rotundo por parte del Ministro Lozano**", sostuvo la vocera, teniendo en cuenta que el plazo vence en menos de tres meses.

Al respecto, el Tribunal Administrativo de Santander abrió un incidente de desacato en contra del Ministerio de Ambiente el pasado 20 de marzo por **no realizar avances significativos entre noviembre de 2018 y marzo de 2019**. El informe del Tribunal también deja constancia de la Procuraduría y Defensoría del Pueblo “por la falta de claridad y el poco tiempo que queda para entregar la nueva delimitación”.

Por su lado, el Ministro Lozano indicó que no ha recibido una invitación por parte de las comunidades para tenerlos en cuenta en el nuevo proceso, lo cual no es la responsabilidad de ellas señala López. Al contrario, la sentencia de la Corte indicó que **el Gobierno debería garantizar el derecho a la participación plena**.

### **La propuesta de las comunidades** 

La delimitación del Páramo debe proteger el agua que produce este ecosistema para más de 2.000.000 de personas en Santander y Norte de Santander. Por tal razón, no se pueden fragmentar los ecosistemas del páramo como se hizo en la primera demarcación. La vocera señala que esta decisión favorece a la empresa árabe **Sociedad Minera de Santander (Minesa)**, que actualmente presenta solicitud de licencia ambiental para un proyecto de extracción de oro en las montañas adyacentes del páramo.

"No estamos de acuerdo con que se fraccione la montaña para legalizar el proyecto de Minesa. Santurbán necesita de las zonas de bosques altoandinas para funcionar porque si se llegara a afectar, se va afectar todo el ecosistema en su integralidad". (Le puede interesar: "[Páramo de Santurbán nuevamente amenazado por solicitud de Minesa](https://archivo.contagioradio.com/solicitud-explotacion-oro-presenta-nuevas-amenaza-al-paramo-santurban/)")

### **La movilización de las comunidades** 

Frente al silencio del Ministerio de Ambiente, las comunidades solicitó el pasado 15 de marzo que el Relator Especial de Naciones Unidas visite el ecosistema, elabore un informe sobre el caso y intermedie para que el Estado colombiano adopten las acciones necesarias para proteger el Páramo.

Además, están recolectando firmas para que la Anla convoque una audiencia pública. En esta reunión, estarían presentado argumentos por los cuales la autoridad debería rechazar la solicitud de licencia ambiental de Minesa. Asimismo, también estarán convocando una gran marcha en Bucaramanga para el **próximo 10 de mayo** a las 2 de la tarde.

<iframe id="audio_33775403" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33775403_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
