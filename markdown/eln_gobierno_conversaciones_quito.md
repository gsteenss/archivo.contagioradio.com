Title: Quinto ciclo de conversaciones entre ELN y gobierno irá hasta el 18 de mayo
Date: 2018-03-15 16:25
Category: Nacional, Paz
Tags: ELN, Gobierno, proceso de paz, Quito
Slug: eln_gobierno_conversaciones_quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/98c5ba09-bb46-4488-8881-91d39d3798c8.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Tomás García Laviana] 

###### [15 Mar 2018] 

Ante la reanudación de las conversaciones de paz, las delegaciones del Gobierno Nacional y el ELN reunidos en Quito, Ecuador, manifestaron que en este quinto ciclo de conversaciones abordarán los puntos referentes a **la participación de la sociedad civil y la evaluación del Cese al Fuego Bilateral** que había culminado el pasado 9 de enero.

De igual forma, como algunos analistas habían considerado, se buscará **propiciar un acuerdo humanitario en el Chocó, se avanzará en el desminado humanitario, y también se trabajará en la pedagogía del proceso** hacia el país y en lo relativo al Grupo de Países de Apoyo, Acompañamiento y Cooperación (GPAAC).

Según el comunicado conjunto número 17, dicho ciclo podría tener una **duración de nueve semanas que finalizarían el 18 de mayo.** Se espera, como lo había explicado el analista, Luis Eduardo Celis, que la dinámica de este ciclo sea en dos mesas en las que se trabaje los temas mencionados. (Le puede interesar: [Cese bilateral y participación civil: temas claves en Quito)](https://archivo.contagioradio.com/cese_bilateral_gobierno_eln_mesa_quito/)

Las delegaciones retomaron el diálogo en una hacienda al sureste de Quito este jueves sobre las 10:00 de la mañana, en Cashapamba. Allí agradecieron la presencia de los países garantes del proceso, Brasil, Chile, Cuba, Noruega, Venezuela y especialmente a Ecuador, país anfitrión de este quinto ciclo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
