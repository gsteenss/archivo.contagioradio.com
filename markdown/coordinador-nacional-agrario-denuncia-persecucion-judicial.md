Title: Coordinador Nacional Agrario denuncia persecución judicial
Date: 2017-06-05 16:21
Category: DDHH, Nacional
Tags: Coordinador Nacioinal Agrario, lideres sociales
Slug: coordinador-nacional-agrario-denuncia-persecucion-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesino-e1460152084815.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [05 Jun 2017] 

Esta semana la Fuerza Pública ha detenido a dos líderes del Coordinador Agrario Nacional (CNA), Dagoberto Galvis el pasado 4 de junio y William Pérez el 31 de mayo. **Capturas que, de acuerdo con un comunicado de prensa del CNA serían arbitrarías y harían parte de la estigmatización al movimiento social**.

**Dagoberto Galvis es  de la JAC, miembro de la Asociación de Campesinos y Comunidades Sin Tierra del Cesar - ASOCAMTICE, del coordinador Nacional Agrario - CNA** y del Congreso de los Pueblos en ese departamento. Su captura se realizó el pasado 4 de junio cuando unidades de la SIJIN y la Fiscalía entraron en su vivienda, ubicada en el barrio "450 años" de Valledupar.

Según la información, William Pérez, el segundo detenido, es un campesino de 44 años de edad, padre cabeza de familia de tres hijos, afiliado a la Junta de Acción Comunal de la Vereda el Tigre del Municipio de Curumaní, integrante del Coordinador Nacional Agrario – CNA y al Congreso de los Pueblos. **Su captura se registró el pasado 31 de mayo sobre el medio día, y fue señalado del delito de Rebelión**.

En el comunicado de prensa el CNA indicó que con estas detenciones **ya son 28 los campesinos capturados en lo corrido de los últimos dos años** en la Serranía del Perijá especialmente en territorios donde existen intereses de monocultivos y extractivismo minero. Le puede interesar: ["En 24 horas han detenido a 3 líderes sociales del Congreso de los Pueblos"](https://archivo.contagioradio.com/capturas-congreso-de-los-pueblos/)

Además, en el texto agregaron “**nos encontramos ante una sistemática persecución al movimiento social campesino y criminilazicacion de la defensa del territorio**, de la madre tierra y de la vida.  Estos falsos positivos judiciales son un formato diseñado por la Fiscalía antiterrorismo usando presuntos desmovilizados de las insurgencias que a cambio de beneficios acusan a campesinos y campesinas de ser colaboradores de la insurgencia”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
