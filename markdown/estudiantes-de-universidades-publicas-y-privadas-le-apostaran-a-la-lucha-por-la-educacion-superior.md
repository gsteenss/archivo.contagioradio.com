Title: Estudiantes de U. públicas y privadas le apuestan a lucha por la educación Superior
Date: 2018-03-21 14:54
Category: Educación, Nacional
Tags: Encuentro de Estudiantes de Educación Superior, ENEES, Universidad privada, Universidad pública
Slug: estudiantes-de-universidades-publicas-y-privadas-le-apostaran-a-la-lucha-por-la-educacion-superior
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/ENEES.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ENEES] 

###### [21 Mar 2018] 

El movimiento Estudiantil colombiano definió su agenda de movilización para este 2018, tras la finalización del Encuentro Nacional de Estudiantes de Educación Superior, en donde participaron universidades públicas y privadas y que dejo como resultado priorizar **la construcción de un movimiento democrático, autónomo y unido que se volverá a encontrar a mediados de septiembre**.

Así mismo, la defensa de una educación gratuita y de calidad, continuar en la lucha por una financiación estatal que permita un mayor acceso a la educación superior, la superación de la deuda histórica **que rondaría los 16 billones de pesos**, el aumento de recursos destinados a la investigación en Ciencia y tecnología y el rechazo a los reiterados recortes presupuestales que se la hace a la educación, seguirán siendo las principales reivindicaciones de los Estudiantes.

De igual forma, el desmonte de la política **“Ser Pilo Paga” como la exigencia de una regulación de las matrículas de las universidades privadas**, también harán parte de la agenda de puntos a discutir con el Ministerio de Educación. (Le puede interesar: ["Diálogo con la comunidad estudiantil: la respuesta al anuncio de Peñalosa")](https://archivo.contagioradio.com/fuerza_publica_universidad_pedagogica_rector_penalosa/)

### **El Balance del ENEES** 

De acuerdo con Cristian Guzmán, representante de la Facultad de Ciencias Políticas y Derecho de la Universidad Nacional, sede Medellín, los estudiantes se reunieron luego de 4 años en donde no se había gestado un escenario nacional de encuentro.

“Participaron muchos colectivos y chicos de otras universidades que plantearon un proceso autocrítico y como bandera de lucha la unidad de movimiento estudiantil de la mano de la democratización del mismo” afirmó Guzmán, sin embargo, manifestó que existieron algunos inconvenientes debido a la falta de colaboración de las instituciones públicas.

Otro de los hechos que generó miedo y zozobra entre los asistentes tuvo que ver con un audio de wathsapp que llegó a los celulares de diferentes participantes, en donde se afirmaba que había una bomba en el recinto en donde se encontraban, acto seguido alguien activo un extintor. Para Guzmán **este hecho pudo haberse controlado si se hubiese hecho un control más seguro frente a quienes asistieron al ENEES**.

<iframe id="audio_24725733" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24725733_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
