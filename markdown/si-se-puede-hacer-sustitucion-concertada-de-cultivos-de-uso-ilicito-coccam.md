Title: Si se puede hacer sustitución concertada de cultivos de uso ilícito: COCCAM
Date: 2017-04-26 16:07
Category: Ambiente, Nacional
Tags: COCCAM, Sustitución de cultivos de uso ilícito
Slug: si-se-puede-hacer-sustitucion-concertada-de-cultivos-de-uso-ilicito-coccam
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/semana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [26 Abr 2017] 

Un nuevo fallo de la Corte Constitucional ratificó la prohibición del uso de glifosato para hacer aspersión aérea cultivos de coca. Sin embargo, el debate sobre la erradicación manual continúa abierto, **poniendo en riesgo los acuerdos pactados con comunidades campesinas para los planes de sustitución de cultivos**.

Contexto que, de acuerdo con Cesar Jerez, vocero de la Coordinadora Nacional de cultivadores de Coca, Amapola y Marihuana, se complejiza con la decisión del presidente Santos de erradicar 50 mil hectáreas y al mismo tiempo ordenar que otras 50 mil, hagan parte del plan de sustitución, “no se pueden hacer las dos cosas al tiempo, el gobierno debió optar por la sustitución, **cumplir a cabalidad con los acuerdos y dejar de lado las medidas de fuerza**” afirmó.

Decisión presidencial que ha generado **tensiones entre los campesinos que quieren unirse a los planes de sustitución y las erradicaciones hechas por la Fuerza Pública**, que se vienen dando en regiones del país como Nariño o Cauca. Le puede interesar:["Erradicación forzada continuará en Tumaco por lentitud de Fast Track"](https://archivo.contagioradio.com/erradicacion-forzada-continuara-tumaco/)

De acuerdo con los censos de COCCAM, las zonas del país con mayores cantidades de cultivos de uso ilícitos  son: **Nariño, Guaviare, Meta, Caqueta, Nudo de Paramillo, Córdoba y Antioquia**, lugares que además ya han avanzado en diálogos con el gobierno Nacional para iniciar con los planes de sustitución.

Jerez señala que la pretensión del gobierno de acabar de “un día para otro” con los cultivos ilícitos se enfrenta a **dos realidades que no han sido combatidas de forma directa por la institución, el narcotráfico y la corrupción**. Le puede interesar: ["Dijimos no a la erradicación, pero si a la sustitución: Campesinos de Argelia, Cauca"](https://archivo.contagioradio.com/dijimos-no-a-la-erradicacion-pero-si-a-la-sustitucion-campesinos-de-argelia-cauca/)

Sumado a ello, se encuentra la intensión de **tomar el control por parte de grupos paramilitares de zonas que antes se encontraban en manos de la guerrilla de las FARC-EP** y que, de acuerdo con campesinos, los han amenazado con obligarlos a que continúen cultivando coca, amapola y marihuana.

Frente a estás problemáticas los campesinos que hacen parte de la COCCAM, le han expuesto al gobierno que los planes de sustitución de cultivos de uso ilícito podrían desarrollarse en un **plazo de 10 años, gradualmente, a partir de las características de cada zona en donde serán aplicados y teniendo en cuenta la importancia de las implicaciones** financieras, para dar cumplimiento a los acuerdos de paz de La Habana.

<iframe id="audio_18361452" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18361452_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
