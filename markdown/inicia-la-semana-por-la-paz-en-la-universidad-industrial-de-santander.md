Title: Inicia la Semana por la Paz en la Universidad Industrial de Santander
Date: 2016-08-16 12:49
Category: Nacional, Paz
Tags: Diálogos de paz en Colombia, Propuestas de paz en las universidades, Semana por la paz en la UIS, Universidad Industrial de Santander
Slug: inicia-la-semana-por-la-paz-en-la-universidad-industrial-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Universidad-Industrial-de-Santander.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EAD UIS ] 

###### [16 Ago 2016] 

Desde este martes y hasta el próximo viernes se lleva a cabo en la Universidad Industrial de Santander la Semana por la Paz, en la que participan estudiantes, profesores y trabajadores, así como exmilitantes del M-19, el EPL y el PRT, reconocidos analistas y representantes de organizaciones defensoras de los derechos humanos, para **debatir en torno a la construcción de la paz en Colombia**.

Las actividades programadas durante esta semana se ven como una oportunidad para conocer de los acuerdos, las oportunidades y los retos, que propiciarán **la construcción de paz con una mirada regional y a partir de un diálogo razonado**, incluyente y democrático, en foros con temáticas que girarán en torno a las universidades como territorios de paz.

Según la docente Ivonne Suarez, la Semana es liderada por la Comisión de Paz creada en la Universidad para **aportar a la construcción de paz desde la pedagogía, la investigación y la extensión de los acuerdos** que se han pactado en La Habana, como tarea fundamental para que los colombianos los conozcan y puedan argumentar su voto en el plebiscito, comprendiendo la importante decisión que están tomando.

Para la docente, esta iniciativa surge como una apuesta de la Universidad para **aportar a la paz desde la articulación de la realidad con la academia**, y se suma a espacios como la Cátedra de Paz, la Maestría en Derechos Humanos y el [[Archivo Oral de Memoria de las Víctimas](http://www.uis.edu.co/webUIS/es/amoviUIS/index.html)], en el que se han recopilado las voces y documentos tanto de los eventos de dolor como de los procesos de resistencia de diversas comunidades, para comprender que a partir de ellos se empieza a construir la paz.

En la programación se han previsto conversatorios sobre experiencias de paz, discusiones alrededor del conflicto y el papel de las universidades en las memorias y archivos de los acuerdos de paz, la Constitución y la Justicia Transicional; así como la presentación de la **obra de teatro 'Historia de ángeles y demonios'** y la exposición 'Esta guerra la paro yo', como ejercicio pedagógico postulado al Premio Nacional de Paz 2015.

[![Semana por la Paz UIS](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Semana-por-la-Paz-UIS.jpg){.aligncenter .size-full .wp-image-27872 width="800" height="949"}](https://archivo.contagioradio.com/inicia-la-semana-por-la-paz-en-la-universidad-industrial-de-santander/semana-por-la-paz-uis/)

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
