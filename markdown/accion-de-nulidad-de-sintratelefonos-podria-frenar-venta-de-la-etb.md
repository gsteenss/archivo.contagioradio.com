Title: Acción de nulidad de Sintrateléfonos podría frenar venta de la ETB
Date: 2017-01-20 12:04
Category: Movilización, Nacional
Tags: revocatoria Peñalosa, Venta de la ETB
Slug: accion-de-nulidad-de-sintratelefonos-podria-frenar-venta-de-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/confidencial-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Confidencial Colombia] 

###### [20 Ene 2017] 

El pasado martes Sintrateléfonos interpuso una acción de nulidad contra el artículo 40 del plan de desarrollo de Bogotá, por las multiples violaciones en los requisitos de procebilidad durante la aprobación del plan, al saltarse el concepto del Consejo Territorial de Planeación. **En caso de que la acción prospere se frenaría la venta de la ETB o se dictarían medidas cautelares del caso.**

A su vez, Sintrateléfonos expuso a través de un comunicado de presa que el mensaje que buscan transmitir con esta acción  de nulidad es que el alcalde Peñalosa se siente y escuche las propuestas que se han hecho para que “a través de una buena administración de esta empresa la haga rentable, competitiva y de ahí se haga inversión en la ciudad”, ya que de acuerdo con el sindicato la **ETB tiene suficiente fuerza y músculo para hacer las inversiones.**

El presidente de esta organización, William Sierra, afirmó que esta hace parte de una de las acciones “que tienen preparadas en desarrollo **para tumbar el nefasto plan del alcalde mayor de Bogotá en regalar la ETB**”. Le puede interesar: ["60 mil firmas recolectadas para cabildo abierto contra venta de ETB"](https://archivo.contagioradio.com/60-mil-firmas-recolectadas-para-cabildo-abierto-contra-venta-de-etb/)

Sintrateléfonos además, hace parte de las organizaciones que se ha inscrito en los comités que buscan la revocatoria del burgomaestre y que **desde el pasado 13 de enero tienen en su poder los formularios para iniciar la recolección de firmas**. Le puede interesar:["Listos formatos de recolección de firmas para revocatoria de Peñalosa"](https://archivo.contagioradio.com/listos-formatos-de-recoleccion-de-firmas-para-revocatoria-de-penalosa/)

###### Reciba toda la información de Contagio Radio en [[su correo]
