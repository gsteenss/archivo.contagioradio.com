Title: 18 grupos narcoparamilitares operan en Colombia
Date: 2018-12-14 17:33
Author: AdminContagio
Category: Nacional
Tags: Acción violenta, INDEPAZ, municipio, Narcoparamilitares, paramilitares
Slug: 18-grupos-narcoparamilitares
Status: published

###### [Foto: Colombia Plural] 

###### [14 Dic 2018] 

El Instituto de Estudios Para el Desarrollo y la Paz INDEPAZ, presentó un informe sobre grupos armados ilegales en Colombia, y su operación durante el primer semestre de 2018. En el documento, **la organización señala el avance de grupos “narcoparamilitares”** así como de grupos creados tras el desarme de las Fuerzas Armadas Revolucionarias de Colombia (FARC); y destaca la reconfiguración que está sufriendo el conflicto armado en el país.

El informe fue elaborado a partir del registro de hechos relacionados con el conflicto por parte de los medios de comunicación, informaciones de las Fuerzas Armadas del Estado, la Procuraduría y la Defensoría del Pueblo, así como organizaciones sociales y el trabajo de campo propio de INDEPAZ. **En la investigación, se tomaron en cuenta hechos como asesinatos, confrontaciones, reclutamiento e incautación de armas**.

En el documento se identifican tres tipos de grupos que hacen presencia en el territorio nacional: narcoparamilitares, grupos creados tras la desmovilización de las FARC y otras guerrillas. Pese a esta diferenciación, relacionada con el surgimiento de los diferentes tipos de grupos, INDEPAZ resalta que **la prensa y la Fuerza Pública suele tratarlos como delincuencia organizada, categorización que evita afrontar el problema de forma más precisa**.

### **AGC: El grupo que tiene mayor presencia en los municipios** 

En el informe se evidencia que el grupo que tuvo mayor presencia en los municipios de Colombia fue **las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), cuyas acciones se registraron en 225 municipios,** siguientes en la lista de acciones por parte de grupos narcoparamilitares aparecen Los puntilleros, en 12 Municipios y Los Rastrojos en 7.

\[caption id="attachment\_59308" align="alignnone" width="644"\][![Acción de grupos narcoparamilitares](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Captura-de-pantalla-2018-12-14-a-las-5.04.03-p.m.-691x485.png){.wp-image-59308 width="644" height="452"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Captura-de-pantalla-2018-12-14-a-las-5.04.03-p.m..png) Informe INDEPAZ\[/caption\]

Por parte de los grupos pos-desarme de las FARC, sus actividades afectaron 73 municipios, mientras las del ELN tuvieron lugar en 136, el EPL en 9 y **las Águilas Negras** en 19. Sobre este último, **el informe resalta que a partir de 2017 fue excluido de la categoría “narcoparamilitar” y se analizan de forma diferenciada**, en tanto no cuentan una estructura permanente o líderes conocidos, y actúan como una razón social utilizada por varios núcleos para “amenazar de muerte a organizaciones y a líderes o líderes sociales”, provocando terror con fines políticos.

INDEPAZ haciendo claridad sobre las dificultades para establecer la cifra, presentó un estimado de los integrantes en armas de las estructuras ilegales, **calculando que 3 mil personas compondrían los grupos narcoparamilitares,** 2,5 mil los grupos posdesarme de las FARC, 2 mil el ELN y 250 el EPL. El Instituto detalla que de los integrantes calculados en los grupos de pos-desarme, 900 serían reincidentes, 300 disidentes y 1,3 mil “corresponderían a nuevos reclutamientos".

### **¿Cómo categorizó los grupos INDEPAZ?** 

Para identificar los grupos posdesarme de las FARC, el Instituto propone 3 categorías: **disidencias, grupos Rearmados para Negocios Ilegales (RNI) y Grupos de Seguridad del Narcotráfico y Mafias (GSNM)**; para los investigadores, incluir todos los grupos en la etiqueta de disidentes o reincidentes es desconocer la realidad de la reconfiguración, y por lo tanto, las formas para afrontarlas.

Adicionalmente sobre estos grupos, el informe señala **“una disminución del 65% en el número de municipios que eran afectados por la acción de las FARC-EP** en relación con los que hoy registran hechos de autoría de los conformados posdesarme de las FARC-EP”. (Le puede interesar:["Sin vida fueron encontrados los cuerpos de dos integrantes de FARC"](https://archivo.contagioradio.com/sin-vida-integrantes-farc/))

Sobre los Narcoparamilitares, el Instituto recordó que tras la desmovilización de las Autodefensas Unidas de Colombia (AUC) **durante el Gobierno Uribe, “se inició una acción institucional por negar la existencia del paramilitarimo”**, razón que llevo a definir a las estructuras armadas posdesmovilización como bandas criminales o BACRIM.

Posteriormente, durante el segundo mandato de Santos, se expidieron las directivas 015 y 016 del Min. Defensa que redefinieron las BACRIM c**omo Grupos Armados Organizados (GAO) y Grupos Armados Delincuenciales (GAD)**. No obstante, dado el relacionamiento con los diferentes tráficos (armas, insumos, dinero) asociados a la comercialización de narcóticos, INDEPAZ optó por categorizarlos como Narcoparamilitares.

La organización divide esta categoría en 3 grupos que se diferencia por su origen y su capacidad de acción: “Los primeros surgen tras la desmovilización de las AUC y cuentan con una injerencia a nivel nacional o en múltiples regiones; los segundos tienen el mismo origen que el anterior, pero su capacidad se circunscribe a niveles regionales y municipales; y las últimas, surgen como grupos de apoyo con funciones logísticas o de crimen a escalas locales, actividades que permiten el aumento de su poder”.

### **“La etapa de posacuerdo cambia radicalmente la situación de conflictos armados”** 

En análisis de los investigadores que realizaron el informe, la etapa del posacuerdo alteró la situación de conflictos armados en el país, esto sucede porque ahora se presentan “confrontaciones regionales o focalizadas”, lo que además implica, **que estos grupos ya no están bajo “las lógicas de lucha por el poder político”**. (Le puede interesar: ["Nuevos respaldos a Ley de Sometimiento de Bandas Criminales"](https://archivo.contagioradio.com/apoyos-ley-de-sometimiento-bandas-criminales/))

Al contrario, el análisis de los datos evaluados por el Informe evidencia como estrategias de acción el uso de operaciones de pequeños grupos armados, “de entre cinco y quince individuos, y **la tercerización de las acciones criminales bajo la subcontratación” de combos, oficinas y otros pequeños grupos**. A ello se suma la acción de carteles internacionales, “que financian el negocio ilícito y asumen el tráfico desde Colombia hacía el exterior”.

Adicionalmente, [el Informe](https://es.scribd.com/document/395720387/Informe-de-INDEPAZ-sobre-conflictos-armados-focalizados) destaca que **agentes del Estado, “incluyendo integrantes de las instituciones y Fuerza Pública” siguen participación en actividades ilegales**, bien sea por omisión y temor, o porque son parte de las cadenas económicas, su relación con el conflicto es “factor fundamental para la persistencia de la criminalidad, y con ello, de violencia armada en varias regiones”.

Por último, INDEPAZ resalta que en el caso del proceso de Desarme, Desmovilización y Reinserción (DDR) con las AUC, como en el proceso de paz con las FARC, “la incorrecta materialización de lo acordado permitió procesos de rearme, muchos de ellos con fuente en antiguos mandos medios o excombatientes de base”. (Le puede interesar: ["Nueva cúpula militar: ¿Vuelve la seguridad democrática?"](https://archivo.contagioradio.com/nueva-cupula-militar-vuelve-la-seguridad-democratica/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
