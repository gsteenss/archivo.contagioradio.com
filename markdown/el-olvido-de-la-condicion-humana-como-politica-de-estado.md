Title: El olvido de la condición humana como política de Estado
Date: 2015-05-28 12:22
Category: Nicole, Opinion
Tags: FARC-EP, Guapi, Medicina Legal, Nicole Jullian, proceso de paz
Slug: el-olvido-de-la-condicion-humana-como-politica-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/politica-del-olvido.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**Por [Nicole Jullian](https://archivo.contagioradio.com/nicole-jullian/)**] 

Para algunas personas estos días recién pasados han sido días muy duros. Para otros han sido días de gran felicidad. Y es que en Colombia la muerte de guerreros, sean estos militares o guerrilleros, tiene connotaciones distintas dependiendo de quién es el que cae.

Luego de enterarme de la muerte de 26 miembros de las FARC-EP producto de un bombardeo aéreo en la zona rural de Guapi el pasado viernes 22 de mayo y luego de escuchar al Presidente Santos ufanarse de la muerte de estas personas, pasé muchos días sin leer nada de prensa. Tomé distancia de la contingencia y más bien invertí las pocas energías que me quedaban en pensar a dónde es que nos lleva el gobierno del Presidente Santos con sus falsas promesas de paz. Pues el hecho de que un Jefe de Estado goce con la muerte de compatriotas no hace sino confirmar que este país está regido (nuevamente) por un ser demente cuya ansias de proseguir con la guerra son muy claras.

[Cuando me atreví a indagar qué de nuevo había ocurrido, me topé con que el jefe de Estado colombiano había afirmado *“*]{.s1}*no más guerrilleros enterrados como NN. El Estado garantizará a sus familias que puedan reclamar a sus seres queridos y darles un sepelio como corresponde. Y así será de aquí en adelante”.* Me llevé una gran sorpresa al confirmar que en Colombia se practica una política de Estado según la cual el Instituto Nacional de Medicina Legal y Ciencias Forenses no es libre en la realización de sus labores, sino que pasa a engrosar la lista de las instituciones estatales que están para complacer los deseos del presidente de turno.

Mientras el actual gobierno da la orden de no invertir ningún segundo de tiempo en la identificación de un guerrillero, el ex Presidente Uribe asistió en abril pasado al funeral de Óscar David Blanco, uno de los once militares muertos en combate en la zona rural del Municipio de Buenos Aires, Cauca. Mientras a las familias de los guerrilleros se les priva del derecho de dar santa sepultura a sus hijos caídos, miembros del Ejército Nacional disfrutan con la manera como fue abatido Raúl Reyes en territorio ecuatoriano y muestran su cuerpo reventado como si se tratara de la disección de una rata.

[Desde hace ya décadas que muchas entidades del Estado se caracterizan por fomentar]{.s2}el propagandismo político y por estar al servicio de la guerra en vez de trabajar arduamente por la construcción positiva de la conciencia humana en los ciudadanos.

Así las cosas, las FARC-EP han decidido suspender la tregua unilateral tras el bombardeo en el Cauca que dejó 26 guerrilleros muertos, insistiendo en que el futuro de Colombia se discute en La Habana y en que la búsqueda de la verdad conduce irremediablemente hacia la creación de una mesa de esclarecimiento del paramilitarismo.

*“Convertida en objeto de consumo, la miseria da morboso placer y mucho dinero. En el mercado de la opulencia, la miseria es una mercancía que se cotiza bien.”* Eduardo Galeano
