Title: “Fracasaron quienes pedían el fin del proceso de Paz” Alvaro Villarraga
Date: 2014-12-26 16:19
Author: CtgAdm
Category: Otra Mirada
Slug: fracasaron-quienes-pedia-el-fin-del-proceso-de-paz-alvaro-villarraga
Status: published

###### Foto: Primiciadiario.com 

[fracasaron](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2014/12/fracasaron.mp3)

Luego del anuncio por parte de Patricia Rey, vocera de comunicaciones del Comité Internacional de la Cruz Roja, de que el orgsanismo ya recibió la solicitud formal de las FARC, Noruega y Cuba como garantes y del gobierno nacional de poner en marcha el operativo para la liberación de las personas en poder de la guerrilla de las FARC, y luego del anuncio del presidente Santos de que tan pronto se dé la liberación de estas personas se reactivarán las conversaciones, queda claro que se derrotaron aquellas propuestas que querían acabar con el proceso de Paz, según Alvaro Villarraga de la Fundación Cultura Democrática.
