Title: Trabajadores denuncian abuso laboral por parte de la empresa Indupalma
Date: 2018-01-26 19:59
Category: DDHH, Nacional
Tags: Indupalma, Min Trabajo
Slug: trabajadores-de-indupalma-tienen-que-pagar-hasta-su-dotacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/indupalma4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agronegocios] 

###### [26 Ene 2018] 

Más de 1.200 trabajadores de Indupalma iniciaron una huelga laboral, exigiendo a esta empresa que finalice la tercerización laboral y mejores las condiciones de trabajo. Los trabajadores denunciaron que **tienen descuentos injustificados en sus salarios y que la empresa ya habría sido sancionada por el Ministerio de Trabajo**.

Andrey Piñeres, trabajador en Indupalma, aseguró que los empleados llevan 22 años contratados por medio de cooperativas asociadas a la empresa y dirigidos por personas que designa la misma entidad, **además manifestó que mensualmente de sus sueldos es descontado el monto que se debe pagar por la seguridad social y gastos que debería asumir la organización** y no el empleado, sin tener en cuenta que lo que devengan es fruto de la producción que cada persona realiza y no siempre pueden trabajar debido a las condiciones climáticas.

“Los trabajadores deben iniciar su labor debiéndole a la cooperativa 26 mil pesos diarios, que corresponden a **descuentos de seguridad social, transporte, dotación, gastos de representación que cobran los gerentes de las directivas**” explicó Piñeres.

Así mismo aseguró que este descuento se hace incluso los días en que el trabajador no devenga porque no va a trabajar y que en total deben hacer una producción de más de **800 mil pesos para que les quede por lo menos un mínimo con el cual sobrevivir**.

### **La multa del Ministerio de Trabajo a Indupalma** 

La sanción del Ministerio de Trabajo, según Piñeres, se impuso a Indupalma por la presunta intermediación ilegal que se hace con la contratación de estas cooperativas, para que, a su vez, contraten a los empleados. La multa que tendrá que pagar la empresa por este acto es de **\$2.950 millones de pesos**, de igual forma cada una de las cooperativas también fueron sancionadas y deberán pagar una multa por **700 millones de pesos**.

### **La respuesta de Indupalma** 

Piñeres afirmó que la empresa rechazó el pliego de exigencias de los trabajadores alegando que no es un conflicto laboral con ellos porque los trabajadores no son contratados directamente con la entidad. Después de esta negativa, los empelados **convocaron a una asamblea general que decidió ir a huelga**.

El pasado 25 de enero los trabajadores de Indupalma se reunión con la viciministra de Trabajo, Griselda Restrepo, en donde se llegó al acuerdo de generar un encuentro el martes de la próxima semana, en donde participen los sindicatos y representantes de la empresa para dialogar y de esa forma lograr entablar un espacio de negociación. (Le puede interesar: ["Colombia en la OCDE sería la parte pobre de la familia de los ricos: Libardo Sarmiento"](https://archivo.contagioradio.com/colombia-ocde-parte-pobre-familia-de-los-ricos/))

<iframe id="audio_23387416" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23387416_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
