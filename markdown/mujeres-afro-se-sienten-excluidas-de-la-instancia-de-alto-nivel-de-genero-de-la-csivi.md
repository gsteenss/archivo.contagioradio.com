Title: Mujeres afro se sienten excluidas de la instancia de género de la CSIVI
Date: 2017-07-27 15:38
Category: Mujer, Nacional
Tags: afrodescendientes, CSIVI, Feministas, Mujeres afro
Slug: mujeres-afro-se-sienten-excluidas-de-la-instancia-de-alto-nivel-de-genero-de-la-csivi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/negrasm4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [27 Jul. 2017] 

Luego de conocerse los nombres de las lideresas que harán parte de la instancia de alto nivel de género de la Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Final –CSIVI-, varias organizaciones afro dieron a conocer un comunicado en el que **muestran su extrañeza al no ser incluidas en este comité.**

Aseguran que con asombro notaron que **la elección no atiende el criterio de inclusión en la diversidad** “pues las únicas mujeres sin participación son las mujeres afrocolombianas, configurándose así un escenario de discriminación”. Le puede interesar: [Foro Internacional busca la participación política de mujeres afro](https://archivo.contagioradio.com/foro-internacional-busca-la-participacion-politica-de-mujeres-afro/)

Así mismo, manifestaron que el enfoque de género del Acuerdo Final no incluyó un enfoque étnico y que esperaban esto fuera corregido con la participación de ellas en la instancia de alto nivel, encontrándose con que la CSIVI **“ha dejado a las mujeres afrodescendientes como las únicas sin asiento**, a pesar de existir candidatas que se presentaron cumpliendo los requisitos necesarios. Que la delegación del gobierno y de las FARC respondan”. Le puede interesar: [Las mujeres tejen paz desde su territorio.](https://archivo.contagioradio.com/las-mujeres-tejen-paz-desde-los-territorios/)

### **“Exigimos respeto por las mujeres negras”** 

En el comunicado aseguran que se declaran en “alerta” por este hecho y rechazan el desconocimiento de todo aquello que las mujeres afro pueden aportar para construir la paz en Colombia.

“Lamentamos el desprecio a la oportunidad de acabar las brechas de la desigualdad y la discriminación (…). **Exigimos respeto por la participación política de las mujeres negras** en igualdad con las mujeres colombianas”. Le puede interesar: [Acuerdos no se pueden quedar entre gobierno y FARC: Cumbre Afro](https://archivo.contagioradio.com/las-comunidades-afro-se-preparan-para-implementar-los-acuerdos/)

### **Mujeres afro no aceptarán esta exclusión** 

De otro lado, la comunicación manifiesta que nuevamente se sintieron excluidas toda vez que para el lanzamiento del enfoque de género en La Habana “las invitaron a última hora” y hoy estuvieron presentes como “observadoras (…) sin una mujer negra como integrante”.

Por tal motivo, manifiestan **no estar dispuestas a "aceptar este trato como ciudadanas de tercera categoría,** no toleraremos más esa falsa excusa de limitaciones técnicas o falta de capacidad”. Le puede interesar: [Gobierno tiene 30 días para cumplirle a los pueblos afrodescendientes](https://archivo.contagioradio.com/gobierno-tiene-30-dias-para-cumplirle-a-los-pueblos-afrodescendientes/)
