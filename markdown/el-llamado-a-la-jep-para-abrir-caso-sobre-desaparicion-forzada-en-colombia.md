Title: El llamado a la JEP para abrir caso sobre desaparición forzada en Colombia
Date: 2019-11-13 17:47
Author: CtgAdm
Category: Judicial, Paz
Tags: centro de memoria, comision de la verdad, conflicto, desaparecidos, JEP, víctimas
Slug: el-llamado-a-la-jep-para-abrir-caso-sobre-desaparicion-forzada-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Minga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]lajornadajalisco.com.mx 

[Este jueves 14 de noviembre víctimas, miembros de Human Rights Everywhere (HREV) y el Movimiento Nacional de Víctimas de Crímenes de Estado (Movice) presentarán ante la  Jurisdicción Especial para la Paz (JEP) "Cartografía de la Desaparición",  una investigación sobre los más de 80.000 desaparecidos víctimas del conflicto en Colombia, con el fin de solicitar la apertura de un caso dedicado al estudio y esclarecimiento de estas desapariciones. (Le puede interesar: [Existe un 99,5% de impunidad en casos de desaparición forzada en Colombia](https://archivo.contagioradio.com/existe-un-995-de-impunidad-en-casos-de-desaparicion-forzada-en-colombia/))]

[ *"hagamos justicia ante la desaparición forzada"*,  será el objetivo de la cartografía que se entregará a la  magistrada Reinere Jaramillo Chaverra], así mismo, en este documento donde participó Erik Arellana, periodista, coautor  e hijo de Nydia Érika Bautista, integrante del del M-19  y desaparecida en 1987,  se habla del delito y la deuda que tiene el Estado con las víctimas de este hecho, que actualmente, según el informe está  en un 99,5 % de impunidad.

Según Arellana, en la JEP ya hay casos que aluden a las víctimas de desaparición forzada,  sin embargo, el número es tan alto que vale la pena abrir un caso único para ellos,"h[ay otros casos colectivos como secuestro, reclutamiento forzado, que trabajan transversalmente el caso de desaparición forzada pero, por la connotación histórica que ha tenido en el país, creemos necesario hacerlo separado".]

### "En Colombia es tal la magnitud de este crimen que el Estado no ha podido consolidar una cifra oficial para desaparición" CNMH 

El último informe "Hasta encontrarlos" presentado por el Centro Nacional de Memoria Histórica (CNMH), indicó que son 82.998 personas registradas como desaparecidas en el marco del conflicto armado en Colombia, en el periodo comprendido entre 1970 hasta el 2015, "familias colombianas sufren la ausencia de sus seres queridos y la incertidumbre que produce la falta de noticias o de evidencias que den cuenta de qué sucedió con sus familiares. ¿Quién se los llevó?, ¿por qué se los llevaron?, ¿por qué tanta indolencia?", añadió el CNMH.

En este mismo informe destacan que los perpetradores de estas desapariciones corresponden en un  52% de los casos a  grupos paramilitares, 10.360 a guerrillas, 2.764 a grupos posdesmovilizacion y 2.484 a gentes de Estado, esto a la luz de 42.471 registros de los que se tiene información. (Le puede interesar:[JEP realizará primera audiencia pública por víctimas de desaparición forzada en la Comuna 13 de Medellín](https://archivo.contagioradio.com/jep-realizara-primera-audiencia-publica-por-victimas-de-desaparicion-forzada-en-la-comuna-13-de-medellin/))

De igual forma Arellana  resaltó que hasta ahora "en Colombia es tal la magnitud de este crimen que el Estado no ha podido consolidar una cifra oficial", donde por un lado el Registro Nacional de Desaparecidos indica que hay 28.755 víctimas, el Registro Único de Víctimas reconoce a 47.762 personas, y el Observatorio de Memoria y conflicto  identifica 82.998 a la luz de 10.000 bases  de dato, esto refleja de alguna manera inexactitud en los registro e incomunicación entre las organizaciones**.**

### **"yo no pido justicia por un caso concreto sino queremos claridad de las causas que motivaron los hechos y que identifiquen a los responsables macro"**

[Para el periodista Erik, el pacto de la desaparición forzada no va exclusivamente a casos puntuales, "yo no pido justicia por un caso concreto sino queremos una claridad de las causas que motivaron los hechos y que identifiquen a los responsables macro que son quienes han motivado e impulsado esta práctica de exterminio en la sociedad colombiana". (Le puede interesar: [Desaparición Forzada, una lucha por la verdad que persiste](https://archivo.contagioradio.com/desaparicion-forzada/))]

El informe enfocado a que la desaparición forzada se investigue en la Comisión de la Verdad, la Unidad de Búsqueda de Personas Desaparecidas  y la JEP se hará en Bogotá [ a las 2:30 pm en el hotel Suite Jones, según Arellana aún es incierto saber cuánto tiempo necesita la sala de magistrados para tomar esta decisión, "estamos haciendo la solicitud porque nos parece prioritario y existe una necesidad en el proceso de reconocimiento , esperamos que la Jurisdicción tome una decisión que respete los derechos de las víctimas y la necesidad de justicia". ]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
