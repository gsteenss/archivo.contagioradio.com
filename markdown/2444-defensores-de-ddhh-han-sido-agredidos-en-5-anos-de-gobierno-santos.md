Title: 2444 defensores de DDHH han sido agredidos en 5 años de gobierno Santos
Date: 2016-03-02 16:32
Category: DDHH, Nacional
Tags: Juan Manuel Santos, Paramilitarismo, Pel, Pelicula El Cambio, Somos defensores
Slug: 2444-defensores-de-ddhh-han-sido-agredidos-en-5-anos-de-gobierno-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/elcambio_contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Somos Defensores] 

###### [02 Mar 2016] 

De acuerdo con el más reciente informe del [‘Programa Somos Defensores’](https://archivo.contagioradio.com/?s=somos+defensores) **durante el periodo de Gobierno de Juan Manuel Santos 2444 defensores y defensoras de derechos humanos fueron agredidos**, de ellos 1687 recibieron amenazas, 346 fueron asesinados, 206 fueron víctimas de atentados, 131 detenidos arbitrariamente, 29 judicializados, a 28 les fue hurtada información sensible y 16 están desaparecidos.

En los dos últimos años se registraron 1308 agresiones individuales, 626 en 2014 y 682 durante 2015, en este periodo 539 defensores fueron amenazados, 63 asesinados, 35 víctimas de atentados, 26 detenidos arbitrariamente, 3 desaparecidos y a 8 les robaron información.

Según refiere el ‘Sistema de Información sobre Agresiones contra Defensores de Derechos Humanos en Colombia’ SIADDHH, **la responsabilidad en la comisión de estos delitos es del 66% de grupos [paramilitares, 25% desconocidos, 7% agentes estatales y 0.5% guerrillas de las FARC-EP y el ELN](https://archivo.contagioradio.com/?s=somos+defensores).**

286 casos situaron a **Bogotá como el municipio con más agresiones registradas en el país. Antioquia con 10 asesinatos y Cauca con 9** se ubicaron como los departamentos con mayor número de defensores asesinados, 30% de ellos habían denunciado amenazas, 4 contaban con medidas de protección de la UNP, otro las tenía vencidas y a uno le habían sido negadas. Otros departamentos críticos fueron Cauca y Valle del Cauca con 160 casos. En Atlántico y Sucre incrementaron las agresiones en 44 y 38, respectivamente.

Como se asevera el informe el nivel de impunidad en la comisión de estos delitos es alto, sólo se ha logrado establecer la responsabilidad en uno de los casos, por lo que “el gobierno Santos se rajó en proteger integralmente a los defensores y líderes sociales en Colombia dejando una estela de impunidad total en torno a la violencia contra estos constructores de la paz”.

'El Cambio', será presentado en Bogotá a través de una producción audiovisual que da cuenta de como en el país la realidad puede llegar a superar la ficción, en  una propuesta que busca retratar con personajes comunes, familiares, colaboradores y enemigos, de la labor de las y los defensores, los retos y peligros que enfrentaron, "qué ganaron, qué perdieron, cuales son sus conflictos y si esta historia tiene un final feliz".
