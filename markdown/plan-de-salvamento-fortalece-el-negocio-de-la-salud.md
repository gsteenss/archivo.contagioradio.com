Title: Plan de salvamento fortalece el negocio de la salud
Date: 2016-04-19 14:55
Category: DDHH, Nacional
Tags: salud en colombia
Slug: plan-de-salvamento-fortalece-el-negocio-de-la-salud
Status: published

###### [Foto: ] 

###### [19 Abril 2016 ] 

[El Ministerio de Salud anunció que el plan de salvamento del gobierno colombiano para enfrentar la [[crisis en la salud](https://archivo.contagioradio.com/balance-de-la-salud-en-colombia-continuo-enferma-durante-el-2015/)] consiste en aprobar \$180 mil millones más en créditos para las EPS, una medida que de acuerdo con Clemencia Mayorga, vocera de la 'Mesa Nacional por el Derecho a la Salud', no asegura que las entidades paguen las cuantiosas deudas que tienen a hospitales al borde del cierre, pues es evidente que **se han lucrado a través de la malversación de recursos públicos**. ]

[Según afirma Mayorga, desde 1993, cuando fue expedida la Ley 100, se ha evidenciado que las **EPS "han recibido todo el dinero que debería garantizar el derecho a la salud** de todos los colombianos" y han protagonizado escándalos por cuenta de grandes desfalcos. Así que la aprobación de créditos "no va a asegurar que garanticen el derecho a la salud de sus pacientes, ni mucho menos que paguen las inmensas deudas a las clínicas y hospitales, que tienen por prestación de servicios, y que hoy las tienen sumidas en crisis y al borde del cierre". ]

Un verdadero plan de salvamento para el sector de la salud en Colombia, debe consistir de acuerdo con Mayorga, en la **inyección directa de recursos públicos a las clínicas y hospitales**, a quienes las EPS les adeudan más de \$12 billones, con graves consecuencias en la atención de pacientes y pago de salarios de sus trabajadores.

Desde la Mesa Nacional se invita a todos los colombianos a que entiendan que el derecho a la salud es un derecho fundamental, una obligación del Estado y que por tanto no se puede permitir que siga siendo **"un negocio de las EPS o de las clínicas y hospitales a costa de las enfermedades de los ciudadanos"**, asevera Mayorga.

Frente a medidas regresivas para la garantía del derecho a la salud, como esta aprobación anunciada por MinSalud y el **no cumplimiento de los 19 artículos de la Ley Estatutaria que fue aprobada hace un año**, diversas organizaciones, entre ellas la Mesa Nacional, han entablado medidas jurídicas para reversarlas.

<iframe src="http://co.ivoox.com/es/player_ej_11226292_2_1.html?data=kpaflJuWfZOhhpywj5WbaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncaTgxtLS0MjNpYzBwt7c1MzFaZO3jLLS1caPksLgjNXc1JDJsIy4xtfSxc3Tb8KfzcaYtcbQucWhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[![Crisis Salud](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Crisis-Salud.jpg){.aligncenter .size-full .wp-image-22918 width="1816" height="2532"}](https://archivo.contagioradio.com/plan-de-salvamento-fortalece-el-negocio-de-la-salud/crisis-salud/)

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
