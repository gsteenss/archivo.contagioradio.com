Title: Buenaventura: ¿La ciudad que encarna el Estado fallido?
Date: 2019-07-09 15:11
Author: CtgAdm
Category: Comunidad, Nacional
Tags: barrio Lleras, buenaventura, Valle del Cauca
Slug: buenaventura-la-ciudad-que-encarna-el-estado-fallido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Buenaventura-ciudad-del-Estado-fallido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [to: Comisión Intereclesial de Justicia y Paz  
] 

Desde el pasado sábado 29 de junio, diferentes organizaciones defensoras de derechos humanos han llamado la atención sobre el aumento de la violencia en Buenaventura, Valle del Cauca. Uno de los focos más preocupantes es el barrio Lleras donde se han denunciado presencia de hombres armados, enfrentamientos y desplazamientos forzados; no obstante, **según el padre Adriel Galván, Representante de FUNDESCODES, la lectura de esta situación debe analizar más que un hecho coyuntural.**

### **La violencia reciente del barrio lleras** 

En el barrio Lleras, donde FUNDESCODES realiza su trabajo se presentan enfrentamientos armados desde hace más de una semana; sin embargo, el Párroco afirma que esto no es novedoso porque siempre ha habido presencia de grupos rearmados paramilitares. Adicionalmente, según él, operan también grupos residuales de las FARC que no se acogieron al proceso de paz y el ELN. (Le puede interesar:["Si el Gobierno no cumple, retomaremos el paro cívico habitantes de Buenaventura"](https://archivo.contagioradio.com/si-el-gobierno-no-cumple-retomaremos-el-paro-civico-habitantes-de-buenaventura/))

Aunque el Padre declaró que el narcotráfico está teniendo incidencia en los hechos ocurridos en el Puerto durante los últimos años, "el tema de fondo son los niveles de violencia que han persistido históricamente" para impulsar lo que algunos líderes de Buenaventura reconocen como economía de enclave. En ese sentido, Galván puntualizó que en el Puerto hay un "desalojo" de la población, "para implementar políticas que tienen que ver con el sector económico".

### **Buenaventura: Un distrito con más de 150 mil personas declaradas como desplazadas** 

Según el Representante de FUNDESCODES, hay un patrón "porque el mayor número de victimizaciones es por desplazamiento". D**e acuerdo a Human Rights Watch, solo en 2013 se presentaron 13 mil desplazamientos en Buenaventura. En cifras oficiales, la Ciudad registra más de 150 mil personas reportadas como desplazadas**, hecho que para Galván, demuestra que la violencia en el territorio no se limita a un tema de narcotráfico.

Adicionalmente, en el Puerto el índice de necesidades básicas no satisfechas triplica los datos de ciudades como Cali, capital de Valle del Cauca; situación que parece pasar desapercibida, pese a que se calcula que por Buenaventura entra cerca del 50% del comercio internacional que llega a Colombia, a través de barcos. Sin embargo, la situación ha sido  vista, por parte del Estado, únicamente como una lucha contra el narcotráfico, lo que ha conllevado la entrada de más pie de fuerza a la Ciudad que no responde a garantías de vida para la población.

### **"En Buenaventura hay un Estado fallido"** 

La Fundación que representa el padre Adriel Galván trabaja con los niños del barrio Lleras, eso le ha permitido acercarse a ellos y ver que "el proyecto de vida de los jóvenes es vivir, la gente quiere vivir bien", pero en Buenaventura no hay condiciones para que eso ocurra: los menores se tienen que educar en contextos adversos, hay un "sistema de salud precario, racista, que es responsabilidad del Estado". De allí que "la violencia de los grupos solo termina echándole leña al fuego", sentenció Galván.

Adicionalmente, el Gobierno no ha cumplido con lo que pidió el paro cívico de 2017; y "todo lo quieren resolver militarmente", pero para el Padre, **"aquí hay un tema, y es que en Buenaventura este es un Estado fallido"**. (Le puede interesar:["Asciende a 10 la cifra de líderes del Paro Cívico de Buenaventura amenazados"](https://archivo.contagioradio.com/asciende-a-10-la-cifra-de-lideres-del-paro-civico-de-buenaventura-amenazados/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38153398" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38153398_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
