Title: A 91 años de la masacre de las bananeras la historia se repite y el negacionismo pretende imponerse
Date: 2019-12-06 16:51
Author: CtgAdm
Category: Memoria, Nacional
Tags: Bananeras, historia, masacre, Masacre de las bananeras, Negacionismo histórico, Refundación histórica, Repúblicas Bananeras
Slug: a-91-anos-de-la-masacre-de-las-bananeras-la-historia-se-repite-y-el-negacionismo-pretende-imponerse
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Masacre-Bananeras.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Comisariato_cienaga.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo.] 

[La madrugada del 6 de diciembre de 1928 ocurrió la Masacre de las Bananeras en Ciénaga, Magdalena. Los obreros reunidos en la plaza cercana al ferrocarril escucharon el sonido de las ametralladoras contra ellos. Hombres, mujeres, niños, multitud de familias se habían congregado en ese sector para protestar en contra de las paupérrimas condiciones laborales y locales que vivían. ]

[Para Javier Guerrero, miembro de la Asociación de Historiadores de Colombia, existía “mucha animadversión con la United Fruit Company. El país estaba bajo una república conservadora, triunfadora de la Guerra de los Mil Días, y en ese contexto fue que surgió la huelga de las bananeras, articulándose a ella muchos intelectuales y liberales de la época”. ]

[**Nueve eran las peticiones que hacían los líderes sindicales para sus compañeros, nueve fueron los muertos que el gobierno del conservador Miguel Abadía Méndez reconoció**. Entre los reclamos se exigía lo siguiente: habitaciones higiénicas y descanso dominical, aumento en 50% de los jornales de los empleados que ganaban menos de 100 pesos mensuales de la época, pago semanal y mejor servicio hospitalario. ]

[No se cumplió ninguna de aquellas exigencias y los cuerpos de los trabajadores fueron a dar al mar, a las fosas sin nombre y a quien sabe qué sitios escondidos entre las miles y miles de plantas de banano. Nadie sabe cuántos hombres, mujeres, niños y ancianos murieron allí, mucho menos la cifra exacta.]

[Para el general Carlos Cortés Vargas, encargado de dar la orden, fueron 47. A un mes de la masacre, el embajador de Estados Unidos en Colombia, Jefferson Caffery, mencionó cerca de 1.000 muertos en un comunicado hacia su país, relatando la situación. Para Gabriel García Márquez la cifra era de más de 3.000. ][(Le puede interesar: María Tila Uribe, reflexiones sobre la masacre de las bananeras)](https://archivo.contagioradio.com/%f0%9f%8e%a5-maria-tila-uribe-reflexiones-sobre-la-masacre-de-las-bananeras/)

[La conmoción nacional ante el hecho se sintió mucho después, cuando el joven abogado Jorge Eliécer Gaitán llevara el caso ante el Congreso de la República, denunciando los hechos. **“La metralla homicida para el pueblo y la rodilla puesta en tierra ante el oro americano”** afirmaba.]

### **La horrible noche** 

[Cerca de 300 soldados se enfrentaron cara a cara con los manifestantes. El ejército, bajo las órdenes del general Cortés, les dio plazo a los manifestantes de salir de allí, tocando un clarín y contando hasta cinco. Desde la multitud gritaron: **“¡Les regalamos esos minutos, cabrones!”.**]

[No alcanzaría a terminar el tercer toque del Clarín cuando la multitud fue atajada por las balas, que como una lluvia de indolencia, se cernieron sobre la noche y sobre los que allí protestaban. “Una ametralladora sobre los techos de la estación del tren”, como menciona Guerrero, sería el arma homicida. No fue solo esa noche, los días siguientes varios fusilamientos y represiones, por parte del Ejército Nacional, fueron llevadas a cabo por toda la zona bananera, en los municipios de Orihuela, Riofrío, Guacamayal, Sevilla y otros. ]

[“Hay mucha confusión entre los relatos históricos de la masacre. Era una época muy convulsionada —menciona Guerrero— estábamos en la etapa de las repúblicas bananeras". Ni los escritores, como García Márquez o Álvaro Cepeda Samudio, ni los historiadores, dan cabida en sus relatos a lo que se vivió en el sitio. ]

[Los tabacos encendidos en la noche, las linternas rudimentarias, la sangre del suelo tras el acto, los gritos y la angustia, todo ello se perdió en un silencio estatal y un negacionismo histórico que “siguen promoviendo los descendientes de esa tradición conservadora”. ]

\[caption id="attachment\_77779" align="aligncenter" width="681"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Comisariato_cienaga.jpg){.wp-image-77779 .size-full width="681" height="410"} Comisariato de la United Fruit Company en Ciénaga, Magdalena.\[/caption\]

### **La protesta social proviene del “diablo”** 

[Como estrategia para desprestigiar la lucha de los trabajadores en huelga, desde el gobierno de Miguel Abadía Méndez se corrió la voz de que los manifestantes eran comunistas y buscaban derrocar la autoridad del gobierno. A la sombra de la Revolución de Octubre y de los convulsos procesos históricos que Europa atravesaba, se les dio la categoría de «instigadores», **demeritando todas sus reivindicaciones y llevando a cabo una propaganda negra** —a través de los medios de comunicación impresos— a lo largo del territorio. ]

[“Se hablaba de la conspiración de los comunistas”, y la hegemonía conservadora, a partir de sus dirigentes y líderes religiosos de la Iglesia Católica, se enfocaron en categorizar a la protesta como una manifestación casi “diabólica”, buscando por todos los medios impedir el avance de la misma, bajar su popularidad para que no influenciara a otros trabajadores, impidiendo así las réplicas y evitando que se conociera la verdad sobre los hechos. ]

[La amenaza del Gobierno de los Estados Unidos, bajo las peticiones de la United Fruit Company, **de invadir militarmente el territorio con marines estadounidenses si no se solucionaba el problema,** contribuyó a la resolución violenta del mismo. La consiguiente devaluación competitiva del banano en la zona, sumado a la relevancia de otras zonas bananeras como las de Centroamérica, azuzó los intereses económicos de la multinacional y evitó a toda costa que sus quejas fueran escuchadas. ]

### **A 91 años de la masacre de las bananeras la historia se repite** 

[La situación en la zona no ha variado mucho desde aquel incidente. La United Fruit Company se fue del país unos años después de la masacre, sin embargo, cambió su nombre y volvió a establecerse con el título de Chiquita Brands. ]

[A mitad de la década de los noventa y principios del Siglo XXI, Chiquita Brands mantenía a sus trabajadores en deplorables condiciones laborales y sin garantías sindicales. El convulso escenario de Colombia, con una guerra interna de décadas, **llevó a que Chiquita pagara a grupos paramilitares, en este caso las Autodefensas Unidas de Colombia (AUC), por protección**, buscando mantener el *estatus quo* de su funcionamiento.]

[Según el Departamento de Justicia de los Estados Unidos, fueron cerca de \$ 1,7 millones de dólares que se pagaron a las AUC entre 1997 y 2004. A cambio, Chiquita obtenía protección de sus empleados en las plantaciones de banano y despojo de tierras a campesinos —llevadas a cabo de manera violenta— y con complicidad de las autoridades de Colombia.][(Lea también: Con marcha, paro estudiantil conmemora 90 años de masacre de las bananeras)](https://archivo.contagioradio.com/masacre-de-las-bananeras/)

### **Una refundación de la memoria ** 

[Declaraciones como las de la Representante a la Cámara, María Fernanda Cabal, al desestimar la masacre por estar incluida dentro del libro «Cien Años de Soledad», de García Márquez, y su constante alegoría a un “Mito Nacional Comunista”, no son prueba de ignorancia por parte de los que defienden esta versión, sino una afrenta directa contra la memoria de la nación. Como menciona Guerrero, “este negacionismo histórico trata de demostrar que aquí no pasó nada”.]

[De igual forma, personajes como José Obdulio Gaviria, atacan directamente a los hechos de la historia que no encajan en su versión de país. El senador por el Centro Democrático sugirió que los millones de desplazados que existen en Colombia —resultado de un conflicto armado interno y una desatención histórica por parte del estado hacia el tema de tierras y reparación de víctimas— no son más que migrantes.][(Lea también: Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino)](https://archivo.contagioradio.com/con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino/)

[Usando el mismo esquema para desvirtuar cualquier lucha o protesta social, en pro de una versión oficial que se acomode a los intereses oligárquicos y políticos de los que dirigen el país, los grandes terratenientes, las multinacionales y los políticos que se enriquecen de este modelo, amenazan la construcción verídica y reparadora de la memoria.  Ya que, como menciona Guerrero, “en Colombia, muchas violencias y muchos hechos vergonzosos se ocultan y se reprimen”.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
