Title: Convocan plantón por la dignidad de los presos en Manizales
Date: 2015-01-30 20:24
Author: CtgAdm
Category: eventos
Tags: crisis carcelaria, Derechos Humanos, manizales, paz soulicones crisis carcelaria
Slug: planton-por-la-dignidad-de-los-presos-en-manizales
Status: published

###### Foto:elindependiente 

El viernes 30 de Enero, familiares y amigos de los presos políticos en Colombia, organizaciones de DDHH de Caldas y Risaralda,  la Fundación de Solidaridad y Defensa con la Población Carcelaria de Colombia y demás sectores sociales que hacen parte del Movimiento Nacional Carcelario han convocado a la realización en la ciudad de Manizales  de un plantón por la dignidad de los presos para el **próximo viernes  30 de enero partir de las  2:00 de la tarde.**

El evento será por  la defensa de los derechos humanos de los detenidos y detenidas en la Cárcel la Blanca y Villa Josefina de Manizales, contra los atropellos del INPEC. La jornada tendrá como escenario el  Palacio De Justicia Fanny González Franco.
