Title: Secuestran, sedan y cortan el cabello a integrante del MOVICE
Date: 2018-01-19 13:43
Category: DDHH, Nacional
Tags: blanca nubia, centro de memoria, MOVICE, violencia contra líderes
Slug: secuestran-sedan-y-cortan-el-cabello-a-integrante-del-movice
Status: published

###### [Foto: Movice] 

###### [19 Ene 2018] 

En la mañana de este viernes el Movimiento de Víctimas de Crímenes de Estado denunció que Blanca Nubia Diaz, fundadora de la organización fue **secuestrada, sedada, rapada** y tirada frente al Centro de Memoria, Paz y Reconciliación del centro de Bogotá el pasado 13 de Enero.

Según la denuncia, **Blanca Nubia salía se su lugar de residencia** cuando fue interceptada por hombres que la sedaron para raptarla. Entre las pocas cosas que recuerda están las palabras de los captores que se referían a ella como "sapa" o guerrillera.

Luego del rapto la defensora de DDHH **fue abandonada en frente de la sede del Centro de Memoria**, lugar en el que frecuentemente los y las integrantes del MOVICE realizan actividades de memoria, de elaboración de propuestas y exigibilidad de derechos.

Según la organización este **no es el primer hecho de violencia que sufre Blanca Nubia** dado que "Blanca Díaz viene desde hace 16 años y medio exigiendo el esclarecimiento de la desaparición, violación y asesinato de su hija Irina del Carmen. Desde entonces Blanca ha sido objeto de diversos actos de intimidación, seguimiento y desplazamiento forzado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
