Title: Corte Suprema de Justicia votará para fiscal general el 23 de Junio
Date: 2016-06-09 16:37
Category: Judicial, Nacional
Tags: Corte Suprema de Justicia, Fiscal General de la Nación, Nestor Humberto Martínez
Slug: corte-suprema-de-justicia-realizara-votacion-para-fiscal-general-el-23-de-junio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/corte-suprema-fiscal-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elinformador] 

###### [9 Jun 2016] 

La Corte Suprema de Justicia aplazó la votación para el cargo de Fiscal General de la Nación para el próximo 23 de Junio luego de escuchar las propuestas de [Nestor Humberto Martínez](https://archivo.contagioradio.com/con-nestor-humberto-martinez-la-fiscalia-quedaria-en-manos-del-poder-empresarial/), Mónica Cifuentes y Yesid Reyes. Según se conoció en la tarde de hoy el **candidato que más votos obtuvo en la primera ronda de votación fue Martínez** que habría alcanzado 14 votos, ganando a Reyes con 8 y Cifuentes con 6.

Según la presidenta de la corporación “No hubo una votación mayoritaria por ninguno**. **Se presentaron votaciones un poco más altas para unos que para otros dependiendo de la ronda” y por ello **se realizará una nueva ronda en 15 días hábiles**. Sin embargo, a pesar de las posturas de organizaciones de DDHH la CSJ declaró que la terna examinada es "viable".

Varias organizaciones de derechos humanos le expresaron a la Corte Suprema de Justicia los criterios y compromisos que debería tener un Fiscal para la paz y **que no ven reflejados la terna** conformada por[ Nestor Humberto Martínez (ex ministro de la Presidencia)](https://archivo.contagioradio.com/con-nestor-humberto-martinez-la-fiscalia-quedaria-en-manos-del-poder-empresarial/), Yesid Reyes  (ministro de Justicia) y Mónica Cifuentes Osorio (asesora jurídica del proceso de paz con las FARC) .** **

Según las organizaciones el nuevo fiscal debe demostrar que puede **superar la impunidad de los graves crímenes que se han cometido en el país**, debe apostarle a los procesos de justicia que se requieren para lograr procesos de reconciliación, debe superar los delitos como el desplazamiento forzado, avanzar en la investigación de críemenes de lesa humanidad y **debe estar comprometido con la superación de la impunidad frente a la corrupción.**

Sin embargo parece que el candidato más opcionado es Nestor Humberto Martínez, quien para varios analistas ya estaba haciendo campaña para el cargo desde que estaba representando al gobierno como Super Ministro. [Ramiro bejarano](https://archivo.contagioradio.com/con-nestor-humberto-martinez-la-fiscalia-quedaria-en-manos-del-poder-empresarial/) expresó que el super ministro **estaba buscando el respaldo de todos los sectores políticos para afianzar su aspiración al cargo del Fiscal General cuando se reunió, a puerta cerrada con el senador Alvaro Uribe.**

También los defensores de DDHH Alberto Yepes y Gustavo Gallón han coincidido en afirmar que la candidatura más peligrosa para el Estado de derecho es la de Martínez puesto que su trayectoria ha sido ligada a intereses empresariales de los grandes grupos económicos en Colombia y con el partido político del vicepresidente Germán Vargas Lleras.

En un Estado Social de Derecho el poder político no puede estar a favor del poder económico, porque puede suceder algo [similar a lo que pasa con el Procurador](https://archivo.contagioradio.com/organizaciones-proponen-acciones-para-fortalecer-la-restitucion-de-tierras/), **se debe rechazar que la persona encargada de investigaciones y sanciones esté "estrechamente comprometida con los sectores económicos”**asevera Yepes.
