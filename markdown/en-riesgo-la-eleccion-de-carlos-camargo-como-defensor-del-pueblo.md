Title: En riesgo la elección de Carlos Camargo como Defensor del Pueblo
Date: 2020-10-16 15:01
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Anular elección, Carlos Camargo, Consejo de Estado, David Racero, Defensoría del Pueblo
Slug: en-riesgo-la-eleccion-de-carlos-camargo-como-defensor-del-pueblo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Defensor-del-Pueblo-Carlos-Camargo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Consejo de Estado admitió la demanda instaurada por el congresista David Racero que busca**  anular la elección del recientemente elegido Defensor del Pueblo, Carlos Camargo;** argumentando que el funcionario no cumple con los requisitos de experiencia para ocupar el cargo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la [Ley 24 de 1992](http://www.secretariasenado.gov.co/senado/basedoc/ley_0024_1992.html#3) el Defensor del Pueblo debe reunir las mismas calidades y requisitos formales exigidos para ser Magistrado de la Corte Suprema de Justicia, de la Corte Constitucional o del Consejo de Estado, los cuales se recogen en el artículo [232](https://www.constitucioncolombia.com/titulo-8/capitulo-1/articulo-232) de la Constitución, estos son: ser colombiano de nacimiento; ser abogado; no haber sido condenado por sentencia judicial a pena privativa de la libertad; **y haber desempeñado, durante quince años, cargos en la Rama Judicial o en el Ministerio Público, o haber ejercido por el mismo tiempo, la profesión de abogado o la cátedra universitaria en disciplinas jurídicas en establecimientos reconocidos oficialmente.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Justamente sobre este último punto de experiencia es que se centran los argumentos del accionante, dado que este señala en la demanda que «*a pesar de que la Comisión de Acreditación documental certificó que los tres candidatos* (a la Defensoría del Pueblo) *cumplían con la experiencia requerida por el artículo 232 de la Constitución Política, en un análisis de las hojas de vida de los candidatos aportadas por la Presidencia de la República \[…\] se comprueba que dos de los candidatos* (entre ellos Camargo) *no cumplen con el requisito de experiencia profesional*». **Asimismo, según se extrajo de la demanda, en los cálculos del accionante Carlos Camargo solo acredita 10 años y 9 meses de experiencia profesional en Derecho.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al respecto, la demanda argumenta que **«*los requisitos de experiencia para acceder al cargo de Defensor del Pueblo exigidos por el artículo 232 de la Constitución son expresos y no admiten interpretaciones que los flexibilicen.*** *Por ello, y en un análisis estricto de las hojas de vida, se comprueba que Camargo y la candidata Miriam Martínez, **no acreditaron la experiencia de ejercicio de 15 años de la profesión de abogado con buen crédito, ni acreditaron haber tenido cargos en la Rama Judicial o en el Ministerio Público en ningún periodo*****».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora que la demanda fue admitida, esta será estudiada por la Sección Quinta del Consejo de Estado, que tendrá que evaluar si hay merito en los argumentos de la demanda y si decide revocar, o no, la elección del Defensor, la cual se produjo el pasado 14 de agosto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La cuestionada elección de Carlos Camargo como Defensor

<!-- /wp:heading -->

<!-- wp:paragraph -->

A los cuestionamientos de la demanda, se suman los que, previo a la elección, habían realizado varias organizaciones sociales, quienes señalaron que Camargo no tiene ninguna experiencia en la promoción y defensa de los Derechos Humanos y sobre el que además recayeron serias críticas por su relación con el presidente Iván Duque, que fue quien lo postuló al cargo, lo que llevó a afirmar que la nominación se realizó a manera de cuota burocrática. (Lea también: [Defensoría del Pueblo será repartida como cuota política del gobierno Duque](https://archivo.contagioradio.com/defensoria-del-pueblo-cuota-politica/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el nombramiento fue criticado por el desempeño de Camargo en anteriores cargos como el de magistrado del Consejo Nacional Electoral –CNE-  en el que engavetó la investigación, pese a las pruebas presentadas, en contra de Óscar Iván Zuluaga por la presunta entrada de dineros de Odebrecht a su campaña presidencial; y también por considerarse que con la posesión del funcionario en el cargo, **el Gobierno estaba cooptando los diversos órganos de control como Fiscalía, Contraloría, Procuraduría y Defensoría;** así lo expresó en su momento León Valencia, director de la** [Fundación Paz y Reconciliación](https://twitter.com/parescolombia)**. (Lea también: [Carlos Camargo será la voz del gobierno en la Defensoría del Pueblo](https://archivo.contagioradio.com/cercania-del-nuevo-defensor-carlos-camargo-al-gobierno-es-un-dano-a-la-democracia/))  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
