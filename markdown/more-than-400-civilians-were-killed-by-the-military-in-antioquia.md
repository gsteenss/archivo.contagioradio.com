Title: More than 400 civilians were killed by the military in Antioquia
Date: 2019-05-30 18:12
Author: CtgAdm
Category: English
Tags: Cases, Ejecucion extrajudicial, Extrajudicial executions
Slug: more-than-400-civilians-were-killed-by-the-military-in-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Ejecuciones-Extra-Judiciales.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Human rights organizations presented a new report this Monday to the Comprehensive System of Truth, Justice, Reparation and Non-Repetition that outlines 156 cases of extrajudicial killings that were carried out in the Antioquia region.

According to the report, elaborated by the Yira Castro Coporation and the Judicial Liberty Corporation, these killings were commited by troops commanded by the Lieutenant colonel Carlos Medina Jurado while he was part of the Artillery Battalion No. 4 and the commander of the XIV Brigade. Byron Góngora, lawyer at the Judicial Liberty Corporation,explained that they found more than 400 victims of these crimes.

**Antioquia, the region with the highest number of extrajudicial killings**

Góngora clarified that the report collected cases of extrajudicial executions  perpetrated in eastern Antioquia and Magdalena Medio, areas that were the most affected by these types of crimes. According to the lawyer, this region is one of the first places where it was first reported that peasants were being detained and later disappeared.

According to his findings, the presence of guerrillas in the region, a military doctrine that falsely assumed peasants to be guerrilla collaborators and orders from the central command to increase combat kills were the conditions that gave way to the realization of these crimes.

The lawyer said that the report is intended to highlight the cases committed in Antioquia, so that they can be prioritized by the Special Peace Jurisdiction within Case 003, which encompasses "Deaths illegally presented as combat kills by State agents."

### **"This report is a call for non-repetition"** 

Finally, Góngora affirmed that the report is especially relevant now because of the recent article published by The New York Times that described new policies from the military high command that could give rise to the return of extrajudicial killings in the country. He said it was important for "the public opinion to understand the structures that gave way to the rise of the extrajudicial killings phenomenon."

A version of the report, "Not neglect nor rotten apples," will be available to the public upon request.
