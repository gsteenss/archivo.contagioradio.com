Title: Apropiación ilegal de tierras por parte de Río Paila alcanza las 45 mil hectáreas
Date: 2015-03-24 17:39
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Acumulacion de tierras, Bridgar y urrutia, Comisión Colombiana de Juristas, Jhenifer Mojica, Ley Urritia, Plan Nacional de Desarrollo, Rio Paila, Vichada
Slug: apropiacion-ilegal-de-tierras-por-parte-de-rio-paila-alcanza-las-45-mil-hectareas
Status: published

###### Foto: fedebiocombustibles.com 

<iframe src="http://www.ivoox.com/player_ek_4258032_2_1.html?data=lZeimpWXdo6ZmKiak5eJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7k08rgw5C2aaSnhqax0ZC0pcrgwpDgx5DXrcjpxpDO0tfTtMrVz8ncjc7QqcjVzdLS0NnJb8XZjNmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jhenifer Mojica] 

A través de la estrategia de creación de **empresas tipo SAS**, la empresa **“Río Paila”**, en Vichada y concretamente en el proyecto “Veracruz” se ha apropiado de cerca de 45 mil hectáreas. Según Jhenifer Mojica, integrante de la **Comisión Colombiana de Juristas**, hay procesos de adjudicación con casi 40 empresas que se constituyeron para violar la ley, y además firmaron créditos en los que el respaldo es el contrato de usufructo a 30 años.

Frente a los campesinos , Mojica afirma que las empresas llegaron a propiciar los negocios aunque los campesinos a quienes se había adjudicado el predio no las estaban vendiendo, según la investigadora l**as empresas SAS compraron hectáreas hasta en 20000 mil pesos** cuando su valor real se acerca al millón de pesos, valor que se incrementó en menos de 4 meses.

En el caso de la empresa Río Paila, no se ha constatado un factor de presión directa por parte de actores ilegales a los campesinos para que vendieran las tierras, sin embargo el departamento del Vichada se caracteriza por ser de control de paramilitar. No obstante en **la fiscalía se está investigando si hubo acción de grupos militares o paramilitares** que facilitaran los negocios.

Mojica, afirma que no es suficiente con que se hagan denuncias en cuanto a la apropiación ilegal de tierras en el país, como es el caso del **magistrado Prettet**, sino que es necesario que se revisen las políticas que están facilitando este tipo de acciones sin que hay investigación y además hay políticas que están orientadas a legalizar ese tipo de prácticas.

A pesar de que estos casos de apropiación ilegal de tierras se vienen denunciando desde hace cerca de 3 años por **Iván Cepeda o Wilson Arias**, y a pesar de que el gobierno anunció demandas para frenar este tipo de acciones, hasta el momento no se ha hecho nada y por el contrario la ley 133 o ley de baldíos se impulsan estas prácticas, usadas en principio por el ex embajador de Colombia ante Estados Unidos con su firma Bridgar y Urrutia.

Adicionalmente Mojica denuncia que el columnista **Mauricio Botero**, quien ha defendido a través de sus columnas el accionar de la empresa Río Paila, le está ocultando a la opinión pública que **hace parte de la junta directiva de esa empresa**.
