Title: Visita oficial de fiscal Bensouda de CPI es llamado de atención para Colombia
Date: 2017-09-12 12:43
Category: DDHH, Nacional
Tags: Corte Penal Internacional, cpi, Fatou Bensouda, Fiscal CPI
Slug: fiscal-cpi-colombia-fatou-bensouda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/BN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Imatin.net] 

###### [12 Sept. 2017] 

En una visita oficial y que ha sido calificada como histórica llegó a Colombia Fatou Bensouda, Fiscal y jefa de **la Corte Penal Internacional quien tiene dentro de su agenda tratar el tema de las ejecuciones extrajudiciales,** los avances en materia de investigación y sanción de esos crímenes en particular a los máximos responsables, así mismo indagaría sobre los temas de responsabilidad de mando en la JEP.

**Camila Moreno, Directora del Centro Internacional para la Justicia Transicional en Colombia** destaca que es la primera vez que un fiscal de la CPI visita de manera oficial el país y no de visita académica “en esta oportunidad viene la Fiscal Fatou Bendousa como parte del seguimiento que la CPI y en particular la Fiscalía de la CPI está haciendo al caso de Colombia”

Cabe recordar que Colombia está bajo observación de la CPI hace varios años, lo que implica un seguimiento a los avances en el **cumplimiento de las obligaciones del Estado en la investigación, juzgamiento y sanción de los crímenes** que serían de competencia de la Corte Penal, como los de lesa humanidad, de guerra y genocidio que son aquellos para lo que en los que los Estados que no pueden o no quieren investigar se activa la competencia de la Corte.

“Lo que hace la Corte es tomar estos casos, investigarlos y juzgar y sancionar a los máximos responsables. En el caso de Colombia hay una situación de impunidad estructural, muchos delitos que son competencia de la Corte están todavía en la impunidad y eso es lo que la Fiscal de la Corte viene monitoreando”.

### **Esta visita constituye un mensaje contundente de la CPI a Colombia** 

Para Moreno **el mensaje que quiere dejar la Fiscal de la CPI con su presencia en el país es que ellos están atentos a los avances** en materia investigativa en Colombia, que quieren unas respuestas concretas y que siguen de manera atenta y con alto “interés lo que está pasando aquí”, en concreto con los máximos responsables de las ejecuciones extrajudiciales.

### **¿Cuántos casos tiene la CPI y qué hace con ellos?** 

Son más de 1200 casos los que tendría a cargo para investigar la CPI, en los que se habla de 23 Generales de las Fuerzas Militares implicados y 8 Coroneles entre activos y retirados. Sin embargo, Human Rights Watch ha entregado cifras que rondan los 3 mil casos y otras organizaciones tienen cifras de 5 mil ejecuciones extrajudiciales.

**“Lo que hace la CPI es seleccionar y centrarse en investigar a los máximos responsables** de los delitos que son de competencia de la Corte, porque su pretensión no es reemplazar a los sistemas nacionales de justicia. Por eso ellos están haciendo seguimiento no a todo el universo de casos sino a unos en los que habría responsabilidad de lo máximo responsables”.

Eso no quiere decir que el mensaje de la Corte Penal sea que solo hay que investigar esos máximos responsables, sino que lo que ha dicho es que existe un principio de complementariedad del Estatuto de Roma que **lo que plantea es que los Estados que lo han suscrito tienen que cumplir con la obligación de investigar, juzgar** y sancionar todos los crímenes internacionales.

### **¿Qué dirá la CPI de las ejecuciones extrajudiciales en la JEP?** 

Será el Estado colombiano y los magistrados de la Jurisdicción Especial de Paz – JEP – quienes decidan en aplicación de los criterios que se encuentran en la Constitución, si los hechos sucedidos por ejecuciones extrajudiciales tienen relación con el conflicto armado o no.

**“Lo que la Corte le dice al Estado es usted verá cómo lo hace, si lo hace con la JEP o la justicia ordinaria,** pero el compromiso es investigar, juzgar y sancionar a los responsables de esos hechos. ¿Cómo lo haga? Eso es potestad del Estado colombiano, sino lo hace, lo hará la CPI”. Le puede interesar: [CPI solicita que 29 generales y coroneles sean juzgados por ejecuciones extrajudiciales](https://archivo.contagioradio.com/corte_penal_internacional_falsos_positivos/)

### **Algunas preocupaciones de la Fiscal Bensouda** 

Moreno recuerda además que **la Fiscal de la CPI ya ha hecho advertencias porque tiene algunas preocupaciones frente al estado de las investigaciones** en los casos de ejecuciones extrajudiciales, como por ejemplo la responsabilidad de mando que está en el Acto Legislativo para la Paz y que se retoma en el Proyecto de Ley Estatutaria que está pendiente de iniciar debates en el Congreso.

**“A ella le preocupa que la definición de mando que adopta el Estado colombiano no se ajusta exactamente al estándar internacional** y que eso podría implicar que esos máximos responsables de delitos de competencia de la Corte al final no sean juzgados”. Le puede interesar: [Si Estado no juzga empresarios y mandos militares "que lo haga la CPI"](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/)

Si bien la agenda de la Fiscal es confidencial, Moreno manifiesta que las reuniones privadas que realizará podrían ir encaminadas a conocer y exigir resultados sobre quienes tendrían o serían los máximos responsables de ejecuciones extrajudiciales.

<iframe id="audio_20828551" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20828551_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
