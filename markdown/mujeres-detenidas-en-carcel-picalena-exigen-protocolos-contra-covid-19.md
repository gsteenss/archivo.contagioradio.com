Title: Mujeres detenidas en cárcel Picaleña exigen protocolos contra Covid 19
Date: 2020-04-30 19:09
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Cárceles, #covid19, #Pandemia, #Picaleña, #Prisionerapolíticas
Slug: mujeres-detenidas-en-carcel-picalena-exigen-protocolos-contra-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/A_UNO_246756.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Un grupo de once mujeres reclusas, que fueron trasladadas de forma arbitraria de la cárcel de Buen Pastor, en Bogotá, a la cárcel de Picaleña en Ibagué, alertan sobre las pésimas condiciones en las que se encuentran y el latente temor de un contagio en un centro penitenciario **que no cuenta con protocolos de protección ante el Covid 19.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con las mujeres, el traslado se produjo el pasado el pasado 24 de marzo, cuando ya todo el país se encontraba en cuarentena. Además, en el vehículo usado para ese movimiento, también fueron transportados otros reclusos de la cárcel La Modelo y La Picota sin ninguna medida de Bioseguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Los internos hombres ya fueron sometidos a la prueba del Covid 19. Sin embargo, hasta la fecha y teniendo pleno conocimiento de que fuimos trasladadas en el mismo bus, **la dirección no se ha preocupado por hacernos la prueba a las once mujeres**" afirman.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel de Picaleña no tiene protocolos contra el Covid 19
---------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 8 de Abril, la Personería de Ibagué realizó una visita a este centro de reclusión. En ella pudo constatar que no hay evidencia de un procedimiento de aislamiento o protocolos sobre medidas de Bioseguridad para disminuir el riesgo del Covid 19. (Le puede interesar:["Ninguna Cárcel en Colombia está lista para afrontar Covid 19")](https://archivo.contagioradio.com/ninguna-carcel-en-colombia-esta-lista-para-afrontar-covid-19-porque/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, según el informe, tampoco se evidencia que el personal **haya recibido información o educación sobre las recomendaciones del Covid 19.** No se constatan acciones de limpieza, desinfección y recolección de residuos, tampoco hay suministro de agua suficiente, ni de insumos para el lavado de manos para el personal del establecimiento.

<!-- /wp:paragraph -->

<!-- wp:heading -->

En condiciones inhumanas se encuentran las mujeres reclusas
-----------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las mujeres denuncian que desde el día del traslado, se encuentran en precarias condiciones. Según ellas, las mujeres son divididas en dos grupos solo para salir a un estrecho pasillo. Un grupo dura 4 horas, mientras que el otro solo 3 horas y media.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hecho que evidencia que las mujeres pasan, aproximandamente, 20 horas encerradas en la celda. Asimismo solo salen durante 20 minutos a una cancha, sin embargo, desde el traslado esto solo ha pasado 4 veces. (Le puede interesar: "[Jueces fallan a favor de población reclusa en 3 cárceles](https://archivo.contagioradio.com/jueces-fallan-a-favor-de-poblacion-reclusa-en-3-carceles/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, también resaltan que de las once mujeres, cinco están sindicadas, dos cumplen con los requisitos del decreto 546 para recibir prisión domiciliaria por 6 meses, mientras que otra sufre de obesidad mórbida y otra de asma crónica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las mujeres están solicitado la implementación urgente de los protocolos de seguridad frente al Covid 19 y la prueba de contagio. De igual forma esperan que no haya retaliaciones en su contra por las denuncias y finalmente se les garantice el derecho a la vida y la salud. (Le puede interesar: "[Rumbo a un genocidio carcelario](https://www.justiciaypazcolombia.com/rumbo-a-un-genocidio-carcelario/)")

<!-- /wp:paragraph -->
