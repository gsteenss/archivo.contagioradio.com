Title: Dos integrantes de ASCAMCAT resultaron heridos luego de atentado en La Gabarra
Date: 2015-02-26 17:11
Author: CtgAdm
Category: DDHH, Nacional
Tags: ASCAMCAT, Catatumbo, Paramilitarismo
Slug: dos-integrantes-de-ascamcat-resultaron-heridos-luego-de-atentado-en-la-gabarra
Status: published

###### Foto: tomada de youtube 

Según la denuncia de la Asociación Campesina del Catatumbo, ASCAMCAT, hombres que se movilizan en una camioneta color gris, llegaron a una casa de integrantes de ASCAMCAT y abrieron fuego contra ellos utilizando armas con silenciador. El hecho se presentó este 26 de febrero de 2015 en horas de la mañana, en el corregimiento de La Gabarra, municipio de Tibú, Norte de Santander. Los responsables del hecho, huyeron tomando como ruta, la carretera que conduce a la cabecera municipal de Tibú.

El resultado del atentado habría sido de una mujer y un hombre heridos, cada uno con  3 impactos de bala en su cuerpo. Una de las dificultades para su atención es que "no existen medios para trasladarlos y que se les brinde atención médica."

Los campesinos solicitan de manera urgente medios para el traslado y la atención de los heridos, y responsabilizaron al Estado por la acción u omisión que causen perjuicios a la vida de estas personas.

En meses anteriores, ASCAMCAT ha denunciado la presencia de grupos de paramilitarismo en la Gabarra y la región.

#### *Con información de la Asociación Campesina del Catatumbo ASCAMCAT* 
