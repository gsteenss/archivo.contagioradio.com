Title: Se anuncia paro nacional para este jueves en Guatemala
Date: 2015-08-25 13:31
Category: El mundo, Entrevistas, Movilización
Tags: Claudia Samayoa, Corrupcion Guatemala, Dorval Carías, Elecciones 2015, Guatemala, Ministerio de comunicaciones Guatemala, Ministerio de Finanzas, Otto Pérez, Paro Nacional, Víctor Corado
Slug: se-anuncia-jornada-de-paro-nacional-para-este-jueves-en-guatemala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/movilización-popular-en-guatemala-piden-renuncia-de-otto-pérez-molina-y-roxana-baldetti.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: [utopiarossa.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_7596207_2_1.html?data=mJqmmJeUe46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRl8afwtPi0MjNpYzkwtfcjdPFp8rjz8bZjdXFtsKfxtjhx5DOucbqxtiYx9OPi9bV1craw9HFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Claudia Samayoa, defensora DDHH] 

###### [25 Ago 2015]

Tras la renuncia de los ministros de finanzas y comunicaciones del gobierno de Otto Pérez Molina, continúan las jornadas de movilización en Guatemala convocando **un paro nacional para el próximo **27 de agosto**,** para exigir la renuncia del mandatario, la suspensión de las elecciones generales 2015 y la aprobación de una reforma al sistema que incluya reformas constitucionales.

Según Claudia Samayoa, lideresa social y defensora de derechos humanos, **van 12 funcionarios del alto gobierno que han renunciado a su cargo, incluso se habla que el secretario técnico del sistema nacional de seguridad** está próximo a desistir de su puesto.

Los funcionarios que han renunciado lo han hecho porque “**les da vergüenza estar en el gobierno, el ministerio de  comunicaciones, fue mencionado como involucrado en la recepción de fondos ilegales**… Ante las presiones de renuncia y a la negativa del presidente los ministros están renunciando”, dice Samayoa, quien agrega que ante la renuncia de **Víctor Corado, Ministro de comunicaciones y Dorval Carías, ministro de finanzas,** el presidente no se ha pronunciado.

Desde tempranas horas de este martes al menos cinco lugares del país iniciaron el día con manifestaciones, donde los ciudadanos han salido a las calles a protestar  “superando las barreras ideólogicas, de clase y generacionales”, señala la líder social. En este momento, hay manifestaciones frete a la Corte Suprema de Justicia, frente al Congreso  y a la Casa Presidencial.

Con el paro del jueves se exigirá la suspensión de las próximas elecciones, teniendo en cuenta que, de acuerdo con la defensora de derechos humanos, **hay más de 1200 candidatos que tienen procesos penales u órdenes de captura.**
