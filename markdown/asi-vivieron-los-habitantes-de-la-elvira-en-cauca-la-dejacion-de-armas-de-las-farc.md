Title: Así vivieron los habitantes de la Elvira en Cauca la dejación de armas de las FARC
Date: 2017-06-14 11:38
Category: Nacional, Paz
Tags: Dejación de armas, implementación acuerdos de paz
Slug: asi-vivieron-los-habitantes-de-la-elvira-en-cauca-la-dejacion-de-armas-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dejación.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC-EP] 

###### [14 Jun 2017] 

Las comunidades también estuvieron presentes en la dejación de armas de las FARC-EP en la zona rural de La Elvira, Cauca, uno de los territorios que más ha sufrido el conflicto en el país. Los habitantes que estuvieron allí presentes expresaron que **este hecho es uno de los más importantes y significativos para la construcción de paz en Colombia**.

**“Los combatientes lloraron de alegría y nosotros sentimos melancolía porque este es un paso más hacia una Colombia diferente”** así relató Héctor Carabalí, líder social afro de La Elvira, quien además aseguró que la dejación de armas da pasó a un nuevo momento, que implica cumplimientos de todos los sectores.

En este sentido los habitantes de La Elvira desde la firma del acuerdo de paz, han iniciado trabajos conjuntos con los integrantes de las FARC-EP para hacer pedagogía de paz y preparar a la comunidad para el recibimiento de los combatientes, “**nos hemos estado preparando todas las comunidades para reencontrarnos con todos los compañeros y compañeras** que en algún momento se alzaron en armas para exigir sus derechos” afirmó Carabali.

<iframe src="http://co.ivoox.com/es/player_ek_19265885_2_1.html?data=kp6fmJqcfJahhpywj5aYaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncamZpJiSo57HuNDmjKjO1MbGpc2ZpJiSo6mJdqSfycbPy9nFstXZjMnSjbHFb6bg187fw4qWh4zn0Mffx5DIqcvVxM6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Un ejemplo de ello es la construcción del Coliseo Nicolas Fernández, edificada en conjunto entre habitantes de La Elvira, Cauca e integrantes de las FARC-EP, espacio se ha convertido en un lugar de intercambio de historias y vivencias. Le puede interesar: ["Dejación de armas de las FARC-EP marca un cambio histórico en el país"](https://archivo.contagioradio.com/42169/)

Sonia Cortes, docente en el municipio Buenos Aires, en Cauca, indicó que es emocionante que los lugares que antes simbolizaban la guerra ahora sean sinónimo de reconciliación “**en la zona donde hubo guerra ahora se respira cordialidad, estar presente en esa situación lo llena a uno de muchas emociones y sentimientos**, esos muchachos ahora tienen la oportunidad de volver a reencontrarse con los que quieren” aseguró Cortez.

<iframe src="http://co.ivoox.com/es/player_ek_19266079_2_1.html?data=kp6fmJuUe5qhhpywj5WYaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncbTjz87OjajTttWZpJiSo57XaZO3jNjcxNfJb8Lhw87S0NnJb8bijNHOjarQusrmwpDd0dePqMbewsjWh6iXaaOnjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **La implementación del Capítulo étnico no avanza** 

Dentro de los Acuerdo de Paz de La Habana se incluyó el Capítulo Étnico, que garantizaba que en el proceso de implementación se realizarían espacios consultivos con las comunidades indígenas y afro tener en cuenta sus perspectivas, planes de desarrollo y dinámicas ancestrales, sin embargo, tras la creación el primero de enero de la comisión de Alto Nivel que haría este proceso, **los habitantes expresaron que no se ha concertado ningún escenario de interlocución**.

Carabali, señaló que aún **no hay avances significativos en la implementación de este capítulo y que hasta hace tan solo 15 días**, se tuvo una primera reunión con el Alto Comisionado para la Paz, en donde se estableció las funciones de la comisión de Alto Nivel que haría seguimiento este proceso. Le puede interesar: ["Delegación de EE.UU insta al gobierno a cumplir capítulo Étnico del Acuerdo de Paz"](https://archivo.contagioradio.com/capitulo-etnico-del-acuerdo37339/)

“Al pueblo Afro no se le ha consultado y desde este espacio le exigimos al gobierno que cumpla con este mandato constitucional” afirmó Carabalí. Otra de las exigencias que tanto comunidades afro como indígenas le hacen el gobierno del presidente Santos, es **el desmonte de las estructuras paramilitares, que aún hacen presencia en estos territorios y amedrentan a los habitantes**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
