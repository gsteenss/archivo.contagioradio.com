Title: El latir de un fotógrafo peregrino: Jesús Abad Colorado
Date: 2019-04-29 13:08
Author: CtgAdm
Category: Memoria
Tags: conflicto, Fotografia, jesús abad colorado, memoria
Slug: el-latir-de-un-fotografo-peregrino-jesus-abad-colorado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.41.41-AM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.40.37-AM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### Texto: Andrés Zea\* 

De pie, empuñando un micrófono con ambas manos y su cabeza levemente inclinada hacia el suelo; una luz cálida en posición cenital apuntó hacia él, como si la mirada de seis de las musas griegas que coronan la sala principal le observaran mientras una multitud de aplausos retumbaron en los muros del centenario Teatro Colón, mismo escenario donde hace casi 3 años se firmaron los acuerdos de paz. Una vez la luz tomó fuerza, permitió detallar esa particular figura de cabello ondulado, jean y camisa, la de Jesús Abad Colorado, o Chucho, como le dicen de cariño. Con la serenidad que le caracteriza se dispuso a conversar con el público sobre “El Testigo” el documental sobre su vida y trabajo periodístico.

Tenía en frente al autor de las fotografías incluidas en el informe ¡Basta Ya! del Centro Nacional de Memoria Histórica, mismo que llegó a mis manos hace un tiempo como un obsequio que fue despertando en mí un enorme interés por los temas relacionados con el conflicto armado y la memoria. Debo aceptar que, más allá de las cifras y relatos que aparecen en el informe, lo que me atrapó fueron las imágenes capturadas por el lente de Jesús Abad. Empecé a especular sobre la forma en que el fotógrafo llegaba tan rápido a esos sitios, en cómo hacía para lograr esas imágenes tan cercanas sin causar contrariedad con las víctimas o los victimarios, pero sobre todo pensaba en sus sentimientos, en lo que podría sentir al atestiguar semejantes vejaciones.

<p style="text-align: justify;">
<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1-1024x576.jpeg){width="1024" height="576" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1-1024x576.jpeg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1-300x169.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1-768x432.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1-370x208.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1.jpeg 1280w"}  

<figcaption>
Víctimas de masacre guerrillera Apartadó, Antioquía 1995

</figcaption>
</figure>
Así que decidí indagar en su vida, Colorado es un periodista antioqueño de raíces campesinas que se interesó por seguir las huellas que ha dejado a su paso el monstruo gigante de la guerra en el territorio colombiano, en un periodo de 25 años. Ha sido testigo de la mayor parte de los acontecimientos violentos de las últimas 3 décadas: la masacre de Bojayá perpetrada en 2002 por guerrilleros del Bloque José María Córdoba de las FARC; el periodo de violencia en la Comuna 13 de Medellín y la operación Orión entre el 2001 al 2003; el desplazamiento masivo tras la masacre en San Carlos, Antioquia, en 2003; del lamentable éxodo de familias en las montañas de San José de Apartadó, Antioquia, en 2005, y de los procesos de memoria y resiliencia en estos  mismos territorios y en otros donde la guerra también ha dejado su marca como en Trujillo, Valle del Cauca, donde entre 1988 y 1994 se registraron 342 víctimas de homicidio, tortura y desaparición forzada, o de El Salado, Bolívar, donde 450 paramilitares asesinaron con sevicia a 60 campesinos.

Si bien ha recorrido gran parte del territorio colombiano su trabajo como periodista va más allá de la reportería gráfica. No es de esos fotógrafos que obturan sin respeto y se marchan luego de haber conseguido lo que necesitan. En esa cualidad se encuentra la respuesta a mi primer interrogante: ¿cómo carajos hace para llegar tan rápido a lugares tan alejados? Jesús Abad aplica algo que en psicología se llama empatía. Recorre los caminos, paso a paso, y habla con las personas de los lugares que visita, escucha sus historias, seguramente les cuenta otras, lo que ayuda a generar mayor confianza en las personas. Así, poco a poco, al ritmo de su propio paso ha ido ganándose el afecto de muchos que le ofrecen su amistad, su casa, su palabra.

<p style="text-align: justify;">
<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1024x576.jpeg){width="1024" height="576" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-1024x576.jpeg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-300x169.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-768x432.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM-370x208.jpeg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.37.24-AM.jpeg 1280w"}

</p>
<figcaption>
Entierro de víctimas tras el atentado al oleoducto de Machuca, Antioquía 1998

</figcaption>
</figure>
Una vez en una conferencia le preguntaron cómo lograba capturar semejantes momentos sin que las personas se resistieran ante su cámara, sin incomodar. Él respondió que era gracias a que las miraba a los ojos, con respeto, a que caminó junto a ellas, a que sintió como ellas, porque no se puso en sus zapatos sino más bien se metió en sus pieles. Jesús Abad Colorado no es de esos fotógrafos que dispara ráfaga tras ráfaga, para él cada obturación es un latido. Por eso enfoca con el ojo izquierdo, porque es el más cercano al corazón. He ahí la razón de que sus fotografías sean tan certeras, porque se detiene, porque espera y obtura, no en el instante decisivo sino más bien en el momento correcto.

Pero el interés de Colorado no solo está en las víctimas sino también en los victimarios, en las huellas que deja la guerra en las estructuras, en el paisaje, en. los cuerpos. Sus fotografías buscan con sobriedad capturar un momento para la memoria. En Bojayá, en 2002, fue uno de los primeros periodistas en hacer presencia después de ocurrida la masacre; tenía un sinfín de escenas crueles para fotografiar pero la imagen que publicó sobre el trágico momento fue la del Cristo despedazado en el suelo de la iglesia destruida donde se refugiaron niños y mujeres. Domingo, uno de los sobrevivientes le preguntó al fotógrafo por qué no había tomado fotos de los brazos y las piernas desmembradas, de la sangre regada, Jesús respondió que ese tipo de imágenes son lo contrario de lo que él busca: no informan, pero sí generan odio y sed de venganza

<p style="letter-spacing: 0.48px; text-align: justify;">
<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.41.41-AM.jpeg){width="838" height="554" sizes="(max-width: 838px) 100vw, 838px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.41.41-AM.jpeg 838w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.41.41-AM-300x198.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.41.41-AM-768x508.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.41.41-AM-370x245.jpeg 370w"}

</p>
<figcaption>
Huellas de la guerra Comuna 13 de Medellín 2003.

</figcaption>
</figure>
Sus fotografías también han servido como fuente documental que evidencia el trasfondo oscuro de la guerra, como la macabra alianza entre paramilitares y Estado durante la Operación Orión en 2002, por ejemplo; o la indolencia de las entidades gubernamentales en los procesos de resiliencia de las comunidades; o como herramienta persuasiva para que el gobierno actué. Razones para justificar el predominio del blanco y negro en sus fotografías, porque considera que de esa forma destaca el respeto que siente por el dolor de las personas y, porque de esa forma sus imágenes se convierten en documentos.

La pasión, el amor, el dolor y el respeto son los pilares en los que se cimienta el trabajo de Jesús Abad. Tal vez es por eso que desde el primer momento en el que observe sus fotografías me sentí tan abrumado, porque su trabajo no está del lado de un medio o de un interés individual, su interés no está en el “espectáculo” no busca generar emociones someras, lo que él desea, pienso, es generar reflexión en los colombianos, que vean la realidad de la que tanto  huyen, esa que le causa pena a los políticos… de la que nadie desea hablar, anhela, provocar algún sentir, uno profundo que llegue hasta el tuétano de los huesos uno que lleve a las personas a exigir a gritos la verdad, la justicia y la no repetición.

<figure>
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.40.37-AM.jpeg){width="942" height="934" sizes="(max-width: 942px) 100vw, 942px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.40.37-AM.jpeg 942w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.40.37-AM-150x150.jpeg 150w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.40.37-AM-300x297.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.40.37-AM-768x761.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-26-at-10.40.37-AM-370x367.jpeg 370w"}

</p>
<figcaption>
Operación Orión, Comuna 13 de Medellín. Octubre de 2002

</figcaption>
</figure>
Jesús Abad permaneció ahí, al frente, el micrófono empuñado con ambas manos, con la mirada inclinada, rodeado de los aplausos que seguían. Observándolo desde mi silla notaba que sus suspiros eran esfuerzos para no llorar. El auditorio se puso de pie, pero él seguía mirando el suelo. Ahí encontré respuesta a otra pregunta: ¿Qué sintió, qué emociones quedan después de ser testigo del horror de la guerra? La respuesta se escribe con facilidad pero un tanto difícil de sentir: si se quiere tener una idea del estremecimiento de la guerra se debe escuchar a las personas que la han sufrido, caminar con ellas. Y no olvidar. Y contar la guerra y sus historias, porque para ellos, las víctimas, narrar sus historias y sentir que realmente fueron escuchadas les va quitando, poco a poco el peso del dolor.

El horror, el miedo y el dolor de los acontecimientos en los que estuvo calaron para siempre en su memoria y gracias -muchas gracias- a su interés por transmitirlas también quedarán para siempre en la memoria colectiva de un país que tanto la necesita.

###### \*Estudiante de Comunicación Social- periodísmo-Universidad Central 
