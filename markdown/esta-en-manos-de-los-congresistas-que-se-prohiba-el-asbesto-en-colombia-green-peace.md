Title: "Está en manos de los congresistas que se prohíba el asbesto en Colombia": Green Peace
Date: 2018-03-20 14:19
Category: DDHH, Nacional
Tags: asbesto, colombia sin asbesto, green peace, ley ana cecilia niño
Slug: esta-en-manos-de-los-congresistas-que-se-prohiba-el-asbesto-en-colombia-green-peace
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/GPC_49601-e1521573525932.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Green Peace] 

###### [20 Mar 2018] 

[Continuando con la lucha por la prohibición del asbesto en Colombia, la organización internacional Green Peace realizó este martes una actividad en la Plaza de Bolívar de Bogotá, en el día en que inicia en el senado **el último debate** de la ley “Ana Cecilia Niño” que busca detener el uso de asbesto, por el que  más de 320 personas al año mueren por la exposición a este mineral en  Colombia.]

[De acuerdo con Silvia Gómez, coordinadora local de Green Peace en Colombia, “hoy que se inician las sesiones legislativas en el Congreso, estamos exigiendo a los senadores que prohíban el asbesto ya”. Con el lema **“a ponerse los pantalones”** los participantes de la actividad tendieron pantalones en la Plaza De Bolívar “para preguntarle a los congresistas si están listos para prohibir el asbesto”.]

### **Hace falta un debate en el Congreso para que se apruebe la ley ** 

[Gómez manifestó que ya la Comisión Séptima del Senado aprobó en primer debate la ley “Ana Cecilia Niño” que lleva por nombre **a una de las víctimas mortales** que ha dejado el asbesto y quien dedicó su vida a la lucha por la prohibición del mineral. Por esto, “estamos exigiendo al Congreso que en diciembre dejó pendiente este proyecto por lo que deben agendarlo, debatirlo y aporbarlo”.]

[Adicionalmente la activista dijo que un año más con asbesto en el país implica la producción de 24 mil toneladas o 540 personas más **“que se mueren por cáncer de ovario, de laringe o por asbestosis”**. Con esto en mente recalcó que está en las manos de los congresistas del país la salud de todos los colombianos “por encima de los intereses económicos de las empresas que están luchando por que no haya prohibición”. (Le puede interesar:["En Colombia mueren 320 personas al año por cáncer asociado al asbesto"](https://archivo.contagioradio.com/en-colombia-mueren-320-personas-al-ano-de-cancer-asociado-a-la-inhalacion-de-asbesto/))]

### **En 60 países del mundo ha sido prohibido el asbesto** 

En ocasiones pasadas, la organización le dijo a Contagio Radio que existen 6 tipos de asbesto en el mundo y en el país se utiliza **el crisotilo** “que tiene ventajas para la industria porque es barato y tiene resistencia al fuego”. Sin embargo, se trata de un mineral cancerígeno que con sólo respirar una fibra “es muy probable que la persona quede expuesta a un cáncer, que se desarrolla en 20 años”.

[Además, la peligrosidad radica en que las pequeñas fibras, cuando son aspiradas,]**se pegan a los pulmones**[ y se va creando un tumor cancerígeno que genera una enfermedad que se llama abestosis que “es reconocida por la Organización Mundial de la Salud y por esto ha sido prohibido en 60 países”.]

### **El lobby de las empresas ha ocasionado que se siga produciendo este material** 

[La organización ha denunciado que el asbesto se encuentra tanto en la industria de los automóviles como en el de la construcción donde la empresa que hace mayor uso del mineral es **Eternit**. A pesar de las alertas que ha impuesto la OMS y los diferentes ministerios a nivel nacional, la empresa no ha detenido el uso del mineral en el país.]

[Green Peace recalcó que esta empresa realiza sus exportaciones de productos sin el mineral pero en **Colombia lo sigue utilizando**. Gómez indicó que “es inaceptable que se siga utilizando el asbesto cuando se tiene todos los argumentos y las evidencias para saber que se trata de un material peligroso para la salud”. (Le puede interesar:["El drama de la salud de las comunidades de la Guajira por la minería del Cerrejón"](https://archivo.contagioradio.com/comunidades-de-la-guajira-exige/))]

[Por esto, indicó que la presión y el lobby de diferentes empresas han conseguido que aun se siga utilizando este material en el país, añadiendo que es “ahora que la presión de los ciudadanos se necesita para **corroborar que no hay razón alguna** para que se siga utilizando este mineral".]

<iframe id="audio_24652494" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24652494_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
