Title: Atentan contra lideresa indígena Lolita Chávez de Guatemala
Date: 2017-06-13 03:06
Category: El mundo, Voces de la Tierra
Tags: Guatemala
Slug: atentado-lideresa-indigena-lolita-chavez-guatemala
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/foto-Nelton-Rivera-e1497340962173.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Desinformémonos]

###### [12 Jun 2017] 

En Guatemala organizaciones sociales denuncian que una de las principales lideresas que defienden los territorios de la amenaza extractivista, Lolita Chávez, fue víctima de un atentado junto a otros líderes de la región.

De acuerdo con la narración de los hechos, el Concejo de los Pueblos Kichees, (CPK) el pasado 7 de junio encontraron un **camión de la ruta de Santa Cruz del Quiche a Chichicastenango, transportando una gran cantidad de troncos de mader**a. Algunos integrantes del CPK decidieron preguntar la procedencia de dicho camión. Lo que se encontraron fue que ese transporte no tenía los permisos necesarios y optaron por trasladarlo a la gobernación departamental para que se aclarara la situación.

Sin embargo, el relato de los hechos evidencia que en lugar de encontrar al gobernador para evidenciar lo que estaba pasando, lo que les llegó unas horas más tarde a la gobernación fue un grupo de **12 hombres armados que empezaron a amenazar a los miembros del Concejo, entre los que se encontraban mujeres y niños y niñas.**

Tras salir de allí, **“fueron perseguidos por un vehículo con hombres fuertemente armados realizando disparos al carro donde viajaban** la lideresa Lolita Chávez Ixcaquic, junto a otros lideres de las comunidades”, cuenta la denuncia publicada.

No obstante, ese día no terminaron los hostigamientos y a la mañana siguiente mientras se dirigía hacia San Andrés Sajcabaja, el vehículo nuevamente copado por miembros del Consejo Comunitario de Desarrollo Urbano y Rural (COCODE), que pertenece al gobierno guatemalteco, y que según denuncian organizaciones sociales, varios de sus miembros han actuado bajo los intereses de empresas madereras. Hasta el momento no se conoce de personas heridas tras el atentado.

### El caso de las empresas madereras 

El CPK ha venido denunciando los intereses económicos que tienen empresas madereras y el propio Estado guatemalteco en este departamento. Como lo aseguran los ambientalistas, esa alianza ha talado miles de hectáreas de árboles, generado graves impactos en el ecosistema y las comunidades de esos territorios.  De acuerdo a las denuncias,  dichas actividades son posibles debido a que entidades estatales gubernamentales como **El Instituto Nacional de Bosques (INAB) dan el aval para que las compañías madereras arrasen con árboles.**

### La lucha de Lolita Chávez y el CPK 

Lolita es lideresa e integrante de la comisión política del Consejo Pueblos K’iche’, también cumple las veces de delegada ante el Consejo de Pueblos de Occidente (CPO), que reúne a comunidades de los departamentos de El Quiché, San Marcos, Huehuetenango, Quetzaltenango, Retalhuleu y Totonicapán, en Guatemala.

Asimismo forma parte de la Iniciativa Mesoamericana de las Mujeres Defensoras de Derechos Humanos, que articula redes de defensoras en Guatemala, El Salvador, México y Honduras; por cuya labor fue galardonada en el año 2014 con e**l Premio Internacional de DDHH Letelier Moffit.**

La lucha de la lideresa junto al CPK, se basa en acciones contra la minería, y las centrales hidroeléctricas y las empresas madereras. La resistencia pacífica de esta mujer se enmarca en enseñanzas ancestrales dirigidas a fortalecer y unir el movimiento. Además centra su trabajo en la reivindicación de los derechos de las mujeres.

Cabe mencionar que los integrantes del **CPK ya han sido objeto de amenazas, difamación, intimidación y ataques. De hecho, uno de sus integrantes fue asesinado el 12 de junio de 2012.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
