Title: Asesinan a joven de 16 años y a comunera indígena en Cauca y Nariño
Date: 2020-09-21 11:36
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Alba Alexandra Pizanda Cuestas, Argelia, Cauca, Mallama, nariño
Slug: asesinan-a-joven-de-16-anos-y-a-comunera-indigena-en-cauca-y-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Comunicado-resguardo-indigena-el-gran-Mallama.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Asesinatos-en-Cauca-y-Narino.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Tras conocerse las masacres registradas en el municipio de Buenos Aires, Cauca y El Charco, Nariño; comunidades denuncian dos nuevos asesinatos registrados en municipios de estos mismos departamentos. (Lea también: [Nueva masacre en el Cauca, 7 jóvenes asesinados](https://archivo.contagioradio.com/nueva-masacre-en-el-cauca-7-jovenes-asesinados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El primer hecho, fue el asesinato de un joven de 16 años y el rapto de dos más en el corregimiento de El Mango, zona rural del municipio de Argelia, Cauca. Según la información de la comunidad, el joven fue asesinado en la plaza de mercado y por el momento se desconoce el paradero de los dos jóvenes que fueron privados de la libertad.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1307890478495551488","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1307890478495551488

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

El segundo hecho violento, fue el asesinato de la comunera indígena, Alba Alexandra Pizanda Cuestas, **en el corregimiento Puspued del municipio de Mallama, Nariño;** donde hombres armados arremetieron contra un vehículo en el que se movilizaba la lideresa indígena acabando con su vida. (Lea también: [Homicidios contra defensoras de DD.HH alcanzan el 91% de impunidad.](https://archivo.contagioradio.com/casos-homicidios-contra-defensoras-de-dd-hh-se-encuentran-en-un-91-de-impunidad/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":90182,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Comunicado-resguardo-indigena-el-gran-Mallama-777x1024.jpg){.wp-image-90182}  

<figcaption>
Comunicado Resguardo Indígena el Gran Mallama

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Cauca y Nariño dos de los departamentos más afectados por la violencia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el más reciente [informe](http://www.indepaz.org.co/wp-content/uploads/2020/09/Masacres-20-09-2020.pdf) del Instituto para el Desarrollo y la Paz -[Indepaz](http://www.indepaz.org.co/)-, **en Colombia se han registrado 61 masacres en lo que va del año 2020 en las cuales han sido asesinadas 248 personas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Cauca y Nariño son dos de los departamentos más afectados. Los dos departamentos registran un total de 9 masacres** y son superados únicamente por el departamento de Antioquia donde se han presentado 14.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
