Title: Muertes de civiles durante cese unilateral se han reducido en un 75%: CERAC
Date: 2015-11-18 18:17
Category: DDHH, Entrevistas
Tags: cerac, cese unilateral, David Correal, Diálogos de La Habana, dialogos de paz, Diálogos de paz en Colombia, FARC, investigador del CERAC
Slug: muertes-de-civiles-durante-cese-unilateral-se-han-reducido-en-un-75-cerac
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Confianza-ciudadana-proceso-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Mujeres Negras Caminan 

<iframe src="http://www.ivoox.com/player_ek_9434198_2_1.html?data=mpmglpadfI6ZmKiak5aJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdbZ09nS1ZDIqYzXytvWzsrXb8Xp08bb1sqPp8bnxpDi0M7QpdXZ08bZjdjJb8nVz5Dfx8nZp46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [David Correal, CERAC] 

###### [18 Nov 2015]

[De acuerdo con el último informe publicado por el CERAC los **4 meses** que lleva el **cese unilateral** por parte de la guerrilla de las FARC, para desescalar el conflicto y agilizar el actual proceso de negociaciones, muestran resultados favorables en la **reducción de la violencia asociada al conflicto** y en el **aumento de la confianza ciudadana frente al proceso de paz**.]

[David Correal, investigador del CERAC, asegura que los **niveles de violencia por cuenta de enfrentamientos directos entre las FFMM y las FARC han disminuido en niveles nunca antes vistos**, “lo que favorece la discusión frente a una eventual implementación de un cese al fuego bilateral”. Así mismo se han agilizado las negociaciones al punto de “tener **acuerdos parciales en puntos neurálgicos como son el tema de víctimas y justicia transicional**”.        ]

[La voluntad política de las **FARC** en el cumplimiento del cese unilateral se ha materializado en la comisión de “**sólo 5 acciones violatorias** que cuentan con evidencia documental para ser atribuidas a esta guerrilla”, lo que representa una **reducción de cerca del 88%** “comparado con el promedio de acciones realizadas durante todos los periodos de cese al fuego en los procesos de paz”, afirma Correal.]

[Desde el CERAC, hasta el momento no se tiene registro de que la fuerza pública haya lanzado bombas desde plataformas aéreas contra campamentos de las FARC, como compromiso acordado para el desescalamiento del conflicto; sin embargo en este periodo las **FARC han denunciado 5 bombardeos,** como si se ha podido corroborar desde la veeduría que se realiza desde el Frente Amplio por la Paz, quienes también han denunciado el fortalecimiento de las estructuras paramilitares en los territorios.]

[Según el informe del CERAC, las **muertes de civiles** durante este desescalamiento se han **reducido en un 75%** en comparación con el promedio diario registrado durante los ceses al fuego, en un 95% en periodos sin ceses al fuego y en un 92% en relación con el proceso de paz. Los **niveles de muertes de combatientes de la fuerza pública han reducido en 83%** y **las de guerrilleros en 65%**.]

[Estas tipo de medidas de desescalamiento han resultado ser positivas para imprimir un **mayor ritmo a las negociaciones**; contribuyendo a la construcción de **condiciones favorables para la implementación de un cese bilatereal**; sin embargo, es poco probable que las partes lleguen a un consenso en el corto plazo, dado que hacen falta la discusión de temas neurálgicos y difíciles para implementar esta iniciativa, concluye el informe.]
