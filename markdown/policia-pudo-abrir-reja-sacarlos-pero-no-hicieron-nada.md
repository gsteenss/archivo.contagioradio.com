Title: Estremecedor relato de hermana de joven incinerado en estación de Policía
Date: 2020-11-11 21:17
Author: AdminContagio
Category: Actualidad, DDHH
Tags: brutalidad policial, CAI, soacha
Slug: policia-pudo-abrir-reja-sacarlos-pero-no-hicieron-nada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Policía/ Bogota.gov

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que los hechos ocurrieron hace dos meses, solo hasta principios de noviembre se dio a conocer cómo 9 de 20 personas que permanecían en proceso de judicialización al interior del CAI de San Mateo fallecieron producto de un incendio al interior de la estación ante la impotencia de sus seres queridos y lo que sería un acto de negligencia por parte de la Policía Nacional que se habría negado a ayudar a los detenidos y dejar que el fuego se propagara.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Días antes de las movilizaciones del 9 y 10 de septiembre cuyo resultado fue el de más de 100 personas heridas y 14 personas que perdieron la vida fruto de la brutalidad policial, el pasado 4 de septiembre **Óscar Alejandro Infante Galindo,, Cristian Gilberto Rincón Caicedo, Juan David Rojas Ordóñez, Cristian Rincón, Anderson Stiven Méndez, Bernar Pinera Gaviria, Yeison Conte,** y un joven que aun no se ha logrado identificar perdieron la vida en medio de un incendio originado cuando se prendió fuego a una cobija en protesta por la negativa de las visitas de sus familiares y los tratos crueles que estaban sufriendo en su detención.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Le suplicamos a la Policía que nos ayudaran, que sacaran a nuestros familiares de ahí"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La hermana de uno de los jóvenes que murieron producto de las llamas, cuyo nombre reservamos, señala que su hermano había llegado hace una semana antes a la estación de San Mateo y que aquel 4 de septiembre se dirigió junto a su mamá al lugar para llevarle una encomienda. Aunque al principio se les negó la entrega por parte de la Policía, posteriormente se les autorizó; por su parte los reclusos desde dentro afirmaban que no les dejaran nada porque al interior la Policía botaría todo lo que entregaran.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Algunos minutos después - relata- comenzó a verse una llamarada que iba en aumento al interior de la celda, al ver el fuego, los familiares pidieron a la Policía que les ayudaran, sin embargo relatan que los agentes presentes respondieron "que se mueren esas gonorreas". [(Lea también:El aparato del Estado se revistió de legalidad para afectar la Movilización: Análisis)](https://archivo.contagioradio.com/aparato-estado-revistio-legalidad-afectar-movilizacion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que uno de los familiares al ver la inactividad de la Policía, rompió la ventana y quitó la reja para ayudar a las personas que se quemaban al interior, mientras otros familiares iban en busca de extintores y mangueras; pese a ello la Policía y los cerca de 20 integrantes que permanecían en el lugar les impidió prestar ayuda; tan solo cerca de media hora después decidieron abrir la reja, sin embargo ya era muy tarde.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Al ver a mi hermano él salió totalmente quemado, salté una barda que había ahí y lo primero que hizo fue la Policía fue sacarme a bolillazos, ellos pudieron abrir su reja y sacarlos para que no se quemaran y no hicieron nada", relata la familiar de una de las víctimas quien afirma que durante los sucesos las autoridades no hicieron nada para apagar el fuego

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aquel día murió uno de los reclusos, otros 11 resultaron heridos de gravedad y uno a uno fallecieron durante la semana siguiente, hasta ser un total de 9 víctimas fatales. Casi dos meses después, expresan sus familiares, "ni el alcalde ni el secretario de Gobierno, nadie nos ha dicho nada (...) ya dos meses y no salen a darnos la cara", pese a tener conocimiento de los hechos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/HeinerGaitan/status/1326538367610179589","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/HeinerGaitan/status/1326538367610179589

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

El **concejal [Diego Cancino](https://twitter.com/cancinodiegoa) quien dio a conocer la denuncia**, afirma que uno de sus principales dudas es por qué la Policía únicamente decidió referirse a los hechos casi dos meses después y no reportó en su momento la muerte de las 9 personas que fallecieron a raíz del fuego al interior de la estación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las familias han denunciado que al interior de la estación las personas que permanecían detenidas habían sido golpeadas el día anterior y no tenían acceso a comida. El concejal a su vez indicó que según un dictamen médico, en la incineración de uno de los jóvenes que se encontraba al interior de la estación de Policía de San Mateo hubo rastros gasolina.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Sobrevivientes del incendio en San Mateo permanecen en grave estado de salud

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a los sobrevivientes, familiares señalan que existen tres personas que permanecen en grave estado de salud, uno perdió una de sus piernas por las heridas mientras los dos restantes se encuentran en una UPJ, donde sostienen, no se les está brindando la atención médica necesaria para casos en los que se cuerpo tiene el 50% y 90% herido por quemaduras.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"No los querían atender porque es gente pobre, había habitantes de calle, y como quizá habían cometido irregularidades no los trataron como seres humanos, independientemente del delito, estamos en un Estado de derecho, aquí el sistema de salud también tiene que repensarse", afirma Cancino.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las familias han sido amenazadas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El concejal agrega que dos meses después de los hechos, los familiares no han querido hablar porque han sido amenazados, "de forma cotidiana, que si hablan las matan y la asesinan", relata, una situación que también ha sufrido uno de los sobrevivientes a quien han amedrentado para que no revele lo que sucedió aquel día.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El concejal de Soacha Heiner Gaitán, quien también denunció en su momento los hechos coincide con la versión y señala que el 7 de septiembre, tres días después, a medida que se informaba sobre deceso de algunos retenidos del CAI de San Mateo, apareció un panfleto de las Águilas Negras declarando como objetivo militar a liderazgos y procesos sociales de Soacha. [(Le puede interesar: Panfleto de Águilas Negras amenaza a integrantes de la Colombia Humana en Suacha)](https://archivo.contagioradio.com/panfleto-de-aguilas-negras-amenaza-a-integrantes-de-la-colombia-humana-en-suacha/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"De dónde vienen esas amenazas si aquí solo hay dos actores, los chicos que fueron asesinados y policías que parecen auspiciaron esta hoguera, si hay estos dos actores nos preguntamos (...) qué tienen que ver las Águilas Negras con la hoguera y el conflicto de quienes murieron," expresa Cancino.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cuál es el nivel de responsabilidad de la Policía?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que estos hechos se dan en medio de un contexto en el que se ha denunciado el reclutamiento forzado de jóvenes en Soacha por parte de grupos armados ilegales que aprovechan la falta de oportunidades y las necesidades de los jóvenes para engrosar sus filas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, como han denunciado organizaciones de DD.HH, sectores de la Fuerza Pública actúan en connivencia con las estructuras delincuenciales, permitiendo los negocios ilegales e incluso, siendo parte de algunos de ellos. Esto ante el negacionismo de autoridades locales que afirman que no hay estructuras armadas ilegales en la zona. [(Lea también: Reclutamiento forzado, pobreza e ineficacia oficial, las tres amenazas para jóvenes de Soacha)](https://archivo.contagioradio.com/reclutamiento-forzado-pobreza-e-ineficacia-oficial-las-tres-amenazas-para-jovenes-de-soacha/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Independientemente de la razón por la que se ocasionó el fuego, familiares se preguntan por qué la Policía no actuó a tiempo para impedir que las personas murieran adentro calcinadas. "¿Qué nivel de responsabilidad tiene la Policía, por qué los videos muestran que las madres estaban clamando por la vida de sus chicos y no se les permitía entrar", cuestiona el concejal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque los medios locales ya habían denunciado los hechos, en aquel entonces, la Policía sostuvo que se trató del accionar de personas retenidas que incendiaron elementos inflamables como colchonetas, almohadas y cobijas, no se calculó el efecto del fuego y la conflagración.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Nos preguntamos por qué había gasolina al interior de la estación y murieron por quemaduras de alto grado, y ¿por qué no fueron salvados? por qué si un chico prende fuego a un colchón, desesperado, después de ser torturado, de aguantar hambre, cuando prende fuego ¿por qué no lo apagan y dejan que ese fuego crezca tanto?". [(Lea también: \[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones)](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cancino apunta a que es necesaria una reforma estructural de la Policía y saber qué rol juega en el municipio, **"necesitamos que el Gobierno asuma, tal como se lo ordenó la Corte Suprema de Justicia que estamos ante una práctica sistemática y que existen responsabilidades de mando".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, la Procuraduría General manifestó que investigará los hechos, pues la denuncia apunta a que el número de víctimas del hecho está relacionado a la falta de cooperación de los uniformados de la estación. “La Procuraduría busca establecer las acciones desplegadas por los uniformados una vez iniciado el incendio, y si son ciertas las denuncias de que en el lugar permanecían 20 jóvenes, quienes protestaron porque se les habría negado la posibilidad de tener una visita familiar".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
