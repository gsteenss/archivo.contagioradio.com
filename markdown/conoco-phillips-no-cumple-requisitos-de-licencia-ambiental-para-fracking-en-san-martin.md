Title: Conoco Phillips no cumple requisitos de Licencia Ambiental para Fracking en San Martín
Date: 2016-11-02 16:53
Category: Ambiente, Nacional
Tags: Abuso de fuerza ESMAD, Fracking colombia, san martin
Slug: conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/San-Martin.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CORDATEC] 

###### [2 Nov de 2016] 

En respuesta a las denuncias radicadas el pasado 9 de Septiembre por el Colectivo de Abogados Luis Carlos Pérez frente a las actividades de Fracking en San Martín, la Contraloría delegada para el medio ambiente, emitió el 1 de Noviembre una respuesta de fondo, en la que informa que **"se** **ordena traslado a la Procuraduría General de la Nación de los hallazgos con incidencia disciplinaria encontrados en contra de la ANLA".**

La respuesta de la Contraloría, devela que “existió una negligente actuación frente actividades del Contrato de Exploración y Producción, E&P Yacimientos No Convencionales del que son titulares las empresas ConocoPhillips y CNE Oil & Gas S.A.”. Le puede interesar: [Gases y aturdidoras del ESMAD contra pobladores de San Martín.](https://archivo.contagioradio.com/gases-aturdidoras-del-esmad-pobladores-san-martin/)

Conforme a los hechos expuestos en la denuncia del CCLCP, el ente de control, encuentra que en efecto **“existió una desatención de las funciones de la Autoridad Ambiental, al no haber exigido a la empresas titulares del contrato, la modificación de la licencia ambiental** para las actividades en el Pozo Picoplata 1 de la vereda Cuatro Bocas del municipio de San Martin”.

Según un comunicado de prensa del Colectivo, esto deja en evidencia **“la improvisación y  gran falta de preparación técnica y legal que se tiene en el país para introducir e imponer la dañina técnica del Fracking”** que viene siendo empleada para la extracción de yacimientos no convencionales, “sólo por sus comprobados efectos perjudiciales debe ser prohibida”.

El Colectivo y las comunidades de San Martín, reciben de forma positiva esta acción, sin embargo, son enfáticos en responsabilizar y exigir al Estado Colombiano para **“que se continúen las investigaciones y se adopten las medidas y sanciones,** en relación con el resto de denuncias y quejas por las irregularidades presentadas por parte del actuar de la ANH".

También, reiteran “un llamado urgente a que se investiguen, cesen y se sancionen las recientes **graves violaciones a los Derechos Humanos perpetradas en contra de la comunidad de San Martin y defensores/as de DDHH por parte de la Fuerza Pública** a favor de la imposición violenta, abusiva e ilegal de este nocivo proyecto”. Le puede interesar: [Fuerte represión del ESMAD contra comunidad de San Martín que se opone al fracking.](https://archivo.contagioradio.com/fuerte-represion-del-esmad-contra-comunidad-de-san-martin-que-se-opone-al-fracking/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
