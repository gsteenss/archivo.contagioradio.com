Title: La estrategia mediática de los implicados en el caso Uribe Vélez
Date: 2020-08-10 21:05
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Uribe, Caso Uribe, Corte Suprema de Justicia, Diego Cadena, Julio Sánchez Cristo
Slug: la-estrategia-mediatica-de-los-implicados-en-el-caso-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diego-Cadena-y-Julio-Sanchez-Cristo-fl.-841.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diego-Cadena-y-Julio-Sanchez-Cristo-fl.-843.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diego-Cadena-y-Germán-Duque-fl.-835.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diego-Cadena-y-Garmán-Duque-fl.-835.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Julio-Sánchez-Cristo-mencionado-en-el-caso-Uribe.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras conocerse el voluminoso auto mediante el cual la Corte Suprema de Justicia ordenó la imposición de medida de aseguramiento en contra del expresidente Álvaro Uribe Vélez, han quedado al descubierto varias conversaciones producto de las interceptaciones a las líneas telefónicas de los implicados. En un acápite de la decisión **la Corte Suprema hace énfasis en el manejo que hacían, los implicados en el caso Uribe, de los medios de comunicación para favorecer sus intereses y orientar la opinión.** (Le puede interesar: [«Duque y su Partido buscan presionar a la Corte Suprema en caso de Álvaro Uribe»](https://archivo.contagioradio.com/duque-y-su-partido-buscan-presionar-a-la-corte-suprema-en-caso-de-alvaro-uribe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Uno de los audios transcritos es entre el abogado del expresidente Uribe, Diego Cadena y Julio Sánchez Cristo, director de la emisora W Radio.** En la transcripción llama la atención el lenguaje cercano que emplea el periodista para referirse al abogado tratándolo de «*mijo*» y «*hermano*». (Le puede interesar: [Razones por las que Diego Cadena, abogado de Álvaro Uribe, será imputado por la Fiscalía](https://archivo.contagioradio.com/razones-por-las-que-diego-cadena-abogado-de-alvaro-uribe-sera-imputado-por-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":88058,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diego-Cadena-y-Julio-Sanchez-Cristo-fl.-841.png){.wp-image-88058}  

<figcaption>
Extracto Auto-2020 de la Corte Suprema de Justicia (Folio 841)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Una vez es realizada la entrevista al aire en W Radio, en la que no se encontraba Sánchez Cristo, el abogado Cadena lo indaga para saber si este escuchó la entrevista a lo que el periodista responde que «*no*» pero le replica preguntándole «*¿cómo salió la cosa?*» y una vez Cadena le responde, **le pregunta «*¿qué dirá el presidente?*» al parecer refiriéndose a Álvaro Uribe.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":88059,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diego-Cadena-y-Julio-Sanchez-Cristo-fl.-843.png){.wp-image-88059}  

<figcaption>
Extracto Auto-2020 de la Corte Suprema de Justicia (Folio 843)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Frente a esto, también apuntó la Corte que: **«*****llama la atención que en la realización de la entrevista tiene manifiesto interés el senador ÁLVARO URIBE VÉLEZ, en la medida que insistentemente le dice a través de mensajes de texto a Cadena que hable con Julio que es amable con él;*** *igualmente es llamativo que pese a que el medio periodístico le dice a Diego Cadena que están dispuestos en el acto a realizar la entrevista al testigo Pardo Hasche, refiera Diego Cadena que no, que “él dice que la quiere preparar, organizar, tener todo organizado” y solo una semana después es que al parecer Enrique Pardo Hasche logra organizar sus ideas y llamar al medio periodístico que está atento a recibirle su relato».*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho ha generado varias reacciones en redes sociales, entre ellas, las de los también periodistas[Julián Martínez](https://twitter.com/JulianFMartinez) y [Gonzalo Guillén](https://twitter.com/HELIODOPTERO).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JulianFMartinez/status/1292335426175488000","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JulianFMartinez/status/1292335426175488000

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/HELIODOPTERO/status/1292289452057010177","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/HELIODOPTERO/status/1292289452057010177

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Julio Sánchez Cristo no es el único periodista mencionado por la Corte en el caso Uribe

<!-- /wp:heading -->

<!-- wp:paragraph -->

En las transcripciones de las llamadas, también se evidencia el interés del abogado Cadena para que el medio televisivo RCN, tal como en el caso de W Radio, entrevistara a Enrique Pardo Hasche.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de las gestiones adelantadas por Cadena, Pardo Hasche concede la entrevista a RCN el 18 abril del año 2018 pero **el abogado Cadena le pide al entrevistador Germán Duque que lo mantenga a él «en la sombra» y que retrase la publicación de la entrevista hasta que Pardo Hasche autorice para «*ver si la sacamos*» argumentando que está «*muy nervioso*».**

<!-- /wp:paragraph -->

<!-- wp:image {"id":88062,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diego-Cadena-y-Germán-Duque-fl.-835.png){.wp-image-88062}

</figure>
<!-- /wp:image -->

<!-- wp:image {"id":88063,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Diego-Cadena-y-Garmán-Duque-fl.-835.png){.wp-image-88063}  

<figcaption>
Extracto Auto-2020 de la Corte Suprema de Justicia (Folio 835)

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Sin embargo, **solo es hasta el 27 de julio de 2018, es decir más de dos meses después que Cadena y Pardo Hasche autorizan la publicación de la entrevista en RCN**; lo cual según la Corte se hizo «*estratégicamente*» para que saliera «*tres días después de haber proferido la Sala de instrucción N°2 apertura formal de la investigación en contra de Álvaro Uribe Vélez*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la Corte **«*La estrategia con los medios periodísticos adelantada por Diego Cadena Ramírez con el consentimiento del senador continuó una vez se reactivó la investigación y se señaló fecha para escuchar en indagatoria a Álvaro Uribe Vélez*».** (Lea también: [“Responsabilidad de Álvaro Uribe en el caso de Diego Cadena es innegable” - Iván Cepeda](https://archivo.contagioradio.com/alvaro-uribe-tiene-responsabilidad-innegable-en-caso-diego-cadena/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
