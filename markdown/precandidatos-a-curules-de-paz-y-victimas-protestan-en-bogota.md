Title: Precandidatos a Curules de paz y víctimas protestan en Bogotá
Date: 2017-12-21 12:18
Category: DDHH, Nacional
Tags: Circunscripciones de paz, curules de paz, participación política, víctimas del conflicto armado
Slug: precandidatos-a-curules-de-paz-y-victimas-protestan-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/victimas-movice-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [21 Dic 2017] 

Precandidatos a las curules de paz y víctimas del conflicto armado se reunieron en Bogotá para realizar un plantón en la Plaza de Bolívar para exigirle al Gobierno Nacional que se garantice, desde las instituciones, su **derecho a la participación política**. Harán saber que las víctimas no han sido el centro de lo acordado en la medida que no se ha garantizado la verdad, la justicia y la no repetición.

De acuerdo con Gloria López, pre candidata de las circunscripciones especiales de paz, las víctimas del conflicto armado están exigiendo que **se cumpla lo que quedó pactado en el acuerdo** de la Habana para la terminación del conflicto armado, “en donde se decía que las víctimas serían el centro de estos acuerdos”.

Teniendo en cuenta el incremento en el número de asesinatos a líderes sociales y los movimientos políticos en el Congreso de la República en lo que tiene que ver con las Circunscripciones Especiales de Paz, las víctimas se han visto en la necesidad de protestar argumentando que han sido **“relegadas en el tema de la participación política”**. (Le puede interesar: ["Denuncian a presidente del senado por prevaricato en circunscripciones de paz"](https://archivo.contagioradio.com/denuncian-a-presidente-senado-por-supuesto-prevaricato-en-circunscripciones-de-paz/))

En el plantón, se reunieron víctimas que viajaron desde Medellín y Chocó e indicaron que “a través de símbolos vamos a demostrar que merecemos que se **nos dé a oportunidad de tener representación política** en el país”. López indicó que, “los territorios abandonados tenían esperanza de poder visibilizar las necesidades y crear alternativas para la población campesina”.

Finalmente, las víctimas han extendido la invitación para participar en el plantón en Bogotá. Manifestaron que es importante **que haya un apoyo a las víctimas** quienes han dicho en varias ocasiones que están presentes en el país y que “no vamos a dejar que otros tomen las decisiones por nosotros argumentando que no tenemos la capacidad de participar y promover acciones políticas que hemos venido desarrollando”.

<iframe id="audio_22785962" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22785962_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
