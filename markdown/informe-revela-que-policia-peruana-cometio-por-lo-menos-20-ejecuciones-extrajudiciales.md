Title: Policía de Perú conformó un "escuadrón de la muerte" para asesinar jóvenes a cambio de ascensos
Date: 2016-08-23 16:33
Category: DDHH, El mundo
Tags: crímenes de estado, Ejecuciones Extrajudiciales, Perú
Slug: informe-revela-que-policia-peruana-cometio-por-lo-menos-20-ejecuciones-extrajudiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Policía-Perú.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Canaln] 

###### [23 Ago 2016] 

Un informe del Ministerio del Interior de Perú, presentado por el viceministro de Orden Interno Rubén Vargas, revela indicios sobre la creación de un 'escuadrón de la muerte' por parte un comandante y siete suboficiales de la Policía peruana quienes cometieron por lo menos **veinte ejecuciones extrajudiciales entre 2012 y 2014** con el fin de obtener ascensos.

De acuerdo con el informe, los policías pertenecientes al Grupo Especial de Inteligencia contra el Crimen Organizado, de la Dirección Contra el Terrorismo, pusieron en marcha los **[operativos irregulares](https://archivo.contagioradio.com/protestas-en-contra-de-proyecto-minero-en-peru-dejan-cuatro-muertos/) en los que murieron veinte ciudadanos** que fueron presentados como supuestos delincuentes, como lo habían denunciado varios medios de información locales.

El viceministro aseguró que las ejecuciones se basaron en [información de inteligencia](https://archivo.contagioradio.com/llegada-de-3200-militares-estadounidenses-a-peru-atenta-contra-la-soberania-de-ese-pais/) que fue alterada por los miembros del Grupo Especial, quienes describieron hechos irreales o hicieron referencia a **personas que no reunían los antecedentes criminales consignados**, como ocurrió con once de los veinte ciudadanos que fueron abatidos, uno de los cuales fue acusado de secuestro sin estar en el lugar en el que ocurrió el hecho.

El representante del ejecutivo aseguró que las unidades policiales investigadas por estos hechos serían por lo menos 100, entre ellos 16 oficiales y 80 suboficiales, quienes habrían cometido las ejecuciones bajo el liderazgo del comandante **Enrique Prado Ravines** entre los años 2011 y 2015, con el objetivo de obtener ascensos y distinciones policiales.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
