Title: La educación: el camino a la resocialización
Date: 2019-08-21 10:51
Author: CtgAdm
Category: Expreso Libertad
Tags: carceles, educacion, prisioneros, Resocializacion
Slug: la-educacion-el-camino-a-la-resocializacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Untitled-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Desde hace un par de meses, algunos docentes de la Universidad Nacional han puesto en marcha un plan para lograr que sus alumnos víctimas de falsos positivos judiciales, continúen con sus estudios estando en prisión.

<div>

Entre estos profesores se encuentran Miguel Ángel Beltrán y Rosemberg Arias, ambos docentes adscritos al departamento de Sociología de esa universidad. Para ellos esta apuesta no solo ha significado garantizar el derecho a la educación que tienen las personas que se encuentran en las cárceles, sino dar una verdadera respuesta a cómo generar una resocialización efectiva.

</div>

<div>

</div>

<div>

\[embed\]https://www.facebook.com/contagioradio/videos/2312180192367609/\[/embed\]

</div>
