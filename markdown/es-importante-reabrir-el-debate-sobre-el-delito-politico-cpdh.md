Title: “Es importante reabrir el debate sobre el delito político” CPDH
Date: 2014-12-12 14:24
Author: CtgAdm
Category: DDHH, Judicial
Slug: es-importante-reabrir-el-debate-sobre-el-delito-politico-cpdh
Status: published

Así lo afirmó el abogado director del Comité Permanente por la Defensa de los DDHH, Diego Martínez. Según expone, como consecuencia de una demanda de inconstitucionalidad presentada por el General Bedoya, y la resultante sentencia C 456 de 1996 de la Corte Constitucional, en la práctica desapareció o desnaturalizó el **Delito Político en Colombia, al punto en que llegó a ser casi imposible argumentar ante tribunales que las conductas de imputados corresponden a este concepto.**

**El delito político, reconocido desde el año 1200**, en el derecho canónico, concibe el accionar que tiene como fin el derrocamiento del régimen vigente y **”ejecutar esa conducta no es un tema de ángeles”**, señala Martínez. Este debate no sólo abriría la puerta al tema de la financiación de la lucha guerrillera por medio del narcotráfico, sino otro tipo de actuaciones en el marco del conflicto político, social y armado, según expone el abogado.

Ante los temores que exponen quienes consideran que en este marco normativo podría incluirse a narcotraficantes no vinculados con organizaciones guerrilleras, Martínez explica que no se trata sólo de buscar amnistías para quienes participen en la conducta, sino de demostrar como dichas acciones no tuvieron un fin en sí mismo, sino que fueron herramientas y en este sentido se relacionaron con la rebelión.
