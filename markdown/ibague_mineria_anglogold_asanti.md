Title: Ibagué le cierra las puertas a la minería de AngloGold Ashanti
Date: 2017-08-01 19:27
Category: Ambiente, Entrevistas
Tags: Anglo Gold Ashanti, consultas populares, Ibagué, Mineria
Slug: ibague_mineria_anglogold_asanti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las2Orillas 

###### [1 Ago 2017] 

“**Los 14 concejales tenían claro el panorama, tenían total claridad de que tenían la facultad de tomar esta decisión**”, expresa el abogado Rodrigo Negrete, con respecto a la votación de los cabildantes que le impidieron la entrada a las empresas mineras a mediana y  gran escala como AngloGold Ashanti.

Se trata del Acuerdo número 012 “Por medio del cual se dictan medidas para la preservación y defensa del patrimonio ecológico y cultural del municipio de Ibagué y se dictan otras disposiciones”, dice el texto que si permite la minería artesanal y la necesaria para extraer materiales de construcción. (Le puede interesar:[Ibagué se convierte en la primera ciudad capital de Colombia en prohibir la minería)](https://archivo.contagioradio.com/ibague-se-convierte-en-la-primera-ciudad-capital-en-prohibir-la-mineria/)

En Ibagué hay muchos títulos mineros con licencia ambiental para sacar materiales de construcción,  estos, por efectos de las sentencias de la Corte Constitucional no se verían afectados. Además, explica Negrete, al ser una ciudad capital, “**se comprende que hay una tipo de minería necesaria para el municipio”**, y agrega, “los que ahora están prohibidos, serían todos aquellos que no cuenten con licencia ambiental”, por ejemplo  los de AngloGold Ashanti.

Impulsado por el alcalde de Ibagué, Guillermo Alfonso Jaramillo, pero ideado por la comunidad, **se trata de uno de los mecanismos contemplados en leyes, sentencias y en la propia Constitución Política** que le dan la capacidad a los alcaldes, concejos municipales y a la población de impedir el desarrollo de proyectos mineros. Es decir las consultas populares, los planes de ordenamiento territorial, y los acuerdos municipales, este último, es el caso de la capital tolimense.

### Las reacciones 

Mientras lideres ambientales como Renzo García, integrante del Comité Ambiental celebran la noticia recordando que “todo el sector minero de Ibagué pagó 23 millones en el año 2016 de impuestos”, las empresas **mineras de esa región del país han rechazado** la decisión del **Concejo de Ibagué** señalando que este tipo de iniciativas atentan contra los intereses de los empresarios formales y que cumplen con el **ordenamiento jurídico existente.**

Por el momento, los ambientalistas y en general las comunidades siguen reiterando en que esta situación evidencia la necesidad de que el gobierno empiece a reaccionar frente a las reclamaciones de los territorios, de manera que “se debe declarar una moratoria contra la minería y llegar a consensos con los comunidades”, concluye el abogado Negrete. (Le puede interesar: ["Consulta popular minera busca frenar 35 títulos mineros en Ibagué"](https://archivo.contagioradio.com/consulta-popular-en-ibague-busca-frenar-35-titulos-mineros/))

<iframe id="audio_20130130" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20130130_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
