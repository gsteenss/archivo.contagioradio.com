Title: Desminado se estaría haciendo en zonas de concesión minera
Date: 2017-02-20 17:35
Category: DDHH, Nacional
Tags: desminado, Zidres
Slug: desminado-se-estaria-haciendo-en-zonas-de-concesion-minera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/despojo-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diario La Piragua] 

###### [20 Feb 2017] 

Pese a que el desminado es uno de los puntos para garantizar la paz en los territorios pactado en los Acuerdos de Paz de La Habana, las comunidades de Montelibano y Puerto Libertador, en Córdoba, denuncian que se ha priorizado el **desminado en lugares que estarían en concesión minera por multinacionales y no en las zonas que privilegien la habitabilidad y vivencia de las mismas.**

Sumado a esta situación, se encuentran los asentamientos de resguardos indígenas y caseríos de campesinos que están en lo que se ha denominado como territorios baldíos, que de acuerdo con la ley ZIDRES, **entrarían a ser disputadas por grandes empresas para poder adquirirlos, obligando a las personas que están en el lugar a abandonarlo.** Le puede interesar: ["Exequibilidad de ley ZIDRES atenta contra Banco de Tierras por la Paz"](https://archivo.contagioradio.com/exequibilidad-de-ley-zidres-atenta-contra-banco-de-tierras-por-la-paz/)

Para los pobladores que se estén priorizando estas zonas **hace parte de intereses de multinacionales para consolidar un plan de extracción** que incluye la construcción de un puerto privado y un complejo agroindustrial en el Urabá Antioqueño y la consecución del proyecto hidroeléctrico URRA2 o Río Sinú.

Finalmente, el colectivo Raíz y Memoria Sergio Restrepo Jaramillo, que esta en el lugar añade que **el recrudecimiento de la presencia de paramilitares en esta zona buscaría el desplazamiento de los pobladores** y que se efectúe de forma mucho más rápida la explotación por parte de las multinacionales. Le puede interesar: ["Paramilitares arremeten en diferentes regiones del país"](https://archivo.contagioradio.com/paramilitares-arremeten-contra-diferentes-regiones-del-pais/)
