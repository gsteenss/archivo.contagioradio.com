Title: En Paujil Caquetá habrá Consulta Popular
Date: 2017-01-13 10:51
Category: Ambiente, Nacional
Tags: consulta popular, Ecopetrol, Emerald Energy, No más Fracking, Paujil Caquetá
Slug: en-paujil-caqueta-habra-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/caqueta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: The City Paper] 

###### [13 Enero 2017] 

Las comunidades del municipio de Paujil Caquetá, recibieron el año nuevo con una buena noticia, la Registraduría Nacional aprobó la solicitud de inscripción de la Consulta Popular denominada **‘Por la Dignidad Amazónica, el Paujil lucha por el territorio, el agua y la vida’,** la cual busca suspender las actividades de sísmica adelantadas allí por Emerald Energy y Ecopetrol.

[Son 43 los bloques petroleros que planean ponerse en marcha en el Caquetá](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/) según la Agencia Nacional de Hidrocarburos, de los cuales los puestos en marcha en los municipios de Doncello, Puerto Rico, Paujil y San Vicente, **afectaron ecosistemas productores de agua  en una de las zonas hídricas más importantes del país,** la cual representa el 18.67% de la región Amazónica colombiana.

Martin Trujillo vocero de la iniciativa ciudadana, aseguró que el próximo lunes el grupo impulsor iniciará la recolección de firmas. **El equipo deberá recoger el 20% de 11.865 personas registradas ante el censo electoral, es decir, 2.373 firmas,** Trujillo señaló que de lograr dicho porcentaje en menos de 20 días, la Registraduría deberá pronunciarse, de lo contrario el trámite tardaría poco más de 6 meses.

“Nuestra idea es recoger más de 5 mil firmas, estamos seguros que podemos lograrlo porque la comunidad no quiere más daños en el territorio” manifestó el vocero. Le puede interesar: [En Caquetá continuarán las movilizaciones hasta que se retiren las petroleras](https://archivo.contagioradio.com/en-caqueta-continuaran-las-movilizaciones-hasta-que-se-retiren-las-petroleras/).

Trujillo indicó que desde septiembre de 2015 se conformó la Mesa Multidisciplinaria del municipio, la cual **realizó las consultas en las distintas veredas y finalmente el 9 de noviembre de 2016, realiza la inscripción ante la Registraduría** cumpliendo los requisitos dispuestos en la ley 1757 de 2015, que dispone lo necesario para el mecanismo de Consulta Popular.

### Movilizaciones para retirar a las petroleras 

Durante septiembre de 2016, las comunidades de Paujil y El Doncello, mantuvieron protestas y bloqueos en vías principales para exigir la suspensión de actividades y la salida de las multinacionales del territorio. Como respuesta obtuvieron fuertes agresiones por parte del ESMAD, [dejando como saldo decenas de heridos, detenidos y fincas destrozadas.](https://archivo.contagioradio.com/esmad-destruye-tres-fincas-en-medio-de-movilizacion-en-caqueta/)

### Sísmica inconsulta 

El vocero reveló que Emerald Energy y Ecopetrol, llevan más de 5 años adelantando explotaciones y exploraciones en la región, “en todo ese tiempo, nunca han socializado ni consultado con las comunidades, así pasó con la sísmica, **la comunidad supo cuando se empezaron a escuchar los ruidos y porque algunos trabajadores nos explicaron, pero la empresa como tal nunca socializó ni preguntó”.**

Martin Trujillo denunció que frente a la decisión de la comunidad de realizar la Consulta, “Emerald anunció que haría una inversión del 1% supuestamente para resarcir los impactos, eso son unos **\$15.000.000 por vereda, y los entregan en estufas ecológicas de leña, pero nosotros nos preguntamos si eso realmente es resarcir los daños”.**

### 

Por ultimo, Trujillo hace un llamado al Gobierno Nacional, Departamental y Municipal para que se hagan cambios sustanciales en los planes de ordenamiento territorial y se asignen recursos para fortalecer las iniciativas ciudadanas. También invita a **organizaciones sociales y ambientales para que acompañen el proceso de Consulta en Paujil y difundan a través de sus redes el desarrollo de la iniciativa.**

###### Reciba toda la información de Contagio Radio en [[su correo]
