Title: En Quibdó han sido asesinados 97 jóvenes en 2020
Date: 2020-08-22 11:58
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinato de jóvenes, Chocó, Quibdó
Slug: en-quibdo-han-sido-asesinados-97-jovenes-en-2020
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/quibdo-manifestacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

[Foto: Marcha Quibdó]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque las masacres y asesinatos contra jóvenes registrados en menos de una semana y que han dejando a cinco adolescentes sin vida en Cali y ocho jóvenes asesinados en Samaniego han puesto los ojos del país en departamentos como Valle y Nariño, la realidad también se vive en otros sectores del pacífico como Quibdó, capital de Chocó que enfrenta una ola de violencia que en lo que va corrido del 2020 ha cobrado la vida de 97 jóvenes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Darwin Lozano, abogado y veedor ciudadano al cierre de esta nota y según los registros que ha realizado **son un total de 97 los homicidios ocurridos contra población joven en el territorio que habitan cerca de 116.000 personas.** [(Le puede interesar: Hace 65 años Gabo escribió sobre el abandono al Chocó)](https://archivo.contagioradio.com/hace-62-anos-gabo-escribio-sobre-el-abandono-al-choco/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El veedor atribuye esta ola de violencia a una guerra por el territorio entre bandas que identifica como **los mexicanos y el Clan del Golfo o autodenominadas Autodefensas Gaitanistas de Colombia.** Al respecto medios han advertido que los integrantes de los carteles mexicanos estarían llegando al pacífico realizando una escala en Ecuador para luego ingresar al país a través de Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Resalta, que lo verdaderamente preocupante es la actitud de la Fuerza Pública de quienes señala se limitan a reconocer el incremento en la tasa de homicidios de la ciudad, sin embargo parece "como si fuesen cómplices de estos grupos porque no hacen nada". En el Chocó, hace presencia la [Séptima División del Ejército](https://twitter.com/Ejercito_Div7)que también tiene jurisdicción en Antioquia, Córdoba y Sucre, **además Fuerza de Tarea Titán que con más de 2.500 hombres debe velar por la protección de 22 municipios, con especial énfasis en el Chocó.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque a finales de julio se registraron enfrentamientos entre la Fuerza Pública y las autodenominadas Autodefensas Gaitanistas de Colombia en la zona rural de Quibdó. Este grupo armado opera a través de redes con pandillas y bandas locales vinculadas a control social, extorsión. sicariato y narcotráfico, además acuden al reclutamiento de menores de edad, homicidios selectivos y desapariciones. [(Lea también: En el Bajo Baudó fue asesinado el profesor Rubilio Papelito)](https://archivo.contagioradio.com/en-el-bajo-baudo-fue-asesinado-el-profesor-rubilio-papelito/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que en el departamento del Chocó también operan frentes del ELN que han expandido su presencia y control, grupos como las AGC también continúan consolidando su accionar armado en el territorio y en ciudades como Quibdó, así lo señala la Defensoría del Pueblo en su alerta temprana de junio del año pasado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Reclutamiento y desaparición forzada también se viven en Quibdó

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de esta situación, Lozano advierte que también se está dando una situación de reclutamiento a menores, "cuando no hay oportunidades, donde la corrupción es el pan de cada de día y hay una población nuestros jóvenes quedan presos de la violencia y de estos grupos y terminan en ese camino erróneo en alzarse en armas". [(Lea también: La estrategia del paramilitarismo para atraer a jóvenes del Chocó a sus filas)](https://archivo.contagioradio.com/la-estrategia-del-paramilitarismo-para-atraer-a-jovenes-del-choco-a-sus-filas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Coalico en el último año, el departamento con mayor registro de reclutamiento a menores es Antioquia con 40 casos, seguido por Chocó con 34, Norte de Santander con 26, Valle del Cauca con 22 y Cauca con 19. [(Lea también: Antioquia y Chocó registran el mayor número de reclutamiento a menores)](https://archivo.contagioradio.com/antioquia-y-choco-registran-el-mayor-numero-de-reclutamiento-a-menores/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además del reclutamiento también asegura que están ocurriendo desapariciones de jóvenes que aunque se estiman que también fueron asesinados, como el caso de **Carlos Andrés Zúñiga Perea, de 17 años, de quien se denunció su desaparición desde la mañana del miércoles 29 de abril de 2020 y fue hallado el domingo 3 de mayo sin vida. "**Este año hemos documentado cada homicidio que sucede para confrontar cualquier información que venga de las autoridades o de Medicina Legal, se trata de un promedio de jóvenes entre los 15 y los 28 años", explica.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado alerta que estos sucesos se están dando en el caso urbano del municipio casi a diario y lo que más le preocupa es la estigmatización que se hace de las víctimas a quienes a menudo señalan de "andar en malos pasoso, pero la población ha demostrado que los jóvenes que han caído en esta ola de violencia no han tenido nada que ver con estos grupos, que dicen ser los responsables". [(Lea también: Con derecho de petición, Ejército exige a líderes del Chocó demostrar lo que denuncian)](https://archivo.contagioradio.com/derecho-de-peticion-ejercito-exige-que-lideres-demostrar-denuncias/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
