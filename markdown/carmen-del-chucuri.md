Title: Comunidades de Carmen del Chucurí en alerta por posibilidad de Fraking en su territorio
Date: 2017-09-01 15:41
Category: Ambiente, Nacional
Tags: Carmen del Chucuri, Ecopetrol, fracking, Parex, petroleo, Santander
Slug: carmen-del-chucuri
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/fracking-santander-e1504298360341.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Equipo Jurídico Pueblos] 

###### [01 Sept 2017] 

Las comunidades de San Vicente y el Carmen del Chucurí en Santander **sentaron su voz de protesta ante los riesgos del proyecto de fracking APE Marteja**, adelantado por Ecopetrol y la empresa canadiense Parex Resources. Ellos y ellas buscarán proteger los derechos colectivos a un ambiente sano y la protección del territorio.

Mauricio Gómez, líder social e integrante de la Asociación Campesina de la Serranía de los Yareguies (ASOCAMPESEYA) afirmó que **“estas empresas quieren hacer perforaciones a grandes profundidades** de más de 20 mil pies utilizando 3 litros de agua por segundo que sirven para abastecer a 10 familias por mes”.

Este proyecto de fracking **se suma a las actividades pasadas de extracción de hidrocarburos** que han perjudicado a las comunidades. Han dicho que estas empresas pretenden contaminar ríos y quebradas “con el argumento de que son solo nacederos de agua”. (Le puede interesar: ["Campesinos de Simacota se oponen a destrucción que ocasionaría empresa Parex"](https://archivo.contagioradio.com/comunidades-se-oponen-a-exploracion-petrolera-en-simacota-santander/))

Gómez indicó que en 2006, uno de los proyectos de Ecopetrol **ocasionó la destrucción de gran parte de la vereda Payoa** a 22 kilometros del municipio Sabana de Torres, “cuando fue cerrado el pozo la acumulación de gas que quedó bajo la tierra ocasionó una gran explosión y derrumbó aproximadamente 20 viviendas”.

Cabe recordar que la empresa Parex Resources **ya hace presencia en el municipio de Simacota también en Santander.** Allí las comunidades han denunciado una constante contaminación de los afluentes hídricos, no tienen acueducto y la empresa ha creado líneas eléctricas para realizar actividad petrolera. (Le puede interesar: ["La polémica detrás del proyecto de Ecopetrol en Guamal, Meta"](https://archivo.contagioradio.com/la-polemica-detras-del-proyecto-ecopetrol-guamal-meta/))

En la audiencia que se realizó hoy está citada la empresa Ecopetrol y los habitantes de los municipios afectados **esperan que la discusión sirva para que no se dé la licencia ambiental** “para frenar el grave daño que le quieren hacer a las comunidades”.

<iframe id="audio_20647148" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20647148_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
