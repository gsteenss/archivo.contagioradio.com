Title: Más de 1400 presos políticos de las FARC continúan en huelga de hambre
Date: 2017-06-29 14:04
Category: DDHH, Nacional
Tags: FARC, Ley de Amnistia, presos politicos, presos políticos de las FARC
Slug: presos-politicos-de-las-farc-mantienen-la-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/libertad-para-los-presos-politicos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Jun 2017]

En un plantón realizado frente a los Juzgados de Ejecución de Penas y Medidas de Seguridad en varias ciudades, personas  y organizaciones sociales continúan **reclamando que se cumpla la ley de amnistía e indulto para los 2577 presos políticos de las FARC,  1400** de ellos** **completan cuatro días de huelga de hambre

**Jesús Santrich, integrante del Estado mayor de las FARC** y quien también se encuentra en huelga de hambre,  manifestó que a parte de la crisis humanitaria carcelaria que viven los presos en general en el país, los presos políticos de las FARC **“se encuentran en una situación peor porque no se les ha cumplido las normas que se derivan del acuerdo de paz”**. Santrich afirmó que los ex combatientes se encuentran todavía detenidos cuando deberían haber salido de los centros penitenciarios desde enero. (Le puede interesar: ["Presos políticos de las FARC exigen que se cumpla ley de amnistía pactada"](https://archivo.contagioradio.com/presos-politicos-de-las-farc-exigen-que-se-cumpla-la-ley-de-amnistia-de-los-acuerdos/))

<iframe src="http://co.ivoox.com/es/player_ek_19544969_2_1.html?data=kp6ilpmdepqhhpywj5WWaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncavZ1IqwlYqmhdSftMbb1tfNp8mZk6iY1dTGtsafydrSzszFb8XZjM3Oz8fWqYzYxpDd1M7XrdDixtfc1ZCRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para las FARC, el gobierno y los jueces de ejecución de penas han puesto trabas para evitar que estas personas sean dejadas en libertad. “Ellos y ellas no han salido por que hay muchas demoras, negligencia y faltan las firmas de las certificaciones. **Los jueces de ejecución de penas se han convertido en liberticidas y mercenarios contra la paz**”.

**70% de los prisioneros políticos de las FARC continúan en la cárcel**

<iframe src="http://co.ivoox.com/es/player_ek_19545269_2_1.html?data=kp6ilpqWepqhhpywj5WbaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncavj1IqwlYqlfYzOwtLc1MaPt9DW08qYy9LUsMbhxtPhw8jNaaSnhqeg0JDIqYzVztPW1dmJh5SZoqnOjdXFtsKf0deah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

José Marbel Zamora es uno de los 800 amnistiados de las FARC que hay hasta el momento. En el plantón realizado en Bogotá,  Zamora manifestó que **“debe haber actos de reciprocidad en este contexto de implementación de los acuerdos”**. Esto en relación con la dejación total de las armas de las FARC y la falta de cumplimiento de la ley 1820 de amnistía e indulto para presos políticos de esa guerrilla. (Le puede interesar:["Presos políticos de las FARC en Florencia inician huelga de hambre"](https://archivo.contagioradio.com/presos_politicos_farc_huelga_carcelaria/))

Para Zamora, **“junto con 2400 presos políticos que están en las cárceles estamos haciendo una tarea para visibilizar a quienes obstaculizan las leyes que se acordaron en la Habana”**. Manifestó además que la comunicación con los presos políticos ha sido constante y les han manifestado que tienen una sensación de incertidumbre frente a los compromisos del gobierno para cumplir lo acordado con las FARC.

Por otra parte los miembros de las FARC le han pedido a la sociedad en general, pero especialmente a quienes han manifestado estar en contra del proceso de paz, “que se apersonen de los acuerdos para lograr que los campesinos y los más pobres tengan condiciones de vida digna”. Además, Santrich recordó que los **acuerdos de paz no son un asunto de Estado sino un proyecto de nación** donde se deben respetar los acuerdos logrados en la Habana.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
