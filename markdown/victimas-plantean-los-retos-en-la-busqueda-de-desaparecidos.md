Title: Víctimas plantean los retos en la búsqueda de desaparecidos
Date: 2019-05-10 18:04
Author: CtgAdm
Category: Nacional, Paz
Tags: CEV, desaparición, JEP, MOVICE, UBPD
Slug: victimas-plantean-los-retos-en-la-busqueda-de-desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Desaparecidos.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CorpoJuridicaLi] 

Durante el 9 y 10 de mayo se llevó a cabo el Encuentro Nacional contra la Desaparición Forzada en Medellín, Antioquia, en el que diferentes organizaciones sociales se reunieron para evaluar su trabajo, ahora que el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR) está en pleno funcionamiento. Durante el encuentro se plantearon los retos y posibilidades que enfrentan los familiares de desaparecidos en el país.

Adriana Arboleda, directora de la Corporación Jurídica Libertad y vocera del Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), resaltó la participación en la reunión de víctimas de diferentes departamentos a nivel nacional; y la presencia de organizaciones como el Movimiento Ríos Vivos Antioquia, CRIDEC y Equitas, para construir una ruta de trabajo sobre la desaparición forzada que tome en cuenta los retos y fortalezas de cara a la actualidad del país.

### **Se necesita cesar el conflicto y apoyar la labor de búsqueda de los desaparecidos**

Arboleda afirmó que la persistencia del conflicto y de las estructuras paramilitares son algunas de las grandes dificultades en la búsqueda y exhumación de desaparecidos; situación a la que se suma "la arremetida de este Gobierno contra el Sistema": contra la Jurisdicción Especial para la Paz (JEP), en su Ley Estatutaria, y contra la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD), expresado en el bajo presupuesto otorgado para su funcionamiento.

En análisis de la vocera del MOVICE,  estas situaciones suponen un reto, pues la apuesta por la paz implica defender la legitimidad de los tres mecanismos que integran el Sistema (JEP, UBPD y CEV); y en ese sentido, el desafío para las organizaciones es seguir rodeando a estas instituciones, y exigir al Gobierno una voluntad para avanzar en el reconocimiento de los derechos de las víctimas. (Le puede interesar:["Comisión de la Verdad lista para tejer nuevos relatos de memoria en Medellín"](https://archivo.contagioradio.com/comision-de-la-verdad-lista-para-tejer-nuevos-relatos-de-memoria-en-medellin/))

Por su parte, de cara a los mecanismos, el reto identificado es la puesta en marcha de los procesos de participación y enfoque territorial; pues aunque la Comisión para el Esclarecimiento de la Verdad (CEV) y la JEP han avanzado con su presencia en los territorios, aún quedan por establecerse mecanismos y agendas de trabajo con las tres instituciones. (Le puede interesar: ["Piden a la JEP proteger 15 lugares donde se hallarían personas desaparecidas"](https://archivo.contagioradio.com/piden-proteger-16-lugares-jep/))

### **Articulación con el Sistema: ¿reto u oportunidad?**

La Directora de la Corporación sostuvo que era necesario dar tiempo a los mecanismos para que desarrollaran sus enfoques y rutas de trabajo con las organizaciones; sin embargo, recordó que por parte del MOVICE, entró en operación el pedido de medidas cautelares a la JEP sobre 16 lugares en los que se cree que hay desaparecidos. Esa situación significó una interlocución entre la Jurisdicción y la Unidad, con la que se creó una mesa técnica para trabajar sobre estos lugares.

En consecuencia, el Movimiento pidió que se informe cuál es el estado de la información que ha entregado la institucionaldiad sobre estos espacios para evaluarla; al tiempo que se planea el trabajo de la Mesa Técnica, y se crean los criterios para definir sus integrantes. Arboleda dijo que el MOVICE estableció también contacto con la Directora de la UBPD para que las medidas cautelares no se extiendan en el tiempo y perduren, sino que se siga una ruta de trabajo sobre estos espacios; ello también significa un desafío en la articulación del movimiento.

### **Los insumos que podrían llegar al Sistema**

Arboleda manifestó que el MOVICE está dando  pasos en la articulación con el Sistema mediante la documentación de casos en el marco del proyecto Colombia Nunca Más, "depurando casos" y agregando otros nuevos para brindar herramientas de trabajo al SIVJRNR. "En algunos capítulos se están trabajando fichas de documentación que contienen información de carácter forense, información que sirve para la labor de identificación" puntualizó, y agregó que hay cerca de 10 personas dedicadas a esta labor, por lo que esperan que a lo largo del año se pueda entregar esta información, para posteriormente trabajar en los planes de búsqueda nacional y regionales.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
