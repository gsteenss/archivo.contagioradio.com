Title: Caso Álvaro Gómez plantea un debate en el que no debe entrar el Gobierno
Date: 2020-10-08 21:57
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Álvaro Gómez Hurtado, Corte Constitucional, Fiscalía General de la Nación, JEP
Slug: caso-alvaro-gomez-plantea-un-debate-en-el-que-no-debe-entrar-el-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Alvaro-Gomez-Hurtado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de que la Fiscalía General y la Jurisdicción Especial para la Paz -[JEP](https://www.jep.gov.co/Paginas/Inicio.aspx)- decidieran citar a comparecer a Rodrigo Londoño y Julián Gallo, también conocido como Carlos Antonio Lozada, en relación con el crimen de Álvaro Gómez Hurtado, surgió un debate jurídico sobre el posible conflicto de competencias que podría surgir entre la Jurisdicción Ordinaria y la Jurisdicción de Paz; para conocer del caso. (Le puede interesar: [Parte de verdad en crimen de Álvaro Gómez se ocultaría en la «Masacre de Mondoñedo»](https://archivo.contagioradio.com/parte-de-verdad-en-crimen-de-alvaro-gomez-se-ocultaria-en-la-masacre-de-mondonedo/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FiscaliaCol/status/1313931392452001794","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FiscaliaCol/status/1313931392452001794

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JEP_Colombia/status/1314025630418325504","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JEP\_Colombia/status/1314025630418325504

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Varios sectores interpretaron el llamado que realizó la Fiscalía como una intromisión en un caso que es de competencia exclusiva de la JEP. No obstante, para otros sectores, representados en el Gobierno Nacional; la Fiscalía tiene plena competencia para seguir investigando el caso.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Justiciaypazcol/status/1314033852306907137","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1314033852306907137

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/petrogustavo/status/1314192111806414848","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/petrogustavo/status/1314192111806414848

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Contagio Radio, consultó al abogado Germán Rodríguez, de la Comisión Intereclesial de Justicia y Paz, para que con su experticia jurídica, brindara claridad sobre el tema. Romero señaló que **los actos acaecidos en el marco del conflicto, y especialmente en el que se libró entre el Estado colombiano y las FARC-EP, eran de competencia de la JEP** y en ese orden, el homicidio de Álvaro Gómez Hurtado, estaría bajo la jurisdicción del Tribunal de Paz, teniendo en cuenta que fue un hecho ejecutado en el marco de ese conflicto y que fueron los propios miembros del antiguo secretariado de las FARC-EP quienes reconocieron la participación de ese grupo armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En todo caso, Rodríguez , argumentó que si bien la JEP era en principio la competente para conocer del asesinato de Gómez Hurtado; **si llegará a presentarse un conflicto de competencia entre la Fiscalía y el Tribunal de Paz, la llamada a resolver dicha controversia sería la Corte Constitucional, según lo ordena la ley.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado Rodríguez, aseguró que con el llamado a Londoño y Lozada, **la Fiscalía de una u otra forma estaba tratando de interferir en las competencias de la JEP.** Según el abogado, el hecho de que la JEP abocara conocimiento frente a este caso, luego de la carta recibida por los miembros del antiguo secretariado, **dejó «*mal parada*» a la Fiscalía en su gestión, pues después de 25 años, ninguna de sus tesis investigativas, había apuntado a que los responsables hubiesen podido ser miembros de las FARC-EP.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Rodríguez aseguró que esa ineficacia investigativa se debía a que el Estado como parte activa del conflicto, históricamente, había utilizado a la Fiscalía —y aun lo sigue haciendo en algunos casos— como un instrumento para perseguir a los opositores políticos y a sus adversarios en la lógica de la confrontación; lo que había llevado a una desnaturalización del órgano y a que este perdiera la objetividad y neutralidad que debía caracterizarlo en el marco de las investigaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, frente a la «*colaboración armónica entre instituciones del Estado*» argumentada por el Gobierno y la Fiscalía para que esta última interviniera en el caso, el abogado Romero, apuntó que **si en realidad la Fiscalía quería actuar en ejercicio de ese principio, debía remitir todo lo investigado en los 25 años en los que estuvo al frente del caso, directamente a la JEP, como órgano competente.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Gobierno no debe interferir en caso de Álvaro Gómez

<!-- /wp:heading -->

<!-- wp:paragraph -->

El abogado Rodríguez señaló que el presidente Iván Duque debe velar por la independencia de poderes, lo cual no hace, cuando sale a declarar que debe ser una jurisdicción u otra la competente para juzgar los hechos, lo que según el jurista, constituye en una intromisión en las decisiones de la Justica. Rodríguez agregó que al Presidente no le corresponde emitir estos juicios, pues si hay un órgano llamado a dirimir esa eventual controversia, ese es la Corte Constitucional. (Le puede interesar: [«Duque y su Partido buscan presionar a la Corte Suprema en caso de Álvaro Uribe»)](https://archivo.contagioradio.com/duque-y-su-partido-buscan-presionar-a-la-corte-suprema-en-caso-de-alvaro-uribe/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, frente a las declaraciones del Comisionado de Paz, Miguel Ceballos, en el sentido de pedir a la JEP considerar apartar a Julián Gallo de su cargo como congresista y evaluar si procedía una privación de la libertad en su contra, por la responsabilidad que había asumido; Romero señaló que eso sería violar el Acuerdo de Paz por parte del Estado colombiano.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Estas opiniones obedecen a una lógica del Gobierno, de desconocer los compromisos que se han adquirido en el marco del Acuerdo \[de Paz\].
>
> <cite>Germán Romero, abogado de la Comisión Intereclesial de Justicia y Paz</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Frente a estas declaraciones del Comisionado Ceballos, el abogado Rodríguez aseguró que son preocupantes y dejan la duda si el Comisionado, está en realidad adelantando una labor tendiente a la paz, ya que es un funcionario que está atacando constantemente instituciones y pactos suscritos en el marco del Acuerdo. (Le puede interesar: [Colombia debe respetar a sus Magistrados y sus decisiones](https://archivo.contagioradio.com/colombia-debe-respetar-a-sus-magistrados-y-sus-decisiones/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
