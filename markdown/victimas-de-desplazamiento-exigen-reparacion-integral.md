Title: Víctimas de desplazamiento exigen reparación integral
Date: 2016-03-16 12:20
Category: Movilización, Nacional
Tags: ANDAS, protesta víctimas, víctimas desplazamiento Bogotá
Slug: victimas-de-desplazamiento-exigen-reparacion-integral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/ANDAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: Alfonso Castillo ] 

<iframe src="http://co.ivoox.com/es/player_ek_10827463_2_1.html?data=kpWllJyYepShhpywj5WaaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5yncbeZpJiSo6nHuMrhwtiYxsqPqMbn0dHO3MbRrcbi1dSYx93Nq8bijNfS0sbWpcTdhqigh6eXsozdz9nSydfFsI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alfonso Castillo, ANDAS] 

###### [16 Mar 2016 ]

Este miércoles organizaciones de víctimas en cabeza de la ‘Asociación de Ayuda Solidaria ANDAS’ se movilizan en el centro de Bogotá para exigir al Gobierno nacional que **cumpla con el pago de las ayudas económicas y la reparación integral** establecida para las víctimas, en el marco de la Ley 1448.

Según denuncia Alfonso Castillo, presidente de ANDAS, la **atención de las instituciones del Estado para las víctimas “es precaria y no restablece sus derechos"** a la salud, la educación, la vivienda y la atención psicosocial, una situación que agudiza las condiciones a las que se enfrentan las familias desplazadas, debido a que como afirma Castillo "el Estado nunca ha proporcionado las ayudas de manera oportuna y suficiente para superar la condición de vulnerabilidad".

[![ANDAS\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/ANDAS_.jpg){.aligncenter .size-full .wp-image-21555 width="1280" height="720"}](https://archivo.contagioradio.com/victimas-de-desplazamiento-exigen-reparacion-integral/andas_/)

De acuerdo con Castillo la encuesta establecida por la Unidad de Víctimas para determinar la atención a las familias desplazadas se ha convertido en una "barrera de acceso" que les **ha impedido ser indemnizadas y contar con ayuda humanitaria**, pues termina concluyendo que "la gente no tiene ningún tipo de necesidad y ya supero su nivel de vulnerabilidad", una condición que no han superado ni siquiera quienes han contado con los "pocos pesos a cuenta gotas” que les llegan como indemnización, según refiere el líder.

Esta movilización que **exige garantías frente a las precarias condiciones de reparación con las que cuentan las familias desplazadas**, pretende ser la antesala de la iniciativa impulsada por distintas organizaciones para modificar la Ley de Víctimas en concordancia con los acuerdos de La Habana, "ampliando la visión de reparación que actualmente es muy precaria", como asevera Castillo.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/)]
