Title: President Duque launches national roundtable to discuss declining security
Date: 2019-06-12 18:12
Author: CtgAdm
Category: English
Tags: Government, Non-repetition, Reparation, Round Table
Slug: president-duque-launches-national-roundtable-to-discuss-declining-security
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Mesa-Nacional-de-Garantías-instalada.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @AlirioUribeMuoz] 

Nine months into his administration, President Iván Duque created on May 30 the National Guarantees Board, an important iniciative to protect human rights defenders in the country. The launch of this organism took place in Popayán, Cauca, where government officials, international organizations and human rights defenders gathered to discuss some of the most important threats to security in the regions.

### **The demands of the social organizations** 

During this meeting, social leaders demanded public policy that strives to protect the communities, developed between the public institutions and social movements. Aida Quilcué, a human rights defender at the Regional Indigenous Council of Cauca (CRIC, by its Spanish initials) said that despite the efforts of the government, "there's a lot left to implement guarantees that contribute to the protection of life."

"We see that every day the violation of rights worsens," she added.

Although threats against indigenous peoples weren't discussed during this session, Quilcué assured that stigmas against this racial group increased following the Southwestern Minga. According to the human rights defender, more than 11 threats against the community were registered. "These threats materialize just as they did with our friends at ACONC and with Edwin Dagua," she said. (Related: ["Duque must be clear about his stance on the peace deal": Retired General](https://archivo.contagioradio.com/duque-must-be-clear-about-his-stance-on-the-peace-deal-retired-general/))

### Five keys issues on Duque's agenda 

The activist indicated that it is necessary to strengthen security measures in the regions, which can be achieved by implementing the peace deal between the government and the FARC. She also called on the government to execute a decree signed by the former president Juan Manuel Santos, which establishes a communal protection system and the creation of colective security measures.

This national board will also consider recommendations presented by international organisms such as the United Nations. One of these is the creation of a CONPES document, which establishes a budget and a specific plan. The social organizations will also talk on subjects such as the right to protest, as well as cases of forced disappearances, displacement and confinements.

Another of the key issues is the continutation of dialogues between the National Liberation Army (ELN, by its Spanish initials) since a peace deal with this guerrilla group would mean more guarantees for the territories most affected by the conflict. Lastly, the representatives of the social organizations asked the government to reactivate the 19 regional board of guarantees with which the communities plan to develop a discussion on efficiente measures to protect the social leaders, from a regional perspective.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
