Title: Familiares de las víctimas del Palacio de Justicia seguirán exigiendo verdad y justicia
Date: 2015-12-17 17:39
Category: Nacional, Otra Mirada
Tags: Palacio de Justicia, plazas vega
Slug: familiares-de-las-victimas-del-palacio-de-justicia-seguiran-exigiendo-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Palacio-de-Justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 17 Dic 2015

“**Tristeza, desilusión, no podemos decir lo que los familiares sentimos frente a un aparato de justicia que no es transparente**, que anula todo un proceso de lucha, que absuelve a una persona que tuvo que ver con la desaparición de nuestros familiares”, expresa Elizabeth López Suspes, hermana de David Suspes, uno de los desaparecidos del Palacio de Justicia.

De acuerdo con declaraciones de Pilar Navarrete, también familiar de los desaparecidos, a muchos de ellos la desilusión y el dolor no los dejo levantar, se trata de una nueva forma como el Estado se burla de las víctimas. Aunque a veces piensan “tirar la toalla”, **los familiares seguirán en esta lucha de 30 años de búsqueda de verdad y justicia**, pese a los múltiples golpes que han recibido.

La votación de 5 a 3 a favor de Alfonso Plazas Vega, alegando que no existen suficientes evidencias que relacionen al militar con la desaparición de Irma Franco y Carlos Augusto Rodríguez, significa **“un exabrupto jurídico",** como lo asegura Alejandra Rodríguez hija de Carlos Augusto, desaparecido en la retoma del Palacio de Justicia y uno de los casos por cuales se señalaba como culpable al coronel.

**"Los desaparecidos no los han desaparecido una vez sino como 50 veces"** expresa María del Socorro, hermana de la guerrillera del M-19 Irma Franco Pineda. Tanto ella como los demás familiares, aseguran que pese al difícil momento que están viviendo, no se van a dejar doblegar tras una lucha de 30 años en contra de la impunidad que nuevamente reina en el país, como lo dice la hija de Carlos Rodríguez.

Algunas de las víctimas del aparato judicial colombiano aseguran que con esta decisión salen victoriosos los congresistas del **Centro Democrático, el presidente Juan Manuel Santos, además de la toda la cúpula militar implicados en casos de ejecuciones extrajudiciales.**

Aunque esperaban que el perdón del presidente no fuera solo un saludo a la bandera, lo cierto es que esta situación los hace pensar que pasaron 30 años en vano. Finalmente, Pilar Navarrete recuerda que durante el acto de reconocimiento de responsabilidad del gobierno colombiano, Santos, siempre dejó en duda la responsabilidad de los militares.
