Title: Demandan pérdida de investidura en contra de Arturo Char
Date: 2020-08-26 15:54
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Arturo Char, Consejo de Estado, Margarita Cabello, Pérdida de investidura, Procuraduría
Slug: demandan-perdida-de-investidura-en-contra-de-arturo-char
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Arturo-Char.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Ante el Consejo de Estado fue radicada una demanda de pérdida de investidura en contra del actual presidente del Senado, Arturo Char del Partido Cambio Radical**; y del senador Eduardo Pulgar del Partido de la U; **por una presunta violación del régimen de conflicto de intereses** del reglamento del Congreso contemplado en la Ley 5° de 1992. (Lea también: [Procuraduría puede cambiar la balanza política y frenar la posibilidad de cambio](https://archivo.contagioradio.com/procuraduria-puede-cambiar-la-balanza-politica-y-frenar-la-posibilidad-de-cambio/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la demanda, **el régimen fue violado debido a que los dos congresistas anunciaron su apoyo a la candidatura de Margarita Cabello** y participaron en el trámite de elección, **estando impedidos para hacerlo, puesto que actualmente tienen procesos disciplinarios abiertos en la Procuraduría.** (Le puede interesar: [Absolución de Iván Cepeda en Procuraduría pone más peso al proceso contra Álvaro Uribe](https://archivo.contagioradio.com/absolucion-de-ivan-cepeda-en-procuraduria-pone-mas-peso-al-proceso-contra-alvaro-uribe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El abogado, Jorge Enrique Camargo, quien funge como accionante, señaló en su demanda que el jueves 20 de agosto, Arturo Char presidió la plenaria donde cada uno de los ternados a la Procuraduría (Margarita Cabello, Wilson Ruiz y Juan Carlos Cortés) tuvieron un espacio para exponer sus planes ante los congresistas. Además, Char participó en la decisión de su bancada de Cambio Radical para votar por Cabello.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A juicio del demandante, **esto constituye una violación al régimen de conflicto de intereses, ya que, los senadores debieron declararse impedidos para participar del trámite de elección y por ende solicitó su destitución ante el Consejo de Estado** mediante la demanda de pérdida de investidura.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Alto Tribunal tendrá que decidir si al demandante le asisten razón, con base en los mandatos del [artículo 183](https://www.constitucioncolombia.com/titulo-6/capitulo-6/articulo-183) de la Constitución Política y de la [Ley 2003 de 2019](https://dapre.presidencia.gov.co/normativa/normativa/LEY%202003%20DEL%2019%20DE%20NOVIEMBRE%20DE%202019.pdf), mediante la cual se modificó el régimen de conflicto de intereses del Congreso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El evidente interés de Arturo Char en la elección de la Procuraduría

<!-- /wp:heading -->

<!-- wp:paragraph -->

Entre tanto, este jueves será elegida la nueva cabeza de la Procuraduría General de la Nación, quien entrará a suceder al actual Procurador Fernando Carrillo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta elección ha suscitado múltiples reacciones en diversos sectores, ya que se asegura que **el futuro nombramiento obedece más a pactos políticos, que a las competencias y calidades de los candidatos para asumir el cargo.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En relación con esos pactos políticos, se conoció que también este jueves será elegido el remplazo del Magistrado Rodrigo Guerrero en la Corte Constitucional. Lo que generó suspicacia, es que en el orden del día aprobado por Arturo Char, en su calidad de Presidente del Senado, primero quedo la elección del nuevo Procurador, que la del nuevo Magistrado, **pese a que a Fernando Carrillo le quedan 5 meses en el cargo y al magistrado Rodrigo Guerrero, menos de dos semanas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Varios sectores coincidieron en señalar que esta decisión demuestra que **ambas elecciones están atadas y hacen parte de una componenda política entre la casa Char y el Gobierno que buscan elegir a Margarita Cabello como Procuradora; y el Partido Liberal que busca la elección de Marino Tadeo como magistrado de la Corte Constitucional.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este proceder, indicaría que Arturo Char estaría buscando asegurar la elección de Cabello en el cargo, antes de sellar el pacto, para que su bancada y la del Gobierno, voten por el candidato a magistrado acordado con los Liberales.  (Lea también: [Otra Mirada: Procuraduría general ¿otro fortín político?](https://archivo.contagioradio.com/otra-mirada-procuraduria-general-otro-fortin-politico/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
