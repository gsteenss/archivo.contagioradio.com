Title: "No fueron agitadores profesionales, fue la ciudadanía inconforme"
Date: 2016-02-11 12:03
Category: Movilización, Otra Mirada
Tags: Biblioteca Tintal, Protestas TransMilenio
Slug: protestas-en-transmilenio-no-fueron-agitadores-profesionales-fue-la-ciudadania-inconforme
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/TransMilenio-Tintal.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ] 

<iframe src="http://www.ivoox.com/player_ek_10399717_2_1.html?data=kpWgm56bdZihhpywj5aXaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncYamk7PcjcvZqdPjz5DOyc7YpcXj08rgjdXWs8fZ1M7c0MbQqdSZk6iYyNrJb83VjMjW18nFqMLihqigh6aopY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Eduardo Matías] 

###### [11 Feb 2016 ] 

Tras los **hechos violentos en los que se transformaron las protestas pacíficas de usuarios** en cinco estaciones de la Troncal de TransMilenio de Las Américas que movilizados por el inconformismo con el mal servicio bloquearon las vías, reclamando mayor frecuencia en las rutas y más articulados, se reportan **por lo menos 24 personas heridas y 100 capturadas**, de las cuales 40 aún permanecen en la URI de Kennedy esperando a que La Fiscalía les resuelva su situación jurídica.

Según afirma Eduardo Matías, abogado integrante de la ‘Fundación Comité de Solidaridad con Presos Políticos’, las protestas de este miércoles son la **respuesta ante la inconformidad de muchos años de la ciudadanía frente al mal servicio de TransMilenio**, que se tornaron violentas ante la respuesta de la actual administración distrital de enviar a efectivos de la Policía y el ESMAD para obligar a los usuarios a retirarse de las vías con la emisión de gases, el uso de bolillos y las patadas con las que algunos de ellos resultaron heridos.

De acuerdo con el abogado, la mayoría de quienes fueron capturados y aún permanecen detenidos “no tienen que ver con los desmanes presentados”, por lo que se espera que en las próximas horas sean dejados en libertad ante la falta de elementos que prueben su responsabilidad en la ruptura de vidrios de las estaciones y buses de TransMilenio, en las que estarían involucrados **miembros del ESMAD que fueron grabados mientras lanzaban gases a la estación de Biblioteca El Tintal en la que se resguardaban algunas personas**.

Matías insiste en que debería ponerse en tela de juicio la cifra de pérdidas presentada por la administración, pues podría estar siendo usada para justificar la **acción violenta de la Alcaldía frente a la inconformidad de los ciudadanos**, y agrega que “no es nada cierto que hayan sectores políticos detrás estos hechos” durante la administración de Petro se dieron protestas, pero él no envió policías a agredir a las personas para disolver las movilizaciones ciudadanas, concluye.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
