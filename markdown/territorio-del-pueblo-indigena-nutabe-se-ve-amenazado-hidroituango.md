Title: Territorio del pueblo indígena Nutabe se ve amenazado por Hidroituango
Date: 2016-11-28 22:53
Category: Ambiente, Nacional
Tags: Comunidad Indígena Nutabes, Desplazamiento HidroItuango, despojo de tierras, genocidio
Slug: territorio-del-pueblo-indigena-nutabe-se-ve-amenazado-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/nutabe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [28 Nov 2016] 

El pueblo indígena Nutabe ubicado en el corregimiento de Orobajo en Sabanalarga Anqtioquia, denunció a través de un comunicado público los graves hechos que viene cometiendo el proyecto Hidroituango en su contra, pues además del despojo de tierras y la contaminación de su entorno, les **han anunciado que para el 2018 inundarán su lugar de asentamiento ancestral habitado por la comunidad hace más de 5 siglos.  
**

A pesar que el pueblo Nutabe tiene como respaldo legal su conformación como Cabildo de Orobajo desde el 2014, y se han acogido a lo dispuesto por la **Ley 89 de 1890** y las disposiciones del **Convenio 169 de la OIT**, el cual se refiere a los mecanismos legales que poseen los pueblos indígenas y tribales, las Empresas Públicas de Medellín insisten en llevar a cabo el proyecto que inundara gran parte de su territorio en 2018.

Dicho proyecto que afectará a un bosque seco tropical de los mejores conservados del país, inundando aproximadamente **3.800 hectáreas para generar 2.400 megavatios,** ya está generando estragos a nivel **económico, social, ambiental y cultural.**

### **¿Cuáles son las denuncias?** 

Denuncian que a pesar de haber expuesto su postura ante entidades como el ANLA y la Dirección de Asuntos Indígenas, ROM y Minorías del Ministerio del Interior, EPM **ha continuado violando sistemáticamente los derechos étnicos de la comunidad indígena Nutabe**, y señalan algunas de esas afectaciones:

-   Irrespeto a nuestro territorio, nuestra cultura y nuestra colectividad, al ingresar a nuestra comunidad sin las autorizaciones respectivas de nuestras autoridades.

<!-- -->

-   Generación de divisiones y conflictos al interior de nuestra comunidad, a través del ofrecimiento de prebendas, negociación individual y desinformación.

<!-- -->

-   Generación de pánico colectivo al difundir que nuestra comunidad no tiene derechos colectivos como pueblo indígena, ni derecho a tener un reasentamiento digno ni a recibir las indemnizaciones y compensaciones por la pérdida de su infraestructura - escuela, cementerios, trapiche, sistema de riego, parque, calles y caminos- y sus modos de vida ancestrales.

<!-- -->

-   Creación de alarma y zozobra al difundir que el plazo de salida del asentamiento de Orobajo es inminente por la construcción de la represa de HIDROITUANGO, y que por tanto en enero del 2017 ya no debe haber ni casas ni habitantes en este territorio indígena.

<!-- -->

-   La negativa sistemática de EPM para negar nuestro derecho al Proceso de Consulta Previa, negando de paso la justa remuneración, compensación y mitigación de los impactos causados a nuestra comunidad y nuestro territorio por HIDROITUANGO.

### **¿Cuáles son sus exigencias?** 

1.  Que las instituciones del Estado Colombiano obligadas a ocuparse del caso como Mininterior, Gerencia Indígena de Antioquia, Defensoría del Pueblo, Procuraduría, Corte Constitucional y Organismos de Derechos Humanos, realicen las acciones necesarias para la protección de la comunidad del Cabildo Indígena Nutabe de Orobajo del municipio de Sabanalarga Antioquia, a fin de evitar la vulneración de sus derechos fundamentales y étnicos.
2.  Que la ANLA garantice el inicio del Proceso de Consulta Previa al que tenemos derecho como pueblo indígena, a fin de evitar la extinción física y cultural de nuestra comunidad Nutabe.
3.  Que EPM y el proyecto Hidroituango respeten nuestros derechos y nuestro territorio, nuestros usos y costumbres, nuestra autonomía y nuestros recursos naturales, tal como lo exige el Convenio 169 de la OIT y la jurisprudencia existente en Colombia sobre la protección y cuidado de los pueblos originarios.
4.  Que las entidades públicas dueñas del proyecto Hidroituango como lo son la Gobernación de Antioquia, IDEA y EPM, cumplan con su deber institucional de respeto a los derechos humanos y étnicos que les obliga nuestra constitución y nuestras leyes.
5.  Que se instaure de forma inmediata una ruta de atención integral a la problemática que vive la comunidad del Cabildo Indígena Nutabe de Orobajo, acorde a sus usos y costumbres y a sus derechos étnicos, con participación amplia y permanente de las organizaciones indígenas del nivel regional y nacional.

Por último hacen un llamado de solidaridad a la Organización Indígena de Antioquia y a la Organización Nacional Indígena de Colombia, así como a los organismos protectores de derechos humanos a nivel nacional e internacional, para **“activar los mecanismos de protección necesarios para la pervivencia de la comunidad indígena de Orobajo, garantizando nuestra autonomía, territorio y cultura Nutabe”.**

Lea Aquí el Convenio de la OIT: https://es.scribd.com/document/332572538/Convenio-169-de-la-OIT-sobre-Pueblos-Indigenas-y-Tribales

###### Reciba toda la información de Contagio Radio en [[su correo]
