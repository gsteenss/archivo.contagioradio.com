Title: Séptimo Día tiene la oportunidad de ejercer un periodismo responsable
Date: 2016-02-05 15:07
Category: Judicial
Tags: Caracol TV, colectivo de Abogados José Alvear Restrepo, Séptimo Día.
Slug: septimo-dia-tiene-la-oportunidad-de-ejercer-un-periodismo-responable
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/septimo-dia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Caracol TV 

<iframe src="http://www.ivoox.com/player_ek_10332609_2_1.html?data=kpWglZeadJqhhpywj5aYaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncbSZpJiSo57UuMrh0JCxh6iXaaK4wpDhy8rSqYzgwpDc0tTWuNbiysnOxpDIqYzZy8rfxcrWb9bijNXS1M7TqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jormary Ortegón, CAJAR] 

###### [5 Feb 2016] 

El Tribunal Superior de Bogotá, falló tutela ordena al programa Séptimo Día de Caracol TV, rectificar las difamaciones proferidas por ese programa contra el Colectivo de Abogados José Alvear Restrepo, en los que **presentó información falsa a pesar de contar con los datos verdaderos presentados por el propio colectivo.**

Se trató de tres programas emitidos en el 2016, que, de acuerdo con Jormary Ortegón, presidenta del Cajar, “tenían la finalidad de estigmatizar a los pueblos indígenas del norte del Cauca y al Colectivo, con esa intensión se hicieron afirmaciones que no correspondían a la realidad frente a la masacre del Nilo hace 24 años”.

Ortegón, asegura que en el Colectivo “se encuentran satisfechos con la decisión”, pues el acompañamiento del colectivo a las comunidades indígenas del Norte del Cauca, ha sido una labor de 24 acompañamiento las víctimas en búsqueda de justicia, logrando que incluso, la CIDH condenara al Estado por esos hechos en lo que participaron tanto paramilitares como militares, explica la abogada.

**“Estos programas estigmatizan a los pueblos indígenas, los señalan de corruptos y violadores de DDHH”,** dice la presidenta del Cajar, agregando que tras la publicación de los programas en Séptimo Día, varios líderes indígenas fueron amenazados, y meses más tarde Feliciano Valencia, líder indígena, fue encarcelado.

Cuando se presentaron los tres programas, el Colectivo solicitó a través de un derecho de petición la rectificación de esa información, sin embargo Séptimo Día se negó a hacerlo, lo que para Ortegón demostró que “no fueron responsables ni imparciales”, y por lo tanto, **“no se ajustaron a los estándares del periodismo, como por ejemplo, contrastar la información… el periodista debe informar y no tergiversar”, dice.**

Por su parte el presentador de Séptimo Día, Manuel Teodoro, a través de su cuenta en Twitter afirmó que el programa no va a hacer ninguna rectificación. “SD no va a rectificar absolutamente nada de ese mediocre bufete. Eso es un invento. Manténganse sintonizados a SD y no lo verán!”. Frente a esta situación, Jormary Ortegón sostiene la declaración de Teodoro “no es contra el [Cajar](https://twitter.com/Ccajar) sino contra los jueces que fallaron la tutela, contra una decisión judicial”, que de no ser acatada, puede haber hasta privación de la libertad.

Finalmente, afirma que la respuesta del presentador de Caracol TV, “**refleja mucho el tipo de periodismo que realizan… el medio debería revisar sus métodos de trabajo** pues no es la primera vez que se les señala”, añadiendo que en el actual momento que vive el país, frente a un posible acuerdo de paz no se puede continuar señalando a unos y otros por su ideología, como lo ha venido haciendo Manuel Teodoro a través de su cuenta de twitter.
