Title: ELN niega vinculación a manifestaciones y reitera voluntad de diálogo
Date: 2020-09-15 18:39
Author: AdminContagio
Category: Actualidad, Movilización
Tags: Abuso de fuerza Policía, ELN, ESMAD, Gobierno Duque
Slug: eln-niega-vinculacion-a-manifestaciones-y-reitera-voluntad-de-dialogo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/PB_UP-scaled-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ELN voces

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Policía Nacional dio a conocer un informe de inteligencia en el que establecen que existen al menos 20 organizaciones vinculadas a protagonizar actos vandálicos en medio de la movilización, incluyendo células del ELN, versión que desde La Habana ha respondido el equipo negociador de la guerrilla rechazando esta versión, al señalar que la manifestación fue una expresión social en contra el actuar violento de la Policía.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el informe hace referencia a grupos conformados por "estudiantes universitarios y menores de edad" como a los responsables de de los desmanes, esta versión contrasta con los múltiples videos difundidos en redes sociales en los que se evidencia a integrantes de la Policía Nacional cometiendo diversos delitos. [(En vídeo\] 10 delitos de la Policía en medio de las movilizaciones)](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, el jefe de la delegación de paz Pablo Beltrán, expresó que las protestas son espontáneas y que la ciudadanía viene manifestándose en contra la brutalidad policial, agregó que “acusar al ELN de estar financiando las protestas o atacando los CAI, es un mal recurso" y que desde la guerrilla no se ha pensando en "ataques urbanos o ataques contra Bogotá". [(Lea también: Miguel Ceballos no le cumple a la paz sino que "promueve el conflicto armado": Cepeda)](https://archivo.contagioradio.com/miguel-ceballos-no-le-cumple-a-la-paz-sino-que-promueve-el-conflicto-armado-cepeda/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ELN expresa voluntad de paz

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tras los hechos ocurridos el pasado 15 de agosto en Samaniego, Nariño en los que fueron asesinados nueve jóvenes, también se señaló al ELN como responsable, pese a ello, también rechazaron su autoría, por el contrario, Beltrán expresó que desde el ELN existe una voluntad de paz "con este o con cualquier gobierno" para sentarse a buscar una salida política al conflicto armado, manifestando que desde La Habana están listos para desarrollar los diálogos de paz. [(Lea también: Cese unilateral al fuego del ELN, una oportunidad para la paz)](https://archivo.contagioradio.com/cese-unilateral-al-fuego-del-eln-una-oportunidad-para-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que el ELN propuso al Gobierno pactar un cese al fuego bilateral durante 90 días con el fin de crear «un clima de distensión humanitaria, favorable para reiniciar los Diálogos de Paz», después de conocerse el llamado del Consejo de Seguridad de la ONU convocando a un cese al fuego global en medio de la pandemia. , pese a ello, el Gobierno rechazó la propuesta. [(Lea también: ELN propone cese bilateral y el Gobierno cierra la puerta a esa posibildad)](https://archivo.contagioradio.com/eln-propone-cese-bilateral-y-el-gobierno-cierra-la-puerta-a-esa-posibildad/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Señalamientos ya se ven reflejados en panfletos amenazantes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Las consecuencias de estos señalamientos, contenidos en informes de inteligencia ya se han visto a través de panfletos amenazantes atribuidos al Bloque Capital de las Águilas Negras quienes han declarado objetivo militar a líderes, organizaciones sociales y mesas de víctimas de la localidad de Usme en Bogotá a quienes acusan de pertenecer a dichos grupos ilegales. [(Lea también: Organizaciones sociales convocaron a un Pacto por la Vida y la Paz)](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PizarroMariaJo/status/1305922410374365184","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PizarroMariaJo/status/1305922410374365184

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto organizaciones sociales y defensores de DD.HH. han advertido que dichas acusaciones legitiman el accionar de quienes se encuentran tras estas amenazas y profundiza los niveles de riesgo y la estigmatización en contra de los liderazgos y las comunidades. [(Le puede interesar: Tribunal ordena a Duque responder a comunidades que exigen cese al fuego en sus territorios)](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
