Title: Los momentos cruciales de la fase exploratoria ELN - Gobierno
Date: 2016-03-30 11:30
Category: Nacional, Paz
Tags: Diálogos de paz ELN, Diálogos de paz en Colombia, ELN
Slug: los-momentos-cruciales-de-la-fase-exploratoria-eln-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/gabino-santos.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AIPAZCOMUN ORG ] 

###### [30 Mar 2016 ]

[Luego de tres años de contactos y dieciocho meses de diálogos exploratorios entre el gobierno y la guerrilla del ELN, desde la ciudad de Caracas se anunció el[[inicio de la fase pública de la mesa de conversaciones de paz](https://archivo.contagioradio.com/se-anuncia-inicio-fase-publica-de-conversaciones-de-paz-con-guerrilla-del-eln/)], con Cuba, Noruega, Chile, Venezuela, Brasil y Ecuador, como países garantes. Un paso importante al que se llega luego de momentos de tensión y discusión que iniciaron en 2013. ]

Tras reuniones llevadas a cabo durante 2013, el Gobierno Nacional y el ELN dieron inicio a la fase exploratoria de conversaciones en junio de 2014, con el fin de **acordar una agenda y diseñar un proceso para hacer viable el fin de conflicto** y construir una paz estable y duradera para Colombia, según informaron en el comunicado conjunto No. 1.

Para ese momento, en la agenda de negociación sólo estaban concertados los puntos de participación de la sociedad y el de víctimas, y se insistía en que la paz no podía reducirse a la desaparición de la guerrilla sino que debía partir de la **discusión de las causas estructurales del conflicto armado colombiano**, en lo que el Gobierno mostraba poca voluntad de acuerdo con lo afirmado por el ELN en este comunicado.

La fase exploratoria continúo con reuniones entre las delegaciones que se extendieron hasta 2015, en septiembre de ese año el máximo comandante del ELN, Nicolás Rodríguez Bautista alias 'Gabino', aseveró en entrevista con Contagio Radio que para formalizar los diálogos hacía falta acordar un 3% de la agenda, y que parte del proceso debía incluir la **participación democrática y protagónica de la sociedad**, para la discusión del modelo extractivo y la política de seguridad nacional, principalmente.

Por su parte en octubre de 2015 el jefe de la delegación del ELN Pablo Beltrán, aseguró que el objetivo de este proceso de negociación es poner fin al conflicto armado interno y **crear condiciones de democratización para luchar por el poder sin acudir a la violencia**; un experimento cuyo buen resultado depende del esfuerzo y voluntad de las dos partes, sí lo que se quiere lograr son mejores condiciones de vida para la sociedad a través de la suscripción de acuerdos, según sostuvó Beltrán.

Sin embargo, pese a los avances para el momento el ambiente era tenso por cuenta de la "falta de voluntad de paz, más allá del discurso" que según la delegación de paz del ELN expresaba el Gobierno nacional al criminalizar la protesta social, la izquierda y los sectores de oposición, pues justo en este mes se dieron varios casos de 'falsos positivos judiciales' contra líderes sociales, y de acuerdo con Beltrán el proceso de paz debía tener en el centro de la discusión las **garantías para el activismo social y polític**o, la defensa de los derechos humanos y el desmonte del paramilitarismo.

**En noviembre de 2015 el ELN aseguró que estaba listo para iniciar la fase pública de negociaciones con el gobierno** pero que estaba a la espera del acuerdo sobre la fecha del último encuentro que llevaría a la formalización, solicitud que obtuvo pronunciamiento oficial en febrero de 2016, cuando el alto comisionado de paz, Frank Pearl, emitió un comunicado en el que afirmaba que no era cierto que no se hubiera definido el día para esa última reunión, y agregó que a esa guerrilla se le estaba acabando el tiempo para iniciar la fase pública, **un ultimátum que poco bien le hacía a la paz**, de acuerdo con el analista Victor de Currea Lugo.

Frente a la evolución de estos diálogos exploratorios y la consolidación de la agenda de negociación, la **sociedad colombiana sentía gran expectativa y extendía su apoyo al inicio de la fase pública**. Organizaciones sociales enviaban comunicados a las partes para que se manifestaran al respecto pues el 2015 había cerrado con una posible agenda sobre el tema víctimas; participación de la sociedad civil; sector agrario; participación política; drogas ilícitas; política minero-energética; explotación de los recursos naturales y medio ambiente.

Agenda y puntos de discusión que durante este año sufrieron algunas transformaciones y [[llegaron a consolidarse en](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/)][[[seis](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/)]: **participación de la sociedad; democracia para la paz; víctimas; transformaciones para la paz; seguridad para la paz y la dejación de las armas; y garantías para el ejercicio de la acción política**.] Puntos que sí bien coinciden con lo que se está discutiendo con las FARC, también podrían tener algunas divergencias que se irán conociendo a medida que la negociación Gobierno - ELN, avance.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ] 
