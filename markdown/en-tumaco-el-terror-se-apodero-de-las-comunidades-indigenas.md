Title: En Tumaco el terror se apoderó de las comunidades
Date: 2020-09-29 20:07
Author: CtgAdm
Category: Actualidad, Otra Mirada
Tags: colombia, indigenas, pueblo Awá, Terror, Tumaco
Slug: en-tumaco-el-terror-se-apodero-de-las-comunidades-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/resguardo-Awa-narino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*Foto: Cortesía Indepaz*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La violencia en el departamento de Nariño tiene múltiples facetas, **lo que la convierte en uno de los centros del [conflicto armado](https://www.colectivodeabogados.org/?Actualizamos-denuncia-sobre-grave-situacion-humanitaria-en-el-resguardo-Inda)en Colombia** más complejos, en comparación con otros territorios, especialmente por las nuevas formas de violencia presentes allí.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comunidad de Nariño, específicamente aquella que habita zonas rurales de **Tumaco ha tenido que enfrentarse históricamente a desplazamientos, reclutamiento forzado, amenazas, hostigamiento** y recientemente, asesinatos y masacres, hechos violentos que se han enfocado en los pueblos indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El ejemplo más cercano es el denunciado el pasado 26 de septiembre, en donde **fueron [asesinadas 5 personas](https://archivo.contagioradio.com/inda-sabaleta-tumaco/)en medio de enfrentamientos de grupos armados** en el resguardo Awá de Inda Sabaleta en Tumaco.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Ccajar/status/1310628894270255104","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Ccajar/status/1310628894270255104

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Un hecho que fue reportado por el [Colectivo Abogados José Alvear Restrepo](https://www.colectivodeabogados.org/?Actualizamos-denuncia-sobre-grave-situacion-humanitaria-en-el-resguardo-Inda) (CAJAR), que indicó que luego de los enfrentamientos entre el grupo armado Los Contadores y el frente Oliver Sinisterra dentro del resguardo Inda en una intención de apoderarse en la totalidad del territorio fueron asesinadas 5 personas, ***"cuyos cuerpos aún no habían sido levantados, además de dos heridos, en medio del enfrentamiento resultaron retenidas al menos 40 personas".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto en una información inicial, estás 40 personas estaban integradas por menores de edad, jóvenes, mujeres, adultos y comunidad indígena; sin embargo **casi 72 horas después se pudo confirmar la realidad del hecho.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto **debido a la dificultad de ingreso al territorio, y se aclaró que algunas de esas personas ya habían sido liberadas**, de estas se encuentran aún desaparecidas dos, una de ellas de la población indígena.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Claves del terror en Tumaco

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según **Rosa María Mateus, abogada del CAJAR**, esta disputa territorial tiene una relación directa con los cultivos de uso ilícito y las cerca de 10 mil hectáreas sembradas de coca en esta zona, junto a la **falta de cumplimiento de la implementación por parte del Gobierno, del Acuerdo de Paz y los programas de tu sustitución voluntaria.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Falta el cumplimiento por parte del Gobierno de unos acuerdos que ya deberían estarse concertando por medio de consultas previas libres e informadas por parte de las comunidades"*
>
> <cite>**Rosa María Mateus, abogada del CAJAR**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Esto según la abogada, **se relaciona con el incumplimiento por parte del Gobierno Nacional de atacar los eslabones del narcotráfico y poder dar solución a este problema**, *"las comunidades indígenas y campesinas se encuentran en medio de estos actores armados, así como los actores estatales que pretenden buscar soluciones por medio de la radicación forzada".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***"Se está respirando un temor también a la denuncia"*** , señalando que las delaciones recibidas indicaban que eran madres valientes las de Tumaco las que habían hecho la denuncia de lo que estaba pasando en el territorio; *"ellas hablaron des el miedo, pero también con la fortaleza de querer saber el paradero de sus seres queridos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro punto también que permite entender según Mateus las claves de este terror, **es el manejo de las rutas del narcotráfico que atraviesan este territorio, zonas encabezadas por comandantes paramilitares** que decidieron no acogerse a ningún proceso de desmovilización y al contrario trasladarse a zonas como Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto indica que se se suma la operación de **disidencias de FARC, reflejando no sólo una profundización de la violencia** en todo el departamento, también el desequilibrio producto del incumplimiento del Acuerdo de Paz. ([FARC pidió perdón a las víctimas en un gesto de paz](https://archivo.contagioradio.com/farc-pidio-perdon-a-las-victimas-en-un-gesto-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y concluyó diciendo que **todo esto hace parte de un engranaje de afectaciones que se vienen viviendo desde hace muchos años**, y que incluso tienen que ver con proyectos como el oleoducto trasandino, en donde se puede llegar a entender parte de la complejidad de todo lo que se está presentando en Tumaco.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un análisis que señala también la responsabilidad de un estado que debe velar por resguardar la vida de los territorio sagrados en donde los proyectos de narcotráfico cada vez se impone con más fuerza, y donde **la respuesta del Estado sólo llega como el el aumento de mas actores armados, como los son las fuerzas militares.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hay que decirlo, las fuerzas militares también han atacado la comunidad, no son pocas las denuncias que tenemos en contra del Ejército qué va a sus territorios".*
>
> <cite>**Rosa María Mateus, abogada del CAJAR**</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### *"Para nosotros no es garantía de vida traer más Fuerza Pública a los territorios"*

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de que en el 2020 se registraron 27 casos violatorios de Derechos Humanos en contra del pueblo Awá, la **Unidad Indígena del Pueblo Awá (UNIPA)**, **alertó sobre un posible desplazamiento masivo que podría generarse de continuar los enfrentamientos** entre los grupos armados, tanto legales como ilegales en el territorio de Inda Sabaleta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un desplazamiento que ya se había registrado en el 2007, en dónde varios núcleos familiares tuvieron que abandonar sus hogares buscando salvaguardar su integridad. ([La memoria ambiental para el “retorno in situ”](https://archivo.contagioradio.com/la-memoria-ambiental-para-el-retorno-in-situ/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Como comunidad indígena estamos pensando en desplazarnos del territorio y no hacia las cabeceras municipales,** sino hacia otros resguardos que nos permitan recobrar nuestra paz en la comunidad y salvar la vida de nuestros compañeros y compañeras"*
>
> <cite>Juan Orlando Moreno, Consejero de la Guardia Indígena del Pueblo Awá</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1310635885239717888","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1310635885239717888

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Juan Orlando Moreno, consejero de la Guardia Indígena del pueblo Awá, indicó que durante el 2020 se han presentado cerca de 30 casos violatorios de Derechos Humanos contra la comunidad indígena, hechos que hasta el momento no ha recibido ningún tipo de respuesta o atención por parte del Gobierno. ([Pueblo Awa en alto riesgo por amenazas, desplazamientos y asesinatos](https://archivo.contagioradio.com/pueblo-awa-en-alto-riesgo-por-amenazas-desplazamientos-y-asesinatos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Y ante los recientes hechos ocurridos en el resguardo Inda Sabaleta en Tumaco, indicó que **la única respuesta recibida por parte del gobierno ha sido el incrementar el pie de fuerza**, *"para nosotros no es garantía de vida traer más militares y policías a la zona, lo que se necesita es la activación de la ruta humanitaria, así como el financiamiento que fortalezca el sistema de protección de la Guardia Indígena".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señaló también que como integrante del resguardo, en el momento no se respira tranquilidad, al contrario se siente desespero, zozobra y miedo, *"desde el fin de semana hay un niño que llora y no quiere salir debajo de su cama, este es un ejemplo claro de lo que todos y todas estamos viviendo, en donde **muchas familias tienen ya una maletica lista por si en cualquier momento les toca salir corriendo".***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### *"En Nariño no existe la palabra posconflicto cuando aún se vive el terror"*

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante el miedo y el claro incumplimiento del Acuerdo de Paz, en el capítulo **étnico, hacia las comunidades que viven en zona rural de Tumaco, Nariño; Sin duda uno de los retos más grandes es lograr conocer la verdad, especialmente ante el terror que hay de hablar** por parte de las comunidades indígenas y campesinas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Jorge García, Coordinador de la Casa de la Verdad en la costa pacífica nariñense**, dio razón a esto señalando que en estos 6 meses e incluso antes, su trabajo a sido complejo para entrar en algunos territorios, *"para nosotros es muy difícil de recorrer y hacer un despliegue que nos permita acercarnos a esa gran verdad que desconoce la gran mayoría del país".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

García enfatizó en que este es un miedo generalizado, y no es producto de algo que se haya generado en los últimos días , sino que **antecede a un historial de afectaciones e impactos a estas comunidades,** pero también en las instituciones que quieren entrar o hacer algún tipo de intervención humanitaria en estas zonas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Nosotros como Comisión de la Verdad hemos percibido este miedo desde que iniciamos en febrero del 2019, **un miedo a hablar, partiendo del desconocimiento de que es dar un testimonio y profundizando en un terror arraigado de años de violencia sistemática** contra los cuerpos y los espíritus de las comunidades indígenas"*
>
> <cite>**Jorge García, Coordinador de la Casa de la Verdad en la costa pacífica nariñense**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Garcia señaló que muy someramente **en ese territorio se puede hablar de posacuerdo, y que la palabra posconflicto aún no ha llegado,** *"al contrario el conflicto en zonas como Tumaco se se recrudece con el acuerdo de La Habana".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un conflicto que para el coordinador resguarda las verdades ocultas bajo el discurso racista histórico desde el estado hacia los pueblos y los territorios, *"el abandono no nos dice mucho, pero detrás de esa frase hay todo un entramado de prácticas racistas que se revelan con la indiferencia total, con el dejar que se maten las personas, porque **las vidas de los indígenas y de los pueblos negros no tienen importancia para el país ni para el estado".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Unas verdades base de ese temor a hablar y a decir la verdad, y en donde para Jorge García **la invitación es a reconocer que el racismo es un flagelo de la humanidad y hay que mencionarlo, partiendo de que esta es la verdad de fondo** que tiene que resolver el estado, y a partir de allí *"tratar a las personas como seres humanos, y no dejarlos tirados a la guerra como si no tuvieran ninguna importancia como personas".*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D358019121988438&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
