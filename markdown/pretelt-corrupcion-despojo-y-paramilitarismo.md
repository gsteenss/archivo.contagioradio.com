Title: “No hay como Dios” ejemplo de paramilitarismo y despojo en finca de magistrado Pretelt
Date: 2015-03-20 22:42
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Corte Constitucional, Desplazamiento, Desplazamiento forzado, despojo, Finca No hay como Dios, Jorge Pretelt, justicia, Justicia y Paz, Ley 975 de 2005, Magistrado, Mancuso, Paramilitarismo, Pretelt
Slug: pretelt-corrupcion-despojo-y-paramilitarismo
Status: published

##### Foto: pulzo.com 

<iframe src="http://www.ivoox.com/player_ek_4243110_2_1.html?data=lZehlZaVdI6ZmKiak52Jd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Ln0JC91MrYqc3ojJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gustavo Gallón, Comisión Colombiana de Juristas] 

El magistrado Jorge Pretelt, investigado por la presunta recepción de \$500 millones para influir en el fallo de una tutela a favor de la empresa Fidupetrol, ahora **es cuestionado por nexos con paramilitares por el título que posee sobre la finca “No hay como Dios”**, en la región del Urabá en Antioquia. Un predio que tiene una historia de desplazamiento, asesinato y usurpación de tierras, protagonizada por las AUC, al mando de Salvatore Mancuso.

**“Es claro, de acuerdo a la Ley 1448 (Restitución de Tierras), que hay una presunción en contra de Pretelt por el contexto que rodea el caso y la actitud del magistrado”**, afirma Gustavo Gallón, director de la Comisión Colombiana de Juristas, quien agrega que una persona que obtiene tierras en circunstancias de violencia y despojo, tiene que probar que actuó de buena fe, exenta de culpas.

La finca **“No hay como Dios”**, de la que hoy es propietario el magistrado y ex-vecino de Uribe, padece una cadena de asesinatos que empiezan su historia desde 1994 a 1997, cuando los campesinos Alejandro Antonio Padilla Guerra y su esposa Evangelina Ortega, terminan desplazados por los paramilitares.

De acuerdo a la Comisión Colombiana de Juristas, el 29 de noviembre de 1994, **100 hombres de Salvatore Mancuso asesinaron a tres hijos de la pareja de campesinos**, Baldomiro, Roberto y Estanislao. Días después, los paramilitares les robaron  500 reses, y forzaron el desplazamiento de la familia Padilla a la finca “No hay como Dios”.

En 1997, fueron nuevamente forzados a desalojar el predio y se desplazan hacia la vereda de San Pelayo, Córdoba. Allí, paramilitares incendian su casa y asesinan a Alejandro Padilla y a Evangelina, a Animadat Padilla, a Edilberto Contreras y a Olfady Contreras Padilla, de 9 años de edad, quien era nieta de Alejandro y Evangelina.

Gildardo Padilla Ortega, uno de los pocos hijos que quedaba vivo, le pidió a Mancuso que parara los homicidios contra su familia, sin embargo, cuatro meses después, otros **dos hermanos de Padilla fueron asesinados.**

Pero la cadena de muertos no paró, en el año 2013, cuando se citó a 25 campesinos a atestiguar como víctimas de despojo y desplazamiento forzado el 29 de octubre de 2013 en Chigorodó, ante la Unidad Nacional de Análisis y Contexto de la Fiscalía, finalmente, el 17 de noviembre del mismo año, **Gilberto Antonio Padilla fue víctima de dos sicarios que lo mataron, luego de declarar sobre el despojo de las tierras de la vía Panamericana.**

“No hay como Dios”, terminó siendo un predio que Pretelt le compró a su esposa en 2005 y que ella le había comprado en el año 2000 a Evelio Enrique Díaz, alias el Burro, un paramilitar que había obligado al anterior propietario, Edilberto Villalba, a venderle esa propiedad.

**Amparado la Ley 975 de 2005 (Ley de Justicia y Paz), el exjefe paramilitar Salvatore Mancuso confesó estos asesinatos.**

“Esto es un retrato triste pero repetido de lo que ocurre en el país, una historia de despojo y  asesinatos con el apoyo de autoridades que terminan beneficiando a personas poderosas como el magistrado de la Corte Constitucional, encargada de garantizar los derechos de las personas… **es bastante difícil creer que Pretelt no conociera esa historia**”, denuncia el director de Coljuristas.

Lo preocupante de este caso, es que se si espera que las instituciones judiciales resuelvan el asunto, **“no se va a resolver nunca”**, por eso, argumenta Gallón, **debe haber presión social y se debe realizar correcciones al sistema judicial,** para que, a la hora de integrar una persona a funciones estatales, se tenga en cuenta  la autoridad moral y la honestidad.

Por el momento, se debe tener sumo cuidado con las afirmaciones de Pretelt, ya que **“ha disparado en todas las direcciones”**, advierte Gustavo Gallón.
