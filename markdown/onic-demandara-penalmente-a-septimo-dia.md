Title: ONIC demandará penalmente a Séptimo Día
Date: 2015-08-03 12:40
Category: Movilización, Otra Mirada
Tags: Manuel Teodoro, ONIC, Organización Nacional Indígena de Colombia, Séptimo Día., toma del cerro del Berlín
Slug: onic-demandara-penalmente-a-septimo-dia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/onic4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

<iframe src="http://www.ivoox.com/player_ek_5911476_2_1.html?data=lp6ek5mbeo6ZmKiak5uJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRk6%2B9pJDRx9LFssXV04qwlYqldYzkxtPOztLJstXZjMaYtYqnd4a1mtXhy9LTb6WZpJiSo6nFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Fernando Arias ONIC] 

###### [3 Agosto 2015] 

Según **Luis Fernando Arias**, Consejero Mayor de la **Organización Nacional Indígena de Colombia**, ONIC, las tres emisiones  del programa periodístico afectan la imagen de las comunidades indígenas en Colombia y generan graves riesgos sobre la vida de las personas señaladas de corrupción y de nexos con las guerrillas.

Frente a los señalamientos por corrupción, Arias explica que **todo el presupuesto que supuestamente ha sido asignado a las comunidades indígenas**, como los 3.3 millones de pesos, han sido manejados por los alcaldes municipales y nunca directamente por las comunidades indígenas como se pretende mostrar en los programas emitidos por el Canal Caracol  y que solamente hasta este año se aprobó un decreto por el cual las propias comunidades tendrían autonomía sobre el manejo de recursos.

El Consejero Mayor de la ONIC también denuncia que es claro el sesgo político del programa al presentar las imágenes de la **toma del cerro del Berlín**, como un acto en contra de las FFMM y a favor de la guerrilla de las FARC, sin embargo el medio obvia información como la condena a 300 años de prisión contra integrantes de esa guerrilla implicados en el asesinato de un comunero.

¿Quién financia el programa? ¿A qué intereses políticos responde? ¿Quién se beneficia con la información presentada? Pregunta Arias al reiterar que demandarán el programa Séptimo Día y emprenderán acciones de denuncia política, porque, según él, **los medios de información poderosos no pueden seguir actuando en contra de los intereses y las movilizaciones indígenas**. Vea también [Comunidades Indígenas recuperan la Madre Tierra en Cauca](https://archivo.contagioradio.com/152-indigenas-heridos-y-silencio-del-gobierno-a-un-mes-de-liberacion-de-la-madre-tierra-en-el-cauca/).

*"Séptimo Día se propuso a desprestigiar la lucha indígena que está marcada de sangre, muerte, desplazamiento, violación"* señala Arias, que además responsabiliza de cualquier acción en contra de las comunidades indígenas a Manuel Teodoro y al programa Séptimo Día.

El Consejero Mayor agrega que muchas veces quienes hablan mal de las comunidades son los integrantes del Centro Democrático que tienen intereses sobre las tierras que pretenden recuperar los indígenas en el Norte del Departamento del Cauca.
