Title: "Hay que insistir en que Gobierno y ELN se sienten a negociar": Victor de Currea
Date: 2016-08-29 14:20
Category: Nacional, Paz
Tags: conversaciones de paz con el ELN, diálogos de paz con el ELN, Dialogos gobierno eln
Slug: hay-que-insistir-en-que-gobierno-y-eln-se-sienten-a-negociar-victor-de-currea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Negociación-con-ELN.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ciudad CCS ] 

###### [29 Ago 2016] 

En su más reciente comunicado el comandante del ELN, Nicolás Rodríguez Bautista, se dirigió a las FARC-EP expresando sus deseos de éxito en este [[nuevo camino](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)] que inicia la organización guerrillera, pese a que no compartan la esencia de los acuerdos pactados con el Gobierno. Para el analista Víctor de Currea Lugo, esta comunicación surge ante todo en el marco de las dificultades que han impedido el inicio formal de las negociaciones, por lo que desde diversos sectores se debe **continuar insistiendo a las dos partes que se sienten de inmediato** pues "sin el ELN la cosa cojea".

De acuerdo con Currea, aunque no se conocen todos los detalles que permitan saber dónde está el escollo de las negociaciones entre el ELN y el Gobierno, pese a que las dos partes compartieron su [[voluntad explícita de negociar](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/)], frente al secuestro los dos tienen razón, pues si bien el Gobierno aceptó que fuera un punto en la agenda, se trata de **un debate nacional que debe entenderse más allá del formalismo de un acuerdo** con una implicación ética interna.

Para el analista este comunicado se da también el marco de [[lo emitido el pasado 5 agosto](https://archivo.contagioradio.com/eln-pide-que-se-respete-la-agenda-los-acuerdos-y-los-espacios-ya-acordados/)] y sí bien podría entenderse como en contravía de lo avanzado durante los últimos cuatro años, deja claro que hay que entender que **una es la dinámica de los micrófonos y otra la de las Mesas de Negociación a puerta cerrada**, pese a que muchas veces la opinión pública las confunda.

"Hay una cosa que históricamente le ha molestado al ELN y es que lo traten como un actor de tercera en un proceso de paz de cuarta" asegura de Currea e insiste en que el Gobierno y algunos sectores sociales no terminan de entender la dinámica de esta guerrilla, pues **no se trata de hacer un remiendo al proceso de paz para incluirla**, sino de actuar en un momento político oportuno que no puede reducirse al recorte de un retazo de los acuerdos con las FARC-EP, pues es un asunto más complejo que implica voluntad política real de las partes.

Según el analista, este mensaje demuestra que en todo caso **el ELN mantiene la puerta abierta a la negociación **y que más que un problema de egos o de tecnicismos, hay una dificultad en conocer la voluntad política de las partes; por lo que [[desde diversos sectores](https://archivo.contagioradio.com/33-eurodiputados-piden-que-inicie-fase-publica-entre-eln-y-gobierno/)] se debe insistir en que las negociaciones inicien formalmente pues es un deber histórico del Gobierno incluir a esta guerrilla en el proceso para que se llegue a la paz completa.

<iframe src="http://co.ivoox.com/es/player_ej_12705109_2_1.html?data=kpekkpqVdJqhhpywj5aUaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1JDIqYy31tffx8aJdqSfotPOzs7XuMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ]
