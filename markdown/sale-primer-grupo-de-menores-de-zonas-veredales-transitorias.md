Title: Sale primer grupo de menores de zonas veredales transitorias
Date: 2017-03-04 23:26
Category: Nacional, Paz
Tags: Menores de edad, Zonas Veredales Transitorias de Normalización
Slug: sale-primer-grupo-de-menores-de-zonas-veredales-transitorias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG_2091.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [05 Mar 2017] 

Un comunicado del Comité Internacional de la Cruz Roja dio a conocer que ayer **salió el primer grupo de menores de edad de las zonas veredales transitorias, en el noroccidente del país. **El grupo fue recibido por una misión humanitaria conformada por delegados del Comité Internacional de la Cruz Roja y otras organizaciones designadas y fueron entregados a un  equipo de la UNICEF, en el centro transitorio de acogida.

Cristhop Harnish afirmó que los menores de edad fueron valorados por el personal médico que acompañaba la misión para **"garantizar que se encontraban en condiciones de ser trasladados al centro de acogida". **

Este seria el segundo grupo de menores que sale de las filas de la guerrilla de las FARC, y bajo los **últimos ajustes hechos entre esta guerrilla y el gobierno para garantizar el respaldo y reincorporación de los menores a sus entornos familiares**. Le puede interesar: ["Protocolo de reincorporación para menores de las FARC fue improvisado"](https://archivo.contagioradio.com/el-protocolo-de-reincorporacion-para-menores-que-salieron-de-farc-ep-fue-improvisado/)

Algunos de los problemas que se presentaron con el primer grupo tendrían que ver con la **ausencia o falta de adecuación institucional que permita la atención integral y en calidad de víctimas de los menores de edad. **Le puede interesar: ["FARC denuncia incumplimientos del gobierno en reincorporación de menores"](https://archivo.contagioradio.com/farc-denuncia-incumplimientos-del-gobierno-reincorporacion-menores-edad/)

Según el acuerdo se debe **garantizar que los menores estén acompañados por sus familiares, que reciban atención psicológica y que se les garantice el goce efectivo de todos sus derechos**, como la educación, la salud y a un ambiente sano. Se espera que con este nuevo gesto de voluntad política por parte de las FARC-EP y el gobierno nacional se siga avanzando en el cumplimiento de los acuerdos de paz en los términos en que se estableció.

###### Reciba toda la información de Contagio Radio en [[su correo]
