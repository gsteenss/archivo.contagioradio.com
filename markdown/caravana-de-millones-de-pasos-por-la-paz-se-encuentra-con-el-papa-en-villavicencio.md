Title: Caravana de "Millones de pasos por la paz" se encuentra con el Papa en Villavicencio
Date: 2017-09-07 15:17
Category: Nacional, Paz
Tags: víctimas del conflicto armado, Visita Papa Francisco
Slug: caravana-de-millones-de-pasos-por-la-paz-se-encuentra-con-el-papa-en-villavicencio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 Sept 2017] 

Este 7 y 8 de septiembre se llevará a cabo la caravana “Millones de pasos por la paz” una jornada de movilización que busca posicionar, en la visita del papa los mensajes por una reconciliación de corazón, la vida sagrada y la participación social.

La inciativa, que hace parte de las actividades de la semana por la paz, ha sido impulsada por organizaciones sociales, víctimas del conflicto armado, comunidades de fe, partidos políticos, ciudadanos comprometidos con la construcción de la paz, **la defensa de los derechos humanos y la justicia.**

La caravana además tiene el objetivo de hacer un llamado frente a la falta de cumplimiento por parte del gobierno nacional en los acuerdos pactados con las FARC, apoyar el cese bilateral firmado entre el Ejército de Liberación Nacional y el gobierno; que no haya más asesinatos a líderes sociales y defensores de derechos humanos y que con la visita del Papa a **Colombia se de paso a una reconciliación, al respeto por la vida y la participación social para alcanzar la paz**.

Se espera que más de dos mil personas se unan a esta caravana que tendrá puntos de salida desde departamentos como el Cauca, Tolima, Valle, Santander, Norte de Santander, Boyacá, Casanare y Meta, para llegar a Villavicencio el 8 de septiembre, lugar que visitará el Papa para participar del evento **“Encuentro con los hombres y la naturaleza”**. (Le puede interesar: ["El Papa nos está devolviendo a lo central del Evangelio"](https://archivo.contagioradio.com/46343/))

De acuerdo con el cronograma e**stablecido desde las 3 de la mañana la carava y los habitantes de Villavicencio** se encontrarán para realizar la caminata hasta la misa campal, hacia las 9:00 am se espera que de inició a la eucaristía, posteriormente se retomará la caminata hasta el polideportivo del Barrio 2000, para realizar una cadena humana mientras el Papa realiza su recorrido. (Le puede interesar ["Reconciliación será el centro de la semana por la paz"](https://archivo.contagioradio.com/semana-por-la-paz/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
