Title: Así fue el plantón por Sergio Urrego
Date: 2014-10-23 22:41
Author: CtgAdm
Category: LGBTI
Tags: DDHH, LGBTI, Sergio Urrego
Slug: asi-fue-el-planton-por-sergio-urrego
Status: published

###### Foto: ulestudiantil.org 

El viernes 12 de Septiembre Otra Mirada estuvimos en el plantón que se realizó en el colegio gimnasio ¨El Castillo campestre¨ por el suicidio del joven libertario Sergio Urrego.

En el mismo lugar donde fue víctima de matoneo por parte de la dirección, y que ahora está en el punto de mira de la fiscalía, se encontraron distintos colectivos LGBTI, para reivindicar la memoria de Sergio y para dejar claro que no se pueden volver a repetir semejantes actos de homofobia por parte de instituciones educativas, así como en la sociedad colombiana en general, exigiendo así a las distintas autoridades que actúen con firmeza ante manifestaciones homofóbicas.

Por último acudieron sus compañeros del colectivo del cual era miembro Sergio, es decir del ULE, Unión libertaria estudiantil.

El colectivo reivindicó la memoria del compañero y explicó que no solo fue perseguido por homosexual sino también por anarquista y culpó a la propia ministra de educación.

Adjuntamos dos entrevistas, la primera reportamos lo sucedido en el plantón y escuchamos a los familiares contarnos sobre el caso y la lucha que han iniciado, en la segunda podemos escuchar distintas voces opinando sobre la homofobia y la memoria de Sergio Urrego.
