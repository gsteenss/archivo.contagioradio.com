Title: Silencian la voz de denuncia de Samuel Gallo, líder comunitario en Antioquia
Date: 2019-01-26 12:59
Author: AdminContagio
Category: Comunidad, DDHH, Líderes sociales
Tags: Antioquia, defensores de derechos humanos, El peñol, Lider social
Slug: samuel-gallo-lider
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Diseño-sin-título-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Ene 2019] 

Este viernes fue denunciado el asesinato de **Samuel Gallo**, líder comunitario de **El Peñol, Antioquia,** quien era reconocido por su voz de denuncia sobre hechos de corrupción en el municipio. Según información de medios locales, Gallo era reconocido por su liderazgo comunitario, situación que lo llevó a ser candidato en dos ocasiones al Concejo de ese Municipio. (Le puede interesar:["Lideresa Maritza Ramírez muere tras ser víctima de ataque en Tumaco"](https://archivo.contagioradio.com/lideresa-maritza-ramirez-pierde-vida-tras-ser-victima-ataque-tumaco/))

De acuerdo a las primeras versiones de los hechos, el asesinato lo habrían cometido dos jóvenes quienes después de ingresar a la Veterinaria del líder, le habrían propinado varias heridas con arma blanca. Ambos jóvenes, uno de ellos menor de edad, ya estarían en manos de la Policía, que adelanta la investigación para determinar los móviles del asesinato. (Le puede interesar: ["Tumaco, municipio con mayor número de líderes asesinados desde 2016"](https://archivo.contagioradio.com/cauca-narino/))

No obstante, según Oscar Yesid Zapata, integrante de la Coordinación Colombia, Europa, Estados Unidos (COEUROPA) nodo Antioquia, hay situaciones que rodean el crimen y generan preocupación, "sobre todo por **el intento de las autoridades de desviar el crimen, como se quiere desviar** el asesinato de la lideresa [Maritza](https://archivo.contagioradio.com/lideresa-maritza-ramirez-pierde-vida-tras-ser-victima-ataque-tumaco/)". En primer lugar, porque según las autoridades, Samuel no tenía nivel de liderazgo, cuando toda la comunidad es testigo de su labor; situación que resulta sospechosa.

En segundo lugar, porque algunas las autoridades como la Policía, señalan que se trató de un intento de hurto, mientras que la misma comunidad logró recuperar unos videos cercanos al lugar de residencia de Gallo, en los que se evidencian el seguimiento que tenía; adicionalmente, un familiar del líder afirmó que él le había dicho que estaba siendo seguido, y que **estaba preocupado por las recientes denuncias de corrupción en la Alcaldía que había realizado.**

### **La situación de los líderes de Antioquia en contexto** 

Con Gallo, **Antioquia** ya ha perdido a dos líderes este año; y según la Defensoría del Pueblo, **junto a Chocó, Cauca y Nariño son los departamentos en los que se presentan mayor número de asesinatos de líderes sociales.** Sobre esta situación se pronunció recientemente la **Comisión Interamericana de Derechos Humanos (CIDH),** haciendo [11 recomendaciones](https://archivo.contagioradio.com/cidh-presenta-recomendaciones-proteger-los-lideres-sociales/) al Estado colombiano, para proteger la vida de estas personas.

Según relata Zapata, la respuesta estatal en el Bajo Cauca Antioqueño ha sido la militarización del territorio, pero dicha **militarización está en cabeza de un general que podría estar involucrado en casos de ejecuciones extrajudiciales,** situación que deja demasiadas preguntas respecto a la idoneidad del general, así como la eficacia de la medida para proteger a los líderes.

Por otra parte, el integrante de COEUROPA afirma que **"el Estado desconoce, o quiere desconocer, posibles nexos entre actores militares y estructuras que atentan contra los líderes",** razón por la que las acciones que se están realizando no están bien encaminadas; a ello se suma que no hay un canal de confianza donde las comunidades puedan hacer denuncias sobre los hechos que las afectan.

### **"El papel del Estado está muy por debajo de lo que las circunstancias requieren"** 

Estas situaciones de riesgo han provocado que los líderes ya no quieran continuar con su labor: "Los líderes han optado por su silencio, como medida de autoprotección", medida que, continua Zapata, es muy peligrosa porque el silencio de los líderes impide que se sepa lo que ocurre en los territorios, afectando el desarrollo mismo de la democracia. (Le puede interesar: ["¿Por qué renuncian los líderes sociales y qué hacer para detenerlo?"](https://archivo.contagioradio.com/renuncian-lideres-que-hacer-evitarlo/))

> [\#Infografía](https://twitter.com/hashtag/Infograf%C3%ADa?src=hash&ref_src=twsrc%5Etfw) Patrones en asesinatos de líderes sociales en Colombia [pic.twitter.com/ZaLNVTadWG](https://t.co/ZaLNVTadWG)
>
> — Contagio Radio (@Contagioradio1) [23 de enero de 2019](https://twitter.com/Contagioradio1/status/1088079976912560128?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
