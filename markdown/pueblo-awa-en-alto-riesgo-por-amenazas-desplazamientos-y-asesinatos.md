Title: Pueblo Awa en alto riesgo por amenazas, desplazamientos y asesinatos
Date: 2020-08-24 13:30
Author: AdminContagio
Category: Actualidad, DDHH
Tags: #Awá, #GruposArmado, #Nariño, #PuebloIndígena
Slug: pueblo-awa-en-alto-riesgo-por-amenazas-desplazamientos-y-asesinatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Awa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Organizaciones sociales y congresistas denuncian la difícil situación del Pueblo Awá, contra quienes el riesgo se ha incrementado por aumento de violencia en Nariño. De acuerdo con la Unidad Indígena del Pueblo Awá, desde la firma del Acuerdo de paz se reportan 40 asesinatos de líderes de esta comunidad, **100 amenazas y más de 7 desplazamientos, con un registro de 800 personas que tuvieron que salir del territorio**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situaciones, que según la comunidad indígena se han recrudecido en el marco de la pandemia del Covid 19 ya que desde principios de 2020 a la fecha se han presentado **30 hechos violentos en contra del Pueblo Awá**, "ocasionados por grupos armados legales e ilegales".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre esas acciones se encuentran 11 asesinatos, 2 masacres, 9 desplazamientos masivos, 3 extorsiones, 2 enfrentamientos armados en los que ha quedado inmersa la población civil y un hecho de hostigamientos. (Le puede interesar: ["Unidad Indígena del Pueblo Awá"](https://www.facebook.com/unidadindigenadelpuebloawa.unipa/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

La violencia en contra de las mujeres y jóvenes Awá
---------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De igual forma, la violencia en contra de las mujeres Awá también se ha intensificado. Según las denuncias de la UNIPA, en lo corrido del año, **7 lideresas** fueron **amenazadas**, y se han presentado casos de violaciones sexuales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo la comunidad afirma que los actos de terror han sido recibidos con mayor fuerza por la población joven de la comunidad Awá, con hechos como el reclutamiento forzado y allanamientos ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, la Organización Cabildo Mayor Awá de Ricaurte, denuncia la masacre perpetrada el pasado 18 de Agosto en la comunidad del Aguacate. **En donde** asesinaron **tres jóvenes, en el resguardo de Pialapi, Pueblo Viejo.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Los grupos armados legales e ilegales, el terror del Pueblo Awá
---------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones sociales y congresista, denuncian que en el territorio actualmente hacen presencia **10 grupos armados ilegales** que, en medio de la pandemia, amedrantan constantemente a la comunidad, roban sus alimentos y pertenencias. Igualmente, intimidan a la Guardia Indígena que intenta establecer controles de seguridad para disminuir el riesgo de contagio de Covid 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otro lado, el Ejército y la Fuerza Pública, también estarían haciendo caso omiso de estos controles, atemorizando a la comunidad indígena. Producto de estas situaciones, tanto las organizaciones sociales como congresistas exigen al presidente Iván Duque el cumplimiento de los Acuerdos de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que en el Capítulo Étnico, el Pueblo Awá está contemplado como sujeto colectivo de derechos priorizado y por lo tanto debe garantizarce su protección. (Le puede interesar: "[Pueblo indígena Awá fue víctima de una nueva masacre"](https://archivo.contagioradio.com/pueblo-indigena-awa-fue-victima-de-una-nueva-masacre/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo solicitan que se avance en el establecimiento de la mesa de concentración con el Pueblo Awá, presedida por la vicepresidenta Martha Lucía Ramírez. También exigen al Ministerio de Defensa que cesen los allanamientos en contra de la comunidad y los operativos de erradicación forzada. Finalmente, piden a todas las instituciones gubernamentales a que cumplan y garanticen la protección de esta comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
