Title: Sí…pero no… bueno sí
Date: 2016-09-01 09:49
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: plebiscito por la paz, proceso de paz
Slug: el-si-en-el-plebiscito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/SÍ.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/dt.common.streams.StreamServer-e1475007648772.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Por [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)**

###### 1 Sep 2016 

Indudablemente no se puede dar la espalda a la historia. La finalización de los diálogos de paz de La Habana y la emisión de un acuerdo final sorprende no sólo por lo estructurado del texto final, por sus posibilidades políticas o por lo complementario e incluyente, sino que sorprende además por el gran apoyo internacional y velocidad con que se ha llegado a una conclusión alentadora.

Por supuesto, es un arma de doble filo jugar a la “democracia del último que llegue” es decir, que luego de que un grupo de fuerzas beligerantes y políticas dialogan durante varios años, acuerdan punto por punto, redactan documentos, cierran y liman asperezas, al final, personas que no tienen una cultura política o una cultura de la lectura voten sí o no a dichos acuerdos y terminen definiendo… eso constituye un riesgo complejo, pues con cultura política o cultura de la lectura, no me refiero a ese ideal fetichizado con el que algunos sueñan una sociedad colombiana con atributos de la alta alcurnia europea, ¡no es eso!, no obstante para nadie es un secreto, que aquí mayoritariamente se consume propaganda política y causa inquietud que por el afán de ponerle el rótulo de “democracia” todo terminara en un proceso típico electoral en manos de los manipuladores de siempre.

Es un arma de doble filo porque caer en el marketing político frente a un problema tan serio o tomar a la ligera un acuerdo tan bien redactado es riesgoso. No obstante, cuando uno se detiene a pensar en el miedo frente a estos riesgos, la esperanza de la gente más cercana al conflicto, transforma el miedo en admiración. Es verdad, admiro esa apuesta, de seguro ganará el sí. No se trata de pronósticos, sino que se trata de ser consecuente con el momento histórico que se vive hoy, un momento en el que acertadamente, está mediando la esperanza por sobre el miedo, la esperanza por sobre la jurisprudencia, la esperanza como una opción que hace rato no les ofrecían a las personas que han vivido el trágico conflicto del país, y a la sociedad colombiana en general.

Es evidente que la paz es un ideal, es un estado hacia el cual debe tender la sociedad colombiana más allá de lo que hayan acordado las Farc y el gobierno Santos; me parece adecuado apoyar esas posiciones en las cuales se aboga por la distinción conceptual entre palabras como guerra, paz, acuerdos, violencia y saber que un acuerdo es una puerta, pero no el paraíso, saber que aún queda guerra en el país, pero que su impacto negativo matemáticamente y humanitariamente se reducirá; saber que la violencia objetivamente no la genera la guerrilla sino el modelo económico. Es importante, saber con certeza que tenemos que mirar al Chocó, al Putumayo, a la Guajira, al Cauca, y a todos los pueblos que hoy están levantados en paros cívicos luchando cuerpo a cuerpo contra el Esmad, que sus luchas son valiosas, que no están olvidados, que no se trata de abrazarnos y llorar de felicidad, sino que se trata de abrazarnos y prepararnos para una lucha más compleja, larga, pero definitivamente en un escenario que no sea el conflicto armado a la escala en la que se encontraba hace unos años.

Obviamente lo que impulsa la esperanza es abrir esa puerta, ¿Qué pasará después? Bueno, la historia de Colombia nunca había tenido en sus manos un acuerdo y un documento de este calibre, por ese motivo, es altamente positivo no sólo abrir la puerta sino traspasarla y vivir en el marco de las nuevas luchas políticas aquello que pueda impulsar un nuevo modelo de sociedad, unas nuevas estrategias de consecución de poder popular, nuevas estrategias de organización social etc.

Bueno y entonces… ¿Salir a votar? Sí porque la coyuntura histórica nos ofrece una posibilidad de nuevos escenarios políticos, escenarios políticos que si luchamos por estructurarlos, pueden ofrecernos futuros maravillosos… aunque mejor no porque la paz es muy difícil de lograr… pero bueno sí, porque me reconozco en las víctimas, en los combatientes, en aquellos que han sufrido una penosa guerra y que están apostándole con esperanza a abrir una puerta que ha costado construirla… bueno sí, porque es necesario que el escenario político de posconflicto genere una nueva probabilidad histórica para la sociedad, total, ¿no le parece triste que ni siquiera existiera esa probabilidad? … ¡Bueno sí!
