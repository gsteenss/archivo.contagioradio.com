Title: Siga en vivo el lanzamiento del libro 'El Aprendiz del Embrujo'
Date: 2019-09-25 09:43
Author: CtgAdm
Category: DDHH, Nacional
Tags: El Aprendiz del Embrujo, Iván Duque
Slug: siga-en-vivo-el-lanzamiento-del-libro-el-aprendiz-del-embrujo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Aprendiz-del-Embrujo-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ccajar] 

Más de 500 organizaciones sociales y no gubernamentales, agrupadas en tres plataformas de derechos humanos, presentan ante al Parlamento Europeo y en seis departamentos del país, 'El aprendiz del Embrujo', un balance en términos de derechos humanos del primer año de gestión del presidente Iván Duque Márquez.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F513530369446235%2F&amp;width=800&amp;show_text=false&amp;appId=1237871699583688&amp;height=413" width="800" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A propósito de los 16 años que se cumplen del lanzamiento del 'Embrujo Autoritario' que analiza el primer año de gobierno, el conflicto y las brechas sociales en el país bajo la presidencia de Álvaro Uribe y la actual de llegada de Iván Duque al poder, Alberto Yepes, integrante de la Coordinación Colombia Europa Estados Unidos (Coeuropa) señala que una vez más este Gobierno es proclive a favorecer intereses corporativos y privados, implantando dinámicas de guerra en los territorios.

Las objeciones a la Jurisdicción Especial para la Paz, la instalación de redes de informantes en las regiones, autorizar el  portes de armas,  propuestas como el regreso a alcaldías militares y una notable expansión del paramilitarismo son muestra de este retroceso en materia de derechos humanos.

Para Yepes,  sin embargo existen algunas diferencias con respecto al Embrujo original, ligadas a **una profundización del modelo corporativo vinculado a la expansión de economías extractivistas** en gran parte del territorio nacional, que cuentan el favorecimiento de grandes conglomerados económicos, los mismos que hoy favorecen a la bancada del Gobierno.

De igual forma señala que desde el Gobierno, se ha adoptado un lenguaje que simula un compromiso con la paz  y los derechos humanos con el  objetivo de ganar el favor de la comunidad internacional,  "como si cambiando su nombre, cambiaran su función", ejemplo de ello, indica Yepes es  la red de informantes ahora red de participación,  que resulta ser una reproducción de la seguridad democrática.

Pese a ello, el defensor de DDHH reconoce que la sociedad que atestiguo el Embrujo Autoritario original, es diferente a la que hoy encuentra Iván Duque en el poder, **"hay un movimiento que clama por volver a la paz, defender los acuerdos y extenderlos, hay un movimiento de víctimas muy vigoroso que se ha hecho sentir y además hay avances en la representación política con una bancada por la paz"**, destaca.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
