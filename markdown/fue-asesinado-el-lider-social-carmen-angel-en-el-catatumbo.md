Title: Fue asesinado el líder social Carmen Ángel en el Catatumbo
Date: 2020-06-23 20:51
Author: CtgAdm
Category: Actualidad, DDHH
Tags: asesinato de líderes sociales, Catatumbo, juntas de acción comunal
Slug: fue-asesinado-el-lider-social-carmen-angel-en-el-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Carmen-Ángel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Líder Carmen Ángel Angarita@CLAROJURE

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En horas de la noche de este martes 23 de junio se conoció el asesinato del líder social **Carmen Ángel Angarita** en zona rural de Convención, Norte de Santander, quien fungía como presidente de la Junta de Acción Comunal (JAC) de la vereda El Hoyo. Según el Instituto de Estudios para el Desarollo y Paz [(Indepaz)](https://twitter.com/Indepaz), con su homicidio ya son 140 los defensores de DD.HH. asesinados en el 2020, 54 desde que fue iniciada la cuarentena a finales de el mes de marzo. [(Le recomendamos leer: Sigue la muerte, durante el fin de semana asesinan tres líderes y excombatientes)](https://archivo.contagioradio.com/sigue-la-muerte-durante-el-fin-de-sesmana-asesinan-tres-lideres-y-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que aún son desconocidos las causas y los sucesos en los que el líder fue asesinado, comunidades que habitan en Catatumbo, Norte de Santander, vienen denunciando la difícil situación humanitaria por cuenta de la confrontación entre grupos armados legales e ilegales, **incluidos el EPL y el ELN, además de la presencia de la Fuerza de Tarea Vulcano y la Fuerza de Despliegue Rápido FUDRA N°3,** quienes permanecen en la región aunque las comunidades han manifestado no sentirse respaldadas. Cabe resaltar que Convención fue lugar en el que fue asesinado a manos de integrantes del Ejército el excombatiente Dimar Torres.  [(Le puede interesar: 167 líderes indígenas han sido asesinados durante la presidencia de Iván Duque : Indepaz)](https://archivo.contagioradio.com/167-lideres-indigenas-han-sido-asesinados-durante-la-presidencia-de-ivan-duque-indepaz/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Carmen Angel, otro integrante de JAC asesinado

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hasta el pasado 8 de junio habían sido asesinados un total de ocho líderes sociales, una cifra que se elevó durante el primer puente festivo del mes en el que también fue asesinado Mauricio Antonio Uribe, miembro de la Junta de Acción Comunal (JAC) de la vereda El Carmen del municipio de Remedios, Antioquia. [(Le recomendamos leer: Liderazgos sociales en alerta máxima. En lo corrido de Junio van 7 asesinatos)](https://archivo.contagioradio.com/liderazgos-sociales-en-alerta-maxima-en-lo-corrido-de-junio-van-7-asesinatos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Guillermo Cardona, fundador de la Confederación Nacional de Acción Comunal (JAC) en menos de ocho días también fueron asesinados el presidente de la JAC de Santa Isabel, José Ernesto, Eider Lopera, vicepresidente de la JAC de Popales en Tarazá Antioquía, Mauricio Antonio Uribe de la JAC El Carmen, en Remedios, Antioquia y Concepción Corredor, presidenta JAC La Pradera en Casanare el pasado 22 de junio. [(También puede leer: En lo corrido de mayo han sido asesinados cinco integrantes de juntas de acción comunal)](https://archivo.contagioradio.com/en-lo-corrido-de-mayo-han-sido-asesinados-cinco-integrantes-de-juntas-de-accion-comunal/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El dirigente campesino atribuye el incremento de la violencia contra integrantes de juntas de acción comunal a la intensificación erradicación forzada y la persecución del campesino a través del accionar del Ejército, campesinos de la región y el área rural de Cúcuta, en Norte de Santander, han pedido al Gobierno que cese las erradicaciones forzadas la erradicación y que esta sea de la mano de los campesinos, gradual y que esté acompaña de proyectos productivos. [(Alejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT)](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->
