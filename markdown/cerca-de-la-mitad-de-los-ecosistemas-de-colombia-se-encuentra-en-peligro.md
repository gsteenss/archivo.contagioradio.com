Title: Cerca de la mitad de los ecosistemas de Colombia se encuentran en peligro
Date: 2017-08-16 16:21
Category: Voces de la Tierra
Tags: Ecosistemas, Instituto alexander von Humbolt
Slug: cerca-de-la-mitad-de-los-ecosistemas-de-colombia-se-encuentra-en-peligro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/maravillas-naturales.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Ago 2017] 

Hace unos meses se viene alertando de la situación que se presenta con el desarrollo del proyecto hidroeléctrico Hidroituango en Antioquia, donde empresas Públicas de Medellín, (EPM) está talando más de 4.000 hectáreas de bosque seco tropical. Un hecho que evidencia, por qué practicamente la mitad de los distintos ecosistema de Colombia se encuentran en peligro.

A esa conclusión llegaron investigadores de la Universidad Javeriana, el Instituto Alexander von Humbolt,  y Conservación Internacional, quienes desde hace 4 años vienen analizando los impactos de las transformaciones en la naturaleza causadas por las diferentes actividades del ser humano. ** **La situación es alarmante: ** de los 81 ecosistemas de Colombia, 38, es decir el 46% se encuentran categorizados en Peligro Crítico y En Peligro.**

La Lista Roja de Ecosistemas, es el documento en el rezan las cifras que justamente muestran que el **Bosque Seco Tropical** (que en Antioquia está siendo talado por EPM), **y el Desierto Tropical; así como las áreas de Bosque Húmedo Tropical del Piedemonte Llanero,** son los tipos de ecosistemas más amenazados.** **[(Le puede interesar: EPM inició tala de 4500árboles) ](https://archivo.contagioradio.com/epm-inicio-tala-de-4-500-hectareas-de-bosque-seco-tropical/)

Según ese informe, los **ecosistemas en Peligro Crítico son 19, la misma cantidad están en la categoría En Peligro;  como Vulnerables se registran 17; en grado de Preocupación Menor hay 26 y un 4 % de los ecosistemas se encuentran en Peligro Crítico** y pertenecen al Sistema Nacional de Áreas Protegidas (SINAP). Respecto a la misma categoría de amenaza un 53 % están en zonas de resguardos indígenas y comunidades afrodescendientes.

De allí que **el Caribe y los Andes **tengan  "las mayores áreas de ecosistemas amenazados por los procesos de transformación y serían aquellas con necesidades de restauración urgente”, como dice el informe.

### ¿Por qué? 

De acuerdo con el reporte Humbolt de biodiversidad del 2015. **Un 34% de los ecosistemas ha sido objeto de tranformaciones,** un fenómeno que en buena medida es el resultado de la expansión de la frontera agrícola.

Esa investigación señala que "la transformación de los ecosistemas es un proceso acumulativo, que responde al asentamiento humano y al aprovechamiento de los recursos forestales, agrícolas y mineros. Así mismo, obedece al desarrollo de la infraestructura física de soporte, cuya dinámica implica que los ecosistemas originales son parcial o completamente reemplazados por paisajes antropogénicos".

El análisis multi-temporal de orden nacional realizado por la Universidad Javeriana, que abarca los últimos cuarenta y cinco años (1970-2014) y encontró que, para el año 2014, el porcentaje de **pérdida de ecosistemas naturales es de 37,5% en bosques, 24,9% en sabanas y 15,9%, en páramos.** [(Le puede interesar: Ecosistemas de Colombia son víctimas de la intervención humana)](https://archivo.contagioradio.com/dia-de-la-ecologia-ecosistemas-colombia/)

### Falta protección 

En medio de ese panorama, [la] Lista Roja de Ecosistemas presentada este miércoles, alerta sobre la falta de protección institucional de la gran variedad de ecosistemas que hay en el país. El documenta advierte que justamente los ecosistemas en mayor riesgo  están mal representados en el Sistema Nacional de Áreas Protegidas. Exactamente 22** **ecosistemas no cuentan con ningún tipo de representación y 17 con menos de un 5 %, esto quiere decir que el **48 % no está cobijado bajo esa categoría de protección. **

Los investigadores señalan que con esta lista lo que se busca es brindar la información necesaria para que el gobierno y sus diferentes instituciones actúen frente a tal escenario. **El objetivo es promover la conservación y el desarrollo sostenible**, así como generar alertas tempranas, analizar con argumentos la toma de decisiones por parte de funcionarios del gobierno y  ayudar a construir resiliencia frente a las consecuencias del cambio climático. ([Le puede interesar: 5 ecosistemas emblemáticos de Colombia amenazados)](https://archivo.contagioradio.com/los-5-ecosistemas-emblematicos-mas-amenazados-de-colombia/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
