Title: "Es necesario modificar el trámite de la extradición en Colombia"
Date: 2015-10-08 13:09
Category: DDHH, Nacional
Tags: alirio uribe, Audiencia pública sobre ley de extradición, Comité de Solidaridad con presos pol, Conflicto en Colombia, Estados Unidos Ley de extradición, Ley de extradición en Colombia, Por la Soberanía No a la extradición, Victor Correa Vélez, Violación de derechos humanos niños
Slug: es-necesario-modificar-el-tramite-de-la-extradicion-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Extradición.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: lainformacion] 

<iframe src="http://www.ivoox.com/player_ek_8866314_2_1.html?data=mZ2jmJiVeI6ZmKiakpuJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidSfz8rQx9jFtsrjjNLcxs7KrcTV05DSzpDYtoa3lIquk9LNuMafxcqYzsaPqdno08bRy8jNaaSnhqeg0JDJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gloria Silva, Comité de Solidaridad con los Presos Políticos] 

###### [08 Oct 2015] 

[Este viernes se llevará a cabo desde las 9:00 am en el auditorio Luis Guillermo Vélez del Congreso de la República, una audiencia pública sobre el Proyecto de Ley 009 presentado el pasado mes de julio y que pretende la **modificación del trámite de extradición de nacionales colombianos**, en el marco de la campaña “Por la Soberanía No a la extradición”.]

[Gloria Silva, abogada defensora de derechos humanos e integrante de la Fundación Comité de Solidaridad con los Presos Políticos, asegura que ni el proyecto de acto legislativo ni la Campaña pretenden la eliminación de la figura de extradición, lo que se busca es que esta Ley sea regulada pues **su actual diseño y aplicación viola los derechos humanos de los extraditados** **así como de quienes integran sus núcleos familiares**, principalmente niños, niñas y adolescentes.   ]

[Silva, afirma que frente a los nacionales pedidos en extradición se están cometiendo gravísimos errores judiciales, por lo que es urgente que en Colombia haya una modificación de la Ley que posibilite un marco jurídico para **garantizar los derechos tantos de las víctimas como de los extraditables** y sentar precedentes, pues cuando los nacionales pedidos en extradición son entregados a otros países **el Estado colombiano se desprende de ellos** “casi como sí se perdiera la ciudadanía colombiana cuando se van a otros países”.  ]

[Debido a que EEUU es el país que más solicita colombianos en extradición, la abogada indica que la Campaña está tratando de generar puentes entre Organizaciones no Gubernamentales y la academia internacional para **garantizar que lo adelantado en Colombia tenga eco en la jurisdicción norteamericana**.  ]

[Esta audiencia pública pretende movilizar la reflexión colectiva de la sociedad civil colombiana y de sus autoridades en torno a la necesidad de modificar la ley de extradición en Colombia para la **reivindicación de las garantías de derechos de quienes son extraditados, el fortalecimiento de la soberanía nacional y la reparación de las víctimas del conflicto**.]

[![extradición\_contagioradio.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/IMG-20151006-WA0003.jpg){.aligncenter .wp-image-15502 width="431" height="567"}](https://archivo.contagioradio.com/es-necesario-modificar-el-tramite-de-la-extradicion-en-colombia/img-20151006-wa0003/)
