Title: 'Abrazatón' en defensa del Río Sogamoso
Date: 2015-03-25 17:34
Author: CtgAdm
Category: Ambiente, Nacional
Tags: abrazatón, Ambiente, hidrosogamoso, ISAGEN, Movimiento Ríos Vivos, represas, Rio Sogamoso, Santander
Slug: abrazaton-en-defensa-del-rio-sogamoso
Status: published

##### [Foto: veredasogamoso.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4263614_2_1.html?data=lZejlZuVeI6ZmKiak5WJd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcPmwt%2FO1oqnd4a2lNOY0tTWb8bgjNeSpZiJhaXjjLjcycbRs9TjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Claudia Patricia Ortíz, Movimiento Prodefensa del río Sogamoso] 

Por la defensa del territorio, la vida, el derecho al trabajo, y la alimentación, los santandereanos y santandereanas participarán este **25 de marzo a la 1:30 de la tarde, en “La abrazatón en defensa del Río Sogamoso”,** afectado por la construcción de la represa de Isagén.

En medio de la vulneración de derechos humanos y los daños ambientales producidos por Hidrosogamoso, las personas afectadas por la hidroeléctrica se plantarán en el Parque García Rovira en Bucaramanga, con el objetivo de “visibilizar y sensibilizar a los habitantes de la región sobre los impactos de la construcción de Isagén, ya que **se están tramitando otras licencias para construir otras represas”** dice Claudia Ortiz, afectada por la represa y vocera del Movimiento Ríos Vivos Colombia.

Algunos de los pobladores que antes vivían del río, siguen frente a la Gobernación de Santander, pidiendo soluciones del gobierno Nacional, ya que el gobierno local “dice que no es responsabilidad de ellos, porque no construyeron la represa”, afirma la Ortíz, quien agrega que la comunidad está cansada de la situación, por lo que piden una actuación rápida del Estado y **una comisión de verificación que evidencie la grave situación de las familias,** que es “muy diferente frente a lo que dice Isagén en los medios de comunicación”.

Claudia, al igual que el resto de personas, **exigen una reparación integral y piden la reubicación,** donde puedan volver a tener  la vida que llevaban antes de la construcción de la represa, ya que su economía se basaba en las dinámicas alrededor del río. “Queremos que el gobierno reconozca los **daños irreversibles a los ecosistemas de los ríos** con la construcción de represas”, expresa Claudia Ortíz.

Cabe recordar que, la **comunidad instauró una acción popular en 2014** con el fin de reclamar los daños causados al medio ambiente y los derechos de reparación colectiva, pues denuncian que las autoridades intentan ocultar las consecuencias sobre la pesca y los cultivos, con el fenómeno del cambio climático.
