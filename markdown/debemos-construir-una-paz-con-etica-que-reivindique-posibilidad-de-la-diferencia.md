Title: Expresiones religiosas, víctimas de exterminio y persecución hablan de paz
Date: 2015-11-17 13:44
Category: Nacional, Paz
Tags: Cristianismo hacia el amor eficaz, Diálogos de paz en la Habana, DIPAZ, III Encuentro Nacional de la Mesa Ecuménica por la Paz, Red CONPAZ
Slug: debemos-construir-una-paz-con-etica-que-reivindique-posibilidad-de-la-diferencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Encuentro-ecuménico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Evangelizadora de los Apóstoles 

<iframe src="http://www.ivoox.com/player_ek_9419704_2_1.html?data=mpmem5yUeI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRidnk08rgy9TSqdSf08rZy8zNs9TV1IqfpZDaaaSnhqaxxdnNscLnjMnSjcrcuMbmzs7by9SPvYzkxtfgx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Sacerdote misionero claretiano Henry Ramírez] 

###### [17 Nov 2015 ]

[Durante el 14, 15 y 16 de noviembre se realizó en la ciudad de Medellín el III Encuentro Nacional de la Mesa Ecuménica por la Paz ‘Cristianismo hacia el amor eficaz’, en el que distintas expresiones religiosas, víctimas de exterminio y persecución en el marco del conflicto armado colombiano y de diferentes partes del país, se dieron cita para constituirse como movimiento social y en esa medida, aportar a la construcción de paz con justicia y vida digna para Colombia.]

[De acuerdo con el sacerdote misionero claretiano Henry Ramírez, uno de los objetivos del encuentro fue aportar a la construcción de los criterios éticos para la conformación de la comisión de la verdad, teniendo en cuenta que el trabajo de la Mesa se ha orientado a fortalecer la unidad del movimiento ecuménico como actor social y político en el país pues "la iglesia de los pobres ha sido protagonista en distintos procesos sociales, acompañando las distintas comunidades campesinas".]

["Nos empezamos a preguntar cuáles eran los criterios éticos de las personas que van a formar parte de esa comisión... nos preguntábamos sí personas que estuvieran investigadas y relacionadas con crímenes, con investigaciones de corrupción podrían ser parte de la Comisión y creemos que no, nuestra tarea en los próximos días será elaborar una expresión pública de los criterios éticos de quienes deben conformar esta Comisión", indica el sacerdote.]

[De acuerdo con Ramírez, "si queremos construir una paz, ésta debe ser una paz con ética, una paz con justicia social, una paz que reivindique la posibilidad de la diferencia, como principio ético" y las personas que integren la Comisión "deben ser idóneas, deben ser reconocidas en el mundo de los derechos humanos... que tengan en cuenta el tema de la mujer, el tema de las identidades sexuales... tiene que ser una Comisión que se piense lo regional, lo cultural, lo religioso...sobre el principio de que la paz se construye con equidad social y de género".   ]

["Creemos que en el marco de lo que se viene dialogando en La Habana y de lo que se espera se dialogue con el ELN es necesario mostrar que este movimiento de iglesia de los pobres también ha sido victimizado y que reivindicamos nuestros derechos a la verdad, a la justicia y a la reparación... sumándonos a las iniciativas de paz de las comunidades que construyen paz desde los territorios a las que apoyamos y reivindicamos", concluye el sacerdote. ]
