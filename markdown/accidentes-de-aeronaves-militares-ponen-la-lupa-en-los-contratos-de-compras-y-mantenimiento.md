Title: Accidentes de aeronaves militares ponen la lupa en los contratos de compras y mantenimiento
Date: 2015-08-05 13:04
Category: Economía, Entrevistas, Paz
Tags: Accidentes Militares, cese unilateral de las FARC, Frente Amplio por la PAz, Iván Cepeda
Slug: accidentes-de-aeronaves-militares-ponen-la-lupa-en-los-contratos-de-compras-y-mantenimiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Black-Hawk-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: aviacol.net 

###### [5 Agosto 2015]

Los recientes accidentes de aeronaves militares y de policía en los que murieron 27 personas en la última semana despiertan dudas sobre los **contratos de compras y mantenimiento a las que deben someterse al trascurrir un tiempo medio de su vida útil**. El Senador Iván Cepeda afirma que se deben realizar todas las investigaciones pertinentes para esclarecer las causas de los siniestros.

El martes o miércoles de la semana entrante se realizará un debate de control político en el que se publicarán resultados de investigaciones en el sentido de **costos de mantenimiento y reparación de aeronaves militares,** vida útil y funciones que cumplen así como las adecuaciones a las que han sido sometidas las aeronaves que llegaron a Colombia desde 1988 como el caso de los primeros helicópteros Black Hawk.

En cuanto a las versiones sobre el posible ataque al helicóptero de la policía, el senador Cepeda afirma que **hay que apegarse a la información que entrega el gobierno** acerca de las causas que provocaron la caída de la aeronave y esperar que las investigaciones arrojen más datos.

Es importante resaltar que el Frente Amplio por la Paz, organización de la que hace parte Cepeda, no ha emitido ningún comunicado relacionado con un rompimiento del **cese unilateral de fuego de la guerrilla de las FARC,** vigente desde el pasado 20 de Julio.

### **Investigaciones preliminares dan cuenta de accidentes y no ataques externos** 

En cuanto al helicóptero **Black Hawk se precipitó a tierra a las 9:15 a.m. mañana de este martes cuando sobrevolaba la vereda Piedras Blancas**, zona rural de Carepa, en el Urabá antioqueño. En el accidente perdieron la vida 16 policías  y dos más resultaron heridos de gravedad.

El Ministro Villegas entregó los primeros resultados de la investigación y afirma que “*La hipótesis más probable es un choque del helicóptero contra la ladera a una velocidad de 120 nudos, que son 180 kilómetros por hora, posiblemente por visibilidad baja, porque el helicóptero se encontró absolutamente destrozado. La pieza más grande puede tener un metro de larga por 50 de ancha*”

Sobre las especulaciones de los senadores del Centro Democrático Álvaro Uribe y Alfredo Rangel, Villegas llama la atención y afirma que **no se pueden dar por ciertas las especulaciones,** “*Es un abuso con la Nación dar por cierto lo que es especulación*” concluye el Ministro.

**El Casa 235 FAC 1261 se accidentó el pasado 31 de Julio** en el municipio de Agustín Codazzi en el departamento del Cesar, con 11 tripulantes a bordo, entre estos un mayor de Ejército colombiano. Minutos antes del siniestro el piloto del avión reportó fallas en uno de los motores y luego se perdió la comunicación.

Según el Ministro de Defensa Luis Carlos Villegas, en el avión Casa 235 FAC 1261 que se accidentó el viernes pasado y en el que murieron 11 integrantes de las FFMM “***No hay explosión, pero sí hay evidencias de congelamiento**, es decir, formación de hielo alrededor de los planos, especialmente el plano derecho*".

El Ministro agrega que "*No hay nada que nos lleve a decir que este accidente esté asociado a una acción externa. La hipótesis más probable es la de condiciones meteorológicas que pudieron llevar a que el avión se volviera incontrolable*"
