Title: Inicia movilización indígena en Popayán para exigir libertad de Feliciano Valencia
Date: 2015-09-21 10:53
Category: DDHH, Nacional
Tags: ACIN, Captura Feliciano Valencia, Cauca, colombia, CRIC, José Hildo Peque, Militarización, Minga Indígena 2008, Movilización Indígena, Movimiento Indígena en Colombia
Slug: indigenas-se-movilizan-en-popayan-para-exigir-libertad-inmediata-de-feliciano-valencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ONIC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cms.onic.org.co] 

<iframe src="http://www.ivoox.com/player_ek_8509443_2_1.html?data=mZqdm5mYd46ZmKiakpyJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2FdxM7OjdLTusrgyt%2FOxc6Jh5SZo5jbjc7SqIa3lIqupszJssKfxtOYstTUpdqZpJiSo5bSb9HV08aYx93Nq8rmjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [José Hildo Peque, CRIC] 

###### [21 Sept 2015] 

[Desde las 10 de la mañana de este lunes, cerca de **200 indígenas entre autoridades, miembros del CRIC y de la Guardia Indígena, se movilizan pacíficamente desde la Universidad Autónoma Intercultural hacia la cárcel de San Isidro en Popayán**, dónde se encuentra recluido el líder indígena Feliciano Valencia.  ]

[El objetivo inmediato de la manifestación social es **instalar formalmente la asamblea permanente de la Guardia Indígena que estará presente en los próximos tres días a las afueras del centro penitenciario,** así como continuar denunciando el atropello del que está siendo víctima Feliciano, que de acuerdo con José Hildo Peque, Consejero Mayor del CRIC “es un hecho que toca a todo el movimiento indígena”.  ]

[Pese a que las autoridades indígenas han acordado que en la movilización no se presentarán acciones de hecho y que no entraran en confrontación con miembros de la fuerza pública, **los puntos de concentración de la movilización están militarizados, con unidades de la policía de tránsito y del ESMAD**.  ]

[La movilización se da en el marco de las directrices acordadas por la Junta Directiva Extraordinaria de Autoridades Indígenas, que tuvo lugar el pasado 17 de septiembre y en la que también se decidió la **reactivación de la Minga Social Indígena y Popular de 2008 (Ver [Más de 30.000 indígenas acuerdan reactivar la Minga de 2008](https://archivo.contagioradio.com/mas-de-30-000-indigenas-acuerdan-reactivar-la-minga-de-2008/)).**]

[De acuerdo con José Hildo “**Feliciano está motivado, sereno esperando las resoluciones de las autoridades tradicionales**” mientras que las comunidades y resguardos indígenas del Cauca continúan en asamblea permanente.]
