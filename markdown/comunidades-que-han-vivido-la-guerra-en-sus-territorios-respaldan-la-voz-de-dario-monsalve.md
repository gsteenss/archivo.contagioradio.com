Title: Comunidades que han vivido la guerra en sus territorios respaldan la voz de Dario Monsalve
Date: 2020-07-09 11:01
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: acuerdo de paz, Monseñor Dario Monsalve, Violencia en los territorios
Slug: comunidades-que-han-vivido-la-guerra-en-sus-territorios-respaldan-la-voz-de-dario-monsalve
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Dario-Monsalve.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @MonsRueda

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En recientes declaraciones, el arzobispo de Cali, Darío de Jesús Monsalve, señaló como responsables al presidente Iván Duque y su partido, de implementar una "venganza genocida para desmembrar" el Acuerdo de paz, algo que denuncia ha sido evidente con aumento de la violencia en el país. Aunque dichas palabras fueron cuestionadas por la Nunciatura Apostólica, comunidades, organizaciones expresaron su apoyo al religioso reiterando que la posición del Gobierno ha debilitado la implementación de lo pactado en La Habana, lo que se ha visto reflejado en los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Monseñor quien ha denunciado en múltiples ocasiones el conflicto y las agresiones contra la población rural y ha acompañado los procesos de las comunidades**, expresó que "desde los comienzos de la campaña electoral, se sentía un espíritu de venganza contra el Gobierno Santos que vislumbró estos procesos, un espíritu de venganza contra el pueblo que los acompañaba y, lo más grave, una venganza contra los mismos excombatientes de las Farc", **una posición que se ha visto fortalecida - indica - con la multiplicación de actores armados en los territorios.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/arzobispodecali/status/1278899714570756096","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/arzobispodecali/status/1278899714570756096

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Dichas acciones, señaló Monseñor Dario Monsalve, han tenido como fin "una venganza genocida para desvertebrar, desmembrar completamente la sociedad, las organizaciones sociales y la democracia en los campos y en los territorios". También se refirió a los diálogos con el ELN, lamentando que el gobierno del presidente Duque les diera fin, una decisión que generó una “gran frustración y una enorme incertidumbre” y que dificulta este tipo de acercamientos en el futuro. [(Lea también: ELN propone cese bilateral y el Gobierno cierra la puerta a esa posibildad)](https://archivo.contagioradio.com/eln-propone-cese-bilateral-y-el-gobierno-cierra-la-puerta-a-esa-posibildad/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Una precisión, no una deslegitimación?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, la Nunciatura Apostólica que representa al Vaticano en el país se apartó de sus declaraciones y precisó que el término “genocidio” usado por Monseñor en su declaración "tiene en el Derecho Internacional un significado que no permite sea usado a la ligera en los legítimos debates o discusiones públicas sobre las políticas concretas de un determinado gobierno”.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que su posición frente a la gestón del Gobierno, "no corresponde a la visión que la Santa Sede tiene de la compleja situación en que versan, en este momento, tanto la aplicación integral de los acuerdos de paz de 2016, como el estado de los contactos y conversaciones que a diversos niveles se mantienen con el ELN, en el ámbito de la comunidad internacional”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los territorios respaldan a Monseñor Dario Monsalve

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tras las declaraciones de la Nunciatura y en señal de apoyo, la [Comisión Interétnica de la Verdad del Pacífico](https://twitter.com/VerdadPacifico), que articula a más de 30 organizaciones étnico-territoriales de pueblos indígenas y afrocolombianos, anunciaron su respaldo a las declaraciones de del arzobispo de Cali, señalando que "reflejan el sentir y el pensar de nuestras comunidades en la región del Pacífico y de otros lugares de Colombia". [(Lea también: Con derecho de petición, Ejército exige a líderes del Chocó demostrar lo que denuncian)](https://archivo.contagioradio.com/derecho-de-peticion-ejercito-exige-que-lideres-demostrar-denuncias/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte señalan que el partido de Gobierno se ha concentrado en debilitar el Acuerdo através de ataques a los mecanismos de Justicia integral como la JEP, la Comisión de la Verdad y la Unidad de Búsqueda de Personas dadas por Desaparecidas, además de desviar el camino en términos de implementación de la reforma rural integral, la sustitución de cultivos de uso ilícito, la protección de las comunidades, el desmonte de estructuras herederas del paramilitarismo y la reincorporación de excombatientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades señalan que Monseñor Darío Monsalve Mejía ha sido coherente y fiel a esta exhortación del Papa Francisco quien durante su visita afirmó que los obispos deben ser una voz profética e independiente en medio del conflicto, "cómodo sería que se mantuviera indiferente y complaciente ante esta tragedia fratricida. Su fidelidad con la paz le ha significado una persecución del Centro Democrático, que desde hace varios años y de diversas formas, le ha hecho acusaciones calumniosas para acallar su voz y deslegitimar su palabra profética", agregan.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
