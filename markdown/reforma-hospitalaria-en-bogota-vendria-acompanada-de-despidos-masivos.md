Title: Reforma hospitalaria en Bogotá vendría acompañada de despidos masivos
Date: 2016-08-02 14:40
Category: DDHH, Nacional
Tags: reforma salud Bogotá, secretario de salud, Sindicado Nacional de La Salud y Seguridad Social
Slug: reforma-hospitalaria-en-bogota-vendria-acompanada-de-despidos-masivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Crisis-hospitalaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diario Bogotano] 

###### [2 Ago 2016] 

En febrero de este año el Sindicado Nacional de La Salud y Seguridad Social denunció que la reforma a la Red Pública Hospitalaria de Bogotá anunciada por el Secretario de Salud y puesta en marcha este lunes, sería el **paso final para la privatización del derecho a la salud en la capital y posiblemente en el país**, denuncia que sostiene María Doris González, presidenta del sindicato, quien agrega que este modelo de salud en el que los 22 hospitales se integrarán en 4 subredes no es conveniente para la ciudad por su extensión territorial.

"Al Secretario de Salud se le olvida que el modelo de salud existente lo implementó él hace 17 años, **él mismo fue el que dejó 22 hospitales de los 32 hospitales que habían**, entonces él viene ahorita, como renovado, a decir que eso no le sirve ya, viene a hablar mal de los gerentes y resulta que los gerentes que habían hace 16 años siguieron trabajando en la red pública" afirma González e insiste en que las pretensiones reales son las de dejar en manos de los privados la prestación de servicios especializados en los hospitales, cuando son responsabilidad del distrito.

Desde el sindicato se denuncia también que esta reforma vendrá acompañada de **despidos masivos de por lo menos 7 mil personas que venía trabajando desde hace 15 años** en los hospitales, así como de la apertura de centros de atención inmediata a los llegarán los pacientes para que les digan a dónde deben trasladarse, acrecentándose la barreras de acceso a los servicios, en suma, "[[se desmejorarán los servicios de salud](https://archivo.contagioradio.com/distrito-reducira-de-164-a-40-los-centros-basicos-hospitalarios/)] y las condiciones laborales", como asevera González.

Para los trabajadores de la salud resulta muy peligroso que se separe lo administrativo de la prestación del servicio como tal, pues la responsabilidad frente a lo que suceda con los pacientes recaerá sobre los médicos y enfermeros sin que se tenga en cuenta la labor de los operadores que tienen que responder con los insumos y medicamentos de los cuales depende la atención en los hospitales. Frente a [[lo que viene sucediendo](https://archivo.contagioradio.com/reforma-hospitalaria-del-distrito-seria-el-paso-final-para-privatizacion-del-servicio-medico/)] el sindicato ha llevado a cabo manifestaciones en la Secretaria de Salud y **programan movilizaciones en distintos puntos de la ciudad**.

<iframe src="http://co.ivoox.com/es/player_ej_12418743_2_1.html?data=kpehk52beJShhpywj5aUaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5ynca7V04qwlYqliMKfpdTfy9iPi9Di24qwlYqldc3Z1IqfpZC3rc_Yhqigh6aop8Lo0JC7w8jNs8_VzZDRx5DQpYzHwtHixpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
