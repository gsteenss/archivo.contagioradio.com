Title: FARC se presenta ante la JEP
Date: 2018-07-13 17:17
Category: Paz, Política
Tags: Audiencia de la JEP, FARC, JEP, Jesús Santrich
Slug: ex-integrantes-de-las-farc-comparecen-ante-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-13-a-las-4.47.12-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: JEP] 

###### [13 Jul 2018]

Este viernes fueron citados ante la JEP 32 ex-jefes de la guerrilla (hoy convertida en partido político) FARC. En la audiencia, los comparecientes se presentaron ante la Sala de Reconocimiento de Verdad, Responsabilidad y Determinación de Hechos y Conductas (SRVR), por hechos relacionados en el informe Nº2 que la Fiscalía le entrego a la JEP denominado "Retención ilegal de personas por parte de las FARC-EP".

A esta diligencia se presentaron Pablo Catatumbo Torres Victoria, Rodrigo Londoño Echeverri, Julián Gallo Cubillo, y en vídeo desde la Cárcel La Picota, Zeuxis Pausias Hernández Solarte "Jesús Santrich", a quien no le fue permitido asistir de forma presencial a la audiencia.

Los 4 integrantes del partido FARC expresaron su deseo y compromiso de asistir a  las diligencias de todas las instituciones creadas por el Sistema de Verdad, Justicia, Reparación y Garantías de No Repetición. Por su parte, los 28 comparecientes que no se presentaron a la audiencia, enviaron a sus abogados apoderados en representación suya.  (Le puede interesar: ["Citaciones y audiencias: El nuevo capítulo de la JEP"](https://archivo.contagioradio.com/citaciones-y-audiencias-nuevo-capitulo-de-jep/))

En el reconocimiento de los comparecientes, Rodrigo Londoño dijo sentirse complacido al ver que los mecanismos creados por el Acuerdo de Paz se estaban cristalizando, y convirtiéndose en realidad.

Noticia en desarrollo.

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
