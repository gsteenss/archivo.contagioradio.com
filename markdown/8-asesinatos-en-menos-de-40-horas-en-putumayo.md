Title: 8 asesinatos en menos de 40 horas en Putumayo
Date: 2016-03-11 17:03
Category: DDHH, Nacional
Tags: asesinatos Putumayo, Putumayo, Yury Quintero
Slug: 8-asesinatos-en-menos-de-40-horas-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Cementerio-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ICRC Org ] 

<iframe src="http://co.ivoox.com/es/player_ek_10768866_2_1.html?data=kpWkmJ2cepehhpywj5acaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncZmfwtjS1c7SpdXj1JDS0JDRqc%2Fj1JDRx5CYdIzc0NfO1ZDJsozE1tniz8bds46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Yury Quintero, Diputada] 

###### [11 Mar 2016 ]

En menos de 40 horas fueron asesinadas ocho personas en Putumayo, dos de ellas en Orito, otras dos en La Hormiga, una en Puerto Caicedo y tres en Puerto Asís. Esta ola de asesinatos se da en el marco de la [[circulación de panfletos](https://archivo.contagioradio.com/se-afianzan-estructuras-paramilitares-en-putumayo/)] amenazantes de 'Los Urabeños' que imponen horarios y restricciones a la movilidad y señalan a "viciosos, ladrones, jaladores de motos, padres y madres alcahuetas que prestan sus casas a prostitutas, expendedores consumidores de vicio, pandillas y todo aquel que haga daño a la sociedad…"; y de otros de las 'Águilas Negras Bloque Capital D.C.' en los que amenazan a "grupos étnicos y otros que dicen ser llamados Reservas Campesinas (...) liderados por un cuanto líderes hijos de puta que tiene nexo con los subversivos de la Farc".

Yury Quintero, diputada de este departamento por el Partido Verde, asegura que en lo que va corrido del año se han reportado 20 asesinatos en Putumayo, varios de ellos contra jóvenes amenazados a través de panfletos que han servido para amedrentar a pobladores de las cabeceras rurales y municipales, quienes después de las seis de la tarde ya no salen de sus casas y no se atreven siquiera a encender las luces. De acuerdo con Quintero estas acciones de violación e intimidación hacen parte de una estrategia de desplazamiento, "usada para que la gente salga de los territorios" y de esta manera se den las condiciones para la implementación de megaproyectos de extracción petrolera por parte de empresas privadas.

Según afirma la diputada estos hechos pueden servir de excusa para aumentar el pie de fuerza que tiempo después custodiara los proyectos mineros, como sucedió entre 1999 y el año 2000 en el corredor Puerto Vega Teteye, en detrimento de la calidad de vida de las comunidades afrodescendientes, campesinas e indígenas del departamento que le están apostando a la construcción de Zonas de Reserva Campesina, para la concreción de la paz en un eventual escenario de posconflicto.

Quintero afirma que la respuesta que han tenido por parte de la Fuerza Pública en los Consejos de Seguridad y frente a las denuncias que se han hecho es que "esos panfletos no pueden generar mayor temor en la gente porque no van más allá de un documento que cualquiera le dio por hacerlo y fotocopiarlo para regarlo en las calles"; sin embargo, esta zozobra se agudiza cuando en las calles están asesinando pobladores. La actuación por parte de las autoridades genera en los habitantes interrogantes frente a la complicidad que puede haber con las estructuras que emiten las amenazas y cometen los asesinatos.

**Los hechos **

De acuerdo con la 'Comisión Intereclesial de Justicia y Paz', el martes 1° de marzo en el Corregimiento Santana, del municipio de Puerto Asís, sobre las 3 de la tarde fue asesinado Héctor Fabio Gómez, quien se encontraba trabajando en una estación de gasolina cuando dos desconocidos se hicieron pasar por clientes y le dispararon. Ese mismo día en sobre las 10:30 de la noche en el barrio Obrero del mismo municipio, sicarios que se movilizaban en una moto dispararon y quitaron la vida a Duber Fernando Vélez, joven de 18 años.

Los demás asesinatos se dieron el miércoles 2 de marzo, el primero ocurrió sobre la 1:20 de la tarde en la comunidad de La Hormiga,municipio de Valle del Guamuez, la víctima fue la joven Ana María Rendón, el segundo se dio horas más tarde y fue cometido por sicarios quienes dispararon seis veces contra Jonathan Calderón. En el sector conocido como El Caldero, municipio de Orito, las personas asesinadas fueron Oscar Alexander Arango y Gonzalo Marino Rodríguez, mientras que en Puerto Caicedo la víctima fue Sabulon Burbano Córdoba.

Por algunos de estos asesinatos la Policía capturó el pasado jueves a Carlos Alberto Salcedo y José Luis Pérez, señalados de ser integrantes de la banda 'La Constru'; sin embargo, ya fueron puestos en libertad.

Frente a esta aguda situación la Red de Derechos Humanos de Marcha Patriótica ha emitido este comunicado:

[Pronunciamiento Red DDHH Marcha Patriotica](https://es.scribd.com/doc/303995610/Pronunciamiento-Red-DDHH-Marcha-Patriotica "View Pronunciamiento Red DDHH Marcha Patriotica on Scribd")

<iframe id="doc_65483" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/303995610/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
