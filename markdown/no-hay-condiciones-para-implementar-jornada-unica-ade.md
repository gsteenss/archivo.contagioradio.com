Title: "No hay condiciones para implementar jornada Única" ADE
Date: 2017-08-17 12:59
Category: Educación, Nacional
Tags: ade, fecode, secretaria de educacion
Slug: no-hay-condiciones-para-implementar-jornada-unica-ade
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/jornada_unica_bogota_despliegue.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Bogota.gov.com] 

###### [17 Ago 2017] 

La Asociación Distrital de Educadores (ADE), FECODE y la Secretaría de Educación se reunieron ayer, para plantear soluciones a las graves problemáticas que vienen denunciando los maestros de la capital, como son el posible despido de más de dos mil docentes provisionales, **la falta de condiciones para continuar implementando la jornada única y los incumplimientos con la entrega de refrigerios.**

### **Despido de docentes provisionales** 

Cerca de 2.200 docentes provisionales habían manifestado su preocupación frente a represalias, **como despidos injustificados y descuentos en los salarios**, por participar en las movilizaciones del paro de docentes en Junio. (Le puede interesar:["ADE denuncia represalias contra docentes que participaron en paro"](https://archivo.contagioradio.com/ade-se-reunira-con-secretaria-de-educacion/))

De acuerdo con William Agudelo, presidente de la Asociación Distrital de Educadores, frente a esta problemática la **Secretaría de Educación se comprometió a que los docentes continúen vinculados a los colegios hasta el 15 de diciembre**.

**La Jornada única**

Desde que inició la implementación de la Jornada Única, los docentes de Bogotá han manifestado la falta de condiciones tanto estructurales como de planta profesoral para poder adoptarla, sin embargo, el **proceso ya se ha puesto en marcha en diferentes colegios**.

Frente a este punto, William Agudelo, expresó que no se logró llegar a ningún acuerdo con la Secretaría de Educación, **para frenar este proyecto hasta que existan garantías para la implementación del mismo**. (Le puede interesar: ["Asociación Distrital de Educadores se toma la Secretaría de Educación"](https://archivo.contagioradio.com/la-asociacion-distrital-de-educadores-se-toma-la-secretaria-de-educacion/))

“La Secretaría sigue insistiendo en que si nosotros planteamos la discusión en torno a la educación, estamos co-gobernando y **que la política de ellos es implementar la jornada única y en el camino ir arreglando las cargas”** afirmó Agudelo.

**Los refrigerios**

Durante la reunión los docentes denunciaron la falta de refrigerios que existe actualmente para los niños en los colegios y aseguraron que radicaran la denuncia **ante la Procuraduría a la espera que se tomen medidas frente a esta situación**.

La próxima reunión se realizará el 23 de agosto y se espera que en ella se puedan avanzar en temas como la jornada única. De igual forma la ADE está convocando un plantón para el 24 de agosto, en frente a la Secretaría para exigir mejoras en el sistema de salud que atiende a los  maestros y maestras del país.

<iframe id="audio_20388507" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20388507_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
