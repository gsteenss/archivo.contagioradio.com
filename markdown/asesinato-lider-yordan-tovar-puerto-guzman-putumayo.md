Title: Con asesinato del líder Yordan Tovar, continúa la violencia en Puerto Guzmán, Putumayo
Date: 2020-01-17 10:56
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato de líderes sociales, Puerto Guzmán, Putumayo
Slug: asesinato-lider-yordan-tovar-puerto-guzman-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Yordan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: la Red de DD.HH. del Putumayo Piamonte Cauca

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 16 de enero, organizaciones sociales denunciaron el homicidio del líder social Yordan Tovar, directivo del  Sindicato de Trabajadores Campesinos Fronterizos del Putumayo, (SINTCAFROMAYO) en Puerto Guzmán, municipio en el que se ha evidenciado un incremento de la violencia desde principios de 2020 y donde han sido asesinados tres líderes en lo corrido del año.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la Red de DD.HH. del Putumayo Piamonte Cauca y Cofanía Jardines de Sucumbíos Ipiales Nariño, Marcha Patriótica, el hecho habría ocurrido a las 7:25 de la noche **del pasado 16 de enero en el Corredor Puerto Vega Teteyé, municipio de Puerto Guzmán.** [(Lea también: Asesinan a lideresa Gloria Ocampo en Putumayo)](https://archivo.contagioradio.com/asesinan-a-lideresa-gloria-ocampo-en-putumayo-2/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Señalan que Yordan se encontraba en la tienda comunitaria del sindicato cuando hombres armados llegaron hasta el lugar y le dispararon en diferentes oportunidades. [(Le puede interesar: Colombia inicia el año con aumento de violencia contra líderes sociales)](https://archivo.contagioradio.com/colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Puerto Guzmán, foco de violencia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La escalada de la violencia en el[municipio](http://Asesinatos%20y%20desplazamientos%20forzados%20en%20Santa%20Lucía,%20Puerto%20Guzmán) se da en medio de una disputa territorial entre el frente primero de las disidencias de las FARC y un grupo que sus habitantes han denominado el Clan de Sinaloa que hace presencia en el municipio desde septiembre de 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El enfrentamiento entre ambas facciones, señalan los pobladores, ha puesto en alerta a las 184 veredas, 11 cabildos y 4 resguardos que hacen parte de los 4.565 kilómetros de territorio [(Le recomendamos leer: Avance paramilitar en Puerto Guzmán, Putumayo deja tres personas asesinadas)](https://archivo.contagioradio.com/avance-paramilitar-en-puerto-guzman-putumayo-deja-tres-personas-asesinadas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que Yordan también hacía parte de Marcha Patriótica, movimiento del que han sido asesinados 44 líderes asesinados desde el inicio del Gobierno Duque y más de 200 en casi una década. [(Lea también: Más de 200 integrantes de Marcha Patriótica han sido asesinados entre 2011 y 2020)](https://archivo.contagioradio.com/asesinatos-marcha-patriotica-2011-2020/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
