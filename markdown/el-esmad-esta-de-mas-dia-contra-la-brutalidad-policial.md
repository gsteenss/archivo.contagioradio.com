Title: "El ESMAD está de más", Día Contra la Brutalidad Policial
Date: 2017-02-24 14:18
Category: Movilización, Nacional
Tags: Día Contra la Brutalidad Policial, ESMAD, nicolas neira, No Más ESMAD
Slug: el-esmad-esta-de-mas-dia-contra-la-brutalidad-policial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Día-Contra-la-Brutalidad-Policial-e1487963734387.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Facción Latina] 

###### [24 Feb 2017] 

Desmontar el Escuadrón Móvil Antidisturbios ESMAD y en su lugar crear un cuerpo civil de mediación de conflictos, derogar los artículos de la Ley de Seguridad Ciudadana que limitan el Derecho a la protesta y la movilización social y exigir a la Fiscalía que investigue y sancione a los agentes del ESMAD, **responsables de diversos crímenes, son algunas de las demandas del movimiento social y que se conmemoran en el Día Contra la Brutalidad Policial.**

La Campaña en Contra de la Brutalidad Policial, impulsada por organizaciones de derechos humanos como la Fundación Nicolás Neira, el MOVICE, entre otras, tienen como objetivo sentar su voz de protesta cada 24 de febrero, puesto que **en esa misma fecha en 1999 nace el ESMAD, en el marco de una modernización de la fuerza pública posibilitada por el Plan Colombia.**

En varias ocasiones distintas organizaciones han denunciado el carácter letal de las armas que usa el ESMAD. Si bien oficialmente instrumentos como el gas pimienta y las descargas eléctricas con pistolas tipo “taser” se conciben como de “letalidad reducida”, según la resolución 02686 del 31 de julio de 2012, en muchos de los casos antes mencionados, se ha denunciado “el uso de armas no convencionales, como pueden ser los cartuchos recalzados de gas lacrimógeno con diversos objetos como balines y bolas de cristal”.

Armas que entre tanto, han sido utilizadas para **terminar con la vida de cientos de estudiantes, campesinos, indígenas, sindicalistas y demás población, que ha hecho uso de su legítimo y constitucional derecho a la protesta** y a la movilización social, los más recientes abusos registrados han sido en La Guajira, Tumaco, Meta, Cauca, Huila y otras regiones, que evidencian el por qué se exige su desmonte.

###  

La sociedad civil ha venido exigiendo que el desmonte de este cuerpo policial, y que ello haga parte de la implementación de los acuerdos, como garantía de seguridad y de no repetición, ya que a pesar de tener mayor presencia en áreas urbanas, las zonas rurales han sido gravemente afectadas por dicha institución y esos casos “han tenido menor visibilidad”. ([Le puede interesar: El prontuario del ESMAD](https://archivo.contagioradio.com/el-prontuario-del-esmad/))

Daniel Coronell, señaló que los gastos de la represión parecen servir únicamente a la industria de **“armas no letales, de las que nuestro país es uno de los principales clientes en el mundo”**, explicó que tales recursos podrían “reorientarse hacia otras necesidades que el posacuerdo generará para la fuerza pública, como puede ser la formación y profesionalización del personal”.

Por último para la conmemoración de **este 24 de Febrero, la cita es en la Kr 7ma con Cll 18 desde las 3:00pm hasta las 7:00pm**, en la que habrán distintas actividades culturales como música, muralismo y juegos reflexivos, acompañados por Changua Big-band and Miseria Maldita, The Impostors Band, La Tremenda Revoltosa Batucada Feminista, Las Guarichas, Mercedes Vivas Benitez, A Las Calles Sin Miedo, @Bicio Visual y el Movimiento de Víctimas De Crímenes De Estado **y quienes no puedan asistir pueden apoyar con el hashtag \#ElESMADEstáDeMás.**

###### Reciba toda la información de Contagio Radio en [[su correo]
