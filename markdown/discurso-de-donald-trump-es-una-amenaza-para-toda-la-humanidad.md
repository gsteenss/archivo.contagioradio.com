Title: Discurso de Donald Trump es una amenaza para toda la humanidad
Date: 2017-09-20 15:34
Category: El mundo, Entrevistas
Tags: Corea del Norte, Donald Trump, ONU, Venezuela
Slug: discurso-de-donald-trump-es-una-amenaza-para-toda-la-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/donald-trump-ONU-guerra.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: el confidencial] 

###### [20 Sept 2017]

Aunque algunos medios de información en Colombia han señalado que el discurso del presidente de Estados Unidos Donald Trump, “se robó el show” en la inauguración de la Asamblea de las Naciones Unidas, el analista político internacional Cesar Torres del Río, afirmó que más allá de un simple Show lo que se está poniendo sobre la mesa es una declaratoria de guerra que amenaza la vida de la especie humana, pues una confrontación nuclear tendría consecuencias catastróficas.

Además señala que la amenaza también se pondría en peligro la actitud democrática que se pretende que triunfe en la mayoría del territorio del mundo. Lo que hace Trump es “defender los intereses de los miembros del club de países con armas nucleares” impidiendo un balance entre las potencias nucleares y asegurar un monopolio que implica poder militar superior a cualquier otro.

### **La posibilidad de una guerra nuclear es objetiva, evidente y actual** 

Del Rio, quien ha estado analizando el panorama político internacional hace referencia al discurso de Trump cuando se refiere al presidente de Corea del Norte como “el señor misil” y agrega que si esa nación persiste en las pruebas nucleares no quedará otra alternativa que “destruirla” lo que implica una posibilidad real de una guerra con armas nucleares en la que también podrían participar naciones con alto poder militar como Rusia e Iran.

El analista señala que recientemente el presidente Putin, de Rusia, **denunció el peligroso acercamiento de las posiciones militares de Estados Unidos y la OTAN** en el mar del Norte y que esto también incluye la posibilidad de una confrontación multilateral, pues China ha manifestado que una incursión militar en ese continente podría desatar la intervención de la potencia económica asiática. Sin embargo son estos adversarios de EEUU los que han impedido que la guerra se desate. [Le puede interesar: ONU advierte difícil situación de los pueblos indígenas en el mundo](https://archivo.contagioradio.com/el-mundo-sigue-rezagado-con-respecto-a-los-derechos-de-los-indigenas-10-anos-despues-de-su-declaracion-historica-advierten-expertos-de-la-onu/)

### **Estados Unidos quiere pero no puede iniciar una guerra nuclear** 

Por otra parte una guerra nuclear también representa una amenaza ambiental que podría representar una adición a los desastres ambientales que se ya son evidentes. Por ejemplo el reciente paso de 3 huracanes por el caribe.

El analista también señala que no es mucho lo que se puede esperar de la ONU dado que la máxima instancia de esa organización es el concejo de seguridad, del cual hacen parte las mismas potencias militares. Entonces, **una declaración de esa instancia no sería muy eficaz a la hora de contrarrestar una amenaza de guerra**. [Lea también: Las funciones de la ONU en el cese bilateral con ELN](https://archivo.contagioradio.com/la-iglesia-seria-veedora-del-cese-bilateral-entre-el-gobierno-y-el-eln/)

La posibilidad de que se desate esa guerra estaría, según el analista, bloqueada momentáneamente dado que hay un margen de actuación tanto diplomáticamente como desde el nivel de las organizaciones sociales en todo el mundo que deben volcar sus esfuerzos para impedir una confrontación.

### **La amenaza a Venezuela es una amenaza para América Latina** 

Respecto de América Latina, Del Rio calificó como acertada la respuesta del presidente de Bolivia Evo Morales, quien recalcó que “América Latina no es el patio trasero de nadie” en referencia a las reacciones de algunos jefes de Estado en el mismo escenario de la asamblea. Además el analista agregó que de no ser por la vía de la fuerza la influencia de Estados Unidos en el continente también pretende establecerse por la vía de las armas.

Además se están aplicando criterios de Democrácia que no corresponden a los valores demostrados en el propio territorio de EEUU. Por esta razón también es importante rechazar ese tipo de intervenciones o declaraciones sobre la situación concreta de Venezuela.

<iframe id="audio_21001702" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21001702_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
