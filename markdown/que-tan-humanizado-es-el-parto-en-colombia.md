Title: ¿Que tan humanizado es el parto en Colombia?
Date: 2016-05-20 16:43
Category: Mujer, Nacional
Tags: derechos de las mujeres, mujeres, Parto Humanizado
Slug: que-tan-humanizado-es-el-parto-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Parto-Humanizado-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: vidacomomama] 

###### [20 May 2016] 

En la semana de **parto humanizado** se realizan actividades y jornadas de reflexión en el mundo en torno a la decisión de traer hijos o hijas al mundo. Sin embargo pocas veces las reflexiones son tan amplias como se esperaría. Algunas publicaciones afirman que hay una **serie de situaciones en las que debería pensarse para comenzar a hablar o exigir el parto humanizado como un derecho.**

Según partohumanizado.com.ar “*el parto y el nacimiento humanizado se fundamenta en la valoración del mundo afectivo-emocional de las personas, la consideración de los deseos y necesidades de sus protagonistas: madre, padre, hija o hijo*” y también “*la libertad de las mujeres o las parejas para tomar decisiones sobre dónde, cómo y con quién parir, en uno de los momentos más conmovedores de su historia.*”

En ese sentido se plantean varias contradicciones frente al modelo de salud generalizado para la práctica de los partos, por lo menos en América Latina, en que **el proceso del parto está [regido por una serie de estándares físicos](https://archivo.contagioradio.com/la-violencia-obstetrica-otra-forma-de-violencia-contra-la-mujer/)** como la medida de la dilatación, las semanas de gestación y las indicaciones médicas para la anestesia y la separación para “limpieza” de los bebes recién nacidos.

### **Según algunas definiciones internacionales el parto humanizado tiene los siguientes elementos:** 

-   Intimidad, seguridad y apoyo emocional
-   Ambiente y entorno apropiado
-   Libertad de expresión
-   Libertad de movimientos y posturas
-   Asistencia profesional respetuosa
-   Procedimientos naturales para abordar el dolor
-   Intimidad y tiempo para recibir al bebe

Testimonios de miles de madres han denunciado que en el proceso de parto los profesionales de la salud tienen entrada libre a los sitios en que las mujeres se aprestan para el parto, **se considera la sala de parto como el territorio de los trabajadores y no de las mujeres, sus familias y los bebes que llegarán al mundo**. Además el proceso está atravesado por una serie de estigmas de “flojas” o sencillamente el "placer de hacer pero no de parir".

Por otra parte, la Organización Mundial de la Salud advirtió acerca de la **“epidemia de cesáreas innecesarias”** que se practican en América Latina, siendo Colombia uno de los países con más altos índices de esta práctica llegando a un 43% después de república Dominicana con 44%. En ese sentido la OMS indició que **esta práctica debe restringirse a los casos en que sea estrictamente necesaria** por indicación médica y no por “evitar el dolor” o por descongestionar las salas de parto.

En Colombia algunas experiencias, desde la práctica, **asimilando conocimientos ancestrales, están desarrollando prácticas que poco a poco van abriendo espacio para que el parto humanizado sea por lo menos un tema de debate formal e informal**. Una de esas experiencias en la Casa de Mujeres Bachue que desde hace varios años trabaja por el derecho de las mujeres a vivir un parto humanizado.

\[embed\]https://www.youtube.com/watch?v=72VkBZSDZAg\[/embed\]
