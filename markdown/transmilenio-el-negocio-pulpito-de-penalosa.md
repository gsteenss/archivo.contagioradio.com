Title: Transmilenio: "el negocio pulpito de Peñalosa"
Date: 2016-02-15 15:23
Category: Economía, Nacional
Tags: Germán Navas, Peñalosa, Transmilenio
Slug: transmilenio-el-negocio-pulpito-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/TM-Peñalosa.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Diario Bogotano ] 

<iframe src="http://www.ivoox.com/player_ek_10444974_2_1.html?data=kpWhlpmde5Whhpywj5WbaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncbXmwtPgr87Qqc%2Fd0Iqgo5CJdpPZzZDbx8zTp8rjjNXiztXNuNCfxcqYssqJh5SZo5bOztTXpYamk5KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Germán Navas Talero] 

###### [15 Feb 2016] 

Este lunes se realiza en el Salón Boyacá del Capitolio Nacional la Audiencia Pública **'Por un mejor metro para Bogotá'**, convocada por el senador del Polo Democrático Alternativo Jorge Enrique Robledo y el representante del mismo partido Germán Navas Talero, un encuentro en el que distintos analistas entre arquitectos, ingenieros y abogados expondrán sus tesis **en favor de la construcción del metro, con miras a lograr un sistema digno de transporte para la capital colombiana**.

"Hace 15 años yo me opuse al sistema TransMilenio (...) pero eso no lo aceptó Peñalosa, **porque en ese momento ya tenía montado el negocio con 'Volvo' y sus amigos transportadores de Bogotá**" segura Navas y agrega que la negativa ante la construcción del metro tiene que ver con una cuestión de "querer monopolizar el transporte", pues **de los 10 mil millones que se reciben diariamente, los privados apropian 90%**, "un negocio pulpito" frente al que el único perdedor es el ciudadano que muy probablemente hace parte del 33% que reeligió al actual alcalde Enrique Peñalosa.

El representante Navas concluye asegurando que **desde el Congreso y con la voluntad política de la ciudadanía se puede ejercer control político** para presionar al Gobierno distrital para que tome la determinación de construir el Metro, teniendo en cuenta los ejemplos a nivel mundial de **ciudades en las que se están cambiando los sistemas de transporte tipo TransMilenio por metros subterráneos** ante las falencias e inconvenientes que se les han presentado.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

 
