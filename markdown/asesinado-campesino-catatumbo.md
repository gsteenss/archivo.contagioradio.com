Title: Asesinan líder Campesino en el Catatumbo
Date: 2018-04-05 14:47
Category: DDHH, Nacional
Tags: Asesinatos, Lideres, Norte de Santander
Slug: asesinado-campesino-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/ascamcat.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ASCAMCAT 

###### 05 Abr 2018 

La Asociación Campesina del Catatumbo ASCAMCAT, informó que Álvaro Pérez, integrante de la organización y líder campesino y responsable del Cómite barrial de San Calixto, fue asesinado con disparos de bala en horas de la noche del miércoles 4 de abril, en hechos que son materia de investigación.

De acuerdo con la comunicación, Pérez fue atacado por un sujeto sin identificar, en un punto de la vía que conduce de Cúcuta a Tibú en Norte de Santander. El integrante de la Asociación, quien era además hermano del directivo Olger Pérez, destacaba por su trayectoria de trabajo urbano y actualmente lideraba un proyecto de vivienda en conjunto con la alcaldía.

Ante la acción criminal, desde la organización hacen un llamado a la comunidad nacional e internacional para que se pronuncien ante la vulnerabilidad en que viven los líderes de esa zona del país, que registra ya dos asesinatos desde la firma del acuerdo de paz, sin que se halla realizado una investigación exhaustiva que de cuenta de los responsables.

Con su manifestación de rechazo por el homicidio, la organización envió un mensaje de solidaridad con la familia de Pérez y alertó sobre los constantes ataques, señalamientos y hostigamientos por parte de medios de comunicación y sectores políticos de los cuáles aseguran ser víctimas. Por último exigen al gobierno velar por el respeto a la vida, honra y bienes del pueblo campesino.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
