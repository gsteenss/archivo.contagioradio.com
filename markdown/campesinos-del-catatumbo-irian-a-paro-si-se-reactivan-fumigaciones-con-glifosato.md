Title: Campesinos del Catatumbo irían a paro si se reactivan fumigaciones con glifosato
Date: 2015-07-27 16:54
Category: Ambiente, Nacional
Tags: ASCAMCAT, Catatumbo, fumigaciones, Glifosato, Guillermo Quintero, Juan Fernando Cristo, ministerio de defensa, Ministerio de Interior, Olga Quintero, paro 2013, paro campesino
Slug: campesinos-del-catatumbo-irian-a-paro-si-se-reactivan-fumigaciones-con-glifosato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/20150428143518143glifosato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.diariodelhuila.com]

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Olga-Quintero.mp3"\]\[/audio\]

##### [Olga Quintero, ASCAMCAT] 

Por medio de una entrevista en una emisora nacional, la **policía anunció que a partir del próximo 10 de agosto, se reactivarían las fumigaciones con glifosato en el Catatumbo**. Esas declaraciones  prendieron las alarmas del campesinado, que anuncia un posible paro tan pronto empiece esa acción, además se anuncia que se tomarán acciones jurídicas conjuntas por parte de todas las regiones del país que están siendo afectadas por las fumigaciones.

La reanudación de aspersiones aéreas se debería a un aumento de cultivos de usos ilícito en esa región del país, aproximadamente en siete mil hectáreas habría aumentado este tipo de cultivos. Frente a este problema, Olga Quintero, dirigente de Asociación Campesina del Catatumbo, ASCAMCAT,  asegura que si los cultivos han aumentado es culpa del gobierno, que no ha cumplido los compromisos con las **400 familias con las que negoció durante el paro del 2013, pese a que estas han cumplido en un 98% los acuerdo**s.

Para Quintero, esta decisión del gobierno, iría en contravía de los estudios del Ministerio de Salud, y sería contradictorio ya que los campesinos llevan meses proponiendo alternativas y programas de sustitución gradual y concertada.

Desde ASCAMCAT, se denuncia que ha aumentado la militarización en la zona, la persecución y amenazas a líderes campesinos, como hacia G**uillermo Quintero que el sábado pasado fue detenido por el coronel Herrera** de plan vial y energético número 10 de la base militar de la Esmeralda. Quintero fue retenido de manera ilegal junto a su esquema de seguridad, les tomaron fotografías y fueron “atropellados de manera verbal”, según dice la denuncia.

Las fumigaciones con glifosato se reanudarían por parte de la **Policía Nacional, con apoyo del Ejercito Nacional y Estados Unidos**. Este domingo continúo la militarización en esa región del país y ayer desembarcaron mil hombres en el municipio de Tibú según informó la comunidad a través de redes sociales.

Durante el día de hoy, los campesinos se reunirán con el Ministro del Interior, Juan Fernando Cristo, para saber si habrá o no fumigaciones, lo que significaría un nuevo paro campesino obligado por el gobierno, como asegura Olga Quintero, quien denuncia desde el Ministerio de Defensa hay una intensión para que se rompa el diálogo.
