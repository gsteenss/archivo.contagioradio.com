Title: Secretariado de las FARC preocupado por adecuación de ZVTN
Date: 2017-02-09 10:04
Category: Nacional, Paz
Tags: FARC, Gobierno, Incumplimientos, Zonas Veredales
Slug: gobierno-miente-al-afirmar-avances-en-las-obras-de-las-zonas-veredales-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Farc-en-Yari.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 Feb. 2017]

En un reciente comunicado el secretariado de las FARC, manifestó que sus **diversos frentes ya completan más de 10 días movilizándose hacia las diversas Zonas Veredales**, "lo se puede entender como un compromiso decidido con la consolidación de la paz para Colombia".

Para el secretariado, llegar a las Zonas Veredales "significa un paso adelante en el camino de la normalización del país y el **inicio de la preparación para lo que debe ser el proceso de reincorporación de los combatientes a la vida civil".**

Sin embargo, aseguran en el comunicado, no pueden dejar de mostrar su **extrañeza por las condiciones de retraso o inexistencia total de obras de infraestructura** logística en todas las zonas del país. Le puede interesar: [Continúan incumplimientos del Gobierno en Zonas Veredales](https://archivo.contagioradio.com/continuan-incumplimientos-del-gobierno-en-zonas-veredales/)

Agregan que es inaudito escuchar en varios medios de comunicación, como **algunos funcionarios "mienten al afirmar avances de porcentajes inexistentes en las obras"**, dado que la realidad reportada es diferente ya que en las Zonas Veredales no hay agua potable, duchas y sanitarios, no existen alojamientos ni lugares adecuados para la preparación y el consumo de alimentos.

En la comunicación continúan insistiendo en las **graves condiciones en las que se encuentran las mujeres gestantes y varios bebés** "víctimas de la indolencia oficial, que los condena a permanecer en condiciones de riesgo para su salud". Le puede interesar: [Denuncian condiciones indignantes en Zona Veredal de Puerto Asís](https://archivo.contagioradio.com/denuncian-condiciones-indignantes-en-zona-veredal-de-puerto-asis/)

Por último, han hecho un **llamado urgente al país y a la comunidad internacional para que se emita algún tipo de pronunciamiento frente a esta grave situación** por la que atraviesan guerrilleros, guerrilleras, niños y niñas de los diversos frentes de las FARC en todo el país. Le puede interesar: [Monterredondo: Contraste entre comodidad militar y precariedad guerrillera](https://archivo.contagioradio.com/monterredondo-contraste-entre-comodidad-militar-y-precariedad-guerrillera/)

"Convocamos a los distintos sectores sociales a que se pronuncien **exigiendo el cumplimiento de los acuerdos firmados por parte del Gobierno**" concluyen. En contexto: [Zonas Veredales bajo la sombra paramilitar](https://archivo.contagioradio.com/zonas-veredales-bajo-la-sombra-paramilitar/)

**Este es el comunicado completo.**

Comunicado del Secretariado de las FARC-EP. Febrero 9 de 2017.

COMUNICADO

La construcción de la paz tiene en la implementación, y en el cumplimiento de los acuerdos firmados por las partes, su pilar fundamental

Apunto de terminar la llegada a las Zonas Veredales Transitorias de normalización de la totalidad de nuestros combatientes, podemos informar al país y al mundo que las FARC-EP han honrado así su compromiso firmado con el Gobierno Nacional.

Durante más de 10 días se sucedió una formidable movilización de hombres y mujeres comprometidos decididamente con la consolidación de la paz para Colombia.

Llegar a las zonas significa un paso adelante en el camino de la normalización del país y el inicio de la preparación para lo que debe ser el proceso de reincorporación de los combatientes a la vida civil en lo económico, lo político y lo social, incluida la dejación de las armas. Es un paso muy importante en el camino de la implementación de los acuerdos.

Implementación que obliga por igual a las partes firmantes según los compromisos adquiridos por cada una de ellas. Por esta razón, a la vez que damos parte de nuestra irrevocable voluntad y decisión de cumplir cabalmente con lo pactado, no podemos dejar de señalar nuestra extrañeza por las condiciones de retraso o inexistencia total de obras de infraestructura logística en todas las zonas del país.

Indigna escuchar a través de los medios masivos de comunicación las declaraciones de funcionarios que mienten al afirmar avances de porcentajes inexistentes en las obras, cuando la realidad es que el común denominador en todas ellas es la carencia de agua potable, de duchas y sanitarios, de alojamientos, de lugares adecuados para la preparación y el consumo de alimentos y el mal estado de las vías. Afortunadamente algunos medios de manera profesional han hecho honor a la verdad informando la situación real que se vive, desvirtuando así las declaraciones de los funcionarios estatales.

Particularmente dramática se torna la situación de las mujeres en estado de gestación y los bebés, víctimas de la indolencia oficial, que los condena a permanecer en condiciones de riesgo para su salud.

Ante esta situación convocamos la solidaridad de todo el país y de la comunidad internacional, de las organizaciones defensoras de los derechos humanos, de las organizaciones que trabajan por los derechos de la mujer y los niños para que se pronuncien y se movilicen frente a la inexplicable desidia estatal.

La construcción de la paz tiene en la implementación, y en el cumplimiento de los acuerdos firmados por las partes, su pilar fundamental.

Siendo la paz una conquista del pueblo colombiano, el incumplimiento gubernamental afecta a la sociedad en conjunto, por lo cual convocamos a los distintos sectores sociales a que se pronuncien exigiendo el cumplimiento de los acuerdos firmados por parte del Gobierno.

Secretariado Nacional de las FARC-EP

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
