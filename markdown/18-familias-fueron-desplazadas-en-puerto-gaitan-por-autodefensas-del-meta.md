Title: 18 familias fueron desplazadas en Puerto Gaitan por Autodefensas del Meta
Date: 2016-09-12 14:13
Category: DDHH, Nacional
Tags: desplazamiento en Puerto Gaitan, paramilitares
Slug: 18-familias-fueron-desplazadas-en-puerto-gaitan-por-autodefensas-del-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/fcmpaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fcmpaz] 

###### [12 de Sept 2016] 

Por lo menos **18 familias fueron desplazadas de los previos El Porvenir, en Puerto Gaitan, Meta, por grupos armados identificados como Autodefensas del Meta y Vichada, bajo las ordenes de la familia Carranza**. Según las denuncias de la comunidad acciones como persecución, intento de desaparición forzada, quema de enceres y de pequeños cultivos, son parte de las estrategias de miedo ejercidas en contra de las personas para que abandonen el lugar.

La corporación claretiana Norman Pérez y el colectivo jurídico Yira Castro, en acompañamiento a los habitantes de El Porvenir, denunciaron que estas estrategias son sistemáticas y se vienen presentando desde el 28 de julio de este año, cuando **intentaron asesinar, con armas de fuego, a dos líderes de la comunidad con el fin de obligar a las personas a un desplazamiento del territorio**. La orden de desalojar y ocupar las tierras habría venido de la viuda de Víctor Carranza. Las familias de ambos líderes no han podido salir de la región por temor a ser asesinados.

De igual forma los habitantes también han expresado que **podría existir un vínculo entre la Fuerza Pública y los grupos armados**, debido a la inoperancia de las mismas para actuar frente a los actos de estos grupos. Evidencia de esto es el desalojo a la [comunidad indígena Cubeo-Sikuan](https://archivo.contagioradio.com/800-indigenas-en-riesgo-de-desplazamiento-por-presuntos-paramilitares/)i, en donde la Policía acompaño a un grupo de hombres que dispararon a los indígenas y no tomaron ninguna medida frente a la protección de los mismos.

La comunidad ha expresado que, pese a las denuncias instauradas en diferentes instancias como la Corte Suprema de Justicia y de la sentencia a favor de la protección de esta población, **las instituciones gubernamentales de la región se encuentran bajo la orden de los grupos paramilitares, impidiendo su accionar en cualquier forma.**

De acuerdo con Jaime León, miembro de la Corporación claretiana Norman Pérez "[estos hechos son parte de un accionar paramilitar y se necesita una voluntad del Estado de combatir realmente a estos grupos,](https://archivo.contagioradio.com/hombres-armados-desplazan-a-familia-en-vichada/) que con amenazan de muerte obligan a las personas a desplazarse”, de igual forma León afirma que estos grupos han anunciado que se están volviendo a reorganizar en esta zona del país.

El Porvenir es un territorio que se encontraba en manos de la familia Carranza y que a través de una resolución del INCODER en el año 2014, perdió valides por la forma de obtener los títulos. Sin embargo, después de que se dio esta aprobación, [inició un proceso de invasión de amigos o ex empleados de la familia Carranza](https://archivo.contagioradio.com/en-puerto-gaitan-cambian-votos-por-tierras-usurpadas-cepeda/), hecho que dificulta la permanencia de las personas en el territorio. Sumado a esto se encuentra **la destinación de estas tierras a un programa piloto de ZIDRES**, en donde de nuevo las familias que allí habitan deberán ser desplazadas sin soluciones por parte del Estado.

<iframe id="audio_12876282" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12876282_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
