Title: Hay que salvar a la Universidad Nacional
Date: 2020-07-31 15:19
Author: AdminContagio
Category: Actualidad, Estudiantes
Tags: Crisis de la Educación, educación pública, Educación Superior, Universidad Nacional
Slug: hace-llamado-al-gobierno-ante-crisis-economica-en-la-universidad-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto:Wikicommons*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 31 de julio más de 100 personas entre estudiantes, poetas, artistas expresidentes, familiares y egresados de la Universidad Nacional, **presentaron una carta abierta al presidente Iván Duque en la que hacen un llamado a la protección cultural y educativa** **de la Universidad Nacional.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señalan que a causa de la situación producida por la pandemia y un acumulado por el modelo de -autofinanciación-, **ha incrementado la crisis económica en la que se encuentra la universidad.** (Le puede interesar: [La noche universitaria](https://archivo.contagioradio.com/la-noche-universitaria/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Destacan que como universidad pública, reciben parte de los impuestos que paga la ciudadanía, rubro que representa un poco más de la mitad de los recursos necesarios para funcionar, y que **el resto del dinero tiene que aportarlo la misma institución con recursos llamados propios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la misiva la financiación proviene de dos fuentes; una de ellas las matrículas que pagan los y las estudiantes junto a sus familias, y la segunda; la venta de servicios y asesorías, **fuentes que según los firmantes de esta carta, se encuentran afectados a causa de la pandemia, develando la fragilidad del modelo de autofinanciación** que año tras año acrecienta el déficit de las universidades públicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y es que en la Universidad Nacional la deuda se acerca a los **\$80,000 millones de pesos**, y según los y las firmantes de la carta, para cubrirla el Gobierno le ha ofrecido a la institución \$5.200 millones de pesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Agregan que **en el primer semestre 2020 la pérdida de ingresos a causa de la pandemia dificultó a más del 40% de la planta estudiantil matricularse,** especialmente a quienes pagan los porcentajes más altos, estudiantado que se encuentran mayormente en los posgrados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la carta u**no de los propósitos de la Nación debe ser disminuir la deserción estudiantil**, los firmantes señalaron que el aporte al valor de las matrículas y un mínimo vital para la subsistencia a cada estudiante, así como una adición presupuestal que termine definitivamente con el déficit creciente y garantice la oferta educativa, es uno de los pasos que se requieren para garantizar la educación a cientos de estudiantes de [universidades públicas](https://unperiodico.unal.edu.co/pages/detail/educacion-publica-una-deuda-de-mas-de-dos-siglos/) del país.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Ahora más que nunca Colombia necesita del conocimiento de la ciencia y de las artes y de la juventud universitaria y sus docentes para cruzar este río de la muerte y reconstruir el país y la otra orilla".***

<!-- /wp:quote -->

<!-- wp:paragraph -->

Poniendo también en evidencia ante el Gobierno que la Universidad Nacional está contribuyendo de manera *"eficaz y solidaria a superar la actual crisis de la salud a salvar vidas y enfrentar de modo sensible e inteligente la tragedia causada por la pandemia".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente resaltaron que **por más de 150 años generaciones de jóvenes han contribuido de manera determinante a la construcción de nación a través de la investigación, la ciencia, el arte, la filosofía**, **las ciencias sociales y la tecnología,** convirtiéndola así en un patrimonio histórico de los colombianos y colombianas, el cual mereces ser reconocido y protegido por la sociedad y el Estado.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Salvemos la universidad Nacional si la universidad Nacional este país sufrirá el más duro golpe a la educación pública a la juventud y a su futuro"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Le puede interesar ver;[Otra Mirada: Matricula cero, por el derecho a la educación](https://archivo.contagioradio.com/matricula-cero-la-clave-para-garantizar-la-educacion-publica-superior/))

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F210520340359898%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
