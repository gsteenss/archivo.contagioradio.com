Title: Guardar o compartir semillas se está convirtiendo en crimen
Date: 2015-04-13 17:21
Author: CtgAdm
Category: Entrevistas, infografia, Nacional, Resistencias
Tags: campesinado, colombia, Monsanto, semillas, semillas campesinas, semillas transgénicas, soberanía alimentaria, Vía Campesina
Slug: guardar-o-compartir-semillas-se-esta-convirtiendo-en-crimen
Status: published

##### Foto: [www.grain.org]

Guardar semillas se ha convertido en una actividad criminalizada, los gobiernos y las multinacionales se han unido en contra de los campesinos y campesinas, que durante miles de años realizan esta actividad y que además, es la **base de la agricultura y uno de los pilares de la producción de alimentos.**

Las empresas vienen ejerciendo gran presión sobre el campesinado, uniéndose al gobierno para crear leyes que limitan crecientemente el rango de aquello que los agricultores pueden hacer con sus propias semillas. **Los tratados de libre comercio, los acuerdos bilaterales de inversión y las iniciativas de integración regional** están endureciendo las formas “suaves” de los derechos de propiedad sobre las semillas.

**“El control de las semillas debe permanecer en manos campesinas**. Éste es el principio, basado en el proceso de producción, que garantiza la soberanía alimentaria de las comunidades rurales y las poblaciones urbanas contra las multinacionales y sus enormes ganancias. Por siglos, los campesinos crearon miles de variedades que son la base del acervo alimentario y la diversificación de la dieta,” dice Guy Kastler, de Vía Campesina.

Con la excusa de defender la propiedad intelectual y con ello, garantizar la calidad, transparencia mercantil, prevención de falsificaciones, etcétera. Las semillas campesinas están expuestas a regulaciones que responden a las exigencias de la industria semillera y biotecnológica.

De acuerdo a la Comunidad Andina, **según la Ley 1032 del 2006**, en Colombia por ejemplo, los campesinos tienen derecho a conservarlas, pero no a venderlas ni a intercambiarlas o regalaras, además el campesino únicamente puede cosechar y sembrar semillas en su propia plantación solo si el cultivo no supera las cinco hectáreas.

Así mismo, **solo pueden comprar semillas certificadas o registradas, y** si en una inspección se encuentra una "causa probable" de infracción o si se es declarado culpable, las autoridades tienen derecho a incautar las semillas, destruir o incautar la cosecha o el producto obtenido y también pueden ir a la cárcel y recibir multas.

Es por eso, que los movimientos sociales a nivel mundial, especialmente las organizaciones campesinas, han empezado a movilizarse para hacer resistencia a la aprobación de leyes que pongan en peligro la soberanía alimentaria.

[![1428959450](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/1428959450.jpg){.wp-image-7252 .aligncenter width="580" height="651"}](https://archivo.contagioradio.com/guardar-o-compartir-semillas-se-esta-convirtiendo-en-crimen/attachment/1428959450/)

Por ejemplo Vía Campesina, viene desarrollando un trabajo importante para hacer frente a las incitativas gubernamentales y empresariales que afectan al campesinado. De acuerdo a esta organización,  “**la ley debería garantizar los derechos del campesinado de conservar, utilizar, intercambiar o vender sus semillas,** protegiéndolas de la biopiratería”.

**[Las leyes de semillas que criminalizan campesinos y campesinas]**

<iframe id="doc_10441" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/261764273/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-FCJa1IRms0EOjWz0jwfi&amp;show_recommendations=true" width="647" height="606" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

**Situación de las semillas campesinas a nivel mundial **

<iframe id="doc_77074" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/261769868/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-DCihgh0R4hnuwDoYCGLd&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>
