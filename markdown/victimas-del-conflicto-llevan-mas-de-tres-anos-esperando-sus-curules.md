Title: Víctimas del conflicto llevan más de tres años esperando sus curules
Date: 2020-01-28 19:08
Author: CtgAdm
Category: Nacional
Tags: curules de paz, Senado
Slug: victimas-del-conflicto-llevan-mas-de-tres-anos-esperando-sus-curules
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/WhatsApp-Image-2020-01-28-at-11.28.12.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Curules-de-Paz.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Curules-de-Paz-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo Contagio Radio {#foto-archivo-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/luz-marina-hache-vocera-del-movimiento-nacional-de_md_47281623_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Luz Marina Hache | MOVICE

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

El lunes 27 de enero se informó que el actual presidente del Senado, Lidio García, revocaría el hundimiento del Proyecto de Acto Legislativo que creaba las 16 curules especiales para víctimas pactadas en el Acuerdo de Paz. Con esta decisión, se destrabaría uno de los proyectos claves que tuvo un difícil tránsito en el Congreso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué son las curules de las víctimas?

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Acuerdo de Paz estipulaba la creación de 16 curules de víctimas en la Cámara de Representantes que funcionarían de forma provisional durante 2 periodos (8 años) en territorios afectados por el conflicto. Los nuevos Congresistas tendrían que crear partidos nuevos, independientes y distintos de los que ya están en el legislativo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las 16 circunscripciones estarán compuestas por los departamentos de Cauca, Nariño, Arauca, Antioquia, Norte de Santander, Caquetá, Chocó, Meta, Guaviare, Bolívar, Sucre, Valle del Cauca, Putumayo, Cesar, Magdalena, Córdoba, Tolima y Antioquia. (Le puede interesar: ["Precandidatos a Curules de paz y víctimas protestan en Bogotá"](https://archivo.contagioradio.com/precandidatos-a-curules-de-paz-y-victimas-protestan-en-bogota/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Centro Democrático, un tropiezo para las víctimas y sus circunscripciones  

<!-- /wp:heading -->

<!-- wp:paragraph -->

La votación final de este Proyecto se realizó el 30 de noviembre de 2017 en el senado, y aunque la iniciativa obtuvo 50 votos a su favor y 7 en contra (suficientes según el quorum establecido para la corporación en ese momento), el presidente del Congreso Ernesto Macías lo archivó, señalando que no había alcanzado el número de votos necesario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La decisión fue objeto de una tutela por el senador Roy Barreras, en la que pidió a la Corte Constitucional tomar cartas en el asunto, aclarando que la cantidad de votos necesarios para la aprobación del Proyecto era 50 y no 52 como decía Macías, tomando en cuenta que 4 senadores estaban capturados. (Le puede interesar: ["Propuesta del Gobierno sobre Curules de Paz busca desconocer lo acordado en La Habana"](https://archivo.contagioradio.com/curules-gobierno-desconocer-acuerdo-habana/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RoyBarreras/status/1221975965552775168","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RoyBarreras/status/1221975965552775168

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Posteriormente, en un intento de aumentar su popularidad, el Presidente Duque dijo que presentaría otro proyecto para permitir la representación de las víctimas en el Congreso. Dicha inciativa se negaba a otorgar 16 curules nuevas, y en cambio, les solicitaba a los partidos que cedieran de espacios de representación para este propósito.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En diciembre de 2019, el movimiento Defendamos La Paz rechazó la propuesta del Gobierno y le pidió al presidente del congreso, Lidio García, que revoque el acto administrativo con el que Macias desechó el Proyecto y lo evíe a sanción presidencial. Pedido que habría echo eco, y permitiría convertir este Proyecto en Ley de la República.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La opinión de las víctimas sobre la posibilidad de tener sus curules

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luz Marina Hache, vocera del Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE) sobre este tema, recordó que las curules aún no son una realidad, y tomando en cuenta todas las dificultades que ha tenido el Proyecto para ser aprobado, señaló que esperarán su promulgación para poder celebrar el avance.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La prudencia para referirse al Proyecto encuentra razón en los antecedentes de este, y otros Proyectos que hacen parte de la implementación legislativa del Acuerdo de Paz como las leyes estatutarias de la Jurisdicción Especial para la Paz. Sin embargo, el Consejo de Estado emitió un comunicado en el que aseguraba que el presidente Duque "no tiene competencia para objetar actos legislativos".

<!-- /wp:paragraph -->

<!-- wp:image {"id":79977,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Curules-de-Paz-1.jpeg){.wp-image-79977}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

En ese sentido, la vocera del [MOVICE](https://movimientodevictimas.org/las-victimas-de-crimenes-de-estado-respaldamos-de-la-corte-validad-las-circunscripciones-especiales-de-paz/) señaló que esperan que se abra la oportunidad para las víctimas de tener voz directa en el Congreso, que no este mediada por una lógica partidista y permita avanzar hacía la paz. (Le puede interesar: ["En manos de Lidio García está la decisión de dar vida a las curules de las víctimas"](https://archivo.contagioradio.com/en-manos-del-presidente-del-senador-esta-la-decision-de-dar-vida-a-las-curules-de-las-victimas/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
