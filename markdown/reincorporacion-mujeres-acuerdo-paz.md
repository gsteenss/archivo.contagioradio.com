Title: La reincorporación de las mujeres en el Acuerdo de Paz
Date: 2018-09-25 16:34
Author: AdminContagio
Category: Expreso Libertad
Tags: excombatienres, FARC, reincorporación
Slug: reincorporacion-mujeres-acuerdo-paz
Status: published

###### Foto: NatGeo 

###### 18 Sep 2018 

Pese a los múltiples incumplimientos que se han dado frente a la reincorporación en el Acuerdo de Paz, **Leidy Sarmiento**, ex prisionera política, señaló que las mujeres que hacen parte de ese camino tendrán que afrontar múltiples retos, entre ellos, acabar con los mitos que se formaron frente a las mujeres que se alzaron en armas, generar rutas de empoderamiento y romper estereotipos.

Escuche en este **Expreso Libertad** los pasos que dan las mujeres ex prisioneras hacia la sociedad civil.

<iframe id="audio_28864995" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28864995_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
