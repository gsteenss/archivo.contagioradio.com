Title: JEP continúa pese a intentos por modificar su esencia de parte del Fiscal
Date: 2017-10-10 13:32
Category: Nacional, Paz
Tags: Cambio Radical, Jurisdicción Especial de Paz
Slug: jep-continua-pese-a-intentos-por-modificar-su-esencia-de-parte-del-fiscal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/senado_farc_operación-tortuga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:HonduRadio] 

###### [10 Octu 2017] 

Fue aprobada la participación política de las FARC en el Congreso de la República, como parte del debate de la JEP, sin embargo, **hay 6 artículos más que fueron modificados y que afanan a sectores de víctimas y derechos humanos**, pues podrían torpedear la búsqueda de la verdad y justicia de los responsables detrás del conflicto armado en el país.

De acuerdo con el senador Iván Cepeda, del Polo Democrático, esos 6 artículos son los que el Fiscal general de la nación, Néstor Humberto Martínez, pidió que se introdujeran, razón por la cual un grupo de congresistas solicitó constancias en los mismo**s 6 artículos para que sean tenidas en cuenta en las discusiones de las plenarias** que se realizarán la próxima semana.

### **Los 6 artículos que serían modificados en la JEP** 

El primero, según Cepeda, atañe a la naturaleza del sistema jurídico, en donde se busca dar paso a un sistema de jurisdicción restaurativa, que no tiene nada que ver con la justicia ordinaría, ni con la justicia retributiva. **Sistema que podría ser modificado de acuerdo a las solicitudes del Fiscal.** (Le puede interesar: ["Intromisión del Fiscal ha sido funesta para la paz: Imelda Daza"](https://archivo.contagioradio.com/47601/))

Otro de los artículos que quiere ser modificado, tiene que ver con las competencias de los jueces y del sistema en general, en donde se establece que el que determina cuál es la selección y priorización, tanto personas como casos, no es el Fiscal General, **sino los magistrados y el Fiscal creados por la jurisdicción y que hacen parte del SIVJRNR contenido en el acuerdo.**

En ese sentido, la pretensión del Fiscal, de acuerdo con Cepeda, es atribuirse algunas facultades para que en casos específicos sea él quien decida sobre la participación de estas personas en la JEP. (Le puede interesar: ["Los puntos gruesos de la JEP cruciales en la aprobación del articulado")](https://archivo.contagioradio.com/tres-puntos-gruesos-de-la-jep-que-serian-cruciales-en-la-aprobacion-del-articulado/)

Sobre los delitos de ejecución continuada, el Fiscal general también habría introducido modificación que pretenden juzgar a ex combatientes por diferentes delitos, cometidos después del cese definitivo de hostilidades. Por último, estaría la participación política de quienes se han acogido al proceso de paz, sin embargo, **el Fiscal no se ha referido a este tema y ha expresado que es un asunto netamente político.**

### **Amenazas a la JEP pueden ser contrarrestadas por la movilización social** 

Faltando poco tiempo para que finalice el periodo de Fast Track, se suman amenazas a la Jurisdicción Especial de Paz, por un lado, se encuentra Cambio Radical y su salida de los debates de la JEP, que para Cepeda **“son el temor de sectores de que con la jurisdicción se pueda iniciar un ciclo de investigación y esclarecimiento de la verdad”.**

Frente a las mayorías y las votaciones, Cepeda afirmó que se están dando coaliciones al interior del Congreso para que durante los **debates se tengan las mayorías, aunque sean un poco “apretadas”** para la aprobación de la Ley Estatutaria.

Frente a esta situación Cepeda reitero el llamado a la movilización social para garantizar que se den los debates en los tiempos establecidos, pero sobre todo para presionar a los congresistas a que cumplan con su palabra y no atenten contra la esencia de los acuerdos de paz.

<iframe id="audio_21376532" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21376532_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
