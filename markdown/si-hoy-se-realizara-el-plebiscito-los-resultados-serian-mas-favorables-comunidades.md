Title: Si hoy se realizara el plebiscito los resultados serían más favorables: comunidades
Date: 2020-10-06 21:22
Author: AdminContagio
Category: Actualidad, DDHH
Tags: acuerdo de paz, plebiscito por la paz
Slug: si-hoy-se-realizara-el-plebiscito-los-resultados-serian-mas-favorables-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cuatro años han transcurrido desde que se refrendara el Acuerdo de la Habana a través de un plebiscito que buscaba que los y las colombianas expresaron su aprobación o rechazo a los acuerdos de paz entre el Gobierno y las FARC-EP. Pese a que el No superó al sí con un 50.21 % frente a un 49.78 % académicos, comunidades y reincorporados analizan las consecuencias de esta consulta para la implementación del Acuerdo, en medio de un panorama en el que la violencia ha escalado en varias regiones del país.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Pareciera que nos están castigando por haberle apostado al plebiscito y al proceso de paz"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**María Miyela Riascos, lideresa e integrante del Comité del Paro Cívico de Buenaventura** recuerda la sensación de impotencia que se sintió en la región del Pacífico aquel 2 de octubre de 2016, y que con dicho resultado parecía darle la espalda al trabajo que se venía realizando desde las comunidades que esperaban un respaldo por parte de la ciudadanía, "fue la primera vez en lo que alcanzo a registrar de la historia de una sociedad que le dice no a la paz, era la posibilidad que teníamos las personas a quienes nos ha tocado vivir la guerra de manera directa".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cuatro años después, Miyela señala que los cambios no han sido muchos, y pese a que se ha visto una escalada en la violencia y continúan las violaciones a los DD.HH, "aquí la esperanza es que se siga luchando por la paz". [(Lea también: Tras cuatro años de la firma del Acuerdo de Paz, la deuda más grande es con el campo colombiano)](https://archivo.contagioradio.com/tras-cuatro-anos-firma-del-acuerdo-de-paz-la-deuda-mas-grande-es-con-el-campo-colombiano/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hemos visto que ha escalado la violencia, sigue el paramilitarismo reinando y parece ser que nos están castigando a las regiones que le apostamos a la paz porque precisamente los que apostamos al sí en esa consulta popular, somos las regiones que nos están masacrando".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con relación a dicha afirmación, **aunque Antioquia votó No, al plebiscito en un 62% hoy es el departamento con un mayor número de masacres, encabezando dicho recuento con 9 sucesos, seguido de departamentos como Cauca y Nariño con siete masacres y que en su momento votaron positivamente al referendo con 63% y 64% de respaldo de la población** respectivamente. Otros departamentos que hoy viven un recrudecimiento en las acciones violentas como Putumayo, Chocó y Córdoba, también votaron en su mayoría a favor del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El fatídico 'No', profundizó la violencia que se ha podido experimentar en los últimos años

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Sebastián Lippez, director del departamento de Ciencia Política de Universidad Javeriana**, señala que el recrudecimiento de la violencia que se está viviendo en el país sería muy similar "incluso si hubiera ganado el sí, seguirán existiendo grupos de personas que quieren torpedear el proceso y que estarían atentando contra las comunidades, excombatientes y líderes, en todo caso habría un escenario muy complejo". [(Le puede interesar: Con Cristian Sánchez ascienden a 230 los firmantes de paz asesinados)](https://archivo.contagioradio.com/con-cristian-sanchez-ascienden-a-230-los-firmantes-de-paz-asesinados/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El académico agrega que la decisión del expresidente Juan Manuel Santos de refrendar el acuerdo "fue un error estratégico", aunque advierte que **"había necesidad e interés de darle protección democrática al acuerdo \[...\] era una idea plagada de entusiasmo por el cuerdo pero desprovista de realidad política"**, pues señala, se desconocía que gran parte de la población no estaba sintonizada con las condiciones del Acuerdo y lo que es más perjudicial, que "la opinión pública es maleable y manipulable".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante tal situación y a propósito de las campañas que se hicieron patrocinando el 'No' en el plebiscito la lideresa de Buenaventura expresa que no hubo la pedagogía necesaria en aquel entonces, por lo que hoy es necesario seguir educando y sensibilizando a las personas en los territorios, resaltando que refrendar los acuerdos era cosechar el trabajo de muchos sectores y que si hoy se realizara de nuevo este ejercicio los resultados serían más generosos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El profesor agrega que el resultado del plebiscito permitió que actores políticos, como el actual partido de Gobierno utilizara "una visión estrecha de la democracia, desde el punto de vista instrumental", acudiendo al resultado electoral y señalar que la democracia o la ciudadanía reclamaba el no, un argumento que para el académico resulta falso pues es necesario recordar el resultado tan estrecho que tuvo el plebiscito, **"es una visión muy estrecha de lo que significa la democracia esta implica la inclusión de las comunidades, ver las condiciones diversas de todos y muchos asuntos que van más allá de las mayorías".**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> No nos podemos dar el gusto de volver a la violencia, pero tenemos el compromiso de que nazca la paz
>
> <cite>Miyela Riascos, lideresa de Buenaventura</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, Gustavo Arbeláez Cardona, dirigente departamental del partido [FARC](https://twitter.com/PartidoFARC) en el Valle del Cauca afirma que es necesario destacar las bondades que surgieron del Acuerdo de Paz pese a los múltiples intentos de su implementación recordando que fruto de su firma, **13.000 personas en armas hicieran un alto en esa actividad además de la entrega de 8.112 armas e incinerar 1,3 millones de cartuchos según la ONU, además del desarrollo de la justicia transicional y el sistema integral de justicia**, resaltando que los seis puntos del acuerdo fueron pensados en función del país y no en función de la antigua guerrilla. [(Lea también: Acuerdo de paz tiene herramientas para frenar la violencia en Colombia: ONU)](https://archivo.contagioradio.com/onu-gobierno-acudir-a-mecanismos-del-acuerdo-para-combatir-violencia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras la salida de las FARC y la esperada llegada del Estado a través de inversión social que no se vio reflejada como se estableció en el acuerdo, tanto Miyela como Gustavo coinciden en que existen "territorios que lamentan incluso la ausencia de la guerrilla en territorios, mire lo ilógico e irracional para que ocurra eso", una situación que a la lideresa le permite concluir que **"la historia nos ha mostrado que aquí el verdadero problema es la negación sistemática de los derechos constitucionales" del país.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Valorar lo que se ha construido pese a las dificultades

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Yo vivía en Anchicayá y me tocaba ver la guerra y cómo del cielo caían balas, en la cuenca del rio Naya había presencia de las FARC y hoy es un territorio en el que nadie quiere vivir porque han venido grupos armados reductos de paramilitares incluso las disidencias y se ha vuelto invivible, pero nosotros creemos que no podemos volver a la guerra", expresa Miyela Riascos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito de la experiencia que vivió la lideresa, Gustavo Arbeláez, agrega que durante su época como combatiente de las FARC operó en aquel mismo territorio en la zona rural de Buenaventura durante 10 años como comandante del frente urbano Manuel Cepeda Vargas, **sin embargo con la firma del Acuerdo, este le permitió ingresa a procesos de reconciliación y reconocimiento de responsabilidades reiterando que en su gran mayoría, los firmantes de la paz tienen la vocación de continuar con este camino. ,**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El profesor concluye que si bien el país sigue dividido por múltiples razones y los antagonismos sociales no se superan con la firma del acuerdo es necesario superar esta situación a través de procesos cambios económicos, sociales y políticos que tengan impactos en las comunidades.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
