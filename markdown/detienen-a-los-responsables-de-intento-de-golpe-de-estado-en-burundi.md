Title: Detienen a los responsables de intento de golpe de estado en Burundi
Date: 2015-05-15 16:43
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Burundi, Golpe de estado fallido en Burundi, Intento de Golpe de estado en Burundi
Slug: detienen-a-los-responsables-de-intento-de-golpe-de-estado-en-burundi
Status: published

###### Foto:Ejecentral.com.mx 

El miércoles en la mañana al menos **tres generales del ejercito oficial de Burundi intentaron propiciar un golpe de estado,** impidiendo el retorno del presidente Pierre Nkurunziza quién se encontraba en Tanzania en una visita oficial. Los militares golpistas lograron impedir que aterrizara el avión presidencial para declarar el gobierno como derrocado.

Algunas fuentes en la región señalan que los militares realizaron el intento de golpe tras la decisión de Nkurunziza de presentarse a las elecciones presidenciales y aspirar a un tercer mandato. Una tercera reelección no está contemplada dentro de las leyes del país y por ello se buscaron las vías de hecho.

Tras la reacción de militares leales al gobierno los conbates dejaron al menos **tres muertos y más de un centenar de heridos.** Sin embargo las tropas lograron detener el intento de golpe y capturar a los principales responsables, uno de **ellos el** general **Godefroid Niyombare,** quien declaró antes de ser detenido junto con tres militares  más  **"...Hemos decidido rendirnos, espero que no nos maten..."**. Por otra parte fuentes oficiales han filtrado informaciones referentes ha una **negociación con los golpistas detenidos para evitar un mal mayor.**

Desde el pasado mes de Abril algunos sectores sociales y militares manifestaron su inconformidad con la decisión del presidente de volver a participar en las elecciones**.** Estas protestas, fuertemente reprimidas por las FFMM**,** han dejado más de **200 heridos y 20 muertos**.

Burundi es actualmente uno de los diez países más pobres de el mundo y ha sido sacudido hace una década por una guerra civil entre etnias, ademas de sufrir la peor epidemia de VIH en el continente africano.,
