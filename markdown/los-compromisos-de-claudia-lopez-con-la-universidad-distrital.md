Title: Los compromisos de Claudia López con la Universidad Distrital
Date: 2019-10-28 17:58
Author: CtgAdm
Category: Educación, Política
Tags: Alcaldía, Claudia López, paro, Universidad Distrital
Slug: los-compromisos-de-claudia-lopez-con-la-universidad-distrital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Universidad-Distrital-en-Protesta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @jerojasrodrigue  
] 

Tras las elecciones locales del pasado domingo 27 de octubre en las que Claudia López resultó elegida alcaldesa, estudiantes de la Universidad Distrital Francisco José de Caldas (UD) **analizaron la victoria de una candidata que en campaña se comprometió con la educación superior, y es cercana a las causas ciudadanas**. Lo que hará López sobre este tema, incluso antes de posesionarse, será clave tomando en cuenta que la Universidad está en paro hasta que no se apruebe la Asamblea Universitaria.

### **López será alcaldesa de Bogotá, y presidenta del CSU de la Universidad  
** 

Julián Báez, representante estudiantil de la UD fue claro en que Bogotá eligió alcaldesa, y **los estudiantes eligieron también a la presidenta del Consejo Superior Universitario, porque esa es una de las funciones del cargo**. En ese sentido, afirmó que esperan que se comprometa con esta labor, porque a lo largo de los últimos cuatro años Peñalosa, nunca asistió a reuniones del Consejo, "que sería lo mínimo que se puede pedir". (Le puede interesar: ["Estudiantes de la Universidad D. tienen razones de sobra para protestar"](https://archivo.contagioradio.com/estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar/))

En segundo lugar, Báez señaló que **le piden la aprobación de la reforma universitaria que permite la Asamblea**, de tal forma que se propicien los espacios de participación y veeduría de los estudiantes. Sobre este mismo aspecto, deseó que se respetaran las propuestas de los jóvenes, para que no se modifiquen las ideas trazadas a lo largo de años. (Le puede interesar: ["Movimiento estudiantil se alista para la semana por la indignación"](https://archivo.contagioradio.com/movimiento-estudiantil-se-alista-para-la-semana-por-la-indignacion/))

Un tercer elemento que explicó Báez fue **la lucha anticorrupción**, una de las banderas asumidas por el partido Alianza Verde en las últimas elecciones, y una de las razones que dió origen a la movilización estudiantil en la UD durante este 2019. Sobre este aspecto, Baéz enfatizó que esperan que el compromiso sea real y efectivo, para controlar que los recursos públicos sean invertidos efectivamente en el bienestar de todos los estudiantes. (Le puede interesar: ["En las calles por la autonomía universitaria"](https://archivo.contagioradio.com/en-las-calles-por-la-autonomia-universitaria/))

### **"Que el ESMAD no sea la respuesta para la protesta social"** 

Defensores de derechos humanos han criticado la actuación del Escuadrón Móvil Antidisturbios (ESMAD) y de los entes que lo controlan y vigilan. En consecuencia, Báez afirmó que **de una alcaldía como la de López se esperaría que el ESMAD no sea la respuesta para la protesta social**, "sino que se generen soluciones concretas" para los problemas contra los que se manifiesta, teniendo en cuenta que en muchos casos, dichos problemas pueden ser solucionados por la Alcaldía.

No obstante, el representante estudiantil de la UD señaló que veían la posibilidad de que López recurra al uso de la fuerza en determinados momentos, porque en varios discursos manifestó que "no permitirá acciones directas de los estudiantes". Baéz lamentó que esta afirmación se haga efectiva, puesto que en algunas ocasiones, como se ha visto en Chile o Ecuador, "las acciones de hecho son las únicas que les dejan a los estudiantes". (Le puede interesar: ["Chile: Los efectos de un modelo económico fallido"](https://archivo.contagioradio.com/chile-los-efectos-de-un-modelo-economico-fallido/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
