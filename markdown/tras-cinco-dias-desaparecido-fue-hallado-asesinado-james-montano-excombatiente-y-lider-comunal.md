Title: Tras cinco días desaparecido fue hallado asesinado James Montaño, excombatiente y lider comunal
Date: 2020-07-07 23:34
Author: CtgAdm
Category: Actualidad, DDHH, Nacional
Slug: tras-cinco-dias-desaparecido-fue-hallado-asesinado-james-montano-excombatiente-y-lider-comunal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/James-Montaño-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Partido FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este martes 7 de julio en horas de la mañana y después de cinco días de su desaparición, fue hallado el cuerpo del excombatiente de [FARC](https://twitter.com/PartidoFARC), James Andrés Montaño de 30 años de edad, en el sitio conocido como **Limoncito a orillas del río San Miguel, en el municipio de Valle del Guamuez Putumayo.** Con su asesinato ya son 216 los firmantes de paz, víctimas de homicidio desde la firma del acuerdo de paz en el 2016. [(Lea también: Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes)](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

James llevó su proceso de dejación de armas y continuó la reincorporación junto al C**onsejo Comunitario Nueva Esperanza, corregimiento Cofanía Jardínes de Sucumbíos, Ipiales, Nariño,** donde actualmente se desempeñaba como vocal, había desaparecido desde el pasado jueves 2 de julio cuando fue visto por última vez cuando se desplazaba solo en una embarcación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Asociación de Consejos Comunitarios Afroamazónicos de las riveras del río San Miguel emprendió la búsqueda de James Montaño desde el pasado 3 de julio encontrando la embarcación hundida, al reemprender la búsqueda el 4 de julio fueron halladas su chaqueta y su bolso a orillas del río. [(Lea también: Por amenazas y riesgos excombatientes de Ituango se desplazan a Mutatá)](https://archivo.contagioradio.com/por-amenazas-y-riesgos-excombatienes-de-ituango-se-desplazan-a-mutata/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones sociales denuncian que la zona donde se presentaron los hechos, es **controlada por la estructura conocida como La Mafia,** **al mando de alias “Bonito”**, quien opera con normalidad en medio de la alta presencia militar de la Brigada XXVII de Selva y la Policía Nacional. Según la Misión de Observación de la ONU entre el 27 de marzo y el pasado 26 de junio han sido asesinados 15 excombatientes en varias regiones del país y 33 en lo que va corrido del año. [(Le recomendamos leer: Excombatiente Edwin Tuirán, fue asesinado en Córdoba)](https://archivo.contagioradio.com/excombatiente-edwin-tuiran-fue-asesinado-en-cordoba/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
