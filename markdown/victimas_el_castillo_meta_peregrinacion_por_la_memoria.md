Title: Víctimas de El Castillo, Meta caminarán 5 días contra el olvido y por la construcción de paz
Date: 2018-02-05 13:28
Category: Paz
Tags: Alto Ariari, El Castillo, Meta, víctimas
Slug: victimas_el_castillo_meta_peregrinacion_por_la_memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/sin-olvido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sin Olvido] 

###### [5 Feb 2018] 

**Del 7 al 11 de febrero de 2018 se llevará a cabo una peregrinación por las víctimas** del  conflicto armado  en el municipio de Castillo, Meta, un rincón del Alto Ariari, donde los 50 años de guerra que ha vivido Colombia, se han dejado ver con desplazamientos masivos, ejecuciones extrajudiciales, tomas insurgentes, asesinatos, y desapariciones forzadas.

Se trata de una región que por años ha sido escenario del abandono estatal y la confrontación armada, pero que en medio de esa situación, las comunidades trabajan contra el olvido y a favor de la construcción de paz del país. En ese marco, se va a desarrollar la peregrinación y otra serie de actividades por la memoria.

**"Hacer memoria duele pero es más terrible es olvidar. Es necesario recordar para sanar heridas del pasado que se manifiestan en el presente"**, expresó el Padre Henry Ramírez, uno de los organizadores de la caminata en El Castillo.

### **La construcción de paz en el Alto Ariari** 

El año pasado se firmó el Pacto Social por la Paz, entre distintos sectores institucionales, excombatientes y la sociedad civil, para que la implementación del Acuerdo de Paz firmado en La Habana, se trabajen desde una perspectiva territorial.

Actualmente, la apuesta de las organizaciones de esta región del país está dirigida a pensar cómo van aportar a la Justicia Especial de Paz, y específicamente a la Comisión de la Verdad y la Unidad de búsqueda de Personas Desaparecidas, teniendo en cuenta que, en un municipio de no más de 15.000 habitantes, **existen más de 600 personas reportadas como desaparecidas.**

"En medio de la guerra, existen organizaciones sociales que mantienen la esperanza, y hoy construyen propuestas para hacer que la paz sea verdadera y con justicia social", señala Soler, quien agrega que en la comunidad hay grandes expectativas con otros temas planteados en el acuerdo de La Habana, como la reforma agraria, participación política y la erradicación de cultivos de uso ilícito.

### **Así será la peregrinación** 

En el marco de esas iniciativas por la construcción de paz, la peregrinación que inicia este  7 de febrero no es la primera que se realiza. Allí ya se han se llevado a cabo caravanas y caminatas, y esta peregrinación es la segunda que se realiza en febrero.

Aunque la primera actividad empezó siendo una caminata de 4 horas, la de este año contempla 5 días para poder pasar por las veredas de Medellín del Ariari, Malavar, Caño Claro, El Encanto, Puerto Esperanza y la Zona Humanitaria de CIVIPAZ, para finalmente, terminar el domingo 11 de febrero en Miravalles con una caminata de 3 horas.

Según el padre Henry, esos lugares fueron escenarios de confrontación, y **lo que se busca con esta marcha "es recorrer el pasado desde el presente para sanar heridas".** Por eso, en cada parada se realizarán actos de memoria para recordar lo que allí se vivió, como la pérdida de Lucero Henao y su hijo David Daniel Henao asesinados el 6 de febrero del 2004, así como las demás víctimas de El Castillo durante los años de la guerra.

[![invitacion peregrinacion](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/DVR8JmYVAAEv3GP-720x540.jpg){.alignnone .size-medium .wp-image-51179 width="720" height="540"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/DVR8JmYVAAEv3GP.jpg)

<iframe id="audio_23559863" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23559863_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
