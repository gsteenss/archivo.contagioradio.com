Title: Registraduría pide suspender consulta popular en Granada, Meta
Date: 2017-10-18 16:44
Category: Ambiente, Nacional
Tags: consulta popular, Granada, Meta
Slug: por-falta-de-presupuesto-se-suspende-consulta-popular-en-granada-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/101_0670.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MetagranadaSU] 

###### [18 Oct 2017] 

La Registraduría Nacional emitió un comunicado en el que pide la suspensión de la consulta popular de Granada, Meta, programada para este 22 de octubre, debido a la falta de recursos económicos para llevarla a cabo, sin embargo, Fabio Montoya, concejal del municipio, señaló que **esa solicitud atenta contra el derecho a decidir que tiene la comunidad.**

De igual forma el concejal por la Alianza Verde expresó que no se entiende cómo si ha existido presupuesto para otras elecciones en otros municipios y no para el de Granada y añadió que la explicación que ha dado la Registraduría, frente a que este presupuesto debe salir de las arcas municipales **y que costaría alrededor de 182 millones de pesos, no se especificó desde un principio**.

### **Los 16 comités confían en que habrá consulta** 

Actualmente hay 16 comités que están trabajando a favor de la realización de la consulta popular, con la finalidad de que se respete y **potencialice la agricultura como la forma tradicional de trabajo que hay en el municipio y su carácter de despensa agrícola**. (Le puede interesar: ["Organizaciones Nacionales e Internacionales piden a la Corte Constitucional que no límite las Consultas populares")](https://archivo.contagioradio.com/organizaciones-nacionales-e-internacionales-piden-a-la-corte-constitucional-que-no-limite-consultas-populares/)

El concejal manifestó que una de las principales razones para oponerse a la exploración y explotación de petróleo y minerales, ha sido la experiencia de municipios aledaños como Guamal o Puerto Gaitán, “**hemos visto la violación de derechos humanos en Guamal, atropellando con el ESMAD a muchas personas**, en San Juan de Arama hablan de arenas bituminosas que es un proceso más nefasto que el Fracking”.

Actualmente en el departamento del Meta, diferentes municipios han iniciado el proceso para llevar a cabo consultas populares, **la próxima en realizarse es en la Macarena el 26 de noviembre**, mientras que el municipio del Castillo está a la espera de que el Tribunal Superior apruebe la pregunta para la consulta. (Le puede interesar: ["Consultas populares en Santander son demandadas por MinMinas"](https://archivo.contagioradio.com/ministerio_minas_consulta_popular_sucre_santander/))

<iframe id="audio_21545591" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21545591_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
