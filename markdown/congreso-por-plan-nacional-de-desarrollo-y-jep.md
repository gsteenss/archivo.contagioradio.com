Title: Oposición y Gobierno miden fuerzas en el Congreso por Plan Nacional de Desarrollo y JEP
Date: 2019-03-26 18:19
Category: Paz, Política
Tags: Alianza Verde, JEP, Objeciones, Plan Nacional de Desarrollo
Slug: congreso-por-plan-nacional-de-desarrollo-y-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Diseño-sin-título-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Mar 2019] 

Esta semana en el Congreso inicia en forma la discusión sobre las objeciones presidenciales a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), al tiempo que se desarrollarán los debates para aprobar el Plan Nacional de Desarrollo (PND); discusiones en las que Gobierno y oposición medirán sus fuerzas en el legislativo, previo al inicio del periodo electoral.

### **Senado y Cámara estarían de acuerdo sobre las objeciones ** 

El senador por la **Alianza Verde, Antonio Sanguino** sostuvo que no esperaban que la Corte Constitucional se inhibiera de pronunciarse sobre las objeciones, sino que lo hiciera "de fondo y de forma definitiva" confirmando que el Presidente tienen facultades para objetar por razones de conveniencia una ley, pero "no puede abusar de esta facultad y objetar por asuntos constitucionales lo que él llama asuntos de conveniencia".

Pero tomando en cuenta la decisión, Sanguino analizó las comisiones accidentales creadas para evaluar las objeciones y dijo que en Cámara, la mayoría rechazará las objeciones de Duque; y añadió que en **el Senado "finalmente va a imperar la responsabilidad con el país y con el Acuerdo de Paz" tomando la misma decisión**. El Senador reconoció que este será un pulso muy fuerte, y el Gobierno buscará tener capacidad de maniobra mediante herramientas conocidas: mermelada, chequera, entre otras.

Para efectos de esta discusión, Sanguino señaló que están preparándose para este 'pulso', ahondando en argumentos jurídicos y políticos, **"porque la paz no puede nunca ser un asunto de inconveniencia"**. (Le puede interesar: ["¿Qué viene para la Ley Estatutaria de la JEP?"](https://archivo.contagioradio.com/ley-estatutaria-jep-2/))

### **La lucha por el Plan Nacional de Desarrollo está más difícil** 

El integrante de la Alianza Verde recordó que su partido intentó en las sesiones conjuntas de las Comisiones Terceras y Cuartas (sobre temas económicos) cambiar el Plan Nacional de Desarrollo pero no fue posible y por eso se radicaron dos ponencias negativas por parte de la oposición. No obstante, el primer gran pulso lo ganó el Gobierno, que logró la aprobación del Plan con votos provenientes de la casa Char.

En ese sentido, Sanguino reconoció que introducir **las modificaciones a un Plan que ha sido ampliamente criticado será difícil**, por lo que intentarán hacerlo en las plenarias como en las Comisiones; y resaltó que la labor deberá abordar las cerca de 2.500 propuestas modificatorias, así como eliminar los "micos y orangutanes que vienen de lo aprobado" la semana pasada.

### **El trabajo de la oposición: Confluencia y unidad para lo que viene** 

Tomando en cuenta que 2019 es un año electoral, y que buena parte de la política sobre lo que ocurre en los territorios pasa también por decisiones desde lo local, el Senador verde indicó que así como las bancadas de oposición han trabajado unidas en el Congreso y en la convocatoria a movilizaciones, confluirán en los espacios electorales regionales. (Le puede interesar: ["Estamos más unidos que nunca para defender la paz: Oposición](https://archivo.contagioradio.com/estamos-mas-unidos-nunca-defender-la-paz-oposicion/)")

Esta confluencia se verá con diferentes carácterísticas en cada región tomando en cuenta las necesidades y particularidades de cada una, frente a lo cual, Sanguino expresó que como movimiento  "el partido Alianza Verde en sí mismo es una experiencia de unidad y de confluencia de distintos sectores alternativos, y haremos nuestro esfuerzo por confluir para lograr espacios de gobernabilidad y representación territorial para las fuerzas alternativas de Colombia".

<iframe id="audio_33753686" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33753686_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
