Title: ¿Podemos hablar de “celebración” navideña en la situación que vive el país?
Date: 2019-12-30 12:30
Author: CtgAdm
Category: A quien corresponda, Opinion
Tags: Fe, Iglesias, Teologia
Slug: celebracion-navidena-en-esta-realidad-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones. ***

[(Romanos 2,24) ]

[Resguardo Waunaam de Pichimá, Litoral, Chocó, 25 de diciembre del 2019.]

*["Me gusta tu Cristo... No me gustan tus cristianos. ]*

*[Tus cristianos son muy diferentes a tu Cristo"]*

[ Mahatma Gandhi.]

[Estimado]

**Hermano en la fe**

[Cristianos, cristianas, personas interesadas]

[Cordial saludo, ]

[Entre nosotros, un tema de discusión y de fondo, ha sido, si asumimos o no, que Dios está presente en las realidades humanas y en el trabajo por transformaciones sociales hacia una vida más digna para la mayoría de los seres humanos, en este contexto, te comparto la reflexión que me lleva a creer que la Navidad acontece va más allá del “mundo religioso” cristiano. ]

[Navidad es la conmemoración del nacimiento del Hijo de Dios, el “Niño Dios” en Nazaret; nacimiento en un lugar concreto, en una familia concreta y en una situación histórica determinada. Navidad es recordar (volver a pasar por el corazón) que Dios se ha hecho ser humano en Jesús, por esto, lo humano se convierte en camino hacia Dios y la historia en el lugar para construir o hacer realidad el Reino de Dios, que El vino a anunciar. En lenguaje bíblico, anunciar es hacer realidad, construir, “poner en práctica” de manera que “el mensaje” (el Evangelio) “y el mensajero” (Jesús de Nazaret) son inseparables. Jesús es el Evangelio realizándose, el Reino de Dios haciéndose, por eso habla de lo piensa y hace, sin separación entre el decir y el hacer. Contrario a muchos líderes de las iglesias que “predicamos y no practicamos” ]

[En cartas pasadas, te recordaba con citas bíblicas, que el Reino de Dios es justicia, verdad, amor, armonía con la creación; preferencia por los empobrecidos por los sistemas económicos, por las víctimas de los todos poderes; es inclusión de las personas discriminadas por su raza, sexo, cultura, religión, clase social… Cuando “]*[Juan el Bautista, en la cárcel, oyó hablar de la actividad de Jesús, envió a unos discípulos preguntarle: ¿Eres el que había de venir o tenemos que]****esperar****[ a otro? Jesús los envió a contarle a Juan lo que estaban viendo y oyendo: los ciegos ven, los cojos andan, lo leprosos quedan limpios, los sordos oyen, los muertos resucitan y a los pobres le anuncian buenas noticias”]*[ (Mateo 11,2-5). El hablar y actuar de Jesús estaba en función del pueblo: de sus dolores, frustraciones, opresiones, liberaciones; al servicio de la vida digna: salud física, emocional, espiritual, social y ambiental, como expresión de una nueva relación entre Dios, las personas, la sociedad y la naturaleza. Unas relaciones libres y liberadoras. ]

**El “esperado”,**[ el Mesías anunciado por los profetas, era]**“alguien”**[ que lograría que “El]*[pueblo]*[(Colombia, América Latina)]*[ que andaba en tinieblas]*[(de la mentira, la corrupción, la doble moral, la manipulación, a exclusión, la discriminación…)]*[ viera]*[(despertara)]*[ la luz de un gran día; vivía en sombras de muerte]*[(masacres, “falsos positivos”, asesinatos de líderes sociales, indígenas, defensores ambientales…)]*[, y una luz les brillo (]*[Isaías 9,1). El Mesías, el]**“esperado”** [vendría]*[“para sostener y consolidar]*[(el reino)]*[ con el derecho y la justicia, desde ahora y por siempre”]*[ (Isaías 9,6); para garantizar que el pobre y la viuda, los más desprotegidos de su tiempo, tuvieran lo mínimo necesario para vivir dignamente (el mínimo vital);]*[“para dar vista a los ciegos, liberar a los oprimidos y a los cautivos]*[(de ayer y de hoy),]*[para anunciar las buenas noticias a los pobres”]*[ (Lucas 4,18-19); para hacer volver a los deportados (desplazados) y llenar el mundo del “conocimiento de Dios”, un conocimiento que transforma lo que hace que unas “especies” vivan de las otras, se devoren unas a otras; un conocimiento que]*[“no juzga por apariencias ni de oídas, que juzga con justicia a los desvalidos y sentencia con rectitud a los oprimidos”]*[ (Isaías 11,4).  ]

[La vida, las palabras y las obras de Jesús mostraron que era el Mesías, el]**“esperado”, el Hijo de Dios, Dios hecho hombre,** [en otras palabras, Dios hecho historia para conducir la historia hacia Dios que es hacer realidad, aquí y ahora, el Reino de Dios. ]

[Navidad, es entonces, Dios que se hace carne e historia en Jesús. Hay navidad cuando en lo personal, familiar, eclesial, social, religioso y ambiental se deja “nacer”, acontecer el Reino de Dios y se asumen los cambios y transformaciones que este “nacimiento” genera. Recordemos lo que significa el nacimiento de un niño o niña para papá, mamá, abuelos, tíos, primos, amigos. Navidad es Dios que se hace niño y habita en nuestra realidad, es Dios que inspira ternura y no miedo, que despierta lo mejor de los seres humanos, en este niño el Reino de Dios se hace realidad en la historia humana “más allá” de los “espacios religiosos tradicionales”. La Navidad acontece en la vida cotidiana, religiosa o no religiosa, de las personas, las familias y la sociedad. Cabe recordar que abundan creyentes cristianos (católicos, evangélicos) que hablan mucho de la venida de Jesucristo y poco practican sus enseñanzas, dicen una cosa y hacen otra, piensan que a Dios solo se le encuentra en los “templos”,  “cultos” y “celebraciones” olvidando que es el Emmanuel, el Dios con nosotros y que está allí donde están los seres humanos. ]

[En medio de la indignación por el asesinato de Lucy Villareal, Marta Cecilia Pérez, Marleny Rueda, Manuel Antonio Perea, Natalia Jiménez, Rodrigo Monsalve, Manuel González, Dilan Cruz, Cristiana Bautista…  y de los hechos que generaron el paro, considero que hay Navidad en el despertar del país, en la nueva conciencia social humana y ambiental que crece y se expresa en las variadas, profundas y multicolores alternativas, protestas y reclamos que quieren una sociedad más justa, humana, solidaria, responsable con la naturaleza y con las futuras generaciones; en la apuesta por una sociedad equitativa, incluyente, sensible y respetuosa de todas las formas de vida y expresiones culturales; en la denuncia de la doble moral, de la memoria acomoda al poder y del memoricidio, de las apariencias, de los privilegios por razones de raza, género, cultura y clase social; en el cuestionamiento a una sociedad en la que “el lugar” de nacimiento condena a vivir sin condiciones básicas para realizarte como persona a las mayorías, o, que “el lugar” de nacimiento coloque por encima de esas mayorías a una minoría socioeconómica que ha puesto el Estado al servicio de su poder y enriquecimiento injusto y desproporcionado. ]

[Hay navidad en los miles de rostros, mensajes, expresiones culturales y artísticas; en la creatividad de los carteles y en la diversidad de expresiones de mujeres, ambientalistas, trabajadores, estudiantes, diversidades sexuales, educadores, desempleados y todo tipo de “marchantes” que exigen y se comprometen con la justicia social, de género y ambiental, con el respeto al otro diferente, con el amor a la vida y a la creación; en la generosidad miles de jóvenes que buscan la “salvación” de la sociedad y del planeta, superando odios,  miedos y autoritarismos, desaterrando verdades ocultas por el poder y el dinero que explican por qué estamos en el fondo de la situación que hemos llegado. Y sin violencia.]**Esto es Navidad porque está en la dirección del Reino vivido y anunciado por Jesús de Nazaret.  **

[Fraternalmente, su hermano en la fe, ]

1.  [ Alberto Franco, CSsR, J&P]

[francoalberto9@gmail.com]
