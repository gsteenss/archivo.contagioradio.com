Title: Centro de Memoria y Fedegan ¿una alianza para exonerar a responsables del conflicto?
Date: 2020-02-25 18:28
Author: CtgAdm
Category: Entrevistas, Memoria
Tags: Centro Nacional de Memoria Histórica, Dario Acevedo, Fedegan, Negacionismo
Slug: centro-de-memoria-y-fedegan-una-alianza-para-exonerar-a-responsables-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Fedegan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Fedegan: Fundagan

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_48357044" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_48357044_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

El presidente de la Federación Colombiana de Ganaderos (Fedegan), José Felix Lafaurie, hizo oficial un convenio establecido con el Centro Nacional de Memoria Histórica, bajo la dirección de Dario Acevedo, cuya posición a lo largo de un año enfrente de la entidad ha sido desconocer la existencia de un conflicto armado en Colombia. A su vez, **sectores como Fedegan han sido señalados históricamente de oponerse a la restitución de tierras y el proyecto de desarrollo agrario,** acudiendo al despojo de tierras y la financiación del paramilitarismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el profesor y politólogo, Álvaro Villarraga, desde el CNMH existe una acción de negar responsabilidad del Estado en la comisión de grandes violaciones de DD.HH. y exonerar a la Fuerza Pública y ahora se hace algo similar con los grandes ganadores que históricamente tienen responsabilidades en el conflicto". [(Le puede interesar: Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino)](https://archivo.contagioradio.com/con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Está muy documentado en cómo muchas de las regiones del país desde los años 70 se reconocen hacendados que organizaban grupos armados para enfrentar las exigencias del movimiento campesino de reclamar acceso a la tierra". Según la Fundación Paz y Reconciliación a lo largo del conflicto en Colombia se despojaron más de 6.000.0000 de hectáreas de tierra, la mayoría de pequeños propietarios y no de los latifundistas, como afirman José Félix Lafaurie y su esposa, la senadora María Fernanda Cabal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Lafaurie, quien afirma que fue el mismo Dario Acevedo quien les sugirió firmar un convenio, considera que si se está construyendo la memoria del conflicto se debe incluir a las víctimas ganaderas, razón por la que la Fundación Colombia Ganadera [(Fundagan)](https://twitter.com/FUNDAGAN/status/1232063016058982401) recolectó en dos informes cerca de 6.202 nombres y 1.936 testimonios, que recogen la historia de víctimas de la violencia pertenecientes al gremio ganadero que serán entregados al CNHM.  [(Le puede interesar: Sin lineamientos, centros de memoria pueden convertirse en aparatos ideológicos de Gobierno)](https://archivo.contagioradio.com/sin-lineamientos-centros-de-memoria-pueden-convertirse-en-aparatos-ideologicos-de-gobierno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, el profesor Villarraga afirma que existe una gran responsabilidad histórica que debe esclarecerse y aunque aclara que sectores ganaderos y comerciantes también sufrieron presión de la guerrilla y del secuestro extorsivo, fue **"más sistemático, amplio y grave el margen de victimización"** producido por los ganaderos contra el campesinado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el profesor, los sectores que rechazan el Acuerdo de Paz no están interesados en la verdad y le temen a la justicia transicional, "no podemos ser ingenuos, el gobierno de Iván Duque es del Centro Democrático y en el marco de esas situaciones el caso del CNMH no es un caso aislado, las políticas están orientadas al negacionismo y a impedir el esclarecimiento de la verdad".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La responsabilidad del sector ganadero en el conflicto

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que exdirectivos de Fedegan como Jorge Aníbal Visbal, fueron condenados por paramilitarismo mientras que su presidente actual, José Félix Lafaurie reconoció en entrevista a la revista Cambio el apoyo de los ganaderos a las Autodefensas. [(Lea también: Persisten las amenazas contra el proceso restitución de tierras)](https://archivo.contagioradio.com/persisten-las-amenaza-contra-el-proceso-restitucion-de-tierras/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "En la década de los años ochenta es muy claro el nexo entre grandes hacendados muchos de ellos ganaderos con el fenómeno de la conformación de grupos paramilitares" - Álvaro Villarraga

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Según declaraciones de jefes paramilitares como Salvatore Mancuso y Diego Fernando Murillo, alias Don Berna; Visbal Martelo mantuvo desde 1998 hasta 2005 encuentros con las autodefensas en diversas fincas de Córdoba. **El entonces directivo, no solo mostraba afinidad con la ideología paramilitar sino que participó en su financiación, pactando pagos a cuotas a cambio de protección.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Respecto a Lafaurie, Benito Osorio, exgobernador encargado de Córdoba y expresidente del Fondo Ganadero de Córdoba que fue condenado a 19 años de prisión por despejar de sus tierras a más de 100 familias campesinas en Córdoba y Urabá, señaló al presidente de Fedegán, José Félix Lafaurie, como intermediario de los paramilitares y del Gobierno de Álvaro Uribe Vélez en 2006.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Lafaurie también entregó estos informes a la Comisión de la Verdad, entidad a la que criticó al considerar que esta busca "poner al empresariado como un actor victimario". Ante estas declaraciones y el rumbo que ha tomado el Centro Nacional de Memoria Histórica, el politólogo concluye que incluso ganaderos, empresarios y Fuerza Pública ya habían sido escuchados y documentados con anterioridad en el CNMH, sin embargo, ahora se trata de **"resistir a la realidad del conflicto armado y las victimizaciones que comprometen al Estado buscando de manera selectiva exonerar a determinados actores del conflicto".**

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2851121381617274%2F&amp;width=500&amp;show_text=false&amp;appId=1237871699583688&amp;height=500" width="500" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
