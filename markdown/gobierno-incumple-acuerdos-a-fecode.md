Title: Gobierno incumple acuerdos a FECODE
Date: 2015-01-30 20:50
Author: CtgAdm
Category: DDHH, Educación, Movilización
Tags: acuerdos, fecode, paro
Slug: gobierno-incumple-acuerdos-a-fecode
Status: published

###### Foto: Colombia.com 

La Federación Colombiana de Trabajadores de la Educación - FECODE- presentó el pasado martes 27 de enero un comunicado en el que afirma que el gobierno colombiano ha incumplido los acuerdos alcanzados durante mayo del 2014, y llama a las y los profesores de todo el país a un **nuevo proceso de movilización**.

El próximo 3 de febrero se reuniría la Junta Nacional de Fecode para ultimar los detalles del **pliego de peticiones** que se presentaría al Ministerio de Educación el 25 del mismo mes, y que contendría como punto fundamental la negociación del **Estatuto de Profesionalización en el apartado de la Evaluación por Competencias**.

Precisamente en mayo del 2014, los docentes habían presentado un pliego de peticiones para negociar el sistema de salud en **régimen especial para las y los maestros, el incremento salarial y la evaluación por competencias**.

Este último fue el único tema en el que maestros y gobierno no lograron ponerse de acuerdo, salvo en el compromiso por discutirlo en los meses siguiente para realizar una reforma de manera consensuada.

Según lo expuesto por FECODE, a pesar de que la organización sindical ha presentado propuestas para el aumento de sueldos y reubicación salarial a lo largo de 7 meses en la mesa de concertación, el g**obierno ha negado todas las iniciativas y dilatado las negociaciones**, afirmando que de no llegar a un acuerdo a su favor, aprobaría una reforma por la vía plebicitaria.

"La Comisión Negociadora, el Comité Ejecutivo y la Junta Nacional por unanimidad, se opusieron a las propuestas del gobierno y exigieron respetar lo acordado: 'que la Evaluación de Competencias no va mas", cita el comunicado.

Finalmente, la organización sindical llama a las y los docentes del país a **unirse en torno al pliego de peticiones que se presentaría a finales de febrero**, rechazando la propuesta plebicitaria que significa, afirman, la legitimación de la política neoliberal en la educación y la labor docente.

<div>

</div>
