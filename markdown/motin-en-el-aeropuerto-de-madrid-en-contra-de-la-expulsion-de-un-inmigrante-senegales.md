Title: Motín en el aeropuerto de Madrid en contra de la expulsión de un inmigrante senegalés
Date: 2015-03-20 18:53
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 10 senegaleses expulsados en motín en Barajas, Motín en Barajas por la expulsión de inmigrante senegalés
Slug: motin-en-el-aeropuerto-de-madrid-en-contra-de-la-expulsion-de-un-inmigrante-senegales
Status: published

##### Foto:Losandes.com 

En total **diez personas de nacionalidad Senegalesa han sido detenidas** por **intentar impedir la expulsión de un inmigrante** mediante la devolución del mismo a su país de origen.

El miércoles sobre las 18:30 horas dos policías entraron en un **vuelo con destino a Dakar** custodiando a un **inmigrante Senegalés, para expulsarlo** y devolverlo a su país cuando el joven inmigrante comenzó a **gritar pidiendo ayuda** a los demás pasajeros, diciendo que no quería irse y que su expulsión no era legal.

Fue entonces cuando **varios pasajeros comenzaron a apoyarle, llegando a agredir a los propios agentes**. Los policías pidieron ayuda, a lo que acudieron más de quince policías. Al final fueron **detenidos diez senegaleses**, acusados de atentado contra la autoridad, a uno de ellos le demandan 7 años de prisión.

El joven senegalés conocido como **Elhadji, de 31 años,** había solicitado el **permiso de residencia. Al serle denegado** había interpuesto un **recurso ante la administración**. En dicha situación  tenía el **pasaporte retenid**o,  por lo que todas las semanas acudía, acompañado de su compañera sentimental, a la comisaría de  policía para firmar y dar parte de su estancia.

Su **abogada María Luisa Menéndez** había solicitado a la administración que **no se ejecutase la orden de expulsión** antes de que se pronunciase sobre el recurso por el permiso de residencia.

Además la letrada declaró que **"...la vida de una persona cambia mucho en cinco años…"**, añadiendo que la persona tenía una pareja estable y una **posibilidad de un contrato laboral indefinido.**

A pesar de todo Elhadji ha sido **víctima de una expulsión exprés** por parte del juzgado de guardia de Oviedo, y ejecutada por la **policía nacional en un plazo de 72 horas máximo**.

**Miles de inmigrantes** se encuentran actualmente en la **misma situación** a la espera del pronunciamiento de la administración, viviendo el **día a día con el miedo de poder ser expulsados** después de haber iniciado una vida nueva, en una nueva tierra.

**Organizaciones de DDHH** han criticado duramente las conocidas como “expulsiones exprés”, ya que en su opinión se suelen **realizar sin previo aviso o sin haberse finalizado los trámites para la solicitud del permiso de residencia**.
