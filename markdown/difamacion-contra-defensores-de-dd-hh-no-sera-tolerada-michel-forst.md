Title: Difamación contra defensores de DD.HH no será tolerada: Michel Forst
Date: 2018-12-04 12:31
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: DDHH, defensores de derechos humanos, Michel Forst
Slug: difamacion-contra-defensores-de-dd-hh-no-sera-tolerada-michel-forst
Status: published

###### Foto: Contagio Radio 

###### 04 Dic 2018 

Son cuatro las ocasiones en las que el relator de la ONU, **Michel Forst** ha visitado  Colombia, sin embargo esta es la primera en la que es invitado por el Gobierno para evaluar la situación del país en materia de derechos humanos. Su conclusión, después de  recorrer diferentes regiones durante diez días y compartir testimonios con los defensores es desalentadora.

El relator explicó que a pesar de ver una reducción en el número de homicidios, el incremento de asesinatos contra defensores de derechos es alarmante, un argumento que reforzó después de reunirse con diversos defensores, que expresaron sus preocupaciones y de enterarse del asesinato de cuatro líderes sociales durante la semana que visitó el país.

### **Actores del Conflicto** 

Durante su visita, Forst afirmo que recibió evidencias fuertes sobre multinacionales que operan en Colombia y que atacan a los defensores, razón por la que decidió indagar y reunirse con representantes de algunas de esas compañías para hablar al respecto. “Puse sobre la mesa una lista de empresas internacionales que operan en Colombia acusadas de complicidad y les he dicho que las investigaré para ver qué rol desempeñan, quiero estar seguro de que esas multinacionales no lastimen a los defensores”, aseguró.

Asímismo el relator habló de otros actores del conflicto como los paramilitares y sugirió que debería existir un plan adicional del gobierno para desmantelar dichos grupos armados, razón por la que explica que “el presupuesto del Ministerio de  Defensa se ha aumentado de manera dramática para garantizar que el Estado tenga los medios para luchar en contra de esos actores armados".

### **Impunidad** 

El relator de la ONU expresó que en su informe dedicará un apartado especial enfocado a la lucha contra  la impunidad, un tema que formó parte de las conversaciones con el fiscal general Néstor Huberto Martínez, "tenemos opiniones diferentes de los resultados del fiscal y queremos investigar para ver sus cifras" indicó.

Para Forst, la impunidad es sin duda uno principales factores que explican la tasa de asesinatos en contra de defensores pues no solo se trata de arrestar al autor material de un delito sino “decidir investigar o no al autor intelectual, muchas veces ligados a actores poderosos”.

### **¿Hay voluntad por parte del Gobierno?** 

Respecto al **Plan de Acción Oportuna (PAO)** y a las propuestas del Gobierno para proteger a los líderes sociales, Frost indicó que esta es solo una parte de la solución pero que aún es necesaria una estrategia que incluya a todos los ministerios y que únicamente le resta depositar su confianza en el presidente Duque, razón por la cual le pidió que hiciera una declaración pública a favor de los defensores, “las campañas de difamación contra los defensores no serán toleradas, es necesario mandar señales de comunicación para que empiece una nueva era para la protección de los defensores”.

### **El derecho a la libre movilización  
** 

Forst también se refirió a las movilizaciones sociales y a la fuerte represión por parte del ESMAD y aclaró que dentro de su informe incluirá una sección dedicada al derecho de protestar libremente, **“he visto fotos y he hablado con estudiantes sobre el nivel de violencia en contra de los jóvenes, hay claras evidencias del uso excesivo de la fuerza por parte de la Policía y eso es algo al que quiero darle seguimiento”**.

De igual forma, aseguró que continuará monitoreando la situación de Colombia y que entregará un informe de 34 páginas en los que dará cuenta de su visita, dicho informe será presentado al Consejo de Derechos Humanos de las Naciones Unidas en Ginebra en marzo de 2019 y será público en marzo de 2020.

<iframe src="https://www.youtube.com/embed/m-RgsPVhyE0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
