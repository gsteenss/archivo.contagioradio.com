Title: Así ha avanzado la lucha feminista en Colombia
Date: 2017-03-07 20:46
Category: Mujer, Nacional
Tags: Día de la mujer, feminismo
Slug: asi-ha-avanzado-la-lucha-feminista-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Cumbre-Mujeres-Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cumbre de Mujeres por la Paz 

###### [7 Mar 2017] 

Aunque hay varios referentes de las luchas de las mujeres en Colombia, como su protagonismo en la resistencia contra los conquistadores españoles, o la disputa por la tierra en el siglo XIX frente a los terratenientes, sólo **hasta el siglo XX tuvieron lugar expresiones organizadas de las mujeres en busca de derechos fundamentales**.

Sin embargo, investigadores como Mauricio Archila reconocen que en Colombia las mujeres rara vez acuden a acciones colectivas alrededor de las demandas de género. De acuerdo con Archila, **el carácter patriarcal del sistema político colombiano, desde el Frente Nacional, hasta la democracia pos-constituyente, ha impedido la consolidación de un movimiento de mujeres** con una agenda concreta y que dispute a profundidad el sistema político y cultural del país.

En la historia del movimiento de mujeres en Colombia puede hablarse de dos momentos: **el primero que se desarrolla desde los años treinta hasta los sesenta,** en los cuales la agenda sufragista y la lucha por la igualdad de derechos civiles marcó el devenir de la agenda de mujeres. Mientras que en el segundo momento, **entre los años setenta a la actualidad**, ha ganado protagonismo la lucha por los derechos sexuales y reproductivos, en contra de la cultura machista.

### **Los inicios** 

A principios del siglo pasado, la conformación de organización y espacios políticos de lucha popular estuvo marcada por la determinación de mujeres como **Juana Julia Guzmán,** que **en 1917 crea el Centro de Emancipación Femenina** y en 1919 juega un rol protagónico en la fundación de la Sociedad de Obreros y Artesanos de Córdoba. Su lucha fue callada, tras ser víctima de la persecución política y no consiguió consolidar un movimiento de mujeres. Por otro lado, para esa misma época **María Cano** lideró durante varios años, el movimiento obrero. Cano fue crucial para la difusión de las ideas socialistas en Colombia y logró consolidar varios movimientos huelguistas reclamando derechos civiles para la población obrera y campesina.

Sólo hasta la década de los 30, bajo el manto de los gobiernos liberales y la Revolución en Marcha de López Pumarejo, el movimiento de mujeres logró consolidarse entorno a una agenda feminista en búsqueda de derechos civiles y políticos para las mujeres. Ese pensamiento tomaba cada vez más fuerza a nivel continental. **En 1944 surge la Unión Femenina de Colombia, Alianza Femenina y Agitación Femenina.** Dichas organizaciones centraron sus esfuerzos en alcanzar el derecho al sufragio, que llegaría casi 10 años después.

En 1948 durante la XI Conferencia de la OEA es aprobada la “**Convención sobre los derechos políticos y civiles de las mujeres”**. Así pues, bajo el gobierno de Rojas Pinilla, tres mil mujeres, lideradas por **Esmeralda Arboleda, Magdalena Feti e Isabel Lleras**, entregan un memorial al gobierno exigiendo el cumplimiento de esa Convención. Y es la presión del movimiento de mujeres lo que permite que sean incluidas en la reforma constitucional de **1954 y logren el derecho al voto.**

### **La lucha reciente** 

Desde los años setenta la consolidación de la agenda feminista ha estado determinada por la ruptura con partidos de izquierda. Es decir, **mujeres que resuelven romper con la militancia partidista para enfocarse en la agenda de género** que en los partidos de izquierda seguía siendo despreciada. Paralelamente al espacio intelectual e izquierdista del movimiento de mujeres, empiezan a surgir a finales de los años setenta organizaciones populares de mujeres, como es el caso de la Organización Femenina Popular de Barrancabermeja.

En los años ochenta las mujeres desempeñaron un papel determinante en la conformación de nuevos espacios políticos, como la Unión Patriótica. Pero sólo hasta la Asamblea Constituyente del año 91 la agenda feminista toma vida cuando cuatro mujeres son elegidas como constituyentes. Sin embargo, sólo **Aida Abella y María Teresa Garcés logran darle eco a la agenda feminista en los debates y decisiones del pleno.** Es así como logran incluir en la nueva Carta política, asuntos como **la participación política de las mujeres, la pluralidad de parejas y credos, o la despenalización del ejercicio de la homosexualidad**. Sin embargo, seguían sin debatirse derechos como la interrupción voluntaria del embarazo o el matrimonio entre personas homosexuales.

En los últimos años el movimiento feminista ha logrado consolidarse políticamente, tanto en militancia partidista como en el ámbito académico, pero además ha resurgido en medio de la luchas campesinas y étnicas. El flagelo de la guerra permitió que se evidenciara la múltiple vulnerabilidad a la cual estaba sometida la mujer colombiana y, es a partir de allí, que **la terminación política del conflicto armado es una agenda asumida integralmente por los movimientos de mujeres. **Asimismo, expertos aseguran que el posicionamiento de problemáticas como la violencia doméstica en todo el territorio nacional, ha sido todo un triunfo de las mujeres organizadas en las regiones.

La participación femenina en cargos de elección popular sigue siendo evidentemente menor a la cifra masculina, sin embargo, el nuevo escenario político del país de cara la terminación del conflicto, le ha permitido **al movimiento feminista un posicionamiento particular como protagonista de las movilizaciones por la paz**.  Aunque aún existen vacíos, las organizaciones de mujeres aseguran que el final de la guerra, representa la posibilidad de alanzar derechos que aún no han sido reconocidos. El mérito hoy día lo tienen las mujeres que resisten hablando fuerte y evidenciando las problemáticas que la guerra invisibilizó.

###### Reciba toda la información de Contagio Radio en [[su correo]
