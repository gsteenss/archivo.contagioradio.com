Title: ¿Por qué decir no a 'La Teletón'?
Date: 2016-02-27 16:28
Category: DDHH, Nacional
Tags: discapacidad, Teletón
Slug: por-que-decir-no-a-la-teleton
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Teleton.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Patria 

<iframe src="http://co.ivoox.com/es/player_ek_10590204_2_1.html?data=kpWim5WWdJWhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5ynca%2FjjMaYzsaPuMbgxtmSpZiJhpTijJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [27 Feb 2016]

Más de **15 fundaciones y colectivos de personas en condición de discapacidad, firmaron un comunicado en el que explican a la población colombiana, por qué es importante no apoyar 'La Teletón'** que cada año anuncian los medios privados de televisión, para recaudar fondos y construir centros de rehabilitación a partir del pesar de la sociedad, como lo asegura Andrés Montoya, representante legal de la Fundación Grandes Transformaciones en Cali, uno de los firmantes.

De acuerdo con él, 'La Teletón', es un show mediático, que revictimiza a la población en condición de discapacidad y que además solo atiende un tipo de discapacidad que es la física, entre otros argumentos presentados en el comunicado.

El llamado es para que estas buenas acciones de las personas, se encaminen, no en dar plata, sino en generar procesos reales de inclusión de la población en condición de discapacidad, generando opciones de trabajo para ellos, espacios públicos óptimos, pero sobre todo, que el **Estado colombiano, aplique las Leyes existentes para garantizar la vida de digna de estas personas.**

### Comunicado: 

*Las personas en condicion de discapacidad, sus familias, cuidadoras, cuidadores y organizaciones, en todo el territorio Colombiano, rechazamos con vehemencia la realización de la TELETÓN por las siguientes razones:*

1.  *La TELETÓN está constituida legalmente como una IPS (institución prestadora de servicios) y en consecuencia no necesitan recaudar dinero de empresas privadas ni de la m[ayoría de los bolsillos caritativos de los Colombianos para funcionar.]{.text_exposed_show data-iceapw="10"}*
2.  *[se atenta en contra de la dignidad de las personas en condición de discapacidad, al mostrar en los medios masivos de comunicación una imagen negativa que refuerza los estereotipos e imaginarios de discriminación y exclusión que existen socialmente hacia la discapacidad.]{.text_exposed_show data-iceapw="41"}*
3.  *[No queremos generar lastima, no queremos que nos tengan pesar, no queremos pedir limosna ni mycho menos que terceros se lucren con el argumento de ayudar a las personas con discapacidad.]{.text_exposed_show data-iceapw="31"}*
4.  *[la TELETÓN atiende solamente la discapacidad física en Colombia, desconociendo los demás tipos de discapacidad.]{.text_exposed_show data-iceapw="15"}*
5.  *[La TELETÓN no representa a las personas con discapacidad, ni tampoco sus expectativas y necesidades.]{.text_exposed_show data-iceapw="15"}*
6.  *[Lo que necesita la población en condición de discapacidad en Colombia no son mas centros de rehabilitación, si no calidad, eficacia y celeridad en el acceso a los servicios de salud, oportunidades educativas, culturales, deportivas y recreativas.]{.text_exposed_show data-iceapw="37"}*
7.  *[Al igual que el acceso en igualdad de condiciones al [empleo]{#IL_AD2 .IL_AD}, en condiciones dignas y acordes al [perfil]{#IL_AD1 .IL_AD} y la experiencia de la persona.]{.text_exposed_show data-iceapw="23"}*
8.  *[Sin olvidar la importancia de eliminar las barreras arquitectónicas, sociales y actitudinales que son los verdaderos factores discapacitantes.]{.text_exposed_show data-iceapw="18"}*
9.  *[La discapacidad no esta en la diferencia corporal de la persona, esta en las actitudes de marginación, intolerancia, incomprensión, negación de oportunidades, rechazo, señalamiento y subvaloración por parte de la sociedad hacia las personas denominadas con “discapacidad”.]{.text_exposed_show data-iceapw="37"}*

*[Este mensaje no es una cadena, tampoco te pasará algo extraordinario si reenvías por lo menos a 10 amigos este mensaje en los próximos 5 minutos, pero si ayudarás a dignificar y hacer valer los derechos de los niños, niñas, jóvenes, adultos y adultos mayores en condición de discapacidad.]{.text_exposed_show data-iceapw="49"}*

*[NO olvides apoyar en las redes sociales, en Twitter **[[‪]** y en facebook en la fanspage **Teletón no Más** y dale Me gusta. Comparte este mensaje por Whats app al mayor número de personas que te sea posible.]{.text_exposed_show data-iceapw="40" data-iceapc="4"}*
