Title: Campesino habría sido asesinado por policías en Tumaco
Date: 2020-02-03 10:41
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato, Cultivos de uso ilícito, ejercito
Slug: campesino-habria-sido-asesinado-por-policias-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/ejercito.militares.militar.soldado.soldados.afp_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La comunidad del Río Mejicano, en Tumaco-Nariño, denunciaron que el pasado primero de febrero, el campesino Segundo Girón fue víctima de un impacto de bala que habría provenido de la Policía, cuando realizaban un operativo de erradicación forzada. Debido a la gravedad de la herida, el hombre falleció mientras era trasladado a un centro médico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con la denuncia, los grupos antinarcóticos llegaron hasta el Consejo Comunitario para iniciar acciones de erradicación forzada de cultivos de uso ilícito. Sin embargo la comunidad se opuso, debido a que algunas de las familias ya se habían acogido a los planes de sustitución de cultivos pactadas en el Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente, la Policía habría reaccionado con el uso de gases lacrimógenos y disparos para dispersar a los campesinos que defendían sus cultivos y sería en ese momento, cuando Girón fue impactado por la bala.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comunidad aseguró que los campesinos que se encontraban en el lugar no portaban armas y estaban en completa indefención. Frente a estos hechos la Defensoría del Pueblo exigió el esclarecimiento judicial y afirmó que " la solución al problema de las drogas requiere integralidad en las acciones del Estado y esencialmente que se adecuen al respeto de los Derechos Humanos".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Masacre del Tandil cometida por policías en Tumaco sigue en la impunidad

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 5 de octubre de 2017, mientras habitantes de la vereda El Tandil formaban un cordón humano, oponiéndose a que grupos antinarcóticos erradicara a la fuerza los cultivos de uso ilícito, uniformados de la Policía dispararon contra la población **asesinando a 7 personas e hiriendo a otras 25**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la fecha, este proceso continúa en completa impunidad sin que se esclarezcan responsabilidades por parte de la Fuerza Pública en los asesinatos. Es importante recalcar que durante los hechos, defensores de derechos humanos y periodistas también fueron víctimas de hostigamientos por parte de los uniformados. (Le puede interesar: ["A dos años de la masacre de El Tandil, en Tumaco persiste la impunidad"](https://archivo.contagioradio.com/a-dos-anos-de-la-masacre-de-el-tandil-en-tumaco-persiste-la-impunidad/))

<!-- /wp:paragraph -->
