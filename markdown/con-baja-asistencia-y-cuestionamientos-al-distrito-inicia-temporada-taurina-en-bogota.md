Title: Con baja asistencia y cuestionamientos al distrito inicia temporada taurina en Bogotá
Date: 2018-01-22 14:41
Category: Animales, Nacional
Tags: Bogotá, corridas de toros, Plaza de Toros, Taurinos, temporada taurina
Slug: con-baja-asistencia-y-cuestionamientos-al-distrito-inicia-temporada-taurina-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/toros-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Yesid Lancheros] 

###### [22 Ene 2018] 

Teniendo en cuenta el inicio de la temporada taurina en Bogotá, las personas que defienden los derechos de los animales **rechazaron esta actividad** y le pidieron al Ministro de Hacienda que desembolse los recursos necesarios para preguntarle a los ciudadanos  si están de acuerdo con que se realice este espectáculo en la capital.

De acuerdo con Julián Coy, integrante de la Plataforma ALTO, “la Corte Constitucional ha emitidos dos sentencias en donde le pidió al Congreso de la República **abrir el debate para prohibir las corridas de toros** y además revivió la posibilidad de re abrir la consulta popular”.

Manifestó que en el Congreso hay una serie de proyectos legislativos que buscan abolir las corridas en Colombia. Sin embargo, la consulta “está en veremos porque el Ministro de Hacienda, Mauricio Cárdenas, **no ha entregado los dineros necesarios** para su convocatoria, aunque ese es su deber”.

### **Alcaldía dispuso de 2000 Policías para “resguardar” la Plaza de Toros** 

Durante el fin de semana que pasó, Enrique Peñalosa **acordonó la plaza de toros con un número significativo de Policías** y además cerró algunas vías de acceso al centro de la ciudad. Esto, para los defensores de los animales, va en contravía de los derechos a la movilidad de los ciudadanos y “es el colmo que Peñalosa ponga tantos Policías a cuidar un evento privado”. (Le puede interesar: ["Votación unánime aprueba en primer debate la abolición de las corridas de toros"](https://archivo.contagioradio.com/votacion-unanime-aprueba-primer-debate-la-abolicion-corridas-toros/))

Además, varias personas denunciaron que los asistentes al evento **consumieron alcohol en espacio público**, algo que está prohibido por el Código de Policía. Por esto, la Plataforma ALTO radicó la queja ante la alcaldía pues “por fotos se ha sabido que se permitió el ingreso de bebidas alcohólicas a la plaza de toros y la Policía no realizó los controles requeridos”.

Denunciaron que desde la alcaldía se está promoviendo el concepto de **ciudadanos de primera y segunda categoría** teniendo en cuenta que ninguna persona puede consumir bebidas alcohólicas en espacio público. Por esto, están reuniendo los materiales probatorios para poner las denuncias correspondientes y reiteraron que “los taurinos en Bogotá están protegidos en exceso por la alcaldía”.

### **Ministro del Interior hizo un llamado para que el Congreso siga debatiendo ley que prohíbe los toros** 

Mientras se realizaba esta actividad, el Ministro del Interior recordó que la Corte Constitucional se ha pronunciado para que el Congreso de la República **legisle en contra de la continuación de estos eventos** y dijo que “la fiesta brava se está extinguiendo por decisiones judiciales y un cambio en la cultura ciudadana”. (Le puede interesar: ["Empieza el trámite de la ley que busca abolir las corridas de toros en Colombia"](https://archivo.contagioradio.com/empieza-tramite-la-ley-busca-abolir-las-corridas-toros-colombia/))

Esto teniendo en cuenta la **poca afluencia de personas que tuvo el inicio de la temporada taurina**. Julián Coy indicó que sólo asistieron 2 mil personas a un lugar que tiene capacidad para albergar 10 mil personas. Para él “esto dice mucho de la capacidad de convocatoria que tienen estos grupos en Bogotá”.

### **Consulta popular se ajusta a la constitución** 

Si bien aún no se ha definido la fecha para la realización de la consulta popular, los grupos que están en contra de las corridas de toros han manifestado que **es deber del Estado promulgar la realización** de este tipo de mecanismos para que los ciudadanos sean quienes decidan si continúan o no las corridas de toros. Ante esto, le han pedido celeridad al Ministerio de hacienda para el desembolso de los recursos.

Coy manifiesta que “el Ministro tiene una interpretación increíble porque ha dicho que son los entes territoriales quienes deben financiar estos mecanismos de participación, cuando es obvio que debe ser el Gobierno”. Además, precisó que la consulta no cuesta 40 mil millones como se ha dicho sino que sus costos se reducen a una décima parte si se hace el mismo día de las elecciones del Congreso.

<iframe id="audio_23289837" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23289837_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div class="text_exposed_show">

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

</div>

<div class="osd-sms-wrapper">

</div>
