Title: Asesinan a Deimer Alberto Lucas, hijo del Gobernador Indígena del pueblo Senú en El Bagre, Antioquia
Date: 2020-10-15 12:38
Author: AdminContagio
Category: Actualidad, Nacional
Tags: bajo cauca antioqueño, Deimer Alberto Lucas, Organización Indígena de Antioquia
Slug: asesinan-a-deimer-alberto-lucas-hijo-del-gobernador-indigena-del-pueblo-senu-en-el-bagre-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Asesinan-a-Deimer-Alberto-Lucas.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este martes se dio a conocer por parte de la Organización Indígena de Antioquia -[OIA](https://twitter.com/OIA_COLOMBIA)- **el asesinato de Deimer Alberto Lucas Lora cerca de la comunidad Senú** **«Luis Cano» del municipio de El Bagre ubicado en el Bajo Cauca antioqueño.** (Lea también: [Es asesinado Euloquio Pascal líder Awá, en Nariño](https://archivo.contagioradio.com/es-asesinado-euloquio-pascal-lider-awa-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Deimer Alberto tenía 16 años y era hijo del Gobernador Mayor Senú del municipio de El Bagre, Antioquia.** Ni Deimer, ni su familia tenían amenazas en su contra según pudo constatar la autoridad indígena. (Lea también: [Ana Lucía Bisbicús, lideresa indígena Awá es asesinada en Nariño](https://archivo.contagioradio.com/ana-lucia-bisbicus-lideresa-indigena-awa-es-asesinada-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según versiones de la comunidad, el asesinato se produjo mientras la víctima se dirigía hacia una quebrada en compañía de un amigo, cuando fueron atacados. La persona que acompañaba al joven logró escapar del ataque, según informó la OIA.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La Organización Indígena del Cauca, hizo un llamado a los actores armados para que respeten la vida de las comunidades étnicas y cesen su accionar violento** y a su vez exigió a las instituciones nacionales e internacionales el acompañamiento a las comunidades en el territorio donde, según la OIA, se ha venido recrudeciendo sistemáticamente la violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Deimer Alberto Lucas, una víctima más de la violencia contra los pueblos  indígenas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según [registros](http://www.indepaz.org.co/paz-al-liderazgo-social/) del Instituto de Estudios para el Desarrollo y la Paz -Indepaz-, en lo que va corrido del año 2020 se han perpetrado 227 asesinatos en contra de líderes, lideresas sociales y personas defensoras de Derechos Humanos, 84 de los cuales pertenecían a comunidades indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta y otra razones se convocó la Minga Indígena que hoy se dirige desde Cali hacia Bogotá, buscando un encuentro con el presidente Iván Duque, ante la negativa del mandatario para reunirse con ellos en el territorio. (Le puede interesar: [Minga del 10 de Octubre avanza a pesar de la estigmatización](https://archivo.contagioradio.com/minga-del-10-de-octubre-avanza-a-pesar-de-la-estigmatizacion/))

<!-- /wp:paragraph -->
