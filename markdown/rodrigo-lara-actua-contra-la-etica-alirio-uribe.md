Title: Rodrigo Lara actúa contra la ética: Alirio Uribe
Date: 2017-10-25 13:35
Category: Nacional, Paz
Tags: acuerdos de paz, Cambio Radical, Rodrigo Lara
Slug: rodrigo-lara-actua-contra-la-etica-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/rodrigo_lara_restrepo_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [25 Oct 2017] 

Dos han sido las conductas calificadas como faltas contra la ética y el ejercicio leal de la política por parte de Rodrígo Lara en un solo día. Por una parte pretende impedir el derecho ciudadano de integrantes de las FARC a participar del escenario de democracia en la Cámara de Representantes, y en segundo **lugar pretendió hacerle trampa al trámite de la reforma política con actuaciones irregulares en la madrugada de este miércoles**.

Por ello varios congresistas y organizaciones sociales están pidiendo la renuncia de Rodrigo Lara a la presidencia del organismo, lo que facilitaría el proceso de implementación. De acuerdo con el representante Alirio Uribe, **este acto demuestra que tanto Lara como el partido al que representa, Cambio Radical, son enemigos de la paz**.

Para Uribe esta exigencia **es “inconstitucional y discriminatoria”** debido a que los integrantes de este partido político se sometieron al proceso de paz, por lo tanto, algunos de ellos se encuentras amnistiados mientras que a otros se les suspendieron los procesos, razón por la cual son ciudadanos y pueden estar en cualquier recito, además el representante expresó que el Congreso de la República es “el escenario más natural para el debate político”.

“Es una actitud agresiva y discriminatorio contra quienes han tomado la decisión de dejar las armas e ingresar a la vida pública” afirmó el representante. (Le puede interesar: ["Cambio Radical y Centro Democrático se unen para hacer trizas el acuerdo: FARC"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-unen-para-hacer-trizas-los-acuerdos-farc/))

### **Plenaria a las 00:05 am** 

Otra de las acciones de Lara fue citar plenaria sobre Reforma electoral a las 00:05 am, sin anunciar la convocatoria previamente, situación que para Uribe buscaba romper el quorum y que cuatro artículos fueran aprobados sin debate, “por fortuna nos sublevamos todos los congresistas y **pusimos una proposición de suspensión de la plenaria para que continúe hoy a las 11 am**”.

Estas medidas tomas por Lara, para congresistas como Uribe, demuestran su “mezquindad” hacia la construcción de paz y señaló que por acciones como esta debería considerarse la revocatoria de Lara como presidente de la Cámara de Representantes, sin embargo, consideró que, **debido a los juegos de poder al interior del congreso es difícil que una decisión como esta se lleve a cabo**.

Congresistas como Angélica Lozano y Germán Navas han afirmado que este tipo de maniobras no se habían visto en ningún presidente de la cámara quienes señalaros que esta es una ofensa a sus compañeros. Así mismo lo manifesto el ministro del Interior **Guillermo Rivera, quién ejerció como representante y tampoco había sido testigo de este tipo de maniobras**. (Le puede interesar: ["Corte Constitucional obliga a funcionarios públicos a cumplir con el acuerdo de paz"](https://archivo.contagioradio.com/corte-constitucional-obliga-a-funcionarios-publicos-a-cumplir-el-acuerdo-de-paz/))

### **Las preocupaciones con la JEP** 

El texto consensuado entre la Fiscalía y la CSIVI ya fue expuesto en senado y próximamente pasará a debate en Cámara, no obstante, para Alirio Uribe hay un artículo que es preocupante debido a que podría ser inconstitucional y es el de posponer la participación política de **las FARC hasta que estén a paz y salvo con la JEP “acto que violaría no solo los acuerdos de paz sino la constitución**”.

Finalmente, sobre las recomendaciones que hizo la Corte Penal Internacional, Uribe señaló que “los parámetros deben ser adoptados por la Corte Constitucional para que se quiten todas las talanqueras” impuestas a la Justica en puntos tan importantes como la cadena de mando.

<iframe id="audio_21688675" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21688675_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
