Title: "La paz se construye sin crímenes de Estado" MOVICE, 10 años
Date: 2015-03-07 03:02
Author: CtgAdm
Category: DDHH, Nacional
Tags: crimenes, estado, MOVICE, paz, víctimas
Slug: la-paz-se-construye-sin-crimenes-de-estado-movice-10-anos
Status: published

###### Foto: Contagio Radio 

El viernes 6 de marzo del 2015, se cumplen 10 años del Movimiento Nacional de Víctimas de Crímenes de Estado.

En el año 2003 el gobierno de Álvaro Uribe Vélez anunció al país el inicio del proceso de desmovilización de las AUC, en un intento por disfrazar el acuerdo político que lo llevó a la presidencia y a sus aliados al Congreso de la República. Se presentó un proyecto de ley de alternatividad penal, que buscaba dar carácter político a los crímenes cometidos por el paramilitarismo y con ello, blindar jurídicamente a los sectores que habían incitado la violencia política en el país.

La respuesta del MOVICE fue contar la verdad y confrontar a los genocidas con el pueblo, las Audiencias Públicas por la Verdad y los Encuentros Nacionales y Regionales del MOVICE permitieron a las víctimas y sus familias romper el silencio en que se encontraban las poblaciones vejadas por el terrorismo de Estado y la estrategia paramilitar en gran parte del territorio nacional.

\[embed\]https://www.youtube.com/watch?v=JjgvBAeftqc&feature=youtu.be\[/embed\]
