Title: Empresa Ingeniería de Vías S.A. está acabando con el Río San Jorge en Cauca
Date: 2015-09-15 15:59
Category: DDHH, Nacional
Tags: Cauca, comunidades afrodescendientes en Colombia, derecho a la consulta previa, extracción de material de río, Galindez, Palo Verde, Patía
Slug: empresa-ingenieria-de-vias-s-a-desconoce-el-derecho-a-la-consulta-previa-de-comunidades-afrodescendientes-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Extracción-de-material-del-río-San-Jorge.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CC el Pilón 

###### [15 Sept 2015] 

Pescadores y familias habitantes del territorio colectivo del Pilón en Galindez, Cauca, se están quedando sin alimentos, sin casa y sin Río. Según denuncian los afrodescendientes, la empresa Ingeniería Vías S.A está acabando con el Río San Jorge que ya perdió su acostumbrado caudal y su curso habitual por la extracción minera inconsulta que  se realiza desde hace 10 años.

Eduardo González, integrante del Concejo Comunitario Corredor Panamericano el Pilón, afirma que están siendo **afectadas más de 40 familias que toman agua directa del río San Jorge. En el área de explotación, 80 familias resultaron perjudicadas por la desviación del  mismo río,** ya que se dañaron los cultivos de pan coger de las comunidades.

La compañía llegó al territorio colectivo en el año 2005, con el objetivo de explotar el material de arrastre del río, **sin cumplir el proceso de consulta previa de las comunidades negras y desconociendo los derechos de la asociación de areneros que vivían de esa actividad que da sustento a 30 familias.**

El Ministerio del Interior verificó todas las inconsistencias de explotación y las condiciones de vida de los afrodescendientes, sin embargo, **no hay una respuesta sobre la violación del derecho a la consulta previa y el desconocimiento de la empresa de los derechos vulnerados.**

La explotación que realiza Ingeniería Vías S.A es directa, introduciendo maquinas retroexcavadoras, que se abren huecos de aproximadamente 2 metros de profundidad y d**iariamente las volquetas cruzan el río de 150 a 200 veces, asegura el líder comunitario.**
