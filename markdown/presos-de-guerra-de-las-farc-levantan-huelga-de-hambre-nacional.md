Title: Presos de guerra de las FARC levantan huelga de hambre nacional
Date: 2015-11-27 17:44
Category: Judicial, Nacional
Tags: crisis carcelaria, FARC, Huelga de hambre, presos de guerra, presos politicos, proceso de paz
Slug: presos-de-guerra-de-las-farc-levantan-huelga-de-hambre-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Crisis-humanitaria-cárceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.defensoria.gov.co]

###### [27 de Nov 2015]

Mediante un comunicado, los prisioneros políticos de guerra de las FARC-EP anunciaron el **fin de la huelga carcelaria a nivel nacional, tras el anuncio del gobierno sobre la liberación de los 30 guerrilleros,** así mismo, resaltan como un gesto de paz las brigadas de salud al interior de los centros carcelarios, a los que se comprometió el gobierno de Santos.

De acuerdo a esas medidas que se tomaron, los prisioneros de guerra han decidido “asistir al desescalamiento , y dar por terminada esta histórica jornada carcelaria”, pues para ellos, “el Gobierno Nacional y las clases privilegiadas han razonado en la concordancia y sensatez recíproca con su contraparte”.

Adicionalmente expresaron que  **“los gestos de paz no son muestra de debilidad, son  manifestaciones de voluntad de paz,** es caminar no con las  pisadas de la voluntad de poder, es caminar en el diálogo y la práctica”.

Cabe recodar que  la huelga de hambre duró 17 días en 21 cárceles del país, en la que más de 1500 presioneras y prisioneros de guerra exigían garantías frente al derecho a la salud , la libertad y la vida de más de **80 integrantes en estado grave de salud, lisiados y heridos de guerra, madres gestantes y lactantes, y  tercera edad.**
