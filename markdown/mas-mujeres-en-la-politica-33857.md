Title: "Con más mujeres en la política Colombia sería un paraíso"
Date: 2016-12-19 14:42
Category: Mujer, Nacional
Tags: Concejala, derechos de las mujeres, mujeres, Oxfam
Slug: mas-mujeres-en-la-politica-33857
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Mujeres-Velez-e1482170360507.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [19 Dic. 2016] 

Las negociaciones entre el gobierno de Juan Manuel Santos y la guerrilla de las FARC-EP han abierto la puerta a que en **el país se conozcan cientos de iniciativas que las comunidades indígenas, afrodescendientes, mestizas y campesinas han trabajado desde hace años en el tema de paz.**

Dichas iniciativas han nacido dada la necesidad de proteger sus territorios, sus vidas y las comunidades mismas. Pero también para han servido para el empoderamiento de sus integrantes ante diversidad de conflictos como **la minería, la agricultura industrial a gran escala, las “bandas criminales” o grupos paramilitares presentes en sus territorios así como la falta de aplicación de ciertas políticas públicas.**

Los sectores sociales han aportado mucho a la paz en Colombia y las mujeres, han sido esas reconstructoras del tejido social como parte de esa lucha.

En Barbosa, Santander, lugar conocido como la “Puerta de Oro de Santander” en Colombia, **hace más de 11 años un grupo de mujeres se ha dado a la tarea de trabajar en talleres de empoderamiento y liderazgo.**

**“Trabajamos específicamente en el tema de posicionar y visibilizar los derechos de las mujeres y prevenir violencias y discriminaciones”** dice de manera contundente Sandra Sáenz directora de la Corporación Mujeres y Saberes y de la Red de Mujeres para el empoderamiento político y económico de la Provincia de Vélez.

**Hace 11 años no existía en la Provincia de Vélez una organización que se preocupara por el tema de las mujeres,** sus realidades, sus proyectos y sus necesidades, por ello los planes de desarrollo municipal no tenían un enfoque de género.

Sin embargo y entendiendo la importancia del tema 11 años después y “sabiendo que hace mucho por conseguir” dice Sandra, se han logrado cosas. **A nivel departamental estas mujeres han logrado tener una política pública de mujeres desde el año 2010 y algunos escenarios municipales han logrado tener mujeres en dichos espacios.**

Para Sandra, el trabajo al que se ven enfrentados los hombres para acceder a cargos públicos es mucho menor que el de las mujeres “en esta cultura patriarcal nuestra, ellos - los hombres- llegan muy fácil, casi que por inercia”.

Pero además agrega **“hay hombres que no saben leer pero llegan a ser Concejales, a nosotras las mujeres se nos exige y mucho más cuando queremos llegar a esos escenarios. Aunque nosotras nos lo estamos auto-exigiendo porque consideramos que es muy serio legislar, que se debe hacer con más responsabilidad”.**

Las transformaciones que hacen las mujeres en la región y los liderazgos que se han dado van encaminados a la búsqueda del bien común. Un ejemplo de ello es que estas mujeres han construido un vivero que les ha permitido un empoderamiento económico.

No puede haber una democracia verdadera si se sigue discriminando e ignorando a las mujeres, es una de las grandes conclusiones a las que han llegado estas mujeres, quienes han entendido y apropiado su importante papel en la historia de Colombia.

**Paz con mujeres**

Las mujeres de esta red han estado trabajando desde múltiples miradas para poder realizar propuestas para una Colombia luego de la firma de los acuerdos de paz, y en la actualidad de su implementación.

**“Quisiéramos que en esos pos acuerdos hubiera una inclusión mucho mayor de las mujeres”** dice sin titubear Sandra. Según esta lideresa hay que pensar los proyectos no pueden ser asistencialistas, sino que por el contrario permitan a las mujeres mejorar su situación económica.

Y concluye diciendo **“No hay que olvidar que la mujeres somos las que estamos soportando la vida familiar y social de los municipios”**.

<iframe src="http://co.ivoox.com/es/player_ek_15179993_2_1.html?data=kpqemZ6dfZShhpywj5aUaZS1lZ6ah5yncZOhhpywj5WRaZi3jpWah5yncbTVz8nfw5C3aaSnhqaex9PeaZO3jLLizMrWqdSf2pDAw8fJtsbnjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Ejemplo de trabajo para alcanzar el liderazgo**

Blanca Nieves Cadena es una mujer concejala del municipio de Chipata en Santander. Este es su segundo periodo como representante de las peticiones de las mujeres en esa cartera municipal, y divide sus labores entre el hogar y la política.

Fue un trabajo largo, no logró alcanzar este cargo que hoy tiene de manera sencilla. Cuenta que desde los 10 años comenzó a ganarse su lugar desde los espacios más pequeños, como los de la catequesis y los grupos juveniles, donde estuvo y desde donde además logró darse cuenta que quienes los lideraban eran mujeres.

“Es un proceso de hace 27 años” dice Blanca, quien debe levantarse todas las mañanas, a las 4:30 a.m. para hacer el desayuno, arreglar la ropa a su esposo y alistarse para ir a sesionar en el Concejo, lugar del cual es la presidenta.

Su esposo y sus dos hijos la han apoyado en sus sueños, sin embargo “es algo muy pesado” dice ella “pero querer es poder” manifiesta. Le puede interesar: [Mujeres listas y preparadas para trabajar por la paz](https://archivo.contagioradio.com/mujeres-preparadas-para-la-paz/)

<iframe src="http://co.ivoox.com/es/player_ek_15180073_2_1.html?data=kpqempWUe5Shhpywj5WZaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncaPgwtPQw5CyrcbqxtiYpcbIqc_VhpewjajTssTZy8bZw5CnrMrkwtmSpZiJhZKftMbb1sbSqMbmjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**La unión hace la fuerza**

De esta manera Blanca comienza su frase para poder manifestar el porqué es importante que cada vez más mujeres hagan parte de la política **“nosotras las  mujeres debemos ser unidas, por qué el machismo reina. No es una tarea fácil, pero estar en la política nos va a ayudar a defendernos, a defender nuestros derechos”.**

Y es que según la Corporación Mujeres y Saberes y la Red de Mujeres para el empoderamiento político y económico de la Provincia de Vélez el camino no ha sido fácil, pero Blanca dice “la política sin mujeres, no es política”.

**Sacar adelante el proyecto político que representa Blanca y estudiar una carrera son los sueños que tiene esta concejala**, que no es solamente la voz de una mujer en el Concejo, sino que por el contrario a través de ella se hace eco de muchas mujeres Chipatences que se han empoderado política y económicamente, y que hoy son ejemplo a seguir en un país en un eventual escenario de posconflicto.

Por su parte, Ingrid Arce quien también es Concejala, en este caso del municipio de Barbosa quiso agregar "Si más mujeres estuvieran en la política Colombia sería un paraíso (...) por ello sueño algún día en Colombia poder conocer una mujer presidenta".

Este proyecto, motivado por Planeta Paz 1 y Oxfam, hace parte de la Agenda Común para la Paz que se ha gestado desde los territorios y que ha sido un proceso de diálogo y reflexión colectiva, donde diversas organizaciones sociales populares aportan su visión y propuestas en torno a la construcción de la paz. Le puede interesar: [Acuerdo de Paz puede ser inicio para reparar mujeres en Colombia](https://archivo.contagioradio.com/acuerdo-de-paz-inicio-para-reparar-mujeres/)

<iframe src="http://co.ivoox.com/es/player_ek_15180178_2_1.html?data=kpqempWVe5mhhpywj5aUaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncariyNfWxpCltsTZhpewjajTssTZy8bZw5CmpdPW0NjOjbjFstXVz8nS1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
