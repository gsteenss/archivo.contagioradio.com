Title: Comunidades indígenas de Jiguamiando, Chocó asediadas por paramilitares
Date: 2017-03-09 18:48
Category: DDHH, Nacional
Tags: Chocó, paramilitares
Slug: pparamilitares_jiguamiando_choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/charlottekesl_pbi_407-e1489103048715.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: PBI 

###### [9 Mar 2016]

La Comisión Intereclesial de Justicia y Paz, denuncia que inició una ofensiva neoparamilitar en Jiguamiando, Chocó, donde este jueves a las 10:00 de la mañana, más de 40 neoparamilitares de las **Autodefensas Gaitanistas de Colombia ingresaron**  
**a predios del resguardo indígena Urada- Jiguamiandó.**

Según la denuncia, **hombres armados, vestidos de camuflado y con radios de comunicación,** ingresaron a la comunidad de Bidoquera- Ancadía, a la vivienda donde se encontraba una mujer junto a cuatro niños. Los hombres aseguraron que se quedarían en el lugar, y amenazaron a las personas, si llegaban a reportar su presencia.

De acuerdo con la comunidad, **cerca de un centenar de paramilitares ingresaron por el río Tamboral y se trasladaron a la cabecera del río Pavarandó**. Allí desarrollan operaciones de control del resguardo indígena Urada Jiguamiandó y del territorio de las comunidades negras de Jiguamiandó. La denuncia indica que la estructura paramilitar estaría plenadno iniciar el control de la finca Santa Rosa, en Puerto Lleras; en la Loma en torno a la Zona Humanitaria de Nueva Esperanza, y otro punto en el peaje a Urada hacia Pavarandó. [(Le puede interesar: Paramilitares se toman caserío de Domingodó en el Chocó)](https://archivo.contagioradio.com/paramilitares-se-toman-caserio-de-domingodo-en-el-choco/)

Cabe recordar que **desde el año pasado, las comunidades indígenas y afros denuncian la presencia neoparamilitar** en el entorno del territorio de Jiguamiandó en Pavarandó, Mutatá, Llano Rico, Cetino y Brisas de Curvaradó, que acompañada del control empresarial ha impedido la restitución efectiva de tierras, como lo concluye el el informe “Una mirada al desplazamiento forzado: persecución penal, aparatos organizados de poder y restitución de tierras en el contexto colombiano", de Abogados sin Fronteras Canadá y la Comisión Intereclesial de Justicia y Paz. [(Le puede interesar: Paramilitares extorsionan y amenazan a pobladores de Murindó, Chocó)](https://archivo.contagioradio.com/paramilitares_amenazan_extorsionan_pobladores_choco/)

###### Reciba toda la información de Contagio Radio en [[su correo]
