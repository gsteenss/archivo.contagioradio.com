Title: ¿Hay un plan para acabar con la memoria de Colombia?
Date: 2019-02-28 15:24
Category: DDHH, Paz
Tags: 'I Encuentro Nacional Postconflicto Salud Mental y Acciones Psicosociales Hacia la Paz', Centro Nacional de Mem, Dario Acevedo
Slug: hay-un-plan-para-acabar-con-la-memoria-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/victimas-movice-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [28 Feb 2019] 

Organizaciones sociales y víctimas del conflicto armado, vienen manifestado sus temores por lo que podría ser un "plan" para acabar con la memoria que se ha construido sobre la responsabilidad de los actores de la guerra, entre los que se encuentran la Fuerza Pública, el paramilitarismo y las guerrillas, que se estaría ejecutando desde el gobierno de Iván Duque.

Este plan tendría como primeros pasos la designación de Dario Acevedo como director del Centro Nacional de Memoria Histórica y las peticiones de renuncia a los directores del Museo Nacional, la Biblioteca Nacional y el Archivo General de la Nación, tres de las instituciones que se han encargado de velar por el cuidado de la memoria histórica del país. De hecho en el Archivo General, **reposan los archivos del DAS que podrían esclarecer relaciones entre esa institución y el paramilitarismo.**

### **Las víctimas retiran sus archivos del CNMH** 

Una de las primeras acciones que han decidido tomar las víctimas del conflicto armado, ha sido retirar sus archivos del Centro Nacional de Memoria Histórica, debido a las afirmaciones pasadas que hizo su nuevo director sobre negar la existencia del conflicto armado en Colombia. (Le puede interesar: ["La ruptura de confianza entre las víctimas y el CNMH"](https://archivo.contagioradio.com/victimas-memoria-historica/))

Según el comunicado, "las víctimas y organizaciones que desde la promulgación de la ley 1448 de 2011, **iniciamos concertaciones de confianza para celebrar, actas, acuerdos y/o convenios con el CNMH** en el objetivo de aportar los archivos físicos y digitales sobre violaciones de Derechos Humanos y del Derecho Internacional Humanitario durante el conflicto armado y la violencia política, **hemos decidido retirar la totalidad de material entregado debido a la falta de garantías reales para su salvaguarda** y buen uso en la reconstrucción de la Memoria encaminada a la consecución de la Verdad que Colombia merece”.

De igual forma el académico Javier Dario Restrepo afirmó en su columna más reciente en el Diario El Heraldo que este tipo de afirmaciones tendrían consecuencias tan graves como decir que "en Colombia el guerrillero no sería guerrillero sino un delincuente o un terrorista, las acciones de los cuerpos de seguridad harían parte de la política de seguridad del ciudadano, y **los acuerdos de paz serían solo un engaño de la guerrilla a gobernantes cándidos o cómplices**". (Le puede interesar:["Ruben Acevedo nuevo director del Centro Nacional de Memoria Histórica"](https://archivo.contagioradio.com/ruben-acevedo-es-el-nuevo-director-del-centro-nacional-de-memoria-historica/))

Sumado a estos hechos, el Departamento de Historia de la Universidad Nacional emitió un comunicado de prensa en el que señaló que les sorprende, el hecho de que Acevedo haya aceptado dirigir una institución que tiene como eje central el conflicto armado y la reconstrucción de memoria desde allí, **razón por la cual le piden que respete la memoria de todas las víctimas. **

### **El plan contra la memoria ** 

El profesor y académico Fabio López De la Roche, aseguró que esta oleada de transformaciones en las instituciones encargadas de proteger la memoria hacen parte de la construcción de la postverdad, es decir "la renuncia a encontrar unos hechos demostrables, a tener una **concepción de país que se base en hechos, y el remplazo de esos hechos por la ideología o las falsas verdades**".

"Hay una visión extrema de la realidad, que se aparta de la consideración juiciosa y objetiva de los hechos para acomodar el análisis de la realidad a una ideología, eso le puede pasar a la izquierda o a la derecha, pero acá estamos asistiendo a una derecha sumamente ideologizada" afirmó el académico.

###### Reciba toda la información de Contagio Radio en [[su correo]
