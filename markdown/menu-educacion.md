Title: menu Educacion
Date: 2019-01-18 04:54
Author: AdminContagio
Slug: menu-educacion
Status: published

[![Estudiantes de U. del Atlántico exigen garantías para el retorno a clases](https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400-216x152.png "Estudiantes de U. del Atlántico exigen garantías para el retorno a clases"){width="216" height="152" sizes="(max-width: 216px) 100vw, 216px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400-216x152.png 216w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400-370x260.png 370w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400-275x195.png 275w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400-550x385.png 550w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400-110x78.png 110w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400-240x170.png 240w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/Captura-de-pantalla-2019-01-14-a-las-6.12.43-p.m.-770x400-340x240.png 340w"}](https://archivo.contagioradio.com/u-del-atlantico-retorno-clases/)  

###### [Estudiantes de U. del Atlántico exigen garantías para el retorno a clases](https://archivo.contagioradio.com/u-del-atlantico-retorno-clases/)

[<time datetime="2019-01-14T18:39:22+00:00" title="2019-01-14T18:39:22+00:00">enero 14, 2019</time>](https://archivo.contagioradio.com/2019/01/14/)  
[](https://archivo.contagioradio.com/como-evitar-que-ministerio-de-ciencia-tecnologia-e-innovacion-se-convierta-en-fortin-politico/)  

###### [¿Cómo evitar que Ministerio de Ciencia, Tecnología e Innovación se convierta en fortín político?](https://archivo.contagioradio.com/como-evitar-que-ministerio-de-ciencia-tecnologia-e-innovacion-se-convierta-en-fortin-politico/)

[<time datetime="2018-12-18T17:00:38+00:00" title="2018-12-18T17:00:38+00:00">diciembre 18, 2018</time>](https://archivo.contagioradio.com/2018/12/18/)  
[](https://archivo.contagioradio.com/cada-institucion-de-educacion-superior-decidira-cuando-levanta-el-paro-unees/)  

###### [Cada Institución de Educación Superior decidirá cuándo levanta el paro: UNEES](https://archivo.contagioradio.com/cada-institucion-de-educacion-superior-decidira-cuando-levanta-el-paro-unees/)

[<time datetime="2018-12-17T14:38:13+00:00" title="2018-12-17T14:38:13+00:00">diciembre 17, 2018</time>](https://archivo.contagioradio.com/2018/12/17/)  
[](https://archivo.contagioradio.com/esteban-mosquera/)  

###### [Esteban Mosquera, víctima del ESMAD en Cauca en el marco del paro estudiantil](https://archivo.contagioradio.com/esteban-mosquera/)

[<time datetime="2018-12-14T14:31:53+00:00" title="2018-12-14T14:31:53+00:00">diciembre 14, 2018</time>](https://archivo.contagioradio.com/2018/12/14/)
