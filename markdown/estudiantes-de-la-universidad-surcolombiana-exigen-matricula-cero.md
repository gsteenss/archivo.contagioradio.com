Title: Sin «matricula cero» estarían en riesgo un 40% de estudiantes de la Surcolombiana
Date: 2020-07-23 23:14
Author: AdminContagio
Category: Actualidad, Educación
Tags: Educación Superior, Matricula Cero, Movimiento estudiantil, Universidad Surcolombiana, Universidades
Slug: estudiantes-de-la-universidad-surcolombiana-exigen-matricula-cero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Estudiantes-Universidad-Surcolombiana-exigen-matricula-cero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Estudiantes exigen matricula cero / Foto: Colectivo Sub-jetividades

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este miércoles 22 de julio se instaló una huelga de hambre por parte de algunos estudiantes de la Universidad Surcolombiana de Neiva, Huila; exigiendo la «matricula cero» para los alumnos de la institución. (Le puede interesar: [La educación superior también requiere medidas para enfrentar el COVID 19](https://archivo.contagioradio.com/la-educacion-superior-tambien-requiere-medidas-para-enfrentar-el-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por medio de un comunicado, el Consejo Superior Estudiantil de la Universidad Surcolombiana -[CSEUS](https://web.facebook.com/CONSEJOESTUDIANTILUSCO)-, señaló que de no aprobarse la «matricula cero», **se estaría poniendo en peligro la permanencia de los estudiantes, pues, se tiene un estimado de deserción académica que oscila entre un 25% a 40%.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, CSEUS manifestó que **el Gobierno Nacional ha aportado aproximadamente 2.500 millones de pesos que  «*son insuficientes para satisfacer las necesidades del estudiantado*».** Por otro lado, señaló que la Gobernación del Huila, pese a que ha expresado su voluntad de aportar recursos aún «*no ha concretado nada*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Consejo Superior hizo dos exigencias puntuales a los gobiernos regional y nacional. **La primera que se destinen «*13 millones de pesos para garantizar la matricula cero de carácter universal*» del segundo semestre del año 2020 y el primero del año 2021.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La segunda, la creación de un fondo financiado con fuentes fijas de ingreso con el cual se pueda garantizar la gratuidad permanente de la matrícula para los estudiantes aún después de superar la contingencia producida por la pandemia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/CONSEJOESTUDIANTILUSCO/videos/916684698806116","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/CONSEJOESTUDIANTILUSCO/videos/916684698806116

</div>

<figcaption>
Instalación de la huelga de hambre - Consejo Superior Estudiantil de la U. Surcolombiana

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:heading {"level":3} -->

### **Estudiantes de otras universidades también se han manifestado para la obtención de la** «**matricula cero**»

<!-- /wp:heading -->

<!-- wp:paragraph -->

La manifestación y huelga de hambre adelantadas por estudiantes de la Universidad Surcolombiana se suma a las ya realizadas por alumnos de otras instituciones educativas como la Universidad de Antioquia y la Universidad Distrital en Bogotá. (Le  puede interesar: [Eduación virtual por Covid-19, una decisión para la que no estamos preparados](https://archivo.contagioradio.com/eduacion-virtual-por-covid-19-una-decision-para-la-que-no-estamos-preparados/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras esta presión de los estudiantes, las directivas de la **Universidad de Antioquia anunciaron hace unos días la aprobación de la** «**matricula cero**» **para sus estudiantes de pregrado durante el segundo semestre del 2020.** Luego de lo cual, pidieron a sus estudiantes, levantar la huelga de hambre que adelantaban.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, el Consejo Superior de la Universidad Distrital en Bogotá, aprobó hace unos días la financiación para garantizar también la «matricula» cero de sus estudiantes de pregrado. El pago de parte de la inversión se haría con recursos del presupuesto de la institución  no ejecutados durante el primer semestre de este año.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, **el rector Ricardo García Duarte y representantes estudiantiles señalaron que esta asignación no era suficiente para costear el programa y que la Universidad podría quedar desfinanciada.** Por ello, instaron a la alcaldesa Claudia López para que desde el Distrito se destinaran recursos adicionales. Por lo que la «matricula cero» para los estudiantes de la Distrital aún no está 100% asegurada.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
