Title: Subcomisión de Género presentará sus resultados el próximo domingo
Date: 2016-07-21 16:39
Category: Nacional, Paz
Tags: comunicado conjunto #81, Diálogos de paz Colombia, Subcomisión de género
Slug: subcomision-de-genero-presentara-sus-resultados-el-proximo-domingo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/subcomision-mujeres-proceso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube ] 

###### [21 Julio 2016]

<div>

De acuerdo con el Comunicado Conjunto N° 81, la Subcomisión de Género ha concluido la **revisión e incorporación del enfoque de género en los puntos referidos a la reforma rural integral, participación política y solución al problema de las drogas ilícitas**, y dará a conocer sus resultados en un acto público el próximo domingo a las 10 de la mañana.

El evento contará con la participación de Phumzile Mlambo-Ngcuka, directora ejecutiva de ONU Mujeres; Zainab Bangura, de la Secretaría General de la ONU para la Violencia Sexual en los Conflictos Armados; Luiza Carvalho, directora regional para las Américas y el Caribe de ONU Mujeres; Belén Sanz, representante de ONU Mujeres en Colombia; María Emma Mejía, representante permanente de Colombia ante la ONU y mujeres de organizaciones sociales colombianas.

"Las delegaciones del Gobierno y las FARC-EP seguiremos trabajando para que la inclusión de género en el Acuerdo Final **visibilice el importante papel de la mujer y de las personas con identidad sexual diversa** en la construcción de una paz estable y duradera", concluye el comunicado.

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

</div>

<div id=":7b" class="ajR" tabindex="0" data-tooltip="Mostrar contenido acortado">

![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif){.ajT}

</div>
