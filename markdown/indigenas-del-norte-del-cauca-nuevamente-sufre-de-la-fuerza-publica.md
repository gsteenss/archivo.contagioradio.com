Title: Indígenas del Norte del Cauca nuevamente sufren represión de la Fuerza pública
Date: 2015-05-29 13:44
Author: AdminContagio
Category: Nacional, Resistencias
Tags: ACIN, Caloto, Cese al fuego, Corinto, Defensoría del Pueblo, desalojos forzosos, Héctro Fabio Dicue, indígenas Norte del Cauca, liberación de la madre tierra, Masacre del Nilo, Ministro de Agricultura, Miranda, Santander de Quilichao
Slug: indigenas-del-norte-del-cauca-nuevamente-sufre-de-la-fuerza-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/image.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario ADN 

<iframe src="http://www.ivoox.com/player_ek_4570156_2_1.html?data=lZqkkpaZeo6ZmKiak5aJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2FYhqigh6aoq8biwtiYxsrQb6%2Fj09nSjcnJsIy3wtrQw5DSucbqwtLS0NnJb9Tpx9fSjcnJb83VjKuah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Héctor Fabio Dicue, ex consejero de la ACIN] 

Continúa la represión por parte del Escuadrón Móvil Antidisturbios, ESMAD contra los indígenas al Norte del Cauca. De acuerdo a  Héctor Dicue, exconsejero de la Asociación de Indígena del Norte del Cauca, ACIN, quien afirma el pasado 21 de mayo, **nuevamente la fuerza pública  realizó un intento de desalojo contra las comunidades** que se encuentra en el proceso de Liberación de la Madre Tierra en Caloto.

Según Dicue, el pasado jueves, los indígenas empezaron a notar la llegada de la fuerza pública; aproximadamente 1500 agentes del ESMAD, Ejercito Nacional, carabineros, y 7 tanquetas estuvieron hasta el domingo pasado desarrollando un operativo de desalojo en la Hacienda la Emperatriz.

La comunidad había decidido no enfrentar a la fuerza pública. Sin embargo, esa acción dejó **4 personas heridas, lo que aumenta a 260 el número de indígenas** que han salido afectados por la represión de de la policía. Además, los cultivos de maíz, fríjol, plátano y yuca, quedaron totalmente destruidos, ya que los agentes del Estado ingresaron al predio protegiendo tractores que al tiempo derribaban las plantaciones.

“La fuerza pública es la que hace el operativo, **no hay presencia de la Defensoría del Pueblo** ni nadie del Estado”, afirma el exconsejero de la ACIN.

La comunidad resiste y asegura que, aunque sigan estos operativos, los indígenas están decididos a continuar con el proceso de Liberación de la Madre Tierra, pero van a analizar cómo enfrentar  la acción arbitraria del gobierno.

El desalojo anterior había dejado **250 heridos y un comunero asesinado.** Luego de eso hubo un tiempo de calma, pero después ocurrió la acción policial el 21 de mayo.

Cabe recordar que desde el pasado 23 de Febrero, comunidades indígenas del Norte del Cauca llevan a cabo este proceso de "Liberación de la madre tierra" para hacer valer las promesas incumplidas del gobierno en cuanto a la entrega de tierras como parte de la **reparación colectiva por la** **masacre del Nilo que contempla 30.000 hectáreas.**

Las tierras reclamadas por los indígenas en los municipios de **Corinto, Miranda y Santander de Quilichao,** asciende a 6.500 hectáreas que el gobierno tiene alquiladas a los ingenios del Cauca, sobre los cuales pesan los intereses del propio Ministro de Agricultura que no se ha pronunciado sobre la reclamación de los indígenas.

Además, Dicue señaló que, tras la suspensión del cese al fuego, la comunidad se encuentra atemorizada y muchas personas de la comunidad ha llegado a irse y dejar sus casas solas por temor a los posibles hostigamientos.
