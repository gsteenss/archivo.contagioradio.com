Title: Puerto Leguízamo, Putumayo, arranca proceso de consulta popular
Date: 2017-08-08 13:54
Category: Ambiente, Nacional
Tags: consulta popular, defensa del territorio, petroleo, puerto leguizamo, Putumayo
Slug: puerto-leguizamo-putumayo-arranca-proceso-de-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PUTUMAYO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [08 Ago 2017] 

Los habitantes del municipio de Puerto Leguizamo en el departamento del Putumayo, **iniciaron el proceso para realizar una consulta popular** que evite que diferentes empresas petroleras lleguen a hacer actividades de exploración minero energéticas en el municipio.

El pasado 5 y 6 de agosto las comunidades campesinas, indígenas y afrodescendientes de este municipio se reunieron con académicos y voceros de otras consultas populares ya realizadas para **trazar una hoja de ruta para el que hacer de las comunidades que buscan defender la vida y el territorio**. (Le puede interesar: ["Proyecto de ley desconoce voluntad del pueblo e consultas populares"](https://archivo.contagioradio.com/consultas-populares/))

El municipio de Puerto Leguízamo se encuentra ubicado en la **subregión del bajo Putumayo de la Amazonía Noroccidental**. Allí, según Yuli Artunduaga, miembro del Comité de mujeres Andino Amazónicas, existen ecosistemas valiosos para la Amazonía como la selva exuberante.

La razón por la cual los habitantes han manifestado su deseo de realizar la consulta popular radica en que, de acuerdo con Artunduaga, **“en la región del Putumayo ha habido descubrimientos de petróleo desde 1963** y los proyectos se han extendido por todos los municipios”. Para evitar que estos proyectos lleguen a Puerto Leguizamo, quieren realizar el mecanismo democrático para que sea la ciudadanía quien decida si permite o no la llegada de grandes empresas a explorar pozos petroleros. (Le puede interesar: ["Actividad petrolera ya había contaminado 49 aljibes y 58 pozos de agua en Acacias, Meta"](https://archivo.contagioradio.com/actividad_petrolera_habia_contaminado_pozos_aljibes_ecopetrol/))

Igualmente, Artunduaga manifestó que en el departamento del Putumayo ya se han asentado empresas como **Gran Tierra Energy Inc, el consorcio Colombia Energy, la empresa Amerisur Exploración y Ecopetrol**. Ante esto, los habitantes de Puerto Leguizamo manifestaron su preocupación por la posibilidad de que estas empresas lleguen “al bosque amazónico virgen donde aún no se ha desarrollado exploración petrolera”.

Finalmente, Artunduaga indicó que **tienen el apoyo de las autoridades municipales para que el proceso de consulta popular** se lleve a cabo. Según ella, los ha acompañado el alcalde y algunos concejales municipales en todo el proceso de defensa del territorio y “empoderamiento de las comunidades que quieren decirle no a la explotación petrolera”.

<iframe id="audio_20229446" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20229446_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
