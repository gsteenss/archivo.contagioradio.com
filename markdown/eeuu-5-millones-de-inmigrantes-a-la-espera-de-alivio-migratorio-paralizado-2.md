Title: 5 millones de inmigrantes a la espera de "alivio migratorio" paralizado
Date: 2015-02-18 21:00
Author: AdminContagio
Category: El mundo, Resistencias
Tags: Barack Obama inmigracion, EEUU, Juez de Texas paraliza reforma migratoria EEUU, Reforma migratoria EEUU
Slug: eeuu-5-millones-de-inmigrantes-a-la-espera-de-alivio-migratorio-paralizado-2
Status: published

###### Foto: Voanoticias.com 

##### <iframe src="http://www.ivoox.com/player_ek_4102289_2_1.html?data=lZadlJecfY6ZmKiak5aJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLmwtHW3MbHrYa3lIqvldOPtsba0Nfaw5DIqYzgwpDW0NLNq9PVxM6SpZiJhpTijKqyt7qRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Ángela Zambrano Red Mexicana de Inmigrantes] 

**Miles de inmigrantes estadounidenses indignados por la paralización de la reforma migratoria de Barack Obama por parte de un juez de Texas.**

En total son más de **5 millones de inmigrantes afectados por la decisión del juez Andrew S. Hanen y de 26 estados conservadores** que acusan al presidente de sobrepasar sus propios poderes en el ejercicio de sus funciones con respecto a la reforma migratoria. Lo que convierte a la acción de paralización de movimiento político antiinmigratorio y de carácter racista.

Según Ángela Zambrano, Red Mexicana de organizaciones migrantes en Los Ángeles, EEUU, **esta decisión es un pulso político de los sectores más conservadores** a la administración de Obama, y un intento de desmoralizar a los inmigrantes para que no se acojan a lo que desde su organización social entienden como "alivio migratorio".

En anteriores manifestaciones a favor de los derechos humanos de los inmigrantes han llegado a salir 1 millón y medio de personas demostrando que la **población documentada e indocumentada ha perdido el miedo a reclamar sus propios derechos** y oponerse frontalmente a **políticas xenófobas** provenientes de sectores conservadores.

Como describen las organizaciones sociales de derechos humanos, la reforma migratoria es una reivindicación histórica del colectivo migratorio, que con la administración de Obama había conseguido plasmar con mucha lucha y oposición del conservadurismo.

El presidente de la nación ha sido **contundente en sus declaraciones la enterarse de la decisión, quién ha dicho que esta competencia es suya y le sorprende la decisión del juez**. En otros países como Guatemala, sus respectivos presidentes también han calificado la decisión como negativa en referencia a sus ciudadanos afectados.
