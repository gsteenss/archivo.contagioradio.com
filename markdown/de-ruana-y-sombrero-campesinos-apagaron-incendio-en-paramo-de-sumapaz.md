Title: "De ruana y sombrero" campesinos apagaron incendio en páramo de Sumapaz
Date: 2020-02-07 08:53
Author: CtgAdm
Category: Ambiente, Nacional
Tags: incendio campesinos alerta, Páramo de Sumapaz
Slug: de-ruana-y-sombrero-campesinos-apagaron-incendio-en-paramo-de-sumapaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/d9fb7e4c-e7a2-4fb7-b78c-489720e45033.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/EQLLpCFWoAAvSSZ.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/EQLLpCFWoAAvSSZ-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/EQMCgvYXkAIdP7g.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @carolltejada  

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/alirio-ramirez-sobre-incendio-paramo-de_md_47539376_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Alirio Ramirez | Habitante de Sumapaz

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

En la mañana de este 6 de enero campesinos y campesinas habitantes de la localidad 20 de Bogotá conocida como San Juan de Sumapaz, ubicada al suroccidente del Parque Nacional páramo de Sumapaz reportaron fuertes focos de incendios que consumían este ecosistema.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La causa del incendio todavía se desconoce, sin embargo, el humo y las cenizas alcanzaron a las poblaciones de La Hoya, en Bogotá, y la Las Vegas, en Sumapaz. (Le puede interesar: <https://archivo.contagioradio.com/en-tiempos-de-sequia-se-reactiva-defensa-del-paramo-santurban/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según **Alirio Ramírez líder** campesino del Sumapaz y habitante de la vereda Lagunitas, las primeras llamas las identificaron al medio día del miércoles 5 de febrero, posteriormente el foco aumento y los vientos hicieron que se volviera casi incontrolable cerca de las 3:00 pm del siguiente día.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Nosotros hicimos un llamado a los bomberos y a las autoridades ambientales de la zona, sin embargo no recibimos respuesta de manera rápida, por eso decidimos ir y apagar nosotros mismos el incendio"*.
>
> <cite>Alirio Ramírez </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Con guadañas, palas y picas atenuaron las llamas del Sumapaz

<!-- /wp:heading -->

<!-- wp:image {"id":80431,"sizeSlug":"large","className":"is-style-default"} -->

<figure class="wp-block-image size-large is-style-default">
![Sumapaz](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/EQMCgvYXkAIdP7g.jpg){.wp-image-80431}  

<figcaption>
Foto: @carolltejada  

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Los incendios pudieron ser controlados hasta la madrugada de este viernes, gracias al trabajo de los habitantes de este sector que acudieron al llamado de alerta del páramo que cuenta con 333.420 hectáreas y una gran variedad de especies entre ellas el oso de anteojos y cóndor de los Andes, lo que hace el más grande del mundo. (Le puede interesar: <https://www.justiciaypazcolombia.com/grave-contaminacion-del-rio-lorenzo-por-petrolera-gran-tierra/> )

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Heidy_UP/status/1225810109353340929","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Heidy\_UP/status/1225810109353340929

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De igual forma Ramírez afirmó que las llamas se encuentran controladas, y que el centro médico de la vereda ya valoró a los habitantes que estuvieron trabajando toda la noche, *"nosotros estamos bien, solo un poco ahogados por el humo que absorbimos, nuestra preocupación ahora es que el sol y los vientos que están haciendo no vuelvan a avivar las llamas"* .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde inicio de este 2020 en varios zonas de país se han registrado 2.358 focos de calor, según el Instituto Amazónico de Investigaciones Científicas (Sinchi). Vale la pena recordar que no solo son los incendios también fuertes heladas las que han afectado los diferentes territorios rurales del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La intervención de agentes externos a la comunidad es otra de las razones que para Ramírez genera este tipo de catástrofes. En las últimas horas el Sindicatos de Trabajadores Agícolas de Sumapaz reportó *"vidrios, bacterias y bolsas de raciones militares fueron encontradas en la zona del incendio y contaminando nuestras fuentes hídricas, algo que se ha vuelto común en las zona donde acampa el Éjercito"*.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/charry_manager/status/1225782598045184000","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/charry\_manager/status/1225782598045184000

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
