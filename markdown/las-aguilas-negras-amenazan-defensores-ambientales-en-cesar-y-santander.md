Title: Águilas Negras amenazan defensores ambientales en Cesar y Santander
Date: 2017-02-28 15:16
Category: DDHH, Nacional
Tags: Canacol Energy y ConocoPhillips, CORDATEC, Fracking en Colombia, USO
Slug: las-aguilas-negras-amenazan-defensores-ambientales-en-cesar-y-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paramilitares....png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [28 Feb 2017 ] 

La Corporación Defensora del Agua, Territorio y Ecosistemas –CORDATEC– denunció que el pasado 27 de febrero circuló en calles de Cesar y Santander **un panfleto firmado por las Águilas Negras, en el que se amenaza la vida de integrantes de las organizaciones** sociales y sindicales que trabajan por la defensa del agua y el territorio, contra los proyectos petroleros y de Fracking.

A través de un comunicado, CORDATEC señaló que “una vez más buscan amedrentar los procesos de resistencia ciudadana a la locomotora minero-energética”, aseguran que sumado a ello, existen denuncias sobre una serie de violaciones a los derechos humanos contra los integrantes de CORDATEC, la USO y FUNTRAMIEXCO, tales como **“agresiones de la fuerza pública, amenazas anónimas, persecución y estigmatización”** por parte de autoridades locales “como el Alcalde de San Martín, Saúl Eduardo Celis” y aunque las autoridades legales saben de ello, no han resuelto la situación. ([Le puede interesar: Comenzó recolección de firmas para revocar al alcalde del fracking en san martín, Cesar](https://archivo.contagioradio.com/comenzo-recoleccion-de-firmas-para-revocar-al-alcalde-del-fracking-en-san-martin-cesar/))

Carlos Andrés Santiago, uno de los integrantes de CORDATEC, manifestó que al iniciar las movilizaciones, protestas y acciones legales por parte de la comunidad contra las empresas ConocoPhillips y Canacol Energy, **“empezaron todas estas amenazas, creemos que dichas empresas están vinculadas con estos hostigamientos”**. ([Le puede interesar: Gobierno colombiano prepararía dos nuevos bloques para fracking](https://archivo.contagioradio.com/gobierno_prepara_fracking_colombia/))

\[caption id="attachment\_36968" align="alignnone" width="581"\]![Panfleto\_Aguilas\_Negras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Panfleto_Aguilas_Negras.jpeg){.size-full .wp-image-36968 width="581" height="812"} Panfleto de las Águilas Negras que circulo por Cesar y Santander\[/caption\]

###  

En la misiva, hacen una exigencia al Gobierno Nacional, a la Fiscalía General de la Nación, a la Policía Nacional, a las autoridades departamentales y municipales, para que investiguen los hechos y **“dar con el origen y los responsables de estas amenazas, y a tomar las acciones que sean necesarias para garantizar la vida** y la integridad de los líderes que hoy se encuentran en riesgo”.

Por último, extienden una invitación a la Defensoría del Pueblo, a organizaciones nacionales e internacionales defensoras de derechos humanos, movimientos ambientalistas colombianos y a medios de comunicación, para que hagan seguimiento y veeduría de la delicada situación que se vive en el municipio de San Martín y otras regiones **“por cuenta de la llegada del Fracking a esta región del país, con los 43 bloques No Convencionales existentes en Colombia”.**

###### Reciba toda la información de Contagio Radio en [[su correo]
