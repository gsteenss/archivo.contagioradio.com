Title: Víctimas reciben con alegría el premio Nobel de Paz para Colombia
Date: 2016-10-07 10:56
Category: Nacional, Paz
Tags: Bojaya, Conpaz, Conversaciones de paz de la habana, FARC, Juan Manuel Santos, La Chinita, víctimas
Slug: victimas-reciben-con-alegria-el-premio-nobel-de-paz-para-colombia
Status: published

###### [Foto: contagioradio.com] 

###### [07 Oct 2016]

Luego de conocerse que Juan Manuel Santos fue galardonado con el premio Nobel de Paz, las víctimas afirman que **es un respiro y una gran alegría luego de la tristeza que les provocó que el NO ganara en el plebiscito** por la paz. Para Jany Silva, integrante de la Red de Comunidades Construyendo Paz en los Territorios, CONPAZ, este premio es también al esfuerzo que se ha hecho desde varios sectores de la sociedad para alcanzar el acuerdo con las FARC.

Silva, señala que el respaldo internacional que se está recibiendo con reconocimientos como este, tiene que ayudar a impulsar a que se firme definitivamente el acuerdo de paz con las FARC es muy importante y hay que tenerlo en cuenta. Además aseguró que **son muchas mas las personas que quieren la paz a las que quieren que siga la guerra.**

Dentro de su análisis, la líder que también es vocera de CONPAZ, agrega que seguramente **si se repitiera la votación del plebiscito seguramente ganaría el SI**, porque el pasado 2 de Octubre se presentaron serias dificultades para que se pudiera ejercer el derecho al voto “muchas personas no pudieron votar porque hubo problemas para el transporte de comunidades muy alejadas de los centros de votación.

Frente a la arremetida de los sectores promotores del NO de cambiar elementos fundamentales de los puntos sobre Reforma Agraria Integral y **[Jurisdicción Especial para la Paz](https://archivo.contagioradio.com/nuevo-fiscal-no-es-prenda-de-garantia-para-las-victimas-conpaz/)**, las comunidades CONPAZ ya se han  manifestado afirmando que ese acuerdo fue construido con ellos y ellas y están dispuestos a defender esos puntos específicamente.

Recientemente las víctimas de la Masacre de la Chinita y de Bojayá manifestaron que están dispuestos a defender los acuerdos puesto que ellos y ellas los construyeron también, por eso **piden [voz y voto](https://archivo.contagioradio.com/pedimos-participacion-directa-voz-y-voto-en-el-nuevo-pacto-social-conpaz/) en el diálogo nacional** que está siendo convocado por el gobierno. Frente a este tema hoy se realiza una reunión entre el presidente y voceros de movimientos sociales y políticos que pondrán sobre la mesa también sus visiones y sobre todo los puntos que son primordiales para la construcción de paz con justicia social.

<iframe id="audio_13221688" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13221688_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
