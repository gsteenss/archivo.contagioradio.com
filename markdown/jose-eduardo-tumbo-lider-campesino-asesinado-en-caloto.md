Title: José Eduardo Tumbo líder campesino asesinado en Caloto
Date: 2019-08-04 11:46
Author: CtgAdm
Category: DDHH, Líderes sociales
Tags: Asesinado, Caloto, Cauca, Derecho Humanos, Líder campesino
Slug: jose-eduardo-tumbo-lider-campesino-asesinado-en-caloto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/EBFtbWkWkAAaX5m.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

**Asesinan a líder campesino del municipio de Caloto, Cauca**, quien había participado en la Minga social por la vida en Cajibio. Los hechos  ocurrieron en horas de la tarde, del sábado 3 de agosto en el corregimiento de Jagual en el sector conocido como la Virgen.

**José Eduardo Tumbo, era campesino e integrante de la Junta de Acción Comunal,** asimismo, participaba de la asociación campesina del municipio. Hasta el momento se desconoce información del acontecimiento, y de quiénes fueron los responsables del crimen. (Le puede interesar: [Guardia Indígena es atacada en la vía Caloto-Toribio, Cauca](https://archivo.contagioradio.com/guardia-indigena-es-atacada-en-la-via-caloto-toribio-cauca/)).

Este crimen se suma a la  violencia y asesinato de líderes y lideresas sociales en Colombia. Según el Instituto para el Desarrollo y la paz (IndePaz): ** 566 líderes sociales y defensores de derechos humanos han sido asesinados** desde el 1 de enero de 2016 al 10 de enero de 2019. (Le puede interesar: [Asesinan al comunero indígena Carlos Biscue en Caloto, Cauca](https://archivo.contagioradio.com/asesinan-al-comunero-indigena-carlos-biscue-en-caloto-cauca/)).

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
