Title: Estudiantes y profesores de la Universidad Nacional se toman el edificio de rectoría
Date: 2015-04-29 16:00
Author: CtgAdm
Category: Educación, Nacional
Tags: estudiantes, feu, Ignacio Mantilla, mane, maria alejandra rojas, paro, trabajador, Universidad Nacional
Slug: estudiantes-y-profesores-de-la-universidad-nacional-se-toman-el-edificio-de-rectoria
Status: published

###### Foto: Salmón Urbano 

<iframe src="http://www.ivoox.com/player_ek_4424585_2_1.html?data=lZmflpqceY6ZmKiakp2Jd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidTo1snWw9PYqdSf2pDd1NTKqdTj08rgjcnJb83VjLrby9vJttTdxcbRjbPFp8rjz8bZjdjJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [María Alejandra Rojas, Representante estudiantil] 

Ya se cumplen 3 semanas desde que los y las trabajadoras de la Universidad Nacional de Colombia se declararan en paro, ante lo que consideran, son incumplimientos por parte de la administración en el incremento de salarios de manera equitativa al interior de la institución. ( ver [A los trabajadores de la Universidad Nacional no les han aumentado el sueldo](https://archivo.contagioradio.com/a-los-trabajadores-de-la-universidad-nacional-no-les-han-aumentado-el-salario/))

A este cese de actividades se sumaron estudiantes y profesores, quienes en Asamblea Triestamentaria dieron a conocer problemáticas que los afectan directamente, relacionadas con la financiación de la universidad y la falta de presupuesto en las diferentes facultades (ver [Estudiantes, trabajadores y profesores de la Universidad Nacional anuncian movilización](https://archivo.contagioradio.com/asamblea-de-estudiantes-profesores-y-trabajadores-de-la-universidad-nacional-anuncia-jornadas-de-movilizacion/) )

En las últimas semanas, los tres estamentos han generado espacios de discusión colectiva para proponer a la administración de la universidad una hoja de ruta para el debate y la solución de los problemas coyunturales y estructurales que aquejan a una de las universidades más importantes del país.

El miércoles 29 de abril, intentaron radicar un pliego con las propuestas hacia la rectoría de la universidad, pero no les fue permitido. Para la representante estudiantil, Maria Alejandra Rojas, esto demuestra la falta de voluntad de la administración, que en 3 semanas ha enviado correos masivos a estudiantes y profesores rechazando el paro, sin ponerse en contacto con trabajadores o estudiantes.

El pliego propuesto por los estamentos contempla 6 puntos que incluyen (1) la apertura de espacios de negociación entre el ministerio de educación, la administración y los trabajadores, sin represalias para los mismos; (2) que se expida un acto administrativo para la realización de un Congreso Universitario, democrático, autónomo, vinculante que genere propuestas en torno a la educación superior y en particular la Universidad Nacional; (3) la definición de una estrategia progresiva para la financiación de la universidad que no contemple adquirir prestamos por parte del Findeter; (4) la redefinición del sistema de admisiones e ingreso a la universidad que está afectando particularmente a las Admisiones Especiales de los estudiantes de las sedes de frontera; (5) la reposición del calendario académico; y (6) que la administración se comprometa a solucionar los problemas que se presentan en torno al sostenimiento de las residencias universitarias que se encuentran en riesgo de ser hipotecadas.

Profesores, trabajadores y estudiantes esperan que la administración asuma una postura de diálogo para beneficio de la universidad y el país.

\[caption id="attachment\_7848" align="alignleft" width="1057"\][![Toma UN](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/IMG-20150429-WA0010.jpg){.size-full .wp-image-7848 width="1057" height="568"}](https://archivo.contagioradio.com/estudiantes-y-profesores-de-la-universidad-nacional-se-toman-el-edificio-de-rectoria/img-20150429-wa0010/) Toma UN\[/caption\]
