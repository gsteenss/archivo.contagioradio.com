Title: George Floyd y los tres detonantes de las protestas en EE.UU.
Date: 2020-06-01 23:19
Author: CtgAdm
Category: Actualidad, El mundo
Tags: Donald Trump, Estdos Unidos, George Floyd, Racismo estructural
Slug: george-floyd-y-los-tres-detonantes-de-las-protestas-en-ee-uu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/49940390081_41a4aff42e_k.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Flickr/

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**El asesinato de George Floyd, puso** en evidencia la discriminación que aún vive Estados Unidos. Y este hecho ha dado pie a una serie de protestas en contra del racismo en un país donde, según un estudio publicado por la Academia Nacional de Ciencias en 2019, aproximadamente 1 de cada 1.000 hombres y niños negros puede esperar morir a manos de la policía, y la población afro representan el 13,4% de su demografía.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Gimena Sánchez de la[Oficina en Washington para Asuntos Latinoamericanos](https://twitter.com/WOLA_org)(Wola) la ola de protestas que desató el asesinato de George Floyd es la suma de varios factores, entre ellos el histórico racismo estructural que ha vivido el país después de la guerra civil; las más de 104.000 muertes en EE.UU. por covid-19, la crisis de los empleos que ha llevado a que más de 2,12 millones de personas a solicitar el subsidio por desempleo y por último, pero no menos indignante, la gestión del presidente Donald Trump.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Y es que el mandatario se ha referido a las protestas que han dejando a más de 4.400 personas bajo arresto desde que comenzaron, como "terrorismo nacional", afirmando que “si una ciudad o estado se niega a tomar las medidas necesaria, entonces desplegaré el ejército de los Estados Unidos”, instando a los gobernadores a actuar contra los manifestantes. Un anuncio en ese sentido se conoció al cierre de esta nota.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"La crisis destapo que el sistema realmente solo apoya a personas que tienen dinero, mostró la quiebra del sistema de salud y cómo ha afectado a personas en situación de pobreza como latinos y afrodescendientes",** de hecho George Floyd trabajaba como personal de seguridad en un restaurant, pese a ello debido a las medidas de confinamiento por la pandemia fue una de las muchas personas que perdió su empleo. [(Le puede interesar: 75 años después, Auschwitz y las nuevas caras del fascismo)](https://archivo.contagioradio.com/75-anos-despues-auschwitz-y-las-nuevas-caras-del-fascismo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque las protestas se han multiplicado en al menos 45 ciudades, la mayoría pacíficamente, también han incrementado los enfrentamientos entre la Fuerza Pública y algunos ciudadanos, mientras en ciudades como **Los Ángeles, Chicago, Miami, Detroit, Washington se** ha extendido el toque de queda, cerca de 5.000 soldados de la Guardia Nacional también se han desplegado en 15 estados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"En las noches hay saqueos organizados entre otros, por grupos de extrema derecha que creen que debe haber una limpieza a través de una guerra racial", relata la defensora de DD.HH. [(Lea también: Informe revela las inhumanas acciones de EE.UU. con migrantes centroamericanos)](https://archivo.contagioradio.com/las-condiciones-de-los-migrantes-se-vuelven-inhumanas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La violencia de las autoridades también es un tema de preocupación, tan solo en 2017, la Policía de Estados Unidos mató a 987 personas, mientras que en 2016, **de las 509 personas asesinadas por la fuerza policial, 123 eran afroamericanas**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El racismo estructural sigue vigente

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Pese al trabajo del movimiento de derechos civiles en los sesenta, pese a figuras como Martin Luther King y las reformas que se han hecho buscando una igualdad, medios locales coinciden en **que después de la victoria electoral de Trump en el 2016, ­grupos supremacistas, neonazis se sintieron legiti­mados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte Sánchez señala que con la llegada de Barack Obama a la presidencia de los EE.UU. existió una falsa noción de que se estaba viviendo en un momento post-racial, pero el asesinato de George Floyd, las políticas de la administración Trump de cara al Covid-19 y la crisis económica han evidenciado lo contrario.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha desigualdad es más clara cuando se revisan los registros de contagios de Covid- 19 en Estados Unidos que revelan cómo las comunidades de color han sido mayormente afectadas, pues según epidemiólogos, los condados con mayor proporción de población negra representaron más de la mitad de todos los casos y casi el 60% de las muertes durante el mes de abril, dejando al descubierto que la población afro tiene menos acceso a los servicios de salud, y sufren frecuentemente de otras enfermedades como **diabetes, hipertensión, obesidad y asma.** [(Le recomendamos leer: «No puedo respirar”)](https://archivo.contagioradio.com/gerorge-floyd-respirar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El "racismo estructural y sistémico del que han advertido, no solo permea el sistema de salud, sino que llega a otras dimensiones como la laboral, la económica y social, pues se trata de una población con menos garantías de salud con relación a otros sectores, reciben menos ingresos y siguen trabajando en las calles durante la pandemia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las protestas van a generar un cambio en los EE.UU.

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"Es muy positivo lo que ha pasado porque la mayoría de las personas están despertando" afirma Gimena Sánchez, pues cree que las protestas y el accionar de la ciudadanía abren la posibilidad de que existan nuevos liderazgos a nivel local para realizar reformas **"pues sienten que los dos partidos tradicionales, democrata y republicano no están con la gente, por lo que se requieren nuevas ideas"**, más allá de saber quién ganará las elecciones presidenciales de noviembre de este año.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sáchez compara la doctrina del enemigo interno que existe en Colombia y que se aplica contra defensores de DD.HH, organizaciones sociales y diferentes gremios con una situación similar en los Estados Unidos que vincula directamente a la Policía, de la que advierte ha sido militarizada con el pasar de los años al otorgarle armas y vehículos usados en Irak, por lo que, advierte se requieren cambios legislativos. [(Lea también: Líderes sociales, el tema ausente de encuentro Trump-Duque)](https://archivo.contagioradio.com/lideres-sociales-deberian-tema-principal-encuentro-trump-duque/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
