Title: Oficina de la ONU en Colombia condena asesinato de campesinos en Tumaco
Date: 2017-10-06 16:35
Category: DDHH, Nacional
Tags: nariño, ONU, Tumaco
Slug: oficina-de-la-onu-en-colombia-condena-asesinato-de-campesinos-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/22255120_237918530073007_1808175056115878088_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [6 Oct 2017] 

La Oficina en Colombia del Alto Comisionado de las Naciones Unidas para los Derechos Humanos, condena las heridas y los asesinatos de campesinos en Tumaco en el marco de las operaciones de erradicación forzada de la Fuerza Pública, y **urgió para que se investigue, juzgue y sancione a los responsables.**

La ONU señala también que las muertes se dan en un contexto **"De problemas estructurales de derechos humanos, sumados a la falta de implementación del Acuerdo de Paz**, y a la urgente y necesaria solidaridad y acción de todos los sectores de la sociedad civil", en concordancia con lo denunciado reiteradamente por los habitantes de la región en el marco de los incumplimientos Estado Colombiano.

"Presencia limitada del Estado; altos niveles de pobreza multidimensional, presencia de economías ilícitas, índices de violencia a niveles endémicos; falta de implementación integral del Acuerdo de Paz en lo relacionado con: el capítulo étnico, cultivos ilícitos, lucha contra organizaciones criminales y reforma rural integral; Amnazas a líderes y lideresas que apoyan la sustitución de cultivos; problemas en la reincorporación de los integrantes de las FARC-EP a la vida civil; aparición de disidencias de las FARC-EP e ingreso de otros grupos armados y/o de carteles de narcotráfico", son las problemáticas a las que se refiere la Organización en su comunicado.

En esa medida la Oficina insta al Estado colombiano a que se **acelere la implementación del acuerdo de paz** y así se ofrezcan condiciones y garantías paras el campesinado, tales como programas efectivos de reforma rural integral construidos con las comunidades; la sustitución voluntaria de cultivos ilícitos; acciones para la protección y garantía de los derechos humanos de la población que habita en los municipios con mayor riesgo; e inclusión política, económica y social de las comunidades rurales.

Finalmente, la ONU manifiesta sus condolencias a la población nariñense y a los familiares y amigos de las víctimas. Asimismo, indica que continuará trabajando en el acompañamiento, monitoreo y análisis de los hechos ocurridos en el Alto Mira y Frontera. [(Le puede interesar: 9 personas muertas en protestas por erradicación forzada en Tumaco)](https://archivo.contagioradio.com/9-personas-erradicacion-forzada-tumaco/)

Por otra parte, en el marco de las acciones en solidaridad con el campesinado diversas organizaciones sociales se manifestaron este viernes frente a la Casa de Nariño en Bogotá, para exigir verdad y justicia en lo sucedido el jueves en Tumaco, y en respaldo a las versiones que da la comunidad sobre el actuar de la fuerza pública.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
