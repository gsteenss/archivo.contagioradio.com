Title: ONIC denuncia dos casos de ejecuciones extrajudiciales en Tame, Arauca
Date: 2018-01-20 18:08
Category: DDHH, Nacional
Tags: Cauca, Ejecuciones Extrajudiciales, falsos positivos, ONIC
Slug: onic_ejecuciones_extrajudiciales_tame_arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/FALSOS-POSITIVOS-e1500995272586.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Pilon] 

###### [20 Ene 2018] 

Dos indígenas en Tame, Arauca habrían sido víctimas de ejecuciones extrajudiciales según denuncia la Organización Nacional Indígena, ONIC. Se trata de **dos hermanos dirigentes indígenas del Pueblo Betoye, a quienes el Ejército Nacional los habría asesinado y habría presentado como guerrilleros del ELN** muertos en combate.

De acuerdo con la ONIC, en la noche de este viernes, Luis Díaz López de 22 años, **secretario del Cabildo y Miller Díaz López quien era el fiscal de la comunidad indígena del Juliero del Pueblo Betoye** llegaban sobre las 11 de la noche después de un jornada de caza en monte. Estando en el resguardo a 400 metros del al caserío, "fueron interceptados por miembros del Ejército Nacional, quienes les disparados de manera indiscriminada, quitándoles la vida en el acto”, dice la denuncia de la ONIC.

Asimismo, algunas personas de la comunidad manifestaron **“que al otro lado de la carretera estaba un grupo armado, no se sabe si son de los disidentes u otro grupo armado** al margen de la ley; prácticamente quedaron en medio del fuego cruzado, cuyos cuerpos fueron encontraros con las flechas y las babillas que cazaron para su sustento”.

### **Bloqueos de la comunidad en rechazo a los asesinatos** 

De acuerdo a las denuncias de las Autoridades Indígenas de las región el Ejército Nacional habría trasladado los cuerpos para Arauca y los hizo pasar como guerrilleros del ELN, una situación que generó la indignación y rechazo de **la comunidad por lo que decidieron  bloquear la vía como protesta hasta que se aclaren los hechos** por parte del Ejército y el gobierno rectifique la acusación, de que se trataba de integrantes de esa guerrilla.

Este sábado desde las horas 00:00 horas, la vía se mantiene bloqueada, con paso cada dos horas como acción de protesta pacífica por parte de miembros de las comunidades Julieros, Roqueros, Blasqueros y Genareros.

Además las autoridades indígenas regionales señalan “que el Ejército Nacional con el afán de confrontar al ELN y/o otros grupos al marguen de la Ley que opera en el departamento de Arauca, en su afán por presentar resultados al gobierno central cometen hechos fuera de sus funciones, como es este caso, y **otro asesinato ayer (viernes 19) en la madrugada, se trata de una joven profesora de un CDI,** que como de costumbre en la región y en comunidades indígenas, se trasladan en las madrugadas entre familiares, esta vez a pelar uno pollo para la venta al detal”.

En este contexto, la ONIC demanda pronunciamiento y rectificación del Ministro de Defensa y el mismo Presidente Santos.  También solicitan el acompañamiento e investigación de los hechos por parte de la Defensoría del Pueblo y Procuraduría General de la Nación, organismos nacionales e internacionales de DDHH y DIH, para que se esclarezcan los hechos y se tomen las medidas pertinentes contra los miembros de la fuerza pública que habrían cometido dichos actos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
