Title: "Uribe ha tenido un aparato para contrarrestar las investigaciones judiciales" Cepeda
Date: 2018-02-19 15:50
Category: DDHH, Nacional
Tags: 12 apostoles, alvaro uribe velez, Iván Cepeda, paramilitares
Slug: uribe-ha-tenido-un-aparato-para-contrarrestar-las-investigaciones-judiciales-cepeda
Status: published

###### [Foto:] 

###### [19 Feb 2018]

Con la decisión de la Corte Suprema de Justicia de ordenar investigar a el senador Álvaro Uribe Vélez, por la posible falsificación de testigos,  **abriría un capítulo que va más allá del posible concierto para delinquir y que demostraría la práctica de los falsos testimonios**. Para Iván Cepeda, la verdadera investigación debe ser sobre los testimonios que comprometen a Álvaro Uribe con el paramilitarismo.

Así mismo, Cépeda aseguró que este paso en la Corte es un avance en términos de justicia “para quienes por años han buscado la verdad con relación a los múltiples hechos en los que está involucrado el senador Uribe”. (Le puede interesar: ["Uribe a investigación por presunta manipulación de testigos contra Iván Cepeda"](https://archivo.contagioradio.com/uribe-testigos-cepeda/))

Para Cepeda, las declaraciones originales que se realizaron en este proceso, tienen señalamientos muy graves en los que se indica que los hermanos “**Álvaro y Santiago Uribe, junto con otros personajes como los señores Santiago Gallón Henao y los Villegas Uribe crearon un grupo paramilitar** que desató, en el municipio de San Roque, Antioquía, una cadena de masacres y asesinatos” afirmó.

En ese mismo sentido, el senador manifestó que en el contenido de las pruebas recolectadas por la Corte Suprema, hay ofrecimientos de testigos a Uribe para que contradiga las pruebas recolectadas en otros procesos judiciales.

“Lo que queda claro es que Uribe ha tenido un aparato para contrarrestar las investigaciones judiciales, compuesto por personajes que no solo sobornan a los testigos sino que también los chantajean”.

### **El siguiente paso en la investigación contra Uribe** 

De acuerdo con Cepeda, una vez la Corte abre la investigación contra el senador y ex presidente Álvaro Uribe Veléz, **también se ordena que se reactiven las investigaciones que están engavetadas en el despacho del Magistrado Gustavo Malo**, que, según Iván Cepeda, “no ha hecho absolutamente nada”. (Le puede interesar: ["Compulsan copias contra Álvaro Uribe Vélez por dos masacres en Antioquia"](https://archivo.contagioradio.com/alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo/))

“Experimentar una satisfacción, porque junto a mi equipo de abogados, de manera paciente, rigurosa, serena y con mucha convicción hemos soportado toda clase de presiones y montajes” y agregó que este proceso que abre la Corte “honra no solamente, mi labor sino también la de muchos colombianos que han estado tras la verdad, buscando justicia con relación a los múltiples hechos en los que está involucrado el senador Uribe”.

Frente a la posibilidad de encontrar justicia, Cepeda manifestó que ya se ha producido una decisión supremamente contundete y en la que hay miles de pruebas y a lo que ahora se espera haga frente Uribe y su defensa. Además, expresó que uno de los impactos de esta decisión es que tiene efectos sobre otros procesos contra el expresidente sobre la creación de grupos paramilitares, las masacres del Aro y la Granja, entre otras.

<iframe id="audio_23879116" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23879116_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
