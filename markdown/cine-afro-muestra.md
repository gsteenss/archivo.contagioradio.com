Title: El cine Afro se toma la Cinemateca Distrital
Date: 2018-06-01 13:34
Author: AdminContagio
Category: 24 Cuadros
Tags: afro, Bogotá, Cine
Slug: cine-afro-muestra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Afiche-Muestra-Afro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cinemateca Distrital 

###### 30 May 2018 

Del 30 de mayo al 8 de junio vuelve a la Cinemateca Distrital de Bogotá la 3ra edición de la Muestra Afro, proyecciones audiovisuales y eventos académicos en los que se exploran las políticas estéticas, culturales y la memoria social de esta comunidad, proyectada desde los ámbitos artísticos de África y la diáspora africana en Colombia, América y Europa.

Bajo el lema Somos Audiovisual, la muestra compuesta por 65 obras de diversos géneros como documental, animación, videoarte, movimientos comunitarios y televisión regional, busca dar cuenta de la diversidad cultural y variedad de oferta para todos los públicos. Adicionalmente en 10 de las funciones se contará con la presencia de sus directores incluyendo a Jhonny Hendrix Hinestroza  y su película Saudó, laberinto de almas el martes 5 de junio a las 7 de la noche.

La programación incluye producciones colombianas que no se han proyectado en la Cinemateca. La curaduría nacional, que estuvo a cargo del colectivo Wi Da Monikongo - Consejo Audiovisual Afrodescendiente de Colombia, se dividió por regiones: Chocó, San Andrés, Cali, Cartagena, Palenque, Villa Paz, entre otras.

“La selección busca que la muestra sea un espacio de circulación del audiovisual [afro]{.il} en Colombia con la participación de todas las personas que trabajan lo [afro]{.il}, aunque no sean afrodescendientes, queremos que nuestras estéticas y las maneras de narrar sean visibles y accesibles a la población colombiana” afirma Liliana Ángulo representante del colectivo.

La invitada internacional de la muestra de cine Afro será Viviane Odun, representante de la Asociación de Profesionales del Audiovisual Negro de Brasil - APAN y cuatro invitados nacionales: Luis Hender Martínez - Colectivo Kucha Soto (Bolívar - Palenque); Dianne Rodríguez, realizadora (Quibdo-Chocó); Ramón Perea - Festival Internacional de Cine [Afro]{.il} Kunta Kinte - Corporación Carabantú y Víctor González Urrutia, realizador (Valle del Cauca - Jamundí - Villapaz).

###### <iframe src="https://www.youtube.com/embed/JmjpetNKm-Q" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
