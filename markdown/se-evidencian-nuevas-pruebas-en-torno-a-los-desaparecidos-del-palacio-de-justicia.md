Title: Se destapan nuevas evidencias en torno a desaparecidos del Palacio de Justicia
Date: 2015-10-23 15:36
Category: DDHH, Nacional
Tags: Ana María Bidegain, Carlos Horacio Urán, colectivo de Abogados José Alvear Restrepo, Corte Interamericana de Derechos Humanos, Crimen magistrado Carlos Horacio Urán, Desaparecidos palacio de justicia, Irma Franco, Palacio de Justicia, Palacio de Justicia noviembre de 1985, Retoma del Palacio de Justicia
Slug: se-evidencian-nuevas-pruebas-en-torno-a-los-desaparecidos-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Palacio-de-Justicia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas ] 

###### [23 Oct 2015 ] 

[El expediente por el crimen del magistrado Carlos Horacio Urán durante la retoma del Palacio de Justicia entre el 6 y 7 de noviembre de 1985, contiene **reportes policiales** en los que se confirma que **civiles reseñados como guerrilleros del M-19** habrían salido con vida del Palacio, conducidos a la Casa del Florero y luego llevados al Batallón para ser **torturados**.]

[El primer informe presenta un listado elaborado por un grupo de inteligencia de la Sijín en el que se relacionan con nombres, apellidos y direcciones **140 personas que fueron sacadas con vida del Palacio** de Justicia el 6 de noviembre. Un primer apartado referencia los nombres de magistrados, abogados, funcionarios y escoltas del Palacio. En el segundo y con el título “Relación de los guerrilleros que participaron en la toma del Palacio de Justicia” se nombran **35 personas supuestamente pertenecientes al M-19**.  ]

[Sin embargo, se ha logrado establecer que este es un **listado erróneo en el que se mezclan civiles e ilegales**. Aparecen en él los nombres de Luis Francisco Otero, conocido como el Comandante Lucho y de Andrés Almarales o Comandante Pacho (muertos ese día), así como de los estudiantes  de derecho de la Universidad Externado de Colombia **Yolanda Santodomingo** y **Eduardo Matson**, junto con el del funcionario de la Corte Suprema **Orlando Quijano**.]

[Estos últimos, de acuerdo con la sentencia de la CIDH de 2014 en la que condena al Estado colombiano por los hechos ocurridos en el Palacio de Justicia en 1985, fueron **detenidos arbitrariamente y luego torturados por agentes del Estado**. Los estudiantes tras ser llevados del Palacio a la Casa del Florero fueron sometidos a interrogatorios mientras eran golpeados, luego fueron conducidos a la Dijín para probar si habían disparado un arma, finalmente los llevaron al Batallón Charry Solano para 24 horas después ser abandonados en San Victorino.]

[El señor Orlando Quijano fue al igual que estos estudiantes señalado como guerrillero, fue torturado y luego dejado en libertad con la **advertencia de que debía permanecer callado** al igual que Yolanda y Eduardo pues “el procedimiento habría sido un error”.]

[El segundo documento fue elaborado por el general José Luis Vargas Villegas, para la época comandante de la Policía de Bogotá y en él se detalla tanto la retoma del Palacio como la información que tenían las autoridades de la toma que el M-19 había planeado y que les habría permitido **1 día antes de la toma reforzar la seguridad del Palacio**, pese a haber quitado la vigilancia de la puerta para permitir la entrada del grupo guerrillero. Lo que confirma que esta era **una toma anunciada**.  ]
