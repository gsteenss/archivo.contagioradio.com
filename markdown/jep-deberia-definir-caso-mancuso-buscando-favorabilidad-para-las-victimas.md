Title: JEP debería definir caso Mancuso buscando favorabilidad para las víctimas
Date: 2020-01-21 14:59
Author: CtgAdm
Category: Judicial, Paz
Tags: JEP, Mancuso, víctimas
Slug: jep-deberia-definir-caso-mancuso-buscando-favorabilidad-para-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Acogimiento-de-Mancuso-a-la-JEP.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo {#foto-archivo .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego que el líder de las Autodefensas Unidad de Colombia (AUC), Salvatore Mancuso afirmara que quiere volver a Colombia y tiene muchas verdades para contarle al país, **resaltando hechos relacionados con el actual Senador Álvaro Uribe,** se han desatado fuertes expectativas sobre el posible ingreso de Mancuso, y otros jefes paramilitares a la Jurisdicción Especial para la Paz (JEP).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El experto jurista internacional Gustavo Gallón, director de la Comisión Colombiana de Juristas aseguró que, en caso de presentarse un conflicto de competencias entre la JEP y la Ley 975 de 2005, el **principio de favorabilidad para las víctimas es el que se debería aplicar**, justamente porque está presente en la misma norma y en uno de los principios fundantes de la Juridicción de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gallón afirmó que una de las primeras cosas que se debe hacer es esperar esas verdades sobre las que el país tiene grandes expectativas. Una vez se sepan los temas que serían revelados por el propio Mancuso y otros ex paramilitares, que están a punto de pagar su condena en Estados Unidos y que volverían a Colombia acogiéndose a la JEP, **se deberán resolver los conflictos de competencias que este acogimiento podría presentar.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Mancuso no es solamente  
un tercero compareciente**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Gallón, una de las primeras preguntas de la JEP podría ser el porqué no contaron ante los tribunales de la Ley 975 lo que van a contar ahora. Sin embargo podría tratarse de **temas que hacen parte de las más de 13.000 compulsas de copias sobre terceros en el conflicto que recibió la Fiscalía** y frente a las cuales no se han dado desarrollos sustanciales, en cuyo caso, el argumento podría ser que el nuevo tribunal puede asumir casos que han sido denunciados pero que no han sido respondidos satisfactoriamente a los derechos de las victimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En torno a la figura de tercero compareciente y no combatiente con la que se acogería Mancuso a la JEP, el jurista señaló que es absurdo que una persona que participó directamente en el conflicto como financiador y como ejecutor, sea acogido solo como tercero y no como determinador. En este caso, si es acogido como determinador tendrían más favorabilidad los derechos a la verdad, la reparación y la no repetición, así como las propuestas de [justicia restaurativa](https://www.justiciaypazcolombia.com/una-universidad-para-la-paz-en-la-selva-chocoana/) que están elaborando las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Le puede interesar: ["La verdad de Mancuso que podría llegar a la JEP"](https://archivo.contagioradio.com/la-verdad-de-mancuso-que-podria-llegar-a-la-jep/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46800950" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46800950_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
