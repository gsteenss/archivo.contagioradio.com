Title: Jornada contra la brutalidad policial en Colombia
Date: 2016-02-24 12:02
Category: DDHH, Nacional
Tags: brutalidad policial, Diego Becerra, ESMAD, nicolas neira
Slug: dia-contra-la-brutalidad-policial-en-colombia
Status: published

###### Foto: Contagio Radio 

<iframe src="http://co.ivoox.com/es/player_ek_10557649_2_1.html?data=kpWil5yaeJqhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLnhqigh6aob9TZ04qwlYqldYzgwpDb18rapYze0Nfbw8nFb8Tjz9nfw5DQpYzW09rhw9HNqMLYjNXczs7HrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gaby Díaz] 

###### [24 Feb 2016] 

Este 24 de febrero, diferentes organizaciones sociales y antimilitaristas convocan a una nueva jornada contra la brutalidad policial en Colombia. Un día en el que a partir de diversas actividades culturales se rechaza el uso de la fuerza por parte de los agentes del Estado como una práctica cotidiana y permanente que contradice el derecho fundamental a estar en desacuerdo y que está consagrado en la Constitución Política.

Los tres ejes de esta sexta jornada, se centrarán en visibilizar el “inmenso presupuesto” que tiene la fuerza pública en comparación al monto destinado a la inversión social, también se recordará a las personas asesinadas por parte de agentes estatales, y como es habitual,  se seguirá reclamando el desmonte del [ESMAD](https://archivo.contagioradio.com/?s=ESMAD), según explica Gaby Díaz, integrante del Hijos e Hijas por la Memoria y Contra la Impunidad.

De acuerdo con cifras de la Policía Nacional, **en 2015 se destinaron cerca de 9 billones de pesos a ese sector.  **Respecto al ESMAD, según Díaz, **un agente sin armas y solo con su traje, le significa a la nación un costo de 3 millones 600 mil pesos**. Costos que son destinados a reprimir las protestas ciudadanas, que a su vez son pagados por los colombianos.

Según el Movimiento Nacional de Víctimas de Crímenes de Estado, MOVICE, la mayoría de crímenes siguen en la impunidad. **Las amenazas, los falsos testigos, la negligencia en los procesos y la modificación del lugar de los hechos, son las estrategias que se usan para mantener en la impunidad** cientos de casos en los que agentes del Estado han usado la fuerza contra algún manifestante causándole, incluso, la muerte, como sucedió con Nicolás Neira que a sus 16 años de edad murió por acción del ESMAD, o Diego Becerra el joven grafitero asesinado por un patrullero.

"Hemos encontrado que todo está diseñado para que estos casos de brutalidad policial pasen impunes”, indica la integrante de Hijos e Hijas por la Memoria y Contra la Impunidad.

**Desde las 3 de la tarde la carrera séptima con calle 18, sector conocido como la “Calle Nico”, será el escenario cultural en el que por medio del arte se hará un llamado al gobierno nacional para que se desmonte el ESMAD,** con el fin de que se garantice la protesta social, más aún, teniendo en cuenta el proceso de paz que se encuentra en su fase final en la Habana, en donde se ha exigido garantías al movimientos social en Colombia.

### Programación del día 

3 pm - Mural (Beligerarte)  
4 pm - Mesa de Radio Relajo y la Redada con micrófono abierto y Estampado "A las calles sin miedo".  
5 pm - Lanzamiento del periódico El Clavel negro  
6 pm - Batucada Feminista Tremenda Revoltosa  
7 pm - Proyección en vivo del Triciclo audiovisual  
7:30 pm - Toque Black Riddim Band

[![cuanto cuesta mantener un agente del esmad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/cuanto-cuesta-mantener-un-agente-del-esmad.jpg){.aligncenter .wp-image-20644 width="651" height="530"}](https://archivo.contagioradio.com/dia-contra-la-brutalidad-policial-en-colombia/cuanto-cuesta-mantener-un-agente-del-esmad/)
