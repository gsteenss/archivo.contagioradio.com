Title: "Jericó no necesita de minería para su sostenibilidad"
Date: 2020-02-21 16:39
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Antioquia, economia, Mineria
Slug: jerico-no-necesita-de-mineria-para-su-sostenibilidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/jericó-minería-e1550514587907.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Jericó Alerta*

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/fernando-jaramillo-sobre-foro-039-la-mineria-como-palanca_md_48222392_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Fernando Jaramillo | coordinador general de la mesa ambiental de Jericó

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

En el marco del foro **'La minería como palanca del desarrollo sostenible'**, la compañía **AngloGold Ashanti** destacó que su proyecto generaría **3.000 empleo**; ante ello la comunidad jericoano rechazó el pronunciamiento y afirmó que *"que no necesitan de minería para la subsistencia económica del municipio"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Departamento Administrativo de Planeación de [Antioquia](http://www.antioquiadatos.gov.co/) en su último informe sobre la producción de los municipios del departamento, Jericó ejecuta al año \$13.805.335, de un aporte sustentado en más de un 50% por la agricultura; lo que lo hace el segundo municipio mas productivo de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fernando Jaramillo **coordinador general de la mesa ambiental de Jericó**, declaró que la afectaciones producto de esta intervención serían catastróficas, "*los jericuanos tienen una situación de vida muy buen, la tasa de desempleo es baja, entonces no se cosible pensar más allá de unos interese económicos la propuesta de la empresa.* ".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y añadió que Jericó es un pueblo patrimonio general de la nación, y un atractivo turístico nacional e internacional, *"el turismo, junto con nuestra herencia cafetera y campesina ha permito por años podamos vivir de manera estable y tranquila, sin conflictos entre nosotros"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"La implementación le dará la tranquilidad a las comunidades de que **el proyecto cumplirá su propósito de convertir la riqueza mineral del territorio en progreso social, económico y ambiental**”.*
>
> <cite> Felipe Márquez Robledo, presidente de AngloGold Ashanti Colombia </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Dentro de las acciones presentadas por la multinacional están planes de reclutamiento, capacitación, entrenamiento y desarrollo profesional *"que permitan potencializar y generar valor en Jericó y sus alrededores"*. (Le puede interesar: <https://archivo.contagioradio.com/por-medio-de-mentiras-minesa-pretende-desacreditar-el-movimiento-social/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto Jaramillo, afirmó que Anglo Gold crea expectativas a los campesinos de una mejor vida, *"una iniciativa de mentiras, que revela una verdad cruda que es la que ya están viviendo los pueblo mineros de nuestro país*, ***ya sea minera legal o ilegal, estos pueblos viven una cantidad de miseria y violencia**"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la empresa son **50 mil millones de pesos los que serán asignados al plan de manejo ambiental**, donde se contempla también la recuperación de los ecosistemas de bosque seco tropical y bosque de alta montaña. (Le puede interesar: <https://archivo.contagioradio.com/nuevo-mineria-en-jerico/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"La empresa llega ofrece salarios mínimos o un poco más arriba de estos, paga con prestaciones, entrega uniforme y prestamos, lo que da ciertos símbolos de poder", para el Coordinador eso rompe toda la tradición campesina y cafetera, creando una expectativas de que es *"mejor emplearse y tener un dinero seguro que trabajar las tierras".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jaramillo finalizó enfatizando "no queremos que nuestro pueblo entre en un proceso de decadencia producto de las afectaciones en la agricultura, mano de obra, fuentes hidrias y turismo productos de la explotación".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
