Title: En Bello cogobierna el crimen, Nuevo Jerusalén es un ejemplo
Date: 2015-01-29 12:14
Author: CtgAdm
Category: Fernando Q, Opinion
Tags: autodefensas, Bello, despojo urbano, finca el cortado, Medellin, Paramilitarismo
Slug: en-bello-cogobierna-el-crimen-nuevo-jerusalen-es-un-ejemplo
Status: published

###### Foto: Sebastian Mena 

Por [**[Fernando Quijano]**](https://archivo.contagioradio.com/fernando-quijano/)

A nadie de la institucionalidad local, departamental e incluso del orden nacional, pareciera importarle qué es el asentamiento Nuevo Jerusalén y mucho menos, las desgracias y atrocidades que allí ocurren. Al parecer, un nuevo pacto entre sectores de la legalidad institucional y la  ilegalidad paramilitar se concretó en dicho territorio, ambiento por  la miseria y el abandono estatal al que está sometido el mismo.   [[[Las pruebas documentales y testimoniales así lo demuestran]

El Estado se enajena de esa realidad debido a un sinnúmero de intereses económicos, políticos, militares, e irónicamente enarbola eslóganes con orgullo, que en últimas son frases vacías y contrarias a la realidad.

Decir que Medellín es “un hogar para la vida” y “la ciudad más innovadora”, que Bello es “el quinto municipio más seguro de Colombia”, que Antioquia es “la más educada”, y mostrar supuestos logros obtenidos en el manejo de la seguridad, en la reducción de la pobreza, en la implementación de un modelo educativo novedoso, no es otra cosa que engañar a la ciudadanía mediante el uso de paliativos.

Señor presidente de la República, alcaldes de Medellín y Bello, gobernador de Antioquia, ¿cuál seguridad, cuál educación y prosperidad democrática podrá haber si existe una tragedia llamada Nuevo Jerusalén?

Si por razones de apretada y agitada agenda, o por falta de información de su aparato de inteligencia, no saben qué es Nuevo Jerusalén, les contaré resumidamente que es un asentamiento ilegal ubicado en el municipio de Bello, donde el crimen es ley y orden, cuya situación es tan crítica que ya existe una acción popular fallada en el 2013, la cual ordena a las autoridades civiles, judiciales y militares del Estado a corregir esta problemática; corrección que no se ha hecho y que a todas luces constituiría un desacato ante un juez que no fuera corrupto.

Este asentamiento ilegal está ubicado sobre un terreno de 60 hectáreas llamado la Finca el Cortado, propiedad (léase bien) del municipio de Medellín aunque en jurisdicción del municipio de Bello, municipio que ya cuenta con antecedentes de despojo urbano y de cogobierno criminal.

En El Cortado hay más de 5000 viviendas habitadas por miles de familias que conviven bajo el control criminal que ejerce una banda paramilitarizada, la cual hizo escuela en las filas paramilitares de los bloques Cacique Nutibara y Héroes de Granada, pertenecientes a las Autodefensas Unidas de Córdoba y Urabá (ACCU).

Los mandos de Nuevo Jerusalén aprendieron las tácticas necesarias para controlar una comunidad: reclutamiento forzado de menores, cobro de diversas extorsiones conocidas como ‘vacunas’, desaparición forzada, homicidios, desmembramiento de seres humanos en casas de tortura, explotación sexual, desplazamiento forzado y despojo urbano, entre otros.

Como si esto fuera poco, en este lugar de rimbombante y esperanzador nombre, cuando la fuerza pública duerme (que es casi siempre) los criminales se entrenan militarmente para seguir fortaleciendo el control territorial y social de sus habitantes.

La connivencia de un sector estatal que permite este negocio se podría fundamentar en el negocio económico que viene siendo El Cortado: el pago obligado de los servicios públicos a los grupos armados ilegales deja una utilidad que se aproxima a los 1.560 millones de pesos anuales, sin contar la venta de lotes que está entre 3 a 7 millones cada uno (se vendieron más de 5000 y la venta continúa), y suma lo obtenido por tráfico de drogas, explotación sexual, prostitución, pagadiario, sicariato, entre otros.

Después de este relato, que desnuda en parte la cotidianidad de Nuevo Jerusalén, se esperaría que de ustedes surgiera voluntad política que diera el visto bueno para la realización de una acción conjunta, de gran envergadura, y que fuera contundente. Pero se necesita inversión social y desmantelar la estructura criminal; y no metralla, no más operaciones Orión, Mariscal o Estrella Seis. Aunque olvidaba que ese tipo de operaciones se realizan solo contra la insurgencia y allá controlan son las bandas criminales que visten, calzan y actúan como paramilitares.
