Title: 71% de los congresistas está en desacuerdo con adocpción igualitaria: MOE
Date: 2015-03-12 17:21
Author: CtgAdm
Category: Nacional, Política
Tags: Adopción igualitaria, Congreso, Consejo Nacional Electoral, eutanasia, genero, matrimonio igualitario, MOE
Slug: encuesta-de-la-moe-revela-posicion-conservadora-del-congreso-colombiano
Status: published

##### Foto: [www.pulzo.com]

<iframe src="http://www.ivoox.com/player_ek_4205492_2_1.html?data=lZedl5mddo6ZmKiak5eJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2FX1srg1saPqMafzcaYr7Spb8XZztrS1dnWpYzgytPSw5DHs8%2Fnxtfjw8nTtsKfxcrZjcjTso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Fabián Hernández , Coordinador de Comunicaciones MOE ] 

**“Es evidente que estamos ante un congreso con convicciones de tipo tradicional"**, es la conclusión de Fabián Hernández, coordinador de comunicaciones de La a Misión de Observación Electoral (MOE), quienes realizaron una encuesta sobre temas sociales, políticos, electorales, económicos y de la agenda de conversaciones de paz, al 80% del total de los congresistas, es decir  211 Senadores y Representantes a la Cámara.

La encuesta se realizó de la mano de la USAID y la Embajada de Suecia, y fue titulada, **“¿Cómo piensa el ‘Congreso de la paz’ 2014 – 2018?”.** Esta, confirma una posición conservadora en la mayoría de las opiniones de los congresistas, lo que responde a la falta de apoyo en temas que tiene que ver, por ejemplo, con los derechos de la comunidad LGBTI, ya que, acorde a los resultados del estudio, **el 71% de los congresistas está en desacuerdo con que parejas homosexuales tengan la posibilidad de adoptar, y además un 53% no aprueba el matrimonio igualitario.**

Los resultados presentados por la MOE, demuestran que no hay un consenso claro respecto a la  legalización de la eutanasia donde el 43% dice estar de acuerdo, el 44% está en desacuerdo, y el 13% de los Congresistas no respondieron.

**Sobre el proceso de paz**

Frente las negociaciones entre el gobierno del presidente Santos y las FARC-EP, el 65% de los Congresistas encuestados respalda el proceso de paz, pero solo el 29% creen que conducirá a la desmovilización, desarme y reintegración de las FARC.

Además, sobre si se debe aplicar el mismo marco jurídico de justicia transicional para guerrilleros y paramilitares el 58% dice estar de acuerdo, el 30% no y 12 % no sabe.

Según una encuesta de la Misión de Observación Electoral el 42% de los congresistas está en desacuerdo con la participación política de las FARC luego de que se firmen los acuerdos.

**Sobre el tema de género**

El 38% del Congreso está de acuerdo con las listas cremallera en la conformación de las listas entre hombres y mujeres de manera paritaria, mientras que el 48% en desacuerdo.

De acuerdo a la encuesta, los congresistas consideran que los componentes que limitan la participación política de las mujeres en el país: en primer lugar un 24% dice que el machismo; en segundo lugar, y con el 19% no saben o no respondieron a la pregunta. Les siguen: Responsabilidades familiares: 17%, miedo e inseguridad 13% la forma de hacer política 11%, procedimientos no transparentes al interior de los partidos políticos 6%. En los últimos lugares, oposición de la familia 4%, menor acceso a recursos 4% y falta de preparación 2%.

Además, se realizó una pregunta hecha solo a mujeres, sobre quienes creen que en mayor medida las discriminan, allí se identificó que los congresistas son quienes más lo hacen, siguen los ciudadanos, luego los medios de comunicación y por último los partidos políticos.

**Sobre el tema electoral**

Sobre la Agenda Electoral el 70% del Congreso se muestra de acuerdo con hacer una reforma estructural del Consejo Nacional Electoral y el 66% considera reformar el código electoral vigente.

Por otra parte, el 33% del Congreso considera importante el voto obligatorio de manera temporal  y solo el 27% estaría de acuerdo con el voto obligatorio de forma permanente.
