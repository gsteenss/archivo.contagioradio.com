Title: Crisis de salud en las cárceles colombianas
Date: 2018-04-03 11:00
Category: Expreso Libertad
Tags: carceles, prisioneros, Salud
Slug: salud-carceles-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/carceles-salud.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las dos orillas] 

###### [27 Mar 2018] 

Desde 1998 , la Corte Constitucional de Colombia declaró el Estado de Inconstucionalidad frente a la crisis de la salud que afrontan los prisioneros en el país, producto del hacinamiento, la violencia al interior de las penitenciarías y la violación a los derechos humanos, que ha provocado la muerte de un sin número de prisioneros. En este Expreso Libertad escuche los relatos y vivencias de quienes estuvieron en las cárceles del país denunciado la crisis en las cárceles.

<iframe id="audio_25008592" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25008592_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
