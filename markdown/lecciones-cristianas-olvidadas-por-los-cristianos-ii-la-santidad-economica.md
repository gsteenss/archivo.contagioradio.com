Title: Lecciones cristianas olvidadas por los cristianos II: la santidad económica
Date: 2020-07-12 14:35
Author: CtgAdm
Category: A quien corresponda, Opinion
Tags: Crisis de la justicia, crisis económica, cristianas, cristianismo., Cristianos
Slug: lecciones-cristianas-olvidadas-por-los-cristianos-ii-la-santidad-economica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

*El amor no se impone por la fuerza, la autoridad o el poder.  
El mensaje de Jesucristo es armonía, verdad, justicia, y amor.*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Estimado  
**Hermano en la fe  
Cristianos, cristianas, personas interesadas**

<!-- /wp:heading -->

<!-- wp:paragraph -->

  
Cordial saludo,  
  
La satisfacción de las necesidades básicas es condición necesaria para una vida humana digna.  Y esta satisfacción depende de unos mínimos económicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La vida digna de personas y pueblos y el respeto a la creación depende del tipo de economía asumida por una sociedad. Por esto hay economías que generan vida y hay economías que matan.  
   
La corriente cristiana de la que haces parte y los sectores  mayoritarios de las iglesias, poco disciernen el tipo de economía que respaldan o justifican, poco analizan si es una economía para la vida o para la muerte, una economía para la protección del planeta o para el beneficio del poder y  
las élites económicas; una economía para la acumulación de capital de unos pocos o para garantizar la alimentación, la salud, la educación, la vivienda, el descanso, la relación armoniosa con la naturaleza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El discernimiento del tipo de economía que responda el cristianismo es una  
exigencia teológica, porque si Dios es Padre-Madre de los seres humanos, si es “Creador”, y por lo tanto, “dueño” del mundo, todos sus hijos e hijas tienen el derecho a disfrutar de él y la obligación de cuidarlo. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

#####   
Por esta razón, reflexiono sobre el tema olvidado de la santidad económica, relacionada con la justicia económica y la paz, que es fruto de la justicia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Lo hago, en primer lugar,  haciendo una mirada a la realidad económica y, en segundo lugar, una reflexión bíblico-teológica.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Unos rasgos de nuestra realidad

<!-- /wp:heading -->

<!-- wp:paragraph -->

Colombia es uno de los países más inequitativos e injustos del mundo, lo que es contradictorio con el cristianismo, de esto te hablé en cartas pasadas: [Una economía que mata y dice que es cristiana](https://archivo.contagioradio.com/una-economia-que-mata-y-se-dice-cristiana/)y por eso culpable de sus consecuencias de sus decisiones, [un modelo socio-económico caus](https://archivo.contagioradio.com/el-modelo-socio-economico-causante-de-la-crisis-se-dice-creyente-sera-verdad/)[a](https://archivo.contagioradio.com/el-modelo-socio-economico-causante-de-la-crisis-se-dice-creyente-sera-verdad/)[nte de la graves crisis y que dice que es creyente .](https://archivo.contagioradio.com/el-modelo-socio-economico-causante-de-la-crisis-se-dice-creyente-sera-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Veamos unos hechos que muestran las profundas injusticias económicas: 

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Primero: **Una economía que “golpea al débil y favorece al fuerte”**. El ministro de hacienda Alberto Carrasquilla afirmó que el salario mínimo en Colombia es “ridículamente alto”; recordemos que él promovió y concretó zonas francas especiales y millonarias exenciones tributarias para beneficiar a empresarios poderosos; dos decisiones lo retratan:

<!-- /wp:list -->

<!-- wp:paragraph -->

**1**. Impulsó e hizo aprobar la eliminación de la mesada 14 para los pensionados; aumentó  la edad  y las semanas de cotización para la jubilación y cerró, a los trabajadores del sector privado, la posibilidad de colocar las pensiones en las negociaciones colectivas, transgrediendo las normas de  la[ OIT](https://www.ilo.org/global/lang--es/index.htm), con el acto legislativo número 01 del 22 de julio del 2005, porque según afectaba del fisco nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**2.** Pocas semanas después, jueves 8 de septiembre 2005, impulsó y firmó el decreto 3150, con el que la Casa de Nariño le concedió a ministros (él incluido), consejeros del presidente, asesores presidenciales, secretarios del presidente y a otros altos funcionarios del Estado una **“bonificación de dirección”** de cuatro sueldos al año (16 mesadas al año). Dos (2) mesadas al año, la mayoría alrededor de un salario mínimo, afectan el fisco nacional, pero cuatro (4) mesadas al año para funcionarios, con salarios de 18, 25 o 30 millones, no lo afectan. Recordemos que Carrasquilla, en el gobierno de Uribe, pasó el IVA del 8 al 16%, mientras se hicieron multimillonarias rebajas a los  grandes capitales.

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Segundo. **Con los impuestos benefician a los grandes capitales y golpean a los pequeños.** A pesar de que Colombia es uno de los países de mayor concentración del ingreso en América Latina y a  nivel  internacional, el régimen tributario  mantiene  esta concentración porque  favorece a  los  ricos.  

<!-- /wp:list -->

<!-- wp:paragraph -->

En el año 2017, las grandes empresas dejaron de pagar \$27,2 billones al Estado, gracias a los 229 “beneficios” tributarios vigentes para ellas, con el compromiso de crear nuevos empleos, lo que no ha ocurrido. La CEPAL los considera excesivos y crean inequidad entre empresas grandes y pequeñas”, entre más grande la empresa más beneficios tiene; hasta el Banco Mundial llamó la atención en esos riesgos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un ejemplo, la deducción de las regalías de los impuestos que las empresas privadas deben pagar; que fue solicitada por la Cámara Asomineros de la ANDI ante la Dirección de Impuestos y Aduanas (DIAN) y aprobada en el 2005; este hecho pasó de agache hasta 2012 cuando fue demandada ante  
el Consejo de Estado y anulada en octubre de 2017. En 12 años, este “beneficio” otorgado a las empresas le costó al Estado, según la revista Dinero, 4.353 millones de dólares (\$13,1 billones, con el dólar de \$3.000).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las **regalías** son los pagos que las compañías petroleras y mineras se comprometen a hacer al Estado por explotar los “recursos naturales” no renovables.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el 2004, Colombia le redujo las regalías a las empresas, pasando del 40% al 10%, y le permitió reducir ese 10% de los impuestos, prácticamente les “regala las regalías" hablan del 75% de impuestos, para la ley son el 33.25% (el impuesto nominal), pero con los “beneficios” las grandes empresas  pagan el 7% de impuestos.

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Tercero. **La profundización de la injusticia económica.** Las pequeñas y medianas  empresas -Mypymes- generan el 98% del empleo, mientras las grandes empresas generan  el 0.44%, pero los créditos del gobierno son a la inversa: grandes empresas: 72%, Mypymes: 23%.

<!-- /wp:list -->

<!-- wp:paragraph -->

Además el microcrédito tiene una tasa de usura del 55%. Los grandes propietarios rurales tienen el 81% detierra en Colombia, los pequeños y medianos tienen 19%; los  pequeños agricultores producen el  70% del total de los alimentos en el país, pero “Agro Produce”, línea especial de crédito para enfrentar las consecuencias del Covid 19 se distribuyó así: grandes propietarios rurales: 94%, medianos: 4%, pequeños agricultores: 2%.

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Cuarto. **Los que mandan: los bancos.** “Los que ponen la plata ponen las condiciones”. Los servicios bancarios en Colombia son uno de los más costosos del mundo y con mayores beneficios del Estado. Ejemplo, las garantías al sistema bancario del gobierno español son del 20%, mientras que en Colombia son del 90%.

<!-- /wp:list -->

<!-- wp:paragraph -->

Los  bancos nacionales  y extranjeros ganaron \$10.5 billones en  el   
año 2019. Por la crisis económica el Banco de la República bajó la tasa de interés de captación (el dinero que un ciudadano deposita en el banco) al  1.91%, mientras que el interés de colocación (el dinero que le prestan a ese ciudadano) está en el 18.69%, una  ganancia exagerada en ese movimiento del 16,78% que empobrece las familias  colombianas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El 90% del dinero con el que trabajan los bancos es de los ahorradores, solo el 10% de los bancos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La calidad de vida de las personas, de la sociedad y el cuidado de la naturaleza dependen de la economía y de los “valores” que la orientan. El empresario Arturo Calle, en una carta a los colombianos en el diario El Tiempo, afirma: “Cada trabajador con un ingreso digno es garantía de  
una mejor sociedad, justa y estable”.  Pero en Colombia, las decisiones económicas de los últimos gobiernos, han estado en “contra” de los sectores que generan empleo y riqueza para el país como los pequeños y medianos productores agrícolas, las pequeñas y medianas empresas e industrias,  
los pequeños y medianos comerciantes, y a “favor” del sector financiero y las grandes empresas nacionales y multinacionales.  Lo cual se agudiza con la crisis de la pandemia, porque el gobierno y  el  Banco  de la  República le han entregado grandes sumas de  dinero al  sistema  financiero, diciendo que es necesario mantener su funcionamiento a toda costa.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Una reflexión creyente

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con esta realidad de fondo, veamos el tema, olvidado, de  la “santidad económica”. Para esta reflexión tomo ideas de Norbert Lohfink, estudioso de la Biblia, reconocido mundialmente, en su artículo “El Reino de Dios y la economía en la Biblia” *(las citas entre comillas sin referencia son de él).*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todas  las iglesias cristianas reconocen que la Alianza, el Éxodo, los Profetas, la ley, el Reino de Dios, la santidad, la fidelidad a Dios y la voluntad de Dios son temas fundamentales. En esto no hay discusión. Las diferencias surgen cuando se empieza a precisar cómo entendemos y asumimos esos temas, con qué criterios estudiamos la Biblia para ver lo que decía en su tiempo y lo que dice para hoy.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La manera de entender y estudiar la Biblia está marcada por ideologías no reconocidas y ligadas a teologías marcadas por intereses económicos y de poder asumidos, sin reconocerlo o sin saberlo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho central del Antiguo Testamento –AT- es el Éxodo: la salida del pueblo de Israel de la esclavitud en Egipto y la llegada a la tierra prometida, como cumplimiento de la Alianza entre Dios y el Pueblo: “Yo seré su Dios y ustedes serán mi pueblo” (Ex 6,7).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El Dios del éxodo acompaña al pueblo por el camino que va de la esclavitud a libertad, porque no habita en los santuarios controlados por los reyes.

<!-- /wp:heading -->

<!-- wp:paragraph -->

La ley son los cinco libros del pentateuco que buscan “regular” la vida  
del pueblo para que en la “tierra prometida” no reproduzcan la esclavitud que han vivido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los profetas están atentos para que el pueblo cumpla la Alianza, la recuerdan constantemente, denuncian sus infidelidades, es decir, sus injusticias y le “exigen” cambio, conversión. Jesús no “vino a abolir la ley de los profetas sino a darles plenitud” (Mt 5,17), las palabras y acciones de Jesús buscan hacer realidad el reino de Dios (paz, justicia, amor, igualdad) en el presente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para la concepción cristiana tradicional la razón de ser de las iglesias es la salvación sobrenatural, futura e individual de las personas, desconociendo que el mensaje de Jesucristo transforma al ser humano y lo lleva a actuar de una “nueva” manera en todas las dimensiones de su vida, incluida la  
económica, que puede ser un medio de salvación, porque “el reinado de Dios, significa en la Biblia, la transformación de este mundo, también en su dimensión económica”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde hace varias décadas (6 o 8), se viene difundiendo una especie “estrategia” para desvirtuar las implicaciones concretas, históricas y temporales del reino de Dios, según el AT, para que el “mundo cristiano” olviden que el éxodo fue “abandono de un sistema del despotismo oriental y la iniciación de un nuevo orden social de libre fraternidad en el Sinaí”, para que desconozcan que para Israel la redención - salvación fue, “sobre todo, un cambio del sistema económico propiciado por su Dios…

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las excavaciones arqueológicas de los últimos decenios nos permiten caracterizar a esta época como un período de descolonización y desurbanización”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pueblo llegó a la “tierra prometida”, a las montañas porque las tierras planas y productivas estaban en manos de los poderos, allí se juntaron los habiru o apirú (nómadas o seminómadas, trabajadores migrantes, sirvientes o esclavos) y les compartieron su experiencia de fe liberadora y  
todos pasaron a ser “labradores libres que vivían en una sociedad igualitaria”, haciendo la voluntad del Dios que los sacó de la esclavitud y los llevó a experimentar “una especie de milagro social y económico”, porque Dios del éxodo se manifiesta cuando una sociedad transforma todas  
sus relaciones. La fidelidad y obediencia al nuevo Dios (el Dios del éxodo) se manifiesta “de manera decisiva en una nueva forma de economía humana”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Las leyes en los primeros libros de la Biblia, especialmente  el “Código de Santidad”, capítulos del 17 al 26 del Levítico y el “Código  Deuteronómico”, capítulos del 12 al 26 del Deuteronomio, buscaban impedir que el pueblo volviera a la esclavitud.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Código de Santidad, en el capítulo 25 del Levítico, establece las condiciones para el sector agrario de la economía, el sector fundamental, garantizara el ideal de la sociedad igualitaria, característica del pueblo de la Alianza y por medio del año sabático y el año jubilar restablece el “ideal” cuando se pierde.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El año sabático, que se celebra cada siete años (Lev 25, 1-7), es el descanso solemne al que tiene derecho la tierra. “El sábado santifica y consagra toda la creación, porque pone la justicia de la igualdad en la cumbre \[…\] El objetivo de las exigencias sabáticas es regresar al momento inicial de la creación… cuando Dios distribuyó justa y equitativamente a todo  
el mundo”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El año jubilar o año de la liberación, que se celebra cada cincuenta años, garantiza que quienes se han sido esclavizados por las deudas recuperen la libertad y que todos los israelitas que se han visto obligados a vender sus propiedades, su tierra y su casa, las recuperen (Lev 25, 8-55); de hecho ven el usufructo de la tierra porque “La tierra no se venderá a perpetuidad, porque la tierra es mía” (23) y además, se establece medidas para garantizar el mínimo necesario para la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El año jubilar o de la emancipación, era conocido en culturas antiguas, como en Grecia con la Sisactía, un conjunto de decretos del legislador griego Solón (638-558 a. C) que “abolían las deudas de todos aquellos que se habían visto obligados a hipotecar sus tierras y que consiguió, en poco tiempo, el relanzamiento de la estancada economía ateniense”. También Ptolomeo IV  
Epifanes, “han condonado totalmente algunos de los ingresos y tributos impuestos en Egipto y ha reducido otros para que el pueblo y todos los demás pudieran tener prosperidad durante su reinado”.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Código Deuteronómico, en el Deuteronomio 15,1-11, establece la ley del perdón o remisión de las deudas, una de las leyes más revolucionarias según los estudiosos de la biblia, que consiste en que “Todo acreedor condonará la deuda del préstamo hecho a su prójimo” (3).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los sistemas de “créditos” y la usura, empobrecían a muchos israelitas hasta el punto de convertirse en esclavos de los acreedores, por eso en diversas leyes del AT se legislaba para impedir el abuso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este proceso lo sintetiza John D. Crossan asís: “En la tradición bíblica -¿y también en todas partes?- la deuda conduce fácilmente a la esclavitud. Pero, con la deuda o como tal la deuda, aparece el interés o la hipoteca \[…\] La ejecución de la hipoteca por impago podía ser mucho más perjudicial  
que los intereses por los préstamos \[…\] La nefasta secuencia va desde el interés o la hipoteca hastallegar a la deuda, y de esta a la esclavitud \[…\] La liberación por deudas durante el años sabático es considerada tan importante que su incumplimiento es la causa específica que provoca la  
devastación de Israel”. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La razón teológica de las exigencias económicas del reino de Dios, del mensaje cristiano, se resumen en dos fórmulas de la Ley de santidad:

<!-- /wp:heading -->

<!-- wp:list -->

-   Primera fórmula: **"Amarás a tu prójimo como a ti mismo.** Yo soy el Señor" (Lv 19, 18). En el AT el “amor es la forma de relación típica dentro de la familia”. La expresión: "Yo mismo" es un semitismo que equivale a decir: "mi familia". Con este mandato el amor se hace extensivo al "prójimo", a quien no pertenece a la familia. En otras palabras: “en la sociedad que Dios establece en Israel debe imperar una relación de tipo familiar, en lugar de la típica relación político-jurídica”, tanto en la vida personal como en la economía. Esta “nueva” relación parece imposible para los seres humanos por eso siempre se ha “considerado como un milagro que el propio Dios realiza en la historia humana”.

<!-- /wp:list -->

<!-- wp:list -->

-   Segunda fórmula: **"Sean santos, porque yo, el Señor su Dios, soy santo"** (Lev 19,2). Esta fórmula se repite cuatro veces en el Código de Santidad y "Santidad" no significa aquí grandeza moral, sino alternativa social frente a otras sociedades del mundo”. El Dios santo, es el Dios diferente a los otros dioses que exigen para ellos sacrificios sin importarle la situación humana, por eso es “totalmente otro" y por eso el pueblo que es fiel y obediente al Dios Santo “debe ser el "totalmente otro" en contraste con otras sociedades del mundo”, porque la ley de santidad implica también una alternativa económica, es decir, la santidad económica. Según el Levítico, la santidad divina configura la santidad humana porque las dos defienden la justicia distributiva. “El nombre santo y la reputación divina del Dios bíblico concierten a la justicia distributiva y restauradora y nuestra santidad procede del hecho de participar de ese carácter, identidad y nombre de Dios”

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### El Nuevo Testamento, toma el concepto del  reino de Dios del Antiguo Testamento lo convierte en un mensaje universal dirigido a toda persona y a toda la humanidad, sin limitarlo a una determinada sociedad.

<!-- /wp:heading -->

<!-- wp:paragraph -->

“Dios quiere transformar todas las sociedades ofreciéndoles una alternativa social, la del pueblo de Israel”. Esta universalización se expresa como una  
peregrinación de las naciones que caminan hacia el pueblo de la Alianza, la sociedad igualitaria, a aprender cómo se puede vivir humanamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La propuesta del Nuevo Testamento puede expresarse así: “ahora termina el tiempo de la espera, precisamente ahora comienza lo que hasta ahora era esperado al final de los tiempos… Jesús lo formula así: "El reino de Dios está cerca" (Mc 1, 15). La novedad de Jesús era decir que había llegado el reino de Dios, todos sus oyentes sabían que era el reino de Dios, por eso no lo describía, su buena noticia era que “ahora comenzaba, lo que hasta ahora era esperado”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la vida de las primeras comunidades cristianas surgió un nuevo estilo de vida en relación con el trabajo y el dinero: “cuando alguien se hacía cristiano \[independiente de su estatus social y condición económica\] decidía también ponerse a trabajar”, en contraste con su entorno socio-  
cultural donde “trabajaban únicamente las mujeres y los esclavos, eventualmente también los mercaderes pobres y los labradores”. La conversión reino de Dios, anunciado por Jesús, “Cambió la relación entre amos y esclavos: se trataban como hermanos y hermanas y comían juntos. Todo esto tuvo que suponer una profunda transformación de la actividad económica desde dentro. También las relaciones comerciales tomaron nueva forma pues se basaban en la confianza mutua… entonces surgió y floreció una forma de sociedad y en su marco también una economía  
en claro contraste con la realidad circundante”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las primeras comunidades cristianas colocan sus bienes y posesiones en común y se repartían de acuerdo a las necesidades de cada uno, por eso no  
había ningún necesitado entre ellos (Hech 2, 42-47; 4,33-35).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 Es muy importante que como cristianos y cristianas revisemos la forma de estudiar la Biblia para ser más fieles a la voluntad de Dios, que quiere que seamos santos como Él es Santo, para reconocer que la santidad tiene una dimensión económica.   
   
Fraternalmente, su hermano en la fe,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
**P. Alberto Franco, CSsR, J&P**  
francoalberto9@gmail.com

<!-- /wp:paragraph -->
