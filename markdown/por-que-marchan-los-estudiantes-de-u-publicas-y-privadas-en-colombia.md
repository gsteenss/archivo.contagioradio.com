Title: ¿Por qué marchan los estudiantes de U. Públicas y Privadas en Colombia?
Date: 2018-04-25 12:47
Category: Educación, Nacional
Tags: Ministerio de Educación, Movimiento estudiantil, ser pilo paga
Slug: por-que-marchan-los-estudiantes-de-u-publicas-y-privadas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [25 Abr 2018] 

Más de 4 mil estudiantes de universidades públicas y privadas se dieron cita hoy en las calles de Colombia, para movilizarse en contra de la política pública “Ser Pilo Paga”, los altos costos de los créditos de Icetex, **el aumento en la brecha de quienes pueden acceder a la educación y la crisis financiera que afronta la misma**.

### **Los estudiantes tienen propuestas** 

Santiago Rodríguez, estudiante de la Pontificia Universidad Javeriana e integrante de la Asamblea Estudiantil de la misma, afirmó que el pliego de motivos se construyó con la participación de estudiantes de **11 universidades privadas de Bogotá, en el marco del Encuentro Nacional de Estudiantes de Educación Superior**.

El primero punto de estas exigencias, tiene que ver con el programa “Ser Pilo Paga”. Los estudiantes están proponiendo que se acabe esta política, **derogando la ley que la convierte en política pública y que los recursos que son invertidos en la misma**, deberían ser redirigidos a la base presupuestal de las universidades públicas. De igual forma, plantean que quienes ya son beneficiarios de esta estrategia, tengan garantías de permanencia y graduación.

Sobre los altos costos de los créditos Icetex, y las deudas que se hacen inpagables, los estudiantes sugieren que se elimine el interés conjunto y se haga una refinanciación de la entidad para que se transforme en un banco público. Además, exigen que se derogue la ley que aprobó el Congreso de la República hace dos semanas, en dónde permite que Icetex se traslade el cobro del **crédito en caso de no pago a los co deudores o en caso de muerte del deudor**.

En referencia a los altos costos de matrículas que deben pagar los estudiantes, están construyendo un índice de precios de la educación superior, que tenga en cuenta la capacidad de pago de las personas y las necesidades de las instituciones educativas, además, **proponen que mientras se elabora ese índice, las universidades privadas congelen las matrículas**.

### **La calidad de la educación una preocupación de los estudiantes** 

De igual forma, los estudiantes están proponiendo que las acreditaciones de alta calidad solo se otorguen a las universidades de educación superior si se hace una reforma curricular en donde primer el interés por el pensamiento crítico. “Lo que está fomentando la educación privada es una **profundización de la relación entre la sociedad y el mercado**” afirmó Rodríguez.

Sobre la investigación, los estudiantes exigen que se dé cumplimiento a la Ley 1286, que habla sobre la financiación a las comunidades científicas. Al mismo tiempo piden al Ministerio de Educación que la financiación a la investigación sea constante y que no dependa de los gobiernos de turno.

**Participación estudiantil.**

Una de las banderas históricas del Movimiento Estudiantil, tienen que ver con la democracia universitaria, en ese sentido, los estudiantes proponen que se continúe profundizando en analizar los **cogobiernos y en ampliar los escenarios de discusión de la comunidad académica en general**.

A su vez, consideran que deben mejorarse los mecanismos de denuncia de acoso sexual en las universidades tanto públicas como privadas, creando canales seguros de denuncia y atención a las personas víctimas. (Le puede interesar: ["Estudiantes de U.Públicas y Privadas le apuestan a la lucha por la educación superior"](https://archivo.contagioradio.com/estudiantes-de-universidades-publicas-y-privadas-le-apostaran-a-la-lucha-por-la-educacion-superior/))

"Hoy la universidad esta totalmente alejada de la realidad y es un espacio transversalisado por el conflicto social y armado, no se habla de los niveles de deserción, de los niveles de acoso, del detrimento de la educación" afirmó Rodríguez.

<iframe id="audio_25623553" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25623553_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
