Title: "Soma Mnemosine" Cuerpo Personal y social
Date: 2015-09-30 15:04
Category: Cultura, eventos
Tags: "Soma Mnemosine" teatro la Candelaria, Patricia Ariza directora teatro, teatro la candelaria
Slug: soma-mnemosine-cuerpo-personal-y-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/patricia-e1443642948748.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [30 Sep 2015] 

Con la presentación de la obra performance "**Soma Mnemosine**", del 30 de septiembre al 10 de octubre a las 7:30 de la noche, el teatro La Candelaria, perfila el cierre de su temporada 2015.

Dirigida por la maestra **Patricia Ariza**, la puesta en escena "nace del trabajo de laboratorio teatral en grupo, en este caso, el pre-texto fue la indagación en el cuerpo tanto en estado de dolor como de júbilo", exploración que da como resultado "un teatro dentro del cuál el contexto violencia-fiesta del país se nos colocó al interior de los cuerpos de los actores y actrices".

[![obra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/obra.jpg){.wp-image-15028 .aligncenter width="393" height="262"}](https://archivo.contagioradio.com/soma-mnemosine-cuerpo-personal-y-social/obra/)

El "cuerpo personal y social", se presenta como tema central de la producción performática, sometido a la violencia, que encuentra en la fiesta su "refugio y espacio de resistencia, una secuencia entre cuerpo-persona-cuerpo-sociedad".

Parte del equipo de "**Soma Mnemosine**", esta compuesto por Carmiña Martínez, Fernando Mendoza, Libardo Flórez, Cesar Amézquita en la creación colectiva, Karen Roa y Franchesco Corbelletta en los videos, Carlos Satizabal en los arreglos Musicales y efectos de sonido, y las maestras Diana Casas y Edna Cabrera en Danzas y Acordeón respectivamente.

[![obra1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/obra1.jpg){.aligncenter .wp-image-15035 width="398" height="265"}](https://archivo.contagioradio.com/soma-mnemosine-cuerpo-personal-y-social/obra1/)

 

**Lugar:** Teatro La Candelaria Calle 12 \# 2-59.

**Fecha:**  Miércoles a Sábado del 30 de Septiembre al 10 de Octubre

**Hora:** 7:30 de la noche.

##### Con información de Prensa Teatro La Candelaria. 
