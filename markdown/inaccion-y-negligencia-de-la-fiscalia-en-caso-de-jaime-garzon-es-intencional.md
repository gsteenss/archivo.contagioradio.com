Title: "Inacción y negligencia de la Fiscalía en caso de Jaime Garzón es intencional"
Date: 2015-06-10 14:08
Category: Judicial, Otra Mirada
Tags: carlos castaño, colectivo jose alvear restrepo, colegio de juristas, Fiscalía, Jaime Garzon, marisol garzon, paz
Slug: inaccion-y-negligencia-de-la-fiscalia-en-caso-de-jaime-garzon-es-intencional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/20150610_111627.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4622825_2_1.html?data=lZuflJ2WeY6ZmKiakpaJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcPjyMbR0ZC3qcPV1NnWw9OPidTX0MfO1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sebastián Escobar, Comisión colombiana de Juristas] 

<iframe src="http://www.ivoox.com/player_ek_4622828_2_1.html?data=lZuflJ2WfI6ZmKiakpWJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcPjyMbR0ZCwucrnjKziy9HQqdPh0JC9x9fJvo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Guillermo Perez, Colectivo de Abogados José Alvear Restrepo] 

   
 

###### [10 jun 2015] 

El martes 9 de junio, el Fiscal 71 delegado ante el Tribunal Superior de Bogotá rechazó la solicitud de la familia del periodista y humorista asesinado, Jaime Garzón, para declarar el suyo como un crimen de lesa humanidad. Para la familia Garzón, "lo que ha hecho la Fiscalía no tiene nombre".

Y es que 16 años después del magnicidio, el crimen continúa en la impunidad: Actualmente sólo se adelantan 2 procesos, en contra de José Miguel Narvaez, quien responde también por el secuestro de Piedad Córdoba y el asesinato de Eduardo Umaña; y en contra del Coronel (r) Jorge Eliecer Plazas Acevedo. La única condena proferida es en contra el extinto jefe paramilitar Carlos Castaño, y no como resultado de un proceso investigativo, sino porque él mismo reconoció públicamente haber dado la orden a la banda "La Terraza" para que ejecutaran el crimen.

\[caption id="attachment\_9933" align="alignright" width="311"\][![Marisol Garzon](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/20150610_105805.jpg){.wp-image-9933 width="311" height="187"}](https://archivo.contagioradio.com/inaccion-y-negligencia-de-la-fiscalia-en-caso-de-jaime-garzon-es-intencional/20150610_105805/) Marisol Garzon\[/caption\]

En el caso de Jaime Garzón se repite la conducta de la Fiscalía con miles de familias colombianas, que ante la inacción y la negligencia por parte del organismo acusador, se ven obligados a asumir la carga investigativa para que los procesos no prescriban y queden en la impunidad.

Según declaró la asistente del Fiscal que dio a conocer el fallo, los colombianos deben estar tranquilos porque el crimen aún no está en riesgo de prescribir; Pero para los abogados de la familia, Sebastian Escobar y Luis Miguel Perez, es inaceptable que los argumentos de la Fiscalía no estén basados en argumentos objetivos, sino que se ciñan criterios instrumentales, del riesgo que se tiene o no en que prescriba.

Por otra parte, aunque la Fiscalía asegura ceñirse al Estatuto de Roma para negar la solicitud, el abogado Luis Guillermo Perez afirma que la decisión es contraria a Derecho, en tanto el magnicidio cumple con los 3 requisitos que exige el Estatuto para que un crimen sea declarado de lesa humanidad: Se asesinó a una persona y este crimen representó un ataque a la sociedad general, como quedó demostrado tras el duelo general que vivió el país; además, sus autores intelectuales tenían conocimiento de que el ataque generaría este efecto.

\[caption id="attachment\_9934" align="aligncenter" width="2037"\][![Decisión de la Fiscalía](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/20150610_111839.jpg){.size-full .wp-image-9934 width="2037" height="570"}](https://archivo.contagioradio.com/inaccion-y-negligencia-de-la-fiscalia-en-caso-de-jaime-garzon-es-intencional/20150610_111839/) Decisión de la Fiscalía\[/caption\]

"Si en 16 años la Fiscalía no han hecho nada ¿qué van a hacer en 4 años que quedan antes de que prescriba?" pregunta Marisol Garzón, hermana del humorista, y recuerda que la negligencia de la Fiscalía ha sido intencional, en tanto, como denunció en su momento el abogado Alirio Uribe, de las más de 40 hipótesis que se tenían sobre el magnicidio, la fiscalía asumió la única que había sido construida con pruebas falsas por el ahora extinto Departamento Administrativo de Seguridad, DAS.

Para los abogados existen al menos 2 claridades en el proceso: Que el homicidio fue el resultado de una alianza entre el Estado y el paramilitarismo, y que al rededor del asesinato se configuran una multiplicidad de crímenes más, contra el ejercicio del periodismo, la defensa de los Derechos Humanos y la paz, y la sociedad colombiana en general.

La familia Garzón perdió esta pelea contra la Fiscalía, pero aseguran que insistirán para que este sea declarado un crimen de lesa humanidad, ante la juez que lleva el caso contra José Miguel Narvaez, el próximo 16 de julio, cuando se escucharán los alegatos finales del proceso. Los abogados de la Comisión colombiana de Juristas y el Colectivo de Abogados José Alvear Restrepo ya interpusieron una petición ante el Sistema Interamericano de Derechos Humanos para que se evalúe y acompañe la solicitud.
