Title: Casas de tortura en el corazón de Bogotá
Date: 2016-06-03 08:59
Category: Fernando Q, Opinion
Tags: bronx bogota, Casas de pique
Slug: casas-de-tortura-en-el-corazon-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/operativos-bronx-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: HSB 

#### **Por [Luis Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [@FdoQuijano](https://twitter.com/FdoQuijano) ** 

##### ***“No hay que poner la palabra antes de la realidad, hay que poner primero la realidad”.*** 

##### **Leonora Carrington.** 

Sobre la existencia o no de las casas de tortura, pique o del terror se viene hablando desde hace más de una década, comienzo por dejar en claro que para mí son casas de tortura las que están instaladas en Medellín y su área metropolitana ya que la modalidad allí implementada se diferencia de las casas de pique en Buenaventura, Barranquilla, entre otras.

[El debate surge cada vez que aparece un cuerpo desmembrado, embolsado o amarrado en algunas de las ciudades ya mencionadas. Unos afirman, tajantemente, que no existen, y entre esos se incluye la institucionalidad, aunque las evidencias desborden la negación. Otros dicen que sí podrían existir, pero de forma simplista y sin análisis alguno plantean que es algo esporádico. Otros afirman —entre los que me cuento— que las casas donde se torturan y asesinan seres humanos existen y hacen parte de las practicas paramilitares urbanas que buscan generar miedo, terror y silencio para poder controlar eficazmente a las comunidades, dejo en claro que esas casas también existen en lo rural.]

[Cada vez que surge el debate los encargados de ocultar la verdad o minimizarla hablan de que su existencia es un mito urbano, los que aseguramos abiertamente que son una realidad difícil de ocultar llevamos la peor parte, todo el tiempo se nos acusa de generar zozobra y miedo con el único propósito de dañar la imagen de las ciudades y sus autoridades, son simples invenciones de mentes calenturientas, la descalificación prima a la hora de debatir, no hay argumentos.]

[El debate más reciente se originó el 13 de octubre del año 2015, cuando publiqué en el portal]*[Análisis Urbano,]*[ el artículo:][[Más allá de negar su existencia, ¿qué hacemos con las casas de tortura?]](http://analisisurbano.org/mas-alla-de-negar-su-existencia-que-hacemos-con-las-casas-de-tortura-en-el-medellin-metropolitano/) [En este se presentaba con pruebas reales la existencia de las casas de tortura en Medellín. La afirmación se afianzaba haciendo un recorrido sobre varios episodios de desmembramiento que se presentaron en la ciudad y la forma falaz como la institucionalidad, a pesar de las evidencias palpables, trató de ocultar el horror que dejan estos actos de barbarie. El artículo incluso nombraba algunos sitios que podrían ocultar las casas de tortura. Tengan plena seguridad de que todo esto va más allá de una novela de horror y ficción, que es lo que han tratado de insinuar las autoridades. Esos calificativos despectivos que nos endilgan solo buscan ocultar la irresponsabilidad y la complicidad de un sector de la institucionalidad con estos sanguinarios delincuentes.]

[Cada vez que denuncio la existencia de las casas de tortura lo hago con la esperanza de que los Gobiernos nacional, departamental y municipal, y por ende el resto de gobiernos del Valle del Aburrá de la mano de la fuerza pública y la Fiscalía, se pellizquen simultáneamente y tomen decisiones de fondo para enfrentar dicha problemática que desafortunadamente ha sido ocultada y silenciada adrede para que el rastro de tortura, muerte atroz y desapariciones no causen impacto negativo en la imagen de la ciudad metropolitana a cuya cabeza se encuentra Medellín, ciudad de contrastes: progreso y criminalidad, innovación mentirosa, tranvía, metro, parques, bibliotecas y en medio de todo eso la pobreza y la desigualdad en aumento.]

[El 25 de mayo del 2016 escribí un apunte urbano sobre][[La Chilly]](http://analisisurbano.org/chilly-prueba-reina-de-la-existencia-de-casas-de-pique-y-tortura-en-colombia/)[, líder de las casas de pique en Buenaventura, y dejé entrever que ella era la prueba reina de la existencia de las casas donde se tortura y se descuartiza seres humanos. Estaba seguro de que el secreto ya no se podría ocultar más tiempo: existen las casas de pique y las casas de tortura.]

[Nuevamente me equivoqué, el tema volvió a ser ignorado, claro está que la prueba reina que representaba el juicio a la Chilly ya no lo es, la prueba fehaciente de la existencia de estos mataderos de seres humanos apareció en el mismo corazón de la capital de Colombia, Bogotá, allí, en el][[Bronx]](http://www.elespectador.com/noticias/judicial/casa-de-tortura-el-bronx-articulo-635125)[, la historia de terror se repite, pareciera calcada de la que han vivido Medellín, Buenaventura, Tuluá, Barranquilla, entre otras ciudades, aunque déjenme decirles que lo hallado allí sorprende por las prácticas utilizadas que incluían perros para devorar las víctimas de los jefes del crimen, quienes al igual que en otras regiones del país cuentan con nómina paralela que les da protección oficial, en otras palabras, miembros de organismos de seguridad del Estado los protegen a cambio de una jugosa cifra de dinero.]

[Finalmente, si ya se sabe de la existencia de las casas de tortura, pique o del terror en varias regiones de Colombia, incluyendo su capital, el presidente Juan Manuel Santos debe ordenar inmediatamente la creación de un bloque de búsqueda compuesto por funcionarios de Fiscalía, Policía Nacional y Ejército que elaboren el análisis y el contexto, además de la cartografía del terror, lo que permitirá entender el alcance real de las  casas de tortura y, por ende, identificar a los autores intelectuales y materiales encargados de realizar estas  prácticas del terror.]

------------------------------------------------------------------------

**Apunte Urbano**

[Qué bueno sería ver al presidente de la república impartiendo órdenes encaminadas a la búsqueda y el desmantelamiento de las casas de tortura, sin embargo, eso solo ocurrirá cuando él se ponga en los zapatos de la ciudadanía que asiste diariamente a la orgía de sangre que estas casas producen. Esperemos que ahora sí lo haga, ya que los hallazgos se encontraron muy cerca de donde vive y gobierna el país.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
