Title: Simulación de conversaciones entre ELN y gobierno: una apuesta de paz desde la ciudadanía
Date: 2017-01-11 15:09
Category: Nacional, Paz
Tags: #PazALaCalle, ELN, mesa de conversaciones Quito
Slug: simulacion-de-conversaciones-entre-eln-y-gobierno-una-apuesta-de-paz-desde-la-ciudadania
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [11 Enero 2017] 

Tras la firma de los acuerdo de paz entre el gobierno y la guerrilla de las FARC-EP, Paz a la calle no se ha quedado quieta y sigue manteniéndose como un escenario ciudadano que permita tener incidencia en el futuro del país, por este motivo, en conjunto con otras organizaciones, este 12 de enero, **se harán una simulación de lo que sería la instalación y las conversaciones en la mesa de Quito.**

El evento se hará al mismo tiempo que se  lleva a cabo la reunión entre la guerrilla del Ejército de Liberación Nacional y el gobierno, mientras que a su vez se realizará en la plaza Simón Bolívar, en Bogotá, la simulación a través de un **performance sobre la importancia de hacer la paz en un país como Colombia y las consecuencias de que continúe la guerra**.

Entre las organizaciones que acompañarán esta iniciativa están  Paz a la Calles, Pazharemos, Javerianos por la paz, entre otros. De acuerdo con Juan Camilo Caicedo, miembro de Paz a la Calle **“es clave que la sociedad civil le apueste al proceso de paz con el ELN, a su vez que continua siendo veedor de la implementación de los acuerdos de paz con la guerrilla de las FARC-EP**”. Le puede interesar: ["Propuestas de la Sociedad civil son claves para destrabar mesa ELN-Gobierno"](https://archivo.contagioradio.com/propuestas-de-la-sociedad-civil-son-claves-para-destrabar-mesa-eln-gobierno/)

<iframe id="audio_16133871" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16133871_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
