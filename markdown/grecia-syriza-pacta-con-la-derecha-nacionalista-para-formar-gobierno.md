Title: Grecia: Syriza pacta con la derecha nacionalista para formar gobierno
Date: 2015-01-26 16:33
Author: CtgAdm
Category: El mundo, Política
Tags: elecciones Grecia, podemos, Syriza, Troika
Slug: grecia-syriza-pacta-con-la-derecha-nacionalista-para-formar-gobierno
Status: published

###### **Foto:Eje Central** 

**Entrevista Rafael Cid**  
<iframe src="http://www.ivoox.com/player_ek_3999842_2_1.html?data=lJ6mm52Ydo6ZmKiakp6Jd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl9rmyt%2FOjczFssKfxtHSxcjNs8%2FZ1JDS0JCrtsbXysaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Entrevista Stavros Kassis**  
<iframe src="http://www.ivoox.com/player_ek_4000525_2_1.html?data=lZWdkpqWeY6ZmKiak5yJd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl9rmyt%2FOjdXFp9XVjMjc0JDQpYzYxtfSxc3Fb8%2FVxM7c0MbQrdTowpDdw9fFb8jjw8rf0MbWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Esta mañana Tsiripas, líder de la formación de izquierdas Syriza, ganadora de las elecciones griegas , ha jurado el cargo de primer ministro en una ceremonia civil, convirtiendose en el primer mandatario griego sin jurar el cargo ante una autoridad religiosa.

Al no alcanzar por dos puntos, la mayoría absoluta para poder gobernar ha tenido que pactar con el partido de derechas y nacionalista, **Griegos Independientes, fuertemente criticado por sus políticas contra la inmigración.**

Muchas son las opiniones en Europa y en el mundo entero al respecto del cambio político en el país mas azotado por la crisis económica y por las medidas de austeridad impuestas por la Troika.

Lo que si es seguro es que el triunfo de dicha formación **rompe con el tradicional bipartidismo y con las formaciones alineadas con las políticas de austeridad**, los recortes sociales, laborales y el continuismo con las políticas orquestadas por el FMI y el BM.

Por otro lado el FMI ya ha advertido que Grecia no es especial y el trato va a ser el mismo con respecto a otros países.En el aire queda la negociación de la deuda económica griega y su salida o no de la zona euro.

Para poder comprender mejor la situación política y las consecuencias que pueda tener dicho cambio sobre países como España, con similares situaciones económicas, sociales y políticas, entrevistamos a Rafael Cid, analísta político de medios independientes del estado español quién nos clarifica aspectos importantes para comprender la actual coyuntura.

También conversamos con Stavros Kassis, activista griego y analísta político, quién además de contextualizar las elecciones, nos advierte sobre la consolidación del partido de ultraderecha Amanecer Dorado, a pesar de que la gran mayoría de sus líderes están en prisión.
