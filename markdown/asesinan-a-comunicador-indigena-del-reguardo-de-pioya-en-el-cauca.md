Title: Asesinan a comunicador indígena del reguardo de Pioyá en Caldono
Date: 2018-03-05 14:52
Category: DDHH, Nacional
Tags: Asesinato de indígenas, Caldono, CRIC, Eider Campo Hurtado, guardia indígena
Slug: asesinan-a-comunicador-indigena-del-reguardo-de-pioya-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/indígena-asesinado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ONIC] 

###### [05 Mar 2018] 

Desde el Consejo Regional Indígena del Cauca (CRIC) denunciaron el asesinato del comunicador indígena **Eider Campo Hurtado** en el Resguardo de Pioyá en el municipio de Caldono en Cauca. El hecho ocurrió en la madrugada del domingo 4 de marzo “cuando un grupo de 4 hombres fuertemente armados ingresan a la casa del Cabildo donde se encontraba un detenido con un prontuario que desarmoniza la comunidad”.

El asesinato ocurrió cuando la Guardia Indígena estaba realizando una persecución para tratar de **recapturar a tres hombres** quienes, de acuerdo con el CRIC, “tienen vínculos con la Fuerza Pública”. Indicaron que cuando la Guardia entró en contacto con estos hombres, cuatro más que estaban armados dispararon contra la comunidad en donde perdió la vida el comunicador indígena.

### **Los hombres armados asaltaron el cabildo indígena para llevarse a 3 “delincuentes”** 

Según el CRIC los hombres que atentaron contra la vida del joven comunicador tenían intenciones de sacar del Resguardo a “tres delincuentes **confesos y convictos** en la Asamblea Comunitaria bajo jurisdicción indígena”. Dice el comunicado que uno de los detenidos era “un informante remunerado por el Ejército Nacional de Colombia”.

Además, indica el CRIC que “esta acción se realiza, sin duda, para evitar que estos delincuentes, **con vínculos criminales con la fuerza pública,** aparecieran en la audiencia programada unas horas después de este asalto armado”. En repetidas ocasiones los indígenas han alertado sobre acciones de “guerra contra los pueblos cuya responsabilidad más aparente recae sobre la Fuerza Pública”. (Le puede interesar:["Campesinos e indígenas están en riesgo permanente: Misión Internacional Vía Campesina"](https://archivo.contagioradio.com/campesino-e-indigenas-estan-en-riesgo-permanente/))

### **Alcalde de Caldono confirmó la presencia de grupos armados en el territorio** 

Pablo Piso, alcalde del municipio de Caldono en el Cauca indicó que una de las personas detenidas en el cabildo indígena **“hace parte de un grupo armado** que está operando en la zona”. En el momento que es sustraído por los hombres armados, la guardia Indígena reacciona y “hubo unos disparos por parte del grupo que acabaron con la vida del comunero”.

Ante esta situación, el alcalde manifestó que “ha sido muy difícil entrar a la zona, pero la Guardia Indígena **ya activo las guardias de los cabildos cercanos** y están en la búsqueda de las personas que se fugaron”. Pese a que las autoridades han tenido indicios de que en la zona operan grupos armados, “no se ha podido confirmar de quién se trata”. Sin embargo, está claro que hay grupos que están ejerciendo control territorial que vienen poniendo en riesgo a las comunidades indígenas.

<iframe id="audio_24247058" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24247058_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
