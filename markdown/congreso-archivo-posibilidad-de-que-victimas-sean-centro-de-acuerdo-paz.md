Title: Congreso archivó posibilidad de que víctimas estén en centro de Acuerdo Paz
Date: 2017-02-15 12:36
Category: DDHH, Nacional
Tags: Fast Track, implementación acuerdos de paz
Slug: congreso-archivo-posibilidad-de-que-victimas-sean-centro-de-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Feb 2017]

Víctimas y organizaciones sociales como el Movimiento de Víctimas de Crímenes de Estado (MOVICE), denunciaron que si bien el universo de víctimas tuvo una participación durante el proceso de paz, **ahora su proposición para que se mantenga esta participación en los debates del Fast Track fue archivada por el Congreso.**

Esto debido a que esa instancia del Estado considera que la participación de una víctima delegada de la Mesa Nacional de víctimas es suficiente representación para los debates. No obstante, las organizaciones de víctimas consideran que este hecho **impide que las críticas que tienen, como la modificación al artículo 28 sobre la cadena de mando o la ausencia de los congresistas en los debates, sean escuchadas y atendidas. **Le puede interesar: ["Organizaciones sociales exigen justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

Para Alejandra Gaviria, integrante de H.I.J.O.S y el MOVICE, hay tres postulados que permiten que las víctimas **“sean el centro del acuerdo” como se había pactado en La Habana y que se están incumpliendo.**

El primero de ellos es que **la Mesa de Nacional de Víctimas nunca fue el mecanismo establecido para que las víctimas del Estado**, las FFMM o del aparato judicial, participaran en los diálogos y en el Acuerdo, debido a que en la ley 975 de Justicia y Paz hay varias exclusiones de lo que se considera víctima como las víctimas de la ley o los mecanismos de participación.

El segundo señalaba que **“para que la paz fuera posible debía tenerse en cuenta a todo el universo de víctimas”** y el tercer postulado era que en las conversaciones no solo se iba a tener en cuentas a las víctimas de las FARC-EP **“si no que en el centro estarían los derechos de todas las víctimas”** motivo por el cual, durante los diálogos participaron víctimas de paramilitarismo, del Estado entre otras. Le puede interesar: ["Organizaciones sociales ponen la lupa en recursos públicos para la paz"](https://archivo.contagioradio.com/los-restos-de-organizaciones-populares-y-movimiento-social-para-construir-paz/)

De igual forma, Silvia Berrocal, víctima de la masacre de La Chinita afirmó que aunque el gobierno se haya comprometido en mantener a las víctimas en el centro de la implementación **“eso no se ha cumplido”** y **continúan a la espera de que se les notifique sobre los compromisos entre gobierno, FARC y víctimas de la Chinita.**

Sin embargo, para volver a ser parte de estos debates, organizaciones de víctimas están pidiendo que **se amplié a la Comisión de Seguimiento Impulso y Verificación a la Implementación del Acuerdo Final**, para que tanto el gobierno como las FARC, escuchen las dudas y propuestas de las víctimas, y a su vez, exista un canal de comunicación más claro que informe sobre el cronograma de debates.

En esta vía, proponen que se instale un **Cónclave en el Congreso** que permite que se trámite más rápido el Fast Track, que para llevar dos meses solo ha aprobado un proyecto de ley, y **que de la posibilidad de generar sesiones más intensivas de debate.** Le puede interesar: ["Del Fast Track al Slow Track"](https://archivo.contagioradio.com/del-fast-track-al-slow-track-en-la-implemetacion-de-acuerdos-de-paz/)

<iframe id="audio_17038480" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17038480_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
