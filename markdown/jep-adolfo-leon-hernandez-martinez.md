Title: Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos
Date: 2019-03-07 12:59
Category: DDHH, Política
Tags: Ejecuciones Extra Judiciales, Fuerzas militares, JEP
Slug: jep-adolfo-leon-hernandez-martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Brigadier-general-Adolfo-León-Hernández-Martínez-jep.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 7 Mar 2019

Organizaciones de DD.HH. presentaron ante la Jurisdicción Especial para la Paz el  informe** **¿Qué futuro nos espera?, que recopila evidencia sobre la** responsabilidad del Teniente Coronel Adolfo León Hernández, en 23 casos de  ejecuciones extrajudiciales,** ocurridas bajo su comandancia. Actualmente,  el teniente está al mando del **Comando de Transformación del Ejército.**

Tal informe, expuesto por el Colectivo de Abogados José Alvear Restrepo, la Comisión Colombiana de Juristas y el Comité de Solidaridad con los Presos Políticos revela cómo fueron asesinadas 39 personas, de las cuales 13 continúan sin ser identificada, simulando combates en los que la mayoría de las víctimas se presentaron como integrantes de Bacrim, y otras como integrantes del ELN o las antiguas FARC.

Con base en dicha evidencia,  se ha contemplado la creación de un sistema de control público de ascenso para las Fuerzas Militares, pues en la actualidad estos son debatidos únicamente en el Congreso, sin consultar a la sociedad, "de igual forma se ha solicitado a la JEP que preventivamente ordene la suspensión de su cargo al teniente coronel **Adolfo León Hernández Martínez"** indicó **Jormary Ortegón, presidenta del Colectivo de Abogados José Alvear Restrepo.**

\[caption id="attachment\_62829" align="alignnone" width="800"\]![El informe es entregado a la JEP](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-07-at-12.22.52-PM-800x450.jpeg){.size-medium .wp-image-62829 width="800" height="450"} El informe es entregado a la JEP\[/caption\]

### **La evidencia recopilada** 

Sobre los patrones evidenciados, el **abogado Harold Vargas** afirma que existieron falsificaciones de documentos, incongruencias entre los testimonios de soldados involucrados y evidencias sobre los cuerpos y las prendas de vestir no correspondientes, evidenciando la responsabilidad del comandante para la época "por que sabía y no ejerció ninguna acción para impedir lo que estaba ocurriendo o bien porque hubo falencias en sus evaluaciones de control".

Incluso, se logró documentar que  en los juicios a los autores materiales por los hechos, fueron defendidos públicamente por el coronel León Hernández, lo que supondría un conocimiento sobre el accionar de sus subordinados, y que algunas de las ejecuciones fueron cometidas por fuera de la jurisdicción en los que operaba el Batallón Popa.[(Le puede interesar: Nueve comandantes del Ejército estarían implicados en falsos positivos: Human Rights Watch)](https://archivo.contagioradio.com/nueve-comandantes-del-ejercito-estarian-implicados-en-falsos-positivos-human-rights-watch/)

De igual forma han solicitado que toda la línea de mando sea investigada; para la época de los hechos, los superiores jerárquicos  de Martínez Hernández fueron el Coronel **Iván Dario Recuero, el Brigadier General  Luis Felipe Paredes Cadena, el Mayor General Enrique González Peña, el General Mario Montoya Uribe, el Comandante de las Fuerzas Militares, Freddy Padilla de León y el actual expresidente  de la República, Álvaro Uribe Vélez.**

El Batallón "La Popa" hace parte de la Brigada Décima Blindada, que a su vez pertenece  a la División Primera del Ejército compuesta de la Brigada Segunda y la Brigada Décima.  Por lo menos el 90% de ejecuciones extrajudiciales cometidas entre el 2000 y 2007 se encuentran documentadas en el Cesar y La Guajira, sin embargo, hay casos documentados en **Bolivar, Magdalena y Atlántico.**

Este es el primero de cinco informes que serán presentados ante la JEP por las organizaciones  de derechos humanos a lo largo del año que buscan evidenciar la responsabilidad  de las fuerzas militares durante la época del conflicto armado. **  
**

###### Reciba toda la información de Contagio Radio en [[su correo]
