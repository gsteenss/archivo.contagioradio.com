Title: "El final de mi vida esta cerca..." Fujimori tras volver a prisión
Date: 2019-01-24 12:30
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: Délitos de lesa humanidad, Fujimori, Perú, PPK
Slug: fujimori-vuelve-prision
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/2018-10-03t174821z_1873548449_rc1806b949e0_rtrmadp_3_peru-fujimori_0-e1548350135136-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Reuters 

###### 24 Ene 2018 

Tras permanecer más de 100 días en una clínica de Lima, el expresidente peruano **Alberto Fujimori fue dado de alta y conducido al penal de Barbadillo en la Diroes**, para que siga cumpliendo con la pena a **25 años de cárcel por violaciones a los derechos humanos** durante los años que estuvo en el poder entre 1990 y 2000.

Según la Corte Suprema del poder judicial, **tras informe presentado por la junta médica del Instituto de Medicina legal, la salud de Fujimori,** quien se encontraba internado por una afección cardíaca desde el 3 de octubre del año anterior, **es estable y puede ser tratado de manera ambulatoria**, de ahí la decisión de enviarlo de nuevo al centro penitenciario en la noche de este miércoles.

En Barbadillo el expresidente **venía cumpliendo su condena desde 2007 hasta 2017**, cuando recibió un polémico indulto ordenado por el entonces presidente **Pedro Pablo Kuczynski**, en lo que se consideró como un pacto con el fujimorismo para evitar la declaratoria de vacancia presidencial, **por las relaciones de PPK con el caso de corrupción de Odebrecht**.

A través de su cuenta en twitter el expresidente de 80 años publico un trino en el que manifestó **"El final de mi vida esta cerca...".** Por primera vez un expresidente estará en prisión al mismo tiempo que su hija, la ex candidata **Keiko Fujimori, quien esta recluída por tres años mientras la fiscalía peruana investiga sus nexos también con Odebrecht**.

Fujimori **fue sentenciado en 2009 por los delitos de homicidio calificado, secuestro agravado y lesiones graves**, relacionadas con las acciones de un escuadrón de la muerte conocido como el Grupo Colina, que según la sentencia actuaba bajo órdenes del mandatario; **decisión histórica por tratarse del primer mandatario juzgado y sentenciado en su propio país por delitos de lesa humanidad**.

###### Reciba toda la información de Contagio Radio en [[su correo]
