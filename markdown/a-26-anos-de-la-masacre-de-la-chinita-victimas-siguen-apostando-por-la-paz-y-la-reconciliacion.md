Title: A 26 años de la masacre de La Chinita, víctimas siguen apostando por la paz y la reconciliación
Date: 2020-01-23 15:27
Author: CtgAdm
Category: Comunidad, Memoria
Tags: La Chinita, paz
Slug: a-26-anos-de-la-masacre-de-la-chinita-victimas-siguen-apostando-por-la-paz-y-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/La-Chinita-26-años.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo Contagio Radio {#foto-archivo-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este jueves 23 de enero se conmemoran 26 años de la Masacre de La Chinita en Apartadó, Antioquia, en la que fueron asesinadas 35 personas. Tras más de dos décadas de los hechos, las víctimas reclaman que aún faltan elementos de la verdad para asegurar la reparación de las víctimas y su derecho a la no repetición.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Los avances en la reparación**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La masacre ocurrió en 1994 a manos de integrantes de las FARC, mientras se adelantaba una verbena en el barrio obrero de Apartadó, creado en predios tomados de la finca La Chinita. Para Silvia Berrocal, víctima de la Masacre, este día es difícil de revivir y "duele todavía ese hecho". (Le puede interesar:["Las víctimas de la masacre de La Chinita sí perdonamos"](https://archivo.contagioradio.com/las-victimas-de-la-masacre-de-la-chinita-si-perdonamos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Berrocal afirma que luego de **26 años de la masacre, y la declaración del Barrio Obrero como sujeto de reparación colectiva** se han adelantado medidas de resarcimiento sociales, económicas y emocionales, pero aún faltan algunas cosas para terminar ese proceso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En primer lugar, la Unidad para las Víctimas les ha brindado asistencia emocional para tramitar su dolor, igualmente se produjo la adecuación del espacio físico de la Junta de Acción Comunal (que Berrocal integra) y se han creado clubes juveniles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero Berrocal aclara que **la reparación no ha sido integral ni ha sido para todas las víctimas**, por ejemplo, para aquellas que tras la masacre abandonaron el territorio, y en su retorno se encuentran sin una vivienda. (Le puede interesar: ["Victimas de la Chinita y FARC avanzan con paso firme hacia la reconciliación"](https://archivo.contagioradio.com/victimas-de-la-chinita-se-reconocen-hoy-como-constructores-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, en el marco del Acuerdo de Paz firmado por las FARC pidieron que se construyeran 52 viviendas, un centro de atención a los hijos e hijas de las viudas de la masacre, una guardería y que se sepa la verdad de lo ocurrido. Pero según Berrocal, esto no se ha cumplido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La líder señala que un paso importante para la reparación de todas las víctimas es saber la verdad de lo que pasó, porque **hasta ahora tienen versiones parciales o difíciles de creer**. En ese sentido, le pide a los dirigentes del ahora partido Farc que les digan qué motivó la masacre y quienes la perpetraron.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Víctimas de La Chinita apuestan por la paz y la reconciliación**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Berrocal señaló el 30 de septiembre de 2016 como una fecha importante para ellos, fue el momento en que integrantes de FARC llegaron al barrio Obrero a pedir perdón por lo ocurrido. Según explica, "nosotros los perdonamos porque queremos que ellos sigan con su vida" aunque aclara que no todas las víctimas dieron ese perdón.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ese perdón que otorgó [Berrocal](https://www.justiciaypazcolombia.com/el-sentido-de-la-voz-de-las-victimas-y-los-lideres-sociales-en-tiempos-de-construccion-de-paz/) le significó amenazas, razón por la que tuvo que abandonar el país; sin embargo, asegura que ella, las víctimas de las 17 masacres que hubo en Urabá "e incluso las de Colombia, **trabajamos para que no haya más asesinatos, estamos trabajando por la paz".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En consecuencia, expresa que para la paz es necesario desmovilizar el corazón y desistir del odio. Berrocal dice que para lograr esa paz también es necesario que se desmovilice el ELN. (Le puede interesar:["Víctimas de Bojayá y la Chinita exigen participación en pacto por la paz"](https://archivo.contagioradio.com/victimas-de-bojaya-y-la-chinita-exigen-participacion-en-pacto-por-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Que se institucionalice la conmemoración**

<!-- /wp:heading -->

<!-- wp:paragraph -->

La conmemoración iniciará sobre las 6:30 de la tarde, momento en que se realizará una misa en el cementerio de Bojayá y se dejarán ofrendas florales. Al evento están invitados representantes de diferentes instituciones, a los que las víctimas les pedirán apoyo para hacer de la conmemoración un evento institucional, y así contar con los recursos y apoyo necesarios para realizarla todos los años.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
