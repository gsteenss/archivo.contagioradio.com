Title: Mucha crítica y poco método: la filantropía intelectual
Date: 2017-07-09 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: derecha, Intelectuales
Slug: mucha-critica-y-poco-metodo-la-filantropia-intelectual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/1-de-mayo-30-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 9 Jul 2017 

Todos hablan, todos critican, muchos aplausos, muchas citas en formato APA, todos son filósofos, todos arrojan pensamientos políticos al maremágnum de las redes sociales, todos se agarran de las mechas en algún momento para luego abrazarse al calor de unas polas; es una bomba discursiva, un fulgor en el debate que da la impresión de que pronto el país estallará, no obstante no estalla… a la espera de que pase algo, pasan los años y los que tienen el poder en Colombia, los mismos que se han robado todo el dinero público, luego de subirse el sueldo y dejar listos los contratos y proyectos de ley a sus amiguitos, nos piden que les sigamos pagando hasta con las berracas bolsas de plástico mientras nos hacen sentir culpables por el daño medio ambiental.

[¿Por qué no pasa nada?  Sencillo: hay mucha crítica y poco método. En Colombia se supone que dejamos hace mucho tiempo de creer en aquellos que estan en el poder público, y entregamos las esperanzas a los grandes críticos que por lo general forman parte de un mundo académico, universitario, bohemio, y en términos vulgares: culto.]

[Esas esperanzas se consolidaron con todo este clima de paz, con todo ese rechazo a la violencia, a las armas, a las guerrillas, a los necios que ponen bombas, un rechazo de las personas a esa sucia violencia mientras luego de postear “viva la paz” o “\#fuerzavíctimasdelandino las mismas personas se ponían a ver la serie “Narcos” en Netflix amando masoquistamente cuanta apología traqueta entrega el mercado, aportando a esa posición hipócrita de rechazar un tipo de violencia, pero no toda.  ]

[Hay mucha crítica y poco método porque ser crítico se ha constituido en una especie de]*[filantropía intelectual]*[ que no permite nada más allá que tener buena parla para participar en un par de foros con intelectuales colombianos fieles al eurocentrismo, o con intelectuales extranjeros que casi siempre conocen mejor nuestro territorio, y al final, subir el certificado al CVLAC.  ]

[¿Por qué no pasa nada? Bueno, tener ideas políticas tales como “algo tiene que cambiar” “algo tiene que pasar” es en todo sentido un deseo, un deseo de liberación. Dicho deseo, que debería sublimarse en la lucha política contra el sistema opresivo, asesino y corrupto que existe en Colombia, se desvía a pasos agigantados por la canaleta de las luchas identitarias… ¿Por qué cree que no afecta de manera general la muerte de líderes sociales? No solo es por los medios masivos y su sesgado papel de jueces… es porque como la guerra… eso pasa en otro lado… ese no es mi problema…   ]

[En el caso concreto de una economía liberal o neoliberal como la colombiana, las luchas identitarias caen como anillo al dedo para quienes controlan el poder, porque orientan a que los individuos pierdan el sentido objetivo de la realidad que los oprime y queden inmersos en una falsa liberación otorgada por los mecanismos de consumo.]

[Esto no significa que la identidad sea perversa, pero el lio más tremendo es que el consumismo es el amante perfecto de las luchas políticas identitarias, si el individuo encuentra algo que lo identifique en el mercado, sea obediencia o desobediencia, pues lo consume ¡eso da igual! El individuo se siente identificado, es decir, al final: preso de una falsa liberación.]

[Mucha crítica y poco método, porque el método implicaría reconocer que es necesario abandonar las posiciones de grandes intelectuales como Foucault o actualmente Zizek quienes movilizan con sus textos a enormes masas de nuevos proletarios (es decir universitarios titulados, pero pagando todo a crédito) entregándoles majestuosos orgasmos mentales en libros muy reconocidos, pero siendo precavidos que sea sólo en las entrevistas, donde salgan afirmaciones tales como:]*[a mí no me pregunten cómo cambiar las cosas, yo solo estaba desarrollando una crítica.  ]*

[La filantropía intelectual es la que no permite que el método para el cambio político progrese, pues en las aulas todos, pero en las calles pocos.]

[Por el contrario, que lindo es un intelectual aprendiendo de campesinos, de ex combatientes o de las capas más pobres y violentadas del país.]

[Que lindo los que critican pero con su trabajo y sus actitudes demuestran que la mentalidad traqueta y provinciana (es decir individualista y codiciosa en términos de Bolívar) no los consume y así sea trabajando en una obra, de recepcionistas, de profesores, de operarios, de vendedores, de coordinadores, de creativos, rompen la imposición cultural y no permiten que la forma en que hoy es manejado el país por esos corruptos sin visión, individualistas y hasta asesinos, se convierta en la manera en que desarrollan la vida cotidiana.]

[Bueno, ¿usted que propone? (]*[ya pensará más “diuno”]*[) primero, que la filantropía intelectual sea modificada por la solidaridad intelectual, hay que recordar que regalar algo a alguien que lo necesita, es diferente a ponerse en los pies de ese alguien. Por eso el primer paso es ser solidarios, pero en serio, sin andar buscando espacio en cielo, ya tiene suficiente con la cuota que paga por su tumba. Posteriormente transformar la solidaridad en cuerpos dispuestos a ocupar el lugar a donde más se necesite.  ]

[Segundo, pensar con el cerebro y no con la barriga, no tener miedo a hablar, pero ante todo tener presente que: saber lo que se dice es mucho más importante que saber cómo se dice. Y quien determina que sí sabes lo que dices, no es la retórica ni la calidad de tu discurso, sino la acción que has hecho para que ésta cobre vida y hable por sí sola.]

[Por último, el método elemental no comienza por la pregunta ¿A cuántos autores has citado? Sino ¿a qué estás dispuesto tú para que las cosas cambien? Esa pregunta, totalmente personal e introspectiva, es la base práctica de toda metodología que esté orientada a una transformación política en Colombia.]

[Hay muchos intelectuales que quieren cambiar las cosas, y nunca pudieron cambiar la forma despótica con que tratan a sus estudiantes, a sus parejas; otros quieren tanto el cambio, que finalmente eso les dio plata, y convirtieron la pobreza en un negocio; otros quieren tanto el cambio que consiguieron novia, amante, moza y contra moza pues hicieron de sus discursos un atributo más de su sex appeal; otros quieren cambiar las cosas, pero para hacer el papel de opresor, pues como muchos masoquistas, en el fondo admiran a su opresor ¿no ve tanto guerrillo setentero que hoy es uribista?…]

[Hay muchos que quieren cambiar las cosas… pero a domicilio. Y pues, criticar sin estar dispuesto a sacrificar nada, es menos que una palabra al aire, o incluso bien escrita y publicada.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
