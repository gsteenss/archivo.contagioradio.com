Title: "Veníamos de una negación absoluta, hace 30 años ser homosexual era un delito"
Date: 2016-06-28 14:37
Category: LGBTI, Nacional
Tags: Día Internacional del Orgullo Gay, Día orgullo gay colombia, discriminación sexual
Slug: veniamos-de-una-negacion-absoluta-hace-30-anos-ser-homosexual-era-un-delito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Día-Orgullo-Gay.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [28 Junio 2016] 

"Hoy es un día con doble carácter, por un lado, de conmemoración por lo ocurrido en Stonewall, pero al mismo tiempo es un día de celebración, de expresar el orgullo contra la vergüenza, de expresar solidaridad y alegría por ser quiénes somos, pero **también es un momento para recordar que aún queda mucho trabajo**", asegura el abogado Mauricio Albarracín, a propósito de la conmemoración del Día Internacional del Orgullo Gay.

Según Albarracín en el último año han habido avances significativos en el reconocimiento de derechos para los integrantes de la comunidad LGBTI, relacionados con el [[matrimonio igualitario](https://archivo.contagioradio.com/adopcion-igualitaria-matrimonio-igualitario-german-prefeti/)], la posibilidad de adopción y la educación sexual en las instituciones educativas, que han fortalecido el movimiento. Sin embargo, todavía **persisten las prácticas violentas y de discriminación en la cotidianidad,** **en instituciones públicas y privadas como notarías y escuelas**.

"Veníamos de una negación absoluta de derechos (...) hace 30 años era delito ser homosexual, luego pasamos a un reconocimiento formal de derechos en la Constitución y **hemos tenido 25 años en los cuales a través de acciones jurídicas y judiciales hemos venido eliminando cada una de las discriminaciones** (...) eso no significa que el reconocimiento formal inmediatamente produzca reconocimientos materiales, lo que estamos es estrenando derechos (...) los retos sociales e institucionales aún son muy grandes", afirma el abogado.

Este domingo diversas organizaciones y activistas marcharán desde el Parque Nacional, bajo el eslogan 'Paz en Igualdad', con el propósito de "buscar los caminos para restaurar las desigualdades y violencias" que afectan a la comunidad LGBTI, porque "no somos ajenos al anhelo de vivir en paz". En ese sentido Albarracín concluye asegurando que **"hay que seguir construyendo la igualdad cotidianamente, allí dónde los retos son más fuertes"**, particularmente en los espacios rurales.

<iframe src="http://co.ivoox.com/es/player_ej_12055958_2_1.html?data=kpedl5qdeZmhhpywj5aUaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5ynca7V1tfWxc7Tb6Lgw8bf1MbHaaSnhqax0JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
