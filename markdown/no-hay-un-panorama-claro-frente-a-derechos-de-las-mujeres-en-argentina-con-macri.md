Title: No hay un panorama claro frente a derechos de las mujeres en Argentina con Macri
Date: 2015-11-28 09:15
Category: El mundo, Mujer
Tags: 25 de Noviembre, Argentina, Día de no Violencia contra la mujer, nadia Fink
Slug: no-hay-un-panorama-claro-frente-a-derechos-de-las-mujeres-en-argentina-con-macri
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Nadia-Fink-foto-Giulia-De-Luca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:marcha.org.ar 

###### [28 Nov 2015] 

**En Argentina las mujeres y la sociedad también se movilizan en el marco del día por la “No violencia contra la mujer”**, tema que a tenido debates muy profundos en el marco del proyecto “ni una menos”; avances en materia legislativa y en materia del cambio de estereotipos en la sociedad, **por ejemplo con la colección de cuentos de antiprincesas lanzada por Nadia Fink**.

Sin embargo **pese a estos avances los femicidios siguen siendo altísimos y pese a que se piensan leyes o anuncios grandes para mitigar el maltrato** es “muy difícil llevarlos afuera” y que se cumplan al interior de la sociedad, **Fink indica que con el nuevo gobierno de Mauricio Macri el panorama sobre los derechos de las mujeres no está claro** “con un triunfo abierto de la derecha más rancia de Latinoamérica”.

Hoy Argentina también **desarrollará distintas actividades desde las 6 de la tarde, convocadas por el Ministerio y distintas organizaciones que vienen trabajando el tema**, que pese a agresiones recibidas en distintos encuentro como en Mar de Plata, en donde aproximadamente 75.000 mujeres se inscribieron y fueron víctimas de represión policial, continúan resistiendo por sus derechos.

El día de ayer salió el **tercer texto de la colección anti heroínas que trata sobre Juana Azurduy, la heroína boliviana de las luchas de independencia** y que participó en luchas por la liberación del Río de la Plata y de esta forma desarticular otros tipos de violencia, como los estereotipos, que como afirma Nadia Fink “la formación de estereotipos, también ayudan a la violencia”.

Con esta colección Fink **busca reivindicar el papel de la mujer como parte importante de la sociedad** en donde ellas “han ocupado cargos muy importantes”, reafirma la escritora, de esta forma ayudar a “construir nuevas infancias y nuevas identidades”. Recordemos que los otros dos textos de la escritora argentina tratan sobre la artista Mexicana Frida Kahlo y la cantautora chilena Victoria Parra.
