Title: Ser Pilo Paga ha gastado más de 350 mil millones del presupuesto de educación
Date: 2017-03-01 17:15
Category: Educación, Nacional
Tags: Crisis de la Educación, ser pilo paga
Slug: ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion
Status: published

###### [Foto: Contagio Radio] 

###### [1 Mar 2017]

El programa Ser Pilo Paga ha gastado más de** 350 mil millones del presupuesto para la educación,  dinero que podría ayudar a superar la crisis de las Universidades Públicas**. Los estudiantes denuncian que con estos recursos se podría contribuir en la disminución del déficit que asciende a 1 billón de pesos.

De acuerdo con las Asociación Colombiana de Estudiantes, un estudiante de universidad pública cuesta en promedio **5 millones de pesos, mientras que un estudiante que se encuentra en el programa Ser Pilo cuesta aproximadamente 15 millones de pesos**, esto debido a que el 80% de los estudiantes que ingresan al programa eligen universidades privadas y costosas para realizar sus estudios.

Además, los estudiantes denuncian que el programa solo ofrece **10.000 becas que no son ni siquiera el 2% de la población bachiller** del país, que según las cifras del  DANE se aproxima a las 482.000 personas. Le puede interesar: ["Reforma a la educación se pasaría vía Fast Track"](https://archivo.contagioradio.com/reforma-al-sistema-de-educacion-se-pasaria-via-fast-track/)

Un informe del portal "Lanzas y Letras" evidencia que las primeras 5 universidades en donde ingresan los estudiantes beneficiados de Ser Pilo Paga son: Universidad La Salle, la Universidad Javeriana, la Universidad Los Andes, la Universidad Bolivariana y la Universidad del Norte, que a futuro terminaría **“fortaleciendo a los más fuertes y debilitando a los más débiles”,** extinguiendo la Educación Superior Pública.

Tanto estudiantes como directivas de diferentes universidades públicas, han manifestado que su interés no es que se finalice el programa Ser Pilo Paga, si no que se **destinen los recursos suficientes para sacar a las universidades públicas de la crisis en la que se encuentran** y de paso mejorar la calidad y la oferta educativa. Le puede interesar:["Miles de docentes se movilizan en defensa de la Educación"](https://archivo.contagioradio.com/miles-de-docentes-se-movilizan-en-defensa-de-la-educacion/)

![WhatsApp Image 2017-02-28 at 5.10.36 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/WhatsApp-Image-2017-02-28-at-5.10.36-PM.jpeg){.alignnone .size-full .wp-image-37085 width="668" height="264"}

###### Reciba toda la información de Contagio Radio en [[su correo]
