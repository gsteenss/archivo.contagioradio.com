Title: Superar la retórica ambiental
Date: 2018-08-31 06:00
Category: CENSAT, Opinion
Tags: ambientalistas, Ambiente, extractivismo, locomotoras, Mineria
Slug: superar-la-retorica-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/censat.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Movete 

#### **[Por: CENSAT Agua Viva - Amigos de la Tierra – Colombia](https://archivo.contagioradio.com/censat-agua-viva/)** 

###### 23 Jul 2018 

**Legados y desafíos en el cambio de gobierno**

[Casi cinco décadas han transcurrido desde que iniciara a nivel global un auge de debates, acuerdos y convenciones sobre temas ambientales. **Colombia, por ejemplo, cuenta con, cuando menos, 48 compromisos internacionales** adquiridos en función de tratados en esta materia. Sin embargo, el Instituto Von Humboldt reporta una disminución de un promedio de 18% en la biodiversidad del país y ha identificado 1200 especies en categoría de amenaza. Un diagnóstico del IDEAM establece que los procesos de desertificación afectan al 16.95% del país y de acuerdo a esta misma entidad la tasa de deforestación en 2017 fue de 219.973 ha, la mayor de los últimos seis años, siendo especialmente preocupante esta situación para la Amazonía, donde la deforestación se duplicó, representando el 65.5% de todas las selvas perdidas en el país. También el IDEAM alerta sobre la pérdida de glaciares, pues de 19 que tenía Colombia en el siglo XX, hoy apenas quedan 6, esto por poner apenas algunos ejemplos.]

[En 2018 el país afrontó tragedias ambientales sin precedentes, el derrame de petróleo en el pozo Lizama 158, la crisis social y ecológica generada por la contingencia en Hidroituango, el otro derrame de petróleo en el Sur de Bolívar, entre muchas otras cuya visibilidad no fue nacional, pero que afectan grave y duraderamente todas las formas de vida. La gestión institucional ha dejado su incapacidad como legado, pues las respuestas han sido lentas, ineficientes, y han pretendido reducir estos desastres a manifestaciones mínimas. En el caso de Lizama, al derrame de 500 barriles o en Hidroituango a imprevistos naturales que no hubieran podido ser tramitados de otra forma, desconociendo con esto la negligencia y mala praxis de los actores involucrados, incluyendo a las mismas autoridades ambientales.]

[Corroborar que el daño ecológico se intensifica, de forma paralela a la retórica de prácticas ambientales responsables y de protección ambiental indica que algo falla en el centro de las estrategias de conservación. El tratamiento y la comprensión de la cuestión ambiental -que a nuestro juicio incluye pero no se agota en la conservación, se vuelca desde los años 80 a una postura predominante, según la cual los límites físicos del planeta no son un obstáculo para el crecimiento económico, y así, la cuestión ambiental paulatinamente va pasando a ser gestionada de forma hegemónica desde tecnocracias y ecuaciones propias de la economía, puntualmente de la economía neoclásica.]

[En Colombia esta racionalidad se intensificó durante los dos períodos Santos. En el discurso del expresidente en la Sexta Plenaria de la Plataforma Intergubernamental de Biodiversidad y Servicios Ecosistémicos (IPBES) que tuvo lugar en Medellín en marzo del presente año, Santos enunció los logros en materia ambiental de sus dos períodos gubernamentales, a la vez que los justificó en la rentabilidad de la conservación y en su financiación mediante instrumentos de mercado, que incluyen los mercados de carbono. Las concepciones que subyacen a afirmaciones de este tipo contribuyen a desdibujar el trasfondo civilizatorio de la crisis ambiental y a la construcción y divulgación de un imaginario según el cual la escasez ecológica es una escasez de capital y puede ser suplida con tecnología o con reacomodos al mismo capital, es decir, con instrumentos de la llamada Economía Verde.]

[Avanzar por esta vía, una de capitalismo verde que acelera y profundiza la destrucción ambiental, pero que se precia de garantizar la protección de los ecosistemas y de la vida, a partir de ponerle precio y de incluirla en dinámicas financieras; implica repetir fórmulas para la conservación ambiental cuyo fracaso ha sido evidente, con el agravante de que la Economía Verde redirecciona los fundamentos de la protección ambiental a la acumulación de capital, y en ese sentido la motivación de la misma ya no reside en garantizar la reproducción de la vida, sino en la ampliación de los negocios, según razonamientos donde no toda la naturaleza es bienvenida, sólo aquélla que es remunerable.]

[Iniciando un nuevo gobierno, el abanico de falsas soluciones a la crisis ambiental parece enraizarse y expandirse. En su Plan de Gobierno Iván Duque presenta 11 propuestas referidas a medio ambiente. A pesar de caracterizarse por ser vagas y poco explícitas, podemos inferir dos grandes apuestas de ellas, que probablemente se asuman sin mayores modificaciones en su Plan Nacional de Desarrollo: i) Uno de los grandes protagonistas para la protección ambiental es el sector empresarial y ii) La financiarización de la naturaleza aparece como el pilar macro del esquema. (Tratamiento de la naturaleza como activo financiero, lo cual permite incluirla en mercados bursátiles a nivel mundial, o lo que es lo mismo, en las dinámicas especulativas globales)]

[El primer punto propone un fortalecimiento de la cultura ambiental a nivel empresarial, es decir, un régimen de principios voluntarios en vez de exigirle cumplimientos estrictos a las empresas en materia ambiental. Al mismo tiempo, la propuesta ambiental del gobierno incorpora actividades extractivas (específicamente minería y explotación de hidrocarburos) como un componente de esta agenda, aclarando que el desarrollo minero energético del país se adelantará con los más altos estándares de responsabilidad, a pesar de que se hace mención de una expansión del sector que incluye la promoción de actividades hidrocarburíferas]*[offshore]*[(mar adentro), siento éstas bastante polémicas por sus altos riesgos y niveles de contaminación.]

[Esta posición contribuye a normalizar de prácticas empresariales nocivas, presentando el accionar como responsable y ceñido a leyes ambientales, minimizando de esta forma los impactos reales del accionar corporativo, con lo cual sus acciones quedan en la impunidad. Acorde a lo anterior y al privilegio de las empresas como sujetos de conservación y protección ambiental, las propuestas van encaminadas en la línea de Economía Verde; específicamente, menciona la promoción de Mecanismos de Desarrollo Limpio y los mercados de carbono, así como la valorización financiera de la naturaleza como activo estratégico, dando vía libre al tratamiento de la contaminación y la conservación como negocios, con lo cual se especula con la vida y se “cuida” sólo en la medida que sea rentable.]

[Con el gobierno Duque se vienen grandes retos para los ambientalismos populares en el país. Aunado a la intensificación de la arremetida extractivista, el tratamiento neoliberal de la cuestión ambiental y la emergencia de ecocapitalismos nos exigen a tod@s quienes defendemos la vida, preguntarnos  ¿cómo nos vamos a situar frente a un tratamiento de la crisis ambiental que habla el lenguaje de la conservación pero cuyo enfoque contribuye a la profundización de la misma y a la elusión de responsabilidades?]

#### [**Leer más columnas de CENSAT Agua Viva - Amigos de la Tierra – Colombia**](https://archivo.contagioradio.com/censat-agua-viva/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radi
