Title: General Villegas reconoce asesinato de Dimar Torres mientras MinDefensa pretende ocultarlo
Date: 2019-04-28 11:48
Author: CtgAdm
Category: DDHH, Nacional
Tags: Catatumbo, Comisión de Paz, Dimar Torres, Ejecucion extrajudicial
Slug: general-villegas-reconoce-asesinato-de-dimar-torres-mientras-mindefensa-pretende-ocultarlo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Diseño-sin-título-e1556470066779.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

**El general Diego Villegas, jefe de la Fuerza de Tarea Vulcano** presente en el Catatumbo, donde fue asesinado el excombatiente de las Farc en proceso de reincorporación, **Dimar Torres** asumió la responsabilidad del caso y pidió que se investiguen los hechos. Una declaración que desmiente la versión **del ministro de Defensa, Guillermo Botero quien intentó encubrir el hecho** tras recientes declaraciones en las que indicó que Dimar murió en medio de un forcejeo.

**"Esto no debía pasar y esto no obedece a una acción militar, pero yo no soy quien para decir lo que pasó allí pero si les doy la tranquilidad de que yo tampoco estoy tranquilo con lo que paso ni voy a arropar con la cobija de la impunidad a las personas que lo hicieron, aquí cada cual responde por lo que hace"** declaró el comandante quien  en audiencia pública pidió perdón a la comunidad, cabe resaltar que Villegas también es investigado por una presunta ejecución extrajudicial.

> "No mataron a cualquier civil; mataron a un miembro de la comunidad, y lo mataron miembros de las fuerzas armadas", Brigadier General Diego Luis Villegas Muñoz, Fuerza de Tarea Vulcano, sobre homicidio de Dimar Torres. (Cortesía Periodismo Joven El Tarra). [pic.twitter.com/NIEqetSwYA](https://t.co/NIEqetSwYA)
>
> — Helena Sánchez (@helenasanchezt) [April 28, 2019](https://twitter.com/helenasanchezt/status/1122330922924290048?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Presencia de la Comisión de paz**

Asímismo, integrantes de la Comisión de Paz del Senado visitaron la región del Catatumbo donde la comunidad de la vereda Campo Alegre, tras su recorrido por el lugar, la Comisión concluyó que la muerte de Dimar fue una ejecución extrajudicial causada por un integrante del Ejército **"en acción no misional, no ordenada y fuera de la ley"** y que evidencias como la fosa hallada y los testimonios de la comunidad sugieren que existió intención de ocultamiento y desaparición del cadáver con complicidad de terceros.

> Luego de su visita ayer a la zona donde fue asesinado Dimar Torres, exmiliciano de Farc, [@ComisiondePaz](https://twitter.com/ComisiondePaz?ref_src=twsrc%5Etfw) concluye que se trata de ejecución extrajudicial, intento de desaparición, y crimen contra el Acuerdo y el proceso de paz. [pic.twitter.com/cYFWK6Ypkn](https://t.co/cYFWK6Ypkn)
>
> — Iván Cepeda Castro (@IvanCepedaCast) [April 28, 2019](https://twitter.com/IvanCepedaCast/status/1122476476492845058?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
"El asesinato de Dimar Torres a manos del Ejército Nacional constituye un crimen de Estado contra la paz, a la Fiscalía le corresponde establecer con prontitud las responsabilidades penales tanto intelectuales como materiales de estos hechos gravísimos contra la paz de Colombia" declaró el **senador Antonio Sanguino.** [(Lea también: Ejército habría torturado, asesinado e intentado desaparecer a Dimar Torres) ](https://archivo.contagioradio.com/ejercito-habria-torturado-asesinado-e-intentado-desaparecer-a-dimar-torres/)

Señalaron que las versiones sobre violación o mutilación continúan en investigación por lo que pidieron a Medicina Legal que entregue el informe oficial sobre el caso lo más pronto posible, de igual forma pidieron a la Fiscalía verificar o descartar si existen o no otras fosas con restos humanos en la zona.

 

[English Version](https://archivo.contagioradio.com/general-villegas-recognizes-the-murder-of-dimar-torres-while-the-ministry-of-defense-hides-the-truth/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
