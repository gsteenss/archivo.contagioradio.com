Title: Ordenan libertad para Lina Jiménez e Iván Ramírez víctimas del montaje judicial Caso Andino
Date: 2020-10-14 18:03
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #CasoAndino, #Fiscalía, #IvánRamírez, #LinaJiménez, #Montajesjudiciales
Slug: ordenan-libertad-para-lina-jimenez-e-ivan-ramirez-victimas-del-montaje-judicial-caso-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/lina-e-ivan.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 14 de octubre, un Juez de garantías ordenó **la libertad inmediata para Lina Jiménez e Iván Ramírez,** luego de que se cumpliera el vencimiento de términos. Tanto Jiménez como Ramírez hacen parte del grupo de 9 personas que denuncian ser víctimas del montaje judicial conocido como caso Andino.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Lina Jiménez e Iván Ramírez: más de tres años en prisión
--------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

En recientes declaraciones, el abogado defensor de Lina e Iván, Jeison Paba, señaló que hasta la fecha la Fiscalía seguía sin presentar pruebas en contra de los jóvenes. Igualmente afirmó que en este proceso se han cometido distintas arbitrariedades, entre ellas la dilación de las audiencias. (Le puede interesar: ["Caso Andino, un proceso cargado de injusticias y dilaciones"](https://archivo.contagioradio.com/caso-andino-un-proceso-cargado-de-injusticias-y-dilaciones/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, la familia de Lina Jiménez denunció las pésimas condiciones en las que se encontraba la mujer, producto de su traslado a la cárcel de Picaleña, en Ibagué. Centro de detención que no cuenta con las medidas de bioseguridad, ni salubridad para afrontar la pandemia del Covid 19, o garantizar los derechos de la población reclusa.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La libertad para las 9 personas del caso Andino
-----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado mes de junio, recuperaron la libertad César Berrera, Cristian Sandoval, **Andrés Mauricio Bohóquez Flórez, Juan Camilo Pulido Rivero, Boris Ernesto Rojas Quijano, Alejandra Méndez Molano** **y Lizeth Rodríguez**, también implicados en este hecho por la Fiscalía. (Le puede interesar: ["Libres e inocentes"](https://www.facebook.com/LibreseInocentes))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si bien se ordenó la libertad inmediata por vencimiento de términos, Lina Jiménez e Iván Ramírez están a la espera de que se efectúe el fallo. De igual forma, los abogados defensores de las 9 personas, son reiterativos en manifestar, que aunque sus apoderados se encuentran fuera de la cárcel, el proceso continúa en los estrados.

<!-- /wp:paragraph -->
