Title: La política que nos identifica
Date: 2016-03-04 13:26
Category: invitado, Opinion, superandianda
Tags: alvaro uribe velez, Donald Trump, EE.UU
Slug: la-politica-que-nos-identifica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/time.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Time 

#### **[Superandianda](https://archivo.contagioradio.com/superandianda/)- [@superandianda](https://twitter.com/Superandianda)** 

###### 4 Mar 2016 

[Si hay una expresión social que haya identificado a  la Colombia ilegal, violenta y narcotraficante, sin duda alguna, es el uribismo. El uribismo más que un pensamiento político, el cual a manera personal pongo en duda, es la expresión de esa Colombia que maltratada por la violencia, resulta agresiva y corrupta en su vivir diario.]

[Por estos días  muchos opinan sobre la campaña a la presidencia de EEUU de Donald Trump, los diferentes comentarios de los medios de comunicación colombianos, y de su público en general, son favorables en la manera que admiran una campaña que, según ellos, habla directamente y sin tapujos. Ese es el tipo de personaje que atrae al público colombiano, a pesar de no tener una propuesta profunda de cambio, vende, pero no vende ideas reales, vende escándalo, vende show; entonces deja de ser candidato para convertirse en una marca comercial que se –vende-.]

[Si echamos una mirada hacia la campaña que llevo a Álvaro Uribe Vélez a la presidencia de Colombia, en realidad el despliegue publicitario lo hicieron los medios de información, publicidad que aún sigue vigente. Le vendieron a Colombia un personaje bravo, de viejas costumbres, vulgar en su hablar, dispuesto a acabar el conflicto colombiano a juete y con los pantalones bien puestos: era el orden.]

[Es por eso que no sorprende, que aun con todo lo que enloda al uribismo, con capturas e investigaciones, existan simpatizantes dispuestos a defenderlo. Entiendo que defienden al capo de las telenovelas, a ese personaje que admiran los medios con cada grosería e inviabilidad que sus dogmáticas ideas expresan; defienden lo indefendible: al ladrón que se esconde, al asesino que huye, al muchacho que se cuela en la fila, al que se levanta de la silla con verraquera y al que se lustra los zapatos frente a la justicia.]

[Lejos quedo la estructurada oratoria. ¡Queremos figuras que representen lo que somos! los que los medios y el mercado nos dicen que somos, lo que miserablemente nos identifica: la violencia, la ilegalidad, el narcotráfico, el delito, la doble moral de las cosas, la justicia que se impone con fuerza  en los más “tontos”.]

[Si Álvaro Uribe Vélez representa esa Colombia que acomoda la justicia, que hace trampa y se justifica, que se persigna, peca y empata; Donald Trump representa ese  EEUU racista y discriminatorio.]
