Title: Con irregularidades inicia desalojo de familias en Puerto Lleras
Date: 2016-12-12 17:44
Category: DDHH, Otra Mirada
Tags: Desalojo, Familias, Meta, Puerto Lleras
Slug: desalojo-familias-puerto-lleras-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/puerto-lleras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Archivo Contagio 

##### 12 Dic 2016 

Dando cumplimiento al anuncio realizado por la inspección munipal de Puerto Lleras, municipio del Meta, este lunes con apoyo del Escudrón movil antidisturbios ESMAD, inició el desalojo de las cerca de 83 familias desplazadas que habitan desde hace más de cinco años predios rurales, por los que hoy [reclama propiedad la empresa Palma Ariari S.A](https://archivo.contagioradio.com/campesinos-desalojo-puerto-lleras-meta/).

Los primeros reportes de la comunidad dan cuenta que en las acciones **han sido derribadas 5 viviendas y han resultado afectados los cultivos de pan coger** que los pobladores tenían para su superviviencia. Acciones que desde entidades como la Defensoría del Pueblo **son consideradas como irregulares al no dar garantías a quienes resultan revictimizados por la intervención policial**.

De acuerdo con Francisco Henao, abogado de la Corporación Yira Castro, por medio de diferentes oficios enviados desde la defensa de la comunidad, se aclaraba que "**el procedimiento que ellos pensaban hacer era completamente irregular toda vez que no se tuvo en cuenta que hay familias víctimas, hay población vulnerable, adultos mayores, niños mujeres**", a lo que tanto el alcalde, el enlace de vítimas como la personería ha sido que el desalojo "va por que va"

Desde la Corporación Jurídica, continua el llamado de exigencia a las autoridades locales y nacionales para que **se garantice la vida e integridad personal de los habitantes de Puerto Lleras**, ante la falta de interes de ofrecerle alternativas a esta población vulnerable, además de cometer serias irregularidades en la identificación plena del predio para delimitar la propiedad de la empresa en cuestión y lo que correspondería a baldíos de la nación.

<iframe id="audio_14900250" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14900250_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
