Title: Cultivadores de coca llevan seis meses esperando una reunión con el Gobierno
Date: 2019-02-19 18:58
Author: AdminContagio
Category: Comunidad, Nacional
Tags: Antioquia, Ituango, Plan de Sustitución de Cultivos de Uso ilícito
Slug: cultivadores-coca-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/30391220415_b03f0f23c3_k-e1550619349489-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Coca Nasa] 

###### [18 Feb 2019] 

[El Gobierno continúa sin cumplir los compromiso adquiridos con las comunidades que accedieron al Programa Nacional Integral de Sustitución voluntario de cultivos de uso ilícito (PNIS), lo que ha producido crisis en diferentes territorios que históricamente han sostenidos su economía a base de la coca.]

**Movilizaciones en Antioquia**

Desde la noche del 12 de febrero al menos 4.500 campesinos de 36 veredas del corregimiento de Santa Rita, se desplazaron hasta el casco urbano de Ituango para expresar su inconformismo con el Gobierno Nacional pues no han sido incluidos en los programas que les permitan tener unos subsidios para reemplazar los cultivos por otro tipo de sembradíos, razón por la cual solicitaron[ la "presencia de personas de alto nivel que tengan capacidad de decisión" para darle una salida positiva a los reclamos que han manifestado.]

### **A la espera de una solución por parte del Estado** 

Los campesinos, quienes han sido enfáticos en que están de acuerdo con la erradicación siempre y cuando se aplique una sustitución lograron llegar a un acuerdo junto al Ejército y la Alcaldía de Ituango en el que **se estableció suspender la erradicación forzada que está realizando la fuerza pública** hasta el 7 de marzo, y quedaron a la espera de reunirse con representantes del Gobierno el próximo 6 de marzo.

El compromiso fue adquirido con la condición de que regresasen a sus viviendas hasta la fecha en que fue concertada la reunión con integrantes del PNIS. [Según el]**'Informe de Monitoreo de Territorios Afectados por Cultivos Ilícitos'**[ de las Naciones Unidas en Antioquia la cifra de cultivos ascendía a 13.681 hectáreas para el 2017. (Le puede interesar: ["Comunidades del Norte de Antioquia piden reanudar diálogos Gobierno-ELN"](https://archivo.contagioradio.com/comunidades-del-norte-de-antioquia-piden-reanudar-dialogos-gobierno-eln/))]

### **Cultivadores llevan 6 meses pidiendo reunión con el Gobierno** 

Actualmente hay cerca de 99 mil familias inscritas en el PNIS a nivel nacional, según el delegado de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana (COCCAM) Leider Miranda, la mayoría de estas familias han cumplido con la erradicación voluntaria, pero aún no ha llegado la asistencia técnica, ni se han desarrollado los proyectos productivos y hay retrocesos en los pagos por la sustitución.

De acuerdo a Miranda, la generalidad de los territorios focalizados para implementar el PNIS es que el Estado contrató las empresas que gestionarán la llegada del apoyo técnico para los cultivadores, pero no giró el dinero para pagar a dichos técnicos. Situación a la que se suma el incumplimiento del punto 1 del Acuerdo de Paz sobre la situación del campo colombiano, y que sigue obligando a las comunidades a sustentar su economía en la siembra de coca.

Si los incumplimientos en materia de la implementación no fueran suficientes, el delegado de la Coccam sostiene que cumplidos 6 meses desde que este Gobierno asumió el mandato, aún no se han podido reunir para trazar la hoja de ruta que se seguirá, ni para dialogar sobre mecanismos de protección para líderes de sustitución de cultivos, porque desde 2016 se contabilizan 48 líderes asesinados.

### **El Gobierno no decide, pero trasnacionales sí** 

El incumplimiento en los pagos para quienes decidieron acogerse al Plan Nacional de Sustitución ha causado graves problemas entre los habitantes de la Zona de Reserva Campesina de la Perla Amazónica, quienes derivaban su sustento económico de pequeñas plantaciones de Coca, pero según las comunidad, solo han recibido 2 pagos en 10 meses. (Le puede interesar: ["Asesinan a Luis Contreras, líder de sustitución en el Catatumbo"](https://archivo.contagioradio.com/asesinan-luis-contreras-promotor-sustitucion-de-cultivos-catatumbo/))

La grave situación ha llevado a que al interior de los procesos organizativos se generen fracturas, porque algunos miembros de la comunidad están dispuestos a aceptar opciones en las que se incluye la entrada de empresas petroleras como Vetra, que con promesas de empleo y mejores condiciones buscan realizar actividades de exploración y explotación en territorios de vocación agrícola, con las consecuencias para el ambiente y el agua que tales acciones conllevan.

<iframe id="audio_32689673" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32689673_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio](http://bit.ly/1ICYhVU)]
