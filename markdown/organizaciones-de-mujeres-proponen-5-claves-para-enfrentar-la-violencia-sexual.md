Title: Organizaciones de mujeres proponen '5 claves' para enfrentar la violencia sexual
Date: 2016-06-20 17:10
Category: Mujer, Nacional
Tags: derechos de las mujeres, FARC, Gobierno Nacional, paz, violencia sexual
Slug: organizaciones-de-mujeres-proponen-5-claves-para-enfrentar-la-violencia-sexual
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/13466178_1404328602915530_7237064495999979093_n-e1466460219821.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corporación Humanas 

###### [20 Jun 2016]

La Corporación Sisma Mujer, la Corporación Humanas y la Red Nacional de Mujeres buscan que se incluya un tratamiento diferenciado para las víctimas de violencia sexual en el marco del conflicto armado y que sea establecido en los acuerdos de paz que se negocian entre el Gobierno y la guerrilla de las FARC.

Se trata de **‘5 Claves’,** un documento donde se explica cómo debería ser integrada la problemática de la violencia sexual en los acuerdos de la Habana, **frente temas como la búsqueda de la verdad, la justicia, la reparación, la no repetición  y el cese al fuego,** que es presentado hoy como resultado de la unión de experiencias de personas y organizaciones de mujeres, feministas y víctimas sobrevivientes que han trabajado este tema.

De acuerdo con Adriana Benjumea, directora de la Corporación Humanas, “en el acuerdo de víctimas firmado el pasado 16 de diciembre, se incluyen algunas de las propuestas que realizaron las organizaciones de mujeres. Por ejemplo que **la violencia sexual no será objeto de amnistían ni indultos”,** lo cual es muy importante para que haya acceso a la justicia para las víctimas. Sin embargo, es esencial que las instituciones se fortalezcan para temas en torno al cese al fuego o la refrendación de los acuerdos, de manera que las **mujeres tengan la posibilidad de decidir libremente sin ningún tipo de amenazas por parte de grupos armados tanto legales como ilegales.**

Así mismo, asegura Benjumea, **que es necesario que haya mayores avances en torno a lo que tiene que ver con las denuncias de violencia sexual contra agentes de la fuerza pública. **Aunque se ha avanzado en los procesos contra integrantes de la guerrilla, aún hay muchas falencias y pocos avances en torno a los casos que se llevan en contra de militares y policías que representan un porcentaje importante de este tipo de delitos.

El documento además señala, que si bien inicialmente es responsabilidad del gobierno y las FARC comprometerse para que no exista este tipo de violaciones de los derechos de las mujeres, lo cierto, **es que la sociedad colombiana también debe adquirir un compromiso pues, “ha sido muy permisiva con las violencias hacia las mujeres”,** señala la directora de la Corporación Humanas, quien añade que los colombianos y colombianas “Deben comprometerse con los derechos de las mujeres, como una condición para una paz sostenible”.

[Libro Cinco Claves Final](https://es.scribd.com/doc/316277628/Libro-Cinco-Claves-Final "View Libro Cinco Claves Final on Scribd")

<iframe id="doc_25910" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/316277628/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
<iframe id="audio_11968131" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11968131_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
