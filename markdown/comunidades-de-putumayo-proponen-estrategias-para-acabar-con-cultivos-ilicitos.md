Title: A pesar de anuncios de gobierno reinicia fumigación con glifosato en el Putumayo
Date: 2015-05-11 14:12
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Alejandro Gaviria, Alejandro Ordoñez, Comisión de Derechos Humanos, Consejo Nacional de Estupefacientes, Corte Constitucional, cultivos ilícitos, Daniel Mejía, Defensoría del Pueblo, Fiscalía, Glifosato, Juan Manuel Santos, Ministerio de Agricultura, Ministerio de Salud, Oficina de Antinarcóticos, política antidrogas, Putumayo, Universidad de los Andes
Slug: comunidades-de-putumayo-proponen-estrategias-para-acabar-con-cultivos-ilicitos
Status: published

##### Foto: [luisfermartinezalvarezconcejal.blogspot.com]

###### [11 de May 2015]

<iframe src="http://www.ivoox.com/player_ek_4478993_2_1.html?data=lZmkmp6dd46ZmKiak5mJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIqdSfxcqYstrYuc7V2tSY0tfTtNDixtOYx9jYtsLoxszWw9iPtMLmwpDOxcbGpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yuri Quintero, Comisión de Derechos Humanos de la Red de Organizaciones Sociales de Putumayo.] 

Comunidades de **Galilea, La Esperanza y La Ceiba del municipio de Puerto Guzmán, Putumayo, fueron fumigadas con glifosato**, pese a la recomendación del Ministro de Salud, Alejandro Gaviria, de suspender este tipo de fumigaciones tras el reporte de la OMS  sobre los efectos cancerígenos de este químico.

Yuri Quintero, responsable de la Comisión de Derechos Humanos de la red de organizaciones sociales del departamento, aseguró que **desde el 5 de mayo la Oficina de Antinarcóticos,** hizo llegar un comunicado a las alcaldías municipales de Putumayo anunciando la **agenda de fumigaciones aéreas y erradicación manual,** lo que ya generó daños en cultivos de pan coger de las comunidades.

Según Yuri, el marco de las protestas del 2014 en ese departamento, cuando las organizaciones sociales y las comunidades se manifestaron en contra de la política antidrogas del gobierno, se había logrado consolidar una mesa de conversaciones con el Ministerio de Interior, Agricultura, Minas y Energía, Ambiente y la Alta Consejería de Humanos, y con ello, se **logró la suspensión de las fumigaciones, sin embargo desde la semana pasada se reactivó está actividad.**

De acuerdo a Yuri Quintero, la coca para las familias del Putumayo significa su fuente de supervivencia, aun así , “sabemos que el cultivo de coca ha sido una herramienta que ha jugado un papel importante en la guerra, por eso, nosotros estamos convencidos y dispuestos a hacer un programa de sustitución”, el cual ya fue creado y radicado en el Ministerio de Agricultura.

Se trata del **Plan de Desarrollo Integral Andinoamazónico,** por medio del cual se construiría un documento  concreto en el que **las comunidades se comprometen a sustituir los cultivos de coca,** siempre y cuando el gobierno de garantías a los campesinos, sin embargo, según Quintero, “el problema es que el gobierno no quiere generar las garantías claras que esto amerita, es decir, inversión real, confianza en los proyectos campesinos, herramientas para las familias, crear criterios generales en industria, vías, impulso del comercio”, para que realmente  se pueda pueden sustituir la coca.

El Ministerio de Salud, el Ministerio de Agricultura, la Defensoría del Pueblo, la Fiscalía, la Corte Constitucional, entre otros entes gubernamentales han recomendado al gobierno nacional dejar de usar el glifosato para la erradicación de cultivos ilícitos, fue así como el presidente, Juan Manuel Santos, anunció que **el próximo 14 de mayo le pedirá al Consejo Nacional de Estupefacientes, CNE**, que suspenda la aspersión con glifosato, por lo que el procurador tildó de “culipronto”, al presidente, tras su decisión.

Sin embargo, aunque el procurado, Alejandro Ordoñez, asegura que si se deja de fumigar con glifosato se “conducirá a la creación de santuarios para el narcotráfico”, para Daniel Mejía, director del Centro de Estudios sobre Seguridad y Drogas de la Universidad de los Andes, **el glifosato tiene una efectividad del 3,5 %, lo que "significa que eliminar una hectárea (de coca) con glifosato le cuesta a Estados Unidos y a Colombia 72.000 dólares".**

Por otro lado,  según Mejía, cuando  el avión pasa, se destruye el cultivo, pero los cultivadores de coca tienen semilleros listos para resembrar. Es decir, que **esta política antidrogas tiene un costo bastante alto y baja efectividad.**

Además, el experto de la Universidad de los Andes, indica que los estudios en Colombia han mostrado que las aspersiones con glifosato tienen efectos negativos **dermatológicos, respiratorios y reproductivos, al provocar pérdida de embarazos**. Así mismo, Yuri Quintero señala que las fumigaciones con este herbicida, traen como consecuencia **desplazamiento, terror, dolor y enfermedades y como cáncer,** por lo que expresa que “es indignante escuchar al procurador a Uribe al Ministro de Defensa, siendo que tenemos una propuesta, clara y consiente, estamos dispuestos a cambiar la situación del Putumayo”, dice responsable de la comisión de derechos humanos del departamento, quien agrega que la propuesta del ministro Juan Carlos Pinzón de cambiar el tipo de químico, generaría otras consecuencias. Para ella, “**el método es sustituir gradual y concertadamente el cultivo de coca”,** como lo han propuesto los pobladores.
