Title: 9na Semana por la memoria "La memoria una aliada para la paz"
Date: 2016-09-22 13:08
Category: eventos, Paz
Tags: Centro Memoria Histórica, colombia, paz, semana por la memoria
Slug: 9-semana-por-la-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Semana-de-la-memoria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Centro Nal. Memoria Histórica 

###### [22 Sep 2016] 

Con la consigna  "**La memoria una aliada para la paz**" desde este jueves y hasta el 30 de septiembre tiene lugar la **9na Semana por la Memoria,** una iniciativa liderada por el Centro Nacional de Memoria Histórica con actividades en Barrancabermeja, Barranquilla, Bogotá, Cali, Chía, Manizales, Medellín, Toribío y Zipaquira.

La [programación](http://centrodememoriahistorica.gov.co/correos/externos/Boletines-2016/19-09-16-semana-memoria-agenda/boletin2.html) incluye eventos académicos y culturales entre los que se encuentran obras de teatro, exposiciones y foros que inician el 22 desde las 5 p.m. con el panel: "**El acuerdo de paz con las FARC y el papel de la educación**" en el marco del Foro "La construcción de la paz: retos de la educación", en el Auditorio Alfonso López Pumarejo de la Universidad Nacional de Colombia, sede Bogotá.

El lunes 26,  se realizará el evento "**Bogotá se viste de Blanco**", con un concierto en la Plaza de Bolívar y la transmisión en directo de la firma día del Acuerdo que pone fin al conflicto colombiano desde la ciudad de Cartagena, proyección que también se verá en el  Auditorio, Museo Casa de la Memoria de Medellín seguida por el encuentro "Conversemos sobre los acuerdos, sesiones de lectura y reflexión sobre los acuerdos de paz.

Otra de las apuestas de la semana por la memoria, tendrá lugar el jueves 29 de septiembre, con el **Canto colectivo por la paz y la memoria**, haciendo propia las historias de resistencia de las víctimas en Colombia, evento que convoca en las ciudades participantes a unirse en la distancia a una sola voz, en diferentes espacios de las ciudades, y la presentación de dos obras teatrales: en Barranquilla "**Memoria en escena**" una apuesta artistica de la Red Juvenil del Suroccidente de Barranquilla que incluye diferentes relatos de lucha y resistencia al conflicto armado, y en Bogotá  "**Casa sin ventanas - Memoria interrumpida**", del Grupo Artístico Institucional de Teatro Experimental de la Universidad Nacional de Colombia.

Durante algunos de los días que dura el evento, estarán en exhibición diferentes exposiciones artisticas entre las que destacan la **Minga muralista del Cauc**a. Los colores de la memoria y de la resistencia, en Toribio, Cauca, el **Foto museo "Memoria viva de la alta montaña"** del Carmen de Bolívar, que se presentará en la Universidad de Bogotá Jorge Tadeo Lozano, y **"Volver la mirada**" en el Centro de Memoria, Paz y Reconciliación.

Todas las actividades que hacen parte de la 9na Semana por la memoria, son de entrada libre y gratuita, para más información consulte en [centrodememoriahistorica.gov.<wbr></wbr>co](http://centrodememoriahistorica.gov.co/)

<iframe src="https://www.youtube.com/embed/ADBqX3_BJoc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
