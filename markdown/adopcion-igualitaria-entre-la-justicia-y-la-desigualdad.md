Title: Adopción igualitaria, entre la justicia y la desigualdad
Date: 2015-11-06 18:47
Category: closet, LGBTI
Tags: Adopción igualitaria, Adopción Igualitaria en Colombia, Ana Leiderman, César Avella, Corte Constitucional, Derechos Humanos, Diversidad, Equidad, Familias homoparentales, Igualdad, LGBTI, matrimonio igualitario, Verónica Botero
Slug: adopcion-igualitaria-entre-la-justicia-y-la-desigualdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/adopc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: elfichero.com 

###### <iframe src="http://www.ivoox.com/player_ek_6735558_2_1.html?data=l5ygl5qZfI6ZmKiakpaJd6Kpmoqgo5iVcYarpJKfj4qbh46kjoqkpZKUcYarpJKuxtTUp8qZpJiSpJjSb8njztTdw9fJstXVzZCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Programa 9 de marzo] 

El matrimonio y la adopción igualitaria se han convertido en dos temas controversiales en Colombia; los términos legales y la moral son los dos factores principales de discusión que han llevado a pasar por alto los derechos de los homosexuales, esto, llevó a Ana Leiderman y Verónica Botero a imponer una tutela exigiendo su derecho a la adopción conjunta.

Clóset Abierto abre el debate sobre el derecho de los niños a tener una familia y el derecho de los homosexuales a conformarla; César Avella, abogado defensor de DDHH y docente de la universidad Nacional de Colombia, habla acerca del fallo de la Corte Constitucional a favor de la adopción conjunta, establecida bajo ciertas condiciones, que el niño sea hijo biológico de alguna de las dos partes y haber convivido mínimo dos años como pareja son las más importantes.

Partiendo de los derechos fundamentales del niño, se exponen estudios psicológicos y científicos que comprueban que la crianza por parte de parejas del mismo género, no afecta en ninguna medida su salud física y mental o su desarrollo integral; el análisis del fallo nos demuestra que a pesar de aprobar solo un factor de la adopción, Colombia está avanzando en materia de derechos y hacia la igualdad en términos legislativos.
