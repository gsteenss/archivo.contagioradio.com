Title: ONU: legalizacion de "devoluciones en caliente de inmigrantes" es una violación de DDHH por parte de España
Date: 2015-03-16 21:36
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: España legaliza las devoluciones en caliente de inmigrantes, inmigracion España, Valla de la vergüenza
Slug: onu-legalizacion-de-devoluciones-en-caliente-de-inmigrantes-es-una-violacion-de-ddhh-por-parte-de-espana
Status: published

###### Foto:Comunicacionestian.com 

El jueves pasado fue **aprobada en España la enmienda de la ley de seguridad ciudadana que legaliza las "devoluciones en caliente" de inmigrantes** que hayan traspasador la frontera.

A pesar de las fuertes críticas vertidas por el ACNUR, la CE, y organizaciones de DDHH, sobre la violación de derechos humanos, en este caso de las personas migrantes, que supone dicha ley el gobierno conservador del PP ha aprobado la enmienda con mayoría absoluta y el rechazo de todos los demás grupos parlamentarios.

La **ambigüedad** surgida ha raíz de la enmienda en el momento en el que las fuerzas de seguridad expulsan ha un inmigrante que ha cruzado la frontera ilegalmente, es la principal causa por la que se **violan sus derechos como persona migrante**.

En esta caso es el **derecho de asilo el que no se respeta** al no permitir identificar a la persona y que pueda tener acceso a un abogado como así lo indican los derechos universales de las personas migrantes.

En declaraciones del **Comisario de la UE Nils Muiznieks: "Puede abrir una brecha en las garantías por las que la comunidad internacional ha luchado duramente desde la II Guerra Mundial".**

Para el **ACNUR** el estado español debe realizar un protocolo para las fuerzas de seguridad donde explique como deben de ser expulsadas las personas que hayan saltado la valla, porque de no ser identificadas y atendidas con abogado, **no se estaría respetando las leyes internacionales**. Por ello el Alto Comisionado **ha pedido al gobierno que retire la ley**.
