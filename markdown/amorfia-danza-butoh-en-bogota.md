Title: Amorfia, danza Butoh en Bogotá
Date: 2016-09-01 16:03
Category: Cultura, eventos
Tags: Danza Butoh, teatro la candelaria, zajana danza
Slug: amorfia-danza-butoh-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/11705179_859293777474964_1582610593031187802_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Zajana Danza 

##### 1 Sep 2016 

Del 1 al 3 de Septiembre, la compañía **Zajana Danza** presenta en el Teatro La Candelaria de Bogotá "**Amorfia**" una pequeña temporada de danza Butoh, que busca resolver mediante la experimentación,  algunas preguntas humanas sobre la naturaleza de las sombras.

**Gloria Helena Ramírez**, practicante de las Artes Marciales, la danza Butoh, la sanación y la investigación y **Xiomara Helena Navarro** bailarina colombiana, terapeuta de shiatsu y Especialista en Ejercicio Físico para la Salud, son las encargadas de dirigir e interpretar el performance configurado para realizarse durante poco más de una hora.

Parafraseando a Junichiro Tanizaki en el ***Elogio de la Sombra***: La obra no esta hecha para ser vista de una sola vez, en un lugar iluminado, sino para ser adivinada en algún lugar oscuro, en medio de una luz difusa que por instantes va revelando uno u otro detalle, de tal manera que constantemente esta oculta en la sombra, suscitando resonancias inexpresables.

<iframe src="https://www.youtube.com/embed/hFxaRGb_QO8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La danza Butoh, surge en 1959 como una **resistencia ante la inminente occidentalización** del Japón, asalto estético y conceptual a los patrones del arte desde una perspectiva que solo podía surgir de la experiencia de la muerte y desde la mas pura visión oriental que integra cuerpo y mente para la danza.

Ante la fortuna de impregnarnos de fragmentos de esta experiencia humana de mano de importantes maestros y por su consejo, continuamos abriendo a nuestra manera las puertas de este extenso territorio.

Amorfia, se presentará durante el primer fin de semana del septiembre a partir de las 7:30 de la noche en el Teatro La Candelaria de Bogotá.

![Danza Butoh \_AMORFIA\_](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Danza-Butoh-_AMORFIA_.jpg){.aligncenter .size-full .wp-image-28706 width="1000" height="1000"}
