Title: Paramilitares impiden el levantamiento del cuerpo de Edier Lopera tras 8 días de su asesinato
Date: 2020-06-24 11:05
Author: PracticasCR
Category: Actualidad, Líderes sociales
Tags: Alertas Temprana, Edier Lopera, paramilitares, PNIS
Slug: paramilitares-impiden-el-levantamiento-del-cuerpo-de-edier-lopera-tras-8-dias-de-su-asesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Los-Caparrapos.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Edier-Lopera.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Edier-Lopera.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Edier-Lopera-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Edier Lopera / ASOCBAC

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El pasado lunes 15 de junio fue asesinado el líder campesino Edier Adán Lopera** por un grupo paramilitar aún no identificado, en la vereda Caracolí, municipio de Tarazá, Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El grupo que perpetró el hecho se niega a permitir el retiro del cuerpo de Edier Lopera por parte de sus familiares y amigos, tras ocho días de su asesinato **«dejando un fuerte mensaje criminal de poder y terror para toda la región del Bajo Cauca que dominan»** según la Asociación Campesina del Bajo Cauca ASOCBAC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mismo sentido afirma Oscar Yesid Zapata, integrante del Nodo Antioquia de la Coordinación Colombia Europa Estados Unidos, que: **«El cadáver se deja expuesto para  que la gente sepa que el que habla se muere»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Edier Lopera **se desempeñaba como **Coordinador del Comité de Conciliación de la Junta de Acción Comunal de la vereda Urales, municipio de Tarazá y era beneficiario del Programa Nacional Voluntario de Cultivos de Uso Ilícito** y miembro de **ASOCBAC** perteneciente al Movimiento Político Marcha Patriótica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El lamentable hecho ocurrió pese a las **Alertas Tempranas** **AT N° 020–19** y  **ATI 027-18** que había emitido la Defensoría del Pueblo con base en las denuncias realizadas por organizaciones y población civil el pasado **2 de junio**, advirtiendo la presencia de actores paramilitares que patrullan las veredas Urales, La Ilusión, El Guáimaro y el Cañón de Iglesias del municipio de Tarazá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En la zona operan grupos armados al margen de la ley como las Autodefensas Gaitanistas de Colombia (AGC), el grupo armado Los Caparrapos y disidencias de las FARC**, los cuales se disputan el oro y las rutas del narcotráfico, dejando a la población civil en el medio. (Le puede interesar: [Grupos ilegales se disputan el Bajo Cauca Antioqueño ante la ausencia del Estado](https://archivo.contagioradio.com/grupos-ilegales-se-disputan-el-bajo-cauca-antioqueno-ante-la-ausencia-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### Presencia estatal a medias {#presencia-estatal-a-medias .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Pese a la fuerte presencia del Ejército y la Fuerza Pública en la zona, ya que **el Bajo Cauca es la zona más militarizada del departamento de Antioquia con por lo menos 5.000 hombres de Fuerza de Tarea Conjunta Aquiles y 2.000 más de la operación Agamenón 2** —según voceros de [CCEEU](https://coeuropa.org.co/158/) (Nodo Antioquia) — los hechos victimizantes en contra de la población civil siguen ocurriendo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Miembros de organizaciones han denunciado **nexos de algunos integrantes de las Fuerzas del Estado con estructuras paramilitares** **«la Fuerza Pública no ha querido depurar ni investigar quiénes son las personas que trabajan para estas estructuras»** denunció Óscar Yesid Zapata, integrante de CCEEU (nodo Antioquia). El defensor de DD.HH. manifestó, además, que la clave está en identificar la facilidad con la que se movilizan estas agrupaciones al margen de la ley en la zona, pese a la gran presencia de efectivos del Ejército Nacional. (Lea también: [Fuerza de Tarea Aquiles no evitó desplazamiento de más 120 familias en Bajo Cauca](https://archivo.contagioradio.com/fuerza-de-tarea-aquiles-no-evito-desplazamiento-de-mas-120-familias-en-bajo-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Zapata también señaló que la política del Gobierno Nacional encabezada por Iván Duque no deja de ser una simple «retorica que no brinda seguridad a las comunidades campesinas». (Le puede interesar: [Retórica de Duque no soluciona la crisis humanitaria: habitantes de Tarazá](https://archivo.contagioradio.com/retorica-de-duque-no-soluciona-la-crisis-humanitaria-habitantes-de-taraza/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### El asesinato de Edier Lopera, un reflejo de la crisis en el Bajo Cauca {#el-asesinato-de-edier-lopera-un-reflejo-de-la-crisis-en-el-bajo-cauca .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este lamentable hecho se suma al asesinato del también líder social Humberto Londoño y a los desplazamientos de cientos de familias que han propiciado los conflictos entre grupos ilegales en la misma zona del Bajo Cauca Antioqueño. (Le puede interesar: [El líder social Humberto Londoño, es asesinado en Tarazá Bajo Cauca](https://archivo.contagioradio.com/el-lider-social-humberto-londono-es-asesinado-en-taraza-bajo-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con este crimen, ya son **93 los líderes sociales asesinados en Antioquia** **después de la firma del Acuerdo de Paz** según el Observatorio de Derechos Humanos de la Fundación Sumapaz y la Corporación Jurídica Libertad. 14 de ellos asesinados en el presente año 2020 según CCEEU.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### Cuerpo de Edier Lopera rescatado luego de más de 8 días de su asesinato {#cuerpo-de-edier-lopera-rescatado-luego-de-más-de-8-días-de-su-asesinato .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El día miércoles 24 de junio en horas de la noche, la Gobernación de Antioquia dio a conocer que **miembros del Ejército y la Policía Nacional recuperaron, en la vereda Caracolí del municipio de Tarazá, el cuerpo del líder Edier Lopera, tras más de ocho días de su asesinato.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Jorge Ignacio Castaño, Secretario de Gobierno (e) de Antioquia **«**las condiciones geográficas y de seguridad en el terreno impidieron adelantar el rescate con mayor celeridad****»****.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https:\/\/twitter.com\/GobAntioquia\/status\/1275965372714438657","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/GobAntioquia/status/1275965372714438657

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
