Title: Balance en salud: Colombia continuó enferma durante el 2015
Date: 2015-12-30 15:57
Category: DDHH, Otra Mirada
Tags: 2015, crisis de la salud
Slug: balance-de-la-salud-en-colombia-continuo-enferma-durante-el-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/SALUD.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 30 Dic 2015 

<iframe src="http://www.ivoox.com/player_ek_9927125_2_1.html?data=mp6fmZaWeY6ZmKialZWJd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhsLgwtPQx4qXhYzAwpDgw9HZqIzZz5Cw0dHTscPdwpDQ0dPYrc%2Fphqigh6eXb8bix8rfz8aPqNbmwtPhx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

No una, ni dos, ni tres veces, el **Ministro de Salud, Alejandro Gaviria** aseguró que en Colombia no existe una crisis de la salud, pero lo cierto, es que la tal **[crisis de la salud](https://archivo.contagioradio.com/?s=salud) si existe**, y no se la han inventado los pacientes, enfermeros, médicos estudiantes y otros funcionarios que repetidamente durante el 2015 salieron a las calles a exigir este derecho.

En enero, el senador del Polo Democrático Alternativo, Jorge Robledo, denunció que el presidente Juan Manuel Santos y el Ministro Gaviria, habían aprobado una Ley Estatutaria de Salud, contraria a los derechos de los ciudadanos y que dejaba sin dientes a la tutela, tratándose  más de una ley en favor de las EPS.

Cuando la ley pasó a manos de la Corte Constitucional, se le realizaron profundos cambios y se “le quito varias cosas que el ministro le había introducido”, como lo  dijo el congresista, quien afirmó que la Corte logró mantener la tutela y los derechos ciudadanos.

Pese a las demoras, Santos firmó la Ley  el 16 de febrero, y en septiembre, la Sala Plena de la Corte Constitucional declaró que la Ley Estatutaria de Salud, que tiene por “objeto garantizar el derecho fundamental a la salud, regularlo y establecer los mecanismos de protección”, no tiene vicios de forma ni de fondo.

Entre los lineamientos más importantes que se estipularon en esta reforma, está el control de precios sobre los medicamentos; se establece el concepto de Integralidad; se elimina el Plan Obligatorio de Salud (POS); no existirán regiones desatendidas y la tutela se mantendrá como mecanismo para hacer valer los derechos de salud de los colombianos y las colombianas.

Pero lo cierto es que de acuerdo a diferentes sectores que defienden el derecho a la salud, el Plan Nacional de Desarrollo, (PND) 2014 – 2018, aprobado este año, no es compatible con la Ley, lo que pondría en riesgo el estricto cumplimiento que requiere.

Para Clemencia Mayorga, vocera de la Mesa Nacional por el Derecho a la Salud, el PND y la Ley Estatutaria deben “ir en la misma vía”, además cabe resaltar que la “Corte Constitucional dijo que** el derecho a la salud no está supeditado a sostenibilidad fiscal”,** por lo que es crucial que para que la Ley se cumpla, "los colombianos deben empezar a exigir su derecho a la salud”, ya que a la fecha no se ha implementado.

Pese a la firma de la Ley estatutaria, el martes 24 de febrero Camila Abuabara, la joven bumanguesa de 25 años de edad, murió tras seis años de resistir a un cáncer de médula ósea, que fue acabañado con la vida de una mujer que no solo tuvo que luchar contra su enfermedad, sino además, con el sistema de salud colombiano.

El caso de Camila, sin embargo, no es excepcional. Ocurre en nuestro país con más regularidad de la señalada por los medios de información. La lucha de Camila durante 6 años, y la de miles de colombianos y colombianas, ha sido contra un sistema de salud que les ha negado la posibilidad de atender sus enfermedad de manera oportuna, y la posibilidad de evitar -en muchos casos- que las personas pierdas su calidad de vida o mueran.

Para el ex-Secretario de Salud y profesor de la Universidad Javeriana, Doctor Román Vega, en el caso de Camila, la EPS no respondió de manera oportuna a las necesidades de tratamiento, y esto redujo drásticamente sus posibilidades de vivir. Incluso la opción tardía de tratarla en los Estados Unidos fue negada por la EPS Sanitas, y solo le fue transplantada la médula ósea que podría salvarla, cuando una sentencia de la Corte Constitucional obligó a la Entidad a efectuarla hace sólo un par de meses, dando como resultado el desenlace que hoy se conoce.

El problema radica, según lo señalado por el Doctor Vega, en que el sistema de salud está concebido en una lógica de mercado, en que las EPS obtienen rentas al reducir el gasto en la atención en salud, poniendo todo tipo de barreras, como el control a las prescripciones médicas y al tratamiento de enfermedades.

"Es la contradicción entre el valor de uso del derecho a la salud, y el valor de cambio que quieren generar los intereses privados para obtener ganancias. Es el modelo capitalista en la salud", declaró.

El cáncer de leucemia linfática, como el que padecía Camila, es una de las enfermedades que aqueja a niños y niñas en Colombia quienes solo tienen un 51% de posibilidades de sobrevivir, Además,  las demoras de las EPS para atender a los menores de edad que viven con esta enfermedad  son la principal causa de muerte de  los infantes en el país como lo concluyó un estudio realizado por el Instituto Nacional de Cancerología y la Universidad de Harvard.

Así como Camila, miles de colombianos y colombianas se vieron obligados a interponer acciones de tutela, para que de parte de las EPS se brindaran los procedimientos necesarios para mantener su salud. El más reciente informe sobre la salud en Colombia que presentó la Defensoría del Pueblo demuestran que las tutelas han aumentado considerablemente de 115.000 a 118.000 en tan solo el último año, con solicitudes para tratamientos, medicamentos y concesión de citas médicas, es decir, más de 360 tutelas diarias para que se garantice este derecho.

Pero el caso de Camila, fue tan solo uno de los desafortunados ejemplos que evidenciaron este año, la profunda crisis de la salud en Colombia. Otro de ellos, es el Hospital Universitario del Valle, que desde mediados del 2015 empezó a ser noticia por las múltiples movilizaciones que se realizaron para exigir que el Estado y específicamente el Ministerio de Salud le presenten atención a la situación en la que se encuentra este hospital.

En julio de este año,  cerca de 3 mil trabajadores y trabajadoras del Hospital completaban 1 semana de movilizaciones y denuncias sobre la grave situación financiera que vive el único centro de salud de tercer nivel del sur occidente colombiano.

Este año se vio agravada la situación del hospital, como consecuencia de una deuda que haciende a los 5.2 billones de pesos. Sólo EPS como Coomeva, Caprecom y Coosalud, le deben al hospital cerca de 120 mil millones de pesos. La consecuencia inmediata es que a los trabajadores con contrato de nómina del deben 2 meses de salario, mientras a los médicos generales que contratan por cooperativa les deben casi 3.

Después de la jornada de movilizaciones y plantones que se hicieron en Bogotá por parte de los estudiantes del la Universidad del Valle exigiendo al ministro de salud, Alejandro Gaviria, el rescate del único hospital de tercer nivel del Valle del Cauca, se  concretó un acuerdo de cinco puntos para salvar la entidad. Sin embargo, a la fecha, nada de eso se ha materializado y el hospital continúa en crisis, tanto que en diciembre, ya habían renunciado un total de 58 médicos generales, sin contar con que en lo corrido del 2015, el centro hospitalario se quedó sin especialistas, como lo denunció este año el doctor Julián Mora, vocero de los médicos generales en el Hospital Universitario del Valle.

Y es que los trabajadores de este sector se enfrentan a múltiples violaciones a su derecho al trabajo como lo evidencia el médico general del HUV, quien asegura que los problemas de los médicos tienen que ver con dos aspectos fundamentales: el primero orbita sobre el aspecto laboral y contractual: según Mora, hay 3 sentencias de la Corte Constitucional, un decreto presidencial y una circular de la Procuraduría que establecen un marco jurídico en donde se dice que todo empleado de un hospital público debe ser contratado de manera directa. “Pero lo que pasa en  el el HUV es que tienen un modelo de contratación a través de falsos sindicatos que hacen contrataciones sin ningún tipo de control por el Ministerio del Trabajo”. Además, se denuncia que bajo la modalidad de la tercerización laboral, los médicos están cotizando EPS sobre un Salario Mínimo Legal Vigente y carecen de prestaciones sociales.

**Los retos** 
-------------

Para este 2016, vienen grandes retos, empezando por la capital del país, donde el pasado  11 de febrero en un acto público, el alcalde Gustavo Petro revivió el Hospital San Juan de Dios, dando inicio al ambicioso proyecto que busca devolverle la vida a este emblemático centro. Sin embargo preocupa la poca claridad que la administración entrante manifiesta sobre el destino del Hospital, cuyo cierre se dio en el proceso de privatización del sistema de salud, iniciado en 1978, cuando el gobierno de Turbay convirtió al hospital en una Fundación privada, en manos del Ministro de Salud, y finalizó al implementarse la Ley 100 de 1993, que privatizó el derecho a la salud.

Para el doctor Mario Hernández, vocero de la Federación Médica Colombiana, el San Juan de Dios "se cerró porque no logró entrar en esta lógica de competencia de la Ley 100. Se dejó morir allí porque no tenía como sobrevivir", pero ahora le quedan grandes retos a los capitalinos y alcalde electo en materia de salud.

Los retos para logran llevar a cabo la reapertura del Hospital en los términos planteados por el proyecto de la Universidad Nacional, requieren **coherencia por parte del Estado colombiano, y su financiación a través de un Plan Nacional de Desarrollo** que no profundice el aseguramiento y la intermediación financiera como lógica de bien estar social.

Según Hernandez, **el proyecto también requiere que la sociedad colombiana en su conjunto se piense y re-apropie de las instituciones que son patrimonio** de las y los colombianos, y constituyen elementos fundamentales de la identidad.

En definitiva a salud es uno de las grandes problemáticas que afecta al país, siendo este un momento en el que atraviesa por una de sus crisis más profundas que se podría definir en tres líneas: 1. Una crisis Financiera que afecta a la Red Pública de Hospitales de Bogotá (ESEs) 2. Una crisis de prestación de servicios que afecta a la población 3. Una crisis laboral por la tercerización y pauperización de los trabajadores del sector, que a su vez significan retos para la sociedad colombiana.

A su vez, esto se traduce en la necesidad de que las colombinas y colombianos se movilicen aun más para el 2016, exigiendo un derecho que ya está pago.

De 18, 8 billones de pesos a 20, 8 billones paso el presupuesto de la salud para el 2016, Pero la crisis de la salud va mucho más allá, el problema no solo es financiero, es necesaria la prevención de enfermedades.

Así mismo, garantizar el derecho a la salud, es además, uno de los grandes retos en materia social, para el 2016 cuando se espera que se firmen los acuerdos de paz en la habana. Todos estos retos, concluyen en uno solo, que para el 2016,  la salud deje de verse como un negocio, como un bien que se vende y se compra, pues la salud es un derechos  unido a otros al de la educación, el trabajo y la vivienda, significan la garantía de todos y todas podamos gozar de una vida digna.

[![1440001462 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/1440001462-1.jpg){.aligncenter .size-full .wp-image-12376 width="1816" height="2532"}](https://archivo.contagioradio.com/en-colombia-la-salud-se-encuentra-en-cuidados-intensivos/1440001462-1/)
