Title: Carta abierta a Piedad Córdoba
Date: 2016-08-16 09:27
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: piedad cordoba, procuraduria
Slug: carta-abierta-a-piedad-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Piedad-Cordoba-e1471357042411.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: poder 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 16 Ago 2016

[Un saludo enorme reciba usted señora Piedad. Las ganas de escribirle esta carta tienen asidero en una razón muy precisa, muy política, pero no puedo negarle que también subyacen profundamente en el bello recuerdo de observar a una universidad paralizada escuchando su discurso, analizando sus propuestas… y con “una Universidad” no me refiero exclusivamente al personal académico, sino que el recuerdo es bello precisamente porque, docentes, estudiantes, técnicos, vigilantes, señores y señoras de servicios generales, absolutamente todos, escuchábamos atentamente un discurso que usted más que nadie sabe lo que cuesta mantener y promover en un país con una concentración de poder, tácita y mediática tan impresionante.]

[Señora Piedad, a propósito de la decisión emitida por el consejo de Estado, no puedo decirle “bienvenida a la política de nuevo” porque eso sería negar el gran trabajo que ha realizado durante estos seis años con los sectores comunitarios y políticos menos escuchados, más olvidados, más reprimidos de este país. La verdad siento vergüenza por lo que hicieron los peces gordos de la información con usted, enfatizados en difamarla, en crear una mala imagen suya; hoy pocos lo recuerdan, pero era como si les picara algo, como si en el fondo, los grandes poderes de este país, supieran la figura “tan peligrosa” que usted podía ser para ellos… es más, aún después de seis años de injusta inhabilidad, todavía amplios sectores de la clase media repiten las editoriales ponzoñosas de tiempo atrás, siguen infestados de un odio pensado desde el poder, y redactado en editoriales de manera sistemática contra su persona; hoy, imagino que esos grandes medios estarán planteando una nueva estrategia, eso sí… depende de lo que decida hacer.  ]

[Señora Piedad, para nadie es un secreto que su lucha política en estos últimos seis años ha estado de la mano de los movimientos sociales y políticos, de las comunidades, de aquellos que no salen a menudo por los grandes medios. Para nadie es un secreto que ahora, ante la decisión del consejo de Estado, los partidos políticos, los grupos de poder político, los tramitantes de votos deben estar endulzando sus oídos, buscando su “capital político”, buscando beneficiarse, buscando reintegrarla de nuevo a un círculo vicioso que usted no necesita, porque usted demostró que se podía hacer política sin partidos, de la mano de los movimientos de base.  ]

[Señora Piedad, su figura política, es la piedra angular de un proceso histórico en el cual, si lo miramos objetivamente, la vislumbra a usted como la única líder con el carácter, el peso, la experiencia y el discurso capaz, de hacer sentir a los colombianos y colombianas que en verdad hay razones de peso para creer en un nuevo país. La memoria histórica no se puede borrar, usted lo ha dicho en varias ocasiones, “esto no es un Estado fallido, es un Estado inviable”, así, sin pelos en la lengua, fue la más fuerte opositora del discurso uribista cuando aquí muchos sufrían de una cosa llamada miedo, usted no lo tuvo; como nadie, fue la única que advirtió, promovió y admiró el proceso desarrollado en la primera década del siglo por Hugo Chávez; como nadie, fue la única que se puso la camiseta con el tema de las liberaciones de secuestrados; como nadie, enfrentó al poder de los medios… eso, señora Piedad, créame que es algo que muchísima gente lo reconoce.   ]

[Por todo lo anterior, el propósito de esta carta, no es otro distinto al de solicitarle abiertamente que confíe hoy más que nunca en sus atributos como mujer de Estado, y logre paulatinamente, con dedicación y esfuerzo demostrar que la gente más olvidada de este país merece una líder de sus características. Es verdad que la democracia, la paz, el bienestar social son una responsabilidad de todos y de todas, pero usted más que nadie sabe que sin una lideresa que emerja auténtica, la gente volverá a la desesperanza de siempre, a votar por lo que los grandes partidos decidan y ofrezcan en forma de “democracia” y esperar lo que digan los grandes medios para sí acaso ver cuál es el menos peor y votar sin conciencia política.  ]

[La gente está cansada de lo mismo, eso creo que usted lo sabe de sobra señora Piedad, la gente necesita esperanza en que las cosas en verdad pueden cambiar, y el principio del cambio, es la emergencia de una mujer, una lideresa política como usted, que asuma la carrera histórica por la presidencia sin depender de partidos políticos tradicionales, o con nombres nuevos, pero con la misma propuesta que todos.  ]

[De ser atrevida, lo logrará… sabemos bien, que la historia está protagonizada por locos y atrevidos; permítame cerrar esta carta, diciéndole, nada más que con la esperanza en mi cabeza, que un movimiento social y político unificado, alejado de las formas tradicionales de hacer política en Colombia, trabajando por un nuevo país, liderado por su persona, tendrá amplias probabilidades de hacer historia.]

[Mis mejores deseos señora Piedad, mucha gente estamos a la espera de su siguiente paso.]

[Atentamente,]

[**Johan Mendoza Torres.  **   ]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
