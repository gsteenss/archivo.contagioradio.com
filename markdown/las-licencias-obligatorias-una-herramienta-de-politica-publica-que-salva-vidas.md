Title: Las licencias obligatorias, una herramienta de política pública que salva vidas
Date: 2018-11-10 07:00
Author: AdminContagio
Category: Mision Salud, Opinion
Tags: Acceso a los medicamentos, Licencias Obligatorias, Medicamentos, Medicina
Slug: las-licencias-obligatorias-una-herramienta-de-politica-publica-que-salva-vidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/medicamentos-genéricos-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Licencias Obligatorias 

###### 10 Nov 2011 

#### **Por [ Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

En salud se conoce como licencia obligatoria la autorización que da un gobierno a un particular para producir o importar y comercializar temporalmente versiones genéricas de un medicamento patentado, sin el consentimiento del titular de la patente, pagando a éste una regalía. Su finalidad es salvaguardar la salud pública de los efectos de las patentes sobre los precios de los medicamentos y el acceso a estos bienes esenciales. El titular de la patente conserva sus derechos exclusivos, excepto en relación con el licenciatario.

[La concesión de licencias obligatorias es un derecho de los Estados, consagrado expresamente en el Acuerdo sobre los ADPIC (1994) y la Declaración de Doha (2001). El ADPIC reglamenta su ejercicio en su artículo 31 y la Declaración de Doha, tras reconocer los efectos negativos de la propiedad intelectual sobre los precios y el acceso (puntos 1 a 4), reafirma el derecho de los Estados de conceder licencias obligatorias (punto 5b), de determinar las bases sobre las cuales éstas se conceden”] [(punto 5b) y de ejercer este derecho]*[“al máximo”]*[(punto 4, segundo párrafo).]

[En este contexto, las Licencias Obligatorias no son una mera expectativa sino un derecho emanado del Sistema de Propiedad Intelectual (SPI), al igual que los denominados Derechos de Propiedad Intelectual (DPI). Diríase que el SPI presenta dos facetas diferentes pero relacionadas entre sí: de un lado regula la protección de los DPI a favor de quienes asumen la responsabilidad de innovación farmacéutica, y del otro concibe unos mecanismos, entre ellos las Licencias Obligatorias, que permiten equilibrar los intereses de estos últimos con los intereses supremos de la sociedad.  ]

[Con una aclaración relevante: como puede leerse en el ADPIC y en la Declaración de Doha,]**ambos estatutos subordinan la primera faceta a la segunda, es decir,** **la protección de la propiedad intelectual a la protección de la salud pública**[, no sólo por tratarse de un derecho de jerarquía superior sino porque tal subordinación es indispensable para que el Estado pueda cumplir las obligaciones inherentes al derecho fundamental a la salud, como son, de acuerdo con el Pacto Internacional de Derechos Económicos, Sociales y Culturales (1966), respetar, proteger y cumplir el derecho fundamental a la salud.]

[Tal es el propósito del ADPIC cuando ordena que la protección de la propiedad intelectual se brinde]*[“de modo que favorezca el bienestar social y económico y el equilibrio de derechos y obligaciones”]*[(art. 7)]*[,]*[y cuando reafirma el derecho de los países]*[ de “adoptar las medidas necesarias para proteger la salud pública”]*[(art. 8).]

[También es la intención de la Declaración de Doha cuando afirma que el ADPIC]*[“puede y deberá ser interpretado y aplicado de una manera que apoye el derecho de los Miembros de la OMC de proteger la salud pública y, en particular, de promover el acceso a medicamentos para todos”]*[(punto 4).]

[Contrastando con esta realidad evidente, últimamente vienen apareciendo en los medios, incluyendo algunos de alto impacto político, artículos que presentan el conjunto normativo conformado entre ADPIC y Doha como si su objetivo único fuera asegurar la protección de la propiedad intelectual, desligado de toda subordinación a valores superiores, como la protección de la salud y la vida, y sostienen que con este norte debe ser leído todo su contenido, incluidas las disposiciones que reglamentan las licencias obligatorias]*[,]*[cuya aplicación sólo es procedente -según ellos- por vía de excepción,]*[ en situaciones verdaderamente extremas.]*

[Tal es el caso, por ejemplo, de un][[artículo publicado el 24 de agosto pasado por The Hill]](https://thehill.com/opinion/international/403541-its-time-for-latin-america-to-close-the-ip-rights-gap)[, un diario cuyo foco central son los miembros del Congreso, el Gobierno y los partidos políticos de Estados Unidos. En el artículo, titulado]*[“Es hora de que América Latina cierre la brecha de los derechos de propiedad intelectual”,]*[ el autor, Philip Thomson, critica que en América Latina varios países estén intentando utilizar el mecanismo de las licencias obligatorias a pesar de que no existan crisis de salud o emergencias]*[“que puedan justificar su uso”.]*[Olvidando que, como bien lo afirma el Grupo de Alto Nivel del Secretario General de las Naciones Unidas sobre el Acceso a Medicamentos en su informe final (Septiembre 2016), la Declaración de Doha deja en manos de los Estados]*[“la determinación soberana de los motivos que justifican la concesión de una licencia obligatoria”]*[.]

[Es una preocupante sorpresa un olvido de esta proporción por parte del analista de propiedad intelectual y comercio internacional de]*[Property Right Alliance]*[, organización dedicada a la defensa de los DPI a nivel nacional e internacional, a menos que esté actuando en cumplimiento de un encargo profesional de actores interesados en desacreditar cualquier instrumento que pueda afectar el monopolio farmacéutico. Encargo que no sería extraño en un entorno en el que hay evidencia de que se compran conciencias de científicos y líderes de opinión (][[ver “Crisis científica internacional a causa de la influencia antiética de la industria farmacéutica”]](https://www.mision-salud.org/actualidad/crisis-cientifica-internacional-a-causa-de-la-influencia-antietica-de-la-industria-farmaceutica/)[) con la finalidad de que presenten como propias ideas y conceptos de interés para las metas comerciales de quien posee el monopolio.]

[Las licencias obligatorias representan una herramienta importante de política pública porque permiten a las autoridades gubernamentales estimular la competencia farmacéutica, con la consiguiente reducción de los precios de los medicamentos y el consiguiente incremento de los niveles de acceso. Experiencias vividas en el mundo muestran que esta herramienta tiene la virtud de reducir los precios de los medicamentos objeto de ellas hasta en un 70% o más.]

[Un caso emblemático es el de la India con el sorafenib (Nexavar®), medicamento patentado por Bayer en ese país en 2007 y utilizado para alargar la vida en casos avanzados de cáncer renal y hepático. La Licencia Obligatoria otorgada por el gobierno indio consiguió reducir el costo del tratamiento en un 97%: de \$5.500 dólares mensuales con el Nexavar® a \$175 dólares mensuales con la versión genérica. Al precio del pionero sólo el 2% de los pacientes elegibles para el tratamiento podían recibirlo. Hoy pueden recibirlo todos. Nada justificaría privar de este beneficio vital a otros pueblos de la Tierra.]

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)**

------------------------------------------------------------------------

#### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
