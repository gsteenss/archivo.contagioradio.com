Title: Bogotá se compromete con la memoria para la paz
Date: 2020-01-14 17:59
Author: AdminContagio
Category: Memoria, Paz
Tags: Bogotá, Claudia Lope, jose antequera, Vladimir Rodriguez
Slug: bogota-compromete-con-memoria-para-la-paz-y-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Memoria-Paz-y-Reconciliación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @ClaudiaLopez

<!-- /wp:paragraph -->

<!-- wp:group -->

<div class="wp-block-group">

<div class="wp-block-group__inner-container">

<!-- wp:paragraph {"align":"justify"} -->
</p>
Este martes 14 de enero se realizó la posesión de José Antequera Guzmán como director del Centro de Memoria, Paz y Reconciliación y de Vladimir Rodríguez Valencia como alto consejero para los Derechos de las Víctimas, la Paz y la Reconciliación. Nombramientos con los que la alcaldesa Claudia López expresó que daba un paso en reafirmar el compromiso de Bogotá como la capital de un país constructor de paz y reconciliación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En su intervención, la Alcaldesa también señaló que esta es la oportunidad para que esta generación logre vivir en paz con su memoria, con su pasado y su presente. Adicionalmente, sostuvo que es el momento de buscar la reconciliación con las otras generaciones, con la naturaleza y sus derechos, al hacer realidad la Constitución de 1991.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Vladimir Rodríguez: Nos une la construcción de un epicentro para la paz**

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Bogota/status/1217119315192881152","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Bogota/status/1217119315192881152

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

En su discurso, Rodríguez declaró que junto a la Alcaldesa tienen "la convicción de la construcción de Bogotá como un epicentro para la paz". Al tiempo, reconoció que la Alta Consejería para los Derechos de las Víctimas, la Paz y la Reconciliación es un espacio que sólo tiene sentido en la medida en que la ciudadanía se una en torno a la defensa de la vida, sobre todas las cosas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Rodríguez también sostuvo que es necesario elaborar una política pública de implementación del Acuerdo de Paz, convirtiendo a Bogotá en una guía sobre lo que significa una implementación exitosa. (Le puede interesar: ["Voces difieren de las declaraciones de ONU sobre implementación del acuerdo de paz"](https://archivo.contagioradio.com/voces-difieren-de-las-declaraciones-de-onu-sobre-implementacion-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **José Antequera: La promesa de un Centro de Memoria Viva**

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Bogota/status/1217123757359292417","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Bogota/status/1217123757359292417

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, [Antequera](https://www.justiciaypazcolombia.com/esposa-e-hijos-de-jose-antequera-lo-recuerdan-20-anos-despues-de-su-asesinato/) afirmó que una ciudad es, por definición, un gran espacio de memoria. En ese sentido, relató que Bogotá necesita un espacio y una política para "preservar, recuperar y la recrear la memoria para la paz y la reconciliación".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El nuevo Director se comprometió a crear un Centro de Memoria vivo, "con espacios para la reflexión y la creatividad, porque "no se trata solo de conocer el pasado, como si ello garantizara la no repetición. Más bien, tenemos que movilizar nuestro pensamiento y acción para que la barbarie no siga ocurriendo".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En consecuencia, sostuvo que desde este espacio no seguirán la corriente negacionista que vive el país en cuanto a la memoria, y dijo que con ayuda de las organizaciones sociales, instituciones y la ciudadanía lograrán "tomar el rumbo indicado con la memoria como lo que es: sentido de orientación", para que Bogotá sea un escenario donde la experiencia de las víctimas tenga expresión y lugar en el presente, y hacia el futuro.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El contraste con una Alcaldía que no incluía el Acuerdo de Paz en sus Planes de Desarrollo**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Defensores de derechos humanos y líderes de Bogotá cuestionaban la anterior alcaldía de Enrique Peñalosa porque no incluyó en su Plan de Ordenamiento Territorial (POT) un capítulo de implementación del Acuerdo de Paz. La evidencia de ello, estaba en la competencia generada entre el suelo urbano y el rural que está en la Capital, problema que aborda el primer punto del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En contraste, la Alcaldesa López aseguró que la invitación a la posesión de Rodríguez y Antequera era, al tiempo, una invitación a convertir a Bogotá en un espacio para honrar la memoria de quienes ya no están, y la esperanza de quienes sí están. (Le puede interesar: ["POT de Peñalosa satisface intereses económicos y no las necesidades de Bogotá"](https://archivo.contagioradio.com/pot-satisface-intereses-economicos/))

<p>
<!-- /wp:paragraph -->

</div>

</div>

<!-- /wp:group -->

<!-- wp:block {"ref":78980} /-->
