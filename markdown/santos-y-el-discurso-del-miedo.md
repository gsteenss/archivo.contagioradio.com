Title: Santos, y el discurso del miedo
Date: 2016-06-22 08:42
Category: Camilo, Opinion
Tags: FARC, Presidente Santos, proceso de paz
Slug: santos-y-el-discurso-del-miedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/presidente-santos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Reintegración ACR 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@CamilodCasas](https://twitter.com/CamilodCasas)** 

###### [22 Jun 2016] 

[El discurso del terror funciona. Hace pocas horas lo volvió a usar Santos en el Foro Económico Mundial en Medellín, delante de los empresarios. Si no se gana el plebiscito el país volverá a la guerra, pues las FARC EP están preparándose para la guerra de guerrillas en las ciudades.]

[Su expresión es parte de una estrategia, más que de polarización con el uribismo, que ya existe, para otros fines. Es un mensaje a la Corte Constitucional para que vea las consecuencias de no aprobar el plebiscito propuesto por el mandatario como mecanismo de refrendación de los acuerdos. Es una presión a las FARC EP para que firmen a la mayor brevedad unas zonas de dejación de armas, aisladas de los cascos urbanos. Las zonas que no serán ni las  14 que propone el gobierno, ni el más de medio centenar que propuso las FARC EP. Este aspecto  ha sido un pulso en La Habana que ya se resolvió, pero al que había que ponerles el aderezo del miedo a las guerrillas. En pocas horas se conocerá el acuerdo sobre los lugares de encuentro para la dejación de armas de las FARC EP.]

[Santos resulta entonces como el desarticulador preventivo  del terror urbano, esperando en medio de su aparente incoherencia,  que la mayoría en agradecimiento respalde su plebiscito. Santos vuelve sobre el subconciente colectivo inoculado de un imaginario de terror de las guerrillas en perspectiva de futuro. Unas FARC sin armas en la política, serán descalificadas por sus contradictores políticos con ese fantasma ya creado que van a llevar la guerra a las ciudades, vendrán cacerías de brujas y un gran ambiente para un electorado presto para la venganza, y allí calará el discurso de la segunda fase de la Pax Romana con Vargas Lleras.]

[Obviamente que el mensaje es hoy un papayaso para que la llamada extrema derecha saque provecho. El sector que está en contra del plebiscito que quiere un sometimiento de las guerrillas al estilo de Uribe, que también proyectó la Pax Neoliberal con los alzados en armas con algunas concesiones encontró otro plato servido de la dulce fruta. Hoy pueden decir que Santos amenazó al país; que entregó el país al castrochavismo que se va a tomar las ciudades, y sobre todo, para sostener que el cese unilateral, que de facto se ha convertido en bilateral,  haya sido usado por las FARC EP para sus ventajas militares. Y al final, el imaginario del terror preconizado por Santos, se muestra compartido por las élites del país entre ellos Uribe, ambientando negativamente el escenario político para la oposición alternativa que podría fortalecerse, firmados los acuerdos.]

[Santos es más astuto de lo que aparece con su frialdad calculadora, que lo hace ver como torpe o zigzagueante o solo enfrentado con el uribismo, con el cual, dicho sea de paso podría reconciliarse, si el Fiscal General elegido es Néstor Humberto Martínez. Hay que leerlo en su proxémica, en sus palabras como  aquellas de la llave de la paz es mía o la “paz” generará seguridad a la inversión o el “el tal paro no existe”.]

[Ciertamente, Santos no es confiable; como nunca lo ha sido ninguno de sus antecesores en la casa de Nariño. Ellos, esos dirigentes, la clase política y empresarial de está republiqueta, incluyendo la que aparece a la luz más narcoparamilitarizada es así. Esa clase política ha vivido a merced de la represión, de la manipulación seductora del país nacional, y de la corrupción,  sin ninguna generosidad ni solidaridad con sus compatriotas, salvo con los que comparten su propio portafolio de negocios.]

[A los que creen en la solución política, Santos les ha dado otra puñalada. Ahora a esas víctimas, también a las del conflicto armado, a esas víctimas del modelo económico y ambiental, a esa sociedad militante en la paz, le asiste una mayor creatividad e inteligencia para lograr convencer a una ciudadanía escéptica, que es necesario respaldar el mecanismo refrendatorio. La única razón de fondo es que la agenda de la paz es la posibilidad hoy de abrir la discusión al modelo ambiental, económico y político alternativo. Esta es la razón de la solución del conflicto armado, intentar por otros medios, la construcción de lo distinto. La refrendación no es por Santos, ni por su pelea  con Uribe, es por una razón de fondo, intentar habilitar alguna garantía básica para la oposición política y el movimiento social, que en realidad es el sujeto  que puede cimentar la paz.]

[No hay que llamarse a equívocos. Santos como Uribe piensan en el mínimo de concesiones a las guerrillas, o mejor, a decisiones políticas que desestructuren las causas que las originaron. Sus palabras ante inversionistas  de Colombia y algunos del planeta, no son equivocación, son la continuidad de una certeza y  de una convicción: la paz es Pax neoliberal, en sus tiempos y afanes. Ahora es el tiempo del plebiscito, y del aislamiento de las ciudades para un proyecto alterativo y alternativo, por eso hay que revivir los fantasmas.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
