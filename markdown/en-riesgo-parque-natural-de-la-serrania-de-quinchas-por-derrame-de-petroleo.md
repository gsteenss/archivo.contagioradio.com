Title: En riesgo Parque Natural de la Serranía de Quinchas por derrame de petróleo
Date: 2018-07-23 14:24
Category: Ambiente, Nacional
Tags: derrame de petróleo, Ocensa, Puerto Boyacá, Serranía de las Quinchas
Slug: en-riesgo-parque-natural-de-la-serrania-de-quinchas-por-derrame-de-petroleo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/33872645_483397725426974_2519311044262756352_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RedProdepaz] 

###### [23 Jul 2018] 

Una emergencia ambiental se presentó el pasado sábado 21 de julio, en el Parque Natural de la Serranía de Quinchas, Boyacá, luego de que un oleoducto de la empresa Ocensa se reventará en la Cuenca de la quebrada La Cristalina. Las comunidades han manifestado que, si bien la mancha de petróleo no ha llegado hasta la quebrada, las lluvias que se han registrado **en los últimos días podrías provocar que la mancha se extienda de forma más rápida afectado tanto a la fauna como a la flora de las riveras de ese río.**

### **“La catástrofe ambiental se produjo hace 24 años”** 

De acuerdo con Hernando Muñeton, presidente de la Federación de pescadores artesanales y ambientales, el daño ambiental se dio desde hace más de 24 años cuando se inició la construcción de un oleoducto que atraviesa la Serranía de la Quinchas, en el municipio de Puerto Boyacá y que provocó la deforestación de este lugar.

“Este corredor es un pulmón, bastante grande y rico en biodiversidad. En su momento la comunidad advirtió de los daños irreparables que se iban a dar**. Allí Ocensa prometió un mantenimiento permanente a las tuberías y una reforestación que no se hizo**” afirmó Muñeton. (Le pude interesar: ["En menos de 6 meses se han presentado dos derrames de petróleo en el Magdalena"](https://archivo.contagioradio.com/en-menos-de-6-meses-se-han-presentado-dos-derrames-de-petroleo-en-el-rio-magdalena/))

De igual forma señaló que existiría información que asegura que a las tuberías no se les habría hecho mantenimiento desde hace 6 años. Sin embargo, una vez se presentó el derrame, la empresa activo el plan de contingencia para evitar un daño mayor en el territorio. Asimismo, denunció que en la Serranía habría una concensión a otra multinacional **dentro del bloque BM9 Y BM11 para que se realice exploración de hidrocarburos**.

Muñeton también recordó que, a 20 kilómetros del casco urbano del municipio de Puerto Boyacá, se encuentra la Ciénaga de Palagua, un ecosistema con **400 hectáreas que se encuentra contaminado después de un derrame provocado hace 20 años**, hecho que debe ser un precedente para evitar los derrames de petróleo en el país.

<iframe id="audio_27199634" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27199634_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
