Title: Egipto abre temporalmente el paso de Rafah
Date: 2015-08-18 14:57
Category: El mundo, Otra Mirada
Tags: Asistencia Humanitaria, Derechos Humanos, Egipto, Gaza, Israel, Palestina
Slug: egipto-abre-temporalmente-el-paso-de-rafah
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Estudiantes-pacientes-y-person_54428854106_54028874188_960_639.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Lavanguardia.com 

###### [18 ago 2015] 

**Egipto dio apertura el día de ayer en ambas direcciones, al paso de Rafah** que limita con la parte meridional de Gaza, beneficiando así a cientos de Palestinos que desde hace tres semanas esperaban para cruzar.

Esta apertura **se da eventualmente desde la caída de Muhamed Mursi en el año 2013**, principalmente con fines humanitarios "La prioridad de viaje a través del cruce será para pacientes que necesiten tratamiento médico urgente y estudiantes fuera de Palestina que poseen dobles nacionalidades", afirma la oficina para las fronteras del Ministerio de relaciones exteriores de Palestina.

**El paso de Rafah es el principal nexo de unión entre los 1,8 millones de residentes de Gaza** y el mundo exterior que no pueden pasar por Israel, debido al bloqueo sobre la zona costera.

Egipto se ha pronunciado en contra de la apertura permanente de este paso, acusando a Hamas de patrocinar actividades terroristas en el Sinaí. En un informe del Ministerio del Interior de Gaza anunció que en este 2015, **el paso solo ha estado abierto durante 15 días únicamente.**
