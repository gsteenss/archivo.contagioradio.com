Title: ELN decreta cese al fuego unilateral durante jornada electoral
Date: 2018-06-11 10:00
Category: Paz, Política
Tags: Cese al fuego, Elecciones presidenciales, ELN, segunda vuelta elecciones
Slug: eln-decreta-cese-al-fuego-unilateral-durante-jornada-electoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/eln-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN] 

###### [11 Jun 2018] 

Por medio una comunicación hecha a través de la revista “Insurrección”, el Comando Central del Ejército de Liberación Nacional-ELN, decretó la suspensión de operaciones militares teniendo de presente la jornada electoral de la segunda vuelta presidencial que se llevará a cabo en el país el 17 de junio. El cese al fuego será a partir del **15 de junio y terminará el 19 del mismo mes**.

El anuncio establece que el cese al fuego lo realizarán “para facilitar la participación de los colombianos y las colombianas **en la segunda vuelta de las elecciones presidenciales".** Recordaron que es la segunda suspensión de operaciones militares que decretan “en menos de un mes” debido a las elecciones.

### **Lectura del ELN de la coyuntura política del país** 

Con relación a la coyuntura política que vive el país, el ELN manifestó que es necesario que “la que la voz del pueblo **pueda plasmarse de forma masiva y libre en las urnas**” a pesar de que alertaron que “hay deformaciones del sistema electoral, de la permanencia de la violencia, del poder mediático y del control de las clases dominantes”. (Le puede interesar:"[Cese unilateral del ELN distensiona la mesa en la Habana: Luis Eduardo Celis](https://archivo.contagioradio.com/cese-unilateral-del-eln-distensiona-la-mesa-en-la-habana-luis-eduardo-celis/)")

Sin embargo, manifestaron que hay “una opción electoral diferente a la del establecimiento y ello marca **un momento histórico especial y** altera el cuadro político que por décadas ha permanecido invariable en el país”. En la editorial, el COCE indicó que en Colombia hay una posibilidad de terminar con la guerra “aunque los partidarios del ex presidente Uribe Vélez y su candidato Duque insistan en hacer trizas la paz”.

Frente a la candidatura del Gustavo Petro, el COCE indicó que la consolidación “como fuerza política **claramente definida por el cambio** y por la paz, ha sorprendido y alertado a todas las fuerzas derechistas y ha acelerado la unificación alrededor de su jefe más extremista, el ex presidente Uribe”.

Finalmente, en la publicación de la revista, manifestaron que hace falta voluntad política por parte del **Gobierno Nacional** para que en los diálogos que se mantienen en la Habana, “se concreten los dos acuerdos en materia de participación y de cese al fuego bilateral, temporal y nacional”.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
