Title: Las 4 preocupaciones en torno al plebiscito por la paz
Date: 2016-08-02 12:10
Category: Entrevistas, Paz
Tags: MOE, paz, Plebiscito
Slug: las-preocupaciones-en-torno-al-plebiscito-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Plebiscito-e1470157319868.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Univisión 

###### [2 Jul 2016] 

**La publicidad, financiación, divulgación y participación en política son las principales preocupaciones frente a las garantías electorales del Plebiscito por la paz**, que ha identificado la Misión de Observación Electoral, MOE, luego de que se conociera el comunicado de la Corte Constitucional avalando este mecanismo de participación de la ciudadanía, pero del cual aún no se conoce la sentencia.

“El comunicado que hace la Corte genera varios cuestionamientos sobre cómo se va a llevar a cabo la campaña en el marco del plebiscito, pues hay una decisión política, pero aún hay muchas preguntas sobre la sentencia real, dando lugar a diversas dudas, sobre el cómo deberá llevarse a cabo esa campaña ya sea por el SI o por el NO", explica Fabián Hernández Cadena, coordinador de comunicaciones de la MOE, quien agrega que es necesario que **se especifiquen cuáles serán las fuentes de financiación de las campañas por las dos posturas. Así como los topes y la rendición de cuentas.**

En este sentido, así fue como la MOE identificó esas cuatro inquietudes:

### 1. Publicidad 

Es preciso aclarar de qué manera se podrá hacer publicidad, quiénes podrán hacerla y también definir cuántos grupos promotores existirán por el sí y por el no, así como si serán nacionales o territoriales. Además de determinar los tiempos en los que podrá hacer publicidad, cómo se entiende la publicidad para el plebiscito y cómo será la regulación de las encuestas.

### 2. Financiación 

En cuanto a la financiación es necesario especificar cuáles serán las fuentes si será de privados o partidos políticos, así como los topes individuales y generales, sin dejar a un lado la importancia la rendición de cuentas. A su vez, genera inquietud si los partidos políticos podrán financiar su campaña por alguna de las posiciones con recursos de su funcionamiento.

### 3. Divulgación 

Frente a la divulgación es imperioso detallar si los partidos políticos podrán emplear sus espacios de divulgación para la campaña por el SÍ o el NO. Además de cuáles serán los contenidos de la divulgación y qué pasará cuando estos no coincidan con el texto del acuerdo final. Teniendo en cuenta la importancia de la divulgación del acuerdo final es necesario aclarar si habrá un tope de financiación para esta divulgación y hasta qué día se podrá realizar.

### 4. Participación en política 

Es importante delimitar con precisión qué se entiende por servidores públicos y si existe un régimen diferenciado. En caso tal, cuáles son los recursos que el Estado brindaría para las campañas por el sí o por el no, en caso de que se le permita a los servidores públicos participar en política, y quién delimitaría la participación que pueden tener éstos.

### ¿Qué va a hacer la MOE? 

Teniendo en cuenta estos retos y dudas, y pese a que no se ha  ha podido establecer las reglas del juego, **la MOE espera lograr contar con dos mil observadores electorales que harían vigilancia en las mesas de votación el día que se lleve a cabo el plebiscito.**

Así mismo, se piensa acompañar los municipios donde podría haber zonas de concentración veredal, también los lugares donde se ha desarrollado el conflicto armado y donde hay grupos armados, además de los municipios donde existe minería criminal. Un análisis del territorio colombiano que se evidenciaría en un mapa de riesgos que ha realizado la MOE y que sería presentado en 15 días.

https://www.youtube.com/watch?v=RNVjINgvWw0&feature=youtu.be  
<iframe src="http://co.ivoox.com/es/player_ej_12417200_2_1.html?data=kpehk5yWdJGhhpywj5aYaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncafVw86SpZiJhZLijK3S1NOJh5SZopbbxsreaZO3jLK8p5CRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
