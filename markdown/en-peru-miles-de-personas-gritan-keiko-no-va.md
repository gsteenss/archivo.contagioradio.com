Title: "Keiko no va" el grito contra la candidatura de la hija de Fujimori
Date: 2016-03-16 11:57
Category: El mundo, Entrevistas
Tags: Elecciones presidenciales, Fujimorismo, Keiko Fujimori, Ollanta Humala, Perú
Slug: en-peru-miles-de-personas-gritan-keiko-no-va
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/protestas-peru-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: dw/afp] 

<iframe src="http://co.ivoox.com/es/player_ek_10827149_2_1.html?data=kpWllJyVeJqhhpywj5aUaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncYamk7DSy9DTb8%2FjjNvOh5eWb8bgjMzfy9nTb8Tjz9nfw5DQpYzXwtPRy8nFuNbmwpDRx5DQpYzcys%2FOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Paloma Duarte, “La Junta”] 

###### [16 Mar 2016] 

En las calles de Lima, cerca de 10 mil personas se movilizaron ayer en contra de la candidatura presidencial de Keiko Fujimori, puesto que **se han comprobado diversos delitos de corrupción con material documental en el que se evidencian pagos en efectivo** por parte de la candidata a electores para favorecerla en las próximas elecciones que se realizarán el 10 de Abril.

La exigencia de la campaña “Keiko No Va” ya trascendió hasta el **tribunal electoral que deberá decidir si valida las pruebas y retira la postulación de Fujimori** que hasta ahora es favorita en las encuestas. Según Paloma Duarte, integrante de la organización social “La Junta” las pruebas son irrefutables y si se actúa en derecho, la campaña fujimorista deberá ser retirada de la contienda.

Ya en el mes de Febrero la candidata por el partido Fuerza Popular, reconoció los diversos actos de [corrupción cometidos durante el gobierno de Alberto Fujimori](https://archivo.contagioradio.com/?s=Per%C3%BA), **y a pesar de las pruebas en que ella misma hace entrega de dineros en efectivo**, afirma que en caso de ganar las elecciones combatirá la corrupción, afirma Duarte.

Lo cierto es que ni las organizaciones sociales, de DDHH y ciudadanos en general creen en el discurso fujimorista puesto que en la memoria reciente de ellos y ellas está firme el recuerdo del nefasto gobierno de la familia, **en el que 314.605 mujeres fueron esterilizadas en el marco del Programa Nacional de Planificación Familiar del gobierno de Alberto Fujimori, se [reprimió la protesta social](https://archivo.contagioradio.com/llegada-de-3200-militares-estadounidenses-a-peru-atenta-contra-la-soberania-de-ese-pais/) y se vendieron más de cien empresas públicas de alto valor comercial y estratégico.**

Se espera que la decisión de los tribunales electorales sea anunciada en las próximas horas, en caso de ser positiva a los reclamos de miles de personas, Keiko Fujimori **deberá ser investigada por la justicia y enjuiciada por los delitos de corrupción y de compra de votos de los que se le acusa.**

Frente a esa posibilidad, Duarte afirma que el movimiento social que se ha manifestado de manera espontánea también responde a una serie de inconformidades sociales y de Derechos Humanos que se han fortalecido durante los últimos años y que obedecen a políticas [que afectan el ambiente, el derecho al agua, el trabajo digno](https://archivo.contagioradio.com/continuan-protestas-en-peru-en-contra-de-proyecto-minero-tia-maria/) y la defensa de los DDHH entre otras.
