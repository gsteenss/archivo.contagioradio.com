Title: Presidente Iván Duque objeta seis puntos de la Ley Estatutaria de la JEP
Date: 2019-03-10 21:58
Author: ContagioRadio
Category: Paz, Política
Tags: acuerdo de paz, Iván Duque, jurisdicción especial para la paz
Slug: presidente-ivan-duque-objeta-seis-puntos-la-ley-estatutaria-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Duque-y-sus-objeciones-a-la-JEP-e1552341356510-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [10 Mar 2019] 

El Presidente Iván Duque anunció este domingo que **objeta seis artículos** de la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP) por razones de inconveniencia. Además, invitó al Congreso a debatir estos puntos a pesar de que la Ley ya había sido aprobada por la legislatura en el 2017 y luego, revisada por la Corte Constitucional el año pasado.

El primer punto que el mandatario objeta es el **articulo 7** porque "no establece de manera clara la obligación principal de los victimarios de reparar integralmente a las víctimas". El presidente agregó que se debe tener claro "la importancia de precisar que los victimarios deben adelantar una reparación material con sus bienes y activos que satisfaga a las víctimas". (Le puede interesar: "[Si Duque objeta la Ley Estatutaria vulneraría derechos de víctimas: organizaciones](https://archivo.contagioradio.com/razones-para-aprobar-la-ley-estatutaria/)")

En segundo lugar, realizó objeciones sobre el **enciso 8 del articulo 63** porque "no determina el alcance de la competencia atribuida al Alto Comisionado para la Paz para verificar la lista de quienes son reconocidos como miembros de los grupos armados que se sometan a un proceso de paz". Según Duque, esta decisión debe seguir siendo competencia del Alto Comisionado para la Paz y al contrario, delincuentes podrían ocultarse y ganar beneficios e impunidad al incorporarse a un proceso de paz "por la puerta de atrás".

En tercer lugar, el presidente también objetó el **inciso tercero del literal j del artículo 79 **porque no precisa las diligencias judiciales que la Fiscalía debe abstenerse de realizar en casos que sean de competencia de la JEP. El primer mandatario sostuvo que esto "genera una situación que perjudica los intereses de las víctimas y desperdicia valiosos recursos investigativos de autoridades con experiencia y capacidad". Por tal razón, afirma que se debe "definir con mayor precisión cuándo y bajo qué circunstancias las investigaciones contra personas sometidas a la JEP se suspenden en la justicia ordinaria".

El cuarto punto objetado es el **parágrafo 2 del artículo 19**. Según Duque, este artículo es inconveniente porque el contenido "renuncia a la acción penal frente a los crímenes de lesa humanidad, genocidio o crímenes de guerra en relación con quienes no son máximos responsables", lo cual él considera constituiría impunidad.

En quinto lugar, el presidente objetó el **artículo 150** que se trata de la extradición de personas por conductas posteriores a la firma del Acuerdo Final. La razón que Duque da es que "no precisa lo que ya fue dicho en la ley de Procedimiento de la JEP cuando expresa que la Sección de Revisión del Tribunal de Paz no puede practicar pruebas". Esto podría afectar la cooperación judicial de Colombia con otros países.***  ***

Finalmente, el sexto punto objectado es el **artículo 153** porque "condiciona la extradición de otras personas al ofrecimiento de la verdad sin establecer ningún tipo de término ni oportunidad para hacerlo". Duque indica que "produce un incentivo perverso para el ingreso a la JEP de terceros bajo el ropaje de supuestos ofrecimientos de verdad".

### **"La JEP seguirá funcionando": presidenta de la JEP** 

Por su lado, Patricia Linares, presidenta de la JEP recibió la decisión de Duque de objetar parcialmente la Ley Estatutaria e indicó que **la justicia transicional "seguirá funcionando plenamente**, ejerciendo a cabalidad todas y cada una de sus competencias y funciones".

### **¿Reformas constitucionales sobre la JEP?** 

En la alocución televisada, el presidente también informó que buscará que al menos tres aspectos del actual ordenamiento constitucional de la justicial transicional sean modificadas. En consecuencia, presentará una reforma constitucional al Congreso durante las siguiente sesión ordinaria que modificaría el acto legislativo 01 de 2017 para incluir tres puntos.

El primer punto **excluiría los delitos sexuales contra niños, niñas y adolescentes** de la competencia de la justicia transicional. En segundo lugar, el acto legislativo dejaría "claro que **quien reincida en las actividades criminales perderá todos los beneficios**". Por último, precisaría que las conductas delictivas que se hayan iniciado antes del 1 de diciembre de 2016 y que continúen ejecutándose después de esa fecha **serán competencia de la justicia ordinaria**.

Para concluir, el presidente afirmó que cualquier decisión que la Rama Ejecutiva respetará cualquier decisión que tomé el Congreso sobre las objeciones y el proyecto de reforma constitucional.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
