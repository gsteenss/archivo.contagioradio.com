Title: “Ningún tribunal de paz puede establecerse para generar impunidad" Alfredo Beltrán
Date: 2016-10-27 15:01
Category: Entrevistas, Paz
Tags: #AcuerdosYA, justicia transicional en colombia
Slug: ningun-tribunal-de-paz-puede-establecerse-para-generar-impunidad-alfredo-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_202-e1495706645670.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [27 de Oct 2016]

Uno de los puntos, dentro de la re negociación de la Habana, que ha generado mayor debate es el **Sistema Integral de Justicia y Reparación**, que era la propuesta de modelo de Justicia transicional hecho por la comisión técnica. Por un lado hay posturas como la del **Fiscal Néstor Humberto Martínez que propone aplicar la Ley 975 de Justicia** y Paz. Por el otro están las sugerencias hechas por la Corte Constitucional al acuerdo y finalmente están los que proponen un **“alivio judicial” para agentes del Estado o terceros.**

En principio la Corte Constitucional propone que los jueces que hagan parte del Tribunal para la paz, sean nacidos en Colombia. En el acuerdo se expresaba la posibilidad de jueces internacionales que generen un equilibrio en caso de que haya la necesidad de adoptar una decisión en empate. De acuerdo con el Magistrado, Alfredo  Beltrán, desde la constitución los jueces si deben ser personas nacidas en el país. **De lo contrario debería hacerse una reforma a la Constitución para aplicar el Sistema Integral de Justicia y Reparación**.

Frente a este punto el ex presidente Andrés Pastrana ha propuesto que se revise la figura del **“amicus curiae”, es decir, una persona extrajera con conocimiento en derecho que podría emitir una opinión no obligatoria** y que no tendría voto decisivo. Estos planteamientos han sido calificados por el presidente Santos como **“viables”** porque no modifican la estructura del Sistema, sino que sugieren complementos al mismo. Le puede interesar:["Organizaciones de víctimas piden audiencia ante CIDH y rechazan propuestas del NO"](https://archivo.contagioradio.com/organizaciones-de-victimas-piden-audiencia-ante-cidh/)

Por otro lado el Centro democrático propuso un “alivio jurídico” a integrantes de las Fuerzas militares y que terceros como empresarios o civiles no sean juzgados dentro de la Justicia transicional.  Según Beltrán, **el “alivio jurídico” puede interpretarse como una “autoadmistía” y ha sido rechazada incluso por la justicia internacional**. Le puede interesar:["Propuestas del Uribe no tienen en cuenta a las víctimas2](https://archivo.contagioradio.com/propuestas-de-uribe-no-nos-tienen-en-cuenta-a-las-victimas/)

Finalmente esta la propuesta de Néstor Humberto Martínez, que sugiere que se aplique la Ley 975 de Justicia y Paz, a los acuerdos de la Habana.  Beltrán señala que esta es la propuesta que debe descartarse por completo, ya que **la 975 no fue redactada o hecha para juzgar los delitos políticos. Además después de los once años que lleva en vigencia la ley ha demostrado que no es útil ni funcional.**

El Magistrado argumenta que cualquiera de estas propuestas expuestas “tendrían que ser discutidas en la Habana por el Gobierno y las FARC-EP” y aclaro que la conformación de este marco jurídico debe tener en cuenta que **“ningún tribunal puede establecerse para generar impunidad y la justicia transicional se requiere para dar el paso del estado actual de guerra a un estado de normalidad, después de que se apliquen las sanciones”.**

<iframe id="audio_13506610" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13506610_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
