Title: Partido de las FARC empezará vida política el primero de septiembre
Date: 2017-07-24 13:56
Category: Nacional, Paz
Tags: acuerdos de paz, FARC, partido políticos de las farc, pleno de las farc
Slug: partido-de-las-farc-empezara-vida-politica-el-primero-de-septiembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/farc-rueda-de-prensa-e1500922588668.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Contagio Radio] 

###### [24 Jul 2017] 

A través de una rueda de prensa, miembros del Estado mayor central de las FARC dieron a conocer que **el nuevo partido de las FARC empezará su vida política a partir del primero de septiembre**. Ellos hicieron referencia a los temas que han tratado en el marco del último pleno de Convergencia Nacional por la Reconciliación y la Paz de cara a la consolidación del partido.

Carlos Antonio Lozada, quien es miembro del estado mayor de las FARC, manifestó que en el último pleno han discutido temas relacionados con la problemática del país **que servirán de insumo para la creación de las propuestas del partido**. Lozada aseguró que “hemos trabajado sobre la política de género del partido, las propuestas al sector de la juventud, la defensa del medio ambiente, el tema agrario y urbano”. (Le puede interesar: ["FARC confía en que se cumpla liberación de presos políticos"](https://archivo.contagioradio.com/44063/))

### **Preocupaciones de las FARC en el proceso de consolidación del partido** 

Dentro de las preocupaciones que manifestaron los integrantes del estado mayor de las FARC, se encuentra el hecho de que, el gobierno que llegue para el periodo 2018, **tenga intenciones de modificar los acuerdos de paz**. Iván Márquez afirmó que “no tiene porque haber re negociación de los acuerdos dado que lo que se firmó ya está depositado en Suiza y hace parte de una declaración unilateral del Estado”.

De igual forma manifestaron que las personas que han estado liderando el proceso de paz, del lado de las FARC, **han sido víctimas de amenazas y sobre esto han puesto las denuncias respectivas ante las instituciones pertinentes** en Colombia y a nivel internacional. Sobre esto Márquez afirmó que “ha habido asesinatos de guerrilleros que han sido indultados", afirmación que hizo con referencia a la preocupación porque se repitan hechos como los asesinatos a los miembros de la Unión Patriótica. (Le puede interesar: ["Denuncian asesinato de integrante de las FARC en Ituango")](https://archivo.contagioradio.com/denuncian-asesinato-de-exguerrillero-de-las-farc-en-ituango/)

### **Apartes de la reunión entre integrantes de las FARC- EP  y ex jefes de las autodefensas ** 

Iván Márquez habló de las conclusiones a las que llegaron ellos y algunos ex jefes del paramilitarismo. Él manifestó que hubo un reconocimiento de la necesidad de que los colombianos y en especial las víctimas conozcan la verdad. **"Todos deben saber qué pasó, quiénes cometieron los hechos para dejar al descubierto la verdad**". Adicionalmente, manifestó que es importante este proceso de esclarecimiento de la verdad para poder acceder a las Justicia Especial para La Paz y de esa forma cumplir con lo acordado en la Habana. (Le puede interesar: ["Luego de reunión de exparamilitares y FARC-EP"](https://archivo.contagioradio.com/la-reunion-de-exparamilitares-y-las-farc-ep-si-van-a-decir-la-verdad/))

Finalmente, Erika Montero, también integrante de las FARC, manifestó que “hay una gran expectativa por lo que va a suceder en el desarrollo del congreso consultivo y la participación de las mujeres”. Ella aseguró que **el nuevo partido contará con la participación de una delegación nutrida de mujeres**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
