Title: "Alcalde Peñalosa es irresponsable y no respeta la Corte Constitucional": Recicladores
Date: 2017-10-11 12:53
Category: Movilización, Nacional
Tags: Bogotá, Enrique Peñalosa, modelo de recolección de basuras, recicladores
Slug: alcalde-penalosa-es-irresponsable-y-no-respeta-a-la-corte-constitucional-recicladores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/recicladores-e1507744363361.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Globalrec.org] 

###### [11 Oct 2017] 

Tras conocerse los pliegos y las condiciones de la licitación del nuevo modelo de aseo que regirá en Bogotá, los **recicladores indicaron que quedaron por fuera**. Afirman que la administración distrital no ha acatado las disposiciones de la Corte Constitucional que los protege.

En días pasado, la Unidad Administrativa Especial de Servicios Públicos (UAESP) indicó que **las licitaciones serán adjudicadas al final del 2017** para que el próximo año la capital tenga un nuevo sistema de recolección de basuras. En este sistema, habrá áreas exclusivas de servicio de recolección y se instalarán contenedores que albergarán el 27% de las basuras de zonas “críticas” de Bogotá.

De acuerdo con Nora Padilla, vocera de una de las asociaciones de recicladores, el modelo de basuras que se va a implementar incluye cambios **que marginan a los recicladores** en la medida que su trabajo va a ser reemplazado por lo que ellos llaman una “contenerización total”. (Le puede interesar: ["Distrito ampliaría hasta 2070 la existencia del relleno sanitario Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

Esto quiere decir que la alcaldía dispondría contenedores de basura en varias zonas teniendo en cuenta la cantidad de toneladas de basura que salgan de éstas áreas. Padilla afirmó que **esto perjudica el trabajo de los recicladores** porque los depósitos impedirían el trabajo de reciclaje ya que tendrían una altura de más de 2 metros.

### **Recicladores tienen el amparo de la Corte Constitucional** 

En el 2015, la Corte Constitucional emitió un fallo en donde establece que **se debe proteger a los recicladores** para que hagan parte del modelo de recolección de basuras para así estabilizar el modelo mismo.

Ante esto, Padilla indicó que harán todo lo posible para que en el nuevo modelo se respete y se cumpla el mandato de la Corte Constitucional que ha sido **desestimado en varias ocasiones por el alcalde Enrique Peñalosa**.  Él indicó que la orden de la Corte es “anti técnica y que no se ajusta a las necesidades de la ciudad”. (Le puede interesar: ["¿Cómo podría aprovecharse la basura del relleno sanitario Doña Juana?"](https://archivo.contagioradio.com/como-podria-aprovecharse-la-basura-del-relleno-sanitario-dona-juana/))

Los recicladores manifestaron que el alcalde **“es una persona irresponsable que no tiene en cuenta que las decisiones de la Corte** que favorecen a un sector de trabajadores y a la ciudad en general”. Padilla manifestó que, con esta postura, se está dejando a un lado la preocupación por el impacto social y ambiental del manejo de los residuos.

Finalmente, indicaron que cuando se instalen los contenedores, la basura se irá completa al relleno sanitario donde los “empresarios pueden poner sus propias plantas de selección del material”. Por esto, se unirán a la movilización de la Semana de la Indignación y **realizarán una marcha el 19 de octubre** para exigir que se respeten sus derechos laborales.

<iframe id="audio_21404375" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21404375_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
