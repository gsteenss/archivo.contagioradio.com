Title: Aumentan las amenazas contra líderes sociales del Bajo Atrato y Urabá
Date: 2017-12-15 14:45
Category: DDHH, Nacional
Tags: Bajo Atrato, lideres sociales, líderes sociales amenazados, lideres sociales asesinados, Urabá Antioqueño
Slug: aumentan-las-amenazas-contra-lideres-sociales-del-bajo-atrato-y-uraba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/paramilitares-san-jose-de-apartado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad de paz de San José de Apartadó] 

###### [15 Dic 2017] 

Llamadas intimidantes y conversaciones de paramilitares en los que aseguran estar a la espera de la orden de asesinar a varios líderes ambientalistas y reclamantes de tierras, son parte de las nuevas denuncias que hacen los líderes del Bajo Atrato, justo cuando se encuentran en Bogotá haciendo las denuncias respectivas ante distintas autoridades.

La Comisión Intereclesial de Justicia y Paz denunció que **se han agudizado las amenazas** a los líderes sociales del Bajo Atrato Chocoano y el Urabá Antioqueño. Esto sucede cuando 20 líderes y lideresas, que están amenazados de muerte, se encuentran en Bogotá exigiéndole al Gobierno Nacional garantías para preservar su vida y la posibilidad de retorno a sus territorios.

En un comunicado, la Comisión informó que **hay un plan “para atentar contra la vida** e integridad de miembros del Consejo Menor de Curvaradó, Fray Tuberquia y Santo Torres”. Además, están en riesgo inminente los líderes Miguel Hoyos, Eustaquio Polo y la lideresa María Ligia Chaverra. Indicó también que “según la fuente, el plan proviene de varios ocupantes de mala fe y sería ejecutado por la estructura neoparamilitar que ejerce presión en la zona”.

En el comunicado informan que **“un integrante de las autodenominadas Autodefensas Gaitanistas de Colombia** fue escuchado cuando sostenía una conversación en un establecimiento público, donde afirmaba lo siguiente: "se está a la espera de la orden para asesinar a los líderes que hacer parte o tengan relación alguna con la Zona Humanitaria Las Camelias”. (Le puede interesar: ["Los empresarios son los que están detrás de los asesinatos": líderes del Bajo Atrato y Urabá"](https://archivo.contagioradio.com/hasta-cuando-y-cuantos-mas-continuan-la-violencia-contra-lideres-sociales-del-bajo-atrato-y-uraba/))

### **Líderes han recibido llamadas sospechosas** 

Adicional a esto, la Comisión enfatizó en que el líder reclamante de tierras, Santo Torres, “ha recibido 15 llamadas” de un celular del cual no responden y al regresar la llamada “la operadora informa que el número no se encuentra disponible”. También, la lideresa y propietaria de una Zona de Biodiversidad ubicada en Caño Manso, Curvaradó, Ledy Tuirán y su hijo de 16 años, **“fueron blanco de amenazas de muerte por parte de las AGC”**.

Con respecto a esto, “las AGC afirmaron que la lideresa desde la figura humanitaria de protección ambiental, **es un impedimento para la implementación de proyectos agroindustriales**”. Así mismo, la Comisión denunció que los paramilitares estarían buscando atentar contra la vida de los habitantes de la Zona Humanitaria y de Biodiversidad de los líderes Guillermo Díaz y Enrique Cabezas.

Hay que recordar que en menos de 10 días fueron asesinados **Mario Castaño y Hernán Bedoya**, líderes sociales del Bajo Atrato. Por esto y los hechos recientes, “exacerban el estado de zozobra de los líderes reclamantes de tierras, sin que existan respuestas concretas de parte del Estado que posibiliten condiciones de protección efectiva para salvaguardar la vida de ellos y ellas”. (Le puede interesar: ["La Navidad sin Mario Castaño"](https://archivo.contagioradio.com/la-navidad-sin-mario-castano/))

### **Procuraduría General de la Nación exigió medidas de protección a los líderes** 

En una reunión que sostuvieron los líderes sociales con el Procurador Fernando Carrillo Flórez, este último **manifestó su preocupación por la grave situación** que están viviendo las comunidades del Bajo Atrato y el Urabá. El diario El Espectador indicó que Carrillo “teme que en los próximos días haya un desplazamiento masivo de miles de habitantes de las comunidades asentadas en las cuencas del río Atrato”.

El Procurador le hizo un llamado a instituciones como a la Fiscalía General de la Nación, el Ministerio del Interior y al Sistema Nacional de Atención y Reparación Integral de Víctimas para que garanticen la seguridad y protección de territorios como La Larga Tumaradó, Jiguamiandó y Curvaradó y Pedeguita y Mancilla. Finalmente, Carrillo informó que va a **iniciar acciones legales para anular el contrato** que habilitó el arrendamiento de tierras del territorio de Piedeguita y Mancilla a la empresa AGROMAR.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
