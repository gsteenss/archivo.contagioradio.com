Title: Hablemos Alguito
Date: 2014-11-25 15:50
Author: AdminContagio
Slug: hablemos-alguito
Status: published

### HABLEMOS ALGUITO

[![Más de 300.000 personas son víctimas de trata de personas en Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/trata-de-personas-e1469852928477-978x540.jpg "Más de 300.000 personas son víctimas de trata de personas en Colombia"){width="978" height="540"}](https://archivo.contagioradio.com/mas-300-000-personas-victimas-trata-personas-colombia/)  

###### [Más de 300.000 personas son víctimas de trata de personas en Colombia](https://archivo.contagioradio.com/mas-300-000-personas-victimas-trata-personas-colombia/)

[<time datetime="2017-07-29T10:00:03+00:00" title="2017-07-29T10:00:03+00:00">julio 29, 2017</time>](https://archivo.contagioradio.com/2017/07/29/)Este 30 de julio se conmemora el Día Mundial Contra la Trata de Personas, un flagelo que ocupa el segundo lugar de negocios ilegales más lucrativos en el mundo.[LEER MÁS](https://archivo.contagioradio.com/mas-300-000-personas-victimas-trata-personas-colombia/)  
[![Lucha contra el Apartheid de Israel a Palestina se toma Bogotá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Palestina-asentamientos-1170x540.jpg "Lucha contra el Apartheid de Israel a Palestina se toma Bogotá"){width="1170" height="540"}](https://archivo.contagioradio.com/lucha-apharteid-palestina/)  

###### [Lucha contra el Apartheid de Israel a Palestina se toma Bogotá](https://archivo.contagioradio.com/lucha-apharteid-palestina/)

[<time datetime="2017-03-30T15:09:50+00:00" title="2017-03-30T15:09:50+00:00">marzo 30, 2017</time>](https://archivo.contagioradio.com/2017/03/30/)La Semana Contra el Apartheid Israelí contra Palestina es uno de los esfuerzos internacionales de movimientos civiles, instituciones y universidades en las principales ciudades del mundo, que busca crear conciencia sobre las políticas de apartheid y los proyectos colonialistas Israelíes sobre el pueblo palestino[LEER MÁS](https://archivo.contagioradio.com/lucha-apharteid-palestina/)  
[![La Arpillera: los desaparecidos de Aysén como arte y memoria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/unnamed-1.jpg "La Arpillera: los desaparecidos de Aysén como arte y memoria"){width="1000" height="437" sizes="(max-width: 1000px) 100vw, 1000px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/unnamed-1.jpg 1000w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/unnamed-1-300x131.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/unnamed-1-768x336.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/unnamed-1-370x162.jpg 370w"}](https://archivo.contagioradio.com/aysenchilememoriaarpillera/)  

###### [La Arpillera: los desaparecidos de Aysén como arte y memoria](https://archivo.contagioradio.com/aysenchilememoriaarpillera/)

[<time datetime="2017-03-14T12:05:45+00:00" title="2017-03-14T12:05:45+00:00">marzo 14, 2017</time>](https://archivo.contagioradio.com/2017/03/14/)La “Arpillera” ha sido un mecanismo de memoria, de resistencia al olvido, ha sido un grito contra la impunidad. Así lo expresan y lo viven las familias de los 12 jóvenes asesinados por Carabineros en Aysén, Chile entre 1997 y 2002[LEER MÁS](https://archivo.contagioradio.com/aysenchilememoriaarpillera/)  
[![Fondo de Acción Urgente, apoyando a las defensoras de DDHH](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/cidh_nota_alquimia-1024x540.jpg "Fondo de Acción Urgente, apoyando a las defensoras de DDHH"){width="1024" height="540"}](https://archivo.contagioradio.com/fondo-de-accion-urgente-20-anos-apoyando-a-defensoras-de-ddhh/)  

###### [Fondo de Acción Urgente, apoyando a las defensoras de DDHH](https://archivo.contagioradio.com/fondo-de-accion-urgente-20-anos-apoyando-a-defensoras-de-ddhh/)

[<time datetime="2016-07-19T16:00:17+00:00" title="2016-07-19T16:00:17+00:00">julio 19, 2016</time>](https://archivo.contagioradio.com/2016/07/19/)Foto: Fondo Alquimia 19 Jul 2016 El Fondo de Acción Urgente (FAU) nace en los Estados Unidos como una propuesta colaborativa de apoyo a defensoras de Derechos Humanos y sus organizaciones, movilizando recursos cuando estas atraviesan situaciones imprevistas que ponen en riesgo el desarrollo de su labor. Concebida en 1995 durante la IV Conferencia Mundial de...[LEER MÁS](https://archivo.contagioradio.com/fondo-de-accion-urgente-20-anos-apoyando-a-defensoras-de-ddhh/)  
[![Las luchas de la Nación indígena U’wa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Uwa-720x540.jpg "Las luchas de la Nación indígena U’wa"){width="720" height="540" sizes="(max-width: 720px) 100vw, 720px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Uwa.jpg 720w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Uwa-300x225.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Uwa-370x278.jpg 370w"}](https://archivo.contagioradio.com/las-luchas-de-la-nacion-indigena-uwa/)  

###### [Las luchas de la Nación indígena U’wa](https://archivo.contagioradio.com/las-luchas-de-la-nacion-indigena-uwa/)

[<time datetime="2016-06-07T22:13:57+00:00" title="2016-06-07T22:13:57+00:00">junio 7, 2016</time>](https://archivo.contagioradio.com/2016/06/07/)Foto: Yaku.eu 7 Jun 2016 La Nación indígena U'wa ubicada en los departamentos de Boyacá, Arauca, Norte de Santander, Casanare, Santander son una de las comunidades indígenas en peligro de desaparecer según la propia Corte Constitucional. Los indígenas han debido enfrentarse a diferentes amenazas entre las que se encuentra el modelo extractivista y el ecoturismo. El pueblo U'wa tiene una larga...[LEER MÁS](https://archivo.contagioradio.com/las-luchas-de-la-nacion-indigena-uwa/)  
[![Reforma tributaria desaparecerá el 94% de las organizaciones sociales del país](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/reforma-tributaria-contagioradio-1170x540.jpg "Reforma tributaria desaparecerá el 94% de las organizaciones sociales del país"){width="1170" height="540"}](https://archivo.contagioradio.com/reforma-tributaria-desaparecera-el-94-de-las-organizaciones-sociales-del-pais/)  

###### [Reforma tributaria desaparecerá el 94% de las organizaciones sociales del país](https://archivo.contagioradio.com/reforma-tributaria-desaparecera-el-94-de-las-organizaciones-sociales-del-pais/)

[<time datetime="2016-05-28T17:41:51+00:00" title="2016-05-28T17:41:51+00:00">mayo 28, 2016</time>](https://archivo.contagioradio.com/2016/05/28/)Foto: acuda 02 May 2016 El gobierno presentará en el segundo semestre del año una reforma tributaria estructural para que sea tramitada en el congreso. En ella se plantea que se debe resolver del déficit fiscal que supera los 6 billones de pesos. Sin embargo no se tiene en cuenta que los pequeños contribuyentes hasta...[LEER MÁS](https://archivo.contagioradio.com/reforma-tributaria-desaparecera-el-94-de-las-organizaciones-sociales-del-pais/)
