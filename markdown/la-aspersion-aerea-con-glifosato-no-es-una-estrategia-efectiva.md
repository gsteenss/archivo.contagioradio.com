Title: Aspersión aérea con glifosato, una estrategia fallida que se repite
Date: 2020-03-02 19:23
Author: CtgAdm
Category: Ambiente, DDHH
Tags: comunidades, Cultivos de coca, Duque, Glifosato
Slug: la-aspersion-aerea-con-glifosato-no-es-una-estrategia-efectiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Glifosato-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ArchivodeContagioRadio

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/pedro-arenas-integrante-corporacion-viso-mutop_md_48431291_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Pedro Arenas | director del observatorio de cultivos de la Corporación Viso Mutop

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Comunidades y organizaciones rechazan el plan del gobierno Duque para l**a reactivación de las aspersiones con[](https://archivo.contagioradio.com/con-politicas-del-uribismo-seguiremos-entregando-los-territorios-a-la-ilegalidad/)glifosato** que podrían iniciar el segundo semestre de 2020, esto ante el decreto que pretende reanudar esta práctica y ante el apoyo decidido del gobierno de Estados Unidos .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para conocer la opinión de una de las organizaciones que ha trabajado durante varios años el tema y ha trabajado de la mano de las comunidades hablamos con **Pedro Arenas**, **director del observatorio de cultivos de la Corporación Viso Mutop**.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "La aspersión aérea con glifosato no es una estrategia efectiva en las disminución de los cultivos de uso ilícito"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Arenas señaló que el accionar del Gobierno corresponde a *"órdenes desleales que ignoran lo que dice la Corte, los grupos ambientalistas y la comunidad en general ante el regreso de las aspersiones y las afectaciones evidentes que ésta genera"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma afirmó que las políticas sobre la erradicación deben ser diseñada y reglamentadas por un órgano distinto a las entidades encargadas de ejecutar los programas, *"la regulación debe derivar una evaluación de riesgo a la salud, medio ambiente y otros; así como* ***el proceso decisorio deberá incluir una revisión automática*, comprensiva, independiente e imparcial***".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señaló también que según el informe presentado en febrero por la Oficina de Naciones Unidas contra la Droga y el Delito (ONUDC) **la resiembra en algunos territorios donde se dió la erradicación voluntaria es del 0,4%**, *"esto contradice lo que dice el Gobierno que pretenden activar el uso de este químico tan cuestionado como es él glifosato bajo el argumento de que los cultivos están creciendo, ignorando las acciones de la comunidad"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y destacó que *"**las fumigaciones no sirven, al contrario causan más daño**, entonces lo que se debe implementar es la sustitución de los cultivos y el cumplimiento a las comunidades"*, según Arenas son tres puntos los que se deben tener en cuenta antes de la fumigación; *"el primero **apostar a la erradicación voluntaria, segundo trabajar por la erradicación terrestre y tercero ejecutar lo pactado en el Acuerdo**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Le puede interesar: <https://archivo.contagioradio.com/coccam-exige-garantias-para-las-familias-que-trabajan-por-la-sustitucion-de-cultivos/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las comunidades claman que no se activen las aspersiones con glifosato

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este 2 de marzo los diputados del departamento de Putumayo, **Jairo López** (Partido Conservador), **Miguel Bravo** (MAIS), **Amparo Córdoba** (Partido Conservador), **Carlos Marroquín** (Partido de la U), **Aurelio Zamora** (Partido de la U) y **Nelsy Peñafiel** (ASÍ), **presentaron una ponencia** destacando los diferentes puntos por los cuales este territorio rechaza la erradicación con aspersión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uno de las solicitudes allí mencionadas va ligada al acompañamiento que debe entregar el Gobierno a las comunidades en las instancias establecidas por el PNIS, así como el **"cumplimiento estricto a todas las acciones contempladas en su programa de gobierno enfocadas en la sustitución voluntaria".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto Pedro Arenas señaló que, *"el Estado antes de llegar a escuchar y dialogar con las comunidades prefiere darles con el “garrote” imponiendo los operativos ya conocidos"*. (Le pude interesar: <https://archivo.contagioradio.com/con-politicas-del-uribismo-seguiremos-entregando-los-territorios-a-la-ilegalidad/> )

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que las comunidades tienen miedo, porque varios ya han vivido esta práctica y saben sus consecuencias, *"a las familias les molestan estos operativos, porque **hay más dinero para la erradicación forzada que al cumplimiento de lo pactado, además en él pasado esto generó desplazamiento forzado por el Estado y afectación a los cultivos de pan coger** de los campesinos"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El costo de la aspersión aérea con glifosato en el aire que respiran los Colombianos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Entre **1994 y 2015 en Colombia se han fumigado con glifosato 1.896.709** hectáreas de coca, **asperjando en promedio entre 6 y 10 litros por hectárea provocando según el senado de la República y la comisión primera graves consecuencias** para la salud, el medio ambiente y en especial contaminación de fuentes hídricas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la universidad de los Andes, en el estudio **"Drogas y política de drogas en Colombia"** para eliminar una hectárea de Coca por medio de la aspersión aérea ***"hay que fumigar en promedio 32 veces**, lo que representa un costo **por hectárea de \$72.000 dolares; con una efectividad promedio de tan sólo 4, 2%**"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un Gobierno alejado de las comunidades

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Arenas y los registros del Observatorio de la **Corporación Viso Mutop**, *"son 18 países los que tienen restringido el uso de este agroquimico en Colombia, por eso es valido pensar que el accionar del Gobierno ignora a los campesinos, los informes de organizamos como la ONU y las evidentes investigaciones que respaldan los daños que causa"*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanDuque/status/1234554065562021890","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanDuque/status/1234554065562021890

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por otro lado y continuando la línea de acción del Gobierno ante este tema, el lunes de 2 de marzo durante un encuentro entre el Presidente de Estados Unidos y el Presidente Duque en la Casa Blanca; Trump manifestó*"se debe hacer aspersión aérea de glifosato en los cultivos ilícitos en Colombia"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello Duque agregó que esta actividad se combinará con la erradicación manual, *"para luchar contra ese crimen"* que ha sido una de sus banderas desde la llegada a la presidencia (Le puede interesar: <https://www.justiciaypazcolombia.com/respaldamos-la-labor-de-la-alta-comisionada-para-los-ddhh-en-colombia-y-su-representante/> )

<!-- /wp:paragraph -->
