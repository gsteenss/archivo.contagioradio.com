Title: La rebelión de Momi
Date: 2016-06-05 12:52
Category: Eleuterio, Opinion
Tags: Cascos azules ONU, Niños soldados Sierra Leona
Slug: la-rebelion-de-momi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/nic3b1os-soldados.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: UNICEF 

#### Por **[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

##### [5 Jun 2016] 

“No existen guerras étnicas ni religiosas en África. Todo eso es mentira. Detrás de cada guerra en África hay un recurso natural que interesa a Occidente. Allí una materia prima equivale a una guerra.” Por ejemplo: el coltán en Congo, el petróleo de Sudán, el uranio en Nigeria o los diamantes en Sierra Leona. Lo que no hay porque no se fabrican en África son armas, éstas vienen de fuera. “Los cinco principales paísesfabricantes de armas son los cinco países que tienen derecho a veto en la ONU (EEUU,Francia, Reino Unido, China y Rusia).” El sexto es España. Quien habla es Chema Caballero, ex-misionero destinado a Sierra Leona durante 18 años.

Sierra Leona se hizo tristemente famosa a principios de los años 90 por la siniestra moda de emplear niños soldado en la cruenta guerra que arrasó el país durante la última década del siglo XX. Cabe decir que entre los llamados niños soldados había también muchísimas niñas. Tanto unos como las otras eran empleados también como esclavos sexuales por sus oficiales superiores, “Momi fue una de esas niñas”. El conflicto desencadenó una violencia inusitada con miles de violaciones, centenares de mutilados, más de cien mil muertos y dos millones de desplazados. La guerra acabó cuando en 1998 la ONU decidió intervenir finalmente enviando a los cascos azules.

El trabajo de Chema consistía en tratar de recuperar a estos niños y niñas que sólo conocían las atrocidades de la guerra. El panorama no era fácil. “Al acabar el conflicto tenemos miles de adolescentes excombatientes, expertas sexuales y sin la menor autoestima, con un destino que les aguarda en las playas de Freetown.” Allí el mercado de la prostitución (también de menores) florecía gracias, entre otras cosas, a los cerca de 17500 soldados de la ONU destinados ha dicho lugar y las 500 ONGs que aparecieron por aquel entonces. Una vez, para dejar el oficio en Freetown, a Momi le ofrecieron un sueldo como costurera. Su respuesta fue aparentemente tan contundente como cargada de lógica: “Lo que puedo ahorrar trabajando dos años como costurera, lo gano con un blanquito en una noche”.

Ahora han pasado los años. Los soldados de la ONU ya se fueron, Momi ha tenido varios hijos y ya no parece resultar tan linda como antes. Desde hace un tiempo y gracias a un microcrédito tiene un pequeño negocio de peluquera en un pueblo rural al interior del país. “La peluquería consiste básicamente en una caseta, dos sillas y un espejo colgado junto a las extensiones y los utensilios con los que hace sus peinados. Gana lo suficiente para mantenerse por sí misma y no piensa casarse. No quiere ni oír hablar de matrimonio, ni de tener pareja fija.” Alguna vez se la ve con algún novio o un amigo, una no renuncia a los placeres de la vida, pero luego cada uno duerme en su casa. Nunca volverá a depender de ningún hombre, eso lo tiene claro.

“Y tengan ustedes en cuenta una cosa, esto, en el África rural donde ahora vive,constituye una auténtica revolución.”
