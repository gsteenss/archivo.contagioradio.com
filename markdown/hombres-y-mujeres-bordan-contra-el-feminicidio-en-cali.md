Title: Hombres y mujeres bordan contra el feminicidio en Cali
Date: 2015-11-13 13:13
Category: Mujer, Nacional
Tags: ‘Bordando para reparar ausencias’, ‘Ellas nos hacen falta’, Cali se moviliza contra el feminicidio, Colectivo Reparando Ausencias, Feminicidio en, Hombres y mujeres bordan contra el feminicidio en Cali, Violencia de género en Colombia
Slug: hombres-y-mujeres-bordan-contra-el-feminicidio-en-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Bordando-Ausencias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ellas Nos Hacen Falta 

###### [13 Nov 2105] 

[Hombres y mujeres integrantes del Colectivo 'Reparando Ausencias' y ciudadanos en general se reúnen en el Bulevar del Río Cali para bordar el nombre de aquellas mujeres que han sido víctimas de feminicidio en esta ciudad, con el fin de **dignificar sus historias**, **reparar sus ausencias**, **reconstruir memoria social**, afirmar un **NUNCA MÁS violencia de género** y hacer que la sociedad caleña comprenda las implicaciones negativas de este fenómeno.]

[De acuerdo con Paola Sánchez, integrante del Colectivo, esta **iniciativa ciudadana contra el feminicidio** tiene la intención fundamental de sensibilizar a la sociedad frente a una problemática que según organizaciones defensoras de derechos humanos, en lo corrido del año reporta un total de **72 mujeres asesinadas en Cali**, **17** de las cuales **corresponderían a feminicidios**, “no podemos dejar que esto siga sucediendo de manera tan impune sin que no nos importe”.]

[Esta movilización denominada ‘Bordando para reparar ausencias’ se da en el marco de la campaña ‘Ellas nos hacen falta’ y está articulada con acciones pedagógicas para la reflexión y visibilización de este fenómeno que alarma, pues **desde el noviazgo las formas de relacionarse física y emocionalmente son violentas, los jóvenes podrían estar replicando lo que ven en sus casas**, según indica Sánchez y en muchas ocasiones “las mujeres no saben que son víctimas o que tienen derechos” **cuando acuden a instancias para defenderse es porque temen por sus vidas**.]

[El Colectivo Reparando Ausencias, integrado por  hombres y mujeres de **diversos sectores sociales, económicos, políticos y culturales que se piensan nuevas formas de relacionarse**, brinda acompañamiento psicosocial y jurídico a familiares de víctimas de feminicidio en la ciudad de Cali y extienden la invitación para participar de esta movilización asistiendo a la **actividad de bordado desde las 4:30** pm o través del hashtag [[‪]
