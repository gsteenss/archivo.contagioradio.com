Title: Bosnia : entre la UE y el éxodo masivo
Date: 2015-03-17 20:09
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Bosnia entrada UE, Bosnia inmigración y desempleo, Bosnia-Herzegovina
Slug: bosnia-entre-la-ue-y-el-exodo-masivo
Status: published

###### Foto:Wikipedia.org 

La **UE ha dado el primer paso para la integración de Bosnia** en la institución comunitaria. Los ministros de exteriores de los países miembros llegaron a un **acuerdo de estabilización y asociación** para que el país balcánico pueda **pedir ayudas para realizar las reformas** necesarias para su entrada a la UE.

Con más de un **44% de desempleo real** el país aún arrastra los problemas derivados de la guerra que ha provocado un éxodo masivo de la población.

Después de la **guerra**, el éxodo fue debido a la situación política y **ahora 15 años después, la salida de ciudadanos se da** por la **crisis económica**, el desempleo y la complicada situación de gobernabilidad consecuencia de los acuerdos de Dayton para la terminación del conflicto armado.

Solamente en el **Este de Bosnia el ultimo año han emigrado más de 80.000 personas**, en un contexto en el que la **UE** o países como Eslovenia **acusan a los ciudadanos bosnios de demandar solicitudes de asilo político falsas**. Acusaciones que dejan en el aire la **posible suspensión de los visados Schengen** liberalizados para el caso de los ciudadanos de Bosnia. Acutalmente Alemania, Eslovenia y Reino Unido son algunos de los países que acogen la inmigración bosnia.

De la población que retornó después del conflicto armado, la gran mayoría fue a trabajar a Serbia y cerca del 70% no ha regresado.

Otro de los inconvenientes es la **difícil gobernabilidad del país**, hundido económicamente y dividido en una federación y en una república con entidades conjuntas y autónomas que impiden la gestión propia del Estado.

La situación estalló el año pasado con **grandes protestas** protagonizadas por todos los sectores de la población, especialmente los relacionados con la industria.

Las heridas de la guerra aún continúan presentes, son miles de refugiados los que aún no han retornado, más de 50 criminales de guerra a espera de ser juzgados y **un excedente de municiones y armamento de más de 17.000 toneladas**.
