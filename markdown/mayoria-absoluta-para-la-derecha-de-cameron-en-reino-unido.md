Title: Mayoría absoluta para la derecha de Cameron en Reino Unido
Date: 2015-05-08 20:34
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 2017 Referéndum sobre la UE en Reino Unido, Ascenso nacionalistas Escoceses, Cameron gana elecciones en Reino Unido, Partido Conservador Reino Unido gana elecciones, Peligra relaciones entre UE y REino Unido por ganar Cameron
Slug: mayoria-absoluta-para-la-derecha-de-cameron-en-reino-unido
Status: published

###### Foto:RTVE.es 

El **partido conservador de Cameron ha ganado con mayoría absoluta** las elecciones en Reino Unido.Con el **36.9 % de los votos excrutados y con 331 escaños** se ha posicionado como la primera fuerza política del país pudiendo gobernar sin pactos con otras formaciones políticas.

Las encuestas auguraban una leve subida pero no la mayoría absoluta. El proyecto económico que **ha sacado al país de la recesión y ha aumentado su PIB**, y **el miedo** ha la **dependencia** de un **gobierno fuerte Escocés** han impulsado a los votantes ha acudir en masa para apoyar al Partido Conservador.

La primera reacción de la **oposición** ha sido la **dimisión de los tres líderes de los principales partidos,** quienes han reconocido la victoria del líder conservador, incluso antes del final del escrutinio ya habían dimitido.

La victoria del partido conservador implica que las relaciones con la UE pueden quedar comprometidas por la convocatoria del **referéndum sobre la permanencia del Reino Unido en la unión en 2017.**

El ejecutivo conservador tiene entre sus manos la **negociación con la UE** sobre la permanencia en la misma y sobre el papel hegemónico de Reino Unido en Europa.

Cameron en su primera comparecencia ante la opinión pública ha dicho que va a dar gran **autonomía a las distintas naciones y en especial a la de Escocia.**

El primer ministro declaró “...Debemos asegurarnos de volver a **unir al país entero**...” y  “...Gobernaremos como el partido de una nación, un Reino Unido. Cumpliré mi palabra e implementaré lo más rápido posible la **devolución de poderes a Gales, Escocia e Irlanda del Norte...¨.**

La **derrota del partido laboral** ha sido histórica siendo esta reconocida en palabras de su propios lideres, que han informado de un **cambio futuro de las políticas** propuestas por el partido.
