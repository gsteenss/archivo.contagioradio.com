Title: Se agudiza crisis en el Hospital de Kennedy
Date: 2016-07-22 13:45
Category: Movilización, Nacional
Tags: crisis de la salud, EPS, Hospital de Kennedy
Slug: se-agudiza-crisis-en-el-hospital-de-kennedy
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Hospital-Kennedy.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Leonardo Cortés 

###### [22 Jul 2016] 

Médicos, enfermeras, y demás funcionarios del Hospital de Kennedy permanecen desde las 6 de la mañana frente a las instalaciones del centro hospitalario protestando por la unificación de la red de hospitales públicos, ya que según ellos, algunos salarios se reducirán **entre un 30% y 40%, sin contar con la falta de insumos para atender a los pacientes.**

De acuerdo con Darío Zapata, vocero del sindicato de los trabajadores del Hospital de Kennedy, durante las protestas **agentes del ESMAD llegaron para reprimir la manifestación, dejando 2 personas heridas,** una de ellas terminó con la mano fracturada y luego fue detenida, aunque en estos momentos ya se encuentra libre. Además, según las declaraciones de Zapata, los efectivos del ESMAD estaban lanzando gases lacrimógenos en la puerta del Hospital donde se encontraban pacientes con problemas respiratorios.

Los manifestantes aseguran que se trata de una protesta pacífica acompañada de carteles y arengas exigiendo respuestas por parte de la administración de Peñalosa y puntualmente del Secretario de Salud, Luis Gonzalo Morales. **“La situación es caótica, el Secretario de Salud congeló el presupuesto del hospital. Mantendremos la protesta, aunque nos indigna la actitud del ESMAD”.**

Y es que la crisis del Hospital continúa profundizándose. El Hospital de Kennedy, que es de tercer nivel cuenta con 389 camas, y solo en urgencias hay un sobrecupo de 160 pacientes, pues durante los fines de semana recibe 300% más de los pacientes que puede atender.  Así mismo, denuncian que **la administración redujo en un 50% los suministros hospitalarios,** no hay medicamentos suficientes, de cuatro ascensores solo funciona uno, no hay materiales para cirugías generales o de ortopedia y la mayoría de los trabajadores son contratistas que deben trabajar todos los días sin posibilidad de tener vacaciones o de pago de primas y dominicales, explica Dario Zapata.

Esa situación se repite a diario no solo en el hospital de Kennedy sino en los demás centros de la red hospitalaria de la capital, que están en esta situación por la falta de medidas eficaces por parte del distrito y del gobierno, y las millonarias deudas de las EPS. Por ejemplo, **al Hospital de Kennedy tres EPS que ya fueron liquidadas, Solsaud, Humana Vivir, Caprecom,  quedaron debiendo  más de \$70 mil millones.**

En total, las EPS deben a los hospitales públicos de Bogotá un billón 58.000 millones de pesos, es por ello que desde el Hospital de Kennedy, **se espera que en los próximos días se repliquen estas movilizaciones los demás centros asistenciales de la capital.**

<iframe src="http://co.ivoox.com/es/player_ej_12308358_2_1.html?data=kpegkp2XeZmhhpywj5aXaZS1lJ6ah5yncZOhhpywj5WRaZi3jpWah5yncaXV04qwlYqliNCfu8bdw9nFaZO3jK3c1dXNuMLgjMnSjbDJssbY2pKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
