Title: Información entregada a Unidad de Búsqueda por FARC esclarecería paradero de 276 personas
Date: 2019-08-20 16:30
Author: CtgAdm
Category: DDHH, Nacional
Tags: FARC, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: informacion-entregada-a-unidad-de-busqueda-por-farc-esclareceria-paradero-de-276-personas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Unidad-de-Búsqueda-de-personas-dadas-por-desaparecidas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UBPD] 

El partido FARC hizo entrega a la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD) de 276 casos documentados para la búsqueda, localización e identificación de personas que desaparecieron en el marco del conflicto armado en 15 departamentos del país, esto como parte del compromiso adquirido para realizar este proceso previo al funcionamiento del Sistema de Verdad, Justicia, Reparación y No Repetición.

Para lograr este cometido, 71 personas del partido FARC, 38 hombres y 33 mujeres, que han articulado su proceso de reincorporación a tareas humanitarias de búsqueda de personas, recolectaron desde 2017 información que ha permitido establecer que de estos 276 formatos, un 64%  representan a integrantes de grupos armados al margen de la ley, **28%  a civiles y 1 % a personas de la Fuerza Pública, mientras el 7%** restante no fue especificado. [(Lea también: Pese a recortes presupuestales la Unidad de Búsqueda de Desaparecidos no se detiene)](https://archivo.contagioradio.com/pese-a-recortes-presupuestales-la-unidad-de-busqueda-de-desaparecidos-no-se-detiene/)

### 72% de los casos de personas desaparecidas ocurren entre 1997 y 2007

La entrega de información se coordinó con Medicina Legal y el Comité Internacional de la Cruz Roja, (CICR), trazando una hoja de ruta que permitió la documentación humanitaria de 276 personas que a la fecha continúan desaparecidas en los departamentos de **Antioquia, Chocó, Tolima, Valle del Cauca, Nariño, Cauca, Huila, La Guajira, Cesar, Norte de Santander, Arauca, Meta, Caqueta y Guaviare** entre 1981 y 2016.

"La Unidad confía que esta entrega sea el primer paso de muchos por parte de FARC para la búsqueda, identificación y entrega digna de personas desaparecidas y que sean cada vez más los excombatientes que se acerquen a entregar información"  expresó su directora, Luz Marina Monsón. [(Le puede interesar: Unidad de Búsqueda comienza piloto de identificación de 2.100 cuerpos en Norte de Santander y en Nariño)](https://archivo.contagioradio.com/unidad-busqueda-piloto-narino-norte-santander/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
