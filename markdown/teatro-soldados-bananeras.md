Title: "Soldados" una mirada a la masacre de las bananeras
Date: 2017-07-17 13:50
Category: eventos
Tags: Alvaro Cepeda Zamudio, Bananeras, soldados obra de teatro
Slug: teatro-soldados-bananeras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/SOLDADOS_BR.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corporación Colombiana de Teatro 

###### [19 Jul 2017] 

Del 20 al 22 de Julio vuelve a la Sala Seki Sano en tres funciones la obra "Soldados", pieza teatral adaptada de uno de los capítulos del libro "La casa grande" del escritor barranquillero Alvaro Cepeda Zamudio.

La obra, narra el viaje de dos soldados asignados a disuadir las protestas de los huelguistas contra la compañía americana United Fruit Company, ocurrida a finales de los años 20 en el municipio de Ciénaga, Magdalena, en la que fueron asesinados gran parte de los sindicalizados en lo que que pasaría a la historia como "La masacre de las bananeras".

Uno de los más aterradores crímenes de estado, se traslada a las tablas gracias al trabajo de "Tramaluna Teatro", bajo la batuta de la dramaturga Patricia Ariza, directora del Festival, y protagonizada por Ángela Triana y Lina Támara, calzando las botas de los dos soldados, quienes a través del diálogo construyen sus vivencias y percepciones en la diatriba de cumplir con el deber por encima de sus convicciones. Le puede interesar: [Patricia Ariza una vida por la cultura y la paz](https://archivo.contagioradio.com/patricia-ariza-una-vida-por-la-cultura-y-la-paz/)

La obra, interpretada en múltiples oportunidades en diferentes escenarios, con hombres como protagonistas principales, presenta como novedad la incorporación de actrices en los roles principales, aportando a la construcción de cultura por la paz, con perspectiva de género, apuesta principal del Festival.

Las funciones de "Soldados" inician a las 7:30 de la noche, en la Sala Seki Sano, calle 12 \# 2- 65.  El costo de la entrada para público general es de \$20.000, estudiantes \$12.000 .
