Title: Consulta Popular Minera en Ibagué por fin tiene vía libre
Date: 2016-12-16 12:45
Category: Ambiente, Nacional
Tags: Comité Ambiental en Defensa de la Vida del Tolima, Consulta Popular Minera en Ibagué, Gran Minería en Colombia, Ibagué
Slug: consulta-popular-minera-ibague-fin-via-libre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Consulta-Popular-Minera-en-Ibagué.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corpenca] 

###### [16 Dic 2016] 

La comunidad de Ibagué recibió con alegría la noticia que la Consulta Popular Minera que busca frenar **35 títulos concedidos a megaproyectos que representan 44.000 hectáreas de la capital tolimense**, fue aprobada por el Consejo de Estado.

Jaime Tocora integrante del Comité Ambiental de Ibagué, dijo que **"la tarea siguiente es reajustar la última parte de la pregunta  **“... impliquen contaminación del suelo, pérdida o contaminación de las aguas o afectación de la vocación agropecuaria y turística del municipio?”, pues el Consejo considera que es imparcial y puede generar confusiones en los votantes.

### **Multinacionales v.s Defensa Ambiental** 

Por otra parte, el defensor ambiental señaló que “uno de los grandes obstáculos ha sido la compra de conciencias por parte de la Anglo Gold Ashanti, **incluso ha comprado concejales para echar abajo los logros de la Consulta en Piedras Tolima”**.

Sin embargo, puntualizó que "aunque la Anglo Gold siga comprando a políticos, tenemos una ciudadanía concientizada y que defiende su territorio, **la gente ya abrió los ojos y ha respondido a las demandas por el bienestar comunitario**, eso no lo pueden comprar”. Le puede interesar: [Suspenden consulta minera pero no el trabajo de ambientalistas en Ibagué.](https://archivo.contagioradio.com/suspenden-consulta-minera-pero-no-el-trabajo-de-ambientalistas-en-ibague/)

Tocora indicó que si bien aunque sea favorable el resultado de la Consulta, no se afectarán los proyectos que ya estén en funcionamiento, lo que quiere la comunidad de Ibagué es "sentar un precedente de que **una comunidad organizada que hace uso efectivo de los elementos constitucionales como Consultas Populares puede frenar a futuro este tipo de proyectos”.**

Por último, el defensor ambiental comentó que el próximo **22 de Enero de 2017 será realizada La Consulta en Cajamarca para frenar el proyecto de La Colosa** y esperan tener durante ese mes una fecha para la Consulta de Ibagué.

<iframe id="audio_15076735" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15076735_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
