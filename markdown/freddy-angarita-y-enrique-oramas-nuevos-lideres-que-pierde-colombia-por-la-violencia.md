Title: Freddy Angarita y Enrique Oramas, nuevos líderes que pierde Colombia por la violencia
Date: 2020-05-17 13:19
Author: AdminContagio
Category: Actualidad, Líderes sociales
Tags: Cúcuta, Líder Social
Slug: freddy-angarita-y-enrique-oramas-nuevos-lideres-que-pierde-colombia-por-la-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Freddy-Angarita-y-Enrique-Oramas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este sábado 16 de mayo se conoció del asesinato de Freddy Angarita, líder comunal en Cúcuta (Norte de Santander) y Jorge Enrique Oramas, líder ambiental en zona rural de Santiago de Cali (Valle del Cauca). Según el Instituto de Estudios para el Desarrollo y la Paz [(INDEPAZ)](http://www.indepaz.org.co/paz-al-liderazgo-social/), solo en este año han sido asesinados ya 100 líderes sociales y defensores de derechos humanos en Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Freddy Angarita, líder comunitario de Cúcuta

<!-- /wp:heading -->

<!-- wp:paragraph -->

En horas de la mañana de este sábado hombres armados llegaron al barrio La Isla, en Cúcuta, y dipararon en múltiples ocasiones contra el líder comunal Freddy Angarita. Angarita acanzó a ser trasladado a centro hospitalario con vida, pero dada la gravedad de sus heridas falleció minutos más tarde.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las primeras informaciones, Freddy Angarita se desempeñaba como uno de los líderes de un proceso de toma de tierras sobre el anillo vía occidental. Adicionalmente, de acuerdo a[medios locales](https://www.laopinion.com.co/judicial/matan-un-lider-comunitario-en-cucuta-196416#OP), en el avance de la investigación se conoció que el líder había recibido una amenaza de muerte recientemente.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Jorge Enrique Oramás, líder ambiental que protegía los farallones de Cali

<!-- /wp:heading -->

<!-- wp:paragraph -->

De igual forma, este sábado, armados habrían asesinado en su casa a Jorge Enrique Oramas, líder ambiental de 70 años que fue encontrado en su finca, en la vereda la Candelaria (zona rural de Cali) con un disparo en su cuerpo. (Le puede interesar: ["Asesinan a Tailor Gil, líder comunal de cáceres"](https://archivo.contagioradio.com/asesinan-a-tailor-gil-lider-comunal-de-caceres/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las primeras informaciones, el defensor ambiental tenía un papel en la lucha contra la minería ilegal en los farallones de Cali. Oramas era socioologo de BIOCANTO y orientador de la iniciativa los Nuevos Maestros del agua, una iniciativa ciudadana, campesina, rural y académica de defensa del territorio en la capital de Valle del Cauca.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/luis.tasceche/videos/10155633765433770","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/luis.tasceche/videos/10155633765433770

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
