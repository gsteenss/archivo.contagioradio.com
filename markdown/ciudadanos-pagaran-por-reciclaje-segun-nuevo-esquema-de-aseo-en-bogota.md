Title: Ciudadanos pagarán por reciclaje según nuevo esquema de aseo en Bogotá
Date: 2016-06-21 14:27
Category: Economía, Nacional
Tags: administración Enrique Peñalosa, nuevo esquema de aseo Bogotá, Unidad Administrativa Especial de Servicios Públicos
Slug: ciudadanos-pagaran-por-reciclaje-segun-nuevo-esquema-de-aseo-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Reciclaje.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía Bogotá] 

###### [21 Junio 2016 ]

Este martes Beatriz Elena Cárdenas, actual directora de la Unidad Administrativa Especial de Servicios Públicos UAESP, dio a conocer el nuevo esquema de aseo para Bogotá que podría costar por lo menos \$5 billones. Dentro de las principales modificaciones que se contemplan está **la obligación que tendrán los ciudadanos de pagar en sus facturas una remuneración para los recicladores,** que llegará a ellos a través de los nuevos prestadores del servicio.

Según afirmó Cárdenas todos los **recicladores deben estar inscritos en organizaciones que se entenderán directamente con los prestadores del servicio**, que pueden ser públicos o privados y la relación será netamente económica, pues las condiciones de funcionamiento de los recicladores serán determinadas en el 'Plan de Gestión Integral de Residuos Sólidos' que se espera sea sancionado en un mes.

En este esquema se contempla la libre competencia de oferentes públicos y privados que celebrarán contratos de concesión, cuya licitación se estima sea abierta en septiembre de este año, la adjudicación en diciembre y la ejecución en febrero o marzo de 2017. De acuerdo con el anuncio, la Comisión de Regulación de Agua Potable y Saneamiento Básico, será la entidad encargada de verificar las condiciones para la celebración de los contratos y la implementación de las denominadas Áreas de Servicio Exclusivo.

En total se estima que sean cinco las Áreas de Servicio Exclusivo, la primera en Usaquén y Chapinero; la segunda conformada por Santa Fe, Candelaria, Mártires, Antonio Nariño, San Cristóbal, Usme y Sumapaz; una tercera integrada por Ciudad Bolívar, Rafael Uribe, Tunjuelito y Bosa; la cuarta por Kennedy, Puente Aranda, Teusaquillo y Fontibón; y la última por Suba, Engativá y Barrios Unidos. **En cada una de las Áreas habrá un prestador diferente y se establece que ningún operador podrá prestar el servicio en más de un área**.

https://www.youtube.com/watch?v=dpCovr4Or\_A

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

   
   
   
 
