Title: "Bailando es como se acaban las guerras" comunidad de Conejo
Date: 2017-01-04 12:44
Category: Nacional, Paz
Tags: Baile, Campamentos, Conejo, FARC, Guajira, ONU
Slug: bailando-es-como-se-acaban-las-guerras-comunidades-de-conejo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/IMG_0820.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Pilón] 

###### [4 Enero 2017] 

Luego de la noticia dada a conocer a través de medios de comunicación, en la que se mostraba cómo algunos integrantes de la ONU bailaban con guerrilleras de las FARC en la celebración de fin de año en Conejo, Guajira, **la comunidad de esta región de Colombia ha entregado una comunicación en la que aseguran que este escándalo es parte de “sectores que se han opuesto al proceso de paz”.**

En el comunicado, la comunidad de Conejo, relata cómo fue esa noche de festejo en la que estuvieron presentes algunos miembros de la comunidad, con familiares de guerrilleros y algunas organizaciones sociales.

“Lo que pudimos ver” dicen en la carta “es como personas de otras culturas y de otros países se compenetraban con el momento, con nuestras costumbres y accedieron a bailar el 31 de diciembre despidiendo el año” y lanzan la pregunta **“¿si un baile de celebración aquí en la guajira es malo para los enemigos de la paz, qué se espera de los otros problemas que hay que resolver?”. **Le puede interesar: [Sin víctimas de población civil se cumplen 4 meses de cese al fuego](https://archivo.contagioradio.com/sin-victimas-de-la-poblacion-civil-se-cumplen-4-meses-del-cese-al-fuego/)

Manifiestan que **esta comunidad a la que “los enemigos de la paz” le “ha negado todo”, prefiere ver bailando a todos:** “soldados policías, observadores y opositores y que llenen nuestros territorios de entusiasmo y de fraternidad, en lugar de enfrentarse a muerte; así es como se acaban las guerras, no con intrigas desde Bogotá”.

De igual modo, afirman que para la comunidad de Conejo el proceso de paz es como “una bendición que nos devuelve la esperanza, después de tantos años de estar en el olvido y el abandono”.

Y concluyen realizando **una recomendación a la ONU “no caiga en la trampa de los enemigos de la paz y sus medios de comunicación.** Todos siguen siendo nuestros invitados. No dejen que se imponga el rencor, la tristeza y el escándalo que perjudiquen a policías, soldados o guerrilleros”.

Según la comunidad de Conejo **próximamente convocarán a un gran baile por la Reconciliación en Conejo y en los campamentos de las FARC en el que además invitarán “a los enemigos de la paz”. **Le puede interesar: [Entregan alimentos descompuestos a guerrilleros de las FARC en zonas de preagrupamiento](http://bit.ly/2iJVmYz)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
