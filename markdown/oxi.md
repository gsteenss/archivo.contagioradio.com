Title: OXI
Date: 2015-07-07 07:41
Category: Eleuterio, Opinion
Tags: Grecia, Referedum, Troika
Slug: oxi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/OXI.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [contraindicaciones] 

#### [**Por [Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)**] 

###### [7 Jul 2015] 

De entre el elenco de filósofos de la antigua Grecia, uno destaca por su opacidad y cierta marginalidad con la que es tratado en los libros de historia y filosofía. Heráclito de Éfeso, llamado el Oscuro por sus contemporáneos a razón de lo enigmático de sus aforismos, puso las bases de una filosofía olvidada por las grandes corrientes del pensamiento occidental. Sólo alguien tan énigmatico como él, el alemán Fredrich Nietzsche, lo recuperó en parte. Entre las breves sentencias que nos dejó, en su mayoría cargadas de una enorme fuerza, rescatamos ahora una que parece apropiada para los tiempos que corren en su tierra más de dos mil años después. El destino era para los antiguos griegos uno de los enigmas tratados en su mitología, sus tragedias y su posterior filosofía, visto siempre como algo inevitable y fuera del alcance humano. Heráclito se opuso: *el destino de una persona depende de su carácter.*

Parecería que el destino del pueblo griego, al igual que el de sus héroes antiguos como Edipo o Aquiles estaba irremediablemente trazado de antemano por fuerzas superiores, extrañas e intangibles como las de la poderosa Troika, al estilo de los dioses del Olimpo mitológico. Desde las alturas de los grandes edificios europeos se han planeado los caminos, los plazos, los pasos y las formas en que la vida del pueblo griego debía desarrollarse, con esa opacidad y misterio tan propios de la niebla que ocultaba la cima del monte Olimpo. Los sucesivos “rescates” y las medidas de austeridad que los acompañan, han tratado de coaccionar la y dirigir el devenir de Grecia, siempre de forma ajena a la voluntad de  su gente. Formas de una presión constante y una intimidación contínua que repercutía de manera directa en el día a día de la población griega.

Con todo y durante todos estos años, siempre ha habido en Grecia quien ha luchado contra ese destino impuesto, desde el primer momento. Esta resistencia ha llegado a un momento crucial con el referendum del pasado domingo. Desde su convocatoria llevada a cabo por un gobierno que nadie quería salvo el propio pueblo griego que lo eligió, las amenazas de los poderosos señores de la Troika fueron una constante. No faltaron las catastróficas predicciones de los pretendidos oráculos expertos, amplificadas por los medios de comunicación a lo largo de todo el continente. El corralito, la salida del euro, la ruina, han sido los fantasmas que asomaban para amedrentar y condicionar el voto, es decir la voluntad de la gente.

Pese a todo, el pueblo griego no se dejó amedrentar. Por mucho que los analistas internacionales expliquen al mundo lo precario de su situación y señalen lo que les conviene hacer, sólo los griegos saben realmente lo que llevan viviendo durante los últimos años. Tal vez no sepan todavía cómo encauzar su más que complicado presente hacia un futuro esperanzador pero seguro saben que no quieren ser esclavos de los dictados del neoliberalismo y así lo manifestaron. Ha sido una demostración de dignidad, de valentía y como decía Heráclito, de carácter. Con ello, también han mandado un poderoso mensaje al mundo, el de que se tienen que contar con la gente si es verdad que, como se pretende decir, vivimos en democracia. La dignidad humana no se negocia por más que desde el Olimpo caigan rayos, suenen truenos o se agiten la tierra y los mares. Toda una lección griega: pase lo que pase el pueblo es siempre  dueño de su propio destino.
