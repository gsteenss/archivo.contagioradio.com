Title: 176 detenidos en cárcel de Cómbita iniciaron huelga de hambre
Date: 2017-02-15 13:36
Category: DDHH, Nacional
Tags: carceles, Combita, ELN, FARC, Huelga de hambre
Slug: 176-detenidos-en-carcel-de-combita-iniciaron-huelga-de-hambre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/carcel-de-combita-914x607.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: pulzo] 

###### [15 Feb 2017]

El pasado 13 de Febrero, 176 internos del patio 1 de la cárcel de máxima seguridad de Combita, en el departamento de Boyacá iniciaron una **huelga de hambre para reclamar atención en salud para 27 internos. Además denuncian el hacinamiento y la ausencia de clasificación para que se garantice su seguridad.**

Según un comunicado de los presos políticos y sociales, los afectados por diversas enfermedades no son remitidos a los especialistas y adicionalmente se niega la posibilidad de la realización de las terapias ordenadas como tratamiento a sus enfermedades. (Lea también, [la Huelga de Hambre es una de las únicas opciones para reclamar derechos en las carceles](https://archivo.contagioradio.com/502-internos-de-15-caceles-del-pais-se-suman-a-la-huelga-de-hambre/))

Uno de los casos que mencionan es de **Norberto Manrique Bernal, integrante del ELN, quien padece de una parálisis facial desde hace más de 25 días sin que hasta el momento se le haya prestado la atención necesaria.** (Lea también [Situación de salud es precaria y agravada en las carceles](https://archivo.contagioradio.com/carceles-colombianas-no-cuentan-ni-con-ibuprofeno-para-atender-presos-enfermos/)).

Otra de las situaciones que motiva la protesta pacífica de las personas recluidas en el Pabellón 1 de Cómbita **es el hacinamiento y la falta de clasificación**. Según la denuncia en muchos casos se encuentran en las mismas celdas guerrilleros, paramilitares o internos con enfermedades mentales, lo que pone en riesgo las vidas de muchos de los detenidos.

Para ellos la responsabilidad de esta situación recae sobre el mismo **Sistema Penitenciario de Colombia, que no garantiza la destinación adecuada de los recursos**, “Hacemos responsable a todo el sistema carcelario y penitenciario nacional de todas las falencias que padecemos por su mala administración de los recursos que la nación aporta a nuestro sostenimiento”, entre las instituciones señaladas están el INPEC, el Ministerio de Justicia y el de Protección Social.

Una de las exigencias es que se cumplan las ordenes de tutela en que la **Corte Constitucional declara un estado de “Orden de cosas inconstitucional”** ordena la creación de un grupo líder para el seguimiento y cumplimiento, conformado por la Defensoría del Pueblo, la Procuraduría General de la Nación y la Presidencia de la República. (Le puede interesar [Situación de las cárceles en Colombia es preocupante](https://archivo.contagioradio.com/condiciones-en-las-carceles-colombianas-son-preocupantes-alan-duncan/))

El comunicado finaliza con un llamado de acompañamiento a la protesta por parte de organismos internacionales como el CICR y de organizaciones de Derechos Humanos como la Fundación Lazos de Dignidad o el Comité de Solidaridad con **Presos políticos entre otros para que sigan desarrollando su labor de denuncia.**

###### Reciba toda la información de Contagio Radio en [[su correo]
