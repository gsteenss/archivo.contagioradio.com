Title: CIA y Fuerzas Armadas de Estados Unidos habrían cometido crímenes de guerra
Date: 2016-11-15 16:24
Category: DDHH, El mundo
Slug: cia-y-fuerzas-armadas-de-estados-unidos-habrian-cometido-crimenes-de-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Chris-Hondros-oct-15-2009-Afganistán-e1479244337561.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Chirs Hondros] 

###### [15 Nov 2016] 

La Corte Penal Internacional señaló en un informe, que la Agencia Central de Inteligencia (CIA) y las fuerzas armadas de Estados Unidos abrían recurrido a **“torturas, tratamiento cruel, ultrajes a la dignidad humana y violaciones”** en los interrogatorios a detenidos en Afganistán y otros países, en campos secretos de detención. Esta sería la base de cientos de casos que comprobarían que Estados Unidos cometió crímenes de guerra entre los años 2003, 2004 y 2014.

De acuerdo con el informe, se habría establecido que miembros de las **Fuerzas Armadas de Estados Unidos habrían sometido al menos a 61 personas a detenidas a torturas, mientras que la CIA, habría torturado a otras 27 personas**. Las violaciones a derechos humanos aparentemente se realizaron en las 34 provincias de Afganistán.

En el texto se indica que “la información disponible sugiere que **las víctimas fueron sometidas a violencias física y sicológica y se presume que los crímenes fueron perpetrados por particular crueldad**, de tal forma que ultrajaron la dignidad humana básica de las víctimas” además agrega que “los abusos no habrían sido cometidos por unos cuantos individuos aislados. Hay indicios de que se cometieron como parte de técnicas de interrogación aprobadas”.

Sin embargo, pese a que el informe cuenta con una base razonable de datos, la Corte Penal Internacional aún no ha solicitado una autorización para la apertura de una investigación exhaustiva de los casos reportados en Afganistán.  A su vez, Estados Unidos tampoco ha reconocido la autoridad de esta corte y con este informe no solo se pone en la mira a unos soldados sino a toda una cadena de mando. Le puede interesar:["Suicidio militar de Estados Unidos"](https://archivo.contagioradio.com/suicidio-militar-de-estados-unidos/)

De otro lado también estarían los **crímenes de guerra cometidos por las fuerzas del estado Afgano**, las milicias talibanas y los grupos afiliadas a estas desde el año 2001 que de igual forma habrían cometido crímenes de guerra.

La Corte Penal Internacional es la primera corte permanente con capacidad para castigar a los responsables de crímenes de agresión, crímenes de guerra, crímenes de  lesa humanidad y genocidios. En lo que lleva de su creación ha dictado 26 órdenes de arresto y en la actualidad tiene 8 investigaciones en curso y 8 exámenes preliminares.

######  Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
