Title: Otra Mirada: Plebiscito por la paz: Cuatro años después
Date: 2020-10-05 21:22
Author: PracticasCR
Category: Otra Mirada, Programas
Tags: acuerdo de paz, plebiscito por la paz
Slug: otra-mirada-plebiscito-por-la-paz-cuatro-anos-despues
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Plebiscito-e1470157319868.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Las 2 Orillas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El 2 de octubre de 2016, se realizó el Plebiscito para aprobar los acuerdos entre el gobierno de Colombia y la guerrilla de las FARC-EP, en el que con un 50,2% ganó el NO. Ante esto, al país llegó la incertidumbre de qué podría pasar con la paz y con el esperado fin del conflicto armado. (Le puede interesar: [FARC- EP reconoce ante la JEP la verdad sobre 6 homicidios, entre ellos el de Álvaro Gómez Hurtado](https://archivo.contagioradio.com/farc-ep-reconoce-ante-la-jep-la-verdad-sobre-6-homicidios-entre-ellos-el-de-alvaro-gomez-hurtado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el programa de Otra Mirada, reflexionando y compartiendo sobre lo que ha pasado a cuatro años del plebiscito, hicieron parte del panel Gustavo Arbeláez, dirigente del Partido FARC Valle del Cauca, María Miyela Riascos, lideresa del Paro Cívico de Buenaventura y Sebastián López, director del departamento de Ciencia Política de la Pontificia Universidad Javeriana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los invitados inician comentando cuál fue la sensación, sobre todo desde los territorios afectados por la violencia y desde el partido FARC, ese 2 de octubre al saber que el país le decía No a la paz y señalando que a cuatro años de la firma, los cambios lamentablemente no han sido muchos y continúa la violación a los derechos de los pueblos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, resaltan la importancia de educar y pedagogizar a los ciudadanos a la hora de tomar decisiones, con el fin de evitar ser engañados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, agregan qué sucedería si el plebiscito se volviera a convocar en estos momentos y explican que el acuerdo sentó una línea que debe continuar para llegar a la paz. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, señalan que aún se mantienen las esperanzas para que se ponga fin a los conflictos armados y llegue la paz al país. (Si desea escuchar el programa del 2 de octubre: Otra Mirada:[El legado de Quino](https://www.facebook.com/contagioradio/videos/1124308887967284))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Si desea escuchar el análisis completo:** [Haga click aquí](https://www.facebook.com/contagioradio/videos/662932944618865)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
