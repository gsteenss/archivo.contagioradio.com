Title: Más de 15 mil personas están en riesgo por inundaciones en Guainía
Date: 2018-07-30 14:19
Category: Nacional
Tags: colombia, Guania, indígenas, inundaciones
Slug: mas-de-15-mil-personas-estan-en-grave-riesgo-por-inundaciones-en-guainia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/canal1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Canal 1] 

###### [30 Jul de 2018] 

Las inundaciones en el departamento del Guainía dejan como saldo más de 15 mil personas afectadas que se encontrarían en grave riesgo debido a que el hospital local se encuentra desbordado en atención, **solo el 38% de las personas damnificadas están recibiendo alimentación que,** además, no tiene en cuenta el enfoque diferencial de la población en su mayoría indígena.

De acuerdo con Laura Osorio, oficial de gestión de riesgos de OXFAM Colombia, se calcula que son 3.482 familias las damnificadas y un total de 6 municipios de 8 que conforman el departamento, los que estarían en alerta roja. De igual forma, Osorio aseguró que aún **no se ha podido establecer si hay población confinada en sus viviendas debido al difícil acceso que en este momento hay a los territorios** y a los altos costos que implica movilizarse en transporte fluvial.

### **Las lluvias y el cambio del uso del suelo** 

Para Osorio, el aumento de las lluvias hace parte del cambio climático que se está registrando en el mundo, sin embargo, afirmó que el cambio en el uso de los suelos del Guainía, más la deforestación que ha aumentado en los últimos años**, logró profundizar los daños de las lluvias y generar las inundaciones**.

En ese sentido manifestó que las poblaciones que están siendo víctima de estas inundaciones corresponden en su gran mayoría a comunidades indígenas, hecho por el cual las medidas y **ayudas otorgadas por el gobierno y las autoridades deben tener en cuenta las diferencias culturales**. (Le puede interesar: ["121 familias se encuentran confinadas por paramilitares: Pueblos indígenas"](https://archivo.contagioradio.com/121-familias-se-encuentran-confinadas-por-paramilitares-pueblos-indigenas/))

“Se requiere un censo con enfoque prioritario para saber las necesidades de la población y conocer los riesgos que enfrentan en el marco de la catástrofe ambiental” expresó Osorio y añadió que se necesita garantizar la seguridad alimentaria, y mejorar el sistema de salud para evitar riesgos, priorizando el acceso del agua potable para las comunidades.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
