Title: Pese a recortes presupuestales la Unidad de Búsqueda de Desaparecidos no se detiene
Date: 2019-03-29 17:04
Category: DDHH, Paz
Tags: Sistema Integral de Justicia, Unidad de Búqueda de personas desaparecidas
Slug: pese-a-recortes-presupuestales-la-unidad-de-busqueda-de-desaparecidos-no-se-detiene
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/D0mzYfdW0AIxcze.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Justiciaypazcol] 

###### [29 Mar 2019] 

Según lo estipulado en el decreto 1395 de 2018, el Gobierno otorgaría a **la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD)**, un presupuesto de forma gradual en tres fases que permitiría la contratación de 156 cargos, equivalente al 30% en 2018, 261 cargos en 2019 correspondientes al 50% y el 20% restante incluiría otros 105 cargos para 2020; completando 522 cargos, sin embargo, un recorte del Gobierno para 2019 ha afectado directamente el desempeño de la entidad.

En principio, el presupuesto solicitado para la UBPD en 2019 correspondiente a recursos de funcionamiento, fue de \$104.109 millones para personal destinados para gastos generales y la contratación de 417 personas según la estipulado en el decreto para 2018 y 2019; sin embargo solo fueron aprobados** \$33.332 millones que están divididos en: \$25.662 millones para personal y \$6.416 gastos generales.**

Aunque los \$33.332 millones ya están asignados, **estos solo cubren la contratación de 58 de los 261 servidores** que estaba previsto contratar para 2019, lo que quiere decir que hay pendientes por financiar 203 cargos además de cubrir los gastos generales, además la no contratación de estas personas, de las que 103 estaban destinados a trabajar en territorio, retrasaría la proyección de trabajo de la UBPD.

Aunque la UBPD continúa funcionando, administrando lo mejor posible los recursos otorgados y cuenta con la cooperación internacional, aun son necesarios **\$12.416 millones adicionales, divididos en \$9.933 millones para cubrir las plazas estipuladas en el decreto y otros  \$2.483 millones** para gastos generales de una entidad que trabajará los siguientes 20 años para conocer el paradero de las personas desaparecidas en el marco del conflicto.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
