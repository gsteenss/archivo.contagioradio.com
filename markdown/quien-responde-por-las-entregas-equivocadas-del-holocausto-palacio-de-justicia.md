Title: ¿Quién responde por las entregas equivocadas del holocausto palacio de justicia?
Date: 2018-03-01 15:16
Category: DDHH, Nacional
Tags: Alfonso Jacquin, M-19, toma y retoma del palacio de justicia
Slug: quien-responde-por-las-entregas-equivocadas-del-holocausto-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/holocausto-palacio-de-justicia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [01 Mar 2018] 

Con la identificación de los cuerpos de los dos integrantes del M-19 Norlaba Trujillo y Alfonso Jacquin y del escolta Libardo Durán, quien se encontraba en el Palacio de Justicia, continúan abriéndose las preguntas frente a **qué fue lo que pasó durante el 6 y 7 de noviembre de 1985 en las labores de levantamiento** de la escena del crimen hechas por el Ejército y quiénes deben responder frente a estos hechos.

Para el abogado Eduardo Carreño, defensor de los familiares víctimas de la toma y retoma del Palacio de Justicia el Estado nunca ha dado una respuesta al por qué se entregaron cuerpos que no correspondían a los familiares, “en el caso de Libardo Durán, se entregaron unos restos calcinados sin ningún elemento que determinara que era de él, **simplemente dijeron esto corresponde a su familia y la familia, honestamente, lo recibe ante la entrega oficial del Estado**”.

Sin embargo, 30 años después, Médicina Legal y la Fiscalía determinaron que en realidad se trataban de los restos de Noralba Trujillo y Alfonso Jacquin. No obstante, la particularidad de encontrar a Alfonso Jacquin radica en que, de acuerdo con las declaraciones de Edilberto Sánchez Rubiano, el jefe del B2 de Ejército, **Jacquin se encontraba vivo y herido en el primer piso del Palacio de Justicia**.

Para el abogado Carreño ahora se debe responde por qué entonces aparece en el cuarto piso, con sus restos óseos incinerados y entregado a otra familia. Los restos de Durán, **fueron encontrados en una fosa común en el Cementerio del Sur de Bogotá**, en donde también había cuerpos de personas de la tragedia de Armero.

### **¿Quién debe responder por las entregas de los cuerpos de las víctimas del Palacio de Justicia?** 

Eduardo Carreño señaló que frente a esta situación la Fiscalía debe continuar con las investigaciones y quienes fueron los altos mandos del Ejército durante la toma y retoma del Palacio, a cargo del operativo, entre los que se encontraban el Coronel Plazas Vega y el Coronel Hernández López, **deben responder por qué se entregaron mal los cuerpos, por qué se alteró la escena, por qué se trastearon los cadáveres y qué intenciones había para hacerlo**.

Además, Carreño manifestó que aún **faltan 34 cajas que contienen restos óseos por ser investigadas e identificadas** y actualmente se estaría en el trabajo de comparación entre los restos encontrados con el ADN de los familiares a los que fueron entregados. (Le puede interesar: ["Ejército deberá responder por torturas a 11 personas, durante la retoma del Palacio de Justicia"](https://archivo.contagioradio.com/torturas_palacio_justicia_retoma/))

Los familiares de los integrantes del M-19, continúan en el proceso de investigación en demandas ante el Consejo de Estado para que se responda por el paradero de 9 miembros de esta guerrilla que aún continúan desaparecidos.

<iframe id="audio_24158206" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24158206_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
