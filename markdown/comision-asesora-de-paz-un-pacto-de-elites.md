Title: Comisión Asesora de Paz, un Pacto de élites
Date: 2015-03-17 12:52
Author: CtgAdm
Category: Camilo, Opinion
Tags: Comisión Asesora de Paz, conversaciones de paz, Juan Manuel Santos, proceso de paz
Slug: comision-asesora-de-paz-un-pacto-de-elites
Status: published

###### Foto: SIG - César Carrión 

#### Por [[**Camilo De Las Casas**]](https://archivo.contagioradio.com/camilo-de-las-casas/) 

La composición de la Comisión Asesora de Paz, CAP, que Santos  anunció hace unas pocas horas es la nítida demostración que su paz es pax Romana (desarme), una recomposición del establecimiento para las nuevas fases de inversión y una simulación de inclusión de lo disidente o de lo diferente (negros e indígenas, ex M-19 y Polo Democrático.

Lo que resulta más cuenta chistes es que haya dicho con el anuncio es que la paz no es del Presidente sino de toda la sociedad, y que, ensus propias palabras atribuya las mismas funciones del Consejo Nacional de Paz, dándole a éste un golpe de gracia. “Con esta Comisión queremos ampliar el espectro de las personas que, al lado del presidente, enriquezcan la reflexión y contribuyan al proceso, manteniendo su independencia crítica. La convocaré cada vez que considere necesario para que el equipo negociador les informe de los avances del proceso, y oiré sus recomendaciones con toda atención”.

Es un chiste no hay ampliación de los sectores sociales que deben ser consultados, se trata de una selección elitosa, perdón la redundancia,[ ]{.Apple-converted-space}que refleja otros afanes como el de lograr antes de octubre el mayor consentimiento del establecimiento y de otras voces para su proceso de pax. Santos está pensando en octubre. En las elecciones regionales y locales para lograr mayorías pero también, como ya se rumora, convocar a una milagrosa papeleta, para que la gente diga sí o no a la paz, hábil jugada para presionar a las guerrillas y para salir airoso frente a un proceso de conversaciones con las FARC EP en el que no se han abordado los asuntos gruesos y en que el proceso con el ELN no arranca aún.

Para el presidente, la CAP que tiene un carácter incluyente y pluralista y supuestamente reúne las diversas tendencias de la opinión. Pero acaso, los excandidatos presidenciales Antanas Mockus, Clara López, actual presidenta del Polo, y Martha L Ramírez o el expresidente Pastrana, del partido conservador;[ ]{.Apple-converted-space}el cardenal Rubén Salazar; Carlos Raúl Yepes, presidente de Bancolombia; Julio Roberto Gómez, expresidente de la Confederación General del Trabajo y actual presidente del partido Alianza Social Independiente, y el general retirado y exministro de Defensa Rafael Samudio, o la líder indígena Ati Quigua o la afro Paula Moreno,[ ]{.Apple-converted-space}expresan el país nacional del que hablaba Gaitán. ¡Claro que no!.

Santos es un gran jugador, no hay duda, sigue el mismo proyecto excluyente de país, simulando participación. Poco a poco ha logrado concitar el conjunto de expresiones del establecimiento entorno a su modelo de conversaciones con la guerrilla de las FARC EP, y al tiempo,[ ]{.Apple-converted-space}está intentando fragmentar la interacción de esta guerrilla con el ELN con el lenguaje guerrerista, y a su vez, propiciar, por la inmadurez del movimiento social, otras nuevas tensiones y fragmentaciones.

A la jerarquía de la iglesia católica, no hay que hacerle mucho para que caiga en la complacencia cortesana con el ejecutivo. El Cardenal Salazar, que no hizo mucho por el proceso o mejor, lo ha hecho acríticamente, repitiendo, la mayoría de la veces, la retórica de Santos, por lo que de él no se espera que hable “de los que no tienen voz”, expresión, soberbia, ingenua,[ ]{.Apple-converted-space}socarrona, infantil y dependiente que supone que los excluidos no hablan o no tienen voz

La dinámica de cooptación está ahí con Ati Quigua, que no podrá hablar contra Monsanto ni por las semillas; de Paula Moreno, que poco habla de los afros excluidos; ni Clara López, que no podrá hablar de lo que piensan sectores de su partido frente a las Doctrinas Militares y Fuero Penal Militar si no dedicada a poner una velita a Santos, mientras este le hace el cajón con el respaldo a la candidatura de Pardo a la alcaldía de Bogotá. Y para cerrar el telón del CAP, la presencia de Vera Grabe, ex militantes del M-19 y del general ® Samudio Molina, uno de los responsables de la contra toma en el Palacio de Justicia, quizás será para cerrar las heridas y dar paso a una reconciliación de ápice de verdad.

Así las cosas, todo es maquillaje, es la misma paz que durante más de 12 procesos se ha proyectado a los colombianos, una paz sin fondo, una paz sin concesiones para cimentar una nueva democracia dónde se habilite un nuevo Pacto EcoSocial, un contrato social

La existencia de esta CAP vuelve a la memoria los modos del pacto frente nacionalista, no se trata, de involucrar a los contradictores de fondo, al movimiento social popular si no de pactos entre élites, en el que se incluye, algo de lo negado, por simular.

Con Santos hay que recordar lo que dijo y eso hay que creérselo, que él tenía en su bolsillo “la llave de la paz”, y tal vez, es más que eso: Santos es la llave, el llavero, la cerradura, la puerta, la[ ]{.Apple-converted-space}ventana, mejor dicho es la casa, y él definió como se entra, quién entra, quién sale, pues todo es bajo su control y eso significa estar y ser del lado de las élites del establecimiento urbano y financiero.
