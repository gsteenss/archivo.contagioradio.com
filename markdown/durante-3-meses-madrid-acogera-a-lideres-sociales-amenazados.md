Title: Durante 3 meses Madrid acogerá a defensores de derechos humanos amenazados
Date: 2018-10-30 13:35
Author: AdminContagio
Category: DDHH, Movilización
Tags: españa, lideres sociales, Madrid, Mundubat
Slug: durante-3-meses-madrid-acogera-a-lideres-sociales-amenazados
Status: published

###### Foto: Madrid Protege 

###### 30 oct 2018 

Este miércoles 31 de octubre será presentado en Bogotá el **Programa de Acogida Temporal de personas defensoras de los Derechos Humanos**, una iniciativa que busca que Madrid en 2019 se convierta en un refugio temporal para activistas perseguidos o en riesgo en otros lugares del mundo, centrando su atención en Colombia para su primera edición.

El propósito del programa, está enfocado en los líderes que luchan por la tierra, el ambiente, la libertad, la diversidad sexual o la memoria histórica entre otras, seleccionando tres de ellos, quienes viajarán durante  3 meses a la capital española, donde vivirán en un ambiente seguro, alejados de las amenazas que enfrentan en su día a día, desarrollando actividades de sensibilización a la población y presentando ante las autoridades las causas que defienden.

De acuerdo con **Mauricio Valiente,  Tercer Teniente de Alcalde del Ayuntamiento de Madrid**, el programa hace parte de las dos iniciativas aprobadas por el ayuntamiento, poniéndose a disposición para reforzar el proceso de paz y ayudar a los defensores de derechos humanos, frente a las situaciones de riesgo que enfrentan.

La intención al culminar su estadía en España, es que los líderes puedan volver a su labor "con ese descanso y eso lazos de comunicación que han mantenido y también con una relación mucho más intensa entre los vecinos de Madrid y el tejido asociativo de Colombia que va a permitir que sigan trabajando con más seguridad"  asegura Valiente.

En la elección de Colombia como centro de la primer edición el programa, la Oficina de Derechos Humanos y Memoria del Ayuntamiento de Madrid y la ONG Mundubat, gestora del proyecto, tuvo en cuenta la transición que vive el país hacia la paz luego de los acuerdos alcanzados entre las FARC y el gobierno, donde a pesar de frenar las hostilidades, cientos de vidas civiles se siguen sacrificando, incluyendo los líderes sociales y ambientales.

Valiente explica que los criterios para seleccionar  a los defensores son: el primero que se encuentren en situación de amenaza, sustentada por las organizaciones de Colombia y evaluada por el comité de selección conformado por representantes del mundo académico, asociativo de la administración; el segundo que la situación no tenga tal calibre que requiera un refugio permanente; y el tercero es que desde su trabajo con organizaciones puedan por medio de su testimonio establecer vínculos.

"No es solamente el descanso durante 3 meses que va a permitir oxigenar el trabajo que luego va a continuar, sino establecer también un vinculo entre el trabajo en el terreno y las entidades que trabajan en Madrid para visibilizar y reforzar y que ese conocimiento mutuo de alguna forma ya quede como un aporte que va a darle seguridad al trabajo posterior y que va a dar un altavoz a sus reivindicaciones"

La presentación tendrá lugar en el **Centro Cultural Gabriel García Márquez** del centro de Bogotá, contará con la presencia de Valiente,  Franklin Castañeda, Delegado de la sociedad civil en la Comisión Nacional de Garantías de Seguridad, Sonsoles García-Nieto, Presidenta de la Red ONGD de Madrid, María Eugenia Rodríguez, Subdirectora del Instituto de Derechos Humanos Bartolomé de las Casas, Universidad Carlos III de Madrid, Ignacio Aznar, Coordinador del Programa de acogida a personas defensoras de DDHH (Fundación Mundubat) y una persona defensora de derechos humanos participante en otro programa de acogida temporal en el estado español.

<iframe id="audio_29713516" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29713516_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

------------------------------------------------------------------------

###### Reciba toda la información de Contagio Radio en [[su correo]
