Title: Uniformados en la JEP coinciden en que habrían más víctimas de desaparición en Dabeiba
Date: 2020-10-29 13:20
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Dabeiba, Desaparición forzada, JEP
Slug: uniformados-en-la-jep-coinciden-en-que-habrian-mas-victimas-de-desaparicion-en-dabeiba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/DABEIBA.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Cementerio de las Mercedes - Dabeiba/ JEP

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Del 8 al 14 de noviembre, la Jurisdicción Especial de Paz (JEP) realizará una tercera diligencia de exhumaciones en Dabeiba, Antioquia ante las revelaciones que han surgido de las confesiones de miembros de la Fuerza Pública y que coinciden en las versiones coincidentes rendidas por 14 uniformados en el marco del caso **003: “Muertes Ilegítimamente presentadas como bajas en combate por Agentes de Estado" y el caso 004 que investiga la región de Urabá** y que estudia las medidas cautelares en 17 territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta tercera jornada de prospección y exhumación de cuerpos en el cementerio Las Mercedes de Dabeiba, Antioquia, que habrían sido desaparecidas en el marco del conflicto armado pertenece a una serie de tareas que se adelantan desde diciembre de 2019 y febrero de 2020 y que han dejado como resultado el hallazgo de 54 cuerpos de personas que están en proceso de identificación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Versiones de uniformados y comunidad sobre Dabeiba coinciden

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En la actualidad, son 14 uniformados, de distinto rango quienes han rendido versión ante la JEP y que han coincidido en sus versiones con relación al traslado de víctimas desde Medellín, la comisión de delitos, modalidad de encubrimiento y desaparición forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los comparecientes de la Fuerza Pública que pertenecieron a la Brigada XI del Ejército que han relatado su versión, afirman haber participado en el asesinato y entierro de entre 45 y 75 personas personas entre 2005 y 2007. [(Lea también: Comunidad de Dabeiba considera hallazgo de la JEP como un paso hacia la verdad)](https://archivo.contagioradio.com/comunidad-dabeiba-jep-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Previo a la diligencia, miembros de la [comunidad](https://archivo.contagioradio.com/dabeiba/) aportarán más información a través de entrevistas a testigos y toma de relatos. En el pasado, sus habitantes **han expresado que aún existe mucha información que no ha salido a la luz relacionada a desapariciones forzadas cometidas en sitios estratégicos como el Cañón de la Llorona, el puente de Urama y la vereda Cajones** donde se habrían cometido acciones criminales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Avances de la JEP en el terreno

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En diciembre de 2019, cuando fue realizada la primera de estas diligencias, fueron recuperados 17 cuerpos relacionados con ejecuciones extrajudiciales, mientras en la segunda jornada que ocurrió entre el 17 y 21 de febrero de 2020 se exhumaron 37 personas más, quienes habrían sido presentadas como bajas en combate por parte de agentes del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Fueron hallados a su vez, los cuerpos de una familia con prendas militares, dos niños y varias mujeres. expertos forenses de la [JEP](https://twitter.com/JEP_Colombia/status/1321442628815200257) además encontraron "cuerpos que llevaban botas de caucho y cráneos con herida de arma de fuego con presencia de ojivas". [(Le puede interesar: Más de 180 batallones cometieron ejecuciones extrajudiciales entre 2002 y 2008)](https://archivo.contagioradio.com/human-rights-watch-expone-su-ultimo-informe-sobre-falsos-positivos-en-colombia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras 18 años de permanecer desaparecido, **Edison Lexánder Lezcano Hurtado, de 23 años, padre de tres hijos y quien se desempeñaba como agricultor en zona rural de Dabeiba fue el primer cuerpo entregado por la JEP** y que fue asesinado por integrantes del Ejército Nacional.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
