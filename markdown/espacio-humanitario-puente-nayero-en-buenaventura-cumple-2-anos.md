Title: Espacio Humanitario Puente Nayero en Buenaventura celebra 2 años
Date: 2016-04-13 17:54
Category: Comunidad, Otra Mirada
Tags: buenaventura, Espacio Humanitario, Paramilitarismo, propuesta de paz
Slug: espacio-humanitario-puente-nayero-en-buenaventura-cumple-2-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/IMG-20160413-WA0004.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: justiciaypazcolombia] 

###### [13 Abril 2016] 

Los habitantes del Espacio Humanitario Puente Nayero en Buenaventura celebran hoy 2 años desde la “liberación de su calle” como afirman algunos. Desde hace dos años esta calle en medio de los barrios de baja mar del mayor puerto del pacífico, han logrado aflojar el cerco paramilitar, [desmontar casas de pique, evitar asesinatos, torturas e impunidad además de evidenciar](https://archivo.contagioradio.com/lideresa-afrocolombiana-doris-valenzuela-es-victima-de-amenazas/), en muchos casos, la complicidad de militares y policías con el control paramilitar.

Según William Mina, uno de los líderes del espacio, los logros son innumerables y por eso se han dedicado a celebrar “con bombos y platillos” **[el segundo aniversario de su calle liberada](https://archivo.contagioradio.com/?s=puente+nayero)**. Agrega que se están planteando la posibilidad de que otras calles también se conviertan en espacios humanitarios, ya que ver la posibilidad concreta de proteger la vida y evitar el cobro de vacunas ha animado a otras personas.

La celebración arrancó hacia las 4 de la mañana cuando los habitantes de Punta Icaco y Piedras Cantan, **barrios aledaños al Puente Nayero**, se despertaron con los golpes de tambores y resonar de los “Guasás”, las voces en los “alabaos” y las risas de los niños y niñas que compartían su alegría.

Una celebración eucarística y un conversatorio sobre los avances del proceso de conversaciones de paz cerraron la tarde. Uno de los puntos clave, en cuanto a los retos para los habitantes del Espacio Humanitario, es la posibilidad de que esa experiencia se multiplique por el país, dado que e**s una de las expresiones desde la sociedad civil de la cultura de la “no violencia” y [un ejemplo a seguir en materia de aplicación del DIH y los DDHH.](https://archivo.contagioradio.com/paramilitares-asesinaron-a-jovenes-que-se-negaron-a-ser-reclutados/)**

Mina asegura que la celebración continuará con actos culturales, concursos y bailes a lo largo de la tarde y hasta la 10 de la noche.

\  
<iframe src="http://co.ivoox.com/es/player_ej_11155531_2_1.html?data=kpael5qZd5Khhpywj5acaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncbjdzdHWw9KPkcriwpCajarXtMLXytSYqtrRpc%2Fd1cbfy9SRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
