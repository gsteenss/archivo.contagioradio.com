Title: DEA y Fiscalía habrían consumado un concierto para delinquir y afectar el proceso de paz: E Santiago
Date: 2020-11-10 12:01
Author: CtgAdm
Category: Actualidad, DDHH
Tags: acuerdo de paz, Fiscalía, JEP, Néstor Humberto Martínez
Slug: entrampamiento-mas-que-una-persecucion-a-santrich-un-ataque-al-acuerdo-de-pz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Ivan-Cepeda.mp3" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Jesús Santrich / Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los cerca de 24 mil audios que hacen parte del expediente de la Corte Suprema de Justicia de **Seuxis Paucias Hernández mejor conocido como Jesús Santrich,** acusado de enviar 10 kilos de cocaína a los Estados Unidos. tras la firma del Acuerdo de la Habana; permitirían identificar si todo hizo parte de **una operación de entrampamiento en la que habrían participado agentes de la DEA con el aval de la Fiscalía General de la Nación.** No solo ocultando información a la Jurisdicción Especial de Paz (JEP) que adelantaba el proceso del jefe político de FARC, sino hiriendo directamente al Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué se puede concluir tras escuchar los audios?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Dese 2017 se comenzó a hacer el seguimiento a 22 líneas telefónicas incluidas las de **Marlon Marín**, sobrino de Iván Márquez, para establecer si este hacía parte de una red de corrupción, dicho seguimiento concluyó en la captura del entonces negociador de la guerilla y hoy parte de la Nueva Marquetalia, ‘Jesús Santrich [(Lea también: Ni ángeles ni demonios)](https://archivo.contagioradio.com/ni-angeles-ni-demonios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una de estas llamadas, planteaba el inicio de una operación de lavado de activos que vinculó a Armando Gómez España y Fabio Younes Arboleda - detenidos en 2018 junto a Jesús Santriích - y a aparentemente integrantes del cartel de Sinaloa de México, uno de ellos identificándose en particular como hijo de Rafael Caro Quintero, fundador del cártel de Guadalajara, sin embargo, eran en realidad agentes encubiertos de la Administración de Control de Drogas (DEA) por sus siglas en inglés.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de los audios, dados a conocer por [El Espectador](https://www.elespectador.com/noticias/investigacion/los-audios-de-la-dea-y-la-fiscalia-que-le-negaron-a-la-jep-sobre-el-caso-santrich/), no solo se pudo establecer que los cinco kilos de droga que hicieron parte de la operación de lavado de activos que desarrollaba Marín, fueron aportados por la Fiscalía por medio de una entrega controlada y no como había manifestado Marlon Marín como testigo de la DEA, que aseguró había sido suministrada por Santrich.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los audios también permiten establecer una frecuente insistencia de los agentes encubiertos de establecer comunicación con los exjefes negociadores de paz de las entonces FARC, Iván Márquez y Jesús Santrich y que estos respondieran por la cifra de 5 millones de dólares supuestamente perdidos por el cartel de Sinaloa en medio de la captura de Vincent Schifano, señalado de ser socio de Santrích en Nueva York y quien cumple hoy una condena de nueve años de prisión por el intento de lavado de \$15 millones de dólares al exnegociador.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Apartes de conversaciones, como la del 29 de marzo de 2018, señalan como los mexicanos, en realidad agentes encubiertos, llamaron a Marlon Marín y para decirle que llegaban a Colombia con el objetivo de entrevistar con Iván Márquez y Jesús Santrich y discutir el éxito de la operación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Néstor Humberto Martínez se contradice en caso Santrich

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Ahora tras conocerse esta información. el exfiscal Néstor Humberto Martínez, quien ejecutó la orden de captura del exjefe guerrillero afirma que nunca supo de la existencia de los 24.000 audios que se han dado a conocer, que desconocía que las líneas telefónicas de Iván Márquez estuvieran interceptadas y que no fue informado sobre la entrega controlada de cocaína por la Fiscalía, contradiciendo declaraciones que dio el 16 de mayo de 2019 a medios de comunicación. C**abe señalar que de los 24.000 audios de los que ahora se conoce su existencia, solo 12 fueron aportados por la Fiscalía a la JEP.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega además que la DEA realizaba una operación paralela a la de la Fiscalía y que desconocía que los aparentes traficantes del cartel de Sinaloa eran agentes encubiertos. Esta versión contradice los datos aportados por El Espectador y que revelan una solicitud hecha por un agente de la DEA a la Fiscalía el 17 de noviembre de 2017 para interceptar 22 líneas telefónicas de una red internacional de narcotráfico, una de las líneas a intervenir era la de Iván Márquez.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, Enrique Santiago, asesor jurídico de las entonces FARC en las negociaciones de paz en la Habana expresó que "nadie nunca hizo mas daño a la paz que Néstor Humberto Martínez. El evidente montaje de Fiscalía y agencias de EE.UU. no pudo destrozar el proceso de Paz, pero provocó unas disidencias que podrían haberse evitado" por lo que señala que conocida la verdad, es necesario corregir el mal causado. [(Lea también: Solo el 15% de instancias creadas para cumplir el Acuerdo de Paz están funcionando)](https://archivo.contagioradio.com/solo-el-15-de-instancias-creadas-para-cumplir-el-acuerdo-de-paz-estan-funcionando/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Creo que todas estas evidencias y revelaciones que ahora se están conociendo, afortunadamente vienen a acreditar que la JEP estaba en lo justo y estaba en toda la razón para pedir que se el entregaran las pruebas" agregando que este actuar tenía la finalidad de "alterar y dejar sin efecto el proceso de paz provocando un gran sismo interno en los antiguos miembros de las FARC".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El asesor jurídico expresó además que el problema está en no caer de nuevo en el "error de apuntar a Jesús Santrich", a quien considera un "instrumento" de dicha operación que evidentemente iba dirigida contra Iván Márquez, y otras figuras como Piedad Córdoba, el ex vicepresidente Óscar Naranjo, también mencionados en los audios y de quienes no se obtuvo tan fácil acceso.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Me preocupa gravemente es que ha habido un concierto para delinquir en una operación falsa para provocar una serie de delitos para arruinar un proceso de paz que ha acabado con un conflicto de 50 años".
>
> <cite>Enrique Santiago</cite>

<!-- /wp:quote -->

<!-- wp:audio {"id":92546} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Ivan-Cepeda.mp3">
</audio>
</figure>
<!-- /wp:audio -->

<!-- wp:heading {"level":3} -->

### "Lo que se buscaba era asestar al proceso de paz un golpe de tal naturaleza que significara su fracaso"

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador Iván Cepeda, coincide con esta postura y señaló que la existencia de estos audios demuestra "que bajo la administración de Néstor Humberto Martínez hubo una planificada y concertada acción para atentar contra el proceso de paz", operaciones en las que era imposible que no participara el exfiscal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cepeda afirma que detrás de estos sucesos se encuentran sectores poderosos nacionales y extranjeros cuyo accionar "a todas luces es ilegal" y aunque no se trata de un fenómeno nuevo, el conocimiento de estos hechos le recuerda el de otros casos como la operación de inteligencia Andrómeda para interceptar a negociadores del proceso o los ataques a la JEP.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A proposito de la conclusión que hace el diputado Santiago sobre el daño que se buscaba hacer al partido FARC, el senador Cepeda señala que en efecto se hizo un daño parcial a la colectivdad y "que en efecto esto tuvo unas consecuencias que hemos condenado desde el principio, nadie justifica que los señores Santrich y Márquez volvieran a las armas", sin embargo destaca que el objetivo final de acabar con el Acuerdo no se logró y se evidencia en los procesos que se han dado en la implementación y que han tenido un efecto benéfico en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A raíz de estos nueva evidencia que sale a la luz, el senador tomó la decisión de ampliar la denuncia penal que ya existía contra Néstor Humberto Martínez y contra una serie de personas de la Fiscalía General de la Nación de ese entonces. Respecto a las afirmaciones del exfiscal, el congresista ha expresado que "sus respuestas son totalmente insatisfactorias, absolutamente contradictorias y en algunos casos contradicen lo que muestran los hechos". [Le puede interesar: Gobierno también es responsable del regreso a las armas de Iván Márquez)](https://archivo.contagioradio.com/gobierno-tambien-es-responsable-del-regreso-a-las-armas-de-ivan-marquez/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1326155488040538123","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1326155488040538123

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### JEP ya había alertado sobre irregularidades en proceso Santrich

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para finales de 2019, la Sección de Revisión de la JEP, ya había alertado sobre diversas irregularidades en el proceso judicial que se siguió contra Jesús Santrich, incluida la utilización de agentes provocadores encubiertos por parte de la DEA, que en su actuar atentaron contra la soberanía nacional, irregularidades en el procedimiento para recoger prueba por parte del Departamento de Justicia de EE.UU. y de la Fiscalía General de la Nación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los magistrados a su vez habían indicado que a sus manos nunca llegaron las pruebas que demostraban que Santrich hubiese cometido un delito pese a que el entonces fiscal, Néstor Humberto Martínez afirmaba que existían numerosas pruebas como evidencia electrónica, pruebas documentales y videos. [(Lea también: Congresistas denuncian que DEA violó la soberanía colombiana en caso Santrich)](https://archivo.contagioradio.com/denuncian-dea-violo-soberania-santrich/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sin embargo, cuando la JEP solicitó este dicho material, el fiscal dijo que no existían debido a que las evidencias probatorias residían en la jurisdicción del país requirente”, es decir, en Estados Unidos. Pese a que esta petición se extendió al Departamento de Justicia tampoco hubo respuesta. [](https://archivo.contagioradio.com/la-desaparicion-de-santrich-no-puede-ser-un-impedimento-para-buscar-la-paz/)[(Le puede interesar: La desaparición de Santrich no puede ser un impedimento para buscar la paz)](https://archivo.contagioradio.com/la-desaparicion-de-santrich-no-puede-ser-un-impedimento-para-buscar-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el surgimiento de estos 24.000 se espera que puedan esclarecerse muchas incógnitas con relación al caso Santrich y que en su momento podrían haber llegado a la JEP, institución que solicitó la evidencia a la fiscalía de Néstor Humberto Martínez sin una respuesta certera; sucesos que derivaron en la salida de Jesús Santrich del Acuerdo de Paz y un posterior retorno a las armas de un sector de las FARC.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
