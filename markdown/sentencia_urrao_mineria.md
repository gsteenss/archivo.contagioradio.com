Title: Sentencia contra prohibición de minería en Urrao sería improcedente
Date: 2017-11-23 14:55
Category: Ambiente, Nacional
Tags: codigo de minas, Corte Constitucional, Mineria, Tribunal Administrativo de Antioquia, Urrao
Slug: sentencia_urrao_mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-e1457467593828.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [22 Nov 2017] 

[Una reciente sentencia del Tribunal Administrativo de Antioquia anuló el acuerdo del concejo municipal de Urrao, donde se dictaban "medidas para la defensa del patrimonio ecológico y cultural del municipio", lo que quiere decir que nuevamente quedaría abierta la posibilidad de que se desarrollen actividades mineras. No obstante, se trataría de una sentencia que tendría **al menos siete irregularidades.**]

De acuerdo con el jurista Rodrigo Negrete, esta situación que enfrenta Urrao es un  precedente que enciende las alarmas en otros municipios del departamento de Antioquia como **Támesis, Jericó, Titiribi y Caicedo,** en donde también se han expedido acuerdos municipales para prohibir la minería a gran y mediana escala que afectan el ambiente.

### **Algunas de las irregularidades** 

El acuerdo invalidado por el tribunal era el 009 aprobado por el consejo municipal en el mes de agosto. Rápidamente las instancias del gobierno se movieron para interponer acciones jurisprudenciales que impidieran que se prohibieran actividades minero-energéticas en ese municipio. Según el abogado, el **Tribunal Administrativo de Antioquia, actuó "extrañamente", siendo "juez y parte**". Una situación que hubiese impedido que esta instancia tomara una decisión, ya que desde la gobernación de Antioquia se fomenta la actividad minera, se fiscaliza y además se autoriza.

Por otro lado, el Tribunal habría desconocido las intervenciones del abogado y otros 9 concejales que actuaban en defensa del acuerdo municipal, y por tanto **se habría violado el derecho a la defensa y el debido proceso,** pues "cualquier persona puede intervenir para atacar o defender los acuerdos municipales", explica Negrete.

Asimismo, entre los argumentos de la sala se cita el artículo 37 del Código de Minas que prohibía que, por ejemplo mediante acuerdos municipales, se impidiera la minería, sin embargo, ese acuerdo fue excluido del ordenamiento jurídico el año pasado mediante la sentencia C-273 del 2016 de la Corte Constitucional. **Es decir que se estaría usando un  artículo de dicho Código que ya no existe**, "lo cual es un precendente grave que podría llevar a los magistrados a estar inmersos en una investigación penal", asegura.

### **¿Qué planea la comunidad?** 

Una situación similar había enfrentado el Tribunal Administrativo del Huila que debió evaluar una demanda contra un acuerdo municipal muy similar al de Urrao, pero contrario a lo que sucedió en Antioquia, tras la debida revisión, **esa instancia consideró válido el acuerdo de prohibir la minería y rechazó la demanda** del gobierno al evaluar como improcedentes los argumentos del gobierno, justamente con base en el análisis de la sentencia de la Corte.

Sin embargo, respecto al caso de Antioquia el abogado Negrete señala que "la situación es alarmante, sorpresivamente hay un fallo sin firmas de magistrados y se vulneraran derechos fundamentales de los defensores y las comunidades".

Pese a la sentencia, Rodrigo Negrete afirma que con el fallo de la Corte Constitucional del año pasado "el horizonte es contundente", y por ello ya se **planean movilizaciones sociales pero también acciones jurídicas tales como una tutela por vías de hecho,** que es un mecanismo que se usa cuando son los propios jueces los que han violado la Ley, y desde el Tribunal de Antioquia "abiertamente se está actuando contrario a la Constitución", dice Negrete.

<iframe id="audio_22236578" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22236578_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

<div class="osd-sms-title">

Compartir:

</div>

</div>
