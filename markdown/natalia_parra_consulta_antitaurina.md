Title: “Es absurdo que la consulta antitaurina se realice por fuera de las elecciones ordinarias”: Natalia Parra
Date: 2017-08-09 16:19
Category: Animales, Nacional
Tags: Consulta Antitaurina, consultas populares
Slug: natalia_parra_consulta_antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/toros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cristian Garavito] 

###### [9 Ago 2017] 

Bien dicen que más vale tarde que nunca. La Alcaldía de Bogotá ha decidido, apenas una semana antes anunciar que este 13 de agosto no se realizará la consulta popular antitaurina como **desde hace meses viene pidiendo la ciudadanía, incluso, mediante redes sociales. **

“Aplazar la consulta es una decisión muy pertinente en la medida en que nadie había estado haciendo la campaña para que las personas escuchen los argumentos de taurinos y antitaurinos y puedan ejercer su derecho al voto”, considera Natalia Parra, directora de la Plataforma ALTO, la organización animalista promotora de la iniciativa.

Desde allí mismo, se ha venido insistiendo en la necesidad de que la consulta se lleve a cabo junto a las elecciones ordinarias en marzo de 2018, por dos razones específicas: brindar garantías de participación democrática y proteger el erario público.

Para la directora de la Plataforma ALTO, **“es absurdo que la consulta se realice por fuera de las elecciones ordinarias”,** ya que esto no solo imposibilita que se respete la participación ciudadana, sino además porque de no hacerla con elecciones ordinarias, dicho **mecanismo implicaría una suma superior a los 45 mil millones de pesos, es decir un 90% más costosa.**

### “Esperamos que la alcaldía esta vez nos escuche” 

Tras el anuncio de la alcaldía, los promotores de la consulta esperan que esta vez si se les escuche y existan todas las "garantías de democracia y moralidad administrativa" para que se ejecute de la mejor manera.

“Esta decisión se pudo haber tomado hace un buen tiempo, cuando la Corte revivió la consulta, pero a veces a **la administración distrital le falta escuchar a quienes hemos impulsado esta consulta. Ya han cometido varios errores”,** dice Parra, quien cuenta que no solo no se tuvo en cuenta los obstáculos con la definición de fecha del 13 de agosto, sino que además en el texto de decreto habían incluido la opción del voto en blanco, cuando este tipo de mecanismos de participación popular no contempla sino la opción del Sí y el No.

Además, cabe resaltar que  la alcaldía **"puede replantearla, en el entendido que la ley 1757 de 2015** no prevé la situación para los casos en los cuales no hay disponibilidad de recursos, únicamente establece por un lado el término dentro del cual se deben llevar a cabo los comicios, o por otro lado, el vencimiento del plazo indicado para ello", explican.

### Lo que sigue 

Ahora solo queda esperar a que la alcaldía elija una nueva fecha, entendiendo ese panorama al que se refieren los animalistas. Por otra parte, el **Consejo de Estado debe determinar quien debe proveer los recursos para que se desarrolle la esa votación.**

Finalmente, la lider animalista asegura que los obstáculos que ha tenido esta consulta sirven para que otros procesos similares se fortalezcan, teniendo en cuenta lo "coartada que a veces es esta democracia", dice Natalia Parra, y añade, "**sentimos que esta consulta ayuda a esclarecer algunos puntos ciegos que tiene la legislación** sobre estos mecanismo de participación, en un momento en que se están volviendo un tema tan importante para las poblaciones".

Por el momento, los activistas llaman a que **desde ya cada ciudadano sea un agente activo** para multiplicar la información sobre la consulta antitaurina, y se trabaje en el voz a voz para acabar con la "tortura en las plazas".

<iframe id="audio_20256682" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20256682_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). {#reciba-toda-la-información-de-contagio-radio-ensu-correoo-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-porcontagio-radio. .m_1018166489710307556gmail-western lang="es-CO" align="left"}
