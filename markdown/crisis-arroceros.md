Title: Para sacar de la crisis a los arroceros, habría que intervenir el precio del grano
Date: 2018-10-01 21:18
Author: AdminContagio
Category: DDHH, Economía
Slug: crisis-arroceros
Status: published

###### [Foto:] 

###### [1 Oct 2018] 

Mientras los cultivadores de arroz denuncian que con los precios del grano en el mercado nacional tendrían pérdidas por más de **500 mil pesos por hectárea,** el Gobierno toma medidas que no dan resultados; por esta razón, el gremio estaría a la espera de una reunión con el presidente de la república, que de no encontrar soluciones a la crisis, podría desencadenar jornadas de movilización nacional.

**Eudoro Álvarez, líder de Dignidad Arrocera**, sostuvo que la situación en el sector es difícil porque al precio de hoy, con la cosecha avanzada en un 70%, los cultivadores perderían más de 500 mil pesos por hectárea. A esto se suman las deudas adquiridas por los agricultores para el proceso productivo, que ya ha generando el embargo de algunas propiedades campesinas.

Para hacer frente a la crisis, el Gobierno destinó **30 mil millones de pesos en incentivos para almacenar arroz hasta 2019**, esperando que se regule el precio del grano al evitar la sobre oferta del mismo; pero propuesta no fue bien recibida por la gran molinería, que son los que finalmente controlan el mercado del arroz empaquetado y por ende, su precio. (Le puede interesar:["Proyecto de Agromar provoca daños ambientales en Bosque primario en Chocó"](https://archivo.contagioradio.com/proyecto-de-agromar-provoca-danos-ambientales-en-bosque-primario-en-el-choco/))

Adicionalmente, según Álvarez, la medida no hace "ni cosquillas" en los precios del mercado porque no hay capacidad de almacenamiento como para tener tal efecto, por ejemplo, **"en Meta hay una producción de unas 400 mil toneladas de arroz, de las cuales se pueden guardar solo 5 mil"**; además, no impactaría positivamente a los cultivadores, porque la gran mayoría no tienen molinos para procesar y guardar el cereal.

### **La importación de arroz: El otro gran problema del sector** 

Aunque el ministro de agricultura, **Andrés Valencia**, califica como un éxito el programa para almacenar arroz y controlar el precio del grano, en este momento **Perú está pidiendo que Colombia reciba parte del cereal que está en el mercado del vecino país,** hecho que para Álvarez se trata de una triangulación que afecta a los productores nacionales. (Le puede interesar: ["Más de 300 mil hectáreas se han entregado de forma irregular en Colombia"](https://archivo.contagioradio.com/300-hectareas-irregular-colombia/))

El líder arrocero indicó que el país vecino importa 300 mil toneladas de arroz de Vietnam y Tailandia, pero una parte de esa importación estarían buscando exportarla a Colombia, lo que sería una triangulación. Para solventar esta situación, Álvarez afirmó que el ministro de Comercio Exterior se comprometió a encontrar rutas de comercialización del arroz nacional en Perú; pero destacó que esa medida es inefectiva porque **el trámite se demora mucho más tiempo que la cosecha del grano.**

### **Arrozeros están intentando dialogar con un Gobierno sordo**

Para el integrante de Dignidad Arrocera, **"sería saludable intervenir el precio del arroz"**, porque para él, es evidente que el mercado se está manipulando, y se podría buscar una solución al actuar como ya se ha hecho con sectores como el de la leche, buscando amparar a productores y consumidores. Álvarez recordó que esa resolución estuvo a punto de ser puesta en marcha, pero el actual Ministro decidió que no era la solución.

A pesar de la negativa a la intervención en los precios, el gremio ha buscado un canal de comunicación con el Gobierno para tratar el tema, y están a la espera de una nueva reunión con el Ministro de Agricultura y el Presidente de la Nación; que **de no encontrar respuestas a la crisis, podrían llevar a jornadas de movilización nacional.** (Le puede interesar:["Tras 5 años del paro agrario la situación del campo no ha mejorado"](https://archivo.contagioradio.com/hay-una-crisis-acumulada-en-varios-sectores-agricolas-cesar-pachon/))

<iframe id="audio_29005880" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29005880_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
