Title: México: Un camino posible de la mano de López Obrador
Date: 2018-07-04 14:48
Category: El mundo, Política
Tags: AMLO, Colombia Humana, Elecciones presidenciales, mexico
Slug: mexico-un-camino-posible
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/01-de-julio-Zócalo.07-1024x682.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AMLO] 

###### [4 Jul 2018] 

A propósito de la victoria de Andrés Manuel López Obrador, candidato a la presidencia mexicana por el partido político Morena, algunos analistas destacan la posibilidad de que México se convierta en un ejemplo a seguir por parte de los progresismos en América Latina, e igualmente señalan algunas de las reacciones y políticas que se esperan desde el país centroamericano.

<iframe src="https://co.ivoox.com/es/player_ek_26894807_2_1.html?data=k5ulm5mcdJihhpywj5aaaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCxqcXdz8aYqcbQsMbb0IqfpZDFssLgytjhw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Carlos Medina Gallego, profesor titular de Ciencia Política en la Universidad Nacional de Colombia, afirmó que López Obrador llegó al poder gracias a un trabajo de más de 4 años, venciendo a las élites oligárquicas y tradicionales, pero sobre todo, **proponiendo atender a sectores excluidos de la población mexicana que no han podido mejorar sus condiciones en décadas.**

Aunque Medina aclara que López Obrador probablemente no resolverá todos los conflictos, sí introducirla cambios que, de tener impacto, llevarán a construir nuevas formas de ciudadanía. En ese sentido, algunos de los desafíos que enfrentara el nuevo mandatario son la construcción de economías redistributivas, reversar el proceso de concentración de la tierra, la persecución de los tráficos ilegales y la intervención estadounidense por parte de Donald Trump. (Le puede interesar: "México y Palestina dijeron no a los muros '[World Without Walls'"](https://archivo.contagioradio.com/mexico-y-palestina-muros/))

Para afrontar esos retos, el profesor de la Universidad Nacional señaló que López Obrador tendrá a su favor algunos sectores de la economía que le permitirán un rango de acción; y **será fundamental la construcción de un equipo de gobierno innovador que le ayude a sortear tanto la intervención norteamericana, así como los grandes intereses de las empresas, los medios de comunicación y los carteles de droga.**

### El espejo con la sociedad colombiana 

**Los problemas a los que se enfrenta la sociedad mexicana son similares a los de Colombia: hay una delincuencia organizada, y presencia de economías ilegales que, de acuerdo con Medina, cuentan con el apoyo de economías legales, lo que les permite usar dineros ilícitos en inversiones lícitas y beneficiarse de ello.**

Adicional a los problemas por la concentración de la tierra y los bajos índices de redistribución de la riqueza, está el hecho de que  las economías ilegales "han permeado la justicia, la fuerza pública, los partidos y los sectores más pudientes de la sociedad mexicana", es decir que tomaron todas las estructuras sociales al igual que en Colombia como indica el analista.

En la campaña política López Obrador también tiene similitudes a la realizada recientemente en Colombia dado que su lugar de comunicación fundamental con el electorado fue la plaza pública, y guarda algunas semejanzas con el programa de gobierno de la Colombia Humana.

**Por estas razones, si el gobierno de Manuel López Obrador sienta las bases para la superación de los conflictos que vive el país centroamericano, puede resultar en un ejemplo para los progresismos latinoamericanos y, un marco de referencia para Colombia.**

<iframe id="audio_26894633" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26894633_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
