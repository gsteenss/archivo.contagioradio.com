Title: Ordenan investigar seguimientos contra abogados de líderes del Congreso de los Pueblos
Date: 2020-12-22 17:46
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Adelso Gallo, Liderazgos campesinos, montajes judiciales, Robert Daza, Teófilo Acuña
Slug: ordenan-investigar-seguimientos-contra-abogados-de-lideres-del-congreso-de-los-pueblos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/Lideres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Líderes sociales / Congreso de los Pueblos

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tras casi una semana de ser privados de la libertad, **los líderes campesinos Adelso Gallo, Robert Daza y Teófilo Acuña** recobraron su libertad. Los líderes habían sido capturados en tres operativos realizados entre el 15 y 16 de diciembre. La noche de este 21 de diciembre, la juez de control de garantías encargada del caso determinó que la medida de privación de libertad genera afectaciones a las comunidades que representan y reconoció la labor de liderazgo que los tres hombres ejercen en sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según **Olimpo Cárdenas Delgado, responsable nacional de DD.HH**. **del Congreso de los Pueblos**, la jueza no recogió los argumentos de la Fiscalía al parecerle "desordenados y que no eran proporcionales con la medida que se pedía", por lo que dictaminó los líderes podrían afrontar el proceso en libertad. De este modo, pese a continuar vinculados al caso, se determinó concluir la medida de aseguramiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cardénas señaló además que la detención de los tres líderes, integrantes de Coordinador Nacional Agrario (CNA) es un acto que busca "criminalizar su proceder en las protestas sociales y llevarlos a procesos bajo el abuso del derecho penal". De este modo, el movimiento agrario coincide en que el aumento de capturas arbitrarias obedece a montajes judiciales por parte de la Fiscalía destinados a deslegitimar los liderazgos sociales como una forma de persecución.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, el abogado **Jorge Reales, defensor del líder Robert Daza expresó que la Fiscalía** no presentó pruebas específicas que representaran la responsabilidad de los líderes por lo que la jueza determinó que no habían elementos que se vinculara a los líderes con actividades irregulares, por lo que planteó que los líderes no constituyen un riesgo de fuga, ni representan un riesgo para la sociedad, al contrario son legitimados por la misma comunidad debido a su accionar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado agrega que la juez solicitó a la Fiscalía compulsar copias a la entidad para investigar hechos de hostigamientos de los que han sido víctimas los abogados defensores de los líderes campesinos. Denuncia a su vez que su teléfono ha sido objeto de interceptaciones y hostigamientos en medio de su actividad como abogado, aunque se desconoce las personas que estarían detrás de estas actividades, se espera que la investigación revele esta información.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"La labor social en el país está sometida a una serie de riesgos, hay una cifra lamentable de personas judicializadas por razón de su oposición al Estado" asegura el abogado, por lo que sostiene que mientras el Gobierno vea la tarea del liderazgo como una forma de desastibilización siempre estará presente ese riesgo de judicialización.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Ser líderes sociales no es un délito"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al momento de su liberación, el **líder campesino Adelso Gallo** resaltó la importancia de poderse defender en libertad; señalando que esto solo pudo ser posible gracias a la convicción de "hacer las cosas bien, convencidos de que la lucha sigue y la defensa del pueblo tiene razón y no tiene por qué esconderse". [(Lea también: Bancada de oposición rechaza montajes judiciales contra líderes sociales)](https://archivo.contagioradio.com/bancada-de-oposicion-rechaza-montajes-judiciales-contra-lideres-sociales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agregó que la libertad se logró con el **"respaldo del pueblo y las [organizaciones sociales](https://www.congresodelospueblos.org/) que reconocieron el humilde aporte al trabajo que hemos hecho"**, finalmente señaló la importancia de que el caso fuera asumido por un "juez independiente" que tomó sus determinaciones acorde a las evidencias. Finalmente manifestó su deseo para seguir trabajando "en lo que el pueblo nos ha encomendado en este momento y para el año que viene".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe señalar que Adelso Gallo ha impulsado llamamientos a la comunidad internacional en diversas audiencias de control político **denunciando los vínculos entre transnacionales petroleras y el paramilitarismo en Arauca, Casanare y Boyacá.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, **Robert Daza, líder social del suroccidente colombiano, también víctim**a de detención arbitraria expresó que las actividades que desarrollan en la búsqueda de que los derechos de las y los campesinos sean plenos "son legitimas y necesarias porque estamos buscando que se les reconozca todo lo que se ha despojado al campesinado, seguimos adelante en esta lucha sin miedo y sin temor".

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/C_Pueblos/status/1341187641702793223","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/C\_Pueblos/status/1341187641702793223

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, **Teófilo Acuña, líder social del sur de Bolívar, y sur y centro del César** quien ya había ha sido víctima de detenciones arbitrarias en 2007 tras oponerse a la explotación de Anglo Gold Ashanti en la región, agradeció a los movimientos sociales de la región y del país que expresaron su rechazo a las capturas arbitrarias y exigieron su libertad, "con todos ustedes logramos esto, estamos demostrando que ser defensor de DD.HH. no es un delito y por esa razón hoy tenemos que seguir demostrando que esos falsos positivos se van cayendo por sí mismos".

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_62853850" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_62853850_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
