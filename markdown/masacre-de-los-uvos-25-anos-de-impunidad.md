Title: Masacre de Los Uvos: 25 años de impunidad
Date: 2016-04-07 19:55
Category: Nacional, Sin Olvido
Tags: Cauca, crímenes de estado, masacre de Los Uvos
Slug: masacre-de-los-uvos-25-anos-de-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Masacre-los-Uvos-e1460076855890.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Libre Cauca 

###### [7 Abr 2016] 

Hace 25 años militares del Batallón José Hilario López con apoyo de estructuras paramilitares, detuvieron y luego ejecutaron extrajudicialmente a 14 hombres y 3 mujeres que preparaban la primera movilización campesina del Macizo Colombiano en el corregimiento de Los Uvos, departamento del Cauca.

Mientras que en la capital La Asamblea Nacional Constituyente discutía un pacto de paz,  soldados del Ejército abordaron el vehículo donde se transportaban los campesinos y obligaron al conductor a regresar a la vereda Monterredondo. Después de media hora de viaje, se hizo descender del vehículo a los pasajeros, les arrebataron sus pertenencias y fueron obligados a tenderse boca abajo sobre la vía, para luego ser asesinados. En ese momento, Pastora García y Henry Suárez pasaban por el lugar, ambos fueron interceptados y ejecutados al igual que los demás campesinos.

Allí fueron ejecutados extrajudicialmente: Alfonso Chilito (25 años), José Belisario Dorado Muñoz (41 años), Saúl Espinosa (42 años), Wilson Gil velásquez (17 años), Hoibar Gómez Mamián (18 años), Ruben Darío Joaquí Narváez (32 años), Santiago Lasso Bolaños (28 años), Adriana López (18 años), Hernán Mamian Moreno (31 años), Leoncio Mellizo Angulo (50 años), Libardo Nieves Dorado (24 años), Yenny Prieto Rengifo (28 años), Hernando Rosero (42 años), Adán Ruano Daza (55 años) y Alejandro Salazar Paz (22 años).

El hecho fue expuesto por el Ejército Nacional, como un acto suscitado por grupos guerrilleros, para encubrir al batallón que había cometido el hecho. Además buscaban destruir las evidencias que involucraban a la XIII Brigada del Ejercito.

De acuerdo con la Comisión de Justicia y Paz, “la patrulla militar pintó consignas alusivas a la Coordinadora Guerrillera Simón Bolívar en el lugar de los hechos con el fin de desviar la investigación de lo ocurrido y en efecto, el Mayor Manuel Rodríguez Diazgranados, comandante del puesto de mando atrasado del Batallón Nº 7 "José Hilario López", denunció a los miembros del 29 frente de las FARC EP ante la Dirección de Orden Público de la Seccional Cali como autores de los delitos de homicidio múltiple y daño en bien ajeno en un intento de desviar la investigación judicial y asegurar la impunidad”.

Pese a sentencias de la CIDH, la reparación por parte del Estado hacia las víctimas de los familiares se cumplido parcialmente, además se mantiene la impunidad frente a la responsabilidad estatal, más sin embargo, las víctimas de este crimen de Estado siguen vivas en la memoria de sus familiares y su búsqueda de verdad, como ellos lo aseguran.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
