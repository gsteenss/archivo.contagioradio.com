Title: Menor Embera de 9 años muere en medio de enfrentamiento armado
Date: 2020-07-16 17:31
Author: CtgAdm
Category: Actualidad, DDHH, Mujer
Slug: menor-embera-de-9-anos-muere-en-medio-de-enfrentamiento-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/fc7a3119-36fd-4dd7-8dda-1fe364a966e1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 16 de Julio se registró el asesinato de una **menor de 9 años perteneciente a la [comunidad Embera](https://archivo.contagioradio.com/fiscalia-no-esta-sirviendo-en-el-pais/)**, integrante del resguardo *Geand*ó, ubicado en el Alto Baudo, departamento del Chocó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según informó a medios locales el alcalde de este municipio, Ulises Palacios, la menor fue identificada como **Luz Elena Cáizamo Rojas,** **quien falleció  
luego de ser impactada con un proyectil de arma de fuego en su cabeza,** como consecuencia de un enfrentamiento armado entre integrantes del ELN y las AGC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte la **Defensoría del Pueblo, señaló que la menor había retornado a este territorio Embera hacia dos meses,** luego de que junto a su familia fueran víctimas de [desplazamiento forzado](https://archivo.contagioradio.com/50-anos-de-promesas-fallidas-a-los-embera/).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RichiNasa/status/1283871584906870784","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RichiNasa/status/1283871584906870784

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

A través de redes sociales se filtraron algunas imágenes sobre el enfrentamiento armado que se dio en este resguardo indígena, y que hasta el momento registro una víctima fatal. Autoridades investigan si este hecho pudo afectar a más integrantes de esta comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez la **Organización Nacional Indígena de Colombia** (ONIC), hizo un llamado a las organizaciones internacionales de Derechos Humanos y al Gobierno Nacional para que **cese la violencia contra los niños y niñas de las comunidades indígenas**, llamado al que se han suman diferentes usuarios de twitter, indicando que este hecho *"se da en respuesta al rechazo del Gobierno ante la propuesta del ELN de un cese al fuego por 90 días"*.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marthaperaltae/status/1283858269958742022","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1283858269958742022

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Finalmente, **Astrid Sánchez,** representante del Chocó en la Comisión Segunda de la Cámara de Representantes, **exigió al Gobierno tomar medidas urgentes, para evitar que en los próximos días se presente nuevamente un desplazamiento de la comunidad**, *"no podemos permitir que los armados ilegales continúen en su empeño de [atentar contra la vida](https://www.onic.org.co/comunicados-regionales/3964-intento-de-masacre-en-el-resguardo-indigena-awa-de-cuasbil-la-faldada) y bienes de los habitantes del chocó estas acciones no cesan y dejan en sus obras al departamento".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
