Title: Es fácil subirse al bus de la victoria
Date: 2016-08-09 17:01
Category: Nacional
Tags: Cómite Olímpico colombiano, Juegos Olímpicos, Oscar Figueroa
Slug: es-facil-subirse-al-bus-de-la-victoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/IMAGEN-16668476-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  Foto:El tiempo 

##### [9 Agos 2016]

Luego de alcanzar la primera medalla de oro para el país en los Juegos Olimpicos de Río 2016, **Oscar Figueroa**, representante de la halterofilia en la categoría  62 kg, se convirtió en el primer colombiano (hombre) en alcanzar la máxima presea que entrega este tipo de certamen. Un triunfo que sirve para celebrar y poner sobre la mesa una vez más el tema del apoyo real que reciben los atletas nacionales, tanto en su preparación como en el desarrollo de las competencias.

Sin duda lo mostrado por Figueroa, deportista nacido en Zaragoza, Antioquia y criado en Cartago Valle a donde llegó con su familia huyendo del conflicto armado, cubre las expectativas que se tenían desde antes de su llegada a territorio brasileño, gracias al importante palmares alcanzado a nivel de campeonatos nacionales, mundiales, panamericanos y por su puesto la medalla de plata alcanzada en las Olimpiadas de Londres 2012, un camino labrado en más de dos décadas de duro trabajo y mucho sacrificio.

Como viene ocurriendo desde hace un tiempo, cuando los deportistas alcanzan este tipo de triunfos, la atención que fijan sobre ellos los grandes medios de información ha servido para que en esos minutos puedan manifestar su descontento con la manera en que desde la institucionalidad se trabaja por el deporte, demandando una mayor atención del Estado en apoyo e inversión antes, durante y después de los buenos resultados, es decir, antes de montarse en el bus de victoria.

"Solo necesito señor presidente y doctora Clara Luz Roldán que aprueben los 10 mil millones para hacer el gran 'Centro de Alto Rendimiento de Suramérica Oscar Figueroa' en la ciudad de Cali. Los pesistas de Colombia no tienen porqué estar sufriendo por la mala dirigencia deportiva. Yo solo quiero eso para que el Valle del Cauca siga sacando campeones", manifestó Figueroa, recordándole al presidente los compromisos adquiridos hace 4 años tras la buena participación en los juegos de 2012.

Varios son los atletas que han tomado una postura crítica, manifestando su descontento con la compleja situación del deporte en el país. Los pronunciamientos de los ciclistas **Esteban Chavez** hacia el alcalde Enrique Peñalosa por la carencia de un equipo ciclístico en la capital del país y de **Winner Anacona** en respuesta a la felicitación del presidente Santos por la representación colombiana en el Tour de Francia, considerando el pedalista que los éxitos alcanzados son fruto del trabajo de cada uno y no del respaldo gubernamental, son claro ejemplo de ello.

Además de lo expuesto, parte de la indignación que produce en un sector de quienes siguen las justas deportivas proviene de situaciones como la presencia de la gobernadora del Valle Dilian Francisca Toro en las gradas de Rio por invitación directa del Comité Olímpico Colombiano (COC), entidad encargada de garantizar las condiciones adecuadas para que los representantes deportivos del país tengan todas las condiciones en cuanto a su desplazamiento, estadía y desempeño en la competición; y aunque la mandataria regional ha manifestado que su visita hace parte de las apuestas de su programa de gobierno por apoyar a los deportistas y que los costos de tal operación no salen del presupuesto público de su departamento, queda la molestia en relación con la pertinencia de invertir dinero que bien podría servir para otros propósitos cercanos a lo deportivo.

Valdría la pena preguntarse si la disciplina, el empeño y sacrificio invertido por colombianos y colombianas provenientes de diferentes regiones del país y sus familias, se ve recompensado no sólo por los organismos encargados de tal labor, también por los espectadores y medios, quienes exigen más de lo que aportan y son los más duros críticos cuando los resultados no se dan de la manera esperada. Si la forma estruendosa en que se festeja corresponde al acompañamiento y respaldo a los procesos desde sus etapas más tempranas, si las diferentes disciplinas deportivas, y sus representantes hombre y mujeres, reciben la misma atención y visibilidad.
