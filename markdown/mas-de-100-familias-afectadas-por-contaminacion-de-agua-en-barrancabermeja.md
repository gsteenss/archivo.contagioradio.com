Title: Más de 100 familias afectadas por contaminación de agua en Barrancabermeja
Date: 2016-09-19 17:52
Category: Ambiente, Nacional
Tags: derrames de petróleo, Ecopetrol, explotación de petróleo, fracking
Slug: mas-de-100-familias-afectadas-por-contaminacion-de-agua-en-barrancabermeja
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Derrame-petróleo-Barranca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: W Radio ] 

###### [19 Sept 2016] 

Las comunidades del corregimiento Campo 23, de Barrancabermeja, denuncian que en los últimos días, de la tierra ha emanado una mezcla de agua y petróleo que no es natural sino que es causada por la técnica que está empleando Ecopetrol junto a la petrolera Occidental Andina, para elevar la presión y producir el arrastre del hidrocarburo.

De acuerdo Moisés Barón, líder de la zona, el petróleo está saliendo a la superficie debido a la inyección de más de **140 barriles de agua diarios que son usados para la producción de 45 mil barriles de crudo**. Los pobladores descartan que se trate de un afloramiento natural, como ha asegurado Ecopetrol, pues la zona es un campo de explotación que ya ha perdido su presión natural.

Para las comunidades es claro que no se trata de un afloramiento natural que indique un nuevo yacimiento o hallazgo petrolero, pues **desde hace más de cinco años la Occidental Andina viene implementando un proceso de recuperación secundaria**, con la inyección de miles de barriles de agua para provocar que el petróleo salga a la superficie.

Ecopetrol intenta encubrir los impactos ambientales de la técnica que está empleando para la recuperación de hidrocarburos, asegura el líder, y agrega que **los primeros brotes de petróleo y agua fueron vistos por las comunidades hace un año**, quienes creyeron que se trataba de algún rompimiento en la tubería o un derrame accidental; sin embargo, hoy están seguras que no accidental sino que es producto de la [[técnica empleada por las petroleras](https://archivo.contagioradio.com/el-costo-humano-del-petroleo-las-deudas-de-la-operacion-de-pacific-rubiales/)].

Uno de los graves riesgos que implica la utilización de esta técnica es que las fracturas inducidas para la explotación petrolera se conecten con las fracturas naturales y **terminen por contaminar los cuerpos de agua superficiales y subterráneos**, como está sucediendo en la región, afirma Barón, por lo que insiste en que es urgente que las autoridades ambientales se pongan al tanto de la situación y apliquen [[sanciones a las empresas](https://archivo.contagioradio.com/campesinos-sin-agua-para-beber-tras-derrame-de-petroleo-en-putumayo/)], porque nunca consultaron a las comunidades la implementación de la técnica y cuando tuvieron conocimiento de la emergencia no lo reportaron.

Todo el Magdalena medio está [[concesionado para fracking](https://archivo.contagioradio.com/en-colombia-hay-22-bloques-petroleros-destinados-para-fracking/)], hay 4 bloques que abarcan parte del Cesar y atraviesan el corredor de los municipios de [[San Martín](https://archivo.contagioradio.com/amenazan-a-ambientalistas-que-buscan-impedir-fracking-en-san-martin-cesar/)], Aguachica, Puerto Wilches, Barrancabermeja, Cimitarra, Puerto Boyacá, incluyendo algunas zonas de Antioquía. Hay otro brazo que pasa por el lado del Tolima hasta la zona de Cundinamarca; desafortunadamente Colombia está hoy designada por el Ministerio de Minas y la Agencia Nacional de Hidrocarburos, con el visto bueno del Ministerio de Ambiente, a los designios de la aplicación de esta nefasta técnica, concluye Barón.

<iframe id="audio_12971411" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12971411_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
