Title: Organizaciones defensoras de DDHH en México exigen “JusticiaParaRuben"
Date: 2015-08-03 14:34
Category: DDHH, El mundo
Tags: asesinatos contra periodistas, Derechos Humanos, Javier Duarte, Libertad de expresión, mexico, Reporteras Sin Fronteras, Rubén Espinosa, Veracruz
Slug: organizaciones-defensoras-de-ddhh-en-mexico-exigen-justiciapararuben
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Ruben-Espinosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.24horas.cl]

###### [3 Agosto 2015]

Organizaciones defensoras de derechos humanos y de libertad de expresión se han manifestado en rechazo al asesinato del periodista Rubén Espinosa, quien fue encontrado muerto el pasado primero de agosto junto con cuatro mujeres.

El fotoperiodista se había trasladado al Distrito Federal, debido a las amenazas que recibió en Veracruz, por lo que Rubén fue el primer periodista desplazado internamente según había denunciado la organización defensora de la libertad de expresión Artículo 19.

**“Me vine en un contexto de violencia contra los periodistas en Veracruz… Allí no hay estado de derecho, no hay justicia por ninguno de los 13 compañeros asesinados”,** había dicho Espinosa en una entrevista el pasado 9 de julio en el programa Periodistas de a Pie.

Veracruz es un Estado que se caracteriza por ser uno de los lugares de México con mayores índices de asesinatos de periodistas, incluso relacionados con Javier Duarte, gobernador de ese Estado.

Precisamente Espinosa, **en una reciente entrevista con el medio "SinEmbargo" había acusado directamente al Gobernador Duarte de los crímenes contra periodistas** que durante los primeros dos años de su administración ya contabilizaban nueve comunicadores asesinados y dos secuestrados, es por eso que uno de los ejes centrales del trabajo de Rubén tenía que ver con el cubrimiento de las protestas en las que se exigía esclarecer los asesinatos de comunicadores.

La fiscalía de Ciudad de México, adelanta las investigaciones del caso en el que se ha identificado a  Nadia Vera, activista de derechos humanos y a Yesenia Quiroz Alfaro, de 18 años de edad, además de una posible colombiana de la que aun no se tiene identificación.

Las víctimas “presentan una herida en la cabeza producida por disparo de arma de fuego" y algunas "escoriaciones en diversas partes del cuerpo presuntamente originadas por un forcejeo", afirmó el fiscal de la ciudad, Rodolfo Ríos.

El hecho también se investiga por robo, aunque entidades como la Comisión Nacional de Derechos Humanos exigió a la fiscalía seguir "**la línea de investigación relacionada con la labor periodística del fotorreportero"** y además solicitó “medidas de protección urgentes" para los familiares de Rubén.

Durante el domingo dos mil personas, en su mayoría comunicadores sociales, continuaron las protestas en distintas ciudades de México exigiendo la verdad sobre este crimen con pancartas que llevan escrito **“\#JusticiaParaRuben" **y  arengas en contra del gobernador de Veracruz.

Cabe recordar que según Reporteros Sin Fronteras, México es uno de los países con mayores índices de riesgo para los periodistas, ya que en su último reporte, **se cuentan más de 80 periodistas asesinados y 17 desaparecidos en los últimos 10 años.**

Compartimos el trabajo del fotógrafo Ruben Espinosa en la red social Instagram [https://instagram.com/espinosafoto/ ](https://instagram.com/espinosafoto/)

\
