Title: Países garantes y acompañantes piden a las partes el cese al fuego bilateral
Date: 2015-07-07 14:25
Category: Nacional, Paz
Tags: Cese al fuego bilateral, Chile, Cuba, desescalamiento del conflicto, FARC, Gobierno Nacional, Noruega, proceso de paz, Venezuela
Slug: paises-garantes-piden-a-las-partes-el-cese-al-fuego-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/El-Gobierno-de-Colombia-y-las-FARC-han-llegado-a-un-acuerdo-sobre-las-condiciones-para-la-liberación-anunciaron-representantes-de-Cuba-y-Noruega-países-garantes-del-proceso-de-paz.-EFE..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.elpais.cr]

###### [7 Jul 2015]

Este martes los países garantes y acompañantes del proceso de paz,  Cuba, Noruega, Chile y Venezuela pidieron a las partes **estudiar un "desescalamiento urgente"** del conflicto armado que conlleve a un **cese al fuego bilateral.**

En el comunicado se pide intensificar las medidas de construcción de confianza para garantizar un clima que permita lograr acuerdos sobre los puntos pendientes en la agenda de las conversaciones. Así mismo, los países reafirmaron su apoyo al proceso de paz.

\[embed\]https://www.youtube.com/watch?v=5hkfPYT918E\[/embed\]
