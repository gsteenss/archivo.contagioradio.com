Title: Jesús Adier Perafán,  segundo líder social asesinado del 2019
Date: 2019-01-04 13:03
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Slug: jesus-adier-perafan-segundo-lider-social-asesinado-del-2019
Status: published

###### [Foto: Archivo Personal] 

###### [04 Ene 2019] 

El segundo líder social asesinado del 2019 es Jesús Adier Perafán, presidente de la Junta Acción Comunal (JAC) en Caicedonia, Valle del Cauca, quien sufrió heridas de bala de un sujeto desconocido la noche del 31 de diciembre, mientras se encontraba en su tienda de abarrotes. Falleció horas después en el hospital.

La Fiscalía de Caicedonia anunció que las grabaciones de las cámaras del establecimiento y del sector serán analizadas para determinar la identidad de los perpetradores, además, el ente afirmó que **investigará si el asesinato de Perafán está relacionado con sus actividades comunitarios** como veedor de salud o presidente de la JAC local.

En su rol de presidente de la JAC de Caicedonia, Perafán se destacaba **por denunciar la corrupción en el manejo de los recursos públicos**. Por otra parte, también se desempeñaba como veedor de salud. De igual forma, Perafán era fundador de la organización política conservadora Coraje Caicedonia y aspiraba al Concejal de Caicedonia en las elecciones del próximo 27 de octubre de 2019.

Con el asesinato de Perafán, se cuentan dos homicidios de líderes sociales en los días iniciales del 2019; el primero se reportó como el asesinato de Gilberto Valencia en Suárez, Cauca. Solo unos días antes, Antonio Guterres, el secretario general de las Naciones Unidas, expresó su "grave preocupación" sobre la continuidad de asesinato de líderes sociales con impunidad en el último trimestre del 2018. (Le puede interesar: "[Consejo de seguridad de Naciones Unidas Preocupada por asesinato de líderes sociales en Colombia](https://archivo.contagioradio.com/onu-preocupada-por-el-asesinato-de-lideres-sociales-en-colombia/)")

###### Reciba toda la información de Contagio Radio en [[su correo]
