Title: Víctimas de la masacre de Rivera en Huila persiguen la esperanza de la verdad
Date: 2018-02-26 15:11
Category: DDHH, Nacional
Tags: concejales de rivera, FARC, Huila, masacre de rivera, rivera
Slug: victimas-de-la-masacre-de-rivera-en-huila-persiguen-la-esperanza-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/masacre-rivera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Fundación Sonrisas de Colores] 

###### [26 Feb 2018] 

Los familiares y habitantes del municipio de Rivera en Huila, realizaron la conmemoración **número 12 de la masacre de nueve concejales** perpetrada por la guerrilla de las FARC en 2006 aseguraron que no se ha realizado ningún tipo de reparación y los familiares de los concejales continúan a la espera de que se les cuente toda la verdad de lo que sucedió.

De acuerdo con Marta Aguirre presidente de la Fundación Sonrisas de Colores, organización que ha acompañado a las familias, el 27 de febrero de 2006 “a la 1:30 pm donde los concejales sesionaban la guerrilla ingresó y de **manera violenta los asesinó**”. Los guerrilleros ingresaron por la parte de atrás del recinto por lo que “tomaron desprevenida a la seguridad de los concejales”.

### **Gobierno no cumplió con compromisos con las familias** 

Estos hechos se registraron cuando el hoy senador Álvaro Uribe era presidente de la República y Juan Manuel Santos era su Ministro de Defensa. “Los dos se comprometieron y realizaron actos públicos donde le dijeron a las familias que las iban a apoyar y acompañar por ser víctimas y cuando las familias los buscaron en la Casa de Nariño **nunca dieron la cara**”, manifestó. (Le puede interesar:["Compulsan copias contra Álvaro Uribe por dos masacres en Antioquia"](https://archivo.contagioradio.com/alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo/))

Además, Aguirre indicó que con sus recursos y el apoyo de algunas empresas privadas y organizaciones sociales pudieron construir **el Obelisco de la Recordación**, un monumento donde aparecen las figuras de la cara de cada uno de los concejales asesinados. La Fundación Sonrisas de Colores, ha promovido una marcha de luz anual, un acto donde las familias realizan actividades de memoria en el municipio.

### **Familias le piden a las FARC que les cuenten la verdad** 

Las familias aseguran que socializaron con las FARC los hechos al momento de las negociaciones en la Habana “a través de Giovanny Córdoba, quien es un docente de Huila”. Él estuvo en la mesa de diálogo que tenía que ver con el tema de las comunidades afrocolombianas y en el tema de género y allí les entregaron **una carta** a los integrantes de ese grupo.

En esa carta, Aguirre aseguró que no les pidieron que hicieran un acto de perdón en la medida en que **“eso es lo mínimo que pueden hacer”**. Lo que le pidieron allí es “un acto de verdad completa” pues “el municipio no quiere verdades a medias”. Están esperando que les cuenten “por qué Rivera, por qué los concejales y por qué hicieron todo lo que hicieron en el departamento del Huila”.

Adicional a esto, indicó que han tenido conversaciones con **Victoria Sandino**, “que es quien puede servir de puente en el acercamiento con las familias”. Sin embargo, “las familias con tanto olvido y con tanto rechazo no están preparadas”. Por esto, aún se encuentran a la espera de que este acercamiento se pueda realizar.

Finalmente, recalcó que el fin de semana que se hizo la conmemoración, el municipio contó con el acompañamiento de **la Unidad de Víctimas** quien socializó la ley de víctimas con las familias y además realizaron un acompañamiento psicosocial “muy valioso”. Esto para las víctimas fue un acto significativo por parte del Estado.

<iframe id="audio_24083039" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24083039_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
