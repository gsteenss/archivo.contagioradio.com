Title: Arauca pide iniciar proceso de paz con ELN ante violencia registrada en paro armado
Date: 2016-09-14 11:14
Category: DDHH, Nacional
Tags: Arauca, ELN, paro armado, proceso de paz
Slug: arauca-pide-iniciar-proceso-de-paz-con-eln-ante-violencia-registrada-en-paro-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/soldados_del_eln-e1473856443724.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zona Cero 

###### [14 Sep 2016] 

El paro armado que adelanta el ELN en seis departamentos del país, ha afectado principalmente al departamento de Arauca, donde los municipios de **Fortul, Arauquita y Saravena, tienen paralizadas el 100% de las actividades** cotidianas, entre ellas, las jornadas educativas, el comercio y la movilidad.

"En momentos en que el mundo ve a Colombia como una esperanza de paz, estas acciones empañan esta alegría, de ahí la necesidad urgente de destrabar la mesa de diálogo con el ELN y que esa** guerrilla muestre verdaderas acciones de paz**", expresa Martín Sandoval, líder social y defensor de derechos humanos de Arauca.

Según Sandoval, la comunidad araucana rechaza estas acciones que tienen paralizado el departamento y que además mantienen en zozobra y atemorizada a la población. Piden que se inicie la [mesa pública de conversaciones con el ELN,](https://archivo.contagioradio.com/comision-etnica-se-ofrece-como-mediadora-para-reanudar-negociacion-gobierno-eln/) y no se responda por parte del gobierno, con más actos de violencia en los que los habitantes son los primeros afectados.

**"Acciones de fuerza de un lado y de otro no son convenientes,** lo que se debe imponer es la sensatez, el diálogo y la solución política, esa la invitación que hacemos, a que **se abra la fase pública de diálogos con ELN.** Estamos seguros de que las conversaciones van a avanzar", dice.

Según se ha conocido, ya han habido actos violentos como la quema de motocicletas en el perímetro urbano de Saravena, debido a que unas mujeres salieron a las calles a llevar a sus hijos al colegio. Ante esa situación, el gobernador de Arauca, anunció la suspensión de clases en todo el departamento, **dejando a 54 mil estudiantes afectados ante las amenazas a la libre movilidad.**

Así mismo, el defensor de derechos humanos reitera que no puede haber una paz incompleta, pues el ELN tiene una fuerte presencia en esa región, y por tanto, es una organización que merece una [negociación diferente](https://archivo.contagioradio.com/hay-que-insistir-en-que-gobierno-y-eln-se-sienten-a-negociar-victor-de-currea/) a la de las FARC.

Además de estos tres municipios, durante el desarrollo del paro también se reporta parálisis con menor intensidad en Tame y aunque la capital de Arauca también se ha visto afectada, allí mayor movilidad.

<iframe id="audio_12905360" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12905360_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
