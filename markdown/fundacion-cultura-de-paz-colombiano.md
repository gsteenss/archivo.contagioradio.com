Title: Fundación Cultura de Paz llama al Estado colombiano a cumplir lo pactado con el ELN
Date: 2019-07-13 12:18
Author: CtgAdm
Category: Paz, Política
Tags: acuerdos, Cuba, ELN, paz
Slug: fundacion-cultura-de-paz-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Vida-Nueva.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Vida Nueva  
] 

La **Fundación Cultura de Paz, presidida por Federico Mayor Zaragoza**, quien fuese director general de la UNESCO, publicó un documento de 45 puntos que recoge el análisis de la situación del proceso de paz con el ELN. El documento analiza jurídicamente la suspensión de los diálogos y las solicitudes del gobierno de Colombia para que los países garantes capturen y extraditen a los y las integrantes de la delegación de paz de esa guerrilla.

Según la Fundación Cultura de Paz, a la luz del Derecho Internacional, el Estado Colombiano está comprometiendo su propia palabra frente a la comunidad internacional y quiere comprometer a los países que, de buena fe, han sido garantes del proceso para que incumplan acuerdos que tienen que ver con la ética, el derecho y la política internacional.

**El derecho internacional respalda a los países garantes y a los países sede de un diálogo que están obrando de buena fe y conforme a lo pactado.** Además de recordar y validar jurídicamente el pacto firmado en caso de ruptura, el documento expresa la necesidad de que se respete el compromiso del país sede y de los países garantes.

Así se señala que la reciente petición del Alto Consejero de Paz, Miguel Ceballos a la INTERPOL para capturar los integrantes de la delegación de paz del ELN y su extradición desde Cuba es una solicitud inviable. **Hacer efectiva esa solicitud implicaría que nunca más podría volverse a confiar en en los países garantes o sede para ningún proceso de solución negociada de conflictos en el mundo.**

### **"Lo pactado obliga, no se puede romper un pacto".** 

Así mismo, recuerdan los firmantes que uno de los deberes del Estado garante y el Estado involucrado en la necesidad de solución de un conflicto, es determinante y obligatorio cumplir el protocolo de retorno para la contraparte. (Le puede interesar: ["ELN depositó acuerdos parciales de paz ante la comunidad internacional"](https://archivo.contagioradio.com/eln-acuerdos-comunidad-internacional/))

Señalan que “el respeto y la presunción de derecho de la que gozan los países garantes, y específicamente la República de Cuba, que aceptó ser sede de los diálogos de paz, es decir para ese fin y no otro, y por eso mantuvo y mantiene claras las condiciones para las partes, entendiendo que ambas – Estado colombiano y ELN – se convocaron para dialogar y construir acuerdos, y nunca para desarrollar, ninguna de las dos, acciones de naturaleza militar, coercitiva, judicial o de índole semejante desde suelo cubano”.

### Las posibles soluciones planteadas 

La Fundación Cultura de Paz plantea posibles soluciones como retomar los diálogos, **suspender las órdenes de captura para que el ELN haga consultas internas y regrese al país sede,** o para que definitivamente se queden en territorio colombiano a la espera de un nuevo llamado a retomar las conversaciones cuando el Estado lo considere.

También se plantea la posibilidad de que la situación se lleve a la Corte Internacional de La Haya y que allí se defina el mejor mecanismo de solución, o que se convoque un panel internacional de expertos que surtan un informe y unas posibles salidas a la luz del Derecho Internacional y el Derecho Internacional Humanitario.

**Agrega el documento que no se puede seguir sosteniendo a la delegación del ELN en circunstancias impropias, ni se debería extender el tiempo de presión sobre los países garantes y el país sede.**

Adicionalmente, hacen un llamado a mantener la posibilidad de una salida dialogada al conflicto e invitan al gobierno colombiano asumir la responsabilidad histórica de garantizar el derecho a la paz de toda Colombia. (Le puede interesar: ["Tras 55 años en armas, ELN afirma que quiere una salida negociada al conflicto"](https://archivo.contagioradio.com/55-anos-eln-quiere-salida-conflicto/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
