Title: Cinco funcionarios públicos de Ferguson han renunciado tras informe sobre racismo
Date: 2015-03-11 19:22
Author: CtgAdm
Category: DDHH, El mundo
Tags: ferguson, informe racismo, jovenes negros
Slug: cinco-funcionarios-publicos-de-ferguson-han-renunciado-tras-informe-sobre-racismo-de-la-policia-y-la-justicia
Status: published

###### Foto:Elciudadano.cl 

Este martes por la noche **dimitió del cargo de administrador** del municipio de **Ferguson**, estado de Misuri en Estados Unidos, **John Shaw** después de las fuertes críticas a raíz del informe emitido por la justicia estadounidense **sobre racismo en el actuar de la policía y las autoridades judiciales en el municipio**.

Shaw antes de dimitir se defendió de las acusaciones declarando que "...Mi oficina nunca ha instruido al departamento de policía para poner en el punto de mira a afroamericanos, ni falsificar multas, ni abusar de los pobres...".

Con el administrador encargado de supervisar todos los servicios de la ciudad, ya van **cinco los funcionarios que han dimitido** desde la publicación del informe. A pesar de las fuertes críticas de la población civil y de organizaciones de DDHH, **el alcalde de la ciudad James Knowles agradeció el trabajo del administrador** y aprovechó para decir que el no tomaría el camino de la renuncia.

Otra de las medidas tomadas al respecto es la **evaluación** por parte de la Fiscalía General de EEUU, de la posibilidad de **desmantelamiento del departamento de policía del municipio**.

En 2014 murió asesinado a manos de la policía el joven negro Michael Brown. Asesinado a tiros mientras iba desarmado, el asesinato provocó la ira y la indignación de la comunidad negra. Fueron **dos semanas de fuertes protestas** que obligaron al estado a imponer el estado de sitio.

Hace dos semanas la **Justicia estadounidense publicó un informe** sobre el proceder de la justicia y de las fuerzas de seguridad en Ferguson acusándoles de seguir un **patrón racista la hora de proceder**. Cifras como el 95% de las personas retenidas más de dos días en la cárcel de la ciudad eran afroamericanos o que el 88% del uso excesivo de la fuerza fue utilizada contra población negra.

Pero los asesinatos contra jóvenes negros desarmados por parte de la policía continúan, el último un **joven de 19 años,** el viernes pasado en el estado de **Wisconsin** , siguiendo el mismo patrón y que ha vuelto a sacar a la comunidad negra a las calles con el lema **"Black Lives Matter" ("La vida de los negros importa").**
