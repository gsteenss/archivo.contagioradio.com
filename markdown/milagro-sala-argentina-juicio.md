Title: Sin veredicto Milagro Sala pasará navidad en prisión
Date: 2016-12-23 12:30
Category: El mundo, Otra Mirada
Tags: Argentina, Dilma Rouseff, Milagro Sala, presos politicos
Slug: milagro-sala-argentina-juicio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/63334_web-juicio-milagyo-sala2-eduaydo-sayapuya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Tiempoar 

##### 23 Dic 2016 

Tras cumplirse las audiencias programadas para esta semana en el j[uicio que se adelanta en contra de Milagro Sala](https://archivo.contagioradio.com/inicio-juicio-milagro-sala/), dirigente del movimiento popular Tupac Amarú y lider barrial e indígena, **no hay un veredicto final**, por lo cual **la activista deberá pasar las fiestas navideñas en detención**.

Durante su intervención de casi dos horas, la abogada **Elizabeth Gómez Alcorta solicitó la absolución de su defendida por ausencia de pruebas y prescripción de los delitos** de amenaza coactiva y daño agravado por los cuales los demandantes, exigen 8 años y la Fiscalía pide 3 años de cumplimiento condicional.

**El testimonio de los dos testigos presentados por Gerardo Morales**, gobernador de Jujuy durante 2009, año en que se realizó el escrache por el que se acusa a Sala y los activistas Graciela López y Ramón Salvatierra, **ha sido desmentido por la defensa en varias oportunidades** aludiendo que "han mentido en distintos momentos y todo su argumento se ha caído".

Los esposos René Arellano y Cristina Chauque, testigos presentados por los querellantes, declararon que habian participado en una supuesta reunión preparatoria del escrache en la que la activista les había conminado a agredir directamente a Morales, e**videncia desvirtuada por la defensa** al asegurar que "a los únicos que podría haber instigado Milagro Sala era a ellos y según sus dichos, no concurrieron a la manifestación. Por lo tanto, si no fueron a la manifestación no fueron instigados".

A la proposición adicional de la abogada Gómez Alcorta de absolver a Sala por la prescripción de los delitos, se suma la **solicitud de investigar al matrimonio por falso testimonio** asi como al jefe policial Fabio Serpa, quien declaró que había ordenado infiltrar una manifestación sin la correspondiente orde judicial.

En la última audiencia que se extendió hasta las 2 a.m. y luego de escuchar los argumentos de la defensa, los magistrados del Tribunal Oral en lo Criminal Federal de Jujuy, presidido por Mario Juárez Almaráz, decidieron extender el dictamen de la sentencia para el próximo miércoles 28 de diciembre.

### Dilma aboga por Milagro Sala 

![Dilma](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/15672699_1292796147440640_1778628566709287927_n.jpg){.alignnone .size-full .wp-image-34056 width="768" height="512"}

La presidenta elegida democráticamente y destituída por el impeachment **Dilma Rousseff, expresó este jueves su apoyo a la líder indígena** y Diputada de Parlasur. A través de su cuenta en Facebook, la ex mandataria escribió un mensaje de apoyo a la campaña [[[\#]{._58cl ._5afz}[libertenamilagro]{._58cm}]{._5afx}](https://www.facebook.com/hashtag/libertenamilagro?source=feed_text){._58cn} , piendo la libertad para Sala.

El llamado de Rousseff se une a las recomendaciones hechas por [El Consejo de derechos humanos de la ONU](https://archivo.contagioradio.com/naciones-unidas-pide-libertad-para-milagro-sala/)y la Comisión Interamericana de derechos humanos, entidades que **consideran que la detención de Milagro Sala es arbitraria y piden su liberación.**
