Title: “Erradicación forzada y sustitución no son compatibles con la implementación”: Cesar Jerez
Date: 2017-01-12 13:08
Category: Nacional, Paz
Tags: erradicación cultivos ilícitos, implementación acuerdos de paz
Slug: erradicacion-forzada-y-sustitucion-no-son-compatibles-con-la-implementacion-cesar-jerez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/coca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Ene 2017] 

**Comunidades campesinas de diferentes regiones del país, han expresado su indignación** frente al   anuncio hecho por el Ministerio de Defensa ,sobre retomar las fumigaciones con glifosato y continuar con la erradicación de cultivos ilícitos de manera forzada, actos que no solamente ponen en riesgo la vida de los campesinos, sino q**ue también van en contra de los acuerdos de paz de la Habana y de la implementación de los mismos.**

De acuerdo con Cesar Jerez, vocero de la Asociación Campesina del Catatumbo, las decisiones anunciadas por Villegas, ministro de defensa, “son preocupantes y ambiguas por parte del gobierno, que de un lado dice que cumplirá los acuerdos de paz de La Habana, mientras que por el otro insiste en las medidas de fuerza con erradicación violenta contra los campesinos” y agregó que **“erradicación forzada y sustitución no son compatibles”.**

Y es que a lo largo del año 2016 y desde principios del 2017, en territorios del país como Catatumbo, Nariño, Putumayo y Córdoba, las comunidades han expresado que pese a que hagan parte de los planes de sustitución de cultivos ilícitos del acuerdo, continúan siendo agredidos y amenazados por parte del Ejército y **"señalados de ser narcotraficantes".** Le puede interesar:["Con erradicación forzada se atenta contra acuerdo de paz: ASCSUCOR"](https://archivo.contagioradio.com/erradicaciones-forzadas-atentan-contra-acuerdo-de-paz/)

El caso más reciente fue denunciado por campesinos de la vereda  de Taque, en el municipio de Toledo, Antioquía, en donde miembros del **Ejército Nacional han estado erradicando de manera forzada cultivos de hoja de coca**, desconociendo que la comunidad está en la construcción de una propuesta de sustitución voluntaria desde el año 2016, que permita garantías reales de vida enmarcadas en los acuerdos de paz de La Habana.

Actos como estos para Jerez “destruyen los consensos que se han construido para lograr que los campesinos se acojan al acuerdo y para que no salgan lastimadas más de **250 mil familias que de manera directa e indirecta, dependen del cultivo de coca, marihuana y amapola en Colombia**, por lo que hay que dejar claro que no es cierto que acuerdos en conjunto entre administradores públicos ni acuerdos individuales con familias, sean parte de la implementación de la sustitución de cultivos ilícitos”

Durante el 10 y el 11 de enero en Bogotá, se llevó a cabo un encuentro de más de **100 campesinos cocaleros** que en respuesta a los múltiples incumplimientos por parte del gobierno, decidieron crear la Coordinadora Nacional de Campesinos Cultivadores de Coca  y Amapola (COCAM)**, con la finalidad de que los campesinos sean protagonistas y tengan incidencia en la construcción de paz** en el país y en la implementación de los acuerdos de paz.

Para finalizar Jerez afirma que **“hay más de 500 puntos en donde el gobierno quiere llevar a cabo erradicaciones forzadas y allí encontrarán una resistencia y acción masiva de protesta de los sectores cocaleros”** Le puede interesar: ["Erradicaciones forzadas son otra forma de matar al campesino"](https://archivo.contagioradio.com/erradicaciones-forzadasotra-manera-de-matar-al-campesinado/)

<iframe id="audio_16189457" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16189457_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
