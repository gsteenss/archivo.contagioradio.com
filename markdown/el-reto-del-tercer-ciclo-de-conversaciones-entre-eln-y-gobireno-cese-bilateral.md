Title: Diálogo ELN - Gobierno debe ir acompañado por acciones de paz desde las montañas
Date: 2017-07-24 14:40
Category: Paz, Política
Tags: ELN, Proceso de paz Quito
Slug: el-reto-del-tercer-ciclo-de-conversaciones-entre-eln-y-gobireno-cese-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Telégrafo] 

###### [24 Jul 2017] 

Inicia el tercer ciclo de conversaciones entre el ELN y el Gobierno Nacional que tiene como principal meta pactar el cese unilateral y temporal al fuego, antes de la llegada del Papa Francisco a Colombia, para el gestor de paz Carlos Velandia, este **reto implicará que ambas partes demuestren su voluntad con acciones que contribuyan a desescalar el conflicto armado.**

Velandia manifestó que solo se tendrán 6 semanas, para que tanto el equipo de conversaciones del ELN como del gobierno, puedan encontrar puntos en común que les permitan llegar a la meta del cese bilateral, “la mejor manera de hablar es el hacer, las dos partes han dicho que tienen voluntad de lograr un cese bilateral al fuego, **entonces que empiecen a desescalar**” afirmó el gestor. (Le puede interesar: ["ELN está listo para firmar el cese bilateral al fuego"](https://archivo.contagioradio.com/42291/))

Desde hace algunas semanas se han producido combates entre ambos sectores, en diferentes regiones del país, para Velandia estos hechos generan** una falta de credibilidad de la sociedad hacia la mesa de conversaciones**, por tal razón, considera que empezar por finiquitar estas acciones es el mejor paso para continuar con las conversaciones.

“El tiempo de la negociación es valiosísimo, las partes no pueden desaprovecharlo ni enredarse, por eso se requiere voluntad si lo que están haciendo en Quito se da mientras que en las montañas de Colombia se están dando situaciones que desdicen**,** va a ser muy difícil, **el trabajo de Quito debe ser acompañado desde los montes de Colombia**” aseguró Velandia. (Le puede interesar: ["ELN y Gobierno incluyen Cese bilateral en la agenda"](https://archivo.contagioradio.com/eln-y-gobierno-incluyen-el-cese-al-fuego-en-la-mesa-de-conversaciones/))

Otro de los riesgos que deberá afrontar el proceso de paz entre el ELN y el gobierno Nacional, es la campaña electoral que ya se ha puesto en marcha, ante lo que Velandia asegura **es necesario llegar prontamente a un punto de no retorno que garantice la continuación de los diálogos a futuro**.

<iframe id="audio_19968218" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19968218_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
