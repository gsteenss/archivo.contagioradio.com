Title: ONU declara ilegales las colonias israelíes en territorio palestino
Date: 2016-12-26 10:41
Category: DDHH, El mundo
Tags: Israel, ONU, Palestina
Slug: onu-declara-ilegales-las-colonias-israelies-territorio-palestino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/palestina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ONU/Archivo 

###### [26 Dic 2016]

Por medio de una votación que impulsó el pasado viernes **Nueva Zelanda, Malasia, Senegal y Venezuela,** el Consejo de Seguridad de Naciones Unidas, estableció mediante la resolución 2334, que las colonias israelíes en territorio palestino son ilegales y por tanto, deben ser detenidas.

Esta resolución establece que "Israel cese inmediatamente y completamente los asentamientos en los territorios palestinos ocupados, incluido Jerusalén Este". Asimismo, señala que las **colonias israelíes "no tienen validez legal"** y  además, "están poniendo en peligro la viabilidad de la solución de los dos Estados", pues es una situación que no contribuye a la paz.

En los últimos meses la ONU ha advertido que las construcciones israelíes en Palestina han incrementado. **Cerca de 430.000 israelíes actualmente ocupan Cisjordania y 200.000 se encuentran en Jerusalén Este.**

### La reacción de Israel 

Una llamada de Donald Trump hizo que Egipto postergarla la votación, pero al ser nuevamente promovida, Estados Unidos decidió abstenerse, aunque el embajador israelí ante la ONU, Danny Danon, manifestó que esperaban que Estados Unidos vetara "esta vergonzosa resolución".

"El gobierno Obama no solamente **falló para proteger a Israel contra esta conspiración en la ONU**, sino que coludió con ella entre bastidores", y ante la llegada de Trump agregó, "**Israel espera trabajar con el presidente electo Donald Trump** y con todos nuestros amigos en el Congreso, tanto republicanos como demócratas, para eliminar los efectos dañinos de esta resolución absurda".

Tras la decisión, por un mes los ministros de Benjamín Netanyahu tienen prohibido viajar o tener relaciones diplomáticas con los 12 países que votaron a favor. Netanyahu, ha asegurado que se trata de **una  resolución es “antiisraelí”.**

Por el momento analistas afirman que el gobierno de Netanyahu arremeterá con medidas económicas. Israel ya canceló su apoyo financiero para Senegal y suspendió la financiación de cuatro organismos de Naciones Unidas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
