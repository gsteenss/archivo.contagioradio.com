Title: Jurisdicción Especial debe beneficiar a las víctimas
Date: 2017-02-20 16:59
Category: Nacional, Paz
Tags: Implementación de los Acuerdos de paz, Jurisdicción Espacial de Paz
Slug: jurisdiccion-especial-debe-beneficiar-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Víctimas-de-Trujillo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [20 Feb 2017] 

Autonomía para la Unidad de Búsqueda de Personas Desaparecidas, tratamiento exclusivo de los casos más graves y representativos por parte del Tribunal para la Paz, **obligación de la Fuerza Pública a reparar integralmente a las víctimas y respeto por parte del Estado a las disposiciones del Derecho Penal Internacional** y el Derecho Internacional Humanitario frente a la cadena de mando, fueron algunas sugerencias hechas por expertos a la Jurisdicción Especial.

Para Enrique Santiago, asesor jurídico de las FARC, entes como la Fiscalía han obviado que existe un Acuerdo firmado y pretenden re negociar “hasta la última palabra de lo que tiene que ver con justicia” y **busca limitar al máximo las competencias de esta Jurisdicción Especial para llevar los casos de guerrilleros a la justicia ordinaria,** señala que la Fiscalía “no debería atribuirse estas competencias".

### La Jurisdicción Especial debe estar del lado de las víctimas 

Las organizaciones resaltaron que quienes deben estar en el centro de los Acuerdos y en general, los beneficiados de la JEP deben ser las víctimas, y señalaron que **“los ajustes hechos por otros sectores han beneficiado a las minorías políticas y económicas”** y han dejado totalmente fuera a las víctimas. ([Le puede interesar: En debate de Jurisdicción Especial para la paz algunos buscan impunidad](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-2/))

Indicaron que la Unidad de Búsqueda requiere autonomía e independencia frente a las ramas del poder público, “para actuar sin restricciones en la búsqueda y localización de personas dadas por desaparecidas en el conflicto” y en particular en aquellos casos en los que “estén involucrados agentes del Estado”, pues es importante que la UBPD **sea percibida por todas las víctimas “como una entidad imparcial en la que puedan confiar”.**

Frente a la competencia del Tribunal para la Paz, manifestaron que este debe concentrarse desde un inicio en la “investigación, juzgamiento y sanción de quienes tuvieron una participación determinante en la comisión de los crímenes más graves y representativos”, para así evitar el colapso de la JEP y hacer posible **“luchar de mejor manera contra la impunidad y contribuir a la satisfacción de los derechos de las víctimas”.**

La creación constitucional de la JEP es esencial para lograr un equilibrio entre la garantía de los derechos de las víctimas y los propósitos del proceso de paz, de dejación de armas y reintegración de los excombatientes, por ello insisten en que es necesario que la JEP **“brinde cierre jurídico y definitivo al conflicto armado”** y eso sólo se logrará con “la autonomía e independencia que tenga frente a otras jurisdicciones”.

### Agentes del Estado no pueden evadir sus responsabilidades 

Destacan que la comunicación y coordinación entre la JEP, la Jurisdicción Ordinaria y la Jurisdicción Especial Indígena, será fundamental para que sea una realidad **la respuesta a las necesidades de las comunidades y la resolución efectiva de conflictos en los territorios. **([Le puede interesar: La Jurisdicción especial no puede dejar fuera a las víctimas](https://archivo.contagioradio.com/jurisdiccion-especial-no-puede-dejar-fuera-a-las-victimas/))

Manifestaron que el buen funcionamiento de la JEP frente a las cadenas de mando, depende de que los juicios **“se realicen en el marco de una negociación global de paz y que se respeten los deberes especiales”** que los agentes estatales tienen frente a los derechos humanos.

Por último, hacen un llamado al Estado colombiano para que no haga parte de los sectores que **“pretenden obstaculizar la justicia para las víctimas”** y a la sociedad colombiana para que se comprometa con la veeduría y control de la fase de implementación, y en especial del desarrollo de la Jurisdicción Especial para la Paz.

###### Reciba toda la información de Contagio Radio en [[su correo]
