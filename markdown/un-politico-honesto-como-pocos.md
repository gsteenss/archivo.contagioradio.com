Title: Un político honesto, como pocos
Date: 2015-04-07 10:00
Author: CtgAdm
Category: Camilo, Opinion
Tags: carlos gaviria, humanista, político
Slug: un-politico-honesto-como-pocos
Status: published

Por [**Camilo De Las Casas - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas)

###### Abril 6 de 2014 

Así es la vida, cuando menos se espera pasa lo que a veces menos se desea, la muerte de alguien. Y entonces queda la posibilidad de reencontrarnos en su memoria o en el olvido. Nos quedan a veces discursos, intervenciones académicas, gestos, olores, imágenes. Siempre la partida genera añoranzas, o expresiones de la bondad del ser humano que parte; muchas veces, en particular los políticos, con mucha hipocresía, con mucha doble cara se refieren a los fallecidos, la política se ha hecho muy distante de la ética, para ellos no hay muerto malo, si les permite seguir en la video, y así, si en vida se hizo la guerra sin ética, con la falsedad de la condición humana, se expresan condolencias o pesares

No voté por **Carlos Gaviria** pero coincidí con él en diversos espacios, algunos seminarios y conversatorios, ambos en la mesa. El recordarlo hoy, incluso, el saberlo en una clínica, me permitió añorar su lucidez y coherencia racional, y su actitud razonable, y liberal en el pleno sentido de la palabra. Quedé en deuda con el maestro Gaviria pues nunca le envié dos ensayos que escribí sobre el paramilitarismo como cultura política, eso me amarga, pero creí que era poco para sus coherentes pronunciamientos como político de izquierda, como jurista y hasta esteta.

No voté por él, justo porque lo consideraba liberal y en mi esquematismo no era posible que fuera el candidato de un partido de izquierda. Con el paso del tiempo a mi ciego dogmatismo le permití una veta de duda y comprendí que lo más revolucionario en Colombia era ese humanista liberal, justo ante la imbricada cultura conservadora y neoconservadora con asiento en la tierra, en la iglesia católica, en el militarismo y también en el empresariado financiero y extractivista.

Carlos Gaviria no fue el político perfecto, pues no existe; tampoco el jurista sin error. Carlos Gaviria era justo un hombre absolutamente dialéctico, liberal y humanista; fue el hombre intentando la coherencia kantiana, obrando de tal manera que su actuar se convertía en máxima. Esto significaba en lo cotidiano un existir pleno, bello en lo estético, ético en lo poético.

Para algunos no era el político deseado porque no arrastraba a las masas, al estilo Gaitán, pero su contacto con la población llevo a generar la percepción del “Papá Noel”, un hombre tierno de alguna manera, pero sobre todo sabio. Incluso sin que la gente pudiera comprender la exquisitez de su lenguaje y sus planteamientos filosóficos, la gente de a pie comprendió que se trataba de un hombre humanista, luchador por la justicia y contra la inequidad. Si bien, no era un sujeto de arrastrar masas al estilo caudillista, eso es verdad, nadie podrá indicar que se le percibía como un practicante de la politiquería, de la deshonestidad, y de la mercantilización de la política o de los negocios en que se ha convertido el ser político.

Era un jurista que debatió casi en solitario o con unos pocos en Colombia, contra todos los vientos, a aquellos que negaban la existencia del conflicto armado interno, en la era de Uribe, y que debatía al Santos de la postiza paz, la pax. Gaviria afirmó basado en la filosofía del derecho la importancia del reconocimiento del delito político enfrentando al encantador de serpientes y sofista, Luis Carlos Restrepo, cuando este lo acusó de ser aliado de las FARC en toda una estrategia de desprestigio, a la que se sumaron posteriormente las operaciones ilegales desde la Casa de Nari.

Carlos Gaviria, era un guerrero de las ideas que se enfrentó al guerrero sin escrúpulos, sin respeto  al honor, a los protocolos mínimos del decoro a Uribe. Èste no conoce la palabra honor en el sentido clásico del hombre limpio y respetuoso de las normas del debate. Uribe careció del respeto al honor del contradictor para irse  a la guerra sucia desde Primero Colombia, y su parapolítica, que luego la trasladó a las operaciones ofensivas e ilegales del DAS, operación calculada contra contra Gaviria y contra el Polo, y muchos otros más. Por eso me cuesta trabajo creer en la condolencia tuitera del uberrista.
