Title: Más de 150 activistas se reúnen en Bogotá para hacer resistencia pacífica a la militarización
Date: 2019-07-31 14:43
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: América Latina, Antimilitarismo, Objeción de conciencia en Colombia
Slug: mas-de-150-activistas-se-reunen-en-bogota-para-hacer-resistencia-pacifica-a-la-militarizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Antimilitaristas.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ConferenciAnti1] 

Más de 150 activistas del mundo se han reunido del 30 de julio al 1 de agosto en Bogotá para asistir a la **Conferencia Internacional Antimilitarismos en Movimiento** , un evento que se realiza cada cuatro años con el fin de [compartir experiencias de narrativas de resistencia a la guerra, dialogando sobre antimilitarismo, objeción de conciencia y la no violencia. ]

**Wendy Ramos, integrante de Justapaz,** organización que hace parte del evento señala que la militarización es una problemática que no solo se vive en Colombia y que el compartir experiencias en esta conferencia les ha permitido conocer escenarios similares en diferentes países de Latinoamérica y el mundo.

### **Se está militarizando la mente de la sociedad** 

Existe la creencia que cuando se trata de militarización, únicamente se ocupa un territorio, sin embargo, el diálogo y las narrativas que han podido ser compartidas permiten concluir que también **"los cuerpos, las mentes y la vida"** han sido invadidos por esta potencialización de las fuerzas armadas en la cotidianidad.

[Ramos señala que la militarización abarca un espectro mucho más amplio que incluye aspectos como la educación, el espacio público, los medios de comunicación o la infancia, "asumimos que está bien que nuestros niños y niñas se vistan de camuflados y lo llevamos también al patriotismo".]

De igual forma, Wendy Ramos también alerta sobre el rol de la mujer en la sociedad y cómo se ha visto militarizado históricamente, al convertir su cuerpo en "un botín de guerra", girando en torno a las dinámicas de la guerra. [(Le puede interesar: Cuatro pasos para ser un objetor de conciencia en Colombia)](https://archivo.contagioradio.com/cuatro-pasos-para-ser-un-objetor-de-conciencia-en-colombia/)

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2F673491506434684%2Fphotos%2Fa.696689307448237%2F696689280781573%2F%3Ftype%3D3&amp;width=500" width="500" height="308" frameborder="0" scrolling="no"></iframe>

### No confundir militarización con militarismo 

Pese a que son diferentes, se tiende a pensar que militarización y militarismo son dos palabas iguales, la primera habla de la potencialización de las Fuerzas Militares para cumplir tareas específicas en un territorio, el militarismo hace referencia a una imposición de valores o ideales militares sobre la sociedad civil, lo que podría resultar incluso más alarmante que la militarización como acción.

[Ante esta tendencia a la militarización, las personas reunidas entorno a esta conferencia, han comenzado a trabajar para buscar alternativas de incidencia, resistencia noviolenta y generar redes entre organizaciones, **"hay que seguir resistiendo a la guerra, aunque nos amenace , aunque nuestros contextos sean complicados, hay que resistir"**, afirma la integrante de Justapaz.]

### Narrativas desde Colombia y el mundo 

Con el apoyo de diversas naciones se abordarán temas como la  militarización de comunidades fronterizas entre México y USA, el impacto del extractivismo en la vida de las mujeres y el ambiente en Bolivia, además de la participación de otros países como Sudán del Sur o Palestina.

> "Sudán del Sur lleva más de 60 años de conflicto armado (...) La violencia no resuelve nuestros problemas sólo los posterga" Moses Monday[\#AntiMilisEnMovimiento](https://twitter.com/hashtag/AntiMilisEnMovimiento?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/dgX406FIfn](https://t.co/dgX406FIfn)
>
> — ANTIMILITARISMOS EN MOVIMIENTO (@ConferenciAnti1) [July 31, 2019](https://twitter.com/ConferenciAnti1/status/1156609041658982402?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Desde Colombia también se compartirán testimonios de las comunidades de **El Garzal, Bolívar o Ituango, Antioquia quienes compartirán las formas no violentas de luchar contra el despojo de tierras despojo de tierras y el extractivismo,** respectivamente.

Adicionalmente también se abordarán otro tipo de problemáticas como el cambio climático, la concentración de tierra, el desplazamientos forzoso, el extractivisimo, tratados de armas, discriminación, racismo e inequidad de género y soluciones a estas como mesas temáticas entorno al diálogo con una paz justa y sustentable, justicia transcional, moviliación social y formas alternativas de comunicación.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_39328804" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39328804_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
