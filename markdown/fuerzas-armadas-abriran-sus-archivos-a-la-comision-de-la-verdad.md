Title: Fuerzas Armadas abrirán sus archivos a la Comisión de la Verdad
Date: 2017-12-17 15:14
Category: Nacional, Paz
Tags: comision de la verdad, Fuerzas Armadas, verdad, víctimas
Slug: fuerzas-armadas-abriran-sus-archivos-a-la-comision-de-la-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Dic 2017] 

El pasado 15 de diciembre, la Comisión para el Esclarecimiento de la Verdad, la Convivencia y la no Repetición se reunió con el Ministro de Defensa, Luis Carlos Villegas y el Estado Mayor de las Fuerzas Militares, hecho que ratificó un compromiso de todas las partes, por aportar a la búsqueda de la verdad, **incluso con la  información que tienen las Fuerzas Armadas y que permite  la posibilidad de conocer el sentir de los militares y de sus familias sobre la historia del conflicto**

En la reunión, en la que participaron el General Alberto Mejía, y los comandantes del Ejército, la Policía, la Armada y la Fuerza Aérea, así como otros altos mandos, también se pudo establecer la importancia de encontrar una verdad que dignifique a las víctimas “que reconozca el sufrimiento de **los miles de colombianos y que siente las bases de la no repetición de los hechos que degradaron la guerra**”.

De acuerdo con el padre Francisco de Roux, director de la Comisión, **se encontraron con unas Fuerzas Armadas comprometidas en la construcción de paz, que han actuado en la protección del proceso** en momentos críticos y que están dispuestas a continuar apoyando la implementación de los Acuerdos de Paz. (Le puede interesar:["Organizaciones sociales respaldan Comisión de la Verdad")](https://archivo.contagioradio.com/comunidades-respaldan-y-apoyan-a-la-comision-de-la-verdad/)

De igual forma, afirmó que también se evidenció un gran interés desde las Fuerzas Armadas por contribuir al esclarecimiento de la verdad en todos los terrenos, incluso en el reconocimiento de responsabilidades y el acceso de la Comisión de la Verdad a los archivos en el cumplimiento de su mandato.

Por otra parte, la Comisión de la Verdad se comprometió a “actuar con rigor objetivo, reconociendo el sufrimiento de las víctimas de todos los lados del conflicto, escuchando a todas las voces y **buscando cerrar las heridas que ha dejado una confrontación de tantos años entre colombianos**”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
