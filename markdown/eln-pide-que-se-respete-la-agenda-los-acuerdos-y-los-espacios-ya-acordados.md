Title: ELN pide que se respete la agenda, los acuerdos y los espacios acordados
Date: 2016-08-08 15:10
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Diálogos de paz Colombia, Negociaciones con ELN
Slug: eln-pide-que-se-respete-la-agenda-los-acuerdos-y-los-espacios-ya-acordados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/entrevista-eln.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [8 Ago 2016] 

El pasado viernes el Comando Central guerrilla del ELN emitió un comunicado en el que asegura que respeta la mesa de diálogos de La Habana y reitera que tiene toda la voluntad de avanzar en la construcción de acuerdos con el Gobierno y con la sociedad en general que es la que debe discutir las reformas estructurales al Estado.

El ELN insistió en la necesidad de poner sobre la mesa los temas contenidos en los acuerdos con las FARC y que es necesario que se profundice la discusión pues en lo que se conoce se "mantiene **intacto el régimen oprobioso de violencia, exclusión, desigualdad, injusticia y depredación**".

De acuerdo con Marylen Serna, integrante del Congreso de los Pueblos, el comunicado da cuenta de la reflexión profunda y estructural del ELN sobre las **causas que llevaron a la lucha armada para lograr un nuevo modelo económico y político**. Para Serna el ELN contrasta estas causas con la realidad actual y confirma que los problemas que dieron origen a la lucha armada no se han resuelto.

Uno de los principales problemas que señala Serna es que el gobierno ha construido las conversaciones de paz sobre la premisa de que no se discute el modelo económico y se ha menguado la posibilidad de la participación de la sociedad en un sentido más amplio, por lo que asegura que el ELN considera que el mismo modelo se implementará con ellos y por eso exigen modificaciones.

Para la integrante del Congreso de los Pueblos, el ELN está pidiendo que se respete la agenda, los acuerdos y los espacios ya acordados, pues las **nuevas condiciones que el gobierno impulsa, hacen que las conversaciones se enreden**; agrega que no se debe perder de vista la posibilidad de discutir las [[transformaciones estructurales que requiere el país](https://www.eln-voces.com/index.php/voces-del-eln/comando-central/editorial/740-por-la-paz-la-resistencia-continua-declaracion-de-la-direccion-nacional-y-el-comando-central-del-ejercito-de-liberacion-nacional)] y el que la sociedad civil participe.

Serna también resalta que se debe insistir en la instalación de la mesa de conversaciones con el ELN y en que se refrenden los acuerdos a los que se llega con las FARC como un paso necesario hacia la construcción de la paz con las reformas estructurales para que las causas del conflicto se cierren y se evite que se abra un nuevo espiral de violencia.

<iframe src="http://co.ivoox.com/es/player_ej_12480199_2_1.html?data=kpehmpWVfZqhhpywj5aVaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5ynca7V097Zx9OPl8bmz8aSlKiPh9DiyNfS1dSPqMafzdTgjdXZqcPg0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
