Title: Iglesias construirán casas de protección en las zonas de concentración
Date: 2016-08-04 10:55
Category: Nacional, Paz
Tags: Casas de protección zonas de concentración, Diálogos de paz en Colombia, DIPAZ, Posconflicto en Colombia
Slug: iglesias-construiran-casas-de-proteccion-en-las-zonas-de-concentracion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/DiPaz_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: DiPaz] 

###### [4 Ago 2016] 

Diversos representantes de iglesias y organizaciones basadas en la fe que se han articulado en el proceso 'Diálogo Intereclesial por la Paz de Colombia' DiPaz, planean la **construcción de dos casas de protección en alguna de las trece zonas de concentración** que han acordado las delegaciones de paz, en el marco de las negociaciones entre la guerrilla de las FARC-EP y el Gobierno colombiano.

Este proceso durante los últimos años ha acompañando a comunidades que en diferentes regiones trabajan en la **construcción de paz desde la acción no violenta y la búsqueda de verdad y justicia**, y desde hace algunos meses viene planteando la construcción de estas casas de protección en las que los pobladores podrán recibir atención psicosocial y resolver sus dudas frente al proceso de paz y el plebiscito.

De acuerdo con Alejandra Albornoz, integrante de DiPaz, esta iniciativa surge en el marco de la veeduría que han hecho del proceso de paz y en particular del cese al fuego, en ese sentido han recorrido municipios del Valle del Cauca, Antioquia y [Chocó](http://bit.ly/2aQR9OQ) para definir [[dónde estarían las dos casas](https://archivo.contagioradio.com/estos-son-los-sitios-de-las-zonas-de-concentracion-de-integrantes-de-las-farc/)], **dialogando con líderes y comunidades para comprender cuáles son sus necesidades, temores y oportunidades** frente al posconflicto.

<iframe src="http://co.ivoox.com/es/player_ej_12441056_2_1.html?data=kpehlpaUeZehhpywj5WaaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs_O0MnWpYy1zcfc1NPTvoampJCxq7Wlno6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
