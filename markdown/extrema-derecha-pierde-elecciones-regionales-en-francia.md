Title: Extrema derecha pierde elecciones regionales en Francia
Date: 2015-12-14 18:56
Category: El mundo
Tags: Elecciones en Francia, francia, Frente Nacional, Marine Le Pen, Nikolas Sarkozy
Slug: extrema-derecha-pierde-elecciones-regionales-en-francia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/ñ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: diario-latercera 

###### [14 Dic 2015]

Contra todo pronóstico, el **partido político Frente Nacional**, encabezado por Marine Le Pen no consiguió obtener la victoria en ninguna región en las elecciones de este domingo 13 de diciembre en el territorio francés, pese a que en primera vuelta de los comicios había tenido considerable éxito.

La tasa de participación de los electores estuvo sobre el 59%, contrastando con el 50% de la primera vuelta. Con estos resultados, el partido derechista de Los Republicanos, del que hace parte el expresidente Nikolas Sarkozy, obtuvo 7 de los 13 escaños; mientras que el oficialismo, junto con otros partidos de izquierda se alzó con 5.

A raíz de los ataques del 13 de noviembre en la ciudad de París, el partido de extrema derecha tomó fuerza y aumentó su popularidad con un discurso en donde se proponían restricciones en materia migratoria y reforzar los esquemas de seguridad.

Las elecciones regionales son la antesala política para las elecciones presidenciales en Francia que se llevarán a cabo a partir del próximo año para nombrar a un nuevo mandatario en 2017. Hay que decir sin embargo, que la precandidata Le Pen del Frente Nacional ha incrementado su popularidad en los sectores más conservadores de la sociedad francesa y apuesta por convertirse en el rostro del ala derechista en el país.
