Title: En Montebonito, Caldas, no quieren proyectos hidroeléctricos
Date: 2017-04-29 06:39
Category: Ambiente, Nacional
Tags: Caldas, consulta poular, Monte Bonito
Slug: habitantes-monte-bonito-caldas-esperan-nieguen-licencia-proyecto-hidroelectrico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Hidroelectricas-Caldas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### [28 Abr 2017] 

Mediante una audiencia pública desarrollada en Montebonito, Caldas quedó claro que la gran mayoría de **la comunidad no quiere que se lleve a cabo proyectos hidroeléctricos**, como los que pretende desarrollar la empresa Latinco.

De acuerdo con Ariel Flores, habitante de la zona y participante de la audiencia pública, “Hubo mucha asistencia y se corroboró que no estamos de acuerdo con la implementación de los proyectos, **solo esperamos que al ver tanta gente con argumentos, haga que le nieguen la licencia ambiental”.**

Y es que a la audiencia pública la comunidad iba preparada. Se armaron de argumentos, de expertos como geólogos, y una serie de pruebas con las que argumentaron que no se puede cambiar la vocación cafetera de ese municipio por implementar un proyecto hidroeléctrico que ya ha secado ** 19 quebradas, dejando sin agua a 90 familias de la zona, y que además tiene en riesgo [otras 100 fuentes de agua de Monte Bonito.](https://archivo.contagioradio.com/100-fuentes-de-agua-estan-en-riesgo-en-monte-bonito-caldas/)**

El paso a seguir es esperar el veredicto de la Defensoría del Pueblo, la Procuraduría y CORPOCALDAS, que asistieron a la audiencia pública y escucharon las razones de la comunidad. Sin embargo, Flores, asegura que “**Si siguen con la idea de implementar el proyecto, tocará irnos a una consulta popular”** como las que están haciendo otras comunidades en diversas partes del país.

“Esto es una lucha continua, pero con tantos argumentos esperamos que las entidades sean conscientes que nos harán un daño muy grande”, expresa el habitante de Monte Bonito, quien subraya que a la audiencia no asistieron los personeros ni los alcaldes de los municipios de Marulanda, ni Manzanera.

Cabe recordar que **gracias a la fuerte oposición de parte de las comunidades el proyecto, que estaba previsto para que iniciara a principios de 2016**, se ha visto retrasado.

<iframe id="audio_18452273" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18452273_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
