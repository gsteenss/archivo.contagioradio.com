Title: Proyecto de Agromar provoca daños ambientales en Bosque Primario en Chocó
Date: 2018-09-21 13:20
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Agromar, Pedeguita y Mancilla, polo, zona de biodiversidad
Slug: proyecto-de-agromar-provoca-danos-ambientales-en-bosque-primario-en-el-choco
Status: published

###### [Foto: Contagio Radio] 

###### [21 Sept 2018]

La Comisión de Justicia y Paz, denunció que **continúan los daños ambientales al bosque primario y fuentes de agua, en la zona de Biodiversidad de la familia Polo**, ubicados en el territorio colectivo de Pedeguita y Mancilla, Chocó, producto del proyecto platanero Agromar, que ya habría afectado 30 ha.

La organización manifestó que en días previos, durante una misión de verificación, tuvo conocimiento sobre un ingreso que habrían realizado trabajadores de Baldoino Mosquera, representante legal del Consejo Comunitario, que en 2016 inconsultamente firmó un convenio con Bernardo José Guerra Genes, representante de Agromar.

Sumado a estos hechos, la construcción de un cable para conectar la platanera con la quebrada Bijao, esta **provocando inundaciones en los terrenos comunitarios y destruyo la Zona de Biodiversidad La Esperanza**, perteneciente a la familia Polo.

### **Amenazas a la familia Polo** 

De acuerdo con la Comisión de Justicia y Paz, Eliodoro Polo, líder social de restitución de tierras, y quien está a cargo de la zona de Biodiversidad, ha sido amenazado en diversas ocasiones por trabajadores de Baldoino Mosquera. (Le puede interesar:["Empresa Agromar provoca desastre ambiental en el Chocó"](https://archivo.contagioradio.com/agromar-en-pedeguita-y-mancilla-en-choco/))

Asimismo, el año pasado, Polo denunció que, la ampliación del agronegocio de plátano de la empresa Agromar deforestó aproximadamente 30 hectáreas, de manera arbitraria e ilegal sin ninguna reacción institucional para proteger los derechos de los pobladores.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
