Title: En Fotos: Así fue la movilización en la Toma de Bogotá
Date: 2017-06-06 15:02
Category: Movilización, Nacional
Tags: Cumbre Agraria, fecode, INPEC, Paro Judicial, Toma de Bogotá
Slug: fotos-toma-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/grantomabogotanuevacolnoticias-e1496774307560.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: NC Noticias] 

###### [06 Jun 2017]

Miles de profesores y profesoras, arroceros, integrantes de las centrales sindicales, integrantes de las dignidades agropecuarias, trabajadores y trabajadoras del sector judicial, padres y madres de familia, estudiantes y personas en general, se movilizaron hoy por las calles de la capital en lo que denominaron la Gran Toma de Bogotá.

La gran mayoría de quienes se movilizaron denuncian que el Estado Colombiano ha incumplido los acuerdos a varios de ellos y no ha garantizado el acceso a los derechos que consagra la Constitución Colombiana.

Contagio Radio realizó una selección de fotografías en las que se retrata la alegría y la firmeza del movimiento social colombiano. [Lea también: FECODE espera que el gobierno invierta en educación los recursos necesarios](https://archivo.contagioradio.com/fecode-acepta-a-procurador-general-como-facilitador-en-las-negociaciones/)

### **Imágenes de** **Toma de Bogotá**. 

\
