Title: La tristeza nos invade el corazón por el caso de Yuliana y de muchas mujeres en Colombia
Date: 2016-12-06 15:59
Category: Mujer, Otra Mirada
Tags: asesinato, Brigard&amp;Urrutia, feminicidio, Rafael Uribe Noguera
Slug: la-tristeza-nos-invade-por-caso-de-yuliana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/4ae58a0845a6694836872611c1511e21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [6 Dic. 2016] 

*“Yo tenía mi vida nueva, mis sueños intactos…mi cuerpo inmaculado, mis manitas quietas. Mis ojos no habían visto el horror. Mis pies no habían corrido con miedo. Mi sonrisa iluminaba al mundo. Mi garganta estaba sin desgarrar. Hasta que llegó el cobarde, el abusador y asesino de niños (niñas) y me cortó las alas para siempre. Es de mejor apellido que el mío, la prensa y los jueces lo taparán, dirán de él…que estaba loco y de mí… que qué hacía en la calle. Lo llevarán a una cárcel de lujo y a mí a un cementerio de pobres”.*

El anterior fragmento fue compartido a través de las redes sociales y es solo una de las cientos de formas en las que **las personas han comenzado a pronunciarse y a demostrar su indignación frente al reciente asesinato de Yuliana Samboní de 7 años, quien fuera brutalmente asesinada.**

Según el dictamen de medicina legal, **Yuliana murió por “asfixia mecánica, por sofocación y estrangulamiento y en el cuerpo de la menor, luego de 10 horas de exámenes lograron identificar que había evidencia que demuestran que fue objeto de abuso sexual.**

Las diversas organizaciones de mujeres ya han comenzado a trabajar en este nuevo caso y han manifestado su repudio ante el fatídico desenlace que tuvo que vivir Yuliana.

**Sandra Mazo, coordinadora de católicas por el Derecho a Decidir aseguró que lo que sucedió con Yuliana es aberrante, indignante, lamentable** “No sé qué palabras decir frente a lo que le pasó a la niña pues no cabe en la cabeza entender el dolor de lo que esto puede significar”.

**Según el Fiscal General de la Nación, Néstor Humberto Martínez “frente a este horrendo crimen podemos decirle al país, a la comunidad y a su familia, que habrá justicia de manera inmediata”.** Y es esa misma la súplica que al unísono han realizado diversas plataformas. Le puede interesar: [Actualmente no hay ni una condena por feminicidio en Colombia ](https://archivo.contagioradio.com/actualmente-no-hay-ni-una-condena-por-feminicidio-en-colombia/)

“Lo mínimo que esperamos es que se haga justicia con este caso y con todos los casos de feminicidio y violencias contra las mujeres y niñas. **Lo que esperamos es que se investigue a fondo lo que pasó (…) ojalá que independientemente de que este asesino tengo mucho dinero se haga justicia” agregó Sandra.**

**¿Por qué el caso de Yuliana es un feminicidio?**

**El feminicidio es el asesinato de una mujer por el hecho de ser mujer**, de modo que cada vez que una mujer es la víctima de un crimen en el cual además se comete por su condición femenina como causa principal, este fenómeno es conocido como feminicidio.

En el caso de Yuliana, según la coordinadora de Católicas por el Derecho a Decidir “estamos ante un asesinato contra una mujer, contra una niña que es abusada por ser niña. Tiene todos los elementos para que sea un feminicidio por la manera como ocurre el crimen, cómo se prepara"

**Si este caso se cataloga como un feminicidio las autoridades podrían condenar al agresor a una pena de hasta 60 años a los cuales se les sumaría los delitos restantes que puedan surgir del hecho, tales como la tortura.** Le puede interesar: [El mundo grita, ni una menos viva nos queremos](https://archivo.contagioradio.com/el-mundo-grita-ni-una-menos-vivas-nos-queremos/)

“Lo que buscamos es que no haya rebaja de penas en estos casos, si eso se cumple es un ejercicio de hacer justicia frente a unas normas y frente a unos delitos que se están cometiendo y que son deplorables” agregó Mazo.

**Educación y prevención, las primeras herramientas**

Según varias organizaciones se hace necesario continuar con los esfuerzos y hacer ejercicios de pedagogía y de educación sexual en todos los espacios, en la casa, en el colegio.

**Cifras oficiales hablan de que cada día en Colombia 22 niñas son víctimas de violencia, de las cuales 11 mueren cada mes. Así mismo, cada día son violadas por lo menos 21 niñas entre 10 y 14 años y cada mes existe un nuevo caso de contagio de VIH/Sida en una niña de 10 a 14 años.**

Por lo que Sandra Mazo realizó una invitación a los y las colombianas “las familias debemos tener más conciencia del tema de la sexualidad y lo que implica. A las niñas no hay que ocultarles información sobre educación sexual y enseñarles que el cuerpo hay que cuidarlo, que no se pueden dejar tocar de nadie. Y debemos estar pendientes por que la mayoría de los casos suceden en las casas” manifestó Sandra Mazo.

**¿Quién es el presunto asesino de Yuliana?**

**El cuerpo sin vida de esta pequeña fue encontrado en el apartamento de Rafael Uribe Noguera,** en Chapinero Alto una localidad de Bogotá. Rafael Uribe es un hombre de 38 años y soltero. Arquitecto egresado del colegio Gimnasio Moderno y titulado de la Universidad Javeriana.

Además es **integrante de una “prestante” familia. Sus padres, María Isabel Noguera de Urrutia y Rafael Uribe Rivera. Es hermano del abogado Francisco José Uribe Noguera, socio desde el 2012 de la firma Brigard & Urrutia.**

**Firma que según lo asegura un informe de investigación titulado “*Divide y comprarás”* una nueva forma de comprar tierras baldías en Colombia, realizado por Oxfam, habría jugado un papel importante en la estrategia para asesorar a empresas que se quedaron irregularmente con terrenos baldíos.**

Este caso obligó a la renuncia de Luis Carlos Urrutia después de que varios congresistas del Polo Democrático denunciaran que la firma de abogados en la que él era el socio principal durante los años en que se produjeron las compras, había asesorado a estas empresas para apropiarse de tierras con antecedente de baldíos en la Altillanura.[\[i\]](#_edn1){#_ednref1}

##### [\[i\]](#_ednref1)

   
<iframe id="audio_14671380" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14671380_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).

<div class="ssba ssba-wrap">

</div>
