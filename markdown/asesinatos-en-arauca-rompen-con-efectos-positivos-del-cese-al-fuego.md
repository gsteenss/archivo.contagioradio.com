Title: Asesinatos en Arauca rompen efectos positivos del cese al fuego
Date: 2016-03-11 17:46
Category: DDHH, Nacional
Tags: Arauquita, asesinato Milton Yesid Escobar, Unión Patriótica
Slug: asesinatos-en-arauca-rompen-con-efectos-positivos-del-cese-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/union-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Macondo ] 

<iframe src="http://co.ivoox.com/es/player_ek_10792699_2_1.html?data=kpWkm5eafZqhhpywj5WbaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncaLnxtjW0MbYs9SfxtOYo9fFucTVjNfcz9XJsozZx8rQ1tTXb9Hj1M7hy9vTt4zYxtGYxcrXqYyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Martín Sandoval] 

###### [11 Mar 2016 ]

[Este jueves a las 6:30 de la tarde en Arauquita fue asesinado Miltón Yesid Escobar, militante de la Unión Patriótica y miembro de la Unidad Nacional de Protección a través de la cual brindaba servicios de seguridad al dirigente político y defensor de los derechos humanos Martín Sandoval, quien asegura que este crimen se da en medio de la "v]iolencia que se ensaña contra el departamento de Arauca" y que ha provocado en los últimos días el asesinato de veinte personas, "rompiendo con la gran tranquilidad que se venía presentando en la región, producto de los efectos positivos de los Diálogos" y del cese al fuego decretado.

El asesinato se presentó a escasos metros de la sede política de la UP en Arauquita y hasta el momento no se conocen los responsables ni el posible móvil del crimen, por lo que se exige se adelanten las investigaciones pertinentes para que el crimen no quede en la impunidad, teniendo en cuenta que éste representa "la falta de garantías para el ejercicio político aún cuando se habla de finalización del conflicto en una región como Arauca", según refiere Sandoval, quien agrega que este hecho "es un campanazo de alerta (...) de aquellos que no gustan ni quieren la paz ni la reconciliación en regiones en las que se ha vivido el conflicto de manera fuerte y degradada".

De acuerdo con el dirigente político este jueves en la tarde también fue asesinado otro campesino, en zona rural del municipio de Arauquita. Pese a esta aguda situación en el departamento, Sandoval asegura que no se puede perder la esperanza ni desfallecer en el intento de terminar el conflicto para empezar una etapa de reconciliación y paz en homenaje a las víctimas.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
