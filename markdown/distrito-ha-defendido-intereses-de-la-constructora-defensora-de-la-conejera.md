Title: "Distrito  ha defendido intereses de la constructora" defensora de La Conejera
Date: 2015-04-30 12:50
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, gloria florez, Humedales, La Conejera, Martha Lucía Zamora, Protesta social, Roberto Sáenz, Suba
Slug: distrito-ha-defendido-intereses-de-la-constructora-defensora-de-la-conejera
Status: published

##### Foto: bluradio.com 

<iframe src="http://www.ivoox.com/player_ek_4430468_2_1.html?data=lZmgkpmafI6ZmKiak5aJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bX08rhw9fNpYy4ytjh1M7Ypc2fycbQx5DKucbm1crgjcnJp83V08bQy9TSqdSfxNTb1tfFb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gina Díaz, defensora del humedal La Conejera] 

**“Esperemos,  no queremos que haya un muerto”**, dice Gina Díaz, **citando a la Secretaría General del Distrito, Martha Lucía Zamora**, quien según Gina, esta semana hizo serias declaraciones que ponen en peligro a las personas que se encuentran en el campamento en defensa del Humedal La Conejera, para que la constructora Praga y la Policía no logre ingresar material de construcción para llevar a cabo el proyecto Reserva del Fontanar.

El proceso de defensa del Humedal La Conejera continúa, en estos momentos la comunidad espera una aclaración sobre la medida cautelar que hay, debido a que **existe un vacío jurídico** ya que no se dejó claro si  se puede o no ingresar material al lugar,  lo que ha generado que en dos ocasiones funcionarios de la constructora acompañados por la policía intenten entrar materiales de construcción.

En ambas momentos, los ambientalistas que llevan meses acampando frente a la constructora, han sido víctimas de vulneraciones y ataques por parte del ESMAD, pese a que se había acordado con Gloria Flórez, Secretaria de Gobierno, que la administración distrital se comprometía a que no se iba a ingresar ningún material de construcción.

De acuerdo a la defensora del humedal, el domingo Martha Zamora dejó claro que **la responsabilidad de lo que pase con los ambientalistas depende únicamente de ellos** mismos, señala Gina. Además, deslegitimó la protesta social y no asumió una responsabilidad frente a quién dio la orden al ESMAD de atacar.

Por su parte, el concejal de Bogotá, Roberto Sáenz, hizo un llamado a la secretaria para que no se criminalice la protesta social, pues se ha dicho que Gina Díaz es una de las personas que viene promoviendo los enfrentamientos, “**cuando hemos estado como garantes para que no se llegue a la violencia**”, asegura la ambientalista.

Así mismo, Gina denuncia que **el distrito solo ha defendido los intereses  de la constructora.** Además preocupa que la secretaria hable de “muertos” lo que “pone en riesgo nuestra integridad y seguridad”, expresa.

**El próximo domingo 3 de mayo, se realizará una marcha tipo carnaval** que llegará hasta el Portal de suba. Los puntos de encuentro son en la biblioteca Virgilio Barco a las nueve de la mañana y en el Humedal Tibabuyes  a las 10 am.
