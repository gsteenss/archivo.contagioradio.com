Title: Se registran dos masacres en menos de 12 horas en Cauca y Antioquia
Date: 2020-11-22 10:57
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Antioquia, Betania, colombia, masacre, Suroeste Antioqueño
Slug: se-registran-dos-masacres-en-menos-de-12-horas-en-cauca-y-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/WhatsApp-Video-2020-11-22-at-6.39.40-AM.mp4" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este fin de semana en **los departamento de Antioquia y Cauca se registraron dos masacres perpetradas por grupos armados que operan en estas regiones de Colombia, en los hechos fueron asesinadas 12 personas**. Durante este años, 303 personas han perdido su vida en 76 masacres.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Justiciaypazcol/status/1330514950666801152","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Justiciaypazcol/status/1330514950666801152

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### 5 personas son masacradas en Argelia, Cauca

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sobre las **10:00 de la noche de este sábado 22 de noviembre en el corregimiento de El Mango en Argelia, Cauca, hombres armados que se movilizaban en una moto y una camioneta** dispararon en contra de un grupo de personas que se encontraban en cercanías de una discoteca del corregimiento.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1330491689002278914","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1330491689002278914

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La acción armada en el sur occidente del departamento del Cauca, **dejó 4 personas asesinadas y causó heridas graves a 10 más**, víctimas que fueron trasladadas a centros de atención médica en Argelia y Popayán. [(Crece a tres la cifra de personas asesinadas en ataque armado en Argelia, Cauca](https://archivo.contagioradio.com/dos-personas-asesinadas-y-tres-heridas-tras-ataque-armado-en-argelia-cauca/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Tati_Ramirez/status/1330503777665900554","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Tati\_Ramirez/status/1330503777665900554

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Dentro de las personas masacradas **se identificó al líder social  Libio Chilito**, quién en los últimos días recolectaba recursos para entregar regalos a los niños de la zona. Otras de las víctimas fueron identificadas como **Faber Joaquín Hoyos, Arlex Daniel Salamanca, Dannover Santiago López, Harold Ruiz Salazar**.

<!-- /wp:paragraph -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/WhatsApp-Video-2020-11-22-at-6.39.40-AM.mp4">
</video>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Por medio de un video registrado por habitantes del corregimiento, se ve el momento en que hombres atraviesan huyendo una cancha deportiva, uno de ellos en muletas y otro lleva en sus hombros el cuerpo de un hombre.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 7 recolectores de café masacrados en Betania, Antioquia

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1330522885476245504","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1330522885476245504

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La segunda de estas masacres se registró en el departamento de Antioquia, municipio de Betania, **en horas de la madrugada de este domingo 22 de noviembre y fueron asesinados siete recolectores de café.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho **fue perpetrado en una finca de la vereda La Julia, ubicada a 5 kilómetros del casco urbano que es Betania, junto a las 7 víctimas fatales también fueron heridas 3 personas** quienes se encuentran recibiendo atención médica en un hospital en la ciudad de Medellín.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según **Leonardo González, coordinador del observatorio de Derechos Humanos del Instituto de Estudios para el Desarrollo y la Paz -Indepaz**- en Betania hay tercerización de las AGC por medio de una banda conocida como "Sangre Negra".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello las comunidades han denunciado la falta de control y ausencia por parte del Estado e indico, ***"No todo es narcotráfico, hay diversos factores e intereses que facilita la presencia de grupos armados"***, resaltando que en el año 2019 la empresa australiana Metminco llegó a la zona para extraer oro en este corregimiento.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 303 personas han sido asesinadas en 76 masacres durante el 2020

<!-- /wp:heading -->

<!-- wp:paragraph -->

Indepaz también da cuenta que durante este año se han cometido 76 masacres en el país, una s**ecuencia de violencia que es encabezada por el departamento de Antioquia con 18 masacres**, seguida de Cauca con 12 masacres, Nariño con 9 masacres y Norte de Santander con 6 masacres

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1330520703767113732","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1330520703767113732

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

(Le puede interesar: [INFORME DE MASACRES EN COLOMBIA DURANTE EL 2020](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

  

<!-- /wp:paragraph -->
