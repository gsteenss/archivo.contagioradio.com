Title: ELN responde carta enviada por los ambientalistas colombianos a esta guerrilla
Date: 2016-10-12 17:06
Category: Ambiente, Nacional
Tags: ambientalistas, Ambiente, Animalistas, Conversacioines de paz en Colombia, conversaciones de paz con el ELN, conversaciones de paz entre ELN y gobierno colombiano, Gobierno Nacional, paz
Slug: eln-responde-carta-enviada-por-los-ambientalistas-colombianos-a-esta-guerrilla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-e1476309678834.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: RevistaSemana 

###### 12 Oct 2016 

Luego de la **carta enviada por un grupo de ambientalistas y animalistas** en el que manifiestan su disposición para ser garantes entre el Gobierno de Colombia y la Guerrilla del **ELN**, ésta delegación agradeció y valoró que quieran ser parte del proceso, velando para que sea transparente e incluyente.

En la misiva aseguran que han prestado atención a cada una de  las solicitudes hechas en la carta y que así mismo comparten muchos de los puntos expuestos en la comunicación.

A su vez, manifiestan que el momento histórico que vive Colombia en el que las movilizaciones por la paz no se han hecho esperar, hace que se deba propiciar la unidad y confluencia de quienes buscan transformaciones en América Latina y el Caribe.

Pablo Beltrán, quien firma esta carta como parte de la delegación de diálogo, asegura además que el ELN **admira la voluntad de los ambientalistas que desean construir de la mano de la sociedad civil** “una agenda de transición y paz social, con la naturaleza y con la vida”.

Por último, asegura que las propuestas por parte del **movimiento ambientalista y animalista** serán un significativo e importante aporte para la paz y se despide asegurando que están “con la expectativa de pronto poder compartir con Ustedes (los ambientalistas y animalitas), las tareas que exigen los propósitos enunciados en la carta”.

Lea también[: Ambientalistas se ofrecen como garantes entre gobierno y ELN](https://archivo.contagioradio.com/ambientalistas-se-ofrecen-como-garantes-entre-gobierno-y-farc/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
