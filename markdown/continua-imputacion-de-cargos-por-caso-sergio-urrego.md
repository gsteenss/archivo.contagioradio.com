Title: Continúa imputación de cargos por caso Sergio Urrego
Date: 2015-05-12 17:16
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Colombia Diversa, Comunidad LGBTI, Derechos Humanos, Discriminación, Fiscalía General de la Nación, Gimnasio Castillo Campestre, Homosexualidad, Ibonn Andrea Cheque Acosta, Imputación, Mauricio Albarracín, Sergio Urrego
Slug: continua-imputacion-de-cargos-por-caso-sergio-urrego
Status: published

###### Foto: Contagio Radio 

La primera etapa de la investigación de la Fiscalía sobre el caso **Sergio Urrego** reúne 900 folios que incluyen testimonios de otros alumnos en que se evidencia la presión que se ejercía sobre el estudiante por su orientación sexual y la relación sentimental que mantenía con uno de sus compañeros, además de otros elementos probatorios. La Fiscalía imputará los cargos de actos de discriminación, falsa denuncia y ocultamiento de pruebas agravado.

La audiencia se realizará 15 de mayo de 2015 se llevará a cabo la cuarta audiencia de imputación de cargos a la ex rectora del colegio **Gimnasio Castillo Campestre, Amanda Azucena Castillo Cortes**, a la psicóloga **Ibonn Andrea Cheque Acosta** y a la veedora de la institución, **Rosalía Ramírez Rodríguez**.

**Sergio Urrego Reyes de 16** años fue víctima de rechazo por parte de las directivas del colegio Gimnasio Castillo Campestre, que lo privaron de su derecho a la educación condicionándolo a cumplir una serie de requisitos para tener acceso a la institución, como acudir a ayuda psicológica por su orientación sexual y demostrar por medio de una certificación que lo estaba cumpliendo, esto además de impedir el libre desarrollo de la personalidad al exigirle que **no tuviera muestras de afecto con su compañero sentimental e intentar cambiar su orientación sexual.**

La lucha del estudiante y su familia para que sus derechos fueran respetados se prolongó durante varios meses a través de tutelas y quejas a la institución, pero, no se logró llegar a un acuerdo que favoreciera a las dos partes, hasta que esta serie de acciones llevaron a Sergio a **quitarse la vida el pasado 4 de agosto de 2014.**

La imputación de cargos hacia las directivas de dicha institución significaría un avance para la comunidad LGBTI pues, en Colombia se evidencia la vulneración a sus derechos, partiendo de la posición de la Procuraduría que apeló la sentencia del caso Sergio Urrego para revocar la decisión del Tribunal Administrativo, considerando que en este caso no hubo discriminación y que el plantel hizo lo que pudo cuando se enfrentó al caso; al respecto Mauricio Albarracín, director de Colombia Diversa asegura que “la Procuraduría está persiguiendo sistemáticamente a la comunidad LGBTI”.
