Title: La receta para la paz con sabor a arepa
Date: 2016-06-02 16:26
Category: Nacional, Paz
Tags: ArePaz, I Festival ArePaz, Quimbaya, Quindío
Slug: la-receta-para-la-paz-con-sabor-a-arepa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/ArePaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía Quimbaya ] 

###### [2 Junio 2016 ]

"¿Usted qué va a hacer cuando se acabe la guerra?" preguntaron pedagogos y sociologos en varias regiones del país, a lo que una señora respondió "arepas", una respuesta que bien supieron comprender este grupo de profesionales para consolidar la iniciativa 'ArePaz', con el fin de **asociar el símbolo gastronómico más representativo de Colombia con el sueño y el anhelo de la paz**.

"La arepa viene del maíz el maíz de la tierra y la tierra es el epicentro del conflicto armado, por tanto, para solucionarlo se requiere solucionar el problema de la tenencia de la tierra y el desarrollo agrícola" asegura el periodista Jorge Rojas, quien hace parte de esta iniciativa a través de la cual se plantea que **mientras hombres y mujeres vayan haciendo arepas, dialoguen sobre cómo hacer la paz en los territorios**, pues así como hay 71 formas distintas de hacer arepas en Colombia, es posible que no haya una sola paz.

La primera cita es este sábado en Quimbaya, Quindío, el municipio colombiano en el que más arepas por metro cuadrado se venden, allí desde las 3 de la tarde se llevará a cabo el I Festival Arepaz, en el que **hombres y mujeres de todas las edades podrán hacer arepas, mientras hablan sobre el punto uno de los acuerdos de La Habana**. En la Plaza de Bolívar, mientras muelen y amasan, podrán dialogar de soberanía alimentaria, desarrollo agrario o baldíos.

Quienes no puedan asistir a este I Festival podrán participar en redes sociales, contando cómo en sus regiones se hacen las arepas, qué van a hacer cuando se acabe la guerra y las múltiples formas en las que desde sus territorios aportan a la paz.

<iframe src="http://co.ivoox.com/es/player_ej_11760881_2_1.html?data=kpakmJWcfJKhhpywj5WcaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncavj08zSjbfTrsLnjJKYstfTq9PZ1M7g1sbXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
