Title: Las críticas tras la elección de Margarita Cabello en Procuraduría
Date: 2020-09-01 11:52
Author: PracticasCR
Category: Actualidad, Nacional
Tags: Margarita Cabello, Nueva Procuradora, Procuraduría General de la Nación
Slug: las-criticas-tras-la-eleccion-de-margarita-cabello-en-procuraduria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Margarita-Cabello.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Finalmente, fue elegida por el Senado de la República, la exministra de Justicia de Iván Duque, Margarita Cabello; como nueva Procuradora General de la Nación, con una votación de 83 votos, mientras que sus compañeros ternados Juan Carlos Cortés y Wilson Ruíz obtuvieron 16 y 0 votos respectivamente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La nueva Procuradora fue apoyada por la Casa Char, los partidos políticos tradicionales como el Centro Democrático, el Partido Conservador, Cambio Radical y la U; y también por el Gobierno quien hasta hace unos días la tenía como cabeza del Ministerio de Justicia. (Lea también: [Demandan pérdida de investidura en contra de Arturo Char](https://archivo.contagioradio.com/demandan-perdida-de-investidura-en-contra-de-arturo-char/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta elección ha suscitado especial preocupación por parte de Organización Sociales y sectores políticos de la oposición; quienes han señalado que **con el nombramiento de Margarita Cabello, el Gobierno de Iván Duque ha cooptado casi en su totalidad las ramas del poder público y los entes de control.** (Lea también: [Procuraduría puede cambiar la balanza política y frenar la posibilidad de cambio](https://archivo.contagioradio.com/procuraduria-puede-cambiar-la-balanza-politica-y-frenar-la-posibilidad-de-cambio/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/GustavoBolivar/status/1298996655862104070","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/GustavoBolivar/status/1298996655862104070

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Adicionalmente, **a Margarita Cabello le atribuyen una radicalidad en sus posturas, similar a la que manejaba el exprocurador Alejandro Ordoñez, quien en su momento utilizó la Procuraduría como una herramienta de persecución política e ideológica, según afirman varios sectores.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/petrogustavo/status/1299061891822759937","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/petrogustavo/status/1299061891822759937

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Daniela Gómez, Coordinadora de Democracia y Gobernabilidad en[ Paz y Reconciliación](https://twitter.com/parescolombia), explicó que** la Procuraduría cuenta con más de 4.100 cargos**, por lo que es un importante botín burocrático para los Partidos Políticos,**sin contar que tiene un poder sancionador que la convierte en una institución relevante en términos de equilibrio de poder**. (Lea también: [Otra Mirada: Procuraduría general ¿otro fortín político?](https://archivo.contagioradio.com/otra-mirada-procuraduria-general-otro-fortin-politico/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, varios de los senadores que participaron en la votación en la que se eligió a Margarita Cabello, están siendo actualmente investigados por la Procuraduría General. En la lista de dichos congresistas, destacan **Armando Benedetti, Eduardo Pulgar, Julián Bedoya, John Besaile y Richard Aguilar** **cuyas bancadas dieron su apoyo a Cabello en la votación y sobre quienes recaen procesos disciplinarios que actualmente están en curso** sobre los que entrará a conocer la nueva Procuradora. Es decir, estos congresistas eligieron la persona que los va a investigar.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Margarita Cabello y la extradición de Salvatore Mancuso

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como Ministra de Justicia, Margarita Cabello, terminó su gestión sin lograr la extradición del exjefe paramilitar Salvatore Mancuso, para que aporte con su verdad a los procesos y contribuya con la reparación de las víctimas y para que cumpla sus deudas pendientes con la justicia colombiana. (Le puede interesar: [Víctimas de Salvatore Mancuso piden su extradición a la Fiscalía](https://archivo.contagioradio.com/victimas-de-salvatore-mancuso-piden-su-extradicion-a-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PizarroMariaJo/status/1299346595557920771","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PizarroMariaJo/status/1299346595557920771

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Este proceso ha estado minado de todo tipo de errores,** Margarita Cabello es señalada como una de las grandes responsables del fracaso que ha significado la extradición del exlíder del Autodefensas Unidas de Colombia -AUC-; de hecho el expresidente **Andrés Pastrana, que desde tiempos de campaña se ha mostrado afín con el Gobierno de Duque, criticó su labor en este trámite y hasta pidió al Presidente que retirará su candidatura a la Procuraduría.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AndresPastrana_/status/1298287497273843712","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AndresPastrana\_/status/1298287497273843712

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
