Title: Gobierno se niega a reconocer conflicto armado en el Catatumbo: organizaciones sociales
Date: 2018-04-18 11:51
Category: DDHH, Paz
Tags: Catatumbo, conflicto armado, ELN, EPL, Gobierno
Slug: gobierno-se-niega-a-reconocer-conflicto-armado-en-el-catatumbo-organizaciones-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/El-estimulo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Estimulo] 

###### [18 Abr 2018]

Organizaciones sociales y defensoras de derechos humanos del Norte de Catatumbo, específicamente del Catatumbo, denunciaron que frente a la crisis humanitaria que atraviesa este departamento, producto de los enfrentamientos y el paro armado entre el EPL y el ELN, **han existido un conjunto de violaciones a los derechos humanos cometidos tanto por las estructuras armadas como por la Fuerza Pública**, razón por la cual instaron a diferentes instuciones y autoridades del Estado para generar soluciones urgentes frente a esta situación.

De acuerdo con las organizaciones, las confrontaciones entre estas guerrillas llevan más de un mes y se han agudizado en los últimos días. Hecho que ha provocado que la población civil "sufra diferentes hechos victimizantes". A su vez informaron que las acciones emprendidas por el gobierno han sido pocas y expresaron que su "sensación" es que el Estado estaría desconociendo la realidad y que **"poco le importaría" la vida de campesinos e indígenas del Catatumbo**.

En esa medida, se creó una comisión de diferentes organizaciones que se reunieron con instituciones como la Defensoría del Pueblo, los senadores del Polo Democrático Iván Cepeda y Alberto Castilla, la Embajada de Alemania, la Unidad Nacional de Protección, la Procuraduría, entre otras, para plantear la urgencia de acciones inmediatas que garanticen el bienestar de las personas en el Catatumbo. (Le puede interesar:["Catatumbo: El reto para el Estado y la Mesa en Quito"](https://archivo.contagioradio.com/catatumbo-el-reto-para-el-estado-y-la-mesa-en-quito/))

De igual forma le exigieron al ELN y al EPL que respeten los derechos humanos y el Derecho Internacional Humanitario. Mientras que a los gobiernos departamentales y locales les pidieron respuestar oportunas y atención a las víctimas. De igual forma, **le señalaron al Gobierno Nacional que de manera urgente el conflicto armado y la crisis humanitaria** que afronta el Catatumbo y que tome medidas prontas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
