Title: Progresistas rechaza señalamientos por parte de gobierno de Bogotá
Date: 2016-02-15 15:37
Category: Nacional, Política
Tags: Bogotá, Bogotá Humana, Gustavo Petro, Jorge Enrique Rojas
Slug: ministerio-de-hacienda-debio-iniciar-proceso-de-licitacion-del-metro-a-finales-de-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/jorge_rojas_rodriguez-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: integracionsocial.gov 

<iframe src="http://www.ivoox.com/player_ek_10445052_2_1.html?data=kpWhlpqUeZOhhpywj5aZaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5ynca7dz87g1srWrdCfxcqYqsbHrcbixcaYxsrGrYa3lIqvlZDNssrXysbfjdXWs8TZ1NSYxsqPsMrXytnOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jorge Enrique Rojas] 

###### [15 Feb 2016] 

Jorge Enrique Rojas afirma que acusaciones de que líderes del Movimiento Progresistas estarían detrás de las [protestas en Transmilenio](https://archivo.contagioradio.com/protestas-en-transmilenio-no-fueron-agitadores-profesionales-fue-la-ciudadania-inconforme/) de la semana pasada, son "infamias infundadas" que no se pueden demostrar judicialmente, que no hay ni un solo ex funcionario que haya sido investigado, detenido o judicializado por las protestas y agrega que los gestores de convivencia son funcionarios de la administración actual y que es este gobierno el que debe responder por lo que hacen sus funcionarios.

Además,  Rojas asegura que  las acusaciones realizadas por algunos funcionarios de la alcaldía actual y replicadas en medios de información como la revista Semana, es una cortina de humo para impedir que se haga un debate profundo sobre el sistema de transporte de la ciudad de Bogotá, capital colombiana, y mantener los buses como alternativa para los próximo 100 años.

En lo referente al artículo de la revista Semana afirma que cometieron todos los errores atribuibles al ejercicio periodístico y todas las faltas atribuibles a la ética periodística “es un artículo tendencioso”.

Frente a la discusión del Metro, Rojas plantea que estuvo presente en la reunión en la que el gobierno nacional se comprometió a hacer la estructuración financiera a través del Ministerio de Hacienda para hacer la licitación de la primera línea del metro y no lo hizo, el gobierno “se casó” con un sistema de buses como el actual. “[yo estuve en la reunión en la que el presidente Santos se comprometió”](https://archivo.contagioradio.com/transmilenio-desde-el-principio-se-baso-en-un-acuerdo-desventajoso/) pero **después de las elecciones se suspendió el proceso licitatorio.**

Además el Banco Mundial avaló los estudios que se realizaron para la primera línea y señala Rojas “por mezquindad política” se impidió que un gobierno de izquierda se alzara con el inicio de la construcción del metro.

Frente a los actos de la semana pasada Rojas afirmó el rechazo a todos los actos de vandalismo, para él **los bloqueos no funcionan y tampoco resuelven el problema que se tiene, **por ello invitó a los foros ciudadanos a las asociaciones de usuarios para que se abran las discusiones.
