Title: Carabinero golpeó a su caballo
Date: 2014-10-31 03:52
Author: CtgAdm
Category: Animales
Tags: Animales, Maltrato animal
Slug: carabinero-golpeo-a-su-caballo
Status: published

#### Foto: Blu Radio 

Estudiantes y profesores de la Universidad Politécnico Grancolombiano en Bogotá, denunciaron que fueron testigos de cómo un carabinero maltrataba a su caballo.

Quienes vieron el hecho se mostraron indignados manifestando que pese a los gritos que le propinaban al maltratador pidiéndole que no le pegara, este simplemente se reía y seguía hiriendo al animal que estaba amarrado. La policía ambiental nunca llegó y el hombre tampoco quiso dar su nombre ni cargo.

Cabe resaltar que la periodista Juanita Kremer, fue quien dio la noticia y dijo que ella misma fue testigo del caso de maltrato animal.
