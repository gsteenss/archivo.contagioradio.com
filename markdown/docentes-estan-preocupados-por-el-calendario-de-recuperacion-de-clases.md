Title: Docentes podrían irse a paro si no se resuelve el calendario de recuperación de clases
Date: 2017-06-30 14:05
Category: Educación, Nacional
Tags: calendario escolar, Docentes, educacion, Paro de maestros
Slug: docentes-estan-preocupados-por-el-calendario-de-recuperacion-de-clases
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/paro-de-docentes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 Jun 2017] 

Maestros y maestras del sector público del país manifestaron su preocupación por la posición del Gobierno, de no aceptar las propuestas de formulación de un de calendario de recuperación para las clases que se perdieron por los 25 días que duró el paro. Ante esto, **podrían irse a paro nuevamente**.

Los docentes le habían planteado al Gobierno que se repusieran las clases durante las 2 semanas de desarrollo institucional que se realiza en noviembre, la semana de receso que se realiza en octubre, **durante 5 sábados y los 5 días festivos que tiene este el semestre**.

Sin embargo, según William Agudelo, presidente de la Asociación Distrital de Educadores “sabemos que no nos han aceptado el calendario porque dicen que el comercio se afecta durante la semana de octubre, los sábados y los festivos”. (Le puede interesar: ["Maestros y maestras ¿un paro de tradición?"](https://archivo.contagioradio.com/maestros-y-maestras-un-paro-de-tradicion/))

De igual forma estableció que “el acuerdo es claro en que íbamos a reponer el tiempo y a hacer un cronograma, pero el Viceministro de Educación Pablo Jaramillo, en retaliación por el paro, propuso que las clases se hicieran hasta el 26 de diciembre más los primeros días de enero”. Agudelo ha manifestado que **“el gobierno ha puesto muchas interferencias para cumplir lo que se acordó el 16 de junio”.**

Los y las maestras han manifestado que ellos van a reponer las clases pero que el **calendario tiene que ser discutido por los entes territoriales y los docentes mismos**. “Tenemos una preocupación frente a los tiempos de la reposición porque extender el calendario más allá del 15 de diciembre afecta a los estudiantes que se van a graduar y que necesitan inscribirse en la universidad”. (Le puede interesar: ["Hay propuestas pero el gobierno no tiene voluntad para negociar: Fecode"](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/))

**Docentes podrían volver a paro**

Agudelo manifestó que “si el Ministerio de Educación no acepta ningún calendario, **nos toca volver a salir a las calles para reclamar que se cumpla lo pactado**”. Para los docentes, quienes ya propusieron un calendario, no es viable  “que trabajemos el 24 y el 31 de diciembre, sabiendo que hay otras alternativas”.

<iframe id="audio_19565477" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19565477_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
