Title: Las Regiones del Centro: una mirada a los Esquemas Asociativos de Entidades Territoriales
Date: 2017-12-18 09:02
Category: Ambiente y Sociedad, Opinion
Tags: Ambiente, entidades territoriales, Mecanismos de partipación
Slug: las-regiones-del-centro-una-mirada-a-los-esquemas-asociativos-de-entidades-territoriales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/1509009127_mani-dnp-plan-desarrollo-11-e1513605322981.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto:prensalibrecasanare 

###### **[Karla Diaz, Asociación Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/)** 

La Ley Orgánica de Ordenamiento Territorial (LOOT) da forma a los Esquemas Asociativos de Entidades Territoriales, cuyo propósito es cumplir de forma más eficiente e integrada los procesos de planeación en el territorio. Ahora bien, el mecanismo luego de 6 años no ha tenido mucha acogida por parte de las entidades territoriales, pese a la necesidad de planear desde una perspectiva amplia, regional y no parroquial, como se ha hecho hasta ahora. El quid de este desinterés parece estar en las viejas enemigas autonomía – presupuesto que esta vez va ganando el pulso hacia la re-centralización de las competencias de planeación, en contravía del discurso de profundización de la democracia que se sostiene desde la firma de los Acuerdos de Paz.

[La Asociación de Municipios, de Departamentos, Áreas Metropolitanas y de Distritos surge como respuesta a las deficiencias en el cumplimiento de las competencias de planeación del ordenamiento por parte de los municipios y la falta de integración en los planes de desarrollo entre municipios y departamentos. La principal finalidad de estas figuras es brindar servicios públicos, ejecutar obras de interés común y llevar a cabo proceso de planificación conjunta.]

Para planear el territorio de forma integrada se crean las Provincias Administrativas y de Planificación (PAP), que asocia a dos o más municipios contiguos; la Región Administrativa y de Planificación (RAP), conformada por departamentos vecinos; la Región Administrativa de Planificación Especial (RAPE), conformada por la Asociación del Distrito Capital y otros departamentos, y finalmente, las Áreas de Desarrollo Territorial (ADT), que congrega a municipios o departamentos por donde pasan proyectos estratégicos para la Nación (CEI, 2013).

Para hacer posible la conformación de estos esquemas se disponen diversos instrumentos de carácter presupuestal, pues sólo las RAP y las RAPE cuentan con personería jurídica, autonomía fiscal y recursos propios. Las demás figuras, como la Asociación de Municipios y Departamento dependen de la firma de contratos-Plan (ahora contratos-paz); mientras otras, como las ADT son dependientes del capital privado provisto por Asociaciones Público Privadas (APP) o la Agencia de Desarrollo Local, la cual está conformada por recursos del Estado y capital privado.

Incluso en figuras como la RAP y la RAPE que gozan de autonomía fiscal y recursos propios surgen varias dudas, pues ni en la LOOT se define el régimen fiscal aplicable, ni el DNP tiene propuesta alguna en término de rentas y jurisdicción de recaudo, cuestión que por su naturaleza tendría que pasar por el Congreso de la República, por ser el único competente en materia tributaria.

Ahora, es claro que la asociatividad supeditada a la firma de contratos-plan no es una base estable para consolidar regiones estables y con vocación de permanencia, pues cada proyecto estará sujeto a un escrutinio ajeno que, entre otras cosas, limita la capacidad de los entes territoriales para determinar qué proyectos son o no son de su interés, en la medida que para asegurar el financiamiento, en un mar amplio de competencia por los recursos centrales, estos deberán alinearse a los imperativos del Plan Nacional de Desarrollo.

Sumado a la dependencia presupuestal como limitante de la autonomía de los entes territoriales, cabe señalar que la conformación de cualquiera de estas figuras está supeditada a la aprobación de la Comisión de Ordenamiento Territorial (COT), el Departamento Nacional de Planeación (DNP) y el Comité Especial Interinstitucional (CEI), todos ellos órganos de carácter técnico centralizados cuya principal función es propender por la coherencia entre los procesos regionales y nacionales de planificación (CEI, 2013). Lo anterior pone sobre la mesa varias incógnitas con respecto al margen de acción de los municipios y departamentos en aquellos temas que no se alinean al interés del gobierno nacional, como son las consultas populares en temas ambientales, ¿Será que el COT, el DNP y el CEI aprobarían la conformación de una Región que se declare no minera o libre de petróleo?

Los indicios llevan a afirmar que los esquemas asociativos tienen más nombre y funciones que recursos y autonomía para llevarlas a cabo, dando un paso atrás sobre la  limitación presupuestal y técnica que hasta ahora incapacitado a los municipios en el cumplimiento de la función que los constituyentes, en su afán de democratizar la planeación, les dieron. Sumado a esto, se da un paso atrás también en la autonomía de los entes territoriales en la planeación, pues el gobierno central y el capital privado se abre caminos para incidir en el ordenamiento de los territorios.

[La pregunta que queda por hacerse entonces es, si en el marco de los Acuerdos de Paz y el proceso de implementación se habla de profundizar la democratización de la planeación por medio de los Programas de Desarrollo con Enfoque Territorial (PDET) con su enfoque participativo y fortalecer a los Entes Territoriales, ¿Por qué se proponen esquemas de asociatividad que re-centralizan la planeación, alineando los proyectos a los imperativos de desarrollo y ordenamiento del mercado y el gobierno central?.]

#### **[Conozca aquí  las columnas de opinión de Ambiente y Sociedad](https://archivo.contagioradio.com/margarita-florez/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

------------------------------------------------------------------------

###### [Bibliografía] 

###### [Comité Especial Interinstitucional – CEI- (2013)]*[Definición legal y funcional de los Esquemas Asociativos de Entidades Territoriales en Colombia,]*[Bogotá: DNP] 

###### Departamento Nacional de Planeación –DNP- (2017) *[POTs Modernos,]*[disponible en:][[https://es.slideshare.net/encuentrored/lanzamiento-territorios-modernos-dnp-junio072016]](https://es.slideshare.net/encuentrored/lanzamiento-territorios-modernos-dnp-junio072016) 
