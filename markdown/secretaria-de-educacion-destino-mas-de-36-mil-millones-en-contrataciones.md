Title: Secretaria de Educación destinó más de $36 mil millones en contrataciones
Date: 2016-08-17 15:22
Category: Educación, Nacional
Tags: colegios en concesión, contratos secretaria de educación, segunda administración peñalosa
Slug: secretaria-de-educacion-destino-mas-de-36-mil-millones-en-contrataciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Rutas-Escolares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía de Bogotá] 

###### [17 Ago 2016 ] 

Durante los primeros siete meses de este año la Secretaria de Educación del Distrito, celebró más de [[2.700 contratos](http://static.elespectador.com/archivos/2016/08/8c95ed5443f92c247ca17624a18d8070.pdf)] por una cifra superior a los \$36 mil millones que se han invertido en asuntos como suministro de combustibles, colegios en concesión, servicios profesionales para lineamientos jurídicos, apoyos profesionales para bibliotecas escolares y acompañamientos de rutas, principalmente.

Según denuncia Germán García, Concejal del Partido Liberal, en **1.226 contratos para servicios profesionales se invirtieron \$26.378.780 millones** y en 1.479 contratos de servicios administrativos se destinaron \$9.664.284 millones; sorprende entonces que mientras el [[magisterio capitalino reclama mejoras](https://archivo.contagioradio.com/300-mil-maestros-de-colombia-inician-paro-nacional-por-incumplimientos-del-gobierno/)] en sus condiciones laborales, la mayor parte de los recursos estén destinados a funcionarios ajenos a las instituciones educativas.

Durante enero, se firmaron 182 contratos por \$3.479 millones; en febrero se celebraron 1.085 contratos por \$7.623 millones; **en marzo fueron 572 contratos por \$9.888 millones**; en abril, 477 por \$7.366; en mayo 134 por \$2.036 millones; en junio 3 por \$30 millones y en julio 252 por \$5.618 millones.

Preocupa la situación porque podría tratarse de nóminas paralelas, teniendo en cuenta la reducida cantidad de contratistas beneficiarios, sumado a que **los datos presentados por la Secretaria no incluyen su planta básica** ni detalles sobre los contratos con personas jurídicas; por lo que se espera que María Victoria Ángulo, actual secretaria de educación, se pronuncie al respecto, y que haya vigilancia de organismos de control.

<div>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

</div>

<div class="block-title-gray">

</div>
