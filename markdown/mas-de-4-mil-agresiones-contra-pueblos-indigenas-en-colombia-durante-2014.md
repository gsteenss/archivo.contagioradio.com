Title: Más de 4 mil agresiones contra pueblos indígenas en Colombia durante 2014
Date: 2015-08-10 17:42
Category: DDHH, Entrevistas, infografia, Nacional
Tags: Caracol TV, Derechos Humanos, informe 9 de agosto, ONIC, ONU, pueblos indígenas, Séptimo Día.
Slug: mas-de-4-mil-agresiones-contra-pueblos-indigenas-en-colombia-durante-2014
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Rueda-de-prensa-ONIC-y-ONU.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @ccajar 

<iframe src="http://www.ivoox.com/player_ek_6453900_2_1.html?data=l5milZ6UdI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRjc%2Fa0Nfax5DXrdXpwsjWh6iXaaOnz5DRx5CoiKm8jMnSjcjTsdbiysnOxsrXb8rixYqwlYqliMjZz8aYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rueda de prensa] 

###### [10 Ago 2015]

El 9 de agosto se celebró el Día Internacional de los Pueblos Indígenas, en el marco de esta conmemoración la Organización Nacional Indígena de Colombia, ONIC en conjunto con la Oficina de las Naciones Unidas, realizó una rueda de prensa para evidenciar la situación de las comunidades indígenas en el país durante el año 2014.

El consejero mayor de la ONIC, Luis Fernando Arias, resaltó que los últimos días, la lucha indígena ha sido estigmatizada y criminalizada por el gobierno y los medios de comunicación como el programa Séptimo Día de Caracol Televisión, y afirmó que “**a los indígenas no nos han regalado nada, los derechos nos los hemos ganado en el proceso de lucha”.**

Por otra parte, aseguró que Colombia es una de las naciones que registra altos índices de impunidad frente a las violaciones de derechos humanos hacia pueblos indígenas, por lo que propuso al país un debate riguroso y claro sobre el futuro de los pueblos nativos.

Finalmente, el representante de la ONU para los Derechos Humanos en Colombia, Todd Howland, expresó que “Es importante que en la mesa, **el Gobierno y las FARC inviten a los pueblos indígenas y afrocolombianos** (...) para hablar sobre las dificultades y desafíos en razón del proceso de paz, para oír sus opiniones, porque sin su participación este proceso de paz no puede ser sostenible", y destacó que en Colombia “en Colombia hay problemas al entender y respetar la Ley propia de pueblos”.

[![opcion-1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/opcion-1.png){.aligncenter .wp-image-12027 .size-full width="869" height="1000"}](https://archivo.contagioradio.com/mas-de-4-mil-agresiones-contra-pueblos-indigenas-en-colombia-durante-2014/opcion-1/)

[Informe Vulneraciones a Los Ddhh e Infracciones Al Dih 2014](https://es.scribd.com/doc/274094802/Informe-Vulneraciones-a-Los-Ddhh-e-Infracciones-Al-Dih-2014 "View Informe Vulneraciones a Los Ddhh e Infracciones Al Dih 2014 on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")  
<iframe id="doc_20404" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/274094802/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-NiP1fk8vHEWwcFbhZeJN&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
