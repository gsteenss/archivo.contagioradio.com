Title: Autodefensas Gaitanistas de Colombia anuncian paro armado
Date: 2016-08-16 14:08
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia
Slug: autodefensas-gaitanistas-anuncian-paro-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Paro_armado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hechoencali] 

###### [16 de Agost] 

Un grupo de aproximadamente 200 hombres que se identificaron como **Autodefensas Gaitanistas de Colombia, atemorizaron a la comunidad de Jiguamiando** en el Bajo Atrato y anunciaron , a través de un mensaje de audio que llegó a diferentes miembros de la comunidad, que el **2o de agosto realizarán un nuevo paro armado**.

En el audio se escucha la vos de un hombre diciendo que **"el paro será el doble de lo que paso, apoyado por las AUC o las bandas criminales del Golfo"**, además  dice que las **personas que sean encontradas en las calles o tiendas serán dadas de baja, razón por la cual deben abastecerse.** El audio llego a comerciantes de Chigorodó en Antioquia y otras zonas aledañas, a traves de redes sociales.

De acuerdo con José Francisco Álvarez, miembro de la red de comunicadores CONPAZ, el grupo en donde habían hombres vestidos de camuflado y civil, llegó a Jiguamiando el pasado 10 de agosto, **retuvieron a dos indígenas identificados como Argenito Jumí Tapia y Jhon Fredy Rubiano Barquín** de 20 y 19 años de edad respectivamente, para ser usados como guías hacía Pavarandó y a[menazaron a la comunidad con no denunciar su presencia en el territorio.](https://archivo.contagioradio.com/autodefensas-gaitanistas-amenazan-a-lideres-en-barrancabermeja/)

A su vez, uno de los miembros de esta organización indicó que buscaban a "Mauricio" hombre señalado por la comunidad de ser tenedor de mala fe de predios indígenas, [hecho que evidencia la relación que existe entre el paramilitarismo y la tenencia de estas tierras](https://archivo.contagioradio.com/autodefensas-gaitanistas-hostigan-a-familias-de-la-vereda-guacamayas-turbo/).

Los jóvenes fueron dejados en libertad el pasado 13 de agosto luego de ser interrogados por parte de la estructura, sobre la presencia de la guerrilla en el territorio. **Cabe resaltar que muy cerca del lugar de retención de los indígenas se ubicará una de las zonas de concentración de las FARC-EP**, de igual modo las autoridades tampoco se han pronunciado frente a la presencia de paramilitares en esta zona.

<iframe src="https://co.ivoox.com/es/player_ej_12565465_2_1.html?data=kpeimJqYepahhpywj5aVaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncavj1IqwlYqlfYy608bbxc7Xp9Cfhqigh52VsNfV08rnh5enb6TDr7WuvJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
