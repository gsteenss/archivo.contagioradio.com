Title: "Lo fuerte de este proceso es la participación de la sociedad", ELN
Date: 2016-03-31 16:46
Category: Entrevistas, Paz
Tags: Caracas, ELN, proceso de paz eln, sociedad civil
Slug: los-fuerte-de-este-proceso-es-la-participacion-de-la-sociedad-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/mesa-de-dialogos-eln-gobierno-copia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alba TV 

###### [31 Mar 2016] 

Luego de que se diera el anuncio del inicio de la [fase pública de conversaciones de paz con el ELN,](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/)en rueda de prensa, la delegación de paz de esa guerrilla, señaló que uno de los puntos esenciales en el marco de estos diálogos es el de la participación de la sociedad civil como sujeto clave para la construcción de la paz en Colombia. “**Lo fuerte de este proceso será la participación de la sociedad,** lo primero que se debe estudiar es el diseño de la participación", aseguró desde Caracas, Antonio García, Jefe de la delegación de paz del ELN.

Para diversos sectores del movimiento social, este momento histórico que se traza con la apertura pública de diálogos con el [ELN](https://archivo.contagioradio.com/?s=eln) “**permite ir avanzando hacia una paz mucho más completa”,** como lo asegura Marelyn Serna, vocera nacional del Congreso de los Pueblos, quien resalta que el gobierno deberá brindar verdaderas garantías y espacios al movimiento social. A su vez, el senador del Polo Democrático Alternativo, asegura que **“Esta es una oportunidad grande para ampliar la participación del movimiento social”.**

Para Serna, es crucial que se desmonten las estructuras paramilitares que actualmente siguen empeñados en acabar con procesos de resistencia en los territorios y además, es necesario que **el punto de Transformaciones para la Paz, signifique reformas  para la política minero-energética , agraria, salud, y trabajo, lo que implicaría cambios en el modelo económico y político,** así Juan Manuel Santos, en su alocución haya asegurado que no se discutirá sobre ese tema.

Finalmente, Germán Roncancio, vocero de Clamor social por la paz y académico de Universidades por la Paz, piensa que este proceso debe servir para generar cambios en los problemas estructurales del país, que a su vez **deberán ser apalancados a través de la participación vinculante del pueblo colombiano,** para que exista realmente una paz con justicia social.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
