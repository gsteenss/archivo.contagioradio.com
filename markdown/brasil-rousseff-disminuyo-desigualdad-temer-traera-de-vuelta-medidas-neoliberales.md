Title: Brasil: "Rousseff disminuyó desigualdad, Temer traerá de vuelta medidas neoliberales"
Date: 2016-09-01 12:14
Category: El mundo, Política
Tags: Dilma Roussef impeachment, Dilma Rousseff, Gobierno de Dilma Rousseff
Slug: brasil-rousseff-disminuyo-desigualdad-temer-traera-de-vuelta-medidas-neoliberales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Manifestación-contra-Temer.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias MVS] 

###### [1 Sep 2016] 

"Esta historia no termina así. Estoy segura que **la interrupción de este proceso por el golpe de estado no es definitiva**. Volveremos. Volveremos para continuar nuestro viaje rumbo a un Brasil en el que el pueblo sea soberano", aseguró la ex mandataria Dilma Rousseff, tras conocer su definitiva destitución, mientras en las calles de Sao Paulo y Río de Janeiro diversos sectores sociales enfrentaban la violenta represión por parte de la Policía a la movilización que emprendieron en rechazo a la decisión tomada por el Senado.

Según afirma Vivían Fernández, integrante de Brasil De Fato, el pueblo amaneció bastante convulsionado pues **no creía que 40 años después volviera a vivir un golpe de Estado**, mucho menos cuando los dos últimos Gobiernos generaron cambios importantes para disminuir la desigualdad económica y social en la nación, transformaciones que estiman se verán laceradas con las políticas de corte neoliberal que pondrá en marcha Michel Temer para implementar un nuevo proyecto de país que impactará negativamente a América Latina.

Parte de los cambios que estos Gobiernos adelantaron para favorecer a los sectores más empobrecidos incluyeron la consolidación de programas de vivienda digna, un sistema de salud público, que el nuevo mandatario amenaza con liquidar, y garantías laborales que podrían afectarse según los recientes anuncios de Temer de aumentar a 70 años la edad de jubilación. Para Fernández esta situación evidencia que **Latinoamérica enfrenta un proceso de cambio caracterizado por tentativas golpistas**, producto del actual debilitamiento de los programas populistas.

"Espero que sepamos unirnos en defensa de causas comunes a todos los progresistas, independientemente de su filiación partidista o posición política. Propongo que luchemos, todos juntos, [[contra el retroceso](https://archivo.contagioradio.com/las-reacciones-en-america-latina-tras-la-destitucion-de-dilma-rousseff/)], contra la agenda conservadora, contra la extinción de derechos, por la soberanía nacional y el restablecimiento de la democracia", expresó la ex mandataria. Intención que es respaldada por diversos sectores sociales que continuarán manifestándose en las calles ante la **imposibilidad Constitucional de convocar elecciones inmediatas**.

<iframe src="http://co.ivoox.com/es/player_ej_12744041_2_1.html?data=kpeklpmUeJKhhpywj5aZaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncbfd187O0JCqqdPihqigh6aVssXZ24qfpZCmtsLnytGYxsqPisLo0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

###### 

 
