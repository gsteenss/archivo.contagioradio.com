Title: Con paso firme avanza un nuevo movimiento estudiantil por el derecho a la educación
Date: 2017-10-23 16:20
Category: Movilización, Nacional
Tags: ICETEX, Movimiento estudiantil
Slug: 3-y-4-de-noviembre-encuentro-distrital-de-estudiantes-de-universidades-publicas-y-privadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-23-at-12.44.12-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Oct 2017] 

Estudiantes de Universidades públicas y privadas tendrían su primer encuentro los próximos **3 y 4 de noviembre, en la Universidad Pedagógica Nacional.** Allí se espera conformar el primer comité de impulso hacia un encuentro Nacional en el que se defina la ruta de movilización para defender el derecho a la educación**.**

De acuerdo con Amalfi Bocanegra, representante ante el Consejo Superior de la Universidad Pedagógica Nacional, **se espera que aunque el evento se realice en Bogotá, la participación sea nacional** para tener un balance del estado actual de la educación superior en el país.

“Lo hemos denominado Encuentro Distrital Ampliado y tiene la intensión de encontrarnos, no solo las delegaciones que hemos asistido sino todos y todas las que quieran participar” afirmó Bocanegra y agregó que una vez se realice este escenario y posteriormente el Encuentro Nacional, se plantee **la posibilidad de hacer el paro estudiantil.** (Le puede interesar: ["Así fue la movilización en defensa de la Educación"](https://archivo.contagioradio.com/estar-seran-las-rutas-de-movilizacion-de-la-marcha-por-la-educacion/))

En el primer escenario de encuentro realizado el pasado viernes 21 de octubre, asistieron estudiantes de universidades públicas como la de Pamplona, la Universidad Industrial de Santander y diferentes universidades privadas de la capital.

### **El movimiento estudiantil y las elecciones 2018** 

Frente a un posible apoyo por parte del estudiantado a un candidato en esta campaña 2018, Amalfi expresó que no esperan que un candidato muestre su disposición para cumplir con lo que le corresponde a la Educación, en término de ser garantizada por el Estado, **“vamos a construir una agenda de movilización y una programática que vaya por encima del proceso electoral”.**

Para los estudiantes un ejemplo de la presión que puede generar la movilización son los 100 billones que se otorgaron como presupuesto para las universidades públicas, que aunque es irrisorio, de no haber un ejercicio de exigencia por parte de la comunidad académica, hubiese sido mucho menos dinero. (Le puede interesar: ["Estudiantes de públicas y privadas listos para afrontar política de "Ser Pilo Paga"](https://archivo.contagioradio.com/48227/))

<iframe id="audio_21636169" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21636169_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
