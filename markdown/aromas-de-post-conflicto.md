Title: Aromas de post-conflicto
Date: 2016-08-05 08:24
Category: Columnistas invitados, Opinion
Tags: Desmonte paramilitar, posconflicto, proceso de paz
Slug: aromas-de-post-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/aromas-de-posconflicto1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zassle 

#### **[Camilo Álvarez Benítez](https://archivo.contagioradio.com/camilo-alvarez-benitez/)- [@camiloalvarezb](https://twitter.com/CamiloAlvarezB)** 

###### 5 de Ago 2016 

Sucedió en Bogotá; La mañana se abría paso mientras miles de gentes corrían para no llegar tarde al trabajo, en su rutina de trabajar para consumir- consumir para trabajar; iba el anacoreta sentado en los estertores de un bus no chatarrizado  por la carrera décima, destino final de la mayoría; el trancón fue entonces el reflejo perfecto para darse cuenta de las parábolas del tiempo: todos con afán nada que hacer con la espera.

Fue cuando el personaje se subió al bus, pagó la mitad de su pasaje, alzó la pierna para evadir la registradora, cruzó su morral hacia su abdomen, miró su campo de batalla, entre mil maromas recorrió cada puesto dejando una caja en las piernas de cada uno de los pasajeros; volvió a la registradora, respiró profundo y entonces:

-¡Muy buenos Días! … Gracias a los que me respondieron, un saludo como un vaso de agua no se le niega a nadie y este país podría cambiar más desde las mañanas con un saludo; mi nombre es Yeison Norbey Atehortua Giraldo, vengo de la comuna 8 de Medellín; cristiano por vocación, sobreviviente de profesión; el día de hoy vengo ofreciéndoles este…-

Los paquetes de incienso en varita de colores “made in India” se le resbalaron de las manos, cayendo entre los pies de algún pasajero; observado con desdén, se agachó lentamente y los recogió, ágilmente los introdujo en su cinto, como si fueran revolver; dejando solamente unos cuantos de muestra en sus manos.

-Cómo les venía diciendo, esta mañana vengo ofreciéndoles este incienso, aromatizado, de esencias naturales importado de la india, de allá donde si saben de inciensos, a un módico costo de 1 por 500 pesos, 3 en 1000; la cajita 1 en 2000, 3 en 5000; son inciensos con aroma a Sándalo, vainilla, rosas, canela, lavanda, cedro, jazmín y pachulí. Ideal para la oficina y el trabajo, para regalarle a la novia o para cualquier momento; recuerden 1 en 500, 3 en 1000; la cajita 1 en 2000, 3 en 5000 -

Recorrió cada uno de los asientos con la mano extendida; -buenos días amigo, ¿Me quiere colaborar? No, gracias.  Buenos días señora, ¿Me quiere colaborar? No, gracias. Buenos días señor, ¿me puede devolver el incienso? ¿Me quiere colaborar? No, Gracias.-

Con una venta en ceros, regresó a la parte frontal del bus; miró de nuevo su campo de batalla, respiró profundo, besó el escapulario de la virgen del Carmen y del cristo salvador; ágilmente retiro las cajas que había puesto en su cinto y empuñándolas cómo un arma dijo:

-¡Muy buenos Días! … Gracias a los que me respondieron, un saludo como un vaso de agua no se le niega a nadie y este país podría cambiar más desde las mañanas con un saludo; mi nombre es Yeison Norbey Atehortua Giraldo vengo de la comuna 8 de Medellín; cristiano por vocación, sobreviviente de profesión; Nací hace 21 años en uno de los barrios más humildes, a los 10 años había participado en mi primer robo, a los 11 ya había probado mi primer fierro, a los 12 ya había coronado mi primer morraco, desde los 13 fui sicario, meras vueltas; a los 16 todos los de mi parche fuimos reclutados por Carlos Castaño, doble cero y don berna en lo que luego se llamó el Bloque Cácique Nutibara, nuestro tema era el control de los barrios y a veces nos ponían a hacer vueltas en la costa matando campesinos o en las ciudades cómo esta, ascendí como gatillero por mi efectividad y por qué con los muñecos que me encargaron, todo salió limpio, siempre me encomiendo a la virgen del Carmen, al Jesucristo salvador y también a mi cucha; desde hace 8 meses me desmovilicé bajo la ley 975 entregando unas armas hechizas al señor Luis Carlos Restrepo, cómo eso estaba muy difícil allá, nos trajeron para Bogotá a mí y a los que teníamos más caliente el parche; nos metieron en unas casas en el barrio Teusaquillo, dizque esperando un subsidio de 1.200.000 mensuales; desde hace 4 meses no nos entregan la luka; yo estoy acostumbrado a trabajar desde pequeño, y entonces me la dedico al rebusque.-

Tomó airé y miro firmemente su público cautivo:

-Mi mamá siempre me dijo, vaya mijo y trabaje honestamente, sino se puede honestamente, pues mijo trabaje y aquí estoy ofreciéndoles este incienso, aromatizado, de esencias naturales importado de la india, de allá donde si saben de inciensos, a un módico costo de 500 pesos, 3 en 1000; la cajita 1 en 2000, 3 en 5000; son inciensos con aroma a Sándalo, vainilla, rosas, canela, lavanda, cedro, jazmín y pachulí. Ideal para la oficina y el trabajo, para regalarle a la novia o para cualquier momento; ¿Sí Me van a Comprar? Porque, Yo la verdad señores y señoras yo no quiero volver a pegar pal monte con motosierra y machete en mano a matar campesinos o venir a sicariar algún personaje por estas calles, o ¿ustedes que piensan? ¿Nadie me va a comprar el hijueputa incienso?

La casa del anacoreta olía saturadamente a Sándalo, cuando me contó esta historia.
