Title: Estas son las primeras víctimas de las medidas de Trump
Date: 2017-01-25 18:34
Category: DDHH, El mundo
Tags: aborto, cambio climatico, Donald Trump Estados Unidos, Palestina
Slug: migrantes-refugiados-naturaleza-palestinos-mujeres-primeras-victimas-las-medidas-trump
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/trump-e1485386330287.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Business Insider]

###### [25 Ene 2017] 

El populismo, nacionalismo y proteccionismo fueron las líneas del primer discurso de Donald Trump como presidente de Estados Unidos. Palabras que a tan solo 5 días de su posesión, cobran sentido y ya empiezan a ser hechos reales, que para muchos, implica la violación de los derechos de las mujeres, los migrantes, los palestinos, y la naturaleza.

### Profundización de la ocupación Israelí 

Dos días después de que el primer ministro israelí, benjamín Netanyahu tuviera una conversación por teléfono con el Trump, en la que se acordó  un encuentro en Washington en febrero, Netanyahu dio a conocer su primer paquete de construcciones en territorios ocupados.  El primero consiste en la puesta en marcha de 560 nuevas viviendas en Jerusalén Este. Un día después, el primer ministro anunció la aprobación de **2.500 viviendas más en los asentamientos de Cisjordania**. Según él “para responder a las necesidades de alojamiento y de la vida cotidiana”.

### Decreto contra el aborto 

Este martes, Trump firmó un decreto que, de acuerdo con organizaciones del mundo, vulnera los derechos sexuales y reproductivos de las mujeres. Se trata de la ley Mexico City Policy, con la que se le prohíbe a las **asociaciones extranjeras realizar abortos, y brindar cualquier tipo de información** sobre el tema. Este decreto había sido derogado por Barack Obama, pero como si hubiese sido un castigo por la multitudinaria marcha de las mujeres en Washington DC, el magnate decidió revivirlo.

### Medidas contra refugiados 

De acuerdo con diferentes medios de Estados Unidos, el magnate alista **órdenes para suspender el programa de refugiados durante al menos seis meses,**  y también detener por los menos por 30 días la emisión de visados para personas provenientes de Irak, Irán, Libia, Somalia, Sudan y Siria. También, refiriéndose al terrorismo, defendió la la tortura contra sospechosos yihadistas.

### El muro y la seguridad fronteriza 

Trump ya anunció que reforzará la seguridad en la frontera de México y Estados Unidos.  Según sus palabras, la **construcción del muro comenzará "en meses"** y "de inmediato" inicia la planifcación, además reiteró que **el costo del muro será "reembolsado por México”** en  “cien por cien". Asimismo, estableció la contratación de 5,000 nuevos agentes fronterizos y la construcción de nuevas instalaciones para detener inmigrantes indocumentados. A su vez, una de las medidas que más preocupa es la restauración del programa Comunidades Seguras, que permitiría a las policías locales establecer el estatus migratorio de cada persona al momento de su arresto lo que fácilmente promueve el aumento de deportaciones.

### Construcción de oleoductos 

Así como se ha conocido que en la página web de la Casa Blanca, se ha borrado todo registro relacionado con el cambio climático, el mandatario ya anunció otras órdenes que generarían graves impactos ambientales. Trump ahora dio aval para la puesta en marcha de los **oleoductos Keystone XL y Dakota Access**. Obama había suspendido el proyecto ya que el proyecto generaba enormes cantidades de gases que causan el cambio climático. Sin embargo, y en contra de los reclamos de ambientalistas, el nuevo presidente ya tomó la decisión de avanzar en la construcción de estos dos oleoductos, como lo había prometido en su campaña.

###  

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
