Title: Un Festival reconoce el liderazgo de las mujeres en las artes escénicas
Date: 2019-05-21 14:35
Author: CtgAdm
Category: eventos
Tags: ‘Bordando para reparar ausencias’, Bogotá, danza contemporánea, genero
Slug: un-festival-reconoce-el-liderazgo-de-las-mujeres-en-las-artes-escenicas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Danza-hysterya.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/3-mujeres-y-mandalas-Casa-de-la-Cultura-de-Suba.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/1-Mujeres-y-Mandalas-Casa-de-la-Cultura-de-Suba.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/28022019-28022019-_MG_8339-Mónica-Osma-Tapias.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Productores Hysterya Fest 

Con la intención de visibilizar y empoderar el liderazgo de las mujeres en las artes escénicas, nace en **Bogotá Hysterya Fest DC, un festival de danza contemporánea que del 27 de mayo al 1 de junio** presentará una programación que combina puestas en escena, cine y talleres en el campo de la danza profesional.

Serán [**10 directoras y coreógrafas de danza contemporánea**](https://hysteryafest.wixsite.com/hysterya-fest/blog) quienes se darán cita en la capital entre quienes se encuentran **Olga Barrios** de Santa Marta, **Juana Ibanaxca Salgado, Carolina Ramírez, Jimena Alviar, Pilar Hernández y Catalina Mosquera** de Bogotá, con la presentación de obras cortas; la proyección de una película de danza de la directora **Jennifer Jesum** de EE.UU.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/1-Mujeres-y-Mandalas-Casa-de-la-Cultura-de-Suba-300x199.jpg){.wp-image-67535 .alignleft width="333" height="221"}![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/3-mujeres-y-mandalas-Casa-de-la-Cultura-de-Suba-300x199.jpg){.wp-image-67534 .alignleft width="333" height="221"}![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/28022019-28022019-_MG_8339-Mónica-Osma-Tapias-300x200.jpg){.wp-image-67536 .alignleft width="335" height="224"}

 

“Abrimos este nuevo espacio para empoderar y visibilizar el trabajo profesional de las directoras y coreógrafas de la danza contemporánea, no solo de Bogotá, sino de donde provengan... **Estamos convencidas que abrir este espacio permitirá que las mujeres alrededor de la práctica artística, podamos reconocernos**” asegura Jimena Alviar, co-directora del Festival.

 

Adicionalmente, en el marco del evento se adelantarán talleres en relación con las maneras de circular las obras de danza y de entrenamiento corporal; el **cuarto Encuentro ENDANZA, ¿Qué es ser profesional en la danza?**;  y un conversatorio sobre el rol de la mujer en la danza.

 

Según **Sheyla Yurivilca**, co-directora del evento, “Hysterya Fest será una nueva opción cultural y artística anual para que los bogotanos y visitantes aprecien la danza contemporánea profesional... Entendiendo la Danza contemporánea como **una expresión escénica en constante evolución**, donde los capitalinos podrán **apreciar la diversidad de lenguajes corporales entorno a temáticas transversales del país**”.

El evento es organizado por JA Compañía de Danza, la Fundación Integrando Fronteras y GP Entrenamiento Corporal, liderado y Codirigido por Jimena Alviar, Sheyla Yurivilca Aguilar y Gabriela Pardo. Bonos de apoyo de \$30.000. A continuación compartimos la programación oficial del evento.

<iframe id="doc_5051" class="scribd_iframe_embed" title="Programación Hysterya Fest DC" src="https://es.scribd.com/embeds/410985920/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-38CGpPVGYdwlFjUMeg9j&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.7790927021696252"></iframe>

<div class="entry-content">

<div class="has-content-area" data-url="https://archivo.contagioradio.com/dia-de-la-afrocolombianidad-una-fecha-para-reafirmar-la-lucha-contra-el-racismo/" data-title="Día de la afrocolombianidad: una fecha para reafirmar la lucha contra el racismo">

###### Reciba toda la información de Contagio Radio en [[su correo]

</div>

</div>
