Title: Solidaridad: la fuerza que ha vencido la violencia en el Paro Nacional
Date: 2019-12-16 13:00
Author: CtgAdm
Category: Movilización, Paro Nacional
Tags: Abuelas del Parkway, ESMAD, Fuerza Pública, guardia indígena, Paro Nacional, recicladores, resistencia
Slug: solidaridad-la-fuerza-que-ha-vencido-la-violencia-en-el-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/11.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/17.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@andrescamachom\_] 

[En el marco del Paro Nacional **se han vivido momentos de sincera generosidad entre los ciudadanos y ciudadanas.** Tras varias semanas de manifestaciones, los episodios violentos y el abuso de la fuerza pública han sido combatidos con música, bailes, protesta y actos de generosidad, solidaridad y compañerismo por parte de la ciudadanía.]

[Dentro de la sintonía de los encuentros en las calles, los parques y las plazas, han surgido momentos de comunidad y confianza en el colectivo, síntomas perdidos dentro de una saciedad cada vez más ensimismada en sí, puestos los ojos en el individuo y reflejados sus rostros en las pantallas de los celulares. ]

\[caption id="attachment\_77634" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/11-1024x683.jpg){.size-large .wp-image-77634 width="1024" height="683"} Paro Nacional Bogotá - Carlos Zea/Contagio Radio\[/caption\]

### **Aquellos que cuidan a los otros en el Paro Nacional** 

[En el marco del Paro Nacional, las manifestaciones han dejado un saldo de más de 300 personas heridas, incluyendo la muerte del joven estudiante Dilan Cruz. En momentos como estos, han aparecido personas que decidieron tomar una posición de defensa contra la fuerza pública y el abuso desmedido del Escuadrón Móvil Antidisturbios.][(Lea también: Agresiones del ESMAD son solo "daños colaterales" para el Gobierno)](https://archivo.contagioradio.com/agresiones-del-esmad-son-solo-danos-colaterales-para-el-gobierno/)

[Tal es el caso de los jóvenes que decidieron establecer la llamada “Primera Línea”. Armados con escudos azules, que en stencil llevan marcado el escudo del ejército rebelde de las películas de Star Wars, estos jóvenes avanzan en la parte delantera y trasera de la movilización cuidando a los manifestantes.]

[Sus razones son claras: **protección y atención a los manifestantes que puedan caer heridos por los ataques del ESMAD o presas de algún tipo de eventualidad dentro de la movilización.** Junto a defensores de derechos humanos y personas con experiencia en atención médica inmediata, los escudos azules han sentado un precedente en la forma en que los ciudadanos se organizan y marchan por sus derechos en el país. ]

### **Lo sentimos, no somos todos ** 

[Los constantes abusos por parte de la Policía Nacional, en gran medida realizados por su Escuadrón Móvil Antidisturbios, han generado una dinámica compleja que termina en resentimientos entre manifestantes y fuerza pública. (Le puede interesar: [Encuentros juveniles: Motores del paro nacional)](https://archivo.contagioradio.com/encuentros-juveniles-motores-del-paro-nacional/)]

[Sin embargo, ciertas acciones comprometidas con el Paro Nacional, realizadas por algunos integrantes de la Policía y el Ejército, han sido recibidas con cariño por parte de los manifestantes. Una de ellas es la del policía captado por un video de celular, que sostiene su teléfono móvil donde se lee una frase que se repite como un anuncio de telemarketing: **“Sin violencia: lo sentimos”. **]

https://twitter.com/LitDelta/status/1198475901257867264

[Por otro lado, el soldado joven Brandon Cely Páez, que afirmó estar cumpliendo con el servicio militar obligatorio, se manifestó a favor del Paro en un video que se difundió por redes sociales. Allí, el militar critica duramente a la institución a la cual pertenece y ratifica su apoyo a las demandas sociales exigidas en las calles. Lamentablemente, después de grabar el video el joven se quitó la vida. ]

[También se presentó un acto de generosidad por parte de varios ciudadanos con el ESMAD. **En una de las zonas de Bogotá donde se presentaron movilizaciones, vecinos del sector y manifestantes tomaron chocolate con pan en compañía.** Las personas demostraron el apoyo a la fuerza pública y los manifestantes la necesidad de unir, no de dividir, a los que marchan y a los que actúan desde el marco legal otorgado por el Estado. ]

### **Los indígenas se toman la ciudad en medio del Paro Nacional** 

\[caption id="attachment\_77641" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/17-1024x683.jpg){.size-large .wp-image-77641 width="1024" height="683"} Paro Nacional Bogotá - Carlos Zea/Contagio Radio\[/caption\]

[La guardia indígena no estuvo exenta de las movilizaciones que se han presentado en el marco del Paro Nacional, decidiendo llegar a Bogotá en los primeros días de diciembre para brindar su apoyo a la causa social de todos los manifestantes que se han movilizado en estos días. (Lea también: [La defensa de la vida, un punto central del Paro Nacional)](https://archivo.contagioradio.com/la-defensa-de-la-vida-un-punto-central-del-paro-nacional/)]

[Sin importar la lluvia, **los indígenas se movilizaron por las principales vías de la capital exigiendo que se respeten los derechos sociales de todas las personas en Colombia.** Su llegada y estadía en la Universidad Nacional de Colombia, como en la sede de la Organización Nacional Indígena de Colombia (ONIC), demostró el compromiso de los indígenas por el país, aun cuando su lucha en territorio lleva mucho más tiempo.]

https://twitter.com/RicardoMalagonS/status/1202562250948694017

[Las donaciones de material de aseo y alimentos, también mostró un lado mucho más humano por parte de distintas personas de la ciudad, que se comprometieron a dar su apoyo a la guardia indígena y ayudaron con los medios disponibles a sustentar su estadía en la ciudad. ]

**Los recicladores se unen al Paro Nacional**

El 11 de diciembre varios recicladores marcharon por el centro de Bogotá. Dando su apoyo a la protesta social y respaldando las demandas que se exigen, los recicladores hicieron escudos con canecas de plástico y cantaron arengas **exigiendo al gobierno de Iván Duque respuestas ante las problemáticas sociales que aquejan al país.**

En el marco del Paro Nacional, han sido varios los sectores de trabajadores y organizaciones comunales que han sumado su voz a un gran grito que busca dejar en firme una posición de descontento antes las medidas provenientes del gobierno de Iván Duque. Junto a muchos, los recicladores se unieron con el fin de ser visibles dentro de la movilización social y apoyar a estudiantes y trabajadores que han venido movilizándose las ultimas semanas.

https://twitter.com/pior\_esnada/status/1204844079537905664

### **Abuelas que se enfrentan a la policía** 

[Dentro de las manifestaciones y plantones que se llevaron a cabo en la zona del Parkway, Teusaquillo, se presentaron inconvenientes entre el ESMAD y los manifestantes. Los miembros del Escuadrón, que llegaron a atacar con aturdidoras y gases a un grupo pequeño de jóvenes, fueron combatidos por varias abuelas y personas mayores, dueños de las casas y mujeres que salieron de sus hogares para proteger a los manifestantes.]

[**Abrazando a los jóvenes, las abuelas lograron impedir que los retuviera la fuerza pública**, que estaba usando la fuerza y la represión contra personas indefensas en el barrio durante los plantones que se habían programado para ese día. Junto a ellas, varios vecinos del sector se agruparon rechazando la actuación del ESMAD y apoyando el Paro Nacional.]

### **Aquí nos cuidamos entre todos y todas**

En el marco de las manifestaciones del Paro Nacional, se vivió un acto siniestro por parte de la Policía Nacional. Una mujer fue montada a un carro particular, retenida ilegalmente y transportada bajo condiciones inconstitucionales. Sucedió al frente de la Universidad Nacional y pudo quedar como un hecho aislado o en desconocimiento de el público en general.

Sin embargo, **un ciudadano preocupado tomó su carro particular y siguió al vehículo, intentando calmar a la mujer que gritaba asustada e intentaba salir del auto como fuera.** Gracias a la intervención del hombre y el correcto seguimiento al auto donde estaba la mujer, ella pudo ser liberada. La oportuna acción de los que estaban allí permitió que aquel acto no pasara a mayores.

https://twitter.com/maikybayona/status/1204608752080609282

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
