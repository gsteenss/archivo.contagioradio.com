Title: 66% de los pueblos indígenas está a punto de desaparecer: ONIC
Date: 2018-07-09 16:08
Category: DDHH
Tags: Derecho Internacional Humanitario, Derechos Humanos, lideres sociales, ONIC, pueblos indígenas
Slug: pueblos-indigenas-a-punto-de-desaparecer-onic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/indigenas-extermnio-onic.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [9 Jul 2018] 

En rueda de prensa, la Organización Nacional Indígenas de Colombia (ONIC), presentó un informe sobre vulneraciones a los Derechos Humanos sufridas por los Pueblos Indígenas en el país. En dicho documento, la organización indica que durante 2017, fueron asesinados 39 líderes, lideresas y autoridades indígenas.

En lo que ha transcurrido de 2018, se reportaron 4.544 casos de violación a los Derechos Humanos; siendo el más reciente caso registrado el asesinato del comunero Fernando Velasco, del Municipio de Caloto, Cauca. (Le puede interesar: [ONIC denuncia asesinato de comunero en Caloto, Cauca](https://archivo.contagioradio.com/onic-denuncia-asesinato-de-comunero-en-caloto-cauca/))

### 10.599 casos de violación a los DDHH reportados desde la firma del acuerdo de paz 

\[caption id="attachment\_54610" align="aligncenter" width="428"\]![Informe ONIC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-09-a-las-3.25.08-p.m.-428x618.png){.size-medium .wp-image-54610 width="428" height="618"} Informe ONIC\[/caption\]

En el informe se alerta que de 102 pueblos indígenas que hoy subsisten en el país, 36 se encuentran en riesgo de extinción y 31 afrontan un proceso inminente de exterminio. Es decir que "un 66% de los pueblos indígenas está ad portas de la desaparición".

En el marco de estos riesgos, la ONIC señaló que entre noviembre de 2016 y el 5 de julio 2018 hubo 10.599 casos de violación a los Derechos Humanos e infracciones al Derecho Internacional Humanitario por situaciones como desplazamiento forzado, confinamiento y amenazas.

\[caption id="attachment\_54611" align="aligncenter" width="695"\]![Informe ONIC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-09-a-las-3.28.12-p.m.-695x564.png){.size-medium .wp-image-54611 width="695" height="564"} Informe ONIC\[/caption\]

En el informe se indica que entre los departamentos en los cuales se presentan mayor número de infracciones al Derecho internacional Humanitario, están Chocó (2253 casos), Cauca (1110), Valle del Cauca (447) y Antioquia (79).

Los actores señalados de cometer estos crímenes en contra de las comunidades indígenas son "el ELN, el EPL, las disidencias de las FARC-EP y las organizaciones herederas del paramilitarismo (Clan del Golfo, Águilas Negras, Los Rastrojos, Autodefensas Gaitanistas de Colombia); y la Fuerza Pública", como se presenta en el informe.

A pesar de los casos registrados, la ONIC señala que existe un sub registro por la ausencia de declaraciones de las víctimas, falencias institucionales y legales para la denuncia de las violaciones denunciadas, así como una definición arbitraria sobre lo que significan los hechos victimizantes.

Por estas razones, la ONIC concluyó haciendo un pedido a que se respete la vida, la cultura y los territorios indígenas, e hizo un llamado al diálogo con el nuevo gobierno para que se establezcan las medidas necesarias de protección sobre los pueblos indígenas de Colombia.

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
