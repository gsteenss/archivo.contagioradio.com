Title: Reflexiones ambientales en tiempos de cuarentena III Agua: ¿Escasez o desigualdad?
Date: 2020-05-19 14:48
Author: Censat
Category: CENSAT, Opinion
Tags: agua y vida, CENSAT, Censat agua Viva, covid19, cuarentena en colombia, derecho al agua
Slug: reflexiones-ambientales-en-tiempos-de-cuarentena-iii-agua-escasez-o-desigualdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Foto_Andrés-Camilo-Gómez-Giraldo.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Foto_Andrés-Camilo-Gómez-Giraldo-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por Andrés Camilo Gómez Giraldo. {#foto-por-andrés-camilo-gómez-giraldo. .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En medio de la crisis civilizatoria, profundizada por la expansión del COVID-19, el agua emerge, nuevamente, como un elemento fundamental para superarla dada su evidente relación con la salud tanto de las personas como de los territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todas las labores de cuidado, limpieza y alimentación dependen íntimamente de la calidad, cantidad, continuidad y libertad de este bien común, y a su vez, los ciclos hidrológicos son esenciales para la existencia y continuidad de todas las formas de vida. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este contexto, los recientes decretos en relación al acceso y uso del agua en Colombia, que privilegian el apoyo a las empresas y no la real garantía de agua para todas, han vuelto a poner sobre la mesa el debate acerca del Derecho Fundamental al agua, que comprendemos como el derecho de las personas acceder al agua, a la protección de los territorios esenciales para el ciclo hídrico, el derecho de las poblaciones de decidir sobre sus aguas y el de otros seres vivos a aprovecharlas. Igualmente, como ya lo ha subrayado la Corte Constitucional, este derecho fundamental se encuentra en conexidad con otros derechos fundamentales y es una condición *sine quo non* para la garantía de los mismos, en consonancia con la resolución 64/292 de Naciones Unidas, que en 2010 reconoció explícitamente el Derecho Humano al agua y al saneamiento.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### A pesar de que garantizar el acceso al agua es un deber del Estado colombiano, el virus ha dejado al descubierto la precarización del sistema de salud y de la prestación de los servicios públicos en nuestro país.

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con el Departamento Nacional de Estadísticas – DANE- de la totalidad de 14’243.223 hogares que viven en el territorio nacional, 1’836.571 no cuentan con servicio de acueducto. Así mismo, se evidencia la brecha respecto al acceso al agua en zonas rurales y urbanas, pues, de los hogares que no cuentan con servicio de acueducto, 1’179.727 están ubicados en zonas rurales. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, las medidas tomadas por el gobierno nacional en torno al agua para hacer frente a la emergencia sanitaria tienen en común la continuidad de una visión mercantilista, donde sistemáticamente se invisibilizan a los acueductos comunitarios como organizaciones que además de encargarse de garantizar el derecho al agua de las comunidades, son actores políticos históricos y parte fundamental de múltiples territorios, rurales y urbano populares en la construcción de su tejido social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre las primeras medidas, el decreto presidencial **411 de 2020** ordena que los prestadores del servicio público domiciliario de acueducto procedan a la re conexión del servicio cuando el mismo hubiese sido suspendido y/o cortado por falta de pago sin costo alguno. Sin embargo, esta disposición no aclara el procedimiento para las personas que no han contado con conexión a una red de agua, más allá de hablar de medios alternos de aprovisionamiento (carrotanques, tanques provisionales de almacenamiento de agua) que en algunos casos nunca han sido utilizados por las comunidades o que por décadas han sido insuficientes, y en otros casos han sido empleados sólo como una estrategia de empresas extractivas para lograr una licencia social, reducir impuestos o limpiar su imagen. Esta es la situación en la que se encuentran muchos asentamientos informales, además de poblaciones vulnerables como habitantes de calle, migrantes y muchas comunidades rurales lejanas que no recibirán el beneficio de re conexión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de lo mencionado, el decreto **465 de 2020** permite a los municipios donde se ubican los distritos de riego de Ranchería (La Guajira), del Triángulo del Tolima (Tolima) y Tesalia-Paicol (Huila) aprovechar el agua almacenada allí para garantizar medios de vida a las comunidades aledañas. Sin embargo, ¿Cómo hacían todas estas personas, familias y comunidades para sobrevivir antes de las tan publicitadas decisiones?, ¿por qué, sabiendo que algunas comunidades no tienen la posibilidad del acceso al agua, es apenas ahora que se abre la posibilidad de servirse de los distritos de riego y de otras opciones de distribución?, ¿Por qué se toman decisiones apenas paliativas y no se emprenden inversiones inmediatas para garantizar el acceso permanente al agua?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Le puede interesar: Guajira sin agua por desvío de arroyo Bruno y sin ayudas para enfrentar al COVID](https://archivo.contagioradio.com/guajira-sin-agua-por-desvio-de-arroyo-bruno-y-sin-ayudas-para-enfrentar-al-covid/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, el gobierno nacional ha dictado otras disposiciones para financiar a las empresas prestadoras de servicios de agua y, de esta manera, asegurar los recursos necesarios para su funcionamiento frente a la probable imposibilidad de pago de los usuarios. Estos mandatos incluyen financiamiento para un pago diferido de las facturas por un lapso de treinta y seis (36) meses a través de créditos con Instituciones Financieras Internacionales (Decreto 528 del 2020), ampliamente conocidas por su papel en la privatización del agua en el país, y el otorgamiento de subsidios, o la asunción parcial o total del costo de los servicios de agua (Decreto 580 del 2020) por las entidades territoriales bajo ciertas condiciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
En este contexto, es necesario preguntarnos:

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿qué sucede con la gestión realizada por los acueductos comunitarios en el territorio nacional?

<!-- /wp:heading -->

<!-- wp:list -->

-   En primer lugar, cabría resaltar que las anteriores medidas de financiación ni siquiera les mencionan y, por el contrario, tienen un enfoque que privilegia empresas privadas para prestación del servicio en ciudades y, en algunos casos, cascos urbanos.

<!-- /wp:list -->

<!-- wp:list -->

-   En segundo lugar, cuando son mencionados, por ejemplo en el decreto **411**, el gobierno pone una carga desmedida sobre los acueductos comunitarios, erróneamente denominados desde la institucionalidad como esquemas diferenciales, ya que al ser prestadores del servicio público de acueducto, estos no sólo tendrían que asumir la reconexión de los usuarios, sino que hasta el momento no tienen ninguna ruta de financiación para enfrentar las dificultades económicas que van a tener sus vecinos, apoyo que tendría que ser asumido por las mismas instituciones del Estado y de ninguna manera a través de créditos.

<!-- /wp:list -->

<!-- wp:list -->

-   En tercer lugar, los acueductos comunitarios siguen enfrentando los requerimientos y exigencias técnicas, organizativas y burocráticas de las autoridades para permanecer como prestadores de servicios públicos y como entidades sin ánimo de lucro.

<!-- /wp:list -->

<!-- wp:paragraph -->

Por último, y frente a la evidente importancia del agua de calidad y cantidad suficiente para la salud pública de todos los colombianos, no podemos olvidar que durante este aislamiento obligatorio las comunidades organizadas en la gestión del agua han estado trabajando sin descanso para que sus territorios tengan agua para suplir sus necesidades familiares y colectivas. Así, hacemos un llamado a apoyar la labor de los acueductos comunitarios y promover la participación efectiva y justa de los procesos para la toma de decisiones en la búsqueda de una soberanía hídrica de los pueblos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

***“Aguas libres, Territorios soberanos, Comunidades organizadas”  
Consigna Confluencia Regional por la vida del agua de Nariño***.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Ver mas: Reflexiones ambientales en tiempos de cuarentena](https://archivo.contagioradio.com/reflexiones-ambientales-en-tiempos-de-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[CENSAT Agua Viva](https://censat.org/)

<!-- /wp:paragraph -->
