Title: En Colombia mueren 10.500 colmenas de abejas cada año
Date: 2017-07-17 13:21
Category: Ambiente, Voces de la Tierra
Tags: abejas, agroquímicos, colombia, extinción de abejas, medio ambiente, pesticidas
Slug: en-colombia-mueren-10-500-colmenas-de-abejas-cada-ano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/efecto-colmena-rescatando-abejas-cdmx-e1500315672825.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ecoosfera] 

###### [17 Jul 2017] 

La Universidad Nacional de Colombia, a través de una comunicación de la Agencia de Noticias UN, **alertó que en el país está disminuyendo el número de abejas debido al mal uso de productos agroquímicos**. Este fenómeno ha venido ocurriendo desde el 2006 en todo el mundo y se calcula que en Colombia 10.500 colmenas de abejas desaparecen cada año.

La experta apícola de la Universidad Nacional, María Guiomar Nates, manifestó que **Colombia se ha unido a la problemática que viven países como Italia, Estados Unidos, Francia, Suiza**, Alemania y Reino Unido. En estos países, las abejas polinizadoras, hacen contacto con productos químicos de la familia de los neonicotinoides que son insecticidas que actúan en el sistema nervioso central de los insectos. (Le puede interesar: ["¿Qué le pasa al planeta si se extinguen las abejas"](https://archivo.contagioradio.com/que-pasa-si-se-extinguen-las-abejas/))

Según la experta **“el efecto que producen estos químicos en las abejas es neurotóxico, por eso presentan desorientación**, su capacidad retentiva disminuye y se vuelven muy susceptibles a adquirir enfermedades, síntomas que hacen que se vuelvan muy débiles”. Esta reducción en el número de abejas, según ella, afectaría la producción de alimentos que consume la humanidad por el rol polinizador que ejercen las abejas.

La comunidad apicola de Guasca en Cundinamarca, por ejemplo, ha manifestado que las fumigaciones con productos tóxicos a los cultivos de papa **acaban con la vida de las colmenas de abejas**. Esta situación se repite en municipios como Guatavita, Facatativá, Nemocón y Tocancipá. (Le puede interesar: ["Agrotóxicos están acabando con los cultivos de abejas en Páramo Grande"](https://archivo.contagioradio.com/agrotoxicos-estan-acabando-con-los-cultivos-de-abejas-en-paramo-grande/))

La Universidad manifestó que, al problema con los químicos se suma que las abejas pueden estar desapareciendo por “el calentamiento global, la deforestación, la introducción de especies no nativas, la urbanización y los cultivos a gran escala como la palma de cera”.

### **La importancia de las abejas en el ecosistema** 

Sin estos insectos, **algunos granos, frutas, hortalizas, verduras y hectáreas de bosque desaparecerían**. Igualmente, la economía cambiaría “porque el precio de los alimentos tendría un alza sin precedentes y se modifica la calidad de vida de los humanos”. La Universidad Nacional hizo énfasis en que “ya se están contratando personas para que actúen como polinizadores en los cultivos porque no existen suficientes abejas para cumplir con esa función”.

En Colombia, según esta institución, **hay más de 600 especies de abejas conocidas pero podrían ser más.** La experta apícola manifestó que “es necesario que se haga un trabajo urgente para ver el comportamiento de sus colonias”. Igualmente, manifestó la Universidad que “hasta el 2014 en Colombia se habían prohibido 195 tipos de pesticidas, pero todavía están siendo usados indiscriminadamente”. Nates afirmó que puede haber leyes, pero no hay quién las controle o las vigile.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
