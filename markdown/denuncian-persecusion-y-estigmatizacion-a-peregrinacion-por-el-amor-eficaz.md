Title: Denuncian persecución y saboteos a peregrinación por el amor eficaz
Date: 2016-02-14 11:31
Category: Paz
Tags: camilo torres, Estigmatización movimiento social en Colombia, peregrinación por el amor eficaz
Slug: denuncian-persecusion-y-estigmatizacion-a-peregrinacion-por-el-amor-eficaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Imagen1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [14 Feb 2016] 

Las organizaciones y líderes que hacen parte de la "Peregrinación por el amor eficaz" hicieron un llamado al gobierno colombiano para brinde las garantías necesarias para la realización de las actividades que se realizan desde el pasado viernes en el marco de los 50 años de la muerte del sacerdote Camilo Torres Restrepo.

A través de un comunicado emitido por representantes que participan en los eventos conmemorativos, se denuncia la continua persecución e intentos de saboteo, antes y durante su inicio, entre los que se cuentan cortes de energía, detención y registro a los autobuses y participantes, amenazas de obstrucción por parte de civiles armados y el derribamiento de árboles para bloquear el paso de los vehículos.

Adicionalmente, los firmantes aseguran que dirigentes políticos de la región y periodistas siguen promoviendo la estigmatización de la peregrinación, asociándolas directamente con actividades organizadas por la guerrilla del Ejército de Liberación Nacional.

A continuación compartimos el texto del comunicado completo.

##### sábado, 13 de febrero de 2016 

### GOBIERNO COLOMBIANO DEBE GARANTIZAR PEREGRINACIÓN POR EL AMOR EFICAZ 

A pesar de la estigmatización que ha caído sobre la iniciativa de distintas organizaciones sociales, ecuménicas, de derechos humanos y personalidades de la academia y la vida política de conmemorar los 50 años de la muerte de Camilo Torres Restrepo, en varias ciudades del país, se han realizado actividades en diversos escenarios, en las que se hace viva la memoria de este sacerdote, sociólogo y luchador social que dio su vida por los pobres.

Más de mil personas de todas las latitudes del país, se dieron cita en Barrancabermeja el día 13 de febrero para sumarse en un acto de memoria que tuvo lugar en la sede del Club Infantes; actividad que culminó con éxito, pese a que ha pretendido ser saboteada a través de actos como la sorpresiva negativa del gerente de la refinería de Ecopetrol de esta ciudad, Donaldo Díaz a autorizar el uso del auditorio y del Club Miramar argumentando que ahí se alojarían guerrilleros y finalmente con la suspensión del fluido eléctrico en el sitio del evento.

Los buses en que se transportaban las delegaciones que concurrieron, fueron en su mayoría requisados minuciosamente por la policía de carreteras, apostados en distintos puntos de las vías que conducen a Barrancabermeja. Los vehículos y las personas fueron fotografiados y filmados por los agentes, se registraron los datos personales de los conductores, tales como nombre, número de celular, entre otros, a quienes además les solicitaron las planillas e información sobre el lugar de procedencia y destino de la ruta. Ningún otro bus o vehículo particular, fue registrado en estos retenes policiales.

En días anteriores, el concejal del Carmen de Chucurí, Nelson Horacio Álvarez quien públicamente en desarrollo de lo que denominaron un consejo de seguridad realizado el domingo 7 de febrero, se declaró a favor de las “autodefensas campesinas”, indicó que tras la peregrinación por el amor eficaz y la construcción de un monumento a la memoria del sacerdote Camilo Torres, se encontraba el Ejército de Liberación Nacional – ELN y que permitir ello implicaría el retorno de la organización insurgente a la región. En esta sesión intervino igualmente el comandante del Batallón Luciano D´elhuyar haciendo eco a tales señalamientos contra los organizadores del acto de conmemoración.

Se tiene conocimiento que la peregrinación será obstruida con la presencia de civiles armados de San Juan Bosco  de la Verde y otros paramilitares de la región, quienes vienen desarrollando una campaña de relacionamiento de esta actividad con el Ejército de Liberación Nacional, a través de la repartición de volantes y afiches en los que se promueve una marcha de oposición.

Simultáneamente, a través de algunos medios de comunicación se publican columnas de opinión, entre estas la del periodista Ricardo Puentes Melo, quien difama no solo contra la peregrinación por el amor eficaz sino también de reconocidos líderes sociales y personalidades como el Sacerdote jesuita Javier Giraldo Moreno y Hernando Hernández Pardo y organizaciones como la Unión Sindical Obrera, la Comisión Intereclesial de Justicia y paz, entre otras.

Estos hechos demuestran el fortalecimiento y control social que tiene el paramilitarismo en varias regiones del País. Exigimos del Gobierno Nacional las garantías necesarias para que cese la estigmatización, persecución, y se impida cualquier acto de saboteo o provocación contra las personas que de manera pacífica pretenden realizar un ejercicio de memoria histórica.

##### Barrancabermeja, 13 de febrero de 2016 

<div style="text-align: left;">

</div>

<div style="text-align: left;">

**Representante a la Cámara por Bogotá Angela María Robledo  
Representante a la Cámara por Antioquia Víctor Correa Vélez  
Representante a la cámara por Bogotá Alirio Uribe Muñoz  
Senador Alberto Castilla Salazar  
Senador Ivan Cepeda Castro  
Poder y Unidad Popular - PUP  
Congreso de los Pueblos - CDP  
Equipo Jurídico Pueblos - EJP  
Organización Femenina Popular - OFP  
Unión Sindical Obrera - Junta Directiva Nacional  
Fundación Javier Alberto Barriga - FUNJAB  
Corporación Social para la Asesoría y Capacitación Comunitaria - COSPACC  
Instituto Nacional Sindical - CEDINS  
Acción Libertaria - AL  
Corporación Sembrar  
Congreso Ambiental de Santander - CAS  
Comité de Integración social del Catatumbo - CISCA  
Movimiento de Trabajadorxs, Campesinxs y Comunidades del Cesar - MTCC  
Asociación Colombiana de Abogados Defensores de Derechos Humanos - ACADEHUM -  
Central Unitaria de trabajadores - Subdirectiva Tolima  
Fundación Comité de solidaridad con los presos políticos - FCSPP  
Sindicato Mixto de trabajadores de las universidades públicas nacionales - SINTRAUNAL  
Red de Hermandad y solidaridad con Colombia - REDHER  
Confluencia Nacional de Mujeres para la Acción Pública  
Organizaciones Sociales de Centro Oriente  
Federación Agrominera del Sur del Bolívar - FEDEAGROMISBOL  
Federación Unitaria de Trabajadores mineros, energeticos - FUNTRAENERGETICA  
Comisión de Derechos Humanos y Paz - USO  
**

</div>

<div style="text-align: left;">

**Comisión Intereclesial de Justicia y Paz  
**

</div>

 
