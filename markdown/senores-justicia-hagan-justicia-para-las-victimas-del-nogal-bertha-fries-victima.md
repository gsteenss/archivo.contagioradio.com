Title: “Señores Justicia, hagan Justicia para las víctimas de El Nogal”: Bertha Fries
Date: 2018-02-07 14:45
Category: DDHH, Entrevistas
Tags: Betha Fries, Club el nogal, das, FARC
Slug: senores-justicia-hagan-justicia-para-las-victimas-del-nogal-bertha-fries-victima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/espectado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [07 Feb 2018] 

A 15 años del atentado en el Club El Nogal, Bertha Fries, víctima de este hecho, insistió en los puntos claves para el esclarecimiento alrededor de lo sucedido. En donde según ella, no solo estuvieron involucrados integrantes de la **entonces guerrilla de las FARC, sino también paramilitares y miembros de la Casa de Nariño**.

“¿A quién le dirigían ese atentado y por qué? Esa sería específicamente para la FARC, la otra pregunta es ¿señores paramilitares ustedes estaban adentro?, ¿Ustedes dormían allá, dormía Mancuso?, otra pregunta …¿señores que hicieron todo el trámite para aceptar al Freddy Arellan para entrar al Club, por qué no revisaron a quién le estaban dando la membrecía?, ¿Por qué no nos dieron la seguridad equivalente a la Casa de Nariño, si acá mantenían ministros? Y por último ¿Quiénes le abrieron la puerta a los paramilitares?”.

Esas son las preguntas que Bertha Fries lleva intentado responder hace 15 años, pero lo único que ha encontrado es la falta de incompetencia del Estado para dar esclarecer las causas del atentado. “Los de la justicia nos tiraron la puerta en las narices”, afirma Fries, quien agrega que pese a que se dieron todas las pruebas tales como huellas, celulares y detalles, el Tribunal Superior de Cundinamarca decidió no usarlas, **debido a que demostraba el previo conocimiento que tenían autoridades como el DAS**, sobre el atentado.

**La impunidad sobre las víctimas del Club El Nogal**

Bertha Fries afirma que el proceso para encontrar la justicia y la verdad, en este caso va para atrás, “esperamos que no tengamos que pasar otros 15 años y estemos en las mismas”. Asimismo, indica que el Acuerdo de paz de La Habana decía que las víctimas estarían en el centro del proceso, **pero pareciera  que se tratara de "el centro de la soledad”**.

De igual forma aseveró que no entiende cuál es la complicación que hay en el proceso con las pruebas que existen y el llamado a que testifiquen los integrantes de la FARC, que ayudarían a dar luces sobre los demás responsables de este hecho. (Le puede interesar: ["FARC contará la verdad sobre atentado del Nogal y pedirá perdón"](https://archivo.contagioradio.com/38897/))

“En este proceso hemos aprendido a ser abogados de nosotros mismos. Éramos anónimos y hoy estamos presentes en la historia nacional, porque no hemos querido que este caso quede escondido y porque **nos hemos dedicado a denunciar y a buscar la verdad**”, cuenta Fries.

Finalmente destacó la situación que han tenido que vivir los funcionarios públicos que participado en el caso: una de las fiscales fue asesinada, dos se encuentran exiliadas y no se tiene mucha información sobre el estado de aquellas personas que fueron informantes antes del atentado.

“Sin saber en qué parte del mundo están, yo hago un homenaje a estas personas que intentaron evitar que el acto se llevara a cabo, **que tuvieron en las manos la posibilidad de darle las pruebas a un Estado que no hizo nada**”, concluye.

### **La reparación por partes** 

El carro bomba puesto en el Club El Nogal, fue uno de las acciones cometidas por la entonces guerrilla FARC-EP. **En este hecho murieron 36 personas, mientras que más de 200 resultaron heridas, entre ellas Bertha Fries**.

No obstante, durante los diálogos de paz en La Habana, las víctimas del atentando acudieron a este lugar para verse cara a cara con los diregentes de la FARC, encuentro que tuvo como resultado el acto de pedido de perdón realizado por parte de la guerrilla y la construcción de una ruta de trabajo que lleve a la reparación y la revelación de las respuestas de algunas de las preguntas que tienen las víctimas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
