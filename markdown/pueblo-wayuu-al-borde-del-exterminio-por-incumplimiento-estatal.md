Title: Pueblo Wayúu a punto de desaparecer por incumplimiento estatal
Date: 2018-04-04 20:00
Category: DDHH, Nacional
Tags: Guajira, indígenas, Pueblo Wayúu
Slug: pueblo-wayuu-al-borde-del-exterminio-por-incumplimiento-estatal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/muerte-de-ninos-wayuu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [04 Abril 2018] 

La falta de atención y cumplimiento del Estado a las diferentes sentencias que han ordenado la protección del pueblo Wayúu, en La Guajira, podría llevar a que este sea exterminado por completo. Así lo afirma Carolina Sáchica, abogada acompañante de procesos a esta comunidad, que además agregó **que no solo es la primera infancia la que muere por desnutrición en esa región del país, sino también las mujeres y los adultos mayores**.

Para Sáchica este hecho no es producto solamente de la falta de acciones por parte del Instituto Colombiano de Bienestar Familiar, sino de un olvido generalizado de todo el aparato estatal que no ha garantizado los derechos de esta comunidad históricamente.“ E**ste es un tema multicausal**, yo no creo que se le pueda atribuir a una sola situación, causa o entidad, sino que corresponde a diferentes entidades”.

En ese sentido afirmó que el Ministerio de Salud, las gobernaciones, la alcaldía departamental, el Ministerio de Educación, entre otros, son responsables de la violación a los derechos humanos de las comunidades Wuayúu, y señaló que la falta de articulación en políticas y medidas es lo que ha profundizado la situación, debido a que “nadie se compromete ni asume una responsabilidad”.

Además, manifestó que las soluciones a estas problemáticas no pueden ser momentáneas ni aisladas, sino que deben ser continuas, y deben estar pensadas a largo plazo, para lograr restituir los derechos del Pueblo Wayúu. (Le puede interesar: ["Investigación periodística demostró que Postobón experimentó con niños Wuayúu"](https://archivo.contagioradio.com/investigacion-periodistica-revela-que-postobon-experimento-con-ninos-wayuu/))

### **Las medidas que amparan al pueblo Wayúu** 

La Corte Interamericana de Derechos Humanos ya profirió medidas cautelares para amparar a los niños, niñas y adolescentes, pertenecientes a la comunidad Wayúu. Posteriormente, esta medida fue ampliada para garantizar la protección de las madres gestantes y lactantes. El 1 de diciembre de 2017, debido a la vulneración continúa a los derechos humanos, la CIDH, decidió extender estas medidas a la población perteneciente a la tercera edad.

Al mismo tiempo, ya hay fallos de altas cortes que también buscan la protección de la comunidad Wayúu, sin embargo, para Sáchica la acción final está en manos del gobierno, **“debe tomar medidas que realmente contrarresten la situación de alarmas y de crisis humanitaria”, concluye la abogada.**

<iframe id="audio_25115440" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25115440_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
