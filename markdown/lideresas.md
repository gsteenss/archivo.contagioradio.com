Title: LIDERESAS
Date: 2019-07-05 10:41
Author: CtgAdm
Slug: lideresas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/grito.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

  
EL PELIGRO DE SER LIDERESA  
EN COLOMBIA
===========================

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/logo-contagio-radio-web-1.png){width="201" height="76"}

Colombia es el país con mayor número de asesinatos contra líderes sociales en Latinoamérica, en el último año (2018), se cometieron 155 homicidios de los cuales el 9% fueron mujeres, cifra que desde hace 3 años viene aumentando pese al acuerdo de paz firmado entre la FARC y el gobierno colombiano. En este especial de Contagio Radio, le contaremos las dificultades que deben superar las lideresas en el país y las violencias de género a las que siguen expuestas.  

**Por: Sandra Gutierrez/Contagio Radio**

Junio 2019

<iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fel-peligro-de-ser-lideresa-en-colombia%2F&amp;layout=button_count&amp;size=small&amp;appId=1237871699583688&amp;width=91&amp;height=20" width="91" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)

En su artículo “[La dificultad de ser una lideresa social hoy en día en Colombia](http://www.indepaz.org.co/wp-content/uploads/2018/09/Arti%CC%81culo-Indepaz-lideresas-sociales-versio%CC%81n-final-WORD.pdf)” el Instituto de Ideas y Desarrollo para la paz señala que, “aunque el nivel de participación de las mujeres en ámbitos políticos y de liderazgo es muy bajo, las tasas de amenazas, violencia y asesinatos contra ellas son muy elevadas”. Según el más reciente informe de Somos defensores, de 155 asesinatos a  líderes sociales 9% fueron mujeres, porcentaje que es muy alto si se tiene en cuenta que tan solo el 12% de los cargos de elección popular son ocupados por ellas. 

“Por lo tanto, mientras en términos absolutos la mayor parte de víctimas son hombres, en términos relativos el porcentaje de lideresas asesinadas es mucho más alto”. Razón por la cual, en espacios de participación política, cuando asesinan a una mujer líder hay impactos más fuertes en la sociedad, debido a que ellas enfrentan mayores obstáculos y dificultades a la hora de acceder a puestos o lograr un rol de líderazgo al interior de sus comunidades.

Igualmente, el reciente informe del grupo de monitoreo para la implementación de la CEDAW en Colombia, y presentado ante ese organismo, señala que las lideresas sociales y defensoras de derechos humanos continúan siendo víctimas, “que enfrentan riesgos diferenciados y efectos desproporcionados, agudizados según los derechos que defienden, su orientación sexual e identidad de género diversa, su etnia, su ubicación territorial y, de manera común a todas ellas, su pertenencia a la población victimizada en la guerra”.

#### ¿Cuáles son las barreras para que las mujeres logren estar en roles de liderazgo y defensa de DD.HH.?

https://youtu.be/POWJCWkYX00

**Diana Salcedo- Limpal Colombia**

En enero de 2018 una lideresa o defensora de Derechos Humanos fue asesinada. Mientras que para enero de 2019 la frecuencia se incrementó a una defensora asesinada cada 10 días.

[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)

Comparativo de cifras:
----------------------

0

### Lideresas asesinadas por el rol político que desempeñaban

###### Del 2016 hasta el 2018

DEFENSORIA DEL PUEBLO

0

### Lideresas asesinadas por el rol político que desempeñaban

###### Del 2016 hasta el 2018

INDEPAZ Y MOVIMIENTO MARCHA PATRIÓTICA

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972.jpg?resize=370%2C247&ssl=1 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972.jpg?w=1200&ssl=1 1200w"}

El informe presentado a la CEDAW también advierte que el último periodo de tiempo, Colombia se ha destacado por la extrema violencia y sevicia en contra de las defensoras. Hecho que se ha evidenciado en las marcas encontradas en los cuerpos de algunas mujeres asesinadas, que dan cuenta de violencias sexuales y torturas.  

Asimismo, la Delegada para los derechos de las mujeres y los asuntos de género, junto con la Defensoría del Pueblo, acompañaron y asesoraron jurídicamente a 143 lideresas o defensoras de derechos humanos, entre enero de 2016 y diciembre de 2017. De ellas, 24 fueron registradas como víctimas de violencia sexual (16,8%), mientras que en 2018, con corte al 31 de octubre, de 61 defensoras acompañadas por las duplas de género, 45 reportaron ser víctimas de amenazas, 8 sufrieron algún tipo de ataque o atentado y 6 reportaron haber sido víctimas de violencia sexual (9,8%)

¿Por qué las asesinan?
======================

"Las mujeres defensoras de derechos humanos las agreden y las asesinan por una motivación doble: Por ser mujeres en una sociedad altamente patriarcal y por ser lideresas de derechos humanos". 

Carolina Mosquera, Sisma Mujer

https://youtu.be/\_w8L\_noqa1I

Carolina Mosquera - Sisma Mujer

Desde la firma de los Acuerdo de Paz, Colombia ha vivido una nueva ola de violencia producto de la pugna por el control territorial de las zonas antes ocupadas por las FARC, que ahora pretenden ser dominadas por grupos paramilitares, disidencias y guerrillas como el ELN o el EPL. Esta etapa del conflicto se ha caracterizado por agresiones directas a quienes desempeñan roles importantes al interior de las comunidades como presidentes de juntas de acción comunal, gobernadores indígenas, entre otros.

Frente a la situación de las mujeres, **la Defensoría del Pueblo emitió una alerta temprana en la que develó que “los casos en los que se registra violencia sexual previa al asesinato, incluyen actos de ensañamiento y tortura contra los cuerpos de las mujeres defensoras y líderesas”.**

De igual modo, manifestó que ese tipo de violencias castigan la participación de las mujeres en los ámbitos públicos, afectando tanto a ellas como a su organización, y buscan inhibir el surgimiento de sus nuevos liderazgos y procesos organizativos visibles.

En ese sentido, en el informe a la CEDAW, se advierte de la necesidad de “distinguir el carácter diferencial de las agresiones contra las mujeres lideresas y defensoras que obedecen a motivaciones sociopolíticas en razón de la defensa de los derechos humanos, en intersección con dimensiones de género asociadas a su condición de ser mujer en una sociedad que la discrimina por el hecho de serlo”.

Esto debido a que en **algunos de los asesinatos de lideresas se han presenciado agresiones que enmarcan una sevicia contra ellas, que podría estar dentro de las hipótesis investigativas de feminicidios**, hecho que actualmente no es tenido en cuenta por la Fiscalía General de la Nación y que ha impedido establecer cuántas lideresas y defensoras pudieron haber sido víctimas de estos crímenes.

[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)

####   
Tipos de violencia contra la mujer

### Amenazas  

Principal violencia que busca a través de la intimidación frenar el trabajo que adelantan las mujeres

### Judicialización  

"Durante 2018, el 60% de los procesos de judicialización arbitraria  
fueron en contra de mujeres" \*

### Homicidios  

En enero de 2018 una lideresa o defensora de Derechos Humanos fue asesinada aproximadamente cada 31 días, mientras que para enero de 2019 la frecuencia se incrementó a una cada 10 días.

### Violencia sexual  

"De enero a octubre de 2018 se presentaron 18 casos de agresión  
sexual en contextos de violencia sociopolítica, de los cuales el 89 % de  
las víctimas fueron mujeres" \*

###### \* Informe: **‘Lideresas Sociales en Colombia: el relato invisible de la crueldad’ CODHES Año: 2018** 

" Cuando tú eres líder tratan de marginarte para que no digas nada"  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/ana-del-carmen-275x195.png){width="275" height="195" sizes="(max-width: 275px) 100vw, 275px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/ana-del-carmen.png?resize=275%2C195&ssl=1 275w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/ana-del-carmen.png?resize=110%2C78&ssl=1 110w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/ana-del-carmen.png?resize=216%2C152&ssl=1 216w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/ana-del-carmen.png?resize=240%2C170&ssl=1 240w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/ana-del-carmen.png?resize=340%2C240&ssl=1 340w"}  
Ana del Carmen Martínez  
Lideresa, Cacarica, chocó  
https://youtu.be/MLK5kXWyAfc

**Ana del Carmen Martínez **hace parte del tejido de líderes de las Comunidades de Autodereminación, Vida y Dignidad (CAVIDA), en Cacarica, Chocó, una apuesta territorial de construcción de paz que surgió tras el desplazamiento forzado provocado por la Operación Génesis en 1997, del que tanto ella como otras familias son víctimas.

Desde entonces, **Ana del Carmen** ha defendido la vida y el territorio, y junto con su comunidad han denunciado la presencia de actores armados que rondan constantemente.

Al mismo tiempo, ha tenido que afrontar los múltiples obstáculos que deben sortear las mujeres que deciden asumir roles de representación política en Colombia, como son los estereotipos de género, las violencias en contra de las mujeres y el riesgo de ser asesinadas por el papel que desempeñan. 

Recomendaciones al Estado para proteger las lideresas
-----------------------------------------------------

Dentro de las recomendaciones que hacen los informes al Estado colombiano para garantizar la protección a las vidas de las lideresas sociales y defensoras de derechos humanos, se encuentran:  
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#### Fiscalía General

1.  A la Fiscalía General de la Nación que tenga en cuenta en sus investigaciones el enfoque diferencial de género.
2.  Consolidar un registro diferencial de los casos de las defensoras de derechos humanos, según delito, estado de las investigaciones, autoría presunta y patrones de discriminación usados por los victimarios.
3.  Garantizar la representación judicial de las defensoras de derechos humanos víctimas de las diferentes agresiones.

#### Gobierno

1.  Implementar el Programa de Protección Diferenciado para lideresas y defensoras, con el objetivo de garantizar el ejercicio político que realizan las mujeres, salvaguardandolas de ser víctimas de violencias, discriminación o desigualdades, y respondiendo a los riesgos diferenciados y efectos desproporcionados que recaen sobre ellas y que se extienden a sus familias y organizaciones.

2.   Articular las nuevas políticas de protección para personas defensoras con el programa integral de garantías para defensoras de derechos humanos y los demás instrumentos relacionados.

Este especial se construyó a partir de las cifras del informe más reciente de organizaciones defensoras de derechos humanos presentado a la CEDAW en marzo del presente año,  el artículo “La dificultad de ser una lideresa social hoy en día en Colombia” del Instituto de Ideas y Desarrollo para la paz y el más reciente informe de la organización Somos Defensores. Cabe resaltar que en lo corrido de este año también han sido asesinadas mujeres líderes o defensoras de derechos humanos, el caso más reciente es el de Idaly Ortega, la presidenta de la Junta de Acción Comunal del municipio de Vijes en Valle del Cauca.

Compartir:

<iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.contagioradio.com%2Fel-peligro-de-ser-lideresa-en-colombia%2F&amp;layout=button_count&amp;size=small&amp;appId=1237871699583688&amp;width=91&amp;height=20" width="91" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
[Tweet](https://twitter.com/share?ref_src=twsrc%5Etfw)

“Así nos toque con nuestra vida, **vamos a garantizar que nuestros hijos, que nuestras hijas, puedan estar tranquilos en nuestros territorios**. Ese fue el legado de nuestros ancestros, eso fue lo que hicieron cuando se liberaron de las cadenas y eso es lo que nosotras vamos a hacer”.

**Francia Marquez, lideresa**
