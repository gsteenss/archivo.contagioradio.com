Title: En Mercaderes, Cauca, también quieren proteger la vida por encima de la Minería
Date: 2019-07-10 17:36
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Cauca, consulta popular, Mercaderes, Mineria
Slug: en-mercaderes-cauca-tambien-quieren-proteger-la-vida-por-encima-de-la-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/minería.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Panoramio] 

Al sur del departamento del Cauca se encuentra ubicado el municipio de Mercaderes, rico en piedras preciosas, metales e hidrocarburos, cuya extracción genera preocupación por los daños que causa en el territorio, y por ello los habitantes de este municipio quieren realizar una consulta popular autónoma en defensa del agua y de la vida el próximo 3 de agosto.

Ulber Castillo, miembro del Comité de Integración del Macizo Colombiano, explica  que “la motivación de realizar una consulta popular nace del problema ambiental que generó la desastrosa desaparición del río San Bingo, además  del impacto en otros afluentes y la extinción de 360 hectáreas de bosque nativo”.

El Río San Bingo fue una de las primeras víctimas de la minería en ese municipio, a  causa del vertimiento de mercurio y cianuro, empleados en la extracción minera, provocando la desaparición de fauna y flora. (Le puede interesar ["Mercaderes Cauca le dice no a la minería"](https://archivo.contagioradio.com/mercaderes-cauca-le-dice-no-a-la-mineria/))

Además con la exploración minera en este territorio no solo se afectan las fuentes hídricas de la región, también la vida de  especies como las abejas. Castillo manifiesta que en su oficio de apicultor y ha perdido panales, tan solo en la etapa de estudios sísmicos,  esto es un claro ejemplo de los impactos ambientales que provoca la actividad minera.

### **Lo que se espera de la consulta popular** 

De 14000 personas habilitadas para votar, se espera que 5.000 participen de esta consulta popular autónoma que busca  generar conciencia de la importancia del agua para la vida y que la comunidad defienda su territorio y sus costumbres agrícolas, artesanales y orfebres.

En caso de que la consulta tenga un resultado favorable a la prohibición de la minería a gran escala, se tendría que actualizar el esquema de ordenamiento territorial para que sea una herramienta legal y así determinar el uso adecuado de los suelos y la defensa del agua. (Le puede interesar ["¿Por qué los municipios del suroeste de Antioquia rechazan la minería?"](https://archivo.contagioradio.com/suroeste-antioquia-rechaza-mineria/))

Para los pobladores de Mercaderes la herramienta para preservar la vida y el agua es la conciencia y que los habitantes  valoren la importancia que tienen los abastecimientos hídricos y lo fundamental que son para la vida, “nosotros no estamos en contra de ninguna persona ni de las actividades, pero no vamos a permitir que afecten el agua”.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_38274330" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38274330_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
