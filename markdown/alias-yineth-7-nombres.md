Title: Alias Yineth" 7 nombres, 7 rostros de una misma mujer
Date: 2018-08-14 09:32
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine Colombiano, Festival de cine por los DDHH
Slug: alias-yineth-7-nombres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/La-mujer-de-los-7-nombres’.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Rhayuela 

###### 13 Ago 2018

Cuando **Daniela Castro** trabajó como asistente de dirección de "Alias María" conoció a Yineth, sin saber que la mujer de la que escuchaba su historia de vida se convertiría en la protagonista de su documental. Una historia de injusticia, violencia, impotencia y esperanza a través de un camino iluminado por la sonrisa de la protagonista.

Fué asi como en compañía de **Nicolás Ordoñez**, iniciaron el proceso de lo que hoy es "Alias Yineth, la mujer de los 7 nombres" un relato conmovedor de una mujer confrontada con su pasado, que dió apertura a la 5 edición del [Festival Internacional de cine por los Derechos Humanos](https://archivo.contagioradio.com/5-festival-cine-derechos-humanos/).

En cada uno de los siete nombres de Yineth se encuentra un ser diferente, con una manera diferente de relacionarse con el mundo, una reinvención. Todas las caras de Yineth no solo se encuentran en un mismo cuerpo, sino que coinciden en que todas huyen de un pasado que siempre las vuelve a encontrar, un pasado del cual ella es adicta.

Cada vestido que usa Yineth es el común denominador: ella y su presente, aun así, uno de sus últimos atuendos nos muestran a una mujer completamente transformada en guerrillera. Así se configura la intensa y desgarradora historia de una mujer que fue reclutada a los 12 años por las FARC y que después de cinco años decide escapar a Bogotá arriesgándose a ser recapturada, pero que una vez alejada de un panorama hostil comienza a proyectar su vida hacia adelante, en busca de reincorporarse a la vida civil.

"Me dieron la oportunidad de vivir. Y eso es lo que intento cada día", dice Yineth, La mujer de los 7 nombres. En esta pulsión sobrepasa caminos dolorosos y, gracias a su fortaleza, se reinventa para construir un mejor futuro.

La historia de Yineth es punto de identificación para muchas otras mujeres – latinoamericanas y del mundo –, quienes viven en un contexto trastornado e invisible: madres cabeza de familia, trabajadoras exitosas, soldados, guerreras, campesinas y sobre todo sobrevivientes… Es una historia que dejará a los asistentes en una reflexión de cómo los seres humanos afrontan la vida.

<iframe src="https://www.youtube.com/embed/SyFBC6YS-cs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Daniela estudió Iluminación y Cámara en la Universidad del Cine de Buenos Aires. Ha escrito y trabajado en el área de fotografía y dirección en varias películas de ficción y documentales producidos en Colombia, Argentina e Italia. Fue asistente de dirección de “Alias María”, película que representó a Colombia en “Una cierta mirada” del Festival de Cannes 2015. Actualmente se encuentra escribiendo su primer largometraje ficción.

Nicolás, Cineasta, fotógrafo y escritor colombiano. Fue Director Creativo de la Escuela de San Antonio de los Baños. Cofundador de Galaxia 311, ha producido, fotografiado y escrito películas que se han estrenado en Rotterdam, Toronto o Cannes. Su más reciente producción, La defensa del dragón, tuvo su premier en la Quinzaine des Réalisateurs, Cannes, 2017. Recientemente dirigió el documental Ventana de tiempo, filmado en Antártica y actualmente en postproducción.
