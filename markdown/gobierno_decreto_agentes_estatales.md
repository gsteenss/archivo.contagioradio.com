Title: Decreto dejaría en libertad a agentes estatales que han violado derechos humanos
Date: 2017-05-04 18:05
Category: DDHH, Nacional
Tags: JEP, miltares, proceso de paz
Slug: gobierno_decreto_agentes_estatales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### [4 May 2017] 

Nuevamente el presidente Juan Manuel Santos, evidencia su falta de voluntad política para refrendar lo que realmente fue firmado entre su gobierno y la guerrilla de las FARC, con respecto a los derechos de las víctimas. Así lo denuncian las organizaciones de víctimas, luego de que este miércoles, **el mandatario emitiera el decreto  Ley 706 de 2017 por medio del cual se cambian las condiciones del Sistema Integral de Verdad, Justicia, Reparación, para los agentes estatales que han cometido crímenes** en el marco del conflicto armado.

Para Sebastián Escobar integrante del Colectivo de Abogados José Alvear Restrepo (CCAJAR), es un decreto preocupante que no garantiza los derechos de las víctimas de crímenes de Estado. De acuerdo con el texto de la Ley expedida por el gobierno, **se busca suspender las órdenes de captura y las medidas de aseguramiento para agentes estatales** que han cometido violaciones a los derechos humanos.

“Vienen a adicionarse una serie de medidas como tratamiento diferenciado que en la práctica **muestra una falta de interés de que se investiguen los crímenes de Estado**”. Por ejemplo, según explica Escobar, “este decreto afecta el artículo 24 del acto legislativo 01 del 2017 que establece de un modo diferente la responsabilidad del superior jerárquico, en contravía de los estándares internacionales”.

Se trata de una propuesta del gobierno que sigue la línea sobre la cual se delegaba, para el Centro Nacional de Memoria Histórica, al ministerio de defensa como integrante de la junta directiva de dicha entidad. [(Le puede interesar: Inclusión del Mindefensa afectaría autonomía del Centro Nacional de Memoria)](https://archivo.contagioradio.com/?p=39346)

Y es que la preocupación de las víctimas no es para menos. Solo hace falta recordar las cifras del  informe ¡Basta Ya! del Centro de Memoria Histórica en el que se demuestra que **la violencia ha estado alimentada mayormente por la responsabilidad de la Fuerza Pública y otros agentes estatales.** Ese informe habla de delitos como la desaparición forzada, que cuenta con 45.944 víctimas directas, atribuidas a grupos paramilitares y agentes de la Fuerza Pública en el 84 % de los casos. Las ejecuciones extrajudiciales, que entre 2002 y 2010 dejaron 4.475 víctimas, son todas ellas responsabilidad de agentes estatales. Así mismo ocurre con los asesinatos selectivos en los que paramilitares y Fuerza Pública son responsables del 69% de los casos; o con 1.982 masacres, cuya responsabilidad en el 56 % de los casos es atribuida a grupos paramilitares en asocio con la Fuerza Pública, autoridades civiles, y civiles financiadores del conflicto. [(Le puede interesar: Falsos positivos nos pueden pasar a la JEP) ](https://archivo.contagioradio.com/?p=39346)

### ¿Qué sucede si deja en libertad a los agentes estatales? 

Frente a ese panorama, Escobar señala que el único tratamiento diferenciado que puede haber, debe hacerse sobre la base de que **el Estado cumple el papel de garante de derechos,** y por tanto,  requiere una mayor responsabilidad en la medida que son quienes deben respetar y garantizar los derechos  de la población. En esa medida, si son los agentes estatales quienes los incumplen, **se habla de un agravante mayor por violar esa posición.**

De acuerdo con el abogado, las medidas de restricción de la libertad son necesarias para proteger  a la sociedad, a las víctimas y al proceso de los casos. Pero al dar libertad a, en este caso, los agentes implicados en la criminalidad estatal, no existen elementos que permitan proteger a las víctimas. **Los avances en las investigación penal, podrían verse afectados ya que se pueden presentar amenazas, se verían torpedeadas las pruebas,** y justamente eso no lo tiene en cuenta el decreto expedido por Santos.

Asimismo, esta **Ley resulta ser un gran desestimulo para los  funcionarios de la Fiscalía General de la nación**, “Llama la atención porque estas medidas a lo que contribuyen es a que los fiscales en regiones que muy valientemente han investigado conductas de la criminalidad estatal, tengan un desestimulo porque es el mismo Fiscal General (Néstor Humberto Martínez) el que no valida esa actividad”, indica el integrante del CCAJAR.

### ¿Qué se puede hacer? 

Este decreto hace parte del componente normativo del Sistema Integral de Verdad, Justicia, Reparación, una vez quede en firme, la Corte Constitucional debe hacer el análisis de constitucionalidad, y una vez tenga el texto, **el alto tribunal lo traslada a la sociedad civil para que intervenga** “esa es la única oportunidad que tenemos para corregirlo”, concluye Sebastián Escobar.

[Decreto suspencion medidas](https://www.scribd.com/document/347360178/Decreto-suspencion-medidas#from_embed "View Decreto suspencion medidas on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_1299" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/347360178/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-decdpvuZFGjFEWkhsx51&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6574344023323615"></iframe><iframe id="audio_18508302" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18508302_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
