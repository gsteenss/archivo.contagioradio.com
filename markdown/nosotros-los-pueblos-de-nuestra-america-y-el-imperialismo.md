Title: “Nosotros, los pueblos de Nuestra América” y  el imperialismo
Date: 2015-04-14 16:29
Author: CtgAdm
Category: Cesar, Opinion
Tags: Argentina, Cuba, Cumbre de los Pueblos, EEUU declara a Venezuela como amenaza para la seguridad nacional, Estados Unidos, Venezuela, VII cumbre de las Américas Panamá
Slug: nosotros-los-pueblos-de-nuestra-america-y-el-imperialismo
Status: published

###### Foto: Juvenal Balán/Granma en www.cubadebate.cu 

#### Por [**[Cesar Torres del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/) **] {#por-cesar-torres-del-río .p1}

La reciente VII Cumbre de las Américas (Abril de 2015) en Panamá ha dado y dará de qué hablar;  fue la reunión de los Estados, de los gobiernos. En paralelo se realizó la Cumbre de los Pueblos, Sindical y de los Movimientos Sociales de Nuestra América (10 y 11 de abril); fue la expresión de los de abajo, de los mestizos.

“Nosotros, los pueblos …” proclamaron a América Latina y al Caribe como zona de Paz y libre de colonialismo; rechazaron las bases militares norteamericanas instaladas en la región; saludaron el regreso de los 5 héroes cubanos, condenaron el bloqueo contra Cuba y exigieron el cierre de la base de Guantánamo; apoyaron a la Revolución Bolivariana y la soberanía y autodeterminación de Puerto Rico, exigiendo la libertad de Oscar López Rivera; vieron positivo el proceso de negociaciones entre las FARC y el gobierno colombiano; manifestaron la solidaridad con Argentina en su lucha por recuperar las Islas Malvinas y respaldaron la aspiración boliviana de una salida al mar; pidieron el retiro de las tropas de ocupación de Haití, el regreso con vida de los 43 estudiantes  mexicanos y la equidad de género;  y convocaron a la defensa de los recursos naturales llamando a la unidad latinoamericana.

La Cumbre de los Estados  reflejó, a su manera y mediante los gobiernos, cierta disposición de cambios estructurales y coyunturales  de los pueblos, como se escuchó en los discursos de Ecuador, Argentina, Venezuela y Nicaragua, entre otros. Interesante de todos modos. Cuba y Venezuela, de un lado, y, de otro, Estados Unidos,  centraron la atención de los medios de comunicación. Como el imperialismo siempre sonríe mientras golpea con el puño de acero, recordemos que en 1933 comenzó a gobernar en Norteamérica el señor Franklin Delano Roosevelt, del partido demócrata (el mismo de Barak Obama). Por entonces había animadversión y descontento de los gobiernos continentales y caribeños por la intervención militar directa de los gringos  en El Salvador, Nicaragua, Haití y Cuba, su apoyo desenfrenado a los gobiernos militares y la injerencia político-diplomática en los asuntos internos y externos. Buscando modificar tal situación, Roosevelt y su gobierno diseñaron la política exterior hacia América Latina denominándola “Buen Vecino”,  estableciendo  que Estados Unidos no intervendría militarmente en los asuntos de latinoamericanos (mientras que, al mismo tiempo continuaría entrometiéndose en los demás asuntos); para que se viera que sus intenciones eran realistas Roosevelt afirmó que si él fuera latinoamericano, sería decididamente antimperialista …

En la Cumbre Obama manifestó idéntica postura.  “Los días en que nuestra agenda en este hemisferio a menudo suponía que Estados Unidos podía interferir con impunidad están en el pasado”. Mentira. Los acuerdos bilaterales sobre inmunidad (impunidad) militar a los soldados norteamericanos firmados con América Latina y con Colombia, que eluden la acción de la Corte Penal Internacional, demuestran que la interferencia (intervención) continuará. Los acuerdos que se firman con el Fondo Monetario Internacional son otro botón de muestra.

Pero eso es sabido por los pueblos. Lo que preocupa, y sobremanera, es que el discurso de Raúl Castro haya depositado una confianza política en la postura norteamericana; el “Obama no es culpable”, “no tiene deuda con nosotros”, no es un error, es un convencimiento político. El imperialismo sigue actuando; el Estado de Excepción mundial impuesto por George Bush hijo y sostenido por Obama niega las garantías civiles y los derechos humanos a los pueblos mestizos. Con todo, el discurso cubano fue al mismo una denuncia contra el imperialismo y eso hay que resaltarlo.

Los pueblos de Nuestra América realizaron su Cumbre. Los Estados de las Américas hicieron la suya. No se pueden mezclar.
