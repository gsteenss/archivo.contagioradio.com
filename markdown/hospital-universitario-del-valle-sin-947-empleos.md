Title: Hospital Universitario del Valle se queda sin 947 puestos de trabajo
Date: 2016-10-27 17:32
Category: Movilización, Nacional
Slug: hospital-universitario-del-valle-sin-947-empleos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/cable-noticias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cable Noticias] 

###### [27 de Oct 2016]

La gobernadora de Cali, Dilian Francisca Toro, anunció que se **eliminarán 547 puestos de trabajo del Hospital Universitario del Valle del Cauca**, motivo por el cual los miembros del sindicato del Hospital se han declarado en asamblea permanente por masacre laboral y denuncian que las notificaciones de despido se están haciendo de forma irregular vía mensaje de texto.

Anteriormente la gobernadora ya **había anulado la contratación de 400 vacantes**, **que sumadas los 547 puestos que no serán renovados, dejan un saldo de 947 empleos clausurados** para uno de los Hospitales más importantes del sur del país.

De acuerdo con Jorge Rodríguez presidente de Sintrahospiclínicas, los despidos masivos se deben a un intento por privatizar el Hospital Universitario del Valle y a **la intención de la gobernadora de transformar el Hospital público en una fundación privada**. Sin embargo la gobernadora afirma que es necesario eliminar los empleos de la nómina para la crisis que afronta el Hospital.

Desde enero de este año Sintrahospiclínicas había informado de la posibilidad de que estos despidos se dieran debido a que el Hospital del Valle se acogió a la ley 550.  Jorge Rodríguez dice que **este es el preámbulo de la liquidación del Hospital** y que parte de ellos son no solamente los despidos sino también la clausura de algunas camas hospitalarias, la baja operatividad y la cancelación de residencias para estudiantes de medicina de la Universidad del Valle. Le puede interesar: ["Renuncian 38 médicos del Hospital Universitario del Valle"](https://archivo.contagioradio.com/renuncian-38-medicosdel-hospital-universitario-del-valle/)

Los cargos que desaparecerán hacen parte de áreas como mantenimiento, lavandería, costurero, vigilancia, entre otras. **Durante los próximos días los empleados se mantendrán en asamblea y movilización permanente**. Le puede interesar: ["Hospital Universitario del Valle no atenderá emergencias por falta de recursos](https://archivo.contagioradio.com/hospital-universitario-del-valle-no-atendera-urgencias-por-falta-de-recursos/)"

<iframe id="audio_13507819" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13507819_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
