Title: Enfrentamientos en Suarez ponen en peligro a más de 4 mil personas
Date: 2018-12-10 10:47
Author: AdminContagio
Category: DDHH, Nacional
Tags: Cauca, Discidencias, Enfrentamientos, FARC, paz
Slug: enfrentamientos-suarez-peligro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Militaress-Boyaca-e1524691374383.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [10 Dic 2018] 

Habitantes de Suárez, Cauca, vienen alertando sobre las afectaciones que enfrentamientos armados han provocado, para más de mil personas, desplazamientos y situaciones de confinamiento en la zona. Según los pobladores, no es la primera vez que se presentan hostigamientos, y aunque llevaban 2 años sin escuchar el ruido de los fusiles, **el incumplimiento del Acuerdo de Paz ha significado un retorno de la violencia.**

**Enrique Wetio, gobernador indígena del Resguardo Indígena Cerro Tijeras en Suárez,** comentó que es la segunda ocasión en que se presentan enfrentamientos en la zona. Esta vez el intercambio inició el viernes entre el Ejército y un grupo que se auto identifica como parte de las FARC. El Gobernador recordó que hacía 2 años no escuchaban el estruendo de las armas, pero  que “**el incumplimiento del Estado y los asesinatos de excombatientes” han provocado que algunas personas decidan alzarse en armas nuevamente**.

Wetio relató que acompañados por una comisión de la Guardia Indígena, se desplazaron a la zona para ayudar a evacuar a niños y adultos mayores que habían quedado solos en las casas. De igual forma, intentaron mediar con los actores armados para que cesen las confrontaciones, añadiendo que la emergencia va a durar porque ninguna de las partes hacen control en el territorio y tampoco quieren ceder.

El Gobernador señaló con preocupación que mientras realizaban las evacuaciones, **fueron testigos de ametrallamientos por parte de un helicóptero de la fuerza pública cerca de algunas casas**, razón por la cual piden se respete el Derecho Internacional Humanitario, que obliga a no vincular a civiles en el conflicto. (Le puede interesar: ["Denuncian que helicóptero del ejército atentó por error contra grupo de indígenas"](https://archivo.contagioradio.com/helicoptero-atento-error-indigenas/))

El líder indígena señaló que la peor situación se vive en la vereda El Naranjal, en la que habitan 300 personas y en Altamira, cerca de 800. También se podrían ver afectadas Bellavista, los Robles y Vetulia, lo que significaría que más de 4 mil personas resulten damnificadas. Adicionalmente, sostuvo que **la llegada de ayuda humanitaria a la zona sería una solución transitoria, porque lo definitivo es que cese el conflicto**.

### **El llamado de las comunidades es a que les permitan vivir en paz** 

Wetio hizó un llamado a la comunidad internacional para que hable con el Gobierno y con los armados para que busquen solucionar de forma negociada el conflicto, asegurando que, aunque el Estado manifiesta que las disicidencias son grupos residuales, en el territorio se ven fortalecidos, y aseveró que mientras en Bogotá se siga con la posición guerrerista de atacar e incumplir el Acuerdo de Paz, el conflicto seguirá avanzando.

De igual forma **llamó la atención a la insurgencia para que no promueva el reclutamiento de menores en los territorios**, y se disponga a dejar las armas, porque finalmente “en el frente de batalla están los hijos de los pobres”, y no importa que uniforme tienen, es entre colombianos que se están enfrentando. (Le puede interesar: ["Coalico insta a ELN y Gobierno a incluir protección de menores de edad en agenda de diálogo"](https://archivo.contagioradio.com/coalico-insta-a-eln-y-gobierno-a-incluir-proteccion-de-menores-de-edad-en-agenda-de-dialogo/))

Por último, el líder indígena envió un mensaje al legislativo asegurando que, “siempre hemos oído la posición de acabar con el otro, pero yo pido a los actores que tienen las armas que las depongan, y a quienes con los discursos y las decisiones que toman en el Congreso depongan sus corazones, y nos dejen vivir en paz”. (Le puede interesar: ["Comunidad de Jiguamiandó denuncia ataque militar aéreo"](https://archivo.contagioradio.com/comunidades-de-jiguamiando-denuncia-ataque-militar-aereo/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
