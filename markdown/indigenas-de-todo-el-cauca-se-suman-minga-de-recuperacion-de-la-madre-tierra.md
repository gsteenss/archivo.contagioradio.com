Title: Indígenas de todo el Cauca se suman a Minga de Liberación de la madre tierra
Date: 2015-02-28 16:51
Author: CtgAdm
Category: Movilización, Nacional
Tags: Feliciano Valencia, liberación de la madre tierra, Moviliación Indígena en el Norte del Cauca
Slug: indigenas-de-todo-el-cauca-se-suman-minga-de-recuperacion-de-la-madre-tierra
Status: published

###### Foto: ONIC 

[En rueda de prensa ofrecida por algunos de los voceros indígenas en Bogotá se denunció que ya son **50 indígenas heridos y hacia las 10 de la mañana de este sábado 28, el ESMAD y la policía iniciaron la quema de cambuches** en los que se resguardan los comuneros, igualmente afirman que continúa la estigmatización por parte de las FFMM y denuncian que las comunidades indígenas no han coordinado acciones con grupos guerrilleros.]

[![ONIC-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/02/B-8T93lWsAE4qSI.jpg){.wp-image-5442 .alignleft width="277" height="208"}](https://archivo.contagioradio.com/actualidad/indigenas-de-todo-el-cauca-se-suman-minga-de-recuperacion-de-la-madre-tierra/attachment/b-8t93lwsae4qsi/)

[A través de los voceros el pueblo Nasa del Norte del Cauca, hizo un llamado **público a Ardila Lule para que se siente a conversar y así se puedan resolver los problemas de tierras** que afectan el departamento y las comunidades indígenas, puesto que muchas de las tierras que el gobierno prometió a los indígenas se encuentran alquiladas a los ingenios del Cauca, propiedad del conglomerado empresarial.]

[La comunidad Nasa también manifestó que **no se puede permitir que se siga ampliando la frontera agrícola**, pues avanza hacia los páramos y las fuentes de agua. Denuncian que muchas de las tierras que antes tenía usos como la ganadería o la agricultura fueron arrasadas y hoy se encuentran completamente destinadas a la siembra extensiva de la caña de azúcar.]

Agregan los voceros que tienen información de la llegada de más integrantes de las FFMM, el ESMAD y la Policía, pero también afirman que más comuneros indígenas se estarán sumando a la protesta en las próximas horas, lo cual podría significar un recrudecimiento de la represión. Por ello hacen un llamado al gobierno nacional a acelerar los trámites de la entrega de tierras.

[Más de 5000 indígenas habitantes del Norte del Cauca se encuentran desde el lunes 23 de Febrero en un proceso de][**[ ]**]**[, ubicadas en Miranda, Corinto y Santader de Quilichao, tierras usadas como productoras de caña para los ingenios del Valle del Cauca. Los indígenas afirman que  durante 6 meses han esperado que el gobierno cumpla lo que prometió en torno a la asignación de tierras para las comunidades indígenas, asignación que asciende a las 20 mil hectáreas.]
