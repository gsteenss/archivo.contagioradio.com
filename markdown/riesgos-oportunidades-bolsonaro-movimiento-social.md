Title: 3 riesgos y 3 oportunidades de victoria de Bolsonaro para el movimiento social
Date: 2018-10-29 12:52
Author: AdminContagio
Category: El mundo, Movilización
Tags: bolsonaro, Brasil, elecciones
Slug: riesgos-oportunidades-bolsonaro-movimiento-social
Status: published

###### Foto:EFE 

###### 29 oct 2018 

Tras la victoria del ultra derechista Jair Bolsonaro (PSL) como nuevo presidente de Brasil con más de 10 millones de diferencia frente a Fernando Haddad (PT), **el panorama para el movimiento social brasilero y latinoamericano, implica nuevos riesgos y oportunidades frente a los cambios que se avisoran**.

Vivian Fernández, integrante de Brasil de Fato, asegura que luego de conocerse el resultado de los comicios cerca de las 7 p.m., **desde el Partido de los Trabajadores hubo un poco de tristeza pero también el análisis de que fue una victoria** por la cantidad de personas que logro movilizar la candidatura de Haddad consiguiendo el 44.9% de los votos.

Ante el nuevo panorama que se avecina, Fernández manifiesta que **desde los movimientos populares se evalúa que con Bolsonaro va a ser un gobierno duro**, sin embargo destaca que no se trata de algo que no hayan enfrentado antes **recordando que durante las dictaduras entre los años 60 y 80; y los gobiernos neoliberales de los 90, existió fueron criminalizados**, concluyendo que "en este gobierno no va a ser diferente".

### **Los riesgos** 

En su apreciación, la analista identifica 3 riesgos principales con la llegada de Bolsonaro al Palacio de Planalto: **El primero desde lo económico** **por la continuidad del proyecto neoliberal de privatizaciones y la reforma previsional,** que atentan contra los derechos económicos y sociales de los más pobres del país, teniendo como principal referente en la rama al empresario Paulo Guedes.

**El segundo tiene que ver con  la criminalización del pensamiento diferente**, afirmando que aun en campaña seguidores de Bolsonaro  "han salido a las calles a maltratar y golpear a quienes piensan distinto y el gobierno no va a ser nada para resolverlo" e incluso apunta que la situación empeorará por la implentación de medidas policivas para criminalizar a la protesta social "a la gente pobre y a la gente negra" añade.

El tercero riesgo que describe Fernández, **se relaciona con la coyuntura de la región**, destacando que el presidente de Colombia Ivan Duque y otros de ultra derecha en la región como Macri en Argentina, Piñera en Chile y el propio Trump, salieron a felicitar a Bolsonaro por su victoria electoral, lo que en su criterio seria el primer paso para el establecimiento de un "**bloque conservador que va a formarse en la región y el principal riesgo va  a ser en contra de Venezuela** por el aislamiento desde lo político y lo económico", advirtiendo sobre una posible intervención militar donde tal vez EEUU no "ponga su mano directamente" pero si la de países como Brasil o Colombia desatando una guerra en el continente.

### **Las oportunidades ** 

Frente a lo que viene, Brasil y el Continente pueden aprovechar la inflexión para convertir las dificultades en oportunidades. Un punto importante, según la integrante de Brasil de Fato, es **organizar a la gente**, "mucha gente que no esta organizada en movimientos, partidos o asociaciones, se movilizó en estas elecciones para la campaña de Fernando Haddad, asi que esta gente que ya esta convocada por el sentido de la disposicion por luchar, **es importante organizarla en espacios donde puedan encontrarse y reflexionar sobre la coyuntura**" asegura.

En su reflexión encuentra además que un segundo aspecto esta relacionado con **la construcción de un proyecto político**, donde los movimientos sociales participen en la articulación de las ideas y debates para un proyecto popular en el país, **brindando a la gente lo que en su comprensión necesitan ahora "una perspectiva de futuro**".

Por último, se refiere a la importancia que en momentos como este tiene la unidad de las naciones, frente a las diferentes persecuciones, violencia económica, cultural e incluso física que están viviendo los pueblos. **Unión que debe ser entendida desde las particularidades de cada país pero también desde la unidad latinoamericana**.

### **4 años de movilizaciones** 

Fernández anuncio que los referentes de los movimientos estar preparados para dar la pelea y que van a seguir insistiendo en la construcción de la democracia en el país y su lucha, lo que implica que **serán cuatro años con muchas movilizaciones que empezarían desde esta misma semana posiblemente el día martes 30 de octubre en diferentes capitales de Brasil**.

<iframe id="audio_29681938" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29681938_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

 
