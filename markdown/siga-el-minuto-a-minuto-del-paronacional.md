Title: Siga el minuto a minuto del #ParoNacional
Date: 2019-04-25 10:32
Author: CtgAdm
Category: Paro Nacional
Tags: Paro Nacional
Slug: siga-el-minuto-a-minuto-del-paronacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/bogota.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D4_7h2LW0AAmG8m.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Buenaventura.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Monteira.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Yopal.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Magdalena.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/LINEA-DE-TIEMPO-PARA-NACIONAL.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Lizama.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_-Risaralda-Caldas.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/CUTBgta_Paro-Nacional.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Puerto-Boyaca.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Bgta-ONIC.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Barrancabermeja.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Tunja.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Oriente-Antioqueno.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Cauca.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Manizales.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Localidad-Suba.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Denuncia-Bogota.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Fusagasuga.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Liga-de-la-Resistencia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Schermata-2019-04-25-alle-13.10.01.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Schermata-2019-04-25-alle-13.55.32.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Catatumbo.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Saravena-Arauca.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/58003882_10156158882800812_8338777660582789120_o.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/ae22ed44-2d35-4345-88b9-229662d3329d.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Disturbios-Bolivar.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Heridos.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Manifestante.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_CISBCSC.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/57870626_1519535681517259_5795054301382443008_n.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Armenia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/logo-full-cric-pgn-color-visible-e1548890320448.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Linea de tiempo del Paro Nacional
---------------------------------

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/logo-full-cric-pgn-color-visible-e1548890320448.png)  
25 abril - 17:13 p.m.  

##### Informe: De la Minga al Paro Nacional

Informe y actualidad sobre la situación en el Cauca. Atraves de un informe la Minga social por la vida, la justicia, la democracia y la paz, se une a los planteamientos del Comando Nacional Unitario (CNU), las centrales sindicales CUT, CTC y CGT y de organizaciones de pensionados CDP y CPC, los movimientos estudiantiles, las centrales obreras, la federación nacional de educadores, Fecode, Asonal Judicial que se movilizaron en el Paro Nacional. Vía:  
@CRIC\_Cauca  
25 abril - 17:13 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Armenia.jpg)  
25 abril - 16:44 p.m.  

##### Armenia

Docentes de Armenia salieron a tomarse las calles de manera masiva hoy para respaldar con fuerza el Paro Nacional.  
Vía:  
@fecode  
25 abril - 16:44 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/57870626_1519535681517259_5795054301382443008_n.jpg)  
25 abril - 16:31 p.m.  

##### Manifestantes bloquearon vía nacional entre puerto Concordia - San José del Guaviare

un grupo de personas integrado por palmeros, indígenas y campesinos decidieron bloquear en las últimas horas la vía nacional que comunica San José del Guaviare con el resto del país.  
Vía:  
@INFONETGUAVIARE  
25 abril - 16:31 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_CISBCSC.jpg)  
25 abril - 16:25 p.m.  

##### CISBCS, Lizama

la Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar, se moviliza en la ruta del Sol, en el sector de la Lizama  
Vía:  
@CISBCSC  
25 abril - 16:25 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Manifestante.jpg)  
25 abril - 16:12 p.m.  

##### Manifestantes heridos, Bogotá​

Manifestante herido en la cabeza por piedra lanzada por la Policia en la esquina de la Casa del Florero. La Fuerza Pública ha herido a varias personas con esta modalidad  
Vía:  
@DefenderLiberta  
25 abril - 16:12 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Heridos.jpg)  
25 abril - 15:51 p.m.  

##### Agresión por Policia, Bogotá

jóvenes de la Universidad Distrital resultaron heridos en la agresión de la Policia contra Paro Nacional.  
Vía:  
@DiegoPintoMi  
25 abril - 15:51 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Disturbios-Bolivar.png)  
25 abril - 15:37 p.m.  

##### Enfrentamientos, Bogotá

Se presentan enfrentamientos entre estudiantes y el Esmad, en la Plaza de Bolívar  
Vía:  
@elespectador  
25 abril - 15:37 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/ae22ed44-2d35-4345-88b9-229662d3329d.jpg)  
25 abril - 15:00 p.m.  

##### Disturbios UNAL

Disturbios en UNAL, se encuentra cerrada la Estación Ciudad Universitaria, en la Troncal Calle 26  
Vía:  
@Contagioradio1​  
25 abril - 15:00 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/58003882_10156158882800812_8338777660582789120_o.jpg)  
25 abril 15:00 p.m.  

##### Campaña

En el día del Paro Nacional se lanza la campaña \#SeValeProtestar  
Vía:  
@Contagioradio1  
25 abril 15:00 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Saravena-Arauca.jpg)  
25 abril - 14:32 p.m.  

##### Saravena, Arauca

Organizaciones sociales, comunidades y sindicatos se movilizaron exigiendo al Gobierno Nacional garantías para líderes sociales, respeto a los territorios campesinos y contra del Plan Nacional de Desarrollo  
Vía:  
@Col\_Informa​  
25 abril - 14:32 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Catatumbo.jpg)  
25 abril - 13:49 p.m  

##### Denuncia, Catatumbo

Caravana de campesinas y campesinos del Catatumbo que se movilizaban hacia Cúcuta en el marco del Paro Nacional fueron detenidos por integrantes del Ejército y la Policía Nacional.  
Vía:  
@Col\_Informa  
25 abril - 13:49 p.m  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Fusagasuga.jpg)  
25 abril - 12:13 p.m.  

##### Fusagasugá

convocan a paro nacional con movilizaciones en rechazo al Plan Nacional Desarrollo. La convocatoria a protesta se fijó en horas de la tarde, con la concentración en el sector La Rotonda del barrio Prados de Altagracia.  
Vía:  
@fusagasuganoti  
25 abril - 12:13 p.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Localidad-Suba.jpg)  
25 abril - 11:55 a.m.  

##### Suba, Bogotá

Desde la Localidad de Suba, en Bogotá, también se vive el Paro Nacional.  
Vía:  
@MovimientoMAIS  
25 abril - 11:55 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Denuncia-Bogota.jpg)  
25 abril - 11:50 a.m.  

##### Denuncia, Bogotá

En este momento se impide el ejercicio de veeduría de DDHH en la Universidad Distrital sede tecnológica (Bogotá) mientras el ESMAD lanza recalzadas al interior de la Universidad.  
25 abril - 11:50 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Manizales.jpg)  
25 abril - 11:50 a.m.  

##### Manizales

estudiantes, indígenas, colectivos, organizaciones sociales y populares como Pueblo Embera y Minga Social Sur Occidente, por la defensa de la vida y el territorio.  
Vía:  
@CRIDEC\_caldas  
25 abril - 11:50 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Cauca.jpg)  
25 abril - 11:41  

##### Cauca

Desde el Cauca las organizaciones Campesinas, Indigenas, Afro, Sindicales y Sociales caminan en el Paro Nacional por la defensa de los derechos.  
Vía:  
@CIMA\_DDHH  
25 abril - 11:41  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Oriente-Antioqueno.jpg)  
25 abril - 11:34 a.m.  

##### Oriente Antioqueño

En el Oriente Antioqueño lxs ciudadanxs también se movilizan en defensa de la vida digna, en contra del Plan Nacional de Desarrollo, la falta de garantías para los líderes sociales.  
Vía:  
@CorpoJuridicaLi  
25 abril - 11:34 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Tunja.jpg)  
25 abril - 11:26 a.m.  

##### Tunja

Campesinado, estudiantes y trabajadores se unen a las exigencias para implementar los Acuerdos de Paz, para rechazar el Plan Nacional de Desarrollo y para defender la educación pública de calidad. Vía:  
@JairoQ\_FARC  
25 abril - 11:26 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Barrancabermeja.jpg)  
25 abril, 11:13 a.m.  

##### Barrancabermeja

La Organización Femenina Popular, las mujeres, los maestros y maestras, trabajadores y trabajadoras, las organizaciones sociales, campesinas, políticas y comunitarias, marchan en Barrancabermeja por la vida, por la defensa del territorio y por la construcción de una paz completa. Vía:  
@OFPMujeres  
25 abril, 11:13 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Puerto-Boyaca.jpg)  
25 abril - 11:03 a.m.  

##### Puerto Boyacá

En Puerto Boyacá se movilizan comunidad y trabajadores petroleros, de la salud y el Magisterio en el \#ParoNacional.  
Es un nuevo clima en esta región azotada por la violencia, son los alcances de la paz, que le abre paso a las libertades, la reconciliación y la democracia.  
Vía:<u>  
@</u>PrensaRural  
25 abril - 11:03 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Bgta-ONIC.jpg)  
25 abril - 10:58 a.m.  

##### Pueblos de la Minga Indígena

Los pueblos de la Minga Indígena @ONIC\_Colombia se movilizan en el Paro Nacional para que se garantice los acuerdos firmados con el gobierno. Vía:  
@cutcolombia  
25 abril - 10:58 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_-Risaralda-Caldas.jpg)  
25 abril - 10:48 a.m.  

##### Vía Risaralda-Caldas

en este momento vía Risaralda - Caldas bloqueada a la altura de La Romelia, hacen presencia maestros, estudiantes y organizaciones sindicales. El paso se habilita cada 20 a 25 minutos. Vía:  
@IsabelUribaron  
25 abril - 10:48 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Lizama.jpg)  
25 abril - 10:21 a.m.  

##### Lizama

La liga de la resistencia en La Lizama está reunida organizando sus acciones de movilización.  
Vía:  
@C\_Pueblos  
25 abril - 10:21 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Magdalena.jpg)  
25 abril - 9:53 a.m.  

##### Magdalena

Los estudiantes del Sena y Universitarios de la Eduación Pública apoyando el paro Nacional por una Colombia Humana  
Vía:  
@HumanaMag  
25 abril - 9:53 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Yopal.jpg)  
25 abril - 9:46 a.m.  

##### Yopal

Traumatismo en varias vías de Yopal, incluida la Marginal del llano por movilizaciones en el marco del paro nacional convocado por maestros, centrales obreras, organizaciones estudiantiles y fuerzas políticas.Vía:  
@Prensalibcasan  
25 abril - 9:46 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Monteira.jpg)  
25 abril - 9:43 a.m.  

##### Montería

Avanza la movilización en Montería contra el Plan Nacional de Desarrollo del presidente Iván Duque. Vía:  
@zenuradiocol  
25 abril - 9:43 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Paro-Nacional_Buenaventura.jpg)  
25 abril - 9:33 a.m.  

##### Buenaventura

Al son de los tambores y la alegría, los Pueblos Indígenas en Buenaventura de @AcivarpV se movilizan a esta hora. El \#ParoNacional es una fiesta por la dignidad, por la vida, por la transformación del país.  
Vía:<u>  
@</u>ONIC\_Colombia  
25 abril - 9:33 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D4_7h2LW0AAmG8m.jpg)  
25 abril - 7:31 a.m.  

##### Vía Pereira-Quibdo

Comunidades indígenas, campesinos, campesinas y afros de Tado, se movilizan desde tempranas horas a lo largo de la vía Pereira-Quibdo en el marco del Paro Nacional \#SeValeProtestar Vía: @ColombiaInforma  
25 abril - 7:31 a.m.  
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/bogota.jpg)  
\#ParoNacional 7:28 a.m  

##### Paro Nacional Bogotá

"Desde el sur de Bogotá porque los barrios exigimos dignidad. Contra Duque y Peñalosa que nos empobrecen con sus política" @DiegoPintoMi  
\#ParoNacional 7:28 a.m
