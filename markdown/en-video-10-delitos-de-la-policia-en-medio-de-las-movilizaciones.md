Title: [En vídeo] 10 delitos de la Policía en medio de las movilizaciones
Date: 2020-09-11 18:42
Author: AdminContagio
Category: Actualidad, Nacional
Tags: ESMAD, Movilización social, Policía, Policía Nacional
Slug: en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Agentes-del-ESMAD-de-la-Policia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Agentes del ESMAD de la Policía Nacional / Foto: Andrés Zea

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Tras el asesinato del ingeniero aeronáutico y estudiante de Derecho, Javier Ordóñez, a manos de agentes de la Policía Nacional;** varios ciudadanos han salido a las calles a manifestarse en contra del abuso policial y de los crímenes de Estado, lo cual ha generado hechos violentos en los que se ha cuestionado el actuar de la Policía y el ESMAD en contra de los manifestantes. (Lea también:[Cinco normativas que deben cambiarse para frenar delitos de la policía](https://archivo.contagioradio.com/cinco-normativas-que-deben-cambiarse-para-frenar-delitos-de-la-policia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Varias de las conductas realizadas por agentes de la Fuerza Pública, que han quedado registradas en videos, constituyen delitos tipificados en el Código Penal y graves faltas disciplinarias**, Contagio Radio hace un recuento de estas conductas delictivas evidenciadas en 16 videos. (Le puede interesar: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Homicidio

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Luego de que agentes de la Policía dispararan de manera indiscriminada en contra de los manifestantes se tiene registro de** **al menos 12 homicidios contra personas entre los 17 y los 30 años.** Las víctimas fatales son Angie Paola Vaquero, Freddy Alexander Mahecha, Andrés Felipe Rodríguez, Cristhián Camilo Hernández Yara, Julián Mauricio González Fory, Germán Smith Puentes, Julieth Ramírez Mesa, Hayder Alexánder Fonseca Castillo, Marcela Zuñiga, Cristhián Andrés Hurtado Meneces, Gabriel Estrada Espinoza y Lorwuan Estiwen Mendoza Aya.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/jcuestanovoa/status/1303895081972387840","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/jcuestanovoa/status/1303895081972387840

</div>

<figcaption>
*Germán Puentes cae herido de muerte por los disparos de la Policía en Suba Rincón*

</figcaption>
</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Tentativa de Homicidio

<!-- /wp:heading -->

<!-- wp:paragraph -->

El propio Secretario de Gobierno de Bogotá, Luis Ernesto Gómez, registró hasta el día de ayer -antes de la segunda jornada de protestas- **al menos 66 heridos por arma de fuego producto de los disparos indiscriminados de agentes de la Policía a los manifestantes.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/edwinmarulanda/status/1304227495466668033","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/edwinmarulanda/status/1304227495466668033

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Lesiones personales

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Más de 400 heridos se han registrado luego de las dos jornadas de protestas, solo en Soacha y Bogotá.** Muchos agentes de Policía quedaron registrados en video, propinando fuertes golpizas a ciudadanos, que en muchos casos, ni siquiera participaban de las protestas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/689062733/posts/10157913235722734/?extid=DC0FYXAvUwFcYpFD\u0026amp;d=n","type":"rich","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-rich is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/689062733/posts/10157913235722734/?extid=DC0FYXAvUwFcYpFD&d=n

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:heading {"level":3} -->

### Desaparición forzada

<!-- /wp:heading -->

<!-- wp:paragraph -->

Algunos ciudadanos y organizaciones no gubernamentales –ONG´s- también han realizado denuncias públicas sobre presuntas desapariciones de personas por parte de la Policía Nacional.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/TembloresOng/status/1304286296555376640","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/TembloresOng/status/1304286296555376640

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JUANCAELBROKY/status/1304222709421932546","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JUANCAELBROKY/status/1304222709421932546

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/TembloresOng/status/1304423448211529728","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/TembloresOng/status/1304423448211529728

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Detención arbitraria

<!-- /wp:heading -->

<!-- wp:paragraph -->

Agentes de la Policía fueron captados haciendo una detención de una persona sin los mínimos protocolos y vulnerando los derechos del ciudadano. En el video se evidencia **cómo un agente acciona un arma marcadora, en por lo menos tres oportunidades a menos de 10 centímetros de distancia en su cabeza, cuello y extremidades inferiores.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/camiloea/videos/10218570789077306","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/camiloea/videos/10218570789077306

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:heading {"level":3} -->

### Tortura y amenaza

<!-- /wp:heading -->

<!-- wp:paragraph -->

En un video también se registró cómo al parecer en un centro de detención, **agentes de la Policía intimidan psicológicamente a un ciudadano que tienen bajo custodia, afirmando que van a jugar a la ruleta rusa con él y que “*lo van a matar*” mientras le propinan golpes en la cabeza.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/camiloea/videos/10218572350996353","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/camiloea/videos/10218572350996353

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:heading {"level":3} -->

### Omisión de socorro

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro de los delitos en el que incurrieron agentes de la Policía, fue omitir el deber constitucional y legal que tienen de dar auxilio a personas cuya vida y salud se encontraba en grave peligro, en la mayoría de los casos, por agresiones de los mismos integrantes de la Fuerza Pública. **Contrario a eso, en algunos casos se llegó a evidenciar incluso, que los arrastraban y golpeaban estando tendidos en el suelo.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/charry_manager/status/1304063265316786177","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/charry\_manager/status/1304063265316786177

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Hurto

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ciudadanos han señalado que agentes de la Policía han arrebatado los bienes de las personas que detienen. La [ONG Temblores](https://twitter.com/TembloresOng) denunció que uniformados estaban «*hurtando los celulares y las billeteras de la ciudadanía y reteniendo a la gente cerca al CAI de Villa Nidia*».

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/TembloresOng/status/1304273406544748545","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/TembloresOng/status/1304273406544748545

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**También en video quedó registrado cómo un Policía, luego de que él y sus compañeros golpearan a un ciclista, se apropia de su bicicleta.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/camiloea/videos/10218570907920277","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/camiloea/videos/10218570907920277

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:heading {"level":3} -->

### Daño en bien ajeno

<!-- /wp:heading -->

<!-- wp:paragraph -->

Uniformados fueron captados por las cámaras haciendo daños y destrozos en residencias; la ciudadanía ha señalado que esta práctica es recurrente, para que la gente culpe a los manifestantes por estos daños y así se deslegitime la movilización y la protesta. Incluso hay videos en los que se ve a policías destruyendo, ellos mismos, un Centro de Atención Inmediata, CAI.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DavidSkyMotors/status/1303956976456654850","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DavidSkyMotors/status/1303956976456654850

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/jiminmypeace/status/1304259136847261698","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/jiminmypeace/status/1304259136847261698

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Faltas disciplinarias graves

<!-- /wp:heading -->

<!-- wp:paragraph -->

En algunos videos quedó registrado cómo uniformados entregaban sus armas de dotación a civiles para que estos dispararan en contra de los manifestantes.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/kirjcp/status/1304379697862709248","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/kirjcp/status/1304379697862709248

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Gabocolombia76/status/1303901006091124737","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Gabocolombia76/status/1303901006091124737

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Un Policía volteó su chaqueta de dotación para evitar que su número de identificación se viera  y agredió a ciudadanos con una especie de varilla.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PrimeraLineacol/status/1303884949683019777","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PrimeraLineacol/status/1303884949683019777

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/jairoriverah/status/1303875372224786434","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/jairoriverah/status/1303875372224786434

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por la calle 26 también fueron grabados algunos sujetos que al parecer con cascos de Policía, se movilizaban con ropa de civil y en motocicletas particulares.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Dumar_Guevara/status/1304194725302415366","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Dumar\_Guevara/status/1304194725302415366

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Son al menos 10 delitos y 3 faltas disciplinarias graves, los que se documentan en los vídeos que captó la ciudadanía, en los que incurren miembros de la Fuerza Pública,** lo que ha llevado a que el malestar y la indignación de los ciudadanos vaya en aumento y a que la alcaldesa de Bogotá haya declarado que la Policía está descontrolada pues no obedece a ordenes suyas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
