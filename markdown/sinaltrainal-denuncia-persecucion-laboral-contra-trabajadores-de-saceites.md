Title: Sinaltrainal denuncia persecución contra trabajadores de Saceites
Date: 2015-08-25 14:31
Category: Movilización, Nacional
Tags: derecho al trabajo, Saceites, Sinaltrainal, Sindicalismo, SIndicatos, Trabajo decente
Slug: sinaltrainal-denuncia-persecucion-laboral-contra-trabajadores-de-saceites
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/sinaltrinal_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: sinaltrainal 

##### <iframe src="http://www.ivoox.com/player_ek_7597837_2_1.html?data=mJqmmZ2Xe46ZmKiakpyJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8riwtHh1MbNssLgjMnS0NrSp8rVjNXS1NjJp9bXyoqwlYqmd8%2BfxNTb1tfFb9XmwsfOzMbIs9PZ1JCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Javier Correa, SINALTRAINAL] 

###### [25 Ago 2015] 

[**12 horas en huelga de hambre completaron el día de ayer en Barrancabermeja trabajadores de la empresa Saceites,** propiedad del grupo Dicorp, que suministra cerca del amplia variedad de los productos vendidos en supermercados, exigiendo respeto por sus derechos humanos y laborales, ante las constantes amenazas y persecuciones por parte de la empresa. ]

[Javier Correa, encargado nacional de Derechos Humanos del Sindicato Nacional de **Trabajadores de la Industria de Alimentos, Sinaltrainal,** denuncia que la situación es problemática tras denunciar públicamente persecuciones y amenazas de muerte en su contra, han recibido en sus panfletos y coronas funerarias, así como funcionarios de la empresa que allanan sus propiedades e interrogan a sus familias.]

[Así mismo, los trabajadores sindicalizados han sido perseguidos y fotografiados tanto dentro de la empresa como en sitios públicos de la ciudad, otros han sido llamados a descargos sin ser empleados o estar fuera de turno de trabajo. Por si fuera poco se han registrado últimamente **35 despidos colectivos de trabajadores no sindicalizados y que están siendo reemplazados por mano de obra subcontratada.**]

[La **huelga de hambre finalizó cuando se logró concretar una reunión en la sede del Ministerio de Trabajo de Bucaramanga con el Ministerio de Trabajo**, así como la sección de derechos humanos de la Policía Nacional, para la instalación de una mesa de trabajo con el objetivo de construir medidas que permitan poner fin a la violación de derechos humanos y laborales de los que están siendo víctimas estos trabajadores.]
