Title: Programa "Tu Eliges" del gobierno, fomenta el endeudamiento de estudiantes
Date: 2015-05-22 22:10
Category: Educación, Otra Mirada
Tags: Carlos Acero, ICETEX, mane, Mesa Amplia Nacional Estudiantil, Programa "Tu Eliges", Pruebas Saber
Slug: programa-tu-eliges-del-gobierno-fomenta-el-endeudamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/icetex-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div>

###### Foto: lasillavacia.com 

Nueva línea de crédito **ICETEX "Tu Eliges"** presentada el día de ayer por el presidente Juan Manuel Santos, fomenta de manera exorbitante el endeudamiento de estudiantes en Colombia, afirman estudiantes de la MANE.

Según lo anunciado por el mandatario, la nueva forma de crédito exoneraría a estudiantes de estratos 1, 2 y 3 de pagar el 100% de su matricula, siempre que tengan un puntaje mayor de 310 en las **Pruebas Saber**. Este, sin embargo, sería un sofisma de distracción.

**Carlos Acero**, representante estudiantil de la **Facultad de Derecho de la Universidad de los Andes y miembro de la MANE**, asegura que "Tu Eliges" es una propuesta en el marco de la **política de financiarización** de la educación por parte del gobierno nacional, con el fin de buscar hacer negocio con la educación. "Por medio del endeudamiento dicen que van a garantizar el derecho (a la educación), pero obviamente a uno no le garantiza nada endeudarse", declara Acero. Los y las estudiantes no pagarían el costo de su matricula en el transcurso de la carrera, pero al finalizarla deberían pagar el costo total de la misma.

En el caso de la **Universidad de los Andes**, el costo promedio de las carreras es de \$13'144.000, y cada año sube en un promedio de \$600.000. Tener este crédito implica adquirir una deuda de \$ 60'000.000 que incrementaría hasta los 80'000.000. "Muchos sabemos que nos estamos metiendo en una 'vaca loca', pero tenemos ganas de estudiar y salir adelante".

Esta propuesta, en la que el presidente de la república asegura avanzar en la meta de "*ser la nación más educada de la región*", perpetúa el saqueo de los recursos de las y los estudiantes y sus familias a largo plazo, por la necesidad de educarse. "De fondo se vislumbra el modelo de endeudamiento, modelo de favorecimiento al sistema financiero mediante el ICETEX", dice el representante estudiantil de Derecho de la Universidad de los Andes.

Carlos Acero, afirma que la nueva línea de crédito no es un avance hacia la gratuidad de la educación, sino todo lo contrario."Destinan recursos públicos para dar líneas de crédito, en vez de destinarlos para incrementar las universidades, mejorar las condiciones y permitir un ingreso gratuito".

En el año 2012, la **cartera morosa del ICETEX ascendió al 34%**. Se prevé que, tal como ocurrió en los EEUU, la burbuja de deudas a través de los créditos, explotará, ante la evidente la incapacidad de estudiantes para pagar sus exorbitantes deudas.

</div>
