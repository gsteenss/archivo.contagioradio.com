Title: Detenciones arbitrarias de trabajadoras sexuales en Bogotá
Date: 2016-01-22 10:14
Category: DDHH, Nacional
Tags: Detención a prostitutas en Bogotá, Mar Candela, Prostitución en Bogotá
Slug: detenciones-arbitrarias-de-trabajadoras-sexuales-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/policias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### 22 Ene 2016.

El abuso de poder hacia las trabajadoras sexuales se ha visto este miércoles en la plaza de Las Mariposas de Bogotá donde agentes de la fuerza pública han detenido, de orma arbitraria, a mujeres que ejercían su trabajo en el sector San Victorino, en el centro de la ciudad.

Según informa Mar Candelas, ideóloga del movimiento Feminismo Artesanal, "es inaudito que la policía no sepa que el trabajo sexual no es un delito, hasta la Corte Constitucional lo reconoció como un trabajo digno".

Y es que, descalzas y ultrajadas, estas mujeres trabajadoras sexuales fueron detenidas por "vender su cuerpo", según el argumento de los agentes. "Si ellas vendieran su cuerpo estarían muertas, lo que hacen es ofrecer un servicio sexual remunerado", explica Candelas dando a entender el poco o nulo conocimiento y respeto que muestran las fuerzas públicas por las mujeres que trabajan en la calle.

Por todo ello, la tarde del viernes se realizó un performance en la Plaza de las Mariposas, dirigido por Mar Candelas y acompañado por otras mujeres, con el objetivo de denunciar esta detención arbitraria, de educar y sensibilizar a la ciudadanía y de solidarizarse con las trabajadoras sexuales ultrajadas en público.
