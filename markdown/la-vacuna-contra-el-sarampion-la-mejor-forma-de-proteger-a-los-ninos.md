Title: La vacuna contra el sarampión la mejor forma de proteger a los niños
Date: 2018-03-25 11:18
Category: DDHH, Nacional
Tags: Sarampión
Slug: la-vacuna-contra-el-sarampion-la-mejor-forma-de-proteger-a-los-ninos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/1480443980750.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Periódico] 

###### [25 Mar 2017] 

Una reciente alarma se ha prendido en Colombia, por el brote de sarampión que se ha presentado, la primera hipótesis que había salido en algunos medios de información, es que era producto de las migraciones de niños de Venezuela a Colombia, sin embargo, el doctor Camilo Prieto, aclaró que es una falsa alarma y que aún no hay una epidemia.

**“Cuando un niño se encuentra vacunado las posibilidades de adquirir la enfermedad es casi nula**” afirmó Prieto y agregó que, en países con sistemas de salud deficientes, con limitaciones y barreras de acceso a la prevención y a los sistemas de vacunación, si es posible que se eleven los casos de brote, no obstante, para que llegue al nivel de epidemia tienen que darse muchos más factores.

### **El movimiento anti vacunas** 

Otra de las preocupaciones de los médicos ha sido el movimiento anti vacuna que se ha venido generando a nivel global y que ha postulado que la aplicación de vacunas es perjudicial para la salud de los niños. Sin embargo, el doctor Prieto señaló que **“la evidencia epimeológica demuestra que vacunar a los niños les salva la vida”**.

Para el caso particular de Colombia, Prieto aseguró que el país tiene un sistema de vacunación eficiente, **pero que se queda corto en el balance general de prevención, promoción y tratamientos**.

### **Los daños al ambiente y la salud si son una alarma roja** 

Finalmente, Prieto afirmó que si hay una alerta roja frente a la relación del daño al ambiente y las afectaciones a la salud que van en aumento, para él, el derrame del pozo La Lizama y la destrucción de ecosistemas que produjo es un ejemplo de ello, **“esta contaminación de hidrocarburos de las fuentes hídricas se traduce en la imposibilidad de tener acceso al agua”**.

Así mismo expresó que la contaminación del aire en Bogotá y Medellín han aumentado los casos de enfermedades respiratorias en Colombia, mientras que la contaminación por mercurio en Chocó y Cauca están enfermando a las personas. (Le puede interesar: ["La salud desde la democracia: una mirada desde la Bioética"](https://archivo.contagioradio.com/catedra_abierta_bioetica_universidad_bosque/))

<iframe id="audio_24806758" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24806758_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
