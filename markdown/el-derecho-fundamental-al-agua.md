Title: El derecho fundamental al agua
Date: 2016-05-24 06:00
Category: Ambiente y Sociedad, Opinion
Tags: Agua, Ambiente, derechos al agua
Slug: el-derecho-fundamental-al-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/derechos-al-agua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario Extra 

#### **Por [Natalia Gomez](https://archivo.contagioradio.com/margarita-florez/)\* - [Asociación Ambiental y Sociedad ](https://archivo.contagioradio.com/margarita-florez/)** 

###### 24 May 2016 

El pasado 17 de mayo se aprobó en segundo debate en el congreso el proyecto de acto legislativo que crea el artículo 11 A de la Constitución y consagra el agua como un derecho fundamental de todos los colombianos. La iniciativa liderada por el senador de la Alianza Verde Jorge Prieto ha logrado reunir un amplio apoyo entre las bancadas de varios partidos, contó con la ponencia en el Senado de varios de los congresistas más mediáticos de todas las orillas partidistas incluyendo a Claudia López, Vivian Morales, Roberto Gerlein y Armando Benedetti.

En el contexto internacional, el proceso de reconocimiento del derecho al agua ha llevado a considerar este derecho como indispensable para la vida y la salud. En el foro de Naciones Unidas los países han aprobado diversos documentos como la Observación general Nº 15 del Comité de Derechos Económicos, Sociales y Culturales que definió este derecho como el derecho “a disponer de agua suficiente, salubre, aceptable, accesible y asequible para el uso personal y doméstico”. El Programa de las Naciones Unidas para el Desarrollo (PNUD) también ha subrayado que la acción pública en relación con el agua debe estar orientada a reconocer que el derecho al agua es un derecho humano básico. Además, En 2008 el Consejo de Derechos Humanos creó el mandato del “Experto independiente sobre la cuestión de las obligaciones de derechos humanos relacionadas con el acceso al agua potable y el saneamiento” con el objetivo de aclarar el alcance y el contenido de las obligaciones que tienen los Estados respecto a este derecho.

En Colombia la jurisprudencia de la Corte constitucional desde sus inicios, apoyada en los avances del derecho internacional, ha considerado el derecho al agua como un derecho fundamental, expresando que: “el agua se considera, también como un derecho fundamental y, se define, de acuerdo con lo establecido por el Comité de Derechos Económicos, Sociales y Culturales, como el derecho de todos de disponer de agua suficiente, salubre, aceptable, accesible y asequible para el uso personal o doméstico”\[1\].De esta manera, se ha establecido que el Estado colombiano debe garantizar que todas las personas podamos acceder al servicio de agua potable en las condiciones de igualdad y calidad necesarias.

A pesar de esto, son múltiples las amenazas que se ciernen sobre las fuentes de agua de nuestro país y por consiguiente sobre la garantía del derecho al agua de todos los colombianos. La minería legal e ilegal; las diversas afectaciones a los páramos, los cuales producen cerca del 70% del agua potable del país junto con los bosques andinos; la agroindustria sin control; la explotación de hidrocarburos tanto de manera tradicional como por medio de la llamada técnica del “Fracking”, para la cual se dice ya se concesionaron seis bloques en el país, y que de acuerdo a los expertos exige entre 9.000 y 29.000 metros cúbicos de agua para la explotación de cada pozo de petróleo y plantea un serio riesgo para la contaminación de los recursos acuíferos subterráneos.

La reforma constitucional que cursa en el Congreso en este momento, no sólo materializa el derecho fundamental al agua en la constitución, también plantea que este derecho es vital para el desarrollo social, ambiental, económico y cultural de las personas, y que el uso prioritario del recurso hídrico debe ser el consumo humano. Potencialmente esta modificación constitucional podría permitir que en los debates acerca de proyectos de infraestructura o de industrias extractivas con potencial de afectar nuestros recursos hídricos, el derecho al agua y las obligaciones que tiene el Estado para su garantía favorezcan que la balanza se incline hacia el lado de la conservación ambiental y la protección de los derechos humanos.

El acto constitucional aún tiene un amplio camino en el Congreso de la República, pues hasta ahora se ha aprobado el segundo debate de los ocho que se deben surtir (cuatro en la Cámara y cuatro en el Senado), sin embargo, el apoyo multipartidista que ha despertado la iniciativa hace pensar que prontamente contaremos con un nuevo artículo en nuestra Constitución Nacional.

###### \[1\] Sentencia T-740 de [2011.MP](http://2011.mp/): Humberto Sierra Porto. Corte Constitucional. 

###### \*Coordinadora del área de democracia ambiental 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
