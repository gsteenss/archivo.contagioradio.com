Title: Asedio paramilitar continúa en territorios colectivos de Curbaradó y Jiguamiandó
Date: 2019-03-21 17:14
Category: Comunidad, DDHH
Tags: Curbaradó, Paramilitares en Bajo Atrato
Slug: asedio-paramilitar-continua-en-territorios-colectivos-de-curbarado-y-jiguamiando
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Mineria-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 21 Mar 2019 

[Desde el pasado 8 de marzo, paramilitares de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) desarrollan operaciones de movilidad y control social sobre la población de la **Zona Humanitaria Las Camelias**, de igual forma los habitantes del lugar han divisado en dos ocasiones helicópteros sobrevolar las zonas de **Brisas de Curbaradó y Belén de Bajira, **lugares que cuentan con presencia de efectivos militares regularmente. ]

Según un comunicado de la **Comisión Intereclesial de Justicia Paz**, el pasado 8 de marzo un grupo de hombres paramilitares de las AGC, vestidos de camuflado ingresó a predios del consejo comunitario de Las Camelias, en Curbaradó donde intimidaron a los pobladores afirmando "que debían acostumbrarse a su presencia, que estaban allí para tomar el control sobre el territorio".

Desde entonces y específicamente el 10 de marzo fue divisado un grupo de hombres armados, vestidos con** pasamontañas y camuflado quienes recorrían ** la carretera que ese encuentra frente de la** Zona Humanitaria. [(Lea también: Paramilitares están retomando el control del Bajo Atrato)](https://archivo.contagioradio.com/paramilitares-estan-retomando-el-control-del-bajo-atrato/)**

A estos hechos se suman los ocurridos [**el pasado 19 de marzo cuando un helicóptero sobrevoló las zonas de Brisas de Curbaradó y Las Camelias**, al escuchar la aeronave, la comunidad corrió a refugiarse en sus casas por miedo y aunque saben que la aeronave aterrizó en Brisas, ignoran con qué fin.]

[En las siguientes 48 horas, habitantes y observadores de DD.HH. lograron identificar la presencia de dos miembros de las AGC, denominados **"puntos"**, hombres armados vestidos de civil que de forma permanente **comunicaban vía telefónica el ingreso de cualquier vehículo que se movilizaba en el sector**.]

Dichas rondas paramilitares continúan presentándose, generando zozobra en los pobladores de la región sin que el Estado ofrezca una respuesta a la crisis humanitaria que se viene presentando al interior de las comunidades del Bajo Atrato.[(Le puede interesar: UNP retira protección a seis líderes sociales amenazados en Bajo Atrato)](https://archivo.contagioradio.com/retira-proteccion-a-seis-lideres/)

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
