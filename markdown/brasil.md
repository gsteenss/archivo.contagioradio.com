Title: Movilizaciones impulsarían decisión sobre futuro de Temer en Brasil
Date: 2017-05-19 12:36
Category: El mundo, Otra Mirada
Tags: Brasil, Impeachment, Temer
Slug: brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Brasil.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/brasil_sintierras.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/brasil-de-fato1-e1488992845311.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/brasil-defato2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Brasil de Fato 

###### 19 May 2017 

El Tribunal Supremo Federal de Brasil ordenó este jueves a**brir una investigación por obstrucción de la justicia contra el presidente no electo Michel Temer**,con base en una serie de evidencias que agudizan uno de los escándalos más grandes en ese país en tiempos de democracia.

En las grabaciones reveladas por la cadena nacional Globo, **el mandatario supuestamente da el aval para que el empresario Joesley Batista**, dueño del imperio empresarial JBS, **comprara el silencio del hoy detenido ex presidente de la Cámara de Diputados Eduardo Cunha**, una de las fichas claves durante el proceso de impeachment contra Rousseff.

De acuerdo con Fania Rodriguez, periodista del portal Brasil de Fato, "**este es el momento más crítico de la democracia brasileña**" junto con el proceso que terminó con la destitución de la entonces presidenta Dilma Rousseff, con la diferencia que "**en ese momento aun se tenia cierta confianza en el Congreso Nacional** "  y algunos jueces y fiscales ahora involucrados por las denuncias del empresario involucrado.

Tras el escándalo desatado posterior a la emisión de tales contenidos, **Temer se pronunció en un discurso televisivo asegurando que no renunciará**, por que "no tiene nada que ocultar y que colaborará con el Tribunal Superior Federal". En su alocución aseguró además que solicitará una investigación sobre las acusaciones en su contra. Le puede interesar: [Movimientos populares piden impeachment contra Temer](https://archivo.contagioradio.com/movimientos-populares-impeachment-temer/).

Para la comunicadora, las consecuencias de lo ocurrido mantienen "un clima de inestabilidad", que **terminará con la renuncia del presidente interino**, sin embargo apunta que posiblemente sus declaraciones obedezcan a una escena teatral para **ganar tiempo y encontrar una salida política más razonable**. Tiempo que ahora depende, además de las determinaciones senatoriales y judiciales, de la respuesta de los brasileños en las calles.

Desde diferentes sectores políticos, incluso algunos de los que apoyaron a Temer y votaron a favor a la destitución de Rousseff, presentaron pedidos de impeachment. Desde otros sectores se plantea que **Temer puede esperar por su casación** o a que el proceso que se adelanta en el Tribunal Supremo Electoral prospere y **la coalición Juma Temer sea cancelada**, alternativas que representarían una salida "más honrosa" para el mandatario.

Las movilizaciones  en respuesta no se hicieron esperar, en capitales estatales y localidades a lo largo de **23 de los 26 estados del país**, para exigir que se convoque a elecciones presidenciales ya. Sao Paulo, Río de Janeiro, Brasilia, Alagoas, y por lo menos 12 ciudades más fueron escenario de airadas protestas, algunas fueron reprimidas por agentes policiales.

"Es la movilización mas grande que he visto desde el año 2013" aseguro la periodista quien se encontraba en la Ciudad de Río de Janeiro donde **calcula se concentraron cerca de 100.000 personas**, destacando que a los movimientos sociales o sindicales que regularmente salen a la calle, se sumaron ciudadanos que de manera espontánea manifestaron su indignación ante lo sucedido.

<iframe id="audio_18789886" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18789886_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Reconstruir la democracia**.

En el Congreso se discute una salida política posible ante la posible renuncia de Temer, **la Constitución política plantea la realización de una elección indirecta** que se realizaría entre los propios diputados. Sin embargo desde los partidos de oposición se esta proponiendo que se realicen elecciones directas, ante lo involucrado que se encuentra el Congreso con las denuncias presentadas.

"Varios diputados están citados en las investigaciones, no sólo JBS (mayor empresa cárnica del mundo)", lo que a su juicio resta legitimidad al Congreso que debe solucionar una crisis institucional como la que se esta presentando. **Si la alianza Juma Temer es sancionada por el Supremo electoral deben realizarse elecciones directas** con participación de la ciudadanía.

Las expectativas ante los posibles caminos que puede tomar la democracia brasileña pasarán por la actitud de los congresistas, medios de comunicación y ciudadanía en general, a quienes se les ha convocado para que el próximo 24 de mayo se manifiesten en Brasilia y logren hacer presión para que se tome una pronto decisión.

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.
