Title: Arte y cultura en el día de no violencia contra las mujeres
Date: 2018-11-23 14:03
Author: AdminContagio
Category: eventos, Otra Mirada
Tags: genero, teatro, Violencia contra la mujer
Slug: dia-no-violencia-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/descarga-e1554833150526.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CCT 

###### 24 Nov 2018 

Durante varios meses diferentes **colectivos de mujeres se han venido reuniendo con el fin de convocar a todas las mujeres para el 25 de noviembre**, día en el que se conmemora en el mundo, el Día Internacional de la No violencia contra las Mujeres.

A partir de las **nueve de la mañana hasta las seis de la tarde** en el **Parque Nacional Olaya Herrera**, se desarrollarán diferentes actividades que involucrarán acciones culturales y artísticas en homenaje a las mujeres.

![no violencia mujer](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/BeFunky-collage-800x605.jpg){.alignnone .size-medium .wp-image-58630 width="800" height="605"}

Una de las actividades es la **acción performática “Cuerpos gramaticales”** en la que **40 mujeres entre víctimas y solidarias se sembrarán durante seis horas**, al cabo de las cuales, renacerán y florecerán para la vida.

Otra de las actividades será la **Batucada Feminista “La Tremenda Revoltosa”**, dirigida por Ochi Curiel, activista y feminista que acompañada de **20 mujeres interpretan tambores, trompetas, y redoblantes** en forma de apoyo a los derechos de las mujeres en Colombia.

Además,  la Corporación Colombiana de Teatro, bajo la dirección de la maestra Patricia Ariza y Nhora Gonzlaez presentará **“Mujeres Rompiendo El Silencio”**, una puesta en escena donde **36 mujeres mediante el teatro, la música y la danza**, denuncian el asesinato de lideresas y las violencias hacia la mujer, reflejando su la lucha y resistencia.

Acompañando a la propuesta teatral de la CCT, **10 mujeres bajo el lema “Por ellas nos rapamos”** realizarán un acto ritual de protesta por el incremento de los feminicidios en Colombia y la muerte de lideresas sociales.

Otros grupos de música y teatro prepararan varias intervenciones a lo largo del día 25.

**Agenda de la jornada:**

- 6:00 a 1:00pm Siembra Cuerpos Gramaticales  
- 1:00 a 1:30pm Batucada Feminista  
- 1:30 a 2:30pm Mujeres Rompiendo El Silencio / Por ellas nos rapamos  
Corporación Colombiana de Teatro  
- 2:30 a 3:00pm Hip Hop Agrario  
- 3:10 a 3:30pm Danza Oro Verde Danza Suramericana  
- 3:35 a 4:05pm Grito Andino  
- 4:10 a 4:40pm Chirimia  
- 4:43 a 5:15pm Lucia Vargas / Karen Tovar

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
