Title: Docentes se preparan para movilización Nacional el 31 de mayo
Date: 2017-05-26 13:18
Category: Educación, Nacional
Tags: fecode, Ministerio de Educación
Slug: 31-de-mayo-dia-de-movilizacion-nacional-de-docentes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/movilización-fecode.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 May 2017] 

EL gremio de Docentes completa 3 semanas sin obtener mayores avances en las conversaciones con la delegación del Ministerio de Educación y el gobierno para superar la crisis que afrontar las maestras y los maestros del país y en general el Sistema de Educación, para la semana que vienen **ya han anunciado una gran movilización el 31 de mayo y diferentes actividades**.

Una de las primeras tareas que están preparando los docentes es **intentar establecer reuniones con alcaldías y gobernaciones del país, para que se sumen con su apoyo al paro**  y conformen una propuesta en defensa de la Educación pública. Le puede interesar: ["Hay propuestas pero gobierno no tiene voluntad para negociar: FECODE"](https://archivo.contagioradio.com/maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar/)

Además de ello, se espera que, durante el fin de semana, cada delegación departamental organice las rutas por donde se movilizarán el próximo 31 de mayo en cinco ciudades del país: Bogotá, Medellín, Bucaramanga, Cali y Barranquilla. A las marchas también **se convocaron padres de familia y estudiantes** que se espera se sumen en los diferentes puntos de concentración.

En los 7 días de conversaciones que se han dado entre los maestros y el gobierno, los puntos más álgidos de negociación han sido los que corresponden al tema financiero. Hasta el momento la propuesta de la nivelación salarial que ha expuesto el gobierno, ha sido entendida como irrisoria para los docentes, en el tema de jornada única continua la exigencia al gobierno que se **implemente una vez se tengan todas las garantías tanto para los estudiantes como para los maestros.**

En cuanto a la petición de los maestros de disminuir el déficit que actualmente afronta el Sistema de Educación en el país, el secretario general de FECODE, Rafael Cuello en una alocución para el gremio docente señaló “necesitamos que se incremente e**l 7.5% del PIB para que nuestra educación tenga más dinero** y nuestras instituciones educativas sean modernas como corresponde y cumplir lo de Colombia la más educada”. Le puede interesar:["Déficit en educación es de 600 mil millones de pesos: FECODE"](https://archivo.contagioradio.com/deficit-en-educacion-es-de-600-mil-millones-de-pesos-fecode/)

De otro lado, la Ministra Yaneth Gihab ha expresado que los docentes deberían levantar el paro, porque se está dialogando en la mesa de conversaciones, en la que han propuesto generar un sistema de bonificaciones que beneficiaría a todos los docentes del país, cada vez que se cumpla un año de trabajo, la segunda alternativa que han expuesto sería ofrecer una bonificación al **65% de los docentes con ingresos más bajos del país**.

Cuello,  manifestó, que, de no encontrar respuesta en el diálogo, **realizarán la gran marcha por Colombia**, que intentará que maestros y maestras de diferentes ciudades del país, lleguen hasta la capital. Le puede interesar: ["334.000 docentes entran en paro Nacional convocado por FECODE"](https://archivo.contagioradio.com/334-000-profesores-se-suman-al-paro-nacional-de-fecode/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
