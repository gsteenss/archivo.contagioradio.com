Title: ELN propone cese bilateral para recibir al Papa Francisco
Date: 2017-06-06 10:28
Category: Nacional, Paz
Tags: ELN, Mesa en Quito
Slug: eln-propone-cese-bilateral-para-recibir-al-papa-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/1beltranhabla.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias RCN] 

###### [06 Jun 2017] 

A través de un comunicado de prensa, el Ejército de Liberación Nacional expresó que espera que antes de que finalice este segundo ciclo de conversaciones se adelanten los mecanismos de participación social que tendrá la ciudadanía y manifestó su voluntad por pactar un **cese bilateral del fuego, antes de septiembre, cuando el Papa Francisco visitará Colombia.**

El ELN señaló que en la mesa de diálogos en Quito se están trabajando dos vías, por un lado **la participación social, las dinámicas y acciones humanitarias** y por el otro lado los temas complementarios como pedagogía y comunicaciones para la paz, el grupo de países de apoyo, el fondo para respaldar la mesa de conversaciones y el desminado humanitario. Le puede interesar: ["5 países crearán fondo económico para apoyar proceso de paz con ELN"](https://archivo.contagioradio.com/5-paises-crearan-fondo-economico-para-apoyar-proceso-de-paz-con-eln/)

De estos puntos ya se tendrían avances en la propuesta de pedagogía y comunicación para la paz, sin embargo los avances en términos de participación aún no se han realizado, de acuerdo al ELN, **porque no se ha puesto en marcha las llamadas Audiencias Preparatorias, un escenario preliminar de consulta con sectores de la sociedad** sobre sus experiencias y propuestas de mecanismos de participación. Le puede interesar:["Díalogos con ELN fortalecen esfuerzos hechos en la Habana con las FARC"](https://archivo.contagioradio.com/dialogos-con-el-eln-fortalecen-esfuerzos-hechos-en-la-habana-con-las-farc/)

En el comunicado, el ELN rechazó los asesinatos a líderes sociales en el país y el accionar de la Fuerza Pública, bajo las órdenes del Estado, contra el ejercicio del derecho a la protesta en Buenaventura y agregó que “**la protesta social es una forma de participación legítima, que el régimen sigue tratando con la estigmatización y la violencia**”.

![WhatsApp Image 2017-06-05 at 9.54.11 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/WhatsApp-Image-2017-06-05-at-9.54.11-AM.jpeg){.alignnone .size-full .wp-image-41709 width="1271" height="869"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
