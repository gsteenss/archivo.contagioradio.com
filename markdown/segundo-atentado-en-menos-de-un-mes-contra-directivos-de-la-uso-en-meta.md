Title: Segundo atentado en menos de un mes contra directivos de la USO en Meta
Date: 2020-02-17 17:23
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Armados, Puerto Gaitán
Slug: segundo-atentado-en-menos-de-un-mes-contra-directivos-de-la-uso-en-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Atentado-a-líder-de-USO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @EdgarMojicaV {#foto-edgarmojicav .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/jonathan-urbano-presidente-subdirectiva-uso-sobre-ataque_md_47927651_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Jonathan Urbano | presidente de la subdirectiva de la Unión Sindical Obrera en Puerto Gaitán (Meta

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

A tempranas horas de este lunes 17 de febrero, fue atacado por hombres armados el presidente de la subdirectiva de la Unión Sindical Obrera en Puerto Gaitán (Meta), Jonathan Urbano, mientras se dirigía a un taller sobre derechos humanos. Urbano salió ileso del ataque, y es el segundo líder sindical que sobrevive a un hecho de este tipo en 2020.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El líder de la USO y su esquema de protección respondieron al ataque**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Urbano salió esta mañana sobre las 3 a.m. desde Villavicencio (Meta) rumbo a Puerto Gaitán para participar en el taller de derechos humanos y paz desarrollado por la Comisión nacional de Derechos Humanos de la USO. Sobre las 4 de la mañana en la vereda El Amor, hombres armados abrieron fuego contra la camioneta en la que se desplazaba el líder y tres acompañantes, incluído su esquema de seguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque el vehículo asignado por la Unidad Nacional de Protección (UNP) no es blindado y recibió varios impactos en la parte trasera, Urbano salió ileso y junto a sus hombres de seguridad repelió el ataque. Los armados huyeron del lugar, mientras Urbano y los ocupantes de la camioneta regresaron a la vivienda y posteriormente avisaron a la policía sobre los hechos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"En Puerto Gaitán es difícil hacer sindicalismo"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Urbano sostuvo que en Puerto Gaitán es difícil hacer sindicalismo, pues hay grupos residuales de las Autodefensas Unidas de Colombia (AUC) y otros actores armados que atentan contra toda actividad de carácter social. Para el líder, esa situación explica las amenazas de las que ha sido víctima desde [abril de 2019](http://www.usofrenteobrero.org/index.php/actualidad/7436-amenaza-a-presidente-de-la-uso-subdirectiva-puerto-gaitan-meta).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, por medio de un comunicado, la USO recordó el pasado 13 de febrero fue atacado con arma blanca David Ramírez, tesorero de la organización en Puerto Gaitán. Por ambas situaciones, el sindicato pidió reforzar la seguridad de los líderes obreros e investigar las amenazas. (Le puede interesar:["Conquistas de la lucha obrera de la USO a 97 años de existencia"](https://archivo.contagioradio.com/conquistas-de-la-lucha-obrera-de-la-uso-a-97-anos-de-existencia/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
