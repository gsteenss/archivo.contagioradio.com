Title: Defensores de La Conejera resisten ante orden de desalojo
Date: 2015-05-04 14:01
Author: CtgAdm
Category: Ambiente, Nacional, Resistencias
Tags: Alcaldía de Bogotá, Alcaldía de Suba, ESMAD, Fuerza Pública, Humedal, La Conejera, Suba
Slug: defensores-de-la-conejera-permaneceran-en-el-humedal-pese-a-orden-de-desalojo
Status: published

##### Foto: @ConejeraResiste 

<iframe src="http://www.ivoox.com/player_ek_4444741_2_1.html?data=lZmhlpyYdY6ZmKiakpqJd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0cbax9PYs4zl1sqYxsrKrcbixcqYrsaPh9Dixs%2FS1MaPtsbnytjhx5DIqdTVzdTX0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Andrés Leyton, defensor del humedal La Conejera] 

**Desde las seis de la mañana cien agentes de la fuerza pública hacen presencia en el campamento que defiende el humedal La Conejera** para desalojar a las personas que llevan más de cuatro meses frente a la constructora, con el objetivo de realizar un ejercicio de resistencia y  veeduría en defensa del Humedal que se ve en riesgo por la realización del proyecto residencial Reserva del Fontanar.

Andrés Leyton, una de las personas que hace parte del campamento que defiende el humedal La Conejera, asegura que **primero llegó el ESMAD y la policía y más tarde llegaron funcionarios de la Alcaldía de Suba** **con la orden de desalojo** por invasión al espacio público y privado.

Según la versión de Leyton, cuando las personas del humedal se percataron de la presencia de la policía, uno de los defensores decidió ir a revisar y **los policías lo retuvieron ilegalmente durante dos horas,** además denuncian que al inicio la fuerza pública llegó sin ningún tipo de orden de desalojo y aun así destruyeron una parte de la huerta que hace parte de una acción que emprende la comunidad y los ambientalistas para recuperar el ambiente.

Los defensores y defensoras, aseguran que **hay  personas infiltradas de la policía que están criminalizando la protesta pacífica,** y son los que empiezan los enfrentamientos contra el ESMAD. Es por eso que las personas del campamento decidieron ** levantar la carpas como acción pacífica,** pero permanecerán en el predio realizando actividades como batucadas y talleres de ecoyoga.

“Pedimos que se garantice el debido proceso, hasta que no haya un fallo no debe seguir la construcción”, asegura el defensor de La Conejera, quien agrega que por parte del distrito no ha habido ningún cumplimiento de los compromisos que habían adquirido con los ambientalistas, en cambio **la alcaldía solo está garantizando la propiedad privada,** dice Leyton.

[![11215465\_10153138945230020\_1731373179\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/11215465_10153138945230020_1731373179_o.jpg){.alignleft .wp-image-8073 width="605" height="454"}](https://archivo.contagioradio.com/defensores-de-la-conejera-permaneceran-en-el-humedal-pese-a-orden-de-desalojo/11215465_10153138945230020_1731373179_o/) [![11201425\_10153138945755020\_1583470199\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/11201425_10153138945755020_1583470199_o.jpg){.alignleft .wp-image-8074 width="603" height="452"}](https://archivo.contagioradio.com/defensores-de-la-conejera-permaneceran-en-el-humedal-pese-a-orden-de-desalojo/11201425_10153138945755020_1583470199_o/) [![11202750\_10153138945685020\_1111336957\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/11202750_10153138945685020_1111336957_o.jpg){.alignleft .wp-image-8075 width="607" height="455"}](https://archivo.contagioradio.com/defensores-de-la-conejera-permaneceran-en-el-humedal-pese-a-orden-de-desalojo/11202750_10153138945685020_1111336957_o/)
