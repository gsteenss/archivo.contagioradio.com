Title: No es cuestión de olvidar y perdonar, es cuestión de reconocer y cambiar: Lederach
Date: 2019-04-10 22:26
Author: CtgAdm
Category: Memoria, Paz
Tags: comision de la verdad, Sistema Integral de Justicia
Slug: no-es-cuestion-de-olvidar-y-perdonar-es-cuestion-de-reconocer-y-cambiar-lederach
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D3zHHDrX4AUTPug.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-10-at-5.33.03-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comisión de la verdad] 

Durante el conversatorio titulado '¿Qué significa atribuir responsabilidad en el ámbito extrajudicial?' que tuvo lugar en la Universidad Javeriana y que contó con la participación del presidente de la Comisión de la Verdad, **el padre Francisco de Roux, el académico y experto en Construcción de la Paz Internacional, John Paul Lederach**, se evaluaron los retos que debe enfrentar esta entidad en comparación con otros acuerdos de paz que han contado con sistemas similares.

Como introducción al tema, De Roux, destacó los aspectos que rodean el concepto de verdad y la importancia de reconocer las responsabilidades que han ejercido diferentes actores a lo largo de más de 50 años de conflicto armado en el país, invitando a la liberación del miedo de aceptar responsabilidades, "veamos la grandeza humana que parte de la conciencia de que  podemos ser juntos la nación que nos merecemos ser"

Por su parte, el profesor **John Paul Lederach** hizo un análisis desde la óptica internacional sobre las dinámicas de polarización que vive el país y en particular un comparativo de la Comisión de la Verdad de Colombia con otros países que acogieron el mismo mecanismo.

### **Otras Comisiones de la Verdad en el mundo** 

El académico seleccionó un total de 34 procesos de paz de alrededor del mundo y analizó su desempeñó a través de las décadas, destacando que de estos 34, tan solo 12 adoptaron mecanismos de conocimiento de la verdad, de los cuales dos fallaron totalmente, del resto, se pudieron ver resultados positivos después del séptimo año en que fueron instaurados, en paralelo del caso de Colombia, el profesor indicó que **ninguno de estos 34 procesos había hecho tal énfasis en los temas de reparación a las víctimas como si el acuerdo de paz firmado entre Gobierno y Farc.**

De igual forma señaló que la Comisión se enfrenta en particular a tres retos, sin contar la disminución en el presupuesto que sufrió este año, dichas problemáticas las resume en el ambiente polarizado que vive el país, la extensa historia de conflicto de más de medio siglo y el corto tiempo con el que cuenta para cumplir su objetivo.

Finalmente, el experto en construcción de paz hizo énfasis en la reparación colectiva, la cual no depende de confesiones de culpa y puede comenzar a ser instaurada, otorga cohesión a los tejidos sociales de las comunidades y restaura la sensación de confianza, "no es cuestión de olvidar y perdonar, es cuestión de reconocer y cambiar" concluyó el profesor quien destaca que las regiones más afectadas por la violencia han sido las pioneras en la construcción de paz.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
