Title: Campesinos del Tolima exigen cese al fuego este 9 de Abril
Date: 2015-04-08 19:52
Author: CtgAdm
Category: Nacional, Paz
Tags: 9 de abril, astracatol, Cese al fuego, paz, Tolima
Slug: campesinos-del-tolima-exigen-cese-al-fuego-este-9-de-abril
Status: published

###### Foto: Astracatol 

<iframe src="http://www.ivoox.com/player_ek_4326049_2_1.html?data=lZifmJWYfY6ZmKiakp2Jd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Lh0crgy9PTt4zYxtGYttTQrc7VjNjSjdLTusrgyt%2FO1Iqnd4a1ktOYx9GPfYzYxpDOxNfNsIzX0NOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [William Acosta, Semillas y Memoria] 

Nuevos enfrentamientos entre el Ejercito y las FARC en el sur del Tolima, municipio de Rio Blanco, Chaparral, ponen en riesgo el cese al fuego unilateral decretado por esta guerrilla a partir del 20 de diciembre del 2014, y la vida de las campesinas y campesinos de este departamento del país.

Según denuncian las organizaciones de Derechos Humanos de la región, el 5 de abril se presentaron enfrentamientos entre el Ejercito y el Frente 21 de las FARC, que también han afectado de manera directa a las comunidades campesinas que "siguen siendo perseguidas y judicializadas de forma ilegal. Son muchos los campesinos que se encuentran en las cárceles de Colombia", señala William Acosta de la organización de DDHH Semillas y memoria.

En los últimos días, los militares "estuvieron en el municipio de Dolores, en la vereda Vegas del Café, y ahí entraron a las viviendas, hicieron censos y otro tipo de cosas que prohibe el Derecho Internacional Humanitario", enfatizó.

Es por este motivo que las y los campesinos han decidido movilizarse el próximo 9 de abril, para exigir el cese bilateral al fuego y de agresiones por parte del Ejercito a estas comunidades. Las organizaciones esperan reunir a 5 mil campesinos y campesinas en la ciudad de Ibagué.
