Title: Sara Curruchich: tradición y resistencia indígena a través de la música
Date: 2019-08-26 18:41
Author: CtgAdm
Category: Comunidad, Cultura
Tags: Guatemala, Indígenas en Lationamérica, Música, Sara Curruchich
Slug: sara-curruchich-tradicion-y-resistencia-indigena-a-traves-de-la-musica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sara-Curruchich.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sara-2.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio ] 

La artista Sara Curruchich es proveniente del pueblo maya Kaqchikel, una de las 25 comunidades que conforman el 40% de la población de Guatemala, hoy con su álbum 'Somos' y a través de 13 canciones, Sara canta a la vida y la resistencia de las comunidades indígenas de Guatemala y Latinoamérica que a diario se enfrentan a problemáticas como el racismo y la discriminación.

El pueblo Kaqchikel, palabra que traduce **'piel roja' o 'boca roja'**  ha sido, según la artista una población con "una historia manchada con sangre", pero por encima de ello ha sido una comunidad con un legado resistente, la misma que vio a Sara Curruchich, crecer y aprender a tocar guitarra, a mismo que la ve llegar y partir cada cierto tiempo para viajar con su guitarra 'Parutz'- palabra que traduce 'Flor de muerto' - y entonar sus melodías alrededor del mundo llevando el mensaje de sus raíces.

Sara quien es actualmente profesional de la Escuela Normal para Maestros de Música Jesús María Alvarado, relata que comenzó a escribir cuando **sintió que como mujer indígena tenía mucho que decir y aportar con su juventud** a la reivindicación de las comunidades indígenas, "dicen que los jóvenes son el futuro, pero los jóvenes somos el presente y estamos caminando", resalta.

La guitarrista se refiere al reto que representa surgir a través de la música en medio de  un mundo, donde se es "triplemente discriminada, por ser mujer, indígena y provenir de un estrato social más bajo, pero destaca el valor de la mujer indígena, personas que ayer hoy han sido poseedoras de "mucho conocimiento y tejedoras de esperanza y de un camino de justicia con dignidad".

<iframe src="https://www.youtube.com/embed/PSM7LnynZjU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### La música como respuesta a la criminalización

"Es doloroso que los gobiernos estén a favor de la industrialización, del despojo de tierras y la criminalización" afirma la artista destacando que existe una lucha muy grande e las comunidades indígenas por exigir una mayor participación, una situación que también se ha visto reflejada en Colombia donde organizaciones indígenas se han manifestado con fuerza por sus derechos como en el caso de la Minga Nacional por la Vida.

Tanto como la pobreza, la desnutrición, el asesinato de defensoras y defensores y la pérdida de saberes como las lenguas tradicionales, son algunas de las problemáticas que comparten las comunidades del continente, "aunque nos dividan ríos o mares muchas de las luchas de los pueblos son similares, la conexión con la tierra y su defensa la mantienen todos los pueblos indígenas",  afirma Sara. [(Le puede interesar: 14 hechos de violencia contra los DD.HH. de la Minga en Cauca)](https://archivo.contagioradio.com/como-avanza-la-minga-en-la-defensa-de-dd-hh-de-los-pueblos-indigenas/)

\[caption id="attachment\_72616" align="aligncenter" width="888"\]![Sara-](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sara-2.png){.size-full .wp-image-72616 width="888" height="564"} Foto: Contagio Radio\[/caption\]

### "Al escribir estas canciones, abordo realidades actuales, de la historia y de la memoria"

La artista guatemalteca destaca el trabajo que realizaron en Colombia junto a Edwin Quiñones, o como es mejor conocido: Kizú, palabra original de Angola que traduce 'hecho para la música', quien ha puesto el ritmo de la marimba a las composiciones de Sara, reflejando la conexión que existe entre diversas culturas a la hora de elaborar música.

> "La música nos permite crear un vínculo y una conexión directa con otras culturas (...) es un universo maravilloso que nos permite, pese a estar separados sentir el latir del tambor como el nuestro".

Respecto a sus composiciones, expresa que pueden darse con una primera palabra o una primer melodía, "lo cierto es que casi siempre vienen por la noche a través de los sueños", explica la compositora, haciendo alusión a la cosmovisión del pueblo maya que sugiere que los antepasados se comunican con sus descendientes para guiarlos en  su camino.

**"La música es como un tejido que tiene muchos hilos y sonoridades"**, apunta la cantante refiriéndose a los pasos que tuvo que dar para lanzar su primer álbum y la forma en que su trabajo reivindica el rol de muchas artistas guatemaltecas que antecedieron a Sara Curruchich y que debido a la guerra dejaron de escribir y componer,"las lucha no son individuales, buscan un bien común y la música es un canal, un instrumento de renacimiento, de armonía y de reconstrucción de tejido social".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
