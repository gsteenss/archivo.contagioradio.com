Title: Familias Cocaleras ya tienen Plan de Sustitución de Cultivos
Date: 2017-02-20 13:57
Category: Nacional, Paz
Tags: Asociación Nacional de Zonas de Reserva Campesina., COCCAM, Familias cocaleras, Implementación de los Acuerdos de paz
Slug: familias-cocaleras-ya-tienen-plan-de-sustitucion-de-cultivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/familias-cocaleras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Amazonía al Día] 

###### [20 Feb 2017] 

Comunidades de la Zona de Reserva Campesina ‘La Perla’ en el Putumayo, han denunciado nuevas erradicaciones forzadas en cultivos de coca, por parte del Ejército, señalan que desde el 2016 el Gobierno **viene incumpliendo lo pactado en La Habana para el tema de sustitución de cultivos e ignora la agenda conjunta propuesta por familias cocaleras** desde la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana –COCCAM–.

Janny Silva una de las lideresas de la Zona de Reserva, manifestó que las comunidades cultivadoras se encuentran bastante preocupadas, pues **“si a las FARC no le cumplen con lo pactado, nos preguntamos cómo será con nuestro acuerdo de sustitución”**. ([Le puede interesar: “Plan de sustitución voluntaria no se está cumpliendo”: COOCCAM](https://archivo.contagioradio.com/plan-de-sustitucion-voluntaria-no-se-esta-cumpliendococcam/))

Hace aproximadamente un año, los cultivadores de coca han denunciado ante distintas autoridades los riesgos que suponen las erradicaciones forzadas para su integridad, las de sus familias, su sostenimiento económico y la ruptura del tejido social, pues se ha vuelto una constante **las amenazas por parte del Ejército y han sido "señalados de ser narcotraficantes".**

La lideresa indica que dichas acusaciones ponen en riesgo la vida de los campesinos y “no están en la misma vía de lo pactado en los acuerdos de paz de la Habana así como de su implementación”. ([Le puede interesar: Erradicaciones forzadas de coca no contribuyen a la paz](https://archivo.contagioradio.com/campesinos-del-catatumbo-dicen-no-a-erradicacion-forzada-de-coca/))

### Ya están listos los proyectos pero no hay garantías 

Por otra parte, Silva señalo que la visita que anunció el presidente Juan Manuel Santos al territorio debe servir para **“darse cuenta por si mismo de lo que esta pasando, de los incumplimientos de su Gobierno”**. Resaltó que dicha visita es fundamental para que las familias puedan dialogar con el presidente y le manifiesten sus exigencias.

Silva comentó que ya los proyectos productivos y de sustitución colectivos “están listos, **estamos esperando que hayan garantías para iniciarlos y socializarlos con las familias cocaleras de distintas regiones”.**

Las comunidades exigen celeridad a la implementación y garantías efectivas de seguridad, pues “las amenazas no paran, hace 3 semanas mataron a un compañero hombres encapuchados, y siguen sin hacer nada”. También hizo un llamado al Gobierno para que **atienda la precaria situación que viven los integrantes en las FARC en la Zona Veredal ‘La Carmelita’ “y sobre todo las madres gestantes y lactantes”.**

Por último, César Jerez vocero de la Asociación Nacional de Zonas de Reserva Campesina, reveló que "hay más de 500 puntos en donde el Gobierno quiere llevar a cabo erradicaciones forzadas y **allí encontrarán una resistencia y acción masiva de protesta de los sectores cocaleros”.**

<iframe id="audio_17115163" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17115163_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
