Title: Humberto de la Calle pone a consideración su renuncia
Date: 2016-10-03 09:50
Category: Nacional, Paz
Slug: humberto-de-la-calle-pone-a-consideracion-su-renuncia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/241744Humberto-De-La-Calle-Perfil-Negociador-Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cablenoticias 

##### 03 Oct 2016

Esta es la carta de Humberto de la Calle al presidente Juan Manuel Santos:

*"Humberto de la Calle, 3 de octubre de 2016*

*Vengo a expresar total apoyo al P. Ha mostrado un liderazgo valiente. Valiente porq prefirió la paz a la inercia de la guerra. Valiente porque se sometió a la decisión de los ciudadanos.*

*La Paz no ha sido derrotada. Voceros del Centro Democrático han señalado q tienen objeciones sobre aspectos de lo acordado pero el deseo de paz es universal y unánime. Cómo siempre lo dije durante las conversaciones, respeto profundamente las opiniones en contra. Es el momento de la unión. Hay que buscar un Acuerdo Nacional.*  
*Continuaré persiguiendo el objetivo de la paz en lo que me resta de vida. Cualquiera q sea el escenario q me corresponda seguiré trabajando con tenacidad y entusiasmo.*  
*Los errores q hayamos cometido son de mi exclusiva responsabilidad. Asumo plenamente mi responsabilidad política.*  
*En consecuencia, he venido a decirle al Presidente q pongo a su disposición mi cargo de Jefe de la Delegación porque no seré obstáculo para lo que sigue. Pero repito q continuaré trabajando por la paz sin pausa en el lugar donde pueda ser útil.*  
*Agradezco al maravilloso equipo de negociadores en La Habana. El país tiene una deuda enorme con ellos."*
