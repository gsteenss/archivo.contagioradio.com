Title: DiPaz presenta a la CEV  informe que revela afectaciones del conflicto armado a expresiones de fe
Date: 2020-10-02 14:00
Author: AdminContagio
Category: Actualidad, Paz
Tags: colombia, conflicto armado, DIPAZ, evangélicos, paz
Slug: informe-de-dipaz-a-la-cev-revela-afectaciones-del-conflicto-armado-a-expresiones-de-fe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Dipaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: @Dipazoficial*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 2 de octubre el Diálogo Intereclesial por la Paz (DiPaz), presentó a la Comisión para el Esclarecimiento de la Verdad (CEV), **el informe, *"[Rol de los evangélicos en el conflicto colombiano"](http://dipazcolombia.org/dipaz-informe-cev-evangelicos-conflicto-colombiano/)*, un documento que da cuenta de las afectaciones de la violencia Colombia a la iglesia** y a las expresiones de fe.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/DiPazOficial/status/1312113890667368449","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/DiPazOficial/status/1312113890667368449

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El informe fue elaborado en el mes de septiembre del 2019 y **recopila más de 20 testimonios, desde 1958 hasta 2016, por diferentes sectores de fe en un aporte para la[construcción de la verdad](https://archivo.contagioradio.com/se-instalo-el-mural-quien-dio-la-orden-2-0/)**, poniendo en evidencia no solamente el trabajo de resistencia y mantención de la esperanza en los diferentes territorios, sino también las afectaciones en la comunidad de fe por parte de los actores armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El escrito del DiPaz se compone de tres capítulos; iniciando con la indagación sobre los avances y retrocesos en materia de incidencia pública de las iglesias, a éste le siguen las **afectaciones de las personas del sector de iglesias no católicas en las regiones del sur occidente,** así como la importancia de estos en el conflicto; y finaliza, con la perspectiva de la participación política de las iglesias, tanto del respaldo, como de la desaprobación del proceso de paz por parte de las expresiones de fe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la entrega del informe asistió, **Pablo Moreno, rector de la universidad Bautista de Colombia y Jenny Neme, representante de la iglesia menonita, ambos integrantes del DiPaz**. Junto a ellos lideresas y lideres religiosos de varias regiones de Colombia, y en representación de la CEV, recibieron el informe, el presidente de la comisión Francisco de Roux , junto a la comisionada Lucía González.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este informe evidencia la posibilidad de ser *"semilla de esperanza en medio del conflicto a través de las respuestas de las iglesias para recuperar la esperanza"*, palabras del pastor Pablo Moreno, indicando que el trabajo del DiPaz además, representa la **importancia de la construcción de la verdad para superar las múltiples violaciones de DDHH pese a la realidad** en donde persiste en distintos territorios el conflicto armado y las violaciones a los Derechos Humanos.

<!-- /wp:paragraph -->
