Title: Después de 5 meses, ANLA sanciona a EPM por sólo 2.420 millones de pesos
Date: 2018-09-13 15:52
Author: AdminContagio
Category: Ambiente, Movilización
Tags: Autoridad Nacional de Licencias Ambientales, EPM, Hidroituango
Slug: despues-de-5-meses-anla-sanciona-a-epm-por-solo-2-420-millones-de-pesos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/De4luDzXkAA2qVR-e1528309998898.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Claudia Julieta Duque] 

###### [13 Sept 2018] 

Tras 5  meses de ocurrido el desastre ambiental y social producido por Hidroituango en municipios del bajo Cauca antioqueño, la Autoridad Nacional de Licencias Ambientales (ANLA), sancionó con una multa por **2.419.681.474 millones de pesos a la empresa que maneja el proyecto, por daños ambientales**. Sin embargo, aún no hay ningún tipo de medida sobre el daño social y económico provocado a las comunidades aledañas.

El vocero del movimiento Ríos Vivos, Miller Dussán, la sanción de la ANLA, es "mínima" y el máximo responsable de las fallas de la hidroeléctrica, su construcción y las afectaciones ambientales sería la ANLA **"por no haber cumplido con su función constitucional de ejercer un estricto control y seguimiento al proyecto"**. (Le puede interesar: ["ANLA y EPM responsables por la emergencia de Hidroituango: Ríos Vivos"](https://archivo.contagioradio.com/anla-y-epm-son-responsables-por-la-emergencia-de-hidroituango-rios-vivos/))

La sanción impuesta por la ANLA, señala que se produce por dos hechos: la primera es la falta de medidas tomadas por Hidroituango cuando a su licencia ambiental se adiciona el proyecto de la vía de San Ándres de Cuerquia, en el 2009. El segundo motivo es por la construcción de una planta de trituración que no había sido autorizada.

### **Es más fácil pagar sanciones que desmontar Hidroituango** 

Dussán señaló que este tipo de empresas como Hidroituango, tienen previsto desde su planes de inversiones el pago de multas, "porque saben que van a actuar irregularmente, y les sale más barato pagar la multa que la suspensión el proyecto", razón por la cual aseguró que la pregunta que debe hacerse a la ANLA es, **por qué aún no se ha suspendido este proyecto**, si incluso la Contraloría General de la Nación ha alertado del riesgo del mismo. (Le puede interesar:["EPM perdió el control sobre Hidroituango: Contraloría"](https://archivo.contagioradio.com/epm-perdio-el-control-sobre-hidroituango-contraloria/))

Además, afirmó que Hidroituango interpondrá un recurso de apelación a la multa para no pagarla, y lo problemático sería que la ANLA no tome acciones y permita que suscriba la acción. Asimismo, informó que en la Fiscalía desde hace 30 años hay otra demanda por daños ambientales interpuesta sin ninguna respuesta por parte de esta autoridad.

<iframe id="audio_28587679" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28587679_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
