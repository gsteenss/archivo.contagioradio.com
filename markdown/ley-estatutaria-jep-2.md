Title: ¿Qué viene para la Ley Estatutaria de la JEP?
Date: 2019-03-21 18:02
Category: Entrevistas, Paz
Tags: Congreso, Corte Constitucional, JEP, Ley estatutaria
Slug: ley-estatutaria-jep-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Diseño-sin-título-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Mar 2019] 

Este miércoles la Corte Constitucional se declaró inhibida para decidir sobre las objeciones presidenciales a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), no obstante, indicó que dicho control constitucional se producirá cuando el Congreso sesione frente al tema antes del 20 de junio,  decisión que fue calificada como prudente por parte del **Representante a la Cámara, David Racero**, considerando el impacto que tiene tal determinación.

Esta decisión de la Corte fue ocasionada por una consulta que recibió por parte de **Alejandro Chacón, presidente de la Cámara de Representantes**, en la que se pedía aclarar la función del congreso para tomar decisiones sobre las objeciones; teniendo en cuenta que aunque las objeciones presidenciales fueron presentadas por inconveniencia, en realidad obedecerían a razones jurídicas previamente resueltas por el alto tribunal.

Ante la posibilidad difundida por varios medios de comunicación de que la Corte dejara sin piso jurídico las objeciones, diferentes analistas jurídicos señalaron que el ente constitucional podría cometer prevaricato, pues entre sus funciones no está resolver consultas de este tipo. Por esa razón, y **aunque Racero esperaba un pronunciamiento "más contundente", indicó que el mismo fue cauteloso** y enmarcado en sus funciones legales.

En ese sentido, el Congresista afirmó que hay una tranquilidad para que el Congreso discuta las objeciones, al tiempo que hay una tranquilidad para las víctimas; en primer lugar porque **Patricia Linares, presidenta de la JEP, ha dicho que esta Institución seguirá funcionando más allá de la Ley Estatutaria**; y en segundo lugar, porque la Corte también indicó que haría el control constitucional sobre lo que ocurra en el congreso, una vez terminado el trámite legislativo de las objeciones.

### **Las Comisiones accidentales conformadas para discutir las Objeciones** 

Para iniciar el procedimiento legislativo, tanto en Cámara como en Senado se tuvieron que conformar comisiones accidentales encargadas de presentar sus ponencias sobre qué decisión debería tomar el congreso en este tema. Las 3 opciones que podría tomar el organismo serían rechazar las objeciones y que el Presidente del Senado sancione la Ley como está, aceptar las objeciones y hacer modificaciones a la Ley para ser sancionada, o que ambas Corporaciones tomen decisiones diferentes, **en cuyo caso el proyecto se archivaría**.

Entre los elegidos por Chacón para hacer parte de la Comisión Accidental de la Cámara están Jairo Cárdenas (Unidad Nacional),  Juanita Goebertus (Alianza Verde), Jaime Lozada (Conservador), Carlos Ardila (Liberal), José López (Cambio Radical), David Racero (Decentes) y Álvaro Prada (Centro Democrático). Esta conformación indica que habría mayorías en favor de rechazar las objeciones; pero en la Comisión del Senado, la balanza está inclinada en el otro sentido.

> Tan chévere.. comisión que estudiará objeciones del presidente Duque es super gobiernista: 6-3 a favor de saltarse el fallo de la [@CConstitucional](https://twitter.com/CConstitucional?ref_src=twsrc%5Etfw) y que los congresistas cometan el delito de prevaricato y falta disciplinaria <https://t.co/LyYNQDPJcs>
>
> — Angélica Lozano Correa (@AngelicaLozanoC) [20 de marzo de 2019](https://twitter.com/AngelicaLozanoC/status/1108365285453692928?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **No es una cuestión de números sino de argumentos: Racero**

La comisión de la Cámara se reunió informalmente el martes, y de forma oficial el miércoles para decidir la hoja de ruta que seguirán en este caso; allí acordaron que plantearían argumentos para llevar a la plenaria, y que llegarían sin pretensiones más allá de dialogar, pues como lo explicó Racero **"evidentemente cada quien ya tiene y asumió unas posiciones" sobre las objeciones.**

Por esta razón, el Representante por la Lista de Decentes indicó que **probablemente no saldría un argumento común de la Comisión**, lo cual también es beneficioso para el debate, porque cada persona puede presentar varias ponencias sobre el mismo tema y así enriquecer la democracia. (Le puede interesar: ["Declaran inconstitucionales las modificaciones del Centro Democrático a la JEP"](https://archivo.contagioradio.com/declaran-inconstitucionales-las-modificaciones-del-centro-democratico-la-jep/))

**"En esta comisión no vale el tema de mayorías, lo importante aquí serán los argumentos fundamentados",** manifestó Racero. Con este panorama, al Congreso no le queda sino discutir las objeciones y tomar las decisiones, para lo cual el Presidente de la Cámara garantizó los tiempos y espacios necesarios, pero recordando que el plazo máximo dado por la Corte para decidir sobre las objeciones será el 20 de junio.

<iframe id="audio_33595052" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33595052_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
