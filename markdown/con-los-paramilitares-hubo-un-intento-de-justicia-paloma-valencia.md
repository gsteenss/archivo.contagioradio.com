Title: "Con los paramilitares hubo un intento de justicia": Paloma Valencia
Date: 2016-09-20 14:39
Category: Nacional, Política
Tags: Claudia López, FARC, Paloma Valencia, paz, proceso de paz, Universidad Externado
Slug: con-los-paramilitares-hubo-un-intento-de-justicia-paloma-valencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Universidad Externado 

###### [20 Sep 2016** **] 

Luego del malestar que generó en algunos sectores políticos la publicación de un artículo dominical de The New York Times, donde se cuestiona la desmovilización de grupos paramilitares, el uribismo consideró que ese proceso fue más efectivo y menos impune, que el que alcanzó el actual gobierno con las FARC.

Durante un debate que se realizó este lunes en la Universidad Externado de Colombia, la senadora Paloma Valencia, del Centro Democrático señaló que **no hay nada “rescatable” del acuerdo de La Habana y que pese a las “buenas intenciones” del mismo,** se premia a responsables de delitos atroces con penas alternativas.

Al referirse a las críticas del diario estadounidense, que tuvo acceso a los expedientes de extradición de paramilitares colombianos, la congresista manifestó que “con los paras hubo un intento de hacer justicia” sin embargo “no pudimos ejecutarlo como los colombianos queríamos”.

“**No hay ni un solo jefe paramilitar que no haya pagado cárcel, a diferencia de los guerrilleros** (…) Los paras entregaron muy poquita plata, 80 mil millones. Las FARC no van a entregar un solo peso. Los paras contaron muy poquita verdad, hay 78 mil confesiones, vamos a ver cuántas salen de las FARC”, manifestó Valencia durante un foro en la Universidad Externado de Colombia.

Por su parte Claudia López, quien también asistió al debate, respondió que de poco sirve tener condenas de papel y muertos de verdad en medio de uno de los conflictos armados más largos de América Latina.

La congresista del Partido Verde detalló que se definió la creación de un Tribunal que va a juzgar los delitos cometidos en el conflicto armado. **Allí se estableció que quienes hayan cometido los crímenes más graves serán sancionados.** Si reconocen su responsabilidad recibirán penas de 5 a 8 años de restricción efectiva de la libertad. Si no reconocen su responsabilidad serán condenados hasta con 20 años de cárcel.

Sobre los costos de la paz, manifestó que mientras un guerrillero en la cárcel le cuesta al bolsillo de los colombianos unos 15 millones anuales, un desmovilizado en proceso de reintegración cuesta 6.

“En este sistema carcelario con elevadísimos niveles de hacinamiento, la posibilidad de reincidencia es del 70%. Por el contrario, según la experiencia que tenemos el 76% de la población desmovilizada no es reincidente. Es decir 7 de cada 10 se mantienen en la legalidad”, indicó la senadora.

 **¿Y si gana el no?**

Contrario a Paloma Valencia, Carlos Holmes Trujillo, rexcandidato vicepresidencial por el Centro Democrático y otro de los invitados al evento, dijo que ve varios aspectos positivos en el acuerdo pero **insiste en que deben reformarse otros para garantizar una paz estable y duradera.**

Pese a que la Corte Constitucional ya se pronunció al respecto, Holmes asegura que los acuerdos y en especial el Acto Legislativo para la Paz, violan la Constitución porque elevan lo pactado a "acuerdo especial para, según él, incorporarlo al Bloque de constitucionalidad.

**“El 2 de octubre el pueblo decidirá si está de acuerdo con una política pública del Gobierno Santos,** no si quiere la paz, porque la paz la queremos todos. Si gana el NO será un mandato al presidente para renegociar y de ser así, nosotros tendríamos el deber de contribuir para garantizar el éxito de ese proceso de reorientación de los acuerdos”, propuso.

Concluyendo el debate, la docente externadista Heidi Abuchaibe, experta en Negociación y Solución Alterna de Conflictos, refutó que un país desgastado, con un gobierno deslegitimado, no retomaría fácilmente una negociación con las FARC. Reconociendo que persisten retos como el ELN y otros grupos ilegales, indicó que nunca en la historia de Colombia habían un ambiente favorable para desarmar a las FARC: apoyo de la comunidad internacional y voluntad política.

**“¿Cuánto tardamos del Caguán hasta los diálogos de Cuba?** El pueblo colombiano debe pensar si está dispuesto a pagar el alto costo de rechazar estos diálogos”, indicó.

https://www.youtube.com/watch?v=mSAtbX6W6Bc&feature=youtu.be  
 
