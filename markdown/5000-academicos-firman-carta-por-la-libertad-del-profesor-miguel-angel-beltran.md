Title: 5000 académicos firman carta por la libertad del profesor Miguel Ángel Beltrán
Date: 2015-11-18 09:39
Category: DDHH, Nacional
Tags: Justice for Colombia, Profesor Miguel ángel Beltrán
Slug: 5000-academicos-firman-carta-por-la-libertad-del-profesor-miguel-angel-beltran
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/profesor-miguel-angel-beltran.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 de Nov 2015] 

El sindicato de profesores universitarios británico **UCU y la ONG británica Justice for Colombia** ha promovido a través de una carta con más de 5000 mil firmas de académicos del mundo, celeridad  en el caso del profesor, sociólogo e historiador **Miguel Ángel Beltrán**, recluido en la  cárcel ERON - Picota de Bogotá desde julio de este año.

La carta será entregada en la Embajada de Colombia en Londres y en la a Corte Suprema de Justicia en ciudad de Bogotá por integrantes de las organizaciones promotores de esta acción.

Según palabras de la Presidenta del sindicato británico UCU Sally Hunt: “Esta petición envía un mensaje claro a las autoridades colombianas que la comunidad internacional no aceptará intimidaciones en contra de los académicos universitarios o a la libertad académica. Pedimos a las autoridades en Colombia que actúen para asegurar que el caso del Doctor Beltran se resuelva en una manera rápida y justa.”

El profesor Miguel Angel Beltrán  fue absuelto en julio de 2011 por los delitos de concierto para delinquir agravado y rebelión, la jueza 4ta Penal Especializada de Bogotá señaló  que ninguna de las pruebas presentadas podían confirmar la idea que el profesor fuera ideologo de  las FARC- EP, con el alias “Jaime Cienfuegos”, falló que fue revocado por l[a Sala Penal del Tribunal Superior de Bogotá que nuevamente en diciembre de 2014 condenó al profesor Beltrán  a 8 años de cárcel.  (Ver "[Ataques contra las universidades tienen fines políticos" Miguel Angel Beltrán](http://bit.ly/1kEwhvn)) ]{.a}

[5,000 academicos firman para pedir resolucion en el caso del profesor Miguel Beltran](https://de.scribd.com/doc/290172978/5-000-academicos-firman-para-pedir-resolucion-en-el-caso-del-profesor-Miguel-Beltran "View 5,000 academicos firman para pedir resolucion en el caso del profesor Miguel Beltran on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_99631" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/290172978/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ruaqZV9m1IwUeWjPqVH6&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

<div style="text-align: justify;">

</div>

<div class="ff2" style="text-align: justify;">

</div>

L
