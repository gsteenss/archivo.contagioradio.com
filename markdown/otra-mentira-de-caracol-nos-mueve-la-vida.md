Title: Otra mentira de Caracol: "nos mueve la vida"
Date: 2015-08-06 14:38
Category: Opinion, Ricardo
Tags: Caracol television, indígenas, Séptimo Día., Wayuu
Slug: otra-mentira-de-caracol-nos-mueve-la-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/teodoro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caracol TV] 

##### **[Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) - @ferr\_es** 

###### [7 Ago 2015] 

**Caracol tiene como lema “Nos mueve la vida”, pero ha puesto una lápida sobre la dirigencia indígena de Colombia.**

Siguiendo los libretos de la  propaganda sucia, el programa ***Séptimo Día***, dirigido por Manuel Teodoro, induce a creer que la mayor parte de los cabildantes, gobernadores y autoridades indígenas son violadores, ladrones, guerrilleros y terroristas. En la destrucción de imagen, el blanco se deshumaniza y luego se aniquila.

El título de la serie informativa sonaba interesante: “**¿La corrupción llegó a los cabildos indígenas?**” (26 de julio y 2 de agosto. El próximo será emitido el 9 de agosto 2015). ***Séptimo Día*** parte de hechos puntuales y los proyecta como hechos generalizados en todas las organizaciones indígenas, de todas las etnias. El pueblo Nasa, los Guambianos, los Wayuu, las organizaciones indígenas del Tolima... todo cabe en el mismo paquete, porque todos son iguales, con los mismos vicios. La “Investigación” de ***Séptimo Día*** exuda racismo.

**Lo primero que se ataca es la autonomía indígena**, un pequeño logro de la Constitución de 1991, tras siglos de arrasamiento contra la población nativa de Colombia.

El programa de caracol delata su intencionalidad cuando incluye como fuentes a los voceros del Centro Democrático, el engendro político de las bandas criminales uribistas.

**¿Será que a Caracol tenemos que recordarle la relación directa entre Centro Democrático – Álvaro Uribe – paramilitarismo – masacres – desplazamiento – usurpación de tierras?  \[1\]** ¡En esta “investigación”, Caracol asigna el papel de acusadores, testigos y fiscales a los mismos victimarios!  Fungen como acusadores los terratenientes del Cauca, las mismas familias que usurparon las tierras que pertenecían a los Resguardos.

En 1987 entrevisté al anciano Lorenzo Lectamo, Médico Tradicional en la vereda Santa Marta, de Mosoco \[Mus cué\] Cauca. Los amigos de Lectamo me contaron las matanzas que hizo el Ejército de Colombia durante la dictadura de Gustavo Rojas Pinilla. Cuando llegaron los soldados, los vecinos de Tálaga se escondieron en las cuevas de arenisca y piedra, justo en la cuesta, donde pudieron escuchar los disparos. En la vereda Santa Marta, de Mosoco, Yandi de Tenorio recordó la masacre de San José. Una noche entera de disparos e incendios. En la mañana los vecinos vieron el lugar: muertos por todas partes y entre ellos, un bebé, vivo, sano. Tenorio y Yandi criaron a ese bebé. Como a un hijo propio. Yandi de Tenorio me dijo con lágrimas en los ojos: **“esa noche nos mataron como a ratoncitos y nadie hizo nada”**.

Luego de las masacres, la mayor parte de las tierras indígenas del Cauca fueron repartidas entre unas pocas familias. Y esas familias, aun detentan el poder político. En el Congreso se repiten hasta hoy esos nombres y apellidos, como una pesadilla  congelada en el tiempo.

Esos genocidas son los permanentes acusadores de las Autoridades Indígenas de Colombia.

El hilo conductor, según el relato de ***Séptimo Día*** es que la dirigencia indígena en general es corrupta, que viola, hurta y es terrorista. Que su ley no sirve, que debe ser derogada (“Armonizada”) Ahora viene la pregunta: ¿Qué intencionalidad subyace en esta ofensiva mediática contra los pueblos indígenas? Una pista la aportaron las “periodistas” cuando cuestionan el mecanismo de Consulta previa a las comunidades...   Ahí saltó la rana verde, croando verdades y ellos mismos delataron sus intenciones.

Resulta que la Consulta previa a las comunidades es el mecanismo mediante el cual las comunidades deben ser informadas y éstas pueden decidir si permiten que en su territorio se ejecuten proyectos como infraestructuras, redes, canales, carreteras, y... **proyectos mineros.**

Si hacemos memoria de la minería depredadora del hábitat y la vida, todo cuadra: Las comunidades indígenas del Cauca, Guajira, Tolima, mencionadas en el programa de Caracol, han hecho sus consultas internas y se han negado a ceder su entorno vital a las empresas explotadoras de gas y petróleo, minas de oro y otros recursos.

¿Fue casualidad que las masacres de los paramilitares contra las comunidades Wayuu ocurrieran a pocos días de que se confirmara la existencia de yacimientos de gas en la Guajira? \[2\]. Si es desplazada o muere la población Wayuu, las multinacionales ya no necesitarán Consulta previa y podrán explotar los hidrocarburos. El programa de Caracol debe ser tomado como un campanazo de alerta, porque repite la matriz de la depredación, el diferente que no merece existir. Está en peligro la vida de los indígenas, su derecho a la organización comunitaria, y la integridad del territorio.

###### \[1\] Imágenes de Uribistas en la cárcel. 

###### <https://www.google.com/search?q=uribistas+en+la+carcel&tbm=isch&tbo=u&source=univ&sa=X&ved=0CCUQsARqFQoTCK_DltjLjscCFcIrHgodlkIGHQ&biw=1024&bih=633> 

###### **\[2\]** Domingo 19 de Abril de 2015 - 3:00pm 

###### **Con el Yanama, etnia wayuu recordó 11 años de la masacre de Bahía Portete** 

###### <http://www.elheraldo.co/la-guajira/con-el-yanama-etnia-wayuu-recordo-11-anos-de-la-masacre-de-bahia-portete-192026> 

###### **Descubren gigantesco yacimiento de gas metano en Colombia** 

###### <http://www.informador.com.mx/economia/2008/43516/6/descubren-gigantesco-yacimiento-de-gas-metano-en-colombia.htm> 
