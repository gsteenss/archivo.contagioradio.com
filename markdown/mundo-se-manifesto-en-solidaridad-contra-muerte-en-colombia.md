Title: El mundo se manifestó en solidaridad y contra la muerte en Colombia
Date: 2019-07-27 17:12
Author: CtgAdm
Category: DDHH, Movilización
Tags: 26 de julio, Lideres, Movilización
Slug: mundo-se-manifesto-en-solidaridad-contra-muerte-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/25.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

[El 26 de Julio el mundo se solidarizó con los líderes, lideresas, defensores, defensoras de derechos humanos y  excombatientes de Colombia, ¿la razón? la protección de la vida en el país, más 130 ciudades hicieron parte de “El Grito”. (Le puede interesar: ["Minuto a minuto de la movilización por la vida de las y los líderes \#ElGrito"](https://archivo.contagioradio.com/minuto-a-minuto-de-la-movilizacion-por-la-vida-de-las-y-los-lideres-elgrito/))]

[La movilización no solo es un grito, es una exigencia para que los asesinatos y amenazas contra los líderes sociales paren ya, diferentes organizaciones, como DiPaz, Defendamos la Paz, Gempaz, Congreso de los Pueblos, Comisión de la verdad,  comunidad LGTBI, incluso partidos políticos como el Polo Democrático, Unión Patriótica, FARC,  se sumaron a la movilización en Bogotá y en varias ciudades del país.  
]

https://twitter.com/SwedeninCOL/status/1154882162786787328

[A su vez en otras ciudades como Berlín, Madrid, Valencia, París, Nueva York, se realizaron distintas actividades como plantones y velatones. ]

https://twitter.com/ColHumanaEuropa/status/1154880942324031493

 

[Jesús Abad Colorado, fotoperiodista quien se ha dedicado a mostrar las atrocidades de la guerra aseguró que "era un día para marchar y no solo solidarizarse con los líderes y sus familias sino  por la vida de un país que tiene que dejar atrás tanta violencia, especialmente la de la clase política".]

Muchos de los asistentes en Bogotá emplearon sobre su pecho imágenes de líderes sociales asesinados, y también a los largo de la movilización se puedo apreciar  un telón con los nombres de quienes han sido masacrados por pensar distinto.

https://twitter.com/cataoquendo/status/1154984779412115456

https://twitter.com/IvanCepedaCast/status/1154902227548549126

 

Uno de los hechos que marcó la movilización en Cartagena fue el abucheo al presidente Duque quien tuvo que abandonar la marca en medio de gritos por parte de cientos de personas que recazaron su presencia.

https://twitter.com/Contagioradio1/status/1154939523555581952

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
