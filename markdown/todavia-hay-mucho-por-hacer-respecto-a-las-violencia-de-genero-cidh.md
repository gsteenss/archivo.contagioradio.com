Title: Hay mucho por hacer respecto a las violencia de género: CIDH
Date: 2015-11-18 16:25
Category: Mujer, Nacional
Tags: CIDH, Corte Interamericana de Derechos Humanos, maltrato a las mujeres, OEA, víctimas de violencia, violencias contra las mujeres en Colombia, violencias de género
Slug: todavia-hay-mucho-por-hacer-respecto-a-las-violencia-de-genero-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Violencias-de-género.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [diarioelmirador.com.ar]

###### [18 Nov 2015]

En el informe **“Estándares jurídicos: igualdad de género y derechos de las mujeres**”, publicado por la Comisión Interamericana de Derechos Humanos (CIDH) este miércoles, se constata que aunque los países adscritos a la OEA han implementado políticas para combatir esta problemática, **continúan presentándose casos de violencia de género. **

El documento señala que las formas de discriminación hacia las mujeres abarcan un amplio espectro de manifestaciones de violencia; entre ellas se encuentran: **la violencia sexual, la discriminación, el abuso por parte de agentes del Estado o particulares y la solución de los problemas mediante la vía judicial.**

Es este último aspecto uno de los que más preocupa al organismo, puesto que las garantías de las mujeres víctimas de la violencia de género para acceder a la justicia son limitadas en varios casos que ocurren en el marco del conflicto armado o al interior de poblaciones marginales, indígenas y rurales.

La CIDH ha manifestado reiteradamente que la impunidad en los crímenes que atentan en contra de los Derechos Humanos, obstaculizan la vía democrática de los Estados; **“ello exige la adopción de medidas inmediatas y comprehensivas para combatir este grave problema”,** dice el informe.

[Informe CIDH Mujeres](https://es.scribd.com/doc/290244981/Informe-CIDH-Mujeres "View Informe CIDH Mujeres on Scribd")

<iframe id="doc_96542" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/290244981/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
