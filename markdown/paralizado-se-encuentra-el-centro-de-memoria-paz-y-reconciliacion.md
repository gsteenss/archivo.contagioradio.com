Title: Paralizado se encuentra el Centro de Memoria Paz y Reconciliación
Date: 2016-08-01 17:28
Category: Nacional, Paz
Tags: Centro de Memoria Paz y reconciliación
Slug: paralizado-se-encuentra-el-centro-de-memoria-paz-y-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/centro-de-memoria-historica-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:revistaaxxis] 

###### [1 de Agos] 

El Centro de Memoria Paz y Reconciliación se encuentra paralizado **debido al bajo presupuesto y el recambio de personal que se está dando al interior de la institución producto de la nueva administración de Bogotá**, este hecho ha generado que no se de la transición en las contrataciones y que la entidad solo funcione con 5 personas, **disminuyendo la capacidad de trabajo y tareas en un momento tan álgido como lo es el proceso de paz en Colombia**.

En la actualidad el presupuesto del Centro de Memoria es de **3.000 millones de pesos al año,** una cifra bastante baja frente a el presupuesto de otros Centros de Memoria del país, como el de Medellin que esta cerca de los 5.000 millones de pesos. De acuerdo con Camilo González Posso, primer director del Centro de Memoria Paz y Reconciliación de Bogotá, el presupuesto ideal para el funcionamiento de la entidad es de **6.500 millones de pesos, monto que cubre el mantenimiento adecuado de la estructura y la nómina suficiente de personal.**

Sobre la contratación, González explicó que los contratos que tenía el personal del Centro de Memoria Paz y Reconciliación, eran temporales y que **debido al cambio de administración no se ha llevado a cabo la renovación de los mismos,** sin embargo, el plazo para realizar las convocatorias de contratación son periodos demasiado largos que dificultan las labores de la institución y la dejan trabajando a media marcha.

"Yo creo que el principal problema, es que está paralizado frente a la política nacional de la verdad , de la preparación para responder a las exigencias de la Comisión de la Verdad y de todas las tareas de reconciliación y no repetición del momento más histórica de Colombia, **en donde deberían estar todas las banderas desplegadas y el Centro lleno de gente trabajando por la paz**" afirma González .

La disminución de las actividades del Centro de Memoria Paz y Reconciliación se da en un [momento de emergencia y necesidad de un espacio para abrir debates](https://archivo.contagioradio.com/el-centro-de-memoria-paz-y-reconciliacion-celebra-tres-anos-con-musica/) sobre la refrendación del plebiscito por la paz y evidencia la falta de presupuesto suficiente para mantener en pie tanto las actividades como el mantenimiento de la institución.

<iframe src="http://co.ivoox.com/es/player_ej_12408029_2_1.html?data=kpehkp2Udpqhhpywj5aYaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCrs8_uhqigh6aVsMbnjLXc1djTaZO3jKjS0NnWs4zBxtLc1M7FcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
