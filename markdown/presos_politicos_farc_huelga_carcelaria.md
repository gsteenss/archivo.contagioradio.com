Title: Presos políticos de las FARC en Florencia inician huelga de hambre
Date: 2017-06-24 04:32
Author: AdminContagio
Category: Judicial, Resistencias
Tags: Acuerdos de paz de la Habana, Amnistia, FARC, Indulto, presos FARC
Slug: presos_politicos_farc_huelga_carcelaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ICRC 

###### [22 Jun 2017]

Presos políticos de las FARC-EP que se encuentran recluidos en el centro cacelario, EPMSC-Florencia, Caquetá, han decidido unirse a la jornada nacional carcelaria de denuncia, por lo que **empezarán una huelga de hambre y desobediencia civil pacífica a partir de las 00:00 horas del lunes 26 de junio.**

La decisión la toman ya que aseguran que el gobierno y específicamente el INPEC no les ha dado soluciones claras a sus peticiones frente a la liberación de las y los presos políticos, como se tiene establecido en el acuerdo final de paz firmado entre el gobierno y las FARC.

"La demora en los extradós judiciales, la negativa continua de las peticiones de libertades condicionadas, las demoras en los traslados de los guerrilleros presos a las ZVTN, las constantes trabas que los estrados judiciales ponen a la aplicación de la ley 1820 de 2016 y de el Decreto 277 de 2017, **en exigencia del cumplimiento inmediato de las normas y de lo pactado entre las partes en acuerdo de paz, la excarcelación y libertad de nuestros presos políticos así como la demora en la expedición de las certificaciones** por parte de la Oficina del Alto Comisionado para la Paz", dice el comunicado.

Cabe recordar que de acuerdo con la Coalición Larga Vida a las Mariposas, ha afirmado que la situación es preocupante para **los más de 3300 prisioneros a nivel nacional que estarían privados de su libertad por delitos políticos o conexos** y que han sido reconocidos como amnistiables por la Oficina del Alto Comisionado de las Naciones Unidas. Sin embargo, son más de 700 solicitudes de amnistía, de las que no se ha recibido ni una sola respuesta afirmativa.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
