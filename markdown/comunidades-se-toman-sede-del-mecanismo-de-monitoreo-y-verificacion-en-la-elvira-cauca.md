Title: Comunidades se toman sede del Mecanismo de Monitoreo y Verificación en la Elvira Cauca
Date: 2017-06-23 11:52
Category: Nacional, Paz
Tags: Conversaciones de paz con las FARC, Dejación de armas, La Elvira
Slug: comunidades-se-toman-sede-del-mecanismo-de-monitoreo-y-verificacion-en-la-elvira-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/la-elvira-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagio radio] 

###### [23 Jun 2017]

Cerca de trescientas personas, habitantes de los corregimientos de Monteredondo, La Elvira y otros aledaños se tomaron la sede del Mecanismo de Monitoreo y Verificación de La Elvira en el departamento del Cauca en protesta por la detención por parte de la policía y el ejército del señor Pedro Luis Zuleta, integrante de las FARC que ya **contaba con el certificado de dejación de armas por parte de la Oficina del Alto Comisionado para la Paz.**

Hacia la 1 de la mañana de este viernes, las personas arribaron al sitio y exigen que se libere a Zuleta o que se entregue la información de su paradero, dado que la información con la que cuentan es que fue detenido por la policía y luego **fue entregado al ejército para un supuesto traslado a la ciudad de Cali.**

Hacia las 10 de la mañana se tuvo información de que integrantes del **Escuadrón Móvil Antidisturbios se dirigía a la sede del Mecanismo, situación que alerta a la comunidad dado que cuando hay presencia del ESMAD** se pueden presentar agresiones en contra de los manifestantes que se resguardan en las instalaciones del Coliseo antiguo de la Elvira.

Hasta el momento se desconoce el paradero de Zuleta y las comunidades siguen exigiendo un pronunciamiento por parte de las autoridades para que se pueda establecer, tanto la ubicación del integrante de las FARC detenido, como las razones de su captura, en caso  de haberlas, o su liberación si se trata de un nuevo error en las bases de datos de la policía.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
