Title: Desalojo violento de 1200 habitantes del Barrio Bilbao en la localidad de Suba
Date: 2017-04-24 15:23
Category: Nacional
Tags: Desalojo, Familias, Peñalosa, Suba
Slug: bilbaosubadesalojo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-24-at-2.48.15-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 24 Abr 2017 

A esta hora cientos de personas habitantes del sector “Bilbao” en la localidad de Suba denuncian que la alcaldía de Suba realizó un desalojo forzoso de las cerca de 300 familias que lo habitan y que en podrían superar las 1200 personas. Además, según la denuncia, también hay amenaza de que le quiten la custodia de los menores de edad. Hasta el momento la acción del ESMAD deja un saldo de 3 casas incendiadas y 10 civiles heridos.

Desde tempranas horas de la mañana al sector llegaron funcionarios de la Alcaldía en compañía de la policía, funcionarios del Instituto Colombiano de Bienestar Familiar y Gestores de Convivencia afirmando que las familias deben desocupar el predio que ocupan desde hace más de 20 años en algunos casos. Según los funcionarios ya se agotaron todas las instancias y se tendría que proceder al desalojo.

Una de las voceras de los habitantes del sector denunció que no se realizó ningún tipo de aviso de esta diligencia y mucho menos se agotaron instancias de diálogo. “Nosotros somos los que estuvimos en la alcaldía buscando que nos escucharan” sin embargo no hubo intensión de diálogo y se procedió al desalojo sin entablar escenarios de discusión o negociación.

<iframe id="audio_18313453" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18313453_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Durante la conversación telefónica con el equipo de Contagio Radio la líder comunitaria denunció que en algunos medios de información se está diciendo que la responsabilidad en las quemas de las casas fue un indigente, sin embargo las personas son enfáticas en señalar al ESMAD de la policía. Además afirman que hay personas que se quedaron en las casas y que podrían resultar afectadas por las llamas que se están propagando.

La vocera afirma que por lo menos 10 personas han resultado heridas, sin embargo el número podría ser mayor porque muchos de ellos se niegan a retirarse del sector. Adicionalmente, ante las informaciones de que algunas de estas familias tienen subsidios de vivienda por parte del Estado los voceros de la comunidad lo niegan afirmando que una de las características de los habitantes de Bilbao es que ninguno recibe ningún tipo de atención, incluso “las instituciones ni siquiera tienen un censo”.

De acuerdo con la alcaldía, la acción tiene como propósito recuperar una zona de preservación ambiental del río Bogotá ubicada en ese sector. Según los funcionarios los ocupantes han retirado parte del jarillón, lo que incrementa el riesgo de un desastre natural ante un desbordamiento del río durante la temporada invernal.

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Desalojo-Familias-Bilbao.mp4\[/KGVID\]

 
