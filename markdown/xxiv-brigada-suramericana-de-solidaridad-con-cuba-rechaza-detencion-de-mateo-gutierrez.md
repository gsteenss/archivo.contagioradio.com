Title: XXIV Brigada Suramericana se solidariza con Mateo Gutíerrez
Date: 2017-04-07 12:29
Category: DDHH, Nacional
Tags: Mateo Gutierrez, Movimiento estudiantil
Slug: xxiv-brigada-suramericana-de-solidaridad-con-cuba-rechaza-detencion-de-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/mateo-gutierrez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Libertad para Mateo ] 

###### [07 Abr 2017]

Las muestras de solidaridad siguen llegando para Mateo Gutiérrez, detenido desde el pasado 23 de febrero, en esta ocasión la XXIV Brigada Suramericana de Solidaridad con Cuba expresó, por medio de un comunicado, s**u rechazo a la detención y exigencia al gobierno del presidente Santos para que libere inmediatamente al estudiante, por la falta de un  debido proceso. **

El texto es firmado por 33 personas que, durante el mes de enero, estuvieron con Mateo participando de esta brigada, en diferentes escenarios de foros y conferencias sobre historia y actualidad de Cuba y que señalan que en ese tiempo que compartieron con el estudiante, encontraron a una persona **“espíritu crítico pero que valora el diálogo y la convivencia pacífica, aún en el difícil contexto político-social que vive su país”**. Le puede interesar:["Mateo Gutiérrez León sería víctima de otro falso positivo"](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/)

Y agregaron que, por esos momentos compartidos y el comportamiento de Mateo consideran que es **“incapaz de participar en los hechos que se le imputan, proclamamos firmemente su inocencia”**, esta carta se suma a las demás acciones que han emprendido tanto familiares como amigos del estudiante para exigir su libertad. Le puede interesar:["Carta de Mateo Gutiérrez desde la prisión"](https://archivo.contagioradio.com/mateo-gutierrez-leon-seria-victima-de-falso-positivo-judicial/)

Comunicado a la opinión pública

*COMUNICADO DE LA 24° BRIGADA SUDAMERICANA DE SOLIDARIDAD CON CUBA*

*Mateo Gutiérrez León, joven de 20 años de edad, estudiante de quinto semestre de Sociología de la Universidad Nacional de Colombia, se encuentra detenido en la cárcel Modelo de Bogotá desde el 23 de febrero del corriente año, acusado de participar en la colocación de un petardo panfletario en Bogotá el 18 de septiembre de 2015. Se encuentra imputado por los delitos de terrorismo, hurto calificado y agravado, tráfico y porte de armas y concierto para delinquir. Se lo señala de formar parte de una organización denominada Movimiento Revolucionario del Pueblo, con supuesta afinidad al ELN.*

*La detención se enmarca en el contexto posterior del atentado del 18 de enero de 2017 producido en el barrio La Macarena, que provocó la muerte de un policía y 27 heridos, el cual fue reivindicado por el ELN. Mateo fue detenido, a partir de la intención del gobierno y la justicia colombiana de exhibir resultados luego del atentado del 18 de enero. Aunque la acusación lo desliga de ese atentado y lo vincula a hechos ocurridos dos años atrás, su detención tiene sentido en ese marco. La fiscalía basa su accionar en el 2° testimonio de un joven que se produce dieciseis meses después del atentado y que lo identifica a partir de un reconocimiento fotográfico, presentando discordancia respecto a la descripción inicial presentada en su primer testimonio, reconocimiento que no coincide con el relato físico-descriptivo realizado por el jóven Bryan Steven Sanchez Gomez -quién describe, en su primera declaración, al sujeto que lo reduce con un cuchillo como un hombre alto, moreno, mono de pelo recogido y depilado. La detención de Mateo constituye un caso de falso positivo judicial, en el marco de la necesidad gubernamental de obtener detenciones rápidas ante un atentado reciente con el fin de conseguir un impacto positivo en la opinión pública (así circulan los primeros rumores que intentan vincular a Mateo con el atentado de la Macarena y con una presunta formación en la preparación de explosivos en Cuba).*

*Esa posibilidad se derrumba dado que Mateo no se encontraba en Colombia en esa fecha, habiendo viajado a Cuba el 12 de enero junto con su madre y retornado el 12 de febrero.*  
*Mateo participó de la XXIV Brigada Suramericana de Solidaridad con Cuba, organizada por el Instituto Cubano de Amistad con los Pueblos y que se desarrolló entre el 22 de enero y el 5 de febrero del corriente año con sede en el Centro Internacional Julio Antonio Mella (CIJAM) sito en la provincia de Artemisa. Durante el desarrollo de la brigada se realizaron diversos talleres y conferencias sobre la historia y la actualidad cubana, así como un conjunto de actividades productivas en cooperativas de la zona y visitas a sitios de relevancia histórica.*

*Denunciamos, también, una contingencia de maniobras dilatorias administrativas por el Gobierno de Santos que perjudican el desarrollo procesal con eficacia y celeridad persiguiendo, como fin, la prolongación de la detención injusta y arbitraria de Mateo. Entendemos que la regla, en el derecho internacional público penal, es la libertad durante el desarrollo del proceso y no el encierro, como sucede en este caso.*

*Los abajo firmantes, participantes de la delegación argentina de la XXIV Brigada Suramericana de Solidaridad con Cuba, desean dejar constancia de las extraordinarias condiciones tanto intelectuales como de índole moral del compañero Mateo Gutiérrez León, quien participó con entusiasmo y lucidez de todas las actividades y tareas que se desarrollaron en la Brigada. Lo consideramos un joven con un inusual espíritu crítico pero que valora el diálogo y la convivencia pacífica, aún en el difícil contexto político-social que vive su país. De tal manera lo creemos incapaz de participar en los hechos que se le imputan, proclamamos firmemente su inocencia y exigimos al gobierno y a la justicia colombianas que procedan a su libertad inmediata, debido a que se han quebrantado los principios del derecho a un debido proceso, a la presunción de inocencia y se le ha imputado el ambiguo y oscuro tipo penal de “terrorismo” con el fin de mantenerlo privado de su libertad.*

*Jose Antonio Lopez Turnes*  
*17.802.284*

*Lucas Maximiliano Garassino*  
*36.538.547*  
*Julieta Aldana Fajardo*  
*37.450.920*  
*Manuel Eloy Garcia*  
*36.691.232*  
*Zannini Franco*  
*32.337.645*  
*Maria Flavia del Rosso*  
*23.695.645*  
*Maria de los Angeles Bernard*  
*32.438.461*  
*Victoria Chena*  
*29288.475*  
*Gaston Merino*  
*35.917.517*  
*Camila Perassolo*  
*39.211.*  
*Lucia Cataldi*  
*30.742.185*  
*Pagella Agostina*  
*34.172.478*  
*Pilar Martinez*  
*37.449.888*  
*Giorgetti Sofia*  
*34.162.923*  
*Ailik Jonatan*  
*27.382.014*  
*Laferrara G. Maximiliano*  
*31.017.721*  
*Slafer Leonardo*  
*34.551.268*  
*Maria Gracia Oliva*  
*36.429.268*  
*Lilana Perna*  
*5.698.821*  
*Maria Esperanza Tissera Grafeville*  
*33.029.831*  
*Ramiro Carlos Guarino*  
*35.413.212*  
*Sergio Osses*  
*16.230.270*  
*Mariela Soledad Seehaus*  
*34.480.874*  
*Carla Pot Kolic (MADRAZA)*  
*37.573.110*  
*Pagella Martina*  
*37.122.857*  
*Malvina Zayat*  
*31.558.116*  
*Molina Mariela Ayelen*  
*31.547.010*  
*Perez Daiana*  
*30.764.649*  
*BRASIL*  
*Barbara Marianoff Vaz*  
*id 1092903374*  
*cpf 014.742.650-22*  
*Luis Fernando Fraga Silva*  
*id 1020833917*  
*Alessandra Kosinski de Oliveira*  
*id 3037734311*  
*Sidonio Luiza*  
*rg 43.505.169-9*  
*Werner Kosinski de Oliveira Saraiva*  
*3106757937*

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
