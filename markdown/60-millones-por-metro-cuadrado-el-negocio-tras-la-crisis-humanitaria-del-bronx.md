Title: $60 millones por metro cuadrado: el negocio tras la crisis humanitaria del Bronx
Date: 2016-08-18 13:16
Category: DDHH, Nacional
Tags: desalojo del Bronx, habitantes de calle en el Bronx, segunda administración peñalosa
Slug: 60-millones-por-metro-cuadrado-el-negocio-tras-la-crisis-humanitaria-del-bronx
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Habitantes-de-calle..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Tiempo] 

###### [18 Ago 2016] 

Este miércoles la Comisión Segunda del Senado debatió la compleja situación de seguridad que afronta Bogotá tras la "intervención irresponsable y sin ninguna planificación" de la alcaldía en el Bronx, asegura el Representante Alirio Uribe, quien agrega que lo que se está viviendo es una "crisis humanitaria sin precedentes", que no sólo afecta a los habitantes de calle sino a los ciudadanos del común, y que es generada por una política equivocada para **recuperar físicamente los inmuebles en beneficio de las inmobiliarias y constructoras que financiaron la campaña del actual alcalde**.

De acuerdo con el Representante, **en la zona del Bronx se construirán locales comerciales cuyo metro cuadrado costará más que los ubicados en Unicentro**; prueba de ello es el centro comercial que se está construyendo en la Jiménez con décima, cuyos metros cuadrados cuestan \$60 millones. "Lo que hay es un negocio y no se tuvo en cuenta la situación humanitaria", afirma Uribe e insiste en que la problemática de los habitantes de calle tiene solución, pero la alcaldía lo que ha querido es improvisar y reprimir a la población, sin neutralizar las redes de microtráfico como se había comprometido.

Por su parte el comerciante Diego Barrera asegura que tras el operativo de desalojo en el Bronx, se presentaron robos en varios de los almacenes y disminuciones en las ventas; sin embargo la **alcaldía no se hizo presente y dejó la responsabilidad en manos del comando de Martires**, que no logró controlar lo que estaba sucediendo dada la cantidad de habitantes de calle que oscilaba entre 200 y 300. Para los comerciantes es evidente que la alcaldía intervino la zona desordenadamente y sin un plan de reubicación oportuno para los habitantes de calle, muchos de los cuales insisten en querer rehabilitarse; por lo que exigen a la alcaldía que los escuchen y atiendan sus demandas.

Una de las propuestas que el Representante está promoviendo es la de la **creación de un campamento humanitario para los habitantes de calle** en el que puedan contar con alimentación, atención en salud y asistencia psicosocial, y no continúen deambulando por las calles ante el confinamiento en que la Policía Nacional los quiere obligar y las prácticas de odio y discriminación que la ciudadanía ha manifestado en su contra; todo ello con la intención de prevenir posibles acciones de la mal llamada limpieza social que se puedan presentar.

Ante el trato cruel, humano y degradante al que se han tenido que enfrentar los habitantes de calle, se está recolectando toda la información para interponer una acción de tutela que obligue al alcalde a tomar medidas de protección y reubicación, mientras se prepara una **denuncia penal y disciplinaria en su contra por incurrir en delitos** como abuso de autoridad, negligencia en política pública, desplazamiento, [[violación de derechos fundamentales](https://archivo.contagioradio.com/desalojo-del-bronx-no-tuvo-una-planeacion-integral/)] y desconocimiento de la ley de protección para el habitante de calle.

<iframe src="http://co.ivoox.com/es/player_ej_12587842_2_1.html?data=kpeimpyceJOhhpywj5aXaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpCajbfJtI-fwpDZw5CnaaSnhqaez8bWpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe src="http://co.ivoox.com/es/player_ej_12589929_2_1.html?data=kpeimp6ddpqhhpywj5WdaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5yncaXdxszcjafFttPZ08aSlKiPh9DhxtfQy8bSuMahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

 
