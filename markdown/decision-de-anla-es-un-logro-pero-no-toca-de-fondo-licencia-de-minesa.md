Title: "ANLA no tomó decisión de fondo sobre la licencia ambiental de Minesa"
Date: 2020-10-05 12:11
Author: AdminContagio
Category: Actualidad, Ambiente
Slug: decision-de-anla-es-un-logro-pero-no-toca-de-fondo-licencia-de-minesa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Fracking-y-Mineria-en-Santurban.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de conocer la decisión de la Autoridad Nacional de Licencias Ambientales (ANLA), en la que se archiva el trámite administrativo que evaluaba la licencia ambiental para el proyecto Soto Norte, señalan que **esta decisión no toca de fondo el poder que tendría Minesa sobre el páramo de Santurbán.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComiteSanturban/status/1312452371541229569","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComiteSanturban/status/1312452371541229569

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este 2 de octubre la ANLA, mediante el [auto No. 09674,](http://www.anla.gov.co/noticias/1151-la-anla-archiva-proyecto-de-mineria-soto-norte) fundamentado en los resultados del estudios realizados por 5 profesionales de la facultad de minas, de la universidad Nacional de Medellín y más de 30 expertos en diferentes disciplinas como, geología geoquímica, hidrogeología entre otras; **evidenció el impacto ambiental qué generaría el proyecto minero Soto Norte, presentado por la multinacional Minesa, para la exportación subterránea de minerales en cercanías al** [**Páramo de Santurbán**.](https://archivo.contagioradio.com/el-gozar-de-un-ambiente-sano-no-frena-el-desarrollo-del-pais/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Como consecuencia del análisis realizado al estudio del impacto ambiental y, a la información allegada posteriormente por la empresa, **el equipo técnico de la ANLA estableció que no era posible emitir una decisión de fondo sobre el proyecto minero por lo que éste debería ser archivado**"*, este es el argumento entregado por la Autoridad, en donde dan un alto momentáneo al trabajo de Minesa.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/tobonsanin/status/1313184928293625862","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/tobonsanin/status/1313184928293625862

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Dentro de las consideraciones entregadas por la Autoridad se encuentra la definición del área de influencia, **aspectos técnicos del depósito de los residuos de la actividad minera, entre otras**, como el plan de manejo de riesgo, la valoración económica y la hidrogeologia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta acción se da a propósito de las más de *"44. 190 intervenciones por parte de la ciudadanía"* para obtener información de este trámite de licencia ambiental, según lo resalta en el comunicado la ANLA, pero, pese a ello diferentes sectores señalan que **esta decisión *"deja la puerta abierta a nuevas solicitudes"* por parte de los licenciamientos de exportación que pueda solicitar la multinacional árabe.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto se traduce en qué aún no se debe dar por sentado que la licencia ambiental de Minesa haya sido negada en su totalidad y de manera definitiva. [Mayerly López](https://archivo.contagioradio.com/anla-suspende-tramite-de-licencia-ambiental-sobre-santurban/), vocera del Comité en la Defensa del Páramo de Santurbán indicó, que a pesar de que este es un logro, **la ciudadanía debe entender que el riesgo continúa ya que *"la ANLA archivó el proyecto, más no niega la licencia ambiental"***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aclarando que la empresa tiene la posibilidad de presentar un recurso de reposición para que la ANLA, reconsidere la decisión. **Apelación que fue presentada este lunes por medio de un comunicado presentado por la sociedad minera de Santander**, y respaldado por Mubadala Investment Group empresa inversora del gobierno de Abu Dhabi.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Instancia que fue argumentada, indicando que **la solicitud de esta licencia ambiental estuvo respaldada en más de 5 años de estudios, y la colaboración durante los últimos ocho meses** en responder a las 107 solicitudes de información por parte de la ANLA.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Argumento, que según la defensora ambiental, tuvo diferentes errores, indicando que*, "Minesa presento tres veces la información incompleta* ... *por eso hacemos un llamado a* ***la ANLA para que con este tipo de licenciamientos sea un poco más rigurosa*, *primando el principio de precaución y más aún cuando se pone en riesgo el agua*** *y ambiente de millones de colombianos y colombianas*"

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"El debate de fondo no ha terminado y n**osotros no descansaremos hasta que todas nuestras cuencas abastecedoras sean zonas de exclusión de los proyectos de megaminería**"*
>
> <cite>[Mayerly López](https://archivo.contagioradio.com/anla-suspende-tramite-de-licencia-ambiental-sobre-santurban/), vocera del Comité en la Defensa del Páramo de Santurbán</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Una disputa por el agua que continúa este martes 6 de octubre en la plenaria del senado, con el debate de control político en donde organizaciones sociales y defensores de Derechos Humanos, presentarán argumentos jurídicos y técnicos que permitan dar cuenta de las razones por las que la ANLA *"debió negar desde un principio la licencia ambiental a Minesa".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
