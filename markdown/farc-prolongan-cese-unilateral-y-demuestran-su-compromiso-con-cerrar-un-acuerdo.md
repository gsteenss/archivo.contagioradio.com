Title: FARC prolongan cese unilateral y demuestran su compromiso con "cerrar un acuerdo"
Date: 2015-08-20 15:34
Category: Nacional, Paz
Tags: Camila Moreno, Centro de Pensamiento de la Universidad Nacional, diálogos en la Habana, FARC, ICTJ, Medina Gallego, Mesa de negociación, proceso de paz, Timochenko
Slug: farc-prolongan-cese-unilateral-y-demuestran-su-compromiso-con-cerrar-un-acuerdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/farc-dialogos-de-paz-cuba.gif" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: telemundo 

<iframe src="http://www.ivoox.com/player_ek_7215890_2_1.html?data=mJeel52ddI6ZmKiak5qJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiqLGpJDd1NTQs8%2FbwtOYxcrXqYzpz87Zw9nJtsLgjN6YxsrRucbn1dfO0JDXuYzX0NLd1NTRrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Medina, académico U. Nacional] 

###### [20 ago 2015] 

Frente al pronunciamiento que lanzó esta mañana las FARC de extender el cese unilateral al fuego que finalizaba hoy, se crea un ambiente de esperanza frente al proceso de paz y demuestra un compromiso de esa guerrilla con la meta de alcanzar los acuerdos y cerrar el conflicto armado, señala Carlos Medina, integrante del **Centro de Pensamiento de la Universidad Nacional.**

Timochenko, comandante de las FARC, indicó con cifras en una carta abierta, lo que han representado los hostigamientos por parte de las fuerzas militares...

“*Como resultado de estas operaciones terrestres, en el departamento de Antioquía, una guerrillera fue capturada y dos más asesinadas; en el Caquetá, en un asalto aéreo a una unidad del frente 14, murió un guerrillero y 3 más están desaparecidos, al tiempo que se intensifican las operaciones y las fumigaciones aéreas en áreas de los frentes 32, 15 y 14 en ese mismo departamento; y en los municipios de Toribío, Caloto, Corinto y Belalcazar en el departamento del Cauca*”.

El cese al fuego que desde un principio ha tenido un carácter unilateral, en donde **se destaca la intención de mantener y avanzar en este proceso de paz, las fuerzas militares siguen operando contra las estructuras de las FARC** en distintas partes del país, señala Medina Gallego.

El académico indica que estar en medio del cese unilateral, no quiere decir que las fuerzas armadas no puedan operar, sin embargo “*las posibilidades de que se puedan presentar enfrentamientos sin que se rompa el cese unilateral son reales*”, pero resulta inconveniente que siga siendo unilateral por el momento en el que se encuentran los diálogos, en que se discuten temas que tienen que ver fundamentalmente con el problema de la justicia.

Medina hace un llamado a que en vista de que “*la guerrilla ha prorrogado su cese unilateral al fuego, es necesario demandarle al gobierno nacional avanzar con el desescalamiento del conflicto*” esto no solo contra el grupo armado, sino también contra la población civil y el ambiente.

Otro punto que se resalta Medina Gallego es que “l*as acciones de las fuerza publica en algunas regiones del país crean un ambiente de desconcierto en la sociedad*”, desconcierto que también es alimentado por parte del sector de la opinión pública que no respalda el proceso.
