Title: "Si no somos capaces de defender la Ciénaga Grande no podría haber paz" Manuel Rodríguez
Date: 2016-07-25 18:01
Category: Ambiente
Tags: Ciénaga Grande se Santa Marta, Manuel Rodríguez, paz, Universidad del Magdalena
Slug: si-no-somos-capaces-de-defender-la-cienaga-grande-no-podria-haber-paz-manuel-rodriguez
Status: published

###### Foto: El Tiempo 

###### [25 Jul 2016] 

La preocupación por la Ciénaga Grande de Santa Marta, **continúa aumentando teniendo en cuenta que se trata del complejo de humedales más importante del país,** que se encuentra en riesgo por la construcción de la carretera Barranquilla – Santa Marta, el robo de tierras y agua para la ganadería extensiva y para los cultivos de palma de aceite y arroz por los cuales se ha represado el agua de los ríos que alimentan la Ciénaga.

De acuerdo con Manuel Rodríguez Becerra, exministro de Ambiente y director del Foro Nacional Ambiental, **“se están cometiendo los mismo errores de hace 40 años cuando murieron 28.000 hectáreas de manglar”.** [El impacto de estas actividades nocivas para el ecosistema](https://archivo.contagioradio.com/cienaga-grande-de-santa-marta-a-punto-de-desaparecer/)ha sido tal, que en tan solo 10 años la producción de peces ha bajado de 27 mil toneladas anuales a 1725 toneladas anuales, según un estudio científico alemán, lo que significa una **reducción de más del 90% en la producción pesquera, sustento de más de 5000 familias del departamento.**

Esta situación, para Rodríguez, podría significar el impulso para el desarrollo de una nueva guerra en la que los actores serían los habitantes de la zona que buscan defender los intereses de cada cual. “**Si no hay acuerdos para proteger la ciénaga, estamos fomentando la guerra. Si no somos capaces de generar la paz entre los habitantes de la ciénaga con sus diferentes intereses y la paz con la naturaleza, no estamos hablando de una paz posible”**, explica el profesor.

Es por ello, que desde el Foro Nacional Ambiental junto con la Universidad del Magdalena, este miércoles 27 de julio desde las 8 de la mañana se realizará el foro “Los Retos Sociales y Ambientales de la Ciénaga Grande de Santa Marta”. El foro se realizará en la Universidad en el Auditorio Julio Otero Muñoz y además podrá seguirse en vivo a través de [streaming.](http://www.foronacionalambiental.org.co/)

[![Foro](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/CoFNz9HW8AACjX0.jpg){.aligncenter .size-full .wp-image-26803 width="1200" height="1088"}](https://archivo.contagioradio.com/si-no-somos-capaces-de-defender-la-cienaga-grande-no-podria-haber-paz-manuel-rodriguez/cofnz9hw8aacjx0/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
