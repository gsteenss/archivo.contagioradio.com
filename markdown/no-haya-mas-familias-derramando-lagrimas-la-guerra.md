Title: "Que no haya más familias derramando lágrimas por la guerra"
Date: 2016-12-12 14:29
Category: DDHH, Sin Olvido
Tags: El Dorado, Javier Apache, víctimas
Slug: no-haya-mas-familias-derramando-lagrimas-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/El-Dorado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Dic. 2016] 

**“Él era un niño… era un niño, ¿por qué lo mataron?” es la pregunta que más de 24 años después se sigue haciendo una madre, Rosalba Caro**, una mujer pequeñita, pero que desborda ternura y tranquilidad. Que en sus ojos y en su dulce voz refleja los dolores que en su corazón permanecen luego de perder a su hijo, un 15 de Noviembre de 1992.

Era una mañana como cualquier otra, en la que la familia Caro Apache se levantaba a hacer sus labores propias del campo en el Castillo, departamento del Meta. Herson **Javier Apache Caro, hijo de Rosalba, con tan solo 15 años, hizo caso a sus padres y se dirigió al caserío de Puerto Unión a vender unos kilos de café.** Esa sería la última vez que desayunarían juntos, que organizarían el tema del café en presencia de Javier, sin saberlo, **ese sería el último día que lo verían con vida.**

Ese 15 de Noviembre de 1992, una patrulla del Ejército Nacional de Colombia, adscrita al Batallón de Infantería 21 Vargas, habría realizado una serie de retenes y allanamientos en el caserío Puerto Unión. Dicha patrulla habría instalado un retén en las entradas y salidas del lugar, y cuentan los pobladores que a muchos de ellos los habrían sometido a malos tratos.

**Cuando Javier se percató de lo que ocurría, sintió miedo por su vida y al parecer sus pies habrían corrido para resguardarse y esperar que todo pasara. Y fue esa la razón suficiente para que los integrantes de la patrulla militar hubiesen disparado contra su humanidad, vilmente, por la espalda, mientras Javier corría.**

Javier se aferró a la vida y logró llegar al Hospital del Castillo. Pero a su corta edad, no pudo seguir luchando y dio un último suspiro a las 11 a.m.

Ante la ausencia de Javier y ver que pasaban las horas y no regresaba a su hogar, sus padres salieron en su búsqueda. Llegaron hasta Medellín del Ariari y se encontraron de frente con la indolencia de algunos integrantes del Ejército.

**Para ellos no valió que una madre suplicara tener alguna razón de su hijo Javier, los integrantes del Ejército, de manera displicente le decían “si salió corriendo es porque algo debía”.** Rosalba no podía creer que le hubieran arrebatado de esa manera a su pequeño. Y que incluso lo tildaran de guerrillero, para esconder sus actos descorazonados.

El sufrimiento que tuvieron que vivir los padres de Javier, hizo que no buscaran justicia durante los primeros años posteriores en los que se cometió el hecho. Sin embargo, Rosa, luego en compañía de sus hijos, y de la Comisión Intereclesial de Justicia y Paz, organización que le acompañó, sacó fuerzas de su corazón, de su alma, de ese vientre que sintió por más de 9 meses crecer y gestar la vida de Javier.

**“Para mi han sido años de mucha espera” y sin titubear Rosalba también dice, mientras pone su mano en el corazón y cierra los ojos “ha sido triste, doloroso (…) duro, porque siempre lo tenía ahí en mi mente, en mi corazón a toda hora, mi hijo, mi hijo, mi hijo”.**

Comenzaron una lucha, que pensaron iba a tardar menos, pero no fue así. Pasaron los minutos, las horas, los días y la justicia y verdad no llegaron. Rosalba parecía cansada, pero continuo con su búsqueda.

Al agotarse las instancias internas, se optó por acudir a instituciones internacionales como la Comisión Interamericana de Derechos Humanos – CIDH – quien fue la que finalmente dio una luz en su camino.

**Fueron 24 largos años los que pasaron, para que finalmente Rosalba pudiera escuchar lo que supo desde el principio “el estado y los militares fueron responsables del asesinato  de Javier Apache”.**

El pasado 11 de Diciembre en el municipio de El Dorado, en el Meta en compañía de sus hijos y familiares que llegaron desde diversos lugares del país, en un acto solemne con rosas blancas se recordó a Javier y el Estado pidió perdón por los hechos, que sin embargo no regresarán a la vida a este pequeño.

**“He aceptado las disculpas” dijo entre sollozos Rosalba** y continúo con voz entrecortada “porque tal vez hoy por hoy conozco del perdón de Dios y él me ha dado fuerzas para aceptarlo (…) **si esto hubiera sucedido tiempo atrás no hubiera perdonado, porque era el dolor, la tristeza, la amargura que hay en mí. Pero hoy Dios ha puesto en mi corazón el perdón para que así mismo haya paz”.**

<iframe src="http://co.ivoox.com/es/player_ek_14904748_2_1.html?data=kpmmkpmbeJmhhpywj5WXaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncbPj1MbZxMaPh8Lm0IqfpZDNstXZ09vS0MjNaaSnhqeg0JDJsozVxNncjcnJb8XdyNPWyM7HpcTdhqigh6eXso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Miserablemente acribillaron a Javier, fueron las palabras de Rosalba, quien se dirigía con esa misma mirada tranquila pero ahora llena de lágrimas a los asistentes al evento “mi hijo no era un guerrillero, era un niño trabajador, que me acompañaba en la finca”.

Javier encontró la muerte a sus 15 años sin buscarla, su familia lo recuerda como un niño que no sabía de la maldad, que los cuidaba y los ayudaba en todas las labores.

Las palabras de fe no hicieron falta. El padre Alberto Franco de la Comisión Intereclesial de Justicia y Paz “(…) no debieran existir los mal “falsos positivos” (…) pero lo bueno es que estemos acá –en El Dorado – aceptando la responsabilidad (…) escuchar lo que las víctimas querían. La verdad dignifica a las víctimas”.

<iframe src="http://co.ivoox.com/es/player_ek_14904805_2_1.html?data=kpmmkpmcdJahhpywj5WbaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncbHW09SYo9HGqdPo0JCz1MbSp9CZk6iYpdTRrdTdhqigh6eXsozYxpC319jYrcTdwpDmjdXFvo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Así, de un modo especial Javier Apache se encuentra dignificado de una manera nueva. Desde el pasado domingo, su mamá, Rosalba llora de modo distinto, y con el tiempo, ojalá en medio de esa búsqueda de paz, no sea solo Rosa sino que todas las mamás de Colombia lloren distinto, para que de esta manera se inicie un camino nuevo.**

Nuevo camino que se abre con la aceptación del daño cometido, con el perdón, pero que tiene un camino largo en el que la reconciliación es pieza clave y así mismo, en el que debe existir la garantía de no repetición y el respeto por la dignidad de las familias víctimas del conflicto armado.

<iframe src="http://co.ivoox.com/es/player_ek_14904861_2_1.html?data=kpmmkpmcepKhhpywj5WbaZS1kpyah5yncZOhhpywj5WRaZi3jpWah5yncbPj1MbZxMaPh8Lm0IqfpZCxpcXmxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

\

 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>

 
