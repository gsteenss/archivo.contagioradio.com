Title: Las mafias tras los incendios en la Amazonia
Date: 2019-01-25 18:28
Author: AdminContagio
Category: Ambiente, Nacional
Slug: las-mafias-tras-los-incendios-la-amazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/quemas-en-la-amazonia-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Carlos Pinto] 

###### [25 ene 2019] 

[Tras un incendio que calcinó 300 hectáreas de bosque en el Guaviare, organizaciones ambientales, junto con las comunidades locales, han pedido a la Fiscalía General de la Nación, investigar el presunto rol de mafias en estos hechos, que según los ambientalistas, inducen quemas para facilitar la construcción ilegal de rutas en la Amazonía colombiano.]

Según Camilo Prieto, **vocero del Movimiento Ambientalista Colombiano,** grupos al margen de la ley, compuestos de disidentes de la antigua FARC-EP y bandas criminales, **aprovechan las sequías causadas por el fenómeno de El Niño para inducir estos incendios y despejar el terreno**. Luego, siguen con la construcción de vías ilegales para crear rutas de transporte y acaparar tierras para sus economías ilegales, entre ellos, la minería de oro y el narcotráfico.

Estas vías ilegales siguen la rutas [diseñadas para la Marginal de la Selva, carretera que se proyectada durante la administración de Juan Manuel Santos para conectar San José del Guaviare con San Vicente del Caguán. ]Este plan fue descartado en marzo del 2018 después que el Instituto de Hidrología, Meteorología y Estudios Ambientales (Ideam) determinara  que puntos críticos de deforestación en el departamento correspondían con áreas designadas para la construcción de la carretera.

Sin embargo, grupos al margen de la ley continúan de manera ilícita la elaboración de este proyecto con efectos nocivos para el medio ambiente. El Movimiento Ambientalista encontró evidencia de una conexión entra estas rutas y los incendios criminales a través de vuelos aéreos, mapas satelitales, visitas al territorio y la participación de las comunidades. Estos estudios demuestran que los registros de puntos de calor y incendios coinciden con las rutas diseñadas para la construcción de la carretera.

### **El silencio de la Fiscalía** 

Los incendios son uno de los factores principales que contribuyen a la deforestación, el cual se ha disparado desde la implementación de los acuerdos de paz.  La Amazonía perdió 178.000 hectáreas de bosque en el 2016, unas 54.000 hectáreas más que en el 2015, s[egún el Instituto de Hidrología, Meteorología y Estudios Ambientales (Ideam)]. Esta cifra solo ha crecido: en el 2017, la deforestación alcanzó 220.000 hectáreas perdidas y según el Ministro de Ambiente, para el 2018 podría llegar a 270.000.

(Le puede interesar: "[La Amazonía está perdiendo 293 hectáreas cada día](https://archivo.contagioradio.com/amazonia-perdiendo-hectareas/)")

Para Prieto, la crisis de deforestación que se documenta en la Amazonía colombiana ha llegado a ser "el drama ambientalista más grande del país" dado el valor ecológico de este ecosistema. En marzo del 2018, la Fiscalía anunció la apertura de 90 procesos por delitos de deforestación en la Amazonía. Sin embargo, **el líder ambientalista afirmó que aún no se han abierto investigaciones sobre estas rutas ilegales a pesar de las denuncias de las comunidades locales.**

Además, Prieto manifestó que las comunidades de [Miraflores y Calamar en Guaviare, unos de los municipios donde más se ha tocado el tema de incendios en el departamento, han denunciado ante el Ministerio de Minas y el Ministerio de Transporte la relación entre estos fuegos criminales y las rutas ilegales. Sin embargo, estas entidades se han pronunciado incompetentes ante estos hechos.]

"Los diseños de estas rutas no están en la órbita de las autoridades entonces esas rutas siguen creciendo y avanzando," dijo Prieto.

Por esta razón, el Movimiento Ambientalista le ha solicitado a la Fiscalía que investigue estos casos de incendios criminales e imponga sanciones a los responsables, además de los beneficiaros de la deforestación. Según Prieto, el despeje de estos terrenos a través de estos incendios le favorece al cultivo de palma, la explotación maderera y en especial, la ganadería extensiva.

<iframe id="audio_31791862" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31791862_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
