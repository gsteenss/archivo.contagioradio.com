Title: Es la oportunidad de retomar diálogos con ELN: DLP
Date: 2020-04-17 19:24
Author: CtgAdm
Category: Actualidad, Nacional
Tags: Defendamos la Paz, Dialogos gobierno eln, ELN, pandemia
Slug: es-la-oportunidad-de-retomar-dialogos-con-eln-dlp
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/eln_0-e1517506384192.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 17 de abril [**Defendamos La paz**](https://archivo.contagioradio.com/defendamos-la-paz-pide-a-gobierno-y-eln-reanudar-dialogos/) convocó a un encuentro virtual para ponen en la mesa de diálogo en medio de la crisis social; el estado actual de la **búsqueda de la paz entre el Gobierno y el Ejercito de Liberación Nacional** (ELN).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un espacio de dos horas, dónde se recopilaron declaraciones análisis he informes de defensores de la paz, nacionales, internacionales y civiles; así como de los voceros de comunidades donde ha hecho presencia por años el ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este encuentro hicieron un llamado al Gobierno Nacional y a los grupos armados irregulares que operan en el país, para que se acojan al al llamado de la [ONU](https://archivo.contagioradio.com/cese-unilateral-al-fuego-del-eln-una-oportunidad-para-la-paz/) y del [Papa Francisco](https://archivo.contagioradio.com/en-necesario-negociar-un-cese-bilateral-eln/)para que se dé un cese unilateral del fuego no solamente durante el mes de abril si no de manera indefinida.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"El ELN ha mostrado su disposición de continuar con los diálogos de paz, (...) hoy el Presidente Duque tiene la posibilidad de hacer parte de un hecho histórico, en pro de la paz completa de nuestro país"*
>
> <cite> Defendamos La Paz. </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Asímismo voceros de DLP como el senador Roy Barreras, agregaron que este es el momento para empezar atender la urgencia humanitaria que tanto requiere el país y las comunidades, *"en medio de las crisis debemos hacernos preguntas importantes encaminadas a hacer posible la paz completa, conseguir el cierre definitivo de la guerra interna y avanzar en la suspensión de la problemática situación en derechos"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Negociar con un grupo insurgente no es hacer un favor al grupo, sino pensar en la paz y estabilidad de las comunidades"

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el dialogo se hizo visible la voz[comunidades](https://www.justiciaypazcolombia.com/es-tiempo-de-cambiar-todos-debemos-ceder-acuerdo-humanitario-global-covid-19-avances-con-eln-intervencion-lider-embera/) del Chocó, Cauca, Nariño, Catatumbo, Sur de Bolívar, Putumayo, entre otras, lugares del país golpeados históricamente por la violencia producto de los enfrentamientos entre grupos armados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

César Amaya, integrante de Paz Completa, señaló en su intervención la importancia de las voces de los territorios en el desarrollo del diálogo por la paz, "nos acostumbramos a escuchar a Bogotá, cuando en los territorios es que pasan las cosas".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte Fernando Sarmiento, integrante del Programa de Desarrollo para la paz del Oriente antioqueño (PRODEPAZ), afirmó, *"son las regiones las que tienen que hablar de esas acciones concretas, porque son ellos son los que han vivido la presencia del ELN"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por parte de comunidades chocoanas [Leyner Palacios](https://archivo.contagioradio.com/leyner-palacios-escolta-asesinado/), quién afirmó que negociar con un grupo insurgente *"no es hacerle un favor al grupo armado, sino pensar en la paz y estabilidad de las comunidades*, y agregó que hoy a las comunidades del Chocó *"no les preocupa la pandemia, ni tampoco les gana el miedo al contagio, ellos y ellas exigen la paz, no quieren más violencia en sus hogares"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma por medio de una carta Argemiro Bailarín, vocero de la comunidad indigena Embera del Bajo Atrato en el Choco, presentó seis peticiones dentro de las cuales propone que se prolongue el cese al fuego unilateral.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como también el compromiso por parte del ELN para no reclutar en sus filas a jóvenes de esta comunidad, y agregó enfáticamente que es hora de un alto a los asesinatos en este territorio.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hoy estamos llamados a cambiar, para respetarnos como seres humanos, hoy debemos respetar la madre tierra, es tiempo para construir unas relaciones justas entre ciudadanos, el Estado y las empresas para nosotros sólo un desarme total abrirá espacio a la justicia"*
>
> <cite> Argemiro Bailarín |Vocero de la comunidad indigena Embera del Bajo Atrato </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Peticiones de las iglesias para los diálogos con ELN

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este conversatorio también hicieron presencia diferentes voceros de la iglesia católica colombiana entre ellos monseñor Héctor Fabio Henao, presidente del Consejo Nacional de Paz y monseñor Darío Monsalve, representante de la Arquidiócesis de Cali.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En sus intervenciones respaldaron argumentos anteriores, señalando que quien debe primar como elemento fundamental en este diálogo son las comunidades.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"La petición hacia el ELN es el respeto a los comunidades y así como un compromiso claro al medio ambiente y la respuesta ha las diferentes solicitudes de los territorios en términos de verdad y justicia espiritual"*
>
> <cite> Monseñor Darío Monsalve | Representante de la Arquidiócesis de Cali </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Asimismo participaron voceros internacionales de iglesias, entre ellos el reverendo **Douglas Leonard del Consejo Mundial de Iglesias** y la **reverenda Anjte Jacklelen Arzobispa de la iglesia Sueca.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Inicialmente el reverendo Douglas desde Nueva York, aseguró que la pandemia es un momento de cambio "estamos en un periodo de pascua, y al igual que Jesús exclamó luego de su resurrección , hoy todos queremos para Colombia que la paz sea con vosotros".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La arzobispa sueca por su parte enfatizó qué es necesario que tanto el ELN como el Gobierno respeten los espacios humanitarios definidos por las comunidades para resguardarse de la guerra, con el apoyo de integrantes de la sociedad y actores de fe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El cierre estuvo a cargo del senador Iván Cepeda, integrante defendamos la paz quién aseveró que es necesario no solamente el cese al fuego por parte los grupos armados ilegales, sino también las Fuerzas Armadas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *" Quienes estamos del lado de la defensa de la paz, creemos que la paz no es una carga o un accesorio, es una condición para que la humanidad pueda avanzar"*
>
> <cite>Ivan Cepeda | Defendamos la Paz</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/301402790844497/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/301402790844497/

</div>

</figure>
<!-- /wp:core-embed/facebook -->
