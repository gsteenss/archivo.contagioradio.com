Title: Líderes sociales, el tema ausente de encuentro Trump-Duque
Date: 2019-02-13 23:13
Author: AdminContagio
Category: DDHH, Líderes sociales, Política
Slug: lideres-sociales-deberian-tema-principal-encuentro-trump-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-21.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Evan Vucci] 

###### [13 Feb 2019] 

Ante el encuentro de hoy entre el presidente Iván Duque con su homólogo estadounidense, Donald Trump, defensores de derechos humanos denunciaron la ausencia del tema de la seguridad de los líderes sociales en Colombia en la agenda de los mandatarios durante su segunda reunión oficial.

Para la experta Gimena Sánchez-Garzoli, de la Oficina de Washington para asuntos Latinoamericanos (WOLA), la conversación entre los presidentes debió enfocarse en la protección de líderes sociales, dado el incremento drástico en las amenazas y asesinatos en contra de esta población tras la firma de los acuerdos de paz.

Sánchez sostuvo que el gobierno de Estados Unidos (EEUU) debe monitorear y exigir la seguridad de los líderes, pues bajo la ley estadounidense, el apoyo financiero a las fuerzas militares colombianas está condicionado a garantizar la protección de los derechos humanos en Colombia.

Sin embargo, según la agente de WOLA al tema no se le ha dado el nivel de importancia que debería tener en el encuentro entre los mandatarios, y por el contrario, los temas principales han sido la crisis en Venezuela y los resultados en la lucha contra narcotráfico, basado en la penalización de los cultivadores, la aspersión aérea y la erradicación forzada.

### **La mirada a Venezuela** 

En esta reunión, el presidente Trump manifestó nuevamente que la opción de una intervención militar, liderada por los Estados Unidos, en Venezuela sigue en la mesa. Al respeto, el gobierno colombiano ha mantenido un "doble discurso," según Sánchez-Garzoli, en el cual por un lado  expresa publicamente su rechazo de una intervención militar en Venezuela y por el otro lado, se hace entender que Colombia respalda "cualquier opción que se puede dar" por el país norteamericano.

Asimismo, la defensora de derechos humanos aseguró que la posición del gobierno de Duque, al respaldar a [Juan Guaidó y aliarse con EEUU, "no son constructivas" y que generan tensiones en el escenario internacional que podrían llegar al punto de "]un enorme choque". Ahí, Sánchez-Garzoli agregó que la confrontación mediática ocasionado por la ayuda humanitaria de Estados Unidos a Venezuela se está aprovechando para fines políticos a expensas de la población venezolana.

Al mismo tiempo, el Comité de Asuntos Exteriores de la Cámara de Representantes ha afirmado su apoyo por una transición democrática en Venezuela y contradecido las declaraciones de Trump sobre la posibilidad de una intervención militar. Sin embargo, sigue preocupante las asignaciones por el gobierno norteamericano de algunos funcionarios llevarán adelante las discusiones alrededor de esta crisis.

Puntualmente, Sánchez-Garzoli cuestionó la designación de Elliot Abrams como enviado especial para Venezuela de Estados Unidos. En 1991, Abrams fue condenado por retener información sobre abusos de derechos humanos en la guerra civil de El Salvador mientras que trabajaba para el gobierno de Ronald Reagan. En su nuevo rol de enviado especial, Abrams realizó una presentación al Congreso hoy sobre las opciones que este organismo puede tomar en relación a la crisis política y humanitaria en Venezuela.

### **¿El regreso de las fumigaciones aéreas?** 

Dado el aumento en la producción de coca en 2017, según el último reporte de las Naciones Unidas, republicanos del Congreso estadounidense han insistido en redoblar esfuerzos para ponerle freno al crecimiento de los cultivos ilícitos. Sánchez afirmó que la cantidad de apoyo financiero para medidas antinarcóticas podrá aumentar este año si el Congreso aprueba el correspondiente proyecto de ley.

Adicionalmente, líderes políticos como el senador republicano Marco Rubio han tomado públicamente posición en favor de la fumigación aérea, medida que la experta resaltó no ha sido efectiva a largo plazo. Además, podría perjudicar la salud de campesinos y debilitar los acuerdos de paz con las antiguas FARC.

### **Los diálogos con el ELN** 

Para Sánchez-Garzoli, las posturas de derecha en Estados Unidos, en relación al tema de Colombia, recuerdan la política de guerra fría del pasado. Además de la posición sobre Venezuela, también preocupan las discusiones alrededor del futuro del proceso de paz con el Ejército de Liberación Nacional (ELN).

Al respecto, el Congreso se ha alineado con la posición del gobierno colombiano, que exige la extradición de los líderes del ELN de Cuba a Colombia. Sánchez-Garzoli sostiene que el gobierno norteamericano podría incluir nuevamente a Cuba en la lista de países patrocinadores del terrorismo internacional, si la isla sigue oponiéndose a entregar a los negociadores en la mesa de la Habana.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
