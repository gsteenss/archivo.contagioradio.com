Title: Sin argumentos: así quedó la Fiscalía en caso Mateo Gutiérrez
Date: 2018-11-08 12:09
Author: AdminContagio
Category: DDHH, Movilización
Tags: Juicio Mateo Gutiérrez
Slug: sin-argumentos-asi-quedo-la-fiscalia-en-caso-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mateo-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 8 Nov 2018 

Tras 20 meses de reclusión y numerosas irregularidades en su proceso, el estudiante de sociología Mateo Gutiérrez; acusado de participar en la creación y detonación de “bombas panfletarias” en Bogotá recuperaría su libertad por decisión del **Juez Sexto penal** de la Fiscalía, la cual no logró probar su responsabilidad en los delitos de los que se acusaba al estudiante quien sería otro de los casos conocidos como **“falso positivo judicial”.**

### **Cronología de su proceso judicial ** 

**23 de febrero de 2017:** **Mateo Gutiérrez** es capturado al ser relacionado con las explosiones en Bogotá, que dejaron un policía muerto y otros más de 15 heridos, e l 18 de Septiembre de 2015.

**16 de marzo de 2017:** Tras tres semanas de captura, avanza un proceso judicial incongruente y sin garantías.

**25 de abril de 2017:** La Fiscalía no ha radicado el escrito de acusación y no se ha realizado una valorización de las pruebas, la Juez 42 del circuito penal negó su libertad.

**22 de junio de junio de 2017:** Se realiza audiencia de acusación donde se presentan pruebas en contra de Mateo Gutiérrez

23 de junio de 2017: La Fiscalía acusa a  Mateo **de cometer los delitos de terrorismo, concierto para delinquir, hurto y porte de artefactos explosivos.**

15 de noviembre de 2017: A raíz del artículo publicado en la Revista Semana el 12 de Noviembre,  sus padres insisten en que medios y Fiscalía llevan dos procesos paralelos acerca del caso

7 de noviembre de 2018: Por ausencia de pruebas, el Juez Sexto penal da el fallo absolutorio algo que en palabras del padre de Mateo, Omar, Gutiérrez " sabían que iba a pasar; pero fue un momento de mucha paz, de alegría y de tranquilidad".

### **Rol de los medios de comunicación  y la educación** 

Ad portas de su libertad, su padre destaca el papel de los medios alternativos y los medios internacionales en la defensa de su hijo mientras que señ**ala la falta de equilibrio en los grandes medios del país** como Revista Semana y su tendencia a quedarse con la versión de entidades como la Policía y la Fiscalía “pero que no ahondan en la versión que presenta la defensa u otras partes, no es un periodismo que tenga fuentes diversas”.

El padre de Mateo fue enfático al manifestar la continua estigmatización hacia su hijo incluso tras conocerse el veredicto de su absolución pues “lo que se dice en los grandes medios de la audiencia de ayer no es la realidad: no valoran los argumentos de la defensa y no hacen énfasis en lo que dijo el Ministerio Público”.

También resaltó que Colombia se está convirtiendo en un país donde **“han hecho carrera los falsos positivos judiciales y sindican a una persona con falsas pruebas y falsos testimonios para arruinar su vida”** y donde a menudo son las personas de estratos bajos quienes pagan las consecuencias, haciendo referencia a los sucesos ocurridos con los atentados en el centro comercial Andino, caso en el que juez intentó vincular a Mateo y que logró ser negado el año pasado.

Se espera que Mateo Gutiérrez recupere la libertad en un momento en el que las universidades públicas defienden sus derechos por una educación de calidad; resulta oportuno destacar que durante los pasados veinte meses fue la Universidad Nacional y en especial las facultades de Sociología y Ciencia Política las que permitieron que el estudiante cursara dos semestres, lo que resultó  “un apoyo moral muy importante” según su padre.

<iframe id="audio_29936360" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29936360_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
