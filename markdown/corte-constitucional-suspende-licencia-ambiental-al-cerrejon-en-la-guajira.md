Title: Corte Constitucional suspende licencia ambiental al Cerrejón en La Guajira
Date: 2017-03-02 15:40
Category: Ambiente, Nacional
Tags: El Cerrejón en La Guajira, Fuerza Wayuu, Pueblos Wayuu en Colombia
Slug: corte-constitucional-suspende-licencia-ambiental-al-cerrejon-en-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/El_Cerrón.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El informador] 

###### [2 Mar 2017] 

Mediante la Sentencia T-704/16, la Corte Constitucional le dio la razón al pueblo indígena Wayuu, mediante el falló se suspendió el plan de manejo ambiental de la licencia que había obtenido la empresa El Cerrejón, **para su proyecto de expansión 'Puerto Bolívar' y además ordenó realizar una Consulta Previa entre las comunidades** y la multinacional que lleva más de 30 años explotando carbón en el territorio.

A través de la tutela T-5.451.805 presentada por la comunidad de Media Luna en Uribia y apoyada por el Magistrado Ponente Luis Ernesto Vargas Silva, la comunidad alegó la **vulneración del ambiente sano, salud y debido proceso** por parte del Ministerio de Ambiente, el Ministerio del Interior, la Autoridad Nacional de Licencias Ambientales y la empresa El Cerrejón, “por la expedición de la licencia ambiental Nº 0428 del 7 de mayo de 2014 para la modificación del Plan de Manejo”.

Jackeline Romero lideresa Wayuu integrante de la organización Fuerza Mujeres Wayuu, manifestó que **"en La Guajira se están llevando todos nuestros recursos y nos están dejando los daños"**, la lideresa comentó que la comunidad recibió positivamente la noticia y esperan que otros graves problemas que afectan a la comunidad, también sean atendidos pues "el tema minero es sólo uno de todos los problemas que presenta la Guajira". ([Le puede interesar: La Guajira necesita que se declare el estado de excepción](https://archivo.contagioradio.com/santos-y-su-error-de-no-declarar-en-estado-de-excepcion-a-la-guajira/))

###  

Romero también puntualizó que este problema no es reciente, ya que El Cerrejón irrumpió en territorio Wayuu desde hace 33 años, y en ninguna ocasión socializó o realizó Consulta Previa, resaltó que el primer responsable es el Estado colombiano y las autoridades ambientales, **"no entendemos cómo se emite certificación de que nosotros los Wayúu no existimos en zona donde harán minería".**

Por otra parte, la lideresa señala que está será una oportunidad para que el pueblo Wayuu pueda hablar y denunciar todos los vejámenes de que han sido victimas en todos estos años de explotación e indicó que **"lo que hay que hacer es hablar ¿qué entiende el Estado por desarrollo?". **([Le puede interesar: Robos, golpes e insultos del ESMAD a indígenas Wayúu en Guajira](https://archivo.contagioradio.com/robos-golpes-e-insultos-de-esmad-a-indigenas-wayuu-en-guajira/))

###  

“Esta Corte concluye que la explotación de carbón en el Cerrejón no sólo ha producido secuelas negativas en el ambiente, sino también en distintos ámbitos de las comunidades étnicas de la Guajira. Las pruebas evidencian que del Puerto Bolívar se producen emisiones de carbón que **contaminan el aire y al estar aproximadamente a 2Km del caserío, los habitantes han venido padeciendo afectaciones en su salud, actividades económicas y contaminación al ambiente** de su zona como se puede comprobar por la Resolución 01749 de 2016 expedida por Corpoguajira”, reza la Sentencia del Alto Tribunal.

Por ultimo la lideresa Wayuu aseguró que estarán atentos de las disposiciones de la Corte para pactar una fecha de realización de la consulta Previa y que el pueblo Wayuu seguirá movilizándose, exigiendo sus derechos y **soluciones por parte del Gobierno para la crisis humanitaria que atraviesan, representada en la alta mortandad de niños y niñas,** carencia de centros educativos y de salud, dificultades de comunicaciones y vías y presencia de grupos paramilitares.

[Sentencia T-704-16 de la Corte Constitucional](https://www.scribd.com/document/340751332/Sentencia-T-704-16-de-la-Corte-Constitucional#from_embed "View Sentencia T-704-16 de la Corte Constitucional on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_74515" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/340751332/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-VEAUeVbtG9ote8zf7KFJ&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6536231884057971"></iframe>  
<iframe id="audio_17322921" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17322921_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
