Title: Xenofobia y racismo marcan coyuntura electoral en Estados Unidos
Date: 2016-07-22 13:21
Category: El mundo, Otra Mirada
Tags: Elecciones Estados Unidos, Racismo Estados Unidos
Slug: xenofobia-y-racismo-marca-coyuntura-electoral-en-estados-unidos
Status: published

###### [Foto: ][elherladodesaltillo] 

###### [22 de Jul] 

Después de los diferentes suceso de racismo en contra de la población afro que se han vivido en Estados Unidos, el día de ayer Donald Trump pronunció su discurso que oficializa su candidatura desde el partido Republicano, **con mensajes fuertes en contra de la población emigrante de este país.**

De acuerdo con la activista colombiana Amy Vélez, Estados Unidos esta afrontando una crisis de tiempo atrás, que empieza a evidenciarse desde la **sinceridad de un político xenófobo y racista como lo es Donald Trump** y que ha podido reflejarse con las nuevas tecnologías que han capturado la violencia y[los abusos de poder en las metrópolis del país hacia las comunidades negras y latinas.](https://archivo.contagioradio.com/estados-unidos-minimiza-su-responsabilidad-en-torturas-de-la-cia/)

"Imaginate cuando en este país llegue la crisis económica, con el discurso racista, la gente estará matando a todo el que no se parezca a ellos, a los emigrantes, y somos nosotros los que hemos traído el trabajo , **nosotros los emigrantes hacemos el trabajo que nadie quiere hacer**, no importa cuanto tiempo pase, [la historia de Hittler se puede repetir en este país" afirmo la activista](https://archivo.contagioradio.com/estados-unidos-no-es-el-dueno-del-continente-adolfo-perez/)".

Sobre la candidatura de Trump, Vélez afirmó que no considera que el vaya a ganar la presidencia de Estados Unidos debido a que **es un político poco conveniente para los mercados internacionales y las relaciones corporativas del país**,"se habla de democracia, no son los presidente son las grandes corporaciones las que deciden como se va a hacer y que se va a hacer, **Trump  no va a ser presidente de Estados Unidos**" .

Finalmente sobre el plebiscito por la paz y la votación por parte de colombianos ubicados en Estados Unidos, Amy Vélez aseguro que **se estan llevando a cabo diferentes actividades que hablan sobre los acuerdos y aclaran muchos de los puntos** pactados por el gobierno de Colombia y la guerrilla de las FARC-EP.

"No podemos seguirnos matando y desangrando, **se aclararon muchas mentiras que los medios de información dicen en Colombia**, una cantidad de mentiras que se han convertido en verdades".

<iframe src="http://co.ivoox.com/es/player_ej_12308156_2_1.html?data=kpegkp2VeZehhpywj5eWaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncaLh2pDDx9HJvoampJCuxdnNusrn1caYxdTQs87Wysbbw5DJsoy5tqaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
