Title: Así se estarían reclutando jóvenes en Bogotá
Date: 2018-07-19 13:01
Category: DDHH, Movilización
Tags: bandas criminales, Bogotá, Hollman Morris, jóvenes en Bogotá, reclutamiento forzado
Slug: asi-se-estarian-reclutando-jovenes-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Ciudad-Bolivar-e1460565457378.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [19 Jul 2018]

El Concejal de Bogotá por los Progresistas, Hollman Morris, denunció que en la capital se podría estar generando el reclutamiento forzado de jóvenes en las localidades de Kennedy, Bosa y Ciudad Bolívar, luego de recibir la denuncia de la desaparición de 10 jóvenes. A estos hechos se suma el asesinato de **64 jóvenes en los primeros 5 meses de este año**, que para Morris evidencia el aumento de control de bandas criminales en estos territorios.

**Los 10 jóvenes fueron reportados desaparecidos en la localidad de Bosa y desde el mes de abril la Defensoría del Pueblo activo la alarma temprana,** sin embargo, hasta ayer este tema llegó al Concejo de Bogotá, producto de la denuncia de Morris, en la que señaló que fue un líder afro de ese territorio el que afirmó que estarían llegando hombres a realizar reclutamientos.

Según el Concejal, las personas reclutadas estarían entre los 15 a 19 años y se les ofrece un pago por un millón de pesos que luego no es entregado, y en caso dado que decidan salir de estas estructuras se les amenaza con asesinarlos a ellos o a sus familiares. Asimismo, afirmó que las organizaciones armadas que estarían detrás de estos reclutamientos serían las **Autodefensas Unidas de Colombia, las Águilas Negras, Los Paisas, Clan del Golfo, Los Rastrojos y disidencias de las FARC**.

Además, el concejal señaló que los jóvenes entrarían a hacer parte de toda una cadena de narcotráfico desde la distribución de las drogas en los barrios, la producción de las mismas en otras regiones del país, hasta el ingreso a las estructuras armadas. (Le puede interesar: ["Militarización del Cauca no impidió asesinato de 2 líderes sociales esta semana"](https://archivo.contagioradio.com/militarizacion-del-cauca-no-ha-impedido-asesinatos-de-dos-lideres-sociales-esta-semana/))

### **La respuesta de las Autoridades distritales  ** 

Referente a las acciones por parte de las autoridades, Morris denunció que la Alcaldía no ha emitido ningún tipo de respuesta, incluso cuando por ley, tendría que haberse **manifestado 10 días después de la activación de la alerta temprana de la Defensoría**.

En esa medida, Morris solicitó a la Policía Distrital **un organigrama con los líderes y cabezas de las bandas del narcotráfico** que operan en la capital. De igual forma pidió que se dé a conocer los decomisos que hace la Policía para conocer mucho mejor la fuerza del narcotráfico en la capital.

“Sí Bogotá sigue por este camino, de un exacerbado crecimiento de los cultivos del narcotráfico y una guerra de los ejércitos de estructuras armadas, podrá convertirse en la meca del narcotráfico en Colombia, y pueden presentarse fenómenos a mediano plazo como un control territorial, por parte de esos ejércitos, en los barrios populares” afirmó Morris.

<iframe id="audio_27143623" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27143623_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

###  
