Title: En el día del maestro, docentes exigen que se salde crisis de la Educación
Date: 2018-05-15 13:43
Category: Educación, Política
Tags: Docentes, fecode, Ministerio de Educación
Slug: en-el-dia-del-maestro-docentes-exigen-que-se-salde-crisis-de-la-educacion
Status: published

###### [Foto: Contagio Radio] 

###### [15 May 2018] 

De acuerdo con William Agudelo, presidente de la Asociación Distrital de Educadores, hoy se conmemora la lucha de las y los docentes del país **"por construir una educación pública"**. Sin embargo, el último paro de los maestros no ha dejado un balance muy alentador, debido a que aún no hay respuestas desde el Ministerio de Educación, que den soluciones prontas a la crisis que vive el gremio.

### **¿Qué piden las y los docentes en Colombia?** 

Para Agudelo, los maestros en el país afrontan una grave crisis porque no se han destinado los recursos necesario que garanticen el derecho a la educación, "**en este momento hay estudiantes protestando en Usme por la falta de maestros"** afirmó el docente y agregó que también hay colegios que se están derrumbando y otros en donde no se esta prestando el servicio de comedor escolar.

Frente a las exigencias que le hacen al Ministerio de Educación, Agudelo señaló que están pidiendo que se aumente el presupuesto para la Educación y denunció que ya se están retrasando los pagos de las pensiones por invalides y jubilación, al igual que tampoco se han generado concursos para el ingreso de más docentes para que ingresen a los colegios, mientras que en **Bogotá el número de maestros provisionales ha ascendido a 8.000. **

El próximo paso de los maestro será realizar una asamblea nacional en la que, de no haber respuesta por parte del Ministerio de Educación, decidan si van a un paro nacional permanente. "La ministra lo que esta haciendo son mesas y ahí ve que va a resolver, se ha planteado que los puntos de negociación no los ha tenido en cuenta".[(Le puede interesar: "FECODE se prepara para el paro del 9 y 10 de mayo")](https://archivo.contagioradio.com/fecode-llama-a-paro-definido-los-proximos-9-y-10-de-mayo/)

<iframe id="audio_26011128" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26011128_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
