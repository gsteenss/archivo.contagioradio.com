Title: Roban base de datos con información sobre asesinatos de sindicalistas en Colombia
Date: 2016-08-29 12:08
Category: DDHH, Nacional
Tags: Derechos Humanos, Robo de informacion, Sindicalistas
Slug: roban-base-de-datos-con-informacion-sobre-asesinatos-de-sindicalistas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Escuela-Nacional-Sindical.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Telemedellín 

###### [29 Ago 2016] 

La sede de la Escuela Nacional Sindical, en San Benito, Medellín, fue asaltada por desconocidos que robaron el **computador institucional del director, Carlos Julio Díaz,** como lo denunciaron ante las autoridades competentes.

Por el techo de la sede, estas personas habían logrado ingresar a la sede. Según la denuncia denunció, hicieron una perforación y accedieron a las instalaciones, revolcaron la oficina del director, así como los escritorios del área administrativa y de la subdirección, violentando las gavetas de los escritorios. Lo grave del hecho, es que en el computador el director de la ENS, **tenía una base de datos sobre violaciones a los derechos humanos de varios sindicalistas, así como otros documentos que tenían guardados en otras gavetas.**

“Tenemos una de las bases de datos más completas sobre hechos de violencia, **desde 1977. Con información de más de 3 mil asesinatos de sindicalistas,** que esperamos sean insumo para la  justicia transicional acordada entre el gobierno y las FARC”, explica Carlos Julio Díaz, quien agrega que aunque todavía no se atreven a decir que se trata de una actuación con fines políticas, “**genera preocupación que se hayan llevado computador y no otros objetos de valor,** como las tarjetas de crédito u otros elementos que teníamos”.

La Escuela había sido objeto de problemas de seguridad hace algunos años, lo que motivó que hoy tengan un esquema de seguridad, sin embargo en el último tiempo no habían sufrido de algún tipo de acciones de esta índole.

Ante ese hecho, la Escuela Nacional Sindical denunció ante las autoridades pertinentes, y solicitan que se adelanten las investigaciones pertinentes para esclarecer las motivaciones del hecho, y las medidas necesarias para que no se vuelvan a presentar. Además denuncian que “sólo hasta hoy fue la Sijin a  sede de la escuela, para atender el robo que sucedió el pasado jueves”.

<iframe src="http://co.ivoox.com/es/player_ej_12704220_2_1.html?data=kpekkpmWdpGhhpywj5WcaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCucoy4hqigh6aopduZk6iYp9jHucbgwpC7w8jNs8_VzZDAy9PIrcTVzZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
