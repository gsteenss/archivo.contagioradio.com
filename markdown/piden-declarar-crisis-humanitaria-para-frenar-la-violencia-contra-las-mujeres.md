Title: Piden declarar crisis humanitaria  para frenar la violencia contra las mujeres
Date: 2020-07-01 23:23
Author: AdminContagio
Category: Actualidad, Mujer
Tags: Emergencia Nacional, feminicidio, Violencia contra las mujeres
Slug: piden-declarar-crisis-humanitaria-para-frenar-la-violencia-contra-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Feminicidio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: MDZ

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El [Movimiento Político Estamos Listas](https://twitter.com/Estamos_Listas/status/1275455578567880704) solicitará ante la Presidencia de la República **la declaratoria  de Crisis Humanitaria de Emergencia Nacional por violencia contra las mujeres** para exigir las medidas administrativas, presupuestales y judiciales necesarias para erradicar estos tipos de violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Dora Saldarriaga, concejala de este movimiento, **«de enero a mayo de 2020 han asesinado 315 mujeres, 16.473 más, han sido víctimas de violencia intrafamiliar, 6.400 víctimas de lesiones no fatales y se han registrado feminicidios en 15 departamentos de los 32 existentes, es decir en un 47% del territorio nacional».**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **«Hay más mujeres víctimas de violencia que contagiadas con Covid-19»**
>
> <cite>Concejala Dora Saldarriaga</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La iniciativa también busca que se desarrolle una política institucional con perspectiva de género y que se haga un seguimiento exhaustivo a los casos de agresión y violencia en contra de la mujer, ya que «El Estado no se está haciendo cargo de ninguna de las violencias que sufren las mujeres, pues no se han hecho mayores esfuerzos para esclarecer e investigar los hechos victimizantes». **«Hay más mujeres víctimas de violencia que contagiadas con Covid-19»**, señala Saldarriaga. (Le puede interesar: [Anhelo de justicia](https://archivo.contagioradio.com/anhelo-de-justicia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Saldarriaga, apunta que **casi un 80% de los feminicidios se podría prevenir si se hiciera un control efectivo a las medidas de protección que se conceden a mujeres víctimas de violencia**. (Lea también: [Feminicidios son sólo la punta del iceberg: Limpal](https://archivo.contagioradio.com/feminicidios-son-solo-la-punta-del-iceberg-limpal/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Restitución de derechos en los casos de violencia contra las mujeres

<!-- /wp:heading -->

<!-- wp:paragraph -->

Milena Mazabel, indígena del pueblo Kokonuko y abogada defensora de DD.HH, afirma frente al abordaje de las instituciones en el caso de la niña Embera que en la restitución de derechos de niños y niñas indígenas hay una falencia institucional respecto de los enfoques diferenciales, ya que, estos procesos se hacen con base al tratamiento general de la población nacional sin tener en cuenta las particularidades culturales de cada etnia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Hay una deuda histórica  frente a los enfoques étnico-diferencial, que implican tener en cuenta las dinámicas culturales de los pueblos, la ubicación geográfica, la situación socio-económica y la diversidad lingüística»  **
>
> <cite>Milena Mazabel, Indígena del pueblo Kokonuko & Abogada defensora de DD.HH.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

También señala que para estos procesos de restablecimiento de derechos se requiere la participación activa de médicos tradicionales y un acompañamiento por parte de las comunidades indígenas haciendo énfasis en que **lo que se vive en estos casos es un dolor colectivo por parte de los pueblos indígenas que no solo involucra a las víctimas y su núcleo cercano.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La abogada indígena también denuncia las presuntas amenazas que han recaído en contra de la familia de la niña Embera tras haber trascendido el hecho.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Es preocupante la situación de la familia, ya que, en estos momentos estaría siendo amenazada por haber hecho la denuncia respecto de este hecho atroz»**
>
> <cite>Milena Mazabel, Indígena del pueblo Kokonuko & Abogada defensora de DD.HH.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La defensora cuestiona la forma en que operan los procesos de restitución de derechos, pues estos no son procesos que se puedan hacer en un día, sino que «*lleva años para una mujer recuperar su autonomía, autoestima y reconocimiento porque son muy grandes las fracturas psicológicas y físicas que se dejan*». (Le puede interesar: [¿La justicia restaurativa puede contrarrestar la violencia de género?](https://archivo.contagioradio.com/justicia-restaurativa-puede-contrarrestar-violencia-genero/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En igual sentido, Fany Kuiru Castro, coordinadora de Mujer, Niñez y Familia de la Organización Nacional Indígena de los Pueblos Indígenas de la Amazonia Colombiana (OPIAC), afirma que toda violencia contra la mujer representa un daño integral para ella y su comunidad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Cuando se viola a una mujer se le está destruyendo de manera integral, pues no se le destruye solo físicamente, sino que también se destruye su espíritu y su vida**»
>
> <cite>Fany Kuiru Castro, Coordinadora de Mujer, Niñez y Familia de la Organización Nacional Indígena de los Pueblos Indígenas de la Amazonia Colombiana (OPIAC)</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### ¿Qué justicia debe atender los casos de violencia en comunidades indígenas?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Fany Kuiru, señala que el desarraigo de algunas costumbres que ha producido la intromisión de la «civilización» en las prácticas de los pueblos indígenas, ha llevado a que los mecanismos de juzgamiento al interior de algunos grupos étnicos no sean suficientes para dirimir los casos de violencia contra la mujer y deban complementarse con las leyes de la legislación colombiana. (Lea también: [Agresión sexual del Ejército contra menor embera es un ataque a los 115 pueblos indígenas del país: ONIC](https://archivo.contagioradio.com/agresion-sexual-del-ejercito-contra-menor-embera-es-un-ataque-a-los-115-pueblos-indigenas-del-pais-onic/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **Una situación como esa se tiene que manejar en coordinación con la justicia ordinaria**»
>
> <cite>any Kuiru Castro, Coordinadora de Mujer, Niñez y Familia de la Organización Nacional Indígena de los Pueblos Indígenas de la Amazonia Colombiana (OPIAC)</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

No obstante, enfatiza en que esto varia de comunidad a comunidad pues todos los pueblos indígenas se diferencian entre sí y cuentan con diferentes mecanismos de castigo y juzgamiento.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/587218102159968/?v=587218102159968","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/587218102159968/?v=587218102159968

</div>

<figcaption>
Otra Mirada: Restitución de derechos para mujeres y niñas indígenas ¿Cómo parar la impunidad de la violencia de género que afecta a las mujeres indígenas?

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
