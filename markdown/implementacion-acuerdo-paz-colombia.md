Title: Implementación del acuerdo de paz necesita el esfuerzo de todos
Date: 2017-01-22 10:00
Category: Entrevistas, Paz
Tags: acuerdos de paz, FARC-EP, Implementación, simon trinidad
Slug: implementacion-acuerdo-paz-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/implementacion-acuerdos-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 20 Ene 2016 

Luego de la reunión de esta semana entre algunos delegados del gobierno para las conversaciones de paz y delegados de las FARC en los llanos del Yarí, se concluyó que el 31 de enero es la fecha límite para la concentración de integrantes de esa guerrilla en las Zonas Veredales Transitorias de Normalización (ZVTN). Carlos Lozano afirmó que **a pesar de los avances hay un ambiente de tensión** **que podría afectar la implementación de los acuerdos**.

**Medios, generadores de opinión y extrema derecha tienen al proceso de paz en un “clima de tensión”**

Para Lozano, aunque las FARC y el gobierno han logrado resolver los problemas que se han atravesado, si **hay una afectación sobre todo por una actitud del gobierno que por una parte no previó la instalación de las ZVTN** y por otra la actitud en torno a **negación de la presencia paramilitar** y las campañas de difamación contra los acuerdos y lo que se ha avanzado de implementación como sucedió en la comunidad del “Conejo”.

En ese sentido, el director del Semanario VOZ afirma que no se entiende para dónde va la estrategia de decir que no hay paramilitarismo “¿para dónde van argumentos como que el atentado a Imelda Daza era un intento de robo de una cadena de oro a uno de los escoltas?” para el analista **este tipo de respuestas no tienen sentido y contribuyen a entorpecer el clima de construcción de paz**.

Esta situación tendría una salida **con un esfuerzo común y muy grande de todos para sacar adelante este proceso**, que ya está avanzado puesto que “no se puede echar por la borda” y debe haber una reacción de parte del Frente Amplio y las fuerzas sociales. “tiene que darse una reacción” no solamente con el proceso con las FARC sino también al proceso con el ELN.

**Francia ha sido coherente en su postura de respaldo al proceso de paz**

Por otra parte Lozano calificó como **muy importante la posición del gobierno francés**, que se manifiesta a través de la próxima visita de Francoise Hollande. Según el analista los reclamos del ex presidente Uribe no caben puesto que su gobierno usó a Francia en sus programas de guerra e Colombia.

Lozano también resaltó el compromiso de Alemania, que a través de su ministro de relaciones exteriores, y argumentó que **con esas expresiones se evidencia que hay un apoyo mayor por parte del mundo que el apoyo que tiene el proceso al interior del país**. Le puede interesar: "[Jurisdicción Especial de Paz debe ser fiel a víctimas del conflicto armado" Iván Cepeda](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-debe-ser-fiel-a-las-victimas-del-conflicto-armado-ivan-cepeda/)

**No indultar a Simón Trinidad fue una muestra de la mezquindad de Estados Unidos**

“Ahí está pintados los gringos” fue la frase con la que Lozano calificó el hecho de que se negara el indulto presidencial al Simón Trinidad. Según Lozano, la única posibilidad de que los juzgados de ese país condenaran a Trinidad **tenía como requisito reconocer su condición de rebelde pero lo que hicieron** fue condenarlo por el secuestro de 3 norteamericanos, con lo cual “no tuvo nada que ver”.

Lozano afirma que **Estados Unidos dijo respaldar el proceso de paz pero cuando se trata de situaciones tan concretas como la libertad del integrante de las FARC no lo hacen**. “lo están condenando a muerte” afirmó el analista.

<iframe id="audio_16564681" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16564681_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
