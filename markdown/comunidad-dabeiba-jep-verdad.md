Title: Comunidad de Dabeiba considera hallazgo de la JEP como un paso hacia la verdad
Date: 2019-12-16 16:32
Author: CtgAdm
Category: DDHH, Nacional
Tags: Dabeiba, Ejecuciones Extrajudiciales, jurisdicción especial para la paz
Slug: comunidad-dabeiba-jep-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Dabeiba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  @JEP\_Colombia] 

Ante el anuncio de la Jurisdicción Especial para la Paz (JEP) del hallazgo de una fosa común en el cementerio Las Mercedes en Dabeiba, Antioquia, que corresponde al lugar donde se encontrarían los restos de más de 50 personas presentadas ilegítimamente como bajas en combate, los habitantes del municipio han señalado que este es solo el inicio del camino para comenzar a revelar la responsabilidad de  agentes de la Fuerza Pública y grupos al margen de la ley en el conflicto armado.

El trabajo de campo de la JEP en este municipio comenzó después de escuchar las versiones de comparecientes de la Fuerza Pública que pertenecieron a la Brigada XI del Ejército quienes afirman haber participado en el asesinato y entierro de entre 45 y 75 personas personas entre 2005 y 2007. Dichas declaraciones hasta el momento han permitido hallar 7 cadáveres completos y diversas estructuras óseas que  están siendo identificadas por Medicina Legal.

De cara al hallazgo de la JEP, Henry, integrante de la comunidad de Vida y Trabajo de La Balsita en Dabeiba,  afirma que aunque el municipio ha sido atropellado por el conflicto por más de tres décadas, este descubrimiento deja una sensación de esperanza y un indicio que guía al país para esclarecer la verdad, "creemos que estos espacios son los que deberían estar revelándose día a día para que más pronto se reconozca gran parte de la verdad" afirma Henry.

Aunque algunos de los habitantes ya habían denunciado cómo La Merced era usado para estos fines, Henry admite que resulta sorpresivo hallar víctimas de ejecuciones extrajudiciales en un lugar considerado sagrado, **"nos deja con más interrogantes y sorprendidos de por qué tenían que estar en ese territorio  tan espiritual y que se ha convertido en un espacio de horror".**

Organizaciones como la Comisión Intereclesial de Justicia y Paz advirtieron incluso desde el 2008, casi una década antes de este hallazgo, sobre la práctica de ejecuciones por parte de las fuerzas militares en Dabeiba como consecuencia de operaciones de presencia militar y paramilitar que actuó en gran medida sobre la población civil. [(Le puede interesar: Más de 180 batallones cometieron ejecuciones extrajudiciales entre 2002 y 2008)](https://archivo.contagioradio.com/human-rights-watch-expone-su-ultimo-informe-sobre-falsos-positivos-en-colombia/)

Según la Coordinación Colombia-Europa-Estados Unidos en su informe Ejecuciones Extrajudiciales : Realidad Inocultable, entre 2002 y 2007 se habían registrado en Colombia **1.122 casos** de ejecuciones extrajudiciales atribuibles directamente a la Fuerza Pública, en contraste con **669** casos registrados entre enero de 1997 y junio de 2002. Frente a este dato, entre 2007 y 2008, **el departamento de Antioquia registró 65 víctimas representando el 12% de los casos a nivel nacional. **

"Siempre manteníamos la hipótesis de cuánta violencia y desaparición y asesinatos habían ocurrido"  expresa el habitante de la comunidad al referirse a los hechos de violencia que se vivieron desde mediados de los años 90  y que continuaron con más fuerza durante el mandato de Álvaro Uribe, y que en la actualidad comienza a "revelar poco a poco las verdades de todos los hechos oscuros y cometidos, bien sea por la Fuerza Pública o grupos al margen de la ley".

### Dabeiba aún guarda secretos

El habitante de la comunidad señala que en Dabeiba aún existen mucha información que todavía no ha salido a la luz relacionada a desapariciones forzadas cometidas en sitios estratégicos como el Cañón de la Llorona, el puente de Urama y la vereda Cajones, que se han prestado para hacer estas acciones criminales. [(Le puede interesar: Cinco casos por los que la JEP pone la lupa a crímenes en Urabá y Bajo Atrato Chocoano)](https://archivo.contagioradio.com/cinco-casos-los-la-jep-pone-la-lupa-crimenes-uraba-atrato-chocoano/)

**"De Dabeiba podemos esperar más, así como se dio este primer paso, hay mucho más que contar y aportarle a la verdad"** agrega Henry, quien asegura que en el municipio pervive el conflicto promovido por estructuras organizadas que buscan el control urbano y rural.

Con relación a la Fuerza Pública y su responsabilidad en los hechos que han salido a la luz, afirma que la comunidad está a la expectativa de que por parte del Ejército una mayor contribución para esclarecer estos hechos, que lo que han hecho es afectar "la credibilidad y confianza de quienes defienden el territorio nacional" y de quienes se esperan contribuyan al posconflicto. [(Lea también: JEP pide información de 9 brigadas implicadas en ejecuciones extrajudiciales) ](https://archivo.contagioradio.com/9-brigadas-implicadas-en-ejecuciones-extrajudiciales/)

La diligencia adelantada por la Jurisdicción Especial para la Paz en el marco del caso 003 que ya ha adelantado  160 versiones por parte de uniformados de la Fuerza Pública, no solo resulta clave para adelantar su labor de justicia transicional, la Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD), entidad que también hace parte del Sistema Integral de Justicia ha señalado que este es un avance "muy importante para las familias de las personas que desde años no tienen información del paradero de sus seres queridos" y que a partir de este hallazgo podrían comenzar a identificar.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45517098" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45517098_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
