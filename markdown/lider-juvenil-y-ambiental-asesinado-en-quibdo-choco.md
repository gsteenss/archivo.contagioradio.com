Title: Líder juvenil y ambiental asesinado en Quibdó, Choco
Date: 2020-12-03 12:42
Author: AdminContagio
Category: Actualidad, Nacional
Tags: asesinato de líderes sociales, Chocó, Quibdó
Slug: lider-juvenil-y-ambiental-asesinado-en-quibdo-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este miércoles 2 de diciembre se confirmó el asesinato del líder ambiental y juvenil Harlin David Rivas Ospina en el barrio Yesca Grande de Quibdó, departamento del Chocó.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1334290602012368896","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1334290602012368896

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De acuerdo con el [Instituto de estudios para el desarrollo y la paz -Indepaz](http://www.indepaz.org.co/lideres/), el joven ingeniero fue asesinado el pasado lunes 30 de noviembre en horas de la mañana, cuando sicarios que se movilizaban en motocicleta, llegaron al establecimiento donde Rivas trabajaba y atentaron contra su vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El líder juvenil era integrante de la Red Nacional Jóvenes de Ambiente y estudiante de Ingeniería Ambiental de la Universidad Tecnológica del Chocó y participó en el proyecto Eduapazcífico en el año 2017 de la Organización Social Manos Visibles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el mismo 30 de noviembre en que Rivas fue asesinado, se conoció del asesinato del comunero y líder indígena, Romelio Ramos Cuetia, en la zona rural del municipio de Suárez, en el norte del departamento del Cauca. (Le puede interesar: [Líder indígena es asesinado en Suárez, Cauca](https://archivo.contagioradio.com/lider-indigena-es-asesinado-en-suarez-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El asesinato de los liderazgos continúa en el país y es que con Harlin David Rivas Ospina, **ya son 262 los líderes, lideresas y defensores de derechos humanos asesinados en lo corrido de este año**. (Le puede interesar: [Agresiones contra defensores de DD.HH. ascendieron a 184 entre julio y septiembre](https://archivo.contagioradio.com/184-agresiones-contra-defensores-de-dd-hh-entre-julio-y-septiembre/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
