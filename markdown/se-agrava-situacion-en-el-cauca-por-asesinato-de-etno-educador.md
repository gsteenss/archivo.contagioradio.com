Title: Se agrava situación en el Cauca por asesinato de etno educador
Date: 2017-08-16 13:37
Category: DDHH, Nacional
Tags: asesinato de educador, Cauca, CRIC, etno educador, resguardo indigena quintana
Slug: se-agrava-situacion-en-el-cauca-por-asesinato-de-etno-educador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/resguardo-indigena-e1502908613560.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Resguardo Indígena Quintana] 

###### [16 Ago 2017] 

El 15 de agosto, a las 7:20 de la mañana **fue asesinado el etno educador José Argumero Campo** quien era profesor de la comunidad indígena Quintana en el pueblo Kokonuko en el departamento del Cauca. El Congreso Regional Indígena del Cauca CRIC, rechazó y condenó el asesinato.

Según el gobernador Alberto Sánchez del resguardo indígena Quintana, en el sitio conocido como La Laguna en el municipio de Popayán, **“el profesor iba de camino al trabajo en el colegio y fue asesinado”**. Indicó además que en días pasados dos motos lo habían seguido. (Le puede interesar: ["Hombres armados secuestran a líder en Caldono, Cauca"](https://archivo.contagioradio.com/44152/))

El Congreso Regional Indígena del Cauca indicó que el etno educador **llevaba 5 años trabajando como profesor** de la “construcción e implementación del sistema de educación indígena propio SIEP de la comunidad indígena Quintana”.

Si bien aún se desconocen los motivos del crimen, el CRIC, ha exigido **“respeto por los derechos humanos,** a la vida como derecho fundamental, a transmitir nuestra cultura como parte del proceso político organizativo de los pueblos indígenas”. (Le puede interesar: ["En 2017 han sido asesinados 15 líderes sociales en el Cauca"](https://archivo.contagioradio.com/en-2017-han-sido-asesinados-15-lideres-sociales-en-el-cauca/))

Las autoridades de Policía se encuentran realizando una investigación de los hechos **para determinar los móviles y los responsables del crimen**. Por el momento, de acuerdo con el gobernador del resguardo, reforzarán la guardia indígena “para saber quién entra y quién sale del resguardo”.

<iframe id="audio_20371369" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20371369_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
