Title: Suspendida consulta popular Minera en Cajamarca por fallo de Consejo de Estado
Date: 2016-12-19 11:23
Category: Ambiente, Nacional
Tags: Cajamarca, Comité Ambiental y Campesino de Cajamarca y Anaime, consulta popular minera
Slug: suspendia-consulta-popular-minera-en-cajamarca-por-fallo-de-consejo-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cajamarca ] 

###### [19 Dic 2016] 

El Consejo de Estado  denegó la pregunta que se realizaría en la Consulta Popular minera en Cajamarca, motivo por el cual queda suspendida. Sin embargo pese a que aún el comité promotor del no a la minería en este lugar, no ha sido avisada, informaron que **ya activaron mecanismos para cambiar la pregunta y retomar lo más pronto posible el proceso.**

De acuerdo con el ente la pregunta ¿Está usted de acuerdo SI o NO con que en el Municipio de Cajamarca se ejecuten actividades que impliquen contaminación del suelo, pérdida o contaminación de las aguas o afectación de la vocación tradicional agropecuaria del municipio, con motivos de proyectos de naturaleza minera? “carece de neutralidad y lesiona la libertad del votante”, razón por la que **ordena replantear la pregunta y mantener en pie, pero suspendida, la posibilidad de que se realice la consulta.**

Este fallo, según Robinson Mejía, promotor y miembro del Comité al No en la consulta popular minera de Cajamarca,  es producto de una tutela interpuesta por la multinacional **Anglogold Ashanti que pretende es dilatar la consulta y continuar con sus labores en el territorio**. Le puede interesar: ["Consulta popular minera en Ibague por fin tiene vía libre"](https://archivo.contagioradio.com/consulta-popular-minera-ibague-fin-via-libre/)

A su vez añade que la creación de la pregunta se dio en un  marco jurídico diferente y que buscaba prohibir la realización de una minera contaminante, pero no en todas las escalas, ni de todos los tipos. **“Hoy estamos pensando en una pregunta que va dirigida a cierto tipo de minería que es la de metales, porque creemos que es la que más contamina**” afirmó Mejía.

Desde el año 2014 pobladores del municipio de Cajamarca se encuentran adelanto este proceso para realizar una consulta minera en su territorio, en el año 2015 el Concejo municipal lo negó, sin embargo en noviembre de este año el Tribunal Superior dio vía libre para su ejecución.

<iframe id="audio_15173789" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15173789_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
