Title: Internos de La Picota completan una semana de desobediencia civil
Date: 2016-07-25 15:45
Category: DDHH, Nacional
Tags: crisis cárcel La Picota, Crisis carcelaria colombia, crisis salud colombia
Slug: internos-de-la-picota-completan-una-semana-de-desobediencia-civil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-carcelaria1-e1463780527863.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  ] 

###### [25 Julio 2016] 

Los internos de La Picota completan una semana de desobediencia civil que contemplan extender **hasta que haya una solución integral frente a la crisis de atención en salud** que enfrentan. De acuerdo con el profesor Miguel Ángel Beltrán, la situación "es verdaderamente crítica y viene de tiempo atrás"; sin embargo, ninguna entidad del Estado ha velado por solucionarla.

Los reclusos exigen que haya una **valoración integral de las personas enfermas, consultas con especialistas, cirugías y tratamientos completos**, que incluyan tanto la realización de exámenes como la entrega de medicamentos. Así mismo llaman la atención frente a la epidemia que se está presentando en la penitenciaria y que agudiza la ya crítica situación por la atraviesan cerca de 400 internos que padecen enfermedades.

Hasta el momento **3 reclusos se han cocido la boca y se estima que sean más quienes se unan** a la medida hasta que haya una solución real, según afirma el profesor Beltrán, por lo que hace "un llamado a la solidaridad, al acompañamiento porque realmente [[la situación en las cárceles](https://archivo.contagioradio.com/internos-de-la-picota-se-declaran-en-desobediencia-civil/)] se ha tendido a invisibilizar por los medios oficiales de comunicación".

<iframe src="http://co.ivoox.com/es/player_ej_12333387_2_1.html?data=kpeglZiXfJihhpywj5WcaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5ynca7dyNrSzpClssjZzZCvx9HYtoa3lIquk9OJdqSfpdTQx9PYqYzJz87jx9fXrdXV087cj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
