Title: La Universidad Nacional se quedó sin presupuesto para contratar docentes
Date: 2015-02-17 21:15
Author: CtgAdm
Category: DDHH, Nacional
Tags: Derecho a la eduación, Ley 30 de 1992, Mesa Amplia Nacional Estudiantil, Universidad Nacional de Colombia
Slug: la-universidad-nacional-se-quedo-sin-presupuesto-para-contratar-docentes
Status: published

###### Foto: Carmela María 

##### <iframe src="http://www.ivoox.com/player_ek_4096490_2_1.html?data=lZWmmJmddI6ZmKiakp2Jd6KlmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKfttPW2MrWt8rYwsmYsMbHrdDiwtGY1cqPtdbZxYqwlYqmd4znytOY0tfJt9bk1srg1tSPtMLmwpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Gabriela Baquero, Representante de Ciencias Humanas, Universidad Nacional] 

##### [Diana Corralain, Representante de Literatura, Universidad Nacional] 

 

La **Universidad Nacional de Colombia** empezó su semestre académico la primera semana de febrero, pero son muchos los estudiantes de la Facultad de Ciencias Humanas que aún no han podido empezar sus clases.

La Facultad **requiere 843 millones de pesos** para contratar a docentes temporales que les permitan desarrollar su semestre, pero **faltan 465 millones de pesos**. Por este motivo, materias de fundamentación disciplinar **no cuentan con docentes asignados a pesar de haber sido ofertadas desde el 2014**. Además, tiene un **déficit de 1.910** millones de pesos para contratar a personal administrativo y de funcionamiento. Las directivas de la Facultad han declarado la ***situación de emergencia financiera***.

El problema no es nuevo en la Universidad. Como resultado de la **Ley 30 de 1992** y la resultante y continua desfinanciación de la institución por parte del Estado, la Universidad se ha visto obligada a buscar nuevos mecanismos para sobrellevar la **crisis financiera**.

Una de las medidas que tomó la Universidad, fue el **congelamiento de la contratación de la planta docente**, y la contratación de **profesores ocasionales** por parte de las Facultades. Dado que la Universidad no tiene presupuesto, **cada Facultad ha tenido que buscar** (cada año en un mayor porcentaje) **el dinero para vincular de manera temporal a profesores que dicten materias**.

Este semestre, y a pesar de que la **Facultad de Ciencias Humanas** ha alquilado sus espacios a externos, ha vendido cursos de extensión para conseguir recursos, y ha cerrado programas de bienestar estudiantil, el presupuesto le es insuficiente.

La **rectoría de la Universidad**, en cabeza de **Ignasio Mantilla**, emitió una comunicación al decano de la Facultad de Ciencias Humanas, Ricardo Sánchez, en el que expresó que **los gastos no pueden exceder los ingresos**, y que "la Rectoría advierte la necesidad de acatar ese principio y, en consecuencia, recomienda efectuar un **plan de ajuste de gastos**". Los estudiantes de la Universidad Nacional se preguntan ¿dónde más esperan recortar gastos?

Ante la desesperada situación se han escuchado propuestas para buscar monitorías voluntarias e incluso profesores *ad honorem*, que puedan dictar clases que son fundamentales para la formación en psicología, trabajo social, sociología, entre otros.

Actualmente la **Universidad Nacional de Colombia** **ocupa el puesto 14 entre las 100 mejores universidades del América Latina**. ¿Cuánto tiempo más logrará ocupar este lugar, sin el presupuesto adecuado que le permita contratar profesores de planta apropiados para el desarrollo de una actividad académica de calidad, investigación de excelencia y extensión para la sociedad?
