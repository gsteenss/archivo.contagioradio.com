Title: Gaza se ha convertido en una cárcel a orillas del mar Mediterráneo
Date: 2016-03-21 09:00
Category: El mundo, Entrevistas
Tags: Cisjordania, conflicto palestina israel, Gaza, isabel perez periodista
Slug: gaza-se-ha-convertido-en-una-carcel-a-orillas-del-mar-mediterraneo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Gaza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Palestina Libre ] 

###### [21 Mar 2016] 

Gaza se ha convertido en una **cárcel a orillas del mar Mediterráneo en la que niños, jóvenes y adultos palestinos resisten desde hace 67 años la invasión de colonos y militares israelíes** que pueden disfrutar de bañarse en una piscina en el verano, mientras que ellos apenas cuentan con poca agua contaminada para lavar, cocinar y asearse, narra la periodista Isabel Pérez quien como ellos ha padecido enfermedades provocadas por las bacterias del agua que a diario debe usar.

Tras la intifada de 2014 el Gobierno israelí puso en marcha la operación militar **'Margen Protector' que ha cobrado la vida de por lo menos 186 palestinos, una cuarta parte de ellos menores de edad** que probablemente regresaban a casa después de la escuela o estaban en la calle caminando, porque hasta eso tienen prohibido los ciudadanos palestinos a los que militares israelíes privan de su derecho a la vida y a hacer uso de los bienes naturales de su propio territorio, como asegura Pérez.

En el marco de esta operación militar **700 niños han sido detenidos y enjuiciados por las fuerzas militares de Israel**, cuyos soldados responden con armas de fuego de alto nivel a las piedras y armas caseras que fabrican algunos jóvenes palestinos indignados con el bloqueo israelí, quienes además de afrontar el asesinato de sus familiares, enfrentan **altos niveles de desempleo, constantes bombardeos y las prohibiciones de circular libremente y reconstruir sus devastados hogares**.

La impunidad que busca legitimar el accionar de las fuerzas militares israelíes es tal que aun cuando se abren procesos contra soldados por el uso indebido de la fuerza, los representantes de Gobierno y altos mandos salen en su defensa y les "dan luz verde para disparar", según comenta Pérez quien agrega que el deseo de libertad, justicia e igualdad que desde distintas partes del mundo alienta la causa de Palestina, debe estar acompañado de **acciones jurídicas en tribunales internacionales como La Haya, que debido a presiones políticas lleva un año sin presentar fallos sobre los casos que allí se tramitan**.

Son por lo menos un millón ochocientos mil palestinos los que viven en Gaza y dos millones en Cisjordania, rodeados de colonias israelíes en las que como hay colonos religiosos que no están de acuerdo con que los palestinos vivan allí, **hay otros que consideran ilegal e ilegítima la situación de vulneración en la que vive el pueblo palestino y están de acuerdo con que se convierta en un Estado**, o en compartir con ellos un mismo territorio en igualdad de condiciones de vida, según narra Pérez.

Informar lo que acontece desde Gaza o Cisjordania es equiparable a un delito al que Israel responde con **detenciones injustificadas y arbitrarias, heredadas de la colonización británica**, o bombardeos a canales de televisión que han dejado sin vida a por lo menos 17 periodistas, para los que **ir a los alambrados en los que se emplazan militares y francotiradores israelíes se ha convertido en un suicidio**. Pérez asegura que los abusos contra periodistas se agudizan al mismo tiempo en que se intensifican los bombardeos, pues **no hay ningún mecanismo de protección para quienes son atacados directamente con balas o gases lacrimógenos**.

Desde diferentes países se gestan movimientos que trabajan para que las condiciones de vida del pueblo palestino mejoren, iniciativa que también es emprendida por algunos de los israelíes que viven en Gaza y Cisjordania, que no sienten ningún tipo de rencor hacia los palestinos y que proponen la **consolidación de un Estado laico en el que tanto a los israelíes como a los palestinos les sean garantizados sus derechos humanos** y no tengan que librar una guerra para sobrevivir. Isabel califica como positivas estas acciones e invita a informarse con diversas fuentes para "saber defender la narrativa palestina frente a Israel".

Frente a la injusticia y los abusos perpetrados por el Gobierno de Israel contra el pueblo palestino, la periodista afirma que el **"pilar de la resistencia está en la esperanza de encontrar una solución política al conflicto"**, lo que implica adoptar las medidas decretadas por la ONU y el apoyo de los países árabes que recientemente han mermado su ayuda a Palestina, desconociendo lo peligrosa que puede llegar a ser la consolidación de un Estado judío que se presenta como democrático pero que a diario ataca a las comunidades de campesinos palestinos que "necesitan respirar en paz y ser tratados con dignidad".

<iframe src="http://co.ivoox.com/es/player_ej_10881621_2_1.html?data=kpWlmpaadpKhhpywj5ecaZS1kZqah5yncZOhhpywj5WRaZi3jpWah5yncajV28aY1cqPrMKfxNTb2MrWuMrY0JDS0JDZssKfxIqwlYqlddPXxtGYw5DTtsrgzcbgjcnJsIzhwteYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
