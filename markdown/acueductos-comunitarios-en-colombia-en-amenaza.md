Title: Acueductos comunitarios en Colombia se encuentran bajo amenaza
Date: 2015-11-09 13:01
Category: Ambiente, Nacional
Tags: acueductos comunitarios, Censat agua Viva, Enda América Latina, multinacionales extractivistas, Penca de Sábila, V Encuentro Nacional de Acueductos Comunitarios
Slug: acueductos-comunitarios-en-colombia-en-amenaza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/acueductos-comunitarios1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

<iframe src="http://www.ivoox.com/player_ek_9327816_2_1.html?data=mpifmZ2Veo6ZmKiak5uJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc7Zz8bnw9iPpYzg0NiYw8jZqcXpxNnc1ZDHs87pz87hw9fNs9SfxtOYpdTQs87Wysaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [8 Nov 2015] 

“Para defender nuestros acueductos necesitamos una ley propia” esta fue una de las propuestas con la que  inicio en la ciudad de ** **Villavicencio el **V Encuentro Nacional de Acueductos Comunitarios “Aguas para la Paz”**.  Con la participación de distintas organizaciones sociales de todo el país como Censat Agua Viva, Penca de Sábila, Enda América Latina, Retaco y una gran cantidad de corporaciones y asociaciones que responden a gestiones comunitarias de acueductos en el territorio colombiano.

Entre las amenazas más preocupantes al derecho al agua las organizaciones denunciaron: las concesiones a multinacionales para la explotación minera, la explotación desmedida de acuíferos, la deforestación y la inexistente de gestión pública del agua.

Una de las razones por las que se realiza dicho encuentro es la **amenaza que sufre la gestión comunitarias de acueductos rurales**. El abandono estatal de su gestión y las nuevas políticas que van encaminadas hacia la privatización que pone en peligro la autonomía de las comunidades sobre el uso del agua.

Vale recordar que los acueductos rurales han decidido organizarse y mediante mesas de trabajo, talleres y dinámicas organizativas han ido recopilando propuestas, necesidades y formas de resistencia de todas las regiones de Colombia.

En el país  los acueductos rurales comunitarios han sido afectados por multinacionales extractivistas. Según diferentes estudios las fuentes hídricas han sido contaminadas con químicos que son utilizados por las empresas mineras, afectando la salud de las familias beneficiarias de estos sistemas de agua.

[![encuentro acueductos comunitarios](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/encuentro-acueductos-comunitarios.jpg){.wp-image-16866 .size-full width="700" height="467"}](https://archivo.contagioradio.com/amenazas-a-los-acueductos-comunitarios-en-colombia/encuentro-acueductos-comunitarios/)
:   V Encuentro Nacional de Acueductos Comunitarios “Aguas para la Paz”

Por otro lado está en juego la propia soberanía de las comunidades y la gestión de los recursos ambientales en su territorio. En un contexto en el que la paz entre insurgencia y estado colombiano esta a la orden del día, parte de las comunidades proponen y exigen que en el punto primero de los posibles acuerdos referido este a la temática agraria incluya la defensa o gestión de los acueductos comunitarios.

Por último, las organizaciones expresaron la importancia  de una ley propia por parte de las organizaciones sociales para poder defender y posicionar jurídicamente los derechos, necesidades y formas de gestión de los acueductos comunitarios. (Ver [Petroleras contaminan 2 litros de agua por segundo en Putumayo](http://bit.ly/1L6Lzhm))
