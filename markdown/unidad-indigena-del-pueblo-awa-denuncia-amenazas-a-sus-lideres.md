Title: Comunidad Indígena Awá denuncia amenazas a sus líderes
Date: 2018-06-19 10:13
Category: DDHH, Movilización
Tags: Awá, Derechos Humanos, indígenas, lideres sociales
Slug: unidad-indigena-del-pueblo-awa-denuncia-amenazas-a-sus-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/awá-e1507841393719.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [19 Jun 2018] 

A través de un comunicado, la comunidad Awá que habita en el resguardo Gran Rosario, ubicado en el municipio de Tumaco, denunció amenazas en contra de sus líderes y autoridades por parte del Frente Oliver Sinisterra (FOS) así como la ocupación de sus territorios.

El comunicado se produce luego de un enfrentamiento que tuvo lugar el pasado domingo 3 de junio, entre el Ejército Nacional y el FOS, cerca al centro educativo de la comunidad de Alto Palay. **Hecho que supuso un pronunciamiento por parte de la comunidad para que se respete su territorio y no se ponga en riesgo la vida de los jóvenes y los miembros de la comunidad.**

Posterior a este pronunciamiento, el día 4 de junio, los líderes del cabildo indígena recibieron amenazas y fueron declarados objetivo militar por parte del FOS. En consecuencia con esa declaración, el consejero y líder del resguardo Gran Rosario sufrió una agresión armada el día 15 de junio mientras se desplazaba por la vía que une a Pasto con Tumaco, resultando ilesos el consejero y todos sus acompañantes. Sin embargo, el vehículo en el que se movilizaban si resulto afectado.

**Según el comunicado, la violencia en contra del cabildo Gran Rosario no se ha detenido desde el asesinato de una familia entera en agosto del 2009**, a estos hechos se suman los diferentes atropellos que ha sufrido el pueblo Awá a lo largo del conflicto colombiano. (Le puede interesar: ["Los elementos de la crisis de asesinatos de los awá en Nariño"](https://archivo.contagioradio.com/indigenas-awa-denuncian-asesinatos-y-hostigamientos-de-estructuras-armadas-contra-su-comunidad/) )

**Por esta razón, los líderes y autoridades del pueblo Awá piden a todos los actores armados que hacen presencia en este lugar, que se respete su territorio, así como sus vidas.** De igual forma hicieron un llamado al Gobierno Nacional para que se garantice la integridad física de los miembros del cabildo e igualmente a la fiscalía para que se adelanten las investigaciones correspondientes.

[Comunicado Resguardo Gran Rosario](https://www.scribd.com/document/382101978/Comunicado-Resguardo-Gran-Rosario#from_embed "View Comunicado Resguardo Gran Rosario on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_35812" class="scribd_iframe_embed" title="Comunicado Resguardo Gran Rosario" src="https://www.scribd.com/embeds/382101978/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-9g4C9549zhYaRICsNDG8&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
