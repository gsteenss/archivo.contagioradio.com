Title: Movimiento Santuario listo para trabajar por los inmigrantes en EEUU
Date: 2017-01-20 16:51
Category: DDHH, El mundo
Tags: Donald Trump, Fascismo, inmigrantes, Movimiento Santuario
Slug: movimiento-santuario-listo-eeuu-34968
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/movimiento-santuario.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: New Sanctuary Movement of Philadelphia] 

###### [20 Ene. 2017] 

Luego de la elección de Donald Trump como nuevo presidente de Estados Unidos, los comportamientos fascistas, racistas y misóginos que analistas han asegurado ha sido mostrado en los discursos del nuevo mandatario han hecho que **latinos, mujeres, comunidad LGBTI y practicantes de diversas religiones aúnen sus esfuerzos para evitar daños y malos tratos en su mayoría a los inmigrantes.**

Según Francisco Herrera activista y gestor cultural **en San Francisco las actitudes violentas siguen siendo noticia todos los días** cuenta que “en días pasados una señora se quería estacionar y entra un señor y mete el carro al estacionamiento y se baja y le dice **‘ya ganó Trump yo ya no tengo que ser correcto políticamente, ya no tengo que se educado, así que te fregaste’**, cosas así muy ridículas estamos viendo en todo el país”.

Y cuenta Francisco que cerca de San Francisco, uno de los condados que ha dicho ser uno de los lugares con más protección “**a un muchacho afroamericano que tiene problemas mentales, un compañero de su escuela le toma una foto y la pone en Instagram y le pinta cadenas en el cuello y dice ‘ya vamos a regresar a la época de la esclavitud**. A nivel de la calle la gente ha tomado una actitud muy violenta”.

Ante estas situaciones y los miedos que han generado en la sociedad, el **Movimiento Santuario ha decidido seguir luchando por los inmigrantes y por ello se han unido cerca de 800 iglesias y congregaciones** para proteger en su mayoría a quienes se encuentran indocumentados en EEUU. Le puede interesar: [Amnístia Internacional invita a ser "Vigilantes de Trump"](https://archivo.contagioradio.com/amnistia-internacional-trump/)

Numerosas eventos que han ido desde vigilias, ruedas de prensa, plantones han sido realizados por el Movimiento Santuario, como muestra de rechazo ante los planes del nuevo presidente de Estados Unidos contra los “más desfavorecidos” como lo han asegurado ellos.

**¿Qué es el Movimiento Santuario?**

Este movimiento nace en 1982, un 24 de marzo, en el momento en que la iglesia Southside Presbyterian en Tucson le manifestó al gobierno de turno que violaría las leyes migratorias pues **habían decidido que su iglesia se convirtiera en un “santuario” que recibiría a los indocumentados provenientes de Centroamérica, que escapaban de los “escuadrones de la muerte”.**

El movimiento tuvo tan buena acogida que alcanzaron en poco tiempo a ser 500 congregaciones entre protestantes, católicas y judías de diferentes ciudades.

Aunque el movimiento no tiene cifras que den cuenta con exactitud del número total de personas que ayudaron, si han dicho que **un sólo abogado ha alcanzado a tener durante la historia del movimiento 3000 casos, que han representado en las Cortes.**

Pese a que el Movimiento Santuario no puede impedir la labor de los agentes de Inmigración lo que pueden es frenar las violaciones al derecho a la protección equitativa, la discriminación racial contra los inmigrantes entre otras acciones.

**Acciones en la “era Trump”**

Ante los crecientes ataques contra las mujeres y los posibles ataques a la población inmigrante, el Movimiento Santuario espera poder estar más alertas para hacer frente a las situaciones que se presenten, “**mantener un muro que no permita los planes de Trump y proteger a los grupos vulnerables”** dijo en una rueda de prensa el Movimiento.

Además de los inmigrantes, este Movimiento pretende **trabajar contra el discurso fascista que según las organizaciones se ha intensificado en Estados Unidos luego de la elección como presidente de Donald Trump.**

Las universidades y población en general, que están haciendo parte de este Movimiento, están dispuestos a abrir su ayuda a todas las personas o comunidades que se sientan agredidas, amenazadas o vulneradas por la nueva administración. Además, este movimiento realizará marchas en todo el país.

<iframe id="audio_16562226" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16562226_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="ssba ssba-wrap">

</div>
