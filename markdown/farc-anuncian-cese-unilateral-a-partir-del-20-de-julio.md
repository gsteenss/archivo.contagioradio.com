Title: FARC anuncian cese unilateral a partir del 20 de Julio
Date: 2015-07-08 10:04
Category: Nacional, Paz
Tags: cese unilateral de las FARC, Constituyentes por la Paz, Conversaciones de paz de la habana, FARC, Frente Amplio por la PAz, Juan Manuel Santos
Slug: farc-anuncian-cese-unilateral-a-partir-del-20-de-julio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/ivan-marquez-farc-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: semana.com 

###### [8 Jul 2015] 

El anuncio fue realizado por la delegación de paz de las FARC en Cuba, luego de la petición de países garantes del proceso, para iniciar acciones acciones de des escalamiento del conflicto y pactar un cese bilateral. Según la delegación de paz este cese unilateral se prolongará por un mes a partir del próximo 20 de Julio.

"Recogiendo el espíritu del llamado de los garantes del proceso, Cuba y Noruega y de los acompañantes del mismo, Venezuela y Chile, anunciamos nuestra disposición de ordenar un cese al fuego unilateral a partir del 20 de julio, por un mes" señala el comunicado en el que también afirman que esperan con ello generar condiciones favorables para un cese bilateral definitivo.

Además, el Estado Mayor Conjunto de esa guerrilla solicitó desde ya, la **veeduría del Frente Amplio por la Paz, las iglesias y el proceso de "Constituyentes por la paz"**. Este sería el sexto Cese Unilateral promulgado por las FARC desde que se inició el proceso de conversaciones de paz en Octubre de 2012.

Este es el comunicado completo.  
*"Saludamos y hacemos nuestro íntegramente el llamamiento realizado en el día de ayer en La Habana por los países garantes y acompañantes de los diálogos de paz. Nos congratulamos de la expresa solicitud a las partes pidiendo el desescalamiento urgente del conflicto armado, restringir al máximo las acciones de todo tipo que causan víctimas y sufrimiento, e intensificar la implementación de medidas de construcción de confianza, incluyendo la adopción de un acuerdo de cese al fuego y de hostilidades bilateral y definitivo, y otro sobre los derechos de las víctimas.*  
*Vinimos a Cuba a alcanzar un acuerdo de paz, a poner fin a una guerra que sobrepasa el medio siglo. Nada puede complacernos más que acabar definitivamente con la confrontación, la violencia, la generación de nuevas víctimas y el sufrimiento del pueblo colombiano a consecuencia del conflicto.*  
*Recogiendo el espíritu del llamado de los garantes del proceso, Cuba y Noruega y de los acompañantes del mismo, Venezuela y Chile, anunciamos nuestra disposición de ordenar un cese al fuego unilateral a partir del 20 de julio, por un mes. Buscamos con ello generar condiciones favorables para avanzar con la contraparte en la concreción del cese al fuego bilateral y definitivo.*  
*Solicitamos desde hoy los buenos oficios como veedores de esta determinación, al Frente Amplio por la Paz, las iglesias y al movimiento constituyente por la paz.*  
***SECRETARIADO DEL ESTADO MAYOR CENTRAL DE LAS FARC-EP"***  
 
