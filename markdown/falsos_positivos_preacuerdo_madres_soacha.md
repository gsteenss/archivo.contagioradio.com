Title: Reclutador de "falsos positivos" de Soacha se compromete con la verdad
Date: 2018-02-14 15:35
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, falsos positivos, madres de soacha
Slug: falsos_positivos_preacuerdo_madres_soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Madres-de-Soacha-e1505154647984.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:La Información ] 

###### [14 Feb 2018] 

Luego de 10 años de impunidad frente al caso de las 19 ejecuciones extrajudiciales cometidas en el municipio de Soacha por parte de la Brigada XV del Ejército, las madres de Soacha aceptaron realizar un preacuerdo con **Pedro Antonio Gámez Díaz, quien reclutó a cinco jóvenes para llevarlos a Ocaña, en Norte de Santander,** donde después fueron ejecutados por militares para presentarlos como guerrilleros muertos en combate.

Así lo han decidido los familiares de las víctimas, entre las que se encontraban dos menores de edad y una persona en condición discapacidad cognitiva. Ante la impunidad reinante en el caso, lo que buscan las familias es una dosis de verdad, especialmente para **conocer algunos de los nombres de los altos mandos del Ejército y civiles involucrados en estos crímenes.**

### **Solo esperan la verdad** 

Este martes Pedro Antonio Gámez, fue condenado a 39 años de cárcel, pero a cambio de que diga la verdad sobre esos hechos, espera una rebaja en su condena. **La audiencia se que se lleva a cabo por los delitos de homicidio agravado y desaparición forzada** en calidad de Coautor, se enmarca en un preacuerdo de la Fiscalía General de la Nación con el imputado se compromete a colaborar con la justicia contando la verdad de los hechos en los casos donde tuvo participación directa y otros de los cuales tenga conocimiento.

Jaime Estiven Valencia Sanabria de 16 años, Jhonatan Orlando Soto Bermúdez de 17 años, Daniel Alexander Martínez de 21 años, Diego Armando Marín Giraldo de 21 años, y Julio Cesar Mesa Vargas fueron las víctimas tanto de Gámez como del Ejército y todos aquellos agentes estatales y civiles que estuvieron detrás de los llamados "falsos positivos".

De acuerdo con la Asociación MINGA,  representante jurídica de algunos de estos casos de ejecuciones extrajudiciales, aunque las familias de las víctimas "consideran que es muy baja la pena acordada, ponen de presente que su lucha que ya completa 10 años, continua basada en la búsqueda de **la verdad y la justicia que solo encontrarán cuando se determine la responsabilidad de los altos mandos militares** y civiles involucrados en estas graves violaciones a los derechos humanos, que por la generalidad y sistematicidad de las mismas han sido consideradas por jueces nacionales y la comunidad internacional como delitos de lesa humanidad".

### **En cifras** 

Según informes del Centro de Investigación y Educación Popular (CINEP), un total de **5.265 ejecuciones extrajudiciales fueron cometidas en Colombia entre los años 2002 y 2010**. En 2015, el Centro Nacional de Memoria Histórica registraba un porcentaje de **impunidad del 95% en los casos de ejecuciones extrajudiciales en el pais**.

Además, un informe del 2015 de Human Right Watch,  evidenciaría que numerosos generales y coroneles tenían conocimiento de varios casos de ''falsos positivos'' y que en algunas ocasiones fueron ellos mismos quienes ordenaron o en su defecto, hicieron posible que se cometieran los asesinatos.

###### Reciba toda la información de Contagio Radio en [[su correo]
