Title: ¿Que viene después?
Date: 2020-04-06 16:20
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: editorial, Opinión, pensamiento crítico
Slug: que-viene-despues
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/sombras-y-barbijos-en-el___20a39C_0_930x525__1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#7a898f"} -->

###### Foto de: Lavaca.org 

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### Aliento también es pronosticar.....

<!-- /wp:heading -->

<!-- wp:paragraph -->

Si el supuesto editorial de **The Washington Post** fue emitido o no por el diario, es lo que menos importa. Nada de mentira hay en esa reflexión que viajó viralmente hace pocos días y que incluso el canal Caracol la retransmitió, una cosa es transmitir mentiras, y otra transmitir una verdad de la cual no tenemos certeza de su fuente

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿acaso las palabras que formula esa supuesta editorial tienen algo de mentira?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aquí en nuestra América, en Asia y en África e incluso en la misma Europa los comunistas, los socialistas, los anarquistas no han hecho sino la praxis permanente de una lucha contra todo lo planteado en esa supuesta editorial. Además, la batalla contra un orden mundial que ha mostrado su cara más nefasta es algo que a estas alturas lo diga un diario o lo diga el otro, no se puede ocultar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por supuesto el reproche de nada sirve ahora y es preferible aprovechar que se levanta una crítica contra el capitalismo a terminar oliendo a estatua. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

#####   
¿Qué viene después?

<!-- /wp:heading -->

<!-- wp:paragraph -->

La reflexión será permanente en aislamiento, los cantos contra el poder someterán a nuestras horas de silencio y nos cargarán de múltiples virtudes para afrontar la batalla que nos queda.  
La muerte es la muerte, el tiempo es el sortilegio que bebemos hasta su casa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si salimos vivos de esta tragedia mundial, saldremos con la conclusión sentida de que será un honor morir luchando contra este orden mundial. Será un honor morir luchando contra la ambición y la injusticia que carcome a Colombia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Si la muerte está de moda, entonces que el fatal destino nos aplace la cita para que de todas las formas posibles sigamos luchando. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sin romanticismo alguno, podemos sospechar que al final seremos sólo un cuerpo abandonado en su solitaria tumba, pero esta no es la única sospecha ... Porque las ideas serán nuestro fantasma, con la certeza de que cada palabra viva irá más allá de nuestra muerte. Esos fantasmas vivirán atormentando a los poderosos y se convertirán en ideas y se convertirán en actos y se convertirán en revoluciones que no darán más espera y que tendrán el alimento para avanzar luchando y luchando durante los años que sean necesarios. Al final de la calle hay un camino de herradura y al final del camino, volteando hacia la izquierda se halla un mirador desde el que ya se observa a las nuevas generaciones danzar las músicas de una nueva Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Caerá el uribismo, caerán los terratenientes, caerán los narcotraficantes, caerán las inmobiliarias, caerá el régimen de los banqueros, caerá el régimen de la información...

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 ¡¡y danzaremos, sí que danzaremos vivos y muertos, envueltos en el llanto alegre de nuestra Victoria!!

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Mas columnas de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)  

<!-- /wp:paragraph -->
