Title: Paula Osorio, firmante de paz es asesinada en Chocó
Date: 2020-11-26 14:53
Author: AdminContagio
Category: Actualidad, Nacional
Tags: asesinato de excombatientes, Chocó, Firmante de paz
Slug: paula-osorio-firmante-de-paz-es-asesinada-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Excombatientes-e1567187945860.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A 4 años de la firma del acuerdo de paz entre las FARC-EP y el gobierno Santos y en la conmemoración del el Día de la Eliminación de la Violencia contra la Mujer, El Partido FARC denunció el asesinato de la firmante de paz, Paula Andrea Osorio García en Atrato, departamento del Chocó.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1331554179219922944","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1331554179219922944

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De acuerdo con la información suministrada, el crimen contra la excombatiente de 25 años se habría producido en horas de la noche del martes 24 de noviembre en Yuto, municipio del Atrato. Paula Andrea Osorio García, conocida también como Marcela García, **fue encontrada sin vida en el sitio conocido como el 9, cerca de un proyecto productivo de piscicultura del kilómetro 8.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Osorio pertenecía al frente 24 de la guerrilla y abandonó las armas en Vidrí, Vigía del Fuente, departamento de Antioquia, tras la firma del acuerdo de paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la fecha ya son 243 los y las excombatientes asesinados desde la firma del acuerdo. (Le puede interesar: [Tres firmantes de paz asesinados en Chocó, Caquetá y Putumayo](https://archivo.contagioradio.com/tres-firmantes-de-paz-asesinados-en-choco-caqueta-y-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/SandinoVictoria/status/1331457297667788807","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/SandinoVictoria/status/1331457297667788807

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Olmedo García, representante a la Cámara por FARC en Antioquia, señala que es bastante lamentable que después de firmar el acuerdo, esperando garantizar la vida de quienes han dejado las armas, estos sigan siendo asesinados. **«Esto indica que en este país es muy difícil la conquista de la paz, porque nosotros hicimos dejación de armas para garantizar la vida de todos y todas y lo que hemos venido presenciando es más de 243 compañeros asesinados»**. (Le puede interesar: [Muertes de los detenidos en «La Modelo» a manos del INPEC fueron intencionales: HRW](https://archivo.contagioradio.com/muertes-de-los-detenidos-en-la-modelo-a-manos-del-inpec-fueron-intencionales-hrw/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, expresa que el compromiso hecho por el presidente Duque es solamente para la comunidad internacional y para generar la sensación de que está comprometido. «Sin embargo no es cierto, porque va en contra de los mismos acuerdos. Por ejemplo no ha cumplido y se ha venido oponiendo con el programa de sustitución de cultivos», programar que además de buscar cambiar la realidad de los territorios, también tiene como objetivo «quitarle el combustible a los diferentes actores armados».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, agrega que las formas para garantizar la vida de los y las excombatientes es **desmontando el paramilitarismo, avanzar en una paz completa con el ELN, desmontar las causas que generan la violencia y desmontar las mafias que están incrustadas en el estado y en las fuerzas militares.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
