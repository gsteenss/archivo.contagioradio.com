Title: Así va la equidad de Género en las Empresas colombianas
Date: 2016-10-25 14:10
Category: Mujer, Otra Mirada
Tags: colombia, empresas, Equidad de género, Ranking
Slug: el-ranking-de-equidad-de-genero-en-las-empresas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/14872489_10157654286275430_147817012_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Aequales 

##### 24 Oct 2016 

La organización **Aequales**, presentó el pasado miércoles los resultados del segundo “**Ranking de Equidad de Género en las Organizaciones en Colombia**” con el cual se busca generar conciencia respecto a la inequidad femenina en el ámbito laboral. [Le puede interesar: Nairo Quintana pedalea por la equidad de Género](https://archivo.contagioradio.com/nairo-quintana-pedalea-por-la-equidad-de-genero/).

La medición se realizó con base en la información recolectada por medio de encuestas virtuales a los encargados del área de recursos humanos de 104 organizaciones, de las cuales 30,8% son públicas y 69,2% privadas. **Se evaluaron cuatro áreas fundamentales para la equidad: gestión de objetivos, cultura organizacional, estructura organizacional y gestión de talento**.

### **Indicadores para medir equidad de género.** 

Entre los principales hallazgos de esta segunda edición, se destacan los siguientes datos:

- El 46,3% de los trabajadores de las organizaciones encuestadas son mujeres, pero sólo el 34% ocupan cargos de alta dirección.

- Únicamente el 29,8% de las organizaciones cuentan con políticas claras sobre la equidad de género y/o diversidad.

- Tan sólo 12,5% cuenta con un manual de comunicaciones para evitar sesgos de género y el 28,8% de las organizaciones dice incluir a hombres y mujeres en sus comunicaciones escritas e imágenes

- El 58,7% dice contar con un protocolo de prevención y acompañamiento ante casos de acoso sexual o laboral, es decir, poco más de la mitad.

Aunque no todos los resultados fueron negativos, pues se identificó que **la brecha salarial entre hombres y mujeres es del 11%** mejorando un punto porcentual respecto de la medición de 2015 que arrojó una brecha del 12%; una constante entre el ranking en sus dos ediciones es que en la medida en que aumenta la posición de liderazgo, disminuye o desaparece la presencia femenina.

El documento fue elaborado en conjunto con el Colegio de Estudios Superiores en Administración CESA, las Secretarías Distritales de Desarrollo Económico y de la Mujer, pese a que esta última ha sufrido duras críticas y un gran recorte presupuestal, afectando gravemente a muchas víctimas de violencia de género, como lo denunciaba el congreso de los pueblos.

El ranking de compañías emitido por la organización Aequales quedó así:

Sector Privado.

1\. Johnson y Johnson.  
2. Citibank.  
3. Fundación Plan.  
4. Telefónica Movistar.  
5. Codensa.  
6. Old Mutual.  
7. Sodexo.  
8. 3M.  
9. Bavaria.  
10. Dow Química Colombia.

Sector Público.  
1. Instituto Distrital para la Protección de la Niñez y la Juventud, IDIPRON.  
2. Catastro Distrital.  
3. Secretaría Distrital de Movilidad.
