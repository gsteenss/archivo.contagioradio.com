Title: Con plantón movimientos insisten en que "ser líder social no es delito"
Date: 2019-01-21 09:34
Author: AdminContagio
Category: DDHH, Líderes sociales, Movilización
Tags: Carlos Alberto Pedraza, Criminalización, Julian Gil, lideres sociales
Slug: planton-ser-lider-social-no-es-un-delito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/FotoAsociaciónMinga-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Asociación Minga 

###### 21 Ene 2019 

Hoy lunes 21 de enero se realizará desde las 9 de la mañana el **Plantón de Solidaridad “Ser líder social no es un delito”**, evento que busca sumar **voces contra la persecución sistemática y criminalización hacia el movimiento social** colombiano y los líderes como portadores de sus banderas.

Históricamente en Colombia se ha evidenciado una grave violación a los derechos humanos. **Desde 2016 diferentes organizaciones han denunciado asesinatos, señalamientos, desapariciones forzadas, atentados entre otros crímenes** a los líderes sociales; y aún no se ha evidenciado una voluntad política por parte del gobierno para neutralizar la situación.

El plantón se realizará en nombre de **Julián Gil**, integrante de la Comisión Internacional y Secretario técnico del Congreso de los pueblos, quien fue judicializado y criminalizado el 6 de junio del 2018, y en conmemoración de **Carlos Alberto Pedraza**, defensor de derechos humanos, integrante del Movimiento Nacional de Víctimas de Crímenes de Estado – MOVICE– y del Congreso de los Pueblos, quien fue desaparecido, torturado y asesinado hace cuatro años.

El plantón se llevará a cabo en la **calle 31 No. 6-20 en los juzgados especializados de Bogotá**, donde, el Congreso de los Pueblos invita a solidarizarse con los líderes y lideresas sociales que luchan por la paz y construyen un país diferente cada día, por ello, afirman que ser líder social no puede costar la vida, ni la libertad, porque ser líder social no es un delito.
