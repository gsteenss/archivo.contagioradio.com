Title: Condena a general (r) Árias Cabrales confirma que sí hubo desapariciones en el Palacio de Justicia
Date: 2019-09-25 16:30
Author: CtgAdm
Category: DDHH, Judicial
Tags: Desapariciones forzadas, Familiares de desaparecidos del Palacio de Justicia
Slug: condena-a-general-r-arias-cabrales-confirma-que-si-hubo-desapariciones-en-el-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/palacio-de-justicia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Ante el fallo de la Corte Suprema de Justicia, que confirma la condena de 35 años de cárcel contra el general (r) Jesús Armando Árias Cabrales, comandante de la Brigada XIII responsable de liderar la retoma del Palacio de Justicia entre el 6 y 7 de noviembre de 1985, los familiares de los desaparecidos celebran esta decisión, que además de condenar a un general de alto rango, confirma que sí existieron hechos de desaparición forzada cometidos por el Ejército.

René Guarín, familiar de María del Pilar Guarín, desaparecida durante la toma y retoma del Palacio de Justicia se refirió a la decisión, resaltando sobre la sentencia que, aunque esta señala la conducta que asumió el general Árias Cabrales durante la retoma en el marco el conflicto, las desapariciones del Palacio esconden un fin político y obedecen al delito de desaparición forzada, clasificado como crimen de lesa humanidad.[(Le puede interesar: ¡Sí existen desaparecidos! familiares de las víctimas del Palacio de Justicia)](https://archivo.contagioradio.com/existen-desaparecidos-l-palacio-de-justicia/)

Es por tal motivo que el fallo de la Corte reafirma a los familiares en su decisión de no presentarse ante la JEP como víctimas en un eventual sometimiento del general Gral Iván Ramirez Quintero, quien busca acogerse a este sistema de justicia especial, y quien durante los hechos del Palacio fue comandante del Comando Operativo de Inteligencia y Contrainteligencia. **"No podemos acreditarnos como víctimas porque son crímenes de Estado que están fuera del alcance de la JEP"**, advierte Rene Guarín.

### Sucesos del Palacio de Justicia han fracturado la confianza en el Estado 

Rene señala que solo en una ocasión, familiares y Árias Cabrales coincidieron y fue en 2008 en las instalaciones de la Fiscalía, cuando este último fue llamado a declarar por los hechos del palacio, sin embargo en aquella ocasión, el general en retiro afirmó que únicamente respondía ante la justicia penal y no ante la justicia ordinaria, desde entonces no ha existido ningún tipo de acercamiento o acto de perdón.

Asimismo, Guarín afirma que tras treinta años de búsqued la verdad continúa sin aparecer, "la hemos arañado consiguiendo versiones aquí y fuera del país para saber qué pasó durante esos días con nuestros familiares", no obstante, hechos como las torturas inflingidas sobre personas aquel día no han sido aclaradas, al igual que las ejecuciones extrajudiciales como es el caso del magistrado Carlos Horacio Urán.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no">[﻿]</iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
