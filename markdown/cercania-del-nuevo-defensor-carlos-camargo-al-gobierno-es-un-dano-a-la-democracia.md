Title: Carlos Camargo será la voz del gobierno en la Defensoría del Pueblo
Date: 2020-08-18 17:35
Author: CtgAdm
Category: Actualidad, Política
Tags: Carlos Camargo, Defensoría del Pueblo, Gobierno Duque
Slug: cercania-del-nuevo-defensor-carlos-camargo-al-gobierno-es-un-dano-a-la-democracia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Carlos-Camargo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: FND/ Carlos Camargo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con una elección de 140 votos a favor en la Cámara de Representantes, **Carlos Camargo Assis** fue nombrado como nuevo Defensor del Pueblo, cargo que ocupará durante los próximos cuatro años. Analistas y movimientos sociales advierten que el funcionario no es apto para ocupar este cargo, primero por su vinculación a problemas en el manejo de recursos en sus cargos anteriores, y segundo, por no contar con la experiencia para ejercer un rol en la defensa de los Derechos Humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**León Valencia, director de la [Fundación Paz y Reconciliación](https://twitter.com/parescolombia)** fue uno de los primeros en advertir, en agosto de 2019 cuando Camargo participaba por la dirección de la Registraduría Nacional, que este había entregado contratos con personas cercanas a los altos tribunales que escogían, en aquel entonces, al próximo aspirante a ese puesto.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Su paso por la **Federación Nacional de Departamentos** también ha sido cuestionado por su manejo de recursos al interior de la institución. Entre los dudosos hechos se incluyen contratos con Liliana del Rosario Araújo, hermana de la magistrada del Consejo de Estado, Mercedes Rocío Araújo, **por el valor de 70 millones de pesos** para asesorar la oficina de Control Interno, y el de la exmagistrada del Consejo de Estado Martha Teresa Briceño de Valencia, contratada en 2018 como asesora de la dirección ejecutiva y la secretaría general **por 15 millones de pesos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que Camargo llega a un cargo donde debe velar por la protección, defensa, promoción, divulgación y ejercicio de los derechos humanos, y donde además, cuenta **con cerca de 600.000 millones de pesos de presupuesto del que dependen más de 4.000 defensores públicos en todo el país.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Carlos Camargo cercano al gobierno Duque

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Valencia advierte que Camargo pertenece a las entrañas del Gobierno, situación que anula su independencia y hace daño a la democracía, "Duque se está apoderando de todos los organismos de control, se apoderó de la Fiscalía, la Contraloría, la Defensoría y ahora va por la Procuraduría". [(Lea también: Procuraduría puede cambiar la balanza política y frenar la posibilidad de cambio)](https://archivo.contagioradio.com/procuraduria-puede-cambiar-la-balanza-politica-y-frenar-la-posibilidad-de-cambio/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El analista señala que los partidos políticos que no habían pactado una coalición alrededor del ejecutivo, ahora están uniéndose al Gobierno por la vía de manejar los organismos de control, "lo único que queda por fuera del gobierno de Duque son las cortes, la Corte Suprema de Justicia y la Corte Constitucional" asegura. [(Le puede interesar: Defensoría del Pueblo será repartida como cuota política del gobierno Duque)](https://archivo.contagioradio.com/defensoria-del-pueblo-cuota-politica/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta cercanía de Camargo viene de años atrá,s cuando fue compañero de Iván Duque en la Universidad Sergio Arboleda. A su vez, es yerno de la exsenadora Nora García Burgos y primo del senador conservador David Barguil, quien enfrenta un proceso de pérdida de investidura ante el Consejo de Estado por sus inasistencias a las plenarias del Congreso.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El nuevo defensor del pueblo además fungió como magistrado del Consejo Nacional Electoral donde engavetó la investigación pese a todas las pruebas presentadas en en contra de Óscar Iván Zuluaga por la presunta entrada de dineros de Odebrecht a su campaña presidencial.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un trabajo que requiere conocer el país

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Valencia destaca que aunque su antecesor, Carlos Negret no se caracterizó por ser **"una persona especialmente crítica u opositora",** estableció un acercamiento con las comunidades, lo que requiere de una persona que recorra las carreteras y ríos de Colombia. Escenario que parece difícil ante la inexperiencia de la que carece Camargo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante dichas críticas, Camargo asegura que su trabajo se expresará en acciones, por lo que afirma que buscara avances más eficaces a través de la interlocución permanente con el Gobierno, los gobiernos regionales y locales. Pese a ello, sectores sociales temen que Camargo no ha referenciado ningún tipo de acercamiento con sectores del Movimiento Social, determinantes en el trabajo de la entidad que tiene grandes retos por la crisis de DD.HH que afronta el país.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
