Title: Familia de líder guerrillero de los años 50 ofrece sus tierras a la construcción de paz
Date: 2017-03-22 15:42
Category: Entrevistas, Paz
Tags: familia Aljure, FARC, mapiripan, Palmera, Poligrow
Slug: familia-aljure-dona-tierras-para-proyectos-productivos-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/mapiripan-dumar-aljure.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 Mar. 2017] 

La familia Aljure poseedora de la Hacienda La Esmeralda en el municipio de Mapiripán departamento del Meta, ha ofrecido un porcentaje de su tierra para que **integrantes de las FARC y víctimas puedan realizar proyectos productivos** y de esa manera aportar a la construcción de paz en Colombia y al retorno a la vida civil de los integrantes de esa guerrilla.

“Nosotros somos nietos del legendario guerrillero liberal Dumar Aljure, quien en los años 50 se acogió a una amnistía de la cual recibió un predio llamado La Esmeralda, y nosotros como herederos de ella, **tuvimos el sentir de ceder una parte de la finca para hacer proyectos productivos con las personas que están en diálogo de paz y con víctimas** del conflicto armado” aseguró William Aljure, uno de los propietarios de la hacienda.

**La hacienda cuenta con más de 120 mil hectáreas** y aunque hasta el momento no se sabe el número de hectáreas que serán cedidas, la intención de hacerlo ya fue manifestado a varias organizaciones internacionales que se reunieron con integrantes de la familia Aljure.

“Tenemos que **hablar con el gobierno nacional y con la guerrilla de las FARC para manifestarles esta intención** y que nos informen de los proyectos productivos y cómo nos organizaríamos allí, porque nosotros como familia estaremos trabajando en la tierra de manera colectiva con otras personas” dijo Aljure. Le puede interesar: [Palmera Poligrow consume 1'700.000 litros de agua diarios en cada plantación en Mapiripán](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/)

William cuenta que él y su familia han sido víctimas del conflicto armado “además del asesinato de mi abuelo a manos del Ejército hemos perdido a 10 familiares, los últimos que tuvimos que recoger fue a mi papá y a mi mamá, **hemos vivido la guerra, también la entendemos y porque queremos ser coherentes** con todo lo que nos ha pasado, por eso **queremos aportar a la paz con lo que está a nuestro alcance”.**

Esta estrategia, además está encaminada a poder cumplir el sueño de su abuelo que era poder repartirla a sus excombatientes y también a campesinos de aquella época que no tenían tierras “esos sueños se opacan cuando lo matan un jueves 4 de abril de 1968, que está por cumplir 49 año de haberlo asesinado el Ejército. **Y nosotros al cumplir estos 49 años y en el marco de estos 50 años de guerra es dejar de hablar tanto e ir a hacerlo.** Estamos dispuestos a recibir a los integrantes de las FARC y a las víctimas”. Le puede interesar: [Piden a la UE frenar las violaciones a DDHH por parte de empresas palmeras](https://archivo.contagioradio.com/indonesia-peru-colombia-y-liberia-pidieron-a-la-ue-frenar-la-violencia-por-parte-de-empresas-palmeras/)

La idea de poder estar en estos territorios, es trabajar mancomunadamente para impedir que **la presencia empresarial que hay en el municipio viole los DDHH** de las comunidades que se encuentran en la región.

Tal como lo han documentado ambientalistas y organizaciones de DDHH, las comunidades se siguen oponiendo a **la presencia de la multinacional Poligrow, que pretendería construir una zona franca en esa área** y adicionalmente montaría una planta extractora, pretendiendo que la zona pase a ser de uso industrial. Situación que modificaría de esta manera el esquema de ordenamiento territorial del municipio, cuyo uso actual es turístico y forestal. Le puede interesar: [Cerca de 19 empresas son las que han acaparado más tierras en Colombia](https://archivo.contagioradio.com/cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia/)

El próximo 4 de abril, las comunidades indígenas Jiw y Sikuani y campesinos de la región realizarán una caminata por la zona para revisar la **situación actual de los territorios y visitar la laguna sagrada de las toninas delfín rosado** del Amazonas y la Nutria gigante, amabas en peligro de extinción. Le puede interesar: [Ordenan suspender operaciones de Poligrow Colombia por daños ambientales](https://archivo.contagioradio.com/suspenden-operaciones-de-poligrow-colombia-por-infracciones-ambiantales/)

<iframe id="audio_17714110" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17714110_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
