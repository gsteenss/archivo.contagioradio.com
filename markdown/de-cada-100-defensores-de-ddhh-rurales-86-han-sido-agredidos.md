Title: De cada 100 defensores de DDHH rurales 86 han sido agredidos
Date: 2015-12-09 15:02
Category: Entrevistas, Nacional
Tags: Activistas, campesinos, conflicto armado, defensores de derechos humanos, dialogos de paz, diego martinez, FARC, Fuerza Pública, Gobierno Nacional, Grupos Paramilitares Sucesores, John Henry González, Líderes campesinos, Paramilitarismo, proceso de paz
Slug: de-cada-100-defensores-de-ddhh-rurales-86-han-sido-agredidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/defensores_rurales_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [protectionline.org]

###### [9 Dic 2015] 

En el marco del lanzamiento del Diseño Participativo del Protocolo de Protección para Defensores de Derechos Humanos en Contextos Rurales, se denunció que los hostigamientos a los activistas son recurrentes, en su mayoría por parte de Grupos Paramilitares, la Fuerza Pública y en menor medida por grupos guerrilleros.

En el documento, elaborado por el Comité Permanente por la Defensa de los Derechos Humanos (CPDH), con la colaboración de organizaciones sociales, activistas y órganos oficiales, se establece que en Colombia, las comunidades y líderes campesinos continúan siendo víctimas de persecuciones y estigmatizaciones.

De acuerdo al informe, el 93% de los hombres y el 82% de las mujeres defensores de DDHH que fueron encuestados, señalaron haber sido víctimas de algún tipo de violación a su integridad. Frente a esta problemática, el vocero del Congreso de Los Pueblos John Henry González expone que “en un contexto de conflicto armado como el que vivimos, datos como estos son escandalosos, pero corresponden a la realidad del país”.

También, en el documento se afirma que es preocupante el hecho de que el 63% de las detenciones a los defensores son arbitrarias. Además, se añade que 8 de cada 10 activistas se siente estigmatizado por su labor y 9 de cada 10, perciben que su organización o comunidad sufre de señalamientos injustificados por realizar su trabajo.

De acuerdo al secretario ejecutivo de la CPDH e integrante de la subcomisión jurídica de en la Mesa de Negociaciones de La Habana Diego Martínez, el diseño de esta propuesta tiene 3 objetivos fundamentales: el primero de ellos es el de hacer una labor de denuncia y concienciación a la sociedad sobre el peligro y la falta de garantías a las que se exponen los activistas y las comunidades; el segundo es que el Estado colombiano adopte nuevas políticas que permitan a los defensores continuar con su labor; el tercero consiste en hacer un esfuerzo por vincular propuestas de las comunidades los líderes campesinos a los diálogos de paz.

Respecto al tercer punto, González como vocero del Congreso de Los Pueblos, dice que “en el imaginario que tenemos todos acerca de los diálogos de paz, es que en Colombia se puedan plantear diferentes propuestas, incluso diferentes a las del establecimiento”. Y aunque celebra que se esté desarrollando un proceso de paz, reitera que debe establecerse “un diálogo entre las organizaciones y el Estado, no solo entre el Estado y la guerrilla”. Además, cuestiona que los diálogos aún no está cambiando una realidad ni se han integrado propuestas que los vinculen: “lo que las comunidades campesinas esperamos no son soluciones que nos reemplacen, sino que nos empoderen”, concluye.

Según el reporte de “Los Nadies” de la organización Somos Defensores, en el 2015, se superó el promedio de homicidios de Defensores registrado en los últimos 20 años, con 51 personas asesinadas por la labor de la defensa de los Derechos Humanos: “entre enero y junio de 2015, fueron registradas 399 agresiones individuales contra defensores(as), discriminadas en 332 amenazas (incremento de 216%), 34 asesinatos (incremento del 15%), 25 atentados, 4 detenciones arbitrarias, 3 casos de uso arbitrario del sistema penal y 1 de robo de información”.
