Title: Habitantes de la zona nororiental en Medellín piden atención especial por Covid-19
Date: 2020-04-01 11:54
Author: AdminContagio
Category: yoreporto
Tags: Alertar temprana, Comunas de Medellín, Covid-19, Medellin, zona nororiental medellín
Slug: en-medellin-habitantes-de-la-zona-nororiental-piden-atencion-especial-por-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Portada.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pronunciamiento_Alerta_ZonaNororientalMDE.pdf" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/IMG_2741-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

*En la zona nororiental de Medellín habita gran parte de la población más vulnerable de esta ciudad. Por esa razón, mediante una alerta temprana piden a la Alcaldía de Medellín unas medidas de atención especial. *  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"right"} -->

***Por Corporación Con-Vivamos***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**123 organizaciones sociales, procesos comunitarios, colectivos y actores académicos de las comunas 1, 2, 3 y 4 de Medellín** suscribieron un Pronunciamiento Público de Alerta Temprana, en el que le pidieron a la Alcaldía que implemente medidas de atención especial para esta población, ante la situación generada por el Covid-19. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Adolfo Taborda Molina**, presidente de la Junta de Acción Comunal del barrio Santo Domingo Savio (comuna 1), explica que:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“la Zona Nororiental ha sido ocupada y ha crecido demográficamente debido al desplazamiento y la violencia en otras zonas del país. Desde el río hasta los límites con Santa Elena hay una gran extensión poblacional, y sabemos que el 60 o 70% de nuestras comunidades viven del día a día, hoy no están trabajando, no se pueden movilizar”. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Y es que según el informe *Medellín cómo vamos* (2020) “El porcentaje de hogares en inseguridad alimentaria severa muestra a las comunas de Popular, Manrique y el corregimiento de Santa Elena en desventaja relativa frente al resto de territorios”

<!-- /wp:heading -->

<!-- wp:paragraph -->

Además, barrios como Popular, Santa Cruz, Manrique, Doce de Octubre, Villa Hermosa y San Javier son los que menores condiciones de vida presentan de acuerdo al Índice Multidimensional de Condiciones de Vida (IMCV). Y en la Comuna 4 – Aranjuez, en barrios y sectores como Moravia, El Oasis, San Pedro y Lovaina, se presentan amplias brechas socioeconómicas y condiciones de desigualdad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es por esta razón que ante las medidas implementadas por el Gobierno nacional para frenar la expansión del Covid-19, no son pocas las necesidades y problemáticas que se han empezado a evidenciar en estos barrios.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como lo manifiesta **Wendy Vera**, integrante de la Biblioteca Sueños de Papel que trabaja en esta zona, “en estas comunas históricamente ha habido problemas educativos, sociales, económicos, que hacen que la problemática del coronavirus sea mucho más difícil de afrontar. Es importante resaltar que acá hay viviendas de estrato 0, donde viven personas que son desplazadas, y de estratos 1, 2, con personas de bajos recursos”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según expresa **Fernando Zapata**, integrante de la Corporación Con-Vivamos: “la alerta temprana que radicamos lo que busca es salvar la mayor cantidad de vidas posible, y por eso lo que se le solicita al Estado es un pliego en cuatro grandes bloques: uno, sobre la situación socioeconómica de las personas que están en mayor grado de vulnerabilidad y empobrecimiento, para que puedan guardar la cuarentena; dos, para que se atienda y prevenga el contagio, y se prevean acciones para atender a quienes resulten enfermos, y así empezar a mitigar el impacto que se genera sobre el sistema de salud, como ya hemos visto en todo el mundo; el tercer bloque incluye propuestas de cómo financiar estas peticiones; y finalmente, un cuarto bloque que tiene que ver con medidas que se deben aplicar en la ciudad”.

<!-- /wp:paragraph -->

<!-- wp:image {"id":82706,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Portada-791x1024.jpg){.wp-image-82706}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Las propuestas abordan temas como la atención diferenciada a pobladores que viven de la economía informal; bancos de alimentos por comunas y redes de abastecimiento local; acceso a servicios públicos y suspensión a cobro en las tarifas; apoyos económicos a microempresarios barriales; líneas de apoyo psicosocial; la activación de comités de gestión de riesgo municipal; la creación de mecanismos para una difusión de información adecuada en los barrios; y el desarrollo de propuestas de cultura viva comunitaria.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así mismo, en dicho pronunciamiento las organizaciones mencionan la necesidad de adecuar y aumentar la capacidad en centros de salud barriales; activar redes territoriales de prestación de servicios de salud para atender enfermedades crónicas y urgencias; educar a líderes y grupos comunitarios para la promoción de la salud y prevención de la enfermedad; generar procesos de comunicación para la salud; dotar de implementos necesarios a personas expuestas a la atención al público o que realicen actividades de prevención en los territorios, entre otros. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Yo le haría un llamado a la alcaldía de Medellín para que se enfoque en las zonas periféricas que necesitamos mucha atención, y también en personas que no saben nada de esto porque no tienen acceso a ningún medio, o no tienen cómo movilizarse para enterarse de estas ayudas”, manifiesta **Yesica Córdoba**. Por su parte, **Adolfo Taborda** dice que “los líderes siempre le hemos servido a la administración; ahora estamos desprotegidos, nadie estaba preparado para esto, y no tenemos capacidad instalada. Necesitamos que el Estado mire para acá”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, Fernando Zapata expresa que “Nosotros llamamos a generar mucha información, a comunicarle mucho a las comunidades para que puedan saber cómo están las cosas. Con esto que estamos haciendo como organizaciones de la Zona Nororiental estamos aprendiendo, y por eso queremos reiterar la invitación a otras comunidades en otros lugares de la ciudad para que también se organicen y busquen soluciones colectivas que requiere este asunto”.

<!-- /wp:paragraph -->

<!-- wp:file {"id":82707,"href":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pronunciamiento_Alerta_ZonaNororientalMDE.pdf"} -->

<div class="wp-block-file">

[Pronunciamiento\_Alerta\_ZonaNororientalMDE](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pronunciamiento_Alerta_ZonaNororientalMDE.pdf)[Descarga](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Pronunciamiento_Alerta_ZonaNororientalMDE.pdf){.wp-block-file__button}

</div>

<!-- /wp:file -->

<!-- wp:paragraph -->

  
[Mas de Yo Reporto](https://archivo.contagioradio.com/?s=Yo+reporto)

<!-- /wp:paragraph -->
