Title: Mujeres Indígenas del mundo siguen luchando por sus derechos
Date: 2017-09-13 16:58
Category: Libertades Sonoras, Mujer
Tags: Día internacional de la mujer indígena, indígenas, Libertades Sonoras, mujeres, Mujeres indígenas
Slug: mujeres-indigenas-luchan-por-sus-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/5-de-septiembre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Se lo explico con plastilina] 

###### [13 Sept. 2017]

Desde el **5 de septiembre de 1983 se conmemora el Día Internacional de la Mujer Indígena**, para rememorar a Bartolina Sisa, indígena peruana, guerrera Aymara y luchadora en contra de la dominación de conquistadores españoles.

Bartolina asumió el liderazgo en los batallones indígenas desde donde mostró gran capacidad de organización, armó todo un batallón de guerrilleros y de mujeres que colaboraron con la resistencia y se opusieron a las violaciones de derechos humanos de los colonizadores en el alto Perú.

**Esta lideresa fue asesinada un 5 de septiembre de 1782** pero su lucha siguió por muchos años. Le puede interesar: [La ONU advierte que la situación de los pueblos indígenas en el mundo es peor que hace 10 años](https://archivo.contagioradio.com/el-mundo-sigue-rezagado-con-respecto-a-los-derechos-de-los-indigenas-10-anos-despues-de-su-declaracion-historica-advierten-expertos-de-la-onu/)

Por ello, en \#LibertadesSonoras quisimos hacer memoria de ella, pero también **rendir un homenaje a las mujeres de estos pueblos del mundo**, que luchan por sus derechos al territorio, a la vida, a la participación política, entre muchos otros. Le puede interesar: [Mujeres indígenas del Resguardo Provincial completan un mes protestando contra Cerrejón](https://archivo.contagioradio.com/45359/)

Además contamos con la presencia de [**Melania Canales Vicepresidenta de la Organización Nacional de Mujeres Indígenas y Amazónicas del Perú** y Vicepresidenta de la Federación Regional de Mujeres Indígenas de Ayacucho.]

<article id="post-46606" class="post__holder post-46606 post type-post status-publish format-standard has-post-thumbnail hentry category-actualidad category-ddhh tag-benjamin-netanyahu tag-boicot tag-colombia tag-israel tag-juan-manuel-santos tag-palestina tag-tlc tag-tratado-de-libre-comercio cat-73-id cat-13-id">
<div class="post_content">

<article id="post-45288" class="post__holder post-45288 post type-post status-publish format-standard has-post-thumbnail hentry category-onda-palestina tag-bds tag-benjamin-netanyahu tag-israel tag-palestina cat-9327-id">
<div class="post_content">

<iframe id="audio_20865363" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20865363_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### 

</div>

</article>

</div>

</article>

