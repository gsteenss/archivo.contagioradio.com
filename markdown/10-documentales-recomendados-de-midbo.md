Title: 7 documentales para ver en MIDBO
Date: 2017-10-31 16:45
Author: AdminContagio
Category: 24 Cuadros
Tags: Bogotá, Documental, MIDBO
Slug: 10-documentales-recomendados-de-midbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/522508679_1280x720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Warmipura Documental 

###### 31 Oct 2017 

La 19 edición de la Muestra Internacional Documental de Bogotá llega de nuevo a la capital de país. El festival estará presentando lo mejor del cine documental desde el 23 de octubre al 10 de noviembre, donde el público podrá disfrutar de más de 90 documentales nacionales e internacionales.

La Muestra Internacional Documental de Bogotá trae para esta edición variadas categorías: Exhibición, competencia Nacional y Panorama nacional; Espejos para Salir del Horror, sobre el conflicto armado; Miradas Emergentes, categoría dedicada a trabajos estudiantiles y Otras Miradas, dedicada al cine internacional.

De igual forma habrá espacios de formación tales como el Taller de Cine Ensayo con Susana Barriga, el Seminario Internacional 'Pensar lo Real' cuyo tema central este año es 'Activismo Documentales'; y un homenaje a la vida y obra de Ricardo Restrepo, uno de los fundadores y director de la Muestra por 15 años.

En contagio radio destacamos algunos documentales imperdibles para esta edición del festival:

**• LA MADRE DE LAS MADRES**

SINOPSIS  
Teresita Gaviria es una madre que tras la desaparición de su hijo se encuentra sumida en un profundo dolor, desde aquel momento ha buscado incansablemente la verdad que le permita restituir su vida. En este largo y doloroso trasegar, se ha encontrado con cientos de madres colombianas que comparten su misma historia. Juntas, exigirán a sus hijos “Vivos, libres y en paz”.

- FUNCIONES  
- Lunes 31 Octubre 6:00 pm. ALDEA  
- Miércoles 01 Noviembre 5:00 pm. Biblioteca Pública Gabriel García Márquez el Tunal  
- Viernes 03 Noviembre 4:30 pm. CMPR

**• KURDISTAN LA GUERRA DE LAS CHICAS**

Mylène Sauloy francia 2016 54 min

SINOPSIS  
Empezó hace unos 40 años… Un grupo de mujeres alevíes, una minoría que defiende una sociedad igualitaria, y mantiene ritos consagrados a la naturaleza, impulsa un movimiento feminista dentro de una organización guerrillera marxista leninista kurda – el PKK. Tras años de entrenamiento militar y formación humanista y política en los montes de Irak, estas mujeres están hoy en día en primera línea de combate contra la barbarie islamista de Daesh, y proponen una sociedad igualitaria, multiconfesional, plurietnica, en una región azotada por la violencia de djihadistas y dictadores medio orientales. Son ellas una flor que crece sobre los escombros de la barbarie machista.

FUNCIONES  
Martes 31 Octubre 6:00 pm. CINE TONALÁ - SALA TONALÁ  
Miércoles 01 NOVIEMBRE 4:30 PM. CMPR

**• MÁS AMOR POR FAVOR**

Adalí Belén Torres Ferrer Perú 2016 22 min 49 seg.

SINOPSIS  
Amador explora su identidad a través de un viaje con el que busca desafiar la definición de género. Teme mutar y convertirse en hombre o mujer. El filme es un recorrido por su ciudad, la familia, los pelos, su cuerpo y sus miedos. Una cabrita limeña que interactúa con los miedos de todos nosotros.

FUNCIONES  
Martes 31 Octubre 1:30 pm. BIBLIOTECA NACIONAL GERMÁN ARCINIEGAS  
Miércoles 01 Noviembre 5:30 pm. BIBLIOTECA NACIONAL AURELIO ARTURO

**• WARMIPURA**  
Melisa Sánchez Hincapié Bolivia 2016 26 min

SINOPSIS  
WARMIPURA refleja las vivencias y remembranzas de un grupo de mujeres que habitan la comunidad de Jatun Churicana en el campo de Chuquisaca, Bolivia. Con sus testimonios entretejen los recuerdos y reconstruyen los conocimientos que transitan generacionalmente. WARMIPURA enaltece las labores cotidianas que configuran las dinámicas culturales de esta comunidad en relación con su propio entorno natural.

FUNCIONES  
Martes 31 Octubre 5 pm. Cine Tonalá Sala Kubrick  
Miércoles 01 Noviembre 6:30. Auditorio Fundadores Universidad Central  
Jueves 02 Noviembre 3 pm. Cinemateca Distrital

**• PRECIADO LÍQUIDO**  
Tatiana Laborde Flórez Colombia 2016 15 min 07 seg.

SINOPSIS  
Madelaine con la gran ayuda de Patricia, al igual que otras mujeres, se reúnen en una esquina para esperar y recoger el agua.

FUNCIONES  
Lunes 30 Octubre 5:30 pm. BIBLIOTECA NACIONAL GERMÁN ARCINIEGAS\*  
\*Encuentro con Tatiana Laborde Flórez, directora película\*  
Viernes 03 Noviembre 1:30 pm. BIBLIOTECA NACIONAL AURELIO ARTURO  
Noviembre 1:30 pm. BIBLIOTECA NACIONAL AURELIO ARTURO

**• LUZ OBSCURA**

Susana de Sousa Portugal 2016 76 min

SINOPSIS  
¿Cuántos miembros de una familia se arrastran a la red de la policía política en la detención de un solo preso político?  
¿Cómo dar forma a alguien que desapareció sin haber tenido una existencia histórica? Tomando como punto de partida las fotografías tomadas por la policía política portuguesa (1926-1974), Luz Obscura intenta revelar cómo un sistema autoritario opera dentro de la intimidad familiar, revelando simultáneamente áreas de represión que moldean el presente.

FUNCIONES  
Miércoles 01 Noviembre 6:00 pm. CINE TONALÁ - SALA TONALÁ  
Jueves 02 Noviembre 4:00 pm. SALA MAMBO  
Viernes 03 Noviembre 5:00 pm. CENTRO ÁTICO AUDITORIO UNIVERSIDAD JAVERIANA

**• A CAMBODIAN SPRING**

SINOPSIS  
“Una primavera camboyana” es un retrato íntimo y único de tres personas atrapadas en el desarrollo caótico y a menudo violento que está conformando la moderna Camboya. Filmada durante seis años, la película muestra la creciente oleada de protestas por los derechos de la tierra que condujeron a la “Primavera Camboyana” y los trágicos sucesos que siguieron. Esta película trata sobre las complejidades – tanto políticas como personales, de luchar por lo que uno cree.

FUNCIONES  
Lunes 30 Octubre 6:30 pm. Auditorio Fundadores Universidad Central  
Sábado 04 Noviembre 2:00 pm . SALA MAMBO  
Domingo 05 Noviembre 5:00 pm. Cinemateca Distrital

###### Encuentre más información sobre Cine en [24 Cuadros](https://archivo.contagioradio.com/programas/24-cuadros/) y los sábados a partir de las 11 a.m. en Contagio Radio 
