Title: Sin Olvido - Nicolas Neira
Date: 2015-01-06 15:26
Author: CtgAdm
Category: Sin Olvido
Slug: nicolas-neira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/nicolas-neira.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/NICOLAS-NEIRA-ANIVERSARIO-2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cortesía de la familia 

[Audio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/fsYf5tv2.mp3)

Hoy, 1 de Mayo recordamos a Nicolas Neira, quien fue asesinado  por integrantes del Escuadrón Móvil Antidisturbios ESMAD, durante la marcha del día del trabajo en el año 2005 a la que se unen sindicalistas, campesinos, estudiantes, desempleados, activistas.

Nicolas Neira de 15 años de edad se encontraba cursando el grado noveno en el Liceo Miguel de la Salle, ese día hace 9 años había decidido encontrarse con unos amigos para comprar libros en la carrera 7ma con calle 18, allí fue,y  se encontraron la represión en la marcha, el junto con demás marchantes fueron atacados por el  ESMAD, que les lanzó gases lacrimógenos, y les propinaron golpes con bastones y patadas.
