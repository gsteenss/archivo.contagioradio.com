Title: Derogan suspensión de audiencias contra militares responsables de Falsos Positivos de Soacha
Date: 2017-05-12 12:19
Category: DDHH, Nacional
Tags: Asesinatos, falsos positivos, JEP
Slug: juzgado-44-de-penal-de-conocimiento-da-la-razon-a-las-victimas-de-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/hijosenbogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [[Foto: Hijos en Bogotá]] 

###### [12 Mayo 2017] 

El juez noveno de garantías recibió la orden de volver a notificar la programación de audiencias en los casos de "falsos positivos" en que fueron **asesinados Daniel Alexander Martínez (21 años), Diego Armando Marín Giraldo (21 años) y Jaime Estiven Valencia Sanabria (16 años).**

La justicia respondió la acción de tutela por vía de hecho que interpuso la señora María Sanabria, madre del asesinado Jaime Estiven Valencia Sanabria. **La tutela abogaba contra la vulneración de los derechos de las víctimas a la igualdad, el debido proceso y el acceso a la administración de justicia.** Le puede interesar:** **["Falsos Positivos no deben pasar por la JEP"](Reciba%20toda%20la%20información%20de%20Contagio%20Radio%20en%20su%20correo%20o%20escúchela%20de%20lunes%20a%20viernes%20de%208%20a%2010%20de%20la%20mañana%20en%20Otra%20Mirada%20por%20Contagio%20Radio.)

Lo establecido el 05 de mayo de este año, por el juzgado 44 Penal de Conocimiento, dejó en claro que **la justicia ordinaria aún tiene competencia en estos casos** puesto que la Justicia Especial para la JEP aún no ha sido creada.

El juez noveno Penal de Conocimiento **se había negado a realizar audiencias de imputación de cargos** contra miembros del ejército argumentando que la Jurisdicción Especial para la Paz –JEP- tendría la competencia.

Las organizaciones de víctimas como MINGA, habían afirmado que “En la legislación vigente **no hay ningún ordenamiento que permita suspender los procesos penales** contra los miembros de la fuerza pública presuntamente responsables de atrocidades”. (Le puede interesar: Ejecuciones extrajudiciales[ son crímenes de lesa humanidad."](https://archivo.contagioradio.com/falsos-positivos-son-crimenes-de-lesa-humanidad/))

### **Jurisdicción ordinaria deberá continuar con los procesos por ejecuciones extrajudiciales** 

Pilar Castillo, de la Asociación MINGA, afirmó que **“lo importante de la decisión es que sienta un precedente para que la jurisdicción ordinaria continúe con las investigaciones de Falsos Positivos”.** El fallo de tutela le da la razón a las víctimas en la medida que clarifica que en este momento no hay ley alguna que le quite competencia a los jueces de la jurisdicción ordinaria en estos casos. Le puede interesar: ["Corte Penal Internacional investigará ejecuciones extrajudiciales en Colombia"](https://archivo.contagioradio.com/corte-penal-internacional-investigaria-falsos-positivos-en-colombia/)

Según Castillo la JEP no ha sido puesta en funcionamiento por lo que de **no atender a las víctimas se estaría configurando la falta de acceso a la verdad, la justicia y la reparación.** La decisión fue bienvenida por la Asociación de víctimas MINGA pues va a ser posible, en los casos de falsos positivos, que se revisen los hechos que tienen vicios en el procedimiento.

<iframe id="audio_18655546" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18655546_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
