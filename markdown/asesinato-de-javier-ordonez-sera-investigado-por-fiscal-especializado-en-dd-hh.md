Title: Asesinato de Javier Ordóñez será investigado por fiscal especializado en DD.HH.
Date: 2020-09-10 14:29
Author: CtgAdm
Category: Actualidad, Judicial
Tags: Javier Ordóñez
Slug: asesinato-de-javier-ordonez-sera-investigado-por-fiscal-especializado-en-dd-hh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Luego de conocerse que el caso de Javier Ordóñez, [abogado asesinado](https://archivo.contagioradio.com/el-abogado-javier-ordonez-fue-asesinado-por-la-policia-afirman-sus-familiares/)este miércoles en Bogotá en medio de una intervención policial, iba a pasar a la Justicia Penal Militar, la Fiscalía General de la Nación, se pronunció señalando que **este caso pasaría a ser investigado por un fiscal especializado en Derechos Humanos.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FiscaliaCol/status/1304085758601498625","type":"rich","providerNameSlug":"twitter"} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FiscaliaCol/status/1304085758601498625

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre las 11 de la mañana el Fiscal General de la Nación, Francisco Barbosa, anuncio que un fiscal especializado de Derechos Humanos asumiría la investigación por el homicidio de Javier Ordóñez , y además agregó ***"se dispuso la creación de un equipo de la más alta calidad forense investigativa y jurídica para dar celeridad al proceso".***

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Enfatizando que para optimizar la investigación sobre el homicido de Javier Ordóñez; **esta investigación se sumará el Cuerpo Técnico de Investigación (CTI)**, realizando diferentes labores de investigación en la recolección de videos y testimonios*, "con el ánimo establecer lo sucedido".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "*Por la gravedad de la afectación evidenciada y por competencia, el caso fue asignado a un **fiscal de la dirección especializada contra las violaciones de los Derechos Humanos, que con la debida diligencia coordinará la obtención del material probatorio** y hará la respectiva valoración*"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

En el mismo comunicado el fiscal Barbosa expresó su rechazo y aseguro iniciar *"****labores de investigación para identificar y judicializar a los responsables de los ataques, contra la infraestructura pública, funcionarios y demás hechos delictivos*"** en el marco de las movilizaciones de este 10 de septiembre en horas de la noche.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La noticia se da en medio de un contexto en el que organizaciones sociales cuestiona el trabajo de la Fiscalía, ente que afirma haber adelantado el esclarecimiento entre un 58 y un 60% los homicidios contra líderes sociales, mientras defensores de DD.HH. afirman que este esclarecimiento no pasa de un 14%, lo que ha llevado a resaltar la necesidad de que la Fiscalía no solo investigue e incremente sus investigaciones sino que impida el aumento la impunidad [(Le puede interesar: "Fiscalía no está sirviendo en el país")](https://archivo.contagioradio.com/fiscalia-no-esta-sirviendo-en-el-pais/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esto mientras sigue sin haber un pronunciamiento oficial por parte del Gobierno ante las [acciones de la **Fuerza Pública**](https://defenderlalibertad.com/boletin-informativo-1-9s/) **que dejaron como resultado de, 24 heridos, 19 por arma de fuego, cerca de 45 detenidos, y 7 posibles casos de homicidios. ** ([Más de 12 personas heridas y dos personas asesinadas, en medio de protestas contra accionar de Fuerza Pública](https://archivo.contagioradio.com/mas-de-12-personas-heridas-y-dos-personas-asesinadas-en-medio-de-protestas-contra-accionar-de-fuerza-publica/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
