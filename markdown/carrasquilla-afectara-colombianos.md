Title: La reforma tributaria de Carrasquilla afectaría al 90% de los colombianos
Date: 2018-08-28 15:56
Category: Economía, Nacional
Tags: Alberto Carrasquilla, empresas, Reforma tributaria, salario minimo
Slug: carrasquilla-afectara-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-28-a-las-3.45.57-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MinHacienda] 

###### [28 Ago 2018] 

Para **Mario Valencia, profesor y analista económico**, sí hay necesidad de hacer una reforma tributaria, pero esta no debe hacerse en la forma en que la ha planteado el ministro de hacienda, **Alberto Carrasquilla,** porque sería nocivo para el crecimiento económico, el ingreso de los hogares y la canasta familiar de todos aquellos que ganan menos de 3 millones de pesos al mes, es decir, el **90% de los colombianos.**

El analista sostuvo que la reforma tributaria como la ha planteado Carrasquilla se resume en "bajarle mas impuestos a las empresas, y trasladar esa carga tributaria a los hogares colombianos, aumentando la base del **IVA** y en el impuesto a la renta", lo que resultaría ser una medida inconveniente para la economía nacional. Aunque Valencia recordó que no sería la primera vez que se intenta una reforma tributaria de este tipo, porque en el gobierno Santos se siguió la misma linea.

El profesor señaló que la reforma afectaría al 90% de los colombianos, que **son los que ganan menos de 3 millones de pesos al mes;** y con la idea de que declaren renta aquellos que ganan más de 1,9 millones de pesos, Carrasquilla está definiendo a los nuevos ricos en Colombia. (Le puede interesar:["La reforma tributaria de Duque: Los ricos más ricos y los pobres más pobres"](https://archivo.contagioradio.com/reforma-tributaria-duque-ricos-pobres/))

La medida es impopular porque se está buscando mayor dinero para **un Estado "que se ha demostrado corrupto e ineficiente en el gasto público"**. Adicionalmente, en Colombia no hay una redistribución de la riqueza, no hay crecimiento económico, no disminuye la desigualdad y no se ha buscado el aumento de la clase media, que la que suele impulsar la economía.

### **Bajar los impuestos a las empresas fue un argumento de campaña** 

Valencia afirmó que la idea de reducir los impuestos a las empresas fue parte de las propuestas de campaña, y se sustenta en el argumento de que las compañías pagan muchos impuestos, hecho que es falso. Además, porque **las corporaciones gozan de beneficios tributarios que rondan los 14 billones de pesos al año.**

Por otra parte, tampoco es cierto que las personas naturales paguen pocos impuestos y la carga fiscal sea sostenida mayoritariamente por las empresas, puesto que según el analista **"el 52% de todo el recaudo en Colombia se adquiere vía impuestos progresivos, es decir, IVA",** de forma tal que los ciudadanos ya están pagando muchos impuestos.

### **¿Cómo sería una buena reforma tributaria?** 

Ante la necesidad de formular una reforma tributaria, Valencia afirmó que esta debería seguir el principio de ser progresiva, esto es, "**que los ricos pagan más impuestos y los menos ricos pagan menos impuestos o incluso, los sectores más pobres de la población no pagan impuestos"**. El analista también afirmó que debería orientarse la tributación a impuestos directos, de renta a las empresas y a las personas naturales "pero definiendo quiénes son los ricos en Colombia".

Una medida adicional debería preocuparse por la DIAN, y su capacidad para vigilar y castigar a quienes evaden impuestos, puesto que según el experto en economía, una parte de la tributación se va a paraísos fiscales, por ejemplo, **"la empresa que tiene el ministro Carrasquilla en Panama",** así como algunos otros que aparecieron en los 'Panama Papers'. (Le puede interesar:["¿Quiénes son los colombianos involucrados en la lista de los Papeles de Panama"](https://archivo.contagioradio.com/colombianos-involucrados-en-papeles-de-panama/))

### **Salario mínimo sí, pero no así** 

Sobre la propuesta del Centro Democrático con la que se pretende aumentar el salario mínimo por una sola vez mediante facultades otorgadas al ejecutivo, Valencia afirmó que sí bien es cierto que **el salario mínimo es insuficiente, no permite satisfacer las necesidades básicas de la población, y es más bajo que el promedio de todos los países que integran la Organización para la Cooperación y el Desarrollo Económico (OCDE);** es una propuesta inviable, que pretende matizar la reforma tributaria y que irá en realidad a las cuentas del sector financiero.

El analista afirmó que hay que subir el salario mínimo, pero este Proyecto de Ley tiene un 'mico' y es que el aumento no iría a la cuenta de nómina de los trabajadores sino a sus cuentas de cesantias con un tiempo de permanencia mínimo, es decir, **ese hipótetico aumento, iría al sector financiero.**

Adicionalmente el efecto de este aumento no sería el esperado, porque no iría directamente al consumo; esto teniendo en cuenta que en el panorama mundial, las crisis se evitan logrando que la clase media crezca, para que aumente la demanda de productos y servicios, y de esa manera se reactive la economía, ante lo que el analista afirma que "en Colombia hacemos todo lo contrario".

<iframe id="audio_28150103" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28150103_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
