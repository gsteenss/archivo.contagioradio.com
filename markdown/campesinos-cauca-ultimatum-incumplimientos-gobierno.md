Title: Campesinos del Cauca fijan ultimatum ante incumplimientos del gobierno
Date: 2019-07-12 17:39
Author: CtgAdm
Category: Movilización, Nacional
Tags: campesinos Cauca, Cauca
Slug: campesinos-cauca-ultimatum-incumplimientos-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/campesinos-cauca.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/ANUC.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

###### [Foto: ANUC] 

Las organizaciones pertenecientes a la Mesa de Derechos Humanos en Defensa de la Vida y el Territorio en Cauca están convocando a una movilización,  para exigir una respuesta efectiva ante los acuerdos pactados desde el 2014  y que han sido incumplidos sistemáticamente por el Gobierno. Entre las exigencias de los campesinos están el mejoramiento de las vías, acceso a vivienda rural y urbana, además de ser reconocidos como sujetos de derecho.

Como parte de su movilización, el pasado 17 de junio, la comunidades campesinas realizaron un plantón pacífico frente a la Agencia de Desarrollo Rural en el departamento del Cauca; lo que desencadenó en una reunión con la ADR y el Ministerio de Agricultura  en la sede de la Agencia Nacional de Tierras en Bogotá el 26 de junio.

Pese a que se ha hablado con el Gobierno aún no existe una solución real por lo que realizarán una **asamblea permanente hasta el 11 de agosto** y  en caso de no ser atendidos, **se movilizarán en el departamento del Cauca.** (Le puede interesar "[«Los acuerdos son para cumplirlos» comunidades negras del Cauca al Gobierno"](https://archivo.contagioradio.com/los-acuerdos-son-para-cumplirlos-comunidades-negras-del-cauca-al-gobierno/))

Nilson Liz, presidente de la Asociación Nacional de Usuarios Campesinos (ANUC) afirma que “**desde el 2014 no se ha cumplido ni siquiera con el 35%** de los acuerdos que eran para cumplirse en 2015**,** en temas como la vivienda, pues para este año  debieron ser entregadas 1500 viviendas rurales y tan solo se han entregado 737, además de  proyectos productivos, salud  y educación. El Gobierno tampoco ha reconocido a la población campesina como sujetos de derecho, aunque  la ONU define este sector como vulnerable y minoritario"

Según Nilson Liz aun no tienen respuestas claras por parte del Gobierno y no existe una agenda de cumplimiento, adicionalmente el tema de tierras está estancado desde el año pasado ya que de 1316 unidades agrícolas solo se han entregado  530, tampoco se ha realizado la derogación de los contratos de concesión del territorio,  pues más del 60% del terriotrio caucano está solicitado para que, alrededor de 24 multinacionales, desarrollen mega proyectos mineros, energéticos  y maderables.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38328263" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38328263_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
