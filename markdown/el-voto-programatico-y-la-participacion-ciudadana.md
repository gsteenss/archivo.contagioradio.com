Title: El voto programático y la participación ciudadana
Date: 2019-07-11 11:20
Author: CtgAdm
Category: Opinion
Tags: elecciones, participación ciudadana
Slug: el-voto-programatico-y-la-participacion-ciudadana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Diseño-sin-título-2.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

Hace pocos días se abrió en todo el país el proceso de inscripción de candidatos y candidatas para las elecciones locales de 2019, donde se elegirán alcaldes, gobernadores, concejales, diputados y ediles. Este hecho marca el inicio formal de las campañas y abre nuevamente las discusiones alrededor de los temas de interés para el desarrollo local y la importancia de la inclusión efectiva de la ciudadanía en la planeación participativa de sus territorios.

En ese sentido, el voto programático cobra relevancia como principio electoral que obliga a los candidato(a)s electo(a)s a cumplir con sus programas de gobierno a través de la ejecución del plan de desarrollo -instrumento que orientará la gestión pública durante el cuatrienio- sin perder de vista la importancia de incluir a la ciudadanía en la identificación de las problemáticas locales y la construcción colectiva de planes y políticas.

La democracia representativa, entendida como la delegación del poder político a un grupo de ciudadanos elegidos, debe ser complementada con un ejercicio de democracia participativa. El hecho de contar con un plan de desarrollo formulado y en proceso de ejecución, no exime a los alcaldes y gobernadores de incentivar y consolidar los distintos espacios formales e informales de participación ciudadana que permitan el control social, la rendición de cuentas y la incidencia en la toma de decisiones sobre lo público.

No obstante, lo que se viene evidenciando en algunos casos, es la apropiación del discurso del voto programático por parte de los alcaldes y gobernadores como justificación para imponer medidas en asuntos críticos a nivel territorial como la movilidad, la seguridad y la infraestructura, sin ser debidamente socializadas y concertadas en procesos de diálogo democrático entre la institucionalidad, las organizaciones de la sociedad civil y la ciudadanía en general.

Un ejemplo preocupante de esta situación es el del Distrito de Barranquilla. Durante los últimos doce años se ha erigido un modelo de ciudad de acuerdo con los intereses de ciertas élites políticas y económicas locales, mientras que las cifras de participación ciudadana son muy pobres, en comparación con otras ciudades del país. De acuerdo con datos de Barranquilla Cómo Vamos, el 85% de los ciudadanos manifiesta no hacer parte de ninguna iniciativa organizativa social o comunitaria.

Esto parece responder a la sensación generalizada de que las cosas marchan bien en una ciudad en la cual el índice de popularidad, que registra el actual mandatario, alcanza el 90%. Aunque en el Distrito existen instancias formales de participación ciudadana, la mayoría sólo son de carácter consultivo, pero su funcionalidad se ha visto empañada por la implementación de un modelo de gobierno que prioriza los espacios informales entre la ciudadanía y la institucionalidad.

De cara al nuevo proceso electoral, es importante hacer un llamado de atención a lo(a)s candidato(a)s y la ciudadanía sobre la relevancia de la participación ciudadana desde el proceso inicial de formulación de los programas de gobierno, la activa inclusión en la construcción de los planes de desarrollo y la posterior incidencia en el control social a la gestión pública.

La democracia participativa no puede quedar relegada únicamente a la utilización del voto programático como carta blanca para la toma unilateral de decisiones por parte de lo(a)s gobernantes de turno.

**Contáctenos: 316 697 8026 –  282 2550**
