Title: INPEC no ha remitido a médico especialista a integrante preso de Cahucopana
Date: 2016-05-02 21:23
Category: DDHH, Nacional
Tags: Cahucopana, INPEC, presos politicos
Slug: inpec-no-ha-remitido-a-medico-especialista-a-integrante-preso-de-cahucopana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Palo-Gordo-e1462241747716.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Pluma 

###### [2 May 2016] 

Carlos Morales, integrante de la Corporación Acción Humanitaria por la Convivencia y la Paz del Nordeste Antioqueño, Cahucopana, **víctima de un montaje judicial y quien se encuentra recluido desde el pasado 27 de septiembre de 2015 en la cárcel de Palo Gordo,** en Santander, se encuentra en grave estado de salud sin que el INPEC lo atienda acorde a su condición.

De acuerdo con la denuncia de Cahucopana, Carlos viene sufriendo fuertes dolores abdominales desde hace más de 15 días, por lo que debió ser valorado por un médico general, quién manifestó que el defensor de derechos humanos debía ser revisado y valorado por un médico cirujano especialista, pero **hasta el momento ni el INPEC ni las autoridades del Centro Penitenciario de Girón han realizado dicha remisión.**

También se denuncia que durante el traslado Morales fue **objeto de tratos crueles por parte de un integrante del INPEC,** quien lo golpeó arbitrariamente. De esta situación se hizo una acción urgente y se puso en conocimiento del nivel central del INPEC.

Según la información de la organización del Nordeste Antioqueño, en el 2012, Carlos sufrió una complicación de salud, que terminó en **una grave peritonitis** que lo tuvo en cuidados intensivos por más de 15 días, de manera que se cree que esta dolencia es la que se está reiterando, lo que implica que la vida e integridad del líder campesino está en grave riesgo, si no es atendido correctamente.

Cahucopana responsabiliza al INPEC, a las autoridades penitenciaras y al cuerpo de guardia del establecimiento penitenciario de Girón, por la situación, exigiendo que las acciones necesarias sean realizadas de manera inmediata.

Cabe recordar que el pasado 4 de diciembre, ante el **Juzgado Cuarto Penal Municipal de Barrancabermeja se solicitó la nulidad y revocatoria de la medida de aseguramiento,** sin que hasta la fecha se haya realizado la audiencia. Esta misma solicitud se ha elevado al **Juzgado 2 Promiscuo Municipal de Puerto Berrío** desde el 29 de marzo de 2016, sin que tampoco se haya fijado la fecha de realización de la audiencia.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
