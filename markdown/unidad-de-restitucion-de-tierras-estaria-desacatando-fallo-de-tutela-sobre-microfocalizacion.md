Title: Unidad de Restitución de Tierras estaría desacatando tutela sobre microfocalización
Date: 2017-06-06 12:19
Category: DDHH, Nacional
Tags: Chocó, Desplazamiento, Microfocalizaciones, Restitución de tierras, víctimas
Slug: unidad-de-restitucion-de-tierras-estaria-desacatando-fallo-de-tutela-sobre-microfocalizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Restitucion-Tierras-Uraba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de prensa IPC  
] 

###### [06 Jun. 2017]

El Juzgado 43 Penal del Circuito de Bogotá, falló una acción de tutela a favor de Claudia Patricia Vázquez Londoño, **luego de más de 5 años sin recibir respuesta** y continuar esperando que sus predios, ubicados en Acandí y en Ungía en el departamento del Chocó, fueran restituidos por parte de la Unidad de Restitución de Tierras.

**Diana Lucia Aldana, abogada de la Comisión de Justicia y Paz** relata que estos predios les fueron despojados con diferentes maniobras y vendidos de manera irregular a varias personas naturales luego del asesinato de su padre, el señor Jaime de Jesús Vázquez Rendón en 1989, momento desde el cual se encuentran en condición de desplazamiento y han intentado que sus tierras sean devueltas. Le puede interesar: [Organizaciones sociales proponen reforma a Ley de Víctimas y Restitución de Tierras](https://archivo.contagioradio.com/organizaciones-sociales-proponen-reforma-a-ley-de-victimas-y-restitucion-de-tierras/)

“La familia ha intentado activar la ruta de restitución de tierras obteniendo de manera reiterada la respuesta, un poco excusándose, en que la unidad de Restitución de Tierras tiene un plazo de 10 años para cumplir la ley y en el principio de gradualidad, por lo que **la institución se ha abstenido de realizar las microfocalizaciones** en gran parte de los territorios de toda Colombia” aseveró la abogada.

La orden que da el Juzgado es que **la Unidad de Restitución de Tierras debe dar una respuesta de fondo a las víctimas.** Es decir, debe entregárseles una certeza de en qué momento se van a realizar las microfocalizaciones. Le puede interesar: ["Proyecto de ley del gobierno acabaría con el Banco de Tierras para la Paz"](https://archivo.contagioradio.com/proyecto-de-ley-de-gobierno-profundizaria-modelo-de-acaparamiento-de-tierras/)

### **La Unidad de Restitución de Tierras no responde** 

De acuerdo al Juzgado, **la Unidad de Restitución no ha dado una respuesta aun sabiendo que tenía un tiempo de 5 días hábiles,** una vez notificada la decisión para entregar el argumento de fondo de por qué no se ha realizado aún la microfocalización, así como el informe de la fecha en que se realizará el análisis de seguridad para poder retornar a la zona y de tal manera determinar la viabilidad de microfocalizar.

“Hasta el momento la Unidad no ha respondido razón por la cual **intentaremos acudir a los incidentes de desacato para obtener la respuesta**, esperando que no vengan más consecuencias jurídicas para la institución como el arresto” añadió Aldana. Le puede interesar: [Unidad de Restitución de Tierras mete un mico a la ley 1448](https://archivo.contagioradio.com/unidad-de-restitucion-de-tierras-mete-un-mico-a-la-ley-1448/)

### **¿Qué son las microfocalizaciones?** 

La microfocalización es un mecanismo de selección de casos en el marco de la Restitución de Tierras que deben ser atendidos prioritariamente, la cual es definida por instituciones que cuentan con la capacidad técnica y el conocimiento histórico del contexto de violencia del país. Le puede interesar: ["Organizaciones Sociales proponen reforma a la Ley de víctimas y Restitución de Tierras"](https://archivo.contagioradio.com/organizaciones-sociales-proponen-reforma-a-ley-de-victimas-y-restitucion-de-tierras/)

Para Aldana, si bien se tiene claro que el proceso de microfocalizaciones son complejos y deben ir acompañados de una especie de consejo de seguridad para que “la Unidad de Restitución de Tierras y el Ejército puedan entrar a los territorios y hacer el respectivo procedimiento, esto **no puede ser excusa para demorar de manera indeterminada los derechos de las personas para acceder a las tierras”.**

<iframe id="audio_19109094" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19109094_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
