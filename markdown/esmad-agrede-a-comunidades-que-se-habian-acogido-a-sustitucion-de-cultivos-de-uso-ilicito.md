Title: ESMAD agrede a comunidades que se habían acogido a sustitución de cultivos de uso ilícito
Date: 2019-06-03 17:56
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Agresiones del ESMAD, erradicación cultivos ilícitos, Puerto Asís Putumayo
Slug: esmad-agrede-a-comunidades-que-se-habian-acogido-a-sustitucion-de-cultivos-de-uso-ilicito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Huila-ESMAD-e1465412848210.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

Comunidades en la vereda **La cumbre del municipio de Puerto Asis, ** Putumayo, denunciaron agresiones por parte del Escuadrón Móvil Antidisturbios (ESMAD) **mientras se manifestaban pacificamente contra la erradicación forzada en su territorio**.

Según la información, **la comunidad se habia declarado en asamblea permanente desde el 28 de mayo** pasado, ante la inminente acción de erradicación por parte de la fuerza pública, lo que provocó que **este lunes 3 de junio se presentaran confrontaciones en la zona**.

Según denuncia la misma comunidad, el **ESMAD agredió de forma indiscriminada a quienes participaban de la manifestación** dejando varias personas afectadas de las que aún ni se ha establecido su situación.

Entre los heridos han reportado el nombre del presidente de **la Junta de Acción Comunal de La Cumbre y dirigente de la organización ASOC PUERTO ASIS, Deobaldo Cruz**, quien fue trasladado a un centro médico por la gravedad de sus heridas.

Está situación se da luego de que la mayoría de las comunidades de esta región, de tradición cocalera, **han afirmado que se acogerian a los planes de sustitución de cultivos de uso ilícito (PNIS)** previstos en el acuerdo de paz, que han sido incumplidos por el estado.

###### Reciba toda la información de Contagio Radio en [[su correo]
