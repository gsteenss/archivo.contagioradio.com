Title: Antonio Gallego Mesa, exguerrillero de las FARC es asesinado en La Macarena, Meta
Date: 2020-03-22 13:17
Author: AdminContagio
Category: Nacional
Slug: antonio-gallego-mesa-exguerrillero-de-las-farc-es-asesinado-en-la-macarena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Antonio-Gallego-Mesa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Aumenta a 191 los asesinatos contra ex firmantes del acuerdo de paz, a través de las redes sociales el Partido FARC informó del asesinato Albeiro Antonio Gallego Mesa, de 68 años de edad, excombatiente y militante de este partido en La Macarena, Meta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hasta el momento se desconocen los hechos en los que se dio el crimen del ex guerrillero. Ver: [Edwin Carrascal excombatiente asesinado en Sucre](https://archivo.contagioradio.com/edwin-carrascal-excombatiente-de-farc-asesinado-en-sucre/)

<!-- /wp:paragraph -->

<!-- wp:html -->

> En días pasados dije que la pandemia del exterminio sistemático sigue cobrando víctimas, ayer en la Macarena fue asesinado el compañero Albeiro Antonio Gallego Mesa de 68 años, ex guerrillero, firmante de la paz. Siento dolor y rabia por este nuevo atentado contra la paz. [pic.twitter.com/VPzxZvCzJ4](https://t.co/VPzxZvCzJ4)
>
> — Sandra Ramírez (@SandraFARC) [March 22, 2020](https://twitter.com/SandraFARC/status/1241748154321719296?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"><br />
</script>
  
<!-- /wp:html -->

</p>
<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1241742097406988289","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1241742097406988289

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En desarrollo...

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
