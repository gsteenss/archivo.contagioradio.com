Title: Desprecio al fanatismo
Date: 2015-04-27 05:43
Author: CtgAdm
Category: Laura D, Opinion
Tags: cristianismo., Estado Islámico
Slug: desprecio-al-fanatismo
Status: published

#### Por **[Laura Duque](https://archivo.contagioradio.com/laura-duque/)** 

¿Querían un ejemplo de cómo era la Edad Media? Ojalá recuerden las imágenes divulgadas por el autodenominado Estado Islámico en las que se ven a sus miembros exhibir sin reparo cómo destruyen la que fue considerada, hace tres milenios, la capital del mundo: Nimrud, ubicada en el Antiguo Irak, la tierra de las primeras cosas; Mesopotamia nos dio la primera escritura, la primera ciudad, el primer código legal escrito y el primer imperio. Nada menos que el mayor hallazgo arqueológico del siglo XX según los conocedores. ¡Una barbaridad!

Una recua de barbudos aniquiló todo vestigio de una historia milenaria en cuestión de minutos. Armados con martillos, sierras eléctricas, taladros... despedazaron siglos de historias documentadas en bellos tallados; y al final, de la manera más insolente y canalla, detonaron con barriles explosivos todo el territorio. Sin lugar a dudas, una aberración. Un crimen histórico de la historia, un sinsentido.

El crimen lo perpetraron convencidos de que Alá, su Dios, no admite competencia alguna, convencidos de que otra deidad resulta pecaminosa y fraudulenta, convencidos de que la destrucción es la salvación. Los energúmenos convirtieron en polvo lo que segundos antes narró; redujeron la historia a fuego y humo. Silencio.

Sabemos que no es la primera vez que las sociedades se eliminan entre sí, por eso sólo nos resta llorar, o, si se quiere, resistir, sobre la sustancia derramada y aceptar que las generaciones venideras sumarán otra laguna que alimentará el desconocimiento, la ignorancia, la pobreza documental... producto de las memorias asesinadas que sólo pueden conducir a las tinieblas. Una memoria incompleta, caldo de cultivo para la desidia y la violencia.

Como suele ocurrir la Colombia rota no se salva. En el país del Sagrado también se impone el silencio y el olvido. Acallan los relatos: asesinan, exilian, extraditan, censuran, distraen, etc., etc. En cambio, a veces sacralizan los extremos, los que no permiten las otras voces.

La memoria es un derecho de los pueblos, y el olvido no puede ser la imposición violenta de unos pocos en detrimento de la historia humana. Llámese fanatismo, intolerancia, ceguera... todo se resume en estupidez, la estupidez humana que impide comprender. ¡Bárbaros y estúpidos!

¿Que quieren borrar todas las huellas de idolatría? entonces que empiecen por la propia. Es un absurdo, pues, de la memoria nadie se escampa, nos irrita o nos alivia, puede ser la vida o la muerte, pero nadie se salva. Hay que confiar en la diversidad, en el gozo del conocer lo mío, pero también lo del otro.

En mi opinión lo ocurrido en Irak no se trató más que de un etnocidio a la historia, del oscurecimiento de los visos de luz y de virtud que ha mostrado la humanidad y de las ganas de hundir el mundo en la ignorancia. La razón de todo radica en el interés esquizofrénico  por el poder y la dominación, que ata los actos del individuo y de los pueblos; el fanatismo como la más dañina de las patologías evidencia lo supersticiosos, temerosos e inoperantes que somos como sociedad, pues quienes más vociferan de ser los defensores de la humanidad, también fueron cómplices por omisión o por interés de que sucediera.

Desprecio cualquier fanatismo religioso, desprecio sus creencias, desprecio su maldita ceguera. Hoy es el Estado Islámico, ayer fue el cristianismo. Siempre ha sido la humanidad.

No desprecio el olvido, solo estoy a favor de la historia, de la pluralidad. El olvido lo creo necesario para sanar, pero que sea por voluntad, y si así lo hiciéremos, en esa compleja elaboración, vale la pena apoyarnos en la filosofía del amor, el perdón y la liberación, así como documentó el memorable Gabriel García Márquez: “La memoria del corazón elimina los malos recuerdos y magnifica los buenos, y gracias a ese artificio, logramos sobrellevar el pasado”.

Twitter: **@lavadupe**
