Title: Una vida aventurada y bienaventurada
Date: 2019-02-24 07:00
Author: CtgAdm
Category: Columnistas, Opinion
Tags: Ernesto Cardenal, Papa
Slug: una-vida-aventurada-y-bienaventurada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Ernesto-Cardenal25040312.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[“Pregunto yo (Cardenal) qué creen que hubiera dicho Herodes si hubiera sabido que una mujer del pueblo había cantado que Dios destronaba a los poderosos y levantaba a los humildes, a los hambrientos llenaba de bienes y a los ricos dejaba sin nada.]

[Ríe la Natalia  y dice: Que estaba loca.]

[Rosita: Que era comunista.]

[Laureano: No es que sólo dirían que la virgen era comunista: era comunista.]

[¿Y qué dirían en Nicaragua si oyeran lo que aquí se habla en Solentiname?]

[Varios: Que somos comunistas.]

[Contesta un joven: Que los hambrientos van a comer.]

[Y otro:   La revolución”.]

*[El Evangelio en Solentiname]*

##### **Por Camilo Castellanos\*** 

[Hace quince días el Vaticano “rehabilitó” al poeta Ernesto Cardenal, a quien el papa Juan Pablo II había suspendido a divinis. “Eres sacerdote ]*[in aeternum ]*[según la orden de Melquisedec”, se dice a los ordenandos el día en que los hacen presbíteros. Sin embargo la autoridad pontificia puede hacer un paréntesis en la interminable eternidad para castigar a los desobedientes que se salen de la fila.]

[Se castigaba así a Cardenal por su compromiso político, por participar en la lucha contra la dictadura somocista y en el régimen sandinista, como ministro de Cultura, pues el sacerdote no puede participar en política, salvo en las excepciones conocidas por todos y por las razones por todos sabidas.]

[La oportunidad del levantamiento de esta sanción es significativa. Cardenal agoniza víctima de una afección renal y en su lecho de enfermo ha recibido la noticia de que el Vaticano ha normalizado su situación, porque lo cierto es que nunca salió de la comunidad de los creyentes que es la Iglesia.]

[Ha sido la vida de Ernesto Cardenal un recorrido accidentado pero constante en sus fidelidades: Dios, el pueblo y la poesía.  Tres amores que se funden en una misma vivencia sin que nadie pueda escindirlos, ni siquiera el todopoderoso papa polaco.  ]

[Cuando era un joven bohemio —de entonces son sus Epigramas en los que canta a sus pretendidas Claudia y Miriam (“Si tu estás en Nueva York / en Nueva York no hay nadie más / y si no estás en Nueva York / en Nueva York no hay nadie”) a la vez que denuncia al tirano—, cuando era un bohemio, Cardenal encuentra su vocación religiosa con la misma radicalidad de todas sus opciones vitales. Entonces abandonó la conspiración y se hizo novicio trapense sin dejar de ser artista. Empero, abandonó la Trapa y años después vino a terminar la Teología en el seminario de vocaciones tardías en La Ceja (Antioquia) y fue ordenado en 1965. A su regreso a Nicaragua fundó Santa María de Solentiname para retomar su vocación contemplativa.]

[Fue una fundación bien particular.  Centro de actividad comunitaria, los campesinos llegaban a las eucaristías y hacían una suerte de homilías participativas que fueron recogidas en el libro El Evangelio en Solentiname. Es de resaltar que lo que fueron resultado de profundas reflexiones de los teólogos de la Liberación, eran intuiciones certeras de los campesinos que reflexionaban con Cardenal. Pero en esta convivencia el campesinado descubrió su vena artística y junto con otros artistas formaron una escuela de pintura primitivista, con lo que la creatividad se articuló a un proyecto de sobrevivencia.]

[Pero además Solentiname se convirtió en un referente para intelectuales de toda Latinoamérica que encontraban en él un espacio para la imaginación y el compartir.]

[El culmen de su poesía mística es el Cántico cósmico en el que trabajó por treinta años, un extraordinario]*[collage]*[ en el que se combinan la ciencia y la vivencia del amor divino y humano, la historia y la cotidianidad, los pensamientos más elaborados y el habla de la calle y que sintetiza su visión de la creación ininterrumpida en la que la conciencia es parte del proceso y las transformaciones revolucionarias son parte de la marcha general del universo.]

[El segundo gran amor de Cardenal fue el pueblo. El inmediato de Solentiname, el más amplio nicaragüense y en general los pueblos de Nuestra América. Particular atención le merecieron los pueblos indígenas, a los que estudió por décadas en sus opresiones y resistencias y que pudo expresarlos en su Homenaje a los indios americanos, en los que se descubre como dijo el crítico que en América la historia es predicción.]

[Su paso por el Ministerio de Cultura fue un Solentiname ampliado.  Su aspiración no fue hacer de los nicaragüenses consumidores de cultura sino productores de belleza y poesía. Pero no era esto lo que preocupaba al polaco que lo castigó sino el inscribirse en un proyecto que buscaba cambiar la vida para beneficio de todos. Confesó alguna vez Cardenal que fueron estos los años más felices de su vida, clausurados de modo cruel con un reparto infame de lo que era de todos y en el que feriaron hasta la esperanza.]

[Cruel pirueta de la historia.  Lo que fue la bella ilusión de un pueblo devino en la absurda pesadilla en la que una yunta concentra los bienes de todos y el terror sobre todos. Esta vez también, Cardenal participa de la desgracia de su gente. Y como en otro tiempo resuena su reclamo: “Y tu eres ahora un Dios clandestino / ¿por qué escondes tu rostro / olvidado de nuestra persecución y nuestra opresión? / Despierta / y ayúdanos! / por tu propio prestigio!”  (Salmo 43)]

[El tercer amor de Cardenal fue la poesía. Y no se va a abundar porque solo de su poesía hablan estas líneas. El hecho es que es una poesía que huyó de la retórica y sus metáforas y se hizo directa y coloquial, historia del día a día, historia de las víctimas y los héroes sin estatua ni nombre, recuento de los tiempos sin tiempo, poesía pura sin artificio como fruto del trabajo y del trato intimo con los pueblos de todas las eras incluida la presente.  Es lo que algunos han llamado antipoesía.]

[Y precisamente por haber cantado con voz propia es el poeta latinoamericano contemporáneo que ha alcanzado mayor reconocimiento universal. Su obra ha sido traducida a las principales lenguas y cuenta con lectores más allá de las convicciones políticas o religiosas.]

[Cardenal acaba de cumplir 94 años.  Está enfermo… pero acaso estará satisfecho de su vida aventurada y bienaventurada.]

------------------------------------------------------------------------

###### \*Defensor de los Derechos humanos y artesano de la paz
