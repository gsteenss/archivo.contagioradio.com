Title: Nuevo testigo reafirma vínculos entre familia Uribe Vélez y 'Los 12 Apóstoles'
Date: 2016-06-17 15:46
Category: Judicial, Otra Mirada
Tags: Familia Uribe Vélez, Los 12 apostoles, paramilitarismo colombia
Slug: nuevo-testigo-reafirma-vinculos-entre-familia-uribe-velez-y-los-12-apostoles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/santiago-uribe-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [17 Junio 2016 ] 

Un nuevo testigo es ahora la pieza clave en el proceso que la Fiscalía adelanta en contra de Santiago Uribe, por su presunta vinculación con el grupo paramilitar 'Los 12 apóstoles'. El testimonio reafirma el **estrecho vínculo entre las familias Uribe Vélez y Castaño Gil, con la Policía, el Ejército, comerciantes y sicarios**, para la comisión de los cientos de asesinatos, desapariciones forzadas, amenazas, torturas y desplazamientos, que ocurrieron en la década de los 90 en el norte de Antioquia.

Este poblador de la zona, que ha sido víctima de constantes persecuciones y amenazas por sus denuncias, asegura que el grupo paramilitar se consolidó a principios de los años 90 en Yarumal y **durante esta década se extendió a los municipios de San Rosa de Osos, Ituango, Valdivia, Angostura y Campamento**, con el pretexto de acabar con el consumo y la venta de sustancias alucinógenas y sacar del camino a los extorsionistas y secuestradores de la zona.

El testigo narra que el grupo se denominó 'Los 12 Apóstoles' porque era liderado por quien para la época era el párroco de la iglesia de Yarumal, el sacerdote Gonzalo Javier Palacio, con quien sicarios y comerciantes como Donato Vargas, en complicidad con la Policía y el Ejército, **salían en las noches vestidos con sotanas y gorros al estilo ruso, acompañados de biblias en las que podían ocultar las armas con las que perpetraban los crímenes** contra quienes eran señalados como colaboradores de los frentes 'Héroes de Anorí' y 'María Cano' del ELN.

La hacienda 'La Carolina', de propiedad de Santiago Uribe, funcionó como el centro de reclutamiento y entrenamiento paramilitar desde el que se planeaban las operaciones de 'Los 12 Apóstoles'. En la finca, los **hermanos Castaño Gil entrenaron a sicarios, paramilitares y militares de la IV Brigada del Ejército Nacional y del Batallón 'Atanasio Girardot'** de Medellín. Esta estructura de funcionamiento se complementaba con jefes de áreas urbanas y rurales; en Yarumal quien comandaba la zona urbana era el ex alcalde de Santa Rosa de Osos, Álvaro Vázquez Arroyave.

El grupo era financiado por comerciantes y ganaderos de la zona, en Yarumal Donato Vargas, quien era dueño de la tienda 'El Milagrito', fungía como tesorero, y Roberto López propietario del almacén 'El Mejor Precio' y Paul Martínez aportaban el dinero con el que se pagaba a los sicarios **sumas de entre \$200 y \$450 mil por asesinar a quienes figuraban en las listas negras como colaboradores de la guerrilla**.

Algunos de los sicarios al servicio del grupo paramilitar, conocidos bajo los alias de 'El enano', 'El erizo' y 'Pelo e' Chonta', aseguraban que **Santiago Uribe Vélez "era el patrón, decía quién debía morir" y enviaba los pagos**. Otros testimonios aseveran que el hacendado era uno de los grandes capitalistas de la zona que basaba su economía en la cría de caballos, toros de lidia y ganado, para aportar la mayor parte de la financiación de 'Los 12 Apóstoles'.

El testigo asegura que cuando el grupo estaba en su mayor apogeo, Álvaro Uribe Vélez era Senador de la República por el partido político Sector Democrático y que "Santiago se escudaba en él, para decirle a los militares de la época que estuvieran tranquilos que su hermano era un político grande a nivel nacional y que iba a ser el próximo gobernador de Antioquia, y si lo fue (...) **Santiago se escondía tras la imagen de su hermano**".

Cuando Juan Carlos Meneses Quintero asumió como coronel de la Policía de Yarumal, el testigo sostuvo un diálogo con él, en el que le manifestó su preocupación por lo que estaba sucediendo en la región, y puntualmente por la existencia de una lista negra en la que figuraban varias de las personas que ya habían sido asesinadas, pero como autoridad local hizo caso omiso y lo contactó con el coronel **Jhon Bayron Marulanda, quién era jefe de inteligencia de la IV Brigada y del B2 y hoy es asesor de la Presidencia en seguridad ciudadana**.

Cuando el grupo delinquía en la noche o se desplazaba por los municipios, el Ejército y la Polícia "limpiaban la zona garantizando que no hubiera presencia de la guerrilla", entre los agentes y miembros del Ejército figuran Luis Alfredo Guevara, Alexander Amaya, Jhon Jairo Álvarez, y el **comandante de Policía de Yarumal, Pedro Manuel Benavides, que se reunía constantemente con Santiago Uribe** y se apodaba 'Capitán Represa', porque luego de asesinar a las víctimas botaba sus cuerpos a las represas.

Hoy 26 años después de todos estos crímenes y violaciones, las comunidades rechazan que el Ministerio de Defensa, la Policía y la alcaldía de Yarumal, hayan instalado en la plaza central un monumento a la vida, **en honor a las supuestas 3 únicas víctimas que dejaron sólo 2 años de actividades de 'Los 12 Apóstoles'**, desconociendo que actuaron durante una década y que fueron responsables, entre otras, de la masacre de Campamento ocurrid el 5 junio de 1990.

<iframe src="http://co.ivoox.com/es/player_ej_11941623_2_1.html?data=kpamlpaadpShhpywj5ecaZS1k5mah5yncZOhhpywj5WRaZi3jpWah5yncbXZ1NnWydSPp8Ln0JCelJDFtIa3lIqvldjYs83Z1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo]](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
