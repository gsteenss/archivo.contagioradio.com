Title: Salario mínimo no cuadra con aumento del costo de vida en Colombia
Date: 2017-12-28 11:53
Category: Economía, Nacional
Tags: 2018, Canasta familiar, iva, salario minimo
Slug: salario_minimo_2018_colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/salario-mínimo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [28 Dic 2017] 

Sumando aumentos del transporte, la gasolina y el IVA, el costo de bienes y servicios de los colombianos durante los últimos meses de 2017 está por encima del 10%, sin tener en cuenta el IPC y la inflación. En las cuentas básicas de un hogar el aumento salarial para 2018 debería superar ese porcentaje, de lo contrario la consecuencia lógica es que las familias tendrán cada vez menos dinero y sus condiciones serán más precarias.

Este jueves se realiza la última reunión con la que se busca lograr algún tipo de acuerdo sobre el aumento del salario mínimo para el 2018. De no ser así, como ha venido sucediendo **desde el 2003 dicho incremento lo impondrá el gobierno nacional por decreto.**

La puja entre los trabajadores y los empresarios va así: al comienzo de la negociación las centrales obreras propusieron un aumento del 10 % y 12 %, sin embargo, esta posibilidad rápidamente fue negada por los empleadores quienes propusieron el 4,5% y 4,9%. Tras algunos días de conversaciones, los empresarios unificaron su propuesta a** 5,1% (equivalente a \$37.324), y los trabajadores optaron por un 9% (\$66.395), aún así la diferencia era 3,9%.**

Ante la difícil posibilidad de lograr un acuerdo con tal diferencia entre las partes, Julio Roberto Gómez, presidente de la Confederación General de Trabajadores, CGT, ha propuesto un aumento del 7%, teniendo en cuenta que el DANE ha señalado que la inflación para 2017 sería de un 4%, a ese porcentaje se le debería aumentar el  1% de productividad y el 2% que pierde el colombiano con la reforma tributaria.

Si no se logra llegar a un acuerdo el decreto del gobierno estaría entre el 5,5% y el 6%, es decir un aumento aproximado de \$40.574 para un total de \$778.291, esto indicaría un aumento real de 1.5% teniendo en cuenta que la inflación es pérdida de poder adquisitivo y para 2017 estaría cercana al 4%.

### **¿Cuánto necesita un familia para vivir?** 

Luis Alejandro Pedraza, presidente de la Central Unitaria de Trabajadores, CUT, señala que en diferentes rubros los colombianos han tenido un aumento en la inflación que no les ha permitido ganar poder adquisitivo a lo largo de los últimos años, por eso se esperaba que el salario subiera el 10%.

Por su parte, Luis Fabio Arias, también integrante de la CUT, explica que "El salario mínimo en **Colombia es uno de los más bajos en Latinoamérica.** Una familia mínimo debe tener el equivalente de dos salarios mínimos. Por lo tanto estamos resueltos a que en estas familias los papás deban trabajar siempre". Por su parte, el presidente de la CGT indica que "el salario mínimo debería ser de \$1'600.000  para apaciguar  el costo de la canasta familiar para los estratos bajos".

### **El costo de vida para 2018** 

El solo aumento del salario mínimo genera incrementos para 2018. Además, con el Índice de Precios al Consumidor (IPC), que determina el Departamento Administrativo Nacional de Estadística (DANE), también se incrementa el costo de los arriendos, los avalúos catastrales, los servicios públicos y los peajes.

Sumado a lo anterior, con el aumento del galón de gosolina de 139 pesos para diciembre, el combustible cerró el año con un alza 1,59%. Asimismo, el transporte público ya cuenta con un incremento de \$100, es decir un 6,4% y la reforma tributaria generó que el IVA haya subido 3 puntos porcentuales, llegando al 19%. Pero además, d**e acuerdo con el informe de inflación del Banco de la República, el costo de los alimentos para el segundo semestre del 2018 será más alto de lo previsto. **

Es decir, que sumando el aumento de la gasolina, el transporte público y el IVA las y los colombianos enfrentan un aumento total de 10%, sin contar otros aspectos como los que definirá el IPC. Así las cuentas, nuevamente quienes salen perdiendo son las y los trabajadores, que siguen contando con un salario mínimo, cada vez más mínimo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
