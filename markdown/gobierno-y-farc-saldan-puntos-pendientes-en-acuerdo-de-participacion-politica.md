Title: Gobierno y FARC saldan puntos pendientes en acuerdo de participación política
Date: 2016-07-05 16:38
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, FARC, Juan Manuel Santos, PArticiapción Política, paz
Slug: gobierno-y-farc-saldan-puntos-pendientes-en-acuerdo-de-participacion-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/participacion-politica-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: marcha patriótica] 

###### [05 Jul 2016]

Las delegaciones del gobierno y las FARC anunciaron que en este ciclo han avanzado en algunos asuntos pendientes en el punto de participación política, en ese sentido llegaron a varios acuerdos que tienen que ver con el estatuto de garantías para el ejercicio de la oposición política, proyecto de ley de garantías y promoción de la [participación](https://archivo.contagioradio.com/?s=participacion+politica) ciudadana y la Reforma del régimen y de la organización electoral.

### **Estatuto de oposición** 

Entre lo anunciado se acordó que habrá una convocatoria amplia a [movimientos políticos](https://archivo.contagioradio.com/proyecto-ciudadania-y-participacion-politica-para-el-postconflicto/)y además a [Marcha Patriótica](https://archivo.contagioradio.com/?s=marcha+patriotica) y [Congreso de los Pueblos](https://archivo.contagioradio.com/?s=congreso+de+los+pueblos). Estos partidos y movimientos conformarán una comisión que definirá los mecanismos para recibir los insumos que les permitan hacer una propuesta en torno  al derecho a la [participación política](https://archivo.contagioradio.com/participacion-de-la-sociedad-civil-sera-clave-en-desmonte-del-paramilitarismo/). “Sobre la base de estos lineamientos el Gobierno Nacional elaborará un proyecto de ley con el acompañamiento de delegados de la Comisión de partidos y movimientos políticos”

Se convocarán también “otras agrupaciones políticas representativas de oposición, según lo acuerden las Partes. La Comisión a través de un evento facilitará la participación de voceros de las organizaciones y movimientos sociales más representativos, expertos y académicos, entre otros” **Esta comisión será convocada por el gobierno y se esperan los aportes una vez sea firmado el acuerdo final**.

### **Promoción de la participación ciudadana** 

La mesa de conversaciones solicitó al **Foro por Colombia, Viva la Ciudadanía y el CINEP** organizar un  espacio de participación de carácter nacional de que trata ese punto y que elaboren, una propuesta para su discusión en la Mesa sobre los criterios y lineamientos para el desarrollo de ese espacio. La propuesta sería entregada en 2 semanas.

### **Reforma del régimen y de la organización electoral** 

Se creará una misión electoral especial con el objetivo de garantizar transparencia en los mecanismos de la democracia en Colombia así como asegurar una participación amplia por parte de la ciudadanía. Esa comisión especial debería entregar una serie de recomendaciones en 6 meses para que el gobierno aplique los cambios que sean necesarios para conseguir ese objetivo

“La Misión estará conformada por 7 expertos de alto nivel, que en su mayoría deberán ser de nacionalidad colombiana, así: un representante de la [Misión de Observación Electoral](https://archivo.contagioradio.com/?s=moe)- MOE y 6 expertos los cuales se seleccionarán por las siguientes organizaciones, el Centro Carter, el Departamento de Ciencia Política de la Universidad Nacional de Colombia, el Departamento de Ciencia Política de la Universidad de los Andes y el Instituto Holandés para la Democracia Multipartidaria –NIMD”

Aquí el texto completo del comunicado conjunto...

[Comunicado Conjunto sobre participación política](https://es.scribd.com/document/317552643/Comunicado-Conjunto-sobre-participacion-politica "View Comunicado Conjunto sobre participación política on Scribd")

<iframe id="doc_35434" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/317552643/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true&amp;show_upsell=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
