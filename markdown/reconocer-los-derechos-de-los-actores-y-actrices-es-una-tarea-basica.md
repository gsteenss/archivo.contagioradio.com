Title: Reconocer los derechos de los actores y actrices es una tarea "básica" ACA
Date: 2016-04-14 15:34
Category: DDHH, Nacional
Tags: actores, Derechos, Ley de Actores
Slug: reconocer-los-derechos-de-los-actores-y-actrices-es-una-tarea-basica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/IMG_1795.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [14 Abril 2016] 

La [Asociación Colombiana de Actores (ACA)](https://archivo.contagioradio.com/aca-presenta-en-bogota-el-estatuto-del-actor/) presentó hoy su proyecto de ley ante los medios de información, que busca garantizar sus derechos laborales, culturales y de autor. La iniciativa será radicada el próximo 20 de abril en el Congreso de la República por  y cuenta con 32 artículos

Entre las peticiones que exigen los actores se enmarcan tres que serían un avance en el desarrollo de la labor: el primero **es que sean reconocidos como profesionales,** ya que en la actualidad ser actor es reconocido como un oficio, hecho que desvirtúa los estudios y la preparación que han realizado los actores y actrices para ejercer.

El segundo es que se **les otorgue condiciones dignas de trabajo**, este punto busca que los actores y las actrices estén cubiertos por una normativa laboral mínima con garantías de seguridad social, salud y pensión. Por otro lado, se expone la necesidad de establecer un horario de jornada laboral y establecer un legitimo derecho al descanso.

Por último **la ley propone que se garanticen los derechos de autor de los actores y que los derechos patrimoniales de autor se reconozcan de manera independiente** a sus salarios. Estos derechos son los que se pagan a los actores por repetición o venta de sus servicios.

Según Julio Correal, vocero de la ACA, la no existencia de una ley ha permitido que la formación y trayectoria de un actor, no se tenga en cuenta a la hora de generar una contratación. A su vez índica que en Colombia, **se ha legislado la industria cultural protegiendo los intereses de los productores y dejando de lado las demandas de los actores y las actrices**, transformándolos en maquilas para la exportación a bajos costos y de buena calidad.

Para finalizar la Asociación Colombiana de Actores aclaró que esta ley no esta respaldada en este momento por ningún partido político y que esperan que el 20 de abril sea el conjunto de las bancadas quienes la aprueben en apoyo y necesidad del gremio de actores.

Entre los artículos del proyecto de ley se encuentran los siguientes:

**Articulo 1**.  **Objeto.** La presente ley tiene por objeto establecer un conjunto de medidas que garanticen el ejercicio de la actuación como una profesión en Colombia, protegiendo los derechos laborales, culturales y de autor de los actores y actrices en sus creaciones, conservación, desarrollo y difusión de su trabajo y obras artísticas.

**Artículo 8. La Actuación como** **profesión.** La actuación es una profesión en Colombia. En consecuencia, el Estado adoptará las medidas y mecanismos necesarios para garantizar los derechos de los profesionales de la actuación en beneficio de los mismos y de los bienes culturales de la nación.

**Artículo 15- Tipo de vinculación para actores y actrices**. Las actividades de los actores y actrices podrán desarrollarse en relación de dependencia o fuera de ella, sea en forma individual o asociada. Independientemente del tipo de vinculación, a los actores y actrices les aplica la normatividad de seguridad social, incluidas las normas de salud y seguridad en el trabajo, en todo caso, para jornadas de trabajo y descansos aplica lo contemplado en la presente ley.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
