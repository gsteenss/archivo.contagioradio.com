Title: Preocupaciones de HRW sobre ley de amnistía
Date: 2016-12-26 12:44
Category: DDHH, Nacional
Tags: Amnistia, colombia, HRW, Jose Miguel Vivianco, paz
Slug: ley-amnistia-colombia-paz-hrw
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/jose_miguel_vivanco1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:VmeTV 

##### 26 Dic 2016 

En carta enviada al presidente Juan Manuel Santos, el director de Human Rights Watch para las Américas **José Miguel Vivanco, listó una serie de preocupaciones que desde la organización no gubernamental encuentran al Proyecto de Ley de amnistía** que sería aprobado este [27 de diciembre en las plenarias de Senado y Cámara de representantes](https://archivo.contagioradio.com/27-de-diciembre-prueba-de-fuego-ley-de-amnistia-33926/).

En la misiva, fechada el 25 de diciembre, **la ONG valora que el proyecto de ley "considere que las violaciones de derechos humanos deben ser excluidas de amnistías"**, sin embargo alerta que algunas disposiciones del mismo que podrían "limitar la posibilidad de juzgar abusos, respecto de los cuales existe una obligación jurídica de asegurar justicia".

Según la organización, en el documento **se presentan una serie de vacíos jurídicos, en relación con las figuras delictivas no enmarcadas en el derecho colombiano y categorías legales "vagas e imprecisas"** en las que se enmarcan las consideraciones del proyecto, lo que a su parecer se "prestan para abusos".

Otro de los puntos que enuncia Human Rights Watch en la comunicación, tiene que ver con **el mecanismo de Libertad condicional para agentes del Estado o Combatientes de las FARC que estén involucrados con la ejecución de crímenes atroces**, considerando que este "no parece contener unas garantías básicas necesarias para asegurar que criminales de guerra no puedan eludir la justicia".

Para el organismo internacional, las ambigüedades que aparecen en el Proyecto legislativo "podrían permitir que las amnistías *de jure* o *de facto* beneficien a responsables de violaciones de derechos humanos", por la falta de "correlación entre lo consignado en el proyecto y lo que aparece en el derecho penal colombiano" ante lo cual recomiendan que **se debería aclarar "cómo se definen cada una de las categorías legales que no tengan una contrapartida explícita"**.

En cuanto a las categorías legales, la ONG señala que **es necesario esclarecer la lista de atrocidades para las cuales no pueden concederse amnistías tanto en alcance como en significado en el marco de la Jurisdicción Especial para la Paz** (JEP), así como la definición e interpretación de algunos términos que facilitarían la amnistía para combatientes que tienen graves responsabilidades penales.

La carta contiene además referencia a las disposiciones sobre la libertad condicional, tanto de agentes del Estado como de los ex combatientes de las FARC, al encontrar que "no existe una entidad con la potestad de exigir que el acusado pague una caución o comparezca periódicamente ante un juez" y que "no establece de forma clara que la libertad condicional será revocada si el acusado comete nuevos delitos".

Al cierre, la organización le recordó al Presidente de la República que "los agentes del estado responsables de abusos no son parte de las negociaciones de paz y las concesiones que se les hagan son innecesarias para alcanzar la paz en el país" añadiendo que "Si su gobierno cede ante las presiones militares y permite que aquellos que han violado derechos humanos se aprovechen del acuerdo de paz para eludir la justicia, mancillará este proceso y los esfuerzos que S.E. ha realizado para que este sea posible".
