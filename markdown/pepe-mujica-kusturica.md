Title: Pepe Mujica desde la mirada de Kusturica
Date: 2018-09-10 08:41
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Documental, pepe mujica
Slug: pepe-mujica-kusturica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/descarga-12.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ecos Latinoamérica 

###### 08 Sep 2018 

A lo largo de cinco años el cineasta serbio Emir Kusturica registró con su cámara momentos íntimos y públicos del expresidente de Uruguay José Mujica, encuentros que dieron como fruto el documental "El Pepe, una vida suprema", estrenado en la más reciente edición del Festival de Cine de Venecia, Italia, en la categoría fuera de competencia.

La pieza audiovisual, cuyo nombre tentativo durante la producción fue "El último héroe" explora las ideologías de Mujica, su vida política, y personalidad simple y austera. También, se exploran las reflexiones del expresidente sobre de los cambios globales, la sustentabilidad, la militancia y el amor.

El dos veces ganador de la Palma de oro en Canes,  no solo retrata el pensamiento del carismático líder Mujica, sino la vida de un hombre humilde, ex militante de Tupamaros, que con firmeza en sus ideales y opositor a la dictadura militar se siente orgulloso de su pasado, del lugar en el que vive y como durante su periodo de gobierno redujo la pobreza de su país.

El documental, que incluye imágenes inéditas de Mujica en sus últimos días como presidente, se estrenó el marco del 75 Festival de Cine de Venecia. Durante su presentación tanto Kusturica como Mujica fueron ovacionados por los asistentes, recibiendo críticas positivas.
