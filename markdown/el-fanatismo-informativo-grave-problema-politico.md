Title: El fanatismo informativo: ¡grave problema político!
Date: 2017-01-17 09:49
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: politica, redes sociales, tecnologia
Slug: el-fanatismo-informativo-grave-problema-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/fanatismo-a-las-redes1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: redessocialesyinternet 

#### **Por:[Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

**Las redes sociales, más allá de su efecto tácito conectivo y virtual entre las personas**, han tenido últimamente un papel (para bien o para mal) educador y moldeador de la cultura política en el mundo. Colombia no se escapa de ese fenómeno y nos hemos acostumbrado a ver cómo miles de personas opinan, se agreden, blasfeman, y si pudieran salir de la pantalla y darle un coscorrón (así como los que da el señor Vargas Lleras) a quien les contradice, lo harían sin pensarlo dos veces.

[¿Cuál es el problema de las redes sociales? Realmente expuesta así la pregunta, no hay mucho que decir. Las redes acercan gente, promueven la opinión contenida de una sociedad que vive en la indudable paradoja de encontrarse amontonada, cada vez más cerca, “más apeñuzcada” como dirían las abuelas, pero a la vez más incapaz de dialogar, de relacionarse, de considerar el hecho de que si el otro piensa distinto, esto, no constituye una razón suficiente para darle trompadas, insultarlo o en el peor de los casos matarlo.]

[Sé que esta sociedad adolece de todo un poco. Si nuestro cuerpo social colombiano pudiese compararse eufemísticamente con algo, sería tal vez con el cuerpo del señor Burns… ¡sufrimos de todos los males! Por eso quiero enfocarme aquí en uno solo: el fanatismo informativo.]

[El fanatismo informativo no sólo es un problema, es un problemón. Es esa tendencia actual presente en los individuos que los lleva a consumir cuanto trino, posteo o artículo emerge sin establecer una conectividad medianamente lógica, o si acaso causal de lo que implica hablar de un tema; los individuos se embaucan en leer más y más información, en dar like, en retwittear, inclusive durante horas y horas, pero no se toman el trabajo de procesar absolutamente ningún fenómeno social, o tal vez de repensarlo en contraposición con procesos históricos y mucho menos relacionarlo con otro u otros ¿cómo se da uno cuenta de eso? En el preciso momento en que se leen opiniones sin sentido, incoherentes, anacrónicas, absurdas, mal redactadas pero que contienen conceptos, personajes o elementos propios del ámbito político.]

[El fanatismo informativo, tiene envuelta a la gente en una anti lógica de la comprensión de la realidad, pues en el afán de no permitir que “otro” le cuente la historia, o en la actitud bastarda de creerse “libre” en un sistema que le oculta el secreto para tenerlo preso, el individuo hoy en Colombia forja la opinión política a partir de memes o trinos exuberantes; en el mejor de los casos artículos, y podría atreverme a decir que ni el mejor de los artículos basta para configurar una opinión política seria.  ]

[El fanatismo informativo produce individuos en los que crece la necesidad de sublimar el deseo de opinar. Opinar se constituye en un tributo a la libertad de expresión, esa ventaja jurídica de las democracias liberales que están sobrecargadas de ese espíritu neoliberal emprendedor, convenciendo a los individuos de ser sus propios dioses, sin tener en cuenta todo lo demás, sean personas o historia, les da igual.]

[Entonces, la libertad de expresión se vuelve “la vieja confiable” dando como resultado que la]*[opinitis aguda]*[ emerja impulsada por el fanatismo informativo generando un círculo vicioso sin sentido, caduco, que día a día se manifiesta en debates superfluos, efímeros, que luego son reemplazados día a día por otros nuevos, ofreciendo la quimera de una sociedad que supuestamente habla de política, no obstante, sin darse cuenta, el fanatismo informativo sólo convoca al ridículo y no hace más que aportar con mucho ánimo a agrandar las estructuras de esta falsa democracia.]

[¿Se puede evadir a un fanático de la información? Por supuesto que sí. El método es sencillo, basta con formularse una simple pero efectiva pregunta a la hora de pensar si entramos o no en un debate político:]*[¿Quién dijo que cualquier opinión es una opinión política?]*[ eso sería similar a decir que cualquiera sin saber, sin medir y sin practicar, podría ser el mejor pastelero.]

[La humildad debe invadir nuestras relaciones sociales, no se trata de imponer una opinión, se trata de argumentarla, pero si los argumentos son derribados en una conversación coherente y lógica, el tema no puede pasar entonces al espíritu del que más insulte, o el que más miserable trate al otro.]

[Creo que en nuestra sociedad, nos hace falta reconocer nuestra ignorancia, nos hace falta reconocer que para hablar de política no solo se puede consumir información, o ver un par de memes sino que además hay que entenderla, desglosarla, compararla para concluir, buscar en la historia, en la teoría, en la literatura, en fin, creo sin duda alguna que nos hace falta decir: “no sé”.]

[Decir “no sé” es confiar en la división social más elemental y bella de la humanidad, esa que no se rige por el dinero, sino por nuestras habilidades y en el cómo las cultivamos para establecer esa magia diferencial de una colectividad a la que se respeta; es confiar que no todos somos zapateros, no todos somos filósofos, no todos sabemos cultivar la tierra, no todos somos obreros, no todos sabemos sobre política. Decir “no sé” es reconocer que para aprender debo buscar al otro, y no solo en la internet por muy buena que sea, pues en internet el diálogo ha fracasado en sus intentos, o si no, percátese por causalidad en las no pocas barbaridades que se dicen en los comentarios por Twitter o Facebook cuando de política se habla.  ]

[Decir “no sé” es humildad, es buscar en el otro, es confiar en que tal vez ese otro que se cultiva tiene algo que decir sobre un tema porque es su experticia. Significa dejar de lado el fanatismo informativo, pues éste moldea individuos que saben mucho y comprenden nada. Aprender a decir “no sé” sería darle una “patadita de la mala suerte” a esa ideología neoliberal que solo habla del éxito y que incita al individuo a ser su propio jefe, su propio dios, su propio profesor, su propio amor… al final, su propio verdugo.]

#### [**[Ver más columnas de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)**] 

------------------------------------------------------------------------

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
