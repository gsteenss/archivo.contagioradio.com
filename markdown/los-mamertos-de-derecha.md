Title: Los mamertos de derecha
Date: 2017-06-22 11:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: alvaro uribe velez, derecha, mamertos, Peñalosa
Slug: los-mamertos-de-derecha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/mamertos-de-derecha1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Memes 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 22 Jun 2017 

[Ni en el diccionario panhispánico de dudas, ni en el diccionario de la lengua española aparece la palabra mamerto. La búsqueda en wolrdReference asocia la palabra “mamerto” a tonto, a provinciano; algunos artículos poco fiables que navegan por Facebook, indican que mamerto alude a un sujeto que conoce poco de aquello de lo que habla. Por último, en un artículo de Santiago Molina (¿qué significa ser mamerto?) hay un acercamiento a otra definición que sugiere, que mamerto es aquel que se mama de la lucha que adelanta.]

[¿cómo definir con certeza entonces esa palabra que más que un término parece una acusación? Pues bien, haciendo un nuevo aporte, considero que ser mamerto no está asociado a un pensamiento político de izquierda exclusivamente o de derecha exclusivamente. Por supuesto sí tiene que ver con las ideas políticas, y bien podría definirse, como ese sujeto que no tiene ni la menor idea de lo que está diciendo, que le importa más ser un criticón antes que establecer una crítica seria, que le importa más atacar a quien no comparte sus ideas, que darse cuenta de su propia ignorancia. Sí, mamerto, es un tipejo o una tipeja, de izquierda]**o de derecha**[ que dice estupideces comprobables en el campo de lo histórico y lo político.]

[Es sabido que la palabra “mamerto” se la han endosado a la izquierda de manera crónica a lo largo de una historia mal contada sobre los buenos y los malos; casi siempre desde la visión eurocéntrica u occidentalizada que a través de los elementos informativos o propagandísticos embaucan la cultura lingüística de una sociedad. No obstante, por estos días y desde hace ya un par de años, en Colombia sucede algo… no tanto con la izquierda, sino con la forma en que ésta es configurada a través del lenguaje de los medios… medios que están en manos de absolutos mamertos de derecha.    ]

[Desde esos típicos señalamientos que se le hacen a cualquiera que abogue por una causa social más allá de los esperpentos filantrópicos que le venden a la gente con nombres de “responsabilidad social” o de “gente que quiere a la gente”, desde esos señalamientos al inconformismo crítico por considerarlo una piedra en el zapato para los mercados, la palabra “mamerto”, parece que sólo ha estado a una orilla del río, y allí radica el error comprensivo…  ]

[No digamos mentiras, mamertos de izquierda también existen, basta con reconocer esa desconfianza inmediata que se desarrolla cuando alguien te dice que Robledo es un gran representante de la izquierda, y allí comprendes que la izquierda sigue poniendo su cuota de mamertos… ¿Qué Robledo es mamerto? Por supuesto que no, sino aquellos que indican que él es de izquierda. ¿Pero y la derecha? ¡¡la derecha también tiene una lista larga y en aumento de mamertos!! Gente que habla como si fuera el rey de España pero todo lo paga a crédito, gente que dice amar el capitalismo pero detesta lo que hace, gente que denigra de las luchas sociales, pero vive pendiente exclusivamente de cuanto objeto obsoleto puede consumir para no sentirse un pobre diablo con su salario que tal vez le alcanza para unas deudas más.]

[Aumentan esos mamertos de derecha… fastidian como un mamerto de izquierda, sólo que al de izquierda todos lo señalan… ¿Y al mamerto de derecha? Bueno, aquí le indico una propuesta eufemística sobre cómo reconocer a un mamerto de derecha.]

1.  [Son aquellos que se la pasan diciendo que la izquierda es un fracaso luego de ver trinos, posteos, memes, el programa la noche de RCN, o la propaganda política de Arizmendi.   ]
2.  [Son los que en las universidades, degradan a la izquierda porque leen periódicos que les regalan en la calle o leen un par de artículos en revistas cuyo gran prestigio no les alcanza para ser un marco teórico serio de los problemas.  ]
3.  [Son aquellos que pagan todo a crédito, no saben quién es Friedman y luego dicen “la pobreza es culpa de los pobres.”]
4.  [Son aquellos que juran que el Polo es un partido de izquierda y que Raisbeck es como una especie de anarco-capitalista.]
5.  [Son aquellos que creen en lo que dice Uribe y todo su combo, sin dudar ni en una sola coma.]
6.  [Son aquellos que odiaban a Chávez por dictador y no tienen ni idea quién era Carlos Andrés Pérez y su caracazo.]
7.  [Los mismos que le dicen burro a Maduro, pero lavan los pies de Peñalosa.]
8.  [Son aquellos que cuando les dices que están equivocados, tienden a ofender o a decir “váyase a Cuba, corra a Venezuela” en vez de gritar, “vaya a la Guajira, corra al Chocó”  ]
9.  [Son aquellos que creen que ser asalariado es ser capitalista.]
10. [Son los que votaron por el “no” engañados por Juan Carlos Vélez, e insisten en que un día de guerra sale más barato que un día de paz.]
11. [Son aquellos que se reían con las gracias de Jaime Garzón y hoy son uribistas o emprendedores neoliberales anarco-capitalistas.  ]
12. [Son aquellos que dicen: “antes sí había ideales” pero no tienen ni idea qué había “antes” ni que significan “ideales”, pues el mundo para un mamerto de derecha, siempre empieza y termina con él.]
13. [Son aquellos que creen que el comunismo les va a quitar el carro que le deben al banco.  ]
14. [Son los que creen que las protestas perjudican la economía, pero no saben que es el cuatro por mil, o por qué nunca lo llaman a votar por una reforma tributaria.   ]
15. [Son los que creen que el tema de la guajira se acaba con anticonceptivos.  ]
16. [Son los que creen que un campesino es atrasado y es mejor volverlo empresario.]

[En Colombia existe hoy una casta de mamertos de derecha que rayan en lo trillado, en lo recalcitrante, en lo traqueto, supersticioso, jurásico, filibustero y místico con todas y cada una de sus declaraciones. Creo yo que el caldo de cultivo que ha sido el sustrato del triunfo para estos mamertos de derecha son dos serias complejidades.]

[La primera: hoy tal parece que en nuestra sociedad la construcción de una postura o un discurso político se hace a partir de trinos, artículos de opinión, o someros debates en los medios. Pronto, incluso en las universidades, con esa piñata de títulos, el conocimiento sucumbirá ante el estándar, y por tanto la estupidez ilustrada cavará su propia tumba, haciendo que la gente le crea más a un meme que a un sabio.]

[La segunda: la universidad en su conjunto parece alejarse de dos cosas, en primer lugar de la realidad, lo que configura mamertos de derecha y de izquierda, y en segundo lugar de la teoría, por tanto, abraza con mayor gusto la opinitis, antes que comprender a fondo una situación política. Esto sin duda, lo aprovecharon los medios y sus posiciones editoriales para dummies, con las que cultivan y cultivan mamertos de derecha.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
