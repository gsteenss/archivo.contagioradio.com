Title: Ya estamos listos para la Semana por la Paz
Date: 2019-08-15 16:42
Author: CtgAdm
Category: eventos
Tags: CINEP, paz, Pontificia Universidad Javeriana, Redepaz
Slug: listos-semana-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/afiche-semana-por-la-paz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diseño de fiche oficial ] 

Este año la **Semana por la Paz,** en   su trigésima segunda edición, que se llevará a cabo del 2 al 9 de septiembre, bajo el lema **“Soy y somos territorio de paz”.** Desde ya  los preparativos  están listos y serán más de 1.000 actividades las  que se desarrollarán  a nivel local y nacional.  (Le puede interesar también:"[Colombia se pinta de esperanza en la Semana por la Paz" ](https://archivo.contagioradio.com/colombia-se-pinta-semana-por-la-paz/))

**Son 200 movimientos y organizaciones sociales** , religiosas, comunidades, instituciones constructoras de paz  y reconciliación,  las que reúnen esfuerzos en la construcción de este evento que  tendrá  cinco elementos enmarcados en el camino del país hacia la reconciliación.

Uno de los puntos más importantes a tratar en este escenario será la persistente situación de amenaza, judicialización y asesinato a líderes y lideresas  sociales. Por otro lado también se hablará de los obstáculos políticos y legislativos para la implementación de los **Acuerdos** **de Paz y de la importancia de retomar** las conversaciones entre el gobierno Duque **y el ELN.**

Asimismo,  la **situación que se genera con las migraciones en Colombia de países como Venezuela, tendrán cabida en este encuentro**. (Le puede interesar también "[La lucha invisible de las mujeres migrantes")](https://archivo.contagioradio.com/lucha_mujeres_migrantes_8m/)

### **Prográmese para las principales actividades:** 

Oficialmente desde el  **2 de septiembre** se hará apertura a los más de **1.000 eventos**, entre ellos se destacan, **500 acciones simbólicas** y pedagógicas territoriales  (Lunes 2 de septiembre), **50 cine-foros** simultáneos titulados  “la Negociación (Martes 3 de septiembre), la entrega del **Premio Nacional en Derechos Humanos** (Miércoles 4 de septiembre) y se hará una declaración simultánea nacional de al menos 1000 territorios de paz (Jueves 5 de septiembre).

De igual forma se hará una jornada simbólica nacional por la vida y la paz (Viernes 6 de septiembre),   durante toda la Semana por la Paz la se hará **instalación de refugios humanitarios territoriales**,  al mismo tiempo que se sembrarán árboles e instalación de jardines territorios de paz (Domingo 8 de septiembre), y el cierre se unirá a la celebración del Día Nacional de los Derechos Humanos (lunes 9 de septiembre). (Le puede interesar también "[Balance de derechos humanos tras 10 días de Minga Nacional)](https://archivo.contagioradio.com/balance-de-derechos-humanos-tras-10-dias-de-minga-nacional/)

El Comité Organizador de La Semana por la paz,  esta liderado por el Secretario Nacional de Pastoral Social –Caritas Colombia, la Red Nacional de Iniciativas Ciudadanas por la Paz y Contra la Guerra **(Redepaz),** El Centro de Investigación y Educación  Popular (CINEP), el Programa por la Paz (PPP), y Pontificia Universidad Javeriana – PUJ.

Este año no hay excusa para no ser partícipe de la Semana por la Paz, ya que mediante el hashtag **\#YEstoTambiénEsPaz** y frases de la vida cotidiana; se podrán destacar aquellas personas que aportan a la construcción  de paz del país acompañado de  fotos o videos de estos individuos o de sus acciones, este contenido se puede empezar  compartir desde a través de todas las redes sociales.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
