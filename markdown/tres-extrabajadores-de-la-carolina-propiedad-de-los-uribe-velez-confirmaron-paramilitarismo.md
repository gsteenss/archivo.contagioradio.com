Title: Extrabajadores de "La Carolina" confirman nexos de los Uribe Vélez con el paramilitarismo
Date: 2018-06-12 12:54
Category: Judicial, Nacional
Tags: 12 apostoles, paramilitares, Santiago uribe
Slug: tres-extrabajadores-de-la-carolina-propiedad-de-los-uribe-velez-confirmaron-paramilitarismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/juiocio-contra-santiago-uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [06 Jun 2018] 

Tres ex trabajadores de la Hacienda “La Carolina” propiedad de Álvaro y Santiago Uribe confirmaron las acciones paramilitares en ese lugar y la relación con el reconocido paramilitar Alberto Osorio Rojas, alias “El Mono de los llanos” o “Rodrigo”, "Sabino" y "El Pelusa" con Santiago Uribe. Además relacionaron que estas personas, los administradores de la hacienda y Uribe Vélez realizaban recorridos portando armas.

En cuatro horas de grabaciones inéditas, **los testigos directos indican como “El Mono” era una persona muy cercana a Santiago Uribe y se reunían frecuentemente en la hacienda**, en particular en la casa conocida como “La Mayoría”.

Los testimonios confirman que los propietarios de la hacienda eran los hermanos Uribe Vélez y que por lo menos cada semana se realizaban reuniones a las que los trabajadores y trabajadoras no tenían acceso y que siempre se realizaban por la noche. En ellas participaba Osorio, lo cual se verificó con el reconocimiento fotográfico de alias “Rodrigo” o “Mono de los llanos” en referencia a su accionar en los Llanos de Cuivá.

Al lugar sólo accedía el estrecho círculo de confianza de Santiago Uribe. Una de las testigos quien trabajó durante 13 años en la hacienda, expresó que se le ordenaba llevar los alimentos y salir inmediatamente de allí. “Ellos se reunían unos tres días, o un día”. Esos algunos eran entre ellos el llamado "Rodrigo" y "El Pelusa".

Las declaraciones rendidas ante la Fiscalía 10 de la Corte Suprema de Justicia en el proceso 14044 contra Alberto Osorio también brindan detalles de los abusos, retenes ilegales, el control sobre la población, obligando a transeúntes a bajarse de los vehículos, incluso a desnudarse.

Uno de los testigos, un extrabajador entre los años 1995 y 1996, quien fue contratado por Santiago Uribe para labores de ganadería, señaló como cuando llegaba el hermano del principal promotor de la campaña de Iván Duque, ingresaba el llamado “Mono” o "Rodrigo" y después llegaba Santiago Uribe, todos salían armados a recorrer las fincas, incluso uno de ellos conocido como “Sabino” portaba un arma larga en esos encuentros.

En los testimonios también se estableció que a algunas de las personas que laboraron por largos periodos de tiempo en la hacienda “la Carolina” no se les pagó liquidación al momento de su salida como empleados, lo cuál generó inconformidades en algunos de ellos.

<iframe src="https://co.ivoox.com/es/player_ek_26517733_2_1.html?data=k5uik5ybd5Shhpywj5WVaZS1lJmah5yncZOhhpywj5WRaZi3jpWah5yncafmwszax9PYs4zo08bPw8_FqNDmwpC1w8jNqc_YwpC5w5CnpdPjzc7bw5CRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Adicionalmente se señala que las fincas aledañas como La Isla y otras funcionaban como si fueran la misma y que Álvaro Vásquez, propietario de uno de los predios cercanos a “La Carolina” y sindicado de ser uno de los integrantes de los “12 Apóstoles” participaba de manera constante en las reuniones en la llamada casa de la “Mayoría”.

En un tercer testimonio se señala que desde principios del año 2000, en “La Carolina” había instalada una base militar que fue retirada una vez se conoció la captura de Santiago Uribe.

Las primeras denuncias sobre las operaciones paramilitares en Yarumal, bajo el nombre de la estructura paramilitar de los "12 Apóstoles", se conocieron por una personera municipal que posteriormente fue asesinada. Conforme a valoraciones de la Fiscalía General de la Nación en la investigación penal de éste proceso son 503 las víctimas de asesinatos, desapariciones y torturas a manos de esta estructura armada en la que participó Santiago Uribe.

Hace ocho años el mayor retirado de la policía Juan Carlos Meneses, que pidió ser acogido por la Jurisdicción Especial de Paz, JEP, testimonió como la finca La Carolina era centro de planificación del grupo paramilitar de los “12 Apóstoles” en Santa Rosa de Osos comandado operacionalmente por Alberto Osorio.

A finales de mes de julio se conocerá si estas pruebas sobrevinientes serán aceptadas por el juez que conoce del proceso contra Santiago Uribe. Hasta el momento parece evidente que estas pruebas están lejos de ser cuestionadas como parte de un complot, pues se trata de testigos solicitados por la defensa del propio empresario Uribe Vélez.

El temor que asiste a familiares de la víctimas consultadas por Contagio Radio es si estos ex trabajadores tendrán garantías a su vida e integridad, dado el poder que la familia mantiene en la región.

Adicionalmente, algunos análisis señalan que es importante que el senador Uribe se retire de la actividad política y retire su respaldo al candidato Duque, pues su poder electoral estaría asegurando la impunidad jurídica tras la idea de unificación de las cortes en la propuesta del su partido, Centro Democrático de la llamada "reforma a la justicia".

###### Reciba toda la información de Contagio Radio en [[su correo]
