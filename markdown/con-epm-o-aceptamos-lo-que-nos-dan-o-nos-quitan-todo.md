Title: EPM debe frenar el sufrimiento que ha causado: Comunidades
Date: 2020-08-20 19:49
Author: AdminContagio
Category: Actualidad, DDHH
Tags: derecho a lo público, Desalojos, deudas, EPM, Hidroituango
Slug: con-epm-o-aceptamos-lo-que-nos-dan-o-nos-quitan-todo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/ituango-protesta-epm.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La **renuncia de ocho miembros de la[junta directiva](https://archivo.contagioradio.com/ituango-en-una-encrucijada-de-pobreza-y-violencia/) de EPM el 11 de agosto, el nombramiento de tres nuevos miembros por el alcalde de Medellín**, Daniel Quintero, este 18 de agosto y las múltiples denuncias por incumplimientos, amenazas y revictimización a los habitantes de Ituango y sus alrededores son hoy la punta del iceberg de esta empresa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un sin de puntos que debe responder EPM, una de las empresas más importantes del país, y que **ha generado controversias dentro de los grupos políticos y empresariales antioqueños, e incluso nacionales que ponen el ojo ante el impacto social, ambiental y económico** generado directa e indirectamente a todo el departamento.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "EPM se aprovecha de la situación de las personas que están aguantando hambre"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mientras las noticias sobre la junta directiva de EPM llenaban todos los encabezados, este 13 de agosto miembros de la comunidad afectada por el proyecto Hidroituango [informaron](https://riosvivoscolombia.org/presion-irregular-e-ilegal-de-epm-a-victimas-de-hidroituango/)que **personal de esta empresa se ha encargado de estigmatizar a los abogados que representan los intereses de los afectados** en los procesos de reparación colectiva.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señalando que, ***"es culpa de los abogados que se haya suspendido el acceso a las ayudas humanitarias"***, información que según el [Movimiento Ríos Vivos](https://riosvivoscolombia.org/no-a-hidroituango/), es completamente falsa y tendenciosa; aclarando que las ayudas humanitarias son absolutamente independientes a los derechos que se exigen de reparación integral y de Justicia que reclaman las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**William de Jesús Gutiérrez, secretario de la vereda El Astillero en el corregimiento de Puerto Valdivia** y coordinador de Ríos Vivos Antioquia, señaló que en este tiempo de [pandemia](https://archivo.contagioradio.com/en-gobierno-duque-han-ocurrido-1-200-violaciones-de-ddhh-contra-el-pueblo-awa-en-narino/)una de las situaciones que más le preocupan como vocero de las comunidades y defensor de Derechos Humanos, es que **no les están ofreciendo garantías de retorno.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmando que, ***"la forma de negociar con las comunidades es difícil, porque nos ponen cláusulas en donde tenemos que elegir entre aguantar hambre, o recibir lo que a ellos bien les parezca"***, esto frente a las diferentes cláusulas presentadas en los contratos que desconocen la realidad y las necesidades que enfrentan hoy las más de 15 municipios y 7 corregimientos afectados por el proyecto de Hidroituango.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Me parece que estas negociaciones son algo arbitrarias, ¿cómo es posible que lo pongan uno a aguantar hambre como un acto de presión para que firme"*
>
> <cite>William de Jesús Gutiérrez |Habitante de Puerto Valdiv****ia****</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Producto de estas acciones hacia las comunidades afectadas según William, **muchos han tenido que renunciar a sus abogados para poder tener al menos un mínimo con que sostener a sus familias**, situación que se da al mismo tiempo que EPM divulga noticias de que todo está bien y que las comunidades están recibiendo subsidios e incluso recuperando sus viviendas, en un retorno territorial en dónde no hay garantías de habitabilidad básicas como un hospital o un centro educativo.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Es inimaginable pensar que una de las cláusulas dice que renunciamos *al pasado presente y futuro en un contexto donde aún hay personas que lloran al recordar lo vivido"*
>
> <cite>William de Jesús Gutiérrez |Habitante de Puerto Valdiv****ia****</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El defensor, finalmente señaló que es alarmante ver cómo las instituciones silencian ante las afectaciones ambientales que recorren gran parte del caudal del[Río Cauca](https://archivo.contagioradio.com/campesinos-de-suarez-cauca-reclaman-predios-a-la-multinacional-smurfit-kappa/) producto del impacto generado por Hidroituango, *"nosotros pedimos que la institucionalidad **no se haga de ojos ciegos ante la situación que vive nuestro amigo, el Río Cauca quién está muriendo, y junto a él morimos quienes por años hemos vivido a su lado".***

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Si no se protege EPM que es público, entonces que se protege?

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Uno de los retos que tiene la defensa de lo público es que en las empresas qué hacen parte de este sector no respondan a los intereses del Gobierno de turno**, sino que apelen más a una política pública qué beneficia al final de cuentas a quienes están siempre afectados a pesar de los cambios que se den en el Gobierno, y eso según **Fredy agudelo abogado especialista en gerencia de servicios** es lo que permitirá proteger a EPM a pesar de estar aportas de una quiebra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto según el abogado es gran parte de la responsabilidad que tiene la junta directiva, "*el papel es que una vez el gerente iniciar las acciones legales que parten por una conciliación como requisito legal, la junta evalúe si ese acuerdo conciliatorio le favorece Epm o no",* pero según el abogado ocurre lo contrario, ***"el gerente tiene que pedirle permiso a la junta para tomar una acción legal en pro de salvaguardar los intereses patrimoniales de la empresa que son iguales a los intereses de la ciudadanía".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez **Fernando Quijano, presidente de la Corporación para la paz y el desarrollo social** señaló que mientras EPM muestra una cara amable externamente, al interior genera un desastre social, público y económico con un megaproyecto como Hidroituango, develando así en donde está los intereses particulares moviendo lo público para su beneficio.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"La lucha hoy es por democratizar las decisiones de EPM en junta qué le encabeza"*
>
> <cite>**Fredy agudelo, abogado especialista en gerencia de servicios**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Conclusión de esto según los analistas, es la deuda que hoy afecta a EPM, la cual está en u**n 57% de endeudamiento y se basa principalmente en el modelo de administración**, una situación económica que pone en riesgo el patrimonio de la empresa y junto a ella el de Antioquia, ***"en una situación bastante preocupante y que aún tiende a empeorar"***, concluyó Quijano.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Entonces, ¿Quiénes deberían integrar la junta de EPM?

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Luz Marina Munera, exconcejala de Medellín**, y administradora pública, señaló que para evitar una centralización de poder en una de las empresas más importantes de Antioquia es necesario que este compuesta por; **un representante de la academia, otro de organizaciones de usuarios de servicios públicos, una ONG y finalmente un representante de los grupos económicos**, garantizando así una junta directiva que sea capaz de sentarse a pensar en la ética de lo público y no en intereses particulares.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Necesitamos hombres y mujeres que se sienten a pensar en Antioquia, en las comunidades y en el montón de víctimas al cual deben responder producto del mal manejo de Hidroituango".*
>
> <cite>**Luz Marina Munera, exconcejala de Medellín**</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "La principal discusión hoy debe ser el bienestar de los humildes"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según **Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos,** hoy las personas se encuentran sufriendo por culpa del proyecto hidroeléctrico hidroituango.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ISAZULETA/status/1295902159494578181","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ISAZULETA/status/1295902159494578181

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:quote -->

> *"¿De quién es hidroituango?, Si deben más de del 50% como nos van a decir que es público y aún más en manos de quién recae la responsabilidad de responder"*
>
> <cite>**Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Agregando que la hidroeléctrico no es de EPM debido a que más del 60% de este proyecto corresponde a la deuda pública, afirmó que **EPM no es de los antioqueños,** puesto que no ha traído desarrollo en las comunidades de las zonas rurales, esto *"a pesar de que saca todo de Antioquia, poco o nada le retribuido a departamento"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Zuleta también agregó que **en materia de defensa de lo público lo único que le pertenece a la comunidad es el Río Cauca**, el cual beneficia a más de 12 millones de personas, y hoy muchos de estos miles quedaron sin nada por culpa del gran impacto ambiental que hay en el río y lo que representaba para las comunidades en el tejido social.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué le espera entonces a las comunidades afectadas?

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RiosVivosColom/status/1296413665038610432","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RiosVivosColom/status/1296413665038610432

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Finalmente Zuleta señaló que la administración actual de EPM, tenía cita con las comunidades el 8 de enero del 2020, compromiso al cual nunca llegaron, ***"nos dejaron plantados en el juzgado, entonces no es válido que hoy vengan a decir que la nueva junta es de los buenos y la de antes era de los malos".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y argumentó que está junta tiene claridad sobre las medidas cautelares y lo que tenía que responder ante la vida, la integridad física y el mínimo vital, algo que ignoraron mientras que las comunidades siguen aguantando hambre. ([Lo que hay detrás de los exmiembros de la Junta Directiva de EPM](https://archivo.contagioradio.com/lo-que-hay-detras-de-los-exmiembros-de-la-junta-directiva-de-epm/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"No se puede lavar las manos diciendo quiénes son los buenos y los malos cuando nadie le ha respondido ni a la comunidad ni a la justicia".*
>
> <cite>**Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y concluyó diciendo que el panorama ante el cumplimiento de las medidas cautelares y la reparación a las comunidades, se ve opacada por **una lucha de poderes en donde un grupo empresarial se cree superior a la justicia** colombiana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Vea el desarrollo completo de este programa en la emisión de Otra Mirada del 18 de julio:*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D814216892316056%26extid%3DW1vu887n8CXcwIOb&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
