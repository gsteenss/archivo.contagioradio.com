Title: Fernando Londoño se retracta de afirmaciones sobre Colectivo José Alvear Restrepo
Date: 2017-02-10 15:46
Category: Judicial, Nacional
Tags: colectivo de abogados, Fernando Londoño
Slug: fernando-londono-se-retracta-de-afirmaciones-sobre-colectivo-jose-alvear-restrepo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/fernando-londoño.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  edh] 

###### [10 Feb 2017]

Durante cerca de 7 minutos el político y **ex ministro del Interior Fernando Londoño se retractó de afirmaciones hechas durante el programa “La Hora de la Verdad”** en contra del **Colectivo de Abogados José Alvear Restrepo** en los que vinculó a la organización de Derechos Humanos con la guerrilla de las FARC y los responsabilizó por las falsas víctimas de la masacre de Mapiripan.

En una lectura de 4 puntos Londoño se rectificó de las acusaciones contra el colectivo por supuestas relaciones permanentes de esa organización con las FARC, o supuestos antecedentes de sus integrantes con esa organización guerrillera. "para nada me consta" ni tampoco apoyo de las FARC a esa institución, señaló.

También precisó que el Ccajar no actuó de mala fe en el proceso por las falsas víctimas de la masacre de Mapiripan y resaltó que las propias personas señaladas de dar testimonio falso aceptaron su responsabilidad y ofrecieron disculpas públicas al Estado, al CCAJAR y a las demás víctimas. Además reconoció el comportamiento ético del Colectivo al devolver los dineros ganados con el litigio y la representación de esas víctimas.

Además aclaró que no conoce ninguna condena penal o disciplinaria contra los integrantes del Ccajar y que la representación de victimas del Estado no los convierte en enemigos de las FFMM además señaló que no conoce una situación en que se hayan pagado falsos testigos para hacer acusar a militares.

Por otra parte reconoció que la labor de representación de las víctimas de crímenes del Estado y de crímenes de guerra es amparada constitucionalmente y reafirmada en el código nacional del abogado y que en ningún caso la representación de víctimas del Estado los convierte en enemigos del Estado o de las FFMM.

El ex ministro aseguró que las afirmaciones realizadas son en cumplimiento de lo exigido por el propio CCAJAR y reconoció la legitimidad de su labor. Londoño aseguró también que nunca volverá a mencionar el nombre del Colectivo de Abogados "para su tranquilidad y la nuestra".
