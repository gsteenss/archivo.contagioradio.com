Title: ¿El uribismo es fariano?
Date: 2015-11-26 06:00
Category: Javier Ruiz, Opinion
Tags: Alvaro Uribe, Javier Ruiz, María Fernanda Cabal, proceso de paz, uribismo
Slug: el-uribismo-es-fariano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/centro-democratico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

#### **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/)- [@ContraGodarria ](http://contragodarria/)** 

###### [26 Nov 2015 ] 

En estos últimos años he sostenido una tesis que se puede considerar macabra y es la siguiente: El uribismo necesita de la violencia, de las bombas, de las muertes y del derramamiento de sangre para poder hablar y expresarse.

[Como se ha podido ver en los diferentes medios de comunicación y en las redes sociales el uribismo en pleno se opone al proceso de paz porque, según ellos, les daría impunidad total a los miembros de las FARC y entregaría el país al terrorismo. Ven con malos ojos los avances en la mesa de negociación de La Habana e intentan enrarecer el ambiente con comentarios ofensivos y desinformadores para ganar adeptos e ir en contra de la paz que es su verdadero objetivo.]

[Cada vez que las negociaciones de paz avanzan es un golpe duro al uribismo y estos no lo han sabido asimilar. Al ver que en el país han bajado las hostilidades de las FARC y han disminuido las acciones de guerra los uribistas se han quedado sin argumentos para convencer a los colombianos de la inconveniencia de buscar una paz definitiva al largo conflicto interno colombiano.]

[El uribismo extraña los secuestros, las bombas, las extorsiones y las muertes de las FARC. Tanto los extraña que al ver que el grupo guerrillero no ha vuelto hacer de las suyas tienen que frivolizar, hacer burlas y realizar comparaciones absurdas como lo hizo la representante a la Cámara por el uribismo María Fernanda Cabal sobre el terrible atentado terrorista sucedido en París días atrás.]

[María Fernanda Cabal es una prueba clara que el uribismo necesita de la violencia y del derramamiento de sangre para poder opinar y ella, que posa de “irreverente” de tener “sentido del humor”, ironizaba sobre lo que paso en la capital de Francia comparándolo sobre lo que pasa en La Habana. Ella, como muchos simpatizantes y militantes del uribismo, ve con malos ojos la paz y pone su grano de arena para que el país siga en guerra a través del insulto, la infamia y la calumnia. Sin mencionar a profundidad otras acciones cuestionables de la representante como la defensa de los expropiadores de tierras, su apoyo a militares violadores de los DD.HH, etc.]

[Teniendo en cuenta lo mencionado anteriormente, el uribista se está quedando sin argumentos para atacar la paz y por eso tienen que recurrir a lo que sucedió en París para confundir comparando el terrorismo islámico con el terrorismo de las FARC diciendo que si en Paris estaban los negociadores de paz por parte del gobierno las muertes y las bombas del grupo terrorista Estado Islámico “quedarían impunes y que los terroristas tendrían curules en el parlamento francés”.]

[Es de una bajeza lo que hizo la representante Cabal que obviamente fue aplaudida por el uribismo en pleno por su “brillante” opinión. Después de eso vinieron más comentarios del mismo estilo cuyos autores también son conocidos uribistas como Alfredo Rangel y Ernesto Yamhure donde destilaron su odio hacia la paz de Colombia en base al atentado terrorista en París.]

[La tesis macabra es verídica: El uribismo necesita de las FARC y que este mismo grupo vuelva a sus andanzas violentas y criminales para poder opinar, poder convencer con su discurso guerrerista para ganar votos y para buscar que no se logre la paz. Sin guerra y sin sangre el uribismo es nada, nada absoluta.  
]

[Ahora solo queda decir dos cosas: Ojalá se llegue a la paz por el bien del país y, lo segundo, no sobra hacer esta pregunta: ¿El uribismo es fariano?]
