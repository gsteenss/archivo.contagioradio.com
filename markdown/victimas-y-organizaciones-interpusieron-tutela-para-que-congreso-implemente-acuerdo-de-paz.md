Title: Víctimas interponen tutela contra el Congreso para implementar el Acuerdo de Paz
Date: 2017-11-15 13:36
Category: Nacional, Paz
Tags: acuerdo de paz, Congreso de la República, JEP, Organizaciones sociales, víctimas del conflicto armado
Slug: victimas-y-organizaciones-interpusieron-tutela-para-que-congreso-implemente-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/DOnqVVdWAAAlr5G-e1510770947262.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ojo a la Paz] 

###### [15 Nov 2017] 

60 líderes y lideresas, algunas víctimas del conflicto armado y 59 organizaciones sociales **interpusieron una acción de tutela** en contra del Congreso de la República y el presidente Juan Manuel Santos con el fin de garantizar la obligación del legislativo frente a la implementación normativa del Acuerdo de Paz. Argumentaron que el derecho a la paz de los ciudadanos se debe respetar y reprocharon la actitud dilatoria que ha tenido el Congreso para avanzar con la implementación.

La tutela la realizaron teniendo en cuenta que está por terminar el plazo o la vigencia del procedimiento legislativo de vía rápida (Fast Track) y **“sólo se ha aprobado un 18% de las leyes necesarias para la implementación del Acuerdo de Paz”**. Además, indicaron que “las dilaciones, maniobra y actuaciones negligentes de un sector del Congreso, han conllevado a un estancamiento del proceso (…) constituyendo una amenaza para la consolidación de la paz”.

### **Congreso de la República no está respetando los derechos de las víctimas** 

De acuerdo con Fernando Vargas, coordinador de incidencia de la Consultoría para los Derechos Humanos y el Desplazamiento (CODHES), “la actitud de buena parte de los miembros del Congreso, **está afectando de manera radical** el goce efectivo del derecho de los colombianos a vivir en paz”. (Le puede interesar: ["Los acuerdos de paz cojean en el Congreso"](https://archivo.contagioradio.com/los-acuerdos-de-paz-cojean-en-el-congreso-de-la-republica/))

Además, consideran como indispensable la **consecución de la paz como garantía para el pleno ejercicio de los demás derechos**. Por esto, han insistido en que “hay una necesidad de satisfacer el derecho de las víctimas a la verdad, la justicia y la reparación”. Con esto en mente, utilizaron el mecanismo de tutela para exigirle al Congreso que “respete la dignidad de las víctimas y por esa vía contribuya a alcanzar la paz”.

### **Congresistas deben asumir su responsabilidad y no dilatar más la implementación** 

Teniendo en cuenta que **sólo quedan 20 días para que se termine la vigencia del Fast Track**, Vargas indicó que las organizaciones sociales han solicitado la implementación del acuerdo en el marco constitucional legal que se ha definido. Para él “es una completa vergüenza que el Congreso de la República no responda a las necesidades de transición democrática del país”. (Le puede interesar: ["Estado de conmoción podría salvar acuerdo de paz"](https://archivo.contagioradio.com/48477/))

Por esto, le hizo un llamado a los congresistas a que **asuman su responsabilidad y realicen la debida diligencia** teniendo en cuenta el deber que tienen de cumplir con los plazos del Fast Track. Igualmente, indicó que la falta de quorum y las dilaciones no pueden ser una excusa “para prolongar de manera indefinida o no racional los plazos que ha establecido el propio acuerdo”.

Finalmente, Fernando Vargas hizo énfasis en que la tutela interpuesta también va encaminada a recordarle a la ciudadanía que "la paz es un compromiso no solamente del Estado o de quienes firmaron un acuerdo, sino que es también **una responsabilidad ciudadana**”. Con esto en mente, hicieron un llamado para que la sociedad asuma la paz como un asunto propio y de compromiso para que las personas puedan ejercer sus derechos.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
