Title: "Postura de la iglesia católica frente al género es contraria a las enseñanzas de Jesús"
Date: 2016-08-15 11:06
Category: LGBTI, Nacional
Tags: catolicas por el derecho a decidir, discriminación sexual colombia, revisión manuales colombia
Slug: postura-de-la-iglesia-catolica-frente-al-genero-es-contraria-a-las-ensenanzas-de-jesus
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/LGBTI.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PanamPost ] 

###### [15 Ago 2016 ] 

Independientemente de la religión, el mundo y la sociedad se han construido con hombres y mujeres, y posturas de rechazo y discriminación como las que está promoviendo la iglesia católica son incorrectas teológicamente hablando, porque **si hay algo que hemos aprendido desde el planteamiento de Jesús es el reconocimiento de la igualdad**, asegura Sandra Masso, coordinadora de la organización 'Católicas por el Derecho a Decidir'.

Masso agrega que la postura que está asumiendo la iglesia católica contra el tema del género es equivocada, manipulada y contraría a lo que ha planteado incluso el Papa Francisco, una lectura teológica muy equivocada y que se refuerza con la actitud asumida por el presidente Juan Manuel Santos, quien demuestra la **falta de laicidad del Estado colombiano**, afectando el principio de la garantía de derechos para toda la población.

Para la organización es inaceptable que los fundamentalismos quieran imponer su visión de que la diversidad y su aceptación es un delito y que el Estado les siga el juego, como está ocurriendo actualmente, pues **satanizar y estigmatizar la perspectiva de género implica desconocer los derechos y conquistas de las mujeres**, que han permitido abrir un espectro de inclusión para lograr relaciones más justas y equitativas entre hombres y mujeres.

"Es lamentable que se esté poniendo el tema de género como si fuera un delito, como si fuera un pecado o algo terrible, es indignante dónde se está poniendo el debate académica, política y teológicamente", pues se desconoce que el género es un concepto sociológico y una [[construcción cultural](https://archivo.contagioradio.com/lo-que-hay-es-un-miedo-profundo-a-pensarse-la-sociedad-mas-alla-de-lo-establecido/)] para las relaciones entre hombres y mujeres **desde el principio de igualdad, no discriminación y reconocimiento de derechos**, concluye Masso.

<iframe src="http://co.ivoox.com/es/player_ej_12564267_2_1.html?data=kpeimJmWepihhpywj5WcaZS1lp6ah5yncZOhhpywj5WRaZi3jpWah5yncYzHwtPR1MaPkcLn1NSSlKiPaZOrpMbhh6iXaaOnzc7Qw9iPtNDmjMrZjanJtsbXydSYw5CoqcTdxc7fh5ebco6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio}
