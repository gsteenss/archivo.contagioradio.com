Title: La memoria es un derecho ciudadano y el Estado debe garantizarlo
Date: 2016-06-02 14:53
Category: eventos, Nacional
Tags: Audiencia pública sobre el deber de la memoria, Centro Nacional de Memoria, El deber de la Memoria del Estado y el Derecho de Memoria de los Pueblos', Red de Lugares de la Memoria
Slug: la-memoria-es-un-derecho-ciudadano-y-el-estado-debe-garantizarlo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Derecho-a-la-memoria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Casa de La Mujer ] 

###### [2 Junio 2016]

Este jueves en el Congreso de la República se realiza la Audiencia 'El deber de la Memoria del Estado y el Derecho de Memoria de los Pueblos', citada por la 'Red Colombiana de Lugares de Memoria', con el objetivo de exigir al gobierno la reglamentación de las leyes establecidas para garantizar el derecho a la memoria, pues es su **obligación apoyar, acompañar e impulsar las iniciativas ciudadanas** en la materia.

De acuerdo con Alejandra Gaviria, del Centro de Memoria, Paz y Reconciliación, se trata de "veinte iniciativas ciudadanas que le han hecho frente al conflicto, al miedo, a la impunidad para generar **procesos con la apuesta de contar lo que nos ha pasado durante el conflicto**", por lo que insisten en la necesidad de que el gobierno colombiano asuma su obligación y brinde las garantías para el fortalecimiento de estas experiencias.

Gaviria asegura que la memoria es la versión del pueblo colombiano de su experiencia del conflicto, como **contrapartida a la historia oficial que no le ha dado voz a la víctimas**, "este país se ha construido sobre una historia en la que a las víctimas se les ha dicho que lo merecían, que algo habrían hecho, además sin garantizarles su derecho a la verdad, a la justicia y a la reparación".

Uno de los principales retos en el posconflicto  es "no seguir contando la historia amañada a los gobernantes de turno", sino consolidar proyectos para que la ciudadanía se quien cuente la historia, teniendo en cuenta que "Colombia hace memoria no sólo de los asesinatos y las violaciones de hace 30 años, sino que hace memoria hoy del sindicalista, del reclamante de tierras que fue asesinado ayer y **eso es un reto, hacer memoria sobre el presente para transformarlo**", concluye Gaviria.

<iframe src="http://co.ivoox.com/es/player_ej_11759906_2_1.html?data=kpakl56ddJehhpywj5WcaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs%2FO0MnWpYy7wtvW1M7Fb46fpLO6qpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 

###### [ ] 
