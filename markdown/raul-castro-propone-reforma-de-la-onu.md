Title: Raúl Castro propone reforma de la ONU
Date: 2015-09-28 21:29
Category: El mundo, Política
Tags: Cuba, discurso raul castro, Naciones Unidas, periodo 70 naciones unidas, Raul Castro, Raul Castro en la ONU
Slug: raul-castro-propone-reforma-de-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/raul-castro-discurso-onu.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: TVMundo] 

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/raul-castro.mp3"\]\[/audio\]

##### [**Discurso del Presidente Raul Castro en la ONU**] 

###### [28 de Sep 2015 ] 

**El discurso del Presidente cubano Raul Castro en la Asamblea General de Naciones Unidas** al celebrarse el 70 aniversario de este organismo internacional estuvo cargado de una critica general a la forma como gobiernos dominantes  han venido utilizando su poder político y militar para desestabilizar naciones.

Entre los temas abordados por el mandatario cubano fue constante  la denuncia contra las naciones que han intervenido en asuntos propios de los países, intervenciones que han sido seguidas de militarización del ciberespacio y las comunicaciones internas. Así mismo retomo apartes de la Carta de las Naciones Unidas que aboga a los derechos fundamentales y expreso; "A pesar que la carta nos llama a reafirmar la fe en los derechos fundamentales del hombre en la dignidad y en el valor de las personas humanas, el disfrute de los derechos humanos continua siendo una utopía para millones de personas, se niega a la humanidad el derecho de vivir en paz y su derecho al desarrollo; es en la pobreza  y en la desigualdad que deben buscarse los problemas de los conflictos generados por el colonialismo y el despojo de las naciones autóctonas" enfatizo el presidente cubano e hizo un llamado a atender y a elevar el nivel de vida de los pueblos.

#### [**El cambio climático **] 

Sobre el tema llamo a los estados a asumir sus responsabilidades comunes pero diferenciando el papel que han desempeñado cada uno, puesto que las responsabilidades no se pueden equiparar,  igualmente señalo el despilfarro que han hecho naciones contra los recursos naturales que han ocasionado como mayores víctimas a pequeños países.

#### [**Solidaridad con países **] 

En su primer discurso en las Naciones Unidas el mandatario expreso su solidaridad con el continente africano, y con los países de América Latina, e invito al respeto que tiene cada pueblo por elegir su sistema político, económico, social y cultural. Entre los países a los que expreso su apoyo está:  Ecuador, Brasil, Argentina y Venezuela, países que han sufrido campañas desestabilizadoras.

Igualmente hizo un llamado a la Unión Europea a atender la crisis migratoria que según Raul Castro son consecuencia de la política que ha asumido esta comunidad internacional.

#### [**Cuba y EE.UU**] 

Por último Castro abordo el tema de las relaciones entre los dos países. "Ahora se inicia un largo  y complejo  proceso hacia la nacionalización de las relaciones que se alcanzarán  cuando se ponga fin al bloqueo  económico, comercial, y financiero, se devuelva a Cuba el territorio ocupado ilegalmente en la base naval de Guatanamo, cesen las transmisiones radiales y televisivas y los programas de subversión y desestabilización contra la isla y se compense a nuestra  pueblo por los daños humanos  y  económicos que aún sufre"  señaló el mandatario y finalizó haciendo un llamado a los 188 gobierno que componen el organismo internacional de las Naciones Unidas para que  se defienda al organismo del  unilaterismo y para que se realice una  profunda reforma que conlleve a la institución a acercarse verdaderamente a los pueblos y crear un organismo más justo, equitativo y digno con el ser humano.
