Title: Arroceros rechazan aumento del 200% en costos del agua
Date: 2016-09-28 15:44
Category: Movilización, Nacional
Tags: agropecuario, Movilizaciones, Tolima
Slug: arroceros-rechazan-aumento-del-200-en-costos-por-uso-del-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/arroceros.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Dinero 

###### 28 Sep 2016 

Ayer se llevó a cabo la marcha Regional por la defensa del sector agropecuario en Ibagué, Tolima, convocada por **500 arroceros, que rechazan el aumento de la tasa por el uso del agua que incrementó en un 200**%, mientras que aseguran que el Ministerio de Ambiente no le cobra a las mineras por el uso excesivo de líquido vital.

De acuerdo con el líder de los arroceros, Juan Pablo Rodríguez, en el año 2004 el Ministerio de Ambiente sacó la Resolución 155 donde se establecen los cobros para el uso del agua con una tasa fija, sin embargo, debido a que **se añadió una nueva fórmula**,  el costo del agua aumentó entre el 2015 y 2016 un 200%.

“Estamos ad portas de que se acabe la salvaguarda que teníamos. Con este cobro dejamos de ser competitivos, vamos a tener que dejar de generar empleo", dice Rodríguez, quien agrega que esta medida afecta a **más de 15 mil familias que subsisten del cultivo de arroz,** es decir, que se impacta negativamente a 22 municipios del departamento del Tolimam “Sería una ofensa al campo nacional”, dice el líder de los arroceros.

Durante la movilización del martes, el secretario de gobierno estuvo en la reunión con delgados del Ministerio de Ambiente y Agricultura, este último evidenció su preocupación pues es consiente de que esa alza afecta la competitividad de los arroceros colombianos,** teniendo en cuenta que los productores internacionales están subsidiados.**

Aunque aún no se le ha dado una respuesta clara a los arroceros, se espera que este jueves y viernes se abra una mesa técnica con organizaciones del sector y con el Ministerio de Ambiente y Agricultura, donde se propondrá la modificación de la fórmula que generó el incremento en el costo del agua.

<iframe id="audio_13098529" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13098529_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
