Title: "Son 30 mil" la consigna en el día de la memoria Argentina
Date: 2017-03-24 16:38
Category: DDHH, El mundo
Tags: Argentina, desparecidos, memoria, plaza de mayo
Slug: argentina-desparecidos-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/abuelas-plaza-de-mayo-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Abuelas Difusión 

###### 24 Mar 2017 

Miles de argentinos salieron a las calles para conmemorar a las víctimas del último golpe cívico militar vivido en ese país hace 41 años. Con pancartas y consignas se vivió un año más del **Día Nacional de la Memoria por la Verdad y la Justicia**, una fecha establecida por el Congreso Nacional desde 2002.

A pesar de la intención del gobierno de cambiar para el lunes el día de la conmemoración, que corresponde a un feriado en Argentina, organizaciones como las **Abuelas de Plaza de Mayo, la Asociación Madres de Plaza de Mayo, HIJOS y otras agrupaciones de familiares de víctimas y desaparecidos por causas políticas**, marcharon hacia la plaza de mayo en la ciudad de Buenos Aires.

De manera paralela el Frente Izquierda y otras organizaciones agrupadas en el Encuentro, Memoria, Verdad y Justicia (EMVJ), se encontraron en la plaza del Congreso para llegar a la Plaza de mayo bajo la consigna "**Son 30.000, fue un genocidio, contra el gobierno de Macri que niega la historia para aplicar el ajuste con represión**".

En la Plaza de Mayo, se escucharon las intervenciones de los participantes en un acto frente a Casa Rosada, donde **el principal destinatario de las críticas fue el Gobierno**, encabezado por Mauricio Macri, y algunos miembros de su gabinete que en el último año **pusieron en duda la cifra de los 30 mil desaparecidos de la dictadura**.

Otras voces se elevaron por la **solicitud de libertad para la líder barrial de Tupac Amaru y Diputada del Parlasur Milagro Sala**, quien se encuentra encarcelada desde diciembre de 2015, en el reclamo colectivo de una "Argentina para todos". Le puede interesar: [Tribunal condena por 3 años a Milagro Sala](https://archivo.contagioradio.com/condena-milagro-sala-argentina/).

\

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
