Title: Las mujeres de Kurdistán, más allá de las armas
Date: 2017-05-10 12:19
Category: Cultura, El mundo
Tags: Kurdistán, mujeres
Slug: mujeres_kurdistan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/guerrilleras-kurdas-Kurdistán-mujeres-e1494436259672.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Nora Miralles 

###### [10 May 2017] 

Aunque parecen lugares lejanos, la lucha del pueblo Kurdo y de los movimientos sociales en América Latina tienen similitudes. Por eso este martes se presentó **el libro 'Civilización Capitalista: la era de los dioses sin máscara y con reyes desnudos',** en la sede de la USO en Bogotá, con el objetivo de compartir experiencias de los movimientos sociales en Kurdistán y Colombia.

Se trata de un libro escrito por **Abdullah Öcalan, líder del movimiento revolucionario kurdo**. La obra pertenece al segundo tomo del 'Manifiesto por la Civilización Democrática', donde se refleja de forma extensa y completa su propuesta política, el nuevo paradigma que adoptó el movimiento tras el abandono de la vía marxista-leninista: el Confederalismo Democrático.

De acuerdo con Melike Yasar, integrante del Congreso Nacional de Kurdistán, el líder de las milicias Kurdas encarcelado muestra en su libro la nueva ideología que está tomando el movimiento social en esa región, pero especialmente el de las mujeres. Y es que la participación de las mujeres no solo  se ha dado a través de las armas, sino que se trata de **una lucha al interior del pueblo desde una mirada antisexista, ecológica y democrática.**

“Como movimiento de mujeres de Kurdistán nos molesta el imaginario en los medios de las mujeres con armas. Esa no es la única lucha de las mujeres. **El 90% de nosotras luchamos dentro de la sociedad desde una mirada femenina**”, explica Yasar, quien agrega que la crítica del libro se centra en la primera clase oprimida, es decir, las mujeres, antes que los trabajadores.

Para la integrante del Congreso Nacional de Kurdistán, la participación de las mujeres en las luchas sociales es esencial para lograr verdaderas transformaciones. Y eso se ha evidenciado tanto en Kurdistán como en América Latina y propiamente, el caso colombiano con la influencia del movimiento de mujeres en el proceso de paz.

Durante los próximos días en el marco de la presentación de este libro, se mostrará cómo las mujeres Kurdas fueron casa por casa a formar a la sociedad, logrando que su movimiento contara con una asamblea propia y autónoma. Un camino, que han recorrido siguiendo el ejemplo en América Latina. Yasar, concluye asegurando que **es necesario este tipo de intercambios de experiencias pues la lucha de las mujeres es algo universal.**

<iframe id="audio_18613481" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18613481_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
