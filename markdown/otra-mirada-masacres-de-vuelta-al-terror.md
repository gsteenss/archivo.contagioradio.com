Title: Otra Mirada: Masacres, ¡De vuelta al terror!
Date: 2020-08-25 22:17
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: Asesinatos, Regreso de las masacres
Slug: otra-mirada-masacres-de-vuelta-al-terror
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/victimas-guerra-desaparecidosefe-1509242035.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ambitojuridico.com

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Indepaz, desde la firma del acuerdo de paz hasta agosto 21 del 2020, 1000 líderes y defensores de derechos humanos han sido asesinados y en lo corrido del año se han perpetrado 45 masacres. Con estas cifras, la sociedad colombiana ha manifestado su miedo y terror frente al posible retorno de una Colombia con violencia. (Le puede interesar: [Las razones que explican el regreso de las masacres a Colombia](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa de Otra Mirada, sobre las masacres en Colombia, participaron Soraya Gutiérrez, abogada y vicepresidenta de Cajar, Alberto Yepes, coordinador del observatorio de DD.HH. y DIH de Coeuropa, Aída Avella, senadora y presidenta Unión Patriótica, Lourdes Castro, coordinadora del programa Somos Defensores y Luis Olave coordinador de Afro Red Latinoamericana y el Caribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el conversatorio cada invitado compartió cómo ve la situación en la que está actualmente el país y qué opinión le merece la sensación de que en vez de avanzar, se esté viendo un retroceso. (Le puede interesar: [El terror de las masacres no puede frenar la movilización social en Colombia](https://archivo.contagioradio.com/el-terror-de-las-masacres-no-puede-frenar-la-movilizacion-social-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Teniendo en cuenta que hay una estructura militar tan fuerte en el país, comentan que las fuerzas militares podrían no estar trabajando en función de las comunidades y de proteger el derecho a la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, explican cuáles son esos elementos que han facilitado que la crisis humanitaria se incremente y expresan que las alternativas son la movilización social, pensar mejor en quienes se está votando e implementar el acuerdo de paz lo más pronto posible.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 19 de agosto: Otra Mirada: [Asentamientos ilegales, una expresión de lucha urbana.](https://www.facebook.com/contagioradio/videos/798769240953782)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el análisis completo [Haga click aquí](https://www.facebook.com/contagioradio/videos/324202188725648)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
