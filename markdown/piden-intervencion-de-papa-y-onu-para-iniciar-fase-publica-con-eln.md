Title: Piden intervención de Papa y  Banki-moon para iniciar fase pública con ELN
Date: 2016-09-15 12:37
Category: Nacional, Paz
Tags: ELN, FARC, guerrilla, paro armado, Paz Completa, proceso de paz
Slug: piden-intervencion-de-papa-y-onu-para-iniciar-fase-publica-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/papa-y-onu-e1473961217256.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ADN Digital 

###### [15 Sep 2016] 

Desde la campaña "Por una Paz Completa", organizaciones sociales y de derechos humanos, académicos y figuras políticas, enviaron una carta al **Papa Francisco y al secretario general de la ONU, Ban Ki-Moon**, solicitando a la comunidad internacional una intervención para que se impulse el inicio de la fase pública de diálogos entre el Gobierno colombiano y la guerrilla del ELN.

“Es posible y urgente que ELN y Gobierno se sienten a negociar, pues no habrá paz completa si no se negocia con ellos”, asegura Luis Eduardo Celis, analista político y uno de los firmantes.

Frente al actual panorama de violencia que se registra en los últimos días con el paro armado de la guerrilla del ELN, desde la campaña se insta tanto a la guerrilla como al gobierno a dar muestras de paz, en ese sentido, aseguran que **un cese bilateral, sería el mejor escenario para negociar.**

Teniendo en cuenta el papel que han jugado el sumo pontífice y el secretario general de la ONU en los diálogos de paz con las FARC, ahora se pide que estos mismos actores intervengan para que se empiecen los diálogos con el ELN. “Desde la campaña Por Una Paz Completa, queremos solicitarles la intervención en este proceso como mediadores, para que contribuyan como hasta ahora, a facilitar la apertura de esta mesa en Quito como se firmó el pasado 30 de Marzo del presente año y de la cual hasta el momento el país no tiene noticias de algún avance”, dice la carta.

Cabe recordar, que el **padre Francisco de Roux,** dijo esta semana que es necesario que se empiecen las conversaciones con esa guerrilla, e incluso **se ofreció como mediador**, “queremos contribuir a facilitar el que esa mesa pública de negociaciones empiece, para que el país tenga la tranquilidad de que hemos empezado realmente una paz fuerte, una terminación de todas las guerras en Colombia”.  Así mismo, instó a que la guerrilla del **ELN deje la confrontación armada y las actuaciones violentas** que afectan a la sociedad civil, tal y como sucede con el paro armado en 6 departamentos del país.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<iframe id="doc_74314" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/324131886/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
