Title: Piden que Corte Constitucional frene legalización de caza deportiva en Colombia
Date: 2019-02-06 12:10
Author: AdminContagio
Category: Ambiente, Judicial
Tags: Caza deportiva, Corte Constitucional
Slug: piden-que-corte-constitucional-frene-legalizacion-de-la-caza-deportiva-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/posta-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Posta 

###### 06 Feb 2019 

A partir del medio día, animalistas y ambientalistas se reunirán **frente a la Corte Constitucional** para solicitar a la entidad judicial, revocar los conceptos que hacen de la caza una práctica reglamentada en Colombia y ponerle fin a una actividad que promueve la tortura animal y que entes como la Procuraduría ya han señalado como inconstitucional.

Natalia Parra, directora de la Plataforma Alto contra el maltrato animal, afirma que la decisión que tome la Corte Constitucional puede marcar un a[ntes y un después en la forma en que los colombianos se comportan con los animales pues  "la caza deportiva es uno de los elementos más fuertes con los que una nación decide ser civilizada o quedarse anclada en formas anacrónicas de comportamiento con la  naturaleza", asegura. ]

> [\#AEstaHora](https://twitter.com/hashtag/AEstaHora?src=hash&ref_src=twsrc%5Etfw) estamos frente a la Corte Constitucional exigiéndole que prohíba la caza deportiva. Que la Corte corte la tortura animal [\#NoALaCazaDeportiva](https://twitter.com/hashtag/NoALaCazaDeportiva?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/YUTDZBn7FM](https://t.co/YUTDZBn7FM)
>
> — Plataforma ALTO (@PlataformaALTO) [February 6, 2019](https://twitter.com/PlataformaALTO/status/1093204578634477568?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **Un vacío legal que busca justificar la caza** 

La animalista explica que en la actualidad existen ciertos vacíos legales que permiten que este tipo de caza puede realizarse en ciertos espacios, con determinadas especies y con permisos específicos, pero sostiene que "con salvedad de la caza de sustento, realizada por las comunidades indígenas de ciertas regiones del país que atrapan a un animal no con un fin comercial sino con el fin de sobrevivir", esta debería ser el único tipo de cacería que esté avalada por la legislación colombiana.

Parra agrega que en Colombia existen clubes de tiro, los cuales justifican su accionar afirmando que no existe maltrato al disparar pues el animal no se da cuenta cuando es impactado, argumentos que son falsos, pues dentro de la misma cacería también existen elementos como la persecución, en la cual a veces son usados perros de caza e incluso la misma circunstancia de fallar un tiro que puede poner en alerta al animal o dejarlo malherido durante días.

Parra reiteró su invitación a participar del plantón y manifestase en contra de la caza deportiva, de igual forma Plataforma Alto adelantó una recolección de  firmas a través de change.org para mostrarlas como evidencia ante la Corte Constitucional y demostrar a la entidad que Colombia rechaza dicha práctica

<iframe id="audio_32329538" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32329538_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
