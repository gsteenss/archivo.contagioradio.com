Title: Poesía de nuevas voces en otros ámbitos
Date: 2015-11-29 11:03
Category: Viaje Literario
Tags: Agenda cultural Gimnasio Moderno, Alejandra Lerma, Angélica Hoyos Guzmán, Bibiana Bernal, Daniela Prado, Ela Cuavas, Gimnasio Moderno, Nuevas voces, Otros ámbitos, poesia
Slug: poesia-de-nuevas-voces-en-otros-ambitos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/elclubdelosliborsperdidos.blogspot.com_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:elclubdelosliborsperdidos.blogspot.com 

###### 17 Nov 2015

Para cerrar este 2015 el Gimnasio Moderno ha organizado el **recital “Nuevas voces, “Otros ámbitos” en donde 8 voces femeninas de diferentes regiones del país** darán visibilidad a su obra y afirmarán que la poesía es más fuerte que la muerte y goza de buena salud, todo **esto el primero de diciembre**.

En la biblioteca **Los Fundadores del Gimnasio Moderno se presentarán** Hannah Escobar nacida en1985, Ela Cuavas (1979), Angélica Hoyos Guzmán (1982), Alejandra Lerma (1991), Daniela Prado (1994), Bibiana Bernal (1985), Annabell Manjarrés Freyle (1985) y Diana Patricia Toro Ángel (1981), quienes **serán presentadas por la poeta Carolina Dávila (1982) quien Obtuvo el Premio Nacional de Poesía del Ministerio de Cultura.**

### **Poema de Daniela Prado**:

***Pienso en los niños que no nacieron  que pudieron ser mis amigos:***  
Aprieto la tierra fuerte con las manos  
beso a mi madre  
beso a mi perro  
Pienso en los niños que no nacieron  
y que pudieron ser mis amigos  
Abrazo el recuerdo que me da un árbol con su sombra  
las cuerdas de un columpio roto  
Quiero creer en los hombres  
como pequeñas corporaciones que emplean gente  
Quiero creer en los niños  
como gotas de agua nueva  
Lloro este siglo como si fuese el último  
Temo mi vida como un suceso fracasado  
Beso a una piedra y me aferro a ella  
entre este mar de brea y sueño  
Pienso en un indio milenario  
que me bendice desde el pasado  
Pienso en los hombres que fui y en las mujeres  
y en ese animal extinto que me lee  
mientras incinero este poema  
   
   
   
   
 
