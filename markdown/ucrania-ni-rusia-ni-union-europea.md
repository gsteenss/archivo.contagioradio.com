Title: Ucrania: Ni Rusia ni Unión Europea
Date: 2015-02-27 15:04
Author: CtgAdm
Category: Cesar, Opinion
Tags: Donetz, Rusia, Siria, Ucrania, Unión Soviética
Slug: ucrania-ni-rusia-ni-union-europea
Status: published

###### Imagen: www.mapsofworld.com 

#### Por  [**[César Torres Del Río]** ](https://archivo.contagioradio.com/cesar-torres-del-rio/) 

El Estado de Ucrania surgió en 1991 como efecto de la autodisolución de la Unión Soviética. Los acuerdos entre rusos y ucranianos determinaron que en Sebastopol, ciudad puerto sobre el Mar Negro, Rusia mantendría parte de su flota naval y la ciudad peninsular sería un distrito autónomo dentro de Ucrania; toda Crimea también tendría su autonomía.  
Con la nueva formación estatal emanó también la Memoria, o las Memorias, en especial la relacionada con los trágicos sucesos vividos durante la segunda guerra mundial. Digamos que en la historia se ha considerado que hay dos Ucranias, la del Este y la del Oeste; y una Crimea que fue “regalada” a Ucrania por parte de la Unión Soviética en 1953. La parte oriental, cuyos estrechos lazos de solidaridad con la URSS se remontan al internacionalismo de la revolución rusa de 1917 a 1924, se ha diferenciado de la occidental la cual siempre ha mantenido resistencias políticas, culturales y religiosas, heredadas de la antigua sumisión al imperio de los zares y por la experiencia negativa del estalinismo soviético que impuso la colectivización forzosa y los desplazamientos.

En este “occidente” tal rechazo a las acciones del aparato burocrático-estatal y gran-ruso de Stalin condujo a que sectores de la población consideraran la presencia de los nazis como un hecho liberador lo cual infortunadamente condujo a la colaboración, incluso en el genocidio judío. Desde ese momento en la URSS se difundió el epíteto político de “fascista” a toda esa región y a sus líderes, categoría que hoy los medios de comunicación rusos y pro-rusos encuentran justificable y sostenible en la guerra adelantada por Putin, verdugo de chechenos y ucranianos. Así, los sucesos de la Plaza Maidan, en febrero de 2014 que llevaron al derrocamiento del régimen de Yanukovich, son interpretados como promovidos por “fascistas”. En marzo, la respuesta imperialista rusa fue la extirpación de la península de Crimea y su incorporación al espacio exsoviético.

En octubre de 2014 se verificaron elecciones legislativas en Ucrania. La coalición en el poder ahora es liderada por el “Frente Popular” (FP), el “Bloque Petro Porochenko” y “Samopomitch” (Autoayuda). El primer ministro es Arseni Iatseniuk, del FP, quien ha prometido construir en la frontera ruso-ucraniana un “Muro Europeo” de cerca de 2.000 kilómetros de longitud. No cabe duda que Iatseniuk hace parte de la élite “oligarca” (así se le dice en Ucrania) que se ha enseñoreado en el territorio y que busca una rápida adaptación a los postulados neoliberales de la Unión Europea; lo propio hay que señalar en cuanto a Pedro Porochenko, el presidente, uno de los acaudalados hombres de negocios con más de un millardo en sus activos.

La guerra ucraniana tiene sus bemoles. La autoproclamación, en abril-mayo de 2014, de dos repúblicas populares en Donetsk y en Lugansk, en el Este, no implica que la población allí residente apoya el aparato armado pro-ruso. Lo mismo cabe decir en cuanto a los habitantes de la Crimea anexada. Ni Porochenko ni Iatseniuk representan los intereses de la Ucrania despojada.

En días pasados se firmó un cese al fuego en la guerra contra Ucrania. En general busca establecer una zona de seguridad alrededor del frente de batalla, la supervisión de eventuales acuerdos de paz por parte de la Organización para la Seguridad y Cooperación de Europa-OSCE y organización de elecciones para determinar el status de las repúblicas populares de Donetz y Lugansk.

La historia sigue repitiéndose. Alemania, Rusia y Francia, son los “determinadores” guerreristas de la inmediata suerte de Ucrania. La vía de la Unión Europea es la de la lenta agonía neoliberal. La opción rusa es la de la desintegración por la acción imperialista. Hay que apostarle a la soberanía del pueblo ucraniano y de sus organizaciones representativas.
