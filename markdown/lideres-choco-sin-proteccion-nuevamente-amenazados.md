Title: Líderes de Chocó, sin protección y nuevamente amenazados
Date: 2019-03-28 11:52
Category: Comunidad, DDHH
Tags: Bajo Atrato chocoano, Tierra y Vida - Chocó, Unidad Nacional de protección
Slug: lideres-choco-sin-proteccion-nuevamente-amenazados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [27 Mar 2019] 

Después de que Yeison Farid Mosquera, director y representante legal de **Tierra y Vida - Chocó**, denunció que su esquema de seguridad y el de por lo menos cinco compañeros más fuese levantado por la Unidad Nacional de Protección (UNP), el reclamante de tierras alertó que una vez más fue víctima de amenazas por paramilitares.

"A raíz de esta denuncia me informan que en una reunión que sostuvieron los paramilitares el 22 de marzo, los paramilitares  dijeron que ahora sí esta fue la copa que  rellenó el vaso, que me creía muy machito porque tenía ese esquema y ahora que no lo tengo sí me van a matar", manifestó el líder. (Le puede interesar: "[UNP retira protección a seis líderes sociales amenazados en Bajo Atrato](https://archivo.contagioradio.com/retira-proteccion-a-seis-lideres/)")

Mosquera asegura que pese a las denuncias que radicó ante las autoridades pertinentes, **la UNP no se ha comunicado con él** para notificarle las razones para retirar los dos escoltas y un carro blindado que le había sido otorgado por la gravedad de las amenazas en su contra. También asegura que a otros integrantes de la organización Tierra y Vida en el pais se les han retirado el esquema de seguridad sin previo aviso.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
