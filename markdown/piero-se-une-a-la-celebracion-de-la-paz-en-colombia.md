Title: Piero se une a la celebración de la paz en Colombia
Date: 2016-08-21 08:00
Category: Nacional, Paz
Tags: Cultura, Música, proceso de paz
Slug: piero-se-une-a-la-celebracion-de-la-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Piero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter ] 

###### [19 Ago 2016] 

El próximo viernes 9 de septiembre Alejandro Lerner, Víctor Heredía, León Gieco y Piero, junto al Binomio de Oro, Maía, César López, Doctor Krápula, Marta Gómez, Nawal, Todo Copas y Monsieur Perine, unirán sus voces para cantar en la Plaza de Bolívar de Bogotá desde la 1 de la tarde y hasta las 10 de la noche, en el marco de la **caravana cultural "La guerra del amor"** que se propone recorrer municipios emblemáticos para el conflicto armado colombiano y los procesos de paz.

De acuerdo con Luis Sanabria de Redepaz, la iniciativa surgió tras conversaciones con los artistas Gieco, Heredia y Piero quienes manifestaron su deseo de **aportar a la construcción paz en Colombia a través del arte**, como la mejor forma de expresión de la cultura y la vida, para desde la sensibilidad construir un mejor país convocando a todo tipo de públicos. En esta versión, la caravana también incluye la realización de una cabalgata de caballitos por la paz que promueven niños, niñas y jóvenes del Colegio Arborizadora Alta de Ciudad Bolívar.

La gira de actividades recorrerá municipio como Cravo Norte, Arauca; La Uribe, Meta; San Vicente del Cagúan, Caquetá; Santo Domingo, Cauca; Marquetalia, Tolima, El Salado, Bolívar; Ciénaga, Magdalena; Ovejas, Sucre; Tierralta Córdoba; Tibú, Antioquia; Riosucio, Choco; Bahía Portete Guajira; y Curumaní, Cesar y estará acompañada de actividades pedagógicas y diversos actos culturales con los que **se motivará a la población a votar por el si en el plebiscito por la paz** con el lema "Reconciliémonos de corazón".

Esta caravana se llevrá a cabo en el marco de la Semana por la Paz que se conmemora desde hace 29 años y que en esta nueva versión incluirá **más de 3 mil actividades para la sensibilización en torno al proceso de paz**, que se realizarán entre el 4 y el 11 de septiembre en diversas regiones del país, como ciclopaseos, partidos de fútbol y programas radiales que se articularán a las diversas iniciativas de pedagogías por la paz lideradas por organizaciones y colectivos.

<iframe src="http://co.ivoox.com/es/player_ej_12641553_2_1.html?data=kpejlpaZeZShhpywj5WcaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYtcbSpcPmysaYh5enb7O5paq9o7-RaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
