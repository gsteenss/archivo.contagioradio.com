Title: Un fantasma recorre Colombia…
Date: 2018-09-11 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: politica, populismo, populismo de derecha, populismo de izquiera
Slug: un-fantasma-recorre-colombia-2
Status: published

###### Foto: Contagio Radio 

###### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 10 Sep 2018 

La connotación peyorativa del populismo ha sido una estrategia que nació para denigrar el caudillismo desde hace muchos años, desde los totalitarismos europeos pasando por los populismos latinoamericanos, no obstante, la tendencia que cataloga peyorativamente al populismo es por antonomasia liberal y monárquica. En Latinoamérica, los populismos abanderaron cambios progresistas definitorios en países como Argentina con Perón, y se constituyeron en la más seria amenaza para las élites, como en Colombia con Gaitán.

Se podría decir que la connotación objetiva, simplemente arrastra a definir como populismo, todo aquello que está con las causas populares, por lo general, causas más emocionales que racionales, y que se define anti elitista.

Pero hoy, por ejemplo, esa oclocracia, ese gobierno de las muchedumbres, en el marco objetivo de la realidad colombiana, lo representa la descomposición de las ideas políticas ante la cultura de la imagen y las emociones, que maneja muy bien el famoso marketing político.

La demagogia del marketing político es la que ha triunfado en Colombia, eso de “votar en contra de” “votar para que no suba tal” “votar para no ser como” “no votar porque me caen mal” etc. son reales visiones que tergiversan un proyecto político, dando como resultado que un día se apoye al que ayer se odiaba, o que muchos voten convencidos por uno pero no por otra, así ambos hubiesen sido de la misma alianza…

Hoy la demagogia del marketing político aparece desastrosa para la política y golpea con fuerza a las muchedumbres ilustradas de consciencia individualista; un fenómeno que guarda distancia, de la prosa que el caudillo ofrecía en las plazas públicas en los años 40 del siglo XX a muchedumbres ignorantes, pero con consciencia de lo popular… claramente el indicador no es el título.

Laclau, teórico sobre el populismo, menciona que hay populismo siempre que las identidades colectivas se construyen en términos de una frontera dicotómica que separa a los de arriba de los de abajo. El populismo, lejos de ser un fenómeno aberrante, transicional o secundario, constituye una dimensión de la identidad colectiva. El populismo es la vía real para comprender algo relativo a la constitución ontológica de lo político; el populismo es la lógica de lo político y por eso, aunque lo desacrediten, él permanece aquí “como un fantasma” recordando a todos que, si no es por lo colectivo, por lo popular, entonces ¿para quién la política? ¿para los directores de medios que andan jugando golf? ¿para los escritorios? ¿para todos menos para la gente?

Ahora bien, en Colombia hoy no existe ninguna línea política que no sea hasta un cierto punto populista, se tilda mucho a la izquierda y a la derecha de sufrir de “populistis aguda”, ¿y es que acaso los autodenominados de centro no padecen del mal llamado defecto? Hay populismo de centro cuando éste se sostiene sobre las acusaciones exacerbadas contra la derecha y la izquierda declarándolas como populistas, cuando se dan por superadas las tesis y sus antítesis, pero no se reconoce una síntesis. Es una paradoja algo graciosa, en la que convenientemente populistas de centro, practican el populismo denigrando del populismo. Les pasa lo mismo que a esos conferencistas que comienzan una conferencia anunciando “no pretendo dar un discurso ideologizado”, siendo que la frase misma ya es un extracto puro de jugosa ideología.

Ese autodenominado centro que ataca a la izquierda y a la derecha empleando el concepto populismo como recurso peyorativo, sin reconocer el aspecto positivo del mismo, constituye de la forma más ingenua y paradójica, un soterrado “populismo de centro”, puesto que emplea el concepto populista como estrategia para desacreditar, no solo a la derecha, o no solo a la izquierda, sino en términos de Laclau a la ontología misma de la política. Ese populismo de centro, que excluye a la izquierda y a la derecha, que las rechaza, que las desautoriza, no se da cuenta de que toma su razón de ser de ellas, y que, negándolas, o denigrando de ellas no desaparece la dicotomía… al final, el populismo de centro podría ser otra clara manifestación de neoliberalismo, cuyo fin no es desacreditar a la izquierda o a la derecha, sino a la política…

Carlos Fernando Galán escribía en su “Manifiesto por el Centro” que ya era “necesario un Centro que entienda que un líder es aquel que ayuda a sanar heridas y no a profundizarlas” mientras que la vez escribía “el país corre el riesgo de caer en un péndulo nefasto entre populismo de derecha y de izquierda” y continuaba, “necesitamos un Centro que sea capaz de hacer política moderna y sin populismo” es decir un Centro que le apuesta a la sanación pero que se manifiesta excluyendo al populismo como aquella expresión ontológica de la política y que le apuesta a la no profundización de las diferencias mientras ataca de paso a la derecha y a la izquierda, simplemente es un centro que se está tomando un vino con el fantasma del populismo.

Mientras la tendencia del centro, al estilo de Galán y toda la cultura pop del Ni Ni, sea la de presentarse como ese tercer incluido del que hablaba Bobbio, que rechaza a la izquierda y a la derecha, sencillamente da por hecho que existe una contradicción entre izquierda y derecha, y además ratifica, sin darse cuenta, que dicha contradicción es su razón de ser… ¡ni modo! queda claro que podría constituirse un centro pero no por la vía del “ni ni” sino por la del “et et”. Por ahora, mientras la estrategia para rechazar el populismo de derecha y de izquierda, sea el populismo de centro, el único que seguirá recorriendo Colombia será un fantasma… el fantasma del populismo.

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
