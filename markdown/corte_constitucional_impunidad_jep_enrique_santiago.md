Title: Senado y Corte Constitucional bendijeron la "fiesta de impunidad" en Colombia: Enrique Santiago
Date: 2017-11-16 13:15
Category: Entrevistas, Paz
Tags: Corte Constitucional, enrique santiago, JEP, Senado
Slug: corte_constitucional_impunidad_jep_enrique_santiago
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/enrique_santiago-e1510854914241.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: websur.net] 

###### [16 Nov 2017] 

"Que el congreso se le ocurra convertir lo que es un deber jurídico, como la defensa de los derechos humanos, en una causa de inhabilidad efectivamente es vergonzoso, pero esto no va a pasar desapercibido por la comunidad internacional", asegura el asesor jurídico del proceso de paz, Enrique Santiago, y agrega que **se siente "usado por no decir engañado".**

### **"Incumplimiento sistemático del acuerdo de paz"** 

Para el abogado, el fallo de la Corte Constitucional y lo pactado en el Senado en la noche del miércoles, modificando aspectos esenciales del acuerdo de Paz va en contra de la aprobado por el propio congreso en el acto legislativo para la paz, donde se establece que** todas las autoridades en el momento de la implementación tienen que respetar el espíritu del acuerdo.**

El jurista señala que las instituciones del Estado están dejando el acuerdo de La Habana irreconocible. Sobre el Congreso, el jurista dice que lo que ha hecho el Senado "criminalizando a los defensores de derechos humanos tachándolos de no imparciales".

En cambio no se tiene en cuenta algo que debería ser natural en el derecho que es la defensa de los derechos humanos, **"es decir que cualquier jurista debe respetar los derechos humanos, eso no es una opción, sea como magistrado, académico, fiscal, se debe defender a las víctimas".** (Le puede interesar:[Las preocupaciones que deja el fallo de la Corte sobre la JEP)](https://archivo.contagioradio.com/49091/)

### **Reacción de la Comunidad Internacional** 

Sobre lo aprobado por el Senado respecto al Comité de Escogencia, para Santiago, se trata de un irrespeto a la comunidad internacional que no ha medido el Congreso, pues se trata de un **Comité operado por personas escogidas por el Secretario General de la ONU, el Tribunal Europeo de Derechos Humanos, entre otras instancias internacionales.**

En ese sentido, el abogado espera que la Corte Constitucional, deje sin efecto "la inmensa mayoría de las aberraciones jurídicas que se incorporaron por parte de un Senado que legisló a la carta, y solo para uno de los actores del conflicto", sin embargo, también manifiesta que es importante que desde otras instituciones se enmiende lo que hicieron los senadores, antes de que el texto de la Ley Estatutaria de la JEP llegue al alto tribunal.

### **Exclusión de terceros en la JEP**

Enrique Santiago, manifiesta que la exclusión de civiles o terceros en la JEP, implicados en el conflicto armado, "Es bendecir la fiesta de impunidad que ha habido en el país". Lo que se revolvió en la Corte sobre dicho tema, **va en la línea del "cinismo" con la que el establecimiento colombiano utiliza el tema de las víctimas,** "a todo el mundo se le llena boca para hablar  las víctimas, pero para impedir que se adopten medidas eficaces para garantizar los derechos de las víctimas".

Ese tema en el fallo de la Corte, significa que se está preservando el inmenso nicho de impunidad que existe en Colombia, obstruyendo el derecho de las víctimas a la justicia, la verdad, la reparación y las garantías de no repetición. (Le puede interesar:[ Inhabilidad de magistrados es inconstitucional)](https://archivo.contagioradio.com/incostitucional-comite-escogencia-jep/)

Incluso, da como ejemplo que la propia Corte Penal Internacional,  ha señalado insistentemente que el mayor núcleo de impunidad es sobre la actuación de civiles, paramilitares y funcionarios públicos. Desde siempre el juzgamiento de terceros ha estado en manos de la justicia ordinaria, pero a la fecha, según la CPI, **hay 15.000 compulsas de copias sobre conductas de civiles relacionadas con grupos paramilitares, que duermen en el despacho de la Fiscalía.**

<iframe id="audio_22116763" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22116763_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
