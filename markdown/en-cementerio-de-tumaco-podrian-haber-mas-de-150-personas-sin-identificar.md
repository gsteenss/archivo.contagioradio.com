Title: En cementerio de Tumaco podrían haber más de 150 personas sin identificar
Date: 2018-07-09 12:59
Category: DDHH, Nacional
Tags: Cementerio de Tumaco, Fiscalía, personas desaparecidas, Unidad de Búqueda de personas desaparecidas
Slug: en-cementerio-de-tumaco-podrian-haber-mas-de-150-personas-sin-identificar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Jul 2018] 

El próximo 12 de julio, en Tumaco Nariño, se llevará a cabo la audiencia pública sobre la situación de las víctimas de desaparición forzada y personas no identificadas en el cementerio de ese municipio, que según el Colectivo Orlando Fals Borda podrían **llegar a ser más de 150 personas**.

De acuerdo con Naya Parra, integrante de esa organización, hay un número significativo de personas que fueron sepultadas en ese cementerio y que **“pueden ser muchas de las personas desaparecidas buscadas por sus familiares”** en el marco del conflicto armado. Además que el cementerio actualmente se encuentra en una grave situación humanitaria, debido a que no tiene más capacidad para recibir cuerpos.

“Pueden haber cientos de cuerpos sin identificar que están en riesgo de que se pierdan por la saturación que tiene el cementerio y la construcción de bóvedas en los lugares de inhumación de los cuerpos sin identificar” afirmó Parra y agregó que debido a esta misma crisis se han sacado **hasta más de medio centenar de cuerpos que no han sido sepultados.**

Sobre el número exacto de cuerpos que podrían estar allí, Parra expresó que si bien se asume que podrían ser más de 150 “no hay consistencia en los datos existentes entre la información que tiene la administración del Cementerio, la que tiene Fiscalía y la que tiene la institución. (Le puede interesar: ["Así funcionará la Unidad de Busqueda de personas desaparecidas"](https://archivo.contagioradio.com/asi-funcionara-la-unidad-busqueda-personas-desaparecidas/))

Esta audiencia es producto del trabajo que adelanta la mesa para la atención a víctimas del departamento de Nariño, de carácter interinstitucional, que viene trabajando desde hace más de 5 años. Asimismo, según Parra, este escenario también servirá para hacer un llamado de atención sobre la importancia de hacer un adecuado proceso de intervención en los cementerios de este departamento.

“Son los cementerios uno de los lugares en donde se deben buscar a las personas desaparecidas en este país, tenemos indicios de que pueden ser más de cien mil personas” aseguró Parra y agregó que **solamente en 423 cementerios hay más de 26 mil cuerpos sin identificar, de acuerdo a un informe del Ministerio del Interior**.

<iframe id="audio_26967932" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26967932_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
