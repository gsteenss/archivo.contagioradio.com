Title: Pasaron nueve meses para que Gobierno instalara Mesa Nacional de Garantías
Date: 2019-05-30 21:14
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: defensores de derechos humanos, Gobierno, lideres sociales, Mesa Nacional de Garantías
Slug: pasaron-nueve-meses-gobierno-mesa-nacional-de-garantias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Mesa-Nacional-de-Garantías-instalada.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @AlirioUribeMuoz  
] 

Este jueves, tras nueve meses de iniciado el periodo de mandato de Duque y el aumento en el asesinato a líderes sociales, el Gobierno instaló la Mesa Nacional de Garantías; u**na importante instancia creada para avanzar en la protección de los derechos humanos en el país.** El lanzamiento del organismo se realizó en la Casa de la Moneda en Popayán, Cauca, donde se reunieron representantes del Gobierno, organismos internacionales y líderes sociales.

Alirio Uribe Muñóz, abogado y activista por los derechos humanos, sostuvo que en representación del presidente Iván Duque asisitieron Nancy Patricia Gutiérrez (ministra de interior) y Francisco Barbosa (consejero presidencial para los DD.HH.); adicionalmente hicieron presencia instituciones como la Procuraduría, Fiscalía y más de 40 voceros de organizaciones sociales. (Le puede interesar: ["ONU registró 13 mil personas desplazadas durante 218 en Catatumbo"](https://archivo.contagioradio.com/personas-desplazadas-durante-2018-catatumbo/))

### Lo que piden las organizaciones sociales

Durante la instalación de la Mesa Nacional de Garantías, líderes sociales expresaron sus preocupaciones frente a la actual situación de derechos humanos en el país; y **evidenciaron la necesidad de crear un política pública de protección a la vida de las comunidades que se concerte entre los sectores institucionales y del movimiento social.** Cabe resaltar que organizaciones como la Oficina para los Derechos Humanos de la ONU en Colombia habían pedido reactivar este escenario.

**Aída Quilcué, acompañante de DD.HH del Consejo Regional Indígena del Cauca (CRIC),** quien fue objeto de amenazas en los más recientes días junto a otros dirigentes indígenas, señaló que pese a los avances, "aún falta mucho para poner en marcha garantías que contribuyan a la protección de la vida, vemos que se agudiza cada día más la vulneración de los derechos".

Aunque las amenazas contra los pueblos indígenas no se discutieron durante esta jornada, Quilcué precisó que la estigmatización aumento después de la Minga del Suroccidente, un hecho reflejado en la aparición de más de 11 panfletos contra la comunidad, **"la lectura que hacemos es que estas amenazan se materializan , como lo ocurrido con los compañeros de ACONC y con Edwin Dagua**".  [(Le puede interesar: No hay tregua para indígenas y líderes sociales en Cauca)](https://archivo.contagioradio.com/no-tregua-indigenas-lideres-sociales-cauca/)

### **Los 5 temas que se pusieron en la agenda del Gobierno**

El Activista señaló que uno de los temas a tratar serán las garantías de protección y seguridad en los territorios mediante de la **implementación de las medidas propuestas por el Acuerdo de Paz**; así mismo, pedirán la implementación del Decreto firmado por el anterior Gobierno que establecía un sistema de protección comunal y la creación de medidas de seguridad colectivas.

De igual forma y tomando en cuenta la presencia de organismos internacionales como la Organización de Naciones Unidas (ONU), se tratarán las recomendaciones internacionales hechas al Gobierno para que proteja a los defensores de derechos humanos; entre ellas, la creación de un documento CONPES que establezca un presupuesto y plan específico. Por su parte, las organizaciones sociales hablarán sobre el tema de la protesta, **así como casos de desaparición, desplazamiento forzado y confinamiento, entre otros, que se presentan en las regiones.**

Finalmente uno de los tema clave fue la **continuidad de los diálogos con el Ejército de Liberación Nacional (ELN)**, puesto que un acuerdo de paz con esta guerrilla significaría avanzar en términos de derechos para los territorios más afectados por el conflicto. Por último, los representantes de organizaciones sociales pidieron al Gobierno la reactivación de las 19 mesas departamentales de garantías; con las que se espera desarrollar un diálogo con enfoque territorial sobre medidas eficaces para proteger la vida de los líderes sociales.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
