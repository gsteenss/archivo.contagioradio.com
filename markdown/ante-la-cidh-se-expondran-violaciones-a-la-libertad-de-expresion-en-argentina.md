Title: Ante la CIDH se expondrán violaciones a la libertad de expresión en Argentina
Date: 2016-04-07 13:13
Category: El mundo, Política
Tags: Ley de Servicios de comunicación Audiovisual, Libertad expresión en Argentina, Mauricio Macri
Slug: ante-la-cidh-se-expondran-violaciones-a-la-libertad-de-expresion-en-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/argentina-ley-de-medios-702x395.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Teledoce 

###### [7 Abr 2016] 

Organizaciones académicas, sindicales y colectivos de comunicación argentinos acompañados por otros de carácter continental, se presentarán este viernes 8 de abril ante la Comisión Interamericana de Derechos Humanos para solicitar garantías en el cumplimiento del derecho a la libertad de expresión y expresar su rechazo ante los cambios a la Ley de Servicios de comunicación Audiovisual en ese país.

En la audiencia pública que tendrá lugar en Washington, la delegación argentina integrada por representantes del conjunto de organismos de derechos humanos, universidades públicas, sindicatos de trabajadores de prensa y redes de radios comunitarias,  expondrán el avasallamiento producido por los decretos y decisiones del gobierno Macri, que modifican la [ley de medios](https://archivo.contagioradio.com/ley-de-medios-amordazada-en-argentina/) y los diferentes [ataques contra la libertad de prensa](https://archivo.contagioradio.com/los-ataques-de-macri-contra-la-libertad-de-prensa-en-argenina/).

Desde las 8:00 de la mañana (Hora de Colombia), las organizaciones convocantes tendrán la oportunidad de exponer sus argumentos, para ser atendidos por representantes del gobierno citados para responder entre otras por las medidas que disolvieron organismos con representación significativa del Poder Legislativo, para reemplazarlos por otro con mayoría automática del Ejecutivo.

Adicionalmente, las organizaciones buscan respuesta a los supuestos llamados de urgencia y necesidad presentados por el gobierno, para fomentar la concentración de medios a través de cambios en los topes regulatorios y  la transferencia de la televisión por cable al sistema comercial de las telecomunicaciones.

Entre los participantes se encuentran el Foro Argentino de Radios Comunitarias (FARCO) y la AsociaciónLatinoamericana de Educación Radiofónica (ALER), quienes transmitirán [en Vivo](http://aler.org/online-as.html) los pormenores de la audiencia.

 
