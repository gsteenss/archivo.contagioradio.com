Title: Mecanismo tripartito entrega informe sobre cese al fuego
Date: 2016-12-01 13:52
Category: Nacional, Paz
Tags: Cese al fuego, FARC, gobierno colombiano, Mecanismo Tripartito, paz
Slug: mecanismo-tripartito-entrega-informe-sobre-cese-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Mecanimos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Macondo] 

###### [1 Dic. 2016] 

**Recientemente el Mecanismo tripartito de Monitoreo y Verificación (MM&V) del Cese al Fuego y de Hostilidades Bilateral y Definitivo dio a conocer los resultados de la verificación** que realizó a propósito  de los hechos ocurridos el pasado 13 de noviembre en el municipio de Santa Rosa, al sur del departamento de Bolívar, en el que murieron dos personas de las Farc y un tercero se encuentra desmovilizado.

En el informe el Mecanismo tripartito de Monitoreo y Verificación asegura que **luego de recolectada la información los resultados aseguran que existieron incidentes relevantes e incumplimientos que se pueden considerar como violaciones al protocolo del 13 de octubre del 2016, que actualmente rige el Cese al Fuego y Hostilidades Bilateral y Definitivo.**

**Según el Mecanismo tripartito, el desplazamiento de los tres integrantes de las Farc no fue informado, ni coordinado** con ninguna de las instituciones encargadas para tales efectos, de manera que a´si se estaría incumpliendo lo establecido en el protocolo que solicita a los integrantes de dicha guerrilla

*“no hacer presencia por fuera de los Puntos de Preagrupamiento Temporal armados y uniformados sin coordinación previa con el MM&V”.*

Posteriormente, el **informe manifiesta que dado el incumplimiento por parte de los integrantes de las Farc, la Fuerza Pública abrió fuego en contra de ellos, sin pretender mediar otro tipo de acción previamente**. Sin embargo, según asegura el MM&V, el argumento de los militares es que estaban “convencidos que estaban ejecutando una acción contra el ELN”. Pese a ello, posteriormente notan que los guerrilleros eran integrantes de las Farc.

**El Mecanismo tripartito aseveró que efectuará las recomendaciones al Gobierno de Colombia y a las FARC-EP** encaminadas a que situaciones como esta no vuelvan a suceder, y recalcan que “el flujo de información y la comunicación es vital para la coordinación entre las partes y con el MM&V”.

**Comisión alterna asegura que Ejército violó cese al fuego**

**El pasado 18 de Noviembre se conformó una comisión de verificación alterna para recolectar información de los hechos que aconteciendo en esta zona del país en donde fallecieron los dos guerrilleros de las Farc.**

**Dicha comisión estuvo compuesta por miembros de la Asociación de Hermandades Agroecológicas y Mineras de Guamocó (AHERAMIGUA)**, en conjunto con las comunidades de la vereda El Golfo, del municipio de Santa Rosa del Sur de Bolívar.

El informe de dicha comisión aseguraba que **“aproximadamente a la 1:30 p.m.  “Joaco” se encontraba hablando por celular y de manera inesperada cayó al piso, el disparo que recibió fue levemente percibido, en ese momento “Mónica” se inclina a ver que le sucedió y  también recibe un disparo quedando en el suelo”.**

Así mismo, se manifiesta que **las personas allí presentes se dan cuenta que los disparos fueron efectuados por francotiradores** “Los miembros del Ejército estaban aproximadamente entre 30 y 40 metros de donde se encontraban los guerrilleros, y tras los disparos lanzan dos ráfagas de disparos al aire. Le puede interesar: [“Joaco” y Mónica también son colombianos: Paz a la Calle](https://archivo.contagioradio.com/joaco-y-monica-tambien-son-colombianos/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
