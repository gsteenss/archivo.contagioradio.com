Title: Milicia islámica Al-Shabab asesina a 148  estudiantes de universidad en Kenya
Date: 2015-04-06 17:54
Author: CtgAdm
Category: DDHH, El mundo
Tags: Kenia atentado terrorista universidad deja 150 muertos, Milicia somalí Al-Shaabab atac universidad en Kenia
Slug: milicia-islamica-al-shabab-asesina-a-148-estudiantes-de-universidad-en-kenya
Status: published

###### Foto:Laverdadnoticias.com 

###### **Entrevista a [Víctor de Currea Lugo], analista político de Oriente Medio y África:** 

<iframe src="http://www.ivoox.com/player_ek_4313090_2_1.html?data=lZielZWddI6ZmKiakpyJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdXZz9nOxtSPuMbm09Tfy9jYpYzX0NPh1MaPuc%2Fd18rf1c7IpcWfxtOYrcrSrcKfxcrXw5CVeY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El jueves 2 de Abril miembros de la milicia somalí Al-Shabab atacaron la universidad de Garissa en Kenia dejando un saldo de **148 estudiantes asesinados** y más de **533 desparecidos**.

La universidad de mayoría cristiana se encuentra a **150 Km de Somalia** y ya había sido lugar de ataques por la milicia kenianas, por lo que estaba en serio peligro de ser víctima de una masacre.

En el ataque los milicianos se **ensañaron contra los alumnos cristianos**, desalojando previamente a todos los estudiantes musulmanes. Actualmente hay cinco detenidos, pero aún no ha sido capturado el cerebro del atentado.

Según **Víctor de Currea Lugo**, analista político del mundo árabe en Colombia, los ataques **responden a la incursión militar de Kenia sobre el sur de Somalia en 2011**. Esta no ha sido la única respuesta de Al-Shabab desde la incursión de Kenia contra sus campos, el último ataque dejó un saldo de 66 muertos en un centro comercial en Nairobi, la capital.

En **1991 fue destituido el presidente de Somalia** y comenzó una guerra civil, en la que distintos **señores de la guerra** se han disputado el poder. Naciones Unidas y EEUU han desplegado cascos azules en la región con poco éxito. Es en este contexto en el que **nace Al-Shabab**.

Kenia cuenta con **500.000 refugiados somalíes** de mayoría musulmana situados en la frontera entre ambos países, este campo de refugiados es el segundo **núcleo urbano más grande de Kenia**.

El país de mayoría cristiana cuenta con una **minoría musulmana del 11%,** en grave riesgo de exclusión social, en total son 4.5 millones de musulmanes.

Víctor de Currea afirma que la solución para frenar grupos islamistas radicales como Al-Shabab en la región, Boko Haram en Nigeria o el más fuerte militarmente, en el mundo árabe, Estado Islámico, **no solo pasa por lo militar** sino que es necesario **políticas que frenen la exclusión social** de las comunidades afectadas, que es donde el islamismo radical más coopta a sus miembros.
