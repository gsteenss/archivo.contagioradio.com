Title: Con fotos ELN revela supervivencia de los soldados retenidos en Boyacá
Date: 2015-11-09 15:05
Category: Nacional
Tags: Boyacá, ELN, Güicán, soldados Andrés Felipe Pérez y Clayder Antonio Rodríguez, soldados secuestrados por el eln
Slug: con-fotos-eln-revela-supervivencia-de-los-soldados-retenidos-en-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/prueba-de-vida-soldados-retenidos-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [9 nov 2015] 

La guerrilla del ELN reveló la supervivencia de los soldados **Andrés Felipe Pérez y Clayder Antonio Rodríguez, **retenidos el pasado 26 de octubre en zona rural  del Municipio de Guican, Boyacá.

Mediante un comunicado ELN  asegura que  **"los dos soldados están sanos y salvos bajo la custodia del ELN,** y nos esforzamos por protegerlos, a pesar de las amenazas y los fuertes operativos militares que tienen la orden de rescatarlos a sangre y fuego”.

Adicionalmente **“el ELN ratifica la voluntad de liberar los soldados prisioneros de guerra lo antes posible, pero esto no depende sólo de la voluntad del ELN, depende que el gobierno permita que los soldados regresen con vida,** que garantice la seguridad y las condiciones de entrega a una Comisión Humanitaria, sin presencia ni amenaza de la fuerza pública**”** agrega comunicado.

Los soldados fueron retenidos luego de una emboscada del ELN  **donde fallecieron once militares y un policía,** el hecho sucedió** **cuando los uniformados y delegados de la Registraduría regresaban al resguardo indígena U´wa durante los comicios electorales del pasado 25 de octubre.

Cabe recordar, que familiares de los soldados marcharon el pasado 4 de Octubre en Tuluá, Valle por la liberación de los dos militares. La madre de Andrés Felipe Pérez pidió al Presidente de la República la intervención  para la pronta liberación de los dos  integrantes del Ejercito.
