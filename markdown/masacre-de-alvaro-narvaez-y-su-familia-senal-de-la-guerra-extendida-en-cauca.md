Title: Masacre de Álvaro Narváez y su familia. Señal de la guerra extendida en Cauca
Date: 2020-04-30 16:33
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Álvaro Narváez, asesinato, Cauca, lideres sociales
Slug: masacre-de-alvaro-narvaez-y-su-familia-senal-de-la-guerra-extendida-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En las últimas horas se conoció el asesinato de **Álvaro Narváez** **juntos a tres integrante de su núcleo familiar**, el hecho se registró en la vereda **El Vado, corregimiento de Mojarras, municipio de Mercaderes, Cauca**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según información de la comunidad sobre las 8:20 pm del miércoles 29 de marzo hombre armados ingresaron a la vivienda de líder comunal, y **dispararon asesinado a su esposa María Delia Daza, su hijo Cristian Narváez y su nieta Yeni Caterine López** y dejando herido a otro de sus hijos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Álvaro Narváez, era **presidente de la Junta de Acción Comunal de la vereda El Vado**, territorio hubicado a 114 km de Popayán y rodeado por zona montañosa, lo cual incrementa la dificultad de comunicación e ingreso al territorio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1255724009162977280","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1255724009162977280

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Así como el líder y senador indígenas, varias organizaciones se han pronunciado ante el aumento de la violencia en el departamento del Cauca, rechazando estas acciones, al mismo tiempo que insisten al [ELN](https://www.justiciaypazcolombia.com/carta-abierta-al-presidente-ivan-duque-acuerdo-humanitario-global-covid19/) mantener el alto en sus acciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado habitantes del territorio señalan que estos hechos pueden ser atribuidos a cualquier grupo armado que transita este territorio; Destacando que en el corregimiento de El Vado, donde se presentó este asesinato se movilización los mismos grupos armados que operan en [Argelia](https://archivo.contagioradio.com/en-cauca-toda-la-comunidad-se-siente-en-riesgo/).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Este tipo masacres son prácticas del paramilitarismo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sin embargo algunas hipótesis afirman que este tipo de prácticas violentas no corresponden a las acciones armadas de la insurgencia, por el contrario condicen con practicas paramilitares, he incluso de las Fuerzas Militares. (Le puede interesar: <https://archivo.contagioradio.com/tres-diferencias-entre-disidencias-de-farc-recorriendo-las-guerras-en-colombia/>).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregan que el municipio de Mercaderes, es un territorio pacifico, y poco golpeado por la violencia, destacando que el último hecho se registró en 2017, cuando fue asesinado otro líder comunal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El asesinato de Álvaro Narváez aumenta a 69 el número de líderes territoriales asesinados en el 2020; 16 de ellos solo en el departamento del Cauca. (Le puede interesar:<https://archivo.contagioradio.com/ausencia-estatal-tiene-al-borde-de-la-hambruna-a-lideres-comunales-de-tumaco/>)

<!-- /wp:paragraph -->
