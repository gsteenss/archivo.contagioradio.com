Title: Bogotá canta un goles para la paz
Date: 2015-04-08 12:09
Author: CtgAdm
Category: Nacional, Paz
Tags: 9 de abril, aldo cadena, diego, futbol, gloria florez, IDRD, maradona, Partido por la Paz, paz, piedad cordoba
Slug: bogota-cantara-un-gol-para-la-paz
Status: published

<div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4325013_2_1.html?data=lZifl5WVd46ZmKiakpWJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMrZxcbRjajTtsXjw8aY1dTGtsafscbf1s7Is4zk0NeYzsaPlMLujoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Piedad Córdoba, Colombianos y Colombianas por la Paz] 

Este viernes 10 de abril se llevará a cabo en Bogotá el Partido por la Paz, en una conjugación de "la cultura, el arte y el deporte al servicio de la paz".

La iniciativa que fue puesta sobre la mesa por líderesas sociales como **Piedad Córdoba**, el presidente Juan Manuel Santos, e incluso la misma **Delegación de Paz de las FARC**, en La Habana en el marco del Mundial de Fútbol 2014, logró concretarse gracias al trabajo mancomunado de organizaciones sociales y populares, instituciones distritales como IDARTES y el IDRD, y el apoyo del gobierno nacional a través de Coldeportes.

</div>

Según lo expuesto por **Aldo Cadena, Director del IDRD**, el evento deportivo iniciará a las 5 de la tarde en el Estadio de Techo, ubicado en la localidad de Kennedy, con un partido de fútbol femenino entre la selección de Bogotá y un grupo compuesto por artistas colombianas.

Le seguirá un partido de fútbol masculino a las 7 de la noche, en que se enfrentará el equipo del ex-jugador argentino Diego Armando Maradona, quien estará acompañado de jugadores amigos como José Luis Brown, Roberto Cadañas, Luis Mendoza y José Basualdo; contra el equipo colombiano integrado por Alexis García, Chonto Herrera, el Tren Valencia, Gato Perez, el Chaca Palacios, el Tigre Castillo, Fredy Rincón, el Chicho Serna, Yerson Gonzales, Roberto Carlos Cortez, Carlos Gutierrez, mientras se espera la confirmación de el Pibe Valderrama y Juan Pablo Angel.

En el transcurso de todo el evento, las y los organizadores, así como las y los jugadores participantes, buscarán realizar gestos simbólicos de paz. La camiseta que portarán los equipos, por ejemplo, llevará escrita la frase **"La paz nos mueve".**

<div>

Para la **Secretaria de Gobierno, Gloria Flórez,** "es muy importante la realización de este partido, no solo por que une a Colombia sino ademas porque da un mensaje a jóvenes y adolecentes que se agrupan en las **barras futboleras**" para que cesen los enfrentamientos violentos entre las hinchadas. "La presencia de la juventud jugará un rol de convivencia y de paz, para de gritar que el fútbol es paz y que este partido se jugará con el corazón de América Latina y de Colombia", señaló.

Aldo Caneda agregó que sólo en Bogotá se llevan a cabo semanalmente más de 100 mil actividades relacionadas con el fútbol, y 70 mil jóvenes realizan jornadas de deportivas de jornadas diarias, y que si todo el **potencial de la juventud** lograra ponerse al servicio de la paz, mucho se lograría para el futuro de Colombia.

Las personas interesadas en asistir al encuentro futbolero del **viernes 10 de abril** pueden reclamar una de las 9 mil las boletas gratuitas en las taquillas del Estadio de Techo el mismo viernes en la mañana, o podrán seguirlo en vivo a través de Canal Capital  y Telesur.

La intensión de las y los organizadores del encuentro es realizar un partido de fútbol similar en **La Habana, Cuba,** en el que puedan participar también las personas integrantes de la delegación de la guerrilla y del gobierno, para replicar los gestos de paz también en ese escenario.

<iframe src="http://www.ivoox.com/player_ek_4325022_2_1.html?data=lZifl5WWdo6ZmKiakpeJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhc3Y0JCww8nJssKf1NTP1MqPlMLm1c7R0ZDUs9OfzcaYssbecYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Aldo Cadena, Director del IDRD] 

##### <iframe src="http://www.ivoox.com/player_ek_4325028_2_1.html?data=lZifl5WWfI6ZmKiakpaJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi83j087OjavQs9PZ25Dg0cfWqYzkwtfhy8nTb9Hj05DZw5DUpduhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Gloria Flórez, Secretaria de Gobierno de Bogotá] 

</div>
