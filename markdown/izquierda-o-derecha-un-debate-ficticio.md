Title: Izquierda o Derecha ¿un debate ficticio?
Date: 2016-12-16 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Fidel Castro, izquierda y derecha, liberalismo
Slug: izquierda-o-derecha-un-debate-ficticio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/izquierda-y-derecha.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Diario Literario Digital. Caricatura: Salvatore] 

###### 16 Dic 2016 

#### **[Por: Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

[El debate entre derecha e izquierda parece ser un debate ficticio porque ha perdido aceleradamente sustentos contextuales que den soporte a ambas expresiones, es decir, por el lado de las alegorías de la izquierda, Chávez ha muerto, mataron a Gadafi, muere Fidel, el gordito come queso de Korea del Norte no sale de su ostra sellada, de los Chinos ni hablemos, y por el lado de las alegorías de la derecha, gente por el estilo de Videla y Pinochet, ya murieron pues los gringos ahora prefieren financiar guerrillas fundamentalistas en medio oriente; y de los fascistas que inundaron el mundo por allá en el siglo XX con uniformes militares y esquematismos castrenses ya no queda absolutamente nadie.]

[¿Qué es lo que hay entonces? Democracia. Pero si vamos al fondo de la cosa por ahí dicen que hay democracias de izquierda y democracias de derecha, entonces la confusión emerge pues es extraño decir que Bush fue un derechista dictador, o que Correa es un dictador izquierdista.]

[El traje y la corbata, las votaciones, y una cantidad de frustrados de izquierda y de derecha terminaron ajustándose en un cómodo modelo como lo es la democracia, no obstante, en el fondo y en la superficie, ese debate de derecha e izquierda murió hace rato, pues al final no fue la democracia la que calmó los ánimos, sino que fue el liberalismo al que todos se solaparon y les fue muy útil para seguir taladrando a sus enemigos o seguir amenizando sus conversaciones con un debate caduco.]

[Hoy por lo general, si uno se fija bien, no ya no hay debates filosóficos en el congreso, tampoco enfrentamientos de corrientes filosófico políticas, ni que hablar de los debates televisados ¡¡esos son una comedia!! Al final la pelea entre dos liberales que se acusan de ser de derecha o de ser de izquierda en un debate, no es más sino por votos o por la ejecución de un contrato, pues luego la gente se da cuenta que juntos, se sirven del statu quo y además son socios de un mismo club.]

[En los debates contemporáneos siempre se hacen comparativas ofensivas con ejemplos ubicados en el pasado y relacionados con esa caduca pelea entre izquierda y derecha, pues hay momentos en que parece que todos se dicen “fachos”, le dicen “facho” al uno y “facho” al otro, pero nadie reconoce el concepto]*[facsi…]*[ para qué saber ¿acaso importa? Por supuesto que no, parece ser que hoy no importa saber qué es izquierda o qué es derecha, parece ser que hoy la tendencia es apelar a la opinión como sustento suplente del conocimiento de la historia pues si incluso el señor Juan Manuel Santos es acusado por la politóloga María Fernanda Cabal de ser comunista, sencillamente algo ya no es coherente.]

[Sin la potestad para dar un veredicto sobre la victoria de la derecha o la izquierda, la verdad creo que las dos perdieron, y juntas tienen a sus sobrevivientes como emisarios consagrados ocultos en las democracias, pero promoviendo el paradigma político, económico y cultural que rige hoy de manera infame los destinos de la humanidad: el liberalismo.]

[De una parte, el liberalismo acogió a los derechistas frustrados muy fácil porque como buenos defensores del statu quo, descubrieron que con las leyes y toda esa carreta de las elecciones, aunque no les gustara, aunque se tardara un poco más y hubiese escándalos o críticas, el poder se salvaguardaba, no como quisieran, de una forma despótica o tiránica, pero a fin de cuentas, el poder se salvaguardaba lejos de la amenaza roja que quería acabar con la gente de bien.]

[De otra parte, el liberalismo acogió con más dificultad (pero los acogió) a esos izquierdistas frustrados, que pasaron de leer a Marx a leer a Camus, que pasaron de aplaudir a Fidel a aplaudir a Vargas Llosa o que pasaron de reír con Jaime Garzón a ser neoliberales consagrados, y aunque en el fondo no les gustara moralmente la idea liberal, podían cubrir perfectamente la frustración de no haber podido ser un Che Guevara; por su parte, el liberalismo les ofreció a buen precio una desobediencia que pudieran pagar con cuotas de indulgencia, a la par que convertían su frustración, en ataques agresivos y psicóticos contra todo lo que les recordara lo que alguna vez fueron.  ]

[En el liberalismo cayeron todos los frustrados. Los de derecha y de izquierda según la conveniencia, juzgan a los enemigos de derechistas, o de izquierdistas, o incluso, una nueva ola de inconformistas liberales critica a ambos bandos desde la comodidad de una posición social ya no abrazada a ninguna clase de teoría o manifiesto, sino enclaustrada en ese fetiche que llaman libertad de expresión, que no es más que un contentillo que otorgan las leyes para que puedas hacer catarsis, echarle la madre al uno u al otro pero sin alterar el statu quo.  ]

[Antes era derecha o izquierda… hoy ese debate lo imponen sin la fuerza y con un trato tan poco profundo e intelectualmente empobrecido de los temas, que hasta produce nauseas… sí, el liberalismo triunfa hoy sin una avanzada que la haga frente, ya ganó Trump, ya ganó Peña Nieto, ya ganó Peñalosa, ya ganó Ortega, ya ganó Maduro, ya ganó Macri, ya ganará Leopoldo, y yo que sepa todos esos ni son izquierda ni son derecha. ¿Qué se le opondrá entonces al liberalismo? ¿Qué se le opondrá a este juego corrupto liberal que aboga por el “libre mercado”, la abolición del Estado, pero jamás aboga por la abolición de los impuestos o el exterminio de la ley?]

[Tengo una sugerencia, porque si digo “propuesta” luego los liberales me dirán en un lado que soy de izquierda, o en otro lado que soy de derecha. El liberalismo es una idea materializada en discurso, cultura y acción política. Ahora bien, el marxismo no pudo con el liberalismo, ni el fascismo tampoco, los errores que tuvieron estos dos últimos fueron al final el mismo: eliminar la libertad individual. Por eso el liberalismo le da “sopa y seco” a la izquierda y a la derecha, no obstante, hay una excelente arma contra el liberalismo, pero lastimosamente muy poco comprendida en Colombia: el anarquismo.]

[El anarquismo es una filosofía política. En Colombia no se comprende así por culpa de los reciclajes culturales que en occidente la asocian con el punk, el nihilismo craso, las drogas, hedonismo craso, o el “anti todo”.  Si lograra comprenderse con algo de profundidad, podríamos dar cuenta que es una idea que postula al individuo por sobre todas las cosas, pero reconoce algo que al liberalismo le fastidia y le quita la máscara, y es que, sin intervención de las leyes, el individuo anarquista reconoce que lo colectivo es un nosotros expresado por cada individuo, por tanto, el valor más elevado es reconocer que sin libertad colectiva no habrá libertad individual.   ]

[Ojalá y se supere ese debate jurásico que ya murió. Que emerja con fuerza un digno rival del liberalismo, pues éste hoy arrasa aceleradamente al mundo, habla sin fundamento contra los unos y contra los otros, mantiene domesticada a la juventud con promesas de éxito, no reconoce a la historia, ignora la teoría, y se la pasa echándole la culpa de absolutamente todo, a dos moribundas como lo son hoy: la izquierda y la derecha.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
