Title: 45 masacres han sido perpetradas en lo que va del 2020: Indepaz
Date: 2020-08-25 22:21
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Homicidios colectivos, INDEPAZ, Iván Duque, Masacres, violencia
Slug: 45-masacres-han-sido-perpetradas-en-lo-que-va-del-2020-indepaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Mapa-de-las-masacres-2020.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Masacres-por-departamentos.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Familiares-de-una-de-las-victimas-de-la-masacres-de-Samaniego.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En medio del cruento escenario de violencia que se vive actualmente en el país; en el que **solo durante este fin de semana se perpetraran cuatro masacres, en los departamentos de Arauca, Cauca, Nariño y Antioquia, en las cuales fueron segadas las vidas de al menos 20 personas;** el Instituto de Estudios para el Desarrollo y la Paz -[Indepaz](http://www.indepaz.org.co/)- publicó un [informe](http://www.indepaz.org.co/wp-content/uploads/2020/08/Masacres-en-Colombia-2020-INDEPAZ.pdf) que documenta las masacres que se han registrado durante el año 2020 en Colombia. (Lea también: [Las razones que explican el regreso de las masacres a Colombia](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Considerando el más reciente hecho violento, en el que fueron asesinadas tres personas en el municipio de Venecia, Antioquia este domingo; **el informe documenta la ocurrencia de 45 masacres desde el 1° de enero hasta el 23 de agosto de 2020, en las que han sido asesinadas 182 personas.** (Le puede interesar: [Cuatro masacres en 48 horas, 3 jóvenes son asesinados en Venecia, Antioquia](https://archivo.contagioradio.com/cuatro-masacres-en-48-horas-3-jovenes-son-asesinados-en-venecia-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":88710,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Mapa-de-las-masacres-2020.png){.wp-image-88710}  

<figcaption>
Mapa de las masacres perpetradas en 2020 - Gráfico: Informe Indepaz

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Asimismo, Indepaz advierte que «*se entiende por masacre el homicidio colectivo intencional de tres (3) o más personas protegidas por el Derecho Internacional Humanitario (DIH), y en estado de indefensión, en iguales circunstancias de tiempo, modo y lugar*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el estudio **el departamento en el que se han presentado más masacres es Antioquia con nueve (9), seguido por Cauca y Nariño con siete (7) cada uno y Norte de Santander con cuatro (4).**

<!-- /wp:paragraph -->

<!-- wp:image {"id":88711,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Masacres-por-departamentos.png){.wp-image-88711}  

<figcaption>
Gráfico: Informe Indepaz

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Para construir el consolidado, Indepaz tiene en cuenta los reportes que realizan las comunidades y organizaciones sociales desde los territorios; las cifras de instituciones oficiales como la Defensoría del Pueblo, la Fiscalía General y el Ministerio de Defensa; y los reportes de organizaciones no gubernamentales -ONG- de índole nacional e internacional.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La postura negacionista del Gobierno Duque frente a las masacres  

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al término del Consejo de Seguridad adelantado en Chachagüí, Nariño, donde fue exigida la presencia del presidente Iván Duque, luego de la masacre perpetrada en Samaniego; lo primero que hizo el presidente, fue señalar que **los hechos violentos que se han venido presentando no eran «masacres», sino «*homicidios colectivos*»**; lo cual, fue criticado por múltiples sectores, señalando que Duque buscaba enfrascarse en una discusión semántica, mientras eludía y negaba el problema de fondo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/GSanchez2019/status/1297864331682549760","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/GSanchez2019/status/1297864331682549760

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Pese a los registros publicados por Indepaz, el presidente Duque insistió en que las masacres han disminuido en su gobierno, y en el Consejo de Seguridad adelantado el 22 de agosto en Chachagüí, señaló que en sus dos años de mandato se habían presentado «*37 homicidios colectivos*»; una cifra muy inferior a la presentada por Indepaz, e incluso por la de la Organización de Naciones Unidas  -ONU- que para el pasado 16 de agosto —sin que hubiesen ocurrido los hechos violentos registrados los últimos días— daba cuenta de la ocurrencia de 33 masacres y siete (7) más por documentar solo en lo que va del año 2020.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONUHumanRights/status/1295093058552377346","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONUHumanRights/status/1295093058552377346

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
