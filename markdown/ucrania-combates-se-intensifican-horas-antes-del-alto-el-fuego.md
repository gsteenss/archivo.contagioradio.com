Title: Ucrania: combates se intensifican horas antes del alto el fuego
Date: 2015-02-14 16:34
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Combates Ucrania, Donetsk, Lugansk, Ucrania
Slug: ucrania-combates-se-intensifican-horas-antes-del-alto-el-fuego
Status: published

###### **Foto:Almomento.mx** 

A pocas horas de comenzar **la tregua decretada este jueves en la capital de Bielorrusia, Minsk**, entre Rusia, Francia, Alemania y Ucrania, con el objetivo de poder pacificar la zona e iniciar un proceso de diálogos de paz, se han intensificado los combates y el fuego de artillería.

Para poder lograr posiciones, fuerzas separatistas prorrusas de la autoproclamada región de Donestk y efectivos del ejército regular ucraniano han **iniciado fuego cruzado esta misma tarde, poco antes de las 23:00**, hora decretada como inicio del alto el fuego.

El presidente ucraniano ha denunciado ante la comunidad internacional los ataques como un intento de ruptura de la tregua, igual ha sucedido con el presidente de la república de Donestk.

Quién ha su vez a asegurado que tienen **ha más de 6.000 soldados ucranianos cercados en la ciudad de Debáltsevo,** y que no los dejarán marchar si no deponen las armas en medio de la coyuntura de la tregua establecida.

En total han sido **18 los civiles asesinados y 120 loa ataques perpetrados entre los dos bandos.**
