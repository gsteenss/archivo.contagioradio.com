Title: Mujeres víctimas de la guerra interponen tutela en Medellín ante revictimización
Date: 2018-04-06 13:35
Category: Mujer, Nacional
Tags: Medellin, Mujeres víctimas del conflicto armado, violencia sexual
Slug: mujeres-victimas-guerra
Status: published

###### [Foto: Carmela María] 

###### [5 Abr 2018] 

En carne viva, Sofía, Ana, Inés, Lucía y Juana mujeres desplazadas y víctimas de violencia sexual han tenido que vivir los horrores de la guerra. Enterraron a sus esposos, padres e hijos asesinados por hombres alzados en armas. Situaciones que ocurrieron en las laderas de Medellín, o en otros rincones de Colombia, que fueron escenario de las escenas más crueles en 50 años de conflicto armado.

Hoy, más allá de ser víctimas, esas mujeres acompañadas de la Corporación Colectiva Justicia Mujer han iniciado la búsqueda queda de su reparación integral. Con ese propósito este miércoles denunciaron que en Medellín las mujeres víctimas de violencia sexual son revictimizadas.

De acuerdo con la denuncia las mujeres, que representan el 52% de las víctimas registradas por la Unidad de Atención y Reparación Integral a Victimas, sólo se les reconoce como desplazadas, y si señalan haber sido víctimas de violencia sexual, las autoridades les exigen pruebas de las violaciones ocurridas años atrás.

Ese panorama motivó la realización de una protesta pacífica frente al Edificio de la Rama Judicial en Medellín, y la presentación de una acción de tutela en la que indican las múltiples formas por las cuales las mujeres constantemente con revictimizadas.

### **Los argumentos en la tutela** 

Dicha acción jurídica, busca que el Estado y puntualmente instituciones como la  Unidad de Víctimas, la Alcaldía de Medellín, el Ministerio de Salud y Protección Social, la Gobernación de Antioquia y la Fiscalía General de la Nación, reconozcan estas mujeres como víctimas de violencia sexual y otros delitos por parte de actores armados, cumpla sus obligaciones de repararlas integralmente con enfoque de género y cese la revictimización. Asimismo, exigen protección inmediata del derecho a la salud física, mental, sexual y social de las víctimas.

Por otra parte, señalan su preocupación por los altos niveles de impunidad ceranos al 90%, frente a los actos violentos contra las mujeres, y especialmente, contra las lideresas sociales en todo el país. "Vamos a continuar realizando diversas acciones de movilización frente a las autoridades judiciales para que den respuesta a las mujeres" "Hay ausencia institucional para dar respuesta oportuna a las mujeres víctimas".

<iframe id="audio_25154272" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25154272_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
