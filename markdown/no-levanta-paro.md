Title: ¿Por qué aún no se levanta el paro estudiantil?
Date: 2018-10-31 17:29
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, estudiantes, Mesa de Concertación, Ministra, paro
Slug: no-levanta-paro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/Captura-de-pantalla-2018-10-31-a-las-5.03.17-p.-m.-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kiyoshi Katsukawa ] 

###### [19 Oct 2018] 

Estudiantes y representantes de Gobierno llegaron a un acuerdo para establecer una mesa de concertación, a realizarse este **jueves a las 11 a.m,** que permita abordar la crisis que enfrenta la educación superior pública, y brindar soluciones para el futuro. Sin embargo, representantes estudiantiles advirtieron que **el paro nacional continuará**, señalando que el movimiento por la educación superior es de largo plazo.

<iframe src="https://co.ivoox.com/es/player_ek_29759695_2_1.html?data=k56kl56afZahhpywj5WcaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncazV087bw5C6pc3Yxtffw9LFaZO3jNvcxcrWpYzYxpDZw5C5kqa5tJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Karina Valderrama, estudiante de la Universidad de la Amazonía y una de las voceras de la UNEES** que participó en el encuentro con el viceministro, sostuvo que estudiantes y Gobierno lograron un acuerdo sobre la agenda que le permitiera a las Instituciones de Educación Superior (IES) públicas salir de la crisis en la que se encuentran; por lo tanto, fue posible pactar la instalación de la mesa de negociaciones, presidida por la Ministra de Educación El encuentro que se realizará este jueves, estará encabezado por la ministra de educación **Maria Victoria Angulo.**

Según Valderrama, **concertar la mesa fue posible gracias a que se cumplieron las exigencias de los estudiantes:** que la Ministra estuviera presente, que hayan garantes internacionales y que tengan participación en la negociación docentes, estudiantes y rectores. Adicionalmente, la vocera de la UNEES resaltó que asesores expertos también acompañaran el proceso para que éste se desarrolle de la mejor forma.

### **Voceros nacionales de la UNEES se encontrarán este fin de semana** 

Valderrama indicó que en la mesa de concertación, los estudiantes buscarán espacios para poder dialogar sobre la situación general que vive la educación pública, en esa medida, esperan articular el proceso con las demandas de todo el gremio del sector educativo incluyendo a FECODE. (Le puede interesar: ["Las 3 condiciones de los estudiantes par ala mesa de concertación con el Gobierno"](https://archivo.contagioradio.com/condiciones-mesa-de-concertacion/))

La integrante de la UNEES adelantó además que el informe sobre lo que ocurra el jueves y las decisiones sobre el futuro inmediato del movimiento, se presentará en el **encuentro de delegados que está programado para este fin de semana en Quindío. ** Según Ana María Nates, integrante del mismo movimiento, las decisiones se tomarán basadas en la "voluntad política para negociar" por parte del Gobierno.

<iframe src="https://co.ivoox.com/es/player_ek_29759720_2_1.html?data=k56kl56bdpGhhpywj5WcaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncaLiwpC6w9eJh5SZoqnOjbPFuMbnhpewjdvTp8bmwpDRx5DQpYzJr6qytZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Sin embargo, Nates aclaró que l**a finalidad del paro no es negociar su fin, sino solucionar los problemas que enfrenta la educación superior pública.** Por lo tanto, hasta que no haya un acuerdo con el Gobierno, las medidas de anormalidad académica continuarán. Al respecto, Valderrama indicó que tras el acuerdo entre los rectores de las IES públicas y el presidente Duque "se ha venido intentando afectar la movilización", sin embargo, pidió que se respeten las formas de protesta estudiantil.

**"Esperamos superar el coyunturalismo y ser un movimiento de largo aliento"**

Nates expresó que l**a mesa de concertación no se limitará al tema financiación porque son igual de relevantes la democracia, la autonomía universitaria, el bienestar y la revisión de la Ley 30 de educación superior.** Adicionalmente, afirmó que cada IES tiene su propio pliego local de exigencias, lo que significa que hay reivindicaciones específicas por institución.

Por estas razones, la integrante de la UNEES aseguró que de ahora en adelante, **el movimiento tendrá mayor énfasis en lo local**, esperando que las fuerzas de base que nutren la movilización estudiantil se mantenga y de esta forma, "superar el coyunturalismo y construir un movimiento de largo aliento". (Le puede interesar: ["¿Por qué están marchando los estudiantes?"](https://archivo.contagioradio.com/marchando-los-estudiantes/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
