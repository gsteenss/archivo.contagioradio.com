Title: Militares condenados podrían acceder a la Jurisdicción Especial de Paz
Date: 2015-12-16 17:48
Category: Entrevistas, Paz
Tags: conversaciones, Corte Penal Internacional, Crímenes cometidos por militares, jurisdicción especial para la paz, Operacion genesis
Slug: militares-en-la-jurisdiccion-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/jurisdiccion-especial-de-paz-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: posta.com.mx 

<iframe src="http://www.ivoox.com/player_ek_9755181_2_1.html?data=mpyil5acdY6ZmKialJiJd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcrgytnO1MrXb8Tjz8nS0MbIs9Sf0dTR1Iqnd4a1pcbbjcbHp8bYxteYw5DQpYy%2B1tfW1cnNp8Tdhqigh6eXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Enrique Santiago] 

###### [16 Dic 2015]

Aunque el acuerdo firmado el 23 de Septiembre no ha sufrido cambios sustanciales con el publicado este martes uno de los cambios está en el mecanismo de juzgamiento de los expresidentes y la posibilidad de la amnistía para personas condenadas o en procesos judiciales que no son combatientes pero están acusadas de delitos políticos.

Para los ex presidentes se establece un régimen en el que el tribunal especial para la paz remitirá a la **Comisión de Acusaciones de la Cámara** los procesos contra ex presidentes que lleguen a esa instancia. Sin embargo, según Enrique Santiago, integrante del equipo de juristas que elaboró el acuerdo, si la Comisión de Acusaciones continúa sin operar de manera efectiva podría intervenir la **Corte Penal Internacional**.

Según el abogado se pueden dar dos posibilidades para los presidentes y expresidentes, una de ellas que simplemente llegue a esa Comisión una acusación sin fundamento y otra, es que resulte a la inversa, es decir, que lo que se remita esté bien fundamentado y que la Comisión de Acusaciones no surta el trámite debido *“quedaría abierta la posibilidad de actuación de la Corte Penal Internacional respecto a ellos únicamente*”.

Otro de los asuntos novedosos dentro de este sistema es que se establece la posibilidad de **amnistía para personas condenadas o procesadas con delitos relacionados al delito político, es decir campesinos, defensores de DDHH u otros que no son combatientes.** Además afirma Enrique Santiago que el tratamiento que reciban integrantes de las FFMM pueden recibir un tratamiento distinto pero equivalente a la amnistía y aclara que no se va a sustraer a ningún funcionario público de la JEP.

Para ejemplificar la situación de militares condenados Enrique Santiago explica que el caso del **General Rito Alejo del Rio, condenado por el asesinato de Marino López en el marco de la Operación Génesis**, hay varias posibilidades, una de ellas que se acoja al sistema, admita la responsabilidad y cuente toda la verdad, en ese caso podría optar a la reconversión de su sentencia y acceder a  penas restaurativas contempladas en la JEP.

Frente a los delitos mal llamados **“Falsos Positivos” o ejecuciones extrajudiciales el jurista explica que podrían contemplarse dentro de la Jurisdicción Especial** siempre y cuando el Sistema reconozca estos crímenes como relacionados con el conflicto, es decir, pasarían de ser homicidio en persona protegida a un delito en el marco del conflicto armado.
