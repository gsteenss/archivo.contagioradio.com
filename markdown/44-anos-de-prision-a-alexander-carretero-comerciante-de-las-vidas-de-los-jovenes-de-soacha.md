Title: 44 años de prisión a Alexander Carretero, comerciante de las vidas de los jóvenes de Soacha
Date: 2017-09-11 13:23
Category: DDHH, Nacional
Tags: alezander carretero, crímenes de estado, Ejecuciones Extrajudiciales, jóvenes de soacha
Slug: 44-anos-de-prision-a-alexander-carretero-comerciante-de-las-vidas-de-los-jovenes-de-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Madres-de-Soacha-e1505154647984.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Información] 

###### [11 Sept 2017]

Se realizó la audiencia de condena contra Alexander Carretero, **responsable de engañar a 17 jóvenes de Soacha con falsas promesas de trabajo**, con el fin de venderlos a integrantes del ejército que luego los ejecutaron y presentaron como personas muertas en combate. Además, habría un acuerdo entre Carretero y la Fiscalía para entregar información sobre los militares implicados en estos hechos.

En la audiencia, Carretero aceptó su responsabilidad en los hechos y fue sentenciado a pagar 528 meses de cárcel y una multa de 1.446 salarios mínimos vigentes.

En los juzgados Especializados de Cundinamarca, se desarrolló la audiencia que pretende ser uno de los puntos de partida **para las investigaciones en contra de cerca de 48 militares** implicados específicamente en los asesinatos de los jóvenes de Soacha.

Según los familiares de los jóvenes asesinados, estos casos estarían fuera del conflicto armado puesto que **se cometieron sencillamente como un negocio de venta de personas** a cambio de dinero y de contraprestaciones como permisos o ascensos para los militares. (Le puede interesar:["Soldados prófugos en casos de "falsos positivos" podrán acceder a JEP" Corte Suprema"](https://archivo.contagioradio.com/44422/))

**Carretero es conocido como el reclutador**

En 2008, según las declaraciones de Carretero ante el juez Segundo Especializado de Bogotá y publicadas en el diario El Espectador, **él reclutó en Soacha a diferentes jóvenes y pidió un millón de pesos por cada uno.** Los jóvenes fueron engañados y llevados a lugares como Ocaña en Norte de Santander por orden de los militares quienes posteriormente los asesinaron.

Carretro, aseguró que el sargento Dayro Palomino **lo contactó en Bogotá para reclutar a los jóvenes.** Al reclutador, le pagaban los gastos de los viajes para llevar a personas como Farid Leonardo Porras, joven de 26 años, hasta Santander donde los esperaban los militares. (Le puede interesar:["Falsos positivos" son crímenes de lesa humanidad"](https://archivo.contagioradio.com/falsos-positivos-son-crimenes-de-lesa-humanidad/))

Las ordenes de los sargentos de Ocaña para Carretero fueron reclutar a personas jóvenes y le pedían que fueran entre dos y tres personas cada vez que lo llamaban. **Él dijo que, en lo posible, intentaba buscar a personas habitantes de calle** no solo de Soacha sino también en los departamentos de Cesar y Santander.

### **Víctimas temen que procesos de "falsos positivos" entren a la JEP** 

Según las organizaciones de DDHH y las víctimas de estos hechos, una de las necesidades en el proceso judicial, es que **se impida que los casos de los mal llamados "falsos positivos" entre a la Jurisdicción Especial para la Paz**. Esto pues, esto sería uno de los intereses de los victimarios para lograr tener beneficios judiciales.

La Sala Penal de la Corte Suprema de Justicia había manifestado en julio del presente año, que hay unas condiciones para que los **casos de ejecución extrajudicial hagan parte de la Jurisdicción Especial para la Paz**, dando beneficios a quienes hicieron parte de estos delitos.

### **Impunidad en casos de ejecución extrajudicial** 

De acuerdo con Human Rights Watch **existe la posibilidad de que los militares investigados por estos casos puedan evitar cualquier condena** si convencen a los tribunales de que no tenían conocimiento ni control sobre los delitos que se estaban cometiendo. Esto dejaría en impunidad a los cerca de 3.000 casos de civiles ejecutados durante 2002 y 2008. (Le puede interesar: ["Juez noveno de garantías "se burla" de las víctimas de ejecuciones extrajudiciales"](https://archivo.contagioradio.com/juez-noveno-de-garantias-se-burla-de-las-victimas-de-ejecuciones-extrajudiciales/))

Finalmente, **hoy se encontrarán los 17 casos que hacen parte de la investigación contra Alexander Carretero de manera unificada**. Esto en la medida que habían sido expuestos en diferentes audiencias de manera separada o por grupos. El reclutador enfrenta cargos por los delitos de desaparición forzada y homicidio agravado, y según las organizaciones sociales será condenado a 44 años de prisión.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
