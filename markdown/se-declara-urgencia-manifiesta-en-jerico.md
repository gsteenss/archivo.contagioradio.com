Title: Jericó en emergencia y calamidad pública tras avalancha
Date: 2019-11-05 17:13
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, avalancha, calamidad pública, Derrumbe, Jericó Antioquía
Slug: se-declara-urgencia-manifiesta-en-jerico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/EIiF4HPWsAIrQT9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@DapardAntioquia] 

[Luego del fuerte deslizamiento de tierra en **Jericó, Antioquia**,  que causó una avalancha y desplazó grandes volúmenes de rocas, material vegetal y escombros este 3 de noviembre, el gobernador de este departamento Luis Pérez ha declarado Urgencia Manifiesta y Calamidad Pública, acción que pone como una condición urgente la ejecución de contratos de suministro de bienes, prestación de servicios y ejecución de obras en el  menor tiempo posible que garantice la seguridad y estabilidad de los habitantes.]

[El desbordamiento de la quebrada La Cascada hasta el momento ha dejado 17 sectores de la localidad afectados, evacuación de 1.000 habitantes y 2 personas lesionadas; según Fernando Jaramillo integrante de la Mesa Ambiental de Jericó, "esto hace parte de un desastre natural que afortunadamente no causó más que daños materiales, la acción ahora es empezar estudios geológicos con expertos que determinen que fue lo ocurrido y se trabaje por evitar nuevos desastres". (Le puede interesar: [No fue un &\#171;desastre natural&\#187; la tragedia en Mocoa](https://archivo.contagioradio.com/no-fue-desastre-natural-lo-ocurrido-mocoa/))]

Así mismo Jaramillo desatacó la atención oportuna de organizaciones como el Departamento Administrativo del Sistema de Prevención Atención y Recuperación de Desastres (DAPARD), la  Gobernación de Antioquia y la constructura Pacífico dos, "[hemos contado con un contingente de soldados que han estado ayudando después de los deslizamientos acá en Jericó, también hemos sentido toda la solidaridad de parte del estado y sobre todo de la ciudadanía que ha estado muy pendiente de nosotros, enviando ayudas alimentarias y de protección". ]

> "pedimos asistencia de las entidades gubernamentales porque tememos que tras una fuerte lluvia se den más desplazamientos de tierra, por eso pedimos atención reformas y refuerzos para que de seguir el invierno no se presente una situación de mayor gravedad"  Fernando Jaramillo

[En la comunidad de Jericó hay  varias necesidades pero la principal según Jaramillo es la  reconstrucción de  las viviendas que quedaron completamente derruidas, destacando que desde varios meses se han venido realizando estudios en la montaña evitando situaciones como la que se dio el sábado, "nosotros hemos visto desde hace varios años que un sector de la montaña que ha mostrado deslizamientos, y ante ellos ya hay estudios realizados liderados por el DAPARD, quienes han estado muy pendiente; de hecho los dineros ya están para ejecutar estos proyectos solo falta dar inicio". ]

[Sin embargo para el integrante de la Mesa Ambiental de Jericó esta no es la única medida que se debe aplicar, "es necesario por parte de los organismos  del estado hacer un diagnóstico de toda la montaña, ver en qué estado se encuentra y hacer obras que mitiguen este impacto, no solo hacer terrazas de contención sino adecuar las  viviendas de quienes están más cercanos a la montaña". (Le puede interesar:[Cambio Climático afecta con mayor fuerza a la población vulnerable del mundo](https://archivo.contagioradio.com/cambios-climaticos-afectan-con-mayor-fuerza-a-la-poblacion-vulnerable-del-mundo/))]

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44239827" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44239827_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo] 
