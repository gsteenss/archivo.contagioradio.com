Title: Ascienden a 12 las familias desplazadas en San José de Uré por confrontación entre grupos armados
Date: 2019-07-18 16:01
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Desplazamiento Forzoso, San Jose de Ure, Sur de Córdoba
Slug: ascienden-a-12-las-familias-desplazadas-en-san-jose-de-ure-por-confrontacion-entre-grupos-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Desplazamiento_Apartado-e1553374753387.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

El pasado 16 de julio, 12 familias provenientes de la vereda El Cerro en San José de Uré,  dejaron sus viviendas en el marco de una posible confrontación entre el **Clan del Golfo, los Caparrapos y disidencias de las FARC,** quienes se vienen disputando el territorio, y a su paso cobrando la vida de campesinos, como sucedió el pasado 6 de julio cuando en El Cerro también fue asesinado Manuel Osuna de 67 años.

Andrés Chica, defensor de derechos humanos e integrante de la Fundación Cordoberxia informa que posterior al asesinato de Manuel Osuna, **los lugares de tránsito de la población fueron sembrados con minas antipersonal y dos casas más fueron quemadas,** lo que llevó a que las familias tomaran la decisión de desplazarse hasta la cabecera municipal. [(Le puede interesar: Denuncian asesinato del campesino Manuel Osuna Tapias en San José de Uré)](https://archivo.contagioradio.com/asesinato-manuel-osuna/)

Chica expresa con preocupación que el fenómeno del desplazamiento se ha naturalizado de tal forma en el sector, que el municipio ha adaptado albergues donde fueron recibidas las personas desplazadas que ahora aguardan a que las autoridades acudan. **Cabe resaltar que sobre San José de Uré existe una serie de advertencias por parte de la Defensoría del Pueblo, consignadas en la Alerta Temprana 071-18 emitida el 10 de septiembre del 2018.**

### ¿Hay algo más detrás de los desplazamientos en San José de Uré ?

Aunque todo apunta a que se trata de una disputa territorial entre el paramilitarismo la que estaría obligando a los campesinos a abandonar el territorio aledaño al Parque Nacional Nudo del Paramillo, para Andrés Chica también podrían estar de por medio los intereses de multinacionales que buscan explotar el territorio y represar el río San Jorge con fines de hacer un proyecto hidroeléctrico  similar a Hidroituango o a la represa Urra I.

"Hemos colocado dos mapas, el de las multinacionales que tienen interés en la zona y otro de la presencia paramilitar", explica Chica, quien sugiere que **"detrás de ese telón, hay acciones para sacar a la gente y permitir avanza esos proyectos"**. [(Lea también: Denuncian desplazamiento de por lo menos 200 familias al sur de Córdoba)](https://archivo.contagioradio.com/cuerpo-campesino-desaparecido-cordoba/)

### Crecen las amenazas contra líderes y minorías

Chica alertó además sobre el aumento de amenazas en contra de líderes sociales en el departamento, una cifra que hasta el fin de semana llegaba a 70 líderes amenazados y a la fecha asciende a 74. Esta situación se suma a la estigmatización que existe contra la comunidad LGBTI, a quienes los grupos armados han identificado, colocándoles letreros con frases peyorativas y obligándoles a realizar labores como la limpieza de las calles, una situación que denota la crisis humanitaria que se vive en el territorio.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38663109" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38663109_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
