Title: Prográmese con las iniciativas de Paz a La Calle
Date: 2016-10-07 14:46
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Campamento por la Paz, Paz a la Calle
Slug: programese-con-las-iniciativas-de-paz-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/LRG__DSC8399-01-e1475869527652.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:PazALaCalle] 

###### [7 Oct de 2016] 

Luego de la Movilización del miércoles, un grupo de 30 personas instaló en la Plaza de Bolívar el Campamento Permanente por la Paz, el objetivo es presionar a las delegaciones negociadoras y dar continuidad de forma más duradera a las iniciativas planteadas desde la Asamblea Ciudadana de \#PazALaCalle para **[exigir una pronta concertación e implementación de los Acuerdos.](https://archivo.contagioradio.com/cese-bilateral-plazo-para-que-no-retorne-la-guerra/)**

Juana Ruíz una de las participantes del campamento, manifestó que la mención del Nobel de Paz a Santos “[significa mas fuerza y es una](https://archivo.contagioradio.com/guardia-indigena-y-cimarrona-serian-garantes-de-acuerdo-de-paz-en-sus-territorios/)[invitación a que todos nos unamos](https://archivo.contagioradio.com/guardia-indigena-y-cimarrona-serian-garantes-de-acuerdo-de-paz-en-sus-territorios/) a esa **presión de la comunidad internacional para obtener los ajustes de manera pronta”**. Afirmó que se trata de una iniciativa independiente y que ninguna organización la encabeza.

Durante la jornada del jueves, desarrollaron actividades informativas sobre los acuerdos a través de música y danza**.** Señaló que han tenido bastante acogida por parte de habitantes del sector y transeúntes**,** quienes les han apoyado con alimentos y participado de las actividades propuestas. Manifestó que se encuentran bien aunque hay algunas cosas que les hacen falta, como sanitarios, elementos de aseo, cobijas y fuentes de electricidad. Sin embargo estas necesidades se han podido solventar con la solidaridad de los comerciantes del sector.

Además la Asamblea Ciudadana celebrada el pasado Jueves en el Park Way definió los siguientes puntos:

1.  Exigir cese bilateral definitivo y la ratificación de los Acuerdos
2.  La movilización continúa.
3.  Se harán réplicas de movilizaciones en las regiones
4.  Se debe proteger el enfoque de género
5.  Se realizará una campaña llamada ¿Por qué voté el si?
6.  Se creará un comité artístico para dinamizar la campaña
7.  Se brindará apoyo al campamento de la Plaza de Bolívar
8.  Nos unimos a la movilización indígena del 12 de octubre
9.  Se hará una “Lecturaton” de los Acuerdos
10. Se mantiene el espacio en las calles para continuar la pedagogía de la paz
11. Debemos crear un símbolo para nuestras acciones
12. Haremos tomas pacíficas del Transmilenio y Centros Comerciales
13. Acompañaremos la marcha campesina el 15 de octubre
14. Apoyaremos con cobijas y enseres a quienes están acampando en la Plaza de Bolívar
15. Haremos Asambleas en todas las localidades
16. Nos reuniremos todos los Lunes a las 7pm hasta que se defina la situación de los Acuerdos

Otros eventos relacionados con las propuestas desde \#PazALaCalle en distintas ciudades del país para los próximos días son:

**<u>Viernes 7 octubre  
</u>Bogotá  
**11:00 am Concentración en la Plaza de Bolívar

5:00   pm Respaldo Nobel a la Paz Colombia, tapete de flores blancas Plaza de Bolívar

6:00 pm Asamblea local de Paz a la Calle. Plazoleta de Banderas, localidad Kennedy

**Medellín  
**3 – 5 pm Plantón

6:00 pm Marcha del silencio desde Parque de los Deseos o Parque de las Luces  
Actividades: Ensamble musical, cartas de perdón y actos de reconciliación

**Barranquilla  
**9:00 am Manifestación Pacífica Respaldando Las Negociaciones de paz. Plaza de la Paz

**Valledupar  
**3:30 pm Plantón en apoyo a los acuerdos de paz frente a la Gobernación

**Ibagué  
**7:00 pm Encuentro por la Paz. Universidad del Tolima

**Armenia Quindío  
**5:00pm Marcha por la Paz (todos de blanco) Parque Sucre

**San Gil - Santander  
**6:00 pm Encuentro por la Paz Parque Principal

**Madrid España  
**8:00 pm Marcha Vamos de Blanco por la paz. Parque El Retiro Monumento Alfonso XII

**Calgary Canadá  
**6:00pm Concentración City Hall

**Edimburgo Escocia  
**6:30pm Vigilia con velas por la paz St Giles´Cathedral High St, Edinburgh

**<u>Sábado 8 octubre  
</u>Medellín  
**2:00 pm Asamblea ciudadana de Paz. Casa del Museo  
2- 5 pm Reunión con el fin de establecer propuestas y agenda de movilización Teatro Matacandelas

**Barranquilla  
**4:00 pm Primera Asamblea Deliberativa Paz a la Calle Barranquilla Parque del Sagrado Corazón

**Zipaquirá  
**4:30 – 7 pm Encuentro Ciudadano para decir Guerra Nunca Más Parque de la Esmeralda Zipaquirá

**Melbourne**

Plantón por la paz a las 8 de la mañana en Federation Square

**<u>Domingo 9 octubre  
</u>Bogotá  
**10:00am Celebración ecuménica en apoyo al Campamento por la Paz instalado en la Plaza de Bolívar

**Cali  
**10: 00 am Camino a la Paz desde el Parque de las Banderas hasta la Plazoleta de San Francisco

**Nueva York  
**2:00 pm Reunidos por la Paz de Colombia. Union Square

**Barcelona España  
**5:00 pm Concentración por la Paz Plaza Catalunya

**<u>[Lunes 10 Octubre  
]</u>Bogotá  
**3:00 pm La Sierra camina por la Paz. Lugar de inicio de la caminata Centro de Memoria, Paz y Reconciliación hasta la Plaza de Bolívar

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
