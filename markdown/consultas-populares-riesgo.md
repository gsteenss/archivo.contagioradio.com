Title: ¿Por qué las Consultas Populares están en riesgo?
Date: 2018-09-24 17:42
Author: AdminContagio
Category: Ambiente, DDHH
Tags: consulta popular en cajamarca, consultas populares, Corte Constitucional
Slug: consultas-populares-riesgo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/C5mq01zXQAAW9sF.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: valentinacmpm] 

###### [24 Sep 2018] 

A los mecanismos de participación los quieren limitar desde dos frentes diferentes: por una parte, **la Corte Constitucional** con el pronunciamiento que realice este jueves frente al caso **Mansarovar Energy vs la Consulta Popular de Cumaral;** y por otra con el Proyecto de Ley presentado por la bancada de **Cambio Radical **que busca poner limites de tiempo a las Consultas Previas.

El **ambientalista y abogado Rodrigo Negrete,** afirmó que la Consulta Popular es un mecanismo con un alcance como ninguno, porque **le permite a la ciudadanía determinar lo que ocurre en su territorio.** Sin embargo, la Corte Constitucional en su 5ª sala plena podría pronunciarse sobre una limitación a este mecanismo de participación. (Le puede interesar: ["Cumaral le dijo no a la explotación minera en en su territorio"](https://archivo.contagioradio.com/cumaral-le-dijo-no-a-la-exploracion-y-explotacion-minera/))

Negrete sostuvo que la ponencia de la magistrada **Cristina Pardo,** está orientada para limitar las Consultas Populares, hecho que en el fondo se traduciría en la imposibilidad para ejecutar este mecanismo de decisión sobre temas relacionados con el extractivismo. (Le puede interesar: ["La discusión sobre las C. Populares en la Corte Constitucional"](https://archivo.contagioradio.com/consultas-popular-la-disputa-en-las-altas-cortes/))

El abogado expresó sus reparos sobre ese pronunciamiento de la Corte debido a que la cuestión de las Consultas Populares era un asunto resuelto. Adicionalmente, existen dudas sobre la idoneidad de la magistrada Pardo ponente para referirse al tema, porque podría estar impedida para tomar decisiones, por su trabajo como asesora en la materia durante el Gobierno Santos.

El ambientalista también aseveró que **al Gobierno Nacional le interesa restringir aquellas consultas que afectan su política macroeconómica,** es decir, aquellas que  tienen que ver con minería, explotación de hidrocarburos o la construcción y operación de hidroeléctricas. (Le puede interesar: ["Comunidades exigen a la Corte Constitucional no modificar C. Populares"](https://archivo.contagioradio.com/se-alista-gran-movilizacion-en-defensa-de-las-consultas-populares/))

### **El panorama para las Consultas está oscuro con esta Corte** 

Para Negrete, lo que estamos viendo con la actual Corte Constitucional es que está 'tumbando' fallos que el mismo tribunal había emitido antes, en razón de que **los actuales magistrados provienen del interior del Gobierno o del sector minero-energético:** El presidente de la Corte es Alejandro Linares, ex-vicepresidente jurídico de Ecopetrol; Cristina Pardo, secretaría jurídica de Santos; y Diana Fajardo, directiva de la Agencia de Defensa Jurídica del Estado.

Una muestra de la tendencia que prevalece en la actual Corte, fue la reciente decisión para revertir la exigencia de indemnización de 'Cerromatoso' a las comunidades afectadas por su actividad, en las cercanías de la mina que opera en Córdoba. Negrete señaló que este hecho marca el nuevo rasero, así como el uso del mecanismo de nulidad para revertir decisiones que el mismo tribunal ha tomado.

### **Cambio Radical presentó Proyecto de Ley para limitar la Consulta Previa** 

Referente a la Consulta Previa, Negrete recordó que la Corte Constitucional ya ha emitido diversos fallos desarrollando la forma en que debe realizarse este proceso, y reconociéndolo como un mecanismo importante para garantizar los derechos fundamentales de las comunidades. (Le puede interesar: ["Qué pasará con las C. Populares en Colombia"](https://archivo.contagioradio.com/que-pasara-con-las-consultas-populares-en-colombia/))

Lo que pretende **Cambio Radical es restringir la aplicación de la Consulta Previa únicamente a pueblos indígenas, tribales, afrocolombianos, roms, palenqueros y raizales de forma directa.** Es decir, que no concibe los efectos indirectos de la explotación minera en el país, adicionalmente, **fija a un año el desarrollo de la Consulta con la posibilidad de prorrogarlo por 6 meses más**, desconociendo los tiempos y procesos de algunas comunidades.

### **¿Qué queda entonces?** 

Negrete sostuvo que la Corte Constitucional no acabaría con las Consultas porque son un mecanismo que está en la Carta de Derechos colombiana, pero las limitaran de forma tal que sea imposible realizarlas. Por eso, aseguró que es necesario hacer uso de las acciones de tutela, populares y de nulidad para defender los derechos fundamentales de las comunidades; y en caso de agotar las instancias nacionales, **acudir al sistema interamericano de derechos humanos.**

<iframe id="audio_28824635" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28824635_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
