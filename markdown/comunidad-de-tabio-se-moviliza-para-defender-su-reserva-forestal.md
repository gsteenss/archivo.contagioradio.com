Title: Comunidad de Tabio se moviliza para defender su reserva forestal
Date: 2015-04-10 16:38
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Agencia Nacional de Licencias Ambientales, Ambiente, ANLA, CAR, Chivor II, Corporación Autónoma Regional, Empresa de Energía de Bogotá, licencias ambientales, municipio, Tabio
Slug: comunidad-de-tabio-se-moviliza-para-defender-su-reserva-forestal
Status: published

##### Foto: Comunidad de Tabio 

<iframe src="http://www.ivoox.com/player_ek_4335948_2_1.html?data=lZigl56YfI6ZmKiak5mJd6KpkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dh1tPWxsbIb8XZjLnOxM7Tb9TZjNLc2M7QrdvV04qwlYqldYzX0NPh1MaPp9Di1Nnf18jHrYa3lIqvldOPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alexander Fonseca, líder comunitario de Tabio] 

Tras la posible construcción de **260 torres de energía en plena reserva forestal en el municipio de Tabio**, la comunidad se moviliza este viernes con el objetivo de alzar su voz en contra la forma como la Empresa de Energía de Bogotá (EEB), está llevando a cabo los procedimientos para hacer posible el proyecto de energía Chivor II.

“Somos conscientes de la importancia del proyecto energético para el país, no tenemos problema contra el proyecto, sino la forma como la empresa está llevando a cabo los estudios y las variables que tienen que tener en cuenta para pasar las líneas de alta tensión”, denuncia Alexander Fonseca, líder comunitario del municipio, quien agrega que la empresa no está manejando una información clara sobre el proyecto, que “**a todas luces lesiona a la comunidad, el ambiente, la propiedad privada entre otras implicaciones**”.

**De acuerdo a Fonseca, la manifestación no quiere tomar tintes políticos**, simplemente las personas que habitan ese municipio y los que viven a sus alrededores, quieren defender la reserva forestal, es por eso que se congregarán en la Variante para marchar hasta el casco urbano de Tabio.

El principal reclamo de los pobladores, es que la **Agencia Nacional de Licencias Ambientales (ANLA) no los ha escuchado** y además se han sentido poco apoyados por las autoridades que deben proteger los recursos naturales. “¿Por qué no se pronuncian, y por qué dicen que la ruta más viable es la reserva forestal?”, expresa el líder comunitario, que resalta la falta de acompañamiento de la **Corporación Autónoma Regional, CAR,** que había emitido un concepto donde se evidencia la importancia de la reserva forestal, afirmando que no conviene pasar líneas de alta tensión por esa zona.

El proyecto denominado “Construcción de la línea de transmisión CHIVOR-CHIVOR II – NORTE – BACATÁ A 230 KV”, contempla un tendido con 260 torres de energía en un **perímetro de 8 kilómetros del municipio, cada torre a 30 metros de distancia.**

Entre las consecuencias negativas que más le preocupa a la comunidad, es la afectación al bosque nativo que le da vida a las fuentes hídricas que abastecen de agua al municipio. Además, en la reserva forestal hay especies endémicas y en vías de extinción. [(Ver nota relacionada).](https://archivo.contagioradio.com/empresa-de-energia-planea-construir-tendido-de-torres-en-plena-reserva-forestal-de-tabio/)
