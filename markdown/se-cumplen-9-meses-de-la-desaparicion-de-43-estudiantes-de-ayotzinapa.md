Title: Se cumplen 9 meses de la desaparición de 43 estudiantes de Ayotzinapa
Date: 2015-06-26 13:20
Category: El mundo, Otra Mirada
Tags: Ayotzinapa, cerezo, comite cerezo, enrique peña, guerrero, hector, iguala, mexico, nieto, normalistas, peña, peña nieto
Slug: se-cumplen-9-meses-de-la-desaparicion-de-43-estudiantes-de-ayotzinapa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Ayotzinapa9meses.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#####  

<iframe src="http://www.ivoox.com/player_ek_4692514_2_1.html?data=lZumlJqVeI6ZmKiakpuJd6Kpl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjNDtjNjSjcjZsdHgxtOYm5DRqdTZ1JDRx5DQpYzYxtjO0sbWrcTdhqigh6eXsozYxpChlZDJt9Xpxc6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Hector Cerezo, Comité Cerezo] 

###### [26 jun 2015] 

Este 26 de junio se cumplen 9 meses desde que 43 estudiantes de la Normal Rural de Ayotzinapa, en el Estado de Guerrero en México, fueran desaparecidos por el grupo paramilitar Guerreros Unidos en presunta colaboración con la policía y el ejercito mexicano.

El gobierno de Enrique Peña Nieto y la Procuraduría emitieron una versión oficial de los hechos, según la cual los estudiantes fueron asesinados y posteriormente calcinados en un basurero en las afueras de Iguala, hechos por los cuales habrían sido capturadas 108 personas incluyendo 74 policías municipales de Iguala y Guerrero.

Sin embargo, investigaciones independientes adelantadas por la Universidad Nacional Autónoma de México, el grupo de Antropología Forense de Argentina, Periodistas e incluso los mismos familiares de los estudiantes, han demostrado que la versión oficial del gobierno es técnicamente imposible.

"Para la Procuraduría General de la República el caso está cerrado, pero tenemos 9 meses en que no ha habido acceso al Derecho a la verdad", asegura Héctor Cerezo, del Comité de DDHH Cerezo. "Se sigue sin saber realmente que pasó, quienes fueron los responsables no solo materiales sino intelectuales y a quien benefició ese hecho". Para el defensor de Derechos Humanos, esta impunidad obedece a que "en México el Ejercito es intocable, y no se les ha podido llamar a que declaren a quienes estuvieron operando esa noche del 26 de junio y durante la madrugada, y quienes incluso pudieron fotografiar a los estudiantes sobrevivientes".

A ello habría que agregar que  en los juicios que se adelantan contra las 108 personas capturadas no se imputa el delito de desaparición forzada, revictimizando a los familiares de los estudiantes y a la población mexicana que sufre por cuenta de este flagelo.

"La apuesta del gobierno es el olvido", asegura Cerezo, y como respuesta, las familias de los estudiantes y organizaciones de Derechos Humanos han convocado a una jornada de movilización y reflexión entre el 26 y el 29 de junio, con marchas en diferentes ciudades del país centroamericano. "Van a ser tres días para exigir el derecho a la verdad, a la justicia. Esta jornada es para evitar el olvido, y para que las autoridades no sientan que ya no hay presión por parte de la sociedad", concluyó el defensor de Derechos Humanos.

\[caption id="attachment\_10548" align="alignleft" width="269"\][![Programación viernes](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PROGRAMA-VIERNES.png){.wp-image-10548 width="269" height="373"}](https://archivo.contagioradio.com/se-cumplen-9-meses-de-la-desaparicion-de-43-estudiantes-de-ayotzinapa/programa-viernes/) Programación viernes\[/caption\]  
\[caption id="attachment\_10550" align="alignright" width="269"\][![Programación domingo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PROGRAMA-DOMINGO.png){.wp-image-10550 width="269" height="372"}](https://archivo.contagioradio.com/se-cumplen-9-meses-de-la-desaparicion-de-43-estudiantes-de-ayotzinapa/programa-domingo/) Programación domingo\[/caption\]  
   
   
   
   
   
   
   
   
   
   
\[caption id="attachment\_10549" align="aligncenter" width="1407"\][![Programación sábado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/PROGRAMA-SABADO.png){.size-full .wp-image-10549 width="1407" height="1185"}](https://archivo.contagioradio.com/se-cumplen-9-meses-de-la-desaparicion-de-43-estudiantes-de-ayotzinapa/programa-sabado/) Programación sábado\[/caption\]
