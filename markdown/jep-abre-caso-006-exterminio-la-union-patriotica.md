Title: JEP abre el caso 006 sobre el exterminio de la Unión Patriótica
Date: 2019-03-05 14:34
Author: CtgAdm
Category: Judicial, Paz
Tags: Crimen de Estado, Unión Patriótica
Slug: jep-abre-caso-006-exterminio-la-union-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/union-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [4 Mar 2019] 

La Jurisdicción Especial para la Paz (JEP) anunció la apertura de un nuevo caso denominado "Victimización de miembros de la Unión Patriótica (UP) por parte de agentes del Estado". Según la Corporación Reiniciar y el Centro Nacional de Memoria Histórica (CNMH), el exterminio de este partido político de izquierda afectó a **más de 6.000 víctimas,** entre 1984 y 2002, quienes eran dirigrentes, militantes y simpatizantes de la UP.

Este caso se basa en los tres informes presentados a la JEP por la Fiscalía General de Nación, la Corporación Reiniciar y el CNMH. En el informe de la Fiscalía, este organismo reconoció que "**las investigaciones en la justicia ordinaria han sido insuficientes para establecer los autores de estos hechos**". Hasta la fecha, existen 863 investigaciones de crimenes relacionados con la UP, cuales vinculan a 277 agentes del Estado como los presuntos responsables. (Le puede interesar: "[Genocidio de la UP irá a la Corte Interamerican de DDHH](https://archivo.contagioradio.com/genocidio-de-la-up-ira-a-la-corte-interamericana-de-ddhh/)")

### **Las acciones de la Fiscalía por encontrar la verdad en exterminio a la UP** 

A pesar de la gran cantidad de procesos abiertos, la Fiscalía solo ha logrado 246 sentencias, entre ellas, se condenó a dos agentes del extinto Departamento Administrativo de Seguridad (DAS), 18 miembros del Ejército y 10 agentes de la Policía. Además, cabe resaltar que la justicia ordinaria **ha reconocido a solo 1.620 víctimas de la UP desde 1984**, que solo es una tercera parte de las más de 6.000 víctimas que han identificado otras organizaciones.

Frente este panorama, la justicia transicional busca establecer los **máximos responsables de estos crímenes** y dar a conocer la verdad detrás de esta persecución política que las víctimas han exigido que sea considerado un genocidio. Según los informes recopilados, "hay evidencia que sugiere la existencia de un patrón sistemático y generalizado de violencia contra los miembros de la UP y, desde hipótesis distintas, señalan como presuntos responsables **a miembros de la Fuerza Pública, agentes del Estado, terceros civiles y grupos paramilitares**".

La Sala de Reconocimiento dio a conocer que **16 miembros de la Fuerza Pública** se han acogido a la justicia transicional por hechos relacionados con el exterminio del partido de izquierda; **otros cuatro miembros de la Fuerza Pública y 13 ex-agentes del DAS** han solicitado su sometimiento por la misma razón.

Entre los miembros de la Fuerza Pública, se encuentran un brigadier general, tres mayores, un capitán, un teniente, un subteniente, dos sargentos viceprimeros, tres sargentos segundos, un cabo segundo, dos soldados profesionales y un agente de inteligencia.

### **La Unión Patriótica** 

Este partido político surgió en 1985 como resultado de los Acuerdos de Uribe, firmados por las antiguas FARC-EP y el gobierno de Belisario Betancur. En las elecciones presidenciales de 1986, el partido alcanzó el tercer lugar con el candidato Jaime Pardo Leal. Además, **este movimiento político obtuvo el 10 % de la votación del país**, un hecho sin precedente para un partido alternativo en Colombia. (Le puede interesar: "[Corporación Reiniciar instaura tutela contra Centro de Memoria Histórica por víctimas de UP](https://archivo.contagioradio.com/corporacion-reiniciar-instaura-tutela-contra-centro-de-memoria-historica-por-victimas-de-up/)")

A partir de este momento, líderes, dirigentes, activistas y campesinos — todos miembros o simpatizantes de la UP — fueron asesinados sistemáticamente. Según los informes recibidos por la JEP, las victímas fueron sujetos a atentos contra "su derecho a la vida, libertad, seguridad e integridad física, sexual y psicológica".

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
