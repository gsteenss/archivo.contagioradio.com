Title: Sin renunciar a la verdad, habitantes de Trujillo resisten desde la memoria
Date: 2019-04-09 23:15
Author: CtgAdm
Category: Memoria, Sin Olvido
Tags: Masacre de Trujillo, Valle del Cauca
Slug: sin-renunciar-a-la-verdad-habitantes-de-trujillo-resisten-desde-la-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/D3vfmU1W0AEST2y.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: sorisgut] 

En conmemoración de los casi 30 años de la Masacre de Trujillo, perpetrada por estructuras paramilitares y de la Fuerza Pública entre los años 1986 a 1994, en los municipios de Trujillo, Bolívar y Riofrío, Valle del Cauca; de las que resultaron más 245 víctimas; familiares y representantes de quienes ya no están han fortalecido con los años un proceso de  resistencia que hoy continúa.

**Orlando Arboleda, habitante de Trujillo** quien integra el proceso de reparación colectiva, señala que durante el homenaje que anualmente realizan de la fatídica fecha, contaron con el apoyo y acompañamiento de diversas organizaciones que han colaborado a que las comunidades narren su verdad y no la versión que "los entes del Estado han mostrado".

Frente al **Sistema Integral de Verdad, Justicia, Reparación y No Repetición**, Orlando considera que ha sido un proceso muy lento, en especial por la incertidumbre que existe al no saber quiénes fueron los responsables y si estos han pagado o no por sus delitos, "empezaremos a hablar de verdad cuando sepamos el por qué lo hicieron", agregando que la población siempre se distinguió por "ser personas campesinas, gente muy trabajadora, que no tenía que ver con gente al margen de la ley" por lo que nunca existió un motivo real para atentar contra sus vidas.

Acerca del proceso de reconciliación y de sanación , Orlando señala que es un proceso bastante difícil, **"todos los grupos al margen de la ley buscaban romper los tejidos sociales"**, pero asegura que a través del tiempo las comunidades que fueron afectadas años atrás,  han logrado entablar relaciones de amistad y confianza con los habitantes de cinco veredas circundantes.

A estos avances al interior de Trujillo, se ha sumado el acompañamiento de la Unidad para las Víctimas y que desde 2012 se unió a la población para crear una cooperativa cafetera, **"95% de la región es cafetera, quisimos darle un valor agregado a nuestro producto" ** afirma Orlando quien destaca que este trabajo lo hacen por el bienestar de sus familas y las próximas generaciones, "para nosotros es muy valioso que sepan el trabajo que estamos haciendo por nuestras comunidades no podemos quedarnos frustrado hay que sacar esto a flote, nosotros desde acá esperamos ver este país en completa paz", concluye.

<iframe id="audio_34276613" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34276613_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
