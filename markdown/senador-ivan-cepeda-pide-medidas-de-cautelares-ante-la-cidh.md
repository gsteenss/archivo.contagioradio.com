Title: Senador Iván Cepeda solicita medidas cautelares ante la CIDH
Date: 2015-11-18 10:39
Category: Judicial, Nacional
Tags: CIDH, destitucion ivan cepeda, Iván Cepeda, medidas cautelares, proceso de paz
Slug: senador-ivan-cepeda-pide-medidas-de-cautelares-ante-la-cidh
Status: published

###### Foto:  Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9429640_2_1.html?data=mpmfm5uYdI6ZmKiakpWJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8biwsnc1JCtuoa3lIquk9OPh8bkxsnOjdXNqMafzsrRy8nFt4zXwtrhx9HFtsbnjMbb1sqPsMKfpK6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Senador Iván Cepeda] 

A través de un comunicado la defensa del congresista del Polo Democrático pidió  a la Corte Interamericana de Derechos Humanos **medidas cautelares ante una eventual sanción o destitución  que puede sufrir el Senador Cepeda** por parte del Ministerio Público, en cabeza del procurador Alejandro Ordoñez.

El escrito radicado este miércoles ante la Comisión Interamericana de Derechos Humanos en Washington, el Centro por la Justicia y el Derecho Internacional *Cejil* y Colectivo de abogados José Alvear Restrepo, solicitando la adopción de medidas cautelares “*en atención al cumplimiento de los requisitos de gravedad y urgencia de la situación, así como a la condición de irreparabilidad del daño*” que pueden sufrir el legislador y sus electores con una eventual sanción o destitución por parte del Ministerio Público.

La defensa solicita a la Comisión, dirigirse al Estado colombiano con el objeto de que suspenda el procedimiento disciplinario hasta que se produzca una decisión de fondo en el Sistema Interamericano de Derechos Humanos.

De acuerdo al grupo de juristas,  el senador Cepeda Castro juega un rol trascendental en los diálogos de paz, y el proceso disciplinario en su contra **“se percibe como un elemento más de la estrategia del Procurador para atacar el proceso de paz”.**

En la solicitud, también se le pide a la CIDH que, a través de la Fiscalía General de la Nación, se adelanten las acciones necesarias para investigar las posibles conductas punibles en las que pudo incurrir el Procurador General de la Nación en el procedimiento adelantado contra el congresista.

[Senador Iván Cepeda pide medidas cautelares ante la CIDH](https://es.scribd.com/doc/290184642/Senador-Ivan-Cepeda-pide-medidas-cautelares-ante-la-CIDH "View Senador Iván Cepeda pide medidas cautelares ante la CIDH on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_4819" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/290184642/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-W0lETMcIE2t2FiD4wiqj&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
