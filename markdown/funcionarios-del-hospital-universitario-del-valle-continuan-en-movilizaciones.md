Title: Funcionarios del Hospital Universitario del Valle continúan en movilizaciones
Date: 2015-09-08 13:47
Category: Movilización, Nacional
Tags: crisis de la salud, EPS, Gobierno Nacional, Hospital Universitario del Valle, Jose Joaquín Jamauca, Ministerio de Salud
Slug: funcionarios-del-hospital-universitario-del-valle-continuan-en-movilizaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/IMG-20150907-WA0017.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sintrahospiclínicas 

<iframe src="http://www.ivoox.com/player_ek_8199599_2_1.html?data=mZamm5qdfY6ZmKiakpqJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitbixM7c0MbWrdDnjMnSzpCss9TkytnOzpC5ssrqxtfgy9nFtsrjjMnSzpC6pc3gxpDQ0dPYrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [José Joquín Jamauca, Sintrahospiclínicas] 

###### [8 sep 2015] 

Aproximadamente 2 mil funcionarios del Hospital Universitario del Valle, continúan en jornada de movilización y paro debido **a los 192 mil millones de pesos que las EPS adeudan** **y los 35.000 millones de pesos** **que el Ministerio de Salud** se comprometió a girar y aún no lo hace.

El **Banco de sangre cerrado, falta de insumos, laboratorios cerrados y la falta de recibos para programar citas, hacen parte del **panorama que actualmente presenta en el hospital del Valle.

De acuerdo con José Joquín Jamauca, coordinador de Derechos Humanos del Sindicato de Trabajadores y Clínicas (Sintrahospiclínicas) del Hospital Universitario del Valle, la protesta se mantendrá hasta que no haya una respuesta concreta del gobierno que atienda la crisis hospitalaria.
