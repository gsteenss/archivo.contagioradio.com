Title: Cuba, el Papa, Obama y Colombia
Date: 2015-04-21 07:37
Author: CtgAdm
Category: Abilio, Opinion
Tags: Evo Morales, Joel Suarez, MST, Papa Francisco, Vaticano
Slug: cuba-el-papa-obama-y-colombia
Status: published

Por [Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - @abiliopena

La fuerza de la dignidad Cubana  sigue  teniendo logros. En octubre del año anterior el papa Francisco convocó  en el Vaticano a líderes de movimientos sociales de todo el mundo. Entre los mas activos participantes, se encontraban el Centro Martin Luther King, Jr (CMLK)  de Cuba,  El Movimiento Sin Tierra de Brasil y Alba Movimientos constituido por movimientos sociales de los países del Alba.

Cuenta el cubano  Joel   Suarez del CMLK que  el estrechón de mano con el Papa, fue bien aprovechado. Le pidió su intermediación ante el presidente Obama, de los Estados Unidos, para la liberación de los tres cubanos presos en ese país, acusados injustamente de  espionaje y de atentar contra la seguridad nacional de la potencia norteamericana. “Vengo de la amada isla de Cuba, -le dijo-  somos hermanos en la fe, Joao Pedro del MST le entregó una carta, yo le pido en el nombre de Dios que usted le preste especial atención”. La solicitud la reforzó el presidente boliviano Evo Morales.

Pero ahí  no terminó  el llamado a la   mediación. Le pidieron con la liberación de los tres cubanos, nada mas ni nada menos,  interceder para la mejoría de relaciones entre la digna isla  y el poderoso país del norte. Así lo consignaron: “Una actuación positiva del Presidente Obama sería un gesto de generosidad y una fuerte señal en el camino de la mejoría de las relaciones entre su gobierno y el cubano, ahora que la contribución de Cuba a la lucha contra el ébola ha recibido el reconocimiento público de las autoridades estadounidenses”.

Los resultados de ésta y seguramente muchas más gestiones  en el pasado, no se hicieron esperar.  En diciembre  se dio la liberación de los tres cubanos y en este mes el presidente Obama presentó al congreso de su país la solicitud de excluir a Cuba de la lista de países que promueven el terrorismo, paso muy importante hacia la normalización de las relaciones que conduzca al final del inmoral bloqueo que Estados Unidos ha impuesto al país caribeño desde hace más cinco décadas.

Joel Suárez nos compartía otra buena noticia: entre el 8 y 10 de julio, en  el marco de la visita del Papa a Bolivia, tendrá lugar la reunión de Movimientos sociales en la que Colombia tendrá un número significativo de cupos. Este encuentro con el Papa será una oportunidad excepcional para pedirle su intervención para la paz con justicia en Colombia que pasa, sin duda, por el reconocimiento de la responsabilidad de algunos de sus miembros en la violencia de nuestro país. Con un Papa como Francisco, puede ser posible.
