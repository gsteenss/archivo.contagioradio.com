Title: Se reabre el debate sobre adopción igualitaria
Date: 2015-02-19 21:41
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Adopción igualitaria, LGBTI, Medellin
Slug: se-reabre-el-debate-sobre-adopcion-igualitaria
Status: published

<div style="text-align: justify;">

##### Foto: Contagio Radio 

<p>
Nuevamente se abre el debate sobre la posibilidad de que las parejas del mismo sexo puedan adoptar. Esta vez, se trata una demanda de un profesor universitario de Medellín llamado Sergio Estrada. **La acción no se basa en los derechos de la comunidad LGBTI, sino en los derechos de la niñez a tener un hogar.**  
La demanda interpuesta por el profesor, hará que la Corte Constitucional reabra la discusión para que **el Código de Infancia y Adolescencia sea modificado y los niños y niñas que no cuentan con una familia puedan ser adoptados por parejas del mismo sexo.** Según Estrada, será crucial que los magistrados no se dejen llevar por sus fundamentos religiosos y morales, sino que basen su criterio en los derechos de la niñez.  
"La acción presentada ante la Corte es resultado de más de cinco años de investigación con el propósito de procurar una protección eficaz de los derechos fundamentales de los niños", afirmó el profesor. En su estudio, el académico resalta las cifras alarmantes de niños y niñas que están en abandono, y dice que este tema no lo tuvo en cuenta la Corte Constitucional al emitir su último fallo sobre la adopción igualitaria.  
La iniciativa tiene como objetivo, **"desomosexualizar" el debate**, para así proteger el derecho fundamental de los menores a tener una familia.  
Sergio Estrada, resaltó que no representa a "ninguna comunidad LGTBI, ni religiosa, es solo académico". La demanda ya fue aceptada por la Corte Constitucional y quedó en manos del magistrado, **Jorge Iván Palacio, esta deberá ser debatida en un mes.**

</div>
