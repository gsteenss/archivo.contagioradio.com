Title: Tarso persiste en la defensa del territorio tras fallo del Tribunal de Antioquia
Date: 2019-02-07 17:54
Author: AdminContagio
Category: Ambiente, Nacional
Tags: acuerdo municipal, Antioquia, Tarso
Slug: tarso-la-defensa-del-territorio-fallo-del-tribunal-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-13.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cinturón Ambiental de Occidente] 

###### [07 Feb 2019] 

Tras el fallo del Tribunal Administrativo de Antioquia (TAA) que anuló el acuerdo municipal de Tarso que prohíbe la minería, las comunidades manifestaron que tomarán** todas las medidas posibles, legales y sociales, para hacer valer la decisión municipal.**

Según Huber Cartagena, concejal de Tarso, la gobernación municipal está explorando todas sus opciones legales para oponerse al fallo del Tribunal que determinó que el consejo "carece de competencia para establecer la prohibición del desarrollo de actividades mineras al ser este un asunto que debe regular el legislador" como dicta los artículos 31 a 38 del código de minas.

Cabe resaltar que **actualmente 13 municipios del sudoeste de Antioquía han prohibido la actividad minera en sus municipios a través de un acuerdo municipal**. En el 2017, el consejo de Tarso aprobó por unanimidad el Acuerdo Municipal 13 para "l[a defensa del patrimonio ecológico y cultural" del territorio de vocación agrícola y ganadera después de que las autoridades nacionales otorgaran un título minero en el municipio.]

Cartagena expresó que "e[s lamentable y incoherente" el reciente fallo dado que el Tribunal previamente había defendido el Acuerdo Municipal ante las objeciones del Alcalde de Tarso, los cuales fueron consideradas como "infundadas". Solo fue cuando la Gobernación de Antioquia pidió la revisión del acuerdo que el Tribunal falló en su contra este pasado 31 de enero.]

"[A uno le queda una sensación de frustración cuando el Tribunal avala este proyecto y luego el mismo Tribunal lo declara invalido," manifestó el concejal. (Le puede interesar: "[Con acuerdo municipal, Urrao le dice 'No' a la minería](https://archivo.contagioradio.com/mineria_urrao/)")]

Frente la decisión del Tribunal, se ha manifestado que este organismo está desconociendo el fallo del Consejo de Estado, que reafirmó en octubre del año pasado  la competencia de los entes municipales para prohibir la minería en sus territorios. En ese momento, un ciudadano de Urrao, Antioquia ganó una tutela amparando la decisión del municipio de impedir la minería.

En esa sentencia, la Sección Cuarta del Consejo de Estado reiteró que las autoridades locales y nacionales deben tomar una decisión conjunta sobre asuntos de la minería, el ambiente y la energía dado que las dos entidades tienen competencia. **Sin embargo, en la ocasión de que no se pueda llegar a un acuerdo mutuo, prima la competencia de las autoridades municipales.**

Cartagena afirmó que con la decisión del Tribunal de Antioquia se hace entender a las regiones que las decisiones sobre el uso del terreno en los municipios se determinan no desde de la ciudadanía directamente afectada, sino desde las oficinas del Gobierno Central en Bogotá.

"Es absurdo que alguien desde Bogotá o desde Medellín haga lo que se le de la gana con el territorio mientras que nosotros somo los que estamos acá y nos vemos afectados," dijo el concejal. El dirigente político afirmó que lo que está en juego es la autodeterminación de la ciudadanía y el futuro de los ecosistemas en Tarso que actualmente son ambientalmente sostenibles.

Cartagena agregó que las comunidades continuarán con la lucha jurídica, además de la lucha por la defensa del territorio. Actualmente, están preparando una movilización social y consultando abogados expertos en el tema para determinar que medidas legales pueden tomar.

<iframe id="audio_32372809" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32372809_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
