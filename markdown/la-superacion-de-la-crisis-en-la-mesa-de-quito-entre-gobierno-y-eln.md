Title: La superación de la crisis en la Mesa de Quito entre Gobierno y  ELN
Date: 2018-01-15 08:00
Category: Abilio, Opinion
Tags: Cese al fuego, ELN, petroleo
Slug: la-superacion-de-la-crisis-en-la-mesa-de-quito-entre-gobierno-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: SIG y El Universal 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 31 Dic 2017

**Incumplimientos a FARC y precios del petróleo claves en la decisión.**

[Sin duda la fuerza de la sociedad civil colombiana que se ha pronunciado en diversas oportunidades  desde la iglesia católica, el diálogo Intereclesial,  DiPaz, las organizaciones de víctimas como CONPAZ, articulaciones como PACIPAZ, académicos, pidiendo tanto al gobierno como al ELN volver a la mesa  de diálogos en Quito  y continuar  con el cese bilateral al fuego, argumentando, fundamentalmente, los alivios humanitarios  para la población, ha incidido en que se cree un clima propicio para  que las dos partes  anuncien su disposición de volverse a sentar a la mesa.]

[La gestión de los países garantes como también  la de Naciones Unidas en su condición de veedora del proceso, ha jugado su rol. Tanto las comunicaciones de la Delegación del ELN como del gobierno a través de Gustavo Bell valoran altamente  su participación.]

[Además de la fuerza social de distinto tipo que clama la continuación del cese bilateral y  de los diálogos,  habría al menos dos factores más  que pueden  estar pesando en la voluntad del gobierno para proseguir con estos diálogos. De un lado sus incumplimientos a lo acordado con la FARC genera un  ambiente de decepción  que podría hacer  que las bases de la  antigua guerrilla busquen  cabida en el ELN y, del otro,  la amenaza de ésta insurgencia a la infraestructura petrolera en momentos en que el crudo ha alcanzado el precio más alto de los últimos 3 años, lo que resolvería  en parte la  crisis de recursos por la que pasa el gobierno.]

[El hipotético ingreso de bases  ex guerrilleras de la  FARC al ELN significaría el fracaso fáctico de un proceso por el que gano el Premio Nobel de Paz el presidente Santos. Los incumplimientos  que lo animarían  fueron claramente enumerados en la intervención de la FARC ante los veedores internacionales Felipe González y Pepe Mujica en Cartagena a comienzos de este mes de enero,  expresando  de entrada  que “la paz atraviesa uno de los momentos más difíciles tras la firma del Acuerdo Final en la Habana”. (][[http://www.wradio.com.co/docs/201801044763470e.pdf]](http://www.wradio.com.co/docs/201801044763470e.pdf)[)]

[A finales del año anterior, diversas expresiones de la FARC llamaban la atención sobre lo que podía significar  estos incumplimientos en la mesa de Quito. Por ejemplo  Santrich invitó al ELN a “[fijarse en lo que iba a firmar](https://canal1.com.co/noticias/santrich-le-advierte-los-negociadores-del-eln-estado-incumple-palabra/)”  y  Timochenko en carta a Santos le notificó que  “[el cincuenta por ciento de lo que se haga en la Mesa de Quito es el cumplimiento de lo pactado con las FARC](https://www.farc-ep.co/comunicado/carta-abierta-al-senor-presidente-de-la-republica-juan-manuel-santos.html)” ]

[Santos, se encuentra entonces con un ELN que puede ver  en los incumplimientos a  la FARC una fortaleza para sus exigencias, pero a su vez  con unas FARC cuyas bases pueden ver en  el ELN una salida a los incumplimientos del gobierno.]

[Ahondando en el segundo factor,  la exigencia histórica de una revisión de la política energética por parte del ELN tienen  en la crisis de la mesa de conversaciones un momento favorable para esa reivindicación, debido al déficit  de ingresos de los últimos  seis años de empresas como Ecopetrol, que según el último informe disponible  de la Contraloría disminuyeron en un 9% y por el salvavidas que significa el  ascenso del precio del petróleo (][[http://www.eltiempo.com/economia/sectores/finanzas-publicas-de-colombia-en-2016-segun-la-contraloria-general-119570]](http://www.eltiempo.com/economia/sectores/finanzas-publicas-de-colombia-en-2016-segun-la-contraloria-general-119570)[).]

[[El barril del crudo  superó esta semana los 68 dólares](http://www.portafolio.co/economia/precio-del-barril-de-petroleo-supera-los-68-dolares-513085),  aumento que  no se registraba desde mayo de 2015, en una economía colombiana  cuya estabilidad depende de los precios del petróleo y el gas.]

[En este momento el ELN vuelve sobre su exigencia  de revisar la política energética y reanuda sus acciones ofensivas con el saboteo a la infraestructura y la retención de un contratista de Ecopetrol.  En reacción  los gremios vinculados al negocio del petróleo, invitaron al gobierno, antes que a responder militarmente, a volver a la mesa y renegociar el cese bilateral al fuego, que beneficia sin duda sus finanzas, y en consecuencia las del gobierno central (http://www.rcnradio.com/nacional/sector-petrolero-le-pide-al-gobierno-al-eln-pactar-cese-fuego-definitivo/).]

[Este contexto que conjuga los llamados de la sociedad colombiana, la ONU,  los países facilitadores otros países de la  de la comunidad internacional con la crisis por los incumplimientos del gobierno a las FARC y el ascenso en los precios del petróleo, pueden determinar la reanudación  de la mesa de Quito en medio la encrucijada humanitaria, política y económica que afecta los  réditos  del  gobierno en los últimos siete  meses que le restan.]

[Los ajustes al cese bilateral, la reanudación de las conversaciones sobre los puntos de la agenda y el sueño de un acuerdo en el que se incluyan garantías para su cumplimiento, es el mejor escenario que daría alas al movimiento social.]

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio
