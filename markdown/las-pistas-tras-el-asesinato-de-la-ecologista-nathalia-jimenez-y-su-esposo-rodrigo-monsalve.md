Title: Las pistas tras el asesinato de la ecóloga Nathalia Jiménez y su esposo Rodrigo Monsalve
Date: 2019-12-23 18:01
Author: CtgAdm
Category: Ambiente, DDHH
Tags: ambientalistas asesinados, Magdalena
Slug: las-pistas-tras-el-asesinato-de-la-ecologista-nathalia-jimenez-y-su-esposo-rodrigo-monsalve
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/646210_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

Este 23 de diciembre, habitantes de la vereda Perico Aguao,en el corregimiento de Guachaca, en Santa Marta Magdalena hallaron dos cuerpos sin vida que corresponden a la ecóloga **Nathalia Jiménez y a su esposo, el estudiante de antropología Rodrigo Monsalve** de quienes se desconocía su paradero desde el 20 de diciembre cuando fueron abordados por personas armadas en su camino a la región de Palomino, Magdalena.

La última vez que se había tenido información de la pareja fue el pasado viernes 20 de diciembre, cerca de las 5:30pm, cuando Jairo Jiménez, padre de Nathalia, se comunicó con ella y mientras hablaban escuchó un grito de su hija y a su yerno diciendo "por favor no nos hagan daño",  segundos después se cortó la comunicación.

Según la información compartida por la Fundación Natura, el coronel Gustavo Berdugo, comandante de la Policía Metropolitana de Santa Marta expresó que gracias a las cámaras de seguridad del peaje Neguanje, se estableció que cerca de las 4:15pm de ese viernes, Nathalia Jiménez y Rodrigo Monsalve cruzaron la troncal del Caribe, vía que conduce de la capital del Magdalena a Palomino, de igual forma hay imágenes del auto cruzando la región de Guachaca.

### No es la primera vez que mueren defensores del ambiente en la vereda Perico Aguao 

Los sucesos ocurridos con la pareja que se dirigía a Palomino, suceden en la misma vereda donde Wilton Orrego, contratista de la Unidad de Parques Nacionales Naturales, adscrito al Parque Sierra Nevada de Santa Marta fue asesinado tras ser alcanzado por cinco disparos de arma de fuego a manos de hombres desconocidos el 15 de enero de este año.[(Le puede interesar: Asesinan a funcionario de Parques Nacionales en la Sierra Nevada)](https://archivo.contagioradio.com/asesinan-a-funcionario-de-parques-nacionales-en-la-sierra-nevada/)

Habitantes de la zona han explicado que en los límites entre La Guajira y Magdalena hay presencia de varias bandas que extorsionan y realizan atracos como Los Pachenca,  lo que plantea que la pareja hubiera sido víctima de un retén ilegal, aunque resulta preliminar poder establecer los hechos, las autoridades señalaron que la pareja fue **"hallada con capuchas en la cabeza y las manos amarradas".**

De igual forma en junio de este año,  en la región se advirtió a la comunidad de Palomino sobre un paro armado en la troncal Caribe como represalia por la muerte de alias "Chucho Mercancía" y alias "Mario",  líderes de  "Los Pachenca", al respecto la Policía de Santa Marta desmintió que se tratara de un un panfleto verídico. A su vez, circuló otro panfleto, de las autodenominadas Autodefensas Gaitanistas de Colombia exigiendo que se reabriera la vía, ante este conflicto de intereses que ha dejado a la población en medio de dichas agrupaciones se ha evidenciando la fuerte influencia paramilitar que existe en la región.**  
**

La Fundación Natura señala  **que ni ella ni la organización  habían recibido amenazas asociadas a su trabajo,** pero han hecho un llamado a las autoridades para que se adelanten las investigaciones que permitan esclarecer los hechos. [(Lea también: Existen 17 casos de amenazas grupales y directas contra trabajadores de Parques Nacionales)](https://archivo.contagioradio.com/existen-17-casos-de-amenazas-grupales-y-directas-contra-trabajadores-de-parques-nacionales/)

Nathalia se desempeñó como gestora territorial del bajo Magdalena y Caribe y actualmennte trabajaba en la ciénaga de Zapatosa, en  Cesar y Magdalena, así como en la Ciénaga de Ayapel en Córdoba como parte del proyecto “Conservación de la Biodiversidad de los ecosistemas dulce acuícolas de la Macrocuenca Magdalena Cauca”. Estudió ecología en la Universidad Javeriana y antropología en la Universidad Nacional y contaba con estudios de maestría en Desarrollo Rural y Cooperación Internacional.  **Ver:** "[¡No queremos muelle!": el grito de los habitantes de Taganga](https://archivo.contagioradio.com/comunidades-rechazan-tercera-modificacion-de-licencia-ambiental-de-puerto-en-taganga/?fbclid=IwAR25ou3AqTJWqvPFyXh1WkJZCbUyATFbW_3v0oD0roLgnT5exmqvcj6yH1o)

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
