Title: Marcha contra la brutalidad policial
Date: 2015-02-25 17:37
Author: CtgAdm
Category: Fotografia
Tags: ESMAD, Marcha contra el ESMAD
Slug: marcha-contra-la-brutalidad-policial
Status: published

A pesar de las fuertes lluvias cientos de personas marcharon por el centro de Bogotá en contra de la brutalidad policial y de la represión estatal contra el derecho a protestar y la desobediencia pacífica y activa en Colombia.

\
