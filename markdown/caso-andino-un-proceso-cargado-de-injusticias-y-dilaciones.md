Title: Caso Andino, un proceso cargado de injusticias y dilaciones
Date: 2020-06-07 15:42
Author: CtgAdm
Category: Nacional
Slug: caso-andino-un-proceso-cargado-de-injusticias-y-dilaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/caso-andino-en-libertad.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/caso-andino-en-libertad-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/caso-andino-en-libertad-3.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6} -->

###### Foto:Javier Jimenez {#fotojavier-jimenez .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El 17 de junio de 2017 un atentado en el Centro Comercial Andino, estremeció a la capital de Colombia. Inmediatamente la Fiscalía señaló a 11 jóvenes de haber sido los responsables de este hecho. Tres años más tarde, **el ente acusador no ha revelado ni una sola prueba contundente** en su contra, mientras que ellos afirman ser víctimas de un montaje judicial.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Tras los sospechosos
--------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde el inició de este proceso jurídico, Boris Rojas, Lizeth Rodríguez, Lina Jiménez, Andres Bohorquez, Cristian Sandoval, Alejandra Mendéz, Iván Ramírez, César Barrera, Camilo Pulido y Natalia Trujillo aseguraron que no eran las personas tras el atentado en el centro comercial Andino. Como también manifestaron no pertenecer al Movimiento Revolucionario del Pueblo, organización armada que según la Fiscalía fue la autora del atentado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Apenas se conocieron las capturas, en los medios de información aparecieron las imágenes de las personas y un supuesto arsenal probatorio que sería presentando ante un juez. Sin embargo, ni las banderas, las usb, los computadores, los explosivos y el material ideológic**o fueron presentados**, de acuerdo con los abogados defensores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los mismo pasó con los testigos, prontamente desechados por la calidad del testimonio. Mientras tanto las 11 personas continuaban en la cárcel, argumentando que su juicio se estaba llevando en los medios de información, "para dar resultados". (Le pude interesar: ["La versión de los jóvenes capturados por el atentado en el Centro Comercial Andino"](https://archivo.contagioradio.com/entrevista-a-boris-rojas-y-lizeth-rodriguez-capturados-por-atentado-a-centro-comercial-andino/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":85146,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Uno de los jóvenes libres - Javier Jimenez Rojas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/caso-andino-en-libertad-2-1024x674.jpg){.wp-image-85146}  

<figcaption>
Cristian Sandoval a las afueras de cárcel La Picota - Foto: Javier Jimenez

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading -->

Cero y van dos
--------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Un año después, el 24 de agosto de 2018, un juez** rechazó la petición de la Fiscalía de prorrogar la medida de aseguramiento**, debido a que la solicitud fue extemporánea. Hecho que significaba que por vencimiento de términos las 11 personas quedaría en libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, días más tarde, un nuevo proceso se abrió en su contra. Esta vez, la Fiscalía aseguraba tener pruebas que relacionaban al MRP con la guerrilla del ELN. Razón por la cual vinculó a las 11 personas en un nuevo proceso judicial, bajo la **Ley de Grupos Armados Organizados 1908 de 2018**. (Le puede interesar: ["La Fiscalía está montando pruebas: Defensa de jóvenes implicados en caso Andino"](https://archivo.contagioradio.com/fiscalia-esta-montando-pruebas-andino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, la defensa de los jóvenes argumentó que al ser esta una ley expedida en el 2018, no podía ser aplicada a un acontecimiento previo. Las audiencias continuaron y con ellas la ausencia de las pruebas que la Fiscalía seguía argumentando tener.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cero y van tres
---------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Posteriormente el pasado 2 de junio, un juez de garantías volvió a conceder la libertad a dos de las personas señaladas: César Barrera y Cristian Sandoval. Una vez más, señalando que existían un vencimiento de términos, razón por la cual ambas personas quedaban en libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un día después, apareció una nueva orden de captura, esta vez por los atentados del Centro Comercial Andino y acciones terroristas en Bogotá y Pereira. Pero una vez más, **el juez 79 de control de garantías declaró ilegal el procedimiento** de ambos y dispuso cancelar las órdenes de captura. (Le puede interesar: ["Colectivo Libres e Inocentes"](https://www.facebook.com/LibreseInocentes/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":85147,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![Reencuentro con la familia luego de pasar cerca de 3 años en la cárcel - Javier Jimenez Rojas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/caso-andino-en-libertad-3-1024x661.jpg){.wp-image-85147}  

<figcaption>
Reencuentro con la familia luego de pasar cerca de 3 años en la cárcel - Foto: Javier Jimenez

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Fue así como el pasado 5 de junio **César Barrera y Cristian Sandoval** quedaron en libertad y salieron del centro penitenciario La Picota. A fuera, además de la luna llena de junio, les esperaron sus familias, amigos y allegados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El momento del abrazo se pospuso más de lo deseado producto de la pandemia del covid 19, y hasta que no tuvieron puestos los trajes bioprotección no pudieron entrelazarse a quienes **los esperaron y defendieron por más de 1.000 día**s.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El mismo 5 de junio, otra orden de un juez otorgó, también por vencimiento de términos, la libertad para cuatro personas más de este proceso; **Andrés Mauricio Bohóquez Flórez, Juan Camilo Pulido Rivero, Boris Ernesto Rojas Quijano y Alejandra Méndez Molano**. Sin embargo, una vez más **otra orden de captura llegó**. En este caso la defensa de las nueve personas que continúan tras las rejas afirman que continuarán en la apelación de esas órdenes y a la espera de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
