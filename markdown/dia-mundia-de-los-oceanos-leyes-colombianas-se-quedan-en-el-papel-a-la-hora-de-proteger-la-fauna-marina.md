Title: Leyes colombianas se quedan en el papel a la hora de proteger la fauna marina
Date: 2016-06-08 16:07
Category: Ambiente, Nacional
Tags: colombia, dia mundial de los oceanos, fauna marina
Slug: dia-mundia-de-los-oceanos-leyes-colombianas-se-quedan-en-el-papel-a-la-hora-de-proteger-la-fauna-marina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/FAUNA2-e1465419586514.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [adry.over-blog.es]

###### [8 Jun 2016] 

**Cambio climático, Islas de basura en  los océanos, pesca con dinamita y cianuro, animales acuáticos para comida, sobrepesca, turismo irresponsable** son algunas de las principales problemáticas a las que se enfrenta la vida marina, y de las cuales el ser humano no es consciente de lo que está causando.

En el Día Mundial de los Océanos, Andrés Matiz, integrante de la Fundación Leucas, asegura que una de las formas para enfrentar esa situación es que **las personas y sobre todo las comunidades aledañas a los océanos, conozcan y se apropien del cuidado y la protección de la fauna marina,** en un país tan biodiverso como Colombia, pero donde también cientos de especies se encuentran en peligro.

Aunque el gobierno dice tener políticas para proteger la fauna acuática, los cierto es que las leyes se quedan en el papel y las ballenas, rayas, tiburones, arrecifes, etc, no son protegidos debidamente por el gobierno que concentra su atención en otro tipo de cosas, mientras las ballenas siguen apareciendo muertas en las playas del pacífico, las tortugas siguen siendo usadas para comida y los extranjeros siguen llegando a los océanos colombianos a pescar.

En el marco del Día Mundial de los Océanos, la organización BirdLife International, señaló de acuerdo a sus investigaciones que **la pesca ahoga y extermina a  300.000 aves marinas cada año, 100.000 de ellas Albatros**, las  aves más grandes del mundo.

Así mismo, según el secretario general de la Comisión Permanente del Pacífico Sur (CPPS), Julián Reyna, la contaminación marina más impactante ocurre con los plásticos y microplásticos que en el Pacífico norte han generado un **isla tan grande que equivale a dos veces el estado de Texas de Estados Unidos.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
