Title: A la Minga indígena llegaron primero los estudiantes que Duque
Date: 2019-03-15 17:36
Category: Comunidad, Educación
Tags: Cauca, educacion, Educación Superior, estudiantes
Slug: minga-indigena-estudiantes-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/marcha_JEP_AGG058A2185.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Mar 2019] 

La Unión Nacional de Estudiantes de Educación Superior (UNEES), desarrollará en la Universidad del Cauca, Popayán, el tercer encuentro de estudiantes (ENEES 3.0) en el que tomarán decisiones fundamentales sobre cómo funcionará ese movimiento durante este año, así como la dinámica de movilizaciones que planean desarrollar. El encuentro se realizará los días 15, 16, 17 y 18 de marzo, y se espera la participación de más de 2 mil estudiantes.

Para Cristian Guzmán, vocero nacional de la UNEES, es cierto que en este momento es difícil llegar hasta Popayán por los bloqueos a la vía producto de la Minga por la Vida, sin embargo, afirma que "es importante no aislar las luchas sino acompañarlas"; además, teniendo en cuenta que uno de los puntos que exigen los indígenas es el cumplimiento del acuerdo pactado entre estudiantes y Gobierno. (Le puede interesar: ["Presidente Duque seguimos esperando su llegada: Minga del Cauca"](https://archivo.contagioradio.com/presidente-duque-seguimos-en-minga-esperando-su-llegada-comunidades-indigenas-del-cauca/))

### **4 días de discusiones sobre educación, el movimiento social y el PND** 

Guzmán reconoció que el tope máximo de participación en los Encuentros ampliados de estudiantes fue superior a las 3 mil personas, sin embargo, señala que en esta ocasión esperan una asistencia superior a las 2 mil personas, quienes durante los 4 días que dura el evento discutirán sobre el futuro del movimiento estudiantil, la articulación con otros movimientos, el cumplimiento del acuerdo firmado con el Gobierno en diciembre y las reformas que deben hacerse en favor de la educación superior.

Sobre el futuro mismo del movimiento estudiantil, el integrante de la UNEES sostuvo que debe repensarse profundamente y cambiar, de forma tal que este movimiento pueda trascender en el tiempo y no vivir experiencias como la caida de la MANE o de otros procesos. En ese sentido, lo que los estudiantes desearían es mantener la vigencia de esta organización, y seguir siendo parte de las coyunturas sociales que se están viviendo en el país.

Entendiendo esa búsqueda por la continuidad en el tiempo y articulación con otros movimientos, los jóvenes propondrán el cierre del conflicto armado como un punto neurálgico a defender, así como la defensa por la vida y el derecho a la protesta como banderas de esa relación a establecer con los demás actores sociales. (Le puede interesar:[" Jóvenes de la Capital se reencuentran por la educación superior"](https://archivo.contagioradio.com/estudiantes-educacion-superior/))

Sobre el cumplimiento de los acuerdos pactados con el Gobierno Nacional, Guzmán adelantó que discutirán sobre la base de cumplimientos parciales que se observan en el Plan Nacional de Desarrollo, pero cuyo enfoque centrarán en proponer métodos de cumplimiento y verificación. De igual forma, la UNEES discutirá temas relevantes como su posición sobre el referendo por la educación, la reforma estructural al ICETEX y la autonomía universitaria.

<iframe id="audio_33421862" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33421862_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
