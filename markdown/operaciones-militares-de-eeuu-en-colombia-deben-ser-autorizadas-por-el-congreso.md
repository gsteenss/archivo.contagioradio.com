Title: Operaciones militares de EEUU en Colombia deben ser autorizadas por el Congreso
Date: 2020-05-30 19:07
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Brigadas de Estados Unidos, Ejército Nacional
Slug: operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Congreso-curules.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Congreso de la República

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A raíz del anuncio hecho por la embajada de Estados Unidos sobre la llegada de una brigada de Asistencia de Fuerza de Seguridad a Colombia, desde sectores alternativos del Congreso de la República han pedido al presidente del Senado, Lidio García que convoque a una reunión urgente en la corporación para debatir sobre su llegada y su propósito en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por medio de su cuenta en Twitter, la senadora de la Unión Patriótica, Aida Avella cuestionó las intenciones de la estadía de integrantes del Ejército de los Estados Unidos, "señor Ministro de Defensa, Ud, le puede aclarar al país, si los soldados Estadounidenses, vienen desarmados? Según sus declaraciones vienen a "'asesorar" A quién engañan", declaró.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Defensores de derechos humanos han alerta que según el [artículo 173](https://www.constitucioncolombia.com/titulo-6/capitulo-4/articulo-173) de la Constitución es requisito que cualquier presencia de tropas extranjeras en territorio colombiano sea debatida y autorizada de forma previa por el Senado. [(Le puede interesar: "La democracia y la soberanía están en cuarentena"](https://archivo.contagioradio.com/la-democracia-y-la-soberania-estan-en-cuarentena/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AidaAvellaE/status/1266718564502900736","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AidaAvellaE/status/1266718564502900736

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque según autoridades de ambos países la llegada de la Fuerza de Asistencia de Seguridad del Ejército de Estados Unidos el próximo 1 de junio tiene el propósito de “colaborar en la lucha contra el narcotráfico”, organizaciones sociales han advertido que su llegada puede implicar una escalada del conflicto en los territorios y posibles confrontaciones con otras naciones. (Le puede interesar: [«Ejército quería que Ariolfo Sánchez fuera un «falso positivo»: Comunidad de Anori, Antioquia»](https://archivo.contagioradio.com/ejercito-queria-que-ariolfo-sanchez-fuera-un-falso-positivo-comunidad-de-anori-antioquia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/LeonVaLenciaA/status/1266010763807916037","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/LeonVaLenciaA/status/1266010763807916037

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Al respecto organizaciones sociales en lugares como Catatumbo y el sur de Córdoba entre ellas la [Asociación Campesina del Catatumbo (ASCAMCAT) ](https://twitter.com/AscamcatOficia/status/1266010360328454150), los campesinos del sur de Córdoba y la [Cumbre Agraria, Campesina, Étnica y Popular](https://www.cumbreagraria.org/defendamos-la-soberania-nacional/) hicieron un llamado a respetar la soberanía nacional y la labor del Congreso con relación a la llegada de tropas extranjeras a Colombia.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
