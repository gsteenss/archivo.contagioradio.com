Title: Sustitución de cultivos de uso ilícito se realizará en 11 veredas de Briceño, Antioquia
Date: 2016-07-12 16:03
Category: Nacional, Paz
Tags: Antioquia, Cultivos de uso ilícito, gobierno FARC, proceso de paz
Slug: sustitucion-de-cultivos-de-uso-ilicito-se-realizara-en-11-veredas-de-briceno-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/cultivos-de-uso-ilicito-e1468357035275.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Presidencia 

###### [12 Jul 2016]

Iniciaron las labores de sustitución de cultivos de uso ilícito en el municipio de Briceño, al norte del departamento de Antioquia en el marco del acuerdo sobre drogas de la mesa de conversaciones de La Habana. Allí **se piensa intervenir 11 veredas que económicamente dependen del cultivo de coca.**

Durante el fin de semana llegó una delegación de las FARC liderada por Pastor Alape junto a una delegación del gobierno en cabeza del ministro para el posconflicto Rafael Pardo, con el objetivo de iniciar las labores del programa de la mano de la **comunidad que además exige garantías de seguridad y de inversión social.**

Son dos las principales preocupaciones de cerca de 9 mil pobladores. La primera tiene que ver con **la presencia de otros actores armados en la zona que viven de los cultivos de uso ilícito,** y la segunda es sobre el nivel de compromiso de parte del gobierno, pues se debe tener en cuenta que pese a lo prometido en el plan de desminado humanitario **nunca se le cumplió a  los habitantes con la construcción de vías y demás obras de inversión social** que irían de la mano con la descontaminación de explosivos en ese territorio, señala Fabio Muñoz, integrante del Movimiento Ríos Vivos de Antioquia.

Es por ello, que la comunidad ha exigido que se le tenga en cuenta a la hora de organizar e implementar este plan de sustitución en **el que participarán 400 familias que dependen de esta economía, y que ahora empezarían a plantar cacao y otro tipo de cultivos** de acuerdo con la creación de proyectos productivos suscritos entre el Gobierno Nacional y las FARC.

Para próximo 18 de julio se llevará a cabo una de las primeras mesas para socializar con la comunidad el punto de drogas acordado en La Habana, y  de acuerdo a los avances la población espera que se realicen otras asambleas comunitarias con el fin de identificar las **necesidades frente a empleo, salud y educación que durante años han sido temas abandonados por el Estado.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en [Otra Mirada](http://bit.ly/1ICYhVU) por Contagio Radio 
