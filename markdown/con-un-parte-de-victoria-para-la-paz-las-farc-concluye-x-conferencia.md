Title: Con un "parte de victoria para la paz" las FARC concluye X Conferencia
Date: 2016-09-23 13:20
Category: Nacional, Paz
Tags: colombia, Conferencia de las FARC, FARC, paz
Slug: con-un-parte-de-victoria-para-la-paz-las-farc-concluye-x-conferencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/unnamed-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

##### [23 Sep 2016] 

"Llegó la paz, díganle a Mauricio Babilonia que ya puede soltar las mariposas amarillas", fue la frase con la que Iván Marquez cerró la rueda de prensa en el marco de la clausura de la X Conferencia de las FARC, en el que esa guerrilla pasa a ser un movimiento político.

Este viernes fue difundido el comunicado oficial por el cuál se da el cierre a la Conferencia, dándo un "parte de victoria para la paz de Colombia" y anunciando lo alcanzado durante estos 7 días.

Los siguientes son los puntos del "Comunicado X Conferencia de las FARC"

1.  La 10 Conferencia de las FARC ha concluído sus deliberaciones con un parte de victoria para la paz de Colombia.
2.  Informamos al pais, a los gobiernos y los pueblos del mundo, que los guerrilleros y guerrilleras, delegados a la Conferencia, han dado su respaldo unánime al Acuerdo Final de la Habana, reafirmando la cohesión interna que ha caracterizado a las FARC en su trayectoría rebelde.
3.  La Conferencia guerrillera ha reiterado su confianza en el Estado Mayor Central y su secretariado, destacando que ha conlcuido con acierto el proceso de reconciliación  y determina la ampliación de dicha instancia a 61 integrantes, para lo cual ha dispuesto realizar en los próximos meses un pleno, que además se ocupará de las tareas políticas por venir.
4.  A las compañeras y compañeros de que hicieron parte del Primer Frente, la Conferencia Guerrillera les dice que las FARC- EP, en proceso de conversión a Movimiento Político legal, tiene sus puertas abiertas, para acogerlos como integrantes de una misma familia.
5.  Felicitamos a los medios de comunicación por su extraordinario cubrimiento del día a día, de los avances de la X Conferencia desde las sabanas del Yarí, trabajo de altas calidades profesionales que hizo posible que el pais estuviese informado oportunamente.
6.  La reconciliación del pais no deja ni vencedores ni vencidos. Ha ganado Colombia y ha ganado el continente. Que la paz nos abrace a todos.
7.  La clausura de la Conferencia tendrá lugar a las 5 de la tarde en la Plaza de eventos el diamante.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
