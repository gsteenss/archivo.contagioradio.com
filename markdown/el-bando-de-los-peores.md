Title: El bando de los peores
Date: 2016-05-06 09:59
Category: Javier Ruiz, Opinion
Tags: alvaro uribe velez, Uribistas
Slug: el-bando-de-los-peores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/fanatismo-uribista.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### AlAlFoto: princo 

#### **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/) - [@contragodarria](https://twitter.com/ContraGodarria)** 

###### 6 May 2016 

### **Uribistas:** 

[Alguna vez Voltaire dijo que “cuando el fanatismo ha gangrenado el cerebro la enfermedad es casi incurable”. Su fanatismo descontrolado los ha llevado a cometer muchas estupideces de las cuales ustedes no se avergüenzan. Ya Bertrand Russell decía que los estúpidos estaban seguros de todo y para ser sincero su fanatismo no tiene cura. Al principio me negaba creer que su devoción a su líder, cuyo nombre ya sabemos, se podía revertir pero creo que no es así.   ]

[Cuando una persona con argumentos sólidos refuta sus [argumentos falaces en defensa de su líder](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/twiiter-contragodarria.png) y todo lo que implica esa persona, ustedes recurren a la mentira, a la difamación, a la calumnia y a la amenaza. Yo sé que su modelo a seguir no es un tipo ideal de líder para mí pero si les gusta su estilo que raya en lo criminal ya los voy entendiendo y como buenos borregos siguen los dictados de su admirado líder. Es notorio que ustedes son acéfalos y no piensan por sí mismo.]

[[Defienden una guerra que no la han luchado y que no la lucharan](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/14.png) porque en su arribismo dirán que se maten los pobres cuando ustedes no son exactamente unos millonarios. Piden guerra desde la comodidad de su casa donde no van a llegar las balas o las explosiones. [Llaman la atención de la peor manera](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/twitter-contragodarria.png) para que su querido líder les mande un saludo de aprobación por defender sus “postulados” y ganar liderazgo dentro de su grupúsculo. Si es necesario amenazar se amenaza. Si es necesario mentir se miente. Si es necesario señalar se señala pero si es necesario matar ustedes dicen “un momento, con señalar basta porque el trabajo sucio lo hacen otros”. Y algunos de ustedes son muy nostálgicos de aquellos señores que a punta de motosierra, machete y fusil “pacificaban” el país y que al día de hoy siguen matando a defensores de DD.HH. y amenazando a quienes ustedes consideran “incomodos” para sus propósitos. Ustedes solamente meten miedo y lo hacen mal porque hasta eso ni les funciona. [La cobardía se les nota y por eso su actuar perverso e irracional](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/15.png).]

[Por eso he decidido recurrir a mi derecho a la intolerancia para decirles que su forma de expresión no debe ser aprobada ni tolerada. Me disculpan, pero hay que dejar de lado algunos preceptos liberales donde se nos dice que toca tolerar las expresiones u opiniones diferentes cuando ustedes no hacen el ejercicio, cuando no hay reciprocidad. Las burlas, los sarcasmos y la crítica mordaz a sus argumentos falaces que he llevado a cabo son “mis armas” y cómo veo eso es tan “peligroso” para ustedes que ya me vieron cómo alguien incomodo e intentan emular a quienes hacen el trabajo sucio. Emular a los rufianes de esquina que hacen panfletos amenazantes con mala ortografía, a los que hacían las chuzadas o a los mismos paras que con mucha nostalgia extrañan.    ]

[También he visto que ustedes odian y dicen combatir al “comunismo” pero no les parece irónico que ustedes con su “querido líder” quieran convertir al país en una Corea del Norte. Que yo sepa ese país es comunista donde todos deben si o si admirar al “querido líder” y si alguien no lo hace es castigado severamente. Pues yo no quiero que Colombia se vuelva una dictadura como aquella del 2002-2010 donde si alguien estaba en oposición al “querido líder” era perseguido y estigmatizado.]

[Seguiré como he venido haciendo con mis opiniones. Ustedes suelen dividir así que haré lo mismo y he decidió llamarlos a ustedes de aquí en adelante “el bando de los peores”   ]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
