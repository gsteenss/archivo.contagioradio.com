Title: Si la ciudadanía importara Colombia y Venezuela tendrían un diálogo
Date: 2020-04-07 17:36
Author: CtgAdm
Category: Actualidad, DDHH
Slug: si-la-ciudadana-importara-colombia-y-venezuela-tendrian-un-dialogo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Diseño-sin-título-12.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El primer fin de semana de Abril comenzó con el desplazamiento cerca de 700 connacionales, que cruzaron la frontera en Cucutá, en busca de un lugar seguro donde pasar la crisis producto del Covid-19, esto según el Servicio Administrativo de Identificación, Migración y Extranjería (Saime) de Venezuela,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo lamentable de esta migración, es que cientos de venezolanos salieron de países como Colombia, donde residían desde el inicio de la crisis y hoy se van obligados a desplazarse forzadamente debido a los casos de violencia, discriminación y agresiones psicológicas, que constituyen un marco de xenofobia que cada día crece más entre los colombianos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/NicolasMaduro/status/1246860281462079489","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/NicolasMaduro/status/1246860281462079489

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En sus redes sociales el presidente de Venezuela Nicolás Maduro, expresó su disposición para recibir a los más de 1000 venezolanos que han retornado desde el pasado 20 de marzo al país. (Le puede interesar: <https://archivo.contagioradio.com/informe-revela-las-barreras-que-afrontan-mujeres-venezolanas-en-el-acceso-al-aborto-legal/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Colombia y Venezuela dos hermanas en constante disputa

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Ronal Rodríguez, Coordinador del Observatorio de Venezuela de la Universidad del Rosario, Colombia vive un "*fenómeno de movilidad humana bastante complejo"*, esto debido a que **cerca de 4,5 millones de Venezolanos que han salido de su país y más de 1 millón de ellos están en Colombia**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que según Rodríguez recibe **un país con precaria experiencia en recepción de movilidad humana**, *"esto genera fenómenos de xenofobia o aporofobia, que es el miedo al extranjero o al pobre"*, en Colombia muchas veces lo desconocido, es marginado y rechazo, reflejo de ellos son las miles de [denuncias acumuladas por agresiones](https://archivo.contagioradio.com/lucha_mujeres_migrantes_8m/) hacia los migrantes.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Lastimosamente muchos colombianos creen que apenas caiga el Gobierno de Nicolás Maduro, los venezolanos van a regresar a su casa y nosotros simplemente vamos a pasar la página como si nada de esto hubiese pasado"*
>
> <cite>Ronal Rodríguez |Coordinador del Observatorio de Venezuela de la Universidad del Rosario </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según Rodríguez, no es cierto que los venezolanos que lleguen a territorio [colombiano](https://www.justiciaypazcolombia.com/mision-de-verificacion-de-las-naciones-unidas-en-colombia-2/) se van a ir del país, cuando pase la '*crisis'* , *"al contrario muchos encontraron acá estabilidad para sus familias, sus empresas y su economía, pero lamentablemente las medidas nacionales se van a quedar cortas en términos de contención y adaptabilidad".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que este es el momento en el que el Gobierno, debe hacer un análisis y replanteamiento las normativas migratorias, *"la gran mayoría de medidas tienen un error gigante y es que se sustentan en la temporalidad siempre dejan un espacio demasiado grande diciendo que este es un fenómeno que se superará en 2 años, meses después sabemos que no es verdad".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, afirmó que producto del aislamiento y a la cuarentena muchos venezolanos que vivían del diario y del trabajo informal se han empezado a desplazar nuevamente a Venezuela país que está mucho menos preparado para enfrentar el Cobit-19, y que por ellos es necesaria una "*coordinación entre Venezuela y Colombia, porque hay 12 millones de personas en la frontera que requieren acciones contundentes he inmeditas"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un espejo que nadie quiere reconocer

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde Colombia son muchas las voces de expertos, políticos, medios de comunicación y población en general que atacan el Gobierno de Nicolás Maduro, y resaltan la gestión Colombiana, ignorando tal vez las similitudes de estas dos hermanas en conflicto que cada vez son mas gemelas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las relaciones son históricas y se entrelazan desde el mismo momento de la independencia y los principios del libertador de ambas naciones, sin embargo no hay que ir muy lejos para reconocerlas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En primer lugar mientras se dice que en Venezuela la gente muere de hambre, y de sed debido a la falta de acceso al agua potable, en Colombia la pandemia puso en evidencia los miles de colombianos que tenían bloqueado este recurso elemental y todo por no poder pagar el servicio del agua, [un agua que vale la pena aclarar, no es ni potable, ni de calidad.](https://archivo.contagioradio.com/estamos-nadando-en-nuestras-propias-heces-denuncian-prisioneros-politicos-de-farc/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se dice que el Gobierno de Venezuela no es transparente con sus ciudadanos, manejando información a lo que pueden decir o conocer los medios nacionales; por su lado Colombia conoció hace algunos meses las intervenciones telefónicas que por años tuvieron funcionarios y medios de comunicación, dejando a la luz la persecución a periodistas y la compra de información.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y si analizamos la situación actual, Venezuela recibe a sus ciudadanos con la promesa de proteger y velar por la vida de todos en medio de la pandemia, información que varios venezolanos han catalogado como *"politiquería y acaparamiento mediático, para que regresamos a nuestro país sin garantías y con la plena seguridad de o que nos morimos de hambre o por el coronavirud"*, señaló Orieta Vergara- Migrante venezolana en Colombia

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### EE.UU un avivador del fuego en medio de dos naciones en conflicto

<!-- /wp:heading -->

<!-- wp:paragraph -->

No es un secreto que muchas de las situaciones que vive Venezuela, están directamente relacionadas con el accionar de Estados Unidos y Colombia, mediante un bloqueo económico y político inhumano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ejemplo de ellos es el anuncio que hizo el pasado 2 de abril el Gobierno de Estados Unidos de movilizar un equipo de Fuerza naval militar, compuesto por buques destructores, barcos de combate, aviones y helicópteros cerca de aguas venezolanas con el fin de realizar tareas de "vigilancia".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto luego de que el Departamento de Justicia del país norte americano, señalara al presidente Nicolás Maduro y otros altos dirigentes de tráfico narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acción que respaldó el Gobierno de Duque no solo aplaudiendo el anuncio de Trump, sino negando desde meses atrás la gobernada como legitimo presidente de Maduro y reconocimiento a Juan Guaidó como primer mandatario.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1246618669775495168","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1246618669775495168

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Acción que no es tampoco ignorada por diferentes magistrados del país, quienes redactaron un documento dirigido a Duque, señalando que la prioridad actual es superar la crisis del coronavirus, y que cualquier medida especialmente aquella que involucre las relaciones internacionales deben ser debatidas en el Congreso de la República.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

 Y agregaron, *" sería sumamente grave desviar y comprometer recursos del presupuesto, capital humano y capacidad institucional en una guerra para derrocar un gobierno de la región"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"En unos años el mundo no recordará la fuerza política de los dirigentes, sino como dos Gobiernos se unieron por la vida de sus ciudadanos"**
>
> <cite> Ronal Rodríguez | Coordinador del Observatorio de Venezuela de la Universidad del Rosario </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según el Coordinador del Observatorio de Venezuela de la Universidad del Rosario, *"Venezuela es el vecino más importante que tenemos independientemente de las políticas, deberíamos tener una relación estrecha, pero lamentablemente hemos dejado esta relación en manos de un sector muy pequeño que no ha sabido manejar esto de forma adecuada"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que, estamos pasando dificultades muy altas, que superan a los países y que requieren de la búsqueda de nuevas relaciones con el gobierno de Venezuela para afrontar la crisis, *"este no va a ser un tema que pase cuando mejore la pandemia al contrario esta va a ser una enfermedad que va a marcar los próximos años del mundo, por eso es importante actuar ahora;lo que quedará en la historia no es cuál país se recuperó primero económicamente, sino como los países se aliaron para atender a la población".*

<!-- /wp:paragraph -->
