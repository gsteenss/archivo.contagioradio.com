Title: Fue asesinado el comunero Cristian Yatacué de 21 años en Toribío, Cauca
Date: 2019-12-16 18:19
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinato de indígenas, Cauca, juventud rebelde
Slug: fue-asesinado-el-comunero-cristian-yatacue-de-21-anos-en-toribio-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Cristian-Yatacue.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

El pasado 15 de diciembre en Toribio, Cauca fue asesinado Cristian Andrés Vitonas Yatacué, comunero indígena y defensor de DD.HH. de 21 años edad quien pertenecía a la **Asociación Indígena Avelino Ul** y hacía parte de los procesos de formación de los jóvenes del municipio, además había participado en la Minga por la Vida que se llevó a cabo entre marzo y abril de este año.

Según Alejandro Salazar, parte de Juventud Rebelde de Cauca, gremio al que también pertenecía Cristian, los hechos ocurrieron en la vereda El Manzano en Toribio donde el joven departía junto a la comunidad, hasta que, en horas de la madrugada ingresaron a la vivienda tres personas armadas de las que se desconoce su identidad, disparando contra el comunero.

Alejandro señala que Cristian no había recibido amenazas en su contra pero es enfático en que su asesinato obedece a las dinámicas del territorio donde la presencia de paramilitares, disidencias y carteles que utilizan el territorio  como corredor, plantean un escenario **"de confrontación y de zozobra  latente en nuestros territorios"**. [(Le puede interesar: Recuperar su territorio le está costando la vida a indígenas en Cauca) ](https://archivo.contagioradio.com/nos-estan-asesinando-llamado-de-comunidades-indigenas-del-cauca/)

**"Mas que hechos aislados son actos sistemáticos que se dan en el Norte del Cauca",** indica Salazar, explicando que con el asesinato de Cristian Yatacué, ya son tres los miembros de la Asociación Indígena Avelino Ul asesinados en los últimos tres meses en Toribio. A través de la Alerta Temprana 026 de 2018 de la Defensoría del Pueblo se ha establecido que este gremio es una de las organizaciones que más riesgos vive en el territorio.

Señala además que el asesinato del comunero es un golpe a los nuevos liderazgos del departamento, "son nuevos liderazgos que se van dando en las zonas, jóvenes que desde muy temprana edad se han organizado para defender sus territorios y cada sangre que se derrama, más allá de lo familiar, afecta a toda la comunidad". [(Lea también: Líder indígena, Genaro Quiguanas fue asesinado en Santander de Quilichao) ](https://archivo.contagioradio.com/lider-indigena-genaro-quiguanas-fue-asesinado-en-santander-de-quilichao/)

Juventud Rebelde ha exigido a las autoridades que avance en sus investigaciones y prevenga este tipo de acciones, "las investigaciones no avanzan más allá de un simple saludo a la bandera por parte del Gobierno" afirma Salazar quien resalta que con el asesinato de Cristian ya son dos los homicidios ocurridos contra integrantes de Juventud Rebelde y un total de 217 homicidios contra integrantes de Marcha Patriótica.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
