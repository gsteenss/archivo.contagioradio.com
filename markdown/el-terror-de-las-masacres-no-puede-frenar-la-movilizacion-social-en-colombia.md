Title: El terror de las masacres no puede frenar la movilización social en Colombia
Date: 2020-08-20 20:54
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Aida Avella, Masacres, Violencia contra niños
Slug: el-terror-de-las-masacres-no-puede-frenar-la-movilizacion-social-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/paro-21-noviembre-1-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En menos de 10 días, se han reportado 3 masacres en los departamentos de Nariño, Cauca y Valle del Cauca. Niños y jóvenes han sido las víctimas de estos actos que siguen develando la crisis que se vive en los territorios, ocasionando terror y el silenciamiento de las comunidades. (Le puede interesar: [Nueva masacre, en Nariño fueron asesinados 9 jóvenes](https://archivo.contagioradio.com/nueva-masacre-en-narino-fueron-asesinados-9-jovenes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luis Olave, coordinador de Afro Red Latinoamericana y el Caribe explica que la situación es crítica en los territorios y los niños y jóvenes son los más vulnerables pues existen interés de los grupos armados en recluirlos.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Nosotros sentimos esta masacre como un mensaje de amenaza para los demás niños, para decir, o vienen con nosotros o los seguimos matando”.
>
> <cite>Luis Olave</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Asimismo, Olave y Lourdes Castro, coordinadora del programa Somos Defensores concuerdan en que es preocupante que cosas tan “escabrosas” como las masacres y los asesinatos se puedan dar en territorios que cuentan con alta presencia militar y que la única función del Gobierno, la policía y el ejército sea oprimir y no dar respuesta; por el contrario, “el estado ha llevado más guerra al territorio”. 

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Estamos totalmente abandonados en el territorio y a pesar de los acuerdos que se vienen realizando, \[...\] los territorios quedaron abandonados y no hay presencia institucional y los grupos narcoparamilitares son los que están gobernando en el territorio”.
>
> <cite>Luis Ovale</cite>

<!-- /wp:quote -->

<!-- wp:heading -->

** “La calle va a ser la única posibilidad de detener toda esta agenda de terror”**
-----------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Todos estos actos de violencia llenan de pánico y terror a quienes, en protección de sus vidas, van silenciando sus voces de liderazgo, llevando a que se vean afectados los procesos de denuncia y de construcción de paz. (Le puede interesar: [Policía Nacional estaría involucrada en masacre de los niños en Llano Verde, Cali](https://archivo.contagioradio.com/policia-nacional-estaria-involucrada-en-masacre-de-los-ninos-en-llano-verde-cali/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún así, compartiendo que “la pandemia para nosotros en el territorio es la guerra” Olave invita a que desde las ciudades se convoque y se proteja el territorio de todas las maneras posibles, ya sea por medio de redes sociales o en las calles.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Esto es una expresión de inconformismo y de agotamiento que va creciendo \[...\] esto da razones y motivos para la esperanza”. 
>
> <cite>Lourdes Castro</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

A esa invitación se suma Alberto Yepes, coordinador del observatorio de DD.HH. y DIH de Coeuropa comentando que **“la calle va a ser la única posibilidad de llamar a rendir cuentas y detener toda esta agenda de terror que quieren imponer en aras de garantizar la impunidad de un solo sujeto”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pero así como es importante manifestarse, la senadora Aida Avello, llama a los colombianos a reflexionar por quienes se está votando para liderar el país y garantizar el bienestar de las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esos actos, Yepes agrega que es necesario “preparar el terreno para el recambio de las autoridades” para que no haya una fragmentación y que todos los sectores puedan participar en esa “reconstrucción de la paz”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Si desea escuchar el análisis completo Haga Click Aquí](https://www.facebook.com/contagioradio/videos/324202188725648)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
