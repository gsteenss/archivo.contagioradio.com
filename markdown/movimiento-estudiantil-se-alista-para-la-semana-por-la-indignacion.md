Title: Movimiento estudiantil se alista para la semana por la indignación
Date: 2019-10-18 18:21
Author: CtgAdm
Category: Educación, Movilización
Tags: ESMAD, estudiantes, marcha, Movilización
Slug: movimiento-estudiantil-se-alista-para-la-semana-por-la-indignacion
Status: published

###### [Foto: Contagio Radio  
] 

El pasado jueves 17 de octubre diferentes manifestaciones se tomaron las principales capitales del país, los jóvenes protestaban contra el artículo 44 del Presupuesto General de la Nación (PND), las reformas laboral y pensional, y pedían el cumplimiento de los acuerdos alcanzados el año pasado. Sin embargo, la jornada de movilización se vió afectada nuevamente por la intervención del Escuadrón Móvil Antidisturbios (ESMAD), que agredió a los estudiantes, y es la principal causa de la jornada de indignación que se realizará la próxima semana.

### **La violencia del ESMAD: Una razón más para protestar** 

Julián Báez, representante estudiantil de la Universidad Distrital, explicó que en la institución están viviendo una coyuntura interna propia: Se perdieron unas carpetas con información sobre el caso de corrupción que se está investigando, el Consejo Superior Universitario (CSU) no quiere responder aún por la demanda de los estudiantes de establecer una asamblea universitaria y tampoco se ha querido aumentar el presupuesto para los viajes académicos. (Le puede interesar: ["Estudiantes de la Universidad Distrital tienen razones de sobra para protestar"](https://archivo.contagioradio.com/estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar/))

Adicionalmente, los jóvenes se movilizaron en rechazo a la propuesta de pagar el 75% del salario mínimo durante un año a los egresados de carreras universitarias durante un año, y contra la idea de aumentar la base de cotización para la pensión así como eliminar el régimen de prima media. Propuestas que Báez calificó como **un atentado contra el trabajo digno de los jóvenes**. (Le puede interesar: ["De 6 países que privatizaron su régimen pensional, 4 revirtieron la decisión: Wilson Arias"](https://archivo.contagioradio.com/de-6-paises-que-privatizaron-su-regimen-pensional-4-revirtieron-la-desicion-wilson-arias/))

En el caso de la **Academia Superior de Artes de Bogotá (ASAB),** parte de la Universidad Distrital, los estudiantes salieron fuera de su sede en la Caracas con Calle 14, pero fueron reprimidos por el ESMAD. Báez afirmó que contra los jóvenes se dispararon balas de goma y pintura, y capturaron a varios estudiantes, incluído un defensor de derechos humanos que fue llevado a una comisaría "bajo un procedimiento irregular". (Le puede interesar: ["ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles"](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/))

### **¿Y contra el artículo 44 del Presupuesto General de la Nación...?** 

El líder estudiantil aclaró que el artículo 44 del PND , según el cual "las universidades estatales pagarán las sentencias o fallos proferidos en contra de la Nación con los recursos asignados por parte de esta", ya había sido incluído en otros presupuestos desde 2014, pero la diferencia **en esta ocasión es que se puede ver materializado.** Según Báez, ello se podría prever dada la insistencia del Gobierno en las plenarias de Cámara de Representantes y Senado para aprobar este artículo.

Báez señaló la inconveniencia de este artículo, al señalar que "como estudiantes estamos realizando unas demandas al Estado por violación a los derechos humanos, en caso de ganarlas, seríamos nosotros, desde las universidades, los que tendríamos que pagarlas". (Le puede interesar: ["Como falsos positivos judiciales, califican capturas de estudiantes en Pereira"](https://archivo.contagioradio.com/como-falsos-positivos-judiciales-califican-capturas-de-estudiantes-en-pereira/))

### **21 al 25 de octubre: Semana por la indignación** 

El representante de la Universidad Distrital también es vocero por parte de la Unión Nacional de Estudiantes de Educación Superior (UNEES) ante el Gobierno, en la mesa de negociación establecida luego de las protestas estudiantiles del año pasado, sin embargo, afirmó que en dicho espacio nunca se expresó lo que se haría mediante este artículo. (Le puede interesar: ["Estudiantes retomarán acciones ante incumplimiento del Gobierno a los acuerdos de 2018"](https://archivo.contagioradio.com/estudiantes-incumplimiento-gobierno-acuerdos/))

Por esa razón, declaró que las universidades están abocadas a **una jornada de constante indignación que probablemente se realice en todo el país entre el 21 y 25 de octubre**, para manifestarse contra la actuación del ESMAD y reclamar políticas que no atenten contra la juventud. (Le puede interesar: ["Desfinanciación del SENA afecta a más de 7000 estudiantes"](https://archivo.contagioradio.com/33-billones-debe-minhacienda-sena/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
