Title: Reportan asesinato de líder social en Valle del Cauca
Date: 2017-05-14 09:48
Category: Nacional
Tags: Asesinatos, lideres sociales, marcha patriotica
Slug: mariocallemarchapatrioticaasesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Asesinan-a-Luz-Herminia-Olarte-lideresa-social-de-Yarumal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Archivo 

##### 14 May 2017 

La Red de Derechos Humanos del Sur Occidente Colombiano Francisco Isaías Cifuentes, denuncio el asesinato del líder social **Mario Andrés Calle,** presidente de **Astracava-Guacarí** en el Valle del Cauca.

De acuerdo con la denuncia, el hombre se encontraba en el municipio de Guacarí, Corregimiento Santa Rosa de Tapias, Vereda Pomares pasadas las 9 de la mañana, cuando al parecer **fue impactado por 6 proyectiles de arma de fuego**. Le puede interesar: [En 2017 han sido asesinados 41 líderes sociales](https://archivo.contagioradio.com/en-el-transcurso-del-2017-han-sido-asesinados-41-lideres-sociales/).

De acuerdo con la información suministrada por la organización, Mario Andrés Calle se desempeñaba en la actualidad como presidente de la Asociación de Trabajadores Campesinos Del Valle del Cauca sub directiva Guacarí, de la Coordinación Campesina Del Valle del Cauca-CCVC, del Proceso de Unidad Popular del Sur Occidente Colombiano-PUPSOC, del Movimiento Político y Social Marcha Patriótica Valle del Cauca.

Noticia en desarrollo.
