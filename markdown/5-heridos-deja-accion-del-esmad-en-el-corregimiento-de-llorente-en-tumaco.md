Title: 5 heridos deja acción del ESMAD en el corregimiento de Llorente en Tumaco
Date: 2017-03-30 17:38
Category: DDHH
Tags: erradicación cultivos ilícitos, Tumaco
Slug: 5-heridos-deja-accion-del-esmad-en-el-corregimiento-de-llorente-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Erradicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [30 Mar 2017] 

Por lo menos 5 personas han resultado heridas luego de una acción conjunta entre el ESMAD, el Escuadrón Móvil de Carabineros (EMCAR) y la Policía Antinarcóticos, contra campesinos del corregimiento de Llorente, en Tumaco. Según la denuncia de las comunidades las **fuerzas de seguridad pretenden imponer la erradicación forzada de los cultivos de uso ilícito a pesar del acuerdo con el gobierno nacional del pasado 14 de marzo.**

La indignación de los campesinos se da también porque el pasado 14 de marzo en el Hotel los Corales de Tumaco, se llegó a la conclusión de que se respetaría el punto 4 de los acuerdos de paz **que contempla la concertación de la erradicación voluntaria y la sustitución de cultivos de uso ilícito**. Le puede interesar: ["COCCAM denuncian contratación de civiles para erradicación forzada" ](https://archivo.contagioradio.com/coccam-denuncia-contratacion-de-civiles-para-erradicacion-forzada/)

Según explicaron los campesinos, ese día se reunieron Sergio Jaramillo, comisionado de Paz, Rafael Pardo, ministro del Pos Conflicto, el gobernador de Nariño Camilo Romero, Pastor Alape, integrante del secretariado de las FARC-EP y otros funcionarios de gobierno departamental y central, así como funcionarios de las Naciones Unidas, **todos ellos fueron testigos del acuerdo. **Le puede interesar: ["Denuncian agresiones y hostigamientos contra campesinos del Caqueta"](https://archivo.contagioradio.com/denuncian-agresiones-y-hostigamientos-contra-campesinos-en-caqueta/)

Según explica Ivan Rosero, integrante de la Junta de Acción Comunal del Carmen de Llorente, las personas afectadas por la acción de las fuerzas de seguridad del Estado ascienden a **2400 personas habitantes de 3 veredas y también el pueblo indígena Awa, que cuenta con presencia en la zona** en varios resguardos y que también se han visto obligados a subsistir con el cultivo de la hoja de coca.

Hasta el momento, las comunidades continúan bloqueando el pacíficamente la carretera que de Pasto conduce a Tumaco y también tratan de impedir que continúe la erradicación forzada en las veredas. **Algunas versiones indican que la protesta continuará hasta tanto no se respete lo pactado en el acuerdo de paz y en la reunión de 14 años.**

###### Reciba toda la información de Contagio Radio en [[su correo]
