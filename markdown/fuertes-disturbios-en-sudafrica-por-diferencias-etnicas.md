Title: Fuertes disturbios en Sudáfrica por diferencias étnicas
Date: 2015-02-17 21:21
Author: CtgAdm
Category: El mundo, Política
Tags: Apartheid, Malamulele, Sudafrica
Slug: fuertes-disturbios-en-sudafrica-por-diferencias-etnicas
Status: published

###### Foto:Enca.com 

Han sido **seis semanas de fuertes disturbios protagonizados por la comunidad de Malamulele**, perteneciente a la provincia de Limpopo, quienes reivindicaban el status de municipio independiente.

La localidad actualmente pertenece al municipio de Thulamela, es por ello que se ha generado el conflicto, ya que la comunidad de **Malamulele reivindica la creación de un municipio propio independiente debido a razones **[**étnicas**].

La comunidad de **origen xitsonga, con una lengua, cultura e idiosincrasia propias**, distintas a las de la étnia tshivenda, que es quien actualmente gobierna el municipio.

La problemática se genera al ser la primera mayoritaria y no tener posibilidad de autogobierno, dicha etnia **ha sido siempre relegada frente a la etnia tshivenda, han sido reprimidos política y lingüísticamente.**

El descontento estalló en las pasada elecciones de Mayo y se ha reavivado ahora. Las consecuencias de las protestas han sido similares a un paro integral, quedando sin funcionamiento los servicios básicos, incluido el transporte.

Las negociaciones con el gobierno comienzan este 17 de Febrero, y en palabras del presidente, "existe la **esperanza de poder negociar" con el fin de que la etnia deje de estar relegada en un plano político.**
