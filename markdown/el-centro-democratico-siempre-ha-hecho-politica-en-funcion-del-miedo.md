Title: "El Centro Democrático siempre ha hecho política en función del miedo
Date: 2015-06-02 14:26
Category: Entrevistas, Paz
Tags: alirio uribe, Alvaro Uribe, Centro Democrático, cese unilateral de las FARC, FARC, Frente Amplio por la PAz, Juan Manuel Santos
Slug: el-centro-democratico-siempre-ha-hecho-politica-en-funcion-del-miedo
Status: published

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4586011_2_1.html?data=lZqlmJWVdY6ZmKiak5mJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nic2fpMrb1tfTb6XZztTQ1Iqnd4a1ktnWxdSPt8rZztXfx5DMpYzcxsjV0ZDUs82ZpJiSo6nYrcTVjMrbj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe, Integrante del Frente Amplio por la Paz] 

*A pesar del informe presentado por el Centro Democrático estos 1000 días de las conversaciones de paz dejan balance muy positivo, según Alirio Uribe.*

En medio del **ciclo 37 de conversaciones de paz entre la guerrilla de las FARC y el gobierno de Colombia**, las cifras presentadas por el Centro Democrático demuestran  que la guerra en Colombia es una barbarie que hay que parar de una vez por todas, afirma Alirio Uribe, representante a la Cámara e integrante del Frente Amplio por la Paz.

Según Uribe, si se le da credibilidad a las cifras entregadas por el **Centro Democrático** lo que se puede evidenciar es que en la reciente etapa de des escalamiento del conflicto la paz debe ser una apuesta de todas y todos, **no pueden seguirse generando víctimas de un lado o de otro**, puesto que la gran cantidad de la sangre y el dolor lo ponen las familias campesinas y humildes de nuestro país.

Recientemente las senadoras Paola Holguín y Thania Vega presentaron cifras de 927 actos de guerra, que han dejado 329 civiles heridos y 148 asesinados. También señalan que durante los mil días del proceso de paz han sido heridos 1.651 miembros de la Fuerza Pública y 700 han sido asesinados.

Por otra parte, según el **Frente Amplio por la Paz**, durante el cese unilateral de las FARC se salvaron las vidas de cerca de 160 militares, y se evitaron las heridas en combate a otros mil integrantes de las FFMM.[Ver enlace de informe de 4to mes de cese unilateral.](https://archivo.contagioradio.com/cese-unilateral-de-las-farc-ha-evitado-160-muertes-de-militares-y-heridas-a-otros-1000/)

Sin embargo, para Alirio Uribe, el **Centro Democrático siempre ha hecho política con la guerra y sus programas se han basado en la propaganda del terror** para conseguir votos impulsados por el miedo.

El ciclo actual de conversaciones de paz, que se cierra este próximo jueves 4, espera dar resultados de **avances concretos en el punto de las víctimas,** dado que es necesario que la gente recupere la confianza en el proceso, luego del escalamiento de la guerra, pero a pesar del avance del **desminado humanitario realizado en dos puntos específicos de los departamentos de Antioquia y Meta.**
