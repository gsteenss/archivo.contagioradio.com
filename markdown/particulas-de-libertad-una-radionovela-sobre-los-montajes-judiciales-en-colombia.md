Title: Partículas de libertad, una radionovela sobre los montajes judiciales en Colombia
Date: 2020-07-15 19:32
Author: AdminContagio
Category: Expreso Libertad, Programas, Radionovela
Tags: Expreso Libertad, Falsos Positivos Judiciales, montaje judicial, montajes judiciales, radionovela
Slug: particulas-de-libertad-una-radionovela-sobre-los-montajes-judiciales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/pl.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Esta 14 de julio, el **Expreso Libertad** lazó su radionovela **"Partículas de Libertad"** con el primer capítulo titulado: **Conocerte Libre**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Una historia que relata el montaje judicial hecho por un integrante de la Fuerza Pública, en contra de 5 estudiantes de universidad pública en Colombia.

<!-- /wp:heading -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La serie de 5 capítulos, está basada en los hechos del **montaje judicial conocido como Caso Lebrija**, ocurrido en el 2012 en el departamento del Santander, cuando se dio la captura de 7 jóvenes, integrantes del movimiento estudiantil y 6 de ellos, estudiantes de universidades públicas del país.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_55216956" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_55216956_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:heading {"level":6,"textColor":"vivid-cyan-blue"} -->

###### [Le puede interesar: Ni las rejas han logrado frenar el empoderamiento de las mujeres](https://archivo.contagioradio.com/empoderamiento-mujeres-carceles-colombia/) {#le-puede-interesar-ni-las-rejas-han-logrado-frenar-el-empoderamiento-de-las-mujeres .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este capítulo llamado Conocerte Libre, los 5 personajes son capturados y sindicados de pertenecer a una organización terrorista, además recuerdan cómo conocieron a Raúl, presunto integrante de la Fuerza Pública que habría generado todo un montaje en contra los jóvenes para incriminarlos.

<!-- /wp:paragraph -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D732615514152007&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Otros Programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)
