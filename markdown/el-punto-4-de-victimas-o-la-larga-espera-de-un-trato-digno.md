Title: El punto 4 de víctimas: o la larga espera de un trato digno
Date: 2015-03-20 15:50
Author: CtgAdm
Category: Nicole, Opinion
Tags: Corte Interamericana de Derechos Humanos, dialogos de paz, víctimas
Slug: el-punto-4-de-victimas-o-la-larga-espera-de-un-trato-digno
Status: published

###### Foto:Contagio Radio 

#### **Por [[Nicole Jullian]](https://archivo.contagioradio.com/nicole-jullian/)** 

###### [19 de marzo de 2015] 

El ciclo 34 de los diálogos de paz arrancó este 18 de marzo con la premisa de poner fin al punto cuarto de víctimas. Al menos así lo anunciaron varios medios masivos de comunicación. La discusión sobre mecanismos de reparación y garantías de no repetición deberán poder asfaltar el camino para que este complejo punto entre en su recta final.

El punto 4 es neurálgico pues aquí se verá el mínimo moral que imponen los equipos negociadores para honrar a cada una de sus víctimas. Los comentarios acerca de que este punto ya lleva siete meses en discusión intentan ejercer presión sobre su fin. Pero insisto, no logro imaginar que pueda existir algún argumento sustancial que permita justificar el acelere de las negociaciones en un tema tan delicado y con tan poco consenso como este.

Ya la sola discusión sobre mecanismos de reparación abre paso a un tema previo aún no resuelto: ¿quién es digno de reparación? ¿quién es víctima de este conflicto? La ley 1448, o también mal llamada ley de víctimas, intentó discursivamente posicionarse como un ejercicio legislativo moderno, ejemplo claro de buenas prácticas legislativas, ejemplo claro de justicia transicional.

Pero, por favor, tomémonos el tiempo y dediquémosle la seriedad que amerita el punto 4 de víctimas. A mi juicio sería muy coherente que en medio de este proceso de paz que intenta poner fin a 50 años de conflicto social y político, el artículo 3 de la ley 1448 que define la calidad de víctima sea declarado inexequible, en razón de su contenido excluyente o si se quiere indigno. Y es que ¿cómo puede ser considerado como un logro el hecho de que se consideran víctimas sólo “[aquellas personas que individual o colectivamente hayan sufrido un daño por hechos ocurridos a partir del 1º de enero de 1985, como consecuencia de infracciones al Derecho Internacional Humanitario o de violaciones graves y manifiestas a las normas internacionales de Derechos Humanos, ocurridas con ocasión del conflicto armado interno.”?]{.s1}

Ya es consenso nacional que Colombia vive ininterrumpidamente un conflicto social y armado desde 1964. Ahora ¿qué hacemos con las víctimas anteriores al 1 de enero de 1985?

No hay que olvidar que la plenaria del Senado del 2 de octubre de 2007 iniciada por el ex senador Juan Fernando Cristo y hoy Ministro de Interior, en la que el parlamento cedía la palabra a un grupo amplio de víctimas, terminó con algunos pocos parlamentarios escuchando sus testimonios. Ojo, estos eran los inicios del debate que daba forma a la ley 1448. Por respeto a las víctimas es un deber, entonces, traer a colación el contraste de esta precaria asistencia con aquella plenaria del 2003, que a salón lleno y con aplausos y todo escuchó los discursos de los jefes paramilitares Ramón Isaza, Ernesto Báez y Salvatore Mancuso.

También es necesario traer a colación aquí que el reconocimiento de responsabilidades y la implementación de políticas de reparación por parte del gobierno frente a graves violaciones de Derechos Humanos responde más a una exigencia de la Corte Interamericana de Derechos Humanos en sus ya 15 sentencias condenatorias en contra del Estado colombiano, que a un acto de voluntad, altura y respeto de Colombia para con sus víctimas de crímenes de Estado.

[No quiero dar a entender que las negociaciones pudieran caerse por este punto 4 de víctimas. Más bien insisto que]{.s1}el artículo 3 de la ley 1448 que define la calidad de víctima debe ser declarado inexequible, en razón de su contenido excluyente. Un trato digo a las víctimas de este conflicto pasa por la concepción moral de los equipos negociadores. Hasta ahí el fin del punto 4 se tomará su tiempo.
