Title: Cuba y la Unión Europea firman acuerdo de cooperación
Date: 2016-12-12 13:59
Category: El mundo, Política
Tags: Bloqueo económico de Cuba, Relaciones Cuba - UE, Unión europea
Slug: nuevo-acuerdo-de-dialogo-politico-y-cooperacion-entre-cuba-y-la-union-europea
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/cuba_UE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Mundo ] 

###### [12 Dic 2016] 

[El ministro de Relaciones Exteriores de Cuba, Bruno Rodríguez y la alta representante de Política Exterior de la Unión Europea, Federica Mogherini, **firmaron un Acuerdo de Diálogo Político y Cooperación para reactivar las relaciones diplomáticas** entre ambas partes y poner fin a la ‘Posición Común’ que desde 1996 impuso el bloqueo a Cuba. ]

[Bruno Rodríguez aseguró que dicho Acuerdo además, “contribuirá a enriquecer una relación histórica y culturalmente intensa, **a desarrollar en mayor medida los vínculos políticos, culturales, comerciales, financieros, científicos, académicos, deportivos y de cooperación con Europa”.**]

[Las delegaciones aseguraron que el Acuerdo de Diálogo Político y Cooperación se aplicará en un primer momento **de manera provisional mientras se lleva a cabo el proceso de ratificación por parte de la Eurocámara**, así como de los parlamentos de los 28 países del bloque.]

[Por su parte, Mogherini aseguró que “la expectativa de crecimiento del flujo económico entre la isla y el bloque económico es superar los]**2.200 millones de euros generados por las** **exportaciones de los** **28 países a Cuba** [y las importaciones que representaron unos 540 millones de euros”.]

[Algunas organizaciones sociales cubanas han manifestado que la firma del acuerdo que lleva construyéndose desde 2014 “debe tener en cuenta la interconexión entre paz y desarrollo, del imperativo de **eliminar la pobreza, de garantizar la dignidad de todos los seres humanos, de detener la amenaza de las armas nucleares y el cambio climático”.**]

[Por último, las partes indicaron que en los meses siguientes a la firma, durante la implementación provisional, se realizarán **debates sobre temas como derechos humanos, soberanía y tratados económicos.**]

###### [Reciba toda la información de Contagio Radio en][[su correo]](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por][[Contagio Radio.]](http://bit.ly/1ICYhVU) 
