Title: Improvisaciones mínimas: una historia diferente cada noche
Date: 2018-01-23 08:00
Category: eventos
Tags: Bogotá, Cultura, la maldita vanidad, teatro
Slug: improvisaciones-teatro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/image001.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Medios y Artes 

##### 20 Ene 2018 

Bajo el formato de **improvisación Long form**, en el que se combinan la representación de situaciones de la vida cotidiana con actuaciones natualistas, escenas en tiempo real y música en vivo, **la compañía teatral Improvisual Project presenta del 19 de Enero al 11 de Febrero  en Bogotá 'Improvisaciones mínimas'**

Durante cada presentación, **el grupo de artistas y el público van descubriendo la trama de la historia **construyendo conjuntamente una experiencia única e irrepetible, característica propia del formato creado en 2004 por **Sergio Paris, director de la compañía de teatro Ketó**, originario de Lima, Perú, con el que ha recorrido un gran número de festivales en Europa y América Latina.

Como técnica, **la improvisación permite al artista convertirse en dramaturgo, director y actor de cada obra mientras la va representando**, logrando así contar historias de personajes comunes y complejos, que se crean y desarrollan de manera diferente durante cada puesta en escena; un recurso del que se vale Improvisaciones mínimas en su forma más teatral y poética.

La compañía Improvisual Project, primer elenco de Long form en Colombia, está integrada en esta oportunidad por los actores **David Moncada**, director en Colombia, junto a **Víctor Tarazon**a, compartirán cada noche escenario con un elenco diferente. Entre los invitados especiales estarán: **Marisol Correa, Cristian Villamil, Camila Vallejo, Felipe Ortiz, Beto Urrea, Emmanuel Restrepo, Morella Zuleta, Daniel Orrantia, Paula Castaño, Paolo Zaccuri, Lucho Guzmán, Felipe Correa** y **Sandra Sánchez** con la música en vivo de **Betsy Rodriguez**.

Las presentaciones tendrán lugar **viernes y sábado a las 8:00 p.m. y el domingo a las 6:30 p.m.** en la Casa de **la Maldita Vanidad**, ubicada en la carrera 19 No 45ª–17 barrio Palermo, Bogotá. El bono de apoyo tiene un costo de \$ 30.000 general 2x1 menores de 25 años y 50% para quienes tienen membresía de la Maldita Vanidad. Reservas: 6055312 /[3192487560](tel:(319)%20248-7560)
