Title: Los desafíos de la Comisión de la Verdad al entrar en su tercer año
Date: 2020-12-02 18:05
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Comisión de la Verdad, conflicto armado
Slug: los-desafios-de-la-comision-de-la-verdad-al-entrar-en-su-tercer-ano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/49562431566_6871c4dda9_c-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Comisión de la Verdad, cuyo mandato inició el 28 de noviembre de 2018 y ha entrado en su tercer y último año, abarcó la primera mitad del 2019 en la preparación y despliegue de la institución en los territorios. A partir del segundo semestre y hasta la actualidad han dedicado sus esfuerzos a recopilar testimonios y simultáneamente a validarlos y analizarlos con el fin de verlos reflejados en la entrega del informe final que se espera aporte al esclarecimiento de una verdad que ha permanecido oculta durante más de 60 años.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No es sencillo hablar donde aún hay actores armados presentes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**Tania Rodríguez, directora de la Dirección de Territorios de la Comisión de la Verdad** señala que han sido varios los retos que han enfrentado en los dos últimos años, en particular la persistencia del conflicto armado y la presencia de actores armados en algunos territorios del país ha impedido que muchos, debido al temor quieran hablar de lo que ha pasado en sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma el proceso tan amplio de escucha que ha desarrollado la Comisión ha llevado a los equipos a escuchar testimonios a lugares apartados donde las poblaciones expresan que el Estado aún no ha llegado en su integralidad. Este escenario, en particular para el 2020 ha resultado difícil en medio de la pandemia generada por el Covid - 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Haba un año proyectado para seguir en las regiones, lo que ha llevado a migrar a la virtualidad, pero la Internet aún no es un bien público, lo que ha significado retrasos", explica Tania quien señala que una vez concluya el proceso de contraste, se comenzará a profundiza en las verdades con las que se comprometió la Comisión. [(Lea también: Exjefes paramilitares y de las FARC-EP piden ser escuchados por la Comisión de la Verdad)](https://archivo.contagioradio.com/exjefes-paramilitares-farc-piden-ser-escuchados-comision-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En particular, para **Audes Jiménez, coordinadora territorial de las regiones Atlántico, norte de Bolívar y San Andrés** afirma que además de las circunstancias de seguridad den zonas lejanas a las capitales donde se ha querido hacer un énfasis, ha resultado difícil reforzar ese énfasis en la cercanía y en la escucha cercana que se ha querido priorizar desde la Comisión, pese a ello el trabajo no se ha detenido.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Durante el 2020, la Comisión de la Verdad ha recopilado 5.939 testimonios de los que 1.325 han sido virtuales
>
> <cite>**Diana Britto**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

**Por su parte, Diana Britto, coordinadora del área de investigación y conocimiento de la [Comisión](https://comisiondelaverdad.co/)de la Verdad** - que se enfoca en particular en el esclarecimiento y la coordinación del informe final - afirma que dicho trabajo ha permitido escuchar las voces de víctimas y responsables y recopilar 11. 825 testimonios traducidos en 22.186 personas escuchadas. Un esfuerzo que se compilará con las múltiples investigaciones del conflicto armado derivadas de procesos judiciales o centros de pensamientos, bases de datos de manera que hay gran material o información secundaria.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Encontramos una disposición muy grande de las víctimas de contar su verdad"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Tania Rodríguez es necesario destacar la disposición que han tenido las víctimas de contar sus vivencias, volverlas a narrar e incluso encontrar aún personas que no han hablado sobre lo que les pasó, dichos avances se han visto de igual forma con excombatientes en el reconocimiento de secuestros a nivel regional, encuentros con las víctimas y de igual forma en diálogos con exparamilitares. [(Lea también: Aún existe una verdad oculta del conflicto que aguarda en el exilio)](https://archivo.contagioradio.com/aun-existe-una-verdad-oculta-del-conflicto-que-aguarda-en-el-exilio/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Asímismo señaló que las dificultades **persisten en poder escuchar el reconocimiento de la responsabilidad el Estado y la Fuerza Pública pese a la entrega de informes por parte de las Fuerzas Militares.** De igual forma se han invitado a funcionarios públicos y empresarios quienes han expresado su desconfianza en la Comisión. [(Lea también: Tras la verdad, hay un sector que no quiere perder el poder del control ideológico)](https://archivo.contagioradio.com/sector-teme-verdad-no-quiere-perder-poder-control-ideologico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La directora de la Dirección de Territorios de la Comisión de la Verdad afirma que en medio de este proceso de contraste **han empezado a surgir preguntas vinculadas al régimen político y el desarrollo del conflicto, la relación que existe entre territorios donde la violencia incrementó y luego se instauraron iniciativas económicas donde hubo procesos de despojo, la alianza entre la Fuerza Pública y el paramilitarismo, la relación entre el narcotráfico y el desarrollo de la violencia y cómo y cuando los actores armados empezaron a vincularse con el narcotráfico y las alianzas que surgieron.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello, Diana Britto resalta además que el poder construir lazos de confianza con las personas en los territorios, cuando se trata de un tiempo tan corto fue un reto al que se pudo responder con la llegada de la mano de entidades aliadas y cooperación internacional.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Debe ser la sociedad la que reciba este informe y se comprometa con él para que nunca más la guerra se repita"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

**"Nuestro relato no puede ser académico y acartonada, debe ser un relato que cualquier ciudadano entienda y se conecte, en últimas nuestro principal objetivo es que podamos contar lo que paso en el conflicto de una manera que conmueva a las personas",** afirma Diana quien considera que el Plebiscito del 2016 fue un primer momento en el que se pudo constatar la desconexión del país con el conflicto, por lo que se espera que la explicación del conflicto sea reparador para la sociedad y no genere más polarización.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Audes Jiménez el mayor reto, es dejar posicionado el compromiso con el legado que va a dejar la Comisión en los territorios y a su vez **comenzar la transformación del país a través de las percepciones de memoria y verdad,** "aquí necesitamos asumir la responsabilidad como sociedad: los actores armados legales e ilegales, el Estado y la sociedad civil necesita entender que el conflicto ha persistido por acción u omisión, todos hemos estado comprometidos".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Diana concluye que pese a la muerte de los comisionados **Alfredo Molano y Ángela Salazar e**n octubre de 2019 y agosto de 2020 y el impacto que tuvo su pérdida en los equipos, la llegada de Alejandro Castillejo y Leyner Palacios de igual forma ha permitido que la tarea de los comisionados continúe en medio de la adaptación de las dinámicas que implica el trabajo en la CEV. [(Le recomendamos leer: Leyner Palacios representará el legado de las víctimas ante la Comisión de la Verdad)](https://archivo.contagioradio.com/leyner-palacios-representara-el-legado-de-las-victimas-ante-la-comision-de-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
