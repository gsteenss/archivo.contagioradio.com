Title: Cese bilateral: el gran objetivo de la mesa de diálogos en La Habana
Date: 2018-05-07 12:17
Category: Nacional
Tags: Cuba, dialogos de paz, ELN, La Habana
Slug: cese-bilateral-el-gran-objetivo-de-la-mesa-de-dialogos-en-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Eln-gobierno-cese.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 May 2018] 

La delegación del ELN anunció que ya se encuentra en La Habana para reanudar el quinto ciclo de conversaciones con el gobierno de Colombia, que tendrá como objetivos principales pactar un nuevo cese bilateral entre ambos sectores, fortalecer las acciones humanitarias y dar paso al mecanismo de participación social que ya se ha adelantado desde el año pasado.

De acuerdo con el analista Víctor de Currea, esta retoma de conversaciones se ha caracterizado porque existe una actitud diferente de parte de las delegaciones "hay un nuevo **tipo de relación mucho más directo, más fraterno, conforme al espíritu de la negociación**".

Este nuevo cese al fuego bilateral que se pacte, según el analista, debe corregir los errores cometidos durante el primer ejercicio de cese, para hacerlo más fuerte y estable, y debe estar respaldado de la puesta en marcha del mecanismo en el que se ha avanzado sobre participación social.

### **Los enfrentamientos entre el ELN y el EPL** 

La grave situación que se ha denunciado en el Catutumbo, Norte de Santander, continúa dejando varias familias desplazadas que llegan a los refugios humanitarios, escapando de los enfrentamientos entre los sectores armados del territorio.

Frente a esta situación, De Currea recordó que si bien es cierto que el ELN debe demostrar una voluntad de paz, el conflicto que se esta generando entre esta guerrilla y el EPL, no esta incluida dentro de lo que se converse en La Habana, **razón por la cual son estas dos guerrillas las que deben encontrar formas de dirimir sus diferencias a través de vías no violentas**.

Organizaciones sociales ya se han ofrecidos para actuar de interlocutores entre ambas, sin embargo aún no se tiene ningún comunicado por parte de ambas guerrillas que exprese una voluntad de inicio de diálogos.  (Le pude interesar:["Catatumbo necesita que se frenen las acciones violentas: ASCMACAT"](https://archivo.contagioradio.com/catatumbo-necesita-que-se-frenen-las-acciones-violentas-ascamcat/))

### **El papel de la sociedad civil y los países garantes** 

Para De Currea el papel que han desarrollado los países garantes en este proceso ha sido fundamental, "hay que resaltar el papel de los garantes como Venezuela y Cuba al momento de trasladar la mesa desde Ecuador a la Habana" afirmó y agregó que incluso el apoyó que otorgó Ecuador durante estos primeros años de la mesa, también fueron fundamentales.

<iframe id="audio_25835935" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25835935_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
