Title: Más de 300 organizaciones y personalidades piden al Pápa interceder por la paz de Colombia
Date: 2018-10-19 16:27
Author: AdminContagio
Category: Nacional
Tags: carta al papa, dialogos de paz con eln
Slug: organizaciones-piden-papa-interceder-paz
Status: published

###### Foto:Uniminuto Radio 

###### 19 Oct 2018 

Más de 300 organizaciones religiosas, sociales, de víctimas y personalidades del país firmaron una carta dirigida al **Papa Francisco** en la que le piden **seguir pendiente de la paz en Colombia**, promovida en el gobierno anterior con la firma de los Acuerdos con la extinta guerrilla de las FARC y para que **continué el proceso con el ELN como única guerrilla que aun pervive**.

En la misiva, se pone de manifiesto al Pontífice la situación por la que pasa la negociación con la llegada del gobierno Duque, culminado el periodo de 30 días de evaluación al proceso establecido por el mandatario para determinar su continuidad, sin embargo **han pasado más de 70 días sin que se re abran formalmente los diálogos**.

La intención de las organizaciones radica en aprovechar la visita que próximamente realizará el presidente Duque a El Vaticano, para que el jerarca de la iglesia católica "**invite a este Gobierno a consolidar la implementación del acuerdo de paz, así mismo, a reiniciar la Mesa de Diálogos de Paz con el ELN**" necesarias para aliviar a las comunidades que sufren las consecuencias de la confrontación armada.

[Carta Abierta Al Papa](https://www.scribd.com/document/391189655/Carta-Abierta-Al-Papa#from_embed "View Carta Abierta Al Papa on Scribd") by on Scribd

<iframe class="scribd_iframe_embed" title="Carta Abierta Al Papa" src="https://www.scribd.com/embeds/391189655/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true&amp;access_key=key-hZkZXfOFpl4HEV82QcH5" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="true" data-aspect-ratio="null"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
