Title: Dos mil casos de violencia de género en medio de la guerra fueron entregados a la JEP
Date: 2018-08-03 14:43
Category: Mujer, Paz
Tags: conflicto armado, mujeres, Violencia de género
Slug: dos-mil-casos-de-violencia-de-genero-en-medio-de-la-guerra-fueron-entregados-a-las-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Mujeres-Comuna-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [03 Agosto 2018] 

Dos mil casos de violencia de género fueron presentados el pasado 2 de agosto a la Jurisdicción Especial para la paz, por parte de la Mesa Nacional de Víctimas. Allí esperan que haya avances concretos frente a la responsabilidad por parte de los diferentes actores armados **en la estrategia sistemática del uso de la violencia como arma sobre los cuerpos y vidas de las mujeres.**

Los casos serán presentados de forma colectiva y las organizaciones de género ya se encuentra adelantando la tarea de documentar los hechos sistemáticos con patrones de violencia basados en género en el marco del conflicto armado.

En ese sentido Juliana Rodríguez, integrante de la coordinación de la Ruta Pacífica de Mujeres expresó que se tendrá que relacionar hechos como casos d**e violencia sexual sobre las mujeres ,en distintas partes del país, que se vincularon con patrones de control del territorio** por estructuras armadas. (Le puede interesar:["¿Cómo va la implementación del Acuerdo de Paz en temas de género?"](https://archivo.contagioradio.com/implementacion-acuerdo-de-paz-en-genero/))

Asimismo señaló que el evento de ayer, fue un acto especial y al mismo tiempo doloroso, producto de la revictimización que han sufrido las mujeres en Colombia a la hora de denunciar la violación a sus derechos en la Justicia ordinaría, en donde han tenido que relatar hasta 5 veces la violencia contra ellas.

El informe de la Corte Penal Internacional, del **2015, manifestó que en Colombia había una impunidad que superaba el 95%** sobre las investigaciones en hechos de violencia contra las mujeres, debido a que desde el 2008, 634 casos llegaron a la Corte Constitucional, de los cuales solo 8 terminaron en condena.

Rodríguez manifestó que las mujeres víctimas de la violencia de género esperan que en la Jurisdicción Especial para la Paz no exista revictimización, y que sea una justicia que llegue en condiciones de dignidad, verdad y reparación.

<iframe id="audio_27581990" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27581990_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
