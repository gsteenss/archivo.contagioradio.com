Title: Comisión Étnica se ofrece como mediadora para reanudar negociación Gobierno-ELN
Date: 2016-09-07 16:32
Category: Nacional, Paz
Tags: Acuerdos de La Habana, Conpaz, Diálogos de paz Colombia, ELN, FARC, Negociaciones con ELN
Slug: comision-etnica-se-ofrece-como-mediadora-para-reanudar-negociacion-gobierno-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Mesa-Étnica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [7 Sept 2016] 

Este lunes la Mesa Étnica para la Paz y Defensa de los Derechos Territoriales, **anunció su disposición para ser mediadora en el proceso de negociaciones **entre el Gobierno y la guerrilla del ELN. Según afirma Edgar Velasco, vocero de Autoridades Indígenas Gobierno Mayor, nuestros pueblos han vivido el dolor y el sufrimiento de la guerra, de allí la importancia de que las partes [[superen el grado de desconfianza y exigencias mutuas](https://archivo.contagioradio.com/eln-el-pais-necesita-que-se-abra-la-mesa-sin-ningun-tipo-de-dilaciones/)] y se sienten a negociar.

Las partes no se pueden quedar en el anuncio de la fase pública, deben comprender el momento actual  del país en relación con los avances del [[proceso de paz con la guerrilla de las FARC-EP](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)], asegura Velasco e insiste en que a la sociedad colombiana le debe quedar claro que **es necesario lograr la consolidación de una paz completa**.

Las organizaciones que conforman la Mesa Étnica, entre ellas la Red de Comunidades Construyendo Paz en los Territorios, desde hace décadas han consolidado experiencias de resistencia no violenta en sus regiones, que han incluido la [[interlocución y mediación en negociaciones de paz](https://archivo.contagioradio.com/conpaz-pide-acelerar-negociaciones-de-paz-con-eln-y-farc/)], por lo que insisten en que **cuentan con la experiencia necesaria para destrabar las conversaciones con el ELN**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
