Title: Interrupción voluntaria del embarazo, un derecho fundamental
Date: 2017-01-12 18:05
Category: Mujer, Nacional
Tags: Aborto Legal en Colombia, Circular 003 de 2013, Interrupción Voluntaria del Embarazo, Tres causales de Aborto
Slug: interrupcion-voluntaria-del-embarazo-un-derecho-fundamental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Abortos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Opinión y Salud] 

###### [12 Enero 2017] 

Varias organizaciones de mujeres y defensoras de derechos humanos, señalaron mediante un comunicado que el fallo del Consejo de Estado sobre la interrupción voluntaria del embarazo –IVE–, mantiene vigente el **reconocimiento de la interrupción voluntaria del embarazo como un derecho fundamental** y además niega la solicitud de algunas clínicas y hospitales de hacer de la objeción de conciencia una norma institucional.

Las organizaciones Católicas por el Derecho a Decidir, Women’s Link Worldwide, De Justicia, La Mesa por la Vida y la Salud de las Mujeres, el Centro de Derechos Reproductivos y Profamilia, ven optimistas que se mantenga lo dispuesto en la **Circular 003 de abril de 2013 de la Superintendencia Nacional de Salud,** por medio de la cual se dan directrices a las EPS, clínicas y hospitales para la realización de la IVE.

Además, en la sentencia C-355 de 2006, el Consejo de Estado dispone que se trata de un derecho fundamental “cuya efectividad está directamente ligada con la responsabilidad del Estado en materia de salud, por lo cual **las garantías de su disfrute dependen del cumplimiento por parte de las autoridades de su obligación de asegurar el acceso a servicios de salud** prestados de manera integral, oportuna, con calidad y eficacia”.

### El Estado y las EPS deben garantizar el acceso al aborto 

Respecto a objeción de conciencia institucional, el Consejo señala que se trata de una decisión individual y afirma que “ningún prestador de servicios de salud privado o público, EPS del régimen subsidiado o contributivo o secretaría de salud en Colombia puede hacer uso de esta figura”.

**El cuerpo médico de algunas EPS como Coomeva, Saludvida, Saludcoop y los hospitales Erasmo Meos y San Ignacio**, manifestaron de forma colectiva su acogida al derecho de objeción de conciencia, lo que generó las aclaraciones del Consejo sobre el riesgo que corren las mujeres solicitantes de la IVE en instituciones que se nieguen a prestar el servicio.

Así, independientemente de que una EPS “tenga dentro de su personal médicos objetores de conciencia, la EPS está en la obligación de garantizar el acceso a la IVE dentro de su red de servicios y **en caso de que no practiquen las Interrupciones Voluntarias, están obligados a remitir a la mujer que lo solicita a otros prestadores que sí la practiquen”.**

Por último las organizaciones señalan en el comunicado que “la Superintendencia de Salud puede investigar y hacer labor de vigilancia a las entidades que no presten los servicios de IVE en los términos establecidos por ley (…) **las entidades de salud no pueden imponer obstáculos a las mujeres que solicitan un aborto** como pedir juntas médicas, exámenes de medicina legal, permisos de los familiares, entre otros”.

[Circular Supersalud 003 de 2013](https://www.scribd.com/document/336408196/Circular-Supersalud-003-de-2013#from_embed "View Circular Supersalud 003 de 2013 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_26385" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/336408196/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-JIgaszXcRFt73ngS5TMs&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
