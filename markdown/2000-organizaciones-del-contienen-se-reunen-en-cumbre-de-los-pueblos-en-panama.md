Title: 2000 organizaciones del contienen se reúnen en Cumbre de los Pueblos en Panamá
Date: 2015-04-08 16:23
Author: CtgAdm
Category: Entrevistas, Movilización
Tags: Comunidades indígenas de Panamá, Cumbre de las Américas, Cumbre de los Pueblos Panamá, Evo Morales, Nicolas Maduro, Panama, Rafael Correa
Slug: 2000-organizaciones-del-contienen-se-reunen-en-cumbre-de-los-pueblos-en-panama
Status: published

###### Foto: minuto30.com 

La **Cumbre de los Pueblos** que se reúne de manera paralela  a la Cumbre Empresarial y presidencial de las Américas, espera reunir delegados y delgadas de organizaciones sociales de todo el continente y algunos invitados especiales de países de Europa, Africa y Asia. Según los organizadores de la Cumbre el presidente **Evo Morales de Bolivia, Nicolás Maduro de Venezuela y el presidente Rafael Correa de Ecuador** han confirmado que asistirán a este evento.

Según Walyd Zaled, abogado defensor de **Derechos Humanos**, la Cumbre Presidencial no genera mayores expectativas en las organizaciones sociales dado que se realiza con enfoque en los negocios nacionales de los países que se representan en ese evento. “ellos van a mirar como aseguran sus capitales” afirma Zaled.

Además la Cumbre Presidencial es una especie de cortina de humo para las luchas sociales que se viven en este momento en Panamá como los **reclamos de las comunidades indígenas** que habitan el norte de ese país, así como las **reivindicaciones de los transportadores de ciudad de Panamá** que realizaron un cese de actividades a inicios de esta semana.

El defensor de DDHH también denuncia que se han declarado festivos regionales para que los habitantes de la capital salgan hacia otras regiones y no se encuentran cupos disponibles en la mayoría de los hoteles de la ciudad. Para el abogado esta es una estrategia de ocultamiento de la realidad del país.
