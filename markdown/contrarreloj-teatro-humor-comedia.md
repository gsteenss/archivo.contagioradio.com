Title: Contrarreloj de humor en 'La comedia'
Date: 2017-02-02 10:15
Category: Cultura, eventos
Tags: Bogotá, La Comedia, Stand up comedy, teatro
Slug: contrarreloj-teatro-humor-comedia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/unnamed-6-e1485983073143.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Teatro La comedia 

##### 2 Feb 2017 

En el inicio de 2017, vuelve al teatro La Comedia de Bogotá, **'Probando material', una carrera contra reloj**, en la que varios de los nuevos talentos y protagonistas del stand up comedy, **buscarán romper records con lo mejor y más reciente de sus rutinas cómicas**.

Cada noche, **11 comediantes entrarán a escena y durante 5 minutos tratarán de conquistar y hacer reir al público**, con sus conflictos y maneras particulares de ver el mundo. Una rutina que durante 2016 realizó 32 funciones con 45 artistas de colombia y de otros paises, convocando más de 2800 personas.

En esta edición, los comediantes invitados son Boti Monsalve, Andrés Torres, Manuel Figueroa, Mafe Cárdenas, Brayan Mora, Juan Buenaventura, Camilo Pardo, Santiago Gordo, Andrés Tejedor, Tato Rapsoda, Andrés Molinares, Edward Silva, Luis Otero, Nina Rancel, Alejandro Tawa, Daniel Ruge, Germán Arciniegas, Johnny Rodríguez , Fabián Garzón, Santiago Gordo, Camilo De la Cruz, Camila Nogales, Rodrígo Ferro, Juan Diego Galvis, Silvia Castañeda, Christian ‘El Pantera Rojas’, Gustavo Pérez, Harvey Neita, Luisa Ospina, Gabriel Murillo, John Malagón, Felipe Gil, Jorge Bolaño y Fernando Fonseca entre otros.

La cita con 'Probando material', es **todos los jueves hasta el 23 de febrero**, a partir de las **8 de la noche en La Comedia**, Carrera 49 No. 94-06. El costo de las entradas es de **\$20.000** con 50% dcto Tercera Edad y acceso a dos estudiantes por el precio de uno válidos en la taquilla del Teatro.
