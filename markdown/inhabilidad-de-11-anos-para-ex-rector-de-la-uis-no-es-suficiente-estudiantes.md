Title: "Inhabilidad de 11 años para ex-rector de la UIS no es suficiente" Estudiantes
Date: 2015-05-11 17:11
Author: CtgAdm
Category: Educación, Nacional
Tags: aceu, camacho pico, estudiantes, industril, mane, Paramilitar, Santander, uis
Slug: inhabilidad-de-11-anos-para-ex-rector-de-la-uis-no-es-suficiente-estudiantes
Status: published

###### **Foto: Radio Uis** 

<iframe src="http://www.ivoox.com/player_ek_4479737_2_1.html?data=lZmkm5yXe46ZmKiak5iJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRb4amk67bysbGrc3dxcbRjdXTtoylkpDOh6iXaaOl0NiYxsrQb6bsjrfSxdnTtozYxpDZw5C5jbSfz9SYx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align: center;">

[Daniel Castillo, Estudiante de la UIS]

</div>

<div>

</div>

En el año 2008 el país conoció una grabación en la que se escucha una conversación entre el entonces rector de la Universidad Industrial de Santander, Jaime Camacho Pico y alias "Felix", en la que el paramilitar solicitaba al rector del centro educativo una lista con nombres de estudiantes y profesores para ejecutar el llamado "plan pistola", amenazas y asesinatos en contra de la comunidad universitaria de la UIS.

<div>

La grabación se habría hecho 1 año antes, en julio del 2007. En más de un año, Jaime Camacho Pico no realizó denuncias ante las autoridades competentes, para alertar sobre la presencia de paramilitares en la institución, ni la violación a los DDHH que pretendían realizar. Por su inacción frente a estos hechos, a finales de abril del 2015 la Procuraduría Segunda Delegada para la Vigilancia Administrativa inhabilitó al ahora ex-rector por 11 años, para el ejercicio de cargos públicos.

Según reza la decisión, Jaime Camacho “sabía perfectamente que este tipo de hechos, comunicación con un presunto paramilitar, con intención de adelantar acciones delictivas en contra de la integridad personal de personas pertenecientes a la comunidad educativa de la UIS, son conductas de connotación delictiva, debían ser puestas en conocimiento de las autoridades correspondientes, para que se iniciaran las averiguaciones correspondientes”.

Los y las estudiantes de la Universidad Industrial de Santander saludan el fallo de la Procuraduría, sin embargo, consideran que es insuficiente y esperan que se adelante el proceso penal pertinente.

Daniel Castillo, estudiante de 6to semestre de la Universidad, asegura que los actos de Camacho Pico "han dejado rastro en nuestra universidad. Muchos estudiantes y profesores debieron exiliarse por la persecución no solo al interior de la Universidad, sino también en sus casas. Muchos incluso no ha podido volver", indica Castillo. "Las estructuras paramilitares aun continúan al interior en la Universidad, y se sientan en los órganos de decisión", asegura el estudiante.

Hasta la fecha, Jaime Camacho Pico sigue desempeñando funciones en la Universidad Industrial de Santander, como parte del grupo de investigación del parque tecnológico Guatiguará.

Los y las estudiantes de la UIS completan su tercera semana en dinámica de asamblea permanente, como respuesta a la intensión de la administración de la institución de realizar una reforma general sin tener en cuenta la participación y propuestas activas de los estamentos profesoral, estudiantil y de trabajadores.

Aseguran que, por ejemplo, la propuesta de crear la "Fundación parque tecnológico Guatiguará", en la cual habría estado trabajando el ex-rector Camacho Pico, son una forma de privatizar la universidad.

La crítica general a la manera en que se eligen los órganos directivos de la Universidad, que permitió las acciones e inacciones del rector de la UIS en el 2007, y que evaden la participación de la comunidad universitaria en sus reformas, sigue rondando los pasillos de la Universidad. "Nuestra bandera siempre ha sido la autonomía y la democracia universitaria", concluye Daniel Castillo.

</div>
