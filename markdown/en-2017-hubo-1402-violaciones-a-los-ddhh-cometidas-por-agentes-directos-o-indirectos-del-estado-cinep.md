Title: En 2017 hubo 1402 violaciones a los DDHH cometidas por agentes directos o indirectos del Estado: CINEP
Date: 2018-05-24 12:02
Category: DDHH, Nacional
Tags: Banco de Datos CINEP, CINEP, Restitución de tierras, violaciones a los derechos humanos
Slug: en-2017-hubo-1402-violaciones-a-los-ddhh-cometidas-por-agentes-directos-o-indirectos-del-estado-cinep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/ESMAD-e1471644079239.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Prensa Rural ] 

###### [24 May 2018 ] 

En un informe presentado por el Banco de Datos de Derechos Humanos y Violencia Política del Cinep y su Programa para la Paz, se registró que en 2017 hubo 1402 casos de  violaciones a los derechos humanos por **abuso de autoridad, intolerancia social y persecución política** cometidos por el agentes directos o indirectos del Estado. El informe se enfoca en valorar las violaciones cometidas especialmente en el contexto de los procesos de restitución de tierras.

El informe titulado **“No son líos de faldas, son líos de tierras”**, detalla que “la guerra sucia sigue vigente en Colombia y las noticias no son las mejores en materia de derechos humanos y derecho internacional humanitario”. Allí, establece que los grupos paramilitares fueron los principales perpetradores de violencia seguido de la Policía Nacional y Ejército Nacional.

### **Violaciones a los derechos humanos en cifras** 

En lo relacionado con las violaciones a los derechos humanos por persecución política, abuso de autoridad e intolerancia social, **entre el 1 de enero y diciembre 31 de 2017**, el Cinpe registró un aumento del 20% en el número de víctimas de ejecuciones extrajudiciales. En cifras, en 2016 hubo 115 víctimas y en 2017 hubo 138 víctimas.

Dentro del contexto de restitución de tierras, el informe indica que el año pasado se registraron **10 víctimas de ejecución extrajudicial. D**e forma paralela, el número de víctimas de amenazas que hacen parte de ese proceso fue de 67 en 2017 mientras que en 2016 hubo 9 amenazas.

A esto se suma el aumento del 44% en los delitos por tortura cometidos por agentes del Estado en 2017 cuando se registraron 42 víctimas frente a las 29 que hubo en 2016 sin tener en cuenta los 4 casos de tortura que ocurrieron contra reclamantes de tierra el año pasado. (Le puede interesar:["Impunidad, movilización social y Buenaventura en "Cien días" del Cinep"](https://archivo.contagioradio.com/buenaventura-el-escenario-ideal-del-posacuerdo/))

En lo relacionado al delito de desaparición forzada, hubo un aumento del 77% desde el 2016 al 2017 teniendo en cuenta que en este año hubo 16 personas desaparecidas. Los delitos por detención arbitraria, judicialización arbitraria y deportación por móviles de persecución política también aumentaron de 40 víctimas a 86 en tan sólo un año.

### **Paramilitares serían quienes más delitos cometieron** 

En lo que tiene que ver con los responsables de la comisión de los delitos que detalla el informe, el Cinep manifiesta que los grupos paramilitares habrían cometido al menos **770 casos** de los registrados en 2017. Seguidamente, la Policía Nacional cometió acciones represivas, especialmente a través del ESMAD, en un total de 540 oportunidades entre las que se encuentran 31 ejecuciones extrajudiciales y 134 detenciones arbitrarias.

De igual forma, las agresiones cometidas por el **Ejército Nacional** aumentaron un 22% en 2017 en comparación con 2016 teniendo en cuenta los 141 hechos victimizantes que se presentaron en 2016. Estos delitos se cometieron en su mayoría en departamentos como Valle del Cauca, Cauca, Antioquia, Chocó y Bogotá. (Le puede interesar:["Cinep exige al gobierno garantizar el derechos a la vida de los colombianos"](https://archivo.contagioradio.com/cinep-exige-a-gobierno-garantizar-la-vida-33167/))

### **Conclusiones del Cinep** 

Con las cifras detalladas que presenta el informe, el Cinep indicó que se pueden establecer tres conclusiones sobre las agresiones cometidas “por abuso de autoridad, intolerancia social y persecución política cometidos por el agentes directos o indirectos del Estado”. En primer lugar, la organización enfatizó en que **en Colombia no se ha reducido la violencia** “sino que han variado los métodos con que se ejercer”.

Segundo, “el Estado y la sociedad tiene un **compromiso con las víctimas** que debe asumir” y tercero, “hay fuerzas políticas que están desestabilizando la implementación de los acuerdos de paz”. Además, señalan que las afectaciones a los derechos humano “coarta los liderazgos” en procesos como la restitución de tierras donde el Cinep registró 56 agresiones contra líderes y procesos de restitución de tierras sólo en 2017.

[20180503 INFORME DDHH 2017 Tierras-2018mayo2 Vf](https://www.scribd.com/document/380073447/20180503-INFORME-DDHH-2017-Tierras-2018mayo2-Vf#from_embed "View 20180503 INFORME DDHH 2017 Tierras-2018mayo2 Vf on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_87016" class="scribd_iframe_embed" title="20180503 INFORME DDHH 2017 Tierras-2018mayo2 Vf" src="https://www.scribd.com/embeds/380073447/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-KgfDi8etdiPl9Z7ByINS&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
