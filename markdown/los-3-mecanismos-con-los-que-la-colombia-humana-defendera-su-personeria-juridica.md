Title: Los 3 mecanismos con los que la Colombia humana defenderá su personería jurídica
Date: 2018-08-29 15:35
Category: Nacional, Política
Tags: Angela Maria Robledo, Colombia Humana, Consejo Nacional Electoral, Gustavo Petro
Slug: los-3-mecanismos-con-los-que-la-colombia-humana-defendera-su-personeria-juridica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Df7trgxX4AESEuS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Ángela María Robledo] 

###### [29 Agost 2018] 

Cómo “macondiana” calificó Ángela María Robledo, representante a la Cámara por la Colombia Humana, la decisión del Consejo Nacional Electoral de negarle a este movimiento la personería jurídica, razón por la cual, Robledo y Gustavo Petro, **ya manifestaron la intención de apelar la medida y recuperar sus derechos políticos y garantías a la oposición a través de la tutela.**

Otros de los posibles escenarios que podrían darse serían el de la reposición o buscar que se salvaguarden los derechos políticos de esta organización en la Comisión Interamericana de Derechos Humanos.

### **Las repercusiones de la decisión del CNE** 

La medida por parte del CNE impediría que Colombia Humana se declare como fuerza opositora en el Congreso y no pueda tener las garantías del estatuto de oposición, además **tampoco permitiría que esta organización política tenga candidatos a las elecciones a alcaldías y gobernaciones del próximo año**.

En ese sentido, Robledo señaló que el CNE pasa por encima de la voluntad de más de 8 millones de personas que votaron a favor del proyecto político de la Colombia Humana y que está utilizando las leyes para “excluir y desestimar un mandato”. (Le puede interesar: ["Las polémicas decisiones del CNE, ¿Es necesario reformarlo?"](https://archivo.contagioradio.com/decisiones-cne-reformarlo/))

El senador por el Polo Democrático, Iván Cepeda manifestó que esta situación evidencia la necesidad de generar una reforma a un organismo parcializado, que garantice una independencia y autonomía y que impida que se tomen decisiones **“antidemocráticas a favor de intereses políticos identificados con el establecimiento tradicional de la política en Colombia**”.

<iframe id="audio_28201214" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28201214_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
