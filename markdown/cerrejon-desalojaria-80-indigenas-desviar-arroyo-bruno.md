Title: El Cerrejón desalojaría 80 indígenas para desviar Arroyo Bruno
Date: 2016-11-23 20:44
Category: DDHH, Entrevistas
Tags: El Cerrejón, indígenas, La Guajira, Mineria
Slug: cerrejon-desalojaria-80-indigenas-desviar-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Pueblos-Wayuu-e1479951584703.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defensoría del Pueblo 

###### [23 Nov 2016] 

La comunidad Wayúu del resguardo Rocío, en La Guajira, denunció que la empresa Carbones El Cerrejón busca desplazarlos de sus territorios para seguir ampliando la frontera extractiva en la Guajira. Según los voceros de la comunidad **el desalojo sería para evitar una consulta previa sobre la posible desviación del Arroyo Bruno** que ha anunciado la multinacional.

Cerca de **80 indígenas** **que viven a menos de 10 metros del Arroyo Bruno**, afluente principal del Río Ranchería y fuente de agua de la que dependen comunidades wayúu, afrodescendientes y cabeceras municipales como las de Albania y Maicao, serían los desalojados.

Leobaldo Sierra, gobernador del resguardo asegura que la empresa usa a dos mujeres que dicen ser dueñas del predio usando unos papeles que aún no conocen, para que luego estas personas vendan el terreno al Cerrejón.

“El domingo dijeron que nos teníamos que ir porque estábamos invadiendo tierras de ellas. Nosotros nunca hemos vendido, lo que quiere la empresa es el desvío”, expresa Sierra, quien agrega que estas mujeres viven cerca de ellos en una finca, a unos pasos de las tierras en las que la comunidad ha vivido ancestralmente.

**“Tengo 41 años y yo nací en esas tierra, y mi mamá tiene 75 años y lleva toda la vida ahí.** **Nosotros tenemos papeles que certifican esa propiedad**”. El gobernador señala que el abogado de la compañía minera dijo que las 27 familias que integran el resguardo tendrían que irse “**por las buenas, o nos echan la policía o el ejército para que nos saquen”,** dice líder indígena.

“Nosotros no estamos de acuerdo con ningún desvío. No nos interesa el billete que ofrecen, nos interesa el agua, que es la vida”, asevera Sierra, y añade, que si la empresa desvía el arroyo, deberán caminar más de 30 minutos para conseguir algo de agua.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
