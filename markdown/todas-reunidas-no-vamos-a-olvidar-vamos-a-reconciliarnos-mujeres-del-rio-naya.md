Title: "Todas reunidas no vamos a olvidar, vamos a reconciliarnos”: Mujeres del Río Naya
Date: 2017-03-17 13:04
Category: Mujer, Nacional
Tags: construcción de paz, mujeres
Slug: todas-reunidas-no-vamos-a-olvidar-vamos-a-reconciliarnos-mujeres-del-rio-naya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ENCEUNTRO-NAYA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Mar 2017] 

300 mujeres del Naya, se darán cita este fin de semana, para participar en el **“Primer Encuentro de Mujeres Rurales del Río Naya”, **en Puerto Merizalde, Valle del Cauca, con la intensión de hacer memoria y continuar con los a portes en sus saberes y luchas a la solución de los conflictos provocados por la violencia, particularmente en la Cuenca del Río Naya.

Este espacio fue convocado por la Asociación de Mujeres AINI “Fuente de Primavera de Flores” del Consejo Comunitario de la Cuenca del Río Naya y contará con tres escenarios de participación: **Memoria Histórica, Mujer Rural y Construcción de Paz, que permitirán dar diferentes reflexiones sobre el empoderamiento** de las participantes, la articulación de propuestas de paz y el fortalecimiento de organizaciones.

De acuerdo con Nidiria Ruíz, integrante de la Asociación de Mujeres AINI, las mujeres en el Naya han sido víctimas directas del conflicto y le apuestan a la paz, “hemos llorado hemos sufrido, hemos visto como han desaparecido a nuestros hermanos, esposos y padres, **ahora queremos tapar esas lágrimas con sonrisas, todas reunidas no vamos a olvidar vamos a reconciliarnos”. **Le puede interesar. ["Las mujeres tejen paz desde los territorios"](https://archivo.contagioradio.com/las-mujeres-tejen-paz-desde-los-territorios/)

El Encuentro se realizará el 18 de marzo desde las 7 de la mañana hasta las 4 de la tarde, algunas mujeres ya empezado a llegar a la Cuenca del Río Naya, **para cumplirle la cita a la memoria y la lucha de las mujeres por la construcción de paz.** Le puede interesar: ["Mujeres rurales siguen exigiendo garantías en sus derechos"](https://archivo.contagioradio.com/mujeres-rurales-siguen-exigiendo-garantia-de-sus-derechos/)

###### Reciba toda la información de Contagio Radio en [[su correo]
