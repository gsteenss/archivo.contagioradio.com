Title: EPM demanda a víctimas de Hidroituango: Ríos Vivos
Date: 2018-06-25 15:42
Category: Ambiente, DDHH
Tags: CIDH, Derechos Humanos, EPM, Hidroituango
Slug: epm-demanda-a-victimas-de-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Captura-de-pantalla-2018-06-25-a-las-3.45.20-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Ccajar] 

###### [25 Jun 2018] 

Según Isabel Cristina Zuleta, vocera de Ríos Vivos, mediante una querella a la Policía, EPM pidió que víctimas de Hidroituango sean desalojados de las oficinas de la compañía en Sabanalarga, Antioquia. La ocupación de estas instalaciones se dio luego de que las víctimas de la represa se desplazarán a las oficinas de la empresa solicitando respuesta para sus difíciles condiciones, y no encontrarán solución.

Isabel Cristina, afirmó que la querella tiene lugar mientras las víctimas por el riesgo de derrumbe que sufre la represa, están en condiciones nocivas de salud y escasez alimentaria. Situación a la que se suma que EPM no realizó el censo de los afectados. Por lo que esta querella de la que

Zuleta añadió que, aunque las personas fueron desalojadas de las zonas de riesgo, “los dejaron en el parque de Sabanalarga y de Ituango (Antioquia) sin ningún tipo de ayuda, de apoyo, de alimentación ni de albergue”. Y agregó que, aunque el compromiso con estas personas fue el de brindarles apoyo para el pago de arriendo en otros lugares, este pago solo se hizo en una ocasión y las familias están siendo desalojadas de las casas que tomaron en alquiler.

### **Situaciones Críticas en Zonas afectadas por Hidroituango** 

La vocera de Ríos Vivos indicó que adicional al tema de los arriendos, en el Coliseo de Ituango no hay servicio de baño por lo que las condiciones de quienes están allí albergados es crítica. Y quienes han abandonado los albergues, están diseminados en diferentes municipios de Antioquia e incluso en Medellín, lo que dificulta la realización del censo de afectados por la emergencia.

Igualmente, Zuleta calificó como "aterradora" la situación sobre la represa, esto por que pese a que hay derrumbes permanentes en Briceño, se levantó la alerta; por lo que muchas familias están retornando a sitios sobre los cuales aún hay peligro inminente.

### **Medidas Cautelares para las comunidades del Río Cauca** 

Por estas razones, el movimiento Ríos Vivos pidió medidas cautelares a la Comisión Interamericana de Derechos Humanos para hacer una revisión de los riesgos de la obra y, está organizando una misión de verificación por parte de la procuraduría.

De igual forma, impulsaran una misión internacional y nacional que emita un concepto sobre la posibilidad de desmantelar la represa en tanto, su construcción “desconoce los derechos de los afectados y la realidad territorial, geológica y civil de la misma obra”.

<iframe id="audio_26723641" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26723641_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
