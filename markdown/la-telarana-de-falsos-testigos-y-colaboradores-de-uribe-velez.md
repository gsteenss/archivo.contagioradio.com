Title: La telaraña de falsos testigos y colaboradores de  Álvaro Uribe Vélez
Date: 2018-02-19 18:31
Category: DDHH, Nacional
Tags: 12 apostoles, Alvaro Uribe, Iván Cepeda, Paramilitarismo
Slug: la-telarana-de-falsos-testigos-y-colaboradores-de-uribe-velez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Alvaro-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blu Radio ] 

###### [19 Feb 2018] 

El abogado Reynaldo Villalba, defensor del senador Iván Cepeda, señaló que el fallo de la Corte Suprema sobre abrir una investigación al ex presidente Álvaro Uribe Vélez, más allá de investigar sobre la manipulación de testigos, tiene que investigar el aparato criminal que se conformó **“donde participaron diversos miembros del círculo cercano a Uribe, en procura de acabar con Cepeda”** y en donde existiría una telaraña que converge en el expresidente

Para Villalba además de Álvaro Uribe, existe todo un flujograma de personajes, que no solo estarían involucrados con la creación de falsos testimonios, sino con otros hechos criminales como masacres y la creación de grupos paramilitares en el país.

Entre ellos se encuentra Wilser Molina, actual alcalde del municipio de Amagá, Antioquia y quien para el momento del inicio del proceso de Cepeda era candidato por el Centro Democrático a este cargo. Molina fue el abogado del paramilitar Luis Alfonso Tuberquia, alias “Memin”. De acuerdo con Villalba a través de “Memin”, Molina contactó a alias “Simón” y alias “Castañeda”, **testigos que Uribe presentó ante la Corte en el proceso del senador Cepeda**.

Villalba manifestó que los testimonios de ambos paramilitares son contradictorios, flaquean y “evidentemente se nota que están mintiendo”. (Le puede interesar:["Uribe ha tenido un aparato para contrarestar la investigaciones judiciales: Cepeda"](https://archivo.contagioradio.com/uribe-ha-tenido-un-aparato-para-contrarrestar-las-investigaciones-judiciales-cepeda/))

Otra de las personas que figura en este entramado es Enrique Areiza, vinculado como testigo en el proceso a el senador Alfredo Ramos, por falsificación de testigos. Según Villalba, Areiza cansado de las amenazas que le llegaban decidió llamar a la fundación que dirigía José Obdulio Gaviria, finalmente este último no pasa al teléfono. Sin embargo, Areiza expresó que Jaime Restrepo Restrepo, más conocido como “El patriota”, fue a visitarlo a la cárcel y allí le hace firmar hojas en blanco.

Posteriormente es revelada una carta escrita a mano y con la firma de Areiza, en la que se afirma que el senador **Iván Cepeda le había ofrecido 100 millones de pesos.** Cuando Areiza es llamado por la Corte Suprema manifestó que jamás recibió un ofrecimiento por parte del senador.

A estas dos personas se suma Pedro Manuel Benavides, quien fue integrante de la Policía Nacional y que tiene señalamientos por vínculos con el grupo paramilitar **“Los 12 Apóstoles”. Benavides declaró en el proceso contra el senador Cepeda**, que Alexander Amaya le había manifestado que Cepeda le dio dinero en un encuentro.

Amaya fue integrante de los “12 Apóstoles” y de acuerdo con Villalba, este jamás tuvo algún tipo de contacto con Cepeda. Durante la declaración ante la Corte Suprema, **Amaya también aseguró que no tuvo ni ha tenido ningún tipo de encuentro o comunicación** el Cepeda.

\[caption id="attachment\_51596" align="alignnone" width="653"\]![Flujograma Reynaldo Villalba](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Flujograma.jpeg){.size-full .wp-image-51596 width="653" height="663"} Flujograma Reynaldo Villalba\[/caption\]

**La investigación reservada de la Corte**

Durante este proceso, la Corte realizó una investigación reservada, es decir sin conocimiento por parte tanto del denunciado como del denunciante, en la que interceptó llamadas del actual senador Álvaro Uribe, **que de acuerdo con Villalba se produjeron en momentos cumbre del proceso**.

Uno de ellos tiene que ver con las llamadas que se producen entre el ex presidente Álvaro Uribe Vélez y Juan Guillermo Villegas, que al mismo tiempo es señalado por el cuidador de la Hacienda, las Guacharacas, propiedad de los Uribe, de ser el fundador del Bloque Metro. (Le puede interesar: ["Uribe a investigación por presunta manipulación de testigos contra Iván Cepeda"](https://archivo.contagioradio.com/uribe-testigos-cepeda/))

### **Uribe y las instancias judiciales** 

De acuerdo con Villalba este no es el primer fallo en el que se solicita realizar investigaciones a Uribe. El ex presidente ya tiene acusaciones por las chuzadas del DAS y recientemente el Tribunal de Medellín compulsó copias para que se le investigue por las mascares del Aro y La Granja “Álvaro Uribe tiene todo un prontuario de violaciones de derechos humanos en los estrados judiciales. Le corresponde a estas autoridades profundizar en las investigaciones y tomar las determinaciones a las que haya lugar” afirmó.

En el caso específico del proceso contra el senador Iván Cepeda, Villalba señaló que la investigación sobre la creación de los falsos testimonios podría culminar en un concierto para delinquir, “al senador Cepeda siempre se le hizo una propaganda desde el Uribismo de manejar una fábrica de testigos, un cartel de falsos testigos y en realidad quien ha estado manejando los hilos de los falsos testigos es un denunciante: Álvaro Uribe”.

<iframe id="audio_23879310" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23879310_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]{.s1}
