Title: Once  congresistas piden la renuncia del Defensor del Pueblo
Date: 2016-01-25 14:56
Category: Mujer
Tags: Defensor del pueblo
Slug: once-congresistas-piden-la-renuncia-del-defensor-del-pueblo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/defensor-del-pueblo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defensoria del Pueblo. 

###### 25 Ene 2016.

Los congresistas de Polo Democrático Alternativo, Iván Cepeda, Jorge Robledo, Alberto Castilla, Alirio Uribe y Víctor Correa, y de la Alianza Verde, Ángela María Robledo, Claudia López, Angélica Lozano, Ana Cristina Paz, Óscar Ospina, e Inti Asprilla, de la Alianza Verde, pidieron la renuncia del Defensor de Pueblo, Jorge Armando Otálora, luego de conocerse las denuncia de la abogada Astrid Helena Cristancho por violencia sexual mientras ejercía como secretaría privada del funcionario.

Dentro de la carta enviada por los congresistas  se solicita una investigación pronta desde la Procuraduría General de la Nación y La Fiscalia y se exige el despido inmediato del funcionario que ha debilita la credibilidad de una institución que vela por los derechos humanos.

**Comunicado de los Congresistas:  **

Doctor

**JORGE ARMANDO OTÁLORA**

Defensor del Pueblo

**C.C. EDUARDO MONTEALEGRE**

Fiscal General de la Nación

**C.C. ALEJANDRO ORDÓÑEZ**

Procurador General

Atendiendo a las graves denuncias relacionadas con hechos que presuntamente configuran delitos de violencia sexual contra una de sus trabajadoras, aprovechando su autoridad y cargo, hechos que se suman a denuncias anteriores sobre acoso laboral y malos tratos a sus subordinados; partiendo de la especial dignidad que entraña el nombre y el ejercicio de quien preside la principal institución del Estado colombiano, encargada de la defensa de los derechos humanos, y reafirmando nuestro compromiso con la defensa de los derechos humanos de las mujeres, y en especial de la defensa de una vida libre de violencias, de manera conjunta y categórica le solicitamos su renuncia inmediata al cargo. Ya, las primeras denuncias por un presunto acoso laboral a sus empleados, debió ser suficiente para que renunciara y para que las autoridades competentes iniciaran las respectivas investigaciones.

Su permanencia en la Defensoría del Pueblo quebranta la credibilidad de la institución y refuerza imaginarios patriarcales que, como servidores y públicos, debemos luchar por desmontar. Usted debe preparar su defensa y contar con todas las garantías para ello. Al tiempo que debe considerarse que la violencia contra las mujeres es un problema que se ha pretendido enmarcar históricamente en la esfera privada, como estrategia para alejar todo reproche social de los actos que en este ámbito se producen, sin embargo, en el plano internacional y dentro de nuestro ordenamiento jurídico, es claro que la violencia contra las mujeres se configura a partir de cualquier acción u omisión, que le cause muerte, daño o sufrimiento físico, sexual, psicológico, económico o patrimonial por su condición de mujer, así como las amenazas de tales actos, la coacción o la privación arbitraria de la libertad, bien sea que se presente en el ámbito público o en el privado. Violencia siempre reprochable, que puede consolidarse en las relaciones de pareja, familiares, en las laborales o en las económicas (Ley 1257/08).

En este marco, el Congreso de la República no puede ser ajeno a los hechos que se han presentado en su caso y en consecuencia, requerimos a la Fiscalía General de la Nación y la Procuraduría General de la Nación, para que adelanten las investigaciones correspondientes, y se apliquen las sanciones más drásticas, en caso de encontrarse responsable de las conductas mencionadas. Del mismo modo, pedimos que se les  brinden todas las garantías de atención sicosocial, jurídica y de seguridad a las personas que han hecho las correspondientes denuncias.

Atentamente,

**IVÁN CEPEDA CASTRO       **  
Senador de la República  Polo Democrático  
**ALBERTO CASTILLA**  
Senador de la República Polo Democrático  
**JORGE ROBLEDO**  
Senador de la República Polo Democrático  
**ALIRIO URIBE**  
Representante a la Cámara Polo Democrático  
** VÍCTOR CORREA**  
Representante a la Cámara Polo Democrático  
** ÁNGELA MARÍA ROBLEDO**  
Representante a la Cámara Alianza Verde  
** CLAUDIA LÓPEZ**  
Senadora Alianza Verde  
** ANGËLICA LOZANO**  
Representante a la Cámara Alianza Verde  
** ANA CRISTINA PAZ**  
Representante a la Cámara Alianza verde  
** INTI ASPRILLA**  
Representante a la Cámara Alianza Verde  
** ÓSCAR OSPINA**  
Representante a la Cámara Alianza Verde
