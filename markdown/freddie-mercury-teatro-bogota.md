Title: Freddie Mercury revive en el teatro con un mensaje de respeto y tolerancia
Date: 2017-09-11 16:31
Category: eventos
Tags: Bogotá, Freddie Mercury, Música, Tetatro
Slug: freddie-mercury-teatro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/freddie-mercury-magic-tour.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radionica 

###### 11 Sept 2017 

"**Yo no me llamo Freddie Mercury**" es el espectáculo musico-teatral con el que Camilo Colmenares revive en escena a la mítica voz principal de la banda inglesa Queen. Un show que, además de poner al público a cantar, **porta un mensaje de tolerancia, respeto por la diversidad y las nuevas manifestaciones de género**.

Además de rendir tributo a una de las grandes voces del rock de todos los tiempos, el montaje busca reivindicar valores y rescatar el respeto por el ser humano, **a partir de una historia inspiradora** que se cuenta en el transcurso de una hora y veinte minutos que dura el show.

![Fredy-Mercury](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Fredy-Merrcury.png){.alignnone .size-full .wp-image-46502 .aligncenter width="786" height="413"}

Durante el espectáculo, Colmenares interpretará algunos de los temas más representativos en la carrera del artista como son **Bohemian Rhapsody, Under Pressure, Crazy Little Thing CalledLove, Break Free, Love Of My Life, Somebody To Love**, agregando un toque de humor fino e inteligente.

"Yo no me llamo Freddie Mercury" se presenta en única función el **martes 12 de septiembre** en la Sala Seki Sano Calle 12 \# 2-65 a partir de las **7:30 de la noche**. La boletería tiene un costo de **\$10.000 para estudiantes y \$20.000 para público general**.

<iframe src="https://www.youtube.com/embed/VueACVjemt8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
