Title: No cesan las fumigaciones con glifosato en Putumayo
Date: 2015-06-10 12:08
Category: Ambiente, Otra Mirada
Tags: aspersiones aéreas, coca, Cultivos de uso ilícito, Glifosato, Gobierno Nacional, Mesa regional de Putumayo, ministerio de defensa, Putumayo, sustitución de cultivos ilícitos
Slug: no-cesan-las-fumigaciones-con-glifosato-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/image.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: diarioadn.co 

<iframe src="http://www.ivoox.com/player_ek_4622100_2_1.html?data=lZuflJaUdI6ZmKialZaJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRb67V08jSzsaPkdaZpJiSpJbTvoampJDS0MjFtsjVxcaYxsrQb9XZzsaYxsqPqtbhyszOxc7TssbnjMnSjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Marcela Muñoz, Mesa Regional de Putumayo] 

Desde el 7 de mayo de este año, la Policía Antinarcóticos, ha intensificado las fumigaciones aéreas con glifosato en**  9 municipios del departamento de Putumayo,** después de que el gobierno nacional anunciara que estas aspersiones tendrían que suspenderse por completo en el mes de octubre.

"La suspensión de fumigaciones con glifosato más que aplaudirla, nosotros creemos que **es justa**", dice Marcela Muñoz, encargada del tema de fumigaciones de la Mesa Regional de Putumayo.

Desde mayo de este año, las autoridades reconocieron que la erradicación con aspersión no ha sido efectiva y que además afecta la salud humana, algo que ya venían denunciando desde hace años las comunidades. “Para fumigar una hectárea de cultivo de coca, el **gobierno nacional se gasta aproximadamente \$2´600.000 y se afecta menos del 1% de las matas cultivadas.**

Muñoz resalta que los municipios **donde se ha aumentado las fumigaciones son zonas petroleras,** y asegura que las aspersiones aéreas hacen parte de un proceso para que las empresas puedan ingresar al territorio para realizar explotación del crudo, ya que generalmente, primero se realiza la fumigación y luego llega la militarización de ese sector, para que finalmente la compañía empiece sus actividades de extracción, lo que genera **desplazamiento y violaciones a los derechos humanos** de las comunidades que habitan esos territorios.

Desde el campesinado, los indígenas y las comunidades afrodescendientes se ha generado propuestas de sustitución de cultivos de uso ilícito para que el gobierno las tenga en  cuenta y genere las oportunidades para que estas se hagan realidad, sin embargo, la integrante de la Mesa Regional, asegura que **mientras las comunidades hablan de sustitución el gobierno habla de erradicación** ya sea manual o con aspersiones aéreas, lo que implica "un ejercicio forzado, donde se atemoriza y estigmatiza a los pobladores".

La propuesta de la Mesa Regional para sustituir cultivos de coca se denomina **fincas agroproductivas,** y se enmarca en un programa donde se promueva el desarrollo de la economía amazónica, basada en la comercialización de los principales cultivos de esta zona. Esta propuesta, beneficiaría a más de 50 mil pobladores y tendría un costo de 60 millones por finca, pero para que sea una realidad, **el gobierno debe realizar inversión social y debe por suspender todo tipo de erradicación que  empobrece más a las familias,** que solo tiene como salida económica el cultivo de mata de coca.

Cabe recordar que desde el Ministerio de Defensa se viene planteando el uso de otro tipo de herbicida para acabar con los cultivos de coca, algo que las comunidades no aprueban, y aseguran que  **el gobierno nacional no debería seguir usando ningún veneno como método de erradicación.**
