Title: “Estados Unidos debería suspender la ayuda militar a Colombia" Congresista Jim McGovern
Date: 2020-05-11 19:25
Author: AdminContagio
Category: Actualidad, Política
Tags: Ayuda militar, Congresista de Estados Unidos, Interceptaciones
Slug: jim-mcgovern-suspender-ayuda-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Jim-McGovern.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @McGovernMA {#foto-mcgovernma .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de conocerse el último escándalo sobre operaciones de inteligencia ilegal del Ejército colombiano en contra de periodistas, defensores de Derechos Humanos y políticos, ya se comienzan a escuchar las voces del congreso de Estados Unidos que piden suspender las ayudas militares y revisar lo que se está haciendo con la ayuda militar de ese país en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El primero en pronunciarse ha sido el representante a la Cámara por el partido Demócrata Jim McGovern, quién a través de sus redes sociales y algunos artículos de prensa sugirió que Estados Unidos revise las ayudas que ha entregado tanto en dinero como en equipos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además adelantó que la ayuda a Colombia debería suspenderse hasta tanto se verifique en qué se están usando dichos recursos. [Lea también: Operaciones ilegales del ejército son propias de regímenes totalitarios](https://archivo.contagioradio.com/seguimientos-contra-periodistas-son-propios-de-regimenes-totalitarios-flip/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Estados Unidos debería suspender la ayuda militar a Colombia y hacer un examen de arriba a abajo de cada centavo, cada pieza de equipo y cada minuto de entrenamiento que hayamos proporcionado.”
>
> <cite>McGovern</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El representante McGovern aseguró, a través de sus redes sociales que “Estados Unidos debería suspender la ayuda militar a Colombia y hacer un examen de arriba a abajo de cada centavo, cada pieza de equipo y cada minuto de entrenamiento que hayamos proporcionado.”

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El ejército incumplió su promesa de no seguir con inteligencia ilegal: McGovern

<!-- /wp:heading -->

<!-- wp:paragraph -->

Adicionalmente **McGovern recordó que “ya en 2014 se descubrieron** interceptaciones ilegales en el marco de las negociaciones de paz con las FARC “En 2014, el ejército colombiano realizó escuchas telefónicas ilegales y vigilancia de las negociaciones de paz. Nos dijeron que nunca volvería a suceder.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Hoy, más operaciones de vigilancia ilegal en \#Colombia contra defensores de derechos humanos y periodistas, incluidos periodistas estadounidenses.” “Hay algo muy, muy mal con el ejército colombiano, sus agencias de inteligencia y su profunda antipatía hacia \#HumanRights, a \#FreePress y un poder judicial independiente.”

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/repmcgovern/status/1259889438701817857?s=12","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/repmcgovern/status/1259889438701817857?s=12

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Operaciones ilegales del ejército espían a líderes comunitarias

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Comisión Intereclesial de Justicia y Paz también denunció que tanto integrantes de su equipo de trabajo como las liderezas Luz Marina Cuchumbe y Jany Silva, que pertencen a comunidades que ellos y ellas apoyan, han sido y son víctimas de estas operaciones ilegales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luz Marina ha sido ampliamente reconocida por su participación en las acciones de víctimas en la Habana en el marco de las conversaciones de paz con las FARC y ha liderado un proceso de garantías de no repetición en INza Cuaca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jany Silva es lidereza de la Zona de Reserva Campesina de la Perla Amazónica y recientemente se reveló un plan para atentar contra su vida en el casco urbano de Puerto Asís o en uno de sus [desplazamientos hacia la zona rural.](https://www.justiciaypazcolombia.com/ante-la-inteligencia-militar-ilegal-contra-dos-de-nuestras-lideresas-luz-marina-y-jani-rita-y-a-todas-las-mujeres-gracias-gestoras-de-vidas/)}

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
