Title: Las razones que explican el regreso de las masacres a Colombia
Date: 2020-08-22 10:19
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Masacres, Regreso de las masacres, violencia
Slug: las-razones-que-explican-el-regreso-de-las-masacres-a-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/El-regreso-de-las-masacres.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"white","fontSize":"small"} -->

Masacres

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La cruenta ola de violencia que tiene al país sumido en una crisis humanitaria, ha llevado a analistas y líderes de organizaciones sociales a identificar algunas de las razones que explican el regreso de las masacres a Colombia. **Según la Organización de Naciones Unidas -ONU-, en lo que va del año 2020, se han documentado 33 masacres y restan 7 más por documentarse; esto equivale a que se lleve registro de más de una masacre por semana.** (Le puede interesar:[5 niños afro fueron masacrados en el barrio Llano Verde en Cali](https://archivo.contagioradio.com/5-ninos-afro-fueron-masacrados-en-el-barrio-llano-verde-en-cali/); [Nueva masacre, en Nariño fueron asesinados 9 jóvenes](https://archivo.contagioradio.com/nueva-masacre-en-narino-fueron-asesinados-9-jovenes/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONUHumanRights/status/1295093058552377346","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONUHumanRights/status/1295093058552377346

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### La expansión del paramilitarismo una de las causas de las masacres

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para Soraya Gutiérrez, vicepresidenta del Colectivo de Abogados José Alvear Restrepo -[CAJAR](https://www.colectivodeabogados.org/)-, **una de las causas que explican la violencia y el regreso de las masacres es la expansión paramilitar,** la cual, según señala, tiene que ver con el fracaso de la desmovilización en el proceso de Justicia y Paz impulsado por el expresidente Uribe. Gutiérrez, refirió **informes de la Defensoría del Pueblo que alertan sobre la presencia de grupos paramilitares, en al menos, un 90% del territorio nacional.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> El problema es que el Gobierno desconoce y niega la existencia del paramilitarismo, tal como lo hace con la existencia del conflicto armado interno»
>
> <cite>Soraya Gutiérrez, vicepresidenta del Colectivo de Abogados José Alvear Restrepo</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

En el mismo sentido, Alberto Yepes, coordinador del  Observatorio de DD.HH y DIH de [Coeuropa](https://coeuropa.org.co/), explicó que **no existe ninguna voluntad de desmontar las estructuras paramilitares por parte del Gobierno**; y que, por el contrario lo que se evidencia es que **«*a lo largo y ancho del país se denuncia por parte de las organizaciones sociales y las comunidades la connivencia que existe entre agentes del Estado y  paramilitares*».**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Yepes, con el confinamiento quedó evidenciado que las razones que exponían las Fuerzas Armadas para no combatir a los grupos paramilitares, manifestando que estos se camuflaban y escudaban dentro de la población civil, no eran más que un «*pretexto*» para justificar su inacción. «*Si hemos visto algo en estos 5 meses de restricciones a la movilidad es que los paramilitares se han asentado en la mayor parte de las regiones donde antes tenía control las FARC, a las cuales el Estado todavía no ha llegado*», afirmó.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todo ello explica, según Yepes, el **surgimiento de grupos como el Clan del Golfo, las autodenominadas Autodefensas Gaitanistas de Colombia -AGC-, los Rastrojos y los Caparrapos que no son otra cosa que continuadores del paramilitarismo.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> A la población y a las organizaciones sociales se les obligó a quedar confinadas y los únicos que quedaron con libre movilidad fueron los grupos paramilitares»
>
> <cite>Alberto Yepes, coordinador del  Observatorio de DD.HH y DIH de [Coeuropa](https://coeuropa.org.co/)</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

De otra parte, Yepes aseguró que **el paramilitarismo no es combatido por parte del Gobierno porque existe un miedo de que los comandantes de estos grupos busquen someterse a la justicia y entreguen a aquellos que están detrás de dichas estructuras** como patrocinadores en el plano económico, político y empresarial. Esto, «*se evidencia también con todas las triquiñuelas que están haciendo para impedir el regreso de Mancuso al país*», apuntó. (Lea también: [Víctimas de Salvatore Mancuso piden su extradición a la Fiscalía](https://archivo.contagioradio.com/victimas-de-salvatore-mancuso-piden-su-extradicion-a-la-fiscalia/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Lo que hay es un pánico terrible en las élites de que los jefes paramilitares puedan contar la verdad, que ellos saben, sobre quiénes fueron los arquitectos, financiadores, organizadores y aquellos que se beneficiaron con todo el despojo que trajo el terror paramilitar»
>
> <cite>Alberto Yepes, coordinador del  Observatorio de DD.HH y DIH de [Coeuropa](https://coeuropa.org.co/)</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### La cooptación de los poderes públicos, los entes de control y las FF.MM.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Adicionalmente, Alberto Yepes, señaló que otra de las causas que explican la violencia y las masacres, es **la cooptación de todos los estamentos del Estado, por parte del Gobierno Nacional y su grupo político.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Como primera medida, para justificar esta posición, afirmó que las Fuerzas Militares no están sirviendo a los sectores populares y a la promoción de los derechos humanos, pero sí están siendo funcionales para las élites políticas que las han instrumentalizado para proteger sus propios intereses. (Lea también: [Hay políticas que propician la criminalidad en las Fuerzas Militares: CCEEU](https://archivo.contagioradio.com/hay-politicas-que-propician-la-criminalidad-en-las-fuerzas-militares-cceeu/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Lo peor que le pudo pasar a las Fuerzas Militares fue haber sido cooptadas por el uribismo \[...\] No podemos esperar que un Gobierno que está comprometido con la reactivación de la guerra y con impedir la implementación del Acuerdo de Paz, le vaya a pedir a las Fuerzas Militares que actúen en contra de sus propios propósitos»
>
> <cite>Alberto Yepes, coordinador del  Observatorio de DD.HH y DIH de [Coeuropa](https://coeuropa.org.co/)</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otro lado, señaló que en las últimas semanas, luego de que se conociera la decisión de la Corte Suprema  para imponer medida de aseguramiento a Álvaro Uribe, lo que se ha visto es **«*una estrategia por llevar el caos y la violencia a todo el país, para generar una sensación de incertidumbre y de inseguridad; que les haga viable la posibilidad de justificar una constituyente, una reforma a la justicia y replantear completamente el orden jurídico establecido*»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alberto Yepes, afirmó que **este actuar es igual al que utilizara en su momento el capo Pablo Escobar, quien llenó de violencia el país para buscar, de esa manera, que se cambiaran las normas para que le favorecieran.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> En aras de la impunidad de su jefe político (Álvaro Uribe), este Gobierno está acabando con el estado de derecho, con la normatividad y la división de poderes, cooptando todos los organismos judiciales y de control, como lo hicieron con la Defensoría del Pueblo, como lo quieren hacer ahora con la Procuraduría, y como lo quieren hacer al suprimir las Altas Cortes para crear una sola *supercorte* que ellos puedan manejar a su amaño para lograr la impunidad de todo este grupo que está hoy en el poder»
>
> <cite>Alberto Yepes, coordinador del  Observatorio de DD.HH y DIH de [Coeuropa](https://coeuropa.org.co/)</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### El despojo de tierras y la estigmatización del campesinado

<!-- /wp:heading -->

<!-- wp:paragraph -->

Aida Avella, senadora y presidenta de la Unión Patriótica -[UP](https://twitter.com/UP_Colombia)-, señaló que como integrante de la UP, fue junto con sus compañeros(as), víctima de un genocidio político, y afirmó que **los mismos métodos de exterminio se emplean en la actualidad contra los reclamantes de tierra que fueron desplazados y despojados de sus parcelas.** (Le puede interesar: [Denuncian que empresa bananera desarrolla campaña en contra de la Restitución de tierras](https://archivo.contagioradio.com/denuncian-que-empresa-bananera-desarrolla-campana-en-contra-de-la-restitucion-de-tierras/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Algunos defienden la propiedad privada para ellos, nosotros defendemos la propiedad privada para los 50 millones de habitantes de este país»  
>
> <cite>Aida Avella, senadora y presidenta de la Unión Patriótica -[UP](https://twitter.com/UP_Colombia)-</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Adicionalmente, la senadora afirmó que existe una gran estigmatización en contra de los cultivadores de coca, quienes siembran la planta como medio de mínima subsistencia, tildándolos como narcotraficantes; lo cual conlleva la persecución y violencia en contra del campesinado por parte de la Fuerza Pública, dejando de lado a las verdaderas mafias que se lucran con el narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El incumplimiento del acuerdo de paz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El acuerdo de paz alcanzado con las FARC contempla el desarrollo rural desde tres campos, la propiedad de la tierra, la sustitución de cultivos de uso ilícito y las condiciones necesarias para que las personas en el campo puedan transitar hacia nuevas y más eficientes maneras de producir alimentos u otros bienes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, en ninguno de esos tres elementos ha avanzado este gobierno que, como es evidente, ha priorizado la erradicación forzada de los cultivos de usos ilícito y ahora avanza hacia una formalización de la tierra que, en muchos casos está en manos de grandes empresas y terratenientes.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
