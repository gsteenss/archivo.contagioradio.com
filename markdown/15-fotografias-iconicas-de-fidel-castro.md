Title: 15 fotografías icónicas de Fidel Castro
Date: 2016-11-26 08:37
Category: Otra Mirada
Tags: Cuba, Fidel Castro
Slug: 15-fotografias-iconicas-de-fidel-castro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/fidel-castro-y-la-fotografia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo Fidel Castro 

###### 26 Nov 2016 

**Fidel Castro** estuvo acompañado de grandes fotógrafos durante largos periodos de su vida, ellos acompañaron sus días en las montañas como guerrillero, su vida como político, su vida como presidente.  Este es un recorrido por algunas de las **imágenes icónicas del Comandante en Jefe de la Revolución Cubana**, quien murió a sus 90 años en capital cubana.

[Fidel Castro Ruz ha pasado a la historia](https://archivo.contagioradio.com/fidel-castro-ruz-ha-pasado-la-histoira/)

\

 
