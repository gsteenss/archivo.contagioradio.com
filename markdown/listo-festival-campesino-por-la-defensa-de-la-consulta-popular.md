Title: Llega el Festival Campesino por la Defensa de la Consulta Popular en Cajamarca
Date: 2016-12-08 11:12
Category: Ambiente, Nacional
Slug: listo-festival-campesino-por-la-defensa-de-la-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### [8 Dic 2016] 

El próximo 10 de diciembre se realizará el lanzamiento de la campaña oficial del No, en la Consulta popular minera de Cajamarca en medio del “**Festival Campesino por la Defensa de la Consulta Popular”**, un primer espacio que busca hacer pedagogía sobre los daños de la minería a gran escala en el territorio y que contará con diversas actividades.

La iniciativa la está preparando el comité impulsor Ambiental de la Consulta Popular en Cajamarca en coordinación con otras organizaciones. De otro lado, la **propuesta también pretende ser un ejercicio de solidaridad con Cajamarca**, motivo por el cual se espera que al Festival lleguen personas de todo el país que expresen su apoyo frente los daños  ambientales que generan los proyectos minero energéticos en el país. Le puede interesar: ["Anglogold Ashanti entuteló consulta popular en Cajamarca"](https://archivo.contagioradio.com/anglogold-ashanti-entutelo-consulta-popular-en-cajamarca/)

Sin embargo, de acuerdo con Robinson Mejía, miembro del comité impulsor de la Consulta Popular, el Alcalde del municipio **sigue sin dar todas las garantías para que la campaña**, tanto de un lado como del otro, tenga las mismas posibilidades de ser comunicada, esto debido a que aún el comité no puede hacer perifoneo por la calles, mientras que la campaña de oposición ya tienen propagandas en las emisoras.

Entre las actividades que se realizaran el sábado están una jornada pedagógia por las calles de Cajamarca, desfiles con muestras culturales y agrícolas de lo que se produce en la despensa de este lugar, también se tendrá la participación de diferentes grupos de música que se presentarán durante la tarde. Le pude interesar: ["Hundido proyecto de ley para que el agua fuese un derecho fundamental"](https://archivo.contagioradio.com/hundido-proyecto-de-ley-para-que-el-agua-fuese-un-derecho-fundamental/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
