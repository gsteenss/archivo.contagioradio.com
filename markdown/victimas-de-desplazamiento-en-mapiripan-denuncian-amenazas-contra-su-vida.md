Title: Víctimas de masacre de Mapiripán denuncian amenazas de muerte
Date: 2017-09-12 13:49
Category: DDHH, Nacional
Tags: amenazas contra defensores, asomudem, defensores de derechos humanos, víctimas de desplazamiento forzado
Slug: victimas-de-desplazamiento-en-mapiripan-denuncian-amenazas-contra-su-vida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/foto1-e1505241740798.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

###### [12 Sept 2017] 

Dos integrantes de la Asociación de Mujeres Desplazadas del Meta (ASOMUDEM) y víctimas de la masacre de Mapiripán **denunciaron haber recibido amenazas en contra de su vida.** Además, indicaron que no es la primera vez que reciben intimidaciones que pretenden obligarlos a detener sus actividades en defensa de los derechos de las víctimas de desplazamiento forzado en los llanos orientales.

De acuerdo con Cecilia Lozano, integrante de ASOMUDEM, las amenazas contra la vida de ellos han sido constantes y **por lo general lo hacen a través de mensajes de texto enviados a los celulares de los defensores de derechos humanos**. También han denunciado que los han seguido personas en motos sin placas y en ocasiones se “han entrado a las casas a revolcar todo”.

La última amenaza la recibieron dos integrantes en un mensaje de texto. Les dijeron palabras insultantes y además de **amenazarlos de muerte a ellos, también lo hicieron contra sus familiares**. (Le puede interesar: ["Mapiripán a 20 años todavía recuerda y sobrevive"](https://archivo.contagioradio.com/mapiripan-a-20-anos-todavia-recuerda-y-sobrevive/))

Indicó que la presencia de grupos armados como los paramilitares que se disputan el territorio en el Meta, **dificulta el trabajo de las y los defensores** que buscan que las víctimas puedan volver a sus territorios.

Las amenazas han sido denunciadas en la Fiscalía pero **no ha habido ninguna reacción por parte de las autoridades**. Únicamente han recibido acompañamiento policial para realizar ciertos desplazamientos.

**ASOMUDEM trabaja por las víctimas de desplazamiento forzado y despojo de tierras**

Esta organización se ha caracterizado por realizar **un acompañamiento a las víctimas de desplazamiento forzado y despojo de tierras** en el Meta y especialmente a las víctimas de la masacre de Mapiripán. De manera conjunta, han tratado de obtener las garantías necesarias para que los mapiripeños que lo deseen puedan volver a su territorio.

Sin embrago, Cecilia Lozano indicó que **no ha sido un proceso fácil en la medida que no han sido escuchados como víctimas** desde hace 20 años. Desde ese entonces, no han recibido ningún tipo de reparación que este asociada a la restitución y posibilidad de retorno a sus hogares. (Le puede interesar: ["¿Qué ha pasado a 20 años de la masacre de Mapiripán?"](https://archivo.contagioradio.com/que-ha-pasado-a-20-anos-de-la-masacre-de-mapiripan/))

Dijo que la restitución de tierras no se ha realizado, entre otras cosas, **porque que no se ha puesto en marcha el plan retorno** para las víctimas impulsado por la Corte Interamericana de Derechos Humanos.

Igualmente, Lozano manifestó que **no ha habido una claridad en los procesos de restitución** para las personas de Mapiripán, debido a que “no está claro si Marpiripán somos las personas que vivíamos allí o es el espacio físico”, es decir que cuando se habla de Mapiripán como territorio de paz, parece dejarse por fuera a su población y sus víctimas.

Finalmente, indicó que **hay muchas víctimas del desplazamiento que están intentando y quieren volver a sus territorios** por lo que le han pedido al Gobierno que les devuelva sus tierras, que los haga partícipes de los procesos de reparación colectiva.

<iframe id="audio_20831180" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20831180_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
