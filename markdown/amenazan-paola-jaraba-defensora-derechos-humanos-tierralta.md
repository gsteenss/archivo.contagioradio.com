Title: Amenazan a Paola Jaraba, defensora de derechos humanos en Tierralta, Córdoba
Date: 2019-06-09 17:41
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: amenaza, cordoba, Lider social, Tierralta
Slug: amenazan-paola-jaraba-defensora-derechos-humanos-tierralta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Defensores-Asesinados-e1477508845527.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado sábado en horas de la noche un hombre amenazó a la líder Paola Andrea Jaraba, diciendole que tenía que abandonar Tierralta, Córdoba, en media hora. Jaraba es una reconocida lideresa, fundadora de la plataforma municipal de derechos humanos y democracia, miembro del consejo de paz, y actualmente está en proceso de restitución de tierras y cuenta con medidas de protección blandas por parte de la Unidad Nacional de Protección (UNP).

Según información de la Fundación Social Cordoberxia, Paola Jaraba fue amenazada de muerte cerca de su casa por un hombre que la tomo del brazo y le dijo que tenía media hora para desocupar el pueblo. La intimidación se suma a la sufrida por el también líder social Albeiro Begambre, y el aspirante a la alcaldía de Tierralta por la Unión Patriótica Miguel Tordecilla en un panfleto. (Le puede interesar: ["No se quedó en amenazas panfleto contra ocupantes de predios en Tierralta, Córdoba"](https://archivo.contagioradio.com/panfleto-contra-ocupantes-predios-tierralta-cordoba/))

Adicionalmente, en el Municipio se presentó un asesinato contra un excombatiente la semana pasada; razón por la que la Fundación llama la atención a las autoridades del municipio para que protejan la vida de los líderes sociales en el territorio, así como a investigar a los autores materiales e intelectuales de las amenazas. (Le puede interesar: ["Asesinan a excombatiente de FARC, Jorge Enrique Sepúlveda en Córdoba"](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-farc-jorge-enrique-sepulveda-en-cordoba/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
