Title: 65.000 madres comunitarias irían a paro indefinido
Date: 2017-01-27 15:45
Category: Movilización, Nacional
Tags: Madres Comunitarias ICBF, Odebrecht, Santos incumple, tercerización laboral
Slug: 65-000-mil-madres-comunitarias-irian-a-paro-indefinido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/madres_comunitarias.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ZonaCero] 

###### [27 Ene 2017] 

“¿Por qué sí hay dinero para quienes tanto daño han hecho al país, **pero no para las mujeres que hemos dado más de 30 años de nuestra vida en el cuidado y educación** de miles de niños y niñas?”, es la pregunta que hace Olinda García una de las madres comunitarias afectadas y presidenta del Sindicato Nacional de Trabajadoras al Cuidado de la Infancia y Adolescentes del Sistema Nacional de Bienestar Familiar –SINTRACIHOBI.

La pregunta surge a partir de la indignación que generó el último anuncio del presidente Juan Manuel Santos, quien anunció que **por “falta de recursos” no se realizará la contratación directa con el ICBF** a las más de 65.000 madres comunitarias presentes en Colombia.

García explica que antes, en abril del 2014, las madres recibieron una carta enviada por Juan Manuel Santos, en la que el presidente se comprometía a otorgarles una contratación directa, el reconocimiento del derecho a una pensión digna y salarios mínimos con todas las prestaciones de ley, **[“en 2016 para que finalizáramos el paro dijo que seríamos empleadas de planta y mire, era mentira”.](https://archivo.contagioradio.com/acuerdo-de-icbf-no-satisface-demandas-de-las-madres-comunitarias/)**

Cristina Plazas, actual directora del ICBF, anunció que aprobar la ley del senador del Polo, Alexander López, la cual propone incorporar a las madres comunitarias en la nómina del ICBF, “sería condenar a muerte el ICBF y pondría en riesgo la subsistencia del programa y el trabajo de estas mujeres”. Esa cartera asegura que **otorgar los contratos a las madres le costaría a la Nación más de “\$800.000 millones anuales, una cifra totalmente inviable”.**

Plazas también planteó a las madres comunitarias, que para ser contratadas de planta debían presentar un concurso, “es indignante que nos exijan eso, cómo es posible si llevamos más de 30 años prestando esta valiosa labor” aseveró Olinda García.

### Los intermediarios 

García denunció que la tercerización laboral de la que son víctimas a manos de las ONG que las contratan, incluye que **“no nos están pagando seguridad social, ni pensión, ni salud”**, añadió que el ICBF tampoco ha hecho un riguroso seguimiento ni control a las ONG contratantes y que han venido teniendo problemas en el tema de la compra de alimentos, “esas empresas hacen mercados insuficientes para la cantidad de niños que atendemos, **un cuarto de frijol no alcanza para alimentar más de 20 niños en una casa,** por ejemplo” puntualizó.

La líder comunitaria aseguró que el Estado “nunca ha sido claro en la creación de políticas públicas sobre el trabajo de nosotras” y que ha dejado en manos de terceros la estructuración del programa, “la situación está así porque la mayoría de las ONG que contratan, **son de funcionarios y exfuncionarios del ICBF y otras de políticos de turno”.**

Además, García reveló que muchas de las ONG destituidas por los robos en La Guajira “las contrataron nuevamente, [imagínese como quedamos nosotras, denunciando a los ladrones y les vuelven a dar contratos”.](https://archivo.contagioradio.com/5-000-madres-comunitarias-realizan-planton-en-las-sedes-del-icbf/)

### Acciones Legales 

Frente a la grave situación que atraviesan las madres comunitarias del país, en 2014 SINTRACIHOBI radicó ante Cámara y Senado los proyectos de ley 227 y 127 que propendían por sus derechos laborales, y fueron prontamente derogados por dichas instancias.

En 2016 interpusieron la acción de tutela T480, reclamando el reconocimiento y pago de los aportes a pensión en el Sistema General de Seguridad Social a cargo del ICBF, y también fue negada en 1ª y 2ª de la Corte, aunque dicha instancia reconoció que es uno de los sectores mayoritariamente explotado, **hasta la fecha no han sido efectivas las medidas de restitución de derechos a las madres comunitarias.**

Katherine Álvarez, una de las abogadas que acompaña el caso de las madres comunitarias, aseguró que es preocupante la situación, pues muchas de ellas tienen más de 70 años, **“ya cuatro de las madres han fallecido, y nunca obtuvieron su pensión”.**

Álvarez también enunció que la Corte tiene hasta abril de 2017 para aprobar este recurso interpuesto en 2016 y hacer efectivas las medidas exigidas, pero Olinda García advierte que “ha habido persecuciones de privados al sindicato para que se retire la acción de tutela, esperamos que este proceso no lo echen abajo como los demás”.

### ¿Qué van a hacer las madres comunitarias? 

García desmintió las aseveraciones de la directora del ICBF, quien dijo que el instituto se ha acogido a las disposiciones de la Corte y “el total de las madres comunitarias tienen contratos hasta 2018”, la presidenta del sindicato señaló que **[“sólo unas pocas tienen contratos hasta el 2018, a las demás les quedan 3 o 4 meses de contrato”.](https://archivo.contagioradio.com/7-mil-madres-comunitarias-en-paro-indefinido/)**

Por último, la presidenta del sindicato expresó que las más de 65.500 madres comunitarias presentes en toda Colombia **“hemos decidido que nos vamos a paro nacional indefinido”**, advirtió que están coordinando con los padres de los más de 8.000 niños y niñas que se verán afectadas por las disposiciones de Juan Manuel Santos, para que **“se unan y nos apoyen a hacer frente a esta grave situación”.**

<iframe id="audio_16690654" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16690654_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
