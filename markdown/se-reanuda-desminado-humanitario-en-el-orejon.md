Title: Se reanuda desminado Humanitario en el Orejón
Date: 2015-08-31 15:38
Category: Nacional, Paz
Tags: desminado humanitario, El Orejón, FARC, Gobierno, proceso de paz, Rios Vivos
Slug: se-reanuda-desminado-humanitario-en-el-orejon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/minas-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:imperiocrucial.com 

<iframe src="http://www.ivoox.com/player_ek_7719157_2_1.html?data=mJyem5aZe46ZmKiak5eJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8af08rO0NrIpYzYxtjay9PFqNCfqdraw9PNuMLmytSYx9OPqc2fsNfSzIqnd4a2lNOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Fabio Muñoz habitante del Orejón] 

###### [Ago 31 2015] 

Las  FARC anunciaron en un comunicado que se reanuda el proceso de descontaminación y desminado y manifestaron su “satisfacción” por dicha continuidad puesto que se constituye en un ejemplo más de que el proceso de conversaciones de paz está encaminado hacia el final de conflicto. Sin embargo insistieron en la necesidad de que el gobierno asuma una actitud coherente con el desmonte del paramilitarismo.

Fabio Muñoz habitante del Orejón en el municipio de Briceño (Antioquía) y miembro de Ríos Vivos, comentó sobre lo que representa que se haya reanudado el desminado en esta región y las medidas que tomó el gobierno sobre la presencia paramilitar.

Sin embargo, los habitantes del Orejón tienen el derecho de mostrarse escépticos frente a la reanudación de estos nuevos trabajos de desminado, ya que el primer lugar en donde se inician estos trabajos es en terrenos que pertenecen a EPM y a la gobernación de Antioquia.

"**Nos preguntamos si en verdad es más para la comunidad o beneficio del proyecto**" además de los incumplimientos que han tenido en cuanto a indemnizaciones a compensaciones con la región, dijo Fabio.

Otra de las preocupaciones de los habitantes es que la tercera fase de este proceso de desminado había sido suspendida por denuncias de [presencia de grupos paramilitares,](https://archivo.contagioradio.com/acciones-paramilitares-obligan-a-suspender-desminado-humanitario-en-el-orejon/) "gaitanistas", en la zona y frente a este tema el único gesto visible ha sido la instalación de un retén militar, del resto no se sabe nada, afirma Muñoz.

Medida que resulta insuficiente y que debería ser asistida por “**Un acompañamiento mas de organizaciones de derechos humanos, nacionales e internacionales, acompañamiento para que las comunidades se sientan más protegidas**” dice Muñoz.

Esta retoma del desminado que se adelanta en El Orejón, sirve como una muestra de lo que representan para la paz estas acciones al interior de las comunidades y Fabio Muñoz dice que este proceso debe adelantarse en todo el país, en todas las zonas que estén con minas, pero además deben estar acompañadas por inversión social “esto generaría una verdadera paz”.
