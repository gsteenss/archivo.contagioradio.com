Title: Campesinos del Catatumbo rechazan erradicación forzada por parte del ejército
Date: 2015-10-02 13:02
Category: DDHH, Nacional
Tags: ASCAMCAT, Catatumbo, César Jeréz, cultivos ilícitos en Colombia, Ejército de Colombia, erradicadores manuales de coca, Juan Manuel Santos, zonas de reserva campesina
Slug: campesinos-del-catatumbo-denuncian-nuevos-incumplimientos-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/4846207w-640x640x80.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.que.es]

###### [2 Oct 2015]

Las comunidades campesinas del Catatumbo, denuncian que militares de la Brigada 30 del **Ejército llegaron hace 10 días a varias fincas de las familias a erradicar manualmente los cultivos de coca**, incumpliendo los acuerdos pactados entre los campesinos de la zona y el gobierno.

César Jeréz, vocero de la Asociación de Zonas de Reserva Campesinas, ANZORC, asegura que con estas acciones se vulnera los derechos de los campesinos y campesinas quienes habían presentado hace una semana un nuevo programa de sustitución gradual de cultivos de uso ilícito al gobierno, sin embargo, **no se avanzó en la firma del acuerdo y nuevamente el gobierno le incumplió a las familias.**

Por esta situación desde la Asociación Campesina del Catatumbo, las comunidades **adelantan nuevas jornadas de movilización** para llamar la atención del Presidente Juan Manuel Santos.

Este año la erradicación manual ha aumentado con respecto al año pasado un 22%. Actualmente, **existen 30 grupos de erradicadores manuales en el país, pero se espera que antes de diciembre este número aumente a 60** como lo anunció el gobierno.
