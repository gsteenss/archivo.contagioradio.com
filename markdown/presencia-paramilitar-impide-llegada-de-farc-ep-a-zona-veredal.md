Title: Presencia Paramilitar impide llegada de FARC-EP a zona veredal
Date: 2017-02-13 12:34
Category: DDHH, Nacional
Tags: Catatumbo, paramilitares
Slug: presencia-paramilitar-impide-llegada-de-farc-ep-a-zona-veredal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Notimundo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notimundo] 

###### [13 Feb 2017] 

Caravana de las FARC-EP continúa sin poder llegar a su punto de concentración en Caño Indio, Norte de Santander, por la **retención que en este momento hacen campesinos que denuncian la presencia de grupos armado**s en la zona de La Gabarra, que se identificaron como “Águilas Negras”. Sin embargo  el Ministro de Defensa y las Fuerzas Militares han negado la existencia de estos grupos en la región.

Para Olga Quintero, vocera de ASCAMCAT lo más preocupante es que tanto el Ministerio del  Interior, en cabeza de Juan Fernando Cristo como el Comandante de las Fuerzas Tarea Conjunto Vulcano digan que **“no es cierto que estos hechos estén ocurriendo y preocupa que el gobierno no este asumiendo con altura esta acción que no es aislada y que ha sido denunciada en otras regiones”**.

La Organización de Naciones Unidas expresó que ante la preocupación ocasionada por la no llegada de la caravana de las FARC-EP hacía su zona veredal, enviará una comisión **“para ayudar a la continuación de este importante movimiento”**. Le puede interesar: ["Paramilitares amenazan con tomar el control en zona de La Gabarra, Norte de Santander"](https://archivo.contagioradio.com/paramilitares-amenazan-de-tomar-el-control-en-zona-de-la-gabarra-norte-de-santander/)

Producto de la aparición de estos grupos armados, entre el viernes 10 y el sábado 11 de febrero, 200 personas  se han desplazado hacia el municipio de Jesús María Semprúm, en Venezuela, mientras que dos comisiones conformadas por 500 campesinos y 30 juntas de acción comunal, recogieron las denuncias por parte de los habitantes  sobre las amenazas hechas por estos grupos que señalaban que **“habían llegado a la región para ocupar el territorio que dejaban las FARC-EP y que el que no quisiera trabajar con ellos se podía ir de la región”**.

En un comunicado de prensa las comunidades de la vereda Neiva, en La Gabarra, expresaron que como medida de seguridad y para salvaguardar la vida de los campesinos se creara un  refugio humanitario, acompañarán de manera pacífica la caravana de las FARC-EP hasta el punto de Caño Indio y **convocan la presencia de la Comisión de Seguimiento e Impulso a la Verificación e Implementación del Acuerdo y del Consejo Nacional de Reincorporación. **

A su vez, indígenas y campesinos exigen el cumplimiento de los puntos**  6,7 y 9 del acuerdo de paz de La Habana y la implementación de la Unidad para el Desmonte de las Estructuras Paramilitares**, a su vez piden que no se repita otro capítulo como el de la masacre de La Gabarra en Norte de Santander. Le puede interesar: ["21 familias wounnan son desplazadas por paramilitares"](https://archivo.contagioradio.com/wounaan-desplazados-paramilitares/)

###### Reciba toda la información de Contagio Radio en [[su correo]
