Title: Comunidad responsabiliza al Ejército del asesinato y desaparición del líder Didian Agudelo
Date: 2020-02-29 11:48
Author: CtgAdm
Category: DDHH, Nacional
Tags: Antioquia, asesinato de líderes sociales, Ejecuciones Extrajudiciales
Slug: comunidad-responsabiliza-al-ejercito-del-asesinato-y-desaparicion-del-lider-didian-agudelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Didian-líder.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Didian Agudelo- Ascat-Na

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones defensoras de DD.HH. informaron sobre el hallazgo del cuerpo sin vida del líder comunal **Didian Erley Agudelo Agudelo en Campamento, Antioquia, después de haber sido reportado como desaparecido desde el pasado 25 de febrero.** Cercanos y familiares del líder aseguran que miembros del Ejército lo habrían retenido contra su voluntad y serían responsables de su asesinato.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La última vez que el líder social Didian Agudelo, de 38 años de edad fue visto con vida fue el pasado martes 25 de febrero cuando salió a trabajar con los animales de pastoreo en su finca ubicada en la vereda La Frisolera, del municipio de Campamento, zona donde existe una fuerte presencia del Ejército. [(Lea también: Es necesaria una comisión independiente que diga qué pasa al interior del Ejército)](https://archivo.contagioradio.com/es-necesaria-una-comision-independiente-que-diga-que-pasa-al-interior-del-ejercito/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Según las denuncias hechas por la comunidad de Campamento, cuando Didian desapareció, integrantes del Ejército se encontraban en la zona**, razón por la desde horas de la mañana del 26 de febrero, campesinos rodearon las afueras del batallón de La Frisolera exigiendo respuestas al Ejército por la desaparición del líder comunitario.  
  
Mientras las versiones de los soldados son contradictorias en cuanto a la ubicación de las tropas al momento de la desaparición - según denuncia la comunidad - [Juan Carlos Ramírez,](https://twitter.com/GralJuanRamirez/status/1232818942340685825) comandante de la Séptima División del Ejército aseguró que activaron los protocolos de búsqueda para dar con su ubicación. A su vez, la Personería del municipio inició una campaña para hallar a Didian. [(Le puede interesar: El silencio cómplice de las Fuerzas Militares en caso Dimar Torres)](https://archivo.contagioradio.com/el-silencio-complice-de-las-fuerzas-militares-en-caso-dimar-torres/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Después de varios días sin conocer su paradero, el cuerpo de Didian fue hallado "ahorcado con su propia camisa" en una zona donde ya se habían realizado labores de búsqueda, según información suministrada por su hijastro. Habitantes afirman que una semana antes de la desaparición y posterior asesinato de Didian, otros dos agricultores habrían sido retenidos de manera ilegal por miembros de **esta división del Ejército que opera en Antioquia, Córdoba, Choco y Sucre, en particular en la región del Bajo Cauca** donde existe presencia y disputas por el territorio entre grupos como Los Caparrapos y las autodenominadas AGC o Clan del Golfo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Didian además de ser un líder campesino fue exconcejal del municipio de Campamento, además fue hijo de quien es en la actualidad el presidente de la Junta de Acción Comunal de la vereda.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Noticia en desarrollo...

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
