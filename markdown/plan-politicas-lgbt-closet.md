Title: Denuncian que Plan Nacional de Desarrollo dejaría políticas LGBT+ 'en el closet'
Date: 2019-03-21 18:19
Category: Entrevistas, Política
Tags: Activistas LGBT, Closet, LGBTI, Plan Naci
Slug: plan-politicas-lgbt-closet
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-21-a-las-6.17.30-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MauroToroO] 

###### [21 Mar 2019] 

**El Plan Nacional de Desarrollo (PND) marca las bases sobre cómo se destinarán los recursos de inversión durante los próximos 4 años**, por esa razón resulta de vital importancia para el Gobierno su aprobación. Sin embargo, diferentes voces que han señalado inconveniencias económicas y políticas del Plan presentado por Duque han sumado una nueva: la exclusión de personas Lesbianas, Gay, Bisexuales y Trans (LGBT+).

<iframe src="https://co.ivoox.com/es/player_ek_33595396_2_1.html?data=lJiim5qXfZehhpywj5WaaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncaTV1cbZy9PFb7Dm1YqwlYqliNuZk6iYpdTSq9PZ1M7g1saPhc3dwtPnw5C6qdPYxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El PND ha causado controversia en varios de sus artículos al punto que las bancadas de oposición han presentado dos ponencias negativas. **Catalina Ortíz, representante a la cámara** y una de las congresistas ponentes afirma que el Partido Verde adelantó esta acción para enfrentar un plan que es nocivo y afecta a los colombianos. (Le puede interesar: ["El Plan Nacional de Desarrollo, un respaldo al sector minero-energético"](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/))

> La bancada del [@PartidoVerdeCoL](https://twitter.com/PartidoVerdeCoL?ref_src=twsrc%5Etfw) radicamos [\#PonenciaNegativa](https://twitter.com/hashtag/PonenciaNegativa?src=hash&ref_src=twsrc%5Etfw) al [\#PND](https://twitter.com/hashtag/PND?src=hash&ref_src=twsrc%5Etfw) porque no incluye acciones concretas, medibles y un presupuesto adecuado para incidir en el desarrollo de Colombia como descentralización, construcción de paz, emprendimiento, equidad de la mujer y medio ambiente [pic.twitter.com/boOyTU9tgw](https://t.co/boOyTU9tgw)
>
> — Catalina Ortiz 🌻 (@cataortizcamara) [20 de marzo de 2019](https://twitter.com/cataortizcamara/status/1108462127692632065?ref_src=twsrc%5Etfw)

### **Personas con identidades de género diversas serían excluidas del PND**

<iframe src="https://co.ivoox.com/es/player_ek_33595377_2_1.html?data=lJiim5qXe5ihhpywj5WaaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5ynca7V08jSzsaPl4a3lIquk9PHrMbuhpewjcnNtsbX1dTfw5Cns83jzsfWw5DIrdfZ09jOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
En la ponencia, Ortíz destaca la denuncia a un artículo en el que se excluye a las poblaciones LGBT+, que asegura resulta nocivo para la garantía de sus derechos. A propósito del tema, **Marcela Sánchez, directora de la organización Colombia Diversa,** explicó que en el Plan existían algunas líneas de políticas públicas relacionadas con temas concretos para estas comunidades.

</p>
Algunas de las problemáticas abordadas incluían dificultades en el acceso al trabajo de personas trans por razones de discriminación, protocolos de atención en salud y frente al acoso escolar de jóvenes que se identifican como LGBT+, **"nada distinto a los compromisos del Estado asumidos en instancias internacionales"** con este grupo poblacional, señaló Sánchez.

Sin embargo, los representantes del Partido Colombia Justa y Libre, organización que acompaña las decisiones del Gobierno, hicieron una proposición para borrar unas líneas con las que no estaban de acuerdo, al tiempo que incluyeron sus demandas junto a la de personas con orientación sexual o identidad de género diversa de forma "anti técnica". (Le puede interesar: ["¿Plan Nacional de Desarrollo de Duque se olvidó de la paz?"](https://archivo.contagioradio.com/plan-de-desarrollo-olvido-paz/))

> Es inconcebible que [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) permita que en el PND se discrimine y se invisibilice a la comunidad LGBTI, y se desconozcan las violaciones a sus derechos [\#LGBTEnelPND](https://twitter.com/hashtag/LGBTEnelPND?src=hash&ref_src=twsrc%5Etfw) En este link pueden ver las modificaciones logradas por [@JhonMiltonR](https://twitter.com/JhonMiltonR?ref_src=twsrc%5Etfw) de [@ColJustaLibres](https://twitter.com/ColJustaLibres?ref_src=twsrc%5Etfw) <https://t.co/KgtGhHbcqV> — Mauricio Toro (@MauroToroO) [20 de marzo de 2019](https://twitter.com/MauroToroO/status/1108414402670280704?ref_src=twsrc%5Etfw)

### **Para sacar las personas LGBT+ usaron la misma técnica en el plebiscito**

De acuerdo a la Directora de Colombia Diversa, en esta ocasión los sectores de iglesias cristianas acudieron a la misma técnica que emplearon en el Acuerdo de Paz para excluir a la población LGBT+. Dicha acción tendría dos vías: Borrar lo que no les gusta o con lo que no están de acuerdo; e **incluirse como grupo discriminado, con la particularidad de no hacerlo como personas, sino como instituciones religiosas que son víctimas de este flagelo**.

"Eso es anti técnico, porque la discriminación de personas por motivos religiosos sí debería tener su propio capítulo y línea en el PND" pero están equiparando la segregación que sufren personas con instituciones, aclaró Sánchez. Por estas razones, la Activista espera que el Congreso no retroceda en los logros y derechos alcanzados, negando la inclusión de personas LGBT+ en el Plan.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
