Title: Existen 17 casos de amenazas grupales y directas contra trabajadores de Parques Nacionales
Date: 2019-01-18 16:38
Author: AdminContagio
Category: DDHH, Nacional
Tags: parques nacionales, Sierra Nevada de Santa Marta
Slug: existen-17-casos-de-amenazas-grupales-y-directas-contra-trabajadores-de-parques-nacionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/DxJM9u4WsAE-Sxa-e1547840372558-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @ParquesNacionales 

###### 16 Ene 2019

[Grupos armados vinculados a la deforestación y ocupación del terreno han amenazado de muerte a funcionarios de Parques Nacionales, una tendencia que se evidenció con el asesinato de **Wilton Fauder Orrego**, miembro del parque de la Sierra Nevada el pasado 14 de enero. El Sindicato de Trabajadores del Sistema Nacional Ambiental (Sintrambiente)  expresó su rechazo y exigió, se garantice la protección del ejercicio fundamental de los defensores de las zonas naturales en Colombia.]

Ante las amenazas provenientes de estos grupos, Wilmen Vásquez Molina, presidente de Sintrambiente denuncia que **Parques Nacionales, al tener conocimiento de la amenaza decide trasladar al funcionario a otro parque para que este siga cumpliendo con su trabajo**, pero sin embargo no ofrece una solución real al problema pues las amenazas continuarán en tanto los actores armados sigan en la zona. [(Lea también: Asesinan a funcionario de Parques Nacionales en la Sierra Nevada)](https://archivo.contagioradio.com/asesinan-a-funcionario-de-parques-nacionales-en-la-sierra-nevada/)

Vasquez Molina afirma que el trabajar como guardabosques implica hacer las veces de inspector  y protector de los recursos naturales, acción que va en contravía de los actores armados que están en el sector como como las autodenominadas **Autodefensas Gaitanistas de Colombia y el ELN. **

“Estamos en un país donde no se garantiza la vida de nadie” expresa  Vásquez Molina quien señaló que se han entablado comunicaciones con la ONU, la OEA, la defensoría del pueblo y  la UNP para hacer frente a estas acciones violentas las cuales ya habían sido denunciadas hace un año ante diferentes organismos de control.

Según Ana María Rocha, responsable de comunicaciones externas de Parques Nacionales, en la actualidad existen 17 amenazas en total entre grupales y directas contra contratistas en 11 diferentes parques entre ellos los de **Macarena, Tinigua, Picachos, Catatumbo, Paramillo, La Paya, Farallones, Sierra Nevada de Santa Marta, Nukak, Sanquianga y Orquídeas.**

[Por su parte, el líder sindical advierte que  la prioridad en este momento son **los parques de la Sierra Nevada y del Tayrona** donde la situación de orden público se ha recrudecido, lo que debería ser un llamado de alerta para que el Gobierno tome cartas en el asunto.]

###### Reciba toda la información de Contagio Radio en [[su correo]
