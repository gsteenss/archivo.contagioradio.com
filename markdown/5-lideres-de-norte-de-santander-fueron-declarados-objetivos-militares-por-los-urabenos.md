Title: Paramilitares amenazan a líderes de Catatumbo y Santander
Date: 2017-03-29 16:36
Category: DDHH, Nacional
Tags: ASCAMCAT, Catatumbo, marcha patriotica, Norte de Santander
Slug: 5-lideres-de-norte-de-santander-fueron-declarados-objetivos-militares-por-los-urabenos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/gaitanistas_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [29 Mar. 2017] 

Las intimidaciones a nombre de “Los Urabeños” continúan, esta vez se conoció del **caso de una amenaza que fue recibida a través de un mensaje al celular de Juan Carlos Quintero,** integrante de Marcha Patriótica, de la Asociación Nacional de Zonas de Reserva Campesina (ANZORC) y de la Asociación Campesina del Catatumbo **(ASCAMCAT)**, quien junto con varios líderes sociales más, son declarados objetivo militar.

**La denuncia fue dada a conocer por la Juventud Rebelde Colombia** a través de un comunicado, en el que aseguran que **dicha amenaza incluye otros nombres** como el de Guillermo Quintero, secretario político de la Juventud Rebelde Norte de Santander, Junior Maldonado secretario de relaciones internacionales de dicha organización, Olmer Pérez y Jonny Abril de ASCAMCAT, así como Judith Maldonado integrante de Voces de Paz

**En el mensaje se asegura que estas personas son “guerrilleros comunistas que pretenden a través del proceso de paz afianzarse en Norte de Santander** y posesionarse como fuerza en Cúcuta y la frontera” además los acusan de ser integrantes de las FARC. Le puede interesar: [Presencia Paramilitar impide llegada de FARC-EP a zona veredal](https://archivo.contagioradio.com/presencia-paramilitar-impide-llegada-de-farc-ep-a-zona-veredal/)

Para **Juan Carlos Quintero, una de las personas que fue amenazada**, **este hecho se suma a varios que han ocurrido en los últimos meses en Catatumbo** “uno tiene que ver con la denuncia misma que hemos hecho de la presencia de estos grupos y la otra tiene que ver con toda una estrategia de ataques mediáticos, de desprestigio y señalamientos de parte de algunos medios”. Le puede interesar: [ASCAMCAT denuncia aumento de amenazas y hostigamientos a sus dirigentes](https://archivo.contagioradio.com/ascamcat-denuncia-aumento-de-amenazas-y-hostigamientos-a-sus-dirigentes/)

Contrario a la negativa del Gobierno nacional frente a la existencia de los grupos paramilitares, **la región del Catatumbo ha sido una de las más afectadas por estos grupos armados desde finales del 2000** y en la actualidad vive las consecuencias de los rezagos.

“Mientras no haya un reconocimiento de que existe este fenómeno como una política de Estado y no se cumpla lo acordado en La Habana, va a ser muy complicado que se pueda hacer un real combate a estas estructuras” recalca Quintero. Le puede interesar: [Sí hay paramilitares en el Catatumbo: Comisión de verificación](https://archivo.contagioradio.com/comision-de-verificacion-en-catatumbo-verifico-denuncias-sobre-presencia-paramilitar/)

Según lo cuenta Quintero, **la respuesta estatal hasta el momento ante la difícil realidad que vive el departamento de Norte de Santander** y en especial el Catatumbo **ha sido militarizar la zona**, sin embargo, para el dirigente, esto no redunda en una reducción de la violencia o del riesgo para la población.

**“Hay una modalidad de limpieza social por ejemplo en el municipio de Tibú** y recordemos que en Tibú está ubicada la Zona Veredal y no hay que olvidar que se acordó un cese al fuego y de hostilidades que incluyen a la población civil (…) y en lo que va de corrido del año **solo el fin de semana pasado hubo una masacre de 6 personas y las autoridades no dan razón de los responsables”**

Ante estas situaciones, las organizaciones que son víctimas de amenazas han solicitado que el Gobierno **implemente ya el punto sobre desmantelamiento de estructuras paramilitares**, y que se inicie una ruta para comenzar a implementar el pacto político de la no utilización de las armas en la política. Le puede interesar: Le puede interesar: [Paramilitares amenazan de tomar el control en zona de La Gabarra, Norte de Santander](https://archivo.contagioradio.com/paramilitares-amenazan-de-tomar-el-control-en-zona-de-la-gabarra-norte-de-santander/)

Además, han solicitado sean **activados de manera ágil los mecanismos de protección acordados para las comunidades**, las organizaciones y los defensores de DDHH.

“A esta situación **hay que buscar responsables que son los funcionarios públicos que han hecho señalamientos irresponsables en contra de ASCAMCAT**. Los medios de comunicación que no están informando para la paz y están haciendo señalamientos muy graves” concluyó Quintero.

### **Comunicado Oficial de Marcha Patriótica - Norte de Santander** 

![WhatsApp Image 2017-03-29 at 4.31.11 PM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/WhatsApp-Image-2017-03-29-at-4.31.11-PM.jpeg){.alignnone .size-full .wp-image-38540 width="622" height="1024"}

<iframe id="audio_17850046" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17850046_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

###### 

<div class="ssba ssba-wrap">

</div>
