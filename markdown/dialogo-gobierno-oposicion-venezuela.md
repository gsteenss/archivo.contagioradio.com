Title: 4 mesas de trabajo acuerdan Gobierno y Oposición en Venezuela
Date: 2016-10-31 14:07
Category: El mundo, Política
Tags: diálogo, Maduro, oposición, paz, Venezuela
Slug: dialogo-gobierno-oposicion-venezuela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/dialogo_ven_5.jpg_1149960893.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: EFE 

###### 31 oct 2016

Con la creación de 4 mesas de trabajo, culminó en Caracas, Venezuela, la primera reunión formal entre el gobierno de **Nicolás Maduro** **y la Mesa de la Unidad Democrática** realizada la noche del 30 de octubre, con presencia de una misión especial de la Unión de Naciones Suramericanas (Unasur) y el representante del Vaticano.

**Soberanía, DDHH y reparación de las víctimas, cronograma electoral y salida a la crisis económica**, son los temas que asumirán las mesas propuestas, coordinadas por mediadores de la Unasur y de la Iglesia católica, en las que además **participará un representante del gobierno y de los partidos de oposición en Venezuela** en cada una.

La negociación será mediada por el expresidente panameño **Manuel Torrijos**, quien estará al frente de la mesa de **generación de Confianza y cronograma electoral**, mientras que el expresidente del gobierno español **José Luis Rodríguez Zapatero** coordinará en diálogo en la mesa “**Paz, respeto al Estado de derecho y a la soberanía nacional**”.

La mesa “**Verdad, justicia, derechos humanos, reparación de víctimas y reconciliación**” quedará bajo la coordinación del representante de la Santa Sede en Venezuela **Claudio María Celli** mientras que para la cuarta mesa “**Económico social**.” el coordinador será el ex presidente de República Dominicana **Leonel Fernández**.

Monseñor Celli, recordó que lo0s venezolanos esperan un diálogo sincero e hizo un llamado al gobierno y la oposición “**les pido en nombre de Francisco, que lleguen a concordar en algunos gestos concretos. El país espera señales auténticas para comprender que el diálogo es una realidad y algo muy serio**”.

Por su parte el presidente Nicolás Maduro aseguró que e**l diálogo es una oportunidad para desarmar el odio y la intolerancia y “abrir el camino al amor”**, agregando que esta es "el arma más poderosa para un proceso de paz (...) **Hay que ser optimista cuando se trata de la paz, el diálogo es la alternativa**”.

Al final del encuentro fue emitido un comunicado en el que las partes se comprometieron a "**disminuir el tono de agresividad del lenguaje utilizado en el debate político**", acordando un compromiso conjunto para mantener la paz y el entendimiento, así como el establecimiento de un plan de trabajo a futuro. El secretario general de la Unasur, **Ernesto Samper**, anunció que el 11 de noviembre tendrá lugar la próxima reunión en un sitio a definir por las partes.  Le puede interesar: [Oposición venezolana violaría la constitución si destituye a Maduro](https://archivo.contagioradio.com/oposicion-de-venezuela-violaria-constitucion-si-destituye-a-maduro/).

### Conversaciones en Venezuela. 

El encuentro convocó por parte del gobierno a la canciller Delcy Rodríguez; el alcalde de Caracas Jorge Rodríguez; el exembajador ante la OEA Roy Chaderton; y el diputado por el Partido Socialista Unido de Venezuela (PSUV), Elías Jaua; mientras que por la oposición asistió el secretario general de la Mesa de la Unidad Democrática (MUD), Jesús Torrealba; el representante de Avanzada Progresista (AP), Henri Falcón; el alcalde de Primero Justicia (PJ), Carlos Ocariz; y el vocero de Un Nuevo Tiempo (UNT), Timoteo Zambrano, expulsado de la alianza opositora en días pasados.
