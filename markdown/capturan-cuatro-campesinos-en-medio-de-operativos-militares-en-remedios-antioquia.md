Title: Capturan cuatro campesinos en medio de operativos militares en Remedios, Antioquia
Date: 2019-02-11 14:21
Author: AdminContagio
Category: Comunidad, Nacional
Tags: Militarización de territorios, Nordeste Antioqueño
Slug: capturan-cuatro-campesinos-en-medio-de-operativos-militares-en-remedios-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Militaress-Boyaca-e1524691374383-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Archivo 

###### 11 Feb 2019 

Comunidades campesinas de Remedios, Antioquia denuncian que el pasado 10 de febrero, el Ejército realizó dos operativos en las veredas Altos de Manilas, Lejanías y Panamá 9, capturando a cuatro campesinos mineros. Según los habitantes del sector las fuerzas armadas se movilizaron vía aérea y terrestre a través del territorio; dicho incremento en la fuerza pública se ha visto reflejado desde que se rompieron los diálogos entre el Gobierno y el ELN.

Según Carlos Morales, representante legal de la Corporación Acción Humanitaria por la Convivencia y la Paz del Nordeste Antioqueño (CAHUCOPANA), durante el operativo en conjunto con la Fiscalía se realizaron dos allanamientos y detuvieron a** Meily Bernal Martínez Albeiro de Jesús Zea Anunciación Delgado Yurlis Astrid Villa,** **campesinos afiliados a la Junta de Acción Comunal de la vereda Panamá 9, padres y madres de familias, mineros tradicionales y personas reconocidas y activas dentro del proceso comunitario.**

Posteriormente en horas de la noche, según relata el representante legal, el Ejército realizó un nuevo operativo recorriendo el caserío de Mina Nueva, esta vez llevando consigo una persona encapuchada de quien se desconoce su identidad, una situación que generó “terror y pánico” en los más de 300 habitantes del caserío y que como señala Morales, “en cierta forma pone en riesgo a la población civil”.

COHUCAPANA intentó entablar comunicación con el Ejército para conocer el destino de las personas capturadas quienes fueron sacadas en helicóptero de la zona, sin embargo no han tenido mayor acceso a información, **“solo sabemos que los tenían en Segovia, pero que fueron llevados a Barrancabermeja, hasta donde sabemos lo que han dicho es que esta es una orden de captura que viene desde Bogotá”** explica Morales quien ve con preocupación cómo se  vuelve a involucrar a la población civil en el contexto del conflicto.

Aunque la zona continúa militarizada y la situación de la comunidad es en general de temor, Morales asegura que los habitantes también están muy pendientes de lo que pueda acontecer, y que permanecen organizados para comenzar a generar protocolos de protección que defiendan a las comunidades mineras y campesinas.

<iframe id="audio_32448383" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32448383_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
