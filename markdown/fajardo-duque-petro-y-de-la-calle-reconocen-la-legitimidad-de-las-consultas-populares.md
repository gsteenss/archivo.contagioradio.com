Title: Cuatro candidatos presidenciales reconocen la legitimidad de las consultas populares
Date: 2018-01-23 18:26
Category: Ambiente, Nacional
Tags: consultas populares, elecciones 2018, Germán Vargas Lleras, Gustavo Petro, Humberto de la Calle, Iván Duque, Marta Lucía Ramírez, Sergio Fajardo
Slug: fajardo-duque-petro-y-de-la-calle-reconocen-la-legitimidad-de-las-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/foro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Foro Nacional Ambiental] 

###### [23 Ene 2017] 

De acuerdo con el Ministerio de Minas **al 15 de diciembre de 2017 se tramitaron 70 consultas populares**. De ellas, 9 fueron votadas y en todas ganó el NO a las actividades extractivistas, mientras que 7 fueron suspendidas a falta de recursos como lo argumenta el Ministerio de Hacienda, sin embargo hay 54 de esos mecanismos de participación que no se han definido en las urnas. De manera que, muy seguramente, este será uno de los temas claves que tendrá que enfrentar un nuevo mandatario.

Las circunstancias al rededor de las consultas populares fue el primer tema que se consideró entre las preguntas del foro realizado en la Universidad de los Andes este martes, sobre las propuestas en materia ambiental de  Humberto de la Calle, Iván Duque, Sergio Fajardo, Gustavo Petro, **Marta Lucía Ramírez y Germán Vargas Lleras. Los dos últimos no asistieron.**

La gran conclusión de esas respuestas, sin importar la orilla política desde la que se hablara, fue la legitimación de dicho mecanismo de participación popular, como resultado de la falta de capacidad de escucha de los gobiernos frente a las situaciones y exigencias de las comunidades que apelan a la protección del ambiente, los animales y las fuentes hídricas.

### **Sergio Fajardo** 

Fajardo, se centró en **la concertación con las comunidades, cuyo valor lo señala como la fortaleza de su propuesta**. Además asegura que es necesario hacer un seguimiento a lo que se promete y acuerda con la comunidad.

*"No tengo la menor duda que se necesitan estas consultas, son una expresión legítima, constitucional de las comunidades que tienen que ser escuchar y que se tienen que atender esas inquietudes (...) Y eso es una obligación del Estado: entender esas circunstancia, asumir el tema ambiental, reconocer las consultas y además incorporar en esa capacidad de discusión todo lo que tiene que ver con los elementos científicos, económicos, sociales y políticos, con el desarrollo de cada una de las comunidades. Eso es fundamental (...) el gobierno nacional tiene muy poca capacidad de articular y trabajar con las comunidades (...) muchas veces se han pensado las políticas públicas desde el centro. En nuestro territorio y saber trabajar con las comunidades.*

*(...) Este esquema que hemos tenido, de despreciar a las comunidades, de descalificarlas, no reconocer el sentido del trabajo ambiental, la incapacidad de llegar al territorio de manera articulada, se convierte en un malestar permanente con las comunidades y se pasa a una confrontación".*

### **Humberto de la Calle** 

De la Calle, anunció que este martes empezó la entrega de sus propuestas en temas específicos. Justamente, las primeras que se darán a conocer tienen que ver con el debate ambiental. Es así como su propuesta sobre la problemática al rededor de las consultas se centró en **escuchar a las comunidades a través de este mecanismos de participación, teniendo en cuenta que el suelo es de la gente, y corrigiendo los vicios que pueden existir en dicha herramienta.**

"*Las consultas no son solo obligatorias sino además convenientes, y me siento muy orgulloso de haber sido el vocero del gobierno en la Constitución del 91 donde se instauró ese mecanismos. No nos podemos devolver. Este tema se liga a que la paz de los humanos en una sola unidad a frente a la paz con la naturaleza (...) Un ejemplo: una comunidad puede haber vivido una pugna con las FARC, pero ahora puede encontrar otra si escasea el agua. Esa va a ser la gran discusión nacional.*

*Deben corregirse algunos vicios de la consultas, por ejemplo debe haber un inventario claro de comunidades étnicas, y debe estabilizar la jurisprudencia (...) Las consultas son validas en todos los puntos cardinales y el fundamento es que se garantice la paz con la naturaleza. *

*Consulta si, paz con la naturaleza, no nos echemos para atrás, racionalicemos la discusión, tengamos los argumentos científicos, debe haber una discusión dialogada, y ese es el  mejor instrumento para resolver los dilemas que hoy tenemos"*

### **Iván Duque**

Sorprendió que el candidato del Centro Democrático **se refirió a la fallida forma como se han manejado las regalías históricamente,** es decir, que también reconoció ese error en los 8 años del gobierno de Álvaro Uribe.

Por otra parte, nombró la necesidad de blindar a los páramos de actividades mineras. Frente a eso, cabe recordar que entre los años 2002 y 2009, el gobierno entregó títulos mineros sin obligar a las empresas nacionales y multinacionales a cumplir mayores requisitos ambientales. Durante esos años se pasó de 1,1 a 8,4 millones de hectáreas tituladas, entregándose **391 títulos mineros sobre páramos en cerca de 108.000 hectáreas.**

Duque se comprometió con la** ratificación del El Convenio de Minamata sobre el Mercurio, **sin embargo dio a entender que las consultas deben tramitarse una vez las comunidades detecten que se han incumplido los compromisos pero no tuvo en cuenta que muchas de estas consultas se están realizando de manera preventiva para evitar daños ambientales.

"*Hay que volver a sembrar confianza. Que importante sería que en la agenda ambiental se empezara a ver lo ambiental como un activo del país, por eso se debe hablar bajo el paradigma de producir conservando y conservando produciendo, para que en todo se tenga claro que lo ambiental es una responsabilidad ineludible.*

*La forma como se han manejado históricamente las regalías es una vergüenza. Se debe tomar decisiones claras: la primera: Mejorar la información sobre la cual se toman decisiones en política ambiental, para saber que no pueden haber proyectos mineros en zonas de páramos. Que si va haber proyectos extractivistas ninguno puede afectar ecosistemas diversos ni comprometer los acuíferos de Colombia. Las CAR deben dejar de ser manejadas bajo un criterio clientelista, así las consultas previas estarían bien acogidas, y no solo se verían como un requisito, igual con las consultas populares. El otro reto es que cualquier esquema de producción debe respetar el ecosistema como el patrimonio más grande que tiene Colombia".*

### **Gustavo Petro** 

La propuesta de Petro, se dirigió entonces a la necesidad de respetar el clamor de las comunidades y respaldar sus propuestas alternativas al extractivismo, como ya muchas las han planteado, en torno al ecoturismo y las actividades agropecuarias. Además, su propuesta puntual tiene que ver con la implementación de una **estrategia de transición a energías limpias, para dejar de depender económicamente de el carbón y le petróleo.**

*"Las consultas han puesto en el tapete una serie de preguntas básicas y fundamentales: Quién es el dueño del territorio y el Estado, qué es el concepto de desarrollo. Si uno se atiene a lo que dicen las consultas, el dueño del territorio y el Estado no es más que la gente, si es que se habla de una democracia. Y el desarrollo no es extraer petróleo y carbón, como hemos hecho en los últimos 30 años. Es lo contrario, incluso es un antidesarrollo.*

*Las consultas lo que dicen es que los señores que están manejando el estado están equivocados, y si la gente es la dueña se tiene que corregir y subordinarse a la decisión  popular.*

*Las resistencias desde el Estado y los actores económicos (a las consultas) no es más que la reticencia a la democracia, y no es más que la reticencia a una manera de concebir el desarrollo. ¿Es sostenible la producción de petroleo o del carbón en el mundo? ¿se puede sostener que es una posisionde equilbrio razonable esa actividad? la respuesta es no. Si la humanidad quiere permanecer la vida en el planeta nos tenemos que separar tajantemente, progresivamente del carbón y del petróleo.*

En términos generales, todos evidenciaron que en sus propuestas ambientales es preponderante, aunque sigue abierta la discusión sobre otros temas tales como sus políticas en torno a la protección animal, y por su puesto se dejó la incógnita sobre cuáles serían las estrategias de conservación ambiental de los candidatos Vargas Lleras y Ramírez. Manuel Rodríguez Becerra, en su bienvenida al evento, como director del Foro Nacional Ambiental, lamentó que no hayan asistido dichos candidatos de la derecha y señaló, **"supongo que no lo hubieran hecho con el gremio minero, pero con el tema ambiental si lo hicieron".**

https://www.youtube.com/watch?time\_continue=2486&v=jgE8cSDGl0Q

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
