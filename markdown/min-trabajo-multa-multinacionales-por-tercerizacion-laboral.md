Title: Más de 8 Multinacionales son multadas por tercerización laboral
Date: 2016-10-25 15:04
Category: Economía, Entrevistas
Tags: SIndicatos, tercerización laboral, Trabajadores
Slug: min-trabajo-multa-multinacionales-por-tercerizacion-laboral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/cerrejon-es-multada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: rociowestendorp] 

###### [25 de Oct 2016] 

La Ministra del Trabajo Clara López, multó a más de ocho empresas y multinacionales como Drummond, Carbones El Cerrejón, la Clínica Marly, entre otras**, por llevar a cabo tercerización laboral en los contratos**. Estas sanciones son producto de diferentes denuncias que han adelantado sindicatos en el país para mejorar las condiciones del trabajo.

La suma total de la multas a pagar ronda los \$15 mil millones,  de los cuales la multinacional Drummond, Carbones El Cerrejón, la contratistas Gecolsa y Dimantec deberán pagar cada una dos mil millones. De igual manera **Indupalma deberá pagar 2. 757 millones y sus 29 cooperativas asociadas deberán pagar en conjunto 22 mil millones.**

Por otro lado empresas como la **Clínica Marly y Ocupar Temporal SA. están multadas cada una con una cifra de \$3.185.259, ** el Hospital Universitario del Caribe, en Cartagena y la Temporal Konekca, también tienen multas por \$68´945.400 y \$689´454.000 respectivamente.

### **Empresas con tercerización laboral deberán mejorar sus prácticas** 

Para no pagar las multas las multinacionales **necesitan llegar a un acuerdo con el Ministerio del Trabajo y los trabajadores para formalizar los contratos**, si incumplen el pacto firmado se procede al cobro de la multa, finalmente los que no se adhieren a este mecanismo pagan la deuda, dinero que se va a los fondos del SENA.

Para la Ministra de Trabajo **este modelo de tercerización laboral afecta de forma directa a la clase media de muchas maneras, además “**en algunos casos se utiliza para impedir la formación de sindicatos o la permanencia de los mismos al interior de las empresas, en otros no se pagan todas las prestaciones de ley a los empleados”. Esta forma de contratación se hace a través de una tercera empresa que no garantiza los derechos laborales en su integralidad. Le puede interesar: ["Trabajadores de la industria petrolera se cocieron la boca exigiendo sus derechos"](https://archivo.contagioradio.com/trabajadores-de-la-industria-petrolera-se-cosieron-la-boca-exigiendo-sus-derechos/)

Establecer estas multas **es el resultado de las  investigaciones que realiza el Ministerio de Trabajo luego de que diferentes sindicatos** presenten demandas con testigos que permitan iniciar este tipo de procesos.

<iframe id="audio_13475176" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13475176_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
