Title: Guerrilleros indultados a la espera de más liberaciones
Date: 2016-01-21 16:14
Category: Nacional, Paz
Tags: FARC-EP, guerrilleros en libertad, guerrilleros indultados FARC, indulto guerrilleros
Slug: guerrilleros-indultados-a-la-espera-de-mas-liberaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Rueda-de-prensa-indultos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio. ] 

<iframe src="http://www.ivoox.com/player_ek_10155589_2_1.html?data=kpWel5qZfJqhhpywj5WbaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncajpxtffy9HQqdPj1JDW0MnZsNXVxdTgjcaPsMKfxtjdx9fFb8XZjNKSpZiJhZLnjNHWxMrWpcTd0NPSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Rene Nariño.] 

###### [[(]21 Ene 2016[)]] 

[Cerca de 12 horas en libertad completan 16 prisioneros políticos de las FARC-EP, siete mujeres y nueve hombres, que fueron indultados tras estar recluidos en distintas cárceles del país sindicados de rebelión. Sandra Patricia Isaza y Carlos Ochoa, 2 de los prisioneros beneficiados con la medida, dieron a conocer en una rueda de prensa un comunicado en el que aseguran reconocer en la aplicación del indulto “**un** **gesto de voluntad importante que ayuda al proceso de paz y da la seguridad de que sí es posible dar pasos trascendentales para la construcción de una nueva Colombia**”.]

[“Decidimos convertirnos en promotores y promotoras de paz, socializando los acuerdos de La Habana y destinando **esfuerzos para que quienes siguen en prisión tengan mejores condiciones de vida**”, manifestaron  y agregaron que quedan a la espera de que los otros 14 prisioneros indultados salgan de la cárcel en los próximos días, pues “es necesario que el país conozca la **difícil situación que padecen los prisioneros políticos en Colombia**; una crisis carcelaria que se extiende a todos los colombianos privados de libertad y que debe ser atendida con urgencia”.]

[A través del comunicado **exigieron al Gobierno nacional hacer efectivos los compromisos** asumidos en noviembre del año pasado, la **implementación de patios únicos** para los prisioneros de guerra y la realización de **brigadas de salud** para la atención de la aguda crisis sanitaria que enfrentan en los centros carcelarios. Concluyeron que queda “mucho por hacer y bastante por construir” y que asumen el “compromiso con el país de trabajar por la reconciliación y la verdadera paz”.]

[Muestra de la aguda crisis que enfrentan los prisioneros políticos René Nariño, prisionero de las FARC-EP, asegura que **tres de sus compañeros completan varias horas en huelga de hambre indefinida con la que exhortan al Gobierno a que cumpla el 70% restante de los compromisos** adquiridos pues les “queda un sinsabor, queda la incertidumbre de lo que pueda pasar con quienes siguen acá tras las rejas. Creemos que el Gobierno nacional tiene todos los mecanismos para poder llevar a cabo las siguientes liberaciones entre hoy y mañana sin más dilación”, asevera. ]

[Los prisioneros políticos de las FARC-EP llaman a la movilización por los incumplimientos en lo pactado en el gesto unilateral de paz, “no sabemos sí pueda llamarse un gesto o toda una maraña de dilaciones con nosotros los presos políticos; **el Gobierno nacional** ha estado en una posición de silencio total frente a todo lo que está ocurriendo en las cárceles del país; **sale a hablar de paz y de paz y quiere imponer fechas para la firma del tratado, pero se le olvida que la paz no es un discurso sino que requiere de acciones** que generen confianza entre las partes y las acciones aquí han sido nulas”, concluye Nariño.   ]

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
