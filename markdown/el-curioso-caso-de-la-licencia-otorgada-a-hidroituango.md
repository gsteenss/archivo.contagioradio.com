Title: El curioso caso de la licencia otorgada a Hidroituango
Date: 2017-04-25 16:08
Category: Ambiente, Voces de la Tierra
Tags: Antioquia, Hidroituango, Rios Vivos
Slug: el-curioso-caso-de-la-licencia-otorgada-a-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [25 Abr 2017] 

A tan solo dos días de que la ANLA hubiese decidido suspender la licencia para la construcción de las líneas de transmisión, por falta de estudios, el Instituto para el Desarrollo de Antioquía anunció en un comunicado de prensa que el **proyecto hidroeléctrico recibe licencia ambiental para la instalación de estas líneas.**

Para el Movimiento Ríos Vivos es “extraño” y cuestionable que los estudios sobre levantamiento de vedas y áreas protegidas impactadas por las líneas, que eran necesarios para continuar con la licencia **se hubiesen realizado en solo dos días, debido a la complejidad de la tarea, y analizadas con rigor en este mismo periodo de tiempo.**

“Hemos insistido por la fuerte presión política y económica que se ejerce sobre este proyecto, sobre el especial control que debe existir porque es uno de los proyectos más politizados del país, en el que **todas las fuerzas políticas y los grandes capitales antioqueños están invirtiendo una gran cantidad de dinero**” expresó Isabel Cristina Zuleta, vocera del movimiento.

Zuleta afirmó que una vez se supo la decisión de la ANLA, para el movimiento era evidente que el gobernador de Antioquia y el Alcalde de Medellín iban a interceder con la finalidad de resolver la licencia **“por medio de la presión indebida sobre las autoridades ambientales**”. Le puede interesar: ["Líneas de Transmisión de Hidroituango podrían afectar la salud: comunidades"](https://archivo.contagioradio.com/comunidades-afectadas-lineas-transmision-hidroituango-denuncian-irregularidades/)

El pasado 18 de abril el gobernador de Antioquia Luís Pérez señaló que el jefe de la cartera de Ambiente, Luis Gilberto Murillo, **había prometido entregar la licencia que necesita la empresa EPM para transportar energía y aseguró que esa luz verde** se daría a tiempo: *“*El ministro de ambiente me prometió* *que esa licencia ambiental que se necesita le van a dar, incluso se prometió que la van a dar a tiempo” expresó.

A su vez, el IDEA es el accionista mayoritario de la obra al ser parte de la gobernación de a Antioquia, Mauricio Tobón, director de esta institución también se refirió al tema e indicó que “**si la licencia no es entregada, las pérdidas para Hidroituango serían de cuatro mil millones de pesos diarios,** además de generar un retraso en el calendario de las obras”. Le puede interesar: ["Familias afectadas por Hidroituango exigen la suspensión del proyecto"](https://archivo.contagioradio.com/familias-afectadas-hidroituango-exigen-la-suspension-del-proyecto/)

Sin embargo, el Movimiento Ríos Vivos advirtió que aún no hay un pronunciamiento oficial por parte de la ANLA, además de ser cierto el otorgamiento, **Zuleta indicó que sería ilegal, por no haberse realizado ni aprobado la solicitud para lleva a cabo una audiencia pública ambiental**.

Frente a este panorama, Ríos Vivos anuncio un proceso de **demandas sobre las líneas y las resoluciones por violación al derecho de acceso a la información y a la participación**, y afirmaron que continuarán generando estrategias que enfrente todos los megaproyectos que hay sobre el territorio. Le puede interesar: ["Solicitan anular la licencia ambiental del mega proyecto Hidroituango"](https://archivo.contagioradio.com/solicitan-anular-la-licencia-ambiental-del-megaproyecto-hidroituango/)

<iframe id="audio_18340800" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18340800_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
