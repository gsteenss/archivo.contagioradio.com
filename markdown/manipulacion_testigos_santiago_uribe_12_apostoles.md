Title: Piden al Estado prevenir manipulación de testigos tras libertad de Santiago Uribe
Date: 2018-03-16 15:00
Category: Judicial, Nacional
Tags: 12 apostoles, Alvaro Uribe, Paramilitarismo, Santiago uribe
Slug: manipulacion_testigos_santiago_uribe_12_apostoles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/santiago-uribe-e1521230199458.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jaiver Nieto / EL TIEMPO] 

###### [16 Mar 2018] 

Santiago Uribe Vélez, investigado por la presunta conformación y financiación del grupo paramilitar de 'Los 12 Apóstoles', quedó en libertad por decisión del Juez 1ro Especializado de Antioquia, teniendo en cuenta que se han cumplido los dos años sin que el Estado determinara su juicio. No obstante **el juicio contra Uribe continúa, deberá asistir a las audiencias que requiera la justicia, y no puede salir del país.**

Así lo explica el abogado de las víctimas de los '12 Apóstoles'. "Es un proceso que tiene unas pruebas muy sólidas y contundentes y lo único que ha pasado es que el proceso penal tiene una reglas del juego que obligan a todas los sujetos procesales. No es una decisión autoritaria del juez (...) la investigación va a seguir su rumbo sin que Santiago Uribe esté preso". (Le puede interesar: [Los testimonios que comprometen a Santiago Uribe con los '12 Apóstoles')](https://archivo.contagioradio.com/los-audios-que-comprometen-a-santiago-uribe-con-los-12-apostoles-1era-parte/)

### **Los temores tras la salida de Uribe** 

Tanto el abogado como las víctimas y la Comisión de Justicia y Paz, hacen un llamado para que el Estado prevenga cualquier tipo de complot y montajes, que con la libertad del hermano del expresidente Álvaro Uribe pudieran estar preparándose, para obstaculizar el proceso.

**"Preocupa que las pruebas existentes y los testigos puedan convertirse en blanco de una estrategia de desprestigio y de montajes para desviar la investigación** y la decisión que en derecho pueda tomar el aparato de justicia ante un acervo probatorio sólido y construido en debido proceso", señala el comunicado de la Comisión de Justicia y Paz, que agrega que el Estado deberá garantizar a las víctimas su derecho a la verdad, a la justicia y la reparación.

De igual manera el abogado indica que **no sería nuevo uno de estos episodios en los que esté involucrado Santiago Uribe, por la presión sobre testigos.** Esa situación podría facilitarse ahora con su libertad, y con el poder que ostenta la familia Uribe Vélez.  [(Le puede interesar: Testigo clave contra Santiago Uribe denuncia plan para asesinarlo)](https://archivo.contagioradio.com/testigo-clave-en-caso-contra-santiago-uribe-denuncia-plan-para-acabar-con-su-vida/)

De acuerdo con Prado, en otros procesos Santiago Uribe ha participado en la manipulación de testigos como lo hizo con los montajes contra algunos magistrados en los procesos de chuzadas del DAS, también lo hizo en el proceso contra su primo Mario Uribe, y hasta en un proceso que se intentó contra el hoy candidato Germán Vargas. Además cabe recordar que en la compulsa de copias contra Álvaro Uribe, la Corte Suprema menciona que Santiago aparecía implicado en la manipulación de testigos contra el senador Iván Cepeda.

El abogado Daniel Prado espera que haya celeridad en el caso, y también existan las garantías para los testigos, de manera que **antes de finalizar el 2018 haya un fallo frente a Santiago Uribe y el grupo paramilitar que cometió más de 500 masacres en Antioquia. **(Le puede interesar: [14 revelaciones de la resolución de acusación contra Santiago Uribe)](https://archivo.contagioradio.com/las-revelaciones-de-la-resolucion-de-acusacion-contra-santiago-uribe-velez/)

<iframe id="audio_24571671" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24571671_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

   
 
