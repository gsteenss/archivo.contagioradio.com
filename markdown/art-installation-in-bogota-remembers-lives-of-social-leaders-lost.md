Title: Art installation in Bogotá commemorates lives of murdered social leaders
Date: 2019-06-13 17:30
Author: CtgAdm
Category: English
Tags: Social Leaders, Truth Commission
Slug: art-installation-in-bogota-remembers-lives-of-social-leaders-lost
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/MG_8672.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

"Quebrantos" is the brainchild of Colombian artist Doris Salcedo that brought together human rights defenders, volunteers and the public to honor the memory of 165 of the more than 600 social leaders assassinated after the signing of the peace deal in November 2016. With fragmented glass, this work denounces how the social fabric of a community is broken with each death of a human rights leader.

This art piece is part of the work carried out by the Truth Commission in defense of the social leaders and that, according to its creator, builds on the idea that " a name survives a person and that it allows for their memory to remain alive."

Between 9 am and 5 pm, close to 80 human rights leaders from various regions of the country, worked alongside about 300 volunteers, in Bolívar Square to commemorate those who were silenced for defending their communities and their territories. Beneath the rain, their efforts materialized in the realization of the artwork, which was written in 35 rows.

The commissioner Alejandra Miller addressed the threats against leaders, saying "there is a huge worry that, despite the peace deal, history is repeating itself and this worry is part of what we want to show society."

### **The protagonists of "Quebrantos"**

**"Many have lost their lives and we can not normalize what is happening in Colombia"** said Julisa Mosquera, survivor of the armed conflict and a district councilor for Black Communities, who joined the realization of this artwork.

In relation to the hopes they have for the Truth Commission, Mosquera said she hoped their investigations would uncover the truth. "We need to know who displaced us, not only those who committed the crime but also who are the intellectual authors of the  extermination of our community" she stated.

\[caption id="attachment\_68669" align="aligncenter" width="415"\]!['Quebrantos'](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/MG_8693-300x200.jpg){.wp-image-68669 width="415" height="277"} Foto: Contagio Radio\[/caption\]

"We'll keep standing we won’t keep silent»

Yeison Mosquera, a social leader from Riosucio, Chocó, spoke on the space opened for the realization of the artistic installation. "We want them to know that everyday we fight for our territory and that even if they kill one of us, we will not be silenced. We will continue to stand."

He also highlighted the exchange of experiences between leaders who arrived from regions such as Catatumbo, Antioquia, Putumayo and Chocó and who have been threatened, but that on 10 June, with"Quebrantos," came together for this collective protest.
