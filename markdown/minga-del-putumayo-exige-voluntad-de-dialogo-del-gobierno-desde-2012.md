Title: Minga del Putumayo exige al Gobierno voluntad de diálogo desde 2012
Date: 2019-05-01 20:03
Author: CtgAdm
Category: Comunidad, Política
Tags: Abuso de fuerza ESMAD, Minga Indígena, Putumayo
Slug: minga-del-putumayo-exige-voluntad-de-dialogo-del-gobierno-desde-2012
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/OPiac.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Opiac] 

Desde el año 2012, las comunidades indígenas del Putumayo en Minga permanente, siguen esperando que el Gobierno asuma su compromiso con la mesa de negociación que fue instalada hace siete años, destinada a resolver sus peticiones en materia de saneamiento, DD.HH. y constitución de resguardos, pues en lugar de garantías territoriales y según relata la población, **más del 60% de sus territorios ha sido concedido a multinacionales petroleras.**

Según Álvaro Cruz, vicepresidente de la Organización Zonal Indígena del Putumayo en la región tampoco se ha avanzado en el Programa Nacional Integral de Sustitución de Cultivos Ilícitos ni en los Planes de Desarrollo con Enfoque Territorial, "más del 50% de iniciativas de los PDE son étnicas y sabemos que han asignado 4.2 billones para este trabajo, y le pedimos al Gobierno que se siente con nosotros para saber cómo se va a implementar", escenario que no se ha dado.[(Lea también: En audiencia senatorial examinarán impactos del extractivismo en Putumayo)](https://archivo.contagioradio.com/en-audiencia-senatorial-examinaran-impactos-del-extractivismo-en-putumayo/)

### La respuesta al diálogo en Putumayo es el ESMAD

El pasado 29 de abril, cerca de las 10:00 am, el ESMAD arremetió contra la **Minga ** en la vereda de Condagua dejando ocho personas heridas entre ellas al presidente de la Organización de los Pueblos Indígenas de la Amazonia Colombiana (OPIAC), Julio César López Jamioy quienes fueron llevadas al hospital de Mocoa y se encuentran fuera de peligro.

**El Vicepresidente de la organización indicó que** el ESMAD no accedió al diálogo y arremetió contra la Minga, "utilizando munición de guerra y escopetas de calibre 12",  adicionalmente evidenciándose la presencia de personas encapuchadas quienes pertenecerían a la Fuerza Pública, filmando a los indígenas en movilización, lo que "podría derivar en posteriores persecuciones contra autoridades y personas indígenas", agrega un comunicado de la OPIAC.

> Esta es la manera en que el ESMAD [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw) arremete en contra de los Mingueros Indígenas del Putumayo, presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) hacemos un llamado al diálogo y no a la represión los Pueblos Indígenas Amazonicos, queremos paz y no violencia. [@DefensoriaCol](https://twitter.com/DefensoriaCol?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [pic.twitter.com/olJdLezos4](https://t.co/olJdLezos4)
>
> — OPIAC (@OPIAC\_Amazonia) [29 de abril de 2019](https://twitter.com/OPIAC_Amazonia/status/1122977053740085248?ref_src=twsrc%5Etfw)

<h3>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
**"La voluntad, no puede ser un discurso, tiene que ser un hecho"**

</h3>
Aunque la Minga tenía planeado reunirse con el Gobierno este 6 de mayo, únicamente hizo presencia en [el resguardo de Condagua la directora de asuntos étnicos del Ministerio del Interior quien expresó que los negociadores del Gobierno no pueden hacer presencia en la región por temas de agenda, pese a que se había acordado trabajar en mesas simultaneas las diferentes temáticas del pliego de peticiones presentado por las comunidades.]

"Para los pueblos indígenas del Putumayo no hay agenda, pero el Gobierno si tiene toda la disposición para el atender las exigencias de las multinacionales, aquí estaremos resisitiendo y esperando a que el Gobierno tenga esa voluntad que tanto expresa" afirma Cruz agregando que este fue un espacio concertado, no fue impuesto y que debe cumplirse.

 

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
