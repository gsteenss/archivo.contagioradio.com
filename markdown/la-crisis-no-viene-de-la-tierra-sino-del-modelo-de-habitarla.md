Title: "La crisis no viene de la Tierra, sino del modelo de habitarla"
Date: 2020-04-23 18:30
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: Ambiente
Slug: la-crisis-no-viene-de-la-tierra-sino-del-modelo-de-habitarla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-23-at-3.34.11-PM.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Ptt-2020-04-22-at-11.55.04-AM-1.ogg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

A propósito del pasado Día Mundial de la Tierra, diferentes organizaciones se pronunciaron haciendo visible los logros, pero también las perdidas que hemos tenido como raza humana, en relación a la madre tierra y nuestra habitabilidad en ella.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La conclusión de parte de varios gestores ambientales fue contundente, *[**"la tierra hace un llamado de urgencia a todos los que habitamos este planeta, y nos dice que lo que la crisis**]{}**actual no es lo peor que puede ocurrir**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Palabras en la voz de Isabel Cristina Zuleta, vocera del Movimientos Ríos Vivos, quien agregó que lo que pasa actualmente es lo que mal llamamos desarrollo, *"este es el caso de las hidroeléctricas, los pozos petroleros y los proyectos mineros, que hoy amenazan a las poblaciones que habitan de manera natural los territorios".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y enfatizo en la responsabilidad estatal, *"El Gobierno de Colombia, no entiende que* ***el* extractivismo *es lo que nos esta poniendo en esta situación,*** *y es lo que se tiene que parar para que no sea peor después"*.

<!-- /wp:paragraph -->

<!-- wp:video {"autoplay":false,"id":83471,"loop":false,"muted":false,"playsInline":false,"src":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-23-at-3.34.11-PM.mp4"} -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-23-at-3.34.11-PM.mp4">
</video>
  

<figcaption>
Vídeo de la lideresa ambiental en el Día Mundial de la Tierra

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:heading {"level":3} -->

### Un modelo de desarrollo insostenible

<!-- /wp:heading -->

<!-- wp:paragraph -->

El abuso de los recursos naturales y la [explotación desmedida](https://archivo.contagioradio.com/el-gozar-de-un-ambiente-sano-no-frena-el-desarrollo-del-pais/)de las especies animales para satisfacer la demanda alimentaría mundial; una alimentación basada en practicas tóxicas y poco sanas que desconocen las semillas nativas y las practicas sostenibles de los campesinas, son el reflejo de un modelo de desarrollo insostenible y suicida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmación que por años ha contrarrestado el campesinado colombiano con la busqueda del reconocimiento de las semillas nativas y un alto a la importación de productos que cierran las puertas a lo local; Gérman Vélez vocero del [Grupo Semillas](https://www.semillas.org.co/) ante ello señala.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"La globalización del sistema alimentario ha sido uno de los generadores de la crisis climática y la devastación de gran parte de los ecosistemas, mediante la imposición de modelos productivos basados en la agricultura industrial, en mega granjas de animales, en la degradación de los suelos, la fuentes hídricas, y la contaminación del ambiental y los alimentos con agroquímicos y transgénicos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que, los sistemas alimentarios globalizados industrializados han tenido un apetito ilimitado para colonizar la tierra, los bosques, la destrucción, manipulación animales y plantas, sin respetar su integridad y su salud, *"**estas formas de producción han profundizado el cambio climático y han desplazado de sus territorios a múltiples pueblos indígenas y campesinos**".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"En estos momentos de crisis ambiental económica sanitaria y alimentaria **el país debía ser un alto en el camino y reflexionar sobre las causas estructurales del fracaso del modelo de desarrollo rural** y sobre las formas insostenibles como producimos y consumimos alimentos"*
>
> <cite> Gérman Vélez| Vocero del Grupo Semillas </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y hace la invitación enfática a migrar a otro tipo de sistemas agroecologíco, *"la producción local nos ha enseñado sobre el consumo responsable justo y solidario, **un mercado realizado hoy en día por millones de familias campesinas, que sustentan en más del 70% la producción y el consumo de alimentos básicos en el país**"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Entendamos que no es el planeta el que está en crisis, sino el modelo de habitarlo"

<!-- /wp:heading -->

<!-- wp:audio {"id":83478} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Ptt-2020-04-22-at-11.55.04-AM-1.ogg">
</audio>
  

<figcaption>
Audio: Andrés Gómez | Censat

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

Con la invitación de unirnos para **construir sanar y reorganizar**, CENSAT, presenta el informe***["Necesitamos una transición ambiental para la reproducción de la vida",](file:///C:/Users/Usuario/Downloads/Documento%20de%20posicionamiento-3.pdf)*** un espacio que permite entender que *"lo que está actualmente en riesgo no es el clima, sino la vida".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desarrollando puntos como el capitalismo verde, que según Censat es *"en pocas palabras **pagar por contaminar**, dicho de otra manera es ponerle precio a las actividades no contaminantes para contabilizar la contaminación que es susceptible a ser comprada".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dejando varias reflexiones entre ellas el poder del pueblo, de las personan que trabajan, aman, conocen y protegen la tierra, *"necesitamos de la agricultura campesina, la protección de selvas y reservas de agua sin necesidad de rejas o ejércitos que las custodien, de sistemas alternativos de transporte, de redes comunitarias que apoyen la soberanía alimentaria, una economía solidaria"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recordándonos que una lucha de 50 años, y que conmemora en el día de tierra debe ser un momento de re-evaluarnos como humanidad, una humanidad que se plantea retos por su **pacha mama**, y le demuestra su amor a partir de cambios reales de la manera de habitar y relacionarnos con la tierra.

<!-- /wp:paragraph -->
