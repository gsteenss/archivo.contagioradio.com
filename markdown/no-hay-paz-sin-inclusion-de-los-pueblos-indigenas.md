Title: No hay paz sin inclusión de los pueblos indígenas
Date: 2019-09-14 17:54
Author: Foro Opina
Category: Opinion
Slug: no-hay-paz-sin-inclusion-de-los-pueblos-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/capturadepantalla20190330alas65837pm-a4e8003be5db7ba55a3d38e8fde89cb1-1200x600.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto de publimetro] 

Un 09 agosto de 1982 se reunió por primera vez el Grupo de Trabajo de las Naciones Unidas sobre las múltiples Poblaciones de pueblos originarios de la Subcomisión de Prevención de Discriminaciones y Protección a las Minorías, por ello se adoptó esa fecha como ocasión para conmemorar el Día de los Pueblos Indígenas en el mundo.

De acuerdo con cifras de este organismo internacional, se registran más de cinco mil grupos en cerca de noventa países, donde 370 millones de indígenas hablan siete mil lenguas autóctonas. Esta población representa el 15% de los grupos humanos más pobres, en situación de vulnerabilidad social, económica y cultural.

En Colombia, la condición de los pueblos indígenas ha sido, históricamente, de exclusión, discriminación e inoperancia del Estado para garantizar su protección y preservación. Los más de setecientos resguardos indígenas están ubicados en una extensión de 31 millones de hectáreas, de las cuáles sólo el 8% son aptas para los cultivos y el desarrollo agrario.

(Le puede interesar:[La ONU advierte que la situación de los pueblos indígenas en el mundo es peor que hace 10 años](https://archivo.contagioradio.com/el-mundo-sigue-rezagado-con-respecto-a-los-derechos-de-los-indigenas-10-anos-despues-de-su-declaracion-historica-advierten-expertos-de-la-onu/))

A diferencia de lo que opinan algunos sectores políticos y terratenientes, las minorías étnicas del país tienen tierras, pero no alimento. Muchas de estas áreas son desiertos, selvas y reservas naturales, lo que limita su capacidad de usufructo. Entre las problemáticas más significativas de los pueblos indígenas en Colombia se encuentra la pobreza: Más de la mitad de los 1,37 millones de indígenas del país se encuentra en situación de pobreza estructural, según el PNUD. Sumado a esto, la explotación de recursos naturales y la imposición de un modelo de economía extractivista también han afectado el tejido social de las minorías étnicas, sus modos de vida ancestrales y la sostenibilidad ambiental de sus territorios.

Prevalecen las llamadas economías de enclave, que no necesariamente implican beneficios a las poblaciones nativas; por el contrario, tienden a profundizar las desigualdades, la pobreza y la contaminación, al tiempo que estancan la diversificación de las economías locales.

Preocupa la situación de regiones como la Amazonía, donde el Gobierno Nacional tiene presupuestada la aprobación de títulos mediante la figura de Áreas de Explotación Minera para la concesión de territorios a empresas multinacionales. Diversas organizaciones sociales y ambientalistas han llamado la atención sobre los peligros de entregar áreas ya delimitadas de departamentos como Guainía, Vaupés y Guaviare a terceros, aún en contravía del movimiento social y político mundial en pro de la preservación del llamado pulmón del mundo.

En ese contexto, los liderazgos sociales de los pueblos indígenas son claves para darle voz a estas comunidades y mantener vigentes sus luchas en la agenda nacional e internacional. No obstante, en Colombia son constantes las denuncias de presiones, persecución y criminalización de líderes indígenas defensores de derechos humanos y de sus territorios. Por un lado, grupos armados organizados, reductos del paramilitarismo, disidencias de las FARC y otros actores, hostigan a las comunidades indígenas a abandonar sus territorios en aras de intereses relacionados con los cultivos ilícitos, las rutas del narcotráfico y la explotación ilegal de recursos naturales. Pero también las fuerzas del Estado estigmatizan y criminalizan las luchas sociales de los pueblos indígenas y las autoridades no brindan garantías para la protección de la vida de los líderes.

##### En el Acuerdo de La Habana: 

Se estableció un capítulo que resalta el rol de las minorías étnicas en construcción de la paz territorial a través de acciones como el acceso a tierras productivas, la participación especial en los programas de desarrollo con enfoque territorial y el programa de sustitución de cultivos ilícitos, su reconocimiento como víctimas del conflicto y la implementación de medidas para contrarrestar la discriminación y exclusión. Para darle cumplimiento a estas disposiciones se requiere de organizaciones indígenas fortalecidas, con capacidad de incidencia e interlocución con otros actores, incluida la institucionalidad. Pero también es fundamental que el gobierno nacional asuma el compromiso y entienda que los pueblos indígenas de Colombia deben ser vistos como actores claves para el desarrollo territorial y no como un mero obstáculo para los intereses de las élites económicas.

Igualmente, es importante que los gobiernos locales garanticen la inclusión de los pueblos indígenas en la planeación participativa del territorio y en la toma de decisiones desde las instancias de participación ciudadana, tales como los consejos de política social, desarrollo rural, cultura y paz. Según la ONIC, desde la firma del Acuerdo, más de 60 líderes indígenas han sido asesinados, lo que implica mayor responsabilidad de los Alcaldes y Gobernadores para garantizar la vida de estas comunidades.

(ver mas: [La inclusión política de los pueblos indígenas enriquece las democracias en América Latina](https://www.undp.org/content/undp/es/home/ourperspective/ourperspectivearticles/2013/05/23/la-inclusion-politica-de-los-pueblos-indigenas-enriquece-las-democracias-en-america-latina.html))

El reto de las nuevas autoridades locales electas es asegurar la activa participación de los pueblos indígenas en la formulación, ejecución y seguimiento de los planes de desarrollo y las políticas públicas con enfoque diferencial. Es así es como se asegura la construcción de una verdadera paz territorial sostenible y con inclusión social.

 

**Por: Foro Nacional por Colombia**

###### \*La Fundación Foro Nacional por Colombia es una organización civil no gubernamental sin ánimo de lucro, creada en 1982 por iniciativa de intelectuales colombianos comprometidos con el fortalecimiento de la democracia y la promoción de la justicia social, la paz y la convivencia. El propósito de nuestro trabajo es crear las condiciones para el ejercicio de una ciudadanía activa con capacidad de incidencia en los asuntos públicos. El pluralismo, la participación ciudadana, la concertación democrática, la corresponsabilidad y la solidaridad son la base para el desarrollo de nuestra misión, con un enfoque diferencial (de género, generación y etnia). Desde sus inicios, Foro rechazó la violencia como forma de acción política. Por ello cobijó la propuesta de una salida negociada al conflicto armado y del fomento de una cultura democrática. Luego de la firma del Acuerdo entre el Gobierno y las FARC, Foro ha orientado su quehacer hacia el objetivo estratégico de contribuir a la construcción de la paz y la convivencia en Colombia. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente). 
