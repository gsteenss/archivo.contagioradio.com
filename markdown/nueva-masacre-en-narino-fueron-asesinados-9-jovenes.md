Title: Nueva masacre, en Nariño  fueron asesinados 9 jóvenes
Date: 2020-08-16 09:29
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: acuerdo de paz, AGC, asesinato, masacre, nariño
Slug: nueva-masacre-en-narino-fueron-asesinados-9-jovenes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Captura-de-Pantalla-2020-08-16-a-las-9.27.12-a.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este domingo 16 de agosto en Samaniego, [Nariño](https://www.justiciaypazcolombia.com/narino-dos-masacres-por-resolver/)**se denunció el asesinato de 9 personas y otras más heridas en medio de una incursión armada** en horas de la noche a 5 minutos de la zona urbana.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1294940495010050048","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1294940495010050048

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

  
El hecho **se presentó en la vereda Catalina, municipio de Samaniego, departamento de Nariño, el 15 de agosto en horas de la noche** y confirmada en la madrugada de este domingo 16 de agosto.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FelicianoValen/status/1294976778092650496","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FelicianoValen/status/1294976778092650496

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Fuentes del territorio aseguran que **sobre las 10:00 pm ingresaron encapuchados a una casa campestre en la vereda Catalina y ejecutaron con arma de fuego a 8 jóvenes** que se encontraban allí reunidos, y posteriormente en horas de la madrugada a una mujer en otra casa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las víctimas hasta el momento fueron i**dentificadas como Byron Patiño, Andrés Obando, Rubén Ibarra, Sebastián Quintero, Daniela Vargas y 3 personas más** que siguen aún sin identificar. (Le puede interesar: [Campesino habría sido asesinado por policías en Tumaco](https://archivo.contagioradio.com/campesino-habria-sido-asesinado-por-policias-en-tumaco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El ELN podría estar involucrado en este hecho esto ya que por medio de un audio conocido por Contagio Radio, advierte a la población de Nariño, que de no atender el llamado por las buenas *"ellos se encargarán de hacer cumplimiento a las malas".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Nos enteramos que la gente de la cabecera municipal no está comprometida con el asunto del covid 19 (… ), se les ha dicho, pero no entienden por las buenas, si no nos colaboran por las buenas , pues les va tocar por las malas, si a ustedes no les importa la vida de sus propios compatriotas, pues a nosotros tampoco nos va a importar la vida de ustedes"*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por medio de múltiples denuncias , panfletos y hechos violentos han evidenciado que en Samaniego operan grupos armados como el ELN, FOS y las AGC, además de la tercera división del Ejército Nacional, Brigadier General Marco Vinicio Mayorga Niño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Diferentes personas se han sumado al llamado a las autoridades militares y al Gobierno a tomar acciones que devuelvan la tranquilidad en la región añadiendo, "*e**stamos convencidos que sólo con inversión oportunidades y cumplimiento de lo estipulado en el Acuerdo de Paz** con sustitución de cultivos de uso ilícito e intervención social lograremos consolidar La Paz"*, aseguró el Gobernador de Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Llamado que se extendió a través de diferentes cuentas de la red social Twitter, en donde exigen no más derramamiento de sangre en el departamento de Nariño, **el cual hasta la fecha registra 20 personas asesinadas en una población que no supera los 50,000 habitantes,** [violencia](https://archivo.contagioradio.com/atentan-contra-la-vida-del-lider-awa-javier-cortes-guanga-en-narino/)que se extiende a toda la región.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/VerdadPacifico/status/1294981985815932929","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/VerdadPacifico/status/1294981985815932929

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/piedadcordoba/status/1294990793581760514?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Etweet","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/piedadcordoba/status/1294990793581760514?ref\_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Etweet

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Ejemplo de ello es la [Alerta Temprana](http://www.indepaz.org.co/wp-content/uploads/2020/02/AT-N%C2%B0-082-18-NAR-Cumbitara-El-Rosario-Leiva-y-Policarpa.pdf)emitida por la población de los municipios del Rosario y Leiva, debido a la presencia de disidencias de las FARC y las AGC; en donde denuncian enfrentamientos armados desde el pasado 8 de agosto y hoy ponen en riesgo de reclutamiento desplazamiento, e incluso violencia sexual a más de [20.000 personas](https://archivo.contagioradio.com/en-riesgo-20-000-personas-por-accionar-de-grupos-armados-en-narino/) en esta zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma el asesinato el pasado 9 de agosto de **Christian Caicedo y Michael Ibarra, de 12 y 17 años**, quienes según versiones de la comunidad iban a llevar una tarea que les habían delegado en su colegio. (Lea también: [Cristian y Maicol, menores víctimas de la barbarie en Cauca y Nariño](https://archivo.contagioradio.com/cristian-y-maicol-menores-victimas-de-la-barbarie-en-cauca-y-narino/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nariño, es uno de los departamentos que enfrentar grandes problemas de violencia por disputa de tierras y que además ha causado múltiples asesinatos a defensores, líderes de Derechos Humanos y comunidades indígenas; así como masacres y el fuego cruzado entre diferentes grupos armados que dejan en medio a la comunidad campesina y que evidencian aún más el olvido por parte del Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Olvidó que según Indepaz, ubica el departamento de Nariño como el tercero más peligroso y con el mayor número de asesinatos desde la firma del Acuerdo de Paz con 84 casos registrados. ( Otra Mirada: [Nariño resiste ante el olvido estatal](https://archivo.contagioradio.com/otra-mirada-narino-resiste-ante-el-olvido-estatal/)).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
