Title: Biciusuario entutela Bogotá por contaminación de aire
Date: 2019-06-28 17:29
Author: CtgAdm
Category: Ambiente, Judicial
Tags: contaminación de aire, Transmilenio
Slug: biciusuario-entutela-bogota-por-contaminacion-de-aire
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/bicicleta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: Foro Mundial de la Bicicleta] 

Ante la Corte Constitucional, fue radicada una acción de tutela en contra del Distrito de Bogotá por **vulnerar su derecho a la dignidad, salud, información y participación ciudadana** al no implementar medidas preventivas para proteger a los bogotanos de la mala calidad de aire.

De acuerdo con la abogada Laura [Santacoloma, el accionante Daniel Bernal comenzó a ver su salud afectada después de su jornada diaria en bicicleta o en transporte público. En particular, desarrolló gripas recurrentes, ojos secos y vías respiratorias obstruidas, lo que estaría relacionado con los altos índices de material particulado generado por los exostos de vehículos de diésel. (Le puede interesar: "][Tres propuestas para respirar mejor en Bogotá](https://archivo.contagioradio.com/tres-propuestas-para-respirar-mejor-en-bogota/)")

Esta año, se ha dado una serie de contingencias ambientales ocasionadas por la mala calidad de aire en Bogotá. Sin embargo, Santacoloma señala que en la Alcaldía no **existen medidas preventivas ni instrumentos de evaluación del impacto en la salud en los ciudadanos que se trasladan diariamente en el  sistema de transporte público**.

"No tenemos datos que nos permitan establecer si como ciudadanos es seguro hacer uso del sistema o de la bicicleta diariamente. ¿O si no es tan seguro, cuáles son las medidas que deberíamos adoptar?", sostuvo la Abogada.

La tutela solicita la elaboración de estudios ciéntificos por parte del Distrito para determinar cuales medidas deberían tomar los ciudadanos. Además, le pide a la Corte Constitucional reconocer acceso al aire limpio como un derecho fundamental.

"En la sentencia T-034 del 2016, se estableció y se recogió la jurisprudencia alrededor del agua como un derecho fundamental. ¿Entonces si el agua es un derecho fundamental, como no vas ser el aire? Uno puede vivir dos o tres días sin aire pero uno no puede vivir más de cinco o seis minutos sin aire", afirmó.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
