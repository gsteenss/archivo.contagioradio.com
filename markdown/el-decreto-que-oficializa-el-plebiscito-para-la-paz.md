Title: El decreto que oficializa el plebiscito para la paz
Date: 2016-08-30 15:58
Category: Otra Mirada, Paz
Tags: FARC, Gobierno, La Habana, paz, Plebiscito
Slug: el-decreto-que-oficializa-el-plebiscito-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/votaciones-e1512084420951.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Votaciones Colombia 

###### [30 Ago 2016] 

["¿Apoya usted el acuerdo final para terminar el conflicto y construir una paz estable y duradera?", es la pregunta que quedó para que los colombianos y colombianas respondan el próximo 2 de octubre en el plebiscito para la paz, convocado por el presidente Juan Manuel Santos.]

Si gana el sí en el plebiscito por la paz, el presidente de la República **podrá incorporar los acuerdos en la Constitución a través de proyectos o actos legislativos  que posteriormente el Congreso debatirá.**

"Si la ciudadanía no se deja arrastrar por la demagogia o por el miedo, sino por el contrario se tranquiliza, se empodera, estudia los acuerdos, escuchas las posiciones y participa activamente, será un total éxito, va a ser algo muy importante para la vida democrática del país" afirma Rodolfo Arango, analista político.

Cabe recordar que de acuerdo con la Misión de Observación Electoral, 243 municipios colombianos están en riesgo por factores de violencia y fraude para la votación del plebiscito por la paz. De ellos, 53 están en riesgo extremo, 83 tienen riesgo alto y 107 riesgo medio. La organización llama la atención de las autoridades estatales para que estén vigilantes y recomienda la presencia de observadores internacionales, teniendo en cuenta que **en departamentos como Chocó, Arauca, Cauca, y Putumayo más de la mitad de los municipios presentan riesgos electorales**.

<iframe id="doc_98575" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/322594756/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
