Title: Tres propuestas de universitarias para erradicar la violencia de género
Date: 2020-02-11 18:58
Author: CtgAdm
Category: Educación, Mujer
Tags: acoso, uis
Slug: tres-propuestas-de-universitarias-para-erradicar-la-violencia-de-genero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Feminicidio-en-la-UIS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @lizethjuliana6 {#foto-lizethjuliana6 .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este martes 11 de febrero se llevó a cabo en la Universidad Industrial de Santander (UIS) un Consejo Superior Universitario ampliado para dar solución a las violencias basadas en género que se están presentando en la Institución, y que recientemente cobraron la vida de las estudiantes **Angie Paola Cruz Ariza y Manuela Betancour Vélez**. En el encuentro, además se denunciaron casos de acoso en la Universidad, lo que invitó a pensar si el protocolo de atención a estos hechos es insuficiente.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Dos estudiantes asesinadas, muchas más víctimas de** violencia de género

<!-- /wp:heading -->

<!-- wp:paragraph -->

Antes de que iniciará el Consejo Superior Universitario ampliado, se realizó una asamblea en la escuela de Trabajo Social y otra en el Instituto de Lenguas, donde las estudiantes denunciaron que se han presentado casos de acoso sexual provenientes tanto de docentes como de alumnos del centro educativo. (Le puede interesar:["Acoso sexual en la universidades, una violencia silenciosa contra las mujeres"](https://archivo.contagioradio.com/acoso-sexual-en-la-universidades/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco de las denuncias, una estudiante de la UIS, que prefirió reservar su nombre, señaló para Contagio Radio, que por lo menos hay un registro de 15 relatos presentados ante el Consejo Superior en donde los víctimarios son dos hombres que pertenecen a la institución y que cometen las agresiones en el campus.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Sociedad machista, Universidad machista**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Colectivos feministas han convocado a reuniones para plantear acciones frente a los feminicidios que se han presentado en la UIS y la Universidad Pedagógica Nacional (el asesinato de la estudiante de Biología Leidy Carolina Rodríguez). Según señaló la estudiante, estas realidades se presentan porque la universidad no se escapa de la lógica de una sociedad machista.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fradiopraxisupn%2Fposts%2F2378721858899099&amp;width=500" width="500" height="502" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

En ese sentido, aseguró que la labor de las Universidades debería ser eliminar esas violencias. No obstante para ella, las Instituciones educativas no hacen lo suficiente. En primer lugar, resaltó que ninguna Universidad tienen una estadística sobre las denuncias por violencias basadas en género.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fposts%2F477148436291084&amp;width=500" width="500" height="731" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

En segundo lugar, afirmó que solo 3 universidades públicas tienen una política de género: La Universidad Nacional, la UIS y la Universidad del Valle. Adicionalmente, sostuvo que de las 32 Universidades Públicas del país, sólo 6 cuentan con protocolos contra las violencias basadas en género: UIS, Universidad Nacional, Universidad del Valle, Universidad Pedagógica Nacional, Universidad de Antioquia y la Universidad Pedagógica y Tecnológica de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, la estudiante declaró que en el marco de los protocolos se hace acompañamiento psicosocial a quienes denuncian, pero no tienen normativas tendientes a sancionar a los responsables. En consecuencia, resaltó que es importante mantener el acompañamiento psicosocial, pero también generar una normatividad que propenda por evitar estos hechos. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las soluciones que han planteado en la UIS**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el marco del Consejo Ampliado en la UIS, la estudiante señaló que se propuso la creación de un espacio educativo (tipo cátedra) en temas de género que todos los estudiantes puedan cursar. De igual forma, se propuso que se otorguen [recursos](https://www.facebook.com/radiopraxisupn/photos/a.831952960242671/2378910102213608/?type=3&theater) a la política de género de la universidad para que desde allí se puedan realizar espacios de reflexión en el Campus sobre las violencias basadas en género.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
