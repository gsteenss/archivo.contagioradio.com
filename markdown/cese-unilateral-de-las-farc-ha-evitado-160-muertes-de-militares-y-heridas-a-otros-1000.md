Title: Cese unilateral de las FARC ha evitado 160 muertes de militares y heridas a otros 1000
Date: 2015-04-23 14:57
Author: CtgAdm
Category: Nacional, Paz
Tags: alirio uribe, Buenos Aires Cauca, cese unilateral de las FARC, FARC, Frente Amplio por la PAz, Juan Manuel Santos
Slug: cese-unilateral-de-las-farc-ha-evitado-160-muertes-de-militares-y-heridas-a-otros-1000
Status: published

###### foto: contagioradio.com 

El Frente Amplio por la Paz, entregó hoy el cuarto informe de veeduría al cese unilateral de las FARC decretado desde el pasado 20 de Diciembre de 2014. Según las cifras entregadas por Alirio Uribe este cese ha ahorrado las vidas de 160 militares y ha impedido que otros 1000 resulten heridos, en comparación con los meses anteriores y cifras del propio ministerio de defensa.

Sin embargo se señala que el cese unilateral se hace insostenible puesto que en el mismo tempo han resultado muertos 27 guerrilleros, 14 heridos y 12 detenidos, lo que evidencia la persistencia de las operaciones militares que en varios casos producen reacciones de defensa por parte de la guerrilla de las FARC.

Respecto de los hechos presentados el pasado 14 de Abril en el departamento del Cauca en los que perdieron la vida 11 integrantes de las FFMM y un integrante de las FARC, el Frente Amplio señala que lamenta que se sigan perdiendo vida y que por ello es necesario continuar exigiendo un cese bilateral de fuego para que se respalde el proceso de conversaciones en hechos reales y verificables que reconstruyan la confianza de la sociedad colombiana.

[HECHOS RELEVANTES 4° informe cese unilateral de las FARC](https://es.scribd.com/doc/262887728/HECHOS-RELEVANTES-4-informe-cese-unilateral-de-las-FARC "View HECHOS RELEVANTES  4° informe cese unilateral de las FARC on Scribd")

<iframe id="doc_87964" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/262887728/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="65%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

Piedad Córdoba afirma que se acogen a la propuesta de la delegación de paz de las FARC en la que se está construyendo un informe detallado de los acontecimientos que rodearon el hecho del 14 de Abril y que para ello se están acompañando de diferentes organizaciones nacionales e internacionales tanto civiles como militares.

La continuidad de los bombardeos siguen siendo una gran preocupación del Frente Amplio así como la muerte de 8 indígenas en lo que va corrido de este mes. Luis Fernando Arias, consejero mayor de la ONIC denunció que esta misma semana murió una indígena en la región del Alto Andágueda en Chocó como consecuencia de las operaciones militares que arreciaron en los últimos días.

\[embed\]https://www.youtube.com/watch?v=K\_ifSCo87b4\[/embed\]  
Ver el informe  
http://issuu.com/comunicacionesfrenteamplio/docs/cuarto\_informe\_veeduria\_cese\_unilat/1
