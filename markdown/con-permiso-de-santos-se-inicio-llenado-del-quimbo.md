Title: Con autorización ilegal de Santos se inicia llenado de El Quimbo
Date: 2015-07-01 16:36
Category: Uncategorized
Tags: ASOQUIMBO, Carlos Mauricio Iriarte, El Quimbo, EMGESA, gobernación de Huila, Huila, Juan Manuel Santos, llenado del Quimbo, Miller Dusan
Slug: con-permiso-de-santos-se-inicio-llenado-del-quimbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/quimbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.dhradio.co]

###### [1 de Julio 2015]

Desde Asoquimbo, se denuncia que este martes a partir de las cinco de la madrugada, se inició el llenado del Quimbo, tras el permiso del **Presidente Juan Manuel Santos, quien habría ordenado ilegalmente el llenado del proyecto hidroeléctrico de EMGESA.**

El gobernador del Huila, Carlos Mauricio Iriarte se mostró sorprendido e indignado por la noticia, resaltando que **EMGESA ni siquiera ha compensado a los damnificados por el proyecto.**

Se trata de un proceso **“poco oportuno y una burla”**, expresa el gobernador del Huila, teniendo en cuenta que EMGESA solo reconoce que ha afectado a 3 mil personas, desconociendo que en total son aproximadamente **30 mil los pobladores que han sufrido las consecuencias** de esta obra.

El llenado del Quimbo, **generaría graves consecuencias ambientales,** como ya se había anunciado. El sector piscícola, sería el más amenazado, lo que a su vez traería consecuencias negativas para la población que vive de la pesca.

La construcción de la hidroeléctrica ya ha dejado más de **12 mil familias desplazadas,** además no ha reparado a los pobladores que se quedaron sin tierras, y de las 2.900 hectáreas de terrenos que debían ser entregadas a los campesinos, “**no se ha entregado ni un milímetro de tierra a la fecha”,** denuncia Miller Dusán, investigador de ASOQUIMBO, quien adicional a eso, asegura que EMGESA ha actuado como quiere, gracias a la [subordinación de la Autoridad Nacional de Licencias Ambientales, ANLA,  frente a la empresa.](https://archivo.contagioradio.com/anla-se-subordina-ante-emgesa-asoquimbo/)

Eso mismo afirma el gobernador Iriarte, quien señala que **EMGESA no ha materializado ninguno de los acuerdos pactados en la licencia ambiental.**
