Title: La intentona golpista en Turquía
Date: 2016-07-18 09:25
Category: Cesar, Opinion
Tags: Golpe de Estado, Turquía
Slug: la-intentona-golpista-en-turquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/turquia-golpe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defne Karadeniz 

#### **[Cesar Torres del Río ](https://archivo.contagioradio.com/cesar-torres-del-rio/)** 

###### 18 Jul 2016 

Sigue siendo extraño el movimiento golpista contra Recep Erdogan, el presidente islámico en Turquía, acción que todos los partidos condenaron incluyendo el grupo islámico dirigido por Fetullah Gülen, religioso opuesto a Erdogan. Si bien los militares han intervenido políticamente en varias oportunidades (1960, 1971, 1980 y 1997) y su experiencia internacional se incrementa día a día - es el segundo mayor ejército en la OTAN después del de Estados Unidos - , no había en el escenario presente factores que indicaran movimientos contrarios de envergadura a las decisiones en el Estado, no porque  estuvieran de acuerdo con las políticas del presidente sino porque la permanente depuración y represión contra el laicismo en las Fuerzas Armadas lo impedían.

[Erdogan no es figura política reciente en su país, cuyo sistema parlamentario desea cambiar al presidencialismo. De acuerdo a la Constitución de 1980 Turquía es un país laico lo cual mantiene los parámetros del fundador de la República  Kemal Atatürk, pero cerca del 90% de la población (80 millones) se declara musulmana. El partido gobernante, AKP (Partido de la Justicia y el desarrollo),   islámico, creado por Erdogan, vehiculiza todo el modelo neoliberal y represivo que se adelanta desde el Estado contra las mujeres, la intelectualidad democrática, el laicismo, los kurdos, los trabajadores, los estudiantes, algunos medios de comunicación y el pueblo turco. Opuesto a ese modelo, Gülen ha venido impulsando una islamización de tipo horizontal que ha alcanzado varios logros en su política de convivencia con el Estado laico, sin que ello signifique, de acuerdo a lo conocido hasta ahora, que fue este líder el que impulsó el movimiento golpista.   ¡Contradictoria situación en la que desde el Estado con Constitución laica y con presidente islámico se persigue al islamismo en un país de mayoría musulmana!  ]***Así, no hay posibilidad alguna de adelantar políticas de reconciliación entre de los distintos sectores sociales***[.]

[Político en declive hasta hace unos pocos días Erdogan se ha recuperado con la intentona golpista. La OTAN, Estados Unidos, la Unión Europea (UE) han emprendido su defensa. Ahora es evidente que las aspiraciones turcas para ingresar a la UE se facilitarán, adicional al hecho del acuerdo firmado el 28 de marzo de este año con “Europa Fortaleza” para servir de país filtro con los refugiados: retener a 2.7 millones en Turquía y readmitir a los devueltos por Grecia; Turquía ingresará a la UE como un inmenso campo de refugiados.  Por otra parte aunque ha declarado que “la cuestión kurda no existe”, con la derrota de la intentona golpista, además, Erdogan tiene ahora mayor libertad para actuar contra la población kurda en el norte de Irak,  en la frontera turco-siria y en la propia Turquía, en la que habitan cerca de 20 millones, con las excusas de actuar contra las fuerzas desestabilizadoras del Estado Islámico y contra el régimen sirio de Al-Assad, igualmente genocida. A propósito, recordemos que fueron los “jóvenes turcos” los que llevaron a cabo el genocidio armenio en 1915.   Por último, Turquía se fortalece como potencia regional y como aliada del imperialismo.  ]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.

<div class="ssba ssba-wrap">

</div>
