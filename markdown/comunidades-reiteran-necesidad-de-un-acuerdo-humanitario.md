Title: Comunidades siguen clamando por un acuerdo humanitario
Date: 2020-11-23 22:26
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: Acuerdo Humanitario, Autodefensas Gaitanistas de Colombia, Disidencias de las FARC, Ejército Nacional
Slug: comunidades-reiteran-necesidad-de-un-acuerdo-humanitario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Comunidades.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: Comisión JyP

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"left"} -->

A través de un[nuevo comunicad](https://www.justiciaypazcolombia.com/comunidades-negras-indigenas-campesinas-piden-a-presidente-duque-y-todos-los-actores-armados-un-cese-de-fuegos-2/)o que precede esfuerzos similares, más de 110 comunidades y procesos de diferentes regiones del país expresaron a los actores armados legales e ilegales que hacen presencia en sus regiones, la necesidad de establecer un ceso al fuego en el territorio y del tal modo reforzar los esfuerzos por la consecución de la paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por medio de la carta las comunidades expresaron sus demandas en cinco puntos claves, dando prioridad al cese de todas las hostilidades entre los actores armados exaltando el valor de la vida, el detener todo tipo de acción que atente contra la población civil y las comunidades, solicitándoles les permitan hacer uso de sus sistemas de autoprotección. [(Lea también: Tribunal ordena a Duque responder a comunidades que exigen cese al fuego en sus territorios)](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Piden a su vez a dichos actores que se abstengan de hacer presencia en los espacios de vida familiar y comunitaria como lo son caseríos, escuelas y centros de abastecimiento y que se detenga el reclutamiento de menores de edad al interior de dichas organizaciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para concluir piden a dichos actores armados, adopten las medidas para que en sus filas se prevenga el contagio de Covid-19 y del tal modo tampoco se afecte a las poblaciones que conviven en dichos espacios en los que hacen presencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con anterioridad, las comunidades firmantes se habían dirigido a la Fuerza Pública pidiéndoles «actuar con el respeto de los DD.HH, el derecho humanitario y el derecho a la paz», con respecto a las continuos señalamientos y acciones por parte del Ejército y la Policía en contra de los procesos comunitarios, en particular hacia los operativos de erradicación forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el mes de agosto, desde los territorios más afectados por la violencia se había hecho una petición a las Fuerzas Militares, que han apoyado la erradicación forzada en los territorios con el fin de suspender sus operaciones y decretar un cese al fuego por 90 días y así acogerse al pronunciamiento del Secretario General de Naciones Unidas, Antonio Guterres de un alto al fuego mundial en medio de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la solicitud no se ha respondido de manera efectiva y congruente, las comunidades han manifestado la esperanza de que el presidente Duque, presidente como comandante en jefe de las fuerzas armadas responda a su solicitud. [(Lea también: ELN propone misión de verificación para cultivos de uso ilícito)](https://archivo.contagioradio.com/eln-propone-mision-de-verificacion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El mismo llamado se ha hecho no solo a aquellos que no se acogieron al Acuerdo de Paz de la Habana, como los frentes Carlos Patiño, Dagoberto Ramos y la columna móvil Jaime Martínez. De igual forma se invitó a grupos que hacen presencia en los territorios como las autodenominadas Autodefensas Gaitanistas de Colombia, La Empresa y La Local en Buenaventura y a los Comandos de la Frontera.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque expresan su comprensión de cara a la falta de oportunidades y garantías que ha llevado a personas a prestar sus servicios al país en la Fuerza Pública, a vincularse a las disidencias en medio de los asesinatos e incumplimientos en el Acuerdo de Paz de 2016 y a la ausencia de garantías para quienes se acogieron a la ley de Justicia y Paz y hoy regresaron a la ilegalidad, las comunidades les invitaron a recordar que **"nuestro origen rural, campesino, afrodescendiente, indígena es parte de nuestras identidades, igual, nuestras historias ancestrales de violencias".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las comunidades agregan que de darse este paso sería el principio para construir mecanismos de diálogo que permitan una interlocución a disposición de actores políticos tanto nacionales como internacionales. [(Lea también: Comunidades claman por la verdad y por acuerdos humanitarios que alivien el dolor de la guerra)](https://archivo.contagioradio.com/comunidades-claman-por-la-verdad-y-por-acuerdos-humanitarios-que-alivien-el-dolor-de-la-guerra/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
