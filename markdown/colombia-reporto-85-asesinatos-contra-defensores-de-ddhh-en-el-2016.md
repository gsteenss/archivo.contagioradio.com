Title: Colombia reportó 85 asesinatos contra Defensores de DDHH en el 2016
Date: 2017-01-06 16:20
Category: DDHH, Nacional
Tags: Balance de Derechos Humanos, colombia
Slug: colombia-reporto-85-asesinatos-contra-defensores-de-ddhh-en-el-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Defensores-Asesinados-e1477508845527.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Derechos Humanos] 

###### [6 Enero 2017] 

De acuerdo con el último informe de Front Line Defenders, organización que recopila información sobre la protección a los derechos humanos en el mundo, durante el año 2016 los actos de violencia en contra de los defensores de derechos humanos aumentaron a 285 asesinatos en el mundo, de los cuales en Colombia se reportaron 85 casos.

A su vez, el informe lanza una alerta frente a que el **49% de los asesinatos se propiciaron en contra de defensores que trabajaban a favor de los derechos de la tierra, de las comunidades indígenas, del ambiente**, o cuando los defensores de derechos humanos en territorios locales se involucraron en campañas contra multinacioneles, como lo fue el caso del asesinato de Berta Cáceres en Honduras, quién adelantaba una lucha en la defensa del territorio.

De igual forma, el documento evidencia que el continente donde se realiza mayor persecución, estigmatización y actos de violencia es en América, con un reporte de **217 asesinatos a defensores de derechos humanos, siendo Colombia uno de los países con mayor número de asesinatos  con una cifra de 85 defensores**

Para Front Line Defenders “En Colombia, la progresión del proceso de paz y el establecimiento de un alto el fuego definitivo entre el gobierno y las Fuerzas Armadas Revolucionarias de Colombia (FARC), junto con el inicio de conversaciones de paz con el Ejército de Liberación Nacional (ELN) **Aumento del nivel de violencia experimentado por los defensores de los derechos humanos**” agregando que aún pese a que se está llevando este proceso, **13 defensores de derechos humanos fueron asesinados en menos de tres semanas. **Le puede interesar: ["Asesinatos de líderes son práctica sistemática: Somos Defensores"](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/)

Y de acuerdo con la información recolectada la organización expresó que “Organizaciones locales informaron que estos y otros ataques fueron llevados a cabo por grupos que intentaron descarrilar o posponer el proceso de paz. Otros de los países americanos que se encuentran reportados con las cifras más altas de violencia contra defensores de derechos humanos son **Brasil con 58 casos de asesinato, Honduras con 33, México con 26, Guatemala con 12 y Perú que reporto un solo caso.**

Para finalizar el documento generó una serie de recomendaciones a diferentes países para avanzar en la defensa de los derechos Humanos en el mundo, entre ellas pide a los gobiernos que creen medidas y programas que no estigmaticen las labores de los defensores de derechos humanos, a su vez insisten en que se necesitan hacer políticas públicas más fuertes que eviten que los intereses de multinacionales u otro tipo de agentes se relacionen con el gobierno. Le puede interesar: ["Protección a defensores de derechos humanos como garantía de paz"](https://archivo.contagioradio.com/proteccion-defensores-de-derechos-humanos/)

[Colombia reportó 85 asesinatos contra Defensores de DDHH en el 2016](https://www.scribd.com/document/335887989/Colombia-reporto-85-asesinatos-contra-Defensores-de-DDHH-en-el-2016#from_embed "View Colombia reportó 85 asesinatos contra Defensores de DDHH en el 2016 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_84757" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/335887989/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-7kWclaPKIuXDbCsjwrWL&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
