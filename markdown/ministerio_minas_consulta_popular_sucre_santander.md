Title: Consultas populares en Santander son demandadas por MinMinas
Date: 2017-10-02 12:41
Category: Ambiente, Voces de la Tierra
Tags: consulta popular, El Peñón, Ministerio de Minas y Energía, Santander, Sucre
Slug: ministerio_minas_consulta_popular_sucre_santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: revistaccesos] 

###### [2 Oct 2017] 

[Nuevamente el Ministerio de Minas ha decidido interponer una acción de tutela en contra de uno de los resultados de las consultas populares en Santander. Se trata de la votación en la que los habitantes de Sucre,  Santander, le dijeron **No a la minería en su territorio con más de 3000 votos.**]

Así lo denunció Maurio Mesa, integrante de la Corporación Compromiso, quien asegura que no es una situación nueva en ese departamento, donde el gobierno **también entuteló la decisión de los pobladores del municipio de Jesús María,** donde se llevó a cabo la consulta el pasado 17 de septiembre.

"La gente estaba muy entusiasmada. Fueron 3016 votos por el NO a la minería en el municipio de Sucre, el alcalde y los gremios optaron por esta decisión para proteger la economía del municipio que se sostiene a base de las actividades agropecuarias, como la producción de mora y la ganadería", señala Mesa.

### **Argumento del MinMinas** 

Pese a ese mandato, argumentando la violación al derecho fundamental al trabajo, el Ministerio de Minas ha interpuesto una acción de tutela ante el Consejo de Estado, desconociendo la postura del Tribunal Administrativo de Santander, que ya había dado el aval de constitucionalidad de la pregunta de la consulta a la que respondieron este domingo los habitantes de Sucre.

"El Ministerio de Minas ya puso una tutela  abogando derechos fundamentales como el derecho al trabajo de ingenieros de minas y de petróleo. **Le contestamos al Ministerio que también nosotros protegemos el derecho al trabajo de los campesinos** y que también los señores ingenieros y las empresas podían haber hecho compaña por el sí, pero no quisieron hacerlo", expresa el lider ambientalista.

En ese sentido, este martes la comunidad prepara un equipo jurídico para sustentar esa respuesta a la acción del ministerio. "Seguramente todas las consultas populares en el país serán objeto de tutelas, pero eso es una batalla jurídica y el debate que se debe dar para que se de cuenta que se trata de acciones que están bajo la constitución".

De hecho, el abogado Rodrigo Negrete **expresa que es mejor que existan las demandas necesarias, **justamente para aclarar que se trata de decisiones amparadas por la propia Constitución, ya que son varias las veces que la Corte se ha pronunciado sobre la minería, dándole facultades a los concejos, al alcalde y avalando las consultas populares, de forma que se garantice la autonomía territorial y la participación de las comunidades.

### **Se prepara El Peñón** 

L**a consulta popular se realizará el próximo 5 de noviembre y necesitará un umbral de 1.600 votos**, allí hay 12 títulos mineros, todos para la explotación de carbón. (Le puede interesar: ["Hay que descarrilar la locomotora minera: Ambientalistas"](https://archivo.contagioradio.com/hay-que-descarrilar-la-locomotora-minera-ambientalistas/))

Mesa afirmó que el proceso para que los tres municipios decidieran hacer las consultas populares es producto del trabajo de la iglesia por la defensa del territorio y de las comunidades organizadas, “las asociaciones de mora, de guayaba, de bocadillo beleño, los comités de profesores y las ongs de Bogotá, han estado participando y asesorando en el camino”.

<iframe id="audio_21217796" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21217796_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
