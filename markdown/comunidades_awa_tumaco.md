Title: Comunidades Awá en Tumaco: En medio del abandono estatal y la militarización
Date: 2017-10-12 15:50
Category: DDHH, Nacional
Tags: Awá, masacre Tumaco, nariño
Slug: comunidades_awa_tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/awá-e1507841393719.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Onic] 

###### [12 Oct 2017] 

[La masacre del pasado 5 de octubre en Tumaco, evidencia la difícil situación en la que viven las comunidades indígenas y campesinas de la zona. Puntualmente los Awá han denunciado la militarización de su territorio. El lugar donde fueron asesinados varios campesinos e indígenas, actualmente se encuentra bajo control de la Policía Antinarcóticos, pese a que hace parte del resguardo de los indígenas.]

[La comunidad reporta la pérdida de la vida de dos de sus integrantes en los hechos del jueves. Los hermanos Jaime Guadalupe País Guanga y Alfonso Taicus Guanga. Pero además de ello, según aseguran, **el perímetro que hoy ocupa la policía tras la masacre, se trata de un lugar de reserva para la comunidad.**]

["La fuerza pública, está donde tenemos la reserva del resguardo. Donde tenemos para hacer cacería, para curar las enfermedades con las plantas medicinales, por eso no queremos que nos sigan talando el bosque, porque si lo hacen, no tenemos cómo cazar, ni como buscar la medicina; por ejemplo, si se presenta una mordedura de culebra", expresa Alirio Rodríguez, fiscal de la comunidad Awá de Piedra Sellada.]

[Y es que la jurisdicción indígena sobre ese territorio se evidencia con lo establecido en la resolución 015 del 24 de mayo de 1996. La zona conocida como Piedra Sellada Yarumal comprende 2281 hectáreas, y de acuerdo con el Ministerio del Interior contempla un territorio donde viven 255 integrantes de esa comunidad, aunque según los indígenas, son 350.]

### **La respuesta de las autoridades** 

["La fuerza pública dice que aquí no hay un resguardo, y no respetan. La poca comida que tenemos sembrada la cogen, señalan a los indígenas, y por eso no queremos que ningún grupo armado legal o ilegal esté en nuestro territorio", señala Mario Pai, líder de la comunidad.]

[Los habitantes de Piedra Sellada, ya no viven tranquilos tras los hechos del 5 de octubre. Además de que la zona que destinan como reserva se encuentra militarizada, constantemente helicópteros de la fuerza pública sobrevuelan el territorio que habitan, por lo que temen que en cualquier momento una operación de erradicación forzada ponga en riesgo la vida de la comunidad, y pueda repetirse un episodio como el que vivieron la semana pasada.]

### **Una masacre anunciada** 

[Los indígenas cuentan que días antes de que sucediera la masacre, la comunidad quiso hacer ver a los policías que estaban en una área que se encuentra bajo jurisdicción indígena. Según el Fiscal de los Awá, el primero de octubre, unos 70 indígenas salieron hacia ese lugar a aclarar la situación, pero la respuesta de **la policía fue atacarlos y lanzar gases lacrimógenos. En el marco de esa acción, tres personas salieron heridas.**]

[Al día siguiente **"le hablé a ellos, bajaron las armas e intentamos explicarles",** cuenta Rodríguez. Fue así como ese dos de octubre, los agentes pidieron los documentos que evidencian que ese territorio es de propiedad indígena, y así fue como el 3 de octubre los nativos llevaron la resolución que certifica que son sus territorios.]

[No obstante, como lo relata Alirio Rodríguez, una comisión de indígenas decidió volver el 4 de octubre al ver que la policía no se retiraba, pero la respuesta de la Fuerza Pública fue que no se trataba de un área que hiciera parte del resguardo, así que allí permanecieron. Situación que desencadenó la masacre, en la que no participaron directamente los indígenas; solo unos pocos estuvieron en el momento preciso cuando empezaron a caer las personas muertas y heridas, entre ellas, de acuerdo con las autoridades nativas, perdieron la vida 4 indígenas, dos Awá y dos Nasa.]

[Ante dicha situación, el subsecretario de la gobernación de Nariño, Italo Pantoja, quien ha seguido de cerca las preocupaciones de las comunidades en ese territorio, tras los hechos del pasado jueves, manifiesta que es necesario que se logren acuerdos, mediante los cuales se pueda]**garantizar el control del Estado, pero a la vez se respete el territorio y los derechos de los indígenas. Pantoja, manifiesta que lo esencial es “emitir **[oportunamente las alertas tempranas y hacer presencia estatal”.]

### **Otras problemáticas que afrontan los Awá** 

[Además de la constante presencia de la Fuerza Pública; en el marco de la Misión de verificación en la que estuvieron la Asociación Minga, Somos Defensores, la gobernación de Nariño y delegadas del  Ministerio del Interior y de la Vicepresidencia; los indígenas mostraron su preocupación por el abandono estatal histórico de la región, que hoy tiene a las niñas y niños sin condiciones dignas para la educación, “No se tiene respaldo del ministerio de educación", dice Pai.]

[También se encuentran sin un centro de salud para la comunidad. "No hay un centro de salud, algunas mujeres embarazadas y sus bebés han muerto”, expresa Mario Pai. La solución que ha visto la población es trasladarse al Ecuador a buscar atención medicinal, sin embargo, en medio de esa travesía **este año han muerto dos bebés mientras intentaban llevarlos a un centro hospitalario.**]

[Por otra parte, si bien se ha firmado la paz con las FARC, las secuelas de la guerra siguen persiguiendo a los nativos. El líder indígena relata que el pasado mes de febrero, un joven pisó una mina antipersonal y murió. Meses atrás, la misma comunidad encontró cuatro artefactos explosivos cercanos a la escucha de la comunidad, y aunque se dio aviso a las autoridades, debió ser la misma comunidad la que tuvo que desactivarlos.]

[Tras la masacre, lo único que tienen claro los indígenas, es que no van a firmar ningún acuerdo de sustitución colectiva, pues no le creen al Estado, y su vez, aseguran que se encuentran en una encrucijada, al no solo padecer la presencia de la policía y los militares, sino que además han surgido cerca de 10 grupos armados ilegales que mantienen en constante zozobra a la comunidad. **“Hemos puesto muertos para que se pueda reconocer esos títulos colectivos, pero no queremos poner más muertos en tiempos de paz”**.]

*\*Nota periodística realizada con apoyo de Somos Defensores y la Asociación Minga*
