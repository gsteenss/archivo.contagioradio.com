Title: Condena a Jorge Noguera es tardía pero ratifica la existencia de los crímenes del DAS
Date: 2017-09-12 15:02
Category: DDHH, Nacional
Tags: alvaro uribe velez, chuzadas, das, Jorge Noguera, Maria del Pilar Hurtado
Slug: victimas-del-das-no-esperan-mucho-de-la-comision-de-acusacion-de-la-camara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/fotoilustracion_chuzadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: zonacero] 

###### [12 Sept. 2017] 

Con pocas esperanzas recibieron **Alirio Uribe, representante a la Cámara y la periodista Claudia Julieta Duque** la decisión de la compulsa de copias para que se adelanten investigaciones en contra de Álvaro Uribe a propósito de la condena de 7 años contra el ex director del DAS, Jorge Noguera por el caso conocido como las Chuzadas, en el que se realizaron seguimientos, torturas y amenazas contra defensores de derechos humanos, periodistas y políticos de oposición al gobierno.

La Corte Suprema de Justicia será quien envié a la Comisión de Acusación de la Cámara de Representantes varias pruebas para **determinar si el ahora senador, Álvaro Uribe Vélez también ordenó la persecución**, en el marco de lo que se conoció como la política de ‘Seguridad Democrática’.

“Esta sentencia nos da una vez más la razón de lo que siempre dijimos y es que **efectivamente el DAS se había vuelto un aparato criminal que estaba persiguiendo a muchísima gente**, entre ellos el grupo G-3 persiguiendo a los defensores de derechos humanos. Esta sentencia corrobora esa persecución”.

En la actualidad hay cerca de 20 ex funcionarios condenados además de María del Pilar Hurtado ex directora del DAS, Bernardo Moreno, secretario de la presidencia de Álvaro Uribe y altos directores del DAS. Le puede interesar: [Actividades criminales del DAS a punto de prescribir](https://archivo.contagioradio.com/a-juicio-exdectectives-del-das-35421/)

“Nosotros siempre hemos denunciado que Álvaro Uribe era la persona que había organizado el DAS. Recordemos que sus jefes de seguridad de la presidencia, los generales que estaban a su servicio hoy están condenados”.

### **¿Qué esperar de estas investigaciones?** 

Dice Alirio Uribe que no confía mucho en los avances que puedan tener las investigaciones que se abran en contra del senador del Centro Democrático Álvaro Uribe, pues hace dos años el senador compareció a la Comisión de Acusaciones y no pasó nada. Le puede interesar: [Fiscalía debe investigar los crímenes de lesa humanidad cometidos por el DAS](https://archivo.contagioradio.com/victimas-del-das/)

“Yo me imagino que esta compulsa de copias, por razones obvias, se va a acumular a esa investigación, entonces **yo no auguro ningún éxito allí. Pero recordemos que la Corte Penal Internacional – CPI- tiene la información** de todo lo que pasó con el DAS y la persecución y también la Comisión Interamericana y confío más en las instancias internacionales” recalcó Alirio Uribe.

Caludia Julieta Duque, periodista víctimas del DAS asegura que la determinación de la Corte Suprema es tardía y que esa demora ha contribuido en la consolidación de la impunidad que ha rodeado este caso.

**“Me parece tardía la decisión de la Corte Suprema y me parece que la Comisión de Acusaciones de la Cámara ha dejado en el limbo** esas investigaciones precisamente para garantizar impunidad del expresidente que hoy funge como senador y que es una vergüenza que continúe libre emitiendo un discurso lleno de odio, cuando debería responder ante la justicia por estos actos”. Le puede interesar: ['ChuzaDAS' confirmaría "cacería criminal" durante gobierno Uribe](https://archivo.contagioradio.com/chuzadas-confirmaria-caceria-criminal-durante-gobierno-uribe/)

### **Noguera tiene una condena menor que sus funcionarios a pesar del control que ejercía** 

Además, Uribe y Duque manifiestan que **la condena a Noguera no es suficiente y que podría catalogarse como inconsistente,** puesto que a funcionarios subalternos de Noguera les impusieron penas de 10 años y a él una de 7 años.

<iframe id="audio_20829338" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20829338_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
