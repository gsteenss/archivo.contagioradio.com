Title: Consejo Superior impide elección democrática de rector de la U Distrital
Date: 2016-04-18 16:17
Category: Movilización, Nacional
Tags: asamblea permanente u distrital, Universidad Distrital
Slug: consejo-superior-impide-eleccion-democratica-del-nuevo-rector-de-u-distrital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Consejo-Superior-U-Distrital.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UD ] 

###### [18 Abril 2016 ]

Desde el pasado viernes estudiantes, profesores y trabajadores de la Universidad Distrital decidieron entrar en asamblea permanente, en protesta ante la **decisión del Consejo Superior de designar al nuevo rector**, desconociendo que en 2014 se había acordado que la dirección de la institución se aprobaría mediante voto popular.

Según Lina Mora, representante estudiantil de la Facultad de Ciencias y Educación, la decisión del Consejo **es "antidemocrática", porque impide que la comunidad universitaria participe de la elección del rector**, teniendo en cuenta que en el Consejo sólo hay dos representantes de los estudiantes y de los docentes, a quienes no les han aprobado la credencial que les permitiría votar.

Además de desconocer el acuerdo pactado en 2014, el Consejo Superior, en los últimos seis años, ha implementado políticas negativas que han provocado una [crisis presupuestal](https://archivo.contagioradio.com/con-triatlon-por-el-derecho-a-la-educacion-vuelven-los-estudiantes-a-las-calles/)en la Universidad con serias consecuencias en su infraestructura y prácticas de investigación. Así mismo, ha desatendido los **problemas de corrupción que han sido denunciados desde 2013**, y en los que se vio involucrado el antiguo rector Roberto Vergara.

De acuerdo con Mora, **Vergara robó dinero del [presupuesto de la Universidad](https://archivo.contagioradio.com/ser-pilo-paga-otra-mermelada-del-gobierno-nacional/) en 2013**, año en el que la institución estuvo paralizada cuatro meses, y en el que Carlos Javier Mosquera asumió como rector encargado. Recientemente Mosquera presentó ante el Consejo de Bogotá una iniciativa para solicitar mayor financiación, pues llevan **años esperando obras de infraestructura y soluciones a la crisis**.

En este momento estudiantes y profesores se reúnen para estudiar los diferentes decretos, resoluciones y acuerdos, pues hasta que el Consejo Superior no derogue la decisión de elegir al nuevo rector por designación, no reanudarán las clases.

<iframe src="http://co.ivoox.com/es/player_ej_11210770_2_1.html?data=kpafk5Wbe5Ghhpywj5WaaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5ynca3dz8aYr9TWpYampJC%2Fx9XWqdTZz9nO0NnJb6bn1drRy8bSuMrgjLqxj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
