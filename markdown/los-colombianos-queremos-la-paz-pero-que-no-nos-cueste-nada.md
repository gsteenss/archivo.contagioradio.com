Title: “Los colombianos queremos la paz, pero que no nos cueste nada”
Date: 2015-05-02 10:25
Author: CtgAdm
Category: Nacional, Paz
Tags: Actividades ilegales del DAS, Alvaro Uribe, Encuesta Gallup, Fernando Giraldo, Juan Manuel Santos, medios de comunicación
Slug: los-colombianos-queremos-la-paz-pero-que-no-nos-cueste-nada
Status: published

De acuerdo al politólogo Fernando Giraldo, la encuesta Gallup de esta semana, demuestra la maleabilidad de la opinión de las personas en Colombia y que la gestión del gobierno se mide por el proceso de paz, justo porque la paz fue la bandera para la reelección, por ello lo que sucede en el proceso afecta directamente la percepción sobre la gestión de Juan Manuel Santos.

El problema, señala el politólogo, es que el gobierno no ha sido estable, ha tenido altibajos y no ha hecho una campaña permanente de pedagogía sobre el propio proceso de paz y las ventajas que un escenario diferente aportará a la realidad colombiana. “Es muy posible que después de esta encuesta de nuevo veamos una apuesta mediática para mejorar la favorabilidad”

**[“Los medios de comunicación no solo informan sino que opinan, juzgan y condenan”]**

Para Giraldo hay algo que es necesario decir en Colombia, los medios de información se están convirtiendo en un actor político que están defendiendo unos intereses particulares, pasan de ser simplemente medios masivos a ser empresas mediáticas orientadas políticamente. “No solo colocan una agenda sino que tienden a incidir en lo que la gente piensa, sesgan en la manera como informan”,  obligan a hablar los temas específicos que sean los que más les parezcan pertinentes.

**[“Los colombianos queremos la paz, pero que no nos cueste nada”]**

Episodios como los escándalos de las FFMM, las operaciones ilegales del DAS, la vinculación de personas y funcionarios cercanos al gobierno de Álvaro Uribe involucrados en diversos crímenes, escándalos como el de la Corte Constitucional, la corrupción para la definición de tutelas y la vinculación con el desplazamiento forzado del magistrado Pretelt, que además sigue en sus funciones; y la pasividad de la sociedad frente a ello demuestra que en Colombia se le perdona a todo el mundo de todo, menos a las FARC.

Señala el analista que en ninguna parte del mundo la paz se ha logrado encontrar sin que haya una mínima dosis de impunidad, si se quiere encontrar la paz, primero debe haber una dosis de impunidad, ningún conflicto en el mundo se ha acabado exigiendo que la contraparte pague todos los daños militares, materiales y sociales, si eso es lo que queremos los colombianos “no se va a alcanzar la paz”

**["En Colombia si hay enemigos de la paz"]**

El hecho de creer que los seres humanos tienden a ser buenos, no podemos creer que no se actúe mal, en Colombia si hay enemigos de la paz, pero si logramos reducirlos es mejor, más rápido encontraremos la paz, esto nos ahorra vidas, para ver si las élites dejan de buscar un chivo expiatorio y se dediquen a buscar mejores condiciones sociales

Los ciudadanos tenemos que entender que hay unos que promueven la guerra y otros que se ven obligados a entrar en la guerra porque no tienen alternativa. Por ello Giraldo califica el reciente encuentro de Álvaro Uribe con el Super Ministro Martínez como una oportunidad de ratificar que en Colombia “si hay enemigos de la paz”.

[encuesta-opinion-publica-gallup-abril-contagio-radio.pdf](https://www.scribd.com/doc/263678905/Gallup-106-Abril-2015 "View encuesta-opinion-publica-gallup-abril-contagio-radio.pdf on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_43523" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/263696463/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Jk7oHqWIOZIsjnpJR0SU&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="1.3323485967503692"></iframe>
