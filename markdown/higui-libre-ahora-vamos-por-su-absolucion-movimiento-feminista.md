Title: ¡Higui libre, ahora vamos por su absolución!: Movimiento feminista
Date: 2017-06-13 15:23
Category: Libertades Sonoras, Mujer
Tags: Argentina, feminicidio, feminismo, Higui, Lesbianas, libertad para Higui
Slug: higui-libre-ahora-vamos-por-su-absolucion-movimiento-feminista
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Higui.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia Presentes] 

###### [13 Jun. 2017] 

Un 16 de octubre de 2016 **Eva Analía de Jesús, conocida como “Higui” caminaba por las calles de Buenos Aires cuando 10 hombres la abordaron,** la maltrataron con golpes, palabras, la escupieron y uno de ellos la violó. Le gritaban que “se lo merecía… por lesbiana”. Tirada en el piso, “Higui” sacó fuerzas y desde lo más profundo de su ser, con una navaja apuñaló al agresor. Lo mató. La Policía la culpó de homicidio simple y la llevó a la cárcel. La acusaron de asesina, pese a haberlo hecho en legítima defensa.

Hoy, después de casi 8 meses encerrada, en medio de un proceso que ha sido catalogado como irregular por la falta de garantías, **las organizaciones de mujeres y feministas gritan al unísono ¡Higui libre!** pues salió de la cárcel con libertad juramentada. Le puede interesar: [No hay un panorama claro frente a derechos de las mujeres en Argentina con Macri](https://archivo.contagioradio.com/no-hay-un-panorama-claro-frente-a-derechos-de-las-mujeres-en-argentina-con-macri/)

**Según Milena Sánchez de la Colectiva Ni Una Menos en Argentina,** esta noticia llena de felicidad al movimiento feminista “nos han devuelto la alegría que por suerte todavía no nos han quitado. **La alegría que es saber que Higui ya está libre y que va a esperar el juicio en libertad** como lo debía estar siempre y no desde donde nunca debió haber estado que fue encarcelada por una *violación correctiva*”.

Para la feminista, lo que intentaron hacer con Higui no fue solamente lo ocurrido ese día, sino que **esa misma “patota de varones” que ya la conocían, habían incendiado su casa 6 meses atrás,** asesinado al perro de Higui como parte del “proceso correctivo que llaman los agresores, que se hace para sacarle lo lesbiana”.

### **La movilización también hizo su parte** 

Adicional a la alegría y al asomo de justicia que significa esta decisión en el caso de Higui, **esto es resultado de la lucha del movimiento de lesbianas y mujeres feministas de Argentina,** en conjunto con organizaciones sociales y políticas que unieron fuerzas para dar visibilidad al caso y hacer el acompañamiento jurídico y psicológico que se requería.

Así mismo, para Sánchez, **la movilización en las calles hizo una fuerte presión que dio como resultado la libertad de Higui** y además la fuerza para el movimiento feminista dado que califican esa decisión como “un resultado colectivo”. Le puede interesar: [70 mil mujeres argentinas contra el patriarcado y el ajuste](https://archivo.contagioradio.com/70-mil-mujeres-argentinas-contra-el-patriarcado-y-el-ajuste/)

### **Higui tendrá que ser reparada por el poder judicial** 

Ante las amenazas de las familias de los agresores y el riesgo que tiene Higui al estar en libertad, el movimiento feminista ha asegurado que bajo la premisa de “queremos estar vivas y en libertad”, seguirán trabajando para que el “sistema patriarcal, que es el que nos condena a que las mujeres vivamos con miedo, que no podamos ocupar el espacio público al igual que lo ocupan los varones, no nos siga matando”.

En cuanto al caso de Higui **seguirán exigiendo que sea reparada por todo el daño causado por el poder judicial** “no hay que olvidar que la Policía en Argentina tiene un gran prontuario de corrupción, de misoginia, de fobia, de machismo y por supuesto de represión y eso también se ve hoy frente a un hecho de violencia como este” añade Sánchez.

Además, otros hechos que harían que el poder judicial se vea en la obligación de reparar a Higui es que **en el momento que se le toma la declaración, ella no tiene ninguna defensora oficial**, tampoco le hicieron una revisión médica, ni la ciudaron. Le puede interesar: [El feminicidio de Micaela García moviliza Argentina](https://archivo.contagioradio.com/39025/)

### **Ahora la consigna es absolución** 

Tan pronto se conoció la decisión de la excarcelación para Higui, el movimiento feminista volcó su trabajo a pedir la absolución “esperamos la absolución por supuesto mientras espera en libertad su juicio. La caratula no cambió de nombre, sigue siendo homicidio simple por lo cual **la lucha la seguiremos dando, considerando la legítima defensa” añadió Sánchez.**

Así mismo aseguró que la lucha que tiene el movimiento hoy es la exigencia al Estado que no solamente fue ausente, sino que se hizo presente mediante políticas misóginas y revictimizantes con Higui. Le puede interesar: [Cuando las mujeres opinamos](https://archivo.contagioradio.com/cuando-las-mujeres-opinamos/).

<iframe id="audio_19247442" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19247442_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Cifras de feminicidios en Argentina** 

![Higui](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/DBLGSvTXoAA1P-6.jpg){.alignnone .size-full .wp-image-42188 width="720" height="1018"}

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
