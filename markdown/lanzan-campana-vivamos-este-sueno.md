Title: Lanzan campaña "Vivamos este sueño"
Date: 2017-04-26 18:39
Category: yoreporto
Slug: lanzan-campana-vivamos-este-sueno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Logo-Contagio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CPDH] 

###### [26 Abr 2017] 

El Comite Permanente para la Defensa de los Derechos Humanos, lanzará hoy su campaña !Vivamos este sueño!, **iniciativa que busca aportar a la construcción de una cultura de paz**, explicando el Sistema Integral de justicia y trabajando la reconstrucción de confianza entre los Colombianos.

La campaña se **desarrollará con intervenciones artísticas en diferentes espacios, talleres y la posibilidad de que organizaciones sociales se unan a esta para llegar a más personas** en el país. Esta propuesta además, tiene un proyecto sobre documentación de casos con víctimas que será presentado a la Comisión de la Verdad y a la Jurisdicción Especial para la Paz.

"Buscamos hablar de que la paz no es el fin último, sino que nos permite generar un escenario propicio para que los colombianos vivan ese sueño que de pronto aplazaron por condiciones propias del conflicto" señaló Jorge Acuña, jefe de prensa del CPDH.  Le puede interesar: ["Se lanza Congreso Nacional de Paz en Colombia"](https://archivo.contagioradio.com/congreso-de-paz/)

El lanzamiento del evento será a las 4:30 pm en el  Teatro El Dorado ECCI. Calle 17 No. 4-64 y contará con la participación de **Humberto de La Calle, Iván Márquez, Alejandra Gaviria y Bertha Fries.** Le puede interesar: ["Guernika premio el Acuerdo de Paz en Colombia"](https://archivo.contagioradio.com/39720/)

Invitación

Durante un largo periodo en nuestra historia, no hemos tenido el tiempo para sentarnos tranquilamente y hablar de nuestros Sueños o hacerlos realidad. Por esto es hora de invitarles a ustedes sin distinción alguna, para escucharnos y mirarnos a los ojos, este Jueves 27 de abril al lanzamiento de la campaña "Vivamos este Sueño", que realizará el  Comité Permanente por la Defensa de los Derechos Humanos- CPDH-.

Una Colombia que construye para la Paz, merece territorios que permitan hacer realidad los Sueños aplazados. ¿... y si juntos hacemos realidad nuestros sueños...?

Los Esperamos! jueves 27 de abril, 4:30 pm. Teatro El Dorado ECCI. Calle 17 No. 4-64; contaremos con la participación de **Humberto de La Calle, Iván Márquez, Alejandra Gaviria y Bertha Fries.**

ENTRADA LIBRE Y SIN INSCRIPCIÓN PREVIA

<iframe id="audio_18378225" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18378225_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
