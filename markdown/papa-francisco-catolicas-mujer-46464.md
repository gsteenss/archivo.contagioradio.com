Title: El primer paso y una carrera por los derechos de las mujeres: Católicas tras visita de Francisco
Date: 2017-09-11 14:53
Category: Mujer, Nacional
Tags: iglesia católica, Jerarquia de la Iglesia, mujeres, Papa Francisco, Sumo Pontìfice, visita del Papa
Slug: papa-francisco-catolicas-mujer-46464
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Mujeres-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [11 Sept. 2017] 

“El Papa nos dejó desafiadas, nos retó, no solo a las mujeres sino a la sociedad colombiana” aseguró **Sandra Mazo directora de católicas por el Derecho a Decidir- CDD- **luego de la visita del sumo pontífice a Colombia, quien añadió que era algo de esperarse luego de seguir los diversos discursos en los que entrega mensajes desafiantes a la jerarquía de la Iglesia Católica que incluyen el cuidado por el ambiente, la igualdad y la opción por los más pobres.

Para Mazo, la jerarquía colombiana no ha estado escuchando de manera atenta y profunda los cambios y apertura que el Papa pretende hacer a la Iglesia **“esperamos que la jerarquía sepa leerlos, interpretarlos y estar a tono con los discursos que el Papa** nos está planteando de cambios necesarios”.

### **Al Papa le faltó avanzar en el discurso hacia las mujeres** 

Otro de los temas en los que las mujeres estaban muy atentas era el discurso que el Papa diera a propósito del papel de ellas en la vida pública y como poseedoras de derechos. Manifiesta Mazo que en ese sentido hay emociones encontradas puesto que, sí se ha reconocido a las mujeres, pero dentro de la Iglesia.

“Hay asuntos propiamente que tienen que ver con derechos sobre todo a decidir donde la estructura en general no cambia, **se sigue mostrando a la mujer en su labor de la maternidad como madre protectora de la familia y ciertos valores.** Todavía se nos sigue poniendo como cuidadoras”.

Por lo que para la directora de Católicas por el Derecho a Decidir a la jerarquía todavía le falta avanzar y plantear el tema del cuidado y la familia como un asunto compartido entre hombres y mujeres.

“Es de destacar que el Papa dijo que las mujeres tenemos que cumplir un papel más activo, más dinámico, más protagónico y reconoce que las mujeres somos las que mantenemos y sustentamos la Iglesia Católica”.

### **Las mujeres aceptan el reto de construir una nueva Iglesia Católica** 

Valores como la justicia, el perdón, el reconocimiento, la reconciliación fueron citados por el Papa en sus discursos y dejados como retos para la sociedad colombiana y para la jerarquía de la Iglesia Católica. Le puede interesar: [El acoso contra mujeres en las universidades, un flagelo que no cesa](https://archivo.contagioradio.com/46181/)

“Si algo hemos hecho las mujeres es empujar, nunca nos hemos quedado quietas esperando que los cambios se den, **siempre hemos sido un motor de cambio, entonces para nosotras el desafío es seguir comandando este tren** y este barco desde los cambios, el asunto es qué tan preparada este la sociedad y la jerarquía católica para cambiar formas de ver el mundo y sobre todo a nosotras. Es un reto muy grande el que planteó el Papa, la inclusión”.

### **La iglesia no puede seguir siendo un obstáculo para avanzar en los derechos de las mujeres** 

Si bien la visita del Papa le alegró a las integrantes de Católicas por el Derecho a Decidir, por todo el ambiente que generó en el país que esperan contribuya al diálogo en temas como los derechos humanos, la justicia social, la reconciliación, las víctimas, para Mazo no se instalaron temas pendientes y deudas históricas de la Iglesia Católica.

“Nuestro llamado y nuestra movilización fue para eso porque **nosotras las mujeres también queremos que se nos pague una deuda histórica** y sentimos que muchas mujeres han acogido este llamado, porque hemos sentido cómo se nos ha discriminado y relegado”. Le puede interesar: [La deuda de la iglesia católica con las mujeres](https://archivo.contagioradio.com/la-deuda-de-la-iglesia-catolica-con-las-mujeres/)

Por último, hizo un llamado a la iglesia católica y a la feligresia para que no se interpongan entre los derechos y las mujeres y de esto modo permita avanzar y apoyarlas para que no se silencien los abusos y las violencias contra las mujeres.

“Ojalá demos el primer paso y muchísimos más, **que emprendamos toda una carrera por el reconocimiento de nuestros derechos”.** Le puede interesar: [Ante llegada del Papa católicas invitan a "dar el primer paso" por los derechos de las mujeres](https://archivo.contagioradio.com/46276/)

<iframe id="audio_20808180" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20808180_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
