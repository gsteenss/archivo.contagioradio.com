Title: Colombia sede de la conferencia "Tierra y Territorios de las Américas"
Date: 2016-08-18 22:31
Category: eventos, Nacional
Tags: Acaparamiento de tierras, economía campesina, territorio
Slug: colombia-sede-de-la-conferencia-tierra-y-territorios-de-las-americas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:palabrasalmargen] 

###### [18 de Agost] 

A partir del próximo 23  hasta el 26 de agosto, se llevará a cabo en Bogotá la Conferencia Internacional  "Tierra y Territorios de las Américas: Acaparamientos, Resistencias y Alternativas", un **escenario que pretende comprender las dinámicas políticas de los procesos de la tierra en América desde el debate y la participación** .

A su vez, el intercambio de experiencias busca identificar las tendencias al interior de los países frente a las formas de **apropiación de tierra, por parte de diferentes actores**, **y su relación con la concentración, el control y el poder político sobre la misma.** Por ende los foros centrales girarán en torno a temáticas como: Dinámicas históricas, estructurales de la concentración y el acaparamiento de tierras y territorio; Estado, poder y tierra en las Américas: acceso y control de la tierra y derechos territoriales; Tierra y territorio, poder político, guerra, conflictos y construcción de paz, entre otros más.

Cabe resaltar que Colombia fue escogida como sede del evento debido al proceso de paz en el que se encuentra que involucra como uno de sus aspectos centrales el problema de la tierra, ya que el **46% de ella se encuentra en manos del 0,4% de la población del país**, mientras que el **80% de la tierra esta dedicada a cultivos extensivos,** y es allí en donde se ha desarrollado el conflicto armado histórico de más de 50 años.

Por otro lado en Colombia, [2.000.000 hectáreas se encuentran en manos de multinacionales y han sido adquiridas a través de procesos fraudulentos](https://archivo.contagioradio.com/cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia/). De acuerdo con el Incoder se han establecido demandas en contra de [9 empresas por acumulación irregular de 24 mil hectáreas](https://archivo.contagioradio.com/las-11-empresas-que-se-oponen-a-la-restitucion-de-tierras-segun-estudio-de-forjando-futuros/). A su vez, ante la Contraloría hay cuenta de más de 120 mil hectáreas denunciadas por el senador Iván Cépeda de haber sido obtenidas de igual forma. Con este panorama **Colombia es uno de los países con la mayor crisis de acaparamiento de tierra** y obtiene como resultado todos los conflictos que esto genera.

El evento se realizará en las instalaciones de la Universidad Externado y contará con ponentes internacionales como **Catherine Legrand**, docente de la Universidad Mcgill, Daniel **Cáceres**, docente de la Universidad de Córdoba, entre otros,  y  ponentes nacionales como **Francia Márquez** lideresa del movimiento social; **Darío Fajardo**, docente de la Universidad del Externado; entre otros.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
