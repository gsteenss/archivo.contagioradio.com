Title: Bloque Sur denuncia incumplimientos en Zona Veredal de Puerto Asís
Date: 2017-02-05 19:10
Category: Nacional, Paz
Tags: Bloque Sur, FARC-EP, Implementación de los Acuerdos de paz, Puerto Asís Putumayo
Slug: bloque-sur-denuncia-incumplimientos-en-zona-veredal-de-puerto-asis
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/IMG-20170205-WA0057.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [5 Feb 2017] 

Terrenos sin infraestructura, riesgo de epidemias, falta de agua, de electricidad y 4 ex combatientes hospitalizados, fueron las situaciones denunciadas por uno de los voceros e integrantes del Comité Político del Bloque Sur de las FARC, al llegar a la Zona Veredal de Puerto Asís, hasta el momento el comité de monitoreo y verificación **les ha dicho que no tienen una fecha clara de entrega de la infraestructura, debido a demoras de los trámites en la capital.**

Mauricio Gareca, integrante del Comité Político, manifestó que el pasado 2 de febrero llegaron a la Zona Veredal La Carmelita, en Puerto Asís Putumayo, 360 ex combatientes, hombres y mujeres de los frentes 32, 48 y 49 y unidades especiales de la guardia del Bloque Sur, de los cuales 4 tuvieron que ser hospitalizados de manera urgente, **4 mujeres tuvieron trabajo de parto recientemente y otras 10 se encuentran en embarazo. **([Le puede interesar: Continúan incumplimientos del gobierno en Zonas Veredales](https://archivo.contagioradio.com/continuan-incumplimientos-del-gobierno-en-zonas-veredales/))

### El Gobierno incumple lo pactado en La Habana 

Gareca señaló que la situación es “preocupante e indignante”, pues esperaban encontrarse con una Zona Veredal que contará con los servicios básicos, pero han tenido que dormir a la intemperie, **“estamos al sol y al agua, de verdad que estamos en la situación que viven muchas familias desplazadas”,** puntualizó el vocero.

El vocero aseguró que el 4 de febrero se reunieron con delegados del comité de monitoreo y verificación, quienes frente a las denuncias respondieron que los trámites para la compra y entrega de materiales de construcción y contratos de servicios públicos, estaban retrasados en la capital. **Hasta el momento no les han dado una fecha puntual para la entrega de ni inicio de obras.**

Además, Gareca reveló que la situación de seguridad “es bastante precaria”, pues hace aproximadamente dos semanas un líder comunitario fue asesinado y han registrado **pintas de grupos paramilitares “en comunidades aledañas a la Zona Veredal”.** ([Le puede interesar: Paramilitares controlan hasta el transporte de las FARC a Zona Veredal](https://archivo.contagioradio.com/paramilitares-torpedean-construccion-de-zonas-veredales/))

Por último, el vocero del Comité Político hace un llamado al Gobierno Nacional y a los entes de control, para que tomen medidas efectivas, a organizaciones sociales, de derechos humanos y medios de comunicación para que estén atentos y **atentas de la situación presente en buena parte de las Zonas Veredales del país y al aumento de presencia y hostigamientos por parte de paramilitares en las regiones.**

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
