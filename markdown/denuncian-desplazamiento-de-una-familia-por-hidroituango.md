Title: Denuncian el desplazamiento de una familia por explosiones de Hidroituango
Date: 2017-10-12 13:44
Category: DDHH, Nacional
Tags: Antioquia, Desplazamiento forzado, hidroeléctica, Hidroituango, Movimiento Ríos Vivos
Slug: denuncian-desplazamiento-de-una-familia-por-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/ituango-protesta-epm.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [12 Oct 2017] 

El Movimiento Ríos Vivos denunció que una familia que habita en Briceño, Antioquia, **fue desplazada por el riesgo que generan las explosiones** ocasionadas para la construcción del proyecto hidroeléctrico Hidroituango. Manifestaron que los desplazamientos de comunidades ocurren a diario y las familias no tienen garantías para volver a sus territorios.

De acuerdo con Isabel Cristina Zuleta, miembro del Movimiento Ríos Vivos de Antioquia, la familia del señor Manuel de Jesús Bernal, que está compuesta por su esposa y **5 hijos menores de edad**, tuvieron que huir de su lugar de vivienda para proteger su integridad física.

Manifestó que debido a las explosiones que ha realizado el consorcio Mispe, que fue contratado por Empresas Públicas de Medellín (EPM), **el riesgo para las poblaciones ha aumentado** y se han tenido que desplazar. Aseguró que las rocas que quedan, “están destruyendo las viviendas de los campesinos y están poniendo en riesgo la vida de muchas familias”. (Le puede interesar: ["Memoria y resistencia en el Cañon del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

En repetidas ocasiones, la familia del señor Bernal, le ha insistido a la empresa y a la Junta de Acción Comunal que **les ayude a buscar un refugio** o un lugar donde puedan seguir realizando sus actividades. Sin embargo, no han tenido respuesta por lo que decidieron desplazarse al corregimiento de Puerto Valdivia.

El señor Bernal denunció, con ayuda del Movimiento Ríos Vivos, **que no ha podido hacer una declaración de desplazamiento forzado** ante la Personería debido a que allí le manifestaron “que el Estado no desplaza y un proyecto hidroeléctrico no desplaza”.

### **Hay un caso desplazamiento diario por los riesgos que genera Hidroituango** 

Si bien es muy difícil calcular el número de desplazados que ha dejado el proyecto, teniendo en cuenta que **se trata de una construcción que abarca un territorio muy grande**, el Movimiento ha intentado hacer un registro aproximado. De acuerdo con ellos, el desplazamiento de familias, como el que ocurrió con la familia Bernal, se presenta a diario. (Le puede interesar: ["Líneas de transmisión de Hidroituango podrían afectar la salud: Comunidades"](https://archivo.contagioradio.com/comunidades-afectadas-lineas-transmision-hidroituango-denuncian-irregularidades/))

Tienen registrados 45 casos de desplazamiento "asociados al riesgo que generan **las explosiones, los insultos, las humillaciones y las amenazas**" que reciben los pobladores de la sub región norte de Antioquia. Zuleta manifestó que el último caso de desplazamiento registrado ocurrió en Sabana Larga, donde un barquero tuvo que destruir su vivienda “porque un vigilante de EPM le dijo que lo tenía que hacer y si no lo desalojaba con la fuerza pública”.

### **Presencia de grupos armados agrava la situación** 

A la problemática del desplazamiento, se suma la presencia de grupos paramilitares en la región que **ha aumentado la preocupación de las comunidades**. Según ellos, la presión de los grupos armados hace que las personas no se puedan quejar, dificultando el registro de la población que está saliendo de sus territorios. (Le puede interesar: ["El curioso caso de la licencia otorgada a Hidroituango"](https://archivo.contagioradio.com/el-curioso-caso-de-la-licencia-otorgada-a-hidroituango/))

Las organizaciones sociales que trabajan con estas comunidades, han insistido en que el Ministerio de Cultura **declare el barequeo como patrimonio cultural** de la nación para así proteger el trabajo ancestral que realizan los campesinos en el río Cauca.

También, le han exigido al Gobierno Nacional que **garantice el mínimo vital a las familias** que están siendo desplazadas. Zuleta indicó que es necesario que existan “refugios y espacios donde puedan tener las condiciones mínimas de supervivencia”.

Finalmente, Ríos Vivos denunció que la empresa que dirige el proyecto de la hidroeléctrica, ha venido contaminado el río Cauca **cuando arroja material de excavación en sus aguas**. Zuleta manifestó que producto del desplazamiento, “algunos pescadores han tenido que trabajar en los cultivos de uso ilícito puesto que no hay garantías para continuar con la pesca”.

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/campesino-hidroituango.mp4\[/KGVID\]

###### Reciba toda la información de Contagio Radio en [[su correo]

######  {#section .osd-sms-wrapper}
