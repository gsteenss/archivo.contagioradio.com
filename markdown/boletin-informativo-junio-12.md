Title: Boletín Informativo junio 12
Date: 2015-06-12 17:10
Category: datos
Tags: Biciclatón, Carlos Lozano, Día mundial del albinismo, escalamiento del conflicto armado, ICBF, Noticias del día en Contagio Radio, Ruedas por la paz con justicia social, Sindicato Madres Comunitarias, sybil Sanabria, talas humedal la Conejera
Slug: boletin-informativo-junio-12
Status: published

*[Noticias del Día: ]*

<iframe src="http://www.ivoox.com/player_ek_4633990_2_1.html?data=lZuglZ6ddI6ZmKiakpmJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpz87cjZaWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Ante la información que se ha conocido en las últimas semanas, acerca del **escalamiento del conflicto armado** ha dejado decenas de víctimas fatales, tanto en las filas guerrilleras como en las de las fuerzas militares y desastres ambientales con impacto sobre civiles; el analista **Carlos Lozano** asegura que esas **son las consecuencias de la absurda decisión de negociar en medio del conflicto**.

-Durante la mañana de este **viernes**, cerca de **5.000 madres comunitarias se movilizaro**n frente a las sedes del Instituto Colombiano de Bienestar Familiar a nivel nacional, para exigir **se reconozcan sus derechos, por medio del pago de las prestaciones sociales y aportes pensionales** adquiridos tras 28 años de servicio a la institución. **Olinda García, Presidenta del sindicato de trabajadoras al cuidado de la infancia y adolescencia del sistema nacional de ICBF**.

-Con la intención **aportar a la construcción de la paz en Colombia**, estudiantes de universidades públicas y privadas convocan a todos y todas a participar en la biciclatón "**Ruedas por la paz con justicia social**", este sábado 13 de Junio. Habla **Sybil Sanabria** de la Uniiversidad javeriana.

-Según **Gina Díaz**, defensora del humedal "**La Conejera**", la constructora Praga S.A. dio inicio hace dos días a la **tala de algunos árboles seleccionados**, con conocimiento de que 11 de ellos no podían ser derribados. Sin embargo, el día de hoy estos f**ueron cortados incumpliendo con el protocolo establecido**.

-En el marco de la conmemoración del **Día mundial del albinismo**, que se celebra proximo 13 de junio, el pais se suma a las actividades que buscan visibilizar a los y las colombianas que presentan esta **condición genética.** **Diana Sanabria, representante de la Fundación Contrastes - Albinos por colombia** explica como se desarrollará la jornada.
