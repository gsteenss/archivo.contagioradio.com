Title: Las órdenes de Trump que agreden a inmigrantes e indocumentados
Date: 2017-02-04 19:53
Category: DDHH, El mundo
Tags: Donald Trump, indocumentados, inmigrantes
Slug: las-ordenes-de-trump-que-agreden-a-inmigrantes-e-indocumentados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/inmigrantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: OPI] 

###### [5 Feb 2017] 

En su primera semana de mandato, el presidente Donald Trump ha firmado tres órdenes ejecutivas señaladas de **inhumanas, inefectivas, costosas y contrarias a los valores de Estados Unidos**, de acuerdo con la organización Latin America Workin Group Education Fund, además agregan  que han puesto en riesgo a varias poblaciones, incluyendo las familias, niños no acompañados, residentes legales, jóvenes dreamers, comunidades fronterizas y a todas las personas indocumentadas.

El temor de las personas, para la organización Latin America Workin Group Education Fund radica en que aún **no se encuentra muy clara la forma de implementar las 3 órdenes, tanto al interior del país como en las fronteras y en el exterior**, con el procesamiento de refugiados, por este motivo ya se han abierto varios juicios en contra del presidente Donald Trump.

### **Las 3 Órdenes de Trump** 

La primera orden es **“la seguridad fronteriza y a la aplicación de la ley migratoria en el interior”**,  firmada el 25 de enero de este año y por la cual se decreta la construcción del muro a lo largo de la frontera entre Estados Unidos y México, de igual forma se aumentan los centros de detención a lo largo de la frontera y el número de agentes fronterizos a 5.000, hechos que evidencian la **militarización del muro**, a su vez  se podrá hacer la **detención de una persona mientras espera el resultado de su procedimiento de asilo, limitando el derechos al debido procedimiento**.

La segunda orden es la de **“seguridad pública en el interior de los Estados Unidos”** decretada en la misma fecha que la primera y  refuerza la aplicación de la ley migratoria actual, en esta se cambia la categoría de inmigrantes a “prioridad” en deportación, además **cancela la asistencia federal a la ciudades “Santuarios”** que no cooperen con las autoridades de migración federales.

Igualmente, ordena la creación de la **oficia para víctimas de crímenes de inmigrantes con órdenes de deportación**, que publicará semanalmente la lista con los nombres de las personas denunciadas, aumentando la criminalización hacía los inmigrantes. Le puede interar: ["Estas son las primeras víctimas de las medidas de Trump"](https://archivo.contagioradio.com/migrantes-refugiados-naturaleza-palestinos-mujeres-primeras-victimas-las-medidas-trump/)

Por último la tercera orden es “Protegiendo a la Nación de la entrada de terroristas extranjeros a Estados Unidos” firmada el 29 de enero. Esta orden **suspende los programas de admisión de refugiados a Estados Unidos,** restringe la entrada de refugiados de Siria por un periodo indeterminado y niega la entrada de inmigrantes de 7 países por 90 días. En ese sentido solo se aceptarán entre **50.000 a 100.000 refugiados durante este año en Estados Unidos.**

**Desde la implementación de estas órdenes miles de norteamericanos han protestado en diferentes estados** para defender y reclamar por las detenciones a inmigrantes y refugiados, incluyendo a personas con visa o residencia legal. Le puede interesar:["Movimiento Santuario listo para trabajar por los inmigrantes en EE.UU"](https://archivo.contagioradio.com/movimiento-santuario-listo-eeuu-34968/)

Asimismo académicos, líderes religiosos, oficiales locales, fiscales estatales, representantes ante la Cámara y la Comisión Interamericana de Derechos Humanos, han expresado su **preocupación frente a la violación de los derechos humanos que se está cometiendo contra inmigrantes, refugiados e indocumentados** al interior de Estados Unidos o en el momento de ingresar al país.

###### Reciba toda la información de Contagio Radio en [[su correo]
