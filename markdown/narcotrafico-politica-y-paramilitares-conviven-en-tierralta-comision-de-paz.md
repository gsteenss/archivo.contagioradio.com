Title: Narcotráfico, política y paramilitares conviven en Tierralta: Comisión de Paz
Date: 2019-06-28 16:21
Author: CtgAdm
Category: Comunidad, Nacional
Tags: cordoba, Maria del Pilar Hurtado, Paramilitarismo, Tierralta
Slug: narcotrafico-politica-y-paramilitares-conviven-en-tierralta-comision-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Tierralta-comisión-e1561756678606.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @IvanCepedaCast] 

Al visitar **Tierralta, Córdoba,** municipio donde fue asesinada la lideresa social **María del Pilar Hurtado** el pasado 21 de junio, la Comisión de Paz del Senado se reunió con los líderes de la comunidad y pudo establecer, tras dialogar con líderes sociales y autoridades locales que existe una unión entre estructuras criminales y actores políticos que ponen en peligro a los habitantes del municipio y podría significar el regreso de la parapolítica a los territorios.

**La senadora del partido FARC, Victoria Sandino**, natal de este municipio se refirió a la situación que se vive en Tierralta, expresó que los hechos de violencia que la obligaron a salir de su tierra son los mismos que hoy afectan a la comunidad, "nos encontramos con el asesinato sistemático de líderes sociales, personas humildes que se dedican a trabajar y a rebuscarse la vida".  [(Lea también: Asesinan al líder y beneficiario de plan de sustitución Manuel Gregorio González en Córdoba)](https://archivo.contagioradio.com/asesinan-al-lider-social-manuel-gregorio-gonzalez-en-cordoba/)

La senadora destacó que la visita les permitió evidenciar que el asesinato de María del Pilar no se trató de un caso aislado sino que sucede de manera sistemática, **"hay una situación de convivencia entre las fuerzas políticas, grupos paramilitares y grupos del narcotráfico", afirmó.  [(Lea también: Con asesinato de María del Pilar Hurtado, paramilitares siguen cumpliendo amenazas en Córdoba)](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/)**

### Irregularidades de la Alcaldía de Tierralta 

Tal como habían alertado organizaciones, entre ellas Cordoberxia, la Comisión constató las irregularidades que existen alrededor de las comunidades que no tienen vivienda y que fueron expulsadas por el ESMAD de unos lotes del municipio, uno de ellos propiedad del padre del alcalde **Fabio Leonardo Otero**, "el asunto es que el alcalde  no se declaró impedido al tener conflicto de intereses, teniendo en cuenta que uno de los lotes es de su padre".

**Actualmente, el alcalde y el secretario de Gobierno, Willington Ortiz Naranjo, son investigados por la Procuraduría** debido a presuntas irregularidades en el control del orden público originadas de la invasión de estos lotes y que habrían derivado en el homicidio de María del Pilar Hurtado y en amenazas contra diversos habitantes del lugar.  [(Le puede interesar: No se quedó en amenazas panfleto contra ocupantes de predios en Tierralta, Córdoba)](https://archivo.contagioradio.com/panfleto-contra-ocupantes-predios-tierralta-cordoba/)

### **Las comunidades continúan en peligro** 

"Queremos señalar que la situación de las comunidades es muy preocupante", alertó la senadora Sandino quien relató que mientras sostenían la reunión a puerta cerrada con los líderes de la región, **"funcionarios y amigos del alcalde fueron a hacer un mitin"** afuera del recinto, amenazando a las personas al interior del lugar donde se celebraba la audiencia.

A estas denuncias se unieron otros senadores de la Comisión como **Antonio Sanguino** quien declaró vía Twitter que "lo que viene ocurriendo en el sur de Córdoba es revelador de nuevas alianzas entre criminalidad y política en contextos regionales" mientras que el senador **Iván Cepeda** indicó que "grupos de hombres armados se pasean a sus anchas en zonas que conocen las autoridades sin que éstas tomen ninguna medida distinta a declaraciones retóricas. mientras los denunciantes son amenazados".

Tras esta visita, la Comisión llevará un informe ante la Fiscalía el próximo 2 de julio recopilando toda la información recavada durante la visita, "pudimos constatar la ineficiencia de las autoridades y las acciones que han adelantado hasta el momento", concluyó.

<iframe id="audio_37724937" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37724937_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
