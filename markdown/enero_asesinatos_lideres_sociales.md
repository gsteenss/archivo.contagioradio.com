Title: Enero, un mes trágico para líderes y reclamantes de tierras
Date: 2018-01-31 20:33
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales
Slug: enero_asesinatos_lideres_sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 Ene 2018] 

Si el 2017 no cerró bien, este nuevo año empieza de la misma manera. Enero cierra con la penosa cifra de 21 lideresas y líderes sociales y reclamantes de tierras asesinados. Durante el 2017 un total de 170 líderes y defensores, según datos del Instituto de Estudios para el Desarrollo y la Paz (Indepaz), y solo en enero de 2018 asesinaron al menos a un líder cada dos días.

Enero comenzó con el asesinato de **Diana Luz Solano** el 16 de enero. Era la hija del gobernador local de la comunidad La Esperanza, Rufino Solano, ocurrido en la vereda La Concha del municipio Zaragoza, departamento de Antioquia.

Al día siguiente, dos fueron las víctimas. **Víctor Manuel Morato**, presidente de la Junta de Acción Comunal, quien fue asesinado en la vereda La Rompida, del municipio de Yondó, Antioquia. Mientras que en Cúcuta, **Jorge Jimmy Celis, **lider de la Junta de Acción Comunal colinas de El Tunal comuna 6 de la ciudad fue asesinado por un hombre encapuchado que le propinó varios impactos con arma de fuego.

El 18 de enero el país  se levantó con la noticia del asesinato de **Plinio Pulgarín**, presidente de la Junta de Acción Comunal de la vereda San Pedrito de San José de Uré en el departamento de Córdoba, además ordenaron a la comunidad desplazarse inmediatamente de sus tierras. Muchos de ellos aducen que aún no hay garantías para regresar.

También el pasado 21 de enero fue asesinado **Víctor Manuel Barrera**, quien** **llevaba afiliado a la USO 27 años. Su muerte se produjo durante un intercambio de disparos entre un grupo armado no identificado y escoltas de la Unidad Nacional de Protección que protegía una comisión de la FARC. Y el día 22 de este mismo mes, fue asesinado **Wuilmer Angarita,** en la vía Tame –Cucutá, mientras se movilizaba en su vehículo.

El 22 de enero, dos indígenas en Tame, Arauca habrían sido víctimas de ejecuciones extrajudiciales por parte Ejército Nacional, según denunció la ONIC. Se trató de los hermanos **Luis Díaz López** de 22 años, secretario del Cabildo y **Miller Díaz López** quien era el fiscal de la comunidad indígena del Juliero del Pueblo Betoye. Pero además ese mismo día y también en Arauca, fue asesinado **Harley Johanny Mogollón,** dirigente de la Asociación Nacional Campesina José Antonio Galán Zorro.

Ese 22 de enero pero en el municipio de Sucumbíos, departamento de Nariño, límites con el municipio de Orito, en Putumayo, fue ultimado de siete balazos por sicarios José **Olmedo Obando,** de 33 años, y quien era un reconocido líder del Consejo Comentario Afro Nueva Esperanza.

En Buenos Aires, Cauca, el 23 de enero el alcalde, Urdely Carabalí Carbonero, confirmó el asesinato de su hermano, **Fares Carabalí,** líder minero de la zona, que perdió la ida en medio de un atentado armado ejecutado sobre la vía que comunica los sectores de Timba y la Balsa.

Luego, el 24 de enero fue hallado el cuerpo del líder social **Víctor Vitola**, quien apareció en aguas del Río Cáceres a la altura del municipio de Cáceres, Bajo Cauca antioqueño. El joven de 30 años había sido reportado como desaparecido desde el 20 de enero.

Posteriormente, el 25 de enero hombres encapuchados acabaron con la vida de los hermanos** ** reclamantes de tierras **Humberto Manuel Escobar Mercado, de 68 años; Prisciliano Manuel Mercado García, de 63 años, y  Manuel Eusebio Osorio Escobar de 53,** a quienes, la vigilancia privada del exdirector de la Caja de Compensación Comfasucre, William Martínez, les venía impidiendo trazar los linderos de su finca, La Concepción en el corregimiento de Guaripa, Sucre.

El mismo 25 fue ultimado **Miguel Eduardo Parra,** quien fue identificado por la comunidad de Chitagá como líder campesino de una vereda de la Asociación Municipal de Juntas de Acción Comunal de Chitagá (Asojuntas), en Norte de Santander. Parra, apareció en el listado de guerrilleros abatidos del Eln tras un operativo militar que fue desarrollado por el Ejército. Según la comunidad fue defensor del páramo Almorzadero y acompañó a algunas poblaciones vulnerables de la zona.

El pasado 27 de enero fue asesinado **Eleazar Tequia,** Guardia Mayor Indígena del pueblo Kankuamo, en la vía Medellín-Chocó, de acuerdo con la Organización Nacional Indígena de Colombia, ONIC, el asesinato fue cometido por integrantes de la Fuerza Pública, cuando los indígenas, después de una movilización, se disponían a retornar a sus territorios.

En el transcurso de esas 24 horas del sábado, Buenaventura lloró la muerte de uno de los líderes del Paro Cívico de mayo. **Temístocles Machado,**  basó su lucha en la defensa de las tierras tanto de su familia como las de la comunidad. Logró unir a la gente para que defendieran su territorio, logrando arrebatarle tierras a grupos armados y empresarios que querían apropiarse de la tierra de la comunidad Isla de la Paz.

Enero cerró con más pérdidas de líderes sociales, en Lôpez de Micay fue asesinado **Nicomes Payán**, reconocido como un gran líder comunitario y era fiscal de la junta de acción comunal de Mamuncia, en ese municipio del Cauca.

Finalmente, el martes 30 de enero, fueron asesinados dos líderes. **Nilson Dávila**, presidente de la junta de acción comunal de la vereda Chaparral, del municipio de Cantagallo, Bolívar; y **Leidy Amaya, **quien se desempeñaba como promotora de salud, fue ultimada en el municipio de San Calixto, Norte de Santander.

En relación con el año 2016, los asesinatos en 2017 aumentaron un 45%, y las zonas más afectadas fueron Cauca, Nariño y Antioquia. Este año, Antioquia nuevamente es uno de los departamentos más peligrosos para los líderes. También se han registrado estos crímenes en Bolívar, Sucre, Norte de Santander, Cauca, Valle del Cauca, Córdoba y Nariño.

###### Reciba toda la información de Contagio Radio en [[su correo]
