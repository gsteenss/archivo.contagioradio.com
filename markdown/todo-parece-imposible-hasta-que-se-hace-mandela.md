Title: ”Todo parece imposible hasta que se hace”, tres años sin Mandela
Date: 2016-12-05 16:27
Category: El mundo
Tags: Apartheid, Mandela, Nelson Mandela, Sudafrica
Slug: todo-parece-imposible-hasta-que-se-hace-mandela
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Mandela-superJumbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: The New York Times] 

###### [5 Dic. 2016] 

**Luego de 3 años del fallecimiento de Nelson Mandela, hoy Sudáfrica y el mundo lo recuerdan. El hombre que pasó a la historia como aquel que luchó contra el apartheid y en pro de la igualdad en Sudáfrica, es rememorado a través de diversas actividades.**

“Madiba” como era conocido en su tierra natal, título otorgado por los ancianos de su tribu, será recordado a través de un coloquio titulado “el mundo soñado por Nelson Mandela”.

Mandela, quien falleció cuando tenía 95 años debido a unas afecciones pulmonares, es recordado como un activista político y de los derechos humanos, quien fuera encarcelado por haber optado por la lucha armada. Por ello, es condenado a cadena perpetua de la cual solo cumplió 27 años.

Este hombre, premio nobel de paz será **recordado además por su participación en las negociaciones políticas con Frederik de Klerk para abolir el apartheid y por haber impulsado la creación de la comisión para la verdad y la reconciliación que permitió investigar las violaciones a los derechos humanos que fueron cometidas durante el apartheid.**

El actual mandatario de Sudáfrica, Jacob Zuma aseguró en una reciente alocución que “Madiba nos enseñó a unirnos, amarnos y respetarnos unos a otros como sudafricanos". Así mismo manifestó que “desde que se inició la nueva Sudáfrica se 'ha hecho mucho', pero el 'camino por recorrer sigue siendo largo y lleno de desafíos'.

**Algunas frases recordadas de Nelson Mandela**

1.  La acción de las masas tiene la capacidad de derrocar gobiernos.
2.  El deporte tiene el poder de transformar el mundo. Tiene el poder de inspirar, de unir a la gente como pocas otras cosas… Tiene más capacidad que los gobiernos de derribar barreras raciales.
3.  No puede haber una revelación más intensa del alma de una sociedad que la forma en la que trata a sus niños
4.  Mi ideal más querido es el de una sociedad libre y democrática en la que todos podamos vivir en armonía y con iguales posibilidades.
5.  Los verdaderos líderes deben estar dispuestos a sacrificarlo todo por la libertad de su pueblo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).

 
