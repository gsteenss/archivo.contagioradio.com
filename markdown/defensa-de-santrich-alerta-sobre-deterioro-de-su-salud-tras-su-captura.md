Title: Defensa de Santrich alerta sobre deterioro de su salud tras su captura.
Date: 2018-04-16 16:22
Category: DDHH, Política
Tags: Fiscalía, JEP, Jesús Santrich
Slug: defensa-de-santrich-alerta-sobre-deterioro-de-su-salud-tras-su-captura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/SANTRICH.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Abr 2018] 

Una semana en huelga de hambre completa Jesús Santrich, integrante de la dirección política de la FARC, luego de su captura. **Este lunes se conoció que fue rechazada la solicitud que había sido interpuesta por la defensa de Santrich**. Sin embargo, para el abogado defensor Gustavo Gallardo, este hecho se suma a la cadena de irregularidades que se han venido dando en este proceso y se teme por el deterioro de salud de Santrich.

Para, Gallardo la detención y captura que hizo la Fiscalía General, fue ilegal e irregular, en ese sentido se esperaba que el mecanismo del Habeas Corpus lograra liberarlo. El paso siguiente de la defensa de Santrich, **será impugnar esta decisión del Tribunal Superior de Bogotá y acudir a la Corte Suprema de Justicia, como segunda instancia**.

“Acá lo importantes es lograr la libertad de Santrinch, para también salvarle la vida, ya lleva una semana larga en huelga de hambre, y consecuentemente que se genere un **ambiente favorable en función de rescatar el Acuerdo de paz y las confianzas entre las partes**” afirmó Gallardo. (Le puede interesar:["Manejo del caso Santrich por parte de la Fiscalía ha sido imprudente: Enrique Santiago"](https://archivo.contagioradio.com/manejo-del-caso-santrich-por-parte-de-la-fiscalia-ha-sido-imprudente-enrique-santiago/))

### **La salud de Santrich se deteriora** 

Gustavo Gallardo señaló Santrich **ha tenido un deterioro en su salud y aseveró que por el momento solo está ingiriendo agua**. Aun así, Gallardo informó que Santrich continúa diciendo que se mantendrá firme al interior del calabozo con dignidad, para que se cumplan los Acuerdos de La Habana y se produzca el proceso de amnistía de los prisioneros políticos que continúan en las cárceles colombianas.

Con respecto a las diferentes pruebas que se han presentado contra Santrich, el abogado manifestó que aún la defensa no tiene conocimiento de estas, razón por la cual no han podido constatar la legalidad de las mismas, **“conocemos lo mismo que conoce toda la opinión pública y son las pruebas que por medios de comunicación estuvieron rodando**”.

Y frente a las afirmaciones que han salido en medios de información sobre un posible escape de Santrich, Gallardo señaló que es completamente falso y que los mismos medios han dicho que el integrante de la FARC ya conocía la orden de captura contra el, y que al contrario de como se ha señalado en los medios, espero a ser capturado en su vivienda en Modelia.

<iframe id="audio_25395912" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25395912_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
