Title: Con triatlón por el derecho a la educación vuelven los estudiantes a las calles
Date: 2015-09-17 12:00
Category: Educación, Movilización, Nacional
Tags: Asociación Nacional de Estudiantes de Secundaria, Comisión Cuarta del Senado, Comisión Tercera del Senado, Congreso de la República, Derecho a la educación, Frente Amplio por la Educación, Presupuesto Nacional 2016, SENA, Triatlón por la educación, Universidad Distrital, Universidad Nacional de Colombia, Universidad Pedagógica Nacional
Slug: con-triatlon-por-el-derecho-a-la-educacion-vuelven-los-estudiantes-a-las-calles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Triatlón.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Frente Amplio por la Educación ] 

<iframe src="http://www.ivoox.com/player_ek_8437894_2_1.html?data=mZmgmZ2deI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRh9DijNnfy8bYsIa3lIqvldOPtNDmjMrZjcnJtsbXydSYw5DQpYzZxdrQw8jNaaSnhqeg0JDaucbg18rbjdHTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Deisy Aparicio, ANDES] 

###### [17 Sept 2015] 

[Desde la mañana de este jueves se lleva a cabo la "**Gran triatlón: persiguiendo el presupuesto para la educación"**, convocada por el Frente Amplio por la Educación, los Derechos y la Paz, en el marco de la actual discusión de las Comisiones Tercera y Cuarta del Senado de la República en torno al presupuesto nacional de 2016. ]

[Estudiantes, profesores y trabajadores partieron desde las 8 de la mañana caminando, trotando y en bicicleta, desde distintas universidades y sedes del SENA, para llegar sobre las 11 am a la calle 19 con carrera 7 y dirigirse a la Plaza de Bolívar.    ]

[La movilización pretende **denunciar públicamente las medidas de privatización y mercantilización acogidas por el gobierno nacional que agudizan la actual crisis estructural del sector**. El **actual recorte del presupuesto nacional para adecuación de aulas, alimentación escolar, investigación y de fortalecimiento de universidades públicas.**]

[Según Deisy Aparicio, representante de ANDES, el gobierno nacional de asigna recursos públicos a programas de financiamiento a la demanda, a través de créditos y subsidios focalizados que provocan el **endeudamiento de sus beneficiarios y el detrimento del derecho a la educación.**]

[El modelo actual de financiamiento de la educación pública en Colombia no supera los \$300mil millones, agudizando el **déficit presupuestal de las universidades públicas que ya supera los \$16 billones** y profundizando las brechas sociales, así como la **concepción de la educación como una mercancía rentable para el capital privado**, señala Aparicio.]

[El Frente Amplio por la educación, integrado por maestros y estudiantes de todos los niveles, trabajadores y organizaciones populares, extendió la invitación a la sociedad colombiana en su conjunto para que marchando o caminando, montando en bicicleta o corriendo exijan un **presupuesto adecuado para la consolidación de un sistema educativo digno en la básica primaria, secundaria y superior, en los niveles técnico, tecnológico y profesional**.]
