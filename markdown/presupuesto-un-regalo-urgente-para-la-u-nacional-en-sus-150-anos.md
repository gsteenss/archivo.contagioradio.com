Title: Universidad Nacional necesita presupuesto para seguir siendo pública
Date: 2017-09-22 16:13
Category: Educación, Entrevistas
Tags: Ser Pilo Pago, Universidad Nacional
Slug: presupuesto-un-regalo-urgente-para-la-u-nacional-en-sus-150-anos
Status: published

###### [Foto: Universidad Nacional] 

###### [22 Sept 2017] 

Además de una oportunidad para celebrar que la Universidad más importante del país, y la mejor, cumple 150 años, también es importante que se proveche la visibilización de una fecha como esta para hacer énfasis en las necesidades que afronta el Alma Mater de los colombianos y colombianas. Según Andrés Salazar, representante de los estudiantes ante el Consejo Superior Universitario, aseguró que es necesario centrar la mirada en algunas problemáticas estructurales que podrían acabar con la querida universidad.

Uno de ellos es la necesidad presupuestal. Según algunos cálculos los problemas de  infraestructura que afronta la planta física requieren la inversión de **118.000.000 de pesos, solamente para mantenimiento y reparación de los 49 edificios** en estado de fragilidad 23 en alto riesgo de vulnnerabilidad, 4 que están en peligro de ruina y los otros 80 que necesitan atención urgente.

Sin embargo el problema del presupuesto no para en la infraestructura, también es necesario fortalecer la planta docente que se ha visto afectada por la contratación, casi masiva de **"profesores taxistas"** que trabajan como catedráticos y que están de un lado para otro para completar un salario como lo describe Salazar.

Además, Mario Hernandez, histórico profesor de la "Nacho" señala que los recursos que necesita la universidad se están destinando a programas como "Ser Pilo Paga" que orientan los recursos hacia universidades que cobran 18 millones por matrícula, mientras que la U Pública se podrían sostener casi 8 estudiantes por ese mismo costo. (Le puede interesar:["Ser Pilo Paga a gastado más de 350 mil millones del presupuesto de educación"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/))

“Enunciar que somos la mejor universidad del país no resuelve la crisis en la que se encuentra sumergida y en donde se necesitan una serie de cambios en la política pública y en el tratamiento del Estado para salvaguardar ese patrimonio de todos los colombianos” afirmó el estudiante.

Y agregó “Como no se puede sacar el dinero del funcionamiento, se va a cumulando el deterioro, p**or años y por décadas, que es lo que tenemos hoy en la Universidad Nacional**, en donde muchos de los edificios se encuentran en estado crítico” señala Salazar.

<iframe id="audio_21041847" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21041847_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **La diversidad de la Universidad Nacional una clave para el pensamiento** 

Frente a la crítica que ronda en varios sectores en torno a que los estudiantes estarían perdiendo un sentido crítico de la realidad, Salazar plantea que la diversidad es la clave de la construcción democrática, entonces al interior del claustro se encuentran grupos de diversos matices políticos y eso fortalece la misma presencia de la universidad en el debate.

Por ejemplo, el aporte del Centro de Pensamiento está siendo clave en los esfuerzos de búsqueda de la paz. En ese sentido se estaría construyendo una nueva forma de participación. "Hace años que en la universidad no hay una pedrea" y eso podría ser indicativos **de que se están potenciando otras formas de incidir en los problemas estructurales y coyunturales de Colombia.**

### **La Universidad Nacional cada vez menos pública** 

Otra de las amenazas para la pérdida de la diversidad son los altos costos de la matrícula, que hacen que cada vez sea más difícil que estudiantes de estratos bajos y medios sigan en la Universidad Nacional. (Le puede interesar:["U.Nacional en crisis por déficit presupuestal de 118 mil millones de pesos"](https://archivo.contagioradio.com/u-nacional-tiene-un-deficit-de-presupuesto-de-118-mil-millones-de-pesos/))

Sumado a ello se encuentra la metodología del examen de admisión a la institución que no está acorde a los conocimientos adquiridos por alumnos de colegios públicos en la capital, dificultado que la población que más necesita ingresar, logre hacerlo, “antes se preguntaba en un salón quienes eran de un colegio público y case **el 70% lo era, ahora es el 40%” afirmó Andrés. **

### **¿Qué regalarle a la “Nacho?** 

Tanto Andrés Salazar como el profesor Mario Hernández, concordaron en señalar que una ampliación de cubertura y acceso, con calidad, para la Universidad Nacional, con un marco de inclusión regional

“Debe ser a través del Estado que se de la financiación de la educación superior, celebramos **150 años, pero esperamos que el próximo año no estemos a punto de cerrar las puertas**” afirmó Hernández. (Le puede interesar:["Por falta de presupuesto se está cayendo la Universidad Nacional"](https://archivo.contagioradio.com/por-falta-de-presupuesto-se-esta-cayendo-la-universidad-nacional/))

<iframe id="audio_21042046" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21042046_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
