Title: Mujeres que están cambiando la historia
Date: 2015-11-25 07:18
Category: Cultura, Mujer
Tags: Antonia Santos, Aung San Suu Kyi, Derechos Humanos, dia de la no violencia contra la mujer, Hermanas Mirabal, Leymah Roberta Gbowee, Manuela Beltrán, María Montessori, Marie Curie, mujer, Mujeres Comuneras, Policarpa Salavarrieta, Rigoberta Menchú, Rosa Luxemburgo
Slug: 11-mujeres-que-dejaron-una-huella-imborrable-en-la-historia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/HERMANAS-MIRABAL.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

Cada 25 de noviembre se conmemora el Día Internacional de la Eliminación de la Violencia contra la mujer, fecha que establecida por la Asamblea General de las Naciones Unidas en 1999 y desde 1981 por otras organizaciones defensoras de los derechos de la mujer, en alusión al asesinato de las hermanas Mirabal en República Dominicana durante la dictadura militar  de Rafael Trujillo.

Mujeres que a través de la historia han dejado una huella imborrable con sus luchas, mujeres que han resistido a la violencia para convertirse en gestoras de nuevas ideas, en reivindicadoras de sus derechos y del papel de la mujer en la defensa de sus ideales, en la defensa de sus derechos.

En Contagio Radio, hacemos un homenaje a estas y otras mujeres que a través de sus acciones  han marcado la historia.

### [**Hermanas Mirabal.**] 

[![Hermanas Mirabal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Hermanas-Mirabal-600x337.jpg){.aligncenter .wp-image-17776 width="600" height="337"}](https://archivo.contagioradio.com/11-mujeres-que-dejaron-una-huella-imborrable-en-la-historia/hermanas-mirabal-2/)

**Patria, Minerva y María Teresa Mirabal**, fueron férreas opositoras al régimen político del dictador Rafael Trujillo. Integraron la Agrupación política 14 de junio, un movimiento clandestino en contra del mandatario dominicano. Fue el mismo dirigente, quien junto con el Servicio de Inteligencia Militar (SIM), orquestó un plan para atentar contra sus vidas.

Con la muerte de Las Mirabal, a quienes llamaban también ‘Las Mariposas’, además de sus constantes acciones represivas, el Gobierno de Trujillo perdió credibilidad política y simpatizantes tanto en el país latinoamericano como en la comunidad internacional, hasta su derrocamiento y asesinato en 1961.

### [**Rigoberta Menchú**] 

![Nobel Peace Prize winner Rigoberta Menchu attends a meeting of indigenous communities in Caracas February 21, 2013. REUTERS/Carlos Garcia Rawlins (VENEZUELA - Tags: POLITICS SOCIETY)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Rigoberta-Menchú-600x393.jpg){.aligncenter .wp-image-17777 .size-large width="600" height="393"}

Es una indígena guatemalteca promotora de las luchas sociales en su país y en el mundo. Desde muy joven se involucró en los movimientos reivindicativos de las comunidades campesinas e indígenas.

Durante la Guerra Civil Guatemalteca, Rigoberta perdió a varios miembros de su familia; entre ellos, sus padres y su hermano. Sin embargo, dentro de su país y recorriendo el mundo, dio testimonio sobre las consecuencias del conflicto en Guatemala y de las necesidades de sus habitantes, transmitiendo siempre un mensaje de paz. Éste trabajo la llevaría a ganar en el año 1992 el premio Nobel de Paz

### [**Aung San Suu Kyi**] 

![Aung San Suu Kyi](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Aung-San-Suu-Kyi-600x400.jpg){.aligncenter .wp-image-17778 .size-large width="600" height="400"}

Es la mujer que ha representado durante décadas el deseo de que en Birmania se implante una democracia que permita alternar el régimen militar imperante en ese país. Durante distintos lapsos de tiempo, ha estado presa. En sumatoria, Aung San ha pasado más de una década privada de libertad en su país.

Ella es la líder del movimiento de oposición LND -Liga Nacional para la Democracia-. Sus esfuerzos han parecido dar los primeros frutos; puesto que después de más de 25 años, en Birmania volvieron a haber elecciones y el LND obtuvo una victoria contundente sobre los militares.

### [**Marie Curie**] 

![Foto:](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Marie-Curie-600x338.jpeg){.aligncenter .wp-image-17779 .size-large width="600" height="338"}

Nació en Polonia, estudió clandestinamente en Varsovia y luego se trasladó a París. Durante su vida científica, se enfocó en sus estudios de física y química y obtuvo en dos ocasiones el premio Nóbel: el de física –que compartió con su esposo Pierre Curie y con Antoine Becquerel en 1903- y el de química en 1911.

Entre sus logros, se encuentra el haber patentado la teoría de la radioactividad y el descubrimiento de los elementos químicos ‘Polonio’ y ‘Bario’.

Para alcanzar metas y desarrollar su trabajo, tuvo que superar las barreras de género que orbitaban en la época.

### [**Leymah Roberta Gbowee**]

\[caption id="attachment\_17780" align="aligncenter" width="600"\]![Foto: uml.edu](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Leymah-Roberta-Gbowee-600x400.jpg){.wp-image-17780 .size-large width="600" height="400"} Foto: uml.edu\[/caption\]

Es una activista y defensora de los Derechos Humanos en Liberia; organizadora del Movimiento de Paz y del grupo ‘*Women of Liberia Mass Action for Peace’,* que dio fin a la Segunda Guerra Civil liberiana en el año 2003 que a la postre, condujo a la elección de Ellen Johnson Sirleaf, como presidenta; la primera mujer en la historia en ocupar el cargo en el continente africano.

Gbowee conformó a un amplio grupo de mujeres cristianas y musulmanas para que rezaran por la paz. Asimismo, realizaron varias acciones simbólicas para terminar el conflicto.

### [**María Montessori.**] 

\[caption id="attachment\_17781" align="aligncenter" width="600"\]![Foto: ibalpe.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/María-Montessori-600x290.jpg){.wp-image-17781 .size-large width="600" height="290"} Foto: ibalpe.com\[/caption\]

María Montessori nació en Italia en 1870, alrededor de una familia acomodada. Desde pequeña, realizó estudios de ingeniería y biología.

Pese a la oposición de su padre, se inscribió a la carrera de Medicina en la Universidad de Roma; fue la primera mujer médico en Italia. Luego de ello, estudió Antropología, obtuvo un doctorado en Filosofía y estuvo interesada en el campo de la Psicología.

Más adelante, se interesó por las causas sociales; se convirtió en defensora de los derechos de las mujeres para luego, especializarse en la educación en la primera infancia y en los niños.

### [**Rosa Luxemburgo.**] 

\[caption id="attachment\_17789" align="aligncenter" width="600"\]![Foto: revoluciontrespuntocero.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Rosa-Luxemburgo1-600x502.jpg){.wp-image-17789 .size-large width="600" height="502"} Foto: revoluciontrespuntocero.com\[/caption\]

Símbolo de la mujer revolucionaria,  política, filósofa, activista y economista de ideología socialista. Militó en varios partidos de izquierda y es una de las figuras más importantes del marxismo.

Una de sus luchas por la reivindicación de los derechos de la mujer fue extipular desde la Segunda Internacional,  el 8 de marzo como  día internacional de la mujer trabajadora, propuesta que  realizó junto a Clara Zetkin

Una de  sus frases más conocidas es “No debemos olvidar, empero, que no se hace la historia sin grandeza de espíritu, sin una elevada moral, sin gestos nobles”.

### [**Simone de Beauvoir**] 

\[caption id="attachment\_17810" align="aligncenter" width="583"\][![tomada de crai.ub.edu](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/simone-de-beauvoir-contagioradio.jpg){.wp-image-17810 width="583" height="418"}](https://archivo.contagioradio.com/11-mujeres-que-dejaron-una-huella-imborrable-en-la-historia/simone-de-beauvoir-contagioradio/) tomada de crai.ub.edu\[/caption\]

Nació en Paris en 1908 Pensadora y novelista francesa, representante del movimiento existencialista ateo y figura importante en la reivindicación de los derechos de la mujer.

Con su libro "*El segundo sexo"* publicado en 1949 fijó un punto de partida teórico para feministas. Elaboró una historia sobre la condición social de la mujer, las distintas características de la opresión masculina, entre otras la exclusión de los procesos de producción y confinada al hogar y a las funciones reproductivas, la mujer perdía todos los vínculos sociales y con ellos la posibilidad de ser libre. Destruyó los mitos femeninos y sostuvo que la lucha para la emancipación de la mujer era distinta y paralela a la lucha de clases, y que el principal problema que debía afrontar el "sexo débil" no era ideológico sino económico.

### [**Mujeres comuneras.**] 

\[caption id="attachment\_17783" align="aligncenter" width="600"\][![Foto: correvedile.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/manuela-beltran1.jpg){.wp-image-17783 width="600" height="394"}](https://archivo.contagioradio.com/11-mujeres-que-dejaron-una-huella-imborrable-en-la-historia/manuela-beltran1/) Foto: correvedile.com\[/caption\]

Durante la independencia de la Nueva Granada sobre la corona española, las mujeres  fueron protagonistas. Entre ellas están:

### [**Manuela Beltrán.**] 

\[caption id="attachment\_17784" align="aligncenter" width="600"\][![Foto: heroinas.blogspot](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/manuela-beltran11.jpg){.wp-image-17784 width="600" height="433"}](https://archivo.contagioradio.com/11-mujeres-que-dejaron-una-huella-imborrable-en-la-historia/manuela-beltran1-2/) Foto: heroinas.blogspot\[/caption\]

En plena plaza pública arrancó y rompió un edicto que estipulaba otro impuesto para los pobladores. Su acto heroico, representó el inicio de la Insurrección Comunera en 1781.

### [**Antonia Santos.**] 

\[caption id="attachment\_17785" align="aligncenter" width="600"\][![Foto: googledoodleus.tumblr.com](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Antonia-Santos.jpg){.wp-image-17785 width="600" height="400"}](https://archivo.contagioradio.com/11-mujeres-que-dejaron-una-huella-imborrable-en-la-historia/antonia-santos/) Foto: googledoodleus.tumblr.com\[/caption\]

Se unió a la causa de Simón Bolívar, creando la ‘Guerrilla de Coromoro’, que luchaba contra los españoles. Sus fuerzas mantuvieron un papel importante en las Batallas del Pantano de Vargas y la de Boyacá. Al ser traicionada por uno de sus conocidos, fue fusilada en 1819.

### [**Policarpa Salavarrieta.**] 

[![Policarpa Salavarrieta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Policarpa-Salavarrieta1.jpg){.aligncenter .size-full .wp-image-17791 width="595" height="407"}](https://archivo.contagioradio.com/11-mujeres-que-dejaron-una-huella-imborrable-en-la-historia/policarpa-salavarrieta-2/)

Fue una espía de las fuerzas independentistas de la República durante el gobierno de La Patria Boba. También, a muy temprana edad, participó en la batalla de independencia de 1810. Murió fusilada durante la Reconquista Española en 1817. Actualmente, se le considera una mártir y prócer de la patria.
