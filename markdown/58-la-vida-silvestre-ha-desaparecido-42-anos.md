Title: 58% de la vida silvestre ha desaparecido en 42 años
Date: 2016-11-08 16:52
Category: Ambiente, Nacional
Tags: cambio climatico, empresas extractivas, especies en vías de extinción
Slug: 58-la-vida-silvestre-ha-desaparecido-42-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/jaguar-brasilia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: SavePlanet] 

###### [8 Nov de 2016] 

Según un informe de la organización World Wildlife Fund, **entre 1970 y 2012, se produjo una disminución del 58% en las poblaciones de vertebrados, peces, aves, mamíferos, reptiles y anfibios, en todo el mundo.** Además advierte que si la tendencia actual sigue su curso, "se calcula que en cuatro años, viviremos la desaparición de dos tercios de la vida silvestre en el mundo".

El informe revela que, 3.706 especies que habitan en tierra, mar y ríos se vieron debilitadas en forma notoria. "A medida que la humanidad sigue exigiendo cada vez más a **la tierra y ejerce presión sobre nuestro capital natural, lo que estamos viendo es la desaparición de la vida silvestre"**, afirmó Colby Loucks, director del Programa de Conservación de Vida Silvestre de la WWF. Le puede interesar: [En Colombia 10 especies de aves migratorias están en peligro de extinción.](https://archivo.contagioradio.com/en-colombia-10-especies-de-aves-migratorias-estan-en-peligro-de-extincion/)

Según WWF, las causas de esta situación son, **la sobre explotación, la contaminación, la presencia de algunas especies invasoras, que migran a otros hábitats al ver perdido el propio y el aumento de la temperatura global. **Le puede interesar: [La tercera parte de las especies animales se extinguirán por el cambio climático.](https://archivo.contagioradio.com/la-tercera-parte-de-las-especies-animales-en-el-mundo-se-extinguiran-por-el-cambio-climatico/)

Aparte de estas causas fundamentales, el informe señala nueve puntos que han sido cruciales para la pérdida de ecosistemas y especies animales, que **habría que tener el cuenta a la hora de frenar el cambio climático:**

1.  Destrucción de los ecosistemas y la biodiversidad
2.  Cambio climático
3.  La acidificación del océano
4.  Cambio del uso de la tierra
5.  Uso insostenible del agua dulce
6.  Perturbación de los flujos biogeoquímicos -aportes de nitrógeno y fósforo a la biósfera-
7.  Alteración de los aerosoles atmosféricos;
8.   Contaminación generada por sustancias nuevas
9.   Agotamiento del ozono de la estratósfera.

**En Colombia existen más de 10 hidroeléctricas, que han generado cambios irreversibles a los ecosistemas,** han ocasionado pérdidas de especies animales y vegetales además de fuertes daños a las comunidades que habitan estos territorios.

Por último, Loucks manifiesta que algunas de las naciones más ricas en biodiversidad, incluidas **Estados Unidos, Canadá y Australia, tienen las mayores huellas ecológicas en el mundo, y son las que menos interés han mostrado en la ratificación de acuerdos** que favorezcan la conservación ambiental y la prevención de un mayor aumento en la temperatura del planeta.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]

[Informe Planeta Vivo 2016 Riesgo y Resilencia en Una Nueva Era](https://www.scribd.com/document/330453339/Informe-Planeta-Vivo-2016-Riesgo-y-Resilencia-en-Una-Nueva-Era#from_embed "View Informe Planeta Vivo 2016 Riesgo y Resilencia en Una Nueva Era on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_43593" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/330453339/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-WzCS0dmZsnWYgP4IaJYf&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7383100902379"></iframe>
