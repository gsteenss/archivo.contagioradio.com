Title: Memoria y resistencia en el Cañón del Río Cauca
Date: 2017-09-26 12:38
Author: AdminContagio
Slug: memoria-y-resistencia-en-el-canon-del-rio-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/cañon-del-rio-cauca.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="https://spark.adobe.com/page/mkneiqZdGdXoE" width="100%" height="700" frameborder="0" scrolling="sí" allowfullscreen="allowfullscreen"> </iframe>

###### [27 Sept 2017]

<p>
<span style="color: #999999;">

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fwww.contagioradio.com%2Fmemoria-y-resistencia-en-el-canon-del-rio-cauca%2F&amp;layout=button_count&amp;size=small&amp;mobile_iframe=true&amp;width=91&amp;height=20&amp;appId" width="91" height="20" frameborder="0" scrolling="no">
</iframe>
[Twittear](https://twitter.com/share){.twitter-share-button}

<script>// <![CDATA[ !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs'); // ]]></script>
</span>

</p>

