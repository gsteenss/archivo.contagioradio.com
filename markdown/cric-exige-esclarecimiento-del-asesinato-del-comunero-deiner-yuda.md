Title: CRIC no permitirá que asesinato del comunero Deiner Yunda quede en la impunidad
Date: 2019-04-24 16:21
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Agresiones del ESMAD, CRIC, Indígenas Asesinados, Minga Nacional
Slug: cric-exige-esclarecimiento-del-asesinato-del-comunero-deiner-yuda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/55949702_1676960492406329_5701197755452489728_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

Este 24 de abril en **Totoró, Cauca, el Consejo Regional Indígena del Cauca,** CRIC, realizó una audiencia pública de denuncia contra el trato militar que le dio el Gobierno a la Minga del suroccidente del país  y el asesinato del comunero Deiner Yunda el pasado 2 de abril. Con este encuentro, el pueblo indígena ratificó su apoyo a los familiares del minguero, y a su vez responsabilizó al Gobierno Nacional por los hechos ocurridos.

Según el **dirigente del CRIC, Yesid Conda**, la audiencia a la que se convocó a diversos entes como la Contraloría, a la Procuraduría, a la Fiscalía, a la Gobernación del Cauca, a la Defensoría del Pueblo pero a la que únicamente asistieron el ente acusador y la MAPP-OEA, exigió resultados en la investigación y puso en conocimiento los acontecimientos que sucedieron el 2 de abril  que resultaron en el asesinato de Ceferino Yunda. [(Lea también: Asesinan a minguero Deiner Yunda tras 22 días de resistencia en Cauca)](https://archivo.contagioradio.com/asesinan-a-minguero-breiner-yundaque-tras-22-dias-de-resistencia-en-cauca/)

### "Tenemos la evidencia, pero no hay avance en la investigación" 

Según la inspección que se hizo y que fue coordinada entre la Guardia Indígena, la Fiscalía y la ONU como organismo garante, no logró establecer un dictamen concreto, sin embargo señalan que Deiner murió por el imapcto de un proyectil de fusil de la Policía, "tenemos la evidencia, no hay avance en la investigación, **pero si tenemos presente que fue la Policía que lo asesinó"**, explica Conda quien agrega que hubo una versión que se dio a la opinión pública de que Deiner fue herido por esquirlas sin embargo en los dictámenes de la necropsia o autopsia no se encontraron esas evidencias.

De igual forma el dirigente manifestó que el CRIC y la ONIC harán presencia en el paro nacional de este 25 de abril en la ciudad de Bogotá y en Cali donde también marcharán  junto a la Asociación de Cabildos Indígenas del Norte del Cauca (ACIN) y los diferentes sectores que se movilizarán alrededor del país, reafirmando que la Minga continúa caminando por la vida.

<iframe id="audio_34917795" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34917795_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
