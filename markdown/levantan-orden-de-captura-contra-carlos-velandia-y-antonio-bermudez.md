Title: Levantan orden de captura contra Carlos Velandia y Antonio Bermúdez
Date: 2016-07-26 13:58
Category: DDHH
Tags: Carlos Velandia, dialogos de paz con eln, francisco galan
Slug: levantan-orden-de-captura-contra-carlos-velandia-y-antonio-bermudez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/carlos-velandia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [26 Julio 2016 ]

De acuerdo con información preliminar fueron levantadas las ordenes de captura contra Carlos Arturo Velandia y Antonio Bermúdez Sánchez.

A través de la resolución Nº 207 de 2016  firmado por el Presidente Juan Manuel Santos se levanto la orden de captura contra el gestor de paz Carlos Arturo Velandia, la resolución fue firmada el día 25 de julio. La orden designa como gestor de paz a **Velandia Jagua** y a **Antonio Bermúdez** para que a través de su experiencia en temas de paz contribuyan a generar acercamiento con las guerrillas y grupos armados al margen de la ley

Carlos Velandia, ex integrante del ELN y uno de los impulsores del proceso de paz con esa guerrilla, fue [[detenido el pasado 20 de junio](https://archivo.contagioradio.com/proceso-contra-carlos-velandia-entra-en-etapa-de-juicio-afectando-su-derecho-a-la-defensa/)], en el aeropuerto internacional 'El Dorado' cuando arribaba a Colombia.

Por su parte, desde el Comité de Solidaridad con los Presos Políticos, organización que asume la defensa de estos dos Gestores de Paz, aseguró a través de un comunicado de prensa que "espera que María Baldina Benítez Sarmiento,  juez penal del circuito especializado de Cali, emita  la suspensión de las medidas judiciales correspondientes para que Carlos Velandia salga en libertad y siga  trabajando en nombre de la paz como lo ha venido haciendo durante más de 10 años".

[![carlos velandia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/carlos-velandia.jpg){.aligncenter .wp-image-26858 width="489" height="652"}](https://archivo.contagioradio.com/levantan-orden-de-captura-contra-carlos-velandia-y-antonio-bermudez/carlos-velandia-2/)

[![Carlos Velandia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Carlos-Velandia.jpg){.aligncenter .wp-image-26833 width="492" height="656"}](https://archivo.contagioradio.com/levantan-orden-de-captura-contra-carlos-velandia-y-antonio-bermudez/carlos-velandia/)  
Noticia en desarrollo...
