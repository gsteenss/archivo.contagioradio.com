Title: Grecia y el Eurogrupo pactan la prorroga del crédito 4 meses más
Date: 2015-02-20 22:36
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: BM, Crisis económica Europea, Europa, FMI, Grecia, Syriza, Troika
Slug: grecia-eurogrupo
Status: published

###### **Foto:Economiasimple.net** 

##### [**Entrevista a Jordi Sebastià:**] 

<iframe src="http://www.ivoox.com/player_ek_4113381_2_1.html?data=lZaelZicdY6ZmKiakp6Jd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjKri1NTIrdHp1cbR0ZDXs8PmxpDZw5DHtsrnytiYx8jTsoa3lIqvldLNp8Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La **crisis económica europea iniciada en 2008 ha provocado que países como Irlanda, Portugal, España o Grecia hayan entrado en una grave recesión de sus economías**, con consecuencias directas como altas tasas de desempleo e inmigración.

De las graves consecuencias sociales que ha tenido la crisis, han **crecido en los países más afectados fuerzas de izquierda distintas a las tradicionales**. Partidos que han cuestionado las políticas neoliberales y las soluciones de la Troika, el FMI y el BM.

En esta coyuntura, la victoria del partido **Syriza en Grecia, o la posible llegada al poder del partido "Podemos" en España**, ambos cuestionan los rescates financieros y las condiciones que estos conllevan, traducidos en austeridad y recortes en derechos sociales y laborales.

Esta última semana **Grecia ha tenido que solicitar una prorroga del rescate de seis meses**, pero esta vez ha realizado un documento con condiciones, para así evitar más consecuencias de la austeridad.

Después de un tira y a floja con el Eurogrupo y las negativas de Alemania, el texto ha sido aprobado y puesto a disposición de los distintos parlamentos. Ahora es momento de analizar las consecuencias de la deuda que han contraído países como España o Grecia.

Entrevistamos a **Jordi Sebastià, europarlamentario por la coalición de izquierdas Primavera Europea**, quién nos indica las claves y las posibles consecuencias de las deudas y del porque de la presión sobre el gobierno griego de Syriza.

#####  
