Title: Indígenas de Brasil bloquean vías para frenar invasión a sus territorios
Date: 2020-08-20 15:51
Author: AdminContagio
Category: Actualidad, El mundo, Nacional
Tags: Coronavirus, Deforestación del Amazonas, indígenas de Brasil
Slug: indigenas-de-brasil-bloquean-vias-para-frenar-invasion-a-sus-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Brasil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

[Foto: @IWGIA]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde este lunes, indígenas del pueblo kayapó mekragnoti en Brasil han bloqueado la carretera amazónica BR-163 del estado de Paraná, cerca a la municipalidad de Novo Progresso "por tiempo indefinido", informó la agencia AFP.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comunidad indígena exige al gobierno ayudas para combatir el coronavirus, así como poner fin a las invasiones de sus territorios para actividades como la minería ilegal y deforestación del Amazonas. (Le puede interesar:[Irreversible daño ambiental por metano en la atmósfera](https://archivo.contagioradio.com/irreversible-dano-ambiental-por-metano-en-la-atmosfera/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La vía BR-163 es una de las más importantes para agricultores brasileños y sus exportaciones de maíz y soja por los puertos fluviales del Amazonas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esto, el lunes en la noche, un juez federal pidió a los manifestantes terminar las protestas pues el bloqueo de la ruta podría afectar la economía de la región, sin embargo las comunidades indígenas alegan su derecho a la protesta además de exigir condiciones de vida digna para ellos y ellas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El martes por la tarde, los manifestantes levantaron brevemente el bloqueo aclarando que sería temporal y por “razones humanitarias” ya que algunos de los conductores atrapados en la carretera a las afueras de Novo Progresso tenían problemas médicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, más tarde ese martes, volvieron a bloquear la vía y anunciaron que estarán hasta que la Agencia Federal de Asuntos Indígenas, el ministro de salud o el secretario de Salud Indígena y el representante del Regulador Ambiental IBAMA acepten reunirse con ellos para llegar a una cuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Cerramos la vía nuevamente y esta vez no tenemos una fecha para reabrirla” indicó el líder indígena Mudjere Kayapo en una declaración para el Instituto Kabo.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Las comunidades indígenas de Brasil son de las más afectadas
------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Brasil es uno de los países más golpeados por el coronavirus y esto no deja de lado a los pueblos indígenas. Cerca de 689 indígenas han muerto y hay más de 26.000 casos de contagio en 154 pueblos originarios, de acuerdo a la [Articulación de Pueblos Indígenas de Brasil (APIB)](http://apib.info/emergencia-indigena/?lang=es) que además culpa al gobierno de Jair Bolsonaro por no tomar acciones contundentes. (También le puede interesar: [Más de 300 grupos indígenas de Brasil podrían desaparecer por el Covid-19](https://archivo.contagioradio.com/mas-de-300-grupos-indigenas-de-brasil-podrian-desaparecer-por-el-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, de los 1.600 habitantes de las doce aldeas de las dos reservas que habitan los kayapó mekragnoti, cuatro han muerto a causa del virus y hay unos 400 infectados, según datos de la ONG Kabú.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se cree que los primeros contagios se debieron al contacto de los indígenas con las poblaciones urbanas y a la continua presencia y operaciones de mineros ilegales en sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, los pueblos indígenas exigen recompensan pues la autopista en la que hoy se encuentran fue totalmente pavimentada a principios del año con el objetivo de llevar el desarrollo a la región del Amazonas.  Aun así, la comunidad indígena que hoy protesta señala que este proyecto ha dañado el medio ambiente y exigen una compensación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además exigen al presidente Bolsonaro, partidario de la agricultura y ganadería en el Amazonas, que se tomen medidas pues además de los incendios que se vienen presentado, en los primeros seis meses de 2020 se registraron más de 3.069,61 km2 cuadrados de área deforestada, según el Instituto Nacional de Investigaciones Espaciales (INPE).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
