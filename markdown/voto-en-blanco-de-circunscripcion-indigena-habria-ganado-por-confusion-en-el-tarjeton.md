Title: Voto en blanco en circunscripción indígena habría ganado por confusión en el tarjetón
Date: 2018-03-14 13:03
Category: Nacional, Política
Tags: circunscripción indígena, Congreso de la República, elecciones, MOE, voto en blanco
Slug: voto-en-blanco-de-circunscripcion-indigena-habria-ganado-por-confusion-en-el-tarjeton
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revistaccesos] 

###### [14 Mar 2018] 

Luego de la jornada electoral que vivió el país el 11 de marzo, el voto en blanco fue el ganador en la circunscripción indígena. Ante esto, la Misión de Observación Electoral recordó que la Constitución establece que es necesario que se vuelvan a hacer estas elecciones y posiblemente el motivo por el que esto sucedió **se debe a que los tarjetones electorales “son confusos”**.

De acuerdo con Camilo Vargas, coordinador del Observatorio Político de la MOE, “lo que ocurrió con las curules indígenas es una situación compleja y según la Constitución Política, lo que ocurre cuando hay una victoria del voto en blanco es que se deben repetir las elecciones”. Para este caso, el voto en blanco **ganó con mayoría absoluta** “tanto en la circunscripción indígena en el Senado como en la Cámara de Representantes”.

### **Tarjetón electoral puede ser confuso** 

Vargas explicó que en Colombia, “las altas votaciones en blanco en la circunscripción indígena se viene dando desde el año 2006 y desde ese año el Consejo Nacional Electoral ha considerado que **el tarjetón puede ser confuso** desde que se rediseñó”. Si bien lo que busca el nuevo tarjetón el simplificar la información para los electores, lo que ha hecho es todo lo contrario. (Le puede interesar:["La decadencia ética que muestran los resultados de las elecciones en marzo en Colombia"](https://archivo.contagioradio.com/la-decadencia-etica-que-muestran-los-resultados-de-las-elecciones-de-marzo-en-colombia/))

Ante esto manifestó que “para la circunscripción nacional, el voto en blanco sale más arriba del voto en blanco para la circunscripción indígena” y esto puede generar confusión para quienes **desean votar en blanco para circunscripción nacional** pues terminan haciéndolo por la indígena. Por esto, para la MOE, “estamos ante un caso de negligencia del Congreso de la República que desde 2006 no ha reglamentado nada sobre el uso del tarjetón que sigue siendo muy confuso”.

Para la Misión Electoral es difícil comprender si “desde la circunscripción indígena **hay una manifestación de su electorado** en contra de las candidaturas que se presentaron, o si por el contrario se trata de una confusión y mucha gente que quiere votar en blanco en la circunscripción nacional, termina haciéndolo por la circunscripción indígena”.

### **Comunidades étnicas están en riesgo jurídico** 

Teniendo en cuenta lo anterior, Vargas recalcó que la situación de inseguridad jurídica “no es nueva y no es única de las circunscripciones indígenas”. Dijo que lo grave de la situación es que “**son todas las circunscripciones étnicas** las que se encuentran en un estado alto de inseguridad jurídica” pues, las curules actuales de las comunidades afrodescendientes, hay movimientos electos bajo reglas distintas, pero en la misma circunscripción.

Afirmó que en los últimos años “ha habido confusión frente a quiénes se pueden presentar bajo la circunscripción afrodescendiente y quién no y debería ser el Congreso de la República **quien reglamente esta situación**, pero no lo ha hecho”.

Es por esto que “lo que queda es que, por ejemplo, se reglamente vía tutela y que sean los jueces quien aclare las reglas de juego pues la normativa electoral atrasada puede estar comprometiendo derechos fundamentales como la pluralidad y la representación de las comunidades étnicas”.

Finalmente, el director del Observatorio Político indicó que es necesario que las instituciones gubernamentales **hablen del tema** “para que la solución esté dada en derecho y sea consensuada”, teniendo en cuenta que unas nuevas elecciones son muy costas y aún no se sabe si hay una viabilidad legal para llevarlas a cabo. Concluyó que es importante que las autoridades aclaren cómo se debe proceder en este caso.

<iframe id="audio_24464342" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24464342_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
