Title: Gobierno colombiano prepararía dos nuevos bloques para fracking
Date: 2017-02-22 11:19
Category: Ambiente, Voces de la Tierra
Tags: ANH, fracking, san martin
Slug: gobierno_prepara_fracking_colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/fracking-e1487780191287.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Al Jazeera]

###### [22 Feb 2017]

[Dos nuevos bloques se estarían tramitando para ser destinados a fracking en Colombia. Así lo denuncia la Corporación Defensora del Agua, Territorio y Ecosistemas, CORDATEC, que asegura que son bloques que se encuentran en trámite para la firma de contrato **adicional de Exploración y Producción de Yacimientos No Convencionales usando el Fracturamiento Hidráulico.**]

[Según la denuncia, se trata del bloque Cerrero, operado por la multinacional **Parex Resources** a través de su subsidiaria “AB Exploración y Producción BV”. Esta área comprende los municipios de Cabuyaro y Barranca de Upía en el departamento del Meta, y Paratebueno en Cundinamarca.]

[El segundo es el bloque VMM-2 de **Conocophillips y Canacol Energy**. Esta actividad afectaría los municipios de Aguachica, Gamarra, Río de Oro y San Martín en el Cesar, además de ese bloque también hace parte Puerto Wilches del departamento de Santander. Según CORDATEC, este bloque sería la segunda fase del proyecto VMM-3 que ya se viene explorando en San Martín.]

![denuncia\_fracking](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/denuncia_fracking-e1487780451970.jpg){.size-full .wp-image-36677 .aligncenter width="474" height="400"}

Pese a las advertencias de la Contraloría Delegada para Minas y Energía a la Agencia Nacional de Hidrocarburos, ANH, que había asegurado que de llevarse a cabo este proyecto habría graves impactos ambientales, el Gobierno Nacional hace caso omiso. [(Le puede interesar: El fracking llegó a Colombia)](https://archivo.contagioradio.com/hecho-fracking-llega-colombia/)

**“La ANH debería abstenerse de suscribir contratos para la explotación de yacimientos no convencionales”,** lo cual llevaría a “alteraciones negativas en el medio ambiente biótico, abiótico y socioeconómico, atribuido al desarrollo del proyecto y a la carencia de medidas de prevención, mitigación, corrección y compensación, cuya no aplicación de parte de los contratistas llevaría a la afectación de cuerpos de agua superficial, subterránea (de acuíferos) e impacte de igual medida el suelo y la biota del ecosistema en el área de exploración y producción”, había manifestado la Contraloría.

Asimismo, se denuncia que las comunidades que hacen parte de los territorios donde se piensa adelantar fracking, tampoco han sido consultadas sobre la realización de dichos proyectos, que desde CORDATEC identifican como **una grave amenaza a la implementación del Acuerdo de Paz.**

[Cabe recordar, que además de esos dos bloques, ya existen 7 contratos firmados en Colombia para exploración y explotación de Yacimientos No Convencionales mediante Fracking que comprenden alrededor de **70 municipios en 7 departamentos del país.** [(Le puede interesar: Este es el mapa del fracking en Colombia)](https://archivo.contagioradio.com/mapa-del-fracking-colombia/)]

###### Reciba toda la información de Contagio Radio en [[su correo]
