Title: Otra Mirada: Defensoría del Pueblo ¿una ONG más?
Date: 2020-08-05 13:53
Author: AdminContagio
Category: Nacional, Otra Mirada, Programas
Tags: Carlos Negret, Defensor del pueblo, Defensoría del Pueblo
Slug: otra-mirada-defensoria-del-pueblo-una-ong-mas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/defensoría-del-pueblo-e1474047687809.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Defensoría del Pueblo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este mes, el presidente Duque enviará a la Cámara de Representantes la terna para que nuevo defensor del pueblo sea elegido, reemplazando a Carlos Negret. Quien llegue a asumir el cargo en la defensoría del pueblo debe proteger, defender los derechos humanos de las comunidades, según la constitución de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, pese a ser la entidad encargada de velar por los derechos y garantías de los territorios, su presencia en las comunidades no es suficiente, por lo que se espera, pero también se pone en duda, que la terna seleccionada pueda hacer un trabajo transparente y comprometido como defensor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para analizar esta situación, participaron Liliana Vargas, abogada, coordinadora proyectos ATI y vocera de la plataforma DESCA, Alberto Yepes coordinador del observatorio de DD.HH. y DIH de la coordinación Colombia-Europa-Estados Unidos, William Salazar presidente de SINDHEP y Andrés Chica Durango, director de la fundación Cordobexia. (Le puede interesar: [UBPD pide protección para sitio de inhumación en El Copey (Cesar)](https://archivo.contagioradio.com/ubpd-pide-proteccion-para-sitio-de-inhumacion-en-el-copey-cesar/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esta charla, los invitados hablaron de cómo se ve desde las organizaciones el trabajo realizado por la defensoría. Además, qué está pasando al interior y al exterior de la misma y qué tan valiosas resultan las alertas tempranas cuando son atendidas a tiempo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Comentan lo que está pasando al interior de la defensoría con los trabajadores y qué ha producido el deterioro en la gestión de la defensoría. Por otro lado, explican cómo garantizar una defensoría del pueblo eficaz y qué riesgos habría si el nuevo defensor responde a intereses económicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Liliana Vargas también comenta que el perfil del defensor debe tener un compromiso con la construcción de la paz y generar confianza en la sociedad colombiana, y obviamente "debe tener una independencia frente a los partidos políticos y al gobierno mismo".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, insisten en que es una elección que debería interesar a todo mundo y debemos pronunciarnos para exigir que los siguientes cargos protegen a los territorios de la manera más transparente.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “El silencio no es una opción”
>
> <cite>Andrés Chica Durango. </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

(Si desea escuchar el programa del 31 de julio: [Otra Mirada: Legalización, es hora de discutirlo](https://www.facebook.com/contagioradio/videos/3713937161952931))

<!-- /wp:paragraph -->

<!-- wp:html -->  
https://www.facebook.com/watch/?v=3713937161952931  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
