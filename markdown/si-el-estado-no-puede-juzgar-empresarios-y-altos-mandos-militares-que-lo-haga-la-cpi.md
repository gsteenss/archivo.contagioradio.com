Title: Si Estado no juzga empresarios y mandos militares "que lo haga la CPI"
Date: 2017-03-24 13:39
Category: Nacional, Paz
Tags: colombia, DDHH, FARC, JEP, paz, víctimas
Slug: si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cpi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Panorama] 

###### [24 Mar. 2017] 

Ante las modificaciones realizadas al Proyecto de ley que regula el Sistema Integral de Verdad, Justicia, Reparación y No Repetición (SIVJRNR), varias organizaciones** expresaron su preocupación** y aseguraron que, **si el Estado no puede juzgar a empresarios y altos mandos en el posconflicto, la Corte Penal Internacional deberá hacerlo.**

Según la Federación Internacional de Derechos Humanos (FIDH), el Colectivo de Abogados José Alvear Restrepo (CAJAR) y el Comité Permanente por la Defensa de los Derechos Humanos (CPDH) **para lograr una paz justa y duradera se hace necesario desmontar el paramilitarismo.**

Además aseguran que “rechazamos la eliminación de la posibilidad de juzgar y sancionar la participación de terceros en la financiación de grupos paramilitares, como quedó establecido en el proyecto de ley aprobado por el Senado” ya que se considera que habría una violación de los tratados internacionales como el Estatuto de Roma. Le puede interesar: [Vergonzoso ausentismo en Cámara para aprobar conciliación de la JEP](https://archivo.contagioradio.com/ausentismo-congreso-jurisdiccion-de-paz/)

En el comunicado añaden que **"Si el Estado colombiano no puede garantizar la persecución y sanción de los particulares** que voluntariamente financiaron a grupos armados, ni de los más altos responsables de los crímenes cometidos por los grupos armados, **incluyendo las fuerzas armadas, la Corte Penal Internacional, deberá hacerlo conforme las facultades dadas por el Estatuto de Roma.” **Le puede interesar: [Del sinsabor de la aprobación de la JEP a la responsabilidad de la sociedad civil](https://archivo.contagioradio.com/del-sinsabor-de-la-aprobacion-de-la-jep-a-la-responsabilidad-de-la-sociedad-civil/)

El proyecto de ley (artículo transitorio 16), al que se refieren estas organizaciones, establece que la Jurisdicción Especial para la Paz – **JEP- solo podrá juzgar a los civiles cuya participación, haya sido activa o determinante en la comisión de crímenes de guerra y de Lesa Humanidad**, lo cual implica que sólo serán sancionados aquellos a quienes se les compruebe una relación directa entre su financiamiento y la comisión de un crimen específico.

De este modo, dice la comunicación, se **desconoce lo acordado en el numeral 32 del punto 5 sobre justicia del Acuerdo Final entre el Gobierno y las FARC** y la responsabilidad de los empresarios en las zonas de conflicto “quienes apoyaron el mantenimiento de grupos armados a sabiendas de los crímenes atroces cometidos por los mismos, en aras de obtener provecho económico”.

Recalcan así mismo, que son preocupantes las limitaciones que fueron establecidas con respecto a la responsabilidad de mando (artículo 23 transitorio), al establecer cuatro** condiciones concurrentes y que desdibujan lo establecido en el artículo 28 del Estatuto de Roma. **Le puede interesar: [Impunidad, la mayor preocupación de organizaciones de DDHH con la JEP](https://archivo.contagioradio.com/impunidad-mayor-preocupacion-de-organizaciones-37994/)

Por último manifiestan que “estas disposiciones además de **vulnerar los derechos de las víctimas, son un claro obstáculo al logro real de la justicia, la paz** y la conciliación en Colombia”. Le puede interesar: [Listado de posibles militares beneficiados con la JEP](https://archivo.contagioradio.com/min-defensa-entrego-listado-de-817-militares-que-quedaran-a-la-espera-de-jep/)

Estas son las 4 condiciones concurrentes a las que hacen referencias las organizaciones firmantes de la carta

1\. Que las conductas punibles hayan sido cometidas dentro del área de responsabilidad asignada a la unidad bajo su mando.  
2. Que el superior tenga la capacidad legal y material de emitir, modificar y hacer cumplir órdenes.  
3. Que el superior tenga la capacidad efectiva de desarrollar y ejecutar operaciones dentro del área donde se cometieron los hechos punibles.  
4. Que “el superior tenga la capacidad material y directa de tomar las medidas adecuadas para evitar o reprimir la conducta o las conductas punibles de sus subordinados.

[Comunicado FIDH, CAJAR y CPDH](https://www.scribd.com/document/342932519/Comunicado-FIDH-CAJAR-y-CPDH#from_embed "View Comunicado FIDH, CAJAR y CPDH on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_16557" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/342932519/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-kvwxbTA9qiWarsJ9bEp0&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
