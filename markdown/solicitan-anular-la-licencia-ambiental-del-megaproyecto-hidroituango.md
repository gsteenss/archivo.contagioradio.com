Title: Solicitan anular la licencia ambiental del megaproyecto Hidroituango
Date: 2017-03-29 17:49
Category: Ambiente, Voces de la Tierra
Tags: Hidroituango
Slug: solicitan-anular-la-licencia-ambiental-del-megaproyecto-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Hidroituango-no-es-paz-e1490827578825.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  Foto: Debate Hidroituango 

###### [29 Mar 2017] 

**El Movimiento Ríos Vivos de la mano del Colectivo de Abogados José Alvear Restrepo, interpuso este miércoles una demanda de nulidad ante el Consejo de Estado**, de la licencia ambiental otorgada al megaproyecto [Hidroituango](https://archivo.contagioradio.com/?s=hidroituango) de Empresas Públicas de Medellín, EPM, teniendo en cuenta afectaciones como el desvío del río Cauca, la tala indiscriminada de bosques, los hechos de corrupción, la destrucción de las economías campesinas de la región y la revictimización de poblaciones afectadas por el conflicto armado y la violencia sociopolítica.

La demanda se argumenta debido a la falta de participación efectiva de las comunidades afectadas, pero también a las irregularidades que se han presentado en todo el procedimiento ambiental, estipulado en la licencia otorgada a EPM por parte de ANLA.

Las irregularidades incluyen al menos **16 modificaciones a la licencia que han causado desalojos, contaminación y el desvío el cañón del río Cauca**, junto con otras violaciones a los derechos humanos de estas comunidades, víctimas del conflicto armado y del megaproyecto. Por hechos como esos, la ANLA ha suspendido provisionalmente la licencia ambiental a Hidroituango en tres ocasiones.

Hidroituango que se perfila como la hidroeléctrica más grande del país, que incluye obras como la construcción de un muro de 225 metros para un embalse de 79 kilómetros de extensión a lo largo de Cañón del Río Cauca, genera graves consecuencias como desplazamientos tanto de las comunidades como de animales que en medio del actuar de la empresa, pierden su hábitat por **la tala de 4000 hectáreas de árboles que pertenecen a ecosistemas como el bosque seco tropical** especialmente protegido por tratados internacionales y normatividad interna.[(Le puede interesar: EPM inició tala de 4.500 hectáreas de bosque seco tropical)](https://archivo.contagioradio.com/epm-inicio-tala-de-4-500-hectareas-de-bosque-seco-tropical/)

A su vez, los argumentos de la demanda también se refieren a la corrupción. Y es el pasado 22 de marzo en el Concejo Municipal de Medellín se llevó a cabo debate de control político al megaproyecto en el que se denunció que Camargo y Correa (firma brasileña involucrada en sobornos e incumplimientos dentro del llamado escándalo Odebrecht) junto con Conconcreto y Coninsa Ramón H, conforman el Consorcio CCC encargado de la construcción de la vía Puerto Valdivia a Zona de Presa que hace parte del megaproyecto. En el debate, **se denunció que el contrato del Consorcio CCC cuenta con 23 actas de modificación que han tenido adiciones por un valor de \$663.807 millones** que equivalen al 35% del valor inicial y prórrogas derivadas de incumplimientos en la licencia ambiental.

La violencia socio-política es otro de los temas que se tienen en cuenta en esta demanda. La población ha sido  desplazada, estigmatizada, perseguida, señalada e incluso varios líderes han sido asesinados. Es por ello que dentro de los argumentos se destaca la coincidencia territorial del área de influencia del megaproyecto con la ocurrencia de **54 masacres perpetradas por grupos paramilitares pertenecientes a las AUC, Bloque Mineros y Bloque Noroccidental** desde finales de los 90. En esa misma línea, El Movimiento Ríos Vivos y el Cajar recuerdan que el Estado colombiano fue condenado por las masacres de El Aro y La Granja, ocurridas en esta zona, y que la Sala de Justicia y Paz del Tribunal Superior de Medellín ordenó que se investigara la relación que existe entre estos crímenes y el desarrollo del proyecto ’Hidroituango’.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
