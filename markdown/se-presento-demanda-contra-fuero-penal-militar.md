Title: Se presentó demanda contra fuero penal militar
Date: 2015-07-15 15:52
Category: DDHH, Entrevistas, Judicial, Nacional
Tags: alirio uribe, Angela Maria Robledo, colectivo de Abogados José Alvear Restrepo, Coljuristas, demanda contra Fuero Penal Militar, Derecho Internacional Humanitario, Derechos Humanos, fuero penal militar, Iván Cepeda, Yomary Ortegón
Slug: se-presento-demanda-contra-fuero-penal-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/planton-fuero-penal-6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Coljuristas 

<iframe src="http://www.ivoox.com/player_ek_4837149_2_1.html?data=lZ2gmZaYfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRl8af0dfS1crSuIa3lIqvlZDIqc7Vz8nOjcjTstXmwpCz18rWs4zExtPOzpCxrc3d1cbfj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jomary Ortegón, Colectivo de Abogados José Alvear Restrepo] 

###### [15 de Julio 2015]

La Comisión Colombiana de Juristas, el Colectivo de Abogados José Alvear Restrepo y más de diez organizaciones defensoras de derechos humanos, víctimas, y algunos congresistas presentaron este miércoles ante la Coste Constitucional, **una demanda contra las reformas al fuero penal militar.**

“**Este no es un momento para plantear reformas que se imaginen a futuro un escenario de guerra**, esta reforma tiene una idea de que la guerra va a perdurar muchos años… las reformas deberían ser reformas para hacer viable la paz y no seguir pensando en la guerra”, es el fundamento de la demanda al que se refiere la abogada Jomary Ortegón, del Colectivo de Abogados José Alvear Restrepo

Una de las preocupaciones, es que por medio de una de las reformas al fuero penal militar, se modifica el artículo 221 de la Constitución, con lo que se pretende **desconocer los derechos humanos y que solo se aplique el Derecho Internacional humanitario.**

Además, una segunda preocupación es que en el Congreso de República también está en curso el **proyecto de Ley 129 que reinterpreta DIH,** por el que se legitimaría las violaciones del derecho a la vida y se justifica “matar”, lo que la abogada califica como “**DIH a la colombiana que justifica la muerte,** como si las muertes fueran costes que debemos asumir en aras de ganar la guerra”.

Durante la presentación de la demanda que también firmaron congresistas como Iván Cepeda, Alirio Uribe y Ángela María Robledo, se realizó un plantón frente a la Corte Constitucional donde se tuvo como mensaje principal **“No licencia para matar”,** expresándole a la Corte  que los colombianos y colombianas se preparan para la paz y no para la guerra.

[Demanda vs Acto Legislativo 01 de 2015](https://es.scribd.com/doc/271689911/Demanda-vs-Acto-Legislativo-01-de-2015 "View Demanda vs Acto Legislativo 01 de 2015 on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_97043" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/271689911/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-OIvSAVhkCOYN5gE99wMC&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6514822848879248"></iframe>
