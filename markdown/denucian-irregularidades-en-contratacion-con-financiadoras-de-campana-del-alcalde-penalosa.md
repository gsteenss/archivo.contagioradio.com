Title: Denuncian irregularidades en contratación con financiadoras de campaña del Alcalde Peñalosa
Date: 2017-07-10 17:17
Category: Judicial, Nacional
Tags: corrupción, Enrique Peñalosa
Slug: denucian-irregularidades-en-contratacion-con-financiadoras-de-campana-del-alcalde-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Enrique_Peñalosa-e1488922778412.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular] 

###### [10 Jul 2017] 

Manuel Sarmiento, concejal del Polo Democrático por Bogotá, denunció prácticas corruptas en las que habría incurrido el alcalde de Bogotá, Enrique Peñalosa, al otorgar contratos a personas u organizaciones que realizaron donaciones cuando se encontraba en campaña electoral, **que podrían sumar más de 50 mil millones de pesos**.

### **¿Quiénes están detrás de las contrataciones?** 

Las empresas y personas involucradas, según Sarmiento son: Jesús Acosta Constructora, que habría donado a la campaña 20 millones de pesos y que recibió un contrato por **297 millones de pesos, en el Fondo de Desarrollo local de los Mártires. **(Le puede interesar:["Las tres razones por las que se cayó el proyecto de valorización de Peñalosa")](https://archivo.contagioradio.com/las-3-razones-por-las-que-se-cayo-proyecto-de-valorizacion-de-penalosa/)

La empresa Luis Lozano: Terrafranco y Flores Ipanema, **donó 40 millones a la campaña y recibió dos contratos**: uno por 471 millones de pesos de la Secretaría de Seguridad y otro por 744 millones de pesos con la Secretaría de Planeación.

Finalmente, esta la empresa Daniel Sarmiento Comercializadora PNS, que donó 100 millones de pesos a la campaña de Peñalosa y **habría recibido un contrato de 6 mil millones de la UAESP** (Unidad Administrativa Especial de Servicios Públicos).

De acuerdo con Sarmiento el Peñalosa estaría incurriendo en dos tipos de actos corruptos, el primero de ellos es entregarle a dedo contratos a quienes le financiaron la campaña, “es una forma como los **politiqueros recompensan a quienes les entregaron recursos para la campaña con cuantiosos contratos**” además agregó que Peñalosa estaría diciendo mentiras, porque habría expresado que él no ha recibido dineros de contratistas del distrito, mientras que esta información demostraría lo contrario.

En segundo lugar, Sarmiento aseveró que hay un problema legal debido a que la ley permite que haya financiación de empresas privadas a las campañas, bajo ciertas restricciones, una de ellas es que quien aporte más del 2% del total de la financiación a un determinado alcalde no puede después, contratar en esa alcaldía. **Daniel Sarmiento Comercializadora PNS donó más del 2%, quedando inhabilitado para ser contratista**.

### **La puerta giratoria de la corrupción** 

Sarmiento alertó que posiblemente podría haber tres contratos de alianzas público privadas sobre el espacio público de Bogota que serían otorgados de la misma manera, y agregó que sería Mario Fernando Pinzón, quien donó 50 millones a la campaña de Peñalosa, la persona interesada en estos contratos. (Le puede interesar: ["Proceso de revocatoria no se ha suspendido: Comité Revoquemos a Peñalosa"](https://archivo.contagioradio.com/proceso-de-revocatoria-no-se-ha-suspendido-comite-revoquemos-a-penalosa/))

Sumado a estos actos de corrupción, el concejal también denunció que Peñalosa contrató como su principal asesor en materia de movilidad a Óscar Edmundo Díaz, quien, de acuerdo con Sarmiento, antes mantenía cercanas relaciones con la empresa Recaudo Bogotá, **uno de los negociantes de Transmilenio, lo que evidenciaría un conflicto de intereses**.

La Secretaría de Habitad también habría realizado una contratación con una empresa perteneciente a Miguel Sandoval, primer gerente de Transmilenio durante el primer mandato de Peñalosa, para que realizará estudios sobre Ciudad Río; **contrato que no se habría celebrado por concurso de méritos o licitación, sino a través de un convenio con la Financiera de Desarrollo Nacional que contrata a la empresa de Sandoval**.

“Esto demuestra como el alcalde Peñalosa **viene contratando a sus compadres y a quienes le aportaron recursos a la campaña** para recompensarlos y todo esto le genera un inmenso daño a la ciudad” manifestó Manuel Sarmiento.

El concejal expresó que su próxima acción será solicitarle a la Personería de Bogotá que investigue esos contratos y las irregularidades que se han presentado, de igual forma señaló que citará un debate de control político en el Consejo, además aseguró que estos hechos deben darle más argumentos a la ciudadanía para **“darse cuenta del pésimo gobierno que favorece a un reducido grupo de personajes**, cercano a la alcaldía de Peñalosa y que afecta a la inmensa mayoría de los ciudadanos”.

<iframe id="audio_19723542" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19723542_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
