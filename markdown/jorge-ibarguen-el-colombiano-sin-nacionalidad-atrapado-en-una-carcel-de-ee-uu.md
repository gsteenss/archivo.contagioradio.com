Title: Jorge  Ibargüen, el colombiano sin nacionalidad atrapado en una cárcel de EE.UU
Date: 2018-02-06 16:59
Category: DDHH, Nacional
Tags: colombia, Estados Unidos, Prisionero Político
Slug: jorge-ibarguen-el-colombiano-sin-nacionalidad-atrapado-en-una-carcel-de-ee-uu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/carcel-colombia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cronista Diario] 

###### [06 Feb 2018] 

Pese a haber cumplido su condena en una cárcel de Estados Unidos, Jorge Ibargüen, es un colombiano atrapado en ese país, que aún no puede retornar a Colombia debido que no tiene un documento de identificación que le dé la nacionalidad. De acuerdo con el abogado Diego Martínez, **como no existió la prestación de servicios consulares se podría estar violando el derecho fundamental a tener un lugar de origen**.

“A él lo condenan sin saber quién es, es decir, **las autoridades norteamericanas lo condenan sin tener plena identificación**” afirmó Martínez, y agregó que continúa en la cárcel porque ninguna autoridad, ni de Estados Unidos, ni la colombiana quieren proporcionarle su documento.

Ibarg*ü*en fue condenado a 14 años de prisión, tras ser capturado en la frontera entre Colombia y Panamá, cuando hacía parte de las filas de la entonces guerrilla FARC-EP. En ese momento, él aseguró que **nunca fue registrado por su familia cuando nació debido a que vivía en el departamento del Chocó** y desde muy temprana edad ingresó a esta organización.

Para el abogado Martínez de no resolverse prontamente esta situación, se estaría violando el derecho fundamental a tener una nacionalidad, y denota la ineficiencia de los servicios consulares sobretodo en el caso de Jorge que c**ontinúa pagando una pena privativa de su libertad por cuenta de la falta de acciones del gobierno colombiano**.

Martínez afirmó que tanto Colombia como Estados Unidos hacen parte de la Convención de Apátridia, en donde lo propio es que alguno de los dos países le proporcione un documento temporal de identificación a Ibarg*ü*en para que retorne a su país y legalice su situación jurídica.

<iframe id="audio_23591620" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23591620_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
