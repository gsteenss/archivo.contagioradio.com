Title: 4 mil millones es déficit de desfinanciación de la Universidad de Caldas.
Date: 2017-05-27 11:26
Category: Educación, Nacional
Tags: Crisis Educación Universitaria, Universidad de Caldas
Slug: 4-mil-millones-es-deficit-de-desfinanciacion-de-la-universidad-de-caldas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/universidad_de_caldas11-e1495902333503.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [27 May 2017] 

Los empleados, doncentes y trabajadores de la Universidad de Caldas se encuentran en asamblea permanente debido a que afirman que sus condiciones salariales son inferiores al resto de las de las universidades públicas del país, con un déficit de **4 mil millones de pesos en planta de funcionamiento,** razón por la cual exigen la formalización de los contratos de los trabajadores en la institución.

Gabriela Marín, trabajadora de la Universidad e integrante de la Asociación Sindical de Funcionarios de la Universidad de Caldas (ASFUC) señala que la universidad tiene una planta de cargos que en este momento cuenta con pocos empleados profesionales en carreras administrativas, **las vinculaciones se están realizando bajo la modalidad provisional y las plantas de trabajadores que quedan son temporales.**

Para Javier Sepúlveda Ríos, presidente de APUC Caldas, esta problemática es producto de la des financiación actual que afrontan las universidades públicas del país “hemos recibido un informe por parte de la administración de la Universidad en donde tenemos **un déficit de cerca de 4 mil millones de pesos solamente en planta de funcionamiento**” afirmó. Le puede interesar:["Docentes se preparan para movilización Nacional el 31 de mayo"](https://archivo.contagioradio.com/31-de-mayo-dia-de-movilizacion-nacional-de-docentes/)

El programa Ser Pilo Paga, de acuerdo con Sepúlveda, estaría contribuyendo en la crisis que vive la universidad “**está básicamente apoyando la sustentación de las universidades privadas** y realmente a las universidades estatales no les están llegando los recursos”. Le puede interesar:["U.Nacional en crisis por déficit presupuestal de 118 mil millones de pesos"](https://archivo.contagioradio.com/u-nacional-tiene-un-deficit-de-presupuesto-de-118-mil-millones-de-pesos/)

Las condiciones salariales de los docentes no son mucho mejores que la de los demás empelados de la institución, Camilo Díaz Fajardo, presidente de ASPU manifiesta que en primera medida tienen un salario mínimo, que se suma a la creación de la figura de ocasionalidad, que ha provocado que **docentes sean contratados esporádicamente por un año o periodos de tiempo más cortos, sin tener estabilidad económica**.

Los trabajadores han asegurado que continuarán en asamblea permanente y tendrán diferentes actividades en la universidad para explicar el pliego de exigencias a cada uno de los sectores que conforman la comunidad universitaria.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
