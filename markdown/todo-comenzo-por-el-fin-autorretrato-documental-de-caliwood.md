Title: "Todo comenzó por el fin" autorretrato documental de Caliwood
Date: 2015-08-19 15:47
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Andrés Caicedo, Carlos Mayolo, Festival de cine de Toronto TIFF, Luis Ospina, Todo comenzó por el fin documental
Slug: todo-comenzo-por-el-fin-autorretrato-documental-de-caliwood
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/por-el-fin1-e1440017433807.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [19, Ago, 2015] 

Ver el trailer de "Todo comenzó por el fin", documental dirigido por Luis Ospina ( Pura sangre, 1982), es viajar en el tiempo. Es el retorno a esos años en los que, junto a Carlos Mayolo y Andrés Caicedo, respiraban y vivían cine, música, teatro y literatura, en una ciudad como la Cali de los años 70 y parte de los 80, donde la rumba fuerte y los excesos acompañaron la explosión inédita de genialidad y talento surgida en esos días.

Es el autorretrato de Caliwood, apelativo con el que se conoce al celebre grupo de jóvenes cinéfilos, que entre 1971 y 1991, produjeron material que hoy hace parte fundamental de la cinematografía colombiana como "Agarrando pueblo", 1977 (Ospina-Mayolo), "Angelita y Miguel Angel"(inconcluso) (Mayolo-Caicedo), "Carne de tu carne", 1983 y "Pura Sangre" , 1982, entre otras producciones argumentales, documentales en corto y largometraje, piezas teatrales y espacios de televisión.

El pasado 11 de Agosto, "*Todo comenzó por el fin*", fue anunciado como uno de los largometrajes que harán parte de la selección oficial del 40 Festival Internacional de Cine de Toronto TIFF, dentro del programa 'Midnight Madness', siendo en esta oportunidad el único largometraje colombiano que participará en esta edición que tendrá lugar entre el 10 y el 20 de septiembre próximos.

Según el director Luis Ospina, "Hacer el estreno mundial de mi película en el Festival Internacional de Toronto es para mí un gran privilegio ya que el TIFF es uno de los más importantes festivales del mundo, que este año cumple 40 años de presentar buen cine. Además es un festival que no solo exhibe las grandes producciones del cine mundial sino también películas arriesgadas y muy personales de países con una incipiente cinematografía, como es el caso de Todo comenzó por el fin* *".

La participación del documental en el TIFF, representa además su estreno mundial en dos proyecciones los días 13 y 16, donde optará además por el premio Grolsch People's Choice Documentary Award, galardón que se le otorga al mejor documental escogido por el público del festival.

<iframe src="https://player.vimeo.com/video/133096649" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

(Con información de [Velvet Voice](http://www.lbv.co/velvet_voice/todocomenzo/001_tcpef_toronto.html))

[TCPF Trailer ESP Toronto](https://vimeo.com/133096649) from [Luis Ospina](https://vimeo.com/luisospina) on [Vimeo](https://vimeo.com).
