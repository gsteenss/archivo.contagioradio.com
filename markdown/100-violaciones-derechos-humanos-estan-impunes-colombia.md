Title: 100% de violaciones a Derechos Humanos están impunes en Colombia
Date: 2016-12-08 11:29
Category: DDHH, Entrevistas
Tags: Comisión Colombiana de Juristas, Cooperación colombia europa, impunidad en Colombia, ONU, Violaciones de DDHH
Slug: 100-violaciones-derechos-humanos-estan-impunes-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Defensores-Asesinados-e1477508845527.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [7 Dic 2016] 

La **Coordinación Colombia Europa,** plataforma que agrupa a más de 200 organizaciones, realizó el  lanzamiento del Informe Alterno presentado al **Comité de derechos humanos de ONU** en el que se revela la situación que vive el país en esta materia.

En el documento presentado inicialmente en Ginebra, se revela que el 100% de los casos de amenazas y hostigamientos a defensores de derechos se encuentran en impunidad, las vulneraciones a personas LGBTI no han sido consideradas en los informes del Estado colombiano y el desmonte del paramilitarismo no se ha hecho efectivo.

**Ana María Rodríguez, integrante de la Comisión Colombiana de Juristas** y quien fue una de las expositoras del informe, comentó que expertos de la ONU preguntaron “por qué temas que ya habían sido objeto de examen y de recomendaciones hace 5 y 10 años siguen siendo temas en el nuevo examen y **porque la respuesta del Estado sigue siendo la misma después de 5, 10 y 15 años”.**

La jurista indicó que el organismo internacional “hizo reclamos muy sentidos al Estado colombiano **por entregar insuficiente información y no recolectar datos concretos sobre el estado y el número de casos que se adelantan por violaciones de derechos humanos”.**

Por otra parte, Rodríguez afirma que hubo felicitaciones por el trabajo hecho por las organizaciones defensoras de los derechos humanos "Naciones Unidas se dieron cuenta que se producía mucha información por parte de la sociedad civil (…) reconocieron que **los informes de la sociedad civil tienen más información  y mejor organizada que la proporcionada por el Estado”.**

**De las 38 recomendaciones y observaciones propuestas, 37 fueron acogidas** por el ente internacional, lo que califica la jurista como positivo ya que estas recomendaciones fueron elaboradas por organizaciones de víctimas campesinas, afro e indígenas, a nivel regional y por organizaciones LGBTI.

Sobre el tema de garantías de seguridad para defensores y líderes sociales, Rodríguez afirma que **“preocupa la creación de muchos mecanismos para tratar un mismo tema y hasta el momento ninguno ha dado una respuesta efectiva** (…) el Estado ofrece medidas reactivas y no preventivas”.

Por último otros de los temas trabajados en el informe son la persistencia de batidas y reclutamiento forzado a manos de las fuerzas militares, las detenciones arbitrarias, el derecho a la objeción de conciencia y la **grave situación de hacinamiento y tratos indignos en las cárceles.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
