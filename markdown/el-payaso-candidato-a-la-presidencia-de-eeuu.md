Title: El payaso candidato a la Presidencia de EEUU
Date: 2015-08-12 11:00
Category: Opinion, Schnaida
Tags: Donald Trump, Estados Unidos, presidencia EEUU, Procurador Ordóñez
Slug: el-payaso-candidato-a-la-presidencia-de-eeuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/donald-trump1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [**[Ian Schnaida](https://archivo.contagioradio.com/ian-schnaida/) - [[~~@~~**IanSchnaida**] 

###### [12 Ago 2015] 

A Donald Trump lo han tachado de xenófobo, racista, homófobo y misógino, y todos esos calificativos se quedan cortos; hay que agregar el de cobarde, pues no puede llamarse de otro modo a quien insulta a alguien en público y cuando tiene la oportunidad de pedir perdón, solo se escucha la arrogancia de quien cree tener el mundo en su cuenta bancaria.

Resalta en él el descaro de quejarse públicamente de ser malinterpretado por las arpías de los medios, y toda la multitud de latinos estúpidos a los cuales no nos alcanza el coeficiente para entender su mensaje de paz mundial.

Su estrategia es salirse del político común y mostrar que no tiene pelos en la lengua para contar las barbaridades que se le pasan por la mente —y habrá con quienes funcione—; pero con Trump como Presidente de EEUU ahí sí ese país volvería a ser ejemplo de valores y tradiciones, donde los matrimonios sean para parejas heterosexuales, los negros deban orinar en baño aparte y donde América sea para los americanos. Sería como Venezuela, donde la gestión pública de Maduro ha liberado al país del capitalismo, de la harina, de la leche, del aceite, del papel higiénico...

Trump es la viva imagen de esos empresarios que se benefician del trabajo de los inmigrantes ilegales, y que por eso no les conviene que se legalicen como americanos, porque habría que pagarles un sueldo con sus prestaciones y obligaciones legales, y todas esas cosas tan incómodas y tan caras.

Es que si hay algo peor que un retrógrado con plata, es uno que esté metido en política —y no me refiero al Procurador Ordóñez—. Esos que utilizan el dinero para comprar las intenciones que no tienen como ganarse intelectualmente, porque no son sujetos políticos con valor, sino con dinero.

Me causa pena ver a Trump actuar su papel de bufón que entretiene a la audiencia con comentarios tan descabellados o salidos de tono que nadie los espera; pero no me deja tranquilo el hecho de verlo posicionarse como una opción tanto en el Partido Republicano como candidato independiente.

Sólo imaginen, si un político no es capaz de manejar los comentarios que no le gustan de una presentadora de televisión, ¿qué sucedería si Putin, Hollande o Castro lo contrariaran? Es capaz de iniciar la III Guerra Mundial por una diferencia personal.

¿Representa realmente este señor los intereses y pensamientos de los estadounidenses? Porque muchos de ellos parecen validar los improperios que salen de su boca como una alcantarilla desembocada, a través de miles de correos que recibe apoyando su discurso.

Entonces, odiado o no, cuenta con el apoyo de personas reales que votarían por él, que apoyan el racismo, la discriminación, la homofobia y un país para los pura sangre, no para la escoria que llega del tercer mundo a aprovecharse del Maná que baña el país.

Amanecerá y veremos cuántos Donald Trump hay en Estados Unidos.
