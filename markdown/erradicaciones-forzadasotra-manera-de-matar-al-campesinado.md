Title: Erradicaciones forzadas son otra manera de matar al campesinado
Date: 2016-11-21 13:23
Category: Ambiente, Nacional
Tags: campesinos, Erradicaciones Forzadas, López de Micay
Slug: erradicaciones-forzadasotra-manera-de-matar-al-campesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Erradicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [21 Nov. 2016] 

Desde el pasado 12 de Noviembre los habitantes del Municipio de López de Micay en el departamento del Valle del Cauca, han denunciado que **hay fuertes desembarcos de tropas militare que han iniciado erradicación forzada de cultivos de uso ilícito a pesar de los acuerdos previos hechos con las comunidades.**

**Son cerca de 500 efectivos los que hacen presencia en la zona, quienes además, según la denuncia de la comunidad, están acabando con las siembras de Pan Coger.**

Según Killian, Párroco de la Iglesia de López de Micay, las comunidades se han dirigido a ellos como iglesia para manifestarles su preocupación ante estos hechos **“las comunidades expresan que también se les está cortando el plátano, les están dañando las fincas de pancoger**, entonces las comunidades están muy preocupadas”.

Aunque los líderes de la zona han intentado mediar, para poder conseguir una solución, pues han calificado de “injusto” que les dañen los cultivos, el Gobierno nacional no les ha dado una solución efectiva **“ellos – los campesinos- están de acuerdo con que se erradique porque es el deber del estado pero ¿nos van a dejar morir de hambre? ¿Qué van a hacer con nosotros?”** manifiesta el Padre frente a las preocupaciones de la comunidad.

Según el párroco, de continuar estas erradicaciones la realidad que se avecina para el municipio de López de Micay es difícil “ahí estamos preocupados nosotros también” dijo. Le puede interesar: [Erradicaciones forzadas de coca no contribuyen a la paz](https://archivo.contagioradio.com/campesinos-del-catatumbo-dicen-no-a-erradicacion-forzada-de-coca/)

Ante esta situación, la comunidad se encuentra en estado de alerta **“la gente está desesperada, no sabe qué hacer en qué va a parar todo el tema de las erradicaciones”** afirmó.

Y agregó que la gente está dispuesta a hacerse matar “**se van a meter –a los cultivos- con palos y están dispuestos a morir, dicen ellos, si el gobierno sigue erradicando. Están dispuestos a enfrentarse con la fuerza pública porque no les están dejando opciones de trabajo”.**

Según cifras de la ONU, para Julio de 2016 en el país habían 96.000 hectáreas de cultivos de coca. Contenido relacionado [Ejército continúa la erradicación forzada en Putumayo](https://archivo.contagioradio.com/ejercito-continua-erradicacion-forzada-de-coca/)

<iframe id="audio_13865285" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13865285_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
