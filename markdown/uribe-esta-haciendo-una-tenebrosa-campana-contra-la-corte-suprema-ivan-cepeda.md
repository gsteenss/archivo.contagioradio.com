Title: "Uribe esta haciendo una tenebrosa campaña contra la Corte Suprema" Iván Cepeda
Date: 2018-07-31 17:33
Category: Paz, Política
Tags: Álvaro Prada, Alvaro Uribe, Fernando Londoño, Iván Cepeda
Slug: uribe-esta-haciendo-una-tenebrosa-campana-contra-la-corte-suprema-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/uribe-y-cepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 Jul 2018]

El senador Iván Cepeda, del Polo Democrático, afirmó en una rueda de prensa que continuará el proceso sobre los falsos testigos, sin "manipular a la opinión pública". De igual forma ** señaló que Uribe estaría llevando acabo una "campaña tenebrosa" contra la Corte Suprema de Justicia y denunció el riesgo en el que están tanto Monsalve y su familia, como él y su equipo de trabajo. **

### **La campaña de Uribe contra la Corte Suprema de Justicia **

Según Iván Cepeda, es mentira que Álvaro Uribe respete o acate las decisiones de la Corte Suprema de Justicia. Para el senador, mientras Uribe afirma, por un lado, atender los llamados de la Corte, por el otro, esta realizando una "**tenebrosa campaña sucia** en contra del magistrado Barceló y de los magistrados que hacen parte de la sala segunda de la Corte Suprema de Justicia".

Uno de los episodios que, de acuerdo con Cepeda, hacen parte de esa campaña, se encuentra relacionados con un informe de contrainteligencia filtrado por un portal llamado "El Expediente" y el programa "Hora de la verdad" de Fernando Londoño, en el que se afirma que el presidente de la República, Juan Manuel Santos, el vicepresidente, Oscar Naranjo, el magistrado Barceló, otro magistrado más e Iván **Cepeda estuvieron reunidos para entregarle un soborno, proveniente de la FARC, a los magistrados de la Corte por 5 millones de dólares**.

En ese sentido, Cepeda señaló que en diálogo con el Ministerio de Defensa pudo establecer que no existe ningún archivo de contrainteligencia que pueda sustentar esas afirmaciones. Razón por la cual, la defensa del senador Cepeda **iniciará acciones en contra de Fernando Londoño y Rugeles, encargado del portal "El Expediente**.

Cepeda también recalcó que, en un principio fue Uribe quién recurrió a la Corte Suprema cuando instauró la demanda en contra suya, y que en ese entonces no desacreditaba las facultades de ese órgano, "pero cuando los magistrados comenzaron a hacer una investigación rigurosa, que **demostró que el que presuntamente esta detrás de la manipulación de testigos, es el expresidente, comenzó la campaña**".

### **El Aservo Probatorio contra Uribe y Prada** 

Frente al conjunto de pruebas que se han recaudado en este proceso, denominadas aservo probatorio, que según Cepeda han intentado ser desvirtuadas en la opinión pública, el senador manifestó que ha existido toda una rigurosidad detrás de la recolección de las mismas y que no consisten solamente en una carta o mensajes de whatsapp como se ha mencionado **sino en más de 200 horas de grabaciones de interceptaciones a los principales actores de estos hechos. **(Le puede interesar:["¿Quién debe investigar a Álvaro Uribe Vélez?"](https://archivo.contagioradio.com/quieninvestigaraaalvarouribe/))

El abogado defensor de Cepeda, Reinaldo Villalba, recordó que la decisión del 16 de febrero de la sala Penal, fue la que determinó la compulsa de copias para que fuera investigado el senador Uribe. En esa medida, Villaba aseguró que "esta claro que Iván Cepeda fue víctima de un montaje, **en donde participaron un número de personas del circulo más cercano de Uribe".**

### **Las amenazas a Jaun Guillermo Monsalve y su familia** 

El senador del Polo Democrático denunció que en los últimos días, se ha puesto en marcha una campaña de amenazas y hostigamientos a Juan Guillermo Monsalve y su familia, que han llevado a que su compañera, Deyanira Gómez, saliera del país.

Frente a Monsalve, Cepeda expresó que este tuvo que ser trasladado de la cárcel en la que se encontraba debido a las múltiples presiones de las que era víctima, **mientras que su compañera ha denunciado seguimientos y un atentando en contra de su vida. **

Además recordó que la Corte Suprema, en el auto de febrero, ordenó medidas especiales de protección sobre varios testigos en procesos contra Uribe, uno de ellos **Areiza, asesinado el pasado 16 de abril, en un atentado contra su vida**. Asimismo, Cepeda afirmó que tanto él, como su familia y unidad de trabajo también han sido víctimas de persecuciones y hostigamientos. (Le puede interesar: ["Corte Suprema abre investigación contra Álvaro Uribe y Álvaro Prada")](https://archivo.contagioradio.com/corte-suprema-abre-investigacion-formal-contra-alvaro-uribe-y-alvaro-prada/)

En esa medida, Cepeda señaló que llevará este caso a la Comisión Interamericana de Derechos Humanos y que pedirá una visita del relator especial de la Naciones Unidas para la independencia de los jueces y abogados, practique una visita en Colombia para que constate la **"persecución en contra de los magistrados"**.

###### Reciba toda la información de Contagio Radio en [[su correo]
