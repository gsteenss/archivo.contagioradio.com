Title: Capturas de líderes del Congreso de los Pueblos podrían declararse ilegales
Date: 2017-03-23 11:29
Category: Judicial, Nacional
Slug: capturas-de-lideres-sociales-del-congreso-de-los-pueblos-podrian-declararse-ilegales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [23 Mar 2017]

Durante la jornada de hoy se realiza la audiencia de legalización de captura de las 12 personas detenidas ayer en la región del Sur de Bolívar, entre ellas la de las capturas de **Isidro Alarcón, Francisco Zabaleta y Milena Quiroz**, integrantes del Congreso de los Pueblos, se espera que la defensa pueda demostrar su inocencia y lograr que sean liberados de manera inmediata.

Hacia las 10: 30 de la mañana se estaba iniciando la audiencia de legalización de los hallanamientos y el primer detenido sobre el que se trabajaría es Isidro Alarcón a quién se le imputaría el delito de **concierto para delinquir agravado**.

La diligencia se realiza ante el Juez segundo municipal con funciones de control de garantías y es solicitada por la fiscalía tercera especializada de Cartagena.

### **Los capturados eran figuras públicas** 

Según explica Zoraida Hernández durante la audiencia de legalización de captura se espera que estas personas recobren la libertad, puesto que los tres miembros del Congreso de los Pueblos no tendrían responsabilidad en cargos como el que se **pretende imputar de ser integrantes de redes de apoyo del ELN**, Tampoco se conoce si les fueron mostradas las ordenes de captura en el procedimiento. Le puede interesar: ([Así fueron las captras de integrantes del Congreso de los Pueblos](https://archivo.contagioradio.com/capturas-congreso-de-los-pueblos/))

Además, en el caso de Milena Quiróz, Zoraida Hernandez señala que es una líder muy reconocida, que ha participado de diversos espacios de interlocución con el gobierno nacional y que **ha representado a las organizaciones de su región en escenarios como la Cumbre Agraria**. ([Lea todo sobre la Cumbre Agraria](https://archivo.contagioradio.com/?s=cumbre+agraria))

En ese sentido si hubiese tenido un requerimiento por parte de los organismos de justicia se hubiese podido realizar la captura en circunstancias diferentes, **puesto que es una persona de la vida pública.**

A esta situación se suma la reciente ola de amenazas y atentados contra integrantes de esa organización que el pasado mes de febrero denunciaron el atentado contra **Alfonso Barón en Valledurpar, las amenazas de muerte contra Daniel Ulcue en Cauca y el atentado contra Carlos Julio Trujillo en el municipio de Agustin Codazzi en Cesar.**

Una de las exigencias del Congreso de los Pueblos es que las tres personas capturadas en medios de los operativos masivos recobren la libertad de manera inmediata, que se garantice su derecho a la defensa y que **cesen las actividades de criminalización de los líderes de regiones** como el Sur de Bolívar pero también en todo el territorio nacional.

<iframe id="audio_17729685" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17729685_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
