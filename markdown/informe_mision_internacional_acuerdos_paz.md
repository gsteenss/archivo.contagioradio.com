Title: "Territorios continúan sin saber qué es la paz": Misión internacional en Colombia
Date: 2018-02-07 17:24
Category: Nacional, Paz
Tags: acuerdo de paz, Misión Internacional en Colombia, PBI
Slug: informe_mision_internacional_acuerdos_paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/marcha-de-las-flores101316_160.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [7 Feb 2018] 

“En los territorios la paz no se siente, la esperanza se mantiene”, es la conclusión del informe que realizó la misión internacional para analizar la implementación del Acuerdo de Paz en la que participaron la Fundación Mundubat junto a Brigadas Internacionales de Paz. Allí, dan cuenta de algunos avances en materia de implementación pero a su vez, señalan los **numerosos incumplimientos por parte del gobierno nacional.**

En la misión participaron un total de 10 personas expertas internacionales en Derechos Humanos, Personas Defensoras, Construcción de Paz y Enfoque de Género, entre otras, que visitaron **Nariño (Tumaco), Valle del Cauca (Buenaventura), Cauca (La Elvira), Chocó (Quibdó) y Urabá (cuencas del Jiguamiandó, Curvaradó y Cacarica),** para hablar directamente con las comunidades y evidenciar la situación que se vive en los territorios.

La misión encontró que pese a las expectativas que despertó el Acuerdo de Paz, estas están lejos de verse cumplidas ya que las poblaciones muestran mucha desilusión debido a la falta de cumplimiento del acuerdo, especialmente en tres aspectos: las garantías de seguridad, las garantías de participación política y reincorporación de excombatientes así como el Sistema Integral de Verdad, Justicia, Reparación y Garantías de No Repetición.

### **Garantías de seguridad** 

Una de las principales preocupaciones tiene que ver con los más de 200 asesinatos de personas defensoras de derechos humanos o con liderazgo social, así como también la permanencia del uso de la “violencia sexual como medida de control sobre la población, en la disputa que pueda tener lugar entre actores armados por el reposicionamiento y el dominio territorial", ya que en los 10 primeros meses del 2017, se registraron 361 casos de violencia sexual en el marco del conflicto armado.

Frente a la Comisión Nacional de Garantías de Seguridad, la misión asegura que esta no parece haber avanzado en su objetivo, ya que s decir dicha **Comisión aún no cuenta con un plan de trabajo claro y transparente para el desmantelamiento de las organizaciones criminales incluyendo los neoparamilitares.**

Otro de los puntos que se mencionan, tiene que ver con lo tarde que puesto en marcha el Cuerpo de Elite de la Policía Nacional, que además "no ha tenido resultados contundentes hasta el momento", así como también sucede con la Unidad Especial de Investigación de la Fiscalía creada por decreto Ley en mayo 2017.

De igual forma la misión señala que tampoco hay mayores avances el nuevo Sistema de Prevención y Alerta que debería dar a la Defensoría del Pueblo autonomía y recursos para prevenir agresiones en contra de líderes sociales.

### **Garantías de participación política y reincorporación de excombatientes** 

Pese a que la Misión internacional reconoce los avances con la aprobación del Estatuto de la Oposición Política mediante el cual se aseguran derechos para los partidos de oposición al gobierno, asegura que sigue existiendo cierto nivel de inseguridad jurídica puesto que existen varios casos de excombatientes detenidos a pesar de haber recibido sus certificados de amnistía y/o indulto; además de y alto nivel de inseguridad física.

En esa línea llaman la atención sobre los vacíos existentes en la reincorporación en materia socioeconómica, pues pudieron constatar que los excombatientes no han recibido ni la formación ni los recursos adecuados para poder empezar una vida como civil.

En cuanto al punto de participación política ciudadana, la misión manifiesta su rechazo a la decisión tomada en el Congreso de la República de no dar paso al proyecto de Ley que buscaba crear las Circunscripciones Transitorias Especiales de Paz, para las víctimas del conflicto. Y finalmente sobre este tema, e**xpresan su preocupación frente las garantías para la protesta social,** debido a las agresiones por parte de la Fuerza Pública hacia quienes se manifiestan.

### **Las víctimas en el centro de los acuerdos** 

En este aspecto, especialmente sobre la Justicia Especial para la Paz, los dos temas que más preocupan a la Misión, tienen que ver las modificaciones que se incorporaron para  inhabilitar a las personas defensoras de derechos humanos para los cargos de magistrado, y imposibilidad de que obligatoriamente terceros civiles, como empresarios, agentes del Estado no miembros de las Fuerzas Públicas por ejemplo, comparezcan ante la JEP. De acuerdo con el informe, "con estas modificaciones, la JEP no cumpliría con su papel de lucha contra la impunidad".

### **Las recomendaciones de la misión** 

De acuerdo con la Misión el gobierno debe "implementar de manera ágil, rápida y eficiente todas las medidas contenidas en el Acuerdo de Paz que son de su competencia, como lo son las medidas relacionadas con garantías de seguridad, de participación política ciudadana y de reincorporación socioeconómica de personas excombatientes".

Asimismo hace unas recomendaciones puntuales a diferentes instituciones. Por ejemplo a la **Defensoría del Pueblo le solicita poner en marcha el nuevo Sistema de Prevención y Alerta para evitar más muertes de líderes sociales.** Frente a esa misma problemática también recomienda al congreso presentar urgentemente un proyecto de ley que garantice el derecho a la movilización y protesta social.

Además la Misión le dice al gobierno que es urgente "**tomar las medidas necesarias para romper todos los vínculos que puedan existir entre miembros de la Fuerza Pública o funcionarios públicos y grupos neoparamilitares".**

Por su parte, a la fiscalía le solicita asegurar investigaciones rápidas, imparciales y contextualizadas de todas las agresiones, hostigamientos, seguimientos, vigilancias y amenazas en contra de personas defensoras de derechos humanos y sancionar a los responsables materiales como intelectuales. En ese sentido, hace un llamado a la necesidad de lograr el desmantelamiento integral de las estructuras criminales sucesoras del paramilitarismo.

Finalmente, entre otras recomendaciones, el informe señala que es importante que el congreso, los partidos políticos y los medios de comunicación muestren  "un compromiso por la paz en Colombia, **contribuir a la despolarización de la sociedad,** fomentar una nueva convivencia y no hacer de los debates actuales una contienda electoral". Y a la comunidad internacional le pide que le exija al Estado Colombiano resultados concretos en la implementación del Acuerdo de Paz.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
