Title: El 'Sí' en el plebiscito continúa ganando en las encuestas
Date: 2016-09-27 15:29
Category: Nacional, Paz
Tags: encuestas, FARC, firma de paz, Gobierno, paz
Slug: el-si-en-el-plebiscito-continua-ganando-en-las-encuestas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/dt.common.streams.StreamServer-e1475007648772.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.laprensa.hn]

###### [27 sep 2016] 

Las encuestas sobre la intención de voto de los últimos días, apuntan a que el Sí, ganaría en el plebiscito del próximo 2 de octubre, según las más recientes encuestas de las firmas, **Ipsos Napoleón Franco, Cifras & Conceptos y la Encuesta de Opinión Nacional. **

De acuerdo con la medición de Ipsos Napoleón Franco publicada hoy, **el 66% de los encuestados aprobaría el plebiscito, frente a un 34% que rechazaría los acuerdos.** La anterior a esa encuesta arrojaba que el Sí tenía un 72% y el No, un 28%. El estudio evidencia que el 81% de las personas consultadas  dijo que no cambiará su decisión sobre su voto, mientras que el 19% dejó abierta la posibilidad de cambiar su elección del 2 de octubre.

Por su parte, el sondeo realizado por Cifras & Conceptos, concluye que este domingo ganaría con **un 62% el Sí, y el 37% diría No a lo acordado** entre el gobierno y las FARC. Además, allí se muestra que 9 millones de colombianos estarían seguros de hacer efectivo su derecho al voto en el plebiscito por la paz.

Los resultado de la Encuesta de Opinión Nacional, concluyen **el 67,7 % de los encuestados que van a participar en la refrendación se irán por el Sí, contra el 32,4 %** que se inclina por el No. Sin embargo, este estudio muestra que apenas el 7,1 % de las personas aseguran que han leído detalladamente los acuerdos, y el 30.5% no conoce lo pactado entre el gobierno y las FARC.

Para el[analista político César Torres del Río](https://archivo.contagioradio.com/firma-de-la-paz-es-el-acontecimiento-mas-importante-en-la-historia-reciente-de-colombia/), esos resultado, más allá de las diferencias porcentuales, deben llamar a que **la ciudadanía a que participe en la refrendación de los acuerdos, pues**  no sólo está en juego la paz, la verdad, la garantía de no repetición y la reparación, sino “la manera en la que participamos”, como afirma el analista.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
