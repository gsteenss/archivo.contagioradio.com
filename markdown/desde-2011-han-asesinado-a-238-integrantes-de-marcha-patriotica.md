Title: Desde 2011 han asesinado a 238 integrantes de Marcha Patriótica
Date: 2020-06-28 12:54
Author: PracticasCR
Category: DDHH, Nacional
Tags: Edier Lopera, genocidio, lideres sociales, Líderes Sociales Asesinados, Marcha Patriótica
Slug: desde-2011-han-asesinado-a-238-integrantes-de-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Marcha-patriótica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Marcha Patriótica

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras el asesinato de Edier Lopera y la retención de su cuerpo como demostración de terror y dominio por parte de los perpetradores del crimen en Antioquia, la [Coordinación Social y Política Marcha Patriótica](https://www.marchapatriotica.org/) manifestó su rechazo por este acto y en general por el exterminio del que señalan estar siendo víctimas los miembros de este colectivo. (Lea también: [Paramilitares impiden el levantamiento del cuerpo de Edier Lopera tras 8 días de su asesinato](https://archivo.contagioradio.com/paramilitares-impiden-el-levantamiento-del-cuerpo-de-edier-lopera-tras-8-dias-de-su-asesinato/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Marcha Patriótica denuncia que desde su constitución en 2011, han sido asesinados 238 miembros del colectivo**, «*sin que el Estado Colombiano tome las medidas necesarias para frenar esta grave situación y sancionar a los responsables de estos hechos*». (Le puede interesar: [Más de 200 integrantes de Marcha Patriótica han sido asesinados entre 2011 y 2020](https://archivo.contagioradio.com/asesinatos-marcha-patriotica-2011-2020/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a haber acudido a instancias internacionales como **la Comisión Interamericana de Derechos Humanos (CIDH), la cual, mediante [Resolución 030 del 5 de mayo de 2018](http://www.oas.org/es/cidh/decisiones/pdf/2018/30-18MC210-17-CO.pdf), decidió otorgar medidas cautelares** solicitando al Estado colombiano la protección de los miembros de Marcha Patriótica tras el estudio de los casos de persecución y asesinatos de sus integrantes, la violencia y el exterminio no han cesado. (Le puede interesar: [Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Marcha Patriótica asegura que luego del decreto de estas medidas, **68** **de sus miembros han sido asesinados**, **59** de ellos desde que Iván Duque asumiera su cargo en la Presidencia de la República y **20** en lo que va corrido del año 2020. Sin contar otros hechos violentos de los que señalan haber sido víctimas como atentados, desapariciones forzadas, señalamientos e intimidaciones, amenazas y robo de información sensible.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### [Marcha Patriótica señala responsables de los asesinato  ]{.has-inline-color .has-very-dark-gray-color} {#marcha-patriótica-señala-responsables-de-los-asesinato .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde Marcha Patriótica se asegura que las principales amenazas contra los integrantes de su colectivo y en general para la construcción de una paz estable y duradera, provienen del **resurgimiento del paramilitarismo**, amparado por la impunidad en las «*prácticas institucionales del Gobierno*», que ha permitido la consolidación, por ejemplo, de Grupos Armados Organizados (GAO), Grupos Armados Organizados Residuales y Grupos Delincuenciales Organizados (GDO). Además señalan «*el incremento de la acción violenta de Militares y Policías del Estado Colombiano*». (Le puede interesar: [Prácticas paramilitares del pasado resurgen en Sucre](https://archivo.contagioradio.com/practicas-paramilitares-del-pasado-resurgen-en-sucre/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello, hacen un llamado a las instituciones de orden nacional e internacional para tomar las medidas y acciones correspondientes para avanzar en el esclarecimiento de los hechos que han denunciado y para que reafirmen su compromiso para garantizar la participación política y respeto a la vida de todos los líderes sociales en el país.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/marchapatriota/status/1276122993161375744","type":"rich","providerNameSlug":"twitter","align":"center","className":""} -->

<figure class="wp-block-embed-twitter aligncenter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marchapatriota/status/1276122993161375744

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
