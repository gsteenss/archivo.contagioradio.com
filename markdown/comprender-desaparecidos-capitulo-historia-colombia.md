Title: Comprender fenómeno de los desaparecidos en Colombia aporta a la memoria histórica del país
Date: 2020-05-07 15:09
Author: CtgAdm
Category: Actualidad, DDHH
Slug: comprender-desaparecidos-capitulo-historia-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Búsqueda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Desaparecidos HE

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito del lanzamiento del Plan Nacional de Búsqueda de la Unidad de Búsqueda de Personas Dadas por Desaparecidas [(UBPD)](https://twitter.com/UBPDcolombia), su directora, Luz Marina Monzón se refirió a los pasos que se están dando para trabajar junto a las familias, organizaciones, instituciones y a la sociedad civil con el fin de avanzar en el esclarecimiento de cerca de 100.000 despariciones y construir la memoria colectiva que permita comprender esta tragedia como un hecho no aislado y que continúa presentándose.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cuál es el relato detrás de los desaparecidos?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Luz Marina Monzón se refirió al aporte de memoria que realizará la UBPD durante las próximas dos décadas, señalando que más allá de hallar a las personas desaparecidas y saber qué ocurrió, debe primar la contribución para conocer cuál es el relato de los desaparecidos, **"¿de qué hablamos cuando buscamos desaparecidos, qué implicó la desaparición de miles de personas en el país durante el conflicto armado?",** buscando así darle un relato histórico a la búsqueda y desaparición en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta intención de crear memoria histórica ofrecerá cada seis meses un informe sobre la búsqueda lo que permitirá conocer sus avances y **crear enlaces con otras entidades como la Comisión de la Verdad para la creación de su informe fina**l, así como ofrecer nuevos elementos que aporten a la reparación y la no repetición. [(Lea también: Quien pretenda negar el conflicto puede intentarlo, pero conocer la verdad es cuestión de tiempo: Sofía Macher)](https://archivo.contagioradio.com/quien-pretenda-negar-el-conflicto-puede-intentarlo-pero-conocer-la-verdad-es-cuestion-de-tiempo-sofia-macher/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En ese sentido señala que el trabajo de la UBPD no debe ser simplificado a la exhumación, sino que implica investigación, recolección de datos, depuración, análisis, recuperación y entrega digna de cuerpos, y reiteró que no solo se están buscando personas muertas, a su vez se intenta localizar a personas vivas de las que no se conoce su paradero.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Se debe priorizar coordinación con otras instituciones

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que se han abierto espacios de diálogo y de confianza con otras instituciones, la directora de la UBPD resalta que como parte del Sistema Integral, tienen la potestad de acceder a cualquier tipo de información, pero en la practica no existe la fluidez y agilidad que requiere el proceso para priorizar la búsqueda. [(Lea también: Hallan restos de Elder Daza, desaparecido por paramilitares en 2008 en Argelia, Cauca)](https://archivo.contagioradio.com/hallan-restos-de-elder-daza-desaparecido-por-paramilitares-en-2008-en-argelia-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"La Unidad tiene la tarea de buscar los desaparecidos pero no puede hacerlo de manera aislada, pero si vamos a ver cuando todas las instituciones deben coordinarse esto no ha mostrado efectividad históricamente", expresa, con la esperanza que este proceso pueda agilizarse con el paso del tiempo. [(Lea también: Exigen activar mecanismos de alerta para identificar 286 cuerpos en cementerios de Nariño)](https://archivo.contagioradio.com/exigen-activar-mecanismos-de-alerta-para-identificar-286-cuerpos-en-cementerios-de-narino/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "La Unidad tiene la tarea de buscar los desaparecidos pero no puede hacerlo de manera aislada"
>
> <cite>Luz Marina Monzon</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### UBPD aplicará un enfoque diferencial étnico de la búsqueda

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Monzón puntualizó que en medio de este acercamiento a familias y organizaciones, un elemento crucial es la búsqueda de los desaparecidos junto a los pueblos étnicos y al interior de sus territorios. [(Le puede interesar: Víctimas de la Comuna 13 no serían 100 sino 450 según datos de JEP)](https://archivo.contagioradio.com/victimas-de-la-comuna-13-no-serian-100-sino-450-segun-datos-de-jep/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Respecto a su participación, señaló que existen autoridades indígenas y organizaciones étnicas que deben ser reconocidas e involucradas en las actividades de búsqueda, lo que significa una gran oportunidad de incluir a personas que no han sido tenido en cuenta por el Estado sin llegar a ponerlas en riesgo por los aportes que podrían realizar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La directora de la entidad resalta que así como la pandemia del Covid-19 convoca a la sociedad para trabajar juntos, **la búsqueda de los desaparecidos es un asunto humanitario que requiere de todos**, "estamos hablando de más de cien mil personas, y eso no significa que eso haya terminado, hoy en Colombia están desapareciendo a personas en muchos lugares". [(Lea también: Pueblo Bello: 30 años comprometidos con la búsqueda de sus seres queridos)](https://archivo.contagioradio.com/pueblo-bello-30-anos-comprometidos-con-la-busqueda-de-sus-seres-queridos/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

-

<!-- /wp:paragraph -->
