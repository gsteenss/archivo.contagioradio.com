Title: Fotos: Así fue la bailatón por la Consulta Antitaurina
Date: 2015-08-14 16:28
Category: Nacional, Voces de la Tierra
Tags: Bogotá, Bogotá Sin Toreo, Consulta Antitaurina, corridas de toros, corridas de toros en Bogotá, derechos de los animales, Natalia Parra Osorio, Plataforma Alto, Plaza La Santamaría
Slug: fotos-asi-fue-la-bailaton-por-la-consulta-antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/11868850_10153390761525020_753436093_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Contagio Radio 

Desde las 11:30 de la mañana ciudadanos y ciudadanas reunidos en la organización "Bogotá Sin Toreo", realizaron un plantón frente al Tribunal de Cundinamarca, y aunque hubo arengas y gritos, predominó el baile en favor de la vida de los toros.

El objetivo del evento "Baila por la consulta", era demostrarle al Tribunal, el apoyo de la ciudadanía diversa a la consulta popular para que sean los bogotanos los que demuestren si hay o no arraigo cultural mayoritario.

Cabe recordar, que el Concejo de Bogotá ya le dio el "sí", a la democracia, con una votación de 29 votos a favor y 6 en contra. Ahora el turno le corresponde al Tribunal de Cundinamarca, quien dará su parte frente a la constitucionalidad de este mecanismo usado por la ciudadanía.

 
