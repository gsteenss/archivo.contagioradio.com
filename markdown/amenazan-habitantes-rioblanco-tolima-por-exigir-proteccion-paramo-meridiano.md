Title: Amenazan habitantes de Rioblanco, Tolima por exigir protección del Páramo del Meridiano
Date: 2019-01-23 16:40
Author: AdminContagio
Category: DDHH, Nacional
Tags: multinacionales extractivistas, Tolima
Slug: amenazan-habitantes-rioblanco-tolima-por-exigir-proteccion-paramo-meridiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/611-1024x768-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Campesino 

###### 23 Ene 2019 

[**El Sindicato de Trabajadores Agrícolas del Tolima (SINTRAGRITOL)** denunció a través de un comunicado el bloqueo que realizan militares, quienes custodian las obras realizadas en el Páramo del Meridiano, por el Grupo de Energía de Bogotá, el cual estaría trabajando sin licencias ambientales en el corregimiento de La Herrera, municipio de Rioblanco; a raíz de dichas denuncias 10 integrantes del gremio han sido amenazados por desconocidos.  
  
**Raúl Salazar, líder social e integrante del sindicato** explica que el año pasado desde el 1 de abril hasta el 2 de junio, junto a los habitantes de la zona adelantaron un paro para rechazar la presencia de la empresa en el municipio, aunque hace la salvedad que el bloqueo únicamente se realizó a los vehículos de la compañía.  
  
Como resultado de los 62 días del paro, Salazar explica que la empresa se comprometió a presentar ante una mesa regional donde estarían presentes el Ministerio de Medio Ambiente, la ANLA, la Alcaldía de Planadas y Rioblanco, los permisos y las autorizaciones ambientales, compromiso que aún no se ha cumplido.   
  
A raíz de su participación en el plantón, el trabajador declaró que tanto él como su familia recibieron varias amenazas anónimas por vía telefónica donde le advierten que le iban a matar por realizar dichas denuncias, **“yo lo que pido es una claridad para el pueblo”** afirma Salazar quien prefirió salir del municipio por un tiempo y solo hasta ahora ha decidido regresar a Rioblanco.   
  
Según el sindicalista, los integrantes del ejército están apostados en el sector de El Venado, vigilando que la población no se acerque a la vía que conduce al Páramo del Meridiano, zona de reserva forestal, **“nuestra fuerza pública en lugar de defender a los colombianos, está defendiendo a esas empresas, tampoco podemos ingresar a ver qué es lo que está sucediendo, ni podemos acercarnos a pesar de ser un acceso público**” explica.]

[Denuncia Sintragritol](https://www.scribd.com/document/398091774/Denuncia-Sintragritol#from_embed "View Denuncia Sintragritol on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_25195" class="scribd_iframe_embed" title="Denuncia Sintragritol " src="https://es.scribd.com/embeds/398091774/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-tV84rXh3JBYkz1rQxDA2&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe><iframe id="audio_31726134" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31726134_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
