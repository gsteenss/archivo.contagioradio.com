Title: Artesanas indígenas en Ciudad Bolivar salvaguardan la cultura ancestral
Date: 2017-03-24 12:50
Category: Comunidad, Nacional
Tags: artesanias, indígenas, indígenas Wounaan, Monam
Slug: mujeres-indigenas-tejen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ARTESANAS-WOUNAN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos:  Bernardino Dura 

###### 24 Abr 2017 

**64 artesanas indígenas wounaan Nonam, viven en Bogotá en la localidad de Ciudad Bolívar**, allí  tejen artesanías, desde pulseras hasta grandes jarrones, su materia prima es  el cogollo de la palma de werregues traído desde el Chocó.

Ellas tejen las artesanías para ayudar el sustento mínimo vital  de la familia en esta ciudad, haciendo un esfuerzo eminente para cubrir los gastos mensuales de sus familias.

A pesar de ser víctimas del conflicto armado no cesan de tejer la artesanía para suplir sus necesidades, aunque cada día es más difícil, ellas siguen tejiendo para mantener su cultura ancestral y para ganarse unos pesos para su hogar.

Una de las mujeres artesana woounaan Nonam, Bernalicia Ismare Opua, dice que es importante dar a conocer  el tejido de la artesanía en werregues para que se conozca el duro trabajo que las mujeres hacen en cada tejido de las artesanías  y para conservar la memoria de nuestros pueblos indígenas.

![ARTESANAS WOUNAN 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ARTESANAS-WOUNAN-4.jpg){.alignnone .size-full .wp-image-38211 width="1000" height="750"}

### **Artesanas wounaan Nonam nos cuentan** **la elaboración de los jarrones en werregues.** 

Ellas organizan los mojoncitos (grupos) de 40 tiras de hilos de la materia prima de werregues y colocan en un sartén con agua revuelto con la tinta, dejando hervir a una temperatura 35 grado por una hora hasta que el werregue este bien teñido.

Teñida la materia prima, cuelgan en un lugar donde hay sol para que se seque, luego por tira de hilos de la fibra con las manos tuercen para que queda la textura en forma de hilo para tejer las artesanías.

Los jarrones, bandejas, fruteros, platos y pulsera se tejen con  hilos torcido de 80 centímetro de largo con la fibra de werregues con diferentes colores para venderlos como artesanías,  diseños autóctonos pintados de colores.

Cuando terminan la artesanía venden a un comprador de la misma etnia a un precio estable y  el comprador vende en los almacenes de cadena de la ciudad y Artesanías de Colombia  y el resto la exporta a otro país.

**Ver: [13 años desplazado, la vida de un Taita Wounaan en Bogotá](https://archivo.contagioradio.com/el-taita-sercelinito-piraza-burgara/)**

\

------------------------------------------------------------------------

###### *![bernardino](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/bernardino-150x150.jpg){.wp-image-27947 .size-thumbnail .alignleft width="150" height="150"}Reportaje elaborado por Bernardino Dura Ismare, Comunicador de CONPAZ,   de la comunidad de Pichimá Quebrada*

###### Como parte del trabajo de formación de Contagio Radio con comunidades afros, indígenas, mestizas y campesinas en toda Colombia, un grupo de comunicadores realiza una serie de trabajos periodísticos recogiendo las apuestas de construcción de paz desde las comunidades y los territorios. Nos alegra compartirlas con nuestros y nuestras lectoras. 
