Title: El teatro la candelaria no se detiene
Date: 2020-10-20 16:32
Author: AdminContagio
Category: Cultura, eventos
Tags: estrenos, teatro, teatro colombia, teatro la candelaria, Temporada teatral
Slug: el-teatro-la-candelaria-no-se-detiene
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/video-unipersonales-2.mp4" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Nora-scaled-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: Jonathan Beltrán - Teatro La Candelaria {#foto-por-jonathan-beltrán---teatro-la-candelaria .has-cyan-bluish-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->

#### Un grupo y dos estrenos en una semana

<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->

##### **“Voces y Retazos” y “No Estoy Sola”**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Dos unipersonales creados en la pandemia por dos integrantes de la candelaria: Nohra González y Patricia Ariza. Las dos recrean desde la mirada personal, la perturbación que producen las muertes de tantos colombianos y colombianas que quizás hayan caído asesinados sin entender la razón de lo sucedido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tampoco los vivos lo entendemos, aunque leamos las noticias, aunque estemos informados de la política, es difícil asimilar esta cotidianidad fatal que se ha ido por desgracia, normalizando.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **“Voces y Retazos”** 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Es la obra de Nohra González quien recrea el dilema entre hablar de lo que sucede en un país rodeado de violencia o hacer un teatro que no tiene que ver con esa realidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El personaje es una actriz escondida en dos mujeres que habitan un mismo cuerpo y se necesitan pero a la vez se hacen daño, mientras realizan su representación teatral unas figuras errantes comienzan a perturbarlas. Esas figuras esperan ser nombradas, esperan ser identificadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

¿Cómo permanecer indiferentes con todas ellas?, llegan también voces, retazos de recuerdos que nos sacuden la memoria y nos cuestionan nuestro presente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es un obra enigmática y compleja que nos hace pensar en el teatro que hacemos perseguido por las sombras de los y las que no están, pero que son a la vez testigos silentes de la tragedia que vivimos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **“No Estoy Sola”**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Es el unipersonal de la Maestra Patricia Ariza que nos habla de las lideresas muertas, del sin número de mujeres que caen por pertenecer a una junta comunal, por pensar distinto, por opinar distinto, por defender la tierra donde viven o por buscar  un lugar donde vivir en este país tan grande y tan ajeno que las desconoce por pobres y por valientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta obra tiene una enorme influencia de las más de 100 performances realizados por Patricia Ariza con el uso de lenguajes no verbales y del video como testigo inocultable del tiempo y del espacio. El video nos muestra la calle con los performances y las listas interminables de nombres de hombres y mujeres que mueren a diario atravesados por las balas y unas listas sin fin, que no terminan nunca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los dos unipersonales se crearon en estos duros meses de pandemia con una dedicación asombrosa y con el apoyo de todo el grupo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos trabajos nos muestran que la Candelaria no se detiene, los estrenos serán de manera digital.

<!-- /wp:paragraph -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/video-unipersonales-2.mp4">
</video>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

**20 de octubre** – Estreno Unipersonal de Nohra González “Voces y Retazos”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**21 de octubre** – Temporada del Unipersonal de Nohra González “Voces y Retazos”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**22 de octubre** - Estreno Unipersonal de Patricia Ariza “No estoy Sola”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**23 de octubre** – Temporada del Unipersonal de Patricia Ariza “No estoy Sola”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contamos con el apoyo de **IDARTES** y **Ministerio de Cultura.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cordialmente,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

PRENSA, [TEATRO LA CANDELARIA](https://teatrolacandelaria.com/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Entérese de mas eventos culturales aqu](https://archivo.contagioradio.com/categoria/eventos/)í

<!-- /wp:paragraph -->
