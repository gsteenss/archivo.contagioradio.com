Title: Asesinan al joven indígena Edgar Trochez Ramos en Cauca
Date: 2019-05-26 12:10
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, Santander de Quilichao
Slug: asesinan-al-joven-indigena-edgar-trochez-ramos-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/lider-social.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Edgar Trochez Ramos, de 26 años, fue asesinado este sábado mientras se movilizaba en motocicleta por el corregimiento de Sanpedro, sobre la via que comunica el municipio de Santander de Quilichao con Jambalo, en el departamento de Cauca.

Según la denuncia, este crimen fue perpetrado por  desconocidos que buscaban robarle la motocicleta donde se movilizaban junto a su compañera que resultó ilesa de éste homicidio.

El joven vivía  en la **vereda Paramillo 2 Resguardo indígena de Munchique los tigres Municipio de Santander de Quilichao, Cauca**. (Le puede interesar: "A[menazan a líderes del CRIC y ACIN que participan en Minga indígena](https://archivo.contagioradio.com/amenazan-a-lideres-del-cric-y-acin-que-participan-en-minga-indigena/)")

Noticias en desarrollo...

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
