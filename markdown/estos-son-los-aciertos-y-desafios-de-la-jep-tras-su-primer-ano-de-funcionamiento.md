Title: Aciertos y desafíos de la JEP, tras su primer año de funcionamiento
Date: 2019-01-18 14:51
Author: AdminContagio
Category: Nacional, Paz
Tags: acuerdo de paz, ICTJ, JEP, víctimas
Slug: estos-son-los-aciertos-y-desafios-de-la-jep-tras-su-primer-ano-de-funcionamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Dqd0SgzWwAA8CQH-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter JEP] 

###### [18 Ene 2019] 

El Centro Internacional de Justicia Transicional, ICTJ por sus siglas en inglés, sacó su más reciente balance sobre el primer año de funcionamiento de la Jurisdicción Especial para la Paz, en el que señaló que si bien su desempeño ha sido sobresaliente, **su mayores retos se encuentran en los ámbitos políticos, de eficiencia y eficacia, y procesales. **

### **Los logros de la JEP a un año de su puesta en marcha** 

De acuerdo con la ICTJ uno de los principales logros ha sido la prontitud en la que este Tribunal ha entrado en funcionamiento, debido a que en otros países, aún después de avalado el Sistema de Verdad Justicia Reparación y no Repetición, empiezan su labor años más tarde. **En el caso de Colombia, la jurisdicción se creó en menos de 10 meses**.

Al mismo tiempo ya cuenta con un acto legislativo, una ley estatutaria y una normativa respaldadas constitucionalmente, además, ha coordinado su labor con las jurisdicciones étnicas, priorizando mecanismos como la consulta previa o los protocolos de relacionamiento interjurisdiccional.

Frente a su administración de justicia, hasta el momento la JEP ha abierto dos casos y tres situaciones en la Sala de Reconocimiento de Verdad y Responsabilidad y la metodología para seleccionarlos se ha basado de forma determinante, en la participación de la víctimas, de sus organizaciones y de otras organizaciones de la sociedad civil, a través de la presentación de informes, hecho que para el ICTJ podría lograr que **"las víctimas sean protagonistas reales de la JEP, desde las primeras etapas procesales". **

### **Los desafíos de la JEP en Colombia** 

La ICTJ también señala los desafíos que tendrá que afrontar la Jurisdicción Especial para la Paz en la misión de generar justicia restaurativa, dividiéndolos en desafíos políticos, de efectividad y eficiencia, y procesales. (Le puede interesar: ["Van por nuestras tierras: Informe entregado a la JEP sobre despojos en el Urabá"](https://archivo.contagioradio.com/van-por-nuestras-tierras-informe/))

En torno a los retos políticos la ICTJ manifestó que el primero será que la JEP se posicione como un alto tribunal que tiene capacidad para administrar justicia, haciendo claridades sobre su operación en tres sentidos, el primero la justicia que otorga en tratamientos penales especiales **frente a los derechos de las victimas de verdad, justicia, reparación y no repetición, en cuyo caso podrá optar por sanciones ordinarias o restaurativas. **

El otro sentido tiene que ver con que la JEP selecciona y prioriza informes con el propósito de hacer frente a la sistematicidad y generalidad de conductas que están bajo su competencia; y el último es que si bien esta jurisdicción nace en el marco de un acuerdo de paz entre el gobierno y FARC, la JEP tiene como objetivo **juzgar a todos los actores del conflicto armado, razón por la cual es fundamental la comparecencia de otros actores** como integrantes de las desmovilizadas AUC.

Con respecto a la efectividad y eficiencia la ICTJ le recordó al Alto Tribunal, la incesante lucha que iniciaron las víctimas por encontrar la verdad, motivo por el que se espera que este escenario tenga una respuesta eficaz, **"en términos del esclarecimiento de los hechos, la identificación de los responsables y las condenas"** y sea efectivo en cuanto a resultados concretos.

En cuanto a las garantías procesales, la ICTJ mencionó la importancia en la Sala de Amnistías e Indultos, de aplicar amnistías más amplias con la finalidad de "evacuar un número importante de comparecientes para concentrarse en quienes tuvieron participación determinante en los hechos más graves y representativos".

De igual forma señaló que es primordial, desde ya, ubicar los recursos para llevar a cabo las sanciones restaurativas en los territorios y garantizar la subsistencia de los comparecientes durante el cumplimiento de las sentencias, al igual que un mecanismo de monitoreo, vigilancia y verificación de los veredictos.

###### Reciba toda la información de Contagio Radio en [[su correo]
