Title: ONIC denuncia asesinato de comunero en Caloto Cauca
Date: 2018-07-06 15:33
Category: DDHH, Nacional
Tags: Caloto, Cauca, Comunero, ONIC
Slug: onic-denuncia-asesinato-de-comunero-en-caloto-cauca
Status: published

###### [Foto: Pulzo] 

###### [06 Jul 2018] 

La Organización Nacional Indígena de Colombia denunció el asesinato del comunero **Luis Fernández Velasco, el pasado 28 de junio, luego de que su cuerpo fuese encontrado con dos impactos de bala**, en el sector conocido como el Arreyana, en la jurisdicción del municipio de Caloto. Sumado a este hecho la organización denunció violaciones a derechos humanos y hostigamiento a sus integrantes.

De acuerdo con la denuncia, el comunero salió de su casa hacia las dos de la tarde y tiempo después personas que transitaban el sector de Arreyana encontraron su cuerpo. Fernández era reconocido en la región como líder social, fundador y coordinador del movimiento juvenil de la vereda Loma Gruesa. (Le puede interesar: ["Estado debe reconocer la sistematicidad en asesinato a líderes sociales: Somos Defensores"](https://archivo.contagioradio.com/estado-debe-reconocer-sistematicidad-en-asesinato-a-lideres-sociales-somos-defensores/))

De igual forma, el Cabildo Indígena de Jambaló manifestó que los habitantes de su territorio se encuentran en riesgo debido a los artefactos explosivos que han sido dejados a los alrededores del resguardo. **Ese mismo 28 de junio, Ana Yamilet Yule Rivera, habitante del lugar, de 17 años, fue la víctima más reciente de una de esas explosiones**.

Además de ese hecho, el pasado 29 de junio rectores de las instituciones educativas del territorio Jambaló, expresaron que haber recibido varias llamadas desde números desconocidos en donde se les extorsiona y se les exigen montos económicos, **de no hacerlo  les aseguran que les harán daño a sus familiares.**

Frente a estos hechos la ONIC les exige a los diferentes grupos armados que respeten el territorio ancestral y no dejen en medio del conflicto armado a la población civil. Así mismo, hacen un llamado a la pronta intervención de los organismos de control, protección y defensa de los derechos humanos tales como la Procuraduría General de la Nación.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
