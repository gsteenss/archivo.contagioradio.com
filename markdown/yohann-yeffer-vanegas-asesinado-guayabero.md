Title: Yohanni Yeffer Vanegas líder de movilización campesina de Guayabero fue asesinado
Date: 2020-06-27 22:50
Author: CtgAdm
Category: Actualidad, Nacional
Slug: yohann-yeffer-vanegas-asesinado-guayabero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/erradicación-forzada-guayabero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

[Foto: 360Radio]{.has-inline-color .has-cyan-bluish-gray-color}

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Comunidades del Guaviare y Sur del Meta, ASCATRAGUA, denunciaron este 27 de Junio que fue asesinado el campesino Yohanni Yeffer Vanegas líder de movilización campesina que desde el pasado 20 de mayo se manifiestan de forma pacífica para **evitar la erradicación forzada de cultivos de uso ilícito en la zona del Guayabero**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia el cuerpo del campesino apareció en inmediaciones de la vereda Picalojo en el sitio conocido como el cruce de Choapal. Adicionalmente la denuncia de quienes habitan la región es que el hermano de Yohani está desaparecido y al cierre de esta nota se han realizado dos recorridos de búsqueda sin que hasta el momento hayan resultados positivos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vanegas Cardona era oriundo de la vereda Puerto Cachicamo quien lideró la movilización campesina hasta el 16 de junio. **Adicionalmente participó como acompañante de los voceros en la reunión del día 11 de junio en el sitio conocido como Tres Esquinas- la báscula de la Macarena en el departamento del meta donde se reunieron voceros del gobierno, altos mandos militares y voceros de la comunidad.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Los señalamientos contra campesinos de Guayabero han sido constantes

<!-- /wp:heading -->

<!-- wp:paragraph -->

En la denuncia las comunidades también resaltan que a pesar de que esta semana se esperaba la presencia del Defensor del Pueblo Carlos Negrette para establecer un diálogo, este nunca se hizo presente, por el contrario, a pesar de la presencia del Defensor Regional del Meta ta**nto las FFMM como la gobernación del meta han afirmado que estás movilizaciones son organizadas por grupos guerrilleros.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*“Adjunto a esto las declaraciones de los generales Eduardo Zapateiro y Raúl Hernando flores quienes han declarado abiertamente que la manifestación pacífica de los campesinos es una asonada organizada por los grupos guerrilleros que existen en esta región de igual modo las declaraciones del gobernador del meta Juan Guillermo Zuluaga que expresaba en NTN noticias que en la reunión del 11 de junio del 2020 en tres esquinas habían estado los representantes de los grupos guerrilleros”* afirma el comunicado de ASCATRAGUA.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Los campesinos del Guayabero buscan alternativas, diálogo con instituciones y soluciones. La respuesta es bala, hoy asesinaron a YOANNY VANEGAS, se acusa al Ejército. La represión escala.  
> Cuántos campesinos muertos espera el Gobierno.  
>   
> Urge una Mesa Integral de Concertración. [pic.twitter.com/IcwtLrVFCT](https://t.co/IcwtLrVFCT)— Aída Avella E (@AidaAvellaE) [June 28, 2020](https://twitter.com/AidaAvellaE/status/1277058717457043459?ref_src=twsrc%5Etfw)
>
> <cite>Twitter de Senadora Aida Avella</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Militares confinan a manifestantes y les impiden la salida de la vereda el Silencio de Vista Hermosa

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro de los apartes de la denuncia señala que luego de las reuniones sostenidas con la defensoría regional del pueblo en la que se clarificó que la movilización es pacífica y que **los campesinos buscaban una salida negociada para evitar perder su única fuente de sustento las unidades militares presentes en la zona los mantienen aislados en la Vereda el Silencio del Municipio de Vistahermosa.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Las comunidades del sur del Meta específicamente de la zona del Guayabero denuncian que unidades de la fuerza de tarea conjunta OMEGA desde hace 2 días están prohibiendo el ingreso y la salida de los campesinos en un sector de la vereda El Silencio, jurisdicción del municipio de Vistahermosa” l**o que implica riesgos en materia de salud por la crisis sanitaria pero también amenazas directas sobre los pobladores que han sido señalados por los militares de pertenecer a estructuras guerrilleras.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Tras mas de un mes de movilizaciones continúa la erradicación y el señalamiento

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde el pasado 2o de mayo cerca de mil personas han decidido manifestarse en contra de los operativos de erradicación forzada adelantados por el ejército, asi como las reiteradas amenazas y estigamitzaciones por parte de la misma fuerza pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la comunidad se han convocado varios espacios de diálogo pero los compromisos de respeto por parte de las FFMM no se han cumplido y la **defensoría del pueblo tampoco ha sido efectiva en su labor de proteger la vida de los campesinos y campesinas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una reciente comunicación quienes se han movilziado han afirmado que tanto el Ejército, como la Policía y el ESMAD han hecho presencia dejando un saldo de cinco campesinos con heridas por impactos de fusil y un centenar de heridos con contusiones por los golpes en diferentes partes de su cuerpo. Lea también [Campesinos de La Macarena, Meta denuncian agresiones constantes por parte de la fuerza pública](https://archivo.contagioradio.com/campesinos-macarena-meta-denuncian-agresiones-constantes-fuerza-publica/?preview_id=60580&preview_nonce=ac5ab016da&preview=true&_thumbnail_id=63396)

<!-- /wp:paragraph -->
