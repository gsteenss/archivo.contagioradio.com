Title: INPEC violenta a las mujeres visitantes en centros de reclusión
Date: 2019-10-16 15:51
Author: CtgAdm
Category: Expreso Libertad
Tags: cárceles colombia, cárceles mujeres, Caso de Falsos Positivos, Falsos Positivos Judiciales, INPEC, víctimas de violencia
Slug: inpec-violenta-a-las-mujeres-visitantes-en-centros-de-reclusion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/inpec.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: Colprensa] 

Rocío Barrera, hermana de César Barrera, víctima de un falso positivo judicial, ha sido víctima en múltiples ocasiones de la violencia del **INPEC** a la hora de realizar el proceso de ingreso a la cárcel, un abuso que ha sido reiterativo contra las mujeres desde la creación de esta institución.

<div style="text-align: justify;">

De acuerdo con Barrera, desde la fila de ingreso a la cárcel las mujeres se ven sometidas a tratos crueles e inhumanos, que son innecesarios. Asimismo aseguró que si bien las requisas son los momentos en donde más agresiones físicas viven las mujeres, por la violencia, agresividad y morbo al que están sometidas, hay una violencia emocional constantes que induce a las mujeres a pensar que merecen el trato que están recibiendo por ser familiares de personas en teoría malas. Ver mas|[Universidad Nacional estaría negando el derecho a la educación de presos políticos](https://archivo.contagioradio.com/universidad-nacional-estaria-negando-el-derecho-a-la-educacion-de-presos-politicos/)

</div>

<div>

</div>

<div style="text-align: justify;">

Finalmente Rocío aseguró que si el **INPEC** mejorará no solo la forma de relacionarse con las visitas, sino evidentemente el trato con la población reclusa, la experiencia sería menos traumática en las cárceles y se podría hablar de un intento de resocialización.

</div>

<div>

</div>

<div>

</div>

<div>

</div>

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F391134685128132%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

(Le puede interesar: [Falsos positivos judiciales contra lideresas sociales](https://archivo.contagioradio.com/falsos-positivos-judiciales-contra-lideresas-sociales/))
