Title: ¿Como meterle pueblo a la paz?
Date: 2016-06-01 18:36
Category: Nacional, Paz
Tags: ELN, paz
Slug: como-meterle-pueblo-a-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Paz-Completa-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Paz completa 

###### [1 Jun 2016] 

Hoy a las 6 de la tarde, en el Planetario de Bogotá, se realizará la presentación del nuevo libro del profesor de la universidad Nacional Víctor de Currea Lugo. El libro “Metiéndole pueblo a la paz”, donde se evidencia la importancia de impulsar la Participación de la sociedad en la construcción de ciudadanía.

Este, es el tercero de una serie de libros, en 2014 se lanzó ‘¿Porqué negociar con el ELN?’, donde se evidencia la importancia de dialogar con este grupo frente a los que significa una paz completa, luego en 2015 se publicó ‘Y sin embargo se mueve’ sobre la agenda de negociación con esa guerrilla, y este año se llega 'Metiéndole pueblo a la paz', donde se resalta el punto de la agenda de negociación sobre la participación de la sociedad.

A partir del análisis de 29 ponencias de diversos sectores de la sociedad civil, convocados para la producción del libro se recogieron propuestas concretas sobre cómo debe ser la participación de las colombianas y los colombianos en ese proceso.

<iframe src="http://co.ivoox.com/es/player_ej_11759743_2_1.html?data=kpakl56beJShhpywj5WdaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1JDIqYy31tffx8aPcYy40MjS0NnJb9qfxtjQ1M7Ys9Ohhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
