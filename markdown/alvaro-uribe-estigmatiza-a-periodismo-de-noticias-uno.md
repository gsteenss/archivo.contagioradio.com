Title: "Álvaro Uribe siempre va a estigmatizar a la persona que revele cosas que él quiere ocultar"
Date: 2017-05-16 12:39
Category: DDHH, Nacional
Tags: Estigmatización a la prensa, Libertad de Prensa, Noticias Uno, periodismo
Slug: alvaro-uribe-estigmatiza-a-periodismo-de-noticias-uno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Uribe-y-Julian-Martinez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  16 mayo 2017 

A través de un trino en su cuenta de Twitter, **Álvaro Uribe Vélez, tildó al periodista Julián Martínez  de ser "pro FARC",** luego que se emitiera a través de Noticias Uno, una investigación que señala que una de las fincas de propiedad de la familia Uribe había recibido subsidios por más de 3000 millones de pesos, cuando el hoy senador era presidente.

En entrevista con Contagio Radio el periodista señaló que esta situación es reiterada y no debe presentarse porque puede poner en riesgo la vida y la labor periodística.

Para Martínez, este fenómeno no es nuevo. Según él **"Álvaro Uribe siempre va a estigmatizar a la persona que revele cosas que él quiere ocultar".** Lo preocupante es que, con la investidura del senador y con la relevancia que tiene en ciertos sectores de la sociedad, sus palabras pueden poner en riesgo a una persona.

Julián Martinez ha sido estigmatizado en varias ocasiones por el senador Uribe. En diciembre 14 de 2016 el periodista confrontó al ex presidente para saber si conocía a Luis Carlos Molina, socio de Pablo Escobar y asesino de quien fue el director del diario El Espectador, Guillermo Cano. Ante esto, **Uribe acusó al periodista de llamarlo asesino,** haciéndolo ver como un periodista que acusa a la sociedad de delitos que no puede probar.

Junto con Cecilia Orozco e Ignacio Gómez, directores de Noticias Uno, han dicho que **la prensa no se debe dejar intimidar** y que, como lo afirma Julián Martínez, "deben seguir trabajando e investigando los fenómenos de corrupción que existen en los gobiernos que ha tenido Colombia.

> A Noticias Uno, su periodista Pro Farc Julián Martínez y a su directora dra Orozco les repetiré la respuesta de 2013 a su repetido infundió
>
> — Álvaro Uribe Vélez (@AlvaroUribeVel) [15 de mayo de 2017](https://twitter.com/AlvaroUribeVel/status/864166579394809856)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
La Fundación para la Libertad de Prensa FLIP, emitió un comunicado en donde rechaza los comentarios del senador del Centro Democrático y le recordó al ex presidente que como funcionario público, **tiene la obligación de adoptar un discurso favorable que contribuya a prevenir la violencia contra la prensa.**

El periodista destaca que la FLIP estuvo al tanto de este suceso como lo ha estado en ocasiones anteriores donde le senador ha hecho uso de su investidura política para atacar a la prensa. En repetidas ocasiones, la Fundación para la Libertad de Prensa le ha recordado al senador que **"debe aportara a construir un ambiente favorable a la deliberación pública, que respete y no afecte el libre flujo de ideas".**

Desde enero del presente año, existe una denuncia ante la fiscalía para que el senador corrija esa conducta, pues pone en situación de riesgo infundado al periodista. Martínez le recordó al senador que **"sus señalamientos son irresponsables pues lo convierten en un censor de quienes tienen la obligación de investigar".**

### **Amenazan a contralor que investigó las irregularidades en "El Ubérrimo"** 

Julián Martínez, también señaló como preocupante el hecho de que Luis Alberto Higuera, ex contralor, quien investigó las denuncias sobre irregularidades en la hacienda "El Ubérrimo" **ha sido víctima reiterada de señalamientos por parte del Senador José Obdulio Gaviria** a través de redes sociales, y afirmó que ha sido víctima de amenazas en los últimos días.

Esa situación no debería permitirse ni repetirse, puesto que, no solamente está en riesgo la vida de Higuera, que actualmente se desempeña como vice rector de la Universidad Pedagógica de Colombia, sino también **el ejercicio de control que desarrollan los funcionarios o ex funcionarios de entidades estatales como la Contraloría.**

<iframe id="audio_18731145" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18731145_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
