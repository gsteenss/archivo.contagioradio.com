Title: Conductores de Transmilenio trabajan más de 16 horas: UGETRANS
Date: 2017-10-06 15:02
Category: Movilización, Nacional
Tags: Trabajadores, Transmilenio
Slug: conductores-de-transmilenio-trabajan-mas-de-16-horas-ugetrans
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/original-e1507320022605.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Xataka Colombia] 

###### [06 Oct 2017] 

Wilson Hoyos presidente de Ugetrans, los conductores son sometidos a turnos de más de 16 horas, sin pago de horas extras y con entre 30 y 40 despidos por semana. Por ello los Sindicatos de conductores de Transmilenio se movilizaron para exigirle esta empresa que **pare la masacre laboral y brinde garantías de trabajo dignas para los empleados.**

Hoyos afirmó que los despidos se vienen dando desde hace un año y que “Transmilenio y las empresas despiden a los trabajadores para tener los buses arrumados en los patios y no prestar el servicio a la comunidad”, **además agregó que no se están generando nuevas contrataciones**, lo que ha provocado que a los conductores que quedan se les extienda la jornada laboral sin que estas sean remuneradas.

Sumado a esta situación, los conductores de Transmilenio agremiados en Ugetrans, denunciaron que si los trabajadores que no cubren las rutas son sancionados, “**estamos amenazados por parte de Transmilenio** y de las empresas privadas para cubrir estas rutas”  y agregó que a los trabajadores se les están haciendo unos descuentos ilegales.

“Solo Transmilenio saca esas multas, no están contempladas en la ley” informó Hoyos y manifestó que podrían tener un costo desde **\$ 50.000 hasta \$800.000 pesos**. Señaló que cuando los transmilenios se estrellan a ellos también les cobran una multa a pesar de que los vehículos estén asegurados.

Los extensos horarios de trabajo generan consecuencias como lesiones en la espalda, columna y problemas psicológicos, “a nosotros esto nos afecta mucho, tenemos pocos ingresos que vienen de estas empresas y aún así, **son despedidos los empleados y vetados, porque dan malas referencias de ellos para que trabajen**” afirmó Hoyos. Le puede interesar: ["Sin estudios del metro aprobarían cupo de endeudamiento para transmilenio"](https://archivo.contagioradio.com/sin-estudios-completos-aprobaria-cupo-de-endeudamiento-para-transmilenio-por-la-carrera-septima/))

En ese sentido los diferentes sindicatos que de Transmilenio aseguraron que continuaran en las movilizaciones para exigir que se terminen los despidos injustificados de los empleados, las extenuantes jornadas laborales y los cobros de multas ilegales.

<iframe id="audio_21314965" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21314965_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
