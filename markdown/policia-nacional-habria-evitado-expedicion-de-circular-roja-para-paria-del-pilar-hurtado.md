Title: María del Pilar Hurtado se entregaría este fin de semana
Date: 2015-01-30 17:50
Author: CtgAdm
Category: DDHH, Judicial
Tags: das, Maria del Pilar Hurtado, Operaciones Ilegales, Paramilitarismo
Slug: policia-nacional-habria-evitado-expedicion-de-circular-roja-para-paria-del-pilar-hurtado
Status: published

###### Foto: pulzo.com 

#### **[Luis Guillermo Pérez]**<iframe src="http://www.ivoox.com/player_ek_4020415_2_1.html?data=lZWfkpmVeY6ZmKiak5eJd6KnmpKSmaiRdY6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzA1s7gjazZrc3gxtfa0ZC0qdPZ25KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46k&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En la mañana de este viernes se conoció que la **Interpol emitió una circular roja contra Maria del Pilar Hurtado**, ex directora del **DAS** y prófuga de la justicia desde el año 2010. Adicionalmente la ex funcionaria estaría tramitando su **entrega a la justicia colombiana, para responder ante la Corte Suprema de Justicia** por las actividades ilegales del DAS y sus **máximos responsables**.

Sin embargo, tanto las víctimas como sus representantes, se preguntan las razones por las cuales no se tramitó antes la circular roja de Interpol y las razones por las que se consideró que Maria del Pilar Hurtado era una perseguida política y no como una persona natural acusada de delitos comunes.

Según el abogado Luis Pérez, secretario general de la **FIDH**, la Interpol tiene la obligación de evaluar el contexto en el que se presenta la emisión de una circular roja, con el objetivo de evitar persecuciones por creencias religiosas, condiciones étnicas o trabajo político. Para los casos colombianos, la oficina central ubicada **en Francia, obtiene la información de su oficina en Colombia**, que actualmente se encuentra a cargo de la Policía Nacional.

La **Federación Internacional de Derechos Humanos** ha dado una fuerte pelea jurídica para que el Estado Colombiano realice los trámites jurídicos acordes a la ley, y la Interpol internacional expida la circular roja para Maria del Pilar Hurtado. Para el abogado, Luis Guillermo Perez, secretario de la FIDH, la oficina de la Interpol en Francia no generó la idea de que la ex-Directora del DAS fuese una perseguida política, sino que esta, por el contrario, fue una información generada por la Oficina de la Policía Nacional en Colombia.

La irregularidad denunciada por la FIDH hace referencia a la **presentación insuficiente de pruebas, e incluso, la presentación de mal información por parte de la Policía Nacional** a la Interpol internacional. A esta inconsistencia se suma que, a pesar de que Maria del Pilar Hurtado dejara su cargo como Directora del DAS en el año 2008, hasta el 2011 aún aparecía como parte de la Junta Directiva de la Interpol en Colombia.

Por su parte, el representante a la Cámara, Alirio Uribe, ha anunciado la realización de un **debate de control político a la Policía Nacional por estas irregularidades.**
