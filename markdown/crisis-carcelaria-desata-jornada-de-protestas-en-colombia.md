Title: Crisis carcelaria desata jornada de protestas en Colombia
Date: 2015-04-24 17:54
Author: CtgAdm
Category: DDHH, Nacional
Tags: Bayron Piedrahita, carcel, Crisis carcelaria colombia, gobierno colombiano, hacinamiento, Movimiento Nacional Carcelario
Slug: crisis-carcelaria-desata-jornada-de-protestas-en-colombia
Status: published

###### Foto: elmalpensante 

##### <iframe src="http://www.ivoox.com/player_ek_4404412_2_1.html?data=lZmdlpmVdo6ZmKiakpuJd6Knl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8ro1sbQy4qnd4a2lNOYxYqnd4a1ktfQx9GRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Testimonio familiares de detenidos] 

<iframe src="http://www.ivoox.com/player_ek_4404475_2_1.html?data=lZmdlpmbeY6ZmKiakpyJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYqdHTtsrVjLjWztvFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Abogada Gloria Silva] 

En crisis se encuentran las cárceles en Colombia. El **Movimiento Nacional Carcelario** ha realizado jornada de actividades con el fin de exigir al gobierno un trato digno a los presos que sufren deplorables condiciones sanitarias, penales y judiciales en Colombia.

La jornada de protesta del Movimiento Nacional Carcelario ha estado acompañada acompañada por una huelga de hambre en la cárcel la Picota, desobediencia civil dentro de varias prisiones, denunciando los tratos inhumanos y degradantes a los que han estado sometidos.

El testimonio de una familiar de los internos demuestra una realidad latente en que los derechos de las personas privadas de la libertad son negados en casi todos los aspectos. "Nosotros tenemos familias, son hijos, esposos, padres a los que se les niega la dignidad humana" "**Las cárceles colombianas son aplanadoras sociales**’’, y asegurando que el Proceso Resocializador, estipulado en el Sistema Carcelario en los artículos 9 y 10 de la Ley 65 de 1993, no se está llevando a cabo en el momento en que los internos se reintegran en la sociedad"

Según organizaciones acompañantes denunciaron que la represión a las jornadas de protesta no se han hecho esperar dado que en la cárcel de San Isidro, Popayán, se encuentra en aislamiento el detenido Julian Joaquin Realpe, quien ha venido liderando la jornada de protesta en esta cárcel y en patio 8. El interno fue sacado del patio de reclusión a cargo del dragoneante AREVALO y por orden del teniente **BAYRON PIEDRAHITA**, quien manifestó que no se iba a permitir que violaran el régimen de la contada en aquel patio, ya que los detenidos vienen saliendo únicamente en ropa interior.

Las exigencias del Movimiento Carcelario son:

-''La instalación de una Mesa Nacional de Concertación como espacio de diálogo del MOVIMIENTO NACIONAL CARCELARIO, la sociedad y el gobierno para tratar soluciones estructurales de fondo a la grave problemática penitenciaria y carcelaria, así como judicial, de todos los presos de Colombia.

-Que de acuerdo al artículo 215 de la Constitución Nacional, el gobierno nacional declare la Emergencia Social y Humanitaria, lo cual obliga a dar soluciones inmediatas a la grave situación planteada. Esto nos lleva a rechazar de plano la creación de más cupos carcelarios, toda vez que ello va en detrimento del erario público y del ﬁn de la misma ley penal.

- Que se apruebe una Rebaja de Penas para todos los presos del país, sin que éste quede supeditado al proceso de paz llevado a cabo hoy en La Habana-Cuba. Otorgamiento real y efectivo de todos los subrogados penales, así como beneﬁcios administrativos para todos los presos, acabando así el criterio personal de cada juez, pues ahora cada juez tiene una ley interpretativa diferente.

-Solución real, deﬁnitiva e inmediata a la grave situación de salud que padecemos todos los presos del país. Acercamiento familiar y procesal para todos los presos y digniﬁcación de las visitas.

- No a la extradición de ningún nacional, por soberanía, patria y dignidad. Sí a la repatriación de nuestros nacionales en cárceles extranjeras''.

El Movimiento Nacional carcelario, junto con los y las familiares de los presos permaneces atentos y atentas ante una respuesta de parte del gobierno colombiano, para que se hagan evidentes las actuaciones que permitan que las condiciones sean dignas y respeten los derechos humanos.
