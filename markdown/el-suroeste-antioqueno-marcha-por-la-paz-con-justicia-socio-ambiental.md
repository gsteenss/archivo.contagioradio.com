Title: El Suroeste Antioqueño marcha por la paz con justicia socio-ambiental
Date: 2016-08-10 17:57
Category: Ambiente, Nacional
Tags: Antioquia, La Habana, Mineria, paz, Plebiscito
Slug: el-suroeste-antioqueno-marcha-por-la-paz-con-justicia-socio-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/tamesis.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [ejatlas.org]

###### [10 Ago 2016] 

**Del 8 al 14 de agosto diversas organizaciones ambientalistas lleva a cabo el “Abrazo a la montaña”,** un recorrido por algunos municipios del suroeste antioqueño a través del cual se hace un reconocimiento del territorio, con el objetivo de generar conciencia en la comunidad y fortalecer el proceso de resistencia en defensa del ambiente.

Una travesía impulsada por el Cinturón Occidental Ambiental , COA, que se retoma luego de 4 años, tras el éxito del primer evento en el que se logró convocar a diversas comunidades de esta zona del departamento. Un sector conocido como el cinturón de oro de Colombia, teniendo en cuenta que **la minería es la principal amenaza, pues el 90% del territorio del suroeste antioqueño está solicitado para esa actividad.**

“No es un cinturón de oro, como se quiere ver en términos de la explotación del patrimonio natural, es un territorio campesino e indígena donde sus habitantes conviven con formas de economía productiva,  y sobre todo donde el patrimonio natural permite una vida adecuada para poblaciones de más de 11 municipios”, dice Danilo Urrea, integrante de la organización Amigos de la Tierra.

Urrea explica que en esta ocasión el recorrido, inició en el municipio de Jardín, y pasará también por Andes, Hispania, Farallones, Pueblorrico, Jericó, Támesis, finalizando en Caramanta. Un camino enfocado en el tema de la justicia socio-ambiental de cara al escenario de posconflicto y el trabajo para la construcción de paz, de manera que también se trabajará una **campaña pedagógica por el SI al plebiscito, explicando los acuerdos de La Habana.**

<iframe src="http://co.ivoox.com/es/player_ej_12505788_2_1.html?data=kpeikpqbfJmhhpywj5WcaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncaXVz87Z0ZC5ttPZwoqfpZClscrb0NiYxsqPsMKf1c7S1NfFcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
