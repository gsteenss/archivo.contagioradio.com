Title: Coronel Plazas Acevedo: de la masacre de Mapiripán al asesinato de Jaime Garzón
Date: 2015-06-23 16:58
Category: Entrevistas, Judicial
Tags: coronel, Fiscalía, general, Jaime Garzon, mapiripan, masacre, medida de aseguramiento, plazas acevedo, rito alejo del rio, uraba
Slug: coronel-plazas-acevedo-de-la-masacre-de-mapiripan-al-asesinato-de-jaime-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/coronel-acevedo-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4679303_2_1.html?data=lZukm5iUd46ZmKiakp6Jd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dm0NPSzpC0sMLuwtiYo8jJusbY0Iqgo5DIqYzgwpDaw9jFp9PZjMnSjbLFtMrmytWSpZiJhZLijMbZjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Eduardo Carreño, Colectivo de Abogados José Alvear Restrepo] 

###### [23 jun 2015] 

El **Coronel en retiro, Jorge Plazas Acevedo**, quien actualmente es investigado por coautoría en el asesinato del periodista **Jaime Garzón**, **deberá responder también ante la Fiscalía por la masacre de Mapiripan**. Según la Fiscalía, el Coronel en retiro sería alias "Don Diego", reconocido paramilitar, y quien habría coordinado el desplazamiento de las estructuras paramilitares desde el Urabá hasta el municipio del Meta para efectuar la masacre.

Pero estos no son los únicos cargos que se le endosan al militar: de su paso por la Brigada XVII en el Urabá y la **estrecha relación con el General (r) Rito Alejo del Rio**, se habla de la persecución sistemática en contra de líderes sociales, campesinos y miembros de la **Unión Patriótica**. Una vez trasladado a Bogotá, su historial delinquido daría cuenta de secuestros, extorsiones y asesinatos a comerciantes.

Y aunque la captura del Coronel Plazas en julio del 2014 (tras una fuga de 11 años en la que se vistiera de campesino para evadir a la justicia) y la medida de aseguramiento dictada por la Fiscalía el 22 de junio del 2015, son positivas porque permiten el reconocimiento del trabajo conjunto entre el Ejercito y el paramilitarismo en Colombia (el mismo por el que la CIDH condenara a Colombia en el año 2005), para el abogado Eduardo Carreño, **aún son muchos los mandos altos y medios que deben responder por la masacre.**

A la fecha han sido condenados el Cabo Montoya, los sub oficiales Miller Urueña y Carlos Gamarra, el Mayor Orozco y el General Uscategui. "*Faltan los demás miembros de la estructura publica que estaban en el aeropuerto de Urabá, en el aeropuerto en San José del Guaviare, y en los retenes de entrada y salida, que no han sido investigados. En eso la Fiscalía ha estado muy lenta*", asegura Carreño.

La investigación refuerza la tesis de que **en Colombia las acciones paramilitares han estado estrechamente vinculadas con las fuerzas militares**, políticas y económicas. Cabe recordar que desde 1965, el gobierno del Frente Nacional autorizó la creación de estas estructuras para-estatales, con el decreto de Seguridad Nacional 33-98, que sería aprobado como Ley de la República mediante la Ley 48 de 1968.

"*Este juicio ayudará a esclarecer más esa relación entre empresarios terratenientes de los llanos orientales, que solicitaron a estructuras para combatir a los movimientos guerrilleros de la zona, pero que en la práctica acabaron con los movimientos campesinos de la región. Esperamos que este tipo de criminales confiesen y aporten algo de verdad a la sociedad en su conjunto*", concluyó Eduardo Carreño.
