Title: "Sodoma y Gomorra es una historia bíblica no un argumento constitucional" Mauricio Albarracín
Date: 2016-04-07 14:52
Category: LGBTI, Nacional
Tags: colombia, Derechos Humanos, lgtbi, matrimonio igualitario
Slug: matrimonio-igualitario-es-una-realidad-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_9408-e1460138902142.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

##### [7 Abr 2016]

Con una **votación de 6 a 3 la sala plena de la Corte Constitucional** aprobó el matrimonio igualitario, uno de los pasos que sigue dando el país para garantizar los derechos sociales y civiles de la comunidad LGBTI que venían librando una ardua lucha frente a este tema.

La decisión se da luego de estudiar las tutelas presentadas por varias parejas del mismo sexo, en las que demandaban su derecho a unirse legalmente, con los mismos beneficios y garantías jurídicas de las parejas heterosexuales.

**“Ayer fue una fiesta democrática, lo que se hizo fue garantizar el Estado social de derecho”** asegura el abogado y defensor de  derechos de la comunidad LGTBTI, Germán Humberto Rincón. Por su parte, Mauricio Albarracin, columnista e integrante de Colombia Diversa, celebra la decisión y la ve como “un paso final” pues es “la última discriminación legal que se remueve de la legislación colombiana”.

Frente a esta decisión de la Corte Constitucional las voces en contra ya se dieron a conocer. El procurador **Alejandro Ordoñez**, afirmó que una reforma legal para frenar la decisión tomada pues considera que **“quedan definitivamente sepultados aspectos esenciales de la Constitución del 91”.** Por su parte, la senadora del Partido Liberal, Viviane Morales, dijo que será necesario que se hagan ajustes legislativos, debido a que el Congreso no ha debatido sobre ese tema.

Sin embargo, este fallo, según el abogado Rincón Perfetti, es **“una bofetada jurídica a quienes encarnan  fundamentalismos como el procurador Alejandro Ordoñez y la senadora Vivian Morales,** que han atacado nuestras cosas, pero curiosamente siendo abogados, tienen un perfil calificado y saben que los derechos humanos no pueden ser objeto de consulta ciudadana, pero lo que hay por debajo son sus creencias religiosas”.

Así mismo, Albarracin señala que este debate “ante todo es constitucional, lo que uno espera que se presente es argumentos constitucionales… **Sodoma y Gomorra, es una historia bíblica no un argumento constitucional,** ese es el error conceptual que  tiene los que se oponen al matrimonio igualitario”, y agrega, “realmente lo que se aceptó es el matrimonio civil en un Estado que es laico”.

Lo cierto es que se establece que  ningún notario o juez podrá negarse a celebrar este tipo de uniones, pues según el abogado, con el fallo, desaparece un documento “desconocido y absurdo”, por el cual los notarios y jueces podían negarse a llevar a cabo estas uniones. Sin embargo, organizaciones como Orgullo LGTBI, sostienen que este fallo debe complementarse con "**el reconocimiento social y económico de las personas homosexuales, bisexuales y transgénero".**

La iglesia y específicamente el Vaticano han anunciado que rechazan el matrimonio igualitario, frente a lo que Germán Rincón expresa que  la comunidad no pide a ninguna clase de iglesia que haga matrimonios, "estamos pidiendo que el Estado garantice los derechos”, y concluye **“Me preocupa que la iglesia salga a hablar mal en contra nuestra cuando la mayoría de sacerdotes son homosexuales y se quieren acostar con nosotros”**.

##### [Germán Humberto Rincón, Abogado comunidad LGTBI] 

<iframe src="http://co.ivoox.com/es/player_ej_11098681_2_1.html?data=kpadm52afJKhhpywj5aUaZS1k5uah5yncZOhhpywj5WRaZi3jpWah5yncajZ09LO0JC2rc%2FX0NOajabGs8jVxdSYpsrKqc%2Fn0NeYxdTRuc%2FdxcbRjbGrmKO9joqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mauricio Albarracín, Colombia Diversa]<iframe src="http://co.ivoox.com/es/player_ej_11098702_2_1.html?data=kpadm52bdJOhhpywj5WbaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5ynca7V1tfWxc7Tb6Lgw8bf1MbHrc%2BZk6iYpdTQs87WysaYps7aqdPnwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
