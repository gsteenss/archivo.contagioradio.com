Title: 5 documentales recogen la memoria del genocidio en Ruanda
Date: 2016-04-06 17:46
Author: AdminContagio
Category: 24 Cuadros, DDHH
Tags: documentales genocidio ruanda, Genocidio Ruanda
Slug: 5-documentales-recogen-la-memoria-del-genocidio-en-ruanda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/141024165944_ruanda_genocidio_bbc_624x351_afp_nocredit.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  

##### [6 Abr 2016] 

El 6 de abril de 1994 se dio inicio a uno de los episodios mas sangrientos de la historia africana. Motivaciones étnicas y políticas cobraron la vida de más de 800.000 personas de la población Tutsi, a manos del gobierno hutu y la minoria tutsi, quienes ostentaban el poder en Ruanda a partir de la independencia de Bélgica alcanzada en 1961.

El asesinato del general Juvenal Habyarimana, presidente de Ruanda desde el golpe de estado propinado a Grégoire Kayibanda en 1973, sumado al avance del Frente Patriótico Ruandés provocó una serie de masacres en toda la nación contra los tutsis, que derivó en desplazamientos masivos hacia campos de refugiados ubicados en los límites geográficos con países como Zaire.

Los crímenes de lesa humanidad cometidos en contra de los tutsis, incluyen la violación sexual a las mujeres producto de las cuales fueron asesinados más de 5 mil niños y el exilio  de casi 3 millones de ruandeses, situación que continua hasta nuestros días, a pesar que históricamente se considera que el genocidio terminó en el mes de julio.

La filmografía existente sobre el suceso histórico, acoge reconocidas producciones de ficción como "Hotel Ruanda" de Terry George  y "Disparando a perros" de Michael Caton-Jones y documentales, algunos de estos últimos disponibles on line.

**Los 100 días que no conmovieron al mundo**. Dir. Vanessa Ragone **(2009)**

Documental argentino que da cuenta de la labor de la jueza argentina Inés Weingber de Roca, integrante del Tribunal criminal internacional por Ruanda, que tiene la tarea asignada de juzgar los crímenes de guerra cometidos durante el genocidio de 1994, a partir de las investigaciones y entrevistas realizadas por Susana Reinoso.

<iframe src="https://www.youtube.com/embed/MSMU3KbKat4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Sevota, mujeres por empoderamiento y reparación genocidio Ruanda.** Prod Associació de Periodistes Fora de Quadre, y el Colectivo Contrast

Sevota es un grupo que trabaja por la reparación y el empoderamiento de las mujeres víctimas de la guerra. Trabajan en red y consiguieron reunir testimonios y documentación que permitió que en 1998 el Tribunal Penal Ruandés condenara por primera vez en el mundo la violencia sexual que sufrieron las mujeres ruandesas en la guerra civil y reconocieran la violación como acto de genocidio y de tortura.

<iframe src="https://www.youtube.com/embed/QcoZRl3zTMY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**Ruanda: La reconciliación obligada.** Dir Oriol Andrés, Carlos Castro, Gemma Garcia, Iolanda Parra **(2013)**

En demasiadas ocasiones, se considera que llega la paz a un territorio cuando se firman los acuerdos o finaliza la guerra, pero es justo entonces cuando arranca el complejo y difícil proceso de construcción de paz, memoria, verdad, reconciliación y justicia. El documental hace parte de la serie  'Después de la Paz. Bosnia, Líbano, Guatemala, Ruanda, Argentina, Camboya, Suráfrica', donde se pretende explicar y confrontar distintos caminos hacia la paz emprendidos en estos países.

<iframe src="https://www.youtube.com/embed/nPWS5FWi564" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**- Next Gen (Una generación después del genocidio en Ruanda)** Prod. MoMo Productions **(2013)**

Proyecto documental rodado durante un viaje de investigación a Ruanda en 2013 , como previo para la creación de una largometraje. La película muestra Ruanda hoy y la esperanza de la gente tiene que construir una sociedad en la que se excluye a nadie , o discriminado . ' Una reinvención de la humanidad ' .

<iframe src="https://www.youtube.com/embed/2Nsk6mdgXVo" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
-** Umurage** Dir. Gorka Gamarra **(2002)**

Documental producido durante la estancia de Gorka Gamarra en Ruanda, un jurista especializado en derecho internacional, durante tres años en los que sintió tan de cerca  la dura realidad vivida en el genocidio ruandés que se vio en la obligación de dar a conocerlo a través de este documental.

[![umurage](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/umurage.jpg){.aligncenter .wp-image-22345 width="599" height="337"}](https://archivo.contagioradio.com/5-documentales-recogen-la-memoria-del-genocidio-en-ruanda/umurage/)
