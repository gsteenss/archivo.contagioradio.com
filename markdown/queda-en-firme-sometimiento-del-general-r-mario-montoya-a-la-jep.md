Title: Queda en firme sometimiento del general (r) Mario Montoya a la JEP
Date: 2019-04-25 11:21
Author: CtgAdm
Category: Judicial, Paz
Tags: JEP, Mario Montoya, operación orion
Slug: queda-en-firme-sometimiento-del-general-r-mario-montoya-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/general_mario_montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

Acorde al Auto 131 de 2019, la Sección de Apelación de la Jurisdicción Especial para la Paz (JEP) ha confirmado el sometimiento del general en retiro **Mario Montoya Uribe**, al mecanismo de justicia, de igual forma confirmó el traslado del sometimiento y expedientes del general a la Sala de Reconocimiento que adelanta  el caso 003 vinculado a muertes ilegítimamente presentadas como bajas en combate por agentes del Estado y de este modo también adelantar el reconocimiento y citación de las víctimas.

Los representantes de las víctimas habían presentado recursos de apelación contra dos resoluciones de la Sala de Definición de Situaciones Jurídicas,  por medio de estas resoluciones la Sala rechazaba la solicitud de los apoderados que  buscaban anular la actuación de sometimiento del general Montoya, argumentando la violación de los derechos de las víctimas por "indebida notificación" argumentando que debían ser vinculados al trámite en la JEP y notificarlos del mismo para garantizar su intervención a todas aquellas personas que tengan el derecho de participar en la audiencia de sometimiento.

A pesar de ello, la Sección de Apelación negó todas las peticiones de las organizaciones conformadas por DH Colombia, el Cajar,  la Comisión Colombiana de Juristas, la Comisión Intereclesial de Justicia y Paz, la Asociación Minga y Humanidad Vigente Corporación Jurídica considerando que una eventual intervensión masiva de las víctimas podría obstruir el desarrollo de una jurisdicción  temporal como la JEP [(Lea también: Los casos por los que debería responder el Gral. Montoya ante la JEP)](https://archivo.contagioradio.com/casos-responder-montoya-ante-jep/)

Por tal motivo, la JEP ha estimado que "**la intervención de las víctimas debe ser proporcional con las características, propósitos y relevancia de cada proceso"**, dándoles prioridad pero también siendo coherente con el procedimiento que lleve a cabo el mecanismo judicial. [(Le puede interesar: "Montoya diga la verdad" exigen las víctimas)](https://archivo.contagioradio.com/montoya-diga-la-verdad-exigen-victimas/)

Según información de Human Rights Watch entre 2006 y 2008, años en los que Montoya estuvo al mando del Ejército "**al menos 2.500 civiles habrían sido víctimas de ejecuciones extrajudiciales", ** adicionalmente Montoya tendría que responder por violaciones a los derechos humanos cometidas durante la Operación Orión**, en la Comuna 13 de Medellín y que dejó un saldo de al menos 100 personas.**

[English Version](https://archivo.contagioradio.com/submission-of-general-mario-montoya-uribe-to-the-jep-has-been-confirmed/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
