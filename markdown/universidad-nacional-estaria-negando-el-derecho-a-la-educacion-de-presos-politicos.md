Title: Universidad Nacional estaría negando el derecho a la educación de presos políticos
Date: 2019-10-03 11:43
Author: CtgAdm
Category: Expreso Libertad
Tags: educacion, educación pública, Expreso Libertad, Falsos Positivos Judiciales, presos politicos, Universidad Nacional, Universidad Nacional de Colombia
Slug: universidad-nacional-estaria-negando-el-derecho-a-la-educacion-de-presos-politicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/universidad_nacional_16_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [FOTO DE WIKICOMMONS] 

En este programa del Expreso Libertad, familiares de presos políticos denunciaron que la Universidad Nacional estaría negando el derecho a la educación de César Barrera y Julian Gil, víctimas de falsos positivos judiciales, luego de que ambos cursaran materias de maestría en esa institución y esta se rehusara a que los muchachos continuarán con el curso de sus carreras.

De acuerdo con la abogada defensora de derechos humanos Gloría Silva, esta acción por parte de la Universidad Nacional, no solo demuestra un talante elitista sino también excluyente frente a quienes son admitidos en sus carreras, agregando que además incurrirían en una extralimitación de sus funciones, ya que estaría actuando como juez y no como centro dedicado a garantizar el acceso a la educación de quienes allí se matriculan.

(vea tambien: [Universidades Bajo S.O.Specha )](https://www.facebook.com/contagioradio/videos/2035696066532349/)

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F738084796631048%2F&amp;width=600&amp;show_text=false&amp;appId=1237871699583688&amp;height=338" width="600" height="338" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

 

(Le puede interesar: [La educación: el camino a la resocialización)](https://archivo.contagioradio.com/la-educacion-el-camino-a-la-resocializacion/)
