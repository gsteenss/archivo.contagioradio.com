Title: Avanza la Cátedra 'Sujetos Territoriales con Paz y Justicia Socioambiental' en el Bajo Atrato chocoano
Date: 2016-06-08 12:27
Category: Comunidad, Nacional
Tags: Cátedra Abierta 'Sujetos Territoriales con Paz y Justicia Socioambiental', Comisión Intereclesial de Justicia y Paz, Diálogos de paz Colombia
Slug: avanza-la-catedra-sujetos-territoriales-con-paz-y-justicia-socioambiental-en-el-bajo-atrato-chocoano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Cátedra-Bajo-Atrato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [8 Junio 2016 ] 

Desde este domingo y hasta el próximo sábado, se lleva a cabo la segunda etapa de la Cátedra Abierta 'Sujetos Territoriales con Paz y Justicia Socioambiental' en el Bajo Atrato chocoano, con la participación de **32 líderes y lideresas, entre jóvenes y adultos, indígenas afrodescendientes y mestizos** de Antioquia, Cundinamarca y Atlántico. Esta propuesta pedagógica es liderada por profesionales de la 'Comisión Intereclesial de Justicia y Paz'.

Durante estos días, los líderes y lideresas intercambiaran saberes en torno a las temáticas **territorio y ambiente, género e identidades y participación y afirmación de derechos**, todo ello, en relación con los avances de los diálogos de paz con las guerrillas de las FARC y el ELN, y la [[Minga Nacional](https://archivo.contagioradio.com/programas/paro-nacional/)] que se adelanta en 27 departamentos de Colombia. Los aportes serán replicados por los participantes en sus comunidades de origen para fortalecer sus procesos organizativos.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u) ]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
