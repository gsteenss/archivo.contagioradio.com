Title: Amenazas, asesinatos y despojo de tierras, situaciones que enfrentan las comunidades
Date: 2018-04-12 13:22
Category: DDHH, Nacional
Tags: acuerdo de paz, comunidades, Misión de Verificación, Organizaciones sociales, organizaciones sociales internacionales
Slug: amenazas-asesinatos-y-despojo-de-tierras-situaciones-que-enfrentan-las-comunidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/comundiades-amenazas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Abr 2018] 

Organizaciones internacionales de la sociedad civil presentaron sus hallazgos tras haber realizado la verificación de la **situación humanitaria y de derechos humanos** de las comunidades afectadas por el conflicto armado en diferentes territorios del país. Concuerdan que en todos los territorios visitados hay una deficiencia en la implementación del acuerdo de paz que está poniendo en riesgo a las comunidades.

Las organizaciones integrantes del Espacio de Cooperación para la Paz como Forum Syd, Fokus, Caritas entre otras, realizaron el acompañamiento a las comunidades del **Norte del Cauca, Meta, Caquetá y Huila** durante el segundo semestre de 2017.

### **Comunidades del Meta carecen de garantías para la** [**restitución**]** de tierras** 

En lo que tiene que ver con los hallazgos en el Meta, las organizaciones sociales evidenciaron una falta de garantías que tienen las comunidades para recuperar los territorios **de los cuales fueron despojados**. Esto ha desencadenado una serie de problemas que involucran el aumento de la violencia y la presión por parte de grupos paramilitares contra las comunidades que luchan por la restitución.

Aseguran que la Corte Constitucional falló a favor de las comunidades campesinas e indígenas que han sido víctimas de despojo y desplazamiento forzado producto de la **guerra y de la consolidación de proyectos agroindustriales** como la siembra de palma de aceite por parte de la empresa Poligrow. Afirmaron que pese a la promulgación de la sentencia, que indica que las instituciones del Estado deben garantizar los derechos a la tierra y el territorio, “no hay avances en ninguna de las órdenes de la Corte”.

Con esto en mente, recomendaron al Estado colombiano “prestar una especial atención e investigar la relación entre **los intereses económicos de terceros** y la restitución de tierras”. Recordaron que es necesario que se investigue “la persistencia de rutas de narcotráfico y otras economías ilegales en la zona como uno de los mayores factores de riesgo para la vida de las personas y organizaciones sociales que defienden la tierra y el territorio”. (Le puede interesar:["Protección a defensores de derechos humanos como garantía de paz"](https://archivo.contagioradio.com/proteccion-defensores-de-derechos-humanos/))

### **Se debe garantizar los derechos de los pueblos indígenas del Cauca** 

En el Norte de Cauca constataron que hay una situación de riesgo para los defensores de derechos humanos y líderes sociales teniendo en cuenta que **no hay protocolos de seguridad** acordados con las comunidades quienes expresaron su temor ante el aumento de amenazas y asesinatos.

Les preocupa la presencia de nuevos grupos armado ilegales en los territorios, que en el Norte del Cauca **son 12 grupos que han amenazado** a las comunidades para obtener el control territorial. Estos grupos “se apoyan en bandas delincuenciales y de sicarios, aumentando el nivel de asesinatos selectivos”.

Una preocupación adicional es la **lentitud con la que avanza la implementación** de lo acordado en la Habana en el punto de la Reforma Rural Integral, la Participación Política, la Reincorporación de las FARC a la vida civil y la Sustitución de Cultivos de Uso Ilícito. De manera paralela, alertan sobre el aumento en la violencia en contra de las mujeres en la medida en que entre enero y noviembre de 2017, las comunidades reportaron 52 asesinatos de mujeres en el Cauca.

Ante estas problemáticas, recomendaron mantener la protección a líderes y defensores de derechos humanos **sin olvidar la** [**importancia**]** de la Guardia Cimarrona**, indígena y campesina. Para esto, es “indispensable que el Gobierno Nacional reconozca los derechos territoriales y ancestrales de los pueblos indígenas garantizando el derecho a la consulta previa y cumplimiento de los acuerdos con las comunidades indígenas”.

### **Caquetá y Huila debe trabajar más por la reconciliación** 

En Caquetá y Huila, hay **“mucha lentitud en la implementación”** del Acuerdo de Paz y esto ha llevado a que, por ejemplo,  los ex combatientes salgan de los espacios de reincorporación poniendo en riesgo el derecho a la no repetición de los actos violentos. También, el aumento de la polarización política, ha puesto en riesgo el proceso de reconciliación. (Le puede interesar:["La falta de implementación de los acuerdos de paz impide la paz territorial"](https://archivo.contagioradio.com/la-falta-de-implementacion-de-los-acuerdos-de-paz-impide-la-paz-territorial/))

Frente al tema de las mujeres, las organizaciones sociales alertaron sobre las barreras que existen para ellas al momento de **acudir a la justicia en casos donde han sido víctimas** de violencia. Además, manifestaron que las mujeres no se sienten recogidas dentro del enfoque de género que plantea el Acuerdo de Paz en la medida en que no tienen las garantías suficientes para la participación política.

Por esto recomendaron que  se continúes con la pedagogía de la paz para contribuir con la implementación de lo acordado y así **“bajar los niveles de polarización,** máximo ahora que se viene un escenario electoral”. Afirmaron que se deben propiciar mensajes de reconciliación y respeto por la vida con el fin de “romper el vínculo entre política y armas”.

Finalmente, recordaron la importancia **del papel del Estado** para garantizar una implementación efectiva a la vez que reiteraron su respaldo “al legítimo trabajo que realizan las organizaciones sociales colombianas para hacer realidad la paz”. Reafirmaron el compromiso de las organizaciones sociales internacionales para procesos de verificación como el que se realizó.
