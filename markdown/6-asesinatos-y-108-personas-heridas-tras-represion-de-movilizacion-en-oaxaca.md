Title: 6 asesinatos y 108 personas heridas tras represión de movilización en Oaxaca
Date: 2016-06-20 13:24
Category: El mundo
Tags: asesinato maestros Oaxaca, reforma educativa México, represión policial México
Slug: 6-asesinatos-y-108-personas-heridas-tras-represion-de-movilizacion-en-oaxaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Oaxaca-represión.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Resumen Latinoamericano ] 

###### [20 Junio 2016] 

Tres meses en paro completa el magisterio mexicano en rechazo a la reforma que pretende imponer el gobierno de Enrique Peña Nieto, que afectaría las condiciones laborales de los docentes y la calidad educativa. Estas movilizaciones que han logrado el apoyo de madres, padres de familia y estudiantes, han sido brutalmente reprimidas en varios de los Estados, la acción más violenta ocurrió **este domingo en Oaxaca cuando la Policía atacó a la población. De este hecho resultaron 6 personas muertas y otras 108 heridas, entre ellas 55 policías. **

De acuerdo con Laura Melchor, del 'Comité de Defensa Integral de Derechos Humanos Gobixha AC', desde las 8 de la mañana de este domingo, un contingente de 4 mil policías fuertemente armados inició el desalojo de los puntos de concentración en Oaxaca y **con armas de fuego, balas de goma y gases lacrimógenos, atacaron** **a los padres y madres de familia, jóvenes y maestros** que se estaban manifestando.

Melchor asegura que mientras los policías atacaban a los maestros, padres y estudiantes, el Gobernador de Oaxaca estaba en una fiesta y desde allí habría **ordenado a directivas de los hospitales públicos no atender a los civiles que solicitaran atención**, lo que dificultó que los heridos fueran atendidos oportunamente e imposibilitó que seis de los pobladores se salvaran, porque además la Policía rodeó los centros hospitalarios.

Las organizaciones reportan que **20 personas permanecen desaparecidas y otras 20 fueron detenidas**, mientras intentaban impedir que la Policía avanzara hacia el Zócalo, en dónde se presentaron apagones e interceptaciones de la red de Internet, además de amenazas de desalojo; sin embargo, maestros, padres y estudiantes se mantienen en movilización y este lunes marchan desde Oaxaca hasta este punto, para insistir en que se establezca una mesa de negociación con el Gobierno.

Conozca el más reciente comunicado de la 'Coordinadora Nacional de Trabajadores de la Educación de México' a propósito de los hechos.

[![Comunicado Oaxaca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Comunicado-Oaxaca.jpg){.aligncenter .size-full .wp-image-25523 width="742" height="960"}](https://archivo.contagioradio.com/policias-asesinan-a-6-manifestantes-y-dejan-heridos-a-otros-108-en-oaxaca/comunicado-oaxaca/)

Vea también: [[32 buses con docentes son retenidos por la Policía](https://archivo.contagioradio.com/mexico-32-buses-con-docentes-son-retenidos-por-la-policia/)]

<p>
   

<iframe src="https://co.ivoox.com/es/player_ej_11967222_2_1.html?data=kpammJyWdpOhhpywj5ecaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5ynca3V1tfOjbLJsMTc0NeYj5CxaaSnhqam2s7Hs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
  

<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
