Title: Julio: Violación de niña embera por las FFMM, sanciones internacionales y exigencia de matrícula cero
Date: 2020-12-31 11:17
Author: CtgAdm
Category: Especiales Contagio Radio, Nacional, Resumen del año
Tags: embera, FFMM, julio, Matricula Cero, niña embera
Slug: julio-violacion-de-nina-embera-por-las-ffmm-sanciones-internacionales-y-exigencia-de-matricula-cero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En el mes de Julio los escándalos para las FFMM no paraban. Se conoció el caso de la violación de una niña embera por parte de 8 soldados en el departamento de Risaralda, una de las consecuencias conocidas fue el retiro del cargo del Sargento Juan Carlos Días que fue quien denunció el abuso y puso a la niña a disposición de las autoridades que debían “restablecer sus derechos”. Este hecho fue interpretado como el castigo para las personas que denuncian actividades delictivas y un llamado a quedarse en silencio ante los crímenes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente la política exterior del gobierno Duque se desmoronaba. Se conoció en Julio que por lo menos 10 relatores de la [ONU](https://www.un.org/es/) habían pedido ingreso al país para estudiar las denuncias por violaciones a los DDHH y a todos ellos se les había negado la entrada o su trámite se estaba haciendo más engorroso que lo normal. Como consecuencia de este tipo de medidas que evidencian la línea de las relaciones internacionales del gobierno Duque, el congreso de EEUU condicionó de nuevo la ayuda militar y económica a Colombia al cumplimiento de los DDHH, la implementación del Acuerdo de Paz y la investigación y prevención de los asesinatos de líderes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al interior de Colombia la situación por el COVID no mejoraba para las grandes mayorías. En este mes de Julio por lo menos 18 centros de educación superior entraron en huelga exigiendo matrícula cero, entre otras peticiones que pretendían mejorar el sistema educativo que no estaba, ni lo está, preparado para asumir la virtualidad con las exigencias mínimas para mantener un estándar alto en la calidad de la educación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Retiran del Ejército a sargento que denunció el abuso de niña Embera

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego que el país conociera los hechos del abuso de la niña indígena en Risaralda por parte de 8 militares, este 1 de julio se conoció el comunicado del Comando Central del Ejército en el que se tomó la decisión de retirar al sargento **viceprimero Juan Carlos Díaz**, militar que comandaba a los uniformados que abusaron de la menor.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El comando general  Eduardo  Zapateiro, señalo “Como comandante de los soldados involucrados en la violación, era su obligación moral, institucional y legal informar, como es el deber de cualquier funcionario, era lo mínimo que debía hacer. [Lea también: Piden a la JEP abrir caso sobre violencia sexual](https://archivo.contagioradio.com/pedimos-a-la-jep-que-se-abra-un-caso-de-violencia-sexual-humanas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Retiran del Ejército a sargento que denunció el abuso de niña Embera](https://archivo.contagioradio.com/retiran-del-ejercito-a-sargento-que-denuncio-el-abuso-de-nina-embera/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Colombia acumula más de 10 solicitudes sin aceptar de Relatores de la ONU"

<!-- /wp:heading -->

<!-- wp:paragraph -->

En medio de la pandemia **ha sido evidente el incremento de la violencia y el riesgo de los liderazgos sociales en Colombia**, al mismo tiempo que los intereses del Gobierno se enfocaban en la contención de la pandemia, la erradicación forzada y la militarización de los territorios como solución para todo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto la comunidad internacional ha hecho evidente su preocupación, frente a los llamados de colectivos defensores de la vida en Colombia que solicitan el apoyo externo, **al mismo tiempo que el Gobierno pareciera ser de oídos sordos y selectivo frente a los mensajes internacionales que adopta.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

["Colombia acumula más de 10 solicitudes sin aceptar de Relatores de la ONU"](https://archivo.contagioradio.com/colombia-acumula-mas-de-10-solicitudes-sin-aceptar-de-relatores-de-la-onu/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Congreso de EEUU condiciona ayuda militar a Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este 21 de julio, la Cámara de Representantes en Washington aprobó, con 295 votos a favor, dos condiciones a la ayuda militar de ese país hacia Colombia. Las medidas fueron tomadas después de los escándalos reiterados por operaciones ilegales de las FFMM contra la oposición y los problemas que ha significado la persistencia de las aspersiones aéreas y las erradicaciones de los cultivos de uso ilícito

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Congreso de EEUU condiciona ayuda militar a Colombia](https://archivo.contagioradio.com/congreso-de-eeuu-condiciona-ayuda-militar-a-colombia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Matrícula Cero: la clave para garantizar la educación pública superior

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mientras instituciones como la Universidad de Nariño que logró un aporte de 1.800 millones de parte de la gobernación del departamento, o la Universidad del Tolima y sus más de 13.000 estudiantes han consolidado la matrícula cero con un monto inicial de 6.000 millones de pesos, estudiantes de diferentes instituciones de educación pública superior continúan a la espera de los entes territoriales para poder acceder a un estudio digno para el segundo semestre del 2020 en medio de la pandemia del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Matrícula Cero: la clave para garantizar la educación pública superior](https://archivo.contagioradio.com/matricula-cero-la-clave-para-garantizar-la-educacion-publica-superior/)

<!-- /wp:paragraph -->
