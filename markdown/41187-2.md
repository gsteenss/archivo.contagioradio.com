Title: “Aún soñamos con ver regresar a nuestros familiares desaparecidos” ASFADDES
Date: 2017-05-26 15:18
Category: DDHH, Paz
Tags: ASFADDES, desaparecidos, Desaparición forzada, víctimas
Slug: 41187-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [26 May. 2017]

A propósito de la **Semana Internacional por los Desaparecidos**, cientos de familiares asociados en ASFADDES (Asociación de Familiares de Detenidos Desaparecidos), están llevando a cabo numerosas actividades para **visibilizar y sensibilizar a la sociedad de la situación y de la lucha de los familiares de las víctimas** de desaparición forzada en Colombia, que según cifras oficiales son 22 mil personas.

“Seguimos trabajando por encontrarlos y porque ellos y ellas sean reconocidos y dignificados, de eso se trata la semana internacional que hacemos hoy en Colombia” manifestó **Gloria Gómez directora de ASFADDES.**

### **Estas son las actividades de la Semana contra la desaparición forzada** 

En Medellín se están llevando a cabo varias actividades como galerías fotográficas, plantones y acciones artísticas, a las que se han sumado otras seccionales de ASFADDES como Piedecuesta, Girón, Bucaramanga, Cúcuta, Neiva, Popayán, Aguachica y Barrancabermeja. Le puede interesar: [23.441 personas han desaparecido de manera forzada en Colombia](https://archivo.contagioradio.com/la-fiscalia-no-ha-avanzado-en-nada-en-la-busqueda-de-desaparecidos-gloria-gomez/)

“Estamos mostrando los rostros de los desaparecidos, hablando de ellos y ellas como ha siempre ha sido nuestro compromiso” recalcó Gómez. Le puede interesar: [El Estado colombiano es el responsable del 84% de las desapariciones forzadas](https://archivo.contagioradio.com/el-estado-colombiano-es-el-responsable-del-84-de-las-desapariciones-forzadas/)

En la capital ya se han realizado varias actividades como un foro con estudiantes universitarios y se prevé que **para el martes 30 de mayo se realice un taller de stencil para plasmar en lienzo los rostros de los desaparecidos de familiares** de personas en Bogotá, para que se pueda construir lo que ASFADDES ha llamado “la colcha de la memoria”.

Por último, el **miércoles 31 la cita será en la Plaza de Lourdes para compartir una galería fotográfica y una muestra artística** que impulsa la Comisión Nacional de Búsqueda de Personas Desaparecidas “allí habrá presencia de familiares y personas de los sectores sociales de los departamentos de Boyacá, Tolima y otras regiones del centro del país, acompañando esta actividad” dice Gómez.

### **Sí a la paz sin desaparecidos** 

Luego de 35 años de búsqueda de personas desaparecidas, Gloria Gómez manifiesta que a propósito de los acuerdos de paz, este año la Semana Internacional tiene dos escenarios “uno de esperanza y otro de preocupación por la puesta en marcha de lo acordado en La Habana”. Le puede interesar: [Estado no mostró voluntad política en la CIDH frente a desaparición forzada: CCJ](https://archivo.contagioradio.com/estado-no-muestra-voluntad-politica-en-el-tema-de-desaparicion-forzada/)

Gómez hace referencia a la entrada en vigor de la Unidad para la Búsqueda de personas dadas por desaparecidas, que es un instrumento que nace a raíz del punto 5 de los Acuerdos de Paz firmados entre el Gobierno y la guerrilla de las FARC. Le puede interesar: Le puede interesar: [Fiscalía ha suspendido 35 mil investigaciones sobre desaparición forzada](https://archivo.contagioradio.com/fiscalia-ha-suspendido-35-mil-investigaciones-sobre-desaparicion-forzada/)

“Desde la justicia, desde la Fiscalía no ha sido posible un resultado eficaz para la ubicación, localización, identificación plena y entrega digna, **nos llena de esperanza el tener ahora esta unidad que esperamos que realmente cumpla con el objetivo** para lo cual fue solicitada, que es producto de las exigencias de los familiares de desaparecidos, pero nos preocupa la posición de la Corte Constitucional” recalcó Gómez.

### **“Aún soñamos con ver regresar a nuestros familiares vivos”** 

Para Gómez, los familiares de las víctimas de desaparición forzada aún sueñan con encontrar a sus familiares con vida, sin embargo, de no ser así han puesto sus esperanzas en esta Unidad de Búsqueda para que cumpla con la búsqueda de sus seres queridos. Le puede interesar: [Banco Genético ha sido fundamental para las víctimas de desaparición forzada](https://archivo.contagioradio.com/banco-genetico-podria-ayudar-a-encontrar-personas-desaparecidas-forzadamente/)

“Que todas las personas desaparecidas puedan ser buscadas, encontradas y puedan ser entregadas en condiciones de dignidad. El sueño de todos es que nos los devuelvan vivos, pero en el caso donde los cuerpos van a ser rescatados de las fosas, **vamos a iniciar este proceso de cicatrización de las heridas que este conflicto** político, social y armado nos ha dejado” concluyó Gómez.

### **La desaparición en cifras** 

Según los datos de Medicina Legal a 2016 habían 23.441 personas desaparecidas de manera forzada en Colombia y las cifras que maneja la Unidad para las Víctimas en el país son por lo menos **45.944 personas desaparecidas** en el marco del conflicto armado interno y un total de 116.344 son reconocidas como víctimas indirectas.

**De los 19.625 casos de mujeres desaparecidas, 11.297 se presentaron entre los años 2004 y 2006, durante el gobierno de Álvaro Uribe Vélez,** y empezaron a disminuir a partir del año 2010.

La Fiscalía da cuenta de 60 mil personas y las organizaciones de familiares víctimas de desaparición forzada, como ASFADDES, Familiares Colombia, la Fundación Nidia Erika Bautista, MOVICE, y Desaparecidos del Palacio de Justicia, registran alrededor de 45 mil desaparecidas y desaparecidos forzados. Le puede interesar: [99% de los casos de desaparición forzada están en la impunidad](https://archivo.contagioradio.com/99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad/)

<iframe id="audio_18922368" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18922368_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
