Title: Cientos de personas y organizaciones hacen llamado para que se prolongue el cese bilateral entre Gobierno y ELN
Date: 2018-01-04 11:13
Category: Nacional, Paz
Tags: Cese al fuego, ELN, Gobierno Nacional, prolongación del cese al fuego
Slug: cientos-de-personas-y-organizaciones-hacen-llamado-para-que-se-prolongue-el-cese-bilateral-entre-gobierno-y-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/mesa-de-dialogos-eln-gobierno-copia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AlbaTv] 

###### [04 Ene 2018] 

Un grupo de más de 170 personas de diversas orientaciones políticas y de diferentes regiones del país entre los que se encuentran académicos, activistas por la paz, empresarios y líderes sociales, enviaron una carta pública al Gobierno Nacional y al ELN para que se prolongue el cese bilateral al fuego.

En la carta hacen énfasis en la necesidad de avanzar en las negociaciones del proceso de paz teniendo en cuenta la importancia del cese bilateral al fuego. Reiteraron que “la salida negociada del conflicto armado es una prioridad” y reconocen que los esfuerzos de las partes en la mesa de Quito ha beneficiado a las comunidades en las diferentes regiones del país.

Además, manifiestan que el cese al fuego“brinda un mejor contexto para la participación de la sociedad en la construcción de paz y aumenta la legitimidad social del proceso de diálogo”. Sin embargo recordaron que es necesario que se investiguen “de manera eficaz” los incidentes que se han presentado. (Le puede interesar: ["ELN pactará otro cese al fuego cuando se "superen" falencias del cese actual"](https://archivo.contagioradio.com/eln-pactara-otro-cese-al-fuego-cuando-se-superen-falencias-del-cese-actual/))

### **Ruptura del cese al fuego distanciaría a las partes de la negociación** 

El texto enviado enfatiza en que el no prolongar el cese al fuego bilateral, “significaría muerte dolor y desplazamiento para la población civil que suele ser la más afectada del conflicto, desmotivaría la participación de la sociedad, y daría más excusas a los enemigos de la paz que hacen de la guerra una bandera electoral”.

Por esto, y teniendo en cuenta que el cese bilateral al fuego finaliza este 9 de enero, los firmantes le pidieron a las partes que se mantenga la decisión de continuar con los diálogos “por medio de un cese bilateral, haciendo los ajustes que se requieran, pero sin dejar espacio a la guerra.”

Entre quienes firman se encuentran Ricardo Alvarado, gobernador de Arauca, José Luis Diago Franco, rector de la Universidad del Cauca, Víctor de Currea-Lugo, profesor Universidad Nacional de Colombia, Piedad Bonet, escritora, Camilo González Posso, presidente Indepaz, Diego Otero, rector Corporación Universitaria de Ciencia y Desarrollo, Mauricio Rodríguez, profesor y periodista, German Castro Caicedo, escritor, Mónica de Greiff, presidenta Cámara de Comercio de Bogotá, Carlos Enrique Cavelier, presidente de La Alquería Gustavo Wilches-Chaux, ambientalista Patricia Lara, escritora.

### **Congresistas se unieron al llamado de prolongar el cese al fuego ** 

Al llamado de las personas y organizaciones se unieron diferentes congresistas entre ellos el senador Iván Cepeda y el representante Alirio Uribe. Ellos le solicitaron al Jefe Negociador con el ELN Gustavo Bell y a su contra parte, Israel Ramírez Pineda, que "mantengan el cese al fuego bilateral, como un acto de voluntad de ambas partes para iniciar el quinto ciclo de negociaciones en un ambiente pacífico y armónico".

Cepeda indicó que "es importante también valorar los avances que se han dado en lo que se refiere al primer punto del programa de negociación que ha contado con la participación de 192 representantes de 181 organizaciones sociales en las audiencias públicas que se han celebrado en esta fase, que le han dado legitimidad al proceso” .

Por su parte, Alirio Uribe reconoció la presencia de las Naciones Unidas en muchas de las zonas de conflicto y pidió que las partes den muestras de voluntad para el desarrollo del proceso de paz con el ELN. Los congresistas le hicieron un llamado a la sociedad civil para que siga apoyando el proceso de negociación.

Por esto, realizaron una petición a través de la plataforma de recolección de firmas [Change.org](https://www.change.org/p/juanmansantos-pazcompleta-firma-para-que-se-mantenga-el-cese-al-fuego-entre-el-gobierno-y-el-eln?recruiter=560397929&utm_source=share_petition&utm_medium=twitter&utm_campaign=psf_combo_share_message), "con el fin de poderles hacer llegar a las partes el sentimiento y anhelos de paz completa de los colombianos".

<iframe id="doc_98275" class="scribd_iframe_embed" title="Carta Por El Cese Gob-ELN" src="https://www.scribd.com/embeds/368442646/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-9lTtbMLCLSc1RMUnlzn4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  {#section .osd-sms-wrapper}
