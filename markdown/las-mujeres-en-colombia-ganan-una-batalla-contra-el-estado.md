Title: Mujeres logran creación del primer subcomite institucional de género en Medellín
Date: 2018-12-13 17:12
Author: AdminContagio
Category: Movilización, Mujer
Tags: Federico Gutiérrez, Medellin, mujeres, violencia sexual
Slug: las-mujeres-en-colombia-ganan-una-batalla-contra-el-estado
Status: published

###### [Foto: Gabriel Galindo] 

###### [13 Dic 2018]

Un fallo de la Justicia Colombiana ordenó la creación del subcomité de género en Medellín, luego de que distintas organizaciones demostraran las revictimizaciones que sufren las mujeres en las políticas públicas de víctimas de esa ciudad, en donde han sido omitidas y no se han garantizado sus derechos.

El litigio comprobó, a partir de las tutelas de 5 mujeres, que **no existe un espacio interinstitucional en el que se aborden las situaciones de riesgo y violencia que afectan principalmente a las mujeres y niñas** por el accionar de actores armados y la ausencia de un enfoque diferencial y con perspectiva de género que ha provocado respuestas dilatorias o evasivas desde los órganos institucionales.

De acuerdo con la vocera de la Colectiva Justicia Mujeres, Natalia Álvarez, también se pudo comprobar "que la vulneración de sus derechos, no solo obedece a la situación de violencia", sino a un desconocimiento de las necesidad e intereses de las mujeres. (Le puede interesar:["Mujeres realizan el primer Tribunal a la justicia patriarcal en Colombia"](https://archivo.contagioradio.com/justicia-patriarcal-mujeres/))

El fallo ordena a 7 instituciones estatales, entre ellas 3 dependencias de la Alcaldía de Medellín, cumplir un plan de intervención "para garantizar a las mujeres la satisfacción de sus derechos y la reparación integral" a partir del enfoque de género, teniendo en cuenta que según la Encuesta de Prevalencia Sexual 2010-2015, en esta ciudad las mujeres afrontan la posibilidad de **sufrir el doble de violencia sexual, con una tasa del 34%, comparada al 18% del resto del país. **

El fallo también recordó que "la protección material de los derechos se logra con la entrega efectiva de beneficios y atención", y que tanto la Alcaldía de Medellín como la Gobernación de Antioquia, deben tomar un papel protagónico con respecto a la medidas de rehabilitación en salud, es decir garantizar que las mujeres tengan una atención que les permita **la recuperación de su salud física, sexual, mental y social, luego de haber sido víctimas del conflicto armado**.

Finalmente Álvarez señaló que esta es una medida histórica, debido a que se da en un momento en el que la Alcaldía de Medellín viene tomando una serie de decisiones regresivas, como la reducción del presupuesto y el debilitamiento del equipo municipal de atención a víctimas, **"por lo tanto creemos que con este fallo gana la ciudad y ganan las mujeres" afirmó. Se espera que el próximo 14 de diciembre se de la orden institucional para el inicio de las funciones de ese subcomite de género.**

###### Reciba toda la información de Contagio Radio en [[su correo]
