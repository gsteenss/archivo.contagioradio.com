Title: Se utilizarán drones para avanzar en la búsqueda de los 43 normalistas desaparecidos de Ayotzinapa
Date: 2015-10-30 16:51
Category: DDHH, El mundo
Tags: 43 normalistas desaparecidos de Ayotzinapa., CIDH, Derechos Humanos, drones, mexico, Municipio de Cocula, Serán utilizados drones para continuar con la búsqueda de los 43 normalistas desaparecidos de Ayotzinapa
Slug: se-utilizaran-drones-para-avanzar-en-la-busqueda-de-los-43-normalistas-desaparecidos-de-ayotzinapa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/NORMALISTAS.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### lopezdiroga 

###### [30 oct 2015]

La Procuraduría General de la República de México **anunció este jueves que se utilizarán drones para continuar con la búsqueda de los 43 normalistas** desaparecidos de Ayotzinapa.

El subprocurador de Derechos Humanos de la PGR, Eber Omar Betanzos afirmó que la búsqueda con vehículos aéreos fue determinada debido que los familiares de los normalistas gestionaron una nueva investigación  por medio de tecnología. Los expertos determinan que “**el uso de drones es una tecnología que nos permitirá observar remociones en la tierra, en posibles acciones de búsqueda”.**

Betanzos aclaró que la búsqueda de los normalistas **se realizará por el grupo interdisciplinario de expertos independientes de la Comisión Interamericana de Derechos Humanos** en el basurero del municipio Cocula donde fueron encontrados algunos los restos de los normalistas.

Esta investigación surge después de que los padres y la CIDH no aceptaran la versión de las autoridades mexicanas, donde se señalaba que los estudiantes fueron incinerados en un basurero en el municipio de Cocula, a su vez se confirma que el Grupo Interdisciplinario de Expertos Independientes (GIEI) de la **CIDH permanecerá en el país otros seis meses para continuar con la investigación .**
