Title: Organizaciones piden que no se militarice lucha contra cultivos de uso ilícito
Date: 2017-08-24 14:50
Category: Nacional, Paz
Tags: acuerdos de paz, coca, Cultivos de coca, Cultivos de uso ilícito, Humanas, WOLA
Slug: ong-nacionales-e-internacionales-preocupados-por-erradicacion-forzada-de-cultivos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/coca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [24 Ago. 2017] 

En una carta dirigida al presidente Juan Manuel Santos, varias organizaciones internacionales y nacionales **le pidieron al primer mandatario que cumpla con los acuerdos que se han hecho con las comunidades en materia de erradicación** y sustitución de cultivos de uso ilícito, además mostraron su profunda preocupación por el uso de militares para llevar a cabo dicho proceso.

Dice **Luz Piedad Caicedo, integrante de la Corporación Humanas,** que esta carta nace luego del seguimiento que vienen haciendo al proceso de implementación de los Acuerdos de Paz y en este caso al punto 4 que habla del tema de cultivos de uso ilícito. Le puede interesar: [“Erradicación forzada y sustitución no son compatibles con la implementación”: Cesar Jerez](https://archivo.contagioradio.com/erradicacion-forzada-y-sustitucion-no-son-compatibles-con-la-implementacion-cesar-jerez/)

**“Vemos que hay contradicciones en la forma como se está llevando a cabo la sustitución de cultivos.** Por ejemplo, Wola ha hecho trabajo de campo para verificar lo que está ocurriendo y organizaciones colombianas hemos estado en contacto con las comunidades analizando y preguntándoles cómo va el proceso. Nos han contado que ha habido confrontaciones con la Fuerza Pública por la erradicación forzosa”.

### **Militarización del proceso de sustitución de cultivos** 

Caicedo recuerda que en el acuerdo se plantea de manera clara que la sustitución es **la alternativa para que el gobierno asuma que se debe resolver esta problemática sin el uso de la fuerza** y además es la posibilidad de abrir espacios para que las comunidades erradiquen siempre y cuando haya sustitución. Le puede interesar: [Campesino herido en protestas contra erradicación forzada en el Meta](https://archivo.contagioradio.com/protestas-erradicacion-forzada-puerto-rico-meta/)

“Lo que vemos ahora es que ha habido erradicación forzada y que esa erradicación se ha llevado a cabo en compañía de agentes armados lo que genera una altísima conflictividad y pues las comunidades están diciendo que no entienden si el Gobierno plantea en el Acuerdo de Paz, que los iban a consultar y si no lo han hecho ¿por qué están erradicando de manera forzada?”.

Para Caicedo cabe recalcar que **no se está hablando de unos campesinos criminales así la conducta este penalizada**, porque en este caso no se trata de criminales sino de un campesinado excluido, que habita zonas aisladas, que es muy pobre y ha visto una posibilidad de subsistencia en los cultivos declarados de uso ilícito. Le puede interesar:[ "Plan de sustitución de cultivos de uso ilícito debe ser integral o no durará: COCCAM"](https://archivo.contagioradio.com/sustitucion-cultivos-ilicitos-cocam/)

### **¿Qué ha pasado con los acuerdos firmados con las comunidades?** 

En la actualidad existen algunas comunidades que han firmado acuerdos con el Gobierno para realizar un proceso de erradicación voluntaria, sin embargo, la integrante de Humanas manifiesta que no solo se trata de erradicar, sino también de sustituir cultivos con planes de desarrollo agrícolas que se diseñan con las ellos y ellas.

Por otro lado, hay otras comunidades en el país a las que no se les ha dicho nada y han sido amedrantados “la Fuerza Pública entra con los erradicadores, a erradicar forzosamente las plantaciones. **No hay una actuación coherente entre las distintas fuerzas del Gobierno,** por un lado va la Dirección de Sustitución de Cultivos, y por otro lado va el Ministerio de Defensa con la erradicación forzada”.

### **Hay un desconcierto en la comunidad** 

Las comunidades que han sido testigos de esta situación de erradicación forzosa han manifestado su descontento con el Gobierno, quienes, pese a la poca credibilidad que tienen con el Estado, han depositado su confianza en el cumplimiento de los Acuerdos de Paz que garantizan la erradicación y sustitución de cultivos.

“Cuando hay acuerdos el Gobierno ha incumplido entonces **el Estado está perdiendo la posibilidad de que nuevamente haya confianza en las instituciones** y que haya confianza en que las cosas que se acuerdan se cumplan”. Le puede interesar: [Ejército continúa haciendo erradicación forzada en el Carmelo, Cauca](https://archivo.contagioradio.com/ejercito-continua-haciendo-erradicacion-forzada-en-el-carmelo-cauca/)

### **Qué viene luego de esta carta** 

Las organizaciones firmantes esperan que la carta ayude a que haya una actuación coherente de parte del Gobierno, que vaya acompañado del reconocimiento de haber firmado un Acuerdo de Paz, que es de obligatorio cumplimiento y que debe desarrollarse de la mano de las comunidades.

“**Es imposible consolidar la paz si el Gobierno incumple con los acuerdos pactados y si las comunidades perciben que lo que les están diciendo no es real**, sino que nuevamente pasan a ser atacados por la Fuerza Pública. Por eso esperamos coherencia e implementación de los acuerdos como quedaron plasmados en el papel”.

<iframe id="audio_20508268" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20508268_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
