Title: Fiscalía debe investigar los crímenes de lesa humanidad cometidos por el DAS
Date: 2017-07-07 14:32
Category: DDHH, Nacional
Tags: crímenes de estado, das, Fiscalía, víctimas
Slug: victimas-del-das
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/fotoilustracion_chuzadas.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/chuzadas-DAS.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 Jul 2017] 

Las víctimas de los crímenes cometidos por el DAS durante la presidencia de Álvaro Uribe, manifestaron **su descontento por la salida de prisión de Bernardo Moreno**, quien fue el secretario general de la presidencia de Uribe Vélez. Según ellos y ellas aún hay crímenes de la central de inteligencia de Colombia que no se han investigado.

El juzgado quinto de ejecución de penas de Bogotá dejó en libertad a Moreno al considerar que ya cumplió con la reparación a las víctimas y no representa un peligro para la sociedad. Sin embargo, según el abogado Luis Guillermo Pérez quien representa a las víctimas, “estos crímenes de Estado no solo fueron interceptaciones y concierto para delinquir, **la Fiscalía tiene que investigar los delitos de persecución política y amenazas que ocurrieron durante ese gobierno**”. (Le puede interesar: ["La impunidad en crímenes de Estado es del 90%: Alejandra Gaviria"](https://archivo.contagioradio.com/la-impunidad-en-crimenes-de-estado-es-del-90-alejandra-gaviria/))

Pérez manifestó además que la libertad de Moreno **no debe ser considerada como un final en las investigaciones del DAS** en la medida que “hay crímenes que deben ser considerados de lesa humanidad y que no han sido esclarecidos”.  El abogado fue enfático en recalcar que “la Fiscalía debe tener compromiso con las investigaciones y ya hemos realizado varios reclamos ante la ONU y la CIDH”.

Para las víctimas, las investigaciones por los delitos cometidos en el marco de las “chuzadas” del DAS “**es un capítulo que no se ha cerrado hasta que no se restablezcan los derechos de las víctimas**, se conozca la verdad y se repare a la sociedad colombiana que termina perjudicada cuando el servicio de inteligencia del gobierno comete los crímenes y no los previene”. (Le puede interesar: ["Chuzadas" confirmaría "cacería criminal" durante gobierno Uribe"](https://archivo.contagioradio.com/chuzadas-confirmaria-caceria-criminal-durante-gobierno-uribe/))

**Fiscalía colombiana estaría obstruyendo la investigación por crímenes del DAS en Europa**

Las investigaciones de las interceptaciones ilegales que hizo el DAS a organizaciones no gubernamentales europeas que trabajaban con sus pares en Colombia, están paralizadas. Según Pérez “**la Fiscalía colombiana no ha autorizado que una comisión europea venga al país** a indagar por los crímenes que cometió esta institución y que traspasaron fronteras colombianas”.

Esta negativa de la Fiscalía es entendida por el abogado como “**un obstáculo que muestra que el Estado quiere cerrar un capítulo donde prevalezca la impunidad** para los responsables”. Adicionalmente recordó que el proceso por este episodio contra el hoy senador Álvaro Uribe “aún no avanza, tantos años de dilación es una forma de negación de los crímenes de Estado”. (Le puede interesar: ["Actividades criminales del DAS a punto de prescribir"](https://archivo.contagioradio.com/a-juicio-exdectectives-del-das-35421/))

Finalmente, Pérez se refirió al caso de la ex directora del DAS Maria del Pilar Hurtado quien hoy se encuentra detenida en Panamá. “**Desafortunadamente ella no colaboró con la justicia** luego de la visita que recibió de Álvaro Uribe y ella decidió cumplir con su sentencia sin señalar la verdad”.

<iframe id="audio_19683357" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19683357_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
