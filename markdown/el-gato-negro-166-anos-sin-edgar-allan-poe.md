Title: "El Gato Negro", 166 años sin Poe
Date: 2015-10-07 14:23
Category: Viaje Literario
Tags: 166 años sin Edgar Allan Poe, Edgar Allan Poe, El gato negro relato Poe
Slug: el-gato-negro-166-anos-sin-edgar-allan-poe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Poe-e1444245757403.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [<iframe src="http://www.ivoox.com/player_ek_8845568_2_1.html?data=mZ2hl5qafI6ZmKiakpiJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptGYycbYs4zixszf0YqWdoy5xczO1JClsMLijLXcx5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>]

###### [El gato negro (Fragmento)] 

###### [7 Oct 2015] 

Edgar Allan Poe, nació en Boston, el 19 de enero de 1809; fue un escritor, poeta, crítico y periodista romántico generalmente reconocido como uno de los maestros universales del relato corto, del cual fue uno de los primeros practicantes en su país. Renovador de la novela gótica, es recordado especialmente por sus cuentos de terror.

Considerado el inventor del relato detectivesco, contribuyó asimismo con varias obras al género emergente de la ciencia-ficción. Por otra parte, fue el primer escritor estadounidense de renombre que intentó hacer de la escritura su modus vivendi, lo que tuvo para él lamentables consecuencias.

Poe escribió cuentos de distintos géneros, poesía, crítica literaria y ensayo, éste sobre los temas más variados, además de una novela larga, algunos de sus relatos más populares son, El cuervo, La máscara de la muerte roja, El barril del amontillado, El doble asesinato de la calle morgue, El gato negro y El corazón delator

Falleció un 7 de octubre de 1849 en Baltimore; al cumplirse un año más de su fallecimiento, compartimos con ustedes un fragmento de su cuento "El gato negro", incluído en la publicación Narraciones extraordinarias.

[![el gato negro corto](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/el-gato-negro16-e1444245565259.jpg){.aligncenter .wp-image-15420 width="523" height="366"}](https://archivo.contagioradio.com/el-gato-negro-166-anos-sin-edgar-allan-poe/el-gato-negro16/)
