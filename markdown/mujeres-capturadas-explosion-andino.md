Title: Perfiles de las cuatro mujeres capturadas y sindicadas como responsables por explosión en el Andino
Date: 2017-06-25 22:00
Category: Judicial, Nacional
Tags: Atentados del Andino, Bogotá, congreso de los pueblos, Movimiento Revolución Popular
Slug: mujeres-capturadas-explosion-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/centro-comercial-andino-tres-personas-muertas-y-diez-heridos-deja-explosion-556459.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: el espectador] 

###### [25 Jun 217] 

Nueve personas fueron capturadas en la tarde de este 24 de Junio en Bogotá y Tolima y han sido señaladas por los medios de información como responsables de la explosión en el Andino, centro comercial de Bogotá. Dentro de ese grupo se encuentran 4 mujeres, reconocidas por trabajar a f**avor de los Derechos Humanos en varias organizaciones y por su trayectoria como estudiantes de la Universidad Nacional.**

La fiscalía habría acusado a las personas capturadas por los delitos de concierto para delinquir con fines terroristas, terrorismo, homicidio agravado en concurso homogeneo, tentativa de homicidio agravado, falsedad en documento público, fabricación tráfico y porte de armas y secuestro.

Sin embargo, aunque el grupo de personas se ha conocido como los capturados del Andino, ya está enfrentando la primera audiencia de legalización de captura aún se desconoce si la fiscalía cuneta con material probatorio que los vincule directamente con la explosión en el Centro Andino dejó un saldo de 3 personas muertas y varias heridas.

### **Los perfiles que se han divulgado en las redes sociales de las 4 mujeres capturadas:** 

**Natalia Trujillo Nova** es abogada egresada de la Universidad Nacional de Colombia y Especialista en Derecho Constitucional también de la Universidad Nacional de Colombia. Ha trabajado en Sisma Mujer, en Idipron y es profesional asociada de Defensa de Niñas y  Niños Internacional Sección Colombia como defensora de derechos de niñas, niños y adolescentes.

Como profesional su desempeño ha sido excelente por su profundo conocimiento del derecho nacional y de los derechos de las mujeres y los niños en particular.

**Lizeth Jhoanna Rodriguez Zárate**: Es abogada de la Universidad Nacional de Colombia. Desde que es estudiante ha trabajado como contratista de la misma universidad, en proyectos de investigación, en la Escuela de Justicia Comunitaria y en la Comisión Intereclesial de Justicia y Paz.

Siempre ha estado interesada en los debates políticos nacionales de actualidad y en la defensa de los derechos humanos.

**Lina Vanesa Jiménez:** Tiene 28 años, artista plástica de la UN. siempre se destacó por sus buenas notas y sus capacidades cognitivas. Es una mujer entregada a su familia y a su trabajo, ama a sus padres hermanos y sobrinos, con quiénes comparte muchos momentos. Junto con su hermana tienen una microempresa dedicada a la confección de ropa. Lina es apasionada por su trabajo, con un corazón noble y humilde.

**Alejandra Mendez Molano:** Abogada y defensora de Derechos Humanos, trabaja como defensora de las personas afectadas por la explosión del relleno sanitario Doña Juana en 1997.

### **Los hombres capturados** 

Los nombres de Boris Ernesto Rojas, Cesar Barrera, Andrés Bohorquez y Juan Camilo Pulido no son tan conocidos en organizaciones de Derechos Humanos pero si en la universidad donde se destacaron por sus labores académicas desde los primeros semestres y de ellos como abogado, Boris Rojas y el otro como sociólogo, Andres.

### **La captura podría ser declarada ilegal si no se legaliza a las 2 am**

En cuanto a la audiencia de legalización de captura que se está realizando debería prolongarse hasta el día de mañana, sin embargo la legalización tiene solamente 36 horas para ser legal, de lo contrario los capturados deben ser puestos en libertad o se estaría cometiendo una irregularidad por parte de los jueces, el plazo es hasta las 2 horas del próximo 26 de Junio.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
