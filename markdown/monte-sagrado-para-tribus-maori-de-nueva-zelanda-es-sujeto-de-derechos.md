Title: Monte sagrado para tribus Maori de Nueva Zelanda es sujeto de derechos
Date: 2017-12-22 16:34
Category: Ambiente, El mundo
Tags: maori, monte sagrado, Nueva Zelanda, protección de medio ambiente
Slug: monte-sagrado-para-tribus-maori-de-nueva-zelanda-es-sujeto-de-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/volcan-taranaki-nz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Astronoo] 

###### [22 Dic 2017] 

En otra de las decisiones ejemplares de Nueva Zelanda le otorgaron derechos legales iguales a los de las personas al monte Taranaki, **lugar sagrado para las tribus étnicas Maori** que viven en la costa occidental de la isla. El monte es el tercer lugar de la geografía de ese país que recibe personalidad legal y además el gobierno pedirá perdón a la comunidad Maori por las infracciones que cometió en el pasado contra el monte.

De acuerdo con el periódico The Guardian, ocho tribus Maori en conjunto con las autoridades el gobierno neozelandés, se encargarán de compartir la guardia y **protección de este lugar sagrado**. Este monte, representa para las comunidades ancestrales “un miembro de la familia” por lo que por años han insistido en que se reconozca la relación del monte con las personas.

El hecho de que se le atribuyan derechos legales implica que “si alguien le hace daño a la montaña, **legalmente le está haciendo daño a las tribus ancestrales**”. En los primeros meses de este año, el gobierno neozelandés, le había atribuido derechos legales también al rio Whanguanui en el marco de un acuerdo en pro de la conservación. (Le puede interesar: ["Sin implementación, ambiente va a seguir siendo víctima en el posconflicto"](https://archivo.contagioradio.com/sin-implementacion-ambiente-va-a-seguir-siendo-victima-en-el-posconflicto/))

Como parte del acuerdo para proteger el monte, el Gobierno de la isla se ha comprometido a **pedir disculpas** por cualquier infracción que se hubiese cometido en el pasado contra la tribu Maori que habita en cercanías del monte y su entorno natural. Esto ha sido bien recibido por las diferentes comunidades Maori del país, teniendo en cuenta que se ha puesto por encima la identidad y la asociación social de estos grupos.

El Monte Taranaki es un volcán que tiene **120,000 años** y está dormido “formado de manera más perfecta en el país”. Hizo erupción por última vez en 1775 y es considerado como el monte más frecuentado por los turistas que van a escalar. También, hace parte fundamental de la cosmogonía de la tribu Maori.

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
