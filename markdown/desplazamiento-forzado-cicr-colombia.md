Title: Desplazamiento forzado y aumento en la violencia: Las preocupaciones del CICR Colombia
Date: 2019-03-29 17:48
Category: DDHH, Nacional
Tags: Christoph Harnisch, CICR, paz
Slug: desplazamiento-forzado-cicr-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/ayuda_humanitaria_Catatumbo-Crédito-Margareth-Figueroa-CICR.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CICR] 

###### [29 Mar 2019] 

Esta semana el jefe de la delegación del Comité Internacional de la Cruz Roja (CICR), Christoph Harnisch presentó los retos que tendrá la entidad durante el presente año, así como sus preocupaciones sobre el balance de lo ocurrido en 2018; entre las cifras presentadas, Harnisch resaltó el incremento en el número de personas afectadas por la violencia, lo que significó para las comunidades un aumento en la inseguridad contra sus vidas.

Entre los principales problemas que se evidenciaron en 2018, el jefe de la delegación del CICR en Colombia resaltó el deterioro significativo en las condiciones de seguridad para las personas, el incremento significativo de los desplazamientos masivos, la ausencia de servicios del Estado en las regiones, la intensificación en el uso de artefactos explosivos para controlar zonas y sus efectos sobre las comunidades, y resaltó la persistencia de la desaparición forzada como arma para infundir miedo.

### **La inasistencia del Estado y la implementación de la paz** 

Harnisch recordó que en muchas zonas del país donde estaban las FARC se configuraron modelos en los que la guerrilla ejercía como Estado, y tras el proceso de paz hubo un vacío de poder que fue siendo tomado por diferentes estructuras criminales y no por la institucionalidad, lo que generó dinámicas que afectan las condiciones de seguridad para los habitantes en esos territorios.

Sin embargo, dicha situación no obedeció únicamente a fallas en la implementación de la paz, sino a un constante desatención del Estado respecto a comunidades que no cuentan con servicios básicos. En ese sentido, el Directivo recomendó a los ministerios y al Gobierno central destinar recursos en mayor inversión social y no para el presupuesto de defensa. (Le puede interesar: ["Campesinos de Córdoba resisten en el territorio pese a amenazas de grupos armados"](https://archivo.contagioradio.com/campesinos-cordoba-resisten-armados/))

### **Desplazamientos masivos y minas para controlar los territorios** 

Contraria a la creencia de que los desplazamientos masivos eran cuestión del pasado, el Jefe del CICR para Colombia afirmó que presenciaron un aumento del 90% en la cantidad de personas desplazadas entre 2017 y 2018, alcanzando 27.780 eventos de desplazamiento masivos (es decir, de 10 o más familias). Por otra parte, una de las situaciones destacadas por Harnisch fue el incremento de víctimas por minas.

[![Desaparecidos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-29-at-3.38.39-PM-800x800.jpeg){.alignnone .size-medium .wp-image-63839 width="800" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-29-at-3.38.39-PM.jpeg)

El Comité registró un aumento de 164 casos registrados entre 2017 y 2018 de personas afectadas por estos artefactos, mientras que en 2019 hasta la fecha se han registrado 76 incidentes. Esta situación es grave, porque como explicó el directivo, todos los grupos armados utilizan esté método para controlar los territorios, así como la movilidad de las comunidades por el mismo. (Le puede interesar: ["En Colombia hay cinco conflictos armados: CICR"](https://archivo.contagioradio.com/colombia-conflictos-armados-cicr/))

[![Víctimas minas en Colombia\_totales\_Square](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Víctimas-minas-en-Colombia_totales_Square-800x800.jpg){.alignnone .size-medium .wp-image-63850 width="800" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Víctimas-minas-en-Colombia_totales_Square.jpg)

### **Desaparición forzada, un fenómeno de ayer y de hoy** 

En un país como Colombia que podría tener entre 3 y 4 veces la cantidad de desaparecidos durante la dictadura en Argentina, Harnisch insistió en la necesidad de hacer un esfuerzo en la búsqueda de estas personas; al tiempo que expresó su preocupación ante la utilización de la desaparición forzada como elemento de control a las comunidades. Este flagelo, según cifras del organismo internacional, se produjo contra 1 persona cada 4 días.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
