Title: Fallo avala destrucción de biblioteca comunitaria en Bello, Antioquia
Date: 2015-09-16 15:55
Author: AdminContagio
Category: DDHH, Nacional, Resistencias
Tags: Antioquia, Bello, Biblioteca comunitaria Niquia, Desalojo biblioteca en Bello, Destrucción Biblioteca Niquia
Slug: fallo-avala-destruccion-de-biblioteca-comunitaria-en-bello-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Bello-21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Biblioteca Comunitaria Niquia ] 

###### [16 Sept 2015] 

[Pese a la movilización adelantada por jóvenes, niños y adultos en los últimos días para impedir el cierre de la Biblioteca Comunitaria Niquia, fue ratificado un fallo del Juzgado Segundo Civil Municipal de Bello en el que **se le ordena a Jesús María Arango hacer entrega del salón en el que funciona la biblioteca, así como cancelar \$1.000.000 a la parroquia por concepto del rubro gastado en el proceso judicial en curso desde 2005**.]

[Esta iniciativa de desalojo fue liderada por el Sacerdote Jorge Mario Acosta, **para la construcción de un supermercado,** pues según alega los predios pertenecen a la parroquia del municipio, desconociendo que 41 años atrás el párroco del momento los donó a Jesús Mario Arango Pero para el establecimiento del centro Cultural.]

[Según este fallo, entre la parroquia y Jesús Mario Arango se configuró un comodato precario, que daba vía libre a que la parroquia, como dueña de los predios, los reclamara cuando considerara pertinente. **Decisión que afecta a quienes asisten constantemente a los talleres de pintura, lectura, teatro y preuniversitarios que en la biblioteca se llevan a cabo**.  ]

[Vea también: [[En Bello pretenden destruir biblioteca para construir un supermercado](https://archivo.contagioradio.com/en-bello-pretenden-destruir-biblioteca-para-construir-un-supermercado/)]]
