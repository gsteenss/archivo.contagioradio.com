Title: Tras 5 años del Paro Agrario la situación del campo no ha mejorado
Date: 2018-09-18 19:49
Author: AdminContagio
Category: Movilización, Nacional
Tags: Alianza del Pacífico, Cesar Pachón, Dignidad Agropecuaria
Slug: hay-una-crisis-acumulada-en-varios-sectores-agricolas-cesar-pachon
Status: published

###### [Foto: Archivo Contagio Radio] 

###### [18 Sep 2018] 

Según el **Representante a la Cámara y líder del movimiento Dignidad Agropecuaria, César Pachón**, mientras el Gobierno insiste en políticas nocivas para el campo como los tratados de libre comercio con la Alianza del Pacifíco; los sectores de producción de lácteos, Arroz, Café y la Panela están en crisis, hecho que se suma a la dificultades que enfrenta la Reforma Rural Integral (RRI).

[Para el líder campesino, **el principal problema que afronta el agro**] **son las trabas que se están presentando para que los agricultores accedan a la [titulación] de sus predios**, porque hasta el momento "no nos permiten legalizar nuestras tierras, y dicen que todo lo que tenemos es baldío", hecho que puede generar un conflicto social. (Le puede interesar: ["Octubre, fecha crucial para la restitución de tierras"](https://archivo.contagioradio.com/octubre-limite-restitucion-de-tierras/))

Pachón aseguró que **el sector agrario recibe sólo el 0,8% del presupuesto nacional, mientras que aporta el 1,6% del PIB,** razón por la que pide más inversión en el sector. También reclamo soluciones efectivas a los ministros que componen las distintas carteras que tienen que ver con el Agro para afrontar la crisis que están padeciendo los sectores de producción de lácteos, el arroz, el café y la panela.

### **La Alianza del Pacífico podría quebrar al sector lechero** 

Según Pachón, la entrada en vigencia de un tratado con la Alianza del Pacífico que incluyera al sector láctico y cárnico podría afectar gravemente a los productores de leche en el país. El líder campesino recordó que aunque en Colombia se cubre la demanda de leche, el gremio nacional entraría a competir por los consumidores locales con Nueva Zelanda, que es uno de los países líderes en la producción láctica.

Sin embargo, la competencia sería desigual porque dadas las condiciones de tecnificación, apoyo estatal y cooperativismo que hay en el país nórdico, **el costo de producción por  cada litro de leche exportada sería de entre 300 y 350 pesos; mientras que el costo de la producción nacional se eleva a 750 pesos.** Dejando en una situación vulnerable a las más de 400 mil familias que viven del sector, y poniendo en juego la sostenibilidad económica del campo colombiano.

### ** "El plan Colombia Siembra esta afectando al sector arrozero"** 

A los problemas por los monopolios que hay por parte de las empresas que compran y procesan el arroz, se suma la denuncia de Pachón sobre el Plan Colombia Siembra, con el que el Ministerio de Agricultura buscó incrementar la producción de víveres en el país pero que terminó con una sobre oferta de alimentos. (Le puede interesar: ["Están agotadas las posibilidades con Min. Agricultura: Arroceros"](https://archivo.contagioradio.com/estan-agotadas-las-posibilidades-con-min-agricultura-arroceros/))

Con este plan, del que se vieron afectados cultivadores de platano, yuca y especialmente, arroz, se brindaron apoyos para incrementar el número de cultivos sembrados sin tener en cuenta que el país ya estaba cubriendo la demanda de dichos alimentos. Por lo tanto, **productores vieron que sus cosechas se perdían porque no había quien la comprara, y peor aún, dada la alta oferta de estos alimentos, bajaba el precio en el mercado.**

### **Caficultores están produciendo a pérdidas** 

Ante el aumento de la producción de diferentes países la carga de café a bajado su precio hasta un nivel que no se veía en años: 663 mil pesos por carga de 125 kilos. Aunque los **caficultores han advertido que por debajo de los 700 mil pesos** por carga se produce a pérdidas, el Gobierno hace caso omiso de las recomendaciones hechas por el sector para regular los precios.

El Representante a la Cámara señaló que valdría la pena que Colombia, como principal productor de Café hiciera acuerdos de cuotas con los demás países productores para estabilizar el precio del grano, y de esta forma, evitar la afectación por la caída de los precios que están sufriendo las **más de 500 mil familias que dependen del sector. **

### **Hay competencia desleal en la producción de Panela** 

Según cifras de la Federación Nacional de Productores de Panela, el producto ha vivido desde hace cerca de **11 meses**[ **una disminución del 40%** en su precio en promedio; adicionalmente, Pachón denunció que hay competencia desleal por parte de otros sectores de la producción nacional que ha generado la reducción de]los costos de producción y un aumento en la oferta del producto.

Según el líder de Dignidad Agropecuaria, la competencia desleal proviene de "sectores que trabajan la caña de azúcar para producir alcohol, que usan los excedentes de la caña para producir panela y azúcar a bajo costo", situación con la que se están viendo afectadas más de 350 mil familias que dependen de este sector.

### **"Serán las asambleas locales las que decidan si nos vamos a paro o no"** 

Pachón aseguró que para enfrentar la crisis del sector agrario realizarán un debate de control político próximamente, entre tanto, realizarán un informe en el que se retomen todas estas cifras, y se expongan las dificultades para acceder a las tierras por parte de los pequeños y medianos campesinos. Adicionalmente, anunció que **el 20 de septiembre se realizará un plantón frente al Ministerio de Comercio Exterior**, reclamando que no se firmen más tratados de libre comercio.

Por último, el líder campesino aseguró que la intención del movimiento agrario es velar por el respeto a los derechos de los agricultores; pero de las reuniones y asambleas locales saldrá la decisión de ir o no a paro. (Le puede interesar: ["El paro agrario también llega a Bogotá"](https://archivo.contagioradio.com/el-paro-agrario-tambien-llega-a-bogota/))

<iframe id="audio_28683671" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28683671_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
