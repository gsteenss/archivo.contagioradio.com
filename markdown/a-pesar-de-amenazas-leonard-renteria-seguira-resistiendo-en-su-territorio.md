Title: A pesar de amenazas, Leonard Rentería seguirá resistiendo en su territorio
Date: 2016-09-07 17:52
Category: DDHH, Entrevistas
Tags: buenaventura, Espacio Humanitario, Leonar Rentería, paz, Plebiscito
Slug: a-pesar-de-amenazas-leonard-renteria-seguira-resistiendo-en-su-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Leonard-Rentería.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El País 

###### [7 Sep 2016] 

A través de una **carta al Fiscal General, Néstor Humberto Martínez, y al Defensor del Pueblo, Carlos Negret,** diversas organizaciones y personas hacen parte de una iniciativa que solicita protección para Leonard Rentería, el joven de Buenaventura que con argumentos confrontó al senador Álvaro Uribe.

“**Los insultos, descalificaciones y amenazas** que ha recibido el joven líder de Buenaventura, Leonardo Rentería, por su intervención en un Foro que promovió la Campaña por el No, son un signo de intolerancia que debe ser investigado y sancionado previniendo así nuevos hechos de ese tipo y garantizando, sobre todo, la integridad del afectado y de su familia”, dice la carta con la que se solicita la protección del joven, que una vez salió de ese foro el pasado sábado empezó a recibir amenazas.

Los primeros mensajes los recibió por Facebook, y el lunes recibió una llamada amenazándolo de muerte. Tanto a él como sus compañeros los han tildado de castrochavistas y guerrilleros por promover el SI al plebiscito e iniciativas artísticas en torno a la paz desde sus comunidades en Buenaventura.

**“Ahora somos castrochavistas y guerrilleros,** lo que en un contexto como el nuestro nos pone prácticamente en una lápida teniendo en cuenta el control paramilitar que hay en nuestro territorio”, dice Leonard, quien a pesar de ese panorama, asegura que su fortaleza está en “La hermandad de los jóvenes que siempre me han dado un abrazo de solidaridad, reconciliación y perdón. Usamos el arte como una estrategia para blindarnos”.

El **Espacio Humanitario de Puente Nayero en Buenaventura** es otro de los elementos que lo han hecho resistir en medio de las amenazas que recibe desde el año 2008. “Allí están los hermanos que resisten, con quienes tenemos acciones colectivas. Se trata de un espacio de salvaguarda, donde la gente logró sacar a los grupos ilegales gracias a su valentía. **Allá me acoge la comunidad. Es un espacio de vida, libertad y protección”.**

Aún no hay respuestas por parte de la Fiscalía o la Defensoría del Pueblo que garanticen la seguridad de este líder social y su familia, sin embargo, sigue firme en su labor social, “Voy a seguir resistiendo en el territorio, vamos a seguir diciendo las cosas y vamos a seguir trabajando por Buenaventura” y concluye expresando que “**Debemos respetar siempre la diferencia.** La firma del tratado no es más que una puerta que nos va a abrir el camino para poder construir paz. Debemos pensarnos si las próximas generaciones merecen vivir en guerra o por el contrario merecen empezar a conocer un país en paz”.

###### <iframe id="audio_12826497" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12826497_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU)
