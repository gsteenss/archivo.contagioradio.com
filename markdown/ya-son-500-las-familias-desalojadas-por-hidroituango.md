Title: Ya son 500 las familias desalojadas por Hidroituango
Date: 2017-02-01 15:11
Category: DDHH, Nacional
Tags: EPM, Familias desplazadas por Hidroituango, Toledo Antioquia
Slug: ya-son-500-las-familias-desalojadas-por-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-01-at-1.56.15-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ríos Vivos] 

###### [1 Feb 2017 ] 

Familias barequeras del municipio de Toledo,  Antioquia, han sido desalojadas por orden del alcalde de ese municipio Johny Alberto Marín. Hasta la fecha se han registrado 500 familias desalojadas desde el inicio del proyecto Hidroituango y EPM proyecta que para finales de 2017 sean 2000 las familias. El movimiento Ríos Vivos denunció que la grave situación que se presenta desde 2011, **ha generado angustia en las familias y 3 habitantes de La Arenera se han quitado la vida.**

El desalojo que inició en horas de la mañana fue justificado por la inspección de policía, quien dijo al movimiento Ríos Vivos: “el río es de las Empresas Públicas de Medellín, **no respondemos por enceres ni la integridad de las familias”**, esto en respuesta a un recurso interpuesto por las comunidades ante la alcaldía.

### Prófugos en sus propios territorios 

Isabel Zuleta, una de las integrantes de Ríos Vivos y quien ha brindado acompañamiento a las familias desalojadas, manifestó que “hay familias que no tienen otro medio de subsistencia”, explicó que dicha prácticas es herencia de sus antepasados y que aunque han intentado realizar otras actividades económicas, **“eso es lo que sabe hacer, ahora tienen que vivir como prófugos en lo que eran sus territorios”**

Zuleta, aseguró que algunas de estas familias [ya habían sido desalojadas en marzo de 2015 y en mayo de 2011,](https://archivo.contagioradio.com/nuevamente-epm-y-esmad-realizan-desalojo-forzoso-por-hidroituango/) de la playa La Arenera y de los municipios de Briceño, San Andrés de Cuerquia y Toledo. Además, señaló que en dichas ocasiones, agentes de policía han despojado a las familias de sus herramientas de trabajo y aunque las familias denunciaron el caso ante la Fiscalía **“no han respondido, dicen que están en una bodega pero que la nueva alcaldía no sabe del tema y la gente no ha recibido sus implementos”.**

El movimiento Ríos Vivos, reveló que existen restricciones a la actividad del barequeo sólo cuando “hay obras en el terreno”, y en el momento Hidroituango no las está realizando, por otra parte también afirmaron que de acuerdo con el Código de Recursos Naturales, “las familias podrían asentarse a 30mts de playa del río, **esa área no puede ser propiedad de nadie, es propiedad del Estado, pero Hidroituango reclama esos terrenos.**

<iframe id="audio_16780616" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16780616_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
