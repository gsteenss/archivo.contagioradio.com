Title: Jesús María Valle: el Apóstol de los derechos humanos
Date: 2018-02-27 08:25
Category: DDHH, Sin Olvido
Tags: alvaro uribe velez, Antioquia, crímenes de estado, Don Berna, Jesús María Valle, Paramilitarismo, Pedro Juan Moreno
Slug: la-lucha-contra-la-impunidad-del-asesinato-de-jesus-maria-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jesus-maria-valle.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: .iejesusmariavallejaramillo.edu.co] 

###### [27 Feb 2018] 

“Si queremos transformar la sociedad colombiana en una sociedad de civilización donde se respete al hombre, nosotros tenemos que tener un código moral superior al código oscuro y ambiguo de la clase dirigente”, fueron algunas de las palabras que pronunció Jesús María Valle, abogado y defensor de derechos humanos durante las exequias de Ramón Emilio Arcila, abogado y líder social de la región y Saturnino López Zuluga, estudiante de ingeniería, ambos asesinados el 30 de diciembre de 1989. Nueve años después esa lucha ante esa clase dirigente que tanto denunciaba Valle, le cobró la vida un 27 de febrero de 1998 en la misma oficina donde redactaba cuanto documento se le ocurría para denunciar la violencia de la época.

Conocido como el Apostol de los derechos humanos, Jesús María Valle nació en el corregimiento de El Aro, en el municipio antioqueño de Ituango, el 28 de febrero de 1943. Fue abogado de la Universidad de Antioquia, dirigente estudiantil y respetado profesor de ética profesional, derecho procesal penal y oratoria forense. Comprometido con la defensa de los derechos humanos de su pueblo, hizo parte del Comité por la Defensa de los Derechos Humanos de Antioquia, y luego, ante **la ola de asesinatos de defensores como Pedro Luis Valencia, Leonardo Betancur, Héctor Abad, y los abogados Luis Fernando Vélez y Carlos Gónima,** debió asumir la presidencia de dicha organización en febrero de 1988.

Fue concejal y diputado, pero además fue el creador de iniciativas como la primera ‘Marcha por la defensa del Derecho a la Vida’ que se celebró en 1983 y el ‘Encuentro de profesionales de Antioquia: Hacia la paz por la justicia social’ en 1985. “Uno de los mejores y más valientes defensores de derechos humanos de la región”, decían los medios de comunicación cuando daban la noticia de su asesinato.

### **Sus denuncias: los vínculos entre los paramilitares, los militares y la gobernación** 

Vivió en carne propia la guerra. Su pueblo, El Aro, fue víctima, el 22 de octubre de 1997, de una vil masacre que según la Comisión Interamericana de DDHH, acabó con la vida de 15 personas de ese corregimiento, pero además se trató del episodio más humillante y terrorífico que han vivido sus habitantes, quienes tras 20 años de lo ocurrido no han encontrado ni verdad, ni justicia, ni reparación. Declaraciones del paramilitar Francisco Barreto, también asesinado, afirman que el gobernador de Antioquia, es decir **Álvaro Uribe Vélez, habría usado un helicóptero de la gobernación en medio de la masacre y la habría corroborado personalmente.**

Se habría tratado entonces de una masacre perpetrada por paramilitares con ayuda del Ejército, como lo ha señalado Salvatore Mancuso, exjefe paramilitar que ordenó la incursión y quien aseguró en 2008 que Pero Juan Moreno, secretario de la gobernación cuando Uribe era quien tenía la batuta del departamento, conoció de propia voz del máximo líder de las AUC, Carlos Castaño, que la masacre se perpetuaría.

Esa tal vez fue una de una de las principales luchas y denuncias de Jesús María Valle. Esa época fue de las más duras para la región antioqueña. **El abogado denunció la relación de los militares con los paramilitares para cometer esa masacre, así como la de La Granja** ocurrida el 11 de junio de 1996. También fue férreo crítico y denunciante de las CONVIVIR, diseñadas e implementadas por Moreno y Uribe como estrategia para acabar con la insurgencia, pero que, según investigaciones, luego mutaron a grupos paramilitares.

El 11 de junio de 1996 en una entrevista del periódico El Colombiano, Valle aseguró '**'Le pedí al gobernador y al comandante de la tercera Brigada que protegiera la población civil de mi pueblo, porque de septiembre a hoy han muerto más de 150 persona**s''. Pero la respuesta fue la estigmatización y los señalamientos por parte del entonces gobernador, considerándolo enemigo de las fuerzas armadas, y luego denunciado por injuria y calumnia.

“Uno pide protección, uno denuncia, va a las comisiones de seguridad y se encuentra que por el aporte que uno hace lo señalan posteriormente como auxiliar de las guerrillas y va ante el comandante de la brigada IV y no se asume un posición activa”, expresó en su momento el jurista. [(Le puede interesar: La otra verdad sobre la muerte de Perdro Juan Moreno)](https://archivo.contagioradio.com/la_otra_verdad_pedro_juan_moreno_uribe/)

De hecho, el general Carlos Alberto Ospina comandante de esa Brigada del Ejército, aseguró en diferentes medios de comunicación que las afirmaciones del abogado eran una mentira, “es absolutamente falso, los militares hacían presencia en la zona para enfrentar a los grupos, sean de izquierda o de derecha”, aseveraba. Hoy, Ospina es uno de militares a quien la Corte Suprema de Justicia compulsa copiar para investigarlo, con seis integrantes de la fuerza pública y junto al hoy senador Álvaro Uribe, ante los posibles vínculos de los militares, la policía y la gobernación con grupos paramilitares en Antioquia.

El rifirrafe con Uribe fue constante en medios de comunicación pero también personalmente. Al jurista no le temblaba la voz para decirle en la cara los vínculos de soldados del Ejército y del propio gobernador con las autodefensas, también aseguró que los militares y los paramilitares se movilizaban juntos y ante ello, **exigía repuestas a Uribe, al comandante del Ejército y al Ministerio de Defensa pero todo quedaba en el limbo.**

<iframe id="doc_30232" class="scribd_iframe_embed" title="Jesus Maria Valle (1)" src="https://www.scribd.com/embeds/372495561/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-fwxGIhW0GybUW90Qv0So&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6536231884057971"></iframe>

### **El asesinato** 

Como en varias entrevistas dijo su sobrino, Mauricio Herrera Valle, para la época**, si se hablaba de paramilitarismo “Se tenía una lápida pegada a la espalda”** y eso fue lo justamente apagó la vida del Apóstol de los derechos humanos. El 27 de febrero de 1998 dejó un vacío en el departamento de Antioquia, cuando dos hombres y una mujer, integrantes de la banda la Terraza ingresaron a su oficina, la 405 del edificio Colón en el centro de Medellín. Al llegar amordazaron a su hermana Nelly Valle y a otra persona que lo esperaba. Cuando el abogado llegó a su oficina y se encontró con los sicarios que lo estaban esperando, lo tiraron al suelo y le propinaron dos tiros con un arma 3.80 con silenciador.

Un día antes de su muerte,  se había presentado en la Fiscalía para rendir indagatoria por la denuncia por calumnia que le había interpuesto Uribe. Allí había manifestado que no había cometido ningún delito, reiteró sus denuncias sobre el actuar de las fuerzas militares con las autodefensas y **dijo que presentaría todas las pruebas que ya tenía para evidenciar el accionar permisivo tanto de la gobernación como del Ejército.**

“Usted para nosotros es muy importante, pero también es un problema”, fue lo que le dijeron los sicarios antes de asesinarlo, y una vez ya muerto, según cuenta la hermana de Valle, le dijeron que lo habían matado porque “estaba fregando mucho al Ejército”.

La familia del jurista denunció que cuando Valle fue asesinado tropas de la IV Brigada del Ejército realizaban operativos de control al porte ilegal de armas en la zona. “¿Quién mató a Jesús María? eso todo el mundo lo sabe, los paramilitares, con los militares, con el DAS, con el gobierno”, expresó ese trágico Octavio Valle Jaramillo, hermano del abogado.

### **La investigación del crimen contra Jesús María Valle** 

Quien asumió la investigación para ese entonces fue la Fiscalía de Medellín que vinculó al jefe paramilitar Carlos Castaño, pero también a los hermanos Francisco Antonio y Jaime Alberto Angulo Osorio, quienes habían participado en la planeación del asesinato. Sin embargo, en marzo de 2001 Castaño fue condenado pero los Angulo fueron absueltos.[(Le puede interesar: Compulsan copias contra Uribe por dos masacres)](https://archivo.contagioradio.com/alvaro_uribe_investigacion_masacres_antioquia_paramilitarismo/)

El caso fue llevado a instancias internacionales, y por una sentencia de la Corte Interamericana de Derechos Humanos del 2008 en la cual se solicitó una investigación imparcial y exhaustiva frente al asesinato y los responsables intelectuales del crimen, en el 2011 la Corte Suprema de Justicia invalidó las sentencias de absolución y ordenó reabrir la investigación en un juzgado penal de Medellín, teniendo en cuenta que según el alto tribunal **“la ejecución de Jesús María Valle permanece en la impunidad”.**

El **28 de septiembre la Fiscalía emitió la resolución No. 048, en la que se declara el asesinato del periodista Jaime Garzón Forero como un crimen de lesa humanidad**, en ese mismo documento, el organismo señalaba que “De lo precedente se infiere que el modus operandi común a los homicidios de defensores de derechos humanos consistió en que miembros de la fuerza pública, escudados en su lucha contrainsurgente, declararon objetivo legítimo a defensores (...), con ocasión de su labor o por tener una postura crítica frente a sectores oficiales, información transmitida al máximo jefe de las AUC, Carlos Castaño Gil, quien ordenó a la banda sicarial la ‘Terraza‘ ejecutar el homicidio”.

![PAG 73](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/PAG-73-523x600.png){.size-large .wp-image-51758 .aligncenter width="523" height="600"}

Para esa decisión se tuvo en cuenta  la declaración de Diego Fernando Murillo, alias ‘Don Berna’. Puntualmente, sobre el asesinato de Valle, este aseguró que fue el secretario de la gobernación de Antioquia, Pedro Juan Moreno, quien solicitó a Carlos Castaño que asesinaran al defensor de derechos humanos. “Se reunieron en la hacienda 53 del corregimiento EL Volador, no solo estas personas sino también varios militares y funcionarios de la Gobernación. Esa orden fue ejecutada por la banda de la TERRAZA, participaron tres personas, dos hombres y una mujer”, dice la sentencia por la cual el 6 de febrero de 2018, el Tribunal Superior de Medellín, condena a **los hacendados antioqueños Jaime Alberto y Francisco Antonio Angulo Osorio a 30 años de prisión y compulsa copias para investigar la participación de siete militares y policías, y también a Álvaro Uribe Vélez por las masacres de El Aro y La Granja.**

![sentencia 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/sentencia-1-600x531.png){.size-large .wp-image-51759 .aligncenter width="600" height="531"}

“Existen suficientes elementos de juicio conforme a lo expresado en esa decisión, que **probablemente comprometen la responsabilidad penal de varias personas como el gobernador de Antioquia de ese entonces Álvaro Uribe Vélez**”, dice la sentencia del Tribunal.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
