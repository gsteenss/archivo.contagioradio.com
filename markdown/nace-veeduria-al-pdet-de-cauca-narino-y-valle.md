Title: Nace veeduría al PDET de Cauca, Nariño y Valle
Date: 2020-02-28 17:19
Author: AdminContagio
Category: Entrevistas, Paz
Tags: acuerdo de paz, PDET
Slug: nace-veeduria-al-pdet-de-cauca-narino-y-valle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Veeduría-del-PDET-en-Cauca-y-Alto-Patía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Espacio Regional de Paz del Cauca {#foto-espacio-regional-de-paz-del-cauca .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->  
<iframe id="audio_48356296" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_48356296_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Este 27 y 28 de febrero se llevó a cabo en Popayán, Cauca, un encuentro de más de 100 líderes sociales de la región en la que evaluaron los retos a los que se enfrenta el Programa de Desarrollo con Enfoque Territorial (PDET) en el Alto Patía y Norte del Cauca. En el encuentro se consolidó la verificación que hacen las comunidades del Programa, así como los mecanismos para mejorar la integración organizacional para hacer este seguimiento.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **PDET, una forma participativa para atender las comunidades afectadas por el conflicto**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los Programas están consignados en el punto 1 del Acuerdo de Paz, sobre la Reforma Rural, como una forma de trabajar en las carencias institucionales en los municipios más afectados por el conflicto. Fue así como se escogieron 16 territorios donde se desarrollarían los PDET's, para impactar a 170 municipios de los 1.122 que tiene Colombia. (Le puede interesar:["Con políticas del uribismo seguiremos entregando los territorios a la ilegalidad"](https://archivo.contagioradio.com/con-politicas-del-uribismo-seguiremos-entregando-los-territorios-a-la-ilegalidad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estos municipios fueron elegidos de acuerdo con 4 criterios: El nivel de pobreza y desconocimiento de derechos, el nivel de escalamiento del conflicto armado, la presencia de cultivos de uso ilícito y la ausencia de institucionalidad estatal. En el caso del PDET del Alto Patía y Cauca, se incluyó a 17 municipios de Cauca, 5 de Nariño y 2 de Valle del Cauca.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Programa del Alto Patía, ¿y la participación comunitaria qué?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Víctor Collazos, coordinador del Espacio Regional de Paz del Cauca (ERPAZ), explicó que el Programa propone en un primer momento constituir pactos municipales y territoriales en 8 puntos que incluyen temas como salud, educación, agro y la transformación económica. Posteriormente, se hace una asamblea de delegados de los municipios incluidos en el PDET y se alcanza un Plan de Acción de Transformación Regional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Surtidos esos pasos, la Agencia de Renovación del Territorio, creada para ser parte de este proceso, elige en conjunto con las comunidades, elementos a priorizar y se implementan las propuestas de las comunidades. Collazos señaló que para el PDET del Alto Patía se hizo la asamblea en diciembre de 2018 con la participación de cerca de 1.200 delegados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De allí surgieron 4.354 iniciativas de las comunidades, pero aún está en proceso la implementación de dichas propuestas. Según relató, hasta el momento se priorizaron 52 iniciativas, pero la elección de las mismas se hizo desde Bogotá, con criterios técnicos y sin el acompañamiento de las comunidades. (Le puede interesar: ["La esperanza del Cauca está en sus jóvenes"](https://archivo.contagioradio.com/la-esperanza-del-cauca-esta-en-sus-jovenes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo a Collazos, avanzar en la implementación del Programa es relevante dada la conflictividad en un territorio como el Cauca en el que se presentan escalamientos largos y cortos del conflicto, así como se evidencia un aumento en las agresiones a líderes sociales. De igual forma recordó la importancia de la participación, señalando que la gente en el territorio no siente que se esté cumpliendo el Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La veeduría al PDET, una forma de avanzar hacia la participación**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Con el fín de fortalecer la incidencia social y comunitaria en el PDET, se dieron cita este jueves y viernes más de 100 líderes y lideresas de Cauca, Nariño y Valle del Cauca en Popayán para crear la veeduría regional del programa. Para Collazos, de esta forma esperan mejorar la capacidad de las comunidades para exigir el cumplimiento de este punto del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, el líder afirmó que esperan mejorar la interrelación de las comunidades para que logren unirse en torno al cumplimiento y la participación en los programas, y por último, lanzar la plataforma web [Te Da Paz](https://www.tedapaz.co/), una herramienta de gestión de la información sobre temas de interés relevante para las comunidades que es de fácil acceso y comprensión.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
