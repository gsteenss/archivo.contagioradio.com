Title: Se cumplen dos meses de la desaparición de Santiago Maldonado en Argentina
Date: 2017-10-02 13:26
Category: DDHH, El mundo
Tags: Argentina, desaparición de Santiago Maldonado, Gendarmería argentina, santiago maldonado
Slug: dos-meses-desaparicion-santiago-maldonado-en-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/DLIov2SU8AE2rAH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Latinoamerica Exuberante] 

###### [2 Oct 2017] 

A dos meses de la desaparición del joven Santiago Maldonado en Argentina, **se realizaron multitudinarias movilizaciones **exigiendo justicia y celeridad en las investigaciones.  Su desaparición,  ha causado gran indignación pues recuerda la época de la dictadura de los 70 cuando las fuerzas militares desaparecían personas indiscriminadamente.

El 1 de octubre se cumplieron dos meses desde que **3 testigos de la comunidad Mapuche** aseguraron haber visto a Maldonado por última vez en la Patagonia. Según ellos, tras una represión de la Gerndarmería, el joven habría sido detenido y movilizado en uno de los vehículos de la guardia policial. (Le puede interesar: ["Desaparición de Santiago Maldonado estaría relacionada con su apoyo al pueblo Mapuche"](https://archivo.contagioradio.com/santiago-maldonado/))

### **Las hipótesis sobre la desaparición de Santiago Maldonado**

Desde ese día, diferentes versiones han aparecido **sin que el gobierno de Mauricio Macri** asuma alguna responsabilidad y sin que aún se tengan pistas del paradero de Maldonado. Por ejemplo, de acuerdo con medios de comunicación argentinos, los investigadores han podido probar que el celular del joven se activó por 22 segundos el 2 de agosto en Chile luego de que un conocido intentara comunicarse con él.

De igual manera, el juez que llevaba el caso y algunos representantes del estado, manifestaron primero que el joven había huido solo a Chile. Luego **dijeron que había muerto ahogado** en el Río Chubut cuando intentó escapar de los Gendarmes ese 1 de agosto. (Le puede interesar: ["Argentinos reclaman verdad y justicia en caso de Santiago Maldonado"](https://archivo.contagioradio.com/argentinos-reclaman-verdad-y-justicia-en-caso-de-santiago-maldonado/))

Además, la familia de Maldonado denunció que el fiscal que llevaba el caso, Guido Otranto, ha desviado las investigaciones. Otranto fue retirado y reemplazado por un nuevo fiscal en virtud de que, como argumentan algunos medios de información, ha borrado y omitido algunas pruebas como el rastreo al celular de Maldonado.

### **La carta de Sergio Maldonado a su hermano** 

El 1 de octubre tuvo lugar una multitudinaria movilización que reunió a la sociedad argentina, **las organizaciones sociales nacionales e internacionales** y activistas políticos quienes le exigieron al gobierno argentino justicia y claridad en el caso.

Sergio Maldonado, en medio de la conmemoración, **leyó una carta en donde le dice a su hermano que lo están esperando**. Le hace saber que “las personas como vos nos enseñan, nos abren los ojos, nos muestran el camino; pero también dejan en evidencia las miserias humanas. Ojalá puedas escucharme y entender la demora en encontrarte.”

Igualmente, menciona que en su desaparición **hay intereses de por medio** y que le gustaría preguntarle a Mauricio Macri y a todos sus ministros “dónde está Santiago Maldonado”. Dice “¿Tan difícil es pedir que te devuelvan? Quiero que los viejos, la abuela y toda la familia dejen de sufrir y que esta pesadilla termine”.

Maldonado indicó en la carta que “Hay muchos intereses en el medio y lamentablemente, para algunos estos intereses son más importantes que la desaparición de una persona en manos de una fuerza del Estado.* ***Para ellos solo somos un número**, una estadística, un casillero. Pero somos hijos, hermanos, amigos y personas con derechos que exigimos justicia”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
