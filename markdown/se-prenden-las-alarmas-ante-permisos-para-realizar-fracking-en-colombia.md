Title: Se prenden las alarmas ante permisos para realizar fracking en Colombia
Date: 2018-02-08 16:46
Category: Ambiente, Nacional
Tags: ANLA, cesar, ConocoPhillips, fracking, san martin
Slug: se-prenden-las-alarmas-ante-permisos-para-realizar-fracking-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/San-Martin.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CORDATEC] 

###### [08 Feb 2018] 

La Alianza Colombia Libre de Fracking denunció el inicio del proceso administrativo que otorga la licencia ambiental para que en el país se empiece extraer petróleo mediante la técnica del fracking.  Como lo venían denunciado los ambientalistas y la comunidad, el pozo PicoPlata 1, en el municipio de San Martín, Cesar, **sería el primer lugar donde se empiece a desarrollar dicha técnica que en varios lugares del mundo ha sido prohibida** debido a los graves impactos ambientales que puede generar.

Tanto organizaciones en defensa de los derechos ambientales como la comunidad, habían señalado que en ese lugar ya se había adelantado la primera fase de exploración de manera ilegal, sin la licencia ambiental correspondiente.

La autorización la dio la Autoridad Nacional de Licencias Ambientales (ANLA). Se trata de una decisión que para Carlos Andrés Santiago integrante de CORDATEC y de la Alianza Colombia Libre de Fracking, tomó por sorpresa a los ambientalistas y a los habitantes de San Martín, debido a que 6 meses antes el Ministro de Ambiente había afirmado que el país no estaba preparado y que no había los suficientes estudios para poder explotar petróleo con base en el fracking.

“No solo es este proyecto, que es el BMN3, sino otro que tiene ConocoPhillips, que también sabemos que van a solicitar licenciamiento que no solo afecta a San Martín y Aguachica, sino también Rió de Oro y Gamarra”, afirmó Carlos Andrés y agregó que **Ecopetrol, igualmente, iniciaría estudios para intervenir la zona del Magdalena Medio con la misma técnica de extracción**.

### **Proyecto BMN3 inició sin autorización** 

La comunidad de San Martín, desde hace dos años había comenzado una movilización y la activación de diferentes mecanismos jurídicos para impedir que el fracking fuera una realidad en su territorio. Sin embargo, El vocero de CORDATEC, señaló que la **primera fase de exploración inició hace un año, con una licencia de yacimientos convencionales**, a la que incluso la Contraloría hizo un llamado de atención debido a la falta de la licencia indicada para proceder.

El siguiente paso, de acuerdo  con Santiago, es que sobre el pozo Pico Plata I, que ya está perforado de forma vertical, se realicen 6 pozos horizontales de 2 km. Situación que aumenta la preocupación de la comunidad debido a que la ubicación de los pozos tocaría la zona del complejo de ciénagas y humedales del río Lebrija que alimenta al río Magdalena y que actualmente tiene animales que se encuentran vía de extinción como el manatí.

“Lo que hemos identificado es que hay un grandísimo riesgo que no se ha medido ni cuantificado y que, pese a eso, el gobierno y las multinacionales siguen con la intención de experimentar a costa de la vida y la salud de las personas del municipio de San Martín". (Le puede interesar: ["En medio de gases y lágrimas ConocoPhillips entra maquinaría a San Martín"](https://archivo.contagioradio.com/en-medio-de-gases-y-lagrimas-conocophillips-logra-entrar-maquinaria-a-san-martin/))

### **Los bloques petroleros en Colombia** 

De acuerdo con la información recopilada por CORDATEC, en Colombia existen 45 bloques petroleros que estarían destinados para la explotación a través de fracking. En 5 de ellos ya hay contratos vigentes, mientras que **43 están ubicados alrededor del páramo de Chingaza, que surte el 80% del agua de Bogotá y del páramo de Sumapaz, el más grande del mundo**.

En ese sentido, Carlos Santiago afirmó que hay más de 80 organizaciones aglutinadas en la Alianza Colombia Libre de Fracking, en donde le piden al gobierno que declare la moratoria de esa técnica de extracción de crudo. Desde allí se están coordinando movilizaciones en diferentes territorios, procesos jurídicos y un proyecto de Ley al Congreso de la República que frenen esta acción.

Asimismo, a finales de febrero, se lanzará la campaña **'100 más dicen Colombia libre de Francking**' y un documental que espera llegar a otro tipo de escenarios de la sociedad en el país, para ponerle freno a la extracción de petróleo. (Le puede interesar: ["ConocoPhillips no cumple requisitos de licencia ambiental para fracking en San Martín"](https://archivo.contagioradio.com/conoco-phillips-no-cumple-requisitos-de-licencia-ambiental-para-fracking-en-san-martin/))

### **El Fracking en las elecciones del 2018** 

En el panorama electoral, para Carlos Santiago, hay dos tendencias: los que están a favor del fracking, en donde se encuentran Iván Duque, por el Centro Democrático, María Lucía Ramírez, del partido Conservador, Alejandro Ordoñez, Germán Vargas Lleras y Juan Carlos Pinzón, que han afirmado en diferentes debates y medios de información estar a favor de esta técnica.

La otra tendencia está conformada por quienes no apoyan el fracking, entre ellos están Humberto de la Calle, Clara López, Sergio Fajardo, Jorge Robledo y Claudia López que mantienen una postura de moratoria al fracking. Por su parte, Gustavo Petro y Piedad Córdoba que han planteado la prohibición de esta técnica de manera tajante.

Para el ambientalista estas elecciones serán cruciales. "Con el voto de las y los colombianos se estará no solo escogiendo a un presidente, **sino el futuro de las generaciones venideras y el derecho colectivo al ambiente"**, concluye.

[auto\_0099\_04012018](https://www.scribd.com/document/371089623/auto-0099-04012018#from_embed "View auto_0099_04012018 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_40617" class="scribd_iframe_embed" title="auto_0099_04012018" src="https://www.scribd.com/embeds/371089623/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-CVTn42HHo4Hy1pKdrxdu&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6540971718636693"></iframe><iframe id="audio_23644091" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23644091_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
