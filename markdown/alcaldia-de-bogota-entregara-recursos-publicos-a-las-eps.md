Title: Alcaldía de Bogotá entregará recursos públicos a las EPS
Date: 2016-02-05 16:27
Category: Movilización
Tags: crisis de la salud, Enrique Peñalosa, Territorios saludables
Slug: alcaldia-de-bogota-entregara-recursos-publicos-a-las-eps
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/IMG-20160205-WA0012.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Carmela María] 

<iframe src="http://www.ivoox.com/player_ek_10332665_2_1.html?data=kpWglZeaepahhpywj5WbaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncaLgxMbZxoqnd4a1pcaYxsqPhtDb0NmSpZiJhZKfxtPh1MrLpdOZpJiSo5aPtsbX1tfg0diPtIa3lIqvo8fQrcTj1JDOjdHFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carolina Corcho] 

<iframe src="http://www.ivoox.com/player_ek_10332686_2_1.html?data=kpWglZeafJehhpywj5WcaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncaLgxMbZxoqnd4a1pcaYxsqPhtDb0NmSpZiJhZKfxtPh1MrLpdOZpJiSo5aPtsbX1tfg0diPtIa3lIqvo8fQrcTj1JDOjdHFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Viviana Lópéz] 

###### [5 Feb 2016 ] 

Este viernes se movilizan en el centro de la capital colombiana cerca de 7 mil trabajadores y trabajadoras del ‘Programa Territorios Saludables’ promovido por el Gobierno de la Bogotá Humana y que la **actual administración de Enrique Peñalosa pretende desmontar**, asignando los \$280 mil millones que fueron aprobados para su ejecución durante este año, a la construcción de centros de atención prioritaria de urgencias que serán administrados por las EPS, quienes le **adeudan más de \$850 mil millones a los 22 hospitales distritales** .

De acuerdo con Carolina Corcho, vocera de la Mesa Nacional por el Derecho a la Salud este programa ha sido reconocido por la OMS y la OPS como uno de los **programas de atención primaria en salud mejor estructurado e implementado en América Latina**, pues ha logrado llevar a cero la tasa de desnutrición y mortalidad materna e infantil, al contar con **cerca de 10 mil especialistas desplazándose por todos los colegios, jardines y hogares ubicados en las 20 localidades**, para desarrollar acciones de prevención y atención de enfermedades.

Con la ejecución de este programa se han beneficiado 390 mil niños y niñas de la primera infancia, 768 mil en edad escolar, 1 millón 42 mil jóvenes, 1 millón 364 mil adultos y 324 mil 955 personas mayores quienes con la decisión del actual alcalde se verían afectados, teniendo en cuenta que **los recursos de la Secretaria de Salud, que debían ser utilizados en la implementación de programas de prevención, se asignarán para financiar la atención en urgencias de las EPS**, lo que según la vocera “genera un detrimento patrimonial para los bogotanos” que favorece la “institucionalización de los paseos de la muerte”.

Por su parte, Viviana López, profesional de Territorios Saludables, denuncia que **en varios hospitales distritales se está obligando a los trabajadores a firmar contratos por 7 o 15 días** en los que deben cumplir con metas muy altas, así mismo algunos coordinadores fueron amenazados con la no firma del contrato sí participaban de la movilización que se viene adelantando.
