Title: La Gobernación de Antioquia acepta que tragedia de Hidroituango fue un error de construcción
Date: 2018-09-06 17:15
Author: AdminContagio
Category: Ambiente, Movilización
Tags: Hidroituango, Movilización, Rios Vivos
Slug: tragedia-hidroituango-fue-error-de-construccion
Status: published

###### [Foto: Gobernación de Antioquia] 

###### [05 Sept 2018]

Esta semana se desarrolló una reunión en la que estuvieron presentes los miembros de la junta directiva de Hidroituango y el gobernador de Antioquia, Luis Perez; allí se discutieron las causas del desastre ocasionado por la represa y se entregó un estudio que afirma que **la tragedia de pasado abril de este año es resultado de un error de construcción, y no está relacionado con causas naturales** como suponían los organismos estatales.

Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, manifestó su sorpresa por la reciente declaración de la Gobernación, afirmando que ya se tenía conocimiento sobre el motivo de los derrumbes producto de las fallas en la construcción de la megaobra. (Le puede interesar: [EPM "perdió el control sobre Hidroituango: Contraloría](https://archivo.contagioradio.com/epm-perdio-el-control-sobre-hidroituango-contraloria/))

Sin embargo, señaló que tanto el **Gobernador de Antioquia, como el Presidente de Empresas Públicas de Medellín, tienen responsabilidad en los acontecimientos**, pero están buscando deshacerse de a misma y descargarla en los empleados encargados de Hidroituango. Muestra de ello serían varios despidos de funcionarios de nivel más bajo.

**Los daños incalculables de Hidroituango**

Aún no se sabe con certeza cuántos son los perjudicados por la tragedia de Hidroituango, según Zuleta existen cientos de personas que no saben si se encuentran en el registro oficial de damnificados, y hay miles de casos que no son reconocidos por el Estado. Sin embargo, desde Ríos Vivos se ha manifestado que podrían ser más de 300 mil personas damnificadas.

Por ahora, Ituango se encuentra aislado, en permanente alerta de derrumbe y con su economía colapsada, hechos que han provocado que muchos de los habitantes se desplazaran hacia Medellín y a numerosos municipios de la Cuenca Baja.** **

[**Por la defensa de la vida hay movilización en Ituango**]

El 8 y 9 de Septiembre se va a desarrollar una movilización en Ituango, con la presencia y colaboración de habitantes de 10 municipios; allí se demandará el **desmantelamiento controlado de la represa** y la atención a la situación de toda la población, exigiendo una fecha límite para la finalización de la emergencia.

De igual forma el 9 de septiembre se realizará el evento **"únete por el clima"** en la que participarán 320 grupos ecologistas de más de 70 países. Este evento se llevará a cabo en Bogotá, Ituango en Antioquia, Pasto e Ipiales en Nariño, Barranquilla en el Atlantico, Cali en el Valle del Cauca, Acacias en Meta y Cauca en Popayán. Este movimiento tendrá la finalidad de levantar una voz de protesta en contra de la producción de gases metanos por medio de los combustibles fósiles. El evento es coordinado por la Organización 350° con la consigna **"Por un mundo sin combustibles fósiles. Por un mundo sin represas"**

<iframe id="audio_28397121" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28397121_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)] 
