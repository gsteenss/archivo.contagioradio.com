Title: Felipe González: ¿un notable legítimo?
Date: 2017-05-05 16:33
Category: Opinion, Vicente Vallies
Tags: acuerdos de paz, Felipe González, JEP
Slug: felipe-gonzalez-un-notable-legitimo-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/FELIPE-GONZALES.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Paulo Whitaker 

#### **Por: [Vicente Vallies]** 

#### **Parte 2 de 3 – Sus amistades** 

###### 5 May 2017 

[En mi][[última columna]](https://archivo.contagioradio.com/felipe-gonzalez-un-notable-legitimo/)[ ponía en duda la idoneidad de Felipe González para asumir el seguimiento de los acuerdos de Paz en Colombia debido a la historia del grupo paramilitar GAL en España. Otros elementos confirman mi opinión; el segundo reto en Colombia está relacionado con]**el tema del territorio**[, y en ese tema las amistades de Felipe González no son recomendables. Cabe recordar que la inequitativa repartición del territorio en Colombia se encuentra en la base del conflicto armado, según Oxfam, Colombia es el país más desigual en el reparto de la tierra de América Latina] [y entre 6 y 8 millones de hectáreas han sido despojadas.]

[¿Qué legitimidad tiene Felipe González cuando sus relaciones en Colombia están vinculadas al despojo de tierras? Su gran amigo, que le introdujo al mundo colombiano, Enrique Sarasola Lerchundi se fue a hacer negocios a Colombia y se casó con Cecilia Marulanda hija de Alberto Marulanda Grillo. Alberto Marulanda Grillo fue uno de los principales terratenientes de Colombia y sus hijos Carlos y Francisco – hermanos de Cecilia – se vieron involucrados en el escándalo de la Hacienda Bellacruz donde paramilitarismo y desplazamiento forzado fueron las herramientas de control territorial.]

[¿Qué pasó en BellaCruz? En 1986 familias campesinas averiguaron la existencia de baldíos de la nación dentro de la hacienda Bellacruz de la familia Marulanda y tomaron la decisión de instalarse en ellos, cultivar la tierra e iniciar una lucha administrativa y judicial para lograr la titulación de estos terrenos. Después de años de desilusiones, en 1994 pensaron haber llegado al final de esta aventura. Efectivamente en abril de este año el Incora emitió una resolución que les daba la razón, decisión ratificada en abril 1996: los terrenos ocupados no eran propiedades de la familia Marulanda. Desafortunadamente unos meses antes de que se hiciera efectiva la decisión del Incora, en febrero 1996 un grupo paramilitar amenazó a las familias campesinas, quemaron sus ranchos y gritaron - como lo relata el periódico Semana - “]*[Hijueputas, salgan de aquí, ladrones, estas tierras son de Carlos Arturo Marulanda]*[”. Las familias campesinas se vieron obligadas a desplazarse. Durante el proceso de la ley de Justicia y Paz, varios desmovilizados le confesaron a la Fiscalía que fueron contratados por los Marulanda para evitar que los campesinos se instalaran en las tierras. Un testigo contó a VerdadAbierta que “]*[los comandantes eran prácticamente Francisco Alberto Marulanda (hermano (de) Carlos Arturo) y Édgar Rodríguez alias ‘Caballito’, un hijo de crianza de los Marulanda]*[”. Frente a esta situación el propio Parlamento Europeo debatió la necesidad de una resolución acusando a Carlos Arturo Marulanda, pero como lo relata el periódico ABC "]*[Enrique Sarasola, el amigo de Felipe González (…) ha llegado a presionar a eurodiputados socialista para evitar que el Parlamento Europeo se pronuncie contra Carlos Arturo Marulanda, embajador de Colombia en Bruselas y cuñado suyo]*[".]

[Carlos Arturo Marulanda fue arrestado en Madrid en el 2001 acusado de formación de grupos armados, terrorismo y malversación de fondos públicos. Fue liberado en 2002. Su hermano, Francisco Marulanda fue condenado en 2003 en primera instancia a 18 años de prisión por la conformación de grupos paramilitares. Fue absuelto por el Tribunal Superior de Magdalena en segunda instancia.]

[En un momento en el cual organizaciones sociales están levantando la voz frente a un proyecto de decreto del gobierno colombiano, que entre otras cosas modifica la finalidad de las tierras baldías y se posiciona así en contra de lo acordado en La Habana ¿Qué va a decir Felipe González? ¿Será imparcial o se alineará con lo que piensan sus amigos en cuanto a la tenencia de tierra en Colombia?]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.] 
