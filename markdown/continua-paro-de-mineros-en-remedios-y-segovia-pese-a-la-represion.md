Title: Continúa paro de mineros en Remedios y Segovia pese a la represión
Date: 2017-08-03 15:47
Category: DDHH, Movilización
Tags: Minería artesanal, mineros, Remedios, Segovia
Slug: continua-paro-de-mineros-en-remedios-y-segovia-pese-a-la-represion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Mineros-de-segovia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Documental Amarillo] 

###### [03 Ago. 2017] 

Luego de la fuerte represión de este lunes por parte del Escuadrón Móvil Antidisturbios (ESMAD), en Remedios hay **un saldo de 17 heridos, 9 enviados a Medellín por la gravedad de las heridas**, mientras que en Segovia 7 personas fueron heridas y 4 tuvieron que ser evacuadas de la zona en un helicóptero.

Catorce días completan los mineros de Segovia y Remedios en paro pacífico, quienes han manifestado que **no levantarán la movilización hasta tanto el Gobierno nacional en cabeza del viceministro de Minas, Carlos Andrés Cante, decidan sentarse a negociar** y determinen declarar la minería ancestral como legal. Así mismo, esperan concertar una reunión con la multinacional Gran Colombian Gold para acordar mejoras en materia laboral.

**Mineros seguirán buscando ser reconocidos como legales**

Dice **Ricaurte García, integrante de la Corporación Acción Humanitaria por la Convivencia y la Paz del Nordeste Antioqueño (Cahucopana)**, que no van a aceptar que el gobierno siga tratándolos como ilegales y que por eso seguirán trabajando para ser escuchados y aprobadas sus propuestas.

“La respuesta que han dado los mineros es clara y es que nos queremos legalizar, porque **el problema más grande es que hemos sido estigmatizados** por el gobierno con las normativas que expide. Queremos la formalización porque no somos ilegales solo informales”. Le puede interesar: "Toque de queda y militarización no detendrán paro" mineros de Segovia y Remedios

### **“Nuestros campesinos han vivido de la minería”** 

Una fuerte crítica que han hecho desde el sector minero de Remedios y Segovia en Antioquia, es que **a las multinacionales que no son de Colombia les permiten trabajar por medio de las licencias** que se les otorgan y sin muchas trabas se convierten en legales, mientras que a los pequeños mineros el Estado los tiene abandonados y los criminaliza.

“Nuestros campesinos han vivido de la minería y a estas multinacionales si les dan los títulos y la formalizaciones, **en cambio a los pequeños mineros nos ponen trancas para formalizarnos**, esa ha sido la lucha que hemos venido dando, nos queremos formalizar, queremos trabajar legalmente, pero el gobierno no ha buscado la solución, solo interviene las minas y no busca la solución para que los mineros se formalicen”.

### **¿Qué ventaja trae la formalización para los mineros?** 

En la actualidad **los mineros se ven enfrentados a enormes pérdidas por ser considerados como “ilegales”**, por ejemplo, tienen que vender el producto a precios mucho más bajos que los que da el mercado y deben trabajar más para poder conseguir más oro y lograr ganar más dinero.

En ese caso, al ser formalizado el ejercicio de la minería artesanal ellos entrarían a jugar un papel legal **“podríamos comercializar nuestro producto legalmente, que es una preocupación**, para vender un gramo de oro hay problema, en algunas partes está a buen precio y nos lo compran barato porque somos informales”. Le puede interesar: [Corte Constitucional pone freno a minería en Resguardo Cañamomo y Lomaprieta](https://archivo.contagioradio.com/corte-constitucional-pone-freno-mineria-resguardo-canamomo-lomaprieta/)

Asegura García que por esa razón muchos mineros de la zona están aguantando hambre, porque pese a que cuentan con el material, no es tan fácil encontrar quién compre por el tema de la ilegalidad “si nos formalizan nos garantizan una estabilidad en la región, porque Remedios y Segovia son municipios ricos en minería”.

### **En materia económica ¿qué significa el paro?** 

García cuenta que **en la actualidad hay escasez de alimento puesto que la gente “vive al día,** comprando el plátano, la yuca, el arroz. Sin embargo, los campesinos son muy solidarios y quienes están en el paro han recibido alimentos para seguir en la movilización”. Le puede interesar: [Mineros antioqueños suspenden paro tras acuerdos con el gobierno](https://archivo.contagioradio.com/mineros-antioquenos-suspenden-paro-tras-acuerdos-con-el-gobierno/)

### **Gobierno no ha instalado mesa de negociaciones** 

La Mesa Minera de Remedios y Segovia manifestó que **ante la negativa del Gobierno de instalar una mesa de diálogo seguirán en la manifestación** hasta que sean escuchados porque nuestra “lucha es justa”. Le puede interesar: [Mineros Artesanales de Marmato le ganan el pulso a la Gran Colombia Gold](https://archivo.contagioradio.com/mineros-artesanales-de-marmato-le-ganan-el-pulso-a-la-gran-colombia-gold/)

Además de la mesa minera, recientemente se unió el gremio campesino y los indígenas de la región por lo que el líder de CAHUCOPANA asevera que “la solución está en las manos del Gobierno, el tiempo que demore la manifestación está en las manos del Gobierno”.** **

<iframe id="audio_20156309" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20156309_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
