Title: Viaje Literario
Date: 2014-11-25 15:51
Author: AdminContagio
Slug: viaje-literario-2
Status: published

### VIAJE LITERARIO

[![10 poemas de Martí convertidos en canción](https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400.jpg "10 poemas de Martí convertidos en canción"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400.jpg 770w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400-300x156.jpg 300w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400-768x399.jpg 768w, https://archivo.contagioradio.com/wp-content/uploads/2019/01/jose_marti_by_gerardogomez-d37x08n-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/10-poemas-de-marti-convertidos-en-cancion/)  

###### [10 poemas de Martí convertidos en canción](https://archivo.contagioradio.com/10-poemas-de-marti-convertidos-en-cancion/)

[<time datetime="2019-01-28T10:20:53+00:00" title="2019-01-28T10:20:53+00:00">enero 28, 2019</time>](https://archivo.contagioradio.com/2019/01/28/)Homenaje a su obra con una selección de 10 temas musicales inspirados en 1o escritos de José Martí[LEER MÁS](https://archivo.contagioradio.com/10-poemas-de-marti-convertidos-en-cancion/)  
[](https://archivo.contagioradio.com/el-otro-yo-de-mario-benedetti/)  

###### [“El otro yo” de Mario Benedetti](https://archivo.contagioradio.com/el-otro-yo-de-mario-benedetti/)

[<time datetime="2018-09-14T09:15:08+00:00" title="2018-09-14T09:15:08+00:00">septiembre 14, 2018</time>](https://archivo.contagioradio.com/2018/09/14/)Mario Benedetti, "El otro yo" 14 Sep 2018 Mario Benedetti nació en paso de los toros, Uruguay el 14 de septiembre de 1920 , fue un escritor, poeta y dramaturgo integrante de la generación del[LEER MÁS](https://archivo.contagioradio.com/el-otro-yo-de-mario-benedetti/)  
[](https://archivo.contagioradio.com/que-dicen-los-escritores-de-habla-hispana-sobre-julio-cortazar/)  

###### [¿Qué dicen los escritores de habla hispana sobre Julio Cortázar?](https://archivo.contagioradio.com/que-dicen-los-escritores-de-habla-hispana-sobre-julio-cortazar/)

[<time datetime="2015-02-12T11:00:27+00:00" title="2015-02-12T11:00:27+00:00">febrero 12, 2015</time>](https://archivo.contagioradio.com/2015/02/12/)12 Feb 2016. Al conmemorarse 32 años de la muerte de Julio Cortázar,  uno de los escritores más destacados de la literatura latinoamericana y universal, recordamos las palabras de grandes colegas y amigos que han opinado[LEER MÁS](https://archivo.contagioradio.com/que-dicen-los-escritores-de-habla-hispana-sobre-julio-cortazar/)  
[](https://archivo.contagioradio.com/viaje-literario-faraon-angola/)  

###### [Viaje Literario – Faraón Angola](https://archivo.contagioradio.com/viaje-literario-faraon-angola/)

[<time datetime="2015-01-24T18:04:10+00:00" title="2015-01-24T18:04:10+00:00">enero 24, 2015</time>](https://archivo.contagioradio.com/2015/01/24/)Viaje Literario - Faraón Angola: "Si hay un escritor que pueda clasificarse de experimental en Colombia es Rodrigo Parra Sandoval. Con una larga y significativa carrera como investigador en Sociología de la educación, su labor[LEER MÁS](https://archivo.contagioradio.com/viaje-literario-faraon-angola/)
