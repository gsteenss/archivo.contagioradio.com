Title: La defensa del agua y la vida se hace Carnaval este 2 de Junio
Date: 2017-06-02 13:59
Category: Ambiente, DDHH
Tags: agua y vida, Marcha carnaval, Mineria, Territorios
Slug: la-defensa-del-agua-y-la-vida-se-hace-carnaval-este-2-de-junio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/marchacarnaval-e1496429818435.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mesa Social] 

###### [02 jun 2017] 

A la gran Marcha Carnaval por la defensa del agua, la vida y el territorio se suman hoy **35 municipios del país.** Protestarán contra las actividades de extracción minera de grandes compañías como Agro Gold Ashanti y Minesa.

Las movilizaciones se realizarán en municipios como Cajamarca donde una caravana de 50 carros llegará a Ibagué a las 2:00 pm. Una vez allí, se unirán a las **160 mil personas que se espera participen en la marcha.**

En Armenia las personas se encontrarán en el Parque Fundadores a las 2:00 pm para llegar a la Plaza de Bolívar donde realizarán actividades cultuales.

En Bucaramanga realizarán un plantón carnaval y una marcha hasta la plaza Luis Carlos Galán para inaugurar la **campaña ciudadana cívica en contra de la mega minería** del páramo de Santurbán.

** Gobierno acomoda normatividad a favor de empresas mineras**

 A los problemas que trae consigo la extracción minera en el ambiente y las poblaciones que habitan en estos territorios, se suma **la negativa del Estado de ratificar a las consultas populares como mecanismo constitucional para defender los territorios.** Diferentes organizaciones y miembros de la sociedad civil, esperan enviar un mensaje de rechazo a las políticas del gobierno que busca privilegiar las intenciones económicas de las grandes empresas mineras. Le puede interesar: ["Todos somos Río Blanco, convergencia en defensa del territorio, el agua y la vida"](https://archivo.contagioradio.com/todos-somos-rio-blanco-una-convergencia-en-defensa-del-territorio-el-agua-y-la-vida/)

El municipio de Cajamarca, por ejemplo, espera que las movilizaciones de hoy sirvan para enviar un mensaje que **valide el resultado de la consulta popular del mes de marzo,** en donde los habitantes de este municipio dijeron que no a la extracción minera de la multinacional Anglo Gold Ashanti.

<iframe src="http://co.ivoox.com/es/player_ek_19048059_2_1.html?data=kp6dlp2UeZqhhpywj5WaaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncavdztLmjbnTttPZ1IqfpZDXs8PmxpDQ0dPXuc3owpDd0dXZsMLmjMrbjajFrsLhwtfQw4qWh4zI0NHWj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según Jimmy Torres, miembro de la organización Conciencia Campesina de Cajamarca, “es una falta de respeto que la multinacional de la mano del gobierno diga que en la consulta popular no votó la mayoría de la gente y lo que hizo Anglo Gold Ashanti fue decirle a la gente que no saliera a votar”.

<iframe src="http://co.ivoox.com/es/player_ek_19048160_2_1.html?data=kp6dlp2VepGhhpywj5aVaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaXVxYqwlYqldc-fotLO28aJdqSf1NTP1MqPsMKfxcrTx9PXpYzYxtGYsoqnd4a1ktfOz9SPqMaftMbb1trWpoa3lIquk5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En Bucaramanga, según Dadán Amaya miembro del Comité para la defensa del agua y el páramo de Santurban, **“estamos explorando medidas jurídicas como acciones populares que nos permitan proteger el páramo y la montaña”.** En la Corte Constitucional se encuentra una acción de tutela que busca que las autoridades realicen una delimitación del territorio santandereano adyacente al páramo consultando a la población. Le puede interesar: ["En fotos: Marcha carnaval, no al fracking, San Martín, Cesar"](https://archivo.contagioradio.com/en-fotos-marcha-carnaval-no-al-fracking-san-martin-cesar/)

<iframe src="http://co.ivoox.com/es/player_ek_19048232_2_1.html?data=kp6dlp2Wd5Ohhpywj5WaaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5ynca-ZpJiSo57XuNDmjLTQw9LUs4ampJDg0cfWqYzk09TQx9jTt4zYxpDQ0dPXuc3owpDd0dXZsMLmjMrbjbaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De igual forma, en el Quindío, según Nestor Ocampo, miembro de la Fundación Ecológica Cosmos de Calarcá, **“hay 4 municipios en el Quindío que ha iniciado procesos para la consulta popular”.** Pijao tiene definido para e 9 de julio realizar la consulta de minería y en camino viene los procesos de Córdoba, Calarcá y Salento.

** La movilización ha mantenido a raya a las compañías mineras**

Para Nestor Ocampo, “las movilizaciones han mantenido a raya a estas compañías. Las comunidades por medio de la presión pacífica, han impedido el avance de los proyectos mineros.” En Bucaramanga, la campaña de movilización ciudadana lleva realizándose por 7 años **cada vez que se despierta “el monstruo minero”** como lo denomina Dadán Amaya. En este territorio, las actividades del gobierno que buscan limpiar el camino para las compañías mineras como Minesa, se han topado con 40 mil personas que buscan proteger el páramo. Le puede interesar: ["Más de 30 municipios del país marcharán en defensa de las consultas populares"](https://archivo.contagioradio.com/mas-de-30-municipios-del-pais-marcharan-en-defensa-de-las-consultas-populares/)

Según Amaya, “el gobierno manipuló la delimitación del páramo dejando a los campesinos sin tierra y dándole la posibilidad a Minesa de construir una mina subterránea que pone en riesgo el recurso hídrico”. De igual forma en el Quindío, hace 9 años **el 62% del territorio estaba comprometido en proyectos mineros. Según** Ocampo, “en ese momento había 98 contratos de concesión minera que ya habían sido otorgados a 80 multinacionales y 133 más estaban en proceso”. Ante esto, 8 de los 12 alcaldes de los municipios quindianos, manifestaron su rechazo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
