Title: La contra violencia 
Date: 2019-10-17 15:30
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: ecuador, Fondo Monetario Internacional, indígenas, Movilización
Slug: la-contra-violencia-en-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/protestas-ecuador.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Columna-Contagio.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

\[audio mp3="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Columna-Contagio.mp3"\]\[/audio\]

Foto: Ocio Latino

 

[El destino de **la protesta social en América Latina** se jugaba en Ecuador. No solo se trataba de una pugna local sobre el decreto impuesto por un típico lacayo del Fondo Monetario Internacional, no. La cosa era más compleja. ]

[En términos políticos estaba sobre la mesa la mascarada republicana hegemónica, los aliados de Estados Unidos, la derecha latinoamericana enfrentada física, verbal, simbólica e históricamente a una fuerza popular que desafió no solo su orden legal, sino a la fuerza policial de un Estado.]

[Asimismo, los ojos de América estaban puestos allí. Sobre un costado de la mesa estaba la tesis pacifista según la cual en Latinoamérica nada se logra con la violencia. Del otro lado se hallaba, “el último reducto de humanidad que le queda a un pueblo oprimido”: la contra violencia. ]

> [según el viejo, nosotros, los colonizados, nos encontramos acorralados entre las armas oficiales que nos apuntan y nuestros tremendos deseos de matar que surgen del fondo de nuestro corazón como ganas de justicia pero que no siempre reconocemos…]

[Ya lo escribía Jean Paul Sartre hace unos años… según el viejo, nosotros, los colonizados, nos encontramos acorralados entre las armas oficiales que nos apuntan y nuestros tremendos deseos de matar que surgen del fondo de nuestro corazón como ganas de justicia pero que no siempre reconocemos… ¿por qué? el viejo decía, porque no es nuestra violencia la que está palpitando en el fondo, sino que la de los opresores, pero invertida; es esa violencia la que crece y nos desgarra. ]

[Ahora bien, **el primer movimiento de nosotros los oprimidos siempre es ocultar profundamente ese deseo de violencia**; ese deseo es rechazado por nuestra moral y por la del opresor (¿la misma?).]

[La tradicional condena liberal a la rebeldía, jamás se salta la tradición de omitir la pregunta]*[¿qué hemos hecho para que los pueblos se rebelen?]*[ Mientras esa pregunta no la respondan con seriedad aquellos tontos que se incomodan por las protestas, sin lugar a duda, los levantamientos, justificadamente continuarán. ]

[Cuando la gente tiene más incertidumbres por la vida que por la muerte, entonces el torrente de la violencia rompe todas las barreras. En Ecuador se jugaba mucho más que la derogación de un decreto. En realidad, se estaba dejando escrita sin papel y sin lápiz, así, a la manera indígena (con la memoria) la ruta que muestra cómo hacer para que la protesta social funcione. ]

[Sin duda alguna, el abrazo dulce a los poderosos, la entrega de flores, el grito “sin violencia”, no borra las señales de las cuotas que abonan los corruptos al levantamiento de los pueblos. Los indígenas en Ecuador no querían tratos legales, no querían ser más la marca de la cara ambientalista y filantrópica de occidente, no querían ser definidos por la prensa hegemónica y su desdichada paz civilista; ellos querían quitarse esas marcas que siempre coloca el poderoso, y en Ecuador demostraron que esas marcas, solamente la contra violencia pudo destruirlas.]

[El indígena violentado, tergiversado, el de las propagandas turísticas, el que produce artículos para el ambientalista que vive en las metrópolis, propuso en Ecuador un nuevo momento histórico para Latinoamérica, ese en el que precisamente mediante la violencia, el falso indígena pudo transformarse y redefinirse autónomamente, mostrarse en su verdad.   ]

> [la gente observó que había un objetivo común, pero más que eso, porque había disposición a la muerte, es decir, se percibió un antagonismo filosófico-político del liberalismo burgués]

[Era necesario romper las barreras que reprimían esa violencia desgarradora del régimen de Moreno y la única manera era mediante la contra violencia. Ese momento lo leyó la población ecuatoriana que no era indígena, y se arrojó solidariamente a ayudarles ¿por qué? sencillamente porque la gente observó que había un objetivo común, pero más que eso, porque había disposición a la muerte, es decir, se percibió un antagonismo filosófico-político del liberalismo burgués (la disposición a la vida). ]

[Observar la palabra, la lucha y las decididas ganas de poner en juego no un par de ideas en un debate estúpido, sino de jugarse el propio cuerpo en esta sociedad del placer exacerbado, fue la enseñanza más grande que ha dejado el pueblo ecuatoriano.  La anti-hegemonía, demostró el indio, no se logra buscando que occidente nos tome en serio, se logró demostrando lo serios que podemos ser: a nuestra manera.  ]

[Épicos días los que vivieron los hermanos ecuatorianos, que luego de ver morir a sus amigos y familiares a manos del régimen de Moreno, siguieron afianzando el último reducto posible de su dignidad: la contra violencia. Absortos observamos desde Colombia, unos viendo con emoción, otros, sobre todo esos pacifistas de sillón que están dedicados a criticar a los violentos mientras se besan los codos, vieron cómo sus tesis se derrumbaron con el pasar de los días, pues la protesta violenta otorgó esa victoria local, y ofreció una gran propuesta para solucionar problemas continentales. ]

[La batalla terminó cuando el régimen de Moreno sabía que, si imprimía más violencia, los indígenas estaban dispuestos a ofrecer más contra violencia, por tanto, se vio perdido, se vio rodeado de una multitud de derechistas latinoamericanos que solo lo defendían desde los medios de información. Pero lo que no calcularon fue que, a esos medios, tocó dejar de hacerles caricaturas y fue necesario salir a callarlos, a quemarles instalaciones, a sacarlos por la fuerza de las protestas. Allí ganaron la batalla los indígenas, por eso Moreno se sintió solo, porque nada, nada puedes hacer cuando tu propio método es superado con un manifiesto de humanidad, y no solo por una capacidad bélica. ]

[Derogado el decreto, la calma regresó. La paz emergente luego de la violencia que manifiestan opresores y oprimidos es una paz sublimada por el reconocimiento que tienen las partes de su potencial... solo allí, hay un lapso temporal, muy corto, pero muy bello, de equilibrio entre hegemonías.  ]

[Y bueno, finalmente hay que decir que en Colombia no falta valor. Eso es una gran mentira de la izquierda espontánea que solo tiene ganas de lanzar piedras, o libros (a veces lo mismo) pero no para refugiarse en su humanidad, sino para sentirse viva y así aumentar su miedo a la muerte, es decir, la afirmación inversa de su amor al régimen. ]

[En Colombia la barbarie tiene niveles más asquerosos. Aquí asesinan a los líderes sociales por la espalda. Los abordan solitariamente. El historial de asesinatos políticos, la larga historia de conflicto armado, las trizas del acuerdo de paz, sumado a que nunca ha gobernado un socialista (como sí en Ecuador) ha reducido la capacidad de organización social y comunitaria; no obstante, aún así las comunidades resisten. ]

[Por eso los más alzados de Colombia están en el campo, porque allá es donde en ocasiones se teme más a la vida propuesta por el régimen que a una muerte luchando contra este. Por eso en el campo muchas personas viven amenazadas, pero aún así protestan con vías de hecho, violentamente, pues asisten a velorios todos los meses y aún así perseveran en la organización en la persecución de un objetivo común. ]

[Por lo anterior, estudiantes, la academia, la sociedad en general, en uso de su razón pública, hoy tiene que dar un gran debate en torno a la violencia, la contra violencia y la paz. El brochazo moralista que gritaba “sin violencia”, ha sido desmentido por los indígenas, desde Ecuador para toda Latinoamérica.  ]

[Por lo pronto, un punto de partida que supera cualquier intento historiográfico (incluida esta columna) de narrar lo ocurrido en Ecuador, es que el pueblo indígena ya lo demostró; perseveró y corrió al último refugio de su humanidad… hacía allá, donde]*[“la violencia, como la lanza de Aquiles, puede cicatrizar las heridas que ha infligido”. ]*

[ ]

##### Ver columna[ de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)
