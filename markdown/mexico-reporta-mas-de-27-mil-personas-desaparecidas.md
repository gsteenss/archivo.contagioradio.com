Title: Durante los últimos 8 años han desaparecido 27 mil personas en México
Date: 2016-02-11 15:27
Category: El mundo
Tags: 43 normalistas, Ayotzinapa, Desaparición forzada, mexico
Slug: mexico-reporta-mas-de-27-mil-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/desaparecidos-Mexico-e1455221979230.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: al Momento 

###### [11 Feb 2016] 

Según el más reciente informe del gobierno mexicano sobre desaparición forzada, en ese país hay un total de **27 mil 659 personas reportadas como desaparecidas**, desde 2007 hasta el 31 de diciembre de 2015.

El Registro Nacional de Datos de Personas Extraviadas o Desaparecidas (RNPED), evidencia que del total de desaparecidos**, 989 están relacionados con el fuero federal, de los cuales 247 son del estado de Guerrero, 165 en Veracruz y 137 en Tamaulipas.** El gobierno de Enrique Peña Nieto, registra el 59.5% del total de los casos de fuero federal.

En los reportes de fuero federal la mayoría corresponde a jóvenes: 56.2 por ciento es de personas que van de los 20 a los 49 años y 19.5 por ciento de ciudadanos entre los cero y los 19 años edad. En 193 casos no se determina la edad de las víctimas.

Por otra parte, sobre los casos de **fuero común, Tamaulipas tiene 5 mil 583 personas en condición de desaparición forzada, luego sigue de Estado de México con 2 mil 662 y Nuevo León con 2 mil 224.**

Esta semana también se evidenció que México es el país que cuenta con el mayor número de periodistas desaparecidos, según cifras del informe presentado el martes, "Periodistas desaparecidos en México" del 2003 al 2015 **se han reportado 23 comunicadores como desaparecidos** en los estados de Veracruz, Tamaulipas y Michoacán.

Cabe recordar que la cifra oficial fue anunciada, luego que los investigadores forenses argentinos negaran la versión de incineración de los 43 normalistas de Ayotzinapa. Según ese equipo, existen pruebas "contundentes" de que los estudiantes no fueron quemados en un basurero en Cocula, como lo había asegurado el Gobierno federal.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
