Title: Coco y la memoria transformante
Date: 2018-01-07 09:58
Category: Camilo, Opinion
Tags: Cine, Cultura, lideres sociales, memoria
Slug: coco-y-la-memoria-transformante
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/coco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Disney Pixar’s 

#### **Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### 7 Ene 2018 

La belleza y la fealdad de lo que somos está ahí, siempre en un cotidiano, acostumbrado a la trágica realidad y a veces despierto a la posibilidad del asombro y maduramente ante el asomo de lo nuevo en lo propio y en lo otro.

El cine un espejo, rastros de nuestro ser, de nuestra memoria, la de la miseria trágica o bella, siempre humana. Coco una película del mundo de Disney, sí de los Estados Unidos, remueve o conmueve, más allá de las razones en ese mundo de lo irracional.

El mundo de los muertos y de los vivos en un mismo escenario sin miramientos maniqueos, desnudando nuestras almas plagadas de negacionismos, de exclusiones y rechazos. Al fin y al cabo podemos en una memoria insana ser sujetos de eso otro, en nosotros mismos, y de postergar en nuestra historia la posibilidad de una esquiva verdad de las verdades, necesaria para un sociedad donde la inclusión y la justicia sea la posibilidad de la paz.

"Era la Tierra de los Muertos; sin embargo, estaba llena de vida", le dicen al niño Miguel Rivera, él quiere ser cómo el cantante que honra Ernesto de la Cruz.

Esa tierra de muertos (y desaparecidos) es vida, de múltiples y bellos colores en los que aparecen los claro oscuros de la Tierra de los Vivos. En la Tierra de los Muertos, allí está la humanidad tal cómo es con la misma memoria histórica sin resolver las negaciones que existen detrás del poder y la fama. En la Tierra de los Muertos el espejismo persiste y solo se libera de la repetición en la memoria transformante. Todo depende de lo que hagan con su historia, la de los muertos, los vivos.

La exclusiones en los retratos en la celebración de los muertos (y los desaparecidos) en la Tierra de los Vivos son una tormenta en el valle de los muertos. Si nadie les recuerda, se mueren al parecer para siempre. En la Tierra de los Muertos, la víctima nunca se victimizó solo buscó ser honrando en la historia de los vivos para ser festejado.

Su inclusión en el valle de la vida es la honra de su historia cómo fue y la posibilidad de una nueva historia, sin repeticiones, distinta. En el valle de los muertos, lo negado y oculto se resuelve a favor de la vida y la alegría en el valle de los vivos, solo porque los vivos conocen y reconocen las verdades de la verdad. El mundo de los muertos equivocamente heroizados se transforma en los encuentros de verdades cuando los vivos reconocen los hechos que eran excluidos.Solo así los rostros de hierro se pulverizan

La falsa idolatrización de Ernesto de la Cruz, el gran cantante y compositor se deshace, en lo despiadadamente humano al ser visto cómo es por sus fanáticos, los muertos en vida y los muertos vivos. Allí se deshace el ídolo.

Lla honra de la vida de asesinados y de desaparecidos en la memoria transformante está ahí. Cómo en Coco:"Cuando no queda nadie en el Mundo de los Vivos que te recuerde, desapareces de este mundo". Y se trata del recuerdo que supera el anclamiento sufriente, que hace de los rituales de la memoria la Vida, una bella vida

Lo vivido en años de violencia política está vivo en uno mismo y siempre en busca de expresión, despliegue y pacificación, así no lo hayamos vivido.

Está ahí en la Coco que espera al padre, en Miguel que expresa el amor interrumpido por la violencia en su bisabuelo, en una familia que se niega a asumir la historia en su complejidad.

Si los asesinatos de Álvaro Gómez o Luis Carlos Galán han sido propiciados estructuras estatales, los guardianes de la memoria deberían así decirlo y reconocerlo. Hacer juegos para mantener la sacralidad de la democracia de papel es una defraudación.

Hay un poderoso sector que ha sido incapaz de ceder a sus propósitos infantiles manteniendo las mentiras. El amor ciego a una democracia excluyente, que usa y tira, o deshecha por su ambición está evitando la verdad. Los beneficiarios asegurando la propiedad mal habida y sus negocios a nombre del capital y de la creencia católica o cristiana, se empeñan tercamente en imponer la falsedad a la fuerza. Cuando el otro le es obstáculo o inconveniente así sea de la misma clase política, le mata, y ahora pretenden ahogar la posibilidad de la verdad.

Así como Ernesto de la Cruz envenenó a Ramón, el auténtico cantante y compositor, así la estrechez del alma dirigente que ha llevado al exterminio de la oposición política, a imposibilitar la democratización de la tierra, y la protección de fuentes de vida sustanciales para el país y la humanidad, está pretendiendo evitar nuestra sentido de seres sanamente vivientes en la verdad.

Estamos en un momento de posibilidad de memoria transformante. Una memoria colectiva que asume lo excluido, lo negado o lo rechazado. Solo reconociendo lo que hoy se desconoce en la historia nacional, en el mundo urbano de lo rural, en las narrativas mediáticas dominantes, nacerá lo nuevo. Tres dinámicas con sus matices podrían converger en esa necesaria memoria más allá de la que quieren asegurar Santos, Uribe, Vargas Lleras, por nombrar algunos.

La Comisión de Esclarecimiento de la verdad. 70 años de la reciente violencia política parcial y fragmentariamente en camino de su resolución con la dejación de armas de las FARC y el Acuerdo del Teatro Colón, abrió ese espacio de memoria incluyente.

La Mesa de Quito en la que el ELN ha expresado que es su decisión de ellos aportar a la verdad, y afirman la.importancia de la verdad toda y de todos.

Y un entramado de decisiones que deberían adoptarse frente a las nuevas formas de paramilitarismo y operaciones criminales sutilmente negadas de su relación con estructuras de Estado pero imbricadas en sus fórmulas de acumulación como Estado corporativo multinacional, que dificilmente sera respondido por Santos.

Por eso más allá hay un pasado vigente de exmandos paramilitares dispuestos a aportar a esas verdad completa. Y las manifestaciones de las AGC con motivo de la visita del Papá y el reciente anuncio de su cese unilateral de fuego nos abre posibilidad para esas verdades.

Lo expresó Pepe Mújica ante la destilación de odio de Uribe es por la descendencia. Las verdades son necesarias ante la tentación presente de las venganzas o de las memorias fanáticas de soberbia militar o económica. La familia Colombia de los vivos y de los muertos tiene un momento en la historia para moverse al ritmo de una melodía, la del bello existir de todas y todos renociendo lo qué es, lo qué fue para que el futuro sea distinto, y la memoria deje de ser un fraude, otro engaño.

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<div dir="auto">

</div>
