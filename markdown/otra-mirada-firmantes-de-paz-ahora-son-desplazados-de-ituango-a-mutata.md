Title: Otra Mirada: Firmantes de paz ahora son desplazados de Ituango a Mutatá
Date: 2020-07-15 22:43
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Desplazamiento Ituango, etcr, Mutatá
Slug: otra-mirada-firmantes-de-paz-ahora-son-desplazados-de-ituango-a-mutata
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/ETCR-ITUANGO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: [@ComunesANT](https://twitter.com/ComunesANT)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el partido FARC, en lo que va del 2020 se han reportado 34 homicidios en contra de excombatientes firmantes de paz, y uno de los territorios que más afectado se ha visto es el ETCR en Ituango, subregión del departamento de Antioquia. Por esto, y por las constantes amenazas por parte de grupos armados y el abandono del Estado, los firmantes de paz, habitantes de este territorio, decidieron realizar un desplazamiento masivo este 15 de julio desde la vereda Santa Lucía a Mutatá, subregión de Urabá. (Si quiere saber más: [Esperamos que en Mutatá encontremos la paz: excombatientes de Ituango](https://archivo.contagioradio.com/esperamos-que-en-mutata-encontremos-la-paz-excombatientes-de-ituango/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gustavo López Alcaraz, delegado de FARC ante el Consejo Territorial de Reincorporación, Antonio Sanguino, senador e integrante de la Comisión de Paz del Senado, Oscar Yesid Zapata, defensor de derechos humanos de la Fundación Sumapaz y Camilo Fagua Castellanos, abogado defensor de derechos humanos fueron los invitados de este programa, quienes como habitantes y expertos explicaron la forma en que se está abordando este desplazamiento, en busca de la paz firmada en el acuerdo del 2016. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, dieron a conocer cuáles fueron las razones para realizar este desplazamiento más allá de huir de la violencia, cómo fue toda la logística con las familias que se estarían movilizando para asegurar que se hiciera de forma segura y, dado que algunas familias decidieron permanecer en el ETCR, explican cuáles son las garantías que estas esperan tener. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y aunque saben que es una salida forzada, explican los invitados que la comunidad está dividida entre la nostalgia y la preocupación por tener que abandonar lo que construyeron, el deseo y la esperanza de estabilizarse en un lugar y tener posibilidades de trabajar fortaleciendo el campo. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde las organizaciones y desde el mismo senado, también explican cómo se ve la situación y qué han hecho para ayudar a las comunidades y trabajar como intermediarias para pedirle al gobierno que no haga oídos sordos a la situación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, aclaran y enfatizan que la situación en Mutatá no deja de ser preocupante ya que esta zona está dominada por grupos paramilitares y que, si no se toman represalias, no va a ser el ETCR de Ituango el único que se vea obligado a dejar el territorio este año.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Si desea escuchar el programa del 13 de julio: [Movilización social en tiempos de pandemia](https://bit.ly/3gX8NOY))

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/606485226733305","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/606485226733305

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
