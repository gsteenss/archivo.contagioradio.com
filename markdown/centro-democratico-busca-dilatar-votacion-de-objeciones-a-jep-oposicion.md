Title: Centro Democrático busca dilatar votación de objeciones a JEP: Oposición
Date: 2019-04-30 14:52
Author: CtgAdm
Category: Paz, Política
Tags: Antonio Sanguino, Centro Democr´, Ley estatutaria
Slug: centro-democratico-busca-dilatar-votacion-de-objeciones-a-jep-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/senado_farc_operación-tortuga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Honduradio] 

Tras más de seis horas de debate en plenaria del Senado, la votación a las objeciones a la ley estatutaria de la Jurisdicción Especial para la Paz (JEP) fue aplazada para hoy a las 2 de la tarde, lo cual generó discordia entre algunos senadores de la oposición que acusaron a los congresistas, aliados con el Gobierno, de retrasar el voto.

El Senador del Partido Verde **Antonio Sanguino** calificó el debate de ayer como "vergonzoso" por [las "maniobras dilatorias" que ha implementado el Centro Democrático y en particular el Presidente del Senado, Ernesto Macías. "Es evidente que al no tener las mayorías para aprobar las objeciones inconstitucionales del Presidente Duque, ha acudido a la dilación, la maniobra, la triquiñuela y la trampa", señaló el congresista.]

El senador habló en referencia por un lado, a la tutela que el Senador Ernesto Macías radicó la semana pasada al considerar que el actual procedimiento para la votación de las objeciones estaba errada porque debieron haber pasado primero por el Senado en cambio de la Cámara de Representantes, donde ya fueron rechazadas. (Le puede interesar: "[Objeciones de Duque a la JEP son rechazadas en la Cámara de Representantes](https://archivo.contagioradio.com/rechazadas-las-objeciones-de-duque-a-la-jep-en-la-camara-de-representantes/)")

El senador también señaló la recusación que presentó el Senador Macías en contra del Senador Iván Cepeda porque un supuesto conflicto de interés dado que su esposa trabaja en la JEP, lo cual generó un debate alargado y más recusaciones en contra de los senadores Roy Barrera, Álvaro Uribe, Fernando Araujo, Laureano Acuña, Ciro Ramírez y el mismo Presidente del Senado, Ernesto Macías.

Al retrasar el voto, el senador indica que el Congreso tampoco podrá proceder con el resto de la agenda legislativa, lo cual pondría "en vilo, por culpa del uribismo, el propio Plan de Desarrollo, que debe ser aprobado antes del 7 de mayo".

<iframe id="audio_35172570" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35172570_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
