Title: Gana el Si en el plebiscito en el exterior
Date: 2016-10-02 16:20
Category: Paz
Slug: gana-el-si-en-el-plebiscito-en-el-exterior
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/elec.jpg_1718483346.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Según unas primeras encuestas realizadas a boca de urna en Europa el SI tiene la ventaja por un porcentaje promedio del 65% contra el 35% que votó por el NO. Aunque la abstención alcanzó un alto margen los colombianos y colombianas que respaldaron el acuerdo final fueron mayoría.

Berlin 305 votos 265=Sí 39=no 1=nulo

desde Valencia:  
- Sí: 70,5%  
- No: 29,5%  
23 mesas de votación.  
en todas ganó el SÍ salvo una que empató.

en Barcelona: 1261 votos por el sí/452 votos por el No

Madrid, de momento:  
31% escrutado  
952 votos en total  
Sí: 69,01%  
No: 30,99%

Escrutinios en Amsterdan  
SI 139  
NO 34

Escrutinios Bruselas Bélgica  
SI. 314  
NO. 71
