Title: Plataformas de DD.HH. solicitan a la ONU permanencia de Misión de Verificación
Date: 2020-09-16 13:31
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Implementación de Acuerdos, ONU
Slug: plataformas-de-dd-hh-solicitan-a-la-onu-permanencia-de-mision-de-verificacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/ONU.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: ONU

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de un comunicado dirigido al Consejo de Seguridad de las Naciones Unidas (ONU), [organizaciones, plataformas y colectivos en defensa de los DD.HH](https://www.colectivodeabogados.org/?Solicitamos-al-Consejo-de-Seguridad-de-Naciones-Unidas-la-renovacion-del). solicitaron la renovación de su mandato, pues resaltan que aunque han transcurrido tres años desde la firma del Acuerdo de Paz, no existen garantías en puntos decisivos del Acuerdo como la Reforma Rural Integral, la participación política de los excombatientes, el desmantelamiento de las estructuras criminales y el incumplimiento en el punto cuatro del acuerdo, relacionado a la sustitución de cultivos de uso ilícito al que 99.000 familias se han inscrito.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Presencia militar no es sinónimo de seguridad para comunidades

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Expresaron cómo la presencia militar en los territorios no ha garantizado la seguridad de las poblaciones, lo que ha tenido como consecuencia que desde la firma del Acuerdo Final hasta julio 15 de 2020 hayan sido asesinados en zonas militarizadas 971 líderes sociales y 215 personas excombatientes. Además, según datos de Indepaz, Desde la firma del acuerdo de paz a la actualidad, se tiene registro de más de 90 líderes asesinados por temas relacionados con sustitución de cultivos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma señalan cómo en otras zonas con presencia de la Fuerza Pública ha sido evidente un incremento en "la criminalidad y el poder armado por parte de diversas estructuras armadas". [(Lea también: Paramilitares impiden el levantamiento del cuerpo de Edier Lopera tras 8 días de su asesinato)](https://archivo.contagioradio.com/paramilitares-impiden-el-levantamiento-del-cuerpo-de-edier-lopera-tras-8-dias-de-su-asesinato/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha crisis, denuncian las organizaciones, se ha agudizado con la Covid-19, sumada a una "falta de voluntad del gobierno nacional en relación con la implementación del Acuerdos de Paz", plasmado no solo en los testimonios de las comunidades, sino en los informes nacionales e internacionales de verificación de implementación del Acuerdo. [(Lea también: Más medidas integrales y menos accionar policial y militar: ONU)](http://Más%20medidas%20integrales%20y%20menos%20accionar%20policial%20y%20militar:%20ONU)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante tal situación han solicitado se impulse e invite al Gobierno y a los actores armados del territorio legales e ilegales a un cese al fuego y poder proteger a las comunidades rurales azotadas por la violencia y apoyarlas en medio de la pandemia. Así mismo solicitaron continuar en la veeduría de la aplicación del enfoque de género y diferencial del acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Se solicitó también, continuar con el seguimiento del cumplimiento de las sanciones del Tribunal de Paz de la Jurisdicción Especial de Paz (JEP), sus comparecientes y de los lugares donde serán ejecutadas las sanciones y el plan de seguridad y vigilancia para garantizar la integridad tanto de los sancionados como de las víctimas y sus territorios.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Las organizaciones reiteraron su compromiso al señalar cómo de la mano de la comunidad internacional han logrado la adopción de mecanismos para proteger a las comunidades e impulsar a iniciativas humanitarias como formas de resolver el conflicto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La ONU mantiene sus ojos sobre Colombia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, desde la Oficina del Alto Comisionado para los Derechos Humanos (ACNUDH), la alta comisionada **Michelle Bachelet expresó su preocupación por la situación que vive el país**, destacando que desde la ONU se ha documentado la muerte de 47 defensores en lo corrido de 2020 y se encuentra en proceso de verificar la de otros 44.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, se manifestó con relación a las recientes manifestaciones ocurridas en Bogotá y otras ciudades del país, constatando el uso excesivo de la fuerza por parte de la Policía y que dejaron como resultado, trece personas asesinadas y 300 heridas," incluidas 77 con arma de fuego", situación que reiteró la Alta Comisionada, está en proceso de verificación y de tal modo "brindar asistencia técnica a las autoridades en materia de control policial de las protestas".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre el Acuerdo de Paz, Bachelett manifestó la importancia de implementarlo en su totalidad "para prevenir más violencia y abusos y violaciones de derechos humanos”.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
