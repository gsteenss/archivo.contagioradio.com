Title: ONIC denuncia la quema de una casa ceremonial del pueblo Kankuamo
Date: 2018-10-19 15:52
Author: AdminContagio
Category: DDHH, Nacional
Tags: Casa de ceremonía, indígenas, ONIC, Pueblo Kankuamo
Slug: quema-casa-kankuamo
Status: published

###### [Foto: @ONIC\_Colombia] 

###### [19 Oct 2018] 

La **Organización Nacional Indígena de Colombia (ONIC)** denunció que el pueblo Kankuamo sufrió el pasado jueves en horas de la noche, **la quema de una Kankurwa, o casa ceremonial de hombres en la comunidad de Atánquez, Cesar,** lugares ceremoniales que son de vital importancia para la conservación de sus tradiciones, y la quema de los mismos ha sido una estrategia usada desde la Colonia como arma de intimidación y amedrantamiento.

El incendio fue provocado cerca de la media noche, y gracias a la respuesta de la comunidad de Atánquez, que habita en el resguardo indígena de la Sierra Nevada de Santa Marta, fue posible evitar que las llamas consumieran la Kankurwa de mujeres y el kiosko de recibo. Sin embargo, denunciaron que **es necesario poner freno a este tipo de acciones que son sistemáticas en los territorios indígenas.**

**John Torres, integrante del Cabildo Menor de la Comunidad de Atánquez,** destacó que estos Centros Espirituales **son de vital importancia para su comunidad, en tanto era la casa en la que el Consejo de mayores se reunía para impartir justicia para su propia gente**.

El hecho se suma a la quema de los centros de ceremonia de hombres y mujeres perpetrada en agosto del año pasado en la comunidad de Guatapurí; situación que se repitió en 2016 en la comunidad de La mina y en 2015 en la comunidad de Chemesquemena, hechos que profundizan el **riesgo de extinción cultural** que viven las comunidades indígenas, declarado por la Corte Constitucional en el Auto 004 del 2009. (Le puede interesar: ["66% de los pueblos indígenas está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

Por esta razón, la Organización pidió que se esclarecieran los hechos, y que en concertación con las Autoridades Indígenas, se tomen las medidas correspondientes para garantizar la "debida protección y cuidado para todos y cada uno de las autoridades, líderes y miembros del pueblo Kankuamo, y de otros pueblos que padecen igual o parecidos atentados".

Desde otro punto de vista, Torres manifestó que la comunidad está conmocionada y triste por los sucesos, pero aseguró que eso no impedirá **que se reconstruya la Kankurwa en el menor tiempo posible. **(Le puede interesar: ["Cuenten con nosotros para la paz, nunca para la guerra: ONIC"](https://archivo.contagioradio.com/cuenten-con-nosotros-para-la-paz-onic/))

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw)| Pueblo Kankuamo sufre otra vez quema de Casa Ceremonial o Kankurwa 👉🏼 <https://t.co/NaROQBhDOW>. [@luiskankui](https://twitter.com/luiskankui?ref_src=twsrc%5Etfw) [@cabildokankui](https://twitter.com/cabildokankui?ref_src=twsrc%5Etfw) [@cabildokankuamo](https://twitter.com/cabildokankuamo?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw) [@ctcsierranevada](https://twitter.com/ctcsierranevada?ref_src=twsrc%5Etfw) [@El\_Pilon](https://twitter.com/El_Pilon?ref_src=twsrc%5Etfw) [@LaPlenaPrensa](https://twitter.com/LaPlenaPrensa?ref_src=twsrc%5Etfw) [@elespectador](https://twitter.com/elespectador?ref_src=twsrc%5Etfw) [@teleSURColombia](https://twitter.com/teleSURColombia?ref_src=twsrc%5Etfw) [@CJAkubadaura](https://twitter.com/CJAkubadaura?ref_src=twsrc%5Etfw) [@Ccajar](https://twitter.com/Ccajar?ref_src=twsrc%5Etfw). [pic.twitter.com/eeNQ1kHbfB](https://t.co/eeNQ1kHbfB)
>
> — ONIC (@ONIC\_Colombia) [19 de octubre de 2018](https://twitter.com/ONIC_Colombia/status/1053282914539106309?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
<iframe id="audio_29472393" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29472393_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
