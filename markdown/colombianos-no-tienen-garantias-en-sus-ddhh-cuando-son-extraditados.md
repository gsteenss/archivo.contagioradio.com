Title: Colombianos no tienen garantías en sus DDHH cuando son extraditados
Date: 2015-08-19 17:44
Category: DDHH, Nacional
Tags: colombia, extradicion, Internacional, Nicaragua, proceso de paz, Proyecto de Ley, Víctor Correa
Slug: colombianos-no-tienen-garantias-en-sus-ddhh-cuando-son-extraditados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/5_Actualidad_19_1okkp01-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El nacional] 

<iframe src="http://www.ivoox.com/player_ek_7129798_2_1.html?data=mJafm5ydfI6ZmKiak5aJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbgwtncjcnJb9bijKfSw9nWaaSnhqax3IqWh4zpz8aYz9rOqdOf0trSjdnNqc%2FZjMrl1tfFqMrowsncjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Beatriz, hermana de Javier Darío extraditado en Nicaragua] 

###### [19 ago 2015]

Una celda de dos metros por dos metros es el espacio en el que ha vivido durante casi 3 años en Nicaragua Javier Darío, un hombre que puede ver el sol unicamente 10 minutos cada 15 días, cuando puede hacerlo, debido a que sus piernas no siempre le responden por la artritis crónica que sufre, sumado a eso, sus días los pasa encadenado de pies y manos.

Se trata de uno de los **2062 casos de extradición de colombianos desde 1997 hasta inicios de este año,** según cifras oficiales, en las cuales se apoya el represente a la cámara Víctor Correa para sustentar su proyecto de Ley  009 de 2015 radicado el pasado 21 de julio, por medio del cual se pretende regular la extradición, con el fin de fortalecer la soberanía del país, pero ante todo garantizar los derechos de los colombianos y la verdad a las víctimas.

 “Mi hermano está condenado en Nicaragua por un crimen que no cometió”, cuenta la hermana de Javier Dario, Beatríz, con voz temblorosa y como si le faltara el aire al igual que le sucede a su hermano en la cárcel de Managua, donde paga una condena de 17 años de prisión tras haber sido **involucrado con el asesinato del cantautor Facundo Cabral**.

Según Beatriz, en 2011 cuando se cometió el asesinato de Cabral, su hermano estaba hospitalizado en Colombia precisamente por los problemas de salud que ha tenido desde hace varios años. Sin embargo, en el 2012, cuando Darío estaba de vacaciones en Nicaragua, lo detuvieron y sin ninguna prueba lo acusaron de ese crimen.

Aunque existen todas las pruebas e incluso el médico que atendió a Javier viajó a Nicaragua como testigo de la situación de su paciente, el gobierno del país centroamericano no ha querido validar ese testimonio y otros documentos presentados. Además, de acuerdo con las palabras de Beatriz el consulado de Colombia en Nicaragua, “no tiene voz ni voto”, y por otro lado, la cancillería colombiana “no ha hecho nada” para ayudarle a su familia, **incluidas dos hijas pequeñas que Javier dejó en Colombia.**

Actualmente,  la cancillería solo ha manifestado que Javier está en fila para ser repatriado, “pero no se avanza en nada”, dice la familiar, quien asegura que a su hermano ya ni siquiera le interesa quedar libre, sino por lo menos poder pagar una condena injusta pero en su país cerca a su familia.

“Le están vulnerando todos los derechos, a mi hermano se le está acabado la vida”, expresa con sufrimiento Beatriz, quien añade que su hermano además de sufrir artritis crónica, padece de hipertensión, y aunque para mantenerse bien deben enviarle sus medicamentos, lo cierto es que en la cárcel Nicaragüense le dan las pastillas “cuando ellos quieren”.

 Pese a la difícil situación de su hermano, la hermana del extraditado, asegura que el proyecto de Ley del representante Correa, significa **“una luz grandísima, toca tener una paciencia inmensa,** pero esperamos que a mi hermano lo cobije este proyecto, porque queremos que esté  aquí con nosotros”, señala Beatriz.

**Proyecto de Ley 009 2015**

El Senador Víctor Correa presentó el proyecto de ley 009 radicado el 21 de julio del 2015 "pretende reglamentar, regular, modificar algunos elementos bajo los cuales opera la extradición en nuestro país". En Colombia se han realizado 2062 extradiciones hasta la fecha de hoy, aunque en estos acuerdos de extradiciones no hay una reciprocidad en cuanto a los acuerdos con otros países como sucede con Estados Unidos.

En la investigación para presentar el proyecto Correa, se encontró con múltiples abusos contra los colombianos en otros países "torturas, tratos inhumanos, no hay acceso efectivo y real a la justicia", indica el representante del Polo Democrático.

Correa también indicó que los extraditables pagan los delitos por los que son requeridos en el extranjero, pero los delitos en Colombia no tienen una condena, fortaleciendo la impunidad.

El enfoque principal está dado en cuatro aspectos:

-   1- Garantías procesales
-   2-Protección de los derechos humanos de los connacionales
-   3- Repatriación
-   4- Procesos de paz, construcción de la verdad para las víctimas.

<div>

</div>
