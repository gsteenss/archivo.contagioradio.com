Title: Gobierno no ha cumplido acuerdos con campesinos de Putumayo
Date: 2015-03-16 20:08
Author: CtgAdm
Category: Comunidad, Nacional, yoreporto
Tags: Comunicado sobre acuerdo para desminado Diálogos Habana, Mesa regional de organizaciones de Putumayo, ZRC "Perla Amazónica" Putumayo
Slug: mesa-de-organizaciones-sociales-del-putumayo-se-pronuncia-sobre-acuerdos-alcanzados-en-la-habana
Status: published

###### Foto:Contagioradio.com 

La **Mesa Regional de Organizaciones Sociales de Putumayo**, Baja Bota Caucana y Cofanía Jardines de Sucumbíos de Ipiales Nariño, damos a conocer a la institucionalidad, iglesias, organizaciones sociales, indígenas, campesinas, afrodescendientes, sindicales, de mujeres, victimas, desplazados, juventudes y comunidad en general, lo siguiente:

Que en el marco de los acuerdos firmados con el gobierno nacional, en cabeza de sus ministros el pasado 05 y 19 de septiembre de 2014 respectivamente, se logra establecer entre las partes cuatro comisiones de trabajo con miras a encontrar la solución a la difícil crisis que atraviesa la región; **COMISION DE SUSTITUCION DE CULTIVOS DE USO ILICITO, COMISION MINERO-ENERGETICA, COMISION DE DERECHOS HUMANOS Y COMISION DE INVERSION SOCIAL**. De las cuales, presentamos hasta la fecha, el siguiente balance;

1.  En la Comisión de Sustitución de Cultivos de Uso Ilícito después de más de cinco sesiones de trabajo conjunto con el Gobierno Nacional, **no se ha logrado concertar el Plan de Sustitución propuesto por la Mesa**. Se ha logrado avanzar en el objetivo general, algunos objetivos específicos, criterios para la aplicación y algunas actividades. No hubo avances en el componente del plan; ORDENAMIENTO CON ENFOQUE TERRITORIAL Y CULTURAL, y su objetivo Garantizar la formalización  de la propiedad de la tierra y promover el acceso progresivo de las comunidades campesinas, indígenas y afrodescendientes, en vista de no alcanzar avances en estos aspectos se propuso elevar estas propuestas a un nivel más alto del Gobierno Nacional con poder de decisión, bajo el compromiso por parte de la comisión del Gobierno de gestionar en el término de 15 días reunión con altos funcionarios.
2.  La Comisión **Minero-energética, no ha logrado avanzar en la recolección de muestras por los incumplimientos en la contratación del laboratorio**, para realizar los respectivos estudios sobre la contaminación y afectación ambiental en la zona de Puerto Vega Teteye, Municipio Puerto Asis.
3.  Por su parte, la **Comisión de Derechos Humanos no ha logrado progresar en los compromisos iniciales adquiridos por parte del Gobierno Nacional**. Por el contrario, y en la región se agudiza la violación sistemática a los derechos humanos de las comunidades.
4.  La Comisión de Inversión Social aún no se instala a pesar del compromiso asumido por el Gobierno Nacional en cabeza del Ministro del Interior de instalar esta comisión la primera semana de marzo.

Los **avances en el proceso de negociación no llenan las expectativas de las organizaciones** que hacen parte de la Mesa Regional, ni de las comunidades de la región que albergan la esperanza de que por fin, después de tantos años de abandono estatal se logren resolver las diferentes problemáticas sociales de la región.

Hacemos un llamado a los garantes de este proceso; Naciones Unidas, Colombianos y Colombianas por La Paz, Defensoría del Pueblo y Comisión Intereclesial de Justicia y Paz, para que en su papel de garantes medien en la búsqueda de soluciones a la crisis del proceso de concertación, para lograr el progreso efectivo de los acuerdos fijados por las partes.

Vemos con **honda preocupación la falta de voluntad del Gobierno Nacional**, razón por la cual, hacemos un llamado a todas las comunidades de la región a respaldar este **proceso de interlocución y a aunar esfuerzos en la búsqueda de soluciones definitivas** a esta problemática, que garantice el goce efectivo de los derechos de todos y todas para conseguir la paz con justicia social y una vida digna.
