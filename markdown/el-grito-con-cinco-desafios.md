Title: El Grito con cinco desafíos
Date: 2019-07-29 13:00
Author: Camilo de las Casas
Category: Camilo, Opinion
Tags: El Grito, lideres sociales, Manifestaciones sociales
Slug: el-grito-con-cinco-desafios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/27jpg.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Zea/Contagio Radio 

El 26 de julio marcó un hito como manifestación social en las calles. La expresión plural y diversa que rechaza la violencia contra los líderes sociales y sus asesinatos (734), según, INDEPAZ, entre el 1 de enero de 2016 y 1 de julio de 2019, ya (59) asesinados en 2019, según, Somos Defensores. Hay que decirlo, que la manifestación también se expresó con menor fuerza o claridad, por los 134 ex combatientes de las FARC.

La expresión civilista, no exclusiva de los grupos sociales organizados, de iglesias, de las diversas identidades sexuales, de los animalistas y ambientalistas, que se identificaron con la convocatoria de Defendamos la Paz, nos indica que hay asuntos en que un gran porcentaje de colombianos nos podemos identificar, tal como en los momentos de las victorias deportivas. El triunfo de ese día en Los Pirineos de Egan Bernal, que sin lograr terminar la etapa por el lodo, se vistió con la camiseta de líder, y dos días después con el triunfo en el Tour de Francia, unió a amplios sectores de colombianos. Algunos continuaron con su aislamiento y amargura. El 26 de julio logró evocar el alma de quiénes se expresaron a favor del Sí, luego de la perdida en el plebiscito, reanimar a los sectores que están comprometidos en la paz, y brindar un horizonte de solidaridad con las víctimas en esta nueva etapa de violencia. Otros continuaron con el saboteo y su amargura.

Y esas actitudes, sumadas al negacionismo e infantilismo es lo que se percibe de Duque. Un presidente rehén de Él. Un alter ego poco creíble, seriamente limitado para mantener el embrujo de su mentor. La animadversión al proceso de paz por parte del partido de gobierno sigue siendo equívoca, y eso lo observa, lo percibe la ciudadanía. Lo de menos fue la rechifla, la exclusión de la marcha y los calificativos al presidente Duque en Cartagena. Fue provocador, inadecuado e impropio que el presidente Duque hubiera intentado marchar. Su respaldo a la movilización debió haberse realizado de otro modo. Ese otro modo, evidentemente es costoso para él, imposibilitado para ser él mismo.

La nueva agenda legislativa definida por Él como la legalización de una nueva contra reforma agraria, la insistencia en doble instancia para los sentenciados por corrupción como Andrés Felipe Arias y los parapolíticos, la creación de una sala especial para los militares frente a su responsabilidad en Crímenes de Lesa Humanidad y Crímenes de Guerra, va en contravía del espíritu del Acuerdo de Paz.

En ese ambiente, adobado por un Ministro de Defensa torpe, cínico y negador de la grave crisis de derechos humanos que refleja el asesinato de personas que en comunidades urbanas y rurales, habla, habla sin que su jefe lo objete. Nunca Duque dijo nada sobre su difamación- Detrás de las movilizaciones sociales está es el impulso de fuerzas del narcotráfico. Criminalizando y desnaturalizando la protesta social El presidente ha sido incapaz de objetar la banalización de la muerte de los excombatientes como el caso de Dilmar Torres. Por el contrario, al militar que pidió perdón por ese hecho nunca lo destacó. Tampoco rechazó la persecución a los militares que denunciaron las políticas de ejecución de civiles impulsado por un sector de los generales. En ambos casos, como en la mayoría de los asesinatos ha guardado silencio, o ha sido tardía su reacción, legitimando la visión de su partido de gobierno

Así lo menor fue la rechifla o la expresión de asesino, la que más allá de descalificarse por ser contra las normas de Carreño, refleja lo que muchos piensan de un gobierno incapaz, de enfrentar con eficacia y coherencia la herencia que recibió de un Santos, también, muy débil para enfrentar la nueva fase de violencia de sectores poderosos locales, regionales y con algún vínculo nacional que bajo la Pax Neoliberal pretenden definir el uso del suelo, del subsuelo a como dé lugar.

Si el presidente Duque pretende pasar a la historia como el de la legalidad, debe adoptar medidas eficaces, por los menos con cinco medidas: 1. Modificar el Plan de Acción Oportuna e inscribirla más bien dentro de la medidas políticas y de orientación criminal de la Comisión Nacional de Garantías del Acuerdo. 2. Reactivar la mesa de conversaciones con el ELN a partir de los avances sustanciales dados durante el gobierno de Santos. 3. Iniciar un debate con definición de nuevas políticas expresas, explícitas, precisas de la Doctrina Militar de la Seguridad Nacional por una Doctrina Militar de la Seguridad Humana Ambiental, y él de la Seguridad Ciudadana, que supere la prejuiciosa y criminal del enemigo interno que continúa rondando en muchos sectores militares y policiales. 4. Reconstituir una propuesta de desmonte de estructuras paramilitares y criminales, y 5. Exigir resultados de la Unidad de Investigación y desmonte del paramilitarismo por lo menos en tres dinámicas regionales

La expresión del grito del 26 de julio tiene unos retos grandes en el horizonte, que combinan de suyo los ámbitos de la movilización y del escenario político. De un lado, la ampliación de los convocantes en Defendamos la Paz, y la sana articulación de lo social y lo político.

Aún estamos en el inicio de la alta montaña con bastante lodo, hay mucho por trepar, y ha sido un buen inicio el pulso ciudadano que se ha ido abriendo. Construir la unidad entre las diferencias frente a los cinco temas, considero entre otros, son los derroteros que se abren. Si el presidente Duque, los asume, cuestión hoy absolutamente improbable, estaremos a puntos de juntarnos todos como parte de un proyecto de país, dejando en la amargura, y el aislamiento a quién vive, infantilmente del rencor y de la guerra, y de la vasallos idólatras.

P.D.

Gracias Egan Bernal por enseñar, que su triunfo honra la memoria, de los que le antecedieron en el ciclismo, porque es posible creer en lo nuevo, a partir de las carencias y las necesidades, y que al fin se logró lo deseado. El triunfo es del equipo. Nuestro triunfo será la paz en las armas para conquistar la democracia incluyente, de justicia socio ambiental. Esto solo es posible en equipo y honrando la memoria.

**Leer más columnas de [opinión de Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**
