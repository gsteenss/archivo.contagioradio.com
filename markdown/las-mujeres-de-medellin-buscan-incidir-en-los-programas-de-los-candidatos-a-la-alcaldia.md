Title: Las mujeres de Medellín buscan incidir en los programas de los candidatos a la alcaldía
Date: 2015-07-22 16:12
Category: DDHH, Mujer
Tags: Alcaldía, candidatos, jeimi arias, Medellin, mesa de mujeres, mujeres, red
Slug: las-mujeres-de-medellin-buscan-incidir-en-los-programas-de-los-candidatos-a-la-alcaldia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Vamos Mujer 

<iframe src="http://www.ivoox.com/player_ek_5146750_2_1.html?data=lpahmJyZdI6ZmKiakp6Jd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMLnjNLizMrWqdSfxcqYr8rIqc3ghqigh6aosozW1tjQw9OPrc%2FXysnW1JDJsozg0NiY0tfTq9PVzsaah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jeimi Arias, Mesa de la Mujer de Medellín] 

###### [22 jul 2015]

La Mesa de la Mujer de Medellín presentó este miércoles 22 de julio la 5ta Agenda de Política de las Mujeres, con el objetivo incidir en la agenda de los candidatos a la alcaldía de esta ciudad.

En 20 años, la Mesa y las organizaciones que la conforman han buscado plasmar las necesidades de las mujeres de la ciudad antioqueña, con el objetivo de que puedan traducirse en programas puntuales de gobierno que las favorezcan. Es esta oportunidad, la construcción de la agenda tomó 8 meses de reuniones y asambleas con la participación de lideresas de las comunas de Medellín y mujeres de organizaciones de base, en una apuesta por interlocutar con el Estado.

Entre los temas que más se evidenciaron en la construcción de esta Agenda se encuentra la necesidad de abordar el conflicto social y armado y sus expresiones locales, que afectan a las mujeres por su relación con los hombres que hacen parte de los actores políticos, sociales y armados de los territorios; pero también la violencia intrafamiliar y los feminicidios, la falta de acceso a la vivienda como propietarias; el acceso a servicios públicos; el empleo, la participación política.

<div>

Las mujeres organizadas en cooperativas, redes y colectivos de base que vienen documentando estas problemáticas y trabajando en su resolución, instan a las autoridades locales a escuchar sus demandas y satisfacer las necesidades de la población, como es su obligación.

</div>
