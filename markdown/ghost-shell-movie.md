Title: Ghost In The Shell: blanqueamiento de personajes y una adaptación poco ambiciosa
Date: 2017-05-02 08:39
Author: AdminContagio
Category: 24 Cuadros
Tags: anime, Cine, Ghost in the shell
Slug: ghost-shell-movie
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Ghost-In-The-Shell-Imagen-Collage.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Paramount Pictures 

##### Por: Jake Sebastián Estrada Charris 

Ghost In The Shell es un manga del género ciberpunk creado por Masamune Shirow en 1989. La obra es una demostración de maestría argumentativa y filosófica. Tanto su versión original como las 2 películas dirigidas por Mamoru Oshii en 1995 y 2004, se preocupan por plantear cuestiones tan interesantes como las raíces de la identidad humana, la sensibilidad ante la individualización del yo en un mundo de masas y la lejanía con las nociones más básicas de fraternidad, sin contar con que son joyas dentro de la industria de la animación. La versión hollywoodense estrenada en 2017, dirigida por Rupert Sanders y protagonizada por Scarlett Johansson no es un trabajo mediocre, pero carece de la fuerza de una historia tan rica que ha influenciado e impresionado a toda una generación.

Con todo y las críticas sobre la decisión de que Scarlett Johansson encarnara a la Mayor Motoko Kusanagi, la actriz hace un papel sólido y su parecido físico es innegable. Destaca también el trabajo del siempre brillante Takeshi Kitano como Daisuke Aramaki junto con la participación de Pilou Asbæk como Batou. Aunque todo ello es rescatable, hay que decir que el uso de actores blancos para una historia ambientada en Japón es realmente exagerado y los roles de personajes asiáticos se ven reducidos al mínimo. Este ejercicio se conoce como ‘whitewashing’ y consiste en omitir la diversidad étnica de las historias y otorgar papeles a intérpretes de rasgos netamente occidentales.

Algo similar ocurrirá con una de las adaptaciones del ánime Death Note dirigida por Adam Wingard y distribuida por el servicio de streaming Netflix, que modificará aspectos como la nacionalidad de sus protagonistas, nombres, además de la ciudad en donde está ambientada (de Tokio a Seattle). Por mencionar otro título, también hemos visto a Emma Stone personificando a una hawaiana nativa en la película ‘Aloha’, teniendo un vasto número de críticas negativas.

¿Funciona esa estrategia de modificar argumentos, blanquear una cantidad significativa de personajes y omitir los aspectos que hacen diferenciadora a una obra? Si estuviese en manos de Ghost In The Shell de Rupert Sanders y en tantos otros títulos que intentaron hacer el mismo ejercicio, la respuesta sería no.

![ghost in the shell](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/unnamed-6.jpg){.wp-image-39773 .aligncenter width="549" height="309"}

Y es que a pesar de que las comparaciones siempre vayan a ser odiosas, resulta inevitable no hacerlo con los magníficos títulos de Japón y la cinta estadounidense, puesto que se está realizando un trabajo de adaptación. A eso se arriesga una película inspirada en libros, obras de teatro, cuentos, etc.

Antes que nada, hay que decir que la versión de 2017 de Ghost In The Shell no es perversa; es por eso que no puede compararse con largometrajes desastrosos como Dragon Ball: Evolution de James Wong (2009). Uno de sus aspectos más destacados son los efectos visuales y la adaptación de la ciudad de Tokio, en donde además de respetar varios de los escenarios originales, pueden notarse alusiones al universo visual de Blade Runner de Ridley Scott (1982), influencia que han tomado decenas de películas entre las que se encuentran títulos como la trilogía del Caballero de La Noche de Cristopher Nolan.

Gracias a sus postulados filosóficos trasladados a las coyunturas del presente y a su estética futurista que goza de una fotografía muy bien lograda, Ghost In The Shell ha sido un referente para cintas como The Matrix de las hermanas Wachowski (1999). En la cinta estelarizada por Scarlett Johansson, desaparecen importantes hilos argumentales y propuestas creativas de la obra original. Es decir, la mayoría de los planteamientos se reducen hasta el punto de trivializar la película.

Se nota que el equipo creativo de la película hollywoodense captó la idea de la obra original y quiso apropiarse de la historia; además de ello, todo fanático de Ghost In The Shell podrá notar varios homenajes, guiños y referencias que se hacen, sobre todo al largometraje de 1995. Le puede interesar: [Con Francia como invitado de honor inicia Eurocine 2017](https://archivo.contagioradio.com/eurocine-bogota-2017/)

<iframe src="https://www.youtube.com/embed/G4VmJcZR0Yg" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

No obstante, ese ejercicio se queda en lo anecdótico, tal vez por falta de ambición o de coraje por mostrar los aspectos que verdaderamente hacen atractiva a Ghost In The Shell. Esto dejó de hacerse presumiblemente con la intención de querer atrapar infructuosamente al público occidental. Por ese motivo, la película es lenta para un consumidor compulsivo de blockbusters y banal para los fans de la franquicia que cuenta incluso con series de TV y animaciones originales, también conocidas como OVAS.
