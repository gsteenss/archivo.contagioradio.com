Title: ¿Qué representa la nominación de "El abrazo de la Serpiente" para el cine colombiano?
Date: 2016-01-17 10:04
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Cinemateca Distrital de Bogotá, el abrazo de la serpiente, Premios Oscar
Slug: que-representa-la-nominacion-de-el-abrazo-de-la-serpiente-para-el-cine-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/1-Foto-de-Andres-Cordoba-e1453042106879.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Andrés Córdoba 

<iframe src="http://www.ivoox.com/player_ek_10100870_2_1.html?data=kpWekpWce5Ghhpywj5aZaZS1kZuah5yncZOhhpywj5WRaZi3jpWah5ynca3jjNbix5DWqdHmxtjS0NnFb83VjNPcz87SpcTdhqigh6eXsozYxpCSlJepsIzVw9fO3NSPqMafzcaYtcqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Julián David Correa, Cinemateca Distrital] 

##### [16 Ene 2016]

Los pronósticos optimistas que apostaban por una nominación para la película "El Abrazo de la Serpiente" fueron acertados y por primera vez en la historia del cine, un film colombiano ha sido escogido como uno de los 5 que competirán el próximo mes de febrero por llevarse el Oscar, en la categoría que premia a las mejores producciones en lengua no inglesa.

Para Ciro Guerra, director de la cinta, la tercera resulto ser la vencida. Tanto su ópera prima “La sombra del caminante” como su segundo largometraje “Los viajes del viento” fueron enviados a la Academia para su consideración, ninguna de los cuales lograría pasar a preselección como si su más reciente producción, razón para pensar que en las cosas podían ser diferentes.

Tan pronto fue presentada encabezando la baraja de producciones provenientes de diferentes lugares del mundo, la alegría nacional desbordó por el orgullo de tener una película colombiana, única latinoamericana en competencia, ocupando los encabezados de los grandes medios de comunicación y convirtiéndose en tema de conversación durante la agenda del día y un par de los siguientes.

Consecuentemente y como si se tratará de un partido de fútbol o un reinado de belleza, muchos de los espectadores también celebraron como propio el triunfo, incluso aquellos que difícilmente han visto ésta (120.000 al anuncio de la nominación) o alguna película colombiana, quienes desconocen por completo lo que representa la elección de la cinta para la producción nacional.

**¿Por qué ahora sí y antes no?**

Para Julián David Correa, director de la Cinemateca Distrital de Bogotá, lo que está pasando con el cine colombiano “no es una tontería” y “hace parte de un proceso que se viene desarrollando desde hace mucho tiempo”, resultados que son consecuencia tanto del gran talento del equipo humano como de la expedición de las leyes de cultura de 1997 y del cine de 2003, y la creación de instituciones que han favorecido el desarrollo audiovisual en el país.

El contar con respaldo presupuestal para desarrollar proyectos cinematográficos, ha permitido un crecimiento del cine nacional, no sólo en la cantidad de películas que se estrenan en el año, sino en la calidad de las producciones que Correa explica con el símil del trabajo de un zapatero que requiere “hacer más de un par de zapatos” para que estos salgan cada vez mejor. Lo que ha permitido que películas nacionales reciban reconocimientos en escenarios tan importantes como Cannes o Berlín.

La importancia de la inversión estatal, que va más allá del gobierno de turno, sumada a la posibilidad que los jóvenes realizadores encuentren escuelas de cine en el país para iniciar su formación académica y a los avances que permiten tener la tecnología al alcance de la mano para realizar producción audiovisual, ha favorecido que esta nueva camada de cineastas aporten a la búsqueda de una identidad del cine nacional que permita que su propio público se identifique con él.

**¿Que pasa con el público?.**

La diversidad de historias producidas desde diferentes narrativas, las nominaciones y los premios, contrasta con la baja asistencia de público que asisten a las salas a ver cine nacional, situación que el Director de la Cinemateca cree se verá favorecida al ser los Oscar “una vitrina para vender el cine”, que motive a los espectadores a nivel local, consiga que más distribuidores internacionales se interesen por mostrar el cine colombiano en el mundo y favorezca a la inversión a las producciones por parte de la empresa privada.

La asistencia de espectadores a ver cine colombiano, pasa además por el tema de la distribución local que es determinada por empresas privadas y como tal obedece a la lógica del negocio: lo que es rentable se mantiene lo que no sale rápidamente de cartelera, situación ante la cual debe “buscarse una manera que el cine colombiano reciba más dinero para el mercadeo” que haga contrapeso a dominio que en la materia tiene el cine de las grandes productoras internacionales.

Correa considera que además es necesaria la inversión en la “formación de públicos que nos enseñe a ver nuestro propio cine y los otros cines del mundo”, que muchas veces no son fáciles de digerir por las estéticas y temáticas que en estos se trabajan, señalando que “no podemos permitir que las únicas películas que son importantes en nuestra vida sean las que nos digan que debemos ser parecidos a los gringos pero no nos hacen preguntarnos sobre nuestra propia identidad y desarrollo”.

<iframe src="https://www.youtube.com/embed/uYteDz0CUi0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

###### [Nominados para la edición 88 de los Premios Oscar] 

Aunque para Julián David Correa el Oscar, que tendrán lugar el próximo 28 de febrero, no es “un premio a la calidad, no es un premio a la más grande obra de arte”, considera que Ciro Guerra merece obtenerlo, "así como los 10 premios que ya se ganó", enfrentándose a 4 de las películas más importantes del año: Mustang (Francia), Son of Saúl (Hungría), Theeb (Jordanía) y A War (Dinamarca).
