Title: ZIDRES legalizaría mecanismos de acumulación de tierras
Date: 2015-12-15 11:59
Category: Ambiente, Nacional
Tags: Acumulacion de tierras, Zidres
Slug: zidres-legalizaria-mecanismos-de-acumulacion-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/uno255.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: las2orillas.com 

###### [15 Dic 2015] 

En plenaria del Senado de la República, continúa la discusión del proyecto de Ley sobre Zonas de Interés de Desarrollo Rural, Económico y Social, Zidres, que según los opositores a esta Ley, tendría como **objetivo legalizaría mecanismos de acumulación de tierras.**

Este lunes cuando se presentó la propuesta, **se presentaron 25 impedimentos,** de manera que se debió aplazar  la discusión y votación del proyecto, hasta que exista una subcomisión que analice cada uno de los impedimentos expuestos por los congresistas.

Pese a que el gobierno nacional asegura que con esta Ley se generaría una mayor productividad en las tierras de algunas zonas del país, para el movimiento social agrario, se trata de un proyecto que beneficia a empresarios y no a los campesinos, teniendo en cuenta que “**las Zidres es el sexto  intento del gobierno del presidente Santos, para que los baldíos puedan ser adquiridos por latifundistas, testaferros y empresarios, con el supuesto de adelantar grandes iniciativas de agroindustria, de manera que se estaría buscando legalizar la adquisición ilegal de baldíos",** explica César Jeréz, vocero de la Asociación Nacional de Zonas de Reserva Campesina, ANZORC.

Jeréz, asegura que a través de las organizaciones sociales y algunos congresistas se está dando a conocer la realidad de esta Ley con la que se pretende e**xtranjerizar la tierra y continuar con la corrupción**. Además, según se trata de una **“propuesta del gobierno que es clientelista”,** al ser uno de los compromisos electorales que hizo Santos para beneficiar a los empresarios, y en cambio se estaría perjudicando uno de los  acuerdos de paz de La Habana donde se considera entregar baldíos a los campesinos.

A su vez, el lunes también el senador del Polo Democrátrico Alternativo, Alberto Castilla, denunció **la acumulación irregular de baldíos en Puerto Gaitán, Meta, por parte de la petrolera Pacific Rubiales,** que pretende ser legalizada a través de la aprobación de las ZIDRES en regiones como la Altillanura en las que otras empresas como Río Paila estarían acaparando un total de 47 mil hectáreas, como lo registró un informe de la Contraloría del 2014.

Según el senador, esta estrategia implementada por Pacific para acumular tierras ha sido la constitución de fiducias a las que ha entregado predios para administrar bajo la figura de fideicomisos, burlando la legislación colombiana puesto que por ley sólo es posible obtener una Unidad Agrícola Familiar en materia de  reforma agraria. (Lea también...)

Así mismo, otras organizaciones como la Oxfam, han estado en contra de este proyecto de Ley, pues lo señalan como una iniciativa que atenta contra los derechos de los campesinos, campesinas y la pequeña producción agraria **aumentando la desigualdad que actualmente padece Colombia.**
