Title: Opiniones encontradas a nivel mundial luego de Atentado contra Charlie Hebdo
Date: 2015-01-15 18:18
Author: CtgAdm
Category: El mundo
Tags: protesta
Slug: opiniones-encontradas-a-nivel-mundial-luego-de-atentado-contra-charlie-hebdo
Status: published

###### Foto: AFP 

Las consecuencias del atentado han provocado un torrente de opiniones de rechazo, a favor de la revista satírica y de la libertad de expresión.

Históricas marchas se están sucediendo a favor de la libertad de expresión y en contra de los atentados. Pero una de ellas consiguió marcar un referente entre opiniones encontradas en las redes sociales. Esta fue la de diversos presidentes en Francia el pasado 11 de Enero en la que participaron  Sarkozy o Netanyahu entre otros, a los que mayoritariamente en las redes sociales se les acusa de perpetrar distintas masacres contra países del mundo islámico en su participación en invasiones militares como la de Libia o Mali.

Pero también han reabierto la caja de pandora en Europa. Opiniones que apuntan hacia la islamofobia, el creciente ascenso de la ultraderecha o el apoyo por parte de EEUU y Francia a islamistas radicales o a invasiones militares como la de Siria o Libia.

Por otro lado más de 19.000 webs han sido atacadas por islamistas radicales en contra de Charlie Hebdo y a favor de los ataques.

Al mismo tiempo  que Al Qaeda reivindicaba el atentado a través de un video cientos de musulmanes dejaban verter sus opiniones en contra de los atentados y a favor de la integración.

Por último diversos analistas de medios apuntan hacia una utilización de carácter alarmista de los sucedido e indican las posibles consecuencias que pueda tener para la convivencia entre grupos sociales y étnicos en la propia Europa.
