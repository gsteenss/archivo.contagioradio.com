Title: ¿La inteligencia militar al servicio del uribismo?
Date: 2020-01-15 12:56
Author: CtgAdm
Category: Entrevistas, Nacional
Slug: la-inteligencia-militar-al-servicio-del-uribismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/uribismo-e1579110008250.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:group -->

<div class="wp-block-group">

<div class="wp-block-group__inner-container">

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->
</p>
###### Foto: @GuillermoBotero {#foto-guillermobotero .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito de la investigación de la Revista Semana, que reveló el sistema de interceptación de comunicaciones usado por el Ejército para vigilar las conversaciones de políticos, periodistas y organizaciones defensoras de derechos humanos el senador Antonio Sanguino, una de las personas objeto de estas interceptaciones, afirmó que eran una muestra del regreso del uribismo al poder.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El retorno del uribismo al poder, el regreso de sus peores prácticas

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sanguino expresó que la denuncia de Semana era una noticia grave para la democracia, la institucionalidad del país y para el ejercicio de la oposición política. Al tiempo, sostuvo que el acontecimiento era la confirmación de que **"con el retorno del Uribismo al poder, volvieron las peores prácticas de esta fuerza política**, que son precisamente las chuzadas y los falsos positivos".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sanguino agregó que lo que se ha conocido es más grave incluso que la misma interceptación, pues además se ha abusado del uso de tecnología donada por otros países para labores de inteligencia, se ha sabido la alianza de oficiales del Ejército con criminales y se ha conocido que dirigentes del Centro Democrático son receptores de la información que se obtenía de forma ilegal.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además, señaló que estas actividades eran conocidas por el Alto Mando Militar, y que se fraguaron operaciones de contrainteligencia para desviar la atención, cuando él y los también senadores Iván Cepeda y Roy Barreras denunciaron presuntos seguimientos en su contra. (Le puede interesar:["Dirección Nacional de Inteligencia estaría detrás de falsos positivos judiciales contra oposición"](https://archivo.contagioradio.com/direccion-nacional-de-inteligencia-contra-oposicion/))

<p>
<!-- /wp:paragraph -->

</div>

</div>

<!-- /wp:group -->

<!-- wp:heading {"level":3} -->

### **Hace mejor labor de investigación un medio de comunicación, que la Fiscalía**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En julio de 2019 los tres senadores denunciaron, a través de una carta al Presidente, que estarían siendo víctimas de seguimientos ordenados por el director de la Dirección Nacional de Inteligencia (DNI) con la intención de hacer falsos positivos judiciales. Sanguino recordó que **luego de esta carta, tanto Presidencia como la DNI negaron lo ocurrido**, y la Fiscalía cerró luego de dos meses la investigación que había abierto sobre el tema.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En cambio, Sanguino rescató la labor del semanario en continuar la investigación hasta llegar a conocerse que dichas interceptaciones sí tenían lugar, que ocurrían contra varios personajes de la vida pública y eran realizadas desde guarniciones militares. (Le puede interesara:["Ejército realizó "chuzadas" a congresistas, jueces, periodistas y defensores de DD.HH."](https://archivo.contagioradio.com/ejercito-realizo-chuzadas-a-congresistas-jueces-periodistas-y-defensores-de-dd-hh/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este hecho, así como el asesinato constante de líderes sociales y excombatientes, para el Senador **ponen en entredicho la credibilidad de las Fuerzas Militares y de la propia Fiscalía**, que son los entes encargados de garantizar que se respeten los derechos de los y las ciudadanas. (Le puede interesar:["Las peores prácticas de inteligencia militar nunca se fueron"](https://archivo.contagioradio.com/las-peores-practicas-de-inteligencia-militar-nunca-se-fueron/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Cómo abordar la situación?**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Sanguino aseguró que era una 'falta de seriedad' que el Gobierno señale que no sabía de las interceptaciones, cuando existe la prueba de la carta que enviaron junto a Barreras y Cepeda advirtiendo sobre esta situación. En consecuencia, espera **que se investigue debidamente el hecho,** en lugar de acudir al desconocimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, manifestó que debería conocerse los nombres de las personas que estaban siendo víctimas de seguimientos ilegales, así como que dichos seguimientos tendrían que ser de conocimiento de la Comisión de Inteligencia y Contrainteligencia del Congreso, porque lo ocurrido "son hechos que corresponden a un régimen dictatorial".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

También, solicitó saber qué integrante del Centro Democrático recibía la información de las interceptaciones, pues hace parte de "un escenario tenebroso de lo que se estaba fraguando en el alto mando militar". (Le puede interesar: ["Congresistas denuncian que DEA violó la soberanía colombiana en caso Santrich"](https://archivo.contagioradio.com/denuncian-dea-violo-soberania-santrich/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por último, el [Senador](https://twitter.com/AntonioSanguino/status/1216801132292595717) reconoció que **están evaluando acudir a instancias internacionales, como el Sistema Interamericano de Derechos Humanos,** para que se proteja su derecho al ejercicio de la política sin riesgos a su intimidad, o seguridad.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46597699" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46597699_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
