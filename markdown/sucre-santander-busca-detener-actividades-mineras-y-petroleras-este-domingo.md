Title: Sucre, Santander busca detener actividades mineras y petroleras este domingo
Date: 2017-09-28 15:39
Category: Ambiente, Nacional
Tags: consulta popular, Sucre
Slug: sucre-santander-busca-detener-actividades-mineras-y-petroleras-este-domingo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/mapiot.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mapiot.net] 

###### [28 Sept 2017] 

El departamento de Santander busca blindarse frente a los proyectos minero-energéticos. Por lo menos así lo están buscando algunos de sus municipios, a los que se une Sucre, que este primero de octubre busca decirle no a la minería y al petróleo.

**"¿Está de acuerdo, sí o no, que en la jurisdicción del municipio de Sucre, Santander, se adelanten actividades de explotación minera y petrolera?"**, es la pregunta que podrán responder este domingo los 6 mil pobladores que están habilitados para votar. (Le puede interesar: ["Gobierno quiere asfixiar consultas populares: Iván Cepeda"](https://archivo.contagioradio.com/gobierno_consultas_populares_ivan_cepeda/))

De acuerdo con Mauricio Mesa, integrante de la Corporación Compromiso, son 22 títulos mineros los que amenazan el territorio. Para extracción minera de carbón hay 18 títulos, 4 para extracción de rocas calizas para empresas cementeras extranjeras como Argos y Cemex. **Para Mesa, la minería es una actividad que puede poner en grave riesgo los acueductos comunitarios de la zona.**

Además las comunidades se encuentran preocupadas por la posible extracción petrolera mediante la técnica de fracking, teniendo en cuenta que la parte baja del municipio de Sucre hace parte del Magdalena Medio, y por tanto estaría destinado para fracturamiento hidráulico, según lo han envidenciado los mapas de bloques petroleras publicados por la Agencia Nacional de Hidrocarburos, **donde se puede ver las zonas destinadas para explotar crudo por métodos no convencionales.**

### **¿Qué defienden?**

Con la consulta popular lo que los habitantes quieren defender son las actividades económicas con las que se han venido sosteniendo. Entre ellas, el ambientalista, destaca la actividad ganadera de la cual está naciendo un nuevo proyecto productivo alrededor de una línea láctea. Asimismo, **destaca el cultivo de mora que luego es distribuido a distintas partes del país, entre ellas a Corabastos, en Bogotá.**

Los habitantes esperan que el Gobierno Nacional respete la decisión de la comunidad de Sucre, y no se repita la situación que ya acontece en el municipio de Jesús María donde la población ya dijo no a los proyectos mineros, pero el Ministerio de Minas ya interpuso una acción de tutela contra el resultado de dicha consulta. (Le puede interesar: ["Con más del 90% de votoas, habitantes de Jesús María, en Santander, le dicen no a la minería y el petroleo"](https://archivo.contagioradio.com/con-mas-del-90-habitantes-de-jesus-maria-le-dicen-no-a-la-mineria-y-el-petroleo/))

<iframe id="audio_21158751" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21158751_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
