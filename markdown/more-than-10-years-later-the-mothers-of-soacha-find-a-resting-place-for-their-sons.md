Title: More than 10 years later, Mothers of Soacha find resting place for sons
Date: 2019-05-29 16:45
Author: CtgAdm
Category: English
Tags: Extrajudicial executions, Soacha Mothers
Slug: more-than-10-years-later-the-mothers-of-soacha-find-a-resting-place-for-their-sons
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/madres_soacha-camila_ramirez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

The Mothers of Soacha have found a final resting place for their sons who were extrajudicially killed in 2008 by military forces. On May 16, the military handed over the titles of plots in Soacha, a suburb of Bogotá, where the family members plan to build a  mausoleum.

For the family members of the victims, this new arrangement means they will no longer have to pay expensive leases in private and public cementeries. Pilar Castillo, one of the lawyers representing the victims, explained that once the bodies were found in Ocaña, in the Norte de Santander region, they were taken to Bogotá and buried in cementaries all over the city, Campos de Cristo, El Apogeo,  respectively in Chapinero and Jardines de Bosa.

These bodies could not be exhumed because they were part of a judicial investigation, but this forced the family members of the victims to pay leases in cementaries every four years and rental fees that were every time more difficult to cover. According to Castillo, the cementary even threatened to evict the bodies and to bury them in a mass grave if they failed to pay.

### Military donated plots for mausoleum 

For this reason, the Mothers of Soacha started a search for a permanent grave for their loved ones. In 2017, General José Alberto Mejía, commander of the Military Forces, pledged to finance the construction of the mausoleum. The blueprint was designed and approved by the family members, but it was difficult to find a plot for the mausoleum.

The lawyer claimed that the budget was settled that same year and the Military said the construction would take four months to complete. Two years later, the parrish priest of Soacha donated plots of land for the mausoleum. (Related: "[Academics around the world ask Duque for serious human rights policy")](https://archivo.contagioradio.com/more-than-250-academics-criticize-duque-for-violence-against-activists/)

The mothers will have to discuss the construction issue again with the new commander of the Military Forces, General Luis Fernando Navarro, but the lawyer argued that she does not expect any difficulties since the decision to finance the mausoleum came directly from the Ministry of Defense. Castillo added that the construction of the mauseoleum is an important step for the mothers who have struggled to obtain a dignified place to honor the memory of their sons.
