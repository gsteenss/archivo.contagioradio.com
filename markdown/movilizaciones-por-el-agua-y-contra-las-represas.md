Title: La movida es por el Agua
Date: 2016-03-14 12:15
Category: Movilización, Nacional
Slug: movilizaciones-por-el-agua-y-contra-las-represas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Movilización-Hidroituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notiagen] 

<iframe src="http://co.ivoox.com/es/player_ek_10796213_2_1.html?data=kpWkm5uWdZShhpywj5WZaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncbbiwpDa0dvNsMruwsjWh6iXaaOnz5DS0JDIqcfZz9jOjcnJsIzVyNrOjd6Pp9Di1dfOjdHFt4zcysnfj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Isabel Cristina Zuleta, Ríos vivos] 

###### [14 Mar 2016]

En el marco del ‘Día Internacional de Acción Contra las Presas y por los Ríos, el Agua y la Vida’ organizaciones sociales, movimientos ambientalistas y delegaciones de municipios afectados por la implementación de megaproyectos hidroeléctricos se movilizan en Huila, Antioquia y Santander en defensa del agua y los territorios y para explicar a la sociedad los impactos negativos ambientales y sociales de la construcción de hidroeléctricas.

“Sabemos que vinieron por el agua, sabemos que a los pobres nos van a quitar el agua y que está siendo seleccionada exclusivamente para los ricos, no solamente de Colombia sino de todo el continente”, asevera Isabel Cristina Zuleta integrante del ‘Movimiento Ríos Vivos’ que impulsa esta jornada de movilización en distintas regiones del país.

Se espera que sean mil personas las que se movilicen en Antioquia, entre pobladores de municipios afectados por el proyecto hidroeléctrico Hidroituango, como Briceño, Ituango, Valdivia, Toledo y Caucasia, y la ciudadanía en general que desde tempranas horas se concentra en el Parque de Los Deseos de Medellín y quienes en la tarde se movilizarán desde el Parque Botero hasta la Alpujarra, según afirma Zuleta.

A las demandas contra la implementación de hidroeléctricas, se suma el llamado de los habitantes de la Comuna 8 de Medellín quienes desde hace un mes padecen racionamiento de agua debido a una determinación por parte de EPM, y que según afirma Zuleta es una acción selectiva que afecta a los barrios más empobrecidos de la capital antioqueña, “una crisis por el agua y una discriminación en términos de clases sociales, que determina quién es el que debe quedarse con el agua”.

En Huila los pobladores afectados con la construcción de la represa El Quimbo planearon cuatro puntos de concentración en municipios como Pitalito, Garzón y Altamira en los que se estima que más de setecientas personas participen de la movilización. Mientras que este domingo jóvenes y adultos participaron de la maratón por el río Sogamoso en defensa del agua y en contra del proyecto Hidrosogamoso y de la eventual construcción de la hidroeléctrica Piedras del sol, un encuentro nutrido que las fuerzas militares trataron de entorpecer.

En este día también se movilizan organizaciones como 'Ecologistas en Acción' quienes han exigido que se suspendan en todo el mundo los proyectos hidroeléctricos debido a que los 50 mil embalses que actualmente funcionan en el mundo han provocado inundación de valles con gran valor ecológico, desplazamiento forzado, pérdida de patrimonio natural y cultural, y la represión, criminalización y muerte de quienes se han opuesto a estos proyectos que han buscado beneficiar intereses empresariales afectando a comunidades ancestrales.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
