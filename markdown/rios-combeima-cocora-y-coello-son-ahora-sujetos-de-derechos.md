Title: Ríos Combeima, Cocora y Coello son ahora sujetos de derechos
Date: 2019-06-07 15:29
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Anglogold Ashanti, Tolima, Tribunal Administrativo del Tolima
Slug: rios-combeima-cocora-y-coello-son-ahora-sujetos-de-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Captura-de-pantalla-121.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gacela por el Mundo] 

El Tribunal Administrativo del Tolima ordenó la anulación de títulos otorgados para la exploración y explotación minera sobre las cuencas de los ríos Combeima, Cocora y Coello, ubicadas en el oriente del departamento del Tolima. **A la misma vez, este fallo histórico reconoció a estos tres ríos como sujetos de derechos**.

["Saludamos el fallo que acaba de hacer el Tribunal Administrativo, en medida de que da pasos muy fuertes, casi en sintonía, con lo que hizo la Corte Constitucional cuando le dio también derechos al Río Atrato y a la Amazonía", afirmó Renzo García, integrante del Comité Ambiental en defensa de la vida. (Le puede interesar: "][El precedente que marca el fallo sobre la Amazonía colombiana](https://archivo.contagioradio.com/fallo_historico_corte_suprema_amazonas/)")

El Tribunal le dio la razón al personero municipal de Ibagué, Isaac Vargas  Morales, quien interpuso una acción popular en 2011, argumentando que los 23 títulos concesionados a las empresas AngloGold Ashanti, hoy Kedahda, así como la firma Oro Barracuda Ltda y Continental Gold, deberían ser anuladas al vulnerar **los derechos de los ciudadanos a un ambiente sano y un espacio público libre de contaminación**.

Según García, estas títulos se superponían sobre las bocatomas del acueducto principal de Ibagué y sobre dichos ríos, poniendo en riesgo la disponibilidad hídrica para el consumo doméstico, actividades agrícolas y uso industrial, entre otros. Cabe resaltar que la cuenca mayor del Río Coello abastece el 6% de todo el agua del Tolima y beneficia más de la mitad de los pobladores del departamento. (Le puede interesar: "[Ciudadanos piden anulación de títulos mineros otorgados en Cajamarca](https://archivo.contagioradio.com/ciudadanos-piden-anulacion-de-titulos-mineros-en-cajarmarca/)")

Después de ocho años de estudio de esta situación, el tribunal regional le solicitó a la Universidad del Tolima de realizar un estudio sobre el impacto ambiental y social de la realización de la minería en esta zona. Además, ordenó a las autoridades no tramitar más concesiones sobre las cuencas mencionadas.

"Nosotros creemos que esta decisión le da la posibilidad a esta sociedad de comenzar a construir una sociedad biocéntrica, una visión que articule el beneficio de los seres humanos pero que también reconozca que ese **bienestar de los humanos depende de la calidad de los ecosistemas**", afirmó.

<iframe id="audio_36818168" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36818168_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
