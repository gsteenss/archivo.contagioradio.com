Title: 'Ni con el pétalo de una rosa' una apuesta para la paz desde las mujeres
Date: 2016-11-16 16:24
Category: Mujer, Nacional
Tags: arte para la paz, construcción de paz, mujeres constructoras de paz, Ni con el pétalo de una rosa
Slug: ni-con-el-petalo-de-una-rosa-una-apuesta-para-la-paz-desde-las-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Penal-de-Ocaña-España.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Casa E Social] 

###### [16 Nov 2016] 

El próximo 20 de Noviembre se inaugura el III Festival Internacional 'Ni con el pétalo de una rosa', como **respuesta al aumento de casos de violencias hacia la mujer en Colombia y en general, al auge de los feminicidios en América Latina**, una apuesta desde el arte como alternativa para la construcción de Paz y la erradicación de las violencias contra las mujeres.

La programación que se extenderá hasta el 27 de Noviembre incluye teatro, tomas de ciudad, flashmob, conciertos, microteatro, ciclovía por las mujeres, conversatorios, talleres y ecoyoga. Serán **más de 100 las actividades que buscan abrir un espacio de encuentro en torno a la paz y la no violencia hacia las mujeres y las niñas.**

### **¿Cuál es la apuesta?** 

Este festival que cumple 3 años reúne diferentes muestras y apuestas de mujeres de otros países, **el invitado especial de este año es México**, uno de los países con más violencia contra las mujeres en el cono sur, y **en el que muchas mujeres y jóvenes están construyendo otras formas de vida sin violencias** desde el arte y la pedagogía.

Tatiana Zabala integrante de Casa Ensamble, fundación que organiza el evento, comenta que el arte **"es una herramienta fundamental para sanar las heridas que ha dejado el conflicto armado colombiano".**

‘Por un país en Paz con las mujeres’ es el lema que lleva esta tercera edición del Festival que mostrará al público dos obras estrechamente relacionadas con la reconciliación y la sanación de las mujeres que han sufrido la guerra, la primera es ‘**Victus’ que se presenta en el teatro Jorge Eliécer Gaitán con entrada gratuita** y el 25 de Noviembre día internacional por la erradicación de la violencia contra las mujeres habrá un **gran performance de mujeres frente al Congreso de la República.**

Por último, Tatiana extiende la invitación a que hombres y mujeres participen de las actividades y sigan la programación a través de **redes sociales y el [portal web de Casa Ensamble.](http://casaesocial.com/festival/)**

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
