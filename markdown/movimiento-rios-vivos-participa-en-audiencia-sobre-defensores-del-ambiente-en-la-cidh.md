Title: Movimiento Rios Vivos participa en audiencia sobre defensores del ambiente en la CIDH
Date: 2015-10-19 16:56
Category: Nacional, yoreporto
Tags: CIDH, Defensores de los DDHH, Hidroituango, Movimiento Ríos Vivos, Proyecto hidroeléctrico El Quimbo
Slug: movimiento-rios-vivos-participa-en-audiencia-sobre-defensores-del-ambiente-en-la-cidh
Status: published

###### Foto: Rios Vivos 

###### [19 Oct 2015] 

El Movimiento en defensa de los territorios y afectados por ***Represas Ríos Vivos Colombia*** participa hoy 19 de octubre de 2015 en la Audiencia de las Américas titulada “*Situación de derechos humanos de defensores y defensoras del medio ambiente en el contexto de las industrias extractivas en América” *que se realizará a las 6:45 hora de Washington y 5:45 hora Colombiana ante la Comisión Interamericana de Derechos Humanos, Salón Rubén Darío

La audiencia destacará los riesgos que corremos las organizaciones y personas que nos atrevemos a defender nuestro territorios, la tierra, las aguas, el derecho a un ambiente sano, enfrentándonos de manera pacifica a los intereses de las empresas que con sus proyectos minero- energéticos principalmente destruyen la naturaleza.

**La audiencia se puede seguir en: <u><http://www.oas.org/es/cidh/default.asp>  </u>ó<u><http://original.livestream.com/OASLive2></u>**
