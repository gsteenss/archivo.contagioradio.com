Title: FARC denuncia que hay 4 guerrilleros muertos desde el inicio del Cese Unilateral en el mes de Julio
Date: 2015-11-03 18:35
Category: Nacional, Paz
Tags: Carlos Antonio Lozada, cese unilateral de las FARC, Constituyentes por la Paz, Conversaciones de paz de la habana, DIPAZ, FARC, Frente Amplio por la PAz, Juan Manuel Santos
Slug: farc-denuncia-que-hay-4-guerrilleros-muertos-desde-el-inicio-del-cese-unilateral-en-el-mes-de-julio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/carlos_antonio_lozada_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pulzo 

###### [3 Nov 2015]

Al iniciarse el ciclo 43 de las conversaciones de paz la delegación de paz de la guerrilla de las FARC denunció una serie de acciones de **copamiento militar** en varias regiones del país. Además afirmaron que los 6 meses para la firma del acuerdo final contarían a partir de la firma definitiva del acuerdo sobre Jurisdicción Especial para la Paz que el gobierno firmó y luego exigió que se siguiera discutiendo.

Carlos Antonio Lozada, integrante de las FARC saludó el respaldo de los gremios económicos al proceso de conversaciones y afirmó “que el logro de la paz requiere del concurso de todos los colombianos sin distingos de ningún tipo” y afirmaron que es “especialmente significativo” el acuerdo expresado en algunas preguntas planteadas por dicho gremio.

Además reiteraron que insisten en la necesidad de refrendar los acuerdos a través de una Asamblea Nacional Constituyente, de esa manera hicieron un balance sobre la reciente visita de delegados del parlamento colombiano, “les expresamos que seguimos creyendo firmemente en que debe ser una asamblea nacional constituyente la que cumpla el papel de refrendar los acuerdos pero además dirimir las salvedades” afirmó Lozada.
