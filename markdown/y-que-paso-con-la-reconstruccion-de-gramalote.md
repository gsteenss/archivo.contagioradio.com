Title: ¿Y qué pasó con la reconstrucción de Gramalote?
Date: 2015-09-21 14:27
Category: Comunidad, Entrevistas
Tags: cambio climatico, Deslizamientos en Gramalote, Desplazamientos por causas climáticas, Emergencias invernales en 2010, Fenómeno de La Niña, Fondo de Adaptación al cambio climático
Slug: y-que-paso-con-la-reconstruccion-de-gramalote
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Gramalote.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: panorama-fronterizo.blogspot.com] 

<iframe src="http://www.ivoox.com/player_ek_8513774_2_1.html?data=mZqelZybeI6ZmKiak5qJd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhp2g25DVuYa3lIqlm5DUpdSZpJiSm5iPp9DijNHOjdfJp9Di1Nnf18jHrYa3lIqmldOPqMafyNfOz8bQs9XZhpizj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carlos Arenas, Displasament Solutions] 

###### [21 Sept 2015] 

[Según el último informe de Displasament Solutions DS, "Desplazamiento Climático y Relocalización Planificada en Colombia: el caso de Gramalote", **las cerca de 3.500 personas que fueron trasladadas a municipios cercanos tras los deslizamientos de tierra que destruyeron Gramalote en 2010, han permanecido desplazadas por casi cinco años y se estima que hasta 2016 puedan retornar a su lugar de origen**.  ]

[Tras el exceso en los niveles de lluvia, como efecto del fenómeno de “La Niña”, se originaron entre el 16 y 17 de diciembre de 2010 en Gramalote, remociones en masa que obligaron a la evacuación del total de su población, con la que **el gobierno nacional se comprometió a adelantar un proceso de relocalización planificado que les permitiera meses después retornar en condiciones dignas**.]

[Sin embargo, de acuerdo con Carlos Arenas, investigador de DS, **desde este anuncio no se han visto más que entregas de mercados y subsidios de arriendos** para estas familias que, pese a afirmar su deseo de regresar a Gramalote, **continúan viviendo en situación de emergencia en Cúcuta, Santiago y Lourdes, principalmente**.]

[Según Arenas, el proyecto de reconstrucción de Gramalote y la relocalización planificada de su población, a cargo del Ministerio de Vivienda y Servivienda, ha tenido graves dificultades. **La selección del sitio más adecuado para reconstruir el municipio fue una decisión que tardó dos años, luego de los cuales se concluyó que debía buscarse otro lugar ya que el escogido no era apropiado**.]

[Desde 2012 el proyecto está a cargo del Fondo Adaptación, agencia estatal creada para atender los procesos de reconstrucción y relocalización, tras los efectos del fenómeno de “La Niña” en el país, no obstante **en los últimos 3 años no ha habido mayores resultados**. Los **estudios necesarios para iniciar las obras de construcción han sido dilatados y hasta mayo de este año se inició formalmente la reconstrucción física de Gramalote**, agrega Arenas.]

[Otro de los aspectos en los que el Informe llama la atención es en la **insuficiencia del programa de construcción de viviendas de interés prioritario**, en el que se enmarca parte de la reconstrucción y relocalización de Gramalote, pues las **1000 familias incluidas no suman el total de las 604 propietarias y 504 arrendatarias que residían en el municipio para 2010**.]

[El Informe concluye afirmando la **urgencia de que las cerca de 3.500 personas en situación de desplazamiento por causas climáticas**, antiguas habitantes de Gramalote, **sean reubicadas por el gobierno nacional en condiciones dignas y acordes para enfrentar el cambio climático**.]

[Por lo que solicita tanto al Fondo de Adaptación como a la mesa de trabajo, integrada por autoridades municipales y líderes locales, que **diriman sus distanciamientos desde el diálogo y la toma de decisiones colectivas**, con el fin de que las próximas etapas del plan de reconstrucción y relocalización sirvan de modelo para resolver el desplazamiento climático en otros lugares de Colombia y de la región.]

[Conozca el informe: ]

[Desplazamiento climático y relocalización planificada en Colombia - El caso de Gramalote](https://archivo.contagioradio.com/y-que-paso-con-la-reconstruccion-de-gramalote/desplazamiento-climatico-y-relocalizacion-planificada-en-colombia-el-caso-de-gramalote/)
