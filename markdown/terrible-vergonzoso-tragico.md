Title: Terrible, vergonzoso, trágico
Date: 2015-08-22 17:19
Category: Camilo, Opinion
Tags: Agua, alirio uribe, audiencia pública, Audiencia senatorial, Comisión de Justicia y Paz, Iván Cepeda, La Habana, petroleo, Petroleras, Putumayo
Slug: terrible-vergonzoso-tragico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/bigwood_colombia090.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### **[Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas)**

###### 22 de agosto de 2015 

Mutaciones genéticas de peces, desplazamiento de animales, deforestación, aguas envenenadas, militarización con violaciones de derechos humanos son parte del desierto amazónico en Putumayo.

Esto es lo mínimo que se puede decir de las operaciones petroleras en Putumayo, reveladas hace poco menos de dos días en una Audiencia Senatorial realizada en Puerto Asís.

71 % del área territorial del Putumayo está intervenida o proyectada para extracción petrolera. Estos quiere decir, que de los 2.488.500 hectáreas del Putumayo,  1.767.637 hectáreas son petroleras.

En los testimonios denuncias de habitantes de la Zona de Reserva Campesina Perla Amazónica,  y de las comunidades Nasa y las de Orito y Teteye, se expresan las graves consecuencias de una política extractiva petrolera rentable para los privados, la mayoría multinacionales; una política de seguridad que privatiza el uso de la fuerza pública y una política ambiental inane ante la dimensión que significa extraer 18 millones de barriles de petróleo al año.

De acuerdo con el análisis de la Comisión de Justicia y Paz, presentado la sesión del pasado sábado,  hay 11 pozos en explotación, 49 bloques en exploración en el Putumayo. Las precisiones de la información presentadas por las operaciones de las empresas Vetra y Amerisur en la ZRC; Gran Tierra de Canadá en las comunidades Nasa son aterradoras.  
Las operaciones petroleras desde el 2002 han contado con una nueva forma de seguridad, diseñadas en el marco de Plan Colombia, en el que Putumayo ha sido uno de sus epicentros, con batallones energéticos como el 27 y Vial 11,y una nueva forma de privatización de lo público con convenios empresas fuerzas militares.

Según informó el reconocido organismo de dh, entre  2001 y 2013 se han celebraron más 320 convenios entre empresas petroleras y la 6taDivisión del Ejército por un valor de 60.772.903.082 millones de pesos; de este total, 35.342.354.382 millones de pesos se destinaron a Batallón Energético 27, esto sin contar otro aporte, de los 30.610.088.339 millones de pesos que se comparte con otras divisiones militares.

Cifras que contrastan con el hecho de que las empresas están obligadas a destinar el 1% del total de la inversión en planes para la recuperación, conservación, preservación y vigilancia de la cuenca hidrográfica que afecten. Para un proyecto de 3 millones de dólares, por ejemplo, deben pagar por una sola vez, para ellos, 75 millones miserables de pesos. Una comparación que refleja el valor de la fuerza para la soberanía multinacional contra los nacionales y contra la humanidad.  
Una comparación que demuestra con absoluta vehemencia la escala de valores del proyecto de país que anima las castas dirigentes y sus partidos. Ellas, sin sentido de patria, legislan, decretan, ofrecen sus servicios como mercaderes políticos y militares al postor privado, y de rodillas, al internacional, sin importar sus conciudadanos empobrecidos o neoesclavizados, pues no hay mundo mejor que el del mercado; sin importar el aceleramiento de la deuda ambiental. Seguridades de todo tipo al foráneo empresarial, inseguridad para la humanidad.

Desconocen a la brava, por ignorancia o por dinero, y solo es retórica, el lenguaje y la política ambiental, cuando fraccionan al Putumayo como parte de la Amazonia, la misma que el presidente Santos, en el discurso verde, manifestó que iba a proteger en un foro internacional. ¡Mentira cochina!, la tragedia ambiental es terrible por esta locomotora empresarial.

Lo dramático no para allí. Más cifras se revelaron por la ONG que demuestran la hipócrita posición y actitud de actores públicos y privados, nos referimos a los vertimientos de crudo a los ríos y las aguas contaminadas por el proceso extractivo. Censurable los efectos ambientales de las acciones de las FARC EP en Putumayo que afectaron los ríos,  ¿pero como calificar lo qué sigue aquí?.

Durante los últimos 10 años, en promedio, cada 4 días ha ocurrido una contingencia ambiental por derramamiento de crudo. 58% de los casos  por fallas de operación de las empresas, y el 42 % restante por  atentados a la infraestructura, y perforaciones ilegales a los tubos.  Los datos no desmentidos por Corpoamazonia permiten colegir que en esta realidad ambiental tan dramática  hay responsabilidades empresariales ocultadas que se han encubierto y escudado con el chivo expiatorio de la guerrilla de las FARC EP. ¿Cómo calificar este engaño?

La burla a nuestros derechos y a los de la humanidad, Amazonia es pulmón del mundo, sigue. Afirmó la Comisión de Justicia y Paz que cada uno de los 11 proyectos petroleros,  captan 2 litros de agua superficial por segundo,  por día 172.800 litros de agua, lo que es equivalente en un año, a más de 63 millones de litros de agua. Para complementar,  las autoridades ambientales autorizan  un permiso de 2.73 litros de agua residual por segundo a los ríos y quebradas de la región. Son entonces 86 millones de litros de agua residual al año. Esta información por supuesto no se presenta en el reciente informe sobre el agua, pues hay que mantener una supuesta y falsa coherencia entre la política minero energética y la ambiental.

Y para cerrar se hace inviable la titulación y la ampliación de titularidad a los más vulnerables. Por los pozos en explotación y los que se piensa  explotar 761.837 hectáreas quedan sin posibilidad de titulación para indígenas, campesinos, afro. Una confirmación más que se suma al reciente resultado inicial del censo agrario que refleja una precarización mayor de los habitantes rurales que poseen no más de 5 hectáreas de tierra o una titularidad colectiva para negros e indígenas que es desconocida por las políticas financieras y económicas.

Audiencia senatorial fundamental en una región estratégica para la pax neoliberal o la paz de la justicia socio ambiental. El problema no se resolverá en La Habana, será en Colombia, sí y solo sí, si se despierta del letargo y de la ignorancia generalizada. El Putumayo, vena de la Amazonia, un departamento, que refleja nuestra tragedia, una tierra de gran riqueza hídrica, boscosa, con bellas gentes, todos siendo arrasados a fuego acelerado con la simulación del progreso con leyes, con normas, con balas y con la extracción petrolera.
