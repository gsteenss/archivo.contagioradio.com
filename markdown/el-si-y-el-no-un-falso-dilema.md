Title: El Sí y el no, un falso dilema
Date: 2016-08-29 12:15
Category: Camilo, Opinion
Tags: campaña no, Campaña si, colombia, paz, plebiscito por la paz
Slug: el-si-y-el-no-un-falso-dilema
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/siono.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [@CamilodCasas](https://twitter.com/CamilodCasas)** 

##### [29 Agos 2016] 

Escuchando pobladores urbanos y rurales se descubre indudablemente que la repetición de las mentiras se hacen verdad, como ocurrió durante el régimen nazi. Sus grandes protagonistas Uribe y Santos.

Votaré por el Sí, debo precisarlo. No porque considere que Santos tiene una altura moral distinta a la de Uribe. Si hacemos recuentos de diversos hechos sabemos que Uribe ofreció a las FARC EP iniciar conversaciones e incluso una constituyente, cuando él era presidente. Santos logra el Acuerdo final del conflicto armado con las FARC EP con menos, es decir, sin un proceso Constituyente o instituyente.

El Sí porque aunque ninguna de las guerrillas, grupos disidentes en armas, fueron derrotados. Y con conciencia de lo nuevo, que puede nacer, ni las FARC EP ni el ELN, en el derecho legítimo a la rebelión, de por sí en una guerra asimétrica, representando sectores de la población, lograron denunciar saboteando un tipo de Estado instaurado, legitimando otro para las mayorías. La eficacia de la política con las armas no logró conquistar en los ciudadanos del común la necesidad de otro orden. La resistencia armada ha ido llegando a sus límites. Esto a pesar de las evidentes contradicciones expresadas en la desigualdad y la exclusión en que vive un altísimo porcentaje de la población en el campo y la ciudad. Es verdad que su altura moral puede ser distinta de sus contradictores. Sin embargo, no siempre lo medios que usaron y usan fueron ni han sido consonantes, con la ética del hombre y la mujer nueva. Es más, “esos errores” o desbordamientos fueron usados por el establecimiento como parte de una estrategia de guerra psicológica que les aisló de la simpatía ciudadana urbana

El Sí porque Santos y Uribe son parte del mismo proyecto de Pax Neoliberal, que se pelea entre vanidades, que disputan entre la propaganda de la mal llamada paz y de la perversa prolongación de la guerra militar, sus egos y no el destino de un país incluyente. Ellos, la clase política que representan, la economía legal e ilegal que les ha aupado y los intereses del sector empresarial privado multinacional, se diferencian en formas entre el proyecto de sociedad premoderno y moderno.

El Sí porque Santos no es la paz y el No de Uribe tampoco es la guerra. Santos es la oxigenación de la podredumbre de la clase política que se deslegitimo parcialmente con Uribe, en la llamada parapolítica con el objetivo de privatizar los territorios y los derechos, como se ve en Tolima, Huila, Caquetá, Putumayo, Meta y se proyecta en el Chocó. Santos son las ZIDRES que eran las Alianzas Estratégicas de Uribe, con retórica participativa. Ambos son las licencias exprés y las limitaciones a la consulta previa. Uribe es el aparente opositor de Santos para continuar deslegitimando el derecho a la construcción de otra sociedad, de un país en otra democracia real. Santos y Uribe son la llave de la consolidación territorial diseñada desde Pastrana y luego llamada Seguridad Democrática y luego apodada Prosperidad Democrático.

El Sí porque el gran aporte de esa lucha armada que pactó y en la que debe aportar el ELN cuando pacte, así como, el EPL, es la posibilidad de sumar, de juntarse, rompiendo espejismos, vanidades, sesgos en una apuesta plurisocial y ambiental, de poder conciente colectivo.

Perder el plebiscito es dejar en el vacío, lo que la guerrilla de las FARC EP en medio de una correlación de fuerzas desfavorables, logró en el Acuerdo como arquitectura para avanzar en otra memoria colectiva, en el enfrentamiento de la impunidad y en las bases políticas para otra sociedad. Ese es el gran valor de las FARC EP haber enfrentado la pretensión de sometimiento del establecimiento y de negacionismo de las razones del derecho a la rebelión, a lograr de fondo reconocimientos políticos favorables para el conjunto de las propuestas del movimiento social, así no siempre se comprenda, así se les valore falsamente como vanguardista, entre otros por sectores étnicos, o por el gran espectro de la llamada izquierda.

El SÍ porque la esperanza de la paz no nacen con los Acuerdos, son parte del corazón de las heterutopías populares, de sus epopéyicas de los millares de despojados, desarraigados, asesinados, desaparecidos, torturados, de los sin techo, de las víctimas ambientales. El Si es un contra corriente a Santos, a Uribe, a Vargas Lleras.

El Sí, el de la gente de a pie, y la organizada, legitimará más que a las FARC EP, al país nacional, al que en el alma aclarada ante la estrategia de verdades que encubren mentiras de Santos y Uribe, avanzará hacia la nueva democracia para la paz con justicia social, económica y ambiental. El plebiscito es un falso dilema, la realidad es la necesidad de esa otra democracia en la que podemos andar un paso más, con un Sí electoral, legitimador del otro país que deseamos.
