Title: Boyacá, el primer departamento en prohibir el uso de asbesto en el país
Date: 2019-05-24 17:12
Author: CtgAdm
Category: Ambiente, Nacional
Tags: asbesto, Boyacá
Slug: boyaca-el-primer-departamento-en-prohibir-el-uso-de-asbesto-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-111.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

El Gobernador de Boyacá, Carlos Andrés Amaya, firmó un decreto el viernes que prohíbe en este departamento el uso de asbesto y la contratación pública con empresas que emplean este material tóxico en la construcción de obras, la primera medida de su orden en el país.

Según el Gobernador, esta acción sirve como un mensaje al Congreso de aprobar el proyecto de ley que eliminaría el uso, comercialización, importación y distribución del mineral en Colombia para el año 2021. "**Instamos al Congreso de la República y el Gobierno Nacional que hagan lo propio también allí y que se prohíbe definitivamente el uso de este elemento tan dañino**", manifestó.

También invitó a los otros gobernadores del país y los alcaldes de los municipios de Boyacá de tomar las mismas medidas para proteger la salud de los ciudadanos de este material. Cabe recordar que los municipios de Chivatá, Tibasosa y Samacá ya han restringido el asbesto mientras que Tunja, Guateque y Moniquirá buscan hacer lo mismo. (Le puede interesar: "[Histórico respaldo en Congreso a prohibición del asbesto](https://archivo.contagioradio.com/historico-respaldo-en-congreso-a-eliminacion-de-asbesto/)")

Boyacá es el cuarto departamento en el país que más genera residuos de asbesto con 47 toneladas producidas, después de Bogotá D.C., Valle del Cauca y Cundinamaraca, según un estudio del  Instituto de Hidrología, Meteorología y Estudios Ambientales, y tal como lo indicó el Gobernador, esta medida reduciría en un 80% el uso de este material en el territorio.

El Gobernador concluyó que este acto "**es una acción por la vida, por las futuras generaciones, por un mundo mejor** y porque gobernamos para la gente, por Boyacá". El decreto rige en el departamento a partir de hoy.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
