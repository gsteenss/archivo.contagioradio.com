Title: Denuncian que noticias Caracol usó “abusivamente” declaraciones de familiares de víctimas del Palacio de Justicia
Date: 2015-11-25 21:27
Category: Judicial, Nacional
Tags: 30 años buscando justicia, 30 años de la retoma del palacio de justicia, Irma Franco, María del Socorro Franco, Palacio de Justicia
Slug: noticias-caracol-uso-abusivamente-declaraciones-de-familiares-de-victimas-del-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/manipulacion_caracol_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: video de nota 

<iframe src="http://www.ivoox.com/player_ek_9520803_2_1.html?data=mpqfkp2Ud46ZmKiakp2Jd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbi1tPQy8bSb9LpxpDb0dnNp8rV1JCww9fFp9DgjNrgh6iXaaOnjIqylIqcdIatpMbP19jNusLhxtPhx4qpdoaskYqmppCRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Maria del Socorro Franco] 

###### [25 Nov 2015]

Maria del Socorro Franco, hermana de Irma Franco Pineda, desaparecida durante la retoma del Palacio de Justicia en el 1985, y sobre la cual el Coronel ® Plazas Vega fue condenado en primera y segunda instancia denunció que **Caracol Televisión manipuló las declaraciones entregadas por ella el pasado 6 de Noviembre** luego del acto de perdón del presidente en cumplimiento de la orden de la Corte Interamericana de DDHH.

A través de una carta difundida en la noche de hoy, la señora Maria del Socorro afirma que en la emisión de noticias Caracol del 24 de Noviembre se utilizó y se “manipuló” su declaración para decir que los familiares de las víctimas del Palacio de Justicia defendían al Coronel Plazas Vega con el titular **“exclusiva los familiares de Irma Franco defienden a al coronel Plazas Vega”.**

María del Socorro afirma que no fue una exclusiva ni una defensa **“Al respecto, debo señalar que ni fue una exclusiva, ni una defensa”** y agrega que Caracol hace un uso abusivo de sus declaraciones “Abusivamente, **Caracol TV utilizó el nombre de mi familia, para justificar una supuesta inocencia de quien ha sido condenado en primera y segunda instancia”.**

La familiar agrega “que no se explica la manipulación burda y grosera que hizo Caracol TV de mis expresiones” al señalar que Plazas Vega ha sido un “Chivo expiatorio” y se le pretende endilgar toda la responsabilidad, cuando hay mayores niveles de responsabilidad “**desde el Presidente Betancourt, sus Ministros, altos mandos militares y numerosos integrantes del Ejército que hoy gozan de la impunidad.**” Señala Franco.

En la carta María del Socorro afirma que las declaraciones del canal la ofenden como víctima “**Decir que mi familia defiende su inocencia me ofende como víctima”** y reitera que no es ético que un medio de comunicación use de esa manera las declaraciones que una víctima ha ofrecido de buena fe “no es ético, ni responsable que un medio de comunicación utilice declaraciones que una víctima ofrece de buena fe, para favorecer a los responsables del dolor”

Esta es la nota...

<iframe src="//players.brightcove.net/3827094934001/default_default/index.html?videoId=4630567226001" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
A continuación la carta completa de María del Socorro Franco...

##### *"Como hermana de IRMA FRANCO PINEDA, víctima de desaparición forzada en los hechos del Palacio de Justicia, denuncio con profundo dolor la manipulación que hizo Noticias Caracol de unas declaraciones que di a los medios de comunicación el pasado 6 de noviembre, en momentos en que justamente el Presidente de la República reconocía la responsabilidad estatal por estos hechos, después de una lucha de 30 años de verdad y justicia de nosotros los familiares.* 

##### *Ayer 24 de noviembre en la edición de las 7:00 p.m., Noticias Caracol abrió la edición de su noticiero, diciendo que en “exclusiva los familiares de Irma Franco defienden a al coronel Plazas Vega”. Al respecto, debo señalar que ni fue una exclusiva, ni una defensa. Abusivamente, Caracol TV utilizó el nombre de mi familia, para justificar una supuesta inocencia de quien ha sido condenado en primera y segunda instancia como responsable de la desaparición de mi hermana.* 

##### *Como ha sido ampliamente conocido y difundido, el mismo Coronel Plazas Vega ha aceptado la desaparición de nuestra hermana por las Fuerzas Militares, y se ha referido a las torturas que ella tuvo que sufrir, por lo que no se explica la manipulación burda y grosera que hizo Caracol TV de mis expresiones. A la hora de presentar la nota, quienes se dicen “periodistas” tomaron un fragmento en el que utilizo la frase “chivo expiatorio”, no para decir que el coronel Plazas es inocente, sino para enfatizar en que a este oficial se le ha endilgado toda la responsabilidad graves crímenes, en los que participaron desde el Presidente Betancourt, sus Ministros, altos mandos militares y numerosos integrantes del Ejército que hoy gozan de la impunidad.* 

##### *Decir que mi familia defiende su inocencia me ofende como víctima, pero también como comunicadora, no es ético, ni responsable que un medio de comunicación utilice declaraciones que una víctima ofrece de buena fe, para favorecer a los responsables del dolor de muchas familias que siguen exigiendo justicia.* 

##### *En ese sentido reafirmo mis declaraciones: no nos satisface una sola condena. Todos los responsables de la desaparición de mi hermana deben responder, nos merecemos como personas saber dónde están los restos de mi hermana, nos merecemos como seres humanos la verdad, y como víctimas merecemos justicia. Reafirmo la solicitud que hemos venido haciendo a lo largo de estos 30 años: la Familia Franco Pineda exige la VERDAD de lo ocurrido con Irma Franco Pineda y los responsables deben responder ante la justicia."* 
