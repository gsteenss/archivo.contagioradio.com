Title: Freiner Lemus, miembro de la Guardia Indígena fue asesinado en Cauca
Date: 2020-12-14 12:38
Author: CtgAdm
Category: Actualidad, Líderes sociales
Tags: Asesinatos, Cauca, guardia indígena, lideres sociales
Slug: freiner-lemus-miembro-de-la-guardia-indigena-es-asesinado-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/diseños-para-notas-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Este domingo 13 de diciembre **fue asesinado Freiner Lemus, excabildante y guardia indígena, en el resguardo de Togoima**, en el municipio Páez, departamento del Cauca. Según el Instituto de estudios para el desarrollo y la paz (Indepaz) son 291 líderes y defensores de DDHH asesinados en 2020.

<!-- /wp:paragraph -->

<!-- wp:embed {"url":"https://twitter.com/marthaperaltae/status/1338316225219358720","type":"rich","providerNameSlug":"twitter","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/marthaperaltae/status/1338316225219358720

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph {"align":"justify"} -->

El Consejo Regional Indígena del Cauca ([CRIC](https://www.cric-colombia.org/portal/excabildante-del-resguardo-de-togoima-fue-asesinado-por-grupos-fuertemente-armados/)), rechazó el hecho a través de un comunicado manifestando que *"nuevamente el municipio de Páez es epicentro de la violencia que azota el país, está vez **grupos fuertemente armados interrumpieron la tranquilidad de la vereda Guadualejo del resguardo de Togoima, asesinando al excabildante Freiner Lemus, quien se encontraba en la zona"***.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Freiner Lemus, tenía 22 años y era integrante del pueblo Nasa ubicado en la vereda de Guadualejo. El hecho que ocurrió sobre las 8:00 pm del domingo se da **previo a la reunión convocada para este lunes, en donde de manera extraordinaria se reunirían en el resguardo** para hablar sobre la situación de desarmonía territorial producto de los conflictos en la región. ([Derechos Humanos: Un camino de afirmación de la vida](https://archivo.contagioradio.com/derechos-humanos-un-camino-de-afirmacion-a-la-vida/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones como Indepaz advierten que los grupos armados consideran enemigas a las comunidades indígenas porque hacen control en su territorio y ejercen autoridad, lo que va en contravía de sus intereses en particular las explotaciones mineras y el narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para octubre de 2020 en Colombia habían sido asesinados 54 miembros de comunidades indígenas, agresiones que han sido registradas con mayor frecuencia en los departamentos de **Chocó, Nariño y Cauca**; este último contando incluso con un aumento del pie de fuerza de la Brigada 29 del Ejército y la Fuerza de Despliegue Rápido (FUDRA) que suman cerca de **10.000 hombres**, sin embargo la violencia continúa.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a que la reunión será aplazada, teniendo en cuenta los nuevos puntos de análisis y exigencias de vida, las 17 autoridades indígenas agrupadas en la Asociación de Autoridades Ancestrales Territoriales Nasa Çxha çxha, rechazaron *"**todo acto de violencia que generan los grupos armados, vengan de donde vengan, que lo único que generan es dolor, rabia y tristeza acabando con la vida de las comunidades** y afectando los propósitos e intereses de los planes de vida de cada espacio territorial".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
