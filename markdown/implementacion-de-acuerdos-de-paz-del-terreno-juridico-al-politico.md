Title: Implementación de acuerdos de paz pasa del terreno jurídico al político: Enrique Santiago
Date: 2016-10-03 11:33
Category: Entrevistas, Paz
Tags: enrique santiago, Jurisdicción Espacial de Paz, plebiscito por la paz
Slug: implementacion-de-acuerdos-de-paz-del-terreno-juridico-al-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Enrique-Santiago.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [03 Oct 2016] 

El asesor jurídico del proceso de paz [Enrique Santiago](https://archivo.contagioradio.com/enrique-santiago-evalua-acuerdo-final-28458/) afirma que luego de los resultados del plebiscito por la paz, lo que cabe es la tristeza porque el pueblo colombiano **perdió la oportunidad de acabar con una guerra de 52 años**, y que se pierda por un margen tan estrecho y con una campaña basada en afirmaciones no reales en torno al acuerdo, porque “el acuerdo no significa impunidad”.

Además Enrique Santiago resalta que no se pueden computar directamente los votos por el NO o los 50.000 votos de diferencia pues muy probablemente no quieren decir directamente que no se esté de acuerdo con la [Jurisdicción Especial de Paz,](https://archivo.contagioradio.com/entrevista-con-enrique-santiago-justicia-en-el-proceso-de-paz/) porque esos votos pueden representar a quienes no quieren el desmonte del paramilitarismo o a quienes **busquen impunidad para delitos cometidos por las fuerzas militares u otros agentes del Estado.**

Para el abogado cabe resaltar que **el plebiscito no tenía efecto jurídico sino político**, es decir, el escenario actual tendrá que ser resuelto por los políticos y no por los abogados. Frente a las medidas que siguen para la implementación de los acuerdos señaló que el [proceso de dajación de armas ya comenzó antes del plebiscito](https://archivo.contagioradio.com/estos-son-los-5-puntos-que-se-firmaran-en-la-habana-con-el-cese-bilateral/) con la destrucción de una serie de explosivos, además resaltó que el presidente decidió dejar vigente el cese bilateral y serán los promotores del NO los que le digan al país cómo quieren continuar.

Otro de los elementos que hay que tener en cuenta en el  análisis del resultado del Plebiscito es que **la paz es un derecho que está por encima de la voluntad de las mayorías, es decir que no debió someterse a referendo** puesto que se podría haber aprobado teniendo en cuenta las facultades constitucionales con las que cuenta el presidente Santos. Además las FARC y es que el plebiscito era innecesario porque la paz es un derecho y un deber establecido en el artículo 22 de la Constitución Política.

Por su parte, según Enrique Santiago, **las FARC ya dejaron clara su posición en torno a su voluntad de no volver a empuñar las armas para hacer política**, lo que queda en adelante es escuchar las propuestas, tanto de los promotores del NO como del propio gobierno nacional que realiza hoy una consulta con todos los partidos.

Frente a la comunidad internacional una cosa clara es que el país no podrá progresar y no se podrá insertar normalmente en la comunidad de naciones con una situación como la actual después de que ganara el NO. A este respecto señala el abogado que **se debe llevar a cabo un pacto nacional amplio de manera urgente.**

Por parte de la comunidad internacional una de las primeras tareas de la Corte Penal Internacional, que respaldó el acuerdo, será investigar los graves crímenes que se han cometido en Colombia y que no corresponden a las FARC, “***lo que no se ha investigado son los crímenes de los paramilitares y de los fuerzas del Estado***” al resto de la comunidad internacional lo que le queda es respaldar la construcción de la paz.

<iframe id="audio_13163173" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13163173_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
