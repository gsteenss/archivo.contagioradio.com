Title: La participación política de FARC no puede ser asesinada
Date: 2019-08-20 15:13
Author: CtgAdm
Category: Expreso Libertad
Tags: amenazas a líderes, asesinato, FARC
Slug: la-participacion-politica-de-farc-no-puede-ser-asesinada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/ETCR-Heiler-Mosquera.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Desde la firma del Acuerdo de Paz en Colombia, se han registrado 141 asesinatos a integrantes del partido FARC, quienes ejercían labores de liderazgos en diferentes Espacios Territoriales de Capacitación y Reincorporación. ([ver mas: Dura carta de las FARC al gobierno por asesinatos de integrantes de ese partido polítco](https://archivo.contagioradio.com/farc_carta_presidente_juan_manuel_santos/))

Para Sofia Nariño e Isabela Sanroque, ambas integrantes de este partido, los hechos no solo se enmarcan en la persistencia de una violencia en los territorios, sino también en la falta de garantías y cumplimientos al Acuerdo de paz por parte del gobierno. Ya que para ellas, estas situaciones van de la mano con el crecimiento del paramilitarismo en el país, que tendría que haberse contrarestado con el desmonte de estas estructuras, pactado en La Habana.

En este programa del **Expreso Libertad**, sintonice las propuestas y herramientas que están usando las y los miembros del partido político FARC para proteger sus vidas y hacer un tránsito a la participación política desde su reincorporación.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F222058018718951%2F&amp;width=800&amp;show_text=false&amp;appId=1237871699583688&amp;height=413" width="800" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
