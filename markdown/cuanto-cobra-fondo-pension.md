Title: ¿Sabe cuánto le cobra su fondo de pensión por administrar su dinero?
Date: 2018-09-06 15:43
Author: AdminContagio
Category: Economía, Judicial
Tags: abogados, Demanda, pensiones
Slug: cuanto-cobra-fondo-pension
Status: published

###### [Foto: Ambassade de France en Colombie] 

###### [5 Sept 2018] 

Por medio de una demanda, los trabajadores colombianos podrán reclamar el 35% de más que los fondos de pensión les han cobrado desde 1994, por cuenta de un exceso cometido por las entidades prestadoras del servicio; según el abogado **Fredy Agudelo**, el porcentaje para un cotizante del salario mínimo desde esa fecha** podría equivaler a  más de 290** millones de pesos a la fecha.

La demanda consiste en reclamar el excedente en el cobro de la comisión de administración que se produjo desde 1994, con la entrada en vigencia de la **Ley 100.** En dicha normativa se establece que además del aporte obligatorio para pensión, que es el **11,5% del salario para fondos privados y el 12% en Colpensiones,** los trabajadores deberán aportar el 4,5% de ese porcentaje para la administración de su pensión.

Pero como asegura el Abogado, los fondos administradores de pensión **han cobrado ese porcentaje del 4,5% sobre la base del salario,** hecho que resulta incomprensible, porque "el fondo de pensiones administra la pensión, no el salario". De esta forma, si una persona cotiza su pensión sobre la base de **1 millón de pesos** tiene que aportar a su fondo de pensiones privado **115 mil pesos;** por administrar ese dinero, está pagando **45 mil pesos adicionales, y no 5 mil como debería ser.**

Con la demanda se está pidiendo que la norma que estipula dichos aportes se cumpla, y que el dinero adicional cotizado desde 1994 por los trabajadores sea devuelto. "En el caso de un trabajador  de salario mínimo que ha cotizado desde ese año, **estamos hablando de más de 290 millones de pesos"**, afirmó Agudelo. (Le puede interesar: ["10 reformas que necesita el sistema de salud en Colombia"](https://archivo.contagioradio.com/10-reformas-sistema-salud/))

El abogado dijo que la demanda se entabló solo hasta 2004 dado el desconocimiento de la norma y el "miedo a reclamar", sin embargo, en este momento el proceso que cursa en el **juzgado 4° de Cali** está en su fase final. Sí desea consultar más sobre este proceso, puede encontrar más información en la pagina de la [Fundación](https://www.avanzacolombia.org/) Avanza Colombia.

<iframe id="audio_28397160" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28397160_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 
