Title: Así será el Paro Internacional de Mujeres en Colombia
Date: 2017-03-01 17:04
Category: Mujer, Nacional
Tags: La Solidaridad es Nuestra Arma, Ni una menos, Paro de Mujeres, Paro Internacional de Mujeres
Slug: asi-sera-el-paro-internacional-de-mujeres-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/parodemujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Proyecto 341] 

###### [1 Mar 2017 ] 

En Colombia las ciudades que se unirán al Paro Internacional de Mujeres son **Manizales, Cali, Barranquilla, Pasto, Medellín, Bogotá y Cúcuta**. Movilizaciones, paro de actividades laborales totales y parciales, paro sexual y reproductivo, plantones y campañas virtuales, son algunas de las iniciativas planteadas por las organizaciones y colectivas.

En la capital, distintas organizaciones de mujeres y feministas han propuesto realizar una movilización en el sur de la ciudad, exactamente en la zona conocida como ‘Cuadra Picha’, al sur de la ciudad, la cita es a las 5:00 pm en la Av. Boyacá con Calle 5 Sur. La plataforma señaló que **se eligió este lugar puesto que allí hay varios establecimientos de licores, discotecas y trabajadoras sexuales.**

Simultáneamente, otras organizaciones han propuesto realizar la **‘Rodada por las Mujeres’, el punto de encuentro es en la Plaza de Bolívar a las 6:00pm** y aún no se ha confirmado el lugar de finalización de la jornada.

En Bucaramanga, las mujeres saldrán con la consigna 'Berracas a las Calles', la marcha iniciará en el **Hospital Universitario a las 2:00pm y terminará con un Festival en el Parque Santander. **

En Pasto, la Batucada Empoderada y otras organizaciones de mujeres, convocan a un Cacerolazo **en la Plaza de Nariño, a las 8:00pm con la consigna 'El 8 a las 8 las mujeres retumbamos'.**

En Manizales, las organizaciones convocan a diversas actividades culturales como círculos de saberes y conversatorios, que tendrán lugar en la Universidad de Caldas, sede central, desde las 10:00am y el cierre de la jornada será en el Parque de las Mujeres en la Kr 23 Cll 48 a **las 5:00pm con la consigna 'somos las nietas de las brujas que no pudieron quemar', acompañadas de un grupo de Cantaoras de Bojayá. **

En Cali La Red de Mujeres de Oriente y otras organizaciones, realizarán **un plantón de 6:00pm a 9:00pm, en la calle 76 con 28,** o avenida ciudad de Cali con troncal de Agua Blanca, al oriente de la ciudad con la consigna "Porque Siempre trabajamos, todas paramos".

En **Barranquilla, las mujeres se darán cita en la Plaza de la Paz Juan Pablo II, a las 6:00pm** harán una pequeña movilización alrededor de este punto de encuentro y luego realizarán un plantón en la plazoleta central.

En Cúcuta, las mujeres realizarán una jornada de presentaciones artísticas, con break dance, hip hop y graffiti, en el **Coliseo cubierto del barrio Escalabrini desde las 4:00pm. **

En Villavicencio las mujeres han propuesto una serie de actividades pedagógicas al rededor de temas de género y el por qué del 8 de Marzo, en la  **Institución educativa Silvia Aponte de 8:00am a 10:00am y en la Institución Educativa Las Palmas** de 2:00pm a 4:00pm.

###### Reciba toda la información de Contagio Radio en [[su correo]
