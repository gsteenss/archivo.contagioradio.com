Title: Por negligencia médica fallece un bebé de guerrillera de las FARC
Date: 2017-04-14 14:42
Category: Nacional, Paz
Tags: Amnistia, Bebé, FARC
Slug: por-negligencia-medica-fallece-un-bebe-de-una-prisionera-de-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/farc-mamas-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [14 Abr. 2017] 

A través de un breve mensaje en redes sociales, se denunció este viernes, que por falta de atención de la Secretaria de Salud de Bogotá y del Ministerio de Salud, **un bebé prematuro falleció. El menor era hijo de una mujer de la guerrilla de las FARC** que se encuentra en prisión.

A través del Twitter, Mujeres de las FARC y de otros integrantes de esa guerrilla, se aseguró que el bebé de Rocío Cuéllar, como se llama la guerrillera, **había nacido con algunas complicaciones y que requería de manera urgente atención médica oportuna.**

> [\#Urgente](https://twitter.com/hashtag/Urgente?src=hash)  
> Prisionera Rocio Guevara dio a luz, bebé prematuro se encuentra en cuidados intensivos.  
> Pedimos traslado de hospital. [@MinSaludCol](https://twitter.com/MinSaludCol)
>
> — Rodrigo Londoño (@TimoFARC) [12 de abril de 2017](https://twitter.com/TimoFARC/status/852174617737408512)  
>  

Pasadas unas horas dieron a conocer las noticia, en la que se manifestaba que por falta de atención medica el bebé había fallecido.

> [\#NegligenciaMataBebésDeLaPaz](https://twitter.com/hashtag/NegligenciaMataBeb%C3%A9sDeLaPaz?src=hash). Negligencia de la Secretaria de Salud del Distrito y del Ministerio de Salud esta madrugada murió [\#BebeDeRocio](https://twitter.com/hashtag/BebeDeRocio?src=hash)
>
> — Mujeres de las FARC (@MujerFariana) [14 de abril de 2017](https://twitter.com/MujerFariana/status/852933176037167104)

<p style="text-align: justify;">
<script src="//platform.twitter.com/widgets.js" async type="mce-mce-no/type" charset="utf-8"></script>
En redes sociales además dicen varios tuiteros que desde la noche de este jueves se estaba **solicitando el traslado del bebé, del Hospital de Engativá** a un centro de salud de mayor nivel por la gravedad de la salud del menor, **llamados que fueron ignorados** por el Secretario de Salud de la capital y el ministerio de Salud. En la comunicación aseguran que “los bebés de las guerrilleras son los hijos e hijas de la paz, **que mueran por negligencia del Estado, es matar la esperanza que nace** con un nuevo país (…) Como lo advertimos ayer, el bebé de Rocío no puede terminar siendo una fatal estadística de falta de atención y negligencia estatal”. Por esta razón, se ha convocado en redes sociales a **una tuiteraton con el numeral \#NegligenciaMataBebésDeLaPaz** desde las 11 de la noche de este viernes.

</p>
A través del Twitter Mujeres Farianas (@MujeresdelasFarc) enviaron un saludo a Rocío y agregaron “la dignidad demostrada por todas nuestras prisioneras merece \#AmnistíaYA. **Exigimos respuestas y soluciones inmediatas por parte del gobierno nacional. Por el bebé de Rocío \#AmnistíaYA** porque \#PazEsCompromiso”

> Exigimos respuestas y soluciones inmediatas por parte del gobierno nacional Por el [\#BebeDeRocio](https://twitter.com/hashtag/BebeDeRocio?src=hash) pedimos [\#AmnistíaYa](https://twitter.com/hashtag/Amnist%C3%ADaYa?src=hash) porque [\#PazEsCompromiso](https://twitter.com/hashtag/PazEsCompromiso?src=hash) — Mujeres de las FARC (@MujerFariana) [14 de abril de 2017](https://twitter.com/MujerFariana/status/852859088979537920)

Hasta febrero de 2017 se calcula que nacerían cerca de 300 bebes, y se aseguraban que podrían ser más a raíz de las nuevas relaciones que se comenzarían a dar luego de la firma de los Acuerdos de paz entre Gobierno y FARC.

###### [Reciba toda la información de Contagio Radio en][su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por][Contagio Radio](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio}

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

