Title: En fotos, así va el Paro Nacional
Date: 2016-05-01 08:51
Category: Paro Nacional
Slug: minuto-a-minuto-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/AsiVaLaMinga-en-Buenaventura.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Tras los persistentes incumplimientos del gobierno, luego de tres años de negociaciones luego del último paro agrario. La Cumbre Agraria Étnica y Popular, sale a las calles de Colombia a movilizarse, exigiendo paz pero con garantías para el campesinado, los indígenas y las comunidades afro del país. Son más de 100 puntos de movilización, 10mil manifestantes en 27 departamentos.

\

#### **Información actualizada de las regiones: ** 

-   [Región Amazónica](https://archivo.contagioradio.com/?p=24546)
-   [Región Caribe](https://archivo.contagioradio.com/?p=24556)
-   [Región Andina](https://archivo.contagioradio.com/?p=24551)
-   [Región de la Orinoquía](https://archivo.contagioradio.com/?p=24552)
-   [Región Pacífica](https://archivo.contagioradio.com/?p=24560)

[Tweets sobre \#ParoNacional](https://twitter.com/hashtag/ParoNacional){.twitter-timeline}  
\[posts\_grid type="blog" columns="4" rows="2" order\_by="date" order="DESC" thumb\_width="100" thumb\_height="100" lightbox="yes" meta="yes" excerpt\_count="20" link="yes" category="paro-nacional" tag="paro"\]
