Title: "Curules para la paz no son un regalo para las FARC": Camilo Vargas
Date: 2016-06-30 12:53
Category: Nacional, Paz
Tags: Circunscripciones de paz, curules de paz
Slug: curules-para-la-paz-no-son-un-regalo-para-las-farc-camilo-vargas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/CURULES-PARA-LA-PAZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] radionacional.co 

###### [30 Junio 2016]

Una de las críticas que rodea a las circunscripciones transitorias de paz o curules acordadas[en los diálogos de la Habana,](https://archivo.contagioradio.com/este-es-el-texto-del-acuerdo-sobre-cese-bilateral-firmado-entre-gobierno-y-farc/) es que serán otorgadas a miembros de las FARC-EP, sin embargo, jurídicamente estos escenarios **solo pueden ser disputados por organizaciones sociales** que se encuentren en los territorios de las circunscripciones.

Esta garantía permite que **ningún partido político o las FARC - EP  pueden competir por un puesto de participación en estos territorios** que por sus condiciones de participación política histórica, por la vulnerabilidad de sus poblaciones a los efectos del conflicto armado o que por su distancia o lejanía del Estado, van a ser privilegiadas con [la posibilidad de escoger representantes a la Cámara directamente](https://archivo.contagioradio.com/estos-son-los-5-puntos-que-se-firmaran-en-la-habana-con-el-cese-bilateral/), es decir sin tener que competir en la circunscripción departamental.

De acuerdo con Camilo Álvarez, coordinador del Observatorio político electoral de la Democracia perteneciente a la Misión de Observación Electoral, "es importante aclarar que **las curules no son un regalo para las FARC-EP**  además, debe tenerse bien referenciada dónde están esas organizaciones sociales que competirán por las curules de Representación a la Cámara, porque de lo contrario estos espacios podrían ser usados por los políticos que pueden muy bien a nombre de una organización social coptar espacios para la corrupción política y el clientelismo".

A su vez, expone que el reto más grande de cara a las circunscripciones transitorias de paz es con las comunidades y en los territorios, en donde debe investigarse que organizaciones hacen presencia en los mismos y a quiénes se les debe dar participación. De igual forma expone que los riesgos más grandes para el ejercicio de la democracia sigue siendo **la violencia electoral ejercida en el marco de la competencia política**.

<iframe src="http://co.ivoox.com/es/player_ej_12080119_2_1.html?data=kpedmpWVdZqhhpywj5aWaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZC6pdPbwtiSlKiPk8Pnxtfjw9nTtsrjjNXczoqnd4a1pdnWxdSPqc3ZxNnc1MbQb8XZjNHOjamRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
