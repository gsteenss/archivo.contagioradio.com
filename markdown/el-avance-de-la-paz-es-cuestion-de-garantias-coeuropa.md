Title: “El avance de la paz es cuestión de garantías” COEUROPA
Date: 2015-01-23 23:43
Author: CtgAdm
Category: DDHH, Paz
Tags: DDHH, Paramilitarismo, paz
Slug: el-avance-de-la-paz-es-cuestion-de-garantias-coeuropa
Status: published

###### Foto: eluniversal.com.co 

**Panfletos, llamadas, correos electrónicos y hasta la entrega de flores mortuorias**, son algunas de las formas en que diversas organizaciones paramilitares como la llamada “Águilas Negras” ha iniciado el 2015, en contra de **personas y organizaciones que se caracterizan por su trabajo en pro de los derechos humanos y la construcción de la paz.**

Ante estas amenazas, la exigencia de las organizaciones es que haya **investigaciones eficaces** tendientes a establecer la procedencia de las amenazas, además de proteger las personas amenazadas y encontrar **mecanismos para prevenir** que este tipo de situaciones se sigan presentando.

La Coordinación Colombia-Europa-Estados Unidos, **COEUROPA** que reúne a, cerca de, 200 organizaciones de defensa de los derechos humanos en Colombia y el mundo afirma que uno de los casos más graves y que ejemplifican la “arremetida paramilitar” es el caso del abogado José Humberto Torres *“quien además de figurar de manera reiterada  en  varios de los panfletos, es el blanco de un plan criminal para acabar con su vida, fraguado presuntamente por paramilitares recluidos en la Cárcel de Barranquilla, entre los que figura Yonnis Rafael Acosta Garizabalo “alias 28”.*

Señala el comunicado que la información reportada por las autoridades, en la planeación de la acción, **habría participado un teniente  de la Policía Nacional** y además que la parte de la policía que hace parte del esquema del defensor, fue retirada sin consulta y sin previo aviso.

A esto se suma la reciente decisión de las FFMM, de **retirar del servicio a 10 uniformados** señalados por su participación en las actividades ilegales de **"Andrómeda"** y el traslado de otros 20 a otros servicios. Sin emabrgo, la gravedad de las interceptaciones y el hecho de que hayn estado dirigidas contra las delegaciones de paz del gobierno y las FARC, **ameritan decisiones más profundas, señalan defensores de DDHH**.

Adjuntamos el comunicado de la plataforma **COEUROPA**

***¡EL AVANCE HACIA LA PAZ ES UNA CUESTIÓN DE GARANTÍAS¡***

***Bogotá 22 de enero de 2015.** El 2015 inició con una nueva ola de panfletos amenazantes contra reconocidos y reconocidas dirigentes políticos, defensores y defensoras de derechos humanos, reclamantes de tierras, líderes y lideresas sociales. Estas acciones amenazantes se dirigen contra personas y organizaciones que realizan un trabajo por la construcción de paz, defensa de los derechos humanos y defensa del territorio.*

*Uno de los casos que refleja la gravedad del riesgo que pesa sobre estos sectores, es el del  abogado José Humberto Torres, quien además de figurar de manera reiterada  en  varios de los panfletos, es el blanco de un plan criminal para acabar con su vida, fraguado presuntamente por paramilitares recluidos en la Cárcel de Barranquilla, entre los que figura Yonnis Rafael Acosta Garizabalo “alias 28”. Según la información reportada por las propias autoridades, en la planeación de la acción, habría participado un teniente  de la Policía Nacional.  Agrava esta situación  la suspensión de manera inconsulta e injustificada  de la protección policial que hacía parte del esquema de seguridad del defensor. *

*La situación de extremo riesgo por la que atraviesa José Humberto Torres, afecta al conjunto del movimiento de derechos humanos y paz, y requiere de resultados prontos y efectivos en materia de investigación  y protección por parte de las autoridades, para garantizar su vida e integridad personal.*

*Después de 7 años de interlocución con el Gobierno Nacional para lograr garantías para la defensa de los derechos humanos, es imperativo que este proceso genere resultados concretos y efectivos.  El Estado en su conjunto debe  por tanto, comprometerse de manera inmediata a garantizar la vida e integridad personal de todas las personas comprometidas con el proceso de paz, la defensa de los derechos humanos y del territorio, que han sido amenazadas.*

*Las plataformas que suscribimos el presente comunicado, insistimos en nuestra preocupación por la falta de resultados en las investigaciones frente a las sistemáticas amenazas y otro tipo de ataques de que han sido objeto personas que defienden derechos humanos y desarrollan un trabajo por la paz, en especial integrantes del Frente Amplio por la Paz y personas de las organizaciones sociales y de derechos humanos de las cuatro plataformas de derechos humanos y paz.  Mientras los autores de las amenazas  y agresiones no sean  identificados, investigados y sancionados por parte de la Fiscalía y los órganos de juzgamiento, mantendrán y extenderán  sus acciones.*

*Este panorama pone en entredicho la intencionalidad y capacidad estatal para garantizar la vida de futuros liderazgos, en un eventual escenario de posconflicto. *

***¡El avance hacia la paz también es una cuestión de garantías¡***
