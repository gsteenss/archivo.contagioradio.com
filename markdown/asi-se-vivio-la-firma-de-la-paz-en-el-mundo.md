Title: Así se vivió la firma de la Paz en el Mundo
Date: 2016-09-27 16:09
Category: Uncategorized
Slug: asi-se-vivio-la-firma-de-la-paz-en-el-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/14444818_942861342485801_4418783030738939285_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Indepaz / Semana 

###### 27 Sep 2016 

Este lunes el Gobierno nacional y las FARC-EP firmaron en la ciudad de Cartagena, el Acuerdo final de Paz para la terminación de una confrontación armada de más de 52 años. **En París, Berlín, Madrid, Washington y Quito, se concentraron cientos de personas** para ver en vivo la proyección del acto protocolario y celebrar este paso histórico.

![berlin](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/berlin-.jpg){.alignnone .size-full .wp-image-29944 width="1200" height="1600"}

En las plazoletas principales de estas ciudades, se dieron cita colombianos y colombianas para ver en vivo la transmisión de la firma final de los acuerdos. **En medio de la nostalgia por la distancia, los recuerdos y las añoranzas del territorio que dejaron, se vieron los rostros llenos de esperanza**, los abrazos, las sonrisas y las lágrimas que hicieron parte de estos emotivos encuentros. Se realizaron en estas plazoletas actos simbólicos con velas, cantos y flores evocando esa Colombia en Paz que quieren y está en construcción, distinta a la Colombia en guerra en la que crecieron y de la que un día se fueron.

Vea también: ["Ofrezco sinceramente perdón a todas las víctimas del conflicto": Rodrigo Londoño. ](https://archivo.contagioradio.com/ofrezco-sinceramente-perdon-a-todas-las-victimas-del-conflicto-rodrigo-londono/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
