Title: Proponen movilización para respaldar el acuerdo de paz debe ser continua
Date: 2017-05-31 15:34
Category: Nacional, Paz
Tags: acuerdos de paz, Movilización social, paz, Viva la ciudadanía
Slug: sociedad-civil-se-moviliza-para-respaldar-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/mesa-social-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [31 May. 2017]

Este 1 de junio, diversas organizaciones y de la sociedad civil harán varios tipos de movilización para volver a respaldar la implementación del acuerdo de paz y la continuidad de una solución política negociada al conflicto armado en Colombia. La cita será **en Bogotá a las 5 pm en el Planetario para salir hasta la Plaza de Bolívar** y “defender nuestra paz” dicen en redes sociales. Le puede interesar: [FARC espera que Santos haga valer sus facultades para sacar adelante acuerdos de paz](https://archivo.contagioradio.com/corte_constitucional_fast_track/)

Antonio Madarriaga, director de Viva la Ciudadanía, una de las organizaciones convocantes, dijo que esta vez no se quedarán solamente en movilizaciones de calle, sino que adicionalmente se realiarán acciones de incidencia y presión al Ejecutivo, al Congreso, a la Corte Constitucional, capacitaciones en áreas rurales, foros y enviando un mensaje de respaldo a la voluntad de paz que ha mostrado las FARC.

“Hay que reconocer la “extraordinaria” voluntad que ha mostrado las FARC de hacer tránsito a ser una organización política, así como la decisión mostrada del gobierno nacional en mantenerse en la línea de cumplir el acuerdo, razón por la cual la movilización de diversas maneras debe continuar” recalcó Madariaga.

### **Un panorama, dos realidades** 

Asegura Madariaga que en la actualidad el país se encuentra ante un panorama donde se pueden ver dos realidades que están en competencia, la primera de ellas es la **“equivocada interpretación que hace la Corte Constitucional de la tesis de la sustitución de la Constitución**, por lo cual una de las respuestas de la sociedad civil es movilizarse para decirle a la Corte que seguimos respaldando los acuerdos”.

Así mismo, dice Madariaga que harán una **“movilización” no solo en las calles, sino también de tipo jurídico para decirle a la Corte** porqué los argumentos expuestos en la sentencia que tumbó dos literales “es una sentencia equivocada y que están haciendo unas interpretaciones nada adecuadas del principio o de la tesis de la sustitución de la Constitución”. Le puede interesar: [Víctimas exigen a Congreso mantener esencia de los Acuerdos de Paz](https://archivo.contagioradio.com/victimas-exigen-a-congreso-mantener-esencia-de-los-acuerdos-de-paz/)

La segunda realidad se presenta en el Congreso, donde existe una fuerte oposición a la implementación de los Acuerdos de Paz, por lo que hay que enviar un mensaje en dos direcciones “el primero es que hay que decirle a éste órgano que la sociedad civil está movilizada en defensa del Acuerdo y de su implementación y el segundo es aprovechar esta condición para incidir en las normas que van a ser tramitadas” recalcó Madariaga.

### **3 mensajes dirigidos al Ejecutivo** 

Dice el integrante de Viva la Ciudadanía que con el Ejecutivo serán enfáticos en 3 mensajes, el primero de ellos es decirle a este órgano que la ciudadanía se encuentra presente en las calles para vigilar y dar seguimiento a la implementación de los acuerdos de paz “aquí estamos, aquí estamos defendiendo la paz”.

El segundo mensaje es que presionarán al Ejecutivo para que cumpla su “parte del acuerdo” y por último se van a llevar a cabo **acciones para estimular la participación de la ciudadanía en los procesos de implementación** en particular en el ámbito rural.

### **También habrá un mensaje para las FARC** 

Dice Madariaga que a los más de 6 mil guerrilleros y guerrilleras que están en las Zonas Veredales, las acciones que realicen en las calles, en los territorios rurales, en la Corte Constitucional y de tipo jurídico les envía el mensaje que hay una ciudadanía que continúa apoyando la paz **“les mandamos el mensaje que persistan en su intención de paz”** reitera. Le puede interesar: [FARC espera que Santos haga valer sus facultades para sacar adelante acuerdos de paz](https://archivo.contagioradio.com/corte_constitucional_fast_track/)

### **Intervenciones en la Corte Constitucional** 

Viva la Ciudadanía aseveró que realizarán una intervención en la Corte para que sean tenidos en cuenta los argumentos en el análisis de Constitucionalidad que ese órgano va a hacer sobre el SIVJRNR que fue pactado en La Habana. Le puede interesar: [Votación en bloque en Congreso esperanza para Acuerdo de Paz: Imelda Daza](https://archivo.contagioradio.com/votacion-en-bloque-en-congreso-esperanza-para-acuerdo-de-paz-imelda-daza/)

“Ese es otro nivel de intervención que haremos. Creemos que no se va a medir esta lucha solamente con las expresiones físicas en calles, sino que obliga a desplegar un conjunto de estrategias cerradas y abiertas” recalcó Madariaga.

### **El camino que viene será largo** 

De esta manera hace referencia Madariaga a las diversas movilizaciones que deberán seguirse realizando dado que, según él, la sociedad civil deberá continuar haciendo un ejercicio de exigibilidad de la aplicación de los acuerdos de paz porque “la posición de la Corte y lo que pasa en el Ejecutivo no va a ser el primer ni último obstáculo con el que se va a encontrar la implementación. El camino que viene será largo”. Le puede interesar: [Ahora Congreso de la República podrá re negociar los acuerdos de Paz](https://archivo.contagioradio.com/jairo-estrada/)

Para Madariaga esta lucha no se va a medir solo con las expresiones físicas en calle, sino que obligan a “desplegar **un conjunto de estrategias, actividades, porque en estos tiempos de paz esto tiene que ser una gran sincronía** de acciones de la sociedad civil en una lógica de implementación del acuerdo en tanto así puede trabajarse en la construcción de una sociedad más justa y más democrática” concluyó.

<iframe id="audio_19006884" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19006884_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
