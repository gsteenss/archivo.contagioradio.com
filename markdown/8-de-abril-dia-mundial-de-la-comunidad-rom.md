Title: 8 de Abril, día mundial de la Comunidad Rom
Date: 2015-04-08 16:31
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Comunidad gitana en Colombia, Día mundial de la Comunidad Rom, Gitanos en Colombia
Slug: 8-de-abril-dia-mundial-de-la-comunidad-rom
Status: published

###### Foto:Nuestramirada.org 

Hoy **8 de Abril de 2015** se conmemora el día del **Pueblo Rom** o, como se conoce a nivel mundial pueblo **“gitano”**. La fecha conmemora el mismo día en el que en Londres se estableció la bandera y el himno de la comunidad. Distintos actos de conmemoración se realizan en todas partes del mundo donde hay presencia de la etnia.

Los Rom se distribuyen por todo el mundo, aunque donde **más comunidades hay es en Europa**  constituyendo la mayor minoría étnica de la UE, siendo **España**, el país donde más gitanos viven en el viejo continente.

La palabra “gitano”, proviene de “egipciano”, que es como se les denomino en España, a la comunidad que venía de una parte de Grecia que se llama Pequeño Egipto.

**“Rom” es el nombre de los gitanos en su propia lengua**, el romanés, y significa “hombre”, en femenino “romí” (mujer) y en plural “roma”. En español también se suele usar el término “romanís”.

El pueblo Rom **emigró desde la India hacía Occidente** hace aproximadamente 1.000 años, donde en distintas etapas comenzaron a emigrar por todo el mundo.

Cuenta la leyenda que el rey de Persia pidió al Rey del norte de la India 1.000 músicos para sus fiestas, éste envió a los gitanos. El rey persa quedó encantado con su música y sus bailes y les pidió que se quedaran para trabajar su ganado y sus cosechas, pero al no tener conocimientos del oficio, no consiguieron cosechar y el ganado murió. En represalia el rey Persa los expulsó comenzando el éxodo del pueblo Rom.

Han sido distintas las **expulsiones y deportaciones** cometidas contra ellos, aunque la más importante la iniciada por los **Reyes Católicos en España**.

**En 1499 se emite la ley “Pragmatica”** sobre los gitanos en España, prohibiéndoles el uso de su lengua “caló” y delimitando seriamente sus derechos como etnia.

Más tare el 30 **de julio de 1749 el rey Fernando VI** decide apresar a todos los gitanos del reino por su rebeldía y su propia idiosincrasia y su posterior expulsión en lo que es conocido como ***La Gran Redada**.*

Por otro lado la **esclavitud** de los gitanos como feriantes o para distintos oficios protoindustriales continuaría en **Rumanía hasta 1864**.

En el marco de la ocupación nazi, en **1944 se fija la fecha del Holocausto Gitano (Samudaripén)**, en el que se estima **murieron más de 500.000 en toda Europa**. En comparación de densidad poblacional el holocausto gitano fue mayor al judío.Hay que destacar **“la noche de los gitanos”,** en la que en el campo de concentración de **Auschwitz**, entre el 1 y el 2 de Agosto de este año fueron asesinados más de 3.000.

Pero la **exclusión no ha terminado**, tampoco el racismo y la xenofobia que actualmente está sacudiendo fuertemente a la etnia, hasta el punto de ser calificado por ellos mismos como una situación de extermino.

Ejemplos como **Eslovaquia que está realizando esterilizaciones forzadas de mujeres** que entran en un hospital para una operación quirúrgica y despiertan esterilizadas.

En **España** los gitanos tienen los más altos índices de bajas en la educación primaria y secundaría y apenas un 20% acceden a la universidad. Los altos índices de Rom en las prisiones o la alta tasa de la comunidad en referencia a la pobreza extrema lo atestiguan.

En este país los gitanos **sufren de una discriminación y continua criminalización** que impide su acceso al trabajo siendo la tasa más alta de desempleo actualmente.

Este año el Secretariado Gitano en España ha sacado una campaña para frenar la discriminación aludiendo a que cuando uno busca el **término gitano en el diccionario de la Real Academia de Lengua Española, uno de los sinónimos que se encuentra es el de “trapacero”, que significa “persona** **Que intenta engañar con astucia y falsedades”**.

Video de la campaña contra la discriminación \#Yonosoytrapacero:

\[embed\]https://www.youtube.com/watch?v=DqBvpWbmdkQ\[/embed\]

Pero la situación de la etnia **Rom en Colombia** es bastante distinta a la de Europa, radicada principalmente en **Bucaramanga**, en comparación con las otras minorías con lenguas propias, como los indígenas o los palenqueros, son los que más la usan,

 Por otro lado en la **Constitución Colombiana está reconocida la etnia y la lengua** con plenos derechos. A pesar de su situación de pobreza su camino a diferencia del europeo esta mediado por las políticas de **inclusión social del propio pueblo Rom en Colombia**.

Diversas son las películas de cine realizadas sobre los gitanos como las del director d cine Emir Kusturica “Tiempo de gitanos”, o grandes cantautores como “Camarón”, referente mundial del estilo musical conocido como el flamenco.
