Title: FARC EP desmiente que asesinatos de líderes en el Cauca sean de su autoría
Date: 2020-05-23 07:21
Author: CtgAdm
Category: Actualidad, DDHH
Slug: aqui-no-hay-una-guerra-por-rutas-del-narcotrafico-aqui-hay-un-posicionamiento-militar-farc-ep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/niños-farc-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 22 de abril las (Fuerzas Revolucionarias de Colombia Ejército del Pueblo Farc-EP), presentaron una carta abierta, dirigida a varios congresistas, en la que abordaron temas como el plan de sustitución de [cultivos de uso ilícit](https://archivo.contagioradio.com/en-lo-corrido-de-mayo-han-sido-asesinados-cinco-integrantes-de-juntas-de-accion-comunal/)o, el asesinato a líderes sociales, el aumento de la violencia en los territorios, especialmente el Cauca, y las medidas del Gobierno tomadas en medio de la cuarentena.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La carta fue enviada a Iván Cepeda, Feliciano Valencia, Antonio Sanguino, Gustavo Petro, Aída Abella, entre otros; y a defensores de la Paz como el Francisco de Roux, Monseñor Luis José Rueda y Camilo González Posso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El documento hace énfasis en el departamento del Cauca, evaluada en una reciente audiencia en donde el **General Wilson Chávez**, señala en la que las Farc como responsables de los asesinatos y amenazas a líderes sociales y ex combatientes, *"con eso justifican su inoperancia ineptitud y ocultan la complicidad del estado con esos asesinatos"*, afirmaron.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello afirmaron que los comunicados en dónde se vincula a las Far, *"son falsos y redactados por bandas criminales y por el Ejército Nacional con el interés en sacar méritos políticos o económicos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez señalan, *"No son pocas las veces que hemos desmentido panfletos amenazantes que han terminado luego con el asesinato y agresiones contra líderes sociales o comunidades, (...) **sería irresponsable que agregadamos o asesinemos al pueblo que defendemos, de quienes nos nutrimos y por quienes luchamos"**.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También ponen como ejemplo la publicación de las llamadas carpetas secretas, la lista de enemigos y las intersecciones ilegales realizadas por el Ejército, ***"¿acaso estamos ante un nuevo modo de operar en el que a nuestro nombre organismos del Estado asesinan sistemáticamente a líderes sociales y ex combatientes?*"**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El grupo afirma que hoy los esfuerzos del Gobierno están enfocados en contrarrestar el narcotráfico y la [minería ilegal](https://archivo.contagioradio.com/asesinan-a-lider-aramis-arenas-bayona-en-becerril-cesar/), *"no nos equivoquemos aquí no hay una guerra por rutas del narcotráfico aquí hay un posicionamiento militar para asegurar zonas donde hay bienes estratégicos"* , agregando que **los intereses del Gobierno simplemente están enfocados en entregar bienes cómo los recursos naturales a multinacionales y empresas extractivistas.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Continúa el incumplimiento a los acuerdos a indígenas campesinos y afros: FARC

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sobre el Plan Nacional Integral de Sustitución de Cultivos Ilícitos (PNIS), señalaron que no solamente ha puesto en riesgo la vida de las organizaciones que trabajan por la sustitución sino también incumple las promesas a quienes se acogen a éste plan.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Señalamientos como el del General Wilson Chawes contra la Coordinadora Nacional de Cultivadores de Coca Amapola y Marihuana (**COCCAM), como una organización articulada al narcotráfico y que impulsa el secuestro es irresponsable y poner riesgo a los líderes que pertenecen a esta organización**" .*

<!-- /wp:quote -->

<!-- wp:paragraph -->

Ante esto destacaron que este tipo de afirmaciones son injustas *"con una población que lo único que ha sufrido son las consecuencias del abandono del Estado y en donde lo**s pobladores rurales no ven la hora de retornar a la producción agrícola tradicional con las garantías para poder habitar** y sobrevivir en estos territorios".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la solicitud del senador **Ernesto Macías de iniciar nuevamente los planes de aspersión aérea con glifosato** agregaron, *"**estos planes se pretenden hacer en municipios donde ya estaba la ruta de intervención del PNIS** lo hacen sin importar la crisis económica y social que se genera ni mucho menos los riesgos a la población a la población".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo destacaron que, *"quienes han puesto y han llenado de temor a las comunidades son las Fuerzas Militares quienes con irresponsabilidad y en acciones desarrolladas por grupos como FUDRA en los territorios han puesto en riesgo las comunidades"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último adicionaron qué, *"el Gobierno se está aprovechando de las dificultades sanitarias para realizar reformas que le garanticen beneficios económicos a la clase de poder*", esto acompañado de una campaña mediática en la que por ejemplo señalan la desmovilización de 32 guerrilleros del ELN en la que según las FARC, ***"ha sido evidenciado históricamente que muchas de estas campañas de [desmovilizacion](https://www.justiciaypazcolombia.com/el-impacto-del-covid-19-en-el-proceso-de-reincorporacion-de-las-farc-en-colombia/) no son más que patrañas orquestadas por el Ejército Nacional y el Gobierno".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al cierre del documento exhortan a las personas nombradas a, *"seguir construyendo una fuerza alternativa que permite en Colombia un gobierno de convergencia democrático y patriótico con el que se hace la guerra y se generen las condiciones para las transformaciones necesarias que acaben de una vez por todas con los factores generados del conflicto social y armado que vive nuestro país*".

<!-- /wp:paragraph -->
