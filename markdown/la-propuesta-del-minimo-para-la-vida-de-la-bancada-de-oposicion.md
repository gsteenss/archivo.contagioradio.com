Title: El Mínimo para la Vida que propone la bancada de oposición
Date: 2020-04-28 19:31
Author: AdminContagio
Category: Actualidad, Economía
Tags: oposición, pandemia, Renta básica
Slug: la-propuesta-del-minimo-para-la-vida-de-la-bancada-de-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Mínimo-Vital-para-la-Vida.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Este martes 28 de abril, congresistas de la bancada de oposición presentaron su propuesta de "Mínimo para la vida" una propuesta dirigida a un sector de la ciudadanía en el marco de la pandemia, que consiste en aportar un mínimo vital del que serían beneficiados más de 30 millones de colombianos en condiciones de vulnerabilidad económica, mientras dure la emergencia sanitaria y por tres meses más.

<!-- /wp:paragraph -->

<!-- wp:html -->

> [\#MínimoParaLavida](https://twitter.com/hashtag/M%C3%ADnimoParaLavida?src=hash&ref_src=twsrc%5Etfw) Este Ingreso para todas y todos debe entregarlo el estado mensualmente, por valor de un salario mínimo, durante el tiempo que dure la declaratoria de emergencia sanitaria y tres meses después. [pic.twitter.com/XoYZm7LwnO](https://t.co/XoYZm7LwnO)
>
> — Iván Cepeda Castro (@IvanCepedaCast) [April 28, 2020](https://twitter.com/IvanCepedaCast/status/1255132093723471874?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

Las bancadas proponen que el **Mínimo para la Vida** tomé como base el salario mínimo, es decir 877 mil pesos, y se agregue un "incremento marginal de acuerdo con la cantidad de personas que conforman el hogar, las variables de vulnerabilidad y un enfoque étnico y de género", llegando a un tope máximo de 2.200.000.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras se entregue el Mínimo para la Vida se suspenderían otros programas asistenciales como Adulto Mayor o Familias en Acción e Ingreso Solidario, de los cuales se tomarían algunos recursos para financiar la propuesta. También se propone la cancelación de la deuda, tomando en cuenta que según expertos, Colombia paga solo por concepto de intereses de deuda 30 billones de pesos anuales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Igualmente, la bancada señala que habría que modificar el sistema tributario, de tal forma que fuera más progresivo para lograr su fin último: la redistribución de la riqueza. El comunicado de la propuesta concluye que "el país cuenta con los recursos necesarios para implementar el MÍNIMO PARA LA VIDA que proponemos. Solo hace falta voluntad política y un cambio en la arquitectura de las medidas adoptadas por el gobierno".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Mínimo para la Vida:**Una medida que debe ser concertada con las fuerzas políticas**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Tomando en cuenta que hay varias fuentes de financiación, entre las que se encuentran incluso los salarios de los altos funcionarios del Estado, el senador Iván Cepeda afirma que la idea del Mínimo para la Vida no es ninguna idea traída de los cabellos", sino algo que se puede implementar y que esperan que se apruebe vía ley en el Congreso lo antes posible.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cepeda explica que, como ya no rige el Estado de Emergencia, se debe hacer un trámite legislativo de la propuesta, en el que todas las fuerzas políticas del país tendrán que concertar para llegar a un objetivo común que beneficie a las familias que necesitan estos recursos. En consecuencia, señala que la bancada de oposición está abierta al diálogo siempre, tomando en consideración la necesidad de este mínimo vital.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, el senador recuerda que la propuesta se enmarca en una reivindicación histórica como lo es la de la renta básica, por lo que una vez superada la pandemia se podría discutir una prórroga del Mínimo Vital por el tiempo que se requiera, y así, "marcar el inicio de una política social distinta en el país".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [Saqueos se detienen con renta básica no con represión: MOVICE Caldas](https://archivo.contagioradio.com/saqueos-se-detienen-con-renta-basica-no-con-represion-movice-manizales/).

<!-- /wp:paragraph -->

<!-- wp:html -->

[Mínimo Para La Vida](https://www.scribd.com/document/458870128/Minimo-Para-La-Vida#from_embed "View Mínimo Para La Vida on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" title="Mínimo Para La Vida" src="https://www.scribd.com/embeds/458870128/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-HpF8xu7FVyL0ZqlPCc6T" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="600" frameborder="0"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
