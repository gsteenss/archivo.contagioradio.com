Title: La lección de  Francisco para el desescalamiento del conflicto colombiano
Date: 2015-07-23 11:52
Category: Abilio, Opinion
Tags: colombia, dialogos de paz, Francisco de Asis, Papa Francisco, proceso de paz
Slug: la-leccion-de-francisco-para-el-desescalamiento-del-conflicto-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/espi4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: capuchinos] 

#### **[[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)** 

###### [23 Jul 2015] 

En  en los años en que  vivió  Francisco de Asís (1181\[2\]-1226),  el mundo católico  estaba en  guerra  con    los musulmanes  por el control de los lugares sagrados de Jerusalén. Fue tal la fuerza de la campaña, conocida como Cruzada,  que  Bernardo de Claraval  prometía el premio  de la vida eterna a quien mataba o a quien moría por esa causa: “Para ellos, morir o matar por Cristo no implica criminalidad alguna y reporta una gran gloria. Además, consiguen dos cosas: muriendo sirven a Cristo, y matando, Cristo mismo se lesdientrega como premio”.

Desde el otro lado de la confrontación un cronista árabe del siglo 13 consignaba  su percepción sobre las dimensiones de la guerra declarada  contra ellos: “Aquí el Islam está confrontado con un pueblo enamorado de la muerte... Celosamente imitan a aquel que adoran; desean morirse por su sepulcro... Proceden con tanta impetuosidad, como las polillas de la noche vuelan a la luz.”

En ese contexto Francisco de Asís no creyó  en la  maldad ontológica de los sarracenos musulmanes ni en la bondad  incuestionable de sus hermanos católicos  y resolvió mediar para evitar que unos y otros se siguieran matando. El bello relato cinematográfico “Francisco y Santa Clara”, da cuenta de este episodio (https://www.youtube.com/watch?v=D7V3HVviKEo).   Visitó el campamento  católico, habló con el cardenal Pelagio  a quien le manifestó que visitaría al Sultán en Egipto para pedirle que pactaran  un cuerdo de paz. El cardenal le dijo que los Sarracenos   decapitaban a sus prisioneros y paseaban sus cabezas. Francisco le replicó   que ellos hacían lo mismo  con la diferencia de que lanzaban las cabezas a los enemigos.

Francisco emprendió el camino con uno de sus  hermanos de comunidad  y al llegar al campamento  Sarraceno  fue detenido mientras el Sultán escuchaba desde la trastienda el interrogatorio. Al oírlo  hablar de paz, del Dios de los pobres, del Dios de Jesús,  se interesó  y  salió al  encuentro del visitante,  y pidió a los guardias que los liberan y curaran. Hablaron por largo tiempo  de  Alá y de Jesús, de la necesidad de buscar otra vía distinta a la violencia para  distribuirse el cuidado de los lugares sagrados de Jerusalén, comunes a las dos religiones. Francisco  se propuso como mediador con los cruzados católicos y el Sultán aceptó que les hablara de un acuerdo de paz. Pero ante el riesgo de que los cruzadas se negaran a aceptarlo, le entregó un cuerno para  que  lo tocara solo si su mediación fracasaba.

Y así fue.  El cardenal  de la cruzada católica, apenas  escuchó a Francisco, ordenó   el ataque contra el campamento del Sultán, despreciando la posibilidad de un acuerdo de paz. Francisco volvió a su comunidad, no sin antes  avisar  a su interlocutor del fracaso de su intento. Caminó  con el corazón partido  ante la contradicción experimentada de un “infiel” que aceptó su oferta y de un “fiel” que la rechazó.

Odios similares se han reproducido en  el occidente cristiano  contra los indígenas, afrodescendientes, judíos, liberales, científicos, comunistas, insurgentes. Lo sectores de poder  no han escatimado calificativos para generar desprecio contra quienes consideran sus enemigos, intentando deshumanizarlos al punto de provocar su eliminación física. Similar ha ocurrido en Colombia con los   alzados en armas contra el establecimiento y contra  todos aquellos a quienes se les identifica  con esa causa.

Hoy la posibilidad de avances  en el proceso de paz pasa  por el desescalamiento de la confrontación armada, pero también,  por  la disminución de la carga  de odio que los sectores de poder reproducen a diario desde los medios masivos de información.   A través de palabras y de imágenes introyectan   en el imaginario colectivo  la convicción de que la eliminación del “enemigo” es un bien para sí y para la sociedad.

Debemos darnos la oportunidad   de ver al insurgente en su condición humana, contemplar  los seis  ceses unilaterales declarados por las Farc cumplidos  casi en su totalidad  y  la disposición al diálogo que ha expresado el Eln. Se requiere  trabajar por una respuesta en consecuencia  de parte del gobierno.

Ya el presidente Santos   ha  afirmado que a las Farc no se les puede seguir llamando terroristas ni narcotraficantes,  un paso importante que debe  ir mas allá.  Quizás si se les escucha sin las prevenciones de los mensajes de odio difuminados como pólvora, podemos comprender  sus posiciones y contribuir a salidas no violentas para resolver las contradicciones, tal como lo hizo Francisco de Asís con el Sultán musulmán en Egipto. Si lo hacemos, quizás nos llevemos sanadoras sorpresas.
