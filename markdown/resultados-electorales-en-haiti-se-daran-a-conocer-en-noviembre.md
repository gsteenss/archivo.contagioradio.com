Title: Resultados electorales en Haití se darán a conocer en noviembre
Date: 2015-10-31 15:45
Category: El mundo, Política
Tags: El concejo Electoral Provisional CEP de Haití informó que los resultados oficiales se darán a conocer a inicios de noviembre, elecciones presidenciales en Haití, Johny Jean Jacques, Jovenel Moise, Jude Celestin
Slug: resultados-electorales-en-haiti-se-daran-a-conocer-en-noviembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/elecciones-de-Haití.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### www.elcirculord.com 

###### [29 oct 2015] 

El pasado domingo se realizaron las elecciones presidenciales en Haití, más de un 30% de los 5.8 millones votantes haitianos inscritos en el Censo se acercaron  a las urnas para ejercer su voto , así lo afirma el jefe de la Misión de Observación Electoral del Organismo panamericano de Haití.

La poca participación de los electores haitianos demuestra falta de credibilidad en el proceso y en los candidatos que saldrían electos, algunos de ellos han sido acusados por delitos como secuestro, tráfico de drogas y de armas, asesinatos selectivos y además por ser miembros de grupos armados.

Johny Jean Jacques, miembro de Cáritas Haití, uno de los responsables del programa de Economía Solidaria (Ecosol) afirma que "*las elecciones serían una estrategia de los políticos para distraer a la población de los graves problemas que padece el país, que no tendría una libertad real para realizar la elección a su modo y tiempo, preso del apoyo financiero internacional"*.

Adicionalmente hace tres años Haití está en la espera de elecciones ya que no pudieron realizarse debido a que los partidos políticos y el poder Ejecutivo nunca llegaron a un acuerdo. Sin emabargo con las elecciones reciente se espera restaurar la confianza en la  política, generando  estabilidad y desarrollo económico, menciona Johny.

Respecto a los comicios electorales presidenciales El concejo Electoral Provisional CEP de Haití informó que los resultados oficiales se darán a conocer a inicios de noviembre, según la encuesta realizada por la Oficina de Investigación en Ciencias de la Computación y el Desarrollo Económico y Social (BRIDES) el candidato presidencial por la Liga Alternativa para el Progreso y la Emancipación de Haití (LAPEH), Jude Celestin, tiene mayor preferencia electoral, mientras que en segundo lugar se ubica el candidato Jovenel Moise del partido gobernante  haitiano PHTK.
