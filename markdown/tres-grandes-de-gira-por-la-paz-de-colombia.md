Title: Tres grandes de la música de gira por la paz de Colombia
Date: 2016-07-10 11:50
Category: Cultura, Otra Mirada
Tags: Gira por la paz de colombia, Leon Gieco, Música, Pablo Milanés, Piero
Slug: tres-grandes-de-gira-por-la-paz-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/ep004567_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Todas las voces todas 

##### [8 Jul 2016]

Las voces de Piero, León Gieco y Pablo Milanés se unirán para cantar por la paz de Colombia en una gira con la que visitarán algunas de las ciudades y municipios del país que recibieron con mayor fuerza las consecuencias de la guerra.

La iniciativa liderada por el cantautor Piero de Benedictis, tendrá como punto de partida un concierto en la ciudad de Bogotá el próximo 9 de septiembre. En palabras de músico argentino, "queremos dejar un símbolo de paz en cada uno de los lugares escogidos, una señal que nos recuerde que estamos en los albores de una nueva historia".

En la lista de municipios que visitará la gira están incluidos San Vicente del Cagúan y Marquetalia, dos poblaciones en las que se han vivido episodios representativos durante los más de 50 años de conflicto armado. Allí, los habitantes, además de escuchar grandes temas musicales, podrán participar de charlas, sembrar árboles e intercambiar semillas nativas como símbolo de renovación.

Con la gira se busca propiciar la concientización sobre la importancia que tiene el proceso de paz para la transformación del país y el papel que todas y todos los colombianos tienen con su decisión de refrendar o no los acuerdos alcanzados en la Habana desde 2012.

 
