Title: Edgar Hernández, líder social, es asesinado en Puerto Caicedo
Date: 2020-11-24 22:39
Author: AdminContagio
Category: Actualidad, Nacional
Tags: asesinato de líderes sociales, asesinatos Putumayo, Puerto Caicedo
Slug: edgar-hernandez-lider-social-es-asesinado-en-puerto-caicedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Red de DD.HH del Putumayo denunció el homicidio de Edgar Hernández, reconocido líder social y presidente de la Junta de Acción Comunal (JAC) de la vereda La Independencia en Puerto Caicedo, departamento de Putumayo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con la información suministrada, el hecho se perpetró este lunes 23 de noviembre en horas de la tarde, en la vereda el Coqueto, vía del corredor San Pedro Arizona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El líder social se transportaba en una moto con un amigo, quien también resultó asesinado en el crimen. De acuerdo con la Red de DD.HH. fue identificado como Teodisfelo Fajardo, campesino que pertenecía al Programa PNIS.** (Le puede interesar: [“La política de guerra del Gobierno reactivó todas formas de violencia”](https://archivo.contagioradio.com/la-politica-de-guerra-del-gobierno-reactivo-todas-formas-de-violencia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de su labor al frente de la Junta de Acción Comunal, Hernández también hacía parte de la Asociación de Trabajadores Campesinos de Alto Mekaya -ATCAM- , era miembro de la Federación Nacional Sindical Unitaria Agropecuaria -Fensuagro- y pertenecía a la coordinación de Marcha Patriótica.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1331050403702730753","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1331050403702730753

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Con Edgar Hernández, la cifra de líderes sociales y defensores de derechos humanos asesinados en lo que va del año 2020, aumenta a 259**, según el [Instituto para el estudio del desarrollo y la paz - Indepaz-](http://www.indepaz.org.co/lideres/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las últimas dos semanas, el departamento de Putumayo ha sido víctima de 3 asesinatos contra líderes sociales y firmantes de paz. Hace menos de una semana, también en Puerto Caicedo, fue asesinado el excombatiente Bryan Stevens Montes Álvarez, en el sector veredal El Picudo, ubicado a 3 horas del casco urbano de Puerto Caicedo. (Le puede interesar: [Firmante de paz, Bryan Stiven López es asesinado en Putumayo](https://archivo.contagioradio.com/firmante-de-paz-bryan-stiven-lopez-es-asesinado-en-putumayo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mientras que el pasado lunes 16 de noviembre, el partido Farc **[denunció](https://twitter.com/PartidoFARC/status/1328386588821385217)** el asesinato del excombatiente **Enod Lopez Verjano y de su **esposa, **Eneriet Penna****,** concejala del partido Conservador****, **en Puerto Guzmán.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
