Title: Más de 2700 especies de fauna y flora en peligro por la deforestación en Colombia
Date: 2017-07-11 18:00
Category: Ambiente, Voces de la Tierra
Tags: Amazonía, animales en peligro de extinción, deforestación
Slug: flora_fauna_en_peligro_deforestacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/mono_cabeciblanco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Flickr Marc32 

###### [11 Jul 2017] 

Las alarmantes cifras sobre deforestación en Colombia son más que números. Esas miles de hectáreas devastadas se traducen en la imposibilidad de que surja vida y en cambio, se pone en riesgo las especies de fauna y flora que allí habitan. De acuerdo con el Intituto Alexander von Humboldt, en los lugares identificados como núcleos de deforestación, al menos, **2.697 especies, de las cuales 503 son animales y 2194 plantas, se encuentran en riesgo debido a la tala de bosques.**

La misma entidad, asegura que ese número puede ser todavía mayor ya que en gran parte de esas áreas no existen datos biológicos o exploraciones suficientes por parte de biólogos u otros expertos. Así lo concluyó el Instituto Humboldt, a través de su Programa Evaluación y Monitoreo de la Biodiversidad, que identificó aquellas especies de flora y fauna que podrían estar presentes en Caquetá, Guaviare y Meta, Norte de Santander, Córdoba, y en el Chocó, donde se concentra gran parte de la biodiversidad del país, pero a su vez, altas tasas de deforestación.

Entre las casi 2.700 especies de flora y fauna existen algunas que se encuentran bajo una doble amenaza, pues 31 de ellas se reconocen en vía de extinción; de ellas, **5 están en la categoría Peligro Crítico, 5 en Peligro,* *y 20 son Vulnerables.**

Cabe destacar que los núcleos de deforestación coinciden con la distribución limitada a nivel mundial de **nueve especies de animales y 106 endémicas de Colombia**. La situación es preocupante y crítica en Meta y Chocó, porque concentran un mayor número de especies únicas, 16 y 81 respectivamente.

\[caption id="attachment\_43486" align="aligncenter" width="516"\]![tabla](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/tabla.png){.wp-image-43486 .size-full width="516" height="289"} Datos: Instituto von Humbolt\[/caption\]

### Algunas de las especies en amenaza 

Tipos de bosque como el caso del abarco, el almendro, los laureles almanegra, el comino y los cedros se encuentra en grave riesgo. Según explica el Instituto, *“*Las especies que pueden acelerar su extinción local por el aumento de la deforestación en sus áreas de distribución son aquellas que dependen de selva húmeda y cobertura boscosa”.

En los núcleos de deforestación de la Amazonia occidental, anfibios como las ranas arborícola y la venenosa, también se encuentran en peligro. Y en una **situación más crítica están los primates tití del Caquetá, y el mono churuco colombiano.**

En el núcleo de deforestación que afecta el Parque Nacional Natural Paramillo, en el sur de Córdoba, preocupan los anfibios como la rana marsupial y la rana de cristal; y en ese mismo departamento aves como el paujil piquiazul, las grandes rapaces, águila arpía y crestada, y la guacamaya verde. Asimismo el **tití cabeciblanco, el mamífero más emblemático y en peligro crítico** por la pérdida de bosque y el tráfico de fauna.

En el Chocó en el sector del río Quito sobresalen la rana dorada y las aves como el paujil chocoano, gavilán plomizo y el **mochilero del Baudó. Un ave rara, en peligro y endémica de este departamento.**

Por su parte, en el núcleo Catatumbo la rana marsupial y el paujil moquiamarillo han perdido la mayor parte de su hábitat en Colombia y además son objeto de cacería.

“Es importante enfatizar en los cambios dramáticos en las condiciones ecológicas de los mencionados territorios, **con posibles consecuencias irreparables sobre los servicios ecosistémicos,** claves para el bienestar de los habitantes locales de estas zonas del país”, explica el Humbolt, que también llama la atención sobre la necesidad de acelerar los procesos de obtención de información biológica en campo para determinar las consecuencias de la deforestación en la biodiversidad. (Le puede interesar: [Gobierno no se preparó para impedir altas tasas de deforestación en el posconflicto”](https://archivo.contagioradio.com/gobierno_deforestacion_posconflicto/ "“Gobierno no se preparó para impedir altas tasas de deforestación en el posconflicto”"))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
