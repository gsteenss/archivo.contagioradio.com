Title: "Aspersión terrestre con glifosato es absurda" Camilo González Posso
Date: 2018-06-27 14:59
Category: Ambiente, Movilización
Tags: coca, Cultivos de uso ilícito, Glifosato, Santos
Slug: aspersion-terrestre-con-glifosato-es-absurda-camilo-gonzalez-posso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/drones-para-fumigar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ministerio de Defensa] 

###### [27 Jun 2018] 

Como “absurda” calificó el director de Indepaz, Camilo González Posso, la aprobación del Consejo Nacional de Estupefacientes, de realizar aspersión terrestre con glifosato utilizando drones, **debido a que esta no solo violaría la prohibición que la Corte Constitucional** ya hizo sobre este tema, sino pondría en riesgo la salud de las comunidades y provocaría daños al ambiente.

“Es todo absurdo y una mentira, los drones vuelan, es una aspersión aérea. Santos usa este truco para tratar de burlar las disposiciones de la Corte Constitucional **que prohibió la aspersión aérea con glifosato para la erradicación de cultivos de uso ilícito**”, medida que es vigente y que es de obligatorio acatamiento.

Además, González Posso afirmó que debe mostrarse el plan de manejo, con qué sustancias coadyuvantes trabajarán y las autorizaciones que avalen que el uso del glifosato no es un agente cancerígeno, incluso en menores cantidades, como lo ha demostrado la Organización Mundial de la Salud, “mientras en el planeta entero está saliendo el glifosato de uso para todo tipo de procedimiento, **en Colombia ahora lo van a resucitar para una nueva guerra química”** afirmó González.

### **Los acuerdos con Estados Unidos** 

El director de Indepaz, manifestó que la estrategia para acabar con los cultivos de uso ilícito busca dar un “mensaje de satisfacción” a la administración de Estados Unidos. De igual forma, González aseguró que la cifra que dio el Ministro de Defensa Luis Carlos Villegas sobre **un total de 180 mil hectáreas en Colombia sembradas con cultivos ilícitos no tiene sustento.**

“La Oficina de Naciones Unidas contra la droga y el delito, no ha sacado su informe anual de monitoreo, que está anunciado para finales del mes de junio. Este Informe tendrá diferencias con el de la Agencia de los Estados Unidos**, porque no va a hablar de 190 mil o 180mil hectáreas**” afirmó González.

De igual forma manifestó que podría existir un incremento en la siembra de cultivos, que no tendría que ser del 23%, debido al cumplimiento por parte de la FARC de los Acuerdos de paz y a que actualmente existirían más de **27 mil familias que hacen parte de los planes de sustitución de cultivos de uso ilícito y de los procesos de reconverción económica**.

En ese sentido, González aseguró que el paso que debe dar Colombia es a dejar de seguir con políticas de persecución y penalización hacia las sustancias de uso ilícito y atacar este problema desde la Salud y la Cultura.  (Le puede interesar:["La erradicación de coca en Arauca fue hecha por los campesinos"](https://archivo.contagioradio.com/la-erradicacion-de-la-coca-en-arauca-fue-hecha-por-los-campesinos/))

<iframe id="audio_26765379" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26765379_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
