Title: Por amenazas y riesgos excombatientes de Ituango se desplazan a Mutatá
Date: 2020-07-03 19:16
Author: CtgAdm
Category: Actualidad, DDHH
Tags: etcr, excombatientes, Partido FARC
Slug: por-amenazas-y-riesgos-excombatienes-de-ituango-se-desplazan-a-mutata
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Ituango-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Espacio Territorial de Ituango @ComunesANT

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El **Partido Fuerza Alternativa Revolucionaria del Común (FARC)** dio a conocer que el próximo 15 de julio, los firmantes de paz que habitan en el municipio de **Ituango, Antioquia** iniciarán el desplazamiento masivo y traslado del ETCR Román Ruíz de la vereda Santa Lucía, hacia la vereda La Fortuna, del municipio de Mutatá a raíz de las amenazas de grupos armados hacia la población y la ausencia de garantías por parte del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según la información compartida, se trata de **al menos 100 excombatientes que se desplazarán junto con sus núcleos familiares; así como los animales y elementos de sus proyectos productivos** recorriendo en caravana un total de 300 kilómetros hasta su próximo destino en Mutatá. [(Le puede interesar: Más de 450 familias desplazadas en Ituango por enfrentamientos armados)](https://archivo.contagioradio.com/mas-de-450-familias-desplazadas-en-ituango-por-enfrentamientos-armados/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La decisión se toma, sustentándose en el informe de verificación de la ONU presentado en abril que posicionó a Antioquia como el departamento con el mayor riesgo para la vida de los excombatientes, registrando 11 reincorporados asesinados **"en Ituango conviven grupos ilegales de toda índole, sabemos que opera el Clan del Golfo, los llamados Caparrapos y disidencias, nuestra gente sale por eso, porque no fue posible hacer la reincorporación",** explicó uno de los delegados del Espacio Territorial. [(Le puede interesar: Asesinan a excombatiente de las FARC en el ETCR de Santa Lucía, Ituango)](https://archivo.contagioradio.com/asesinan-a-excombatiente-de-las-farc-en-el-etcr-de-santa-lucia-ituango/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pastor Alape, representante del Partido FARC ante el Consejo Nacional de Reincorporación (CNR), explica que el Gobierno se comprometió a arrendar con compromiso de compra dos predios para la ubicación inicial de la comunidad, además se organizarán reuniones con las autoridades de Mutatá y la Gobernación de Antioquia que se comprometió al acompañamiento de la comunidad. [(Lea también: Asesinan a Jhon Montaño, hijo de excombatiente de FARC en Algeciras, Huila)](https://archivo.contagioradio.com/asesinan-a-jhon-montano-hijo-de-excombatiente-de-farc-en-algeciras-huila/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Ituango, municipio de riesgo para excombatientes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que el espacio territorial se pensó como una zona que obtuvo desde diciembre de 2019 un contrato de arrendamiento de un predio de 500 hectáreas para la construcción del espacio territorial donde se potenciarán los trabajos sociales y agropecuarios. [(Le recomendamos leer: 500 hectáreas para la construcción del ETCR de Santa Lucía, Ituango)](https://archivo.contagioradio.com/construccion-del-etcr-de-santa-lucia-ituango/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde enero, ya se había alertado sobre el desplazamiento de al menos 62 habitantes del espacio territorial. Desde la firma del Acuerdo de Paz **han sido asesinados según el [Partido FARC](https://twitter.com/PartidoFARC) 214 excombatientes**, pese a ello, la comunidad de reincorporados afirma que se mantienen "firmes en su pacto por sembrar reconciliación". [(Le recomendamos leer: El miedo que sentíamos en la guerra, es el miedo que ahora sentimos al salir de casa: excombatientes)](https://archivo.contagioradio.com/el-miedo-que-sentiamos-en-la-guerra-es-el-miedo-que-ahora-sentimos-al-salir-de-casa-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
