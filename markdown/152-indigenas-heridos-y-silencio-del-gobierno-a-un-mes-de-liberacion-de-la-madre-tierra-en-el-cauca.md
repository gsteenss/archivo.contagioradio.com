Title: 152 indígenas heridos y silencio del gobierno a un mes de liberación de la madre tierra en el Cauca
Date: 2015-03-24 15:37
Author: CtgAdm
Category: Movilización, Nacional
Tags: Defensoría del Pueblo, Indigenas del Norte del Cauca, liberación de la madre tierra, Nasa, ONU
Slug: 152-indigenas-heridos-y-silencio-del-gobierno-a-un-mes-de-liberacion-de-la-madre-tierra-en-el-cauca
Status: published

###### Foto: eluniversal.com 

Durante la semana pasada se presentaron nuevos intentos de desalojo a las comunidades Nasa en el Norte del Cauca, sobre todo en la finca **“La emperatriz” en Caloto**, puesto que esa finca se encuentra sobre la carretera panamericana, relata Feliciano Valencia, líder indígena y vocero de las comunidades. Durante este fin de semana se ha realizado un balance que arroja la nefasta cifra de **152 indígenas heridos, dos de ellos de gravedad.**

Esta cifra y el impresionante número de agentes del ESMAD apostados en el territorio contrasta con el silencio gubernamental. Según Valencia no se han podido adelantar conversaciones o acuerdos con el gobierno a pesar de la **mediación que desde hace 2 semanas adelantan la ONU, la Defensoría del Pueblo y voceros de la ONIC.**

Durante las últimas dos semanas las comunidades indígenas en Caloto, Santander de Quilichao y Miranda se encuentran desarrollando **actividades de siembra de cultivos de Pan Coger como el fríjol y el maíz**, y se mantendrán en esa directriz, dada la ausencia del gobierno para que se cumpla la exigencia de tierras por parte de los indígenas de esa región.

Este proceso de **liberación de la Madre Tierra** se adelanta desde finales del mes de Febrero en 3 puntos de la región del **Norte del Cauca**, en fincas que las comunidades Nasa reclaman como parte dela reparación por la masacre del Nilo y como medida de cumplimiento de diversos acuerdos incumplidos durante años por el gobierno nacional.
