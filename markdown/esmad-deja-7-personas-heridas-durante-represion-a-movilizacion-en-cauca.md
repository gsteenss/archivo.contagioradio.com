Title: ESMAD hiere a una bebé recién nacida durante movilización al norte del Cauca
Date: 2016-04-26 15:40
Category: Movilización, Nacional
Tags: Cauca, ESMAD, Francia Márquez
Slug: esmad-deja-7-personas-heridas-durante-represion-a-movilizacion-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ANCOC-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACONC] 

###### [26 Abr 2016] 

La Asociación de Consejos Comunitarios del Norte del Cauca, ANCOC, denunció que durante la jornada de protesta pacífica que adelantan 2 mil personas sobre la Vía Panamericana, el ESMAD atacó la movilización dejando una persona herida por arma de fuego y seis personas más lesionadas por artefactos explosivos que disparó ese escuadrón contra la población civil.

**“Ayer estábamos pacíficamente y de una vez llegaron a agredirnos, estamos cansados de que nos desplacen, de que no respeten nuestros territorios, de que llenen nuestros ríos de mercurio y que la gente se esté muriendo”**, expresó Francia Márquez, vocera del Movimiento Mujeres Afrocolombianas por la Vida y los Territorios Ancestrales, quien vivió la arremetida del ESMAD, y quien asegura que la represión ha sido la única respuesta del gobierno nacional a las exigencias de las comunidades negras.

Las comunidades negras denuncian que el **Ministerio del Interior y su dirección de Consulta Previa, niegan la presencia de la población negra al norte del Cauca** sin tener en cuenta que esas comunidades son ancestrales y habitan sus territorios desde 1636. Por otra parte, se denuncia que con la Ley Páez se creó una zona franca que solo se terminó beneficiando a las empresas privadas; asimismo, piden que se realice consulta previa por la construcción de la doble calzada de Villa Rica – Santander de Quilichao – Popayán; entre otras exigencias.

Niñas, niños, mujeres y adultos mayores resultaron atemorizados por los ataques del ESMAD, mientras se movilizaban por el centro histórico de Quinamayo en Santander de Quilichao. **A esta hora permanecen en asamblea permanente las más de 2 mil personas  sobre la vía Panamaricana** en uno solo de los carriles y custodiados por ESMAD. La líder afrocolombiana, aseguró que no se moverán del lugar hasta que no haya presencia de  altos funcionarios del gobierno que atiendan sus exigencias.

**“Somos una comunidad pacífica y de manera pacífica se han logrado acuerdos firmados por ministros que no se han cumplido”,** dice Márquez quien agrega, “Pareciera que nosotros no le hubiéramos aportado a este país, lo que vemos es racismos estructural y discriminación estatal”.

### **Última hora:** 

Tras permanecer sobre la vía Panamericana en asamblea permanente esperando respuestas a sus peticiones por parte del gobierno nacional, el ESMAD empezó a atacar con gases lacrimógenos a la comunidad para que desalojaran la carretera. En ese momento, las personas salen corriendo a resguardarse en una casa que quedaba arriba de la carretera a donde es arrojado un gas que hiere a una bebé de aproximadamente 3 semanas de nacida que habitaba la casa.

Por el momento se desconoce el estado de salud de la pequeña quien fue llevada a un centro hospitalario y además se pudo establecer que otra menor de edad salió herida y otras 14 personas fuero detenidas.

<iframe src="http://co.ivoox.com/es/player_ej_11314930_2_1.html?data=kpagk5mdd5Ghhpywj5eUaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncafmwtPQy8aPkYa3lIquk9fVucbujJKYr9rOqdPZ1JCuyNfTp9Dg0NLPy8bSpdSf0dTfjdHFb7fdxcaYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
