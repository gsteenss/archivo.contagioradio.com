Title: 5 campesinos asesinados por paramilitares en el Bajo Atrato chocoano
Date: 2016-01-13 17:03
Category: DDHH, Nacional
Tags: asesinato de campesinos en Salaquí, Bajo Atrato chocoano, Paramilitares en Chocó
Slug: 5-campesinos-asesinados-por-paramilitares-en-el-bajo-atrato-chocoano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: vivelohoy.com 

###### [13 Ene 2016]

[De acuerdo con la denuncia de la ‘Comisión Intereclesial de Justicia y Paz’, el pasado 10 de enero integrantes de grupos **paramilitares asesinaron a 5  afrodescendientes**, en el trayecto entre ‘Caño Seco’ y ‘Playa Bonita’, río Salaquí, Bajo Atrato chocoano, acusándolos de ser colaboradores de la guerrilla.]

[Pobladores aseveran que los cerca de 1000 paramilitares se asentaron en los corredores entre Truandó, Salaquí y Cacarica. Algunos de ellos se trasladaron desde ‘Playa Bonita’ para apoyar **una incursión en las Zonas Humanitarias ‘Nueva Vida’ y ‘Nueva Esperanza en Dios’,** y otros cerca de 250 arribaron a la comunidad de ‘Caño Seco’, desde el pasado 7 de enero.  ]

[Los paramilitares que se presentaron como "Gaitanistas" reunieron a la comunidad y **afirmaron que no se iban a retirar del territorio, que tenían un proyecto de desarrollo y progreso para la región** y que contaban con apoyo en Ríosucio. Luego, retuvieron e interrogaron por 4 horas a 2 pobladores a quienes señalaron de ser colaboradores de la guerrilla de las FARC-EP.]

[Durante el último trimestre del 2015 el **Estado Mayor de las FFMM conoció directamente de la [presencia paramilitar en la región](https://archivo.contagioradio.com/paramilitares-amenazan-con-tomar-el-control-del-bajo-atrato-chocoano/)** y asumió el compromiso de actuar, sin embargo, estos hechos demuestran que **ninguna medida eficaz se ha adoptado**, pese también, a las decisiones de la CIDH en el Caso Génesis, que ordena implementar mecanismos de protección para los afromestizos de Cacarica.]
