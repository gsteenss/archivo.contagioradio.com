Title: “100 Años de Soledad, el Viaje de Elisa” ballet para todos en Bogotá
Date: 2017-04-29 15:06
Category: Uncategorized
Tags: ballet, Bogotá, Cultura, eventos
Slug: ballet-bogota-garcia-marquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/image001.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Escuela MME 

###### 29 Abr 2017 

La Fundación Escuela de Artes MME presenta “**100 Años de Soledad, El Viaje de Elisa**” una pieza de belleza y elegancia que a través del ballet le da vida a la afamada novela del nobel de literatura Gabriel García Márquez y el libro Expedición Macondo de Irene Vasco.

Esta pieza artística relata con gran fantasía el recuerdo de Elisa, una célebre y soñadora escritora, que desde su experiencia lectora con la espectacular obra Cien años de Soledad cuando era tan solo una adolescente forja un maravilloso futuro, en dónde el público podrá disfrutar de momentos icónicos, como la fundación de Macondo, la llegada de los gitanos y los diferentes relatos de los integrantes de la familia, presentados a partir de bellas coreografías de ballet.

La Fundación Escuela de Artes MME más allá de ser una organización que diseña, construye y promueve espacios incluyentes para la formación artística de niños y jóvenes en danza, música y artes plásticas, busca con esta obra promover un programa de BECAS auto gestionadas para chic@s, para acobijar a ocho estudiantes de ballet entre los 10 y 19 años, que debido a la ausencia de recursos económicos no podrían costear por sus propios medios para un proceso de formación artística.

Cien años de soledad, el viaje de Elisa tendrá solo dos funciones, el domingo 30 de abril a las 3:30 p.m. y 5: 30p.m en Factoría L’EXPLOSE Carrera 25 \# 50-34 con un bono de apoyo de \$15.000 y el domingo 7 de mayo a las 5:00 p.m. en el Centro Comercial Plaza las Américas. Le puede interesar: [Juguetes cantados: un viaje con humor por ritmos y aires musicales](https://archivo.contagioradio.com/juguetes-cantados-teatro/).
