Title: "Duque puede contar con nosotros para construir paz" Encuentro por la paz de Colombia
Date: 2018-08-03 15:41
Category: Movilización, Nacional
Tags: ELN, Mesa de La Habana
Slug: duque-puede-contar-con-nosotros-para-construir-paz-encuentro-nacional-por-la-paz-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/justapaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justapaz] 

###### [03 Agosto 2018] 

Más de mil personas integrantes de diversos sectores sociales, corrientes políticas y víctimas del conflicto armado, se reunieron en el Encuentro Nacional por la Paz de Colombia para pedirle al electo presidente Iván Duque que continúe con la mesa de diálogos con la guerrilla del ELN, **sin intentar el sometimiento o condicionamiento del proceso,  y de trámite y cumplimiento al Acuerdo de Paz**.

El Encuentro, que contó la participación de líderes sociales de diferentes territorios del país, le hizo un llamado a Duque frente al proceso de paz con el ELN, en el que le expresan que **"esas conversaciones son fundamentales para cerrar el ciclo de la violencia política en el país y dar paso a una fase de transformaciones pacíficas**", razón por la cual consideran que el inicio del nuevo mandato, puede ser el punto de partida para construir el camino a la paz, desde múltiples orillas.

De igual forma, afirmaron que existen avances importantes en la mesa de La Habana, además del primer cese al fuego bilateral y los dos ceses al fuego unilaterales, como la construcción del mecanismo de participación para la sociedad civil. En esa medida pidieron que se **"salvaguarde la posibilidad de un nuevo cese bilateral al fuego y que su Gobierno designe una delegación para dar continuidad a la Mesa"**. (Le puede interesar:["Duque podría darle una oportunidad al proceso de paz con el ELN: De Cuerrea")](https://archivo.contagioradio.com/duque-podria-darle-una-oportunidad-al-proceso-de-paz-con-el-eln-de-currea/)

Finalmente convocaron a la sociedad en general a realizar un esfuerzo de creatividad, compromiso, rigor y generosidad, para **"hacer valer el derecho y la obligación de la paz previstos en la Constitución Política",** y le hicieron saber a Duque que puede contar con las más de mil personas que firmaron el llamado y participaron en el Encuentro, "para contribuir con todos los esfuerzos que se hagan desde el gobierno nacional, el Congreso de la República, el empresariado y las organizaciones sociales para avanzar en la construcción de la paz".

###### Reciba toda la información de Contagio Radio en [[su correo]
