Title: Hablan por las víctimas pero cuando van al Congreso no las escuchan
Date: 2019-04-02 18:20
Category: Paz, Política
Tags: Congreso, JEP, paz, víctimas
Slug: victimas-congreso-escuchan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Congreso-No-atiende-a-víctimas-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @DavidRacero] 

###### [2 Abr 2019] 

Este lunes se llevó a cabo en la Cámara de Representantes una audiencia sobre las objeciones presidenciales a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP), evento en el que se escucharon las voces del Fiscal General, integrantes del Gobierno y organizaciones de la sociedad civil, entre ellas movimientos de víctimas. En esta corporación es probable que se rechacen las objeciones, sin embargo, lo que llamó la atención de la audiencia fue la actitud de los congresistas frente a los invitados al recinto.

Erick Arellana, vocero del Movimiento Nacional de Víctimas de Crímenes de Estado y participante de la audiencia, recordó que en otras ocasiones el Congreso tuvo mayor asistencia de parlamentarios a este tipo de eventos; no obstante, una constante ha sido el hacer 'oídos sordos a las voces de las víctimas'. A ello se suma una tendencia identificada por Arellana, con la que se quiere desconocer a otra víctimas que no sean de las FARC.

El integrante del MOVICE afirmó que había un orden establecido de participación en la Audiencia, en la que primero hablarían las víctimas de las FARC y posteriormente los voceros de su Movimiento, pero el discurso del Fiscal (que tomó el doble de tiempo asignado para cada intervención) alteró el curso del evento. (Le puede interesar: ["Duro reclamo tras objeciones de Gobierno Duque contra la JEP"](https://archivo.contagioradio.com/dura-carta-victimas-tras-objeciones-gobierno-duque-la-jep/))

### **"Que se respalde y se impulse la JEP"** 

Arellana señaló que el mensaje de las víctimas iba encaminado a apoyar las instituciones que integran el Sistema Integral de Verdad, Justicia, Reparación y No Repetición; por lo que pidieron "que se respalde y se impulse la JEP", como forma de garantizar sus derechos y poder tener acceso a la verdad de lo ocurrido en el conflicto. (Le puede interesar: ["Antioquia y Eje Cafetero, listos para trabajar junto a la Comisión de la Verdad"](https://archivo.contagioradio.com/antioquia-y-eje-cafetero-listos-para-aportar-sus-relatos-a-la-comision-de-la-verdad/))

Por esta razón, le pidieron al Gobierno que no hable en nombre de las víctimas como lo ha hecho supuestamente, al presentar las objeciones a la Ley Estatutaria o cuestionar la Jurisdicción; y más aún, en el desarrollo del presupuesto para la reparación de las víctimas que quedó recortado en el Plan Nacional de Desarrollo. (Le puede interesar: ["Oposición y Gobierno miden fuerzas en el Congreso por Plan Nacional de Desarrollo y JEP"](https://archivo.contagioradio.com/congreso-por-plan-nacional-de-desarrollo-y-jep/))

### **El doble discurso del Gobierno sobre la paz** 

En la Cámara de Representantes las objeciones probablemente serán rechazadas, en el Senado, las fuerzas políticas parecen estar en la misma dirección tras la decisión de Cambio Radical de no apoyar en conjunto los reparos; a ello se añade la labor de la oposición en el Congreso, que Arellana destacó, y agregó que estaba fomentando la participación de otras voces en los debates.

> Hoy radicamos el rechazo de las objeciones a la JEP y la insistencia en el texto de la Corte.
>
> Un gran pacto por la paz no se construye a punta de objeciones.
>
> Se construye llamando a todas las fuerzas políticas para transformar los territorios más afectados por la guerra. [pic.twitter.com/227VDwTyDz](https://t.co/227VDwTyDz)
>
> — Juanita Goebertus (@JuanitaGoe) [2 de abril de 2019](https://twitter.com/JuanitaGoe/status/1113124285541908480?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Sin embargo, uno de los hechos destacados por el Activista es que el discurso del Gobierno intenta "disminuir el juego sucio que hacen" a través de medios de comunicación y redes sociales señalando que apoyan a las víctimas; mientras atacan al Sistema creado para garantizar sus derechos. Situación que se ve ejemplificada en el argumento esgrimido por el Fiscal para aprobar la Ley Estatutaria sin las objeciones: por el temor a posibles intervenciones de la Corte Penal Internacional; o el anunció del partido de Gobierno (Centro Democrático), de intentar impulsar un Proyecto de Acto Legislativo para modificar la JEP.

<iframe id="audio_34096789" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34096789_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
