Title: Batucada feminista "La tremenda revoltosa¨conmemoró el día de la mujer trabajadora
Date: 2015-03-09 21:51
Author: CtgAdm
Category: Mujer, Nacional
Tags: Batucada "La tremenda revoltosa", Dia de la mujer trabajadora Bogotá, iglesia de lourdes
Slug: batucada-feminista-la-tremenda-revoltosa%c2%a8-se-tomo-la-plaza-de-lurdes-en-conmemoracion-del-dia-de-la-mujer-trabajadora
Status: published

##### Foto:Batucada feminista "La tremenda revoltosa" 

###### **Entrevista con[ Kellyn Duarte y Gabriela Díaz]**: 

<iframe src="http://www.ivoox.com/player_ek_4189781_2_1.html?data=lZalm5ycdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhsLo1sjOxsaPqsbhytPW1dnFb9TZjNncz8aPsMKfyszZx9jNpYzYxpC519fIqdSf0cbfw5DHs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**¨La tremenda revoltosa¨, batucada feminista,  ¨Gafas violetas¨, ¨Tejidos al viento¨, "La Tulpa" e Hijas Bogotá** realizaron este sábado una **acción** con el objetivo de iniciar una  jornada de lucha en conmemoración del **día de la mujer trabajadora en Bogotá**.

A las 19h del sábado y, en la céntrica plaza de la Iglesia de Lourdes en el barrio Chapinero se realizó la acción que contó con un comunicado a favor de los **derechos humanos de las mujeres, en contra del patriarcado y de las políticas que atacan sus derechos**.

La batucada amenizó la noche con la música y cantos a favor de la liberación de la mujer. El cierre de la acción contó con una performance **denunciando la violencia de género y homenajeando a las víctimas.**

Contagio Radio conversó con **Kellyn Duarte y Gabriela Díaz** de Hijas Bogotá, quienes nos explican el descontento y la **falta de respeto** con la conmemoración **del día de la mujer trabajadora, al realizar la ¨Marcha por la vida digna¨  convocada por el ex alcalde de Bogotá Antanas Mockus**.

Por otra parte nos explican la continuidad de conductas patriarcales y micromachismos dentro de los **movimientos sociales y las organizaciones de derechos humanos**, así mismo invita a  a hombres y mujeres a acabar con dichas conductas y la necesidad de la lucha conjunta.
