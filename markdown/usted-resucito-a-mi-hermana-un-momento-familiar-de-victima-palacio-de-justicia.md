Title: Actores revivieron por un momento víctimas del Palacio de Justicia
Date: 2015-10-30 16:33
Category: Sin Olvido
Tags: 6 de noviembre de 1985, Actores Colombianos, Derechos Humanos, Homenaje a víctimas del palacio de justicia, Irma Franco, Luz Estela Luengas, M-19, Palacio de Justicia, Radio derechos Humanos, Retoma del Palacio de Justicia, Víctimas del Palacio de justicia
Slug: usted-resucito-a-mi-hermana-un-momento-familiar-de-victima-palacio-de-justicia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Luz-Stella.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:publimetro.com 

<iframe src="http://www.ivoox.com/player_ek_9223100_2_1.html?data=mpeflZaUdI6ZmKiak52Jd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmdToxsmY1MrXucTd1YqwlYqmd4zVjNLWjc3Jts7Vz8aY19OPsdDhxtPh0YqXhYy6wtLWzs7FtozYxpDjh6iXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luz Stella Luengas, actriz] 

###### [30 oct 2015]

[Durante la presentación del audiovisual "**[30 años esperando justicia](https://archivo.contagioradio.com/homenaje-actores-palacio-de-justicia-homenaje-victimas/)**" dedicado a la memoria de los desaparecidos el 6 y 7 noviembre de 1985 en el Palacio de Justicia y a sus familias, se vivieron emotivas expresiones de solidaridad y agradecimiento, entorno al proyecto que permitió por unos minutos dar voz a quienes han permanecido ausentes por 30 años.]

Socorro, hermana de Irma Franco manifestó su agradecimiento a la actriz colombiana Luz Stella Luengas, encargada de encarnar en video a la desaparecida simpatizante del M19, por **resucitarla un momento**, relatando en primera persona parte de su historia, metas y sueños truncados por quienes la condujeron viva fuera del Palacio y luego la torturaron.

Para la actriz colombiana, el **homenaje sirve como muestra de la “necesidad absoluta de que se esclarezca todo lo que sucedió**”, aportando con su caracterización a la representación simbólica de por lo menos siete millones de víctimas que ha dejado "todo este absurdo y ridículo conflicto y esta guerra”.

El resultado del trabajo de los 15 actores y actrices vinculados al proyecto, representa  “un apoyo, una ayuda que nos permite exigir junto con los familiares el derecho que todos tenemos a la verdad y a la justicia” asegura Luengas  y agrega que “no podemos ser ignorantes ante tanto dolor de nuestros compatriotas” asumiendo desde su profesión un compromiso con las víctimas y sus familias.

Para la fecha de la toma y posterior retoma del Palacio de Justicia, Luz Stella aún era estudiante de teatro, evento que desde entonces despertaría en ella “un compromiso muy grande con todo el tema de la guerra en Colombia y las víctimas” y de su arte con la gente para “hacer un trabajo dirigido hacia ese público, y construir sociedad contribuyendo a que todos en Colombia tengamos un mejor país”.

La actriz resalta además el papel fundamental de la reconciliación para alcanzar la anhelada paz, ante lo cual se debe reconocer y **exigir al Estado colombiano y a todos los responsables de las víctimas "una explicación y una justificación, y que haya justicia** para todos nosotros" búsqueda que debe involucrar a todos los sectores de la sociedad nacional.

Frente al papel de los medios de información en la construcción de la memoria, Luengas indica que pese a que se han encargado de “ocultar, de enredar, de dar las noticias a sus conveniencias políticas y económicas, **debemos aceptar que “en una democracia todos tenemos derecho a estar”** y que debemos “reconocer en el supuesto enemigo a un ser humano”.

En cuanto a la verdad, su opinión apunta a establecer acuerdos donde los responsables de los delitos vinculados con la guerra "pongan la cara y asuman su responsabilidad", y subraya la necesidad de que todo esto "no se repita" alcanzando compromisos reales con la vida de todos y todas.

**Ver también:** [Homenaje Víctimas Palacio de Justicia](https://archivo.contagioradio.com/homenaje-actores-palacio-de-justicia-homenaje-victimas/)
