Title: Patrimonio arqueológico de comunidad Nasa en riesgo por exploración petrolera
Date: 2016-07-06 17:34
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Comunidad Nasa, Consulta Previa, Exploración Petrolera, Putumayo
Slug: patrimonio-arqueologico-riesgo-nasa-26122
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/putumayo-arqueologia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kiwnas Cxhab] 

###### [6 jul 2016]

En un comunicado público el resguardo Nasa **Kiwnas Cxhab** del Putumayo denunció que la empresa Gran Tierra profanó un sitio sagrado en el que se hallaron arqueológicos restos considerados patrimonio sagrado de la comunidad, y están en riesgo por los trabajos de la empresa. Además afirman que el **Ministerio del Interior desconoció la presencia de comunidades** en la zona donde adjudicó una licencia ambiental para el desarrollo del proyecto.

Concretamente, según el comunicado público, el pasado 1 de Julio fueron informados del hallazgo de vestigios arqueológicos en el sitio en que se desarrolla el proyecto y que de inmediato decidieron movilizar a la guardia indígena para verificar, y constataron que hubo profanación del territorio sagrado. Según los primeros informes en el **sitio, se hallaron varios vestigios que están siendo expuestos por trabajadores que se encuentran en el lugar.**

La comunidad indígena asegura que aunque siempre se han opuesto al desarrollo de este tipo de proyectos en sus territorios y que el Ministerio del Interior ha desconocido su presencia, la **empresa Gran Tierra está desarrollando las actividades de exploración** en el “Área de perforación exploratoria Cumplidor - Bloque PUT-7, licenciado mediante Resolución 0073 del 27 de enero de 2016”.

Los indígenas Nasa exigen la presencia de los entes de control y de las autoridades, así como del **Instituto Colombiano de Antropología e Historia,** puesto que los hallazgos hacen parte del patrimonio arqueológico de la nación y del pueblo Nasa del Putumayo y de Nariño, legítimos propietarios de las tierras en que se desarrolla la actividad de la empresa.
