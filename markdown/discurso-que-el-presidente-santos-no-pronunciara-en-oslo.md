Title: Discurso que el Presidente Santos no pronunciará en Oslo
Date: 2016-12-09 10:07
Category: Nacional, Paz
Tags: Premio nobel de paz
Slug: discurso-que-el-presidente-santos-no-pronunciara-en-oslo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/entrega-premio-nobel-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Presidencia 

Algunos vecinos de la localidad de Teusaquillo en Bogotá, agrupados en Paz/Haremos, se dieron a imaginar lo que pudiera ser el **discurso del presidente Juan Manuel Santos en la recepción del Nobel de Paz**. Este es el resultado.

**Su Majestad Harald V,**

**Señores del Comité Noruego del Premio Nobel de Paz,**

[Señoras y señores,]

[Vengo ante ustedes como mensajero de la esperanza, como vocero del más profundo anhelo de las mujeres y los hombres de mi país: lograr una Colombia en paz. No ostento otro mérito para estar hoy ante ustedes que haber contribuido a realizar esta aspiración esquiva pues hasta ahora se escapó cuando quiera  que estuvo a nuestro alcance.]

[Ante todo, debo agradecer al Reino de Noruega, en cabeza de su jefe de Estado, el compromiso con la paz de nuestro país. No ha sido un comportamiento  de última hora: por décadas, hemos contado con la solidaridad de su gobierno y de su pueblo, que han apoyado iniciativas de paz de la sociedad civil, han tendido puentes entre las partes concernidas en el conflicto y en su superación y han sido garantes del proceso de diálogo y negociación que hemos cerrado con buen suceso. En nombre de mi pueblo, reitero los sentimientos de reconocimiento y  gratitud.]

[Sea el momento de agradecer la valiosa contribución del gobierno y el pueblo cubanos, a los que acompañamos solidarios en esta coyuntura luctuosa.  La paz de Colombia estará asociada para siempre al nombre de Cuba, en cuyos gobernantes siempre se encontró hospitalidad y estímulo.    ]

[El 8 de diciembre de 1982, en circunstancias similares a las de hoy, un conciudadano nuestro hizo el inventario de nuestras desgracias.  Contó que no hemos tenido un instante de sosiego, pero también que a pesar de ese destino, nuestro futuro estaba atado a la defensa de la vida. Proclamó: “Ni los diluvios ni las pestes, ni las hambrunas ni los cataclismos, ni siquiera las guerras eternas a través de los siglos y los siglos han conseguido reducir la ventaja tenaz de la vida sobre la muerte”. De esa fecha a hoy, ese inventario no sólo se incrementó sino que alcanzó simas de degradación que tenemos la obligación de recordar y de contar.]

[Se cuentan en varios centenares de miles las personas sacrificadas en el altar de la guerra, con la crudelísima secuela de madres que perdieron a sus hijos, de viudas y de huérfanos. Son más de sesenta mil los desaparecidos en razón de la contienda fratricida. Casi cuarenta mil de nuestros compatriotas han padecido el secuestro. Seis de cada diez municipios han sido sembrados de minas antipersonas. Decenas de billones de pesos hubo de derramar el país en el sifón insaciable de la contienda armada, recursos que debieron servir para dar educación, techo, salud y oportunidades en uno de los países con mayores desigualdades del planeta. Es “el tamaño de nuestra soledad” como la denominó el más grande de los colombianos del siglo XX, la pesadilla irracional de la que todos los colombianos sin excepción hemos sido protagonistas como actores o como víctimas.]

[Tan dolorosas manifestaciones son síntoma de las profundas dolencias que aquejan a la sociedad colombiana, que durante siglos ha venido construyéndose en la exclusión, la discriminación y la intolerancia, matrices generadoras de pobreza,  violencia y antidemocracia.    ]

[Dicho lo anterior, cabe afirmar que la paz no sólo será la superación de una desventurada época de barbarie, sino la apertura a un tiempo de oportunidades en el que nuestro país podrá ver florecer todas sus potencialidades en los terrenos del arte y el conocimiento, en la generación de riqueza y vida buena. De las ruinas de la guerra, surgirá un país renovado en el que todos sus habitantes deberán gozar a plenitud de sus derechos.]

[Por cuatro años, las delegaciones del gobierno y de las FARC crearon este campo de posibilidad tejiendo con paciencia y sin pausa coincidencias y poniendo entre paréntesis décadas de distanciamientos y profundos desacuerdos. No fue una tarea fácil: hubo que construir confianzas, acercar posiciones, discernir lo principal y lo accesorio, de modo que lo secundario cediera el paso a lo fundamental.   ]

[Todo esto fue posible, en buena medida, por la generosidad de los interlocutores del gobierno en este diálogo prolongado. Renunciaron a planteamientos que cultivaron por décadas, aplazaron parte de sus aspiraciones, incluso se sobrepusieron al sacrificio de su máximo comandante. Por la paz, se abrieron a reformular los acuerdos suscritos luego del insuceso del reciente plebiscito. Lo cierto es que hemos tenido una contraparte magnánima y ajena a toda mezquindad.]

[Personalmente, debo agradecer a las mujeres y hombres amantes de la paz que vieron en este servidor un artesano de la concordia, comprometido con la reconstrucción de la convivencia.  Así recibí su apoyo sin condicionamientos,  sin exigencia alguna ni expectativas indecorosas. Este desprendimiento allanó el camino de la reconciliación.  ]

[El proceso que vamos cerrando también es fruto de las varias tentativas en la búsqueda de la paz, algunas coronadas por el éxito,  otras con  resultados insatisfactorios. Unas y otras fueron sembrando la esperanza. A los compatriotas que con éxito construyeron procesos de reconciliación, debemos reconocerles la condición de artífices de la nación renacida.  ]

[Durante décadas, miles de mujeres y hombres,  hicieron pedagogía de la paz, adelantaron iniciativas ciudadanas para  desinflar el globo de la guerra y con creatividad posicionaron símbolos y conceptos que abrieron esta oportunidad para nuestra patria. Es de resaltar el aporte invaluable de la migración y el exilio colombianos que a manera de avanzada de paz fueron portavoces de la paz por fuera del país.  ]

[Mención especial debo hacer de los miles de jóvenes que irrumpieron en las calles y las plazas de mi país, a reafirmar su fe en el futuro cuando en razón de los resultados del plebiscito, buena parte de los colombianos eran presa del pesimismo y el abatimiento.]

[Tanta generosidad, tan terca obstinación, tanta creatividad persistente, no siempre encuentran un eco propicio. Hay por desgracia quienes parecieran no comprender la significación histórica de la paz,  presos en la estrechez de sus intereses, de sus prejuicios y de su mala voluntad, como si pretendieran que su país viviera por siempre la pesadilla que ha estado padeciendo. En procura de este designio, no parecieran escatimar ningún recurso: la palabra insidiosa, la mentira desvergonzada, hasta el atentado letal. Han dicho sin cordura ni reato que su propósito es hacer invivible la república.   ]

[Por lo mismo, muchos encuentran el horizonte surcado de oscuros nubarrones, como si retornaran los días aciagos en los que los enemigos agazapados de la paz, como los denominó el ex ministro de la carcajada homérica, cerraron el camino a la reconciliación.  ]

[Debo expresar mi absoluta confianza en que esta vez no tendrán éxito.  Millones de colombianos —yo entre ellos— estamos jugados por la concordia. Muchos de los que parecieran ser hostiles a la paz,  empiezan a recapacitar y serán bienvenidos al campo de los que se proponen contribuir a deponer los odios y acercar las voluntades para reconstruir la nación. Me acompaña la certeza de que la paz no naufragará porque ésta es la decisión de la gente de buena voluntad, que por fortuna es la más numerosa. Como es también aspiración suya que la paz sea completa porque las conversaciones con el Ejército de Liberación Nacional lleguen a un feliz término.  Hago votos porque los obstáculos que han aparecido  puedan superarse y se pueda llegar a un pacto que afiance la paz.   ]

[Colombia tiene un acuerdo para construir una paz estable y duradera. Nos hubiera gustado que Gabriel García Márquez estuviera aquí a nuestro lado anunciando esta buena nueva. Aunque nuestras desgracias las padecimos en soledad, cuando nos embarcamos en esta magnífica aventura estuvimos acompañados por la amistad y la solidaridad de la comunidad internacional. Esperamos contar con su concurso para sobreponernos a las dificultades y  lograr una paz sólida porque tiene a la justicia por fundamento y llamada a perdurar porque conduce a una sociedad  y un Estado en los que haya espacio para todos los hijos de Colombia.   ]

[Muchas gracias.]

[Discurso que el Presidente Santos no pronunciará en Oslo](https://www.scribd.com/document/333733936/Discurso-que-el-Presidente-Santos-no-pronunciara-en-Oslo#from_embed "View Discurso que el Presidente Santos no pronunciará en Oslo on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_72356" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/333733936/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-XiESbJRfvGGSqWJlHbUP&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6188186813186813"></iframe>
