Title: Ejército dispara contra jóvenes en Teorama, Norte de Santander
Date: 2016-02-17 17:00
Category: DDHH, Nacional
Tags: Ejército Nacional, Norte de Santander, Toerama
Slug: ejercito-dispara-contra-jovenes-en-teorama-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Teorama-Militares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Área Cúcuta] 

<iframe src="http://www.ivoox.com/player_ek_10473774_2_1.html?data=kpWhmZibe5Whhpywj5WbaZS1kZqah5yncZOhhpywj5WRaZi3jpWah5yncabehqigh6adtsTd1dSYxs7XtMLmwpDQ0dPYtsKfy4qwlYqmd9fZz8rgjcrSb7XZ0NfOz8aJdqSfr9Tf1sqPqMafjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Juan Carlos Quintero, ASCAMCAT] 

###### [17 Feb 2016]

El domingo 15 de febrero soldados del Ejército Nacional dispararon contra Jean Carlos Serrano y Vladimir Guerrero, jóvenes de 17 y 18 años, cuando llegaban a su residencia ubicada en la vereda Puente Amarillo, del municipio de Teorama, Norte de Santander. El primer joven recibió cuatro impactos de bala en sus extremidades inferiores y el segundo, resultó herido en su pierna derecha. Actualmente se encuentran estables.

Juan Carlos Quintero, integrante de ASCAMCAT, asegura que los dos jóvenes heridos han recibido amenazas por parte de integrantes de la Polícia, quienes han manifestado serias intenciones de judicializarlos, mientras se encuentran recluidos en el hospital Emiro Quintero Cañizares de Ocaña. Por su parte el Ejército en un comunicado público aseveró que sus miembros habían encontrado a los jóvenes en un camino de la vereda y que allí se vieron obligados a dispararles.

Este suceso ocurre en el marco de las operaciones que polícias, militares y agentes de la DEA vienen adelantando en el corregimiento El Aserrío, jurisdicción de Toerama, Norte de Santander, que han conllevado a restricciones en la movilidad para los campesinos y continúos bombardeos, frente a los que las comunidades han emprendido acciones de hecho como impedir actividades en el oleoducto Caño Limón Coveñas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
