Title: Ultras agreden a manifestantes en día del orgullo gay en España
Date: 2017-06-19 11:54
Category: El mundo, Otra Mirada
Tags: españa, homofobia, lgtbi, ultraderechistas
Slug: orgullo-gayhomofobiaespana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/lo_nuestros_desfile.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La crónica 

###### 19 Jun 2017 

En el marco de las manifestaciones por el Día del orgullo gay en la ciudad española de Murcia, **un grupo de ultraderechistas del grupo denominado "Lo nuestro" agredió a quienes participaban de las actividades en ese lugar**, utilizando para tal fin palos y banderas de España.

De acuerdo con la información, la delegación de la ciudad había autorizado una concentración del grupo neonazi, pidiéndoles que se desplazaran para que no coincidieran con los puntos por los que circularían los desfiles. Sin embargo **a pesar de la parcial escolta policial varios manifestantes denunciaron ser víctimas de agresión**.

Integrantes de "Lo nuestro", agrupación que pide comida para ciudadanos españoles, irrumpieron en la marcha antes de que esta tomara camino, provocando que **algunos manifestantes salieran huyendo del lugar**. Se reporta que entre los heridos se encuentra Israel Sánchez, fotográfo del periódico "La opinión" de Murcia. Le puede interesar: [Paramilitares amenazan a víctimas colombianas exiliadas en España](https://archivo.contagioradio.com/amenazas-de-paramilitares-llegan-a-las-victimas-colombianas-exiliadas-en-espana/).

Desde movimientos políticos como Podemos, PSOE e Izquierda Unida, se ha elevando una exigencia para que **el Delegado del Gobierno Antonio Sánchez Solís dimita de su cargo** por permitir una manifestación fascista el mismo día y casi a la misma hora en que se conmemora el orgullo gay en la ciudad, mientras que desde Cambiemos se ha catalogado su actuación como una "irresponsabilidad".

Las críticas también han estado direccionadas a la actuación policial contra quienes participaban pacíficamente del desfile, de manera paralela a las agresiones de los ultras contra ellos **sin que alguno haya sido identificado o detenido**, mientras que han sido multados activistas de Izquierda Unida y del Partido Comunista de España, por no portar su DNI, algunos de ellos diputados, quienes según la denuncia fueron tratados como delincuentes desconociendo sus acreditaciones parlamentarias.

<div class="article-body">

</div>

<div class="article-tags">

</div>
