Title: Mueren niños por desnutrición en Reserva Indígena de Caño Mochuelo, Casanare
Date: 2019-09-06 15:01
Author: CtgAdm
Category: DDHH, Nacional
Tags: Acceso a Salud, Caño Mochuelo, comunidades, desnutrición
Slug: muerte-por-desnutricion-ninos-cano-mochuelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/8507258401_a1b4707a0b_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Derechos Indígenas] 

Las autoridades de la Reserva Indígena de Caño Mochuelo, situado en el departamento de Casanare, emitieron un comunicado denunciando **la muerte de niños por desnutrición, **así como las malas condiciones de salubridad. Concretamente, dos menores fallecieron en el transcurso de una semana.“Las comunidades carecen de la necesidad de agua potable” y “la entidad de salud no cubre en su totalidad la atención a la población”, alerta Alexander Tudupial Tabutju, representante del Cabildo indígena de la Reserva. (Le puede interesar: [Indígenas Jiw del Meta completan 5 meses sin recibir atención humanitaria](https://archivo.contagioradio.com/indigenas-jiw-del-meta-completan-5-meses-sin-recibir-atencion-humanitaria/))

Comunidades como Caño Mochuelo presentan un abandono estatal. Es por ello que en el documento apelan a entidades públicas, privadas nacionales, departamentales e internacionales y exigen un diálogo entre las instituciones y las autoridades indígenas de la zona.

Denuncian que “el Instituto Colombiano de Bienestar Familiar (ICBF) es la única entidad que ha hecho presencia en nuestras comunidades” y que no han contado con el respaldo del gobierno colombiano ante esta **crisis nutricional**. El Plan Departamental de Seguridad Alimentaria y Nutricional del Casanare "Bastimento" 2011-2020 muestra datos alarmantes: más de la mitad de los menores indígenas valorados presenta desnutrición crónica y tan solo el 16,6% presenta un peso normal.

Según el Sistema de Información Indígena de Colombia, la infraestructura en la zona tiene una absoluta deficiencia en dotación. Además, el Plan de Salvaguarda de los Pueblos Indígenas del Resguardo Caño Mochuelo alerta que "las comunidades no están en capacidad de reclamar sus derechos frente a la salud porque no se tiene la suficiente claridad sobre la estructura y el funcionamiento del sistema de salud nacional ni el relacionado con los pueblos indígenas".

> **“Rechazamos la muerte de nuestros niños por desnutrición y mal estado de salud en nuestro resguardo se nos están acabando” - Alexander Tudupial Tabutju, Cabildo indígena de la Reserva**

La protección de las comunidades indígenas es fundamental para la **preservación de su cultura, sus territorios así como su soberanía alimentaria**. Es por ello que denuncian la falta de apoyo y protección y exigen una respuesta inmediata por parte de las autoridades. (Le puede interesar: [Organizaciones indígenas insisten en el diálogo pese a histórico incumplimiento](https://archivo.contagioradio.com/proteccion-de-lideres-indigenas-en-colombia-una-deuda-del-gobierno/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
