Title: Movimiento estudiantil está de luto en Chile y América Latina
Date: 2015-05-15 14:43
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Chile, educacion, estudiantes, fech, melissa sepulveda
Slug: asesinato-de-2-estudiantes-en-chile-demuestra-la-descomposicion-social-por-defender-la-propiedad-privada-melissa-sepulveda
Status: published

###### Foto: BioChile 

<iframe src="http://www.ivoox.com/player_ek_4502129_2_1.html?data=lZqdlJaWfY6ZmKiakpuJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdTZ1M7bw9nTb8XZjJeYx9jYucXdwtPhx9iPqc%2BfxM3WzsqPaZOmxcra18rXuNPVjNHOjcnJt8TjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Melissa Sepulveda, Ex-presidenta de la FECH] 

###### [15 may 2015] 

El movimiento estudiantil chileno había llamado a una realizar una movilización movilización el jueves 14 de mayo, según afirmó la presidenta de la FECH, Valentina Saavedra, para presionar al gobierno de Michelle Bachelet a "convocar finalmente un espacio en el que los diversos actores sociales podamos definir los pilares de la nueva educación", en el marco de la presentación pública de cuentas que deberá realizar la presidenta el próximo 21 de mayo, plazo que tiene el gobierno para presentar la iniciativa sobre educación pública.

El paisaje se vistió una vez más de estudiantes con pancartas, banderas y consignas, y mareas de jóvenes llenaron las principales avenidas de Santiago, Concepción y Valparaiso.

Sin embargo, la fiesta de los estudiantes se enlutó en horas de la tarde, cuando dos jóvenes que participaban en la jornada fueron asesinados. Exequiel Borbarán, de 18 años, y Diego Guzmán, de 25, estaban haciendo un grafitti en un edificio residencial cerca a la Plaza Victoria, en Valparaíso, cuando el conserje de la misma les increpó furioso. Pero fue un joven de 22 años que vive en la residencia, el que se salió con un arma de fuego, y disparó, a Exequiel en el cuello, y a Diego en el tórax.

Para Melissa Sepulveda, ex-presidenta de la FECH, "que haya sido un joven quien haya asesinado a otros dos estudiantes muestra el nivel de descomposición social en nuestro país; que por la defensa de la propiedad privada a toda costa, en este caso por pegar un papel o hacer un rayado, alguien asesine a otras dos personas"

El hecho generó rechazo tanto al interior del movimiento estudiantil, como en la sociedad chilena, que se volcó a las plazas de las principales ciudades en horas de la noche para hacer "velatones" por los estudiantes caídos.

En el año 2011, durante las protestas estudiantiles que exigían un nuevo sistema de educación, la fuerza estatal de los carabineros asesinó al estudiante Manuel Gutierrez; ese fue, sin embargo, otro escenario. "Hoy día nos enfrentamos a una situación en que una persona común y corriente se arroja el derecho de matar a otro solo por haber atacado esta propiedad privada, es un nivel de individualismo y de descomposición social, y por eso ha sido tan fuerte y tan chocante para nosotros", asegura Sepúlveda.
