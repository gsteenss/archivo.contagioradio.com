Title: Sí hay recursos para Avianca pero no para sectores más vulnerables
Date: 2020-09-01 12:18
Author: AdminContagio
Category: Actualidad, Economía
Tags: Avianca, Covid-19, Gobierno Duque
Slug: si-hay-recursos-para-avianca-pero-no-para-sectores-mas-vulnerables
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Presupuesto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Avianca: Wiki Commons

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el mes de abril de este año, Avianca pidió al Gobierno una financiación que le permitiera sobrevivir en el mercado, la ciudadanía se había mostrado en contra de la petición pues argumentaba que el dinero público y el Estado debían estar al servicio de otros sectores mucho más vulnerables como la pequeña y mediana empresa y no ser el salvavidas de empresas extranjeras. Cuatro meses después el Gobierno de Iván Duque autorizó un crédito por **un valor de al menos 370 millones de dólares para la aerolínea.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Avianca se acogió en mayo a la ley de bancarrota de Estados Unidos iniciando un proceso de reorganización debido al impacto negativo de la pandemia, que llevó a un desplome de las acciones de la compañía aérea que para entonces tenía un valor en el mercado cercano a los 66.000 millones de pesos equivalente a 17 millones de dólares, **por lo que su préstamo supera con creces incluso el valor que tiene en la actualidad.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante el anunció, el [procurador Fernando Carrillo](https://twitter.com/PGN_COL)advirtió que "emprender esta maniobra de rescate sin involucrar a otros países que también se beneficiarían de la actividad de Avianca puede significar una desproporcionada o excesiva presión sobre la capacidad financiera del Estado”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Procuraduría advierte sobre el crédito hecho a Avianca

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Dentro de sus advertencias la Procuraduría señaló que el origen del crédito proviene del Fondo de Mitigación de Emergencias (FOME) cuyos recursos responden al decreto 444 de 2020 que establece que cuando invierten dineros en “instrumentos de capital y/o deuda”, deben ser evaluados de forma conjunta y teniendo en cuenta las necesidades sociales y económicas ocasionadas por la covid-19, por lo que dicha evaluación debe ser divulgada debido a la magnitud del préstamo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que existe el riesgo de que el crédito no asegure que la aerolínea pueda operar con normalidad, consumiendo recursos que podrían ser de mayor utilidad **"para salvar puestos de trabajo y crear capacidad de generación de ingresos, en ese o en otro sector de interés general".** [(Lea también: Trabajadores de 54 plantas de ECOPETROL protestan contra la posible venta de la empresa estatal)](https://archivo.contagioradio.com/trabajadores-de-54-plantas-de-ecopetrol-protestan-contra-la-posible-venta-de-la-empresa-estatal/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, la ciudadanía, que ya había manifestado su rechazo ante este anunció, también ha expresado su descontento, ante una medida que otorga cerca de 1.4 billones a una compañía mientras otras medidas destinadas a como la **Renta Básica que beneficiaría a nueve millones de colombianos con un costo de 31 billones de pesos en un periodo de cinco meses o la Matrícula Cero que beneficiaría a los estudiantes de universidades públicas con un aporte de 500.000 millones de pesos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

También se ha advertido que el préstamo que se realizará a la aerolínea por el valor de **370 millones de dólares**, es incluso superior al presupuesto asignado al sector de la salud, empleo púbico, ciencia tecnología e innovación y agricultura. [(Lea también: Lea también: Matrícula Cero: la clave para garantizar la educación pública superior)](https://archivo.contagioradio.com/matricula-cero-la-clave-para-garantizar-la-educacion-publica-superior/)

<!-- /wp:paragraph -->

<!-- wp:image {"id":89146,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Presupuesto.jpg){.wp-image-89146}  

<figcaption>
Dirección de Inversiones y Finanzas Públicas del Departamento Nacional de Planeación

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### Otros sectores continúan en crisis

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a las críticas, el presidente Iván Duque, se refirió al crédito de 370 millones de dólares afirmando que con la decisión se busca respaldar la seguridad aérea, la conectividad y el turismo y los empleos directos e indirectos de al menos 500.000 personas. [(Lea también:](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/)[Mejorar imagen de Duque con recursos para la paz: una muestra de insensibilidad y desconexión hacia el país](https://archivo.contagioradio.com/mejorar-imagen-de-duque-con-recursos-para-la-paz-una-muestra-de-insensibilidad-y-desconexion-hacia-el-pais/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el Ministerio de Vivienda argumentan que otras naciones han seguido los mismos pasos como Alemania con la aerolínea Lufthansa Italia con Alitalia y Francia con AirFrance, sin embargo es necesario resaltar que Avianca actualmente pertenece a capital extranjero por lo que no es ya una empresa nacional.[(Lea también: Avianca ni ayuda ni es colombiana: Protesta ciudadana)](https://archivo.contagioradio.com/las-razones-para-rechazar-ayuda-del-gobierno-a-avianca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El anuncio se da en un momento en el que el desempleo en Colombia llegó al 20.2% en julio, frente al 10.7% del 2019, según el DANE, es decir que la población sin un empleo en Colombia para este mes fue de 4,6 millones de personas, 1,9 millones más que en el mismo mes del año pasado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En contraste al espaldarazo dado por el Gobierno a Avianca, que tiene una participación del 31,6% en el transporte de carga de Colombia y **representa cerca de 14,6 billones de pesos para la economía del país;** organizaciones como la Federación Nacional de Comerciantes (Fenalco) han denunciado que las ayudas financieras siguen sin llegar a otros sectores afectados del comercio incluidas las pequeñas y medianas empresas, fuente de empleo de más de 16 millones de personas, además de **representar más de 90% del sector productivo nacional y generan el 35% del PIB,** según cifras del DANE.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
