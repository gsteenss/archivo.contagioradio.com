Title: Ordenan libertad para César Barrera y Cristian Sándoval, víctimas de montaje judicial Andino
Date: 2020-06-02 16:44
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #CasoAndino, #Prisionerospolíticos, #Víctimas
Slug: ordenan-libertad-para-cesar-barrera-y-cristian-sandoval-victimas-de-montaje-judicial-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/jovenes-detenidos-andino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Tras audiencia, juez **otorgó la libertad inmediata a César Barrea y Cristian Sandoval por vencimiento de términos**. Ambos hombres habían sido víctimas de lo que se denominó como el montaje judicial del "Caso Andino y estuvieron en la cárcel durante más de dos años.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por este mismo hecho actualmente hay otras 8 personas privadas de la libertad: **Alejandra Méndez, Lizeth Rodríguez y Lina Jiménez,** quienes se encuentran en la cárcel de Picaleña, en Ibagué, en condiciones de total vulneración a los derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Boris Rojas, Andrés Bohorquez, Juan Camilo Pulido e Iván Ramírez, que se encuentran en la cárcel La Picota, en Bogotá, y Natalia Trujillo**, que se encuentra en otra reclusión. (Le puede interesar:["La versión de los jóvenes capturados por el atentado en el Centro Comercial Andino"](https://archivo.contagioradio.com/entrevista-a-boris-rojas-y-lizeth-rodriguez-capturados-por-atentado-a-centro-comercial-andino/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

El caso Andino un manto de impunidad
------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La captura de las diez personas se produjo el pasado 17 de junio, días después del atentado realizado en el Centro Comercial Andino, en Bogotá. Ello debido a que según la Fiscalía, los jóvenes eran los presuntos autores de esta acción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, la defensa de cada una de estas personas, ha manifestado que en **ninguna de las audiencias se han presentado pruebas que comprometa a los jóvenes con estos hechos,** razón por la cual aseguran que fueron víctimas de un "montaje judicial". (Le podría interesar: ["Colectivo Libres e inocentes"](https://www.facebook.com/LibreseInocentes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El próximo 3 de Junio también se realizará la audiencia por vencimiento de términos de Lizeth Rodríguez, en la que de acuerdo con los familiares de estos jóvenes esperan se de un panorama similar.

<!-- /wp:paragraph -->
