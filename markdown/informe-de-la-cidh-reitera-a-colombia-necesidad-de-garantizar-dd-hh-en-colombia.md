Title: Informe de la CIDH reitera a Colombia necesidad de garantizar DD.HH.
Date: 2017-04-28 17:26
Category: DDHH, Nacional
Tags: CIDH, Derechos Humanos, lideres sociales, paz, víctimas
Slug: informe-de-la-cidh-reitera-a-colombia-necesidad-de-garantizar-dd-hh-en-colombia
Status: published

###### [Foto: Archivo] 

###### [28 Abr. 2107]

La Comisión Interamericana de Derechos Humanos (CIDH), dio a conocer su más reciente informe anual en el que expone la situación en materia de DD.HH. de los Estados Miembros. Para el caso de Colombia **la CIDH mantiene sus preocupaciones sobre el asesinato de líderes sociales, la crisis carcelaria, el avance en los derechos de las mujeres y la comunidad LGTBI.**

Aunque en este informe detalla de manera especial la situación que acontece en materia de derechos humanos en países como Cuba, República Dominicana y Venezuela, Colombia también tuvo su capítulo, en el que si bien se **destaca el alcance y los avances que han significado el proceso de paz,** siguen haciendo recomendaciones especiales a la situación que vive el país en garantía de DD.HH.

Para el caso de Colombia, **la CIDH ha saludado de manera general los avances que se han logrado en el país luego de los acuerdos de paz** entre la guerrilla de las FARC y el Gobierno Nacional. Acuerdos que han permitido la inclusión de los grupos étnicos, las mujeres y la comunidad LGBTI, así como las oportunidades en materia de acceso a la verdad, justicia y reparación para las víctimas del conflicto armado.

Sin embargo, **la CIDH recordó las obligaciones en las que se encuentra pendientes el Estado colombiano** en materia de garantía de DD.HH. y lamentó el aumento de asesinatos de defensores y defensoras de derechos humanos.

“La CIDH notó qué, si bien las denuncias de amenazas, atentados, detenciones arbitrarias y hostigamientos contra defensoras y defensores de derechos humanos disminuyeron significativamente respecto del 2015, **los asesinatos aumentaron en los últimos meses de 2016” asegura el informe. **Le puede interesar: [En CIDH evidencian tareas pendientes del Estado para el desmonte del paramilitarismo](https://archivo.contagioradio.com/cidhparamilitarismoestadoddhh/)

Cabe recordar que, de acuerdo a organizaciones de la sociedad civil, 19 defensores y defensoras de derechos humanos y líderes comunitarios fueron asesinados entre julio y septiembre de 2016 (3 más que el trimestre anterior y que el mismo periodo en el 2015), **lo que asciende a un total de 54 asesinatos entre enero y octubre “en la mayoría de estos casos, los autores serían grupos armados ilegales” manifiesta la CIDH. **Le puede interesar: [CIDH responde ante vulnerabilidad de defensores de DDHH](https://archivo.contagioradio.com/cidh-ordena-medidas-cautelares-para-alberto-yepes-defensor-de-ddhh/)

En cuanto a la grave situación de las personas privadas de la libertad, el órgano recalcó que **es importante atender la crisis carcelaria que vive el país por tanto, el Estado debe adoptar medidas urgentes** eficaces para asegurar el acceso a la salud médica de tipo psiquiátrico. Así como la provisión de agua potable, y argumenta que es necesario que Colombia “ratifique el Protocolo Facultativo de la Convención de las Naciones Unidas contra la Tortura y Otros Tratos o Penas Crueles, Inhumanos o Degradantes”.

En términos de avances en los derecho de las comunidades LGTBI, para la CIDH **el Estado debe “diseñar y adoptar las medidas necesarias para prevenir actos de violencia y discriminación contra personas lesbianas, gays, bisexuales, trans e intersex,** protegerlas de estos abusos, y responder debida diligencia cuando son cometidos, tanto por parte de agentes estatales como por parte de terceras personas y grupos armados, en todo el territorio nacional.  En el contexto de las negociaciones para la firma del Acuerdo”.

Para el caso de las mujeres en el marco del conflicto armado, **el Estado sigue en mora de lograr la implementación y el fortalecimiento de medidas para prevenir, sancionar y erradicar la violencia y discriminación contra las mujeres.** Además, continúa recordándole que debe realizar un trabajo con enfoque diferencial en el que las mujeres indígenas y afro sean escuchadas y se les garantice su acceso a los derechos a la salud, educación, justicia y economía.Le puede interesar: [120 defensores de DDHH asesinados en 14 meses en Colombia: Defensoría](https://archivo.contagioradio.com/120-defensores-asesinados-defensoria/)

Dentro de su informe, **la CIDH hace hincapié en la situación que afrontan los niños y las niñas de Colombia,** para quienes solicita hacer investigaciones pertinentes para obtener información completa y veraz sobre niños reclutados por grupos armados ilegales, así como que debe garantizar el trato especial a los niños que se han reincorporado a la vida civil.

Por su parte, la Comisión reiteró que sigue convencido que **la consolidación de la paz es “un requisito indispensable para el ejercicio y respeto de los derechos humanos**” y agrega que ese es el gran reto y responsabilidad que tiene Colombia en la fase de implementación. Le puede interesar: [Ante ola de asesinatos de líderes organizaciones de DD.HH acuden a CIDH](https://archivo.contagioradio.com/situacion-de-lideres-sociales-en-colombia-ante-la-cidh/)

[Informe Anual 2016 - Colombia](https://www.scribd.com/document/346697403/Informe-Anual-2016-Colombia#from_embed "View Informe Anual 2016 - Colombia on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_52244" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/346697403/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-uZVsuG1HT7IlMOCfXnAD&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
