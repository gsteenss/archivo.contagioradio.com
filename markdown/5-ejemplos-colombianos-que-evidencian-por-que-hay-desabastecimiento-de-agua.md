Title: La responsabilidad empresarial frente al desabastecimiento de agua
Date: 2016-11-28 14:11
Category: Ambiente, Voces de la Tierra
Tags: Agua, La Guajira, Mineria, ONU, petroleo
Slug: 5-ejemplos-colombianos-que-evidencian-por-que-hay-desabastecimiento-de-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/La-Guajira-e1480359645641.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defensoría del Pueblo 

###### 28 Nov 2016 

Este lunes la **ONU ha advertido que el planeta se encuentra en una crisis de desabastecimiento de agua potable.** "El mundo avanza por un camino que lleva hacia lo insostenible", dijo Peter Thomson, presidente de la Asamblea General de la ONU y participante de la **Cumbre de Agua que se celebra en Budapest.**

El caso colombiano es la muestra de lo que puede estar sucediendo en el resto del planeta. El país **posee el mayor volumen de agua después de la Unión Soviética, Canadá y Brasil, con al menos 737.000 cuerpos de agua** entre ríos, quebradas, caños y lagunas, pero muchas de estas riquezas hídricas se encuentran en peligro, no solo por el fenómeno de El Niño, sino porque la economía colombiana se basa en un modelo extractivista que genera el desgaste de las fuentes hídricas.

Aquí le mostramos algunos de los casos recientes que evidencian cómo la actividades de empresas petroleras, mineras y de monocultivos promueven el desabastecimiento de agua en Colombia.

### Barrancabermeja 

Las comunidades del corregimiento Campo 23, de Barrancabermeja, denunciaron a principios de octubre que [la tierra ha emanado una mezcla de agua y petróleo](https://archivo.contagioradio.com/mas-de-100-familias-afectadas-por-contaminacion-de-agua-en-barrancabermeja/) que no es natural sino que es causada por la técnica que está empleando **Ecopetrol junto a la petrolera Occidental Andina,** para elevar la presión y producir el arrastre del hidrocarburo. Líderes de la comunidad aseguraron que el petróleo está saliendo a la superficie debido a la inyección de más de 140 barriles de agua diarios que son usados para la producción de 45 mil barriles de crudo.

### La Guajira 

Según datos de la UNICEF en los últimos **[6 años han muerto 5 mil niños del pueblo Wayúu por desnutrición](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/)**. Una situación que se ha generado por la falta de agua en una región que es desértica, pero en la que además la **empresa Carbones El Cerrejón** tiene una de las minas de carbón a cielo abierto más grandes del mundo.

La comunidad asegura desde hace décadas que esa actividad minera ha generado que hoy 26 fuentes de agua estén completamente secas. El PNUD, ha dicho que el consumo de agua por persona al día en La Guajira es de 0,7 litros, mientras que, de acuerdo con la organización Censat Agua Viva, la mina El Cerrejón usa diariamente [17 millones de litros extraídos del Río Ranchería](https://archivo.contagioradio.com/mina-de-carbon-del-cerrejon-usa-diariamente-17-millones-de-litros-de-agua/) para limpiar el polvo de las vías de transporte.

### Monte Bonito 

**La empresa Latinco,** que ya tiene las licencias otorgadas por la Corporación Autónoma Regional de Caldas, CORPOCALDAS, pretende desarrollar un proyecto de Micro Hidroeléctricas que pondría [en riesgo de secarse a 100 fuentes de agua](https://archivo.contagioradio.com/100-fuentes-de-agua-estan-en-riesgo-en-monte-bonito-caldas/), según denuncian los habitantes de la zona, que ya han visto como esos proyectos afectan a otras comunidades como en el caso de [Bolivia, Caldas y la UT GC CHOC](https://archivo.contagioradio.com/construccion-de-hidroelectricas-deja-sin-agua-a-90-familias-en-caldas/).

### Mapiripán 

De acuerdo con la Comisión de Justicia y Paz, en este municipio del Meta, la **empresa italiana Poligrow** consume cerca de 454 millones de litros de agua por los cuales paga 6 millones de pesos en un contrato de 5 años. La organización defensora de derechos humanos asegura que esta multinacional tiene permisos de captación de agua más alta que petroleras, pues[ usa más de 1'700.000 litros diarios en Mapiripán.](https://archivo.contagioradio.com/audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan/)

Según denuncian,** **13 fuentes de agua se han secado debido a la [plantación de 15 mil hectáreas de palma aceitera](https://archivo.contagioradio.com/cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia/) que acaban con las palmas de moriche y las fuentes hídricas subterráneas de la región, afectando más de 200 familias.

### Lago de Tota 

Hace 5 años, 6 municipios del departamento de Boyacá, vienen siendo afectados por el inicio de la actividad petrolera cerca al Lago de Tota. Hoy, pese a las movilizaciones de las comunidades, denuncian que [210 cuerpos de agua han desaparecido](https://archivo.contagioradio.com/210-cuerpos-de-agua-cercanos-al-lago-de-tota-desaparecieron-por-actividad-petrolera/), por cuenta de la explotación y extracción de crudo, por parte de la **multinacional francesa Maurel & Prom, que en el 2011 junto a la Compañía Geofísica Latinoamericana, CGL,** dieron a conocer a las comunidades de Betania, Tota, Pesca, Firavitoba, Iza y Sogamoso un engañoso proyecto de sísmica y extracción petrolera, que afectó a los territorios aledaños a la cuenca del Lago de Tota, como lo denunciaron integrantes del Colectivo por la Defensa de la Provincia de Sugamuxi.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
