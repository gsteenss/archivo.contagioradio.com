Title: Comunicado: Organizaciones sociales solicitan al DNP ampliar tiempos para la formulación de los Planes de Desarrollo
Date: 2020-04-03 19:00
Author: Foro Opina
Category: Nacional, Opinion
Tags: DNP, Foro opina
Slug: organizaciones-sociales-solicitan-al-dnp-ampliar-tiempos-para-la-formulacion-de-los-planes-de-desarrollo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/luis-alberto-rodriguez_2065459_20190822200130.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#596468"} -->

###### Luis Alberto Rodríguez, director del DNP. Foto de: El Universal 

<!-- /wp:heading -->

<!-- wp:paragraph -->

**03 de abril de 2020. **La Ley 152 de 1994 sienta las bases para el proceso de formulación de los planes de desarrollo, indicando una ruta y unos tiempos de actuación. Sin embargo, con la propagación del covid-19 en Colombia, **es evidente que los nuevos alcaldes y gobernadores del país están enfocando sus esfuerzos y sus equipos de trabajo en la contención y mitigación de la expansión del virus**, por lo tanto, la formulación de los Planes ha sufrido importantes retrasos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es por ello, que varias organizaciones sociales del país nos unimos al llamado realizado por los miembros de la Comisión de Ordenamiento Territorial del Senado y solicitamos al Departamento Nacional de Planeación (DNP), en cabeza del Sr. Luis Alberto Rodríguez, a que se tomen medidas para **ampliar los plazos de formulación de los planes de desarrollo, a que se fijen unos lineamientos y se disponga de acompañamiento técnico** a los entes territoriales para enfrentar de manera adecuada este nuevo reto para el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es importante señalar que uno de los principios de la planeación territorial es la **participación ciudadana**, que en el marco de esta coyuntura se ha visto limitada por la imposibilidad de mantener los espacios de discusión presencial y las dificultades en muchas zonas del país para hacer uso de herramientas tecnológicas que garanticen la adecuada y necesaria contribución de los **Consejos Territoriales de Planeación** y de organizaciones sociales y comunitarias en la construcción de estos documentos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es necesario ampliar los plazos para que los CTP entreguen sus conceptos y logren incluirse recomendaciones para afrontar las consecuencias que esta emergencia. Igualmente, se requieren mayores claridades sobre estrategias para **asegurar la efectiva discusión de los planes de desarrollo en los Concejos y Asambleas Departamentales. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así mismo, la actual situación de pandemia y sus nefastas consecuencias obligan a **replantear la planeación socioeconómica, las políticas públicas** y proyectos específicos en los planes, **así como los ajustes institucionales y presupuestales** para la atención de los distintos ámbitos del desarrollo humano, social y económico que se verán afectados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Invitamos a la Federación Colombiana de Municipios y la Federación Colombiana de Departamentos a que también se pronuncien y se unan a este llamado de atención junto con las organizaciones sociales, los consejeros de planeación y los congresistas de diversos partidos políticos que ya se han manifestado al respecto, pues **es de suma urgencia que el DNP anuncie prontas medidas ante esta situación.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **Alianza de Organizaciones Sociales y Afines por una Cooperación Internacional para la Paz y la Democracia en Colombia**:

<!-- /wp:heading -->

<!-- wp:paragraph -->

-[Asociación Colombiana de Proyectos](https://www.facebook.com/Asociacion-Colombiana-de-Proyectos-1284342824997359/) -[Asociación Nacional de Zonas de Reserva Campesina ANZORC](http://anzorc.com/) -Asociación PIWAN -[APRODEFA](https://www.facebook.com/pages/category/Non-Governmental-Organization--NGO-/Aprodefa-Asociaci%C3%B3n-pro-Desarrollo-de-Familias-426148730788756/) -[Caribe Afirmativo](https://caribeafirmativo.lgbt/) -[Casa de la Mujer](https://www.facebook.com/CasaMujerColombia/) -[Central Unitaria de Trabajadores CUT -](https://twitter.com/cutcolombia?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor)[Centro de Investigación y Educación Popular CINEP](https://www.cinep.org.co/Home2/) -[Confluencia de Mujeres para la Acción Pública](https://www.facebook.com/LaConflu/) -Consuelo Cepeda, Periodista -Colectivo La Raíz de Engativá -[Coordinación Colombia-Europa Estados- Unidos (CCEEU)](https://coeuropa.org.co/) -Corporación para la Educación y Autogestión Ciudadana -[Corporación Región](https://www.region.org.co/) -[Corporación Reiniciar](https://www.facebook.com/corporacion.reiniciar/) -[Corporación Ubuntu Territorio y Paz](https://www.facebook.com/cubuntucolombia/about/?ref=page_internal) -[Corporación VisoMutop](http://visomutop.org/page/homepage) -[Corporación Viva La Ciudadanía](http://viva.org.co/) -Darío Villamizar, Integrante del ICTJ. -Diana Castro, Economista e historiadora E-Paz -Corporación Escenarios para la Paz -[Escuela Nacional Sindical](http://www.ens.org.co/) -Fabiola Rodríguez López, Defensora de paz -[Fundación Buen Vivir ICBM](https://www.facebook.com/Fundaci%C3%B3n-Buen-Vivir-399220697103149/) -[Fundación Foro Nacional por Colombia](https://foro.org.co/)-Fundación Foro Capítulo Costa Atlántica -[Fundación Foro Capítulo Suroccidente](http://forosuroccidente.org/) -[Fundación Foro Capítulo Región Central](http://fundacionfororegioncentral.org/) -Gloria Moreno, Periodista -[Héctor Fabio Cardona, Periodista y asesor asuntos públicos](https://twitter.com/hfcardonag?lang=es) -Hernán Ortiz, Profesor Universidad del Cauca -Iglesia Anglicana -[Instituto Popular de Capacitación - IPC](http://ipc.org.co/) -[John Jairo Ocampo, Periodista](https://twitter.com/jjocampo_?lang=es) -[Juan Fernando Cristo, ex Ministro del Interior](https://twitter.com/cristobustos?lang=es) -Luis Mayorga, constructor de paz -María Clara Mondragón, Economista -Mauricio Arroyave, Director de Despierta Bogotá -[Mesa Técnica de Trabajo Altos de la Estancia](https://www.facebook.com/colectivo.ciudadbolivar/) -[Movice Capítulo Atlántico](https://movimientodevictimas.org/tag/atlantico/) -[Movimiento Social Discapacidad Colombia- MOSODIC](https://www.facebook.com/mosodic.co/) -Movimiento Alternativa -Muvicopaz -[Plataforma Colombia de Derechos Humanos, Democracia y Desarrollo](https://www.facebook.com/PCDHDD/) -Plataforma La Alianza -[Revista Generación Paz](https://generacionpaz.co/) -[Ruta Pacífica de las Mujeres](https://rutapacifica.org.co/wp/) -Tierra Patria [-Unión Patriótica Atlántico  ](https://www.facebook.com/upbarranquilla/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Nuestros Columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
