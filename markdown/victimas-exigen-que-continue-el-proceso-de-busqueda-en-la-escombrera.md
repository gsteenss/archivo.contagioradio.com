Title: Víctimas exigen que continúe el proceso de búsqueda en La Escombrera
Date: 2015-12-16 13:30
Category: DDHH, Nacional
Tags: Desaparición forzada, La escombrera, Mujeres Caminando por la Verdad
Slug: victimas-exigen-que-continue-el-proceso-de-busqueda-en-la-escombrera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/La-Escombrera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corporación Jurídica Libertad 

###### [16 Dic 2015]

Con la realización de un acto simbólico y la presentación de un informe por parte de las organizaciones de víctimas, concluye este miércoles  la primera etapa de búsqueda de personas desaparecidas en el Polígono Uno de La Escombrera en la comuna 13 de Medellín, que finaliza este año **sin haberse encontrado ningún resto óseo,** según informa La Fiscalía General de la Nación.

Apenas han sido halladas **algunas prendas de vestir, accesorios y un documento,** que se encuentran en etapa de investigación, según explicó en días pasados, Carlos Fidel Villamil, director nacional de Justicia Transicional de la Fiscalía.

La organización “Mujeres Caminando por la Verdad”, ha exigido la **continuidad del proceso de búsqueda en la Escombrera en otras zonas de ese sector**, así mismo se exige el cumplimiento del Plan Integral de Búsqueda con el que se propone investigar los sitios señalados como fosas comunes.

La Escombrera cuenta con un área aproximada de 75 hectáreas y cinco millones de toneladas de escombros, lo que la convierten en la fosa urbana más grande del mundo, donde los paramilitares habrían arrojado decenas de restos de sus víctimas, en el marco de la Operación Orión, de las Fuerzas militares, que, de acuerdo a diversos análisis e informes de organismos de DDHH**,** consolidó el control paramilitar que persiste hasta ahora.

**Las más 20 acciones militares** (según la Corporación Jurídica Libertad), **dejaron 650 víctimas directas y al menos 92 casos de desaparición forzada documentados a la fecha.** 13 años después la comunidad reclama que el Estado no ha garantizado las condiciones de protección necesaria a la población.

Cabe recodar que el Polígono Uno fue uno de los lugares señalados por desmovilizados del bloque Cacique Nutibara, entre ellos alias ‘Móvil 8’, que en su testimonio afirmó que en ese sector de Medellín  habría por lo menos 50 víctimas de desaparición forzada.
