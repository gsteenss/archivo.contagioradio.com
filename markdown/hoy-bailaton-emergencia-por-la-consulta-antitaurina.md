Title: ¡Hoy bailatón emergencia por la Consulta Antitaurina!
Date: 2015-09-18 12:36
Category: Animales, Movilización, Voces de la Tierra
Tags: #ConsultaAntitaurina, Consulta Antitaurina, corridas de toros en Bogotá, elecciones 25 de octubre, elecciones regionales, Jaime Suárez, Maltrato animal, Martha Lucía Zamora, Registraduría Distrital
Slug: hoy-bailaton-emergencia-por-la-consulta-antitaurina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/bailaton-9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Hoy en la tarde se llevará a cabo una reunión **entre el registrador Distrital, Jaime Hernando Suárez, y la Secretaria General del distrito, Martha Lucía Zamora**, donde se analizarán los costos y las posibilidades de que la consulta popular antitaurina se realice con las elecciones regionales.

Tras la reunión de este jueves, entre el Ministerio de Hacienda y el Distrito, se revisaron costos y se evidenció que la **consulta popular no cu 34 mil millones deesta pesos si se realiza el 25 de octubre, en realidad tendría un valor de 1.600 millones pesos.**

Aunque el Ministerio aseguró que no existen el dinero para gestionar la consulta, se abrió la posibilidad de que esta sea asumida por la Alcaldía de la capital.

En el marco de esa importante reunión en la Registraduría, ciudadanos y ciudadanas que defienden la vida de los toros han decidido realizar una **tercera “Bailatón de emergencia” frente a la sede de la Registraduría por la calle 26** para apoyar la iniciativa de la Alcaldía y finalmente la ciudadanía sea la que decida el próximo 25 de octubre si hay o no arraigo cultural mayoritario frente a las corridas de toros.

[![11997318\_10153464817815020\_827518259\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/11997318_10153464817815020_827518259_n.jpg){.aligncenter .wp-image-14256 width="660" height="261"}](https://archivo.contagioradio.com/hoy-bailaton-emergencia-por-la-consulta-antitaurina/11997318_10153464817815020_827518259_n/)
