Title: Tsipras claudica ante la troika
Date: 2015-07-15 06:00
Category: Cesar, Opinion
Tags: Alexis Tsipras, FMI, Grecia, Jean-Claude Juncker, La troika, OTAN, Syriza
Slug: tsipras-claudica-ante-la-troika
Status: published

#### [[César Torres del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/) - @logicoanalista2 ] 

###### [15 Jul 2015 ] 

Llegó la hora cero para Grecia. Ante todos nosotros, ante todos los pueblos de Europa y del Sur, el resultado del referendo del 5 de julio, un no claro contra el programa de austeridad formulado por la Troika (UE, Banco Central y el FMI), ha sido desconocido por Alexis Tsipras, primer ministro griego. La democracia tampoco funciona para los bancos ni para los gobiernos de la Eurozona, comenzando por el de Alemania. Estamos ante una grave situación económica y social, ante la tragedia.

La situación actual era previsible. Desde el acuerdo del 20 de febrero de 2015 el gobierno de Tsipras y el grupo central de Syriza se habían manifestado favorables a negociar el paquete de austeridad de la Eurozona, lo que de hecho anunciaba la ruptura con el programa general de Syriza, y por anticipado con el resultado del referendo. El 6 de julio la Declaración Común – no firmada por todos los integrantes de la coalición – decía que el resultado del referendo no era un mandato para la ruptura con la Eurozona sino uno para “continuar y consolidar el esfuerzo por llegar a un acuerdo socialmente justo y económicamente viable”. Dice el adagio que “más claro no canta el gallo” …

O sea, ni con Syriza ni con la democracia. El programa de Syriza es claramente anti-neoliberal con puntos interesantes como la nacionalización de los sectores ferroviarios, transporte aéreo y en especial la banca; restablecimiento de los convenios colectivos; suspensión del pago de intereses de la deuda externa; reducción de  impuestos a los trabajadores y aumento de subsidios; revisión eco-ambiental del programa de desarrollo; control de precios; inclusión social de los inmigrantes; sistema gratuito de salud y educación; retiro de la OTAN (organismo militar imperialista) y cierre de las bases militares extranjeras; restauración de pensiones y del salario mínimo; empleo seguro y, en particular, introducción de la democracia directa y de organismos de autogestión. Sobre esto último dice el programa: “Nuestro objetivo estratégico es el socialismo con democracia …”.

¿Qué podemos decir hoy, entonces, sobre el proceso griego, sus instituciones y sobre el campo de la ciudadanía marginada?

Syriza es una coalición de partidos de izquierda y como tal basa su accionar a partir de alianzas; su audiencia se ubica ante todo en el nivel electoral en un espacio caracterizado por niveles bajos de lucha social. Ni Tsipras ni su gobierno movilizaron al 62% que votó por el “no” en el sentido del programa que hemos mencionado; sus maniobras buscaban la mejor adecuación posible al programa del presidente de la Comisión Europea Jean-Claude Juncker y de la canciller alemana Angela Merkel. Quienes fueron derrotados (el PC, Nueva Democracia, El Río)  tienen ahora ministerios y son los que implementan la orientación hacia el programa de austeridad.

Por el contrario, y sin ser partidos con anclaje mayoritario, en la orientación del programa de Syriza ha venido trabajando la Red Network – alianza de Izquierda Obrera Internacionalista y otros sectores con la Corriente de Izquierda –  lo que en buena parte se apreció en la participación popular y ciudadana en favor del  “no”; tales agrupaciones proponen dirigirse políticamente hacia Antarsya y el Partido Comunista, en una clara orientación social y de clase, de izquierda. Podrá sonar como “no realista” tal movilidad estratégica; pero no hay duda de que es mediante éste tipo de acciones como se puede enfrentar a la lógica del capital (Troika) y a las maniobras político-electorales de la razón de Estado (Tsipras).

Lo que es difícil de aceptar es que ahora viene el mayor ajuste neoliberal en la zona euro. Y aceptado con “soberanía” por el gobierno de Tsipras. El “rescate” de la Troika es para salvar a los bancos griegos, a los armadores, a los acreedores de la deuda externa, al sistema del capital.
