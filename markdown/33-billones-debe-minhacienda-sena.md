Title: Desfinanciación del SENA afecta a más de 7000 estudiantes
Date: 2019-10-18 07:00
Author: CtgAdm
Category: Educación, Movilización
Tags: crisis en la educación, deuda, Ley de Financiación, Marchas, Ministerio de hacienda, Movilización, SENA
Slug: 33-billones-debe-minhacienda-sena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EHAZHcZXYAAGUVE.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/EHAZHcZXYAAGUVE-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@wilsonariasc] 

Trabajadores y estudiantes del Servicio Nacional de Aprendizaje (SENA), se movilizaron este 16 de octubre por diferentes vías principales de Bogotá, en respuesta a la deuda que ha ido acumulando el Ministerio de Hacienda desde el 2016 con la reforma tributaria de Santos, que se totaliza en \$3,3 billones de pesos de presupuesto que exigen ser devuelto a la institución.

Las movilizaciones salieron desde las sedes en la  1 de mayo, Paloquemao, Carrera 30 y  Jímenes, y tenían como punto de concentración la Plaza de Bolívar, donde se estaba llevando a cabo en el Congreso de la República el  segundo debate sobre el presupuesto general de la nación, según voceros del movimiento, por medio del dialogo y la movilización, quieren abordar temas que afectan a la comunidad educativa.

Jony Acosta, integrante del Sindicato de Trabajadores Públicos del SENA (Sindesena), afirmó que la falta de este rubro a representando un problema al intentar responder a las necesidades de más de 7 mil aprendices que conforman este organismo, "tenemos estudiantes en condiciones deplorables, no tienen materiales, los centros de trabajo no cumplen las normas de seguridad y salud, por eso salimos para exigir  al gobierno que nos reintegre estos fondos necesarios".

### Origen de la desfinanciación del SENA

El SENA se crea en  en 1957  bajo la Ley 118 con el propósito de ser una entidad de enseñanza laboral y técnica, financiada con recursos de los trabajadores al hacer un aporte mínimo obligatorio de su salario para generar una institución que capacite a las personas en careras técnicas y tecnológias. En ese momento se presentaba bajo la figura de aportes parafiscales, presupuesto paralelo a las rentas del estado.

Según Acosta este presupuesto se han venido transformando con el correr de los años, "primero fue el CREE que era el impuesto a la equidad y que también fue eliminado por una reforma tributaria de Santos en el 2016, generando un paro de 37 días que logró una destinación específica  y un pacto social que ahora de alguna manera protege al SENA", agregó el funcionario.

 

### El SENA se suma también al las manifestaciones del Movimiento Estudiantil

Acosta agregó que el SENA como institución que  capacita a los trabajadores colombianos, también marchan en rechazo a la reforma pensional y laboral que propone el gobierno de Ivan Duque, y que en voz del sindicalista afecta y afectará a las futuras generaciones de trabajadores. (Le puede interesar:[Duque llevará al Congreso la misma Ley de Financiamiento que le devolvieron](https://archivo.contagioradio.com/duque-llevara-congreso-misma-ley-de-financiamiento/))

Así mismo, señaló  que como vocero de Sindesena se unirán a la agenda de marchas del movimiento estudiantil, desarrollada el 17 de octubre, y al paro nacional el 21 del mismo mes. (Le puede interesar:[Como falsos positivos judiciales, califican capturas de estudiantes en Pereira](https://archivo.contagioradio.com/como-falsos-positivos-judiciales-califican-capturas-de-estudiantes-en-pereira/))

 

 

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43274754" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43274754_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
