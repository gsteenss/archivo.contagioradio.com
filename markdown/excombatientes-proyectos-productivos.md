Title: Comienza discusión sobre proyectos productivos para excombatientes de las FARC-EP #YoReporto
Date: 2017-06-22 16:09
Category: yoreporto
Tags: excombatientes, FARC, paz
Slug: excombatientes-proyectos-productivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/WhatsApp-Image-2017-06-22-at-15.01.21-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

##### 22 Jun 2017 

En el día de ayer, en la ciudad de Cali, una comitiva de las FARC-EP, participó de un almuerzo con la Fundación Paso Colombia, la cual tiene convenio con el Ministerio de posconflicto, encargado de generar las rutas de procesos productivos para los excombatientes que inician su proceso de incorporarse a la vida civil.

En el transcurso del almuerzo se presentó una propuesta sobre la elaboración de proyectos que se vienen realizando con los combatientes que están localizados en las Zonas Veredales de Monterredondo y Caldono. (Le puede interesar: [Cinco propuestas para evitar el fracaso total de ley de restitución de tierras](https://archivo.contagioradio.com/42667/))

Posteriormente, se reunieron con la gobernadora del Valle: Dilian Francisca Toro y parte de su gabinete, entre ellos el consejero de paz y el presidente de la Asamblea Departamental; en este escenario se debatió sobre la necesidad de construir la Paz con enfoque territorial y se logró el compromiso de la gobernadora frente a las garantías para los excombatientes una vez que se reincorporen a la vida civil.

![WhatsApp Image 2017-06-22 at 15.01.21 (2)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/WhatsApp-Image-2017-06-22-at-15.01.21-2.jpeg){.alignnone .wp-image-42704 width="562" height="305"} ![WhatsApp Image 2017-06-22 at 15.01.21 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/WhatsApp-Image-2017-06-22-at-15.01.21-1.jpeg){.alignnone .wp-image-42705 width="561" height="316"} ![WhatsApp Image 2017-06-22 at 15.01.21](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/WhatsApp-Image-2017-06-22-at-15.01.21.jpeg){.alignnone .wp-image-42706 width="561" height="329"}

Por: Componente de comunicación del MMV de las FARC \#YoReporto
