Title: Jóvenes del Bajo Atrato se reúnen para apostarle a la construcción de Paz
Date: 2017-06-06 14:50
Category: Comunidad, Nacional
Tags: Bajo Atrato, jovenes
Slug: jovenes-del-bajo-atrato-se-reunen-para-apostarle-a-la-construccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/CONSTRUYENDO-FUTURO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidades CONPAZ] 

###### [06 Jun 2017] 

Desde el 5 de junio se está realizando en la Zona Humanitaria de Camelias, en la cuenca del Río Curvadaró, **el Primer Encuentro de Jóvenes del Bajo Atrato por la paz**, que busca que los jóvenes que habitan en los municipios aledaños a la zona humanitaria intercambien propuestas y experiencias sobre la construcción de paz.

El Encuentro contó con la participación de aproximadamente 120 jóvenes provenientes de territorios que ha vivido el conflicto armado como Río Sucio, Bojaya, Jiguamiandó, Vigía del Fuerte, entre otros. Le puede interesar: ["Colombia sale a las calles a defender los acuerdos de paz"](https://archivo.contagioradio.com/colombia-sale-a-las-calles-a-defender-los-acuerdos-de-paz/)

Para Jhon Jader Córdoba, integrante del comité impulsor del Encuentro, el objetivo de esta iniciativa es “**plantear propuestas de carácter social y organizativo que les permita avanzar en una articulación** de procesos que tengan en cuenta el contexto del Bajo Atrato y construya desde los jóvenes alternativas de paz”.

El evento inicio con un panel sobre construcción y alternativas de paz en **donde participó Pablo Atrato, integrante de las FARC-EP**, defensores de derechos humanos de la Comisión de Justicia y Paz, defensores de derechos humanos de DiPaz y Ligia María Chavarra, matriarca de la comunidad. Le puede interesar:["Los retos de la Unidad del desmonte del Paramilitarismo"](https://archivo.contagioradio.com/se-crea-en-colombia-unidad-de-desmonte-del-paramilitarismo/)

Hoy los jóvenes trabajarán por mesas en donde compartirán sus experiencias y propuestas de paz para luego analizar las necesidades de las comunidades. Jhon Jader manifestó que una vez finalice el Encuentro se espera que se de una participación más fuerte desde los jóvenes en los espacios de construcción de paz “**somos lo que tenemos que protagonizar este nuevo momento de los diálogos de paz**, en el tema de la implementación y proyectarnos a futuro para que nuestras necesidades se suplan y se garanticen nuestros derechos”.

<iframe id="audio_19115022" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19115022_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
