Title: El Plan Nacional de Desarrollo, un respaldo al sector minero-energético
Date: 2019-02-26 17:35
Category: Ambiente, Entrevistas
Tags: Bancada Alternativa, Estudio de Impacto Ambiental, Iván Duque, páramos, Plan Nacional de Desarrollo
Slug: plan-nacional-desarrollo-respaldo-al-sector-minero-energetico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/minería.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [26 Feb 2019] 

Organizaciones ambientales advierten que el Plan Nacional de Desarrollo (PND) 2018-2022, presentado por el Gobierno de Iván Duque, propone políticas que facilitarían el procedimiento de solicitudes de licencias ambientales por el sector minero-energético y que evidencian una visión desde el Gobierno, centrado en el fortalecimiento del modelo extractivista.

Según el abogado ambiental Rodrigo Negrete, **los artículos 223 y 224 de la Ley 1450 de 2011**, con el que se expide el Plan Nacional de Desarrollo 2010-2014, establecen el procedimiento del Estudio de Impacto Ambiental y el otorgamiento de licencias ambientales. Estos dos artículos son derogadas en el Plan de Duque, es decir, **quedarían sin efecto**.

Negrete manifestó que este nuevo proyecto genera una "incertidumbre jurídica" sobre la exigencia del estudio y licencia ambiental que se establecen en la Ley 99 de 1993 como requisitos para la explotación minero-energética. La derogatoria de estos dos artículos no solo **contradicen la Ley 99 de 1993 sino también las modificaciones a esta Ley** que se sancionaron en los planes de desarrollo del Gobierno de Juan Manuel Santos.

El abogado afirma que con la eliminación de estos artículos "**no se va saber con certeza cual es la norma que finalmente va a estar vigente**." Lo que sí queda claro es que el Gobierno busca profundizar y ampliar el sector mino-energético en los territorios. (Le puede interesar: "[Comunidades de la Guajira demandaron a Cerrejón y piden anular licencia ambiental](https://archivo.contagioradio.com/comunidades-la-guajira-presentan-demanda-cerrejon-estado/)")

"En cambio de fortalecer el instrumento de licenciamiento ambiental, en lugar de darle más rigor, en lugar de establecer licencia ambiental para la exploración minera, lo que están haciendo es derogando dos artículos que ya fueron modificados," explicó Negrete.

Otra norma que es cuestionada por los ambientalistas y la Bancada Alternativa es el **artículo 18 del Plan de Desarrollo, que establece el procedimiento a la hora de obtener una licencia ambiental y formalizar actividades mineras;** según Negrete, esta norma es una "amnistía" para los proyectos minero-energéticos ilegales porque permite que reciban la licencia ambiental aunque ya hayan comenzado a operar.

### **El mensaje contradictoria del Plan de Desarrollo** 

Otro aspecto preocupante de la derogatoria de los artículos, es que estas modificaciones no son explicadas o justificadas en el resto del Plan de Desarrollo. De hecho, Negrete sostiene que hay una **inconsistencia entre lo que se propusó en las bases del documento y lo que se manifiesta en los articulados**.

Los bases contenían propuestas que abordan la biodiversidad, el cambio climático, la deforestación, la transición energética, la conservación, el uso sostenible, los mercados verdes, entre otros temas, que ya no** se reflejan en los articulados**. Incluso, el abogado sostiene que las energías limpias no son mencionadas y que en cambio, sí se plantea el uso de la técnica de fracturacion hidraulica, conocida como fracking.

### **El retroceso del Gobierno** 

Los reclamos de las organizaciones ambientales sobre el PND son numerosos y ya el Gobierno nacional ha anunciado el retiro de un artículo. Se trata de una norma que deroga el artículo 173 de la Ley 1753 de 2015, en el cual se establece el plazo para **la delimitación de páramos y que prohíbe actividades agropecuarias, de exploración o explotación de recursos naturales en este ecosistema**. (Le puede interesar: "[A poco pasos de convertirse en ley proyecto que protege los páramos](https://archivo.contagioradio.com/a-pocos-pasos-de-convertirse-en-ley-proyecto-que-protege-los-paramos/)")

Según el Ministerio de Medio Ambiente y Desarrollo Sostenible, "para evitar interpretaciones jurídicas o técnicas, así como para dar mayor claridad sobre la importancia de la protección de los páramos para la sostenibilidad de Colombia, ese Ministerio y el Departamento de Planeación Nacional **tramitarán la exclusión de dicha derogatoria**”.

### **La participación de las comunidades** 

Negrete relata que la Bancada Alternativa está realizando un estudio "exhaustivo y ciudadosa" de las propuestas del Gobierno y al mismo tiempo, elaborando contrapropuestas. (Le puede interesar: ¿[Plan de Desarrollo Nacional de Duque se olvidó de la paz?](https://archivo.contagioradio.com/plan-de-desarrollo-olvido-paz/)")

Sin embargo, sostiene que es necesario que el Congreso realice un debate amplio, **con la participación de las comunidades afectadas**. Además, afirmó que la Constitución Política establece que los entes territoriales, incluyendo a las entidades indígenas, deberían intervenir y opinar sobre el Plan de Desarrollo.

Estos dos puntos son particularmente importantes, dada la última decisión de la Corte Constitucional que dejó la consulta popular sin piso jurídico, un mecanismo de participación ciudadana que ya habían usado por lo menos ocho municipios para prohibir actividad de explotación minera y de hidrocarburos. Por tal razón, es necesario el fortalecimiento de la movilización social y los redes sociales para que transcienden sus opiniones.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
