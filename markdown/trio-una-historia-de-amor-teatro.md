Title: Trío, una historia de amor al arte
Date: 2019-05-08 13:00
Author: CtgAdm
Category: eventos
Tags: Música, teatro, teatro libre
Slug: trio-una-historia-de-amor-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-06-at-5.34.53-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Trio.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Congregación 

Cómo parte de su franja "Grupo Residente" el Teatro Libre presenta a grupo teatral **La Congregación**, quienes estrenaran el espacio con **Trio,** una obra teatral que promete ofrecer lo mejor de las artes escénicas y la música a los espectadores y fieles asistentes al tradicional teatro de la capital.

Trío, narra la historia de amor al arte, en el que el grupo musical Trinidad **le canta al oficio del artista desde canciones originales interpretadas por varios personaje**s; un cantante de restaurante del centro de Bogotá que se gana la vida recurriendo a la lástima e inventando que padece una enfermedad mortal; dos, cantante de buses que suplican ayuda para sacar a su hijo imaginario del hospital, y tres, estudiante que se defiende tocando guitarra en plazas y parques públicos para pagar el semestre en la Nacional.

Las historias de estos personajes se unen cuando una noche, durante una lluvia capitalina, se encuentran y componen una canción, que los hará ir juntos de “toque en toque” haciéndole el quite al desempleo, al hambre, en medio de la ilusión de un oficio que algún día les recompensará tan desmedido amor.

<iframe src="https://www.youtube.com/embed/U1ToN-P7BCs" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La Congregación, fundada en 2006 bajo la dirección de **Johan Velandia**, está integrada por **Diana Belmonte**, premio India Catalina 2019 a mejor actriz revelación del año por su papel en “Garzón”; **Santiago Alarcón**, premio India Catalina a mejor actor protagónico por la misma serie; **Milton Lopezarrubla, César Álvarez, Rafael Zea**, entre otros.

Trío, esta se presentará del **10 al 25 de mayo**, el costo de la boletería es de **40.000 pesos** y las funciones serán en el Teatro Libre de Chapinero.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-06-at-5.34.53-PM-300x157.jpeg){.aligncenter .wp-image-66568 width="552" height="288"}
