Title: ¡Que nadie se quede en casa! 21 de noviembre, paro nacional
Date: 2019-11-07 09:32
Author: CtgAdm
Category: Entrevistas, Movilización, Paro Nacional
Tags: marcha, oposición, Paro Nacional, Senadores
Slug: que-nadie-se-quede-en-casa-21-de-noviembre-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/paro-nacional.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PensiónSalario] 

Este miércoles, congresistas de la bancada alternativa en conjunto con sindicatos y movimientos sociales dieron una rueda de prensa en el Senado para anunciar su apoyo al paro nacional del próximo 21 de noviembre. En el evento, representantes a la cámara, senadores y activistas manifestaron las razones para acompañar dicha jornada de movilización, y señalaron las próximas fechas importantes en aras de preparar esa jornada.

### **Paro nacional contra las reformas laborales y pensionales**

En la rueda de prensa estuvieron presentes representantes de la Central Unitaria de Trabajadores (CUT) y la Confederación de Trabajadores de Colombia (CTC), quienes denunciaron que algunos empresarios ya están contando con una reforma laboral que aún no llega, y han empezado a despedir antiguos empleados para contratar personas jóvenes y pagar salarios bajos. Según Luis Ortíz, representante de la CTC, la reforma y el nivel de salarios actuales no permiten que los trabajadores crean en un futuro donde puede ver satisfechas sus necesidades.

Siguiendo con esta idea, el senador Alberto Castilla advirtió también sobre la presentación de una reforma pensional, que fortalecería los Beneficios Económicos Periódicos (BEPS) y de esta manera, eliminaría general de pensiones tal como se conoce actualmente. (Le puede interesar: ["De 6 países que privatizaron su régimen pensional, 4 revirtieron la decisión: Wilson Arias"](https://archivo.contagioradio.com/de-6-paises-que-privatizaron-su-regimen-pensional-4-revirtieron-la-desicion-wilson-arias/))

Por su parte, el senador Wilson Arias cuestionó que se esté pensando en una próxima reforma tributaria, mientras "2.400.000 colombianos pasan hambre y 600.000 han perdido su empleo". En ese sentido, el representante a la cámara Jairo Cala sostuvo que esta movilización, como las de Ecuador y Chile es una respuesta a la "crisis de un modelo económico que ya demostró que no sirve para las mayorías sino a pocos sectores económicos".

> Este 21 de Noviembre todos al [\#PARONACIONAL](https://twitter.com/hashtag/PARONACIONAL?src=hash&ref_src=twsrc%5Etfw) I Colombia unida contra el paquetazo anti popular y neoliberal del presidente Duque e impuesto por el FMI. Demostraremos que somos latinoamericanos dignos. ¡Viva Colombia libre! [pic.twitter.com/JQCBUjipYG](https://t.co/JQCBUjipYG)
>
> — CUT Antioquia (@CutAntioquia) [November 5, 2019](https://twitter.com/CutAntioquia/status/1191810546527543296?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **"El Gobierno es enemigo de los jóvenes"**

Alejandro Palacio, líder de la Asociación Colombiana de Representantes Estudiantiles (ACREES), recordó que en el Presupuesto General de la Nación (PGN) se incluyó en su artículo 44 la posibilidad de que las universidades estatales paguen de sus presupuestos las demandas que pierda la nación. A ello se suma la preocupación expresada por la senadora Aída Avella, respecto a las altas tasas de desempleo juvenil así como la intención de pagar menos de un salario mínimo durante el primer año laboral a los profesionales que se gradúen.

Nelson Alarcón, presidente de la Federación Colombiana de Trabajadores de la Educación (FECODE) señaló que los maestros también acompañarán el paro buscando que se modifique el Sistema General de Participaciones (SGP) para aumentar el aporte que hace el gobierno central a las entidades territoriales para cubrir servicios básicos como salud, educación o acceso a servicios públicos. Por estas razones, Palacio señaló que el Gobierno es enemigo de los jóvenes, y tienen razones suficientes para salir a protestar.

### **"Que el 21 no se quede nadie en la casa"**

El senador Gustavo Bolivar afirmó que en Colombia no se implementará un 'paquetazo' como ocurrió en Ecuador, sino que las reformas económicas, laborales y pensionales se introducirán una a una, por la misma razón, declaró que se debía generar una movilización consistente, "que se mantenga en el tiempo hasta lograr las aspiraciones de todos los sectores". En el mismo sentido, el senador Iván Cepeda aseveró que la movilización no debe ser interpretada como una confabulación de Cuba o Venezuela, sino como la respuesta a un conjunto de medidas que no benefician a los colombianos.

En su explicación, Cepeda argumentó que era momento de que el Gobierno cambie su política, porque "se ha dedicado a desenterrar las prácticas de la seguridad democrática" y "parece como si hubiéramos involucionado en el tiempo diez años". En consonancia, el representante Cala concluyó que por el apoyo al Acuerdo de Paz y en rechazo a los líderes sociales, integrantes de los pueblos indígenas y excombatientes, "que el 21 no se quede nadie en la casa".

### **Otras fechas a tener en cuenta antes del gran paro nacional**

El senador Alberto Castilla sostuvo que el próximo 12 de noviembre se realizará una reunión en la vicepresidencia del Senado junto al senador Alexander López para preparar detalles del paro nacional, asimismo, invitó a un debate de control político que se desarrollará el miércoles 13 de noviembre sobre la implementación de una política externa de tipo económico que se está imponiendo a los Estados, y que se realizará en la comisión séptima.

También el senador Antonio Sanguino anunció que haciendo uso del estatuto de oposición, que brinda la posibilidad a los partidos declarados de esta manera de proponer la agenda legislativa en tres ocasiones por periodo, realizarán una jornada de control político a los ministros el próximo 19 de octubre sobre el paquetazo económico que se quiere impulsar en el Congreso.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
