Title: Oh problema: Los emocionales
Date: 2018-02-22 09:31
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: elecciones, politica
Slug: oh-problema-los-emocionales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/amor-y-odio-politica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: reflexión revolucionaria 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 21 Feb 2017 

[El problema político más grave que tiene Colombia no son los funcionarios corruptos. El problema político más grave consiste en que no se ha comprendido para qué sirve la política. Una pequeña parte de la gente cree que la política es un negocio al cual hay que acceder para enriquecerse. Otra gran parte de la gente cree que la política es un juego en el que siempre pierde a causa de aquellos que accedieron al juego simplemente para enriquecerse.]

[Sellado semejante círculo vicioso de la desconfianza y el despilfarro, poco o nada se fomenta la comprensión lógica e histórica del funcionamiento de tamaña perversidad, pues eso requeriría tiempo y un esfuerzo racional considerable. ¿Entonces? ¿qué es lo que ha mediado? ¿qué es más fácil que leer y tomarse el trabajo de pensar? … entregarse a las emociones.]

[El amor y el odio están liderando unos impulsos que, aunque pretendan funcionar como productores de supuestas opiniones políticas, continúan emergiendo al vaivén de las coyunturas, dando como resultado que así de rápido como emergen, aún más rápido desaparezcan. ¡Claro! como es de esperarse, cuando una acción es hecha con odio, envidia, recelo, cabe la posibilidad de que aparezca la violencia...o como dirían los sociólogos: las violencias. Los emocionales, finalmente fomentan todo, menos la política, son sin duda un gran problema.]

[Resulta incoherente afirmar que la política son los funcionarios corruptos, o creer literalmente que el problema de la política es la corrupción; quedarse en esa justificación, es ser un dócil y domesticado sujeto, que no se pregunta por aquello que causa la corrupción de una manera tan sistemática como está presente en Colombia… ¿pero acaso no es eso lo que hacen todos y cada uno de los políticos? ¿prometer que lucharán contra la corrupción?]

[A la hora de pensar políticamente, emociones como el odio y el amor sin estar mezcladas con un análisis serio del panorama social, histórico y político, son tan útiles como la secretaría de seguridad de Peñalosa. No sirven para nada. Pues las emociones prefieren impulsar aquello en lo que se cree por encima de lo que realmente es; por eso, es que hoy sufrimos de esa latosa]*[opinitis aguda]*[.]

[No era para más, ya que los emocionales de la política, parten de dos falsedades tremendas: la primera, creer que son objetivos. La segunda, creer que en la política existen puntos medios.  No hay objetividad en la política, porque los intereses políticos de un sujeto pueden estar mediados por dos cosas: por su interés de clase social (una consciencia política), o por andar apoyando a quien no le representa (una inconsciencia política).]

[Andar apoyando a quien no les representa es típico, porque la publicidad y la propaganda, que son mecanismos netamente emocionales, son una compota, una papilla mucho más fácil de digerir que un buen análisis histórico de su condición social. Lo anterior responde de paso, el por qué no hay puntos medios, y aunque es una realidad que no necesariamente significa guerra, o violencia, sí significa plenamente DIFERENCIA.]

[Amor y odio movilizan “la política” en Colombia, porque pensar profundamente es más demorado, desalentador y complejo.]*[Mejor votar por éste porque que tal gane ese otro infeliz.]* *[Ojalá ese otro desgraciado nunca gane]*[,]*[por eso prefiero votar por este desdichado]*[,]*[que, aunque filibustero y pillo, entre los dos, es el menos hijueputa]*[. ¿parece una blasfemia? ¿una exageración? Claro que no, claro que no … en las venas de nuestros barrios, lugares de trabajo u hogares, conocemos a muchos que opinan así. Así como opinan los emocionales… los agentes reproductores del problema político más grave que tiene Colombia: ignorar que la política sirve para configurar la posibilidad de vivir juntos, iguales pero diferentes. ¿será eso posible? … piense usted la respuesta.  ]

<iframe id="audio_23958426" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23958426_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
