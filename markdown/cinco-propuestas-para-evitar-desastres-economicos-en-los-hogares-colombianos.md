Title: Cinco propuestas para evitar desastres económicos en los hogares colombianos
Date: 2020-03-19 18:09
Author: AdminContagio
Category: Actualidad, Economía
Tags: economia, oposición
Slug: cinco-propuestas-para-evitar-desastres-economicos-en-los-hogares-colombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Propuestas-contra-el-COVID-19.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @EuskJosean {#foto-euskjosean .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Congresistas de bancada de la oposición han señalado que Colombia se está enfrentando a un reto como ninguno con la epidemia del Covid-19, lo que en materia económica significa que el presidente Duque deberá decidir entre proteger a la mayoría de colombianos, o seguir beneficiando a los grandes capitales. Varios congresistas, siguiendo políticas como las que se están tomando en Francia, han hecho propuestas para beneficiar a la población en momentos de cuarentena.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El COVID 19 afectará la economía, sobre todo a los hogares de menores ingresos**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador por el Polo Wilson Arias explica que la economía sin duda se verá afectada por el Coronavirus, porque la producción disminuirá ante una cuarentena, que es la forma en que los países más afectados como China han logrado reducir los índices de infección. Sin embargo, recuerda que tal decisión afectaría a las personas más vulnerables.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ejemplo, a los más de 5 millones de personas que tienen trabajos informales y derivan su sustento del dinero que logran hacer a diario, o los más de 3 millones de desempleados. A ellos se suma que 6 de cada 10 adultos mayores de 60 años aún responden por alguien de su familia , y según estadísticas nacionales, solo el 24% de los mayores de 65 años tienen pensión.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1238570662329036804","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1238570662329036804

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **"Se está actuando a la medida de lo que pide Sarmiento Angulo y el Grupo Aval"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En sus declaraciones y decretos, el Gobierno dijo que acelerará la devolución de IVA para los hogares más vulnerables, concederá créditos a campesinos y otorgará un pago más de los subsidios del programa Familias en Acción, entre otras medidas. Por su parte, el Banco de la República puso a disposición de los bancos recursos hasta por 17 billones, en caso de que lleguen a presentar situaciones de iliquidez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Arias, las decisiones son una muestra de que "se está actuando a la medida de lo que pide Sarmiento Angulo y el Grupo Aval", y el que se 'permita' la refinanciación de los créditos no es una solución humanitaria sino una posibilidad que sigue beneficiando al sector financiero, que igual seguirá ganando dinero vía intereses.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Senador sostiene que se debería declarar una moratoria en las deudas del ICETEX, en el pago de los cánones de arrendamiento o de hipotecas para proteger a la población. Dichas decisiones se podrían emprender vía decreto, pero "las medidas exigentes de inversión social para salvar la vida de la gente no se están tomando".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Cinco propuestas para atender las necesidades de los colombianos**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para Arias, en primer lugar se debe flexibilizar la regla fiscal en Colombia, que establece unos topes máximos de endeudamiento, para invertir mayores recursos en aumentar la capacidad sanitaria del país. Según el Instituto Nacional de Salud (INS) en Colombia podría haber 500 mil casos graves de Covid-19 pero solo hay 8 mil camas en Unidades de Cuidados Intensivos (UCI) para atenderlos, por lo que se necesitaría aumentar la capacidad hospitalaria.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/wilsonariasc/status/1240337161779589126","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/wilsonariasc/status/1240337161779589126

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

De igual forma, propone que se apele a una política monetaria expansiva para que se produzca dinero por ejemplo, mediante la emisión de títulos, en aras de tener liquidez para inyectar al sistema de salud y hacer inversiones en obras para mover la economía una vez pase la epidemia. (Le puede intereasar: ["Nueva reforma tributaria significará 'crecimiento económico' para los de siempre"](https://archivo.contagioradio.com/nueva-reforma-tributaria-significara-crecimiento-economico-para-los-mismos-de-siempre/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Congresista manifiesta necesario un decreto que traslade recursos del Ministerio de Defensa al Ministerio de Salud y Departamento de la Protección Social para que se puedan realizar brigadas de prevención en los barrios, así como apoyar a los hogares más vulnerables en términos de alimentación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Arias también propone prohibir desalojos a deudores morosos para asegurar que todas las personas tengan un lugar en el cual pasar la cuarentena, y en el mismo sentido, garantizar el mínimo vital de agua, electricidad y teléfono para aliviar la carga económica de las familias. (Le puede interesar: ["Holding financiero; la jugadita de Duque privatizando los dineros públicos"](https://archivo.contagioradio.com/holding-financiero-la-jugadita-de-duque-privatizando-los-dineros-publicos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, el Senador por el Polo recuerda que el sector financiero en 2019 reportó ganancias por 11 billones de pesos, por lo tanto, este año se deberían modificar los beneficios tributarios que tienen esos grandes capitales y en su lugar, apoyar la industria nacional mediante subsidios y otros beneficios económicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Representantes a la cámara como [David Racero](https://www.facebook.com/DavidRaceroM/photos/a.359282227613868/1236811406527608/?type=3&theater) o[Katherine Miranda](https://twitter.com/MirandaBogota/status/1240669146276409344) también han hecho propuestas en el sentido de proteger la economía familiar. Para Arias, ante las propuestas y necesidades en las calles, el presidente Duque se encuentra en una disyuntiva en la que deberá decidir si ["salva al neoliberalismo o salva la vida de la gente"](https://twitter.com/wilsonariasc/status/1240745157730983938).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
