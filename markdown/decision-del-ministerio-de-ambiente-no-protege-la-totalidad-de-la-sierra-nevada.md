Title: "Sierra Nevada es como un cuerpo humano" no se le puede quitar un pedazo
Date: 2017-11-27 11:58
Category: Entrevistas, Movilización
Tags: arhuacos, Confederación indígena Tayrona, Indígenas arhuacos, Ministerio de Ambiente, Movilización Indígena, Sierra Nevada
Slug: decision-del-ministerio-de-ambiente-no-protege-la-totalidad-de-la-sierra-nevada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/arhuacos-indigenas-e1511801126214.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Aventure Colombia] 

###### [27 Nov 2017] 

Luego de casi un mes de movilizaciones, los indígenas del pueblo Arhuaco instalaron la mesa de negociación con el Gobierno Nacional. A pesar de que el Ministerio de Ambiente manifestó que **congelará las licencias de minería de 585 mil hectáreas**, que hacen parte del territorio de la Sierra Nevada, la Confederación Indígena Tayrona indicó que esta medida no protege la totalidad del territorio por lo que la amenaza minera continúa.

De acuerdo con Hermes Torres, secretario general de la Confederación Indígena Tayrona, lo afirmado por el ministro de Ambiente no está en la línea de la negociación y nada tiene que ver con la mesa abierta. Para Torres se trata de un anuncio, que en caso de cumplirse, sería insuficiente puesto que ellos y ellas están luchando por la defensa de la Sierra como territorio integral.

Indicó además que, “después de tantos días de espera, se instaló la mesa con el Gobierno Nacional”. Sin embargo, fue enfático en manifestar que **no se logró lo que los indígenas esperaban** en la medida que no se declaró la Sierra en su totalidad como un lugar libre de minería, “pero aceptaron que tenemos razones y que es necesario crear una hoja de ruta para encontrar una manera conjunta de proteger el territorio de la Sierra hasta la línea negra, considerado territorio ancestral”. (Le puede interesar: ["Más de 1000 solicitudes mineras amenazan la Sierra Nevada de Santa Marta"](https://archivo.contagioradio.com/arhuacos-continuan-en-movilizacion-exigiendo-se-respeten-sus-derechos-sobre-la-sierra-nevada/))

### **La Sierra Nevada es un ecosistema que funciona como un cuerpo humano** 

De acuerdo con el anuncio del Ministerio de Ambiente, Luis Gilberto Murillo, el Gobierno Nacional congeló los títulos y concesiones mineras de 585 mil hectáreas. Sin embrago, Torres indicó que esta medida **no es suficiente y no garantiza la protección de la totalidad del territorio ancestral**. Además, dijo la decisión está por fuera de los reclamos que han venido haciendo hace más de 30 días.

Si bien se está protegiendo un pedazo de la Sierra Nevada que corresponde a la parte alta, los Arhuacos indicaron que la parte baja queda desprotegida. Para ilustrar esto, Torres indicó que **el territorio ancestral funciona como un cuerpo humano**, “si cualquier parte del cuerpo tiene una dificultad, no estaríamos sanos, la Sierra Nevada funciona igual, no se puede pensar en proteger una sola área”. (Le puede interesar: ["Arhuacos exigen que la Sierra Nevada sea zona libre de minería"](https://archivo.contagioradio.com/arhuacos-exigen-que-la-sierra-nevada-sea-zona-libre-de-mineria/))

Es por esto que el discurso de los indígenas se ha basado en **entender el territorio de manera integral** donde se debe proteger en su totalidad. La Sierra, es un territorio que está delimitado por la llamada línea negra “que fue reconocida por el Gobierno Nacional mediante una resolución en el año 73 y va en torno a la totalidad de la Sierra Nevada”.

Esta línea fue ampliada y ratificada en 1995 por lo que los indígenas parten de este reconocimiento de su territorio para exigir que se proteja y se conserve **desde los cerros nevados hasta esa demarcación que se tiene**. Es por este que consideran que el anuncio del Ministerio de Ambiente, está por fuera de este escenario.

Finalmente, la movilización indígena buscará concretar con el Gobierno Nacional sus peticiones para proteger la totalidad del territorio. Para esto, han propuesto la creación de un mecanismo jurídico **que blinde la Sierra de cualquier actividad minera** debido a que “no solo se está protegiendo a los pueblos indígenas, sino a la sociedad en general”. Hay que recordar que de la Sierra Nevada nacen más de 750 quebradas y 36 ríos que abastecen a las poblaciones del Cesar, la Guajira y el Magdalena.

<iframe id="audio_22332308" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22332308_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
