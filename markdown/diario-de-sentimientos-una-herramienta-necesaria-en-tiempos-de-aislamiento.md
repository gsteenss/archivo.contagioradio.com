Title: Diario de Emociones, una herramienta necesaria en tiempos de aislamiento
Date: 2020-04-04 13:59
Author: CtgAdm
Category: Actualidad, Mujer
Tags: Covid-19, paz, Virtualmente
Slug: diario-de-sentimientos-una-herramienta-necesaria-en-tiempos-de-aislamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Diario-de-sentimientos-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Diario-de-sentimientos-2-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Por: Karla Trujillo. Virtualmente

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La emergencia sanitaria por el COVID-19 ha obligado al  
gobierno ha tomar medidas radicales para evitar la propagación del virus y el  
colapso del sistema de salud. Sin embargo, las disposiciones nacionales sobre  
la cuarentena han desnudado los grandes males de la sociedad como la pobreza,  
la inequidad, la precariedad laboral, el abandono Estatal, la discriminación y  
la violencia contra las mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, el confinamiento obligatorio ha dejado al descubierto muchas cosas que creíamos olvidadas como la importancia de cuidar nuestras relaciones personales, reflexionar sobre nuestra vida y dedicarnos tiempo para conocernos. En esta situación especial, nuestros hábitos y rutinas cambian, el exceso de información y el estar en nuestras casas durante bastante tiempo pueden generar emociones como miedo, tristeza, desolación e incertidumbre. Y para ello una de las herramientas es el **Diario de Sentimientos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es importante que logremos identificar que la aparición de estas emociones es normal, que generalmente vienen y se van y que están relacionadas con nuestra historia, nuestra identidad y con el momento que estamos viviendo. Para más información visita nuestro programa Virtualmente <https://www.facebook.com/contagioradio/videos/223700368714835/>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Queremos darte unos consejos  
para que puedas hacer un seguimiento a tus emociones usando un diario emocional  
que te permita llevar el registro de cómo te sientes a lo largo del día, cuáles  
son tus respuestas y qué puedes aprender de cada situación. Para esto, puedes  
tener un cuaderno y utilizar marcadores para decorarlo como quieras. También,  
puedes tenerlo en tu celular y sincronizarlo en todos tus dispositivos para  
llevarlo a la mano.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Diario de Emociones incluye las siguientes categorías:

<!-- /wp:heading -->

<!-- wp:list {"ordered":true} -->

1.  La **situación** que ha generado la emoción. Puedes escribir los  
   detalles que quieras y de forma objetiva.
2.  Los **pensamientos** que tuviste en tu cabeza cuando se  
   presentó la situación.
3.  Las **sensaciones físicas** que experimentaste en ese  
   momento.
4.  Las **emociones** que sentiste. ¡Pilas! Indaga muy bien,  
   tal vez la primera emoción que sientes está enmascarcando otras.
5.  El **significado** de tus emociones: ¿qué crees que te  
   quieren decir?
6.  Las **acciones** que hiciste después de sentir la emoción.  
   ¿cómo las expresaste y cómo actuaste?
7.  El **plan de acción**: Trata de elaborarlo después de  
   analizar lo que sientes y tus acciones. Piensa en qué podrías hacer si la  
   situación se repite.
8.  El **aprendiza**je: No olvides incluir las conclusiones  
   del día a día, piensa si tu cuerpo te ayudó a identificar tus emociones,  
   si actuaste de la mejor manera y cómo puedes mejorar con tu plan de  
   acción.

<!-- /wp:list -->

<!-- wp:paragraph -->

Recuerda que no  
importa si en el primer momento no puedes llenar todas las casillas, a  
medida que pase el tiempo y cojas práctica será más fácil ser consciente de tus  
emociones y cómo puedes actuar cuando las experimentes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Acá te dejamos dos ejemplos de cómo puedes diligenciar tu diario. <https://www.somosinteligenciaemocional.com/el-diario-emocional/>

<!-- /wp:paragraph -->

<!-- wp:image {"id":82808,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Diario-de-sentimientos-2-1-723x1024.jpg){.wp-image-82808}

</figure>
<!-- /wp:image -->
