Title: El sistema de salud está totalmente colapsado afirman gremios de la salud
Date: 2020-07-23 21:19
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Colapso hospitalario, Covid-19, Ley 100, pandemia, sistema de salud
Slug: el-sistema-de-salud-esta-totalmente-colapsado-afirman-gremios-de-la-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Sistema-de-salud-colapsado.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Pruebas-covid-19-colapso-del-sistema-de-salud.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Ante el inminente colapso de la red hospitalaria y en general del sistema de salud para enfrentar uno de los puntos más críticos de la pandemia**, el foco de atención en Colombia se ha situado sobre este tema y en particular sobre la acción del Gobierno para contener las consecuencias de la expansión del virus. (Le puede interesar: [OtraMirada: ¿Y dónde están los recursos para atender la pandemia?](https://archivo.contagioradio.com/otramirada-y-donde-estan-los-recursos-para-atender-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Carolina Corcho, vicepresidenta de la Federación Médica Colombiana, apoyada en cifras del Observatorio Fiscal de la Universidad Javeriana, **«*de la promesa inicial del Gobierno para girar 7 billones al sector salud, solo se han girado 0,9 billones*»**;  pese a que este es un sector que tiene un déficit cercano a los 15 billones de pesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El colapso, según agremiaciones y trabajadores de la salud, afecta cada fase del sistema desde su etapa diagnóstica y preventiva hasta sus fases asistencial y administrativa. (Le puede interesar: [El 70% de municipios dependen de una red de salud pública ilíquida y en pandemia](https://archivo.contagioradio.com/el-70-de-municipios-dependen-de-una-red-de-salud-publica-iliquida-y-en-pandemia/))   

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La crisis en la fase diagnóstica

<!-- /wp:heading -->

<!-- wp:paragraph -->

Marlene Vélez, presidenta del [Colegio Nacional de Bacteriología](https://cnbcolombia.org/), **señaló que existe un colapso en la fase diagnostica porque el país no estaba preparado en infraestructura, personal y equipos médicos para practicar oportuna y masivamente las pruebas de Covid-19.** (Le puede interesar: [San Juan de Dios es monumento nacional que debería reforzarse y no demolerse](https://archivo.contagioradio.com/san-juan-de-dios-es-monumento-nacional-que-deberia-reforzarse-y-no-demolerse/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, señaló que **en Colombia son apenas 97 los laboratorios habilitados para realizar las pruebas**, los cuales son insuficientes teniendo en cuenta que ni siquiera se ha llegado al pico máximo de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Vélez, afirmó que la etapa diagnóstica —fundamental para la efectiva contención del virus— representada por los bacteriólogos y epidemiólogos que trabajan en los laboratorios, habían sido un sector desconocido completamente por el Gobierno en sus políticas y medidas adoptadas.  

<!-- /wp:paragraph -->

<!-- wp:image {"id":87207,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Pruebas-covid-19-colapso-del-sistema-de-salud-1024x576.jpg){.wp-image-87207}  

<figcaption>
Foto: Getty Images

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:heading {"level":3} -->

### La crisis de la fase asistencial

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cecilia Vargas, presidenta de la [Organización Colegial de Enfermería](https://www.oceinfo.org.co/), señaló que después de casi 130 días de pandemia y confinamiento en el país «*quedó demostrado que la Ley 100 no tiene respuestas para una situación de emergencia*». También aseguró que **existe «*un déficit de personal de la salud muy grande*» para atender la crisis**, pese a lo cual, **ha habido despidos, incluso de trabajadores de la salud que resultaron contagiados con el Covid-19 ejerciendo su labor.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Cecilia Vargas coincidió en que existe un colapso del sistema de salud y una falta de efectividad en la política pública del Gobierno para manejar la crisis, lo que ha llevado incluso a que **al menos 4.067 trabajadores del sector salud estén contagiados del virus con corte al 22 de julio** según cifras del Instituto Nacional de Salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Carolina Corcho, vicepresidenta de la [Federación Médica Colombiana](https://federacionmedicacolombiana.com/), se sumó a la opinión de que el sistema de salud está colapsado, señalando que actualmente nuestro país está ante unos picos de contagio incluso más altos de los que tuvo Europa en su peor momento y fue enfática en afirmar que si los sistemas de salud francés, español e italiano, colapsaron frente a un número inferior de infectados de los que hoy tiene Colombia, **era un sinsentido decir que el sistema colombiano no está colapsado**, teniendo en cuenta que es un sistema más frágil respecto a los europeos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También sostuvo que **existen retrasos de hasta 15 días para la toma de muestras** porque las pruebas las están practicando las EPS, entidades que no cuentan con la suficiencia operativa para adelantar esta gestión de manera eficaz y que **esto ha llevado a que no se haya podido establecer un mapa epidemiológico  que dé cuenta de la magnitud real de la crisis del Covid-19 en el país.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**«*Estamos viendo el país con 15 días de retraso*»**, señaló la doctora Corcho, quien dijo que aunado a eso, las cifras estaban siendo subestimadas porque el **número de pruebas practicadas por día no era suficiente para el total de la población en Colombia** y que esto es particularmente grave para la adopción de medidas de política pública, pues éstas, se toman con base en la información de la curva de contagio.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **El sistema de salud es demasiado frágil. Este es un sistema que sin pandemia tiene 1 millón de quejas y 210 mil tutelas al año por inatención. Un déficit de 15 billones de pesos y un 80% de sus trabajadores precarizados laboralmente, luego, no pueden decir que con la pandemia va a funcionar mejor. Eso no cabe en ninguna lógica»**
>
> <cite>Carolina Corcho, vicepresidenta de la Federación Médica Colombiana</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### La crisis del sistema de salud en su integralidad y su necesidad de una reforma estructural

<!-- /wp:heading -->

<!-- wp:paragraph -->

Yesid Camacho, expresidente de la Asociación Nacional Sindical de Trabajadores y Servidores Públicos de la Salud y Seguridad Social Integral de Colombia -[ANTHOC](http://www.anthoc.org/)-,aseguró que el problema no es del virus, sino de un sistema de salud que obedece a unos cánones del modelo económico capitalista y que la mejor muestra de ello es que **las EPS «*hayan mejorado sus ingresos en más de 70 billones de pesos en los 4 meses que van de pandemia*»**, ya que, han disminuido ostensiblemente la prestación de servicios y por ende ha bajado la facturación de las clínicas y hospitales, sin que esto haya significado que a las EPS se les haya dejado de pagar los «*14 billones de pesos mensuales que valen las unidades de pago por capitación*» que pagan los usuarios del sistema.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **«En la medida en que las EPS no presten servicios, ni paguen lo que los hospitales facturan, más extraen del sistema. El sistema de salud no está en crisis, lo que está en crisis es la salud de los colombianos»**
>
> <cite>Yesid Camacho, expresidente de ANTHOC</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por lo anterior, Camacho, insistió en que **es importante impulsar una reforma estructural del sistema de salud y no quedarse en «*simples proyectos de ley que disfrazan la crisis*»**, refiriéndose puntualmente a la propuesta del senador Fabián Castillo del Partido Cambio Radical que entraría a reformar la Ley 100 de 1992.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/412854579652436","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/412854579652436

</div>

<figcaption>
Otra Mirada: Crisis de la salud

</figcaption>
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
