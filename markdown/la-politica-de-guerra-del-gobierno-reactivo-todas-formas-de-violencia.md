Title: “La política de guerra del Gobierno reactivó todas formas de violencia”
Date: 2020-11-23 22:22
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Argelia, Masacres, violencia
Slug: la-politica-de-guerra-del-gobierno-reactivo-todas-formas-de-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Violencia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En Colombia se vivió uno de los fines de semana más violentos en lo que va de este año 2020, caracterizado ya de por sí, por un recrudecimiento de la violencia expresada en asesinatos, masacres, desapariciones, desplazamientos y varios otros flagelos que aquejan a las comunidades a lo largo y ancho del territorio nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Cauca y Antioquia se perpetraron masacres que dejaron una cifra de 13 personas asesinadas; también se confirmó el asesinato de los docentes y líderes sindicales Byron Revelo Insuasty y Douglas Cortés en Nariño y Risaralda respectivamente; y el viernes se registró el homicidio del líder social Jhonny Walter Castro en el municipio de Linares, Nariño.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La respuesta institucional, ante este oscuro panorama de violencia, se dio a través del ministro de Defensa Carlos Holmes Trujillo, quien luego de realizar un consejo de seguridad en Betania, Antioquia; expresó que “*el narcotráfico es el peor enemigo de los colombianos*” y lo catalogó como la principal causa de la violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otra parte, pese al convulsionado y violento fin de semana y a las elevadas cifras de asesinatos y masacres en lo que va del año 2020, el ministro Holmes Trujillo expresó en una rueda de prensa, este lunes, que los “*principales indicadores de violencia y criminalidad han registrado una tendencia a la baja*”.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CarlosHolmesTru/status/1330892114683637763","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CarlosHolmesTru/status/1330892114683637763

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Visión reduccionista del Gobierno

<!-- /wp:heading -->

<!-- wp:paragraph -->

Frente a esto, Lourdes Castro, coordinadora del programa [Somos Defensores](https://somosdefensores.org/); señaló que **el Gobierno Nacional y en particular su Ministro de Defensa insisten en un “*negacionismo*” frente a la grave situación de derechos humanos en los territorios y en minimizar las causas de la violencia desde una posición reduccionista**, “*partiendo de un diagnostico muy parcial y limitado que es achacar la responsabilidad de todo lo que pasa al narcotráfico*”. Además, señaló que “*después de un fin de semana tan violento, seguir insistiendo en que los índices de violencia están disminuyendo es desconocer la realidad*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, el profesor Camilo González Posso, director del Instituto para el Desarrollo y la Paz -[Indepaz-](http://www.indepaz.org.co/); afirmó que las declaraciones del ministro Holmes Trujillo “*son absurdas*” al querer aplicar a todo la causa del narcotráfico, ya que, frente a asesinatos puntuales de líderes sociales y sindicales la hipótesis tendría que ser otra, pues **hay una “*acción criminal dirigida en contra de los líderes que encabezan acciones de protesta y se oponen a la política del Gobierno*”.** En relación con esto, el profesor González Posso, señaló que **el 45% de los líderes sociales asesinados habían participado activamente de marchas, mingas o movilizaciones; por lo cual englobar todas las causas de la violencia en el narcotráfico es una “*mitificación encubridora*”.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Habrá que preguntarle al Ministro \[Carlos Holmes Trujillo\] si los docentes, líderes del magisterio y los ambientalistas fueron asesinados por el narcotráfico”
>
> <cite>Camilo González Posso, director de Indepaz</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El profesor González Posso, agregó que esta posición del Gobierno desvía la atención de las acciones adelantadas en el marco de la política pública de seguridad, desorienta a las autoridades ante un diagnostico limitado y además “*pervierte la acción de la Fuerza Pública*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, Lourdes Castro, señaló que **no es que el Gobierno ignore lo que verdaderamente está pasando y las causas generadoras de violencia, sino que aún consciente de ello, no está interesado en contener este tipo de situaciones porque de uno u otro modo la violencia que se vive en el país, “*legitima el discurso político ideológico de su mentor que es el de la seguridad democrática*”.** (Le puede interesar: [Las razones que explican el regreso de las masacres a Colombia](https://archivo.contagioradio.com/las-razones-que-explican-el-regreso-de-las-masacres-a-colombia/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las causas generadoras de violencia desatendidas con la posición del Gobierno**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para Camilo González Posso, el discurso del Gobierno ignora todo lo que ha sido el fenómeno de violencias en Colombia al reducirlo todo a narcotráfico, ya que en el país hay disputas por la tierra y por lo recursos; conflictividades asociadas a grandes proyectos económicos de infraestructura o minero energéticos; agresiones producto de la reestructuración del paramilitarismo y **una multiplicidad de causas generadoras de violencia que han sido desatendidas detrás del discurso reduccionista del narcotráfico.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “En Colombia se ha consolidado un modelo violento de acumulación de riqueza y de poder; un modelo mafioso que se entrelaza con lo que ha sido todo el rastro y legado dejado por el paramilitarismo”.
>
> <cite>Camilo González Posso, director de Indepaz</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Además, señaló como otra de las causas de la violencia, la negativa del Gobierno a implementar integralmente el Acuerdo de Paz, lo cual ha generado la reactivación de violencias.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Se abandonó una política de paz para volver a asumir una política de guerra y eso está reactivando motores de violencia en muchos frentes”
>
> <cite>Camilo González Posso, director de Indepaz</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

En el mismo sentido, Lourdes Castro señaló que por parte del Gobierno “*se ha hecho evidente esa voluntad de querer hacer trizas el Acuerdo*”, lo cual no permite transitar en una ruta distinta a la violencia, en la que se priorice la construcción de paz.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “El incumplimiento del Acuerdo de Paz es un elemento fundamental de la recomposición de las violencias que estamos viviendo en los territorios”
>
> <cite>Lourdes Castro, coordinadora del Programa Somos Defensores</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
