Title: Casi 6.000 familias indígenas en riesgo por COVID en sus territorios
Date: 2020-04-18 14:39
Author: CtgAdm
Category: Nacional
Slug: casi-6-000-familias-indigenas-en-riesgo-de-crisis-humanitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/alimentacion-de-ninos-indigenas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 18 de abril la Organiza Nacional Indígena Colombiana (ONIC) en el reporte 015 de la situación de riesgo de los pueblos indígenas ante la pandemia, anunciaron que **la situación empeora afectando y poniendo en riesgo a 201.576 familias indígenas, 580 más que en el último reporte**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo denunciaron que **535.113 familias indígenas se encuentran en riesgo de de una crisis humanitaria**, y agregaron el reporte 5 nuevos casos de contagio al interior de las comunidades étnicas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dos del Pueblo Binacional Yukpa, dos más en del Pueblo los Pastos quienes actualmente se encuentran en recuperación, y el caso más reciente el de un hombre de 34 años del Pueblo Zenú.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"el riesgo en pueblos indígenas por falta de agua potable alimentos e implementos de bioseguridad y conflicto armado agravan la situación e incrementan las afecciones, alertamos a los pueblos indígenas que habitan los departamentos de Magdalena, Cauca y Huila adoptar todas las medidas necesarias "* , además agregaron que el contagio se ha trasladado a pequeños municipios como es el Timbio en Cauca y San Martín en César,

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 90 indígenas embera del Chocó reportan síntomas de COVID

<!-- /wp:heading -->

<!-- wp:paragraph -->

Hace menos de 24 horas la ONIC anunciaba que cerca de **90 indígenas pertenecientes al Pueblo Emberá del Chocó**, reportan síntomas como fiebre, tos seca, dolor de cabeza y cuerpo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma que en los últimos días en este territorio se r**eportaron 3 fallecimientos**, el primero de **menor de 3 meses** con claros síntomas de tos seca y fiebre, el siguiente otro **menor 4 meses**, con un fuerte dolor de oído y fiebre, y el último de una **mujer de 60 año**s con el mismo cuatro sintomatologico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta es una comunidad indígena Embera, habita en Peñas Blancas en el resguardo Peña Planca Río Traundó, en el Municipio de Rio Sucio, Chocó. A horas del casco urbano más cercano h**abitan estas cerca 600 familias y 320 personas de la etnia Emberá Dóbida**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto la [ONIC](https://archivo.contagioradio.com/tras-asesinato-de-indigena-pueblo-awa-exige-garantias-en-medio-de-las-balas-y-el-covid-19/)señaló que harán un especial monitoreo a la comunidad, que en las últimas horas generó una alerta epidemiológica con requerimiento de atención urgente, esperando una pronta respuesta del Gobierno Departamental. A esto suma que el Municipio de Riosucio no cuenta con UCO en el único hospital de segundo nivel.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Señalaron también en el reporte que los pueblos negros, además de tener varios casos de contagio, carecen de infraestructura hospitalaria, equipos y personal médico, y que además no han recibido el apoyo requerido; así como también indicaron que, **"*las pruebas no se están aplicando y demora mucho tiempo los resultados a esto se suman los panfletos amenazas a líderes y [persecución en el territorio](https://www.justiciaypazcolombia.com/persisten-asesinatos-en-putumayo/)"****.*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Avanzar en fortalecimiento espiritual cultural la guardia indígena y la soberanía alimentaria".*
>
> <cite>ONIC</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El llamado desde la ONIC es a los entes reguladores de la salud en el país para que junto con el Gobierno actúen de manera oportuna y en cumplimiento de sus mandatos constitucionales *"ante la alerta pandémica en las comunidades étnicas, así como también la urgencia ante apoyos humanitarios de elementos de bioseguridad para la guardia indígena, agua potable y sistema de riego que garantice la productividad de alimentos*".

<!-- /wp:paragraph -->
