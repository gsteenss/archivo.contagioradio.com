Title: Por la educación superior y los líderes sociales, ¡A la calle!
Date: 2018-12-13 10:51
Author: AdminContagio
Category: Educación, Movilización
Tags: estudiantes, marcha, Paro Estudiantil, UNEES
Slug: estudiantes-marchan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/ESTUDIANTES-MARCHAN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Dic 2018] 

Este miércoles se llevará a cabo la última marcha estudiantil del año en la que los jóvenes saldrán a las calles para protestar en contra de los asesinatos de líderes sociales, que solo desde que inició el gobierno Duque se contabilizan en más de 80 casos; contra las violaciones a los derechos humanos durante el paro estudiantil, y por la educación superior.

**Valentina Ávila, vocera nacional de la Unión Nacional de Estudiantes por la Educación Superior (UNEES)**, manifestó que a estos hechos se suma la poca iniciativa que ha tenido el Gobierno en la Mesa de Negociación con estudiantes y profesores, con la que buscan superar la crisis en la que se encuentran las **Instituciones de Educación Superior (IES)**. Evidencia de esa situación, es la negativa del Gobierno a aceptar las propuestas de recursos llevadas por los estudiantes.

En lugar de ello, el presidente Duque afirmó que el tope máximo de recursos que se podía entregar a las Universidades era igual al Indice de Precios al Consumidor (IPC), + 4,6 puntos porcentuales durante los próximos años. Entre tanto, **los estudiantes han propuesto diferentes formas para que el aumento sea de IPC+4,6 anual, es decir, un total de 560 mil millones de pesos al año**.

### **Instituciones de Educación Superior saldrán juntas desde la UN** 

En Bogotá, **todas las IES se concentrarán a las 12 del medio día en la Universidad Nacional**, allí se ofrecerá un almuerzo para quienes se movilicen. Posteriormente la marcha se desplazará por la calle 45 hasta la Avenida Caracas, luego, caminarán hasta la calle 32, lugar en el que tomarán la carrera séptima. (Le puede interesar: ["Con marcha, paro estudiantil conmemora 90 años de masacre de las bananeras"](https://archivo.contagioradio.com/masacre-de-las-bananeras/))

**La llegada final será en el parque de los periodistas, frente al edificio del Icetex**; Ávila indicó que esperan la participación de bastantes personas, aunque reconoció que dada la fecha de la marcha, y que muchos jóvenes regresan a sus lugares de origen para estar con sus familias, será menos numerosa que las movilizaciones de octubre y noviembre. (Le puede interesar: ["UNEES denuncia persecución y amenazas desde fuerza pública"](https://archivo.contagioradio.com/persecucion-fuerza-publica-estudiantes/))

### **Estudiantes pasan al tablero a Ministerio de Interior y de Defensa.** 

**Este viernes 14 de septiembre se llevará a cabo la audiencia pública en el Congreso de la República sobre la situación de los derechos humanos en el marco de las movilizaciones estudiantiles**. El evento se desarrollará entre las 8 de la mañana y la 1 de la tarde, y están invitados los ministros de Defensa, y de Interior, así como la Ministra de Educación, para que reciban de primera mano las denuncias que desde la UNEES se han hecho sobre represión al movimiento estudiantil.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
