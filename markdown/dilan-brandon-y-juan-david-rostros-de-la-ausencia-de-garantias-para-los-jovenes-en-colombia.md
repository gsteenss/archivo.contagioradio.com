Title: Dilan, Brandon y Juan David: rostros de la ausencia de garantías para los jóvenes en Colombia
Date: 2019-11-27 18:54
Author: CtgAdm
Category: DDHH, Nacional
Tags: educacion, Paro Nacional, servicio militar obligatorio
Slug: dilan-brandon-y-juan-david-rostros-de-la-ausencia-de-garantias-para-los-jovenes-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Diseño-sin-título-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

Los casos de los estudiantes **Dilan Cruz, ** **Juan David Rojas** y el testimonio del soldado **Brandon Cely,** quien se habría suicidado tras anunciar su apoyo al paro y ser acusado por sus superiores de  "izquierda extremista" y un "disociador", son un ejemplo de la realidad que viven los jóvenes con menores oportunidades en Colombia.

Al homicidio de **Dilan Cruz**, quien falleció como resultado del uso desmedido de la fuerza de agentes del **Escuadrón Móvil Antidisturbios** (ESMAD), se suma el de **Juan David Rojas**, un estudiante de séptimo grado del colegio **Alfonso López Michelsen** quien fue asesinado el 22 de noviembre en medio de las protestas populares que tenían lugar en el barrio Bosa El Recreo.

Según explica **Willie Carmona**, profesor de la localidad, si bien el asesinato del  joven, no fue una acción directa de un agente del Estado sino probablemente de un actor delincuencial, sí se da en el marco de las protestas que se han desarrollado en la localidad en medio del paro, pese a que tanto él, como Dilan se movilizaban por exigir los mismos derechos, su caso no ha alcanzado la misma trascendencia en la opinión pública.

"La noticia no ha tenido el bastante eco porque pareciera que se busca que pase como una muerte de tantas que suceden en este país y no en el marco de las protestas del paro" inquiere el profesor quien agrega que este caso obedece a una lógica de todos los territorios populares de Colombia, en las que "producto de un modelo económico que excluye a grandes capas de pobladores y en particular a los jóvenes que no tienen garantías de acceso al trabajo, a la salud, a la recreación o una vivienda digna y que terminan inmersos en contextos donde perviven la delincuencia común o el microtráfico.

### Tampoco hay garantías para los jóvenes en el Ejército 

Otro ejemplo de la difícil realidad que enfrentan los jóvenes de Colombia son los más recientes casos de dos soldados del Ejército Nacional quienes han expresado su inconformismo defendiendo al paro nacional y su derecho a la educación. El primero, Brandon Cely de 21 años quien tras ser estigmatizado por las autoridades de su  institución al expresar su apoyo al paro nacional y denunciar las falencias de la Fuerza Pública, anunció que terminaría con su vida, soñaba con ser médico pero nunca tuvo las condiciones económicas para lograrlo.

El segundo caso, el de Juan Sebastián Mendieta soldado del Batallón de infantería 39 quien realizó otro video apoyando el paro y que al negarse a borrarlo fue transferido a Bogotá y del que se espera exista nueva información pues el expresar su inconformidad y apoyar las protestas le conllevaría

> [\#ATENCION](https://twitter.com/hashtag/ATENCION?src=hash&ref_src=twsrc%5Etfw) El soldado JUAN SEBASTIAN MENDIETA HERRERA, después del video han solicitado que sea trasladado para el Batallón de la 106 en Bogotá, la última ubicación que reportó fue la Base Militar BONACA, por Granada, Cundinamarca. No se ha sabido más [pic.twitter.com/cbzbUrFUHp](https://t.co/cbzbUrFUHp)
>
> — Madame Simone ??? (@MadameSiimone) [November 26, 2019](https://twitter.com/MadameSiimone/status/1199430435115327489?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
"En la práctica, lo único fijo para un joven de un sector popular a los 18 años es prestar el servicio militar, situación que no aplica para los estratos 4, 5 o 6, que tienen  otras posibilidades" señala el profesor quien reitera la necesidad de que en Colombia el prestar el servicio obligatorio no exista. [(Lea también: La Objeción de Conciencia, un derecho ignorado en Colombia)](https://archivo.contagioradio.com/objecion-conciencia-derecho-ignorado/)

### Ausencia de oportunidades para la juventud 

El profesor Carmona argumenta que en Colombia, las personas entre los 18 y 24 años son quienes tienen una mayor dificultad a la hora de encontrar un trabajo bien remunerado, mientras que en Bogotá, tan solo el 4% de la población egresada de colegios públicos tienen acceso a la educación superior, una estadística que resalta cómo jóvenes como Dilan Cruz quien se encontraba finalizando su bachillerato o Brandon Cely quien aspiraba a estudiar medicina no tenían garantizado un acceso a la educación de cara a un Estado que permanece indolente ante esta situación.

"Cuando uno crece se estrella con una realidad social donde no hay empleo, no hay seguridad social, ni posibilidad de continuar sus estudios,  y donde la  agresión y estigmatización es diaria en los territorios ¿qué oportunidad tiene un joven en este país?", sentencia.  [(Le sugerimos leer: Las razones para pedir el desmonte del ESMAD a 20 años de su creación](https://archivo.contagioradio.com/las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion/))

Aunque los casos de estos jóvenes es la realidad que viven miles en el país, el profesor resalta la otra realidad que comienza a dibujarse  y es que en gran parte de los seis días de paro y movilización han caído sobre los hombros de los jóvenes, "tenemos esperanza de que esta juventud despierte de ese letargo, son las dos caras de la moneda", concluye el profesor.  [(Le puede interesar: Paro Nacional: una expresión ciudadana de contundente movilización)](https://archivo.contagioradio.com/paro-nacional-una-expresion-ciudadana-de-contundente-movilizacion/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_44903206" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44903206_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
