Title: Gobierno continúa incumpliéndole al Catatumbo
Date: 2016-08-01 16:37
Category: Movilización, Nacional
Tags: Catatumbo, Gobierno Nacional, Norte de Santander, Paramilitarismo
Slug: gobierno-continua-incumpliendole-al-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/catatumbo-e1470086923403.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ASCAMCAT 

###### [1 Ago 2016] 

<div>

<div class="texto_80">

Luego de tres años de negociaciones a través de la Mesa de interlocución y acuerdo, los campesinos del Catatumbo denuncian que el gobierno les sigue incumpliendo y pese a los múltiples anuncios mediáticos de inversiones para esta zona del país, aseguran que eso no se ha materializado y en cambio **de los 14 puntos establecidos solo se ha logrado acuerdos en 3.**

La semana pasada se tenía planeada una sesión de interlocución en la ciudad de Cúcuta, sin embargo no hubo tal reunión como lo asegura Juan Carlos Quintero, vocero de ASCAMCAT, quien expresa que esa negociación “**Ha sido un escenario en el que el gobierno ha incumplido en un 60%**, de 14 puntos solo se han firmado acuerdos en 3, y además el gobierno ha querido disminuir este espacio en una mesa técnica”.

El jueves se conoció las cartas enviadas por los campesinos a la mesa de conversaciones de La Habana, **donde denunciaban los 59 asesinatos que se han presentado este año y la persistencia del paramilitarismo en la zona,** además de los intentos de parte del gobierno por deslegitimar las organizaciones del departamento.  Según el vocero de ASCAMCAT, estas cartas habrían generado una reacción de rabia y rechazo por parte del gobierno, por lo que luego el viceministro de agricultura, Juan Pablo Díaz Granados dijo que no iba a estar en la mesa del Catatumbo porque había una mesa paralela en el municipio de Tibú.

Frente a ese incumplimiento, el viernes se convocó a la comunidad para realizar un plantón pacífico en Tibú, allí exigían que permitieran la participación de los campesinos del Catatumbo, sin embargo, solo se admitió la entrada de tres líderes campesinos, sin lograrse algún tipo de acuerdo.

Ante esa situación ASCAMCAT **solicita una reunión urgente de alto nivel con los garantes y los ministros de Interior, Agricultura y Posconflicto** para establecer un acuerdo político y una ruta de cumplimiento de los compromisos del Gobierno con los campesinos del Catatumbo.

Aunque través de medios de comunicación **desde 2012 se han anunciado inversiones por 5 billones de pesos para el Catatumbo, desde ASCAMCAT** se asegura que tales inversiones no se han visto en el territorio, de manera que si así continúa la respuesta del gobierno a las necesidades del campesinado, posiblemente haya jornadas escalonadas de movilización y como última medida se piensa avanzar hacia un paro.

<iframe src="http://co.ivoox.com/es/player_ej_12407967_2_1.html?data=kpehkpydepihhpywj5aUaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYpcbWsNDnjLbiy9PYqdPjhpewjaa3h6LBpKbBj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

</div>

</div>

 
