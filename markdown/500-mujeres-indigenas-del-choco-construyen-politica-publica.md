Title: 500 Mujeres indígenas del Chocó construyen política pública
Date: 2016-04-01 16:29
Category: Mujer, Nacional
Tags: Mujeres indígenas, Primer congreso de mujeres indigenas del departamento del Chocó
Slug: 500-mujeres-indigenas-del-choco-construyen-politica-publica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/IMG_0462-e1459545414629.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### **[Foto: mesa indígena del depto. del Chocó]** 

###### **[1 abr 2016]** 

Desde el 31 de marzo al 1 de abril en Quibdó, Chocó, se realiza el Primer congreso de mujeres indígenas de ese departamento. 500 mujeres de seis pueblos indígenas buscan construir una política pública que incluya las necesidades de las mujeres para el goce de sus derechos.

Lucy Chamorro Caldera, integrante de la Mesa Indígena del departamento del Chocó, explica que en los lineamientos de la política pública se debatirán los derechos sexuales y reproductivos de las mujeres, territorios de paz en comunidades y resguardos indígenas, educación con enfoque diferencial, sistema de salud indígena propio, y derechos humanos, así como la afectación de los territorios en los resguardos.

Las mujeres de los pueblos Emberá Dovidá, Emberá Chamí, Katío, Eyábida, Wounaan, y Tule aseguran que debe haber territorios de paz con planes integrales acordes a cada particularidad de los pueblos indígenas desde un enfoque diferencial, y aspiran que los resultados sean implementados en los planes de desarrollo del departamento.

<iframe src="http://co.ivoox.com/es/player_ej_11013640_2_1.html?data=kpadk5iaeJGhhpywj5WbaZS1kpuah5yncZOhhpywj5WRaZi3jpWah5ynca3pxN6Ypc3FsdDm09SYj5CVqdOfxNTbydfJt9CfxcqYz9rOqdPZ1JDW0MmJh5SZoqnUx9PFt4zYxtGYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
