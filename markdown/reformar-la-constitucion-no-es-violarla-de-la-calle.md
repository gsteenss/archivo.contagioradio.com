Title: "Reformar la constitución no es violarla" De la Calle
Date: 2015-08-18 15:02
Category: Nacional, Política
Tags: ELN, FARC, Juan Manuel Santos, paz con el eln, Proceso de conversaciones de paz
Slug: reformar-la-constitucion-no-es-violarla-de-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Humberto-de-la-calle-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpueblo.com.co 

###### [18 Ago 2015 ] 

El jefe del equipo negociador del Gobierno en la Mesa de Diálogos de la Habana, Humberto De La Calle, se pronunció en rueda de prensa sobre la propuesta de crear una comisión especial para la implementación de los acuerdos resultantes del proceso.

De La Calle afirma que esta alternativa no es una propuesta definitiva; sin embargo, de consolidarse no implicaría la revocatoria del actual Congreso ni la derogación de procedimientos constitucionales: "*Reformar la Constitución no es violarla. Lo acordado debe ser convertido en realidad y si se necesitan reformas constitucionales para ello, pues hay que hacerlas*".

De La Calle aseguró además que la **creación de la Comisión Especial, mal llamada ‘Congresito’**, se haría mediante un acto legislativo que permitiría la participación tanto de las FARC como del actual Congreso, para legislar en temas **justicia transicional, participación en política y desarme**. La polémica en relación con denominar esta alternativa como ‘Congresito’ radica en que éste hace alusión a la creación de una Asamblea Nacional Constituyente, tal como ocurrió en 1991.

Otra de las alternativas que se estiman es la aplicación de un referéndum, en el mes de octubre, a través del cual se interrogue a la sociedad colombiana sobre su credibilidad respecto a los actuales diálogos, que según las últimas encuestas va en detrimento.
