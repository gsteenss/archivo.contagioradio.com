Title: Palestinos e israelíes marchan contra ocupación
Date: 2016-04-06 15:37
Category: Otra Mirada
Tags: Cisjordania, Ocupación israelí, Palestina
Slug: palestinos-e-israelies-marchan-contra-ocupacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Marcha-Palestinos-Israelies.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Activestills Org ] 

###### [6 Abril 2016 ]

Más de 300 palestinos e israelíes marcharon el pasado viernes en Cisjordania, para exigir que cese la ocupación y el establecimiento de asentamientos de colonos. La movilización se realizó con el fin de mostrar la unión entre el pueblo palestino y el israelí.

La marcha partió de 'Tunnels Checkpoint', el principal puesto de control en Jerusalén y terminó en Husan Junction, punto que advierte a los israelíes de que están en territorio palestino.

En esta jornada no se presentaron acciones violentas y la principal consigna fue 'Hay Otro Camino'. Movilizaciones similares han sido promovidas mensualmente por una red de Organizaciones por la Paz y Contra la Ocupación.

Pese a estas muestras de [[solidaridad entre pobladores palestinos e israelíes](https://archivo.contagioradio.com/gaza-se-ha-convertido-en-una-carcel-a-orillas-del-mar-mediterraneo/)], según fuentes locales, este miércoles las Fuerzas de Seguridad de Israel arrestaron a trece palestinos y demolieron ocho viviendas durante una redada en Cisjordania y Jerusalén.

Tras la demolición se dieron enfrentamientos entre los palestinos y las fuerzas israelíes, en los que dos palestinos fueron heridos con disparos en sus piernas y decenas se afectaron con el gas lacrimógeno.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
