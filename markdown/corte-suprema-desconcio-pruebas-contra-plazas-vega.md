Title: Las pruebas contra Plazas Vega que fueron “desconocidas” por la Corte Suprema
Date: 2015-12-17 15:07
Category: Judicial, Nacional
Tags: Ccajar, CEJIL, Comisión de Justicia y Paz, holocausto del Palacio de Justicia, Palacio de Justicia, plazas vega
Slug: corte-suprema-desconcio-pruebas-contra-plazas-vega
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/holocausto-palacio-de-justicia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elpais 

###### [17 Dic 2015]

Según los apoderados de los familiares de las víctimas del Palacio de Justicia, la Corte Suprema estudió una serie de pruebas testimoniales, las corroboró y concluyó que había dudas razonables, sin embargo **hubo muchas otras pruebas que no estudió y que permiten concluir que el Coronel ® Plazas Vega tuvo responsabilidad directa** en los crímenes cometidos y que era responsable también la cadena de mando.

En un comunicado público de este 17 de Diciembre, una vez notificados del fallo, el Centro por la Justicia y el Derecho Internacional (CEJIL), el Colectivo de Abogados “José Alvear Restrepo” (Cajar) y la Comisión Intereclesial de Justicia y Paz, publicaron un comunicado en el que explican la serie de **pruebas que la Corte habría desestimado para tomar la decisión de la absolución.**

### **Testimonios de sobrevivientes y otros testigos directos** 

Por ejemplo, las organizaciones de DDHH explican que hay pruebas suficientes que ubican a Plazas Vega *“en el puesto de mando adelantado ubicado en la Casa del Florero, gestionando el traslado, las órdenes de tortura y retención a las personas sospechosas de ser guerrilleras o ’especiales’”*, situación que sería indicativa de la responsabilidad directa del militar.

Otro de los aspectos que señala el comunicado tiene que ver con los testimonios de las propias víctimas que lograron salvar sus vidas, incluso, después de ser torturadas y conducidas a la Escuela de Caballería por órdenes de Plazas.

### **Pruebas documentales, fotografías y videos** 

También mencionan las pruebas fotográficas y documentales en que se comprueba que “*se garantizó la disposición previa de sitio para ubicar el Puesto de Mando Adelantado en la Casa del Florero y en donde se apostarían los miembros del Ejército una vez iniciada la toma, con los álbumes fotográficos de los integrantes del M-19*”.

Además, las organizaciones de DDHH reiteran que acudirán ante instancias internacionales como la Corte Interamericana de Derechos Humanos que ya emitió un fallo y a la que la Corte Suprema también sobrepasa al solicitar la suspensión de las órdenes emitidas por ese tribunal como parte de la reparación a las víctimas del Estado.

A continuación compartimos el comunicado completo…

#### "Washington D.C., 17 de diciembre de 2015.- El día de ayer, la Sala Penal de la Corte Suprema de Justicia de Colombia (CSJ) absolvió al coronel (r) Luis Alfonso Plazas Vega con una votación de tres a cinco a su favor, ordenando su libertad inmediata. De esta manera, Plazas Vega queda eximido de toda responsabilidad por la desaparición forzada de dos personas en la retoma del Palacio de Justicia ocurrida en Bogotá el 7 de noviembre de 1985 y ordena investigar a los miembros de las fuerzas militares por exceso del uso de la fuerza durante la operación. 

#### El Centro por la Justicia y el Derecho Internacional (CEJIL), el Colectivo de Abogados “José Alvear Restrepo” (Cajar) y la Comisión Intereclesial de Justicia y Paz manifiestan su rechazo ante esta decisión, resultado de un estudio de un recurso de casación en contra del fallo de segunda instancia que condenó al coronel (r) Plazas Vega a 30 años de prisión. 

#### A pesar de las dos condenas previas y las numerosas pruebas existentes en los procesos, la Corte Suprema considera en casación que no existen suficientes evidencias que relacionen a Plazas Vega con la desaparición de Irma Franco Pineda y Carlos Augusto Rodríguez Vera. 

#### Al respecto, existen gran cantidad de pruebas testimoniales que al parecer fueron desconocidas por el Alto Tribunal y que sitúan al Coronel Plazas Vega en el puesto de mando adelantado ubicado en la Casa del Florero, gestionando el traslado, las órdenes de tortura y retención a las personas sospechosas de ser guerrilleras o “especiales”. Por ello, resulta inexplicable que la Sala desconozca la participación directa de Plazas Vega en la desaparición forzada de Irma Franco Pineda y Carlos Augusto Rodríguez Vera, quienes salieron con vida del Palacio en poder del ejército y fueron conducidos a este puesto de mando sin que hasta hoy se tenga conocimiento de su paradero. 

#### Además, como parte del plan elaborado por el Ejército Nacional, se garantizó la disposición previa de sitio para ubicar el Puesto de Mando Adelantado en la Casa del Florero y en donde se apostarían los miembros del Ejército una vez iniciada la toma, con los álbumes fotográficos de los integrantes del M-19. En este lugar también fue ubicado Plazas Vega por las víctimas de tortura que rindieron declaración dentro del proceso penal. 

#### Asimismo, la decisión de la CSJ declara parcialmente nulas una serie de reparaciones que, en su momento, fueron ordenadas de acuerdo a los estándares de derecho internacional, así como a la publicación de la sentencia en la web del Ministerio de Defensa y Ejército Nacional por un año como una garantía de no repetición; la prohibición de que las unidades militares lleven el nombre del acusado; y la celebración por parte del Ejército de un acto de reconocimiento público para pedir perdón por los actos que llevaron a la desaparición forzada de la dos víctimas. 

#### “El fallo es una afrenta a la verdad y a la justicia por las cuales las víctimas, sus familiares y la sociedad colombiana llevan luchando durante más de 30 años. A través de esta decisión la Corte Suprema de Justicia demuestra un gran desconocimiento y el desacato de las obligaciones internacionales del Estado colombiano en el caso del Palacio de Justicia”, señaló Viviana Krsticevic, Directora Ejecutiva de CEJIL. 

#### El 9 de junio de 2010, el coronel (r) fue condenado en primera instancia por el Juzgado Tercero Penal del Circuito Especializado de Bogotá a 30 años de prisión por la desaparición forzada de 12 personas. Dicha sentencia fue ratificada por el Tribunal Superior de Bogotá el 30 de enero de 2012, en la que Plazas Vega fue juzgado como “autor mediato” de la despaparición de Irma Franco y Carlos Augusto Rodríguez Vera. Con ello, Plazas Vega fue una de las primeras personas condenadas en segunda instancia en relación al caso del Palacio de Justicia. 

#### “Esperamos que las propuestas que están siendo realizadas por la Fiscalía General de plantear nuevas líneas de investigación sobre del abuso de fuerza en la retoma del Palacio de Justicia por parte de integrantes Fuerza Pública colombiana, rindan resultados concretos y duraderos de sancionar a los responsables de los hechos”, dijo Rafael Barrios, abogado de Cajar. 

#### El 10 de diciembre de 2014 la Corte Interamericana de Derechos Humanos (Corte IDH) declaró responsable al Estado colombiano por la desaparición de 11 personas durante la retoma del Palacio de Justicia. El Tribunal interamericano ordenó al Estado llevar a cabo la investigación y sanción de todos los responsables. Hasta la fecha existían solamente dos condenas por algunas de las desapariciones forzadas. Una contra el general (r) Arias Cabrales por la desaparición de cinco víctimas y otra contra el coronel (r) Plazas Vega, quien se encontraba privado de libertad desde hacía nueve años. La absolución de Plazas Vega significa que solamente existe una condena vigente en relación a estos graves hechos. 

#### Las organizaciones adscritas a este comunicado reiteran la obligación del Estado de cumplir en su integralidad el fallo de la Corte IDH. Asimismo, el paso dado ayer por la Corte Suprema en este caso emblemático, no sólo supone una nueva afrenta hacia los familiares de los desaparecidos y la sociedad colombiana, sino que limita el impacto del acto de reconocimiento de responsabilidad, realizado hace apenas un mes por el Presidente de la República, durante el cual reconoció que las cicatrices provocadas por *“la falta de esclarecimiento judicial de los hechos, por el retardo injustificado en la administración de justicia… solo se irán borrando en la medida en que haya respuestas, verdad y compromiso por encontrar a los desaparecidos”*. El Estado colombiano debe cumplir con sus responsabilidades internacionales, esclarecer los hechos y sancionar a todos los responsables de las violaciones cometidas durante la retoma del Palacio de Justicia." 
