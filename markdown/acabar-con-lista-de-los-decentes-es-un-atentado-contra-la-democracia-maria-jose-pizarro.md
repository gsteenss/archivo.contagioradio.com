Title: Acabar con lista de los Decentes es un atentado contra la democracia: María José Pizarro
Date: 2018-02-02 14:23
Category: Política
Tags: Consejo Nacional Electoral, Gustavo Petro, Lista Decentes, Maria Jose PIzarro
Slug: acabar-con-lista-de-los-decentes-es-un-atentado-contra-la-democracia-maria-jose-pizarro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Maria-josé-pizarro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jóvenes por los Decentes ] 

###### [02 Feb 2018] 

La magistrada Yolima Carrillo, presidenta del Consejo Nacional Electoral, planteó la necesidad de revisar todas las listas inscritas en coalición, porque nunca se tramitó una ley estatutaria para reglamentarlas, lo que significaría que deben ser revocadas, **decisión que afectaría principalmente al a lista de los Decentes que reúne a diferentes coaliciones de centro e izquierda en el país liderada por Gustavo Petro.**

De acuerdo con María José Pizarro, candidata a la Cámara por Bogotá en esta lista, el anuncio del CNE, es un **“atentado contra la democracia del país”**, que no solamente podría ser inconstitucional, sino que viola el derecho a la participación política, el derecho a elegir y ser elegido, por lo tanto, para ella, el CNE estaría actuando en contra de los intereses de los ciudadanos.

La lista de los Decentes pudo inscribirse a las elecciones de Congreso de 2018 tras un fallo a una tutela que favoreció la inscripción de coaliciones. Para Pizarro, la intensión que habría en revocar la lista Decente estaría en beneficiar a los partidos tradicionales en el país **“esto sería un atentado a la democracia, no a la democracia de partido sino a la democracia ciudadana”.**

Frente a esta situación Pizarro afirmó que solicitarán apoyo de la OEA, debido a que se violaría uno de los principios fundacionales de esta organización en la que Colombia está inscrita, de igual forma expresó que las personas que están inscritas en la lista Decente son ciudadanos **“del común, de a pie, que nos hemos organizado para participar en las elecciones**, que hemos hecho grandísimos esfuerzos, tanto personales como colectivos, para acceder a los puestos de representación popular”.

Así mismo, la candidata por la lista de los Decentes afirmó que las personas que integran esta propuesta se están analizando el tema y han decidido no actuar deliberadamente hasta que no haya una respuesta por parte del Consejo Nacional Electoral. (Le pude interesar: ["CNE deberá respetar Constitución: Comité de revocatoria a Peñalosa"](https://archivo.contagioradio.com/la-revocatoria-a-penalosa-continua/))

<iframe id="audio_23522405" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23522405_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
