Title: ¿Habrá algo que celebrar del Acuerdo de París?
Date: 2015-12-18 08:26
Category: CENSAT, Opinion
Tags: Acuerdo de París, cambio climatico, COP 21
Slug: acuerdo-de-paris-en-la-cop-21
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/justicia-climatica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [foto: Amigos de la tierra] 

#### **[Censat Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### 18 Dic 2015 

Recientemente, 195 países firmaron el *Acuerdo de París* sobre el clima, anunciado como un pacto histórico, por lo que tiene celebrando a gobiernos, empresarios, corporaciones conservacionistas y otros grupos de poder. No obstante la publicidad triunfalista, sabemos que el Acuerdo no pasa de ser un engaño, pues se han antepuesto los intereses empresariales a la urgencia de dar soluciones reales a la crisis ambiental.

¿Por qué no celebramos? Los motivos de celebración expuestos al público no tienen ningún fundamento sólido. En primer lugar porque mediante la vaguedad del lenguaje y la retórica se diluyen los compromisos, por ejemplo no es cierto que se establezca el objetivo de 1,5 °C como límite máximo de incremento de temperatura admitida, solo se invita a *proseguir esfuerzos* para lograrlo, el texto no habla de *dejar de emitir* sino de alcanzar un *equilibrio* entre lo emitido y lo capturado. Además el Acuerdo carece de objetivos claros de reducción de emisiones para países industrializados y de compromisos vinculantes.  Así mismo el reconocimiento de las obligaciones respecto a los derechos humanos, el derecho a la salud, los derechos de pueblos indígenas fue exaltado en el preámbulo, para luego obviarse en el articulado.

De otro lado, el mismo Acuerdo observa con preocupación que las Contribuciones Previstas Determinadas a Nivel Nacional (INDC, sigla derivada de su nombre en inglés) son insuficientes para evitar el incremento de la temperatura media global. Por otra parte, dichas INDC no tienen restricción en la forma de alcanzar sus metas, así que las falsas soluciones estarán a la orden del día exacerbando la conflictividad ambiental. En este sentido, son ampliamente promovidos proyectos como REDD+, monocultivos extensivos, la captura y almacenamiento de carbono (CCS), la agricultura climáticamente inteligente y la bioenergía. Por si fuera poco, el Acuerdo no menciona ni una sola vez los combustibles fósiles, de modo que las sociedades petroadictas seguirán como si nada.

En cuanto a daños y pérdidas, el Acuerdo dice reconocer la importancia de afrontarlas, pero al tiempo aclara que “no implica ninguna forma de responsabilidad jurídica o indemnización”, es decir que nadie responde por las afectaciones que ya se están dando. Y sobre la provisión de financiación, los 100 mil  millones de dólares anuales para enfrentar mitigación y adaptación a nivel global, además de insuficientes, no incluyen claridad sobre costos, procedimientos y reparto entre los países.

El gobierno colombiano fue armónico con el Acuerdo engañoso de París al hacer grandes promesas de reducción de emisiones y ofrecer nuestra naturaleza para sus negocios verdes (como era de esperarse, la película *Magia Salvaje* fue su catálogo de productos). La representación oficial de Colombia también se destacó, vergonzosamente, por ser el único país de la Alianza Independiente de América Latina y el Caribe, AILAC, en oponerse a incluir los derechos humanos en el objetivo del Acuerdo.

El gobierno de Juan Manuel Santos propuso en la COP21 la iniciativa *Colombia Sostenible* que con apoyo del Banco Interamericano de Desarrollo busca recaudar fondos para reducir la deforestación y para el costo ambiental del postconflicto. Esta iniciativa así como el programa *Visión Amazonía* que busca reducir a cero la deforestación neta para el año 2020*,* se lograría mediante sumas y restas sospechosas, integrando las plantaciones extensivas en las cuentas de reducción de la deforestación, al tiempo que se tendrían en cuenta como sumideros de carbono. En este contexto, conviene destacar el papel que desempeñarían las llamadas Zonas de Interés de Desarrollo Rural, Económico y Social (Zidres), impulsadas desde el Plan Nacional de Desarrollo (PND) y cuestionadas precisamente porque son un mecanismo útil al acaparamiento de tierras y que promueve agresivamente el modelo monocultural de la agroindustria. Sumado a estos programas, el gobierno colombiano buscará cumplir sus metas de reducción de emisiones incrementando en 2,5 millones de hectáreas las áreas protegidas, delimitando los 36 complejos de páramos, apalancando las alianzas público privadas (APP) y la inversión extranjera directa, y dando un enfoque costo-efectivo a sus planes mediante la implementación de instrumentos de mercado.

Todas estas propuestas desde el gobierno nacional nos plantean grandes preocupaciones: si Colombia tiene sus mayores emisiones en el sector forestal (39%) y agropecuario (19%) ¿Por qué promover los monocultivos y la agroindustria para 'solucionarlo' cuando estos son conocidos por ser grandes generadores de degradación ambiental? ¿cómo lograr la reducción de la deforestación cuando se consolidan las Zidres que legitiman el acaparamiento y sustraen automáticamente las reservas forestales de Ley segunda?.

Todo lo anterior da cuenta de que no hay nada que celebrar del Acuerdo de París. Antes bien, será recordado por la incapacidad de la Convención para responder con honestidad y con efectividad a la crisis ambiental, al tiempo que muestra su eficacia para defender los intereses de las empresas.

En cambio, sí aplaudimos las multitudinarias movilizaciones que invocan el cambio sistémico, la revolución energética y la justicia climática. Los pueblos seguiremos levantando la voz en la calle ante la creciente conciencia de que el asunto *¡No es el clima, sino el sistema!*
