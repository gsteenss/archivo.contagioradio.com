Title: El paro agrario también llega a Bogotá
Date: 2016-05-29 19:13
Category: Paro Nacional
Tags: Bogotá, Minga, Movilizaciones Bogotá, Paro Agrario, Paro Nacional
Slug: el-paro-agrario-tambien-llega-a-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Paro-nacional-bogota-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  cablenoticias] 

###### [29 May 2016]

El Paro Agrario que inicia esta semana en todo el país también llega a Bogotá, la capital del país, donde se están impulsando varias reivindicaciones. Más de 200 mil campesinos, indígenas y afrodescendientes protagonizarán a partir de este 30 de Mayo en más de 100 puntos, ubicados en 27 departamentos del país.

La Cumbre Agraria convocó esta movilización dados los reiterados incumplimientos por parte del gobierno al pliego de [exigencias construido antes del Paro Nacional del 2014](https://archivo.contagioradio.com/el-pliego-de-peticiones-del-paro-nacional/). Después de esa fecha se instaló la Cumbre Agraria pero hasta el momento no se han tenido resultados de las conversaciones.

**Lunes 30 de Mayo:** se realizará una bicicletada informativa en la que se espera que los habitantes de la capital que semanalmente disfrutan de la ciclo vía, se enteren de las exigencias de los campesinos y campesinas alrededor del país y se solidaricen con esas luchas. La actividad informativa se realizará en la avenida Boyacá con avenida primero de Mayo a partir de las 8 am.

**31 de Mayo:**  Frente al Concejo de Bogotá se realizará un plantón y cacerolazo a partir de las 12 del medio día, convocado por organizaciones barriales, sindicales y ciudadanas que se oponen a la venta de la ETB y a varios de los puntos del Plan de Desarrollo Distrital que cursa en el Concejo de la ciudad.

**1 de Junio:** Habrá movilización desde la universidad pedagógica en la calle 72 con carrera 11 y se prevé una marcha hacia el centro de la ciudad a la que se sumarían estudiantes de la Universidad Distrital y de la Universidad Nacional.

**5 de Junio:** En el marco de las múltiples reivindicaciones de organizaciones ciudadanas, en el parque del Barrio La Aurora, en el sur de la ciudad se realizará un evento de cierre simbólico del basurero “Doña Juana” que es un foco fuerte de contaminación pero sobre el cual hay diversas propuestas para mejorar el tratamiento de los residuos, incluso los habitantes del sector afirman que se puede generar energía.

**9 de Junio:** se realizará una actividad de “rodada tortuga” en la que se pretende impulsar un proceso de revocatoria contra el Alcalde de la Capital.

Según los voceros de la Cumbre Agraria, durante toda la semana se estarán anunciando a través de las redes sociales otras movilizaciones. En Twitter se impulsarán \# PáreseDuro, \#CambiemosEsto, \#MingaNacional, \#ParoNacional y \#MingaLeDigo.

A través de contagio radio estaremos compartiendo información en la sección especial [Paro Agrario.](https://archivo.contagioradio.com/category/programas/paro-agrario/)
