Title: Unificar periodos electorales busca frenar avance de la izquierda
Date: 2018-10-23 18:51
Author: AdminContagio
Category: Nacional, Política
Tags: Alcaldes, Enrique Peñalosa, oposición, Proyecto de Acto Legislativo, Unificación de Periodos
Slug: unificar-periodos-electorales
Status: published

###### [Foto: @GustavoBolivar] 

###### [19 Oct 2018] 

Ante la aprobación en primer debate el Proyecto de Acto Legislativo con el que se buscaría unificar los periodos de alcaldes y gobernadores con el del presidente, diferentes sectores políticos criticaron la iniciativa porque **implicaría extender el periodo de los actuales administradores locales y regionales hasta 2022.**

**Daniel García-Peña, profesor de la Universidad Nacional** sostuvo que es desafortunado desde diferentes puntos de vista unificar los periodos de departamentos y municipios al proceso electoral, porque **ello significaría volver a los "viejos tiempos del centralismo"**, cuando lo local estaba subordinada a la lógica nacional. El catedrático recordó que la Constitución del 91 separó los periodos, logrando que las dinámicas electorales y políticas tuvieran 'juego' propio, independientes de la política de la capital.

Desde ese punto de vista, unificar los periodos sería negativo para la democracia participativa porque **"establecería una dependencia de los sistemas locales a los partidos y redes clientelares a nivel nacional"**; hecho que para García tiene nombre propio, pues partidos "que funcionan en torno al clientelismo" como La U, Cambio Radical, Conservador y Liberal se verían beneficiados. (Le puede interesar:["¿Qué implica la unificación de los periodos de alcaldes, congresistas y presidente?"](https://archivo.contagioradio.com/unificar-cargos-eleccion/))

Por otra parte, el académico resaltó que con la unificación de periodos, los planes de desarrollo locales estarían sujetos al plan de desarrollo nacional, es decir, "**en Bogotá se deciden los temas de presupuesto y planificación, y en los municipios se aplica lo que dice la capital"**. Situación que para García sería un retroceso para el sistema democrático colombiano, considerando que antes de la Constitución del 91, alcaldes y gobernadores eran elegidos desde la capital.

### **El miedo a la oposición** 

García indica que existe un elemento perverso en tratar de aplazar las elecciones locales, pues se estaría buscando evitar lo visto en los comicios que se llevaron a cabo este año. El académico puntualizó que con las elecciones presidenciales, para congresistas e incluso, la consulta anticorrupción, se observó un aumento de la capacidad electoral de los movimientos progresistas; **razón que tendrían los partidos tradicionales para "poner distancia" entre las próximas elecciones y el momento político que vive el país.**

No obstante, el docente expuso que congresistas como Rodrigo Lara, de Cambio Radical, y algunos voceros del Centro Democrático, se han manifestado contra la iniciativa; por lo tanto, **cabe la posibilidad de que la misma se hunda en alguno de los 7 debates que restan** en las comisiones primeras de cámara y senado, o en las plenarias de ambas corporaciones.

Al proceso le restaría además el control por parte de la Corte Constitucional, situación relevante, pues como lo manifestó García, **"la iniciativa es completamente inconstitucional,** porque la ciudadanía entregó un mandato a alcaldes y gobernadores por 4 años, y alargar dicho periodo sería inconstitucional". (Le puede interesar: ["Los retos a los que se enfrenta la bancada de oposición"](https://archivo.contagioradio.com/los-retos-a-los-que-se-enfrenta-la-bancada-de-oposicion/))

<iframe id="audio_29555894" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29555894_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
