Title: Denuncian incursión paramilitar en Comunidad campesina de Dabeiba, Antioquia
Date: 2018-12-07 17:34
Author: AdminContagio
Category: Comunidad, Nacional
Tags: Dabeiba, Incursión Paramilitar
Slug: denuncian-incursion-paramilitar-comunidad-campesina-dabeiba-antioquia
Status: published

###### [Foto: Contagio Radio] 

###### 07 Dic 2018 

Habitantes de la **Comunidad de Vida y Trabajo La Balsita, en Dabeiba, Antioquia**, denunciaron que el pasado 6 de diciembre hombres reconocidos como autoparamilitares ingresaron al territorio y pidieron a los habitantes los datos personales de los niños y jóvenes de la población, demanda que los pobladores rechazaron.

Cerca de las 11:30 am, a la comunidad llegaron seis hombres armados señalados de hacer parte de las autodenominadas **Autodefensas Gaitanistas de Colombia**, quienes llegaron al lugar movilizándose en motos, con la excusa de festejar el inicio de las fiestas navideñas y ofrecer regalos a los niños de La Balsita.

Los hombres exigieron la entrega de **un censo con datos de los jóvenes y niños de la comunidad,** solicitud negada por los pobladores quienes les pidieron que se marcharan del territorio. Los desconocidos permanecieron cerca de una hora más en el lugar y decidieron abandonarlo después de mediodía.

La presencia de dichos actores en el territorio prende las alarmas de la Comunidad de Vida y Trabajo La Balsita, que el pasado 18 de noviembre conmemoró 21 años de desplazamiento por cuenta de la Operación “Septiembre Negro” perpetrada por paramilitares y militares en Chocó y Antioquia en 1996. [(Le puede interesar: Somos esa luz, esa esperanza para lograr una patria justa: comunidad de Dabeiba)](https://archivo.contagioradio.com/somos-esa-luz-esa-esperanza-para-lograr-una-patria-justa-comunidad-de-dabeiba/)

###### Reciba toda la información de Contagio Radio en [[su correo]
