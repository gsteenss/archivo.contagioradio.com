Title: Asesinan a Holmes Niscue y Huber Hoyos, líderes en el pacífico
Date: 2018-08-21 15:50
Category: DDHH, Nacional
Tags: Asesinado, Awá, Cauca, Lider social, Tumaco
Slug: asesinan-lideres-pacifico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/holmes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Holmes Niscue] 

###### [21 Ago 2018] 

Dos nuevas muertes de líderes enlutan al país, por un lado el líder campesino **Huber Hoyos,** quien recibió cinco disparos que acabaron con su vida, en el Municipio de Higuerillos, Cauca; y por otr**o Holmes Alberto Niscue,** líder indígena del pueblo **Awa,** que fue asesinado el domingo en la noche por hombres armados en el corregimiento de la Guayacana, cerca a Tumaco, Nariño.

Hoyos era integrante del proceso campesino de Almaguer, en **Cauca** y según la información que se conoce hasta el momento, hombres armados lo abordaron en Higuerillos, atentando contra su vida y provocando heridas de bala a su esposa, quien fue trasladada a un hospital de Popayán. (Le puede interesar: ["Bajo constante amenaza viven líderes sociales en Cauca"](https://archivo.contagioradio.com/bajo-amenaza-viven-lideres-en-cauca/))

### **Holmes Niscue era líder del Resguardo indígena Awá de Gran Rosario ** 

El líder indígena fue abordado por hombres armados que se desplazaban en un "Sprint", quienes lo interceptaron buscando que subiera con ellos al vehículo, pero ante la negativa del líder dispararon contra su humanidad. Según River Pai, Consejero Mayor del Pueblo Awa Unipa, **ocurrió a escasos metros de la estación de policía del Corregimiento de la Guacamaya.**

Niscue era líder del Resguardo indígena Awá de Gran Rosario en Tumaco, y tenía medidas de seguridad por parte de la Unidad Nacional de Protección en razón de amenazas que había recibido en su contra, **protección que consistía en un chaleco antibalas y un celular,** elementos que para Pai, no garantizaban la integridad del líder. (Le puede interesar: ["66% de los pueblos indígenas está a punto de desaparecer: ONIC"](https://archivo.contagioradio.com/pueblos-indigenas-a-punto-de-desaparecer-onic/))

El Consejero del pueblo Awa afirmó que su comunidad ha sufrido por el olvido histórico al que han sido sometidos, y sus peticiones no han sido atendidas a pesar de las amenazas y atentados sufridos. Para el líder, es necesario establecer una protección integral tanto individual como colectiva que responda a las necesidades de la zona, porque hasta ahora, **la militarización de territorio ha demostrado no ser una solución.**

<iframe id="audio_27979647" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27979647_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
