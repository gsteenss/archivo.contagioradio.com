Title: Los costos de la guerra y la paz
Date: 2016-08-26 18:22
Category: Nacional, Paz
Tags: FARC, Gobierno, La Habana, paz
Slug: los-costos-de-la-guerra-la-carcel-y-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Colombia-celebra-acuerdo-FARC_LNCIMA20160623_0146_5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Nación 

###### [26 Ago 2016] 

\$24.000.000 es la cifra aproximada  que se entregará a cada uno de los integrantes de las FARC una vez dejen las armas y busquen reintegrarse a la vida civil. Una cifra aunque parece escandalosa no es más que una cifra muy similar que se le suele dar todos aquellos miembros de grupos armados ilegales que empiezan a hacer parte del programa de la Agencia Colombiana para la Reintegración, ACR. **Un monto apenas razonable para que una persona pueda restablecer su vida y encontrar opciones para no volver a armarse.**

La cifra se divide así. De acuerdo con el texto del acuerdo final, cada guerrillero desarmado recibirá una asignación única de normalización equivalente a 2 millones de pesos; Además cada integrante de las FARC recibirán una asignación mensual, equivalente al 90% del salario mínimo mensual legal vigente (SMMLV) únicamente por 24 meses, que representa \$620.509, siempre y cuando el guerrillero o guerrillera demuestre que no cuenta con un vínculo contractual que les genere ingresos. Ese primer total suma \$14.892.228.

Así mismo, el acuerdo establece que con el fin de apoyar a los integrantes de la guerrilla con sus proyectos productivos se les autorizará un monto único por \$8.000.000 por una única vez. **Para un total exacto de \$24.892.228, condicionado a que estén validando la primaria, bachillerato, o estén tomando un curso del Sena** para que puedan aterrizar esos conocimientos en un trabajo formal.

Para muchos, puede ser un total exagerado para unas personas que decidieron empuñar las armas. Sin embargo, haciendo comparaciones, tanto la guerra como mantenerlos en las cárceles, que en este momento están en total hacinamiento, saldría mucho más costoso, y poco coherente con lo que significa la construcción de paz.

Así lo ha demostró la delegación de paz del gobierno, en los últimos días. Frank Pearl, aseguró que “si uno compara cuánto se gasta en Colombia por año en una persona que está en la cárcel donde el porcentaje de reincidencia es del 75%, frente a las cifras de lo que costará un reinsertado, **éstas son 40 o 50% menores y el porcentaje de éxito de la reintegración es del 92%".**

Por otra parte, el costo de la guerra es mucho más alto. **“**¿Cuánto cuesta un Black Hawk?, como 28 millones de dólares, ¿es mejor para Colombia seguir con el gasto en defensa o es mejor pagarle 600.000 pesos a un guerrillero?”, dijo esta semana en rueda de prensa el senador Roy Barreras.

Haciendo cuentas básicas, los costos de la reintegración de cerca de **7500 integrantes de las FARC, como lo tiene pensado el gobierno, costaría un total de \$186.691.710.000,** y sumando los 280 mil millones del plebliscito, daría una cifra exacta de \$466.691.710.000.

Ahora, un solo bombardeo contra algún campamento de la guerrilla le ha llegado a costar al gobierno 25 mil millones de pesos, lo que quiere decir que si se hiciera al menos un bombardeo al mes, eso equivaldría a una suma de 300 mil millones de pesos, en tan solo un año, y únicamente hablando de bombardeos, **si la guerra continuara mínimo unos 20 años más, esa cifra ascendería a 6 billones de pesos.**

De manera tal, que aunque esos 24 millones de pesos para cada guerrillero o guerrillera parecen mucho, lo cierto es que **la cárcel o la guerra resulta mucho más costo, y con ello, se continúa aumentando la cifra de víctimas del conflicto armado.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
