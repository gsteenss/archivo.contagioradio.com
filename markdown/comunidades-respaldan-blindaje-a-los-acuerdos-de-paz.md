Title: Comunidades respaldan blindaje a los acuerdos de paz
Date: 2016-05-16 12:52
Category: Nacional, Paz
Tags: Conpaz, Conversaciones de paz de la habana, FARC, Juan Manuel Santos
Slug: comunidades-respaldan-blindaje-a-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/conpaz-cacarica-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: comunidadesconpaz.blogspot] 

###### [16 May 2016] 

Tras el anuncio del pasado viernes 13 de Mayo, en que la mesa de conversaciones de paz informa del mecanismo para que el acuerdo final de paz sea elevado a la calidad de tratado especial, que quedaría vinculado al bloque de constitucionalidad y que sería respaldado por la ONU **las comunidades han reaccionado de manera positiva**, puesto que ese blindaje sería una garantía del cumplimiento de los acuerdos en futuros gobiernos.

El cumplimiento de los acuerdos de paz es ha sido, desde el inicio de los diálogos, una de las preocupaciones de las comunidades a las que la guerra afecta directamente, pero **no solamente por las bombas y los miles de muertos que durante más de 60 años han estado presentes**, sino también por las [reivindicaciones en torno a la propiedad de la tierra](https://archivo.contagioradio.com/comunidades-conpaz-evaluan-positivamente-la-propuesta-de-las-terrepaz/), el paramilitarismo, la sustitución de los cultivos de uso ilícito, entre otras.

Para las comunidades de la red **[CONPAZ](https://archivo.contagioradio.com/?s=conpaz)**, Comunidades Construyendo Paz en los Territorios, este blindaje podría convertirse en garantía de que tanto **la comunidad nacional, como la internacional, a través de la ONU, sean garantes para los acuerdos** en todos sus elementos, no solamente por la seguridad de quienes han hecho parte de la guerra olas [víctimas directas](https://archivo.contagioradio.com/nuestros-desaparecidos-no-estan-enmarcadas-como-victimas-en-el-marco-del-conflicto-armado-conpaz/), sino de los demás acuerdos sobre Drogas, [Víctimas](https://archivo.contagioradio.com/paramilitares-amenazan-a-integrantes-de-conpaz/) y Desarrollo Agrario.

<iframe src="http://co.ivoox.com/es/player_ej_11523798_2_1.html?data=kpailJibfZmhhpywj5WXaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5ynca7V087OjarZq8biysaYr9TXtdbZ08aYj5Cnk6%2FEor%2Bah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
