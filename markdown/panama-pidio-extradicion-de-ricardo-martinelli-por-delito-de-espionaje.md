Title: Panamá pidió extradición de Ricardo Martinelli por delito de espionaje
Date: 2016-09-27 15:22
Category: El mundo, Judicial
Tags: Derechos Humanos, Espionaje ilegal, Panama, Ricardo Martinelli
Slug: panama-pidio-extradicion-de-ricardo-martinelli-por-delito-de-espionaje
Status: published

###### [Foto: laestrella] 

###### [27 Sep 2016]

La solicitud de extradición la realizó el Ministerio de Relaciones Exteriores este lunes basándose en las investigaciones que se realizan por el espionaje realizado por Martinelli a más de 150 personas y organizaciones de derechos humanos, así como a personajes de la oposición. El proceso de trámite de la **solicitud queda en manos del Departamento de Estado de Estados Unidos que a partir de ahora iniciará el estudio del caso.**

Las actividades de **espionaje por parte del gobierno de Martinelli se habrían realizado durante casi todo su periodo de gobierno**, es decir, desde 2009 hasta 2014 cuando fue remplazado por Juan Carlos Varela, quien también trabajó en su gobierno. Ya desde el pasado mes de Mayo la Corte Suprema de Panamá había solicitado el [inicio del trámite de extradición y en Diciembre de 2015 solicitó su detención.](https://archivo.contagioradio.com/orden-de-detencion-a-ricardo-martinelli/)

Martinelli ha afirmado que desde Diciembre de 2015 se encuentra radicado en Miami, puesto que ha señalado el proceso judicial en su contra como una venganza política por parte de Varela. Sin embargo **Ricardo Matinnelli también es señalado por delitos contra el erario público por malversaciones de fondos destinados a la compra de alimentos para niños y niñas en edad escolar por sumas multimillonarias**, además por el pago de sobornos a empresas de la construcción.

Desde Octubre de 2015 se estimaba que la pena que podría enfrentar el ex presidente podría ascender a 21 años. En su momento el fiscal encargado del caso, Harry Diaz, pidió penas de 4 años por interceptación de llamadas sin orden judicial, 4 años por seguimiento, vigilancia y persecución, 10 años por peculado por sustracción o malversación y otros 3 años por un agravante de peculado de uso.
