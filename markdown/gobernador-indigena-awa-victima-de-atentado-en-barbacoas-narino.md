Title: Gobernador indígena Awá, víctima de atentado en Barbacoas, Nariño
Date: 2019-01-02 12:00
Author: AdminContagio
Category: DDHH, Nacional
Tags: Barbacoas Nariño, Grupos armados ilegales, Líder Indígena, ONIC
Slug: gobernador-indigena-awa-victima-de-atentado-en-barbacoas-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/WhatsApp-Image-2017-10-09-at-4.04.25-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [02 Ene 2019] 

La Organización Nacional Indígena de Colombia denunció a través de un comunicado de prensa que el pasado 28 de diciembre de 2018**, Alejandro Pascal Pai, gobernador indígena del Resguarda Awá Pingullo Sardinero, fue víctima de un atentando dentro de su vivienda**, "a manos de una estructura armada ilegal". Tanto él como su esposa salieron gravemente heridos de los hechos.

De acuerdo con la ONIC, la situación fue presenciada por los 6 hijos del líder indígena, que lograron salir ilesos del atentado, sin embargo, una comisión de derechos humanos, que llegó hasta el territorio el 30 de diciembre, **constató la gravedad de lo sucedido y pudo acompañar la salida de la familia del lugar para garantizar su protección.**

Desde la Unidad Indígena del Pueblo Awá - UNIPA denunciaron **"la sistematicidad de los hechos victimízantes y el cambio de estrategia por parte de los diversos actores armados** que tienen intereses frente al control de su territorio ɨnkal Awá", en el departamento de Nariño, debido al aumento en asesinato y amenazas a líderes indígenas. (Le puede interesar:["Paramilitares ofrecen recompensas por asesinar líderes indígenas"](https://archivo.contagioradio.com/panfletos-firmados-por-aguilas-negras-anuncian-recompensas-a-quienes-asesinen-a-lideres-indigenas/))

Por su parte, la ONIC reiteró que en el año 2018, el departamento del Cauca fue uno de los más asediados por la violencia en contra de indígenas, **que se ha evidenciado en el mayor control de grupos armados ilegales en el territorio.** Razón por la cual hizo un llamado al gobierno Nacional, a la comunidad internacional y a las organizaciones defensoras de derechos humanos para que garanticen la seguridad de las comunidades indígenas.

###### [Reciba toda la información de Contagio Radio en [[su correo] 
