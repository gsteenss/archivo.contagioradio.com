Title: Licencias, concesiones y empresas deberían retirarse de Cumaral
Date: 2017-06-05 12:34
Category: Ambiente, Entrevistas
Tags: consulta popular, Cumaral, Mineria
Slug: licencias-concesiones-y-empresas-deberian-retirarse-de-cumaral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Cumaral-vota-e1496683218788.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Poder Ciudadano] 

###### [05 jun 2017] 

Tras la contundente victoria de la consulta popular en Cumaral Meta, **donde 7.475 personas votaron no a la exploración y explotación de hidrocarburos en el territorio,** los habitantes esperan que las licencias de hidrocarburos, que le fueron otorgadas a empresas como Monsarovar Energy Colombia Ltda y Pacific E&P, sean retiradas.

Para Carolina Orduz, miembro del Comité promotor impulsor de la Consulta Popular en Cumaral, **“luego de la decisión soberana y vinculante del pueblo, viene una lucha jurídica grande para de alguna manera, blindar la decisión de los cumaraleños”**. Con los resultados de la consulta popular, los habitantes de este municipio del Meta, buscan que se desarrollen proyectos de desarrollo sostenible para proteger los ecosistemas. Le puede interesar: ["Cumaral le dijo No a la exploración y explotación minera en su territorio"](https://archivo.contagioradio.com/cumaral-le-dijo-no-a-la-exploracion-y-explotacion-minera/)

### **Lo que se votó en la consulta popular en Cumaral** 

Cumaral es un municipio que está ubicado a 25 kilimetros de la capital del Meta, Villavicencio. En 2012, se celebró el contrato 009 entre la Agencia Nacional de Hidrocarburos y la empresa china Monsarovar Energy Colombia Ltda, **que comprende 22.650 hectáreas entre Cumaral y Medina en Cundinamarca.** Este proyecto implicaba un alto riesgo sísmico en la región, debido a que es una zona de recarga hídrica en el Pie de Monte Llanero.

En 2016 la empresa indochina Mansarovar Energy llegó al municipio para hacer actividades de exploración en el Bloque Llanos 69. Este bloque está conformado por 22.000 de las 69.000 hectáreas que conforman Cumaral. Según Orduz, **“el 73% del territorio ha estado concesionado a bloques petroleros** que amenazan fuentes hídricas que son vecinas del casco urbano y de donde la población se surte de agua”. Le puede interesar: ["¿Quién dijo miedo?"](https://archivo.contagioradio.com/quien-dijo-miedo/)

Orduz afirmó también que “un alto porcentaje de la victoria de la consulta fue la voluntad política del alcalde, él hizo el trámite atendiendo a la solicitud de los cumaraleños”. En 2012, con la llegada de las compañías petro mineras, la comunidad no tuvo el apoyo de las instituciones locales de turno para convocar a una consulta popular. Hoy, 5 años después, los habitantes lograron contar con el apoyo del alcalde y como lo afirma Orduz, **“le cumplimos a las generaciones futuras”.**

**Asociación Colombiana de Petróleos deslegitima resultados de consulta popular**

El presidente de la Asociación Colombiana de Petróleos, Francisco Lloreda, se refirió el día de hoy a los resultados de la Consulta Popular en Cumaral. En comunicación de medios de información, Lloreda afirmó que, **"los resultados de la consulta no son vinculantes".** Para esta Asociación, los resultados de las votaciones no afectarán el desarrollo de los proyectos extractivistas en el Meta que ya se están realizando. Según ellos, "la población fue engañada para votar no y los resultados solo aplican a proyectos futuros".

<iframe id="audio_19084180" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19084180_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
