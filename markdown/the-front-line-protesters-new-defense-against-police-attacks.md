Title: The Front Line: Protesters' new defense against police attacks
Date: 2019-12-05 12:15
Author: CtgAdm
Category: English
Tags: Front Line, National Strike, Police brutality
Slug: the-front-line-protesters-new-defense-against-police-attacks
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/WhatsApp-Image-2019-12-04-at-2.00.50-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Contagio Radio] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[Amidst weeks of violent repression from riot control police during street demonstrations, protesters have launched what they call the Front Line, a form of self-defense and peaceful resistance to protect citizens from acts of police brutality that have resulted in more than 300 people wounded and the death of Dilan Cruz.]

[The Front Line is modeled after a self-defense team that was created in Chile to respond to the violent actions of the national police force. The close to 70 people that make up the Front Line stand in strategic areas of the demonstrations to prevent bullets or tear gas, shot by police, from affecting the rest of the citizenry. One of the members of this initiative in Colombia said they reject any violence. ]

[“We are in charge of protecting the people, but we also look to inspire them to no run away every time the riot control police attack us,” said Nao, who preferred to remain anonymous.]

Some members of the Front Line use shields to protect the protesters. Others serve as "firefighters" to extinguish the flames and nurses to attend those wounded. "It's important that the people know in the demonstration of December 4 that the people can march safely, that they can feel at ease. We are not going to win with force because we are not the military or the police. We are students and we will win with intelligence," Nao said.

The student added that they seek to keep the marches peaceful because the strike will be more successful "if the marches continue with their objective, but if they are also safe and everyone can go back home after participating." Nao said that the First Line is not affiliated with any political party, but they support the objectives of the strike to pressure the government into repealing bad policies in terms of education, employment, health and human rights."

"We won't win against the riot control police with force. There is a media war that we also want to win, and we are going to do that by protecting the people so that there are no wounded, no arbitrary arrests," Nao said.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
