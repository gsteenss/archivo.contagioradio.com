Title: Empresa minera contaminó con arsénico la sangre de los pobladores de Cajamarca, Perú
Date: 2015-09-10 12:14
Category: El mundo, Movilización
Tags: arsénico, Banco Mundial, Cajamarca, Cía. de Minas Buenaventura de Perú, contaminación por arsénico, enfermedades causadas por el arsénico, enfermedades causadas por minería, IFC, International Finance Corporation, minera Yanacocha, Newmont Mining Corporation de EEUU, Perú
Slug: empresa-minera-contamino-con-arsenico-la-sangre-de-los-pobladores-de-cajamarca-peru
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/s1190040.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [celendinlibre.wordpress.com]

**La sangre de los pobladores de Cajamarca, en Perú, está contaminada de arsénico  **por las actividades de la minera Yanacocha, según concluyó un estudio de la Universidad Nacional de Cajamarca.

De acuerdo con la investigación, los habitantes de esa ciudad, se contaminaron por consumir productos que fueron expuestos al agua proveniente de la Planta El Milagro que utiliza la minera Yanacocha, la mina de oro más grande de Sudamérica, que desarrolla sus actividades en cuatro cuencas:  **la Quebrada Honda, Río Chonta, Río Porcón y Río Rejo.**

Con muestras de uñas y cabellos de los pobladores de  Cajamarca y de las comunidades de La Ramada y Tual,  los investigadores encontraron que los habitantes bebieron agua contaminada, lo puede ocasionar  diversas **enfermedades por el arsénico en la sangre, que pasarían de generación en generación.**

**Irritación de estómago e intestino, disminución de de glóbulos rojos y blancos, irritación de los pulmones, lesiones en la piel, diabetes, posibilidades de cáncer de Piel, Pulmón, Riñones e Hígado,** podría ser el resultado de esta contaminación si las cantidades de arsénico en la sangre no son muy altas.

Mientras que si  se comprueba que los habitantes de esa región de Perú, tienen altos niveles de arsénico en su cuerpo, los niños, niñas y adultos que hacen parte de esta población podrían sufrir de **infertilidad, aborto en las mujeres, daños en el cerebro y problemas cardiacos. **

Además, por cuenta de las actividades mineras, los habitantes de Cajamarca han denunciado que se encuentran sin agua potable, lo que ha dificultado sus labores agrícolas ya que el **caudal del río principal pasó de 138 a 38 litros por segundo.**

Los accionistas de la empresa minera son  Newmont Mining Corporation de EEUU, Cía. de Minas Buenaventura de Perú y International Finance Corporation, ** institución miembro del Grupo del Banco Mundial.**
