Title: Águilas Negras amenazan de muerte a Héctor Carabalí líder social en Buenos Aires Cauca
Date: 2019-01-19 10:10
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Aguilas Negras, Buenos Aires, Cauca, Héctor Maríno Carabalí, lideres sociales
Slug: amenazan-de-muerte-a-lider-social-en-buenos-aires-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/foto-1-nota-3r_r0-e1547909040390-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Diario del Cauca] 

###### [19 Ene 2019] 

La Corporación Claretiana Norman Pérez denunció que durante el mes de enero, al correo del líder social Héctor Marino Carabalí, llegaron dos amenazas firmadas por el grupo paramilitar Águilas Negras, Bloque Capital en donde le advierten que **si él y los demás líderes del territorio siguen haciendo denuncias públicas, los van a asesinar.**

Ambos correos fueron enviados desde la dirección electrónica vivasm056mail.com, a nombre del remitente Manuel Vivás. El primero de ellos llegó el pasado 8 de enero, en el que además de amenazar a Carabalí, **intimidan a los líderes sociales Andrés Posu, Edis Caloto y Deyanira Peña.  ** (Le puede interesar: ["CIDH presenta recomendaciones para proteger líderes sociales"](https://archivo.contagioradio.com/cidh-presenta-recomendaciones-proteger-los-lideres-sociales/))

De acuerdo con la Corporación, el texto del correo señala lo siguiente, “durante mucho tiempo hemos enviado notificaciones para que estas personas se abstengan de seguir haciendo denuncias públicas en sus territorios no han hecho caso por tal motivo haremos cumplir todo, **o se van o los matamos.** La orden es matarlos y pagaremos por cada uno de ellos. Así tengan esquemas de protección, estén re ubicados debajo de las piedras los vamos a encontrar”.

El siguiente correo llegó el 9 de enero y allí le dicen a los líderes que* *"estamos cansados de que los líderes sociales y defensores de derechos humanos convoquen a marchas, denuncien y jodan tanto, **seguiremos exterminando a todo aquel que hace caso omiso a nuestros llamados de atención**".

### **Los líderes sociales en el Cauca bajo la mira de los grupos armados** 

Héctor Marino Carabalí ha sido uno de los líderes sociales más destacados del departamento del Cauca, llevando sus denuncias hasta la Comisión Interamericana de Derechos humanos en **donde relató la violencia de la que han sido víctimas los defensores de derechos humanos. **

El pasado 21 de julio del 2018, Carabalí ya había informado al gobierno y las autoridades nacionales, las amenazas que había recibido por parte del grupo las Águilas Negras, razón por la cual tuvo que abandonar el municipio de Buenos Aires.

Sumado a estos hechos, se encuentra el panfleto que salió a finales del año pasado en el que desde el mismo grupo paramilitar se ofrecía una recompensa por el asesinato a líderes indígenas del mismo departamento.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]{.s1}
