Title: Académicos del mundo piden a Duque políticas serias de DDHH
Date: 2019-05-26 13:13
Author: CtgAdm
Category: DDHH, Política
Slug: academicos-mundiales-critican-a-duque-por-ola-de-violencia-contra-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/acuerdo-nacional-duque-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

A través de una carta abierta, **más de 250 académicos del mundo criticaron fuertemente las políticas del gobierno del Presidente Iván Duque**, que, según ellos, han fallado en detener una escalada en agresiones en contra de defensores de derechos humanos y reincorporados. En lo que va corrido de 2019, **han sido asesinados al menos 62 líderes sociales**, de acuerdo a un reporte del Centro de Investigación y Educación Popular.

"Frente a estos hechos, **nos declaramos indignados por la falta de reconocimiento de esta situación por parte del gobierno y reclamamos acciones de fondo que eviten este continuo y sistemático derramamiento de sangre**, así como nuevos sucesos lamentables como el atentado ocurrido el 4 de mayo contra Francia Márquez y otros líderes y lideresas del norte del Cauca", dice la carta.

Según el documento, esta ola de violencia se debe a las disputas por varios sectores de territorios en que se busca establecer proyectos extractivistas de gran escala. Los académicos sostuvieron que el interés económico en estas regiones genera la escalada de conflictos socio-ambientales, en el cual grupos armados legal e ilegal trabajan juntos para despojar las comunidades de su territorios. (Le puede interesar: "[Según CINEP en 2018 fueron victimizados 98 líderes cívicos y comunales](https://archivo.contagioradio.com/en-2018-fueron-victimizados-98-lideres-civicos-y-comunales-cinep/)")

Finalmente, la carta invitó al Presidente a responder a esta denuncia con las medidas que el gobierno ha tomado para frenar el recrudecimiento del conflicto. Además, concluyó la necesidad de entidades internacionales como Human Rights Watch y Amnistía Internacional organizar comisiones de verificación  para generar información veraz que permita prevenir nuevos casos y hacer justicia para los hechos ya ocurridos.

Bogotá, D. C., Colombia,

21 mayo de 2019

Señor

Iván Duque Márquez

Presidente de la República de Colombia

Carta Abierta

Académicos de Colombia y del mundo, miramos con gran preocupación los acontecimientos relacionados con las amenazas, persecuciones judiciales, y asesinatos de líderes y lideresas sociales, de excombatientes, así como defensores y defensoras de derechos humanos y del medio ambiente en Colombia. De acuerdo con el Centro de Investigación y Educación Popular, CINEP/Programa por la paz, en 2018 dentro de la categoría de violencia política, se perpetraron 648 asesinatos, 1151 casos de amenaza de muerte, 304 lesionados, 48 atentados, 22 desapariciones forzadas, tres agresiones sexuales y 243 detenciones arbitrarias. En lo que va corrido de 2019, han sido asesinados al menos 62 líderes sociales.

Frente a estos hechos, nos declaramos indignados por la falta de reconocimiento de esta situación por parte del gobierno y reclamamos acciones de fondo que eviten este continuo y sistemático derramamiento de sangre, así como nuevos sucesos lamentables como el atentado ocurrido el 4 de mayo contra Francia Márquez y otros líderes y lideresas del norte del Cauca.

Observamos que, desde los lugares de poder gubernamental y los medios de comunicación, se incita a una escalada de odio y violencia que rompe la poca paz alcanzada, pero aún más, como señala Daniel Pécaut (2001), se declara una guerra contra la sociedad.

A lo largo de nuestros trabajos académicos sobre las dinámicas locales, regionales e internacionales, advertimos que los territorios de interés geopolítico se convierten en foco que propicia la escalada de conflictos por expansión extractiva. Hemos planteado que existen vínculos perversos entre fuerzas legales e ilegales para expulsar a las poblaciones de sus territorios, evidenciando las mismas relaciones expuestas por académicos como Sassen (2015), Harvey (2004), Escobar (2014), el analista de defensa Herold (2007), entre otros, sobre la expulsión, el vaciamiento y la desposesión de territorios, para el avance de la acumulación a gran escala de los proyectos extractivos.

En el caso de Colombia se advierte esta situación en relación con la política económica que promueve las medidas extractivas como eje de desarrollo, lo cual incentiva y facilita que sectores de poder que representan intereses diversos copen los territorios, instalándose una escalada de asesinatos contra líderes y lideresas quienes defienden los derechos de los pueblos y las comunidades locales. Es una realidad de tiempo atrás, pero que se presenta Página 1 de 53 Carta Abierta al Presidente Iván Duque Márquez de Académicos del mundo y de Colombia con mayor impacto a partir de la firma de los acuerdos con las FARC - EP desde 2016, en contraposición con los propósitos de la paz territorial esperada.

Lo anterior permite inferir que la causa de amenazas y asesinatos se relaciona con intereses de varios sectores en los territorios en que se proyectan actividades extractivas a gran escala, lo cual coincide con las versiones en procesos de justicia transicional, esclarecimiento de la verdad, y en Sentencias de la Corte Constitucional, como la del Alto Andágueda, que llevan a deducir la relación de vulneración de derechos humanos con estas actividades.

Frases recurrentes en los mensajes amenazantes como: “acabar con cualquiera que se interponga al desarrollo en el país”, ubica a los pobladores como objetivo militar, porque son muchos de ellos representados por sus líderes y lideresas quienes se oponen a los proyectos extractivos, buscando evitar impactos negativos en ecosistemas y poblaciones.

Por lo mismo, nos llama la atención el abandono tanto de las instituciones del Estado como de la gran prensa, frente a este fenómeno de amenazas, persecución judicial y asesinatos, pero al mismo tiempo, no dudan en señalar y estigmatizar la protesta social, las actividades de liderazgo social y la oposición a las políticas de gobierno.

Resulta preocupante que sólo hasta cuando se atenta contra Francia Márquez, una lideresa reconocida internacionalmente y premio Goldman, quien se encontraba con un destacado grupo de líderes, hecho que trascendió internacionalmente, usted se pronuncie como gobernante. Su gobierno no nos permite entender cuáles son los esfuerzos para responder a esta grave crisis; observamos que las medidas hasta ahora tomadas son insuficientes y se limitan a esquemas de seguridad precarios y a una búsqueda, las más de las veces infructuosa de los autores materiales, sin indagar el fenómeno en su integralidad, para reconocer los autores intelectuales y los sectores que están detrás de esta estrategia de despojo y exterminio.

Como académicos le pedimos que, en su calidad de jefe de gobierno ordene abrir una investigación profunda, transparente y de cara al mundo, que muestre lo que verdaderamente ocurre frente a estas circunstancias que nos enlutan a diario.

Al mismo tiempo, dada la falta de acción de su gobierno y la magnitud del problema, consideramos necesario y urgente que entidades internacionales como Human Rights Watch y Amnistía Internacional, organicen comisiones de verificación e investigaciones de fondo sobre el particular, para que podamos tener información veraz que permita prevenir nuevos casos y hacer justicia para los hechos ya ocurridos.

De su parte esperamos que reconozca que estamos en un momento histórico en el que es posible cambiar el rumbo de la economía nacional y de la política social, por una opción de protección de la vida y del medio ambiente con réditos traducidos en buen vivir para las generaciones venideras. Página 2 de 53 Carta Abierta al Presidente Iván Duque Márquez de Académicos del mundo y de Colombia.

Lo invitamos a que responda abiertamente a esta carta y nos cuente cuáles han sido los avances y decisiones para evitar este derramamiento de sangre y cuáles las nuevas medidas para esclarecer esta dolorosa e inaudita situación humanitaria. A la espera de su respuesta, los abajo firmantes le seguiremos apostando a la paz mundial, a la paz territorial integral, una paz anhelada en cualquier lugar del mundo y en esta Colombia sufrida.

Con copia a: Su Santidad Papa Francisco. Human Rights Watch y Amnistía Internacional.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
