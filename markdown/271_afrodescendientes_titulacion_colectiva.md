Title: Colombia está en deuda con titulación de 271 territorios colectivos
Date: 2017-12-11 17:16
Category: Ambiente, Nacional
Tags: baldios, Comunidades afrodescendientes, titulación colectiva
Slug: 271_afrodescendientes_titulacion_colectiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/editorial-1-e1513024248790.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: etnoterritorios] 

###### [11 Dic 2017] 

Un análisis desarrollado por el **Observatorio de Territorios Étnicos y Campesinos (OTEC) de la Universidad Javeriana** identificó 271 territorios de comunidades afrodescendientes sin reconocimiento legal o titulación colectiva que están amenazados por actividades agroindustriales y extractivas, pese a que los consejos comunitarios  dan cuenta de la ocupación ancestral en territorios de 18 departamentos y 103 municipios.

Se trata de un informe que realizó el observatorio en articulación con la coalición La Iniciativa para los Derechos y Recursos (RRI), El Proceso de Comunidades Negras (PCN), y el Consejo Nacional de Paz Afrodescendiente (CONPA).

Allí se muestra que muchas de estas solicitudes de titulación colectiva llevan más de una década sin ninguna respuesta: **de los 271 casos revisados, el 29% no cuenta con ningún tipo de información frente al estado de la solicitud y el 39% siguen detenidos por falta de documentos y solo el 13% ha tenido auto de aceptación, auto de notificación o visita técnica.**

Dichas cifras dan cuenta de una recopilación de reclamos de consejos comunitarios registrados desde hace 20 años, un insumo crítico para las comunidades afrodescendientes ya que la agencia nacional de tierras o el INCODER informan que no tienen estos datos en sus registros.* *

**Asimismo, lo datos arrojaron que de 148 consejos comunitarios en titulación colectiva, 25% de ellos tienen afectaciones por proyectos agroindustriales en sus territorios; el 23% por hidrocarburos, el 6% por ductos y el 7% por proyectos de infraestructura. "Un panorama** que evidencian las situaciones de vulnerabilidad que amenazan considerablemente su condición de etnia", dice el Observatorio.

### **La titulación como una forma de protección del ambiente** 

De acuerdo con Johana Herrera, directora de Observatorio de Territorios Étnicos y Campesinos, resolver las titulaciones colectivas implica una oportunidad para el país, entendiendo que el papel de las comunidades negras e indígenas ha sido fundamental en la conservación ambiental, incluso afirma que "sin las comunidades negras e indígenas no tendríamos las selvas o los bosques húmedos que tenemos".

En ese sentido, el informe señala que la propiedad colectiva es una oportunidad para garantizar la continuidad de esta sostenibilidad. Para Herrera, “el reconocimiento de los territorios colectivos de las comunidades afrodescendientes es fundamental para la conservación de ecosistemas vitales, para la supervivencia y el bienestar de estos pueblos, y en el contexto actual, es esencial para lograr una paz estable y duradera.”

### [**La propuesta del OTEC**] 

La directora del Observatorio manifiesta que uno de los principales problemas para que en 18 departamentos haya solicitudes de titulación que no han sido atendidas, es por aspectos técnicos que desembocan en la falta de documentación por parte de Estado. Por ejemplo se encontraros solicitudes presentadas hace años que no avanzan porque falta la cédula del representante lega, y esa información no ha sido devuelta a las comunidades.

Ante dicha problemática, el **OTEC creó un sistema información con material cartográfico que cruza datos elaborados a partir del trabajo de campo con comunidades y bases de datos estatales**.

El acceso al sistema de información estará habilitado en una plataforma digital que será de gran utilidad, a nivel nacional puede despertar el interés de sectores políticos y de la sociedad civil, que ven en la titulación colectiva la forma de salvaguardar la identidad étnica.

Esos datos también podrán usarse en el plano regional como insumo para visibilizar las problemáticas a las que las comunidades negras están expuestas, para que a nivel local pueden permitir el desarrollo e implementación de políticas públicas y programas sociales que reduzcan la vulnerabilidad de estas comunidades, como lo explica Cristian Guerrero, investigador del OTEC.

Una información que será fundamental para exigir los derechos de las comunidades históricamente desconocidas y que además aportará al trabajo de las diferentes instituciones del gobierno, que deben responder por los procesos de titulación colectiva.

<iframe id="audio_22587186" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22587186_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
