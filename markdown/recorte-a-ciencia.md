Title: Colombia le cierra las puertas a la investigación y la ciencia
Date: 2017-08-23 13:46
Category: Educación, Nacional
Tags: Ciencia y Tecnología, Recorte a ciencia y teconología, recorte presupuestal
Slug: recorte-a-ciencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/investigación-e1503513943790.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Protocolo Foreign Affairs & Lifestyle] 

###### [23 Ago 2017] 

Con relación al recorte presupuestal para el 2018 para la ciencia y la tecnología, miembros de la comunidad científica en Colombia manifestaron su rechazo a lo que denominan una incoherencia y un golpe duro al desarrollo del país. Recordaron que **el recorte será de más del 40%** teniendo en cuenta que en la actualidad Colombia destina en ciencia únicamente 1,54 dólares por año por habitante.

En rueda de prensa, académicos y científicos indicaron que en Colombia **hay un problema cultural y de responsabilidad social** que se refleja en los pocos recursos que se invierten para la ciencia. Para Enrique Forero, presidente de la Academia Colombiana de Ciencias Exactas, Físicas y Naturales, “el presupuesto en defensa continúa en aumento mientras que el de educación y ciencia sigue disminuyendo”. (Le puede interesar: ["Ahora sí, recursos de ciencia e investigación serán destinados a vía terciarias"](https://archivo.contagioradio.com/recursos-de-ciencia-e-investigacion-seran-destinados-a-vias-terciarias/))

De igual manera, Eduardo Posada, presidente de la Asociación Colombiana para el Avance de la Ciencia, indicó que **“la inversión en ciencia representa una salida para la crisis económica e impulsa al país hacia el desarrollo”**. Dijo también que en Colombia existe la capacidad para desarrollar avances científicos y tecnológicos, pero no hay garantías ni condiciones para las personas que se están formando en estos campos.

**Inversión en ciencia garantiza el éxito de la implementación de los acuerdos de paz**

Los académicos indicaron además que para garantizar el éxito de la implementación de los acuerdos de paz entre el Gobierno Nacional y las FARC **se debe hacer un trabajo más fuerte en ciencia**. Posada manifestó que “es necesario invertir en investigación para que por ejemplo se pueda fortalecer el sector agrícola”. (Le puede interesar: ["Recursos de ciencia y tecnología no se están invirtiendo en investigación"](https://archivo.contagioradio.com/fondo-de-ciencia-y-tecnologia/))

Igualmente, Lucy Gabriela Delgado, directora del programa de Farmacia de la Universidad Nacional, manifestó que “es un error recortar el presupuesto en esta etapa porque **seguimos con la mentalidad guaquera, explotando petróleo sin usar el conocimiento derivado de las ciencias**, es matar la posibilidad de tener otro modelo de  desarrollo económico  para el país”.

**Colombia tiene capacidades para salir del rezago en ciencia y tecnología**

Lucy Gabriela Delgado, manifestó que Colombia **está rezagada en desarrollo científico**, “mientras no haya una financiación sostenida y políticas serias a largo plazo, no va a haber un desarrollo que revele frutos que puedan ser significativos para Colombia”. Sin embargo, ella recalcó que en Colombia ha habido grandes aportes desde la ciencia “que evidencian que la ciencia y la tecnología es la única salida del subdesarrollo”. (Le puede interesar: ["O vías o ciencia, pero no las dos: Gobierno Nacional"](https://archivo.contagioradio.com/o-vias-o-ciencia-pero-no-las-dos-gobierno-nacional/))

Finalmente, ellos y ellas invitaron al **plantón que realizarán mañana a las 10:00 am en la Plaza de bolívar en Bogotá** para manifestar el rechazo al recorte presupuestal para la ciencia. Indicaron que le harán llegar al presidente Juan Manuel Santos una carta en la que dirán “Ciencia sin recorte" y estará respaldada por  más de 17 mil firmas recogidas por medio de una petición realizada en la plataforma Change.org.

<iframe id="audio_20506046" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20506046_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
