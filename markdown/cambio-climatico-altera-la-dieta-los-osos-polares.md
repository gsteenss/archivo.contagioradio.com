Title: Cambio climático altera la dieta de los osos polares
Date: 2017-05-16 20:00
Category: Animales, Voces de la Tierra
Tags: cambio climatico, osos polares
Slug: cambio-climatico-altera-la-dieta-los-osos-polares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/osos_polares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: upsocl 

###### [16 May 2017]

La fractura de los ecosistemas ya se vive por cuenta del cambio climático. **Los osos polares y las especies de aves del Ártico son los animales que desde ya están viviendo estos efectos causados por el deshielo** de los polos como lo concluye un estudio realizado por el Instituto Polar de Noruega.

Según ese estudio, los **osos polares cambiaron de dieta y pasaron de alimentarse de focas, a comer huevos de ganso abandonados.** Esta situación se da debido a que ahora les es prácticamente imposible cazar focas en las costas pues ya no hay hielo lo suficientemente sólido que les facilite atrapar a estos animales.

El estudio publicado en la revista 'Journal of Animal Ecology' analizó el comportamiento de 67 osos y 6 focas, concluye además que, si bien esta situación pone en mayor riesgo la vida de los osos polares, que se encuentran **en grave amenaza de extinción, pues no es comparable la ingesta de una foca a un huevos de ganso.**

A esta lista de animales en peligro por cuenta del aumento de las temperaturas podrían sumarse las especies de aves de estas zonas. El Instituto Polar de Noruega, asegura que el ciclo de este ecosistema se ve alterado, pues las focas ahora llevan la ventaja, mientras se ha evidenciado una **disminución del 90% de la población de pájaros.** [(Le puede interesar: Nuevas imágenes muestran impactos del cambio climático)](https://archivo.contagioradio.com/nuevas-imagenes-evidencian-los-terribles-efectos-del-cambio-climatico/)

###### Reciba toda la información de Contagio Radio en [[su correo]
