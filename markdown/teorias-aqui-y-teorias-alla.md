Title: Teorías aquí y teorías allá
Date: 2020-04-27 14:59
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: guerra, lideres sociales, medios de comunicación, medios de comunicación alternativos, Teorías, utopías, verdad
Slug: teorias-aqui-y-teorias-alla
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/unnamed.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":5} -->

##### No estamos en una guerra… pero la  
cosa se parece. Y sería preciso recordar hoy, que cuando una guerra comienza la  
primera víctima es la verdad.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En estos momentos estamos como  
sociedad global apuntando a todas partes y a nada a la vez. El desconcierto de  
una situación que nunca se había vivido bajo estas condiciones, despierta el  
acostumbrado efecto neoliberal de buscar un “único culpable”. Los norteamericanos  
señalan a los chinos, y todos en occidente solo replican lo que dice “papá”;  
pero los chinos culpan al ejército norteamericano y me imagino que todos allá  
repetirán lo que dice el partido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otros dicen que es un sistema de “depuración” de las capas más pobres de las sociedades, otros dicen que fue para alivianar la carga del sistema pensional europeo, los ecocidas dicen que la culpa es del “humano” y que la naturaleza se está vengando, otros dicen que es un castigo de Dios y que por ende es mejor que vayan buscando el pelo que está incrustado en la mitad de la biblia; otros dicen que es un plan extraterrestre y me imagino que algunos insistirán hasta el último día de sus vidas, que es obra de Maduro y los cubanos… en fin.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿Sabe algo? No tenemos ni puta idea.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Al estar viviendo este momento  
histórico tan complejo, la necesidad de buscar menos teorías de la  
conspiración, y más acercamientos a presupuestos reales, a hechos y formas que  
bajo su aparición en la historia durante otros periodos nos permitan no  
adivinar, sino pronosticar con criterio, es más que imperativa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los estudios sobre la cultura  
política derivada de sociedades liberales defensoras de la libertad individual  
a ultranza permiten saber y no adivinar, por qué a los países occidentales les  
ha costado tanta dificultad asumir los efectos de la pandemia. Asimismo, los  
estudios sobre el gobierno y la cultura política en sociedades con modelos como  
el chino permiten deducir y no adivinar, por qué les va mejor con el tema de la  
disciplina en momentos de crisis.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estudios sobre la historia  
política y cultural de las naciones, permiten ver cómo Alemania, Rusia, Japón o  
Corea del Sur a pesar de ser democracias, no tienen problemas con el aumento  
del grado de autoritarismo en tiempos de crisis, son sociedades que han tenido  
en su orden, a Hitler, a los Zares, al Partido comunista, al Kaiser, al  
emperador etc. por lo que una dosis de disciplina no los escandaliza como sí a  
un occidental promedio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Ahora bien, respecto a Colombia la cosa es aún más complicada.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No somos una sociedad que conozca su historia, pero aparte de todo, si aquí la vicepresidenta de la república tiene a su empresa familiar relacionada con narcotraficantes, si el presidente de la república tuvo nexos con el contrabandista y narcotraficante ñeñe Hernández, o si descubrimos el fraude estadístico que maneja la Registraduría Nacional del Estado Civil, no pasa literalmente nada. ¿Por qué? ¿Porque Caracol TV, RCN, Semana y toda la horda de escuderos de este gobierno en apariencia mafioso, nos engañan sin piedad? ¿a la pobrecita sociedad colombiana?  ¡¡falso!!

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La sociedad colombiana ama ser  
engañada, el engaño es cómodo y las vanguardias revolucionarias que debieron  
surgir por allá en noviembre de 2019, las compusieron, bolitas de odio individualistas  
o intelectuales que convencieron a millones de que un Estado penetrado por  
mafiosos podía cambiarse por obra y gracia de besos y abrazos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los que gritaron convencidos  
durante noviembre de 2019 “no queremos líderes” ahora hoy pagan y nos hacen  
pagar las consecuencias de las políticas de un gobierno (con líder eterno) que  
defenderá inmobiliarias y banqueros por encima del que sea.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Necesitamos líderes.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ya basta de la afirmación odiosa y pedante de nuestro ser, en medio de un país que se desmorona, de nada sirve “creernos los más sinceros y que no nos callamos nada”, si no tenemos en cuenta al otro, si no tenemos perspectiva para nuestra afirmación. No quiero adivinar lo que viene después. No juguemos al horóscopo, juguemos a que somos colombianos que conocen su historia y que no plantean distopías sino realidades que se nos vendrán encima:

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El control del poder político aumentará despóticamente porque Duque, aún debe pagar favores electorales propios del sistema clientelar que habita en la institucionalidad colombiana y que nos ha negado la posibilidad de ser tácitamente una república.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El uribismo ha gobernado con el apoyo de los banqueros, así que la prioridad serán los bancos y no la gente. Lo lograrán, con la ventaja de que, gobernando a punta de decretos, la democracia se reduce al mínimo, y con la cuarentena la posibilidad de que las masas alteren agendas de gobierno se ve seriamente afectada. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Carrasquilla nos meterá cuentos propios del lenguaje ideológico neoliberal y de sus guerras psicológicas como el de “apetito de riesgo” para responsabilizarnos de la falta de liquidez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La responsabilidad de una tragedia causada por una clase dirigente en particular y sus políticas mezquinas e interesadas será otorgada a la gente, y esa intención será grabada en el imaginario colectivo desde líneas editoriales con un sentido lingüístico definido por los medios corporativos de información. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para eso, Semana, a cargo de la ex ministra de Uribe, será el nuevo frente ideológico de la batalla, sumado a El tiempo, Caracol TV, RCN, y de ahí para abajo todo ese poco de vulgaridades dirigidas por Vélez, Morales, Gómez, Dávila etc. etc. etc. no harán sino contener la posibilidad de una revolución, toda vez que protegen al grupo de poder, pero calman nuestras ganas de bajarlos con entretenimiento, buenas notas y noticias de mierda. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### No existe la solidaridad, ni la amistad real entre los amos y los esclavos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No importa cuánto lo intentes con discursos optimistas, o con la ayuda del orientalismo reciclado: Sarmiento Angulo no será tu amigo. No se puede.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo ojalá la izquierda blandita como un pan de yuca, deje de ser contemplativa y paciente con uribistas que se burlan hasta de sus enemigos con cáncer o que se ríen de los presos muriendo, que le echan la culpa al pobre de su pobreza, que desautorizan a la academia, y ojalá entiendan de una buena vez que esa gente no va a cambiar, y si no va a cambiar, quiere decir que la razón no es un elemento para tratarles, sino que se deberá buscar una emoción… pero una adecuada, no les demos el papayaso de que nos digan “creadores de odio”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Las utopías existen, pero no son esas que creamos para tranquilizarnos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Son las que nos hacen avanzar en la historia, conscientes de que, si estamos preocupados por lo que viene “después de todo esto”, sería mejor pensar, qué vamos a hacer con los verdaderos responsables no del virus, sino de las malas políticas que se asumieron y profundizaron la crisis de los pueblos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al carajo el presentimiento de  
que vendrá un mundo de paz y amor, en el que naturaleza y ser humano, serán  
dignos de cuidado. ¡bah! Mientras sigan en el poder los mismos de siempre,  
defiendo su modelo político, su modelo económico, su forma de ver las cosas, su  
manera de comunicar la realidad, su visión sobre la cultura, su visión sobre la  
educación, convertirán su mundo, entre chiste, cagada y chanza en “la forma  
natural de las cosas”, entonces, si no hacemos nada, “lo que viene después de  
esto” será más de SU mundo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿Qué nos queda si estamos en  
cuarentena y no podemos ni siquiera salir a marchar? el lenguaje… y el lenguaje  
es un arma.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Correr la voz de que ya nos hemos dado cuenta… comunicarnos tras las bambalinas de su teatro, afilar los dientes, construir conciencia política rechazando la individualización de un problema de gobierno, hacer redes de apoyo usando las tecnologías, fomentar cooperación económica de base, generar noticieros alternativos, propagar el inconformismo, cuestionar la solidaridad individual como “única solución” y levantar los pilares de una lucha social.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### No nos dejemos culpar por los medios informativos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

No olvide que el lenguaje es un arma y hoy el gobierno eso lo sabe muy bien, por eso trabaja en llave con sus respectivos amigos en los medios. Tenga presente empresa de medios y sus dueños:

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **El Tiempo, City TV:** Sarmiento Angulo. En su editorial del 9 de junio de 2018 abiertamente sin tapujos, declaró apoyar la campaña del entonces candidato Iván Duque.
-   **Semana:** su directora editorial es Sandra Suárez, ex ministra de ambiente, de vivienda y desarrollo territorial de Álvaro Uribe. Además, fue directora del Plan Colombia.
-   **RCN, La FM:** grupo Ardila Lule. 
-   **Caracol Radio y W radio:** grupo Prisa, fundado por Jesús Polanco Gutiérrez, quien le publicaba el periódico oficial al fascismo en la España de Franco. Franco, el amigo y aliado de Hitler y Mussolini.
-   **Blu radio, El Espectador, Caracol TV**: grupo Santo Domingo.

<!-- /wp:list -->

<!-- wp:paragraph -->

Y así, la lista es larga… ojalá no olvidemos, que la amistad entre amos y esclavos jamás será verdadera. Por tanto, los dueños de los medios que son amigos del presidente jamás serán nuestros amigos, ni por mucho entretenimiento que nos regalen, ni por algunas notas buenas que nos ofrezcan. Ojo ahí, alteremos todas nuestras defensas.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas: [Otras columnas de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)

<!-- /wp:paragraph -->
