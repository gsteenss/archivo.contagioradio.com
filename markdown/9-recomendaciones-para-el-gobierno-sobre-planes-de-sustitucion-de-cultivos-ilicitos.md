Title: 9 recomendaciones para el Gobierno sobre planes de sustitución de cultivos ilícitos
Date: 2017-05-03 09:06
Category: DDHH, Nacional
Tags: Acuerdos de La Habana, sustitución de cultivos ilícitos
Slug: 9-recomendaciones-para-el-gobierno-sobre-planes-de-sustitucion-de-cultivos-ilicitos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cocaleras-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [03 May 2017] 

Un grupo plural, conformado por organizaciones de la sociedad civil, centros de investigaciones adscritos a las universidades y académicos, que apoyan el proceso de paz entre el gobierno y las FARC-EP, desarrollaron **9 recomendaciones sobre la implementación del punto cuatro de los acuerdos de paz, sobre sustitución de los cultivos ilícitos**, con miras a aportar herramientas que permitan que este paso se dé con mejores garantías para los campesinos.

Este documento fue enviado a diferentes órganos del gobierno como a la Dirección para la Atención Integral de la Lucha contra las drogas, a la Alta Consejería para el Posconflicto, al Ministerio de Justicia, a la Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Fina, entre otras, con la finalidad de “asegurar las transformaciones profundas que requiere el campo colombiano para que se **modifiquen las condiciones que han permitido que los cultivos de uso ilícito permanezcan en los territorios”**.

### **Recomendaciones** 

Sobre la puesta en marcha de los planes de sustitución de cultivos ilícitos, las organizaciones exponen que estos planes hacen parte de una reforma rural integral que no se ha materializado por lo tanto señalan que “**es necesario garantizar que las medidas de sustitución de cultivos de uso ilícito hagan parte de la política de desarrollo rural** en su sentido más amplio” y no fragmentadas como se ha venido desarrollando.

Para poder avanzar en este punto, le recomiendan al gobierno “diseñar y poner en marcha una estrategia de **monitoreo con indicadores y metas de transformación real de los territorios y no sólo de hectáreas erradicadas o sembradas**”, que incluya otros índices como el de nivel de pobreza para establecer avances y retrocesos. Le puede interesar: ["Si se puede hacer sustitución de cultivos de uso ilícitos: COCCAM"](https://archivo.contagioradio.com/si-se-puede-hacer-sustitucion-concertada-de-cultivos-de-uso-ilicito-coccam/)

En concordancia con el punto anterior, la siguiente recomendación es la **importancia de definir una política pública integral y sostenible para la sustitución de cultivos** de uso ilícito, ya que, hasta el momento, no se conoce ningún documento que fundamente los planes de sustitución. Para la construcción del mismo, las organizaciones expresaron que debe existir un enfoque territorial diferencial de género y étnico.

La tercera recomendación hace énfasis en como ese **enfoque territorial debe guiar la intervención que se haga en cada lugar**, teniendo como centro las características de cada vereda y corregimiento, y no exclusivamente las familias, reduciendo de esta forma la posibilidad de que los avances no sean sostenibles.

Frente a la cuarta recomendación, las organizaciones exponen la necesidad de **garantizar un manejo responsable y transparente, por parte del gobierno, de los recursos** que estén presupuestados, para que de esa forma el Estado no asuma compromisos que no están respaldados por una designación previa. Le puede interesar: ["Dijimos no a la erradicación, pero sí a la sustitución: Campesinos de Argelia, Cauca"](https://archivo.contagioradio.com/dijimos-no-a-la-erradicacion-pero-si-a-la-sustitucion-campesinos-de-argelia-cauca/)

Otra de las recomendaciones que aparece en el documento tiene que ver con las erradicaciones forzadas que se están llevando a cabo en diferentes partes del país; las organizaciones indicaron que es importante que el **gobierno privilegie los planes de sustitución de cultivos ilícitos y las articule con las erradicaciones manuales**, sin embargo, los campesinos han expresado que estas dos actividades, se están realizando en paralelo, en contravía de lo acordado en La Habana.

De otro lado, las organizaciones expresaron que las FARC-EP deben definir, con claridad,de qué manera van a cumplir con el compromiso de contribuir a solucionar el problema de las drogas ilícitas, de igual forma expresaron su preocupación por **la participación de las comunidades en la construcción de los planes de erradicación**, para que los mismos tengan legitimidad y respaldo de la ciudadanía.

Para finalizar, este grupo plural afirmó que debe **existir una celeridad en la presentación del proyecto de ley para el tratamiento penal y diferencial del que trata el Acuerdo Final**, en el Congreso, “con el fin de brindar seguridad jurídica tanto a los funcionarios que están involucrados en la sustitución, como a las comunidades que suscriben los acuerdos” al igual que garantías de la vida e integridad física de las comunidades, que han sido víctimas del conflicto armado en el país y que se han visto amenazadas por el re agrupamiento de estructuras paramilitares.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
