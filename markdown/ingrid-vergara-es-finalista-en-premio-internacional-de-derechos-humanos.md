Title: Ingrid Vergara es finalista en premio internacional de Derechos humanos
Date: 2016-05-14 17:34
Category: Nacional
Tags: Derechos Humanos, MOVICE, paz
Slug: ingrid-vergara-es-finalista-en-premio-internacional-de-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Ingrid-vergara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Amnistía Internacional 

###### [13 May 2016] 

Por su lucha en la búsqueda de justicia y verdad frente a los crímenes de Estatales, Ingrid Vergara,  secretaria del Movimiento de víctimas de crímenes de Estado, es finalista del Premio Internacional Front Line Defenders para defensores y defensoras de derechos humanos en riesgo.

El pasado 10 de mayo Vergara, fue mencionada como una de las seis finalistas para ganarse este reconocimiento a aquellos que pese a las amenazas en su contra continúan defendiendo a las víctimas en los países donde el número de líderes sociales asesinados continúa en aumento, como sucede en Colombia, en donde se contabilizaron entre enero y noviembre de 2015 54 de los de los 87 en total que sucedieron en América Latina.

La defensora de derechos humanos colombiana, fue preseleccionada entre 126 nominaciones de 53 países. Además de ella, también hay líderes sociales nominados de países como Azerbaiyán, Birmania/Myanmar, Colombia, Honduras, Palestina y Tanzania.

Cada uno de los finalistas a nuestro premio anual lucha por la justicia y los derechos humanos en su comunidad, y lo hace enfrentando serios riesgos. Ingrid defiende los derechos de las comunidades indígenas y exige justicia para los crímenes de Estado en uno de los países más peligrosos del mundo para defensores y defensoras de derechos humanos. Nos sentimos honrados de reconocerla como una de las finalistas”, dijo Mary Lawlor, directora ejecutiva de Front Line Defenders, al anunciar a los finalistas en Dublín, Irlanda.

Ingrid Vergara, hace parte de los defensores humanos del MOVICE. Una organización que hace presencia en los departamentos de Atlántico, Sucre y Magdalena Medio, donde la principal amenaza son los grupos paramilitares que han  amenazado de  de muerte de forma constante en los últimos años, a través de panfletos, llamadas telefónicas y comentarios comunicados a miembros de sus comunidades, a personas como Ingrid, que pese a esa situación continúan en su labor de defensa de los derechos humanos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
