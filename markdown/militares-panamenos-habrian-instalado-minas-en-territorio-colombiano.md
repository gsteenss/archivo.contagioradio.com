Title: Militares panameños habrían instalado minas en territorio colombiano
Date: 2016-07-05 12:37
Category: DDHH, Nacional
Tags: base militar binacional Colombia Panamá, bases militares de EEUU en Colombia, Comisión de Justicia y Paz, indígenas Wounaan
Slug: militares-panamenos-habrian-instalado-minas-en-territorio-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/militares-panama.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [5 Julio 2016] 

De acuerdo con la 'Comisión Intereclesial de Justicia y Paz', el pasado lunes 20 de junio en inmediaciones del sitio conocido como 'Palo de Letras', en Chocó, un grupo de militares regulares panameños intimidaron a indígenas Wounaan de la comunidad Juin Pubur, les manifestaron que tenían prohibido transitar por el lugar y **les advirtieron que habían instalado minas en el sector**.

<div class="texte entry-content">

Los pobladores indígenas estaban de cacería cuando fueron abordados por los militares panameños que custodian la base militar binacional 'Guamal', construida hace 3 años por los Gobiernos de Panamá, Colombia y Estados Unidos, **sin contar con la consulta previa a las comunidades y desconociendo el Convenio 169 de la OIT** que busca garantizar los derechos a la tierra, la salud y la educación para los pueblos originarios.

</div>

Las comunidades afrocolombianas e indígenas reiteran que la base militar fue construida en el territorio colectivo que les fue adjudicado en 1988 y que la presencia de los militares les ha [[impedido moverse libremente, cazar y pescar](https://archivo.contagioradio.com/cacarica-19-anos-operacion-genesis/)], por lo que reclaman la atención de los Gobiernos de Panamá y Colombia, teniendo en cuenta que los **efectivos panameños están controlando zonas que pertenecen a territorio colombiano**.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

   
 
