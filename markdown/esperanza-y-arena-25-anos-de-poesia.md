Title: Esperanza y Arena 25 años de poesía
Date: 2015-09-26 11:00
Category: Cultura, eventos
Tags: Grupo poético Esperanza y Arena 25 años, Universidad Santo Tomás
Slug: esperanza-y-arena-25-anos-de-poesia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/1_La_poes_a-e1443218303291.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [25 Sep 2015]

El sábado 26 de septiembre en el auditorio Acta de independencia de la Universidad Santo Tomás, sede Lourdes, el grupo poético "**Esperanza y Arena**" celebrará 25 años de fundación, con el lanzamiento y presentación del libro: *En AmorArte*.

Para conmemorar el nacimiento del colectivo cultural, se realizará un recital, con presencia  y voces de los poetas Guillermo Quijano (Premio Nacional de Poesía Carlos Martín); el poeta y periodista José Luis Díaz Granados ( Premio Nacional de novela); María del Socorro Jaramillo (Premio de literatura Ciudad Santa Marta) y Orlando Becerra, cofundador del Teatro Libre de Bogotá entre otros.

**"Esperanza y Arena"** fue fundado en el año 1991 en el barrio La Candelaria, fruto del taller impartido por Jaime García Maffla. En la actualidad cuentan con más de 30 publicaciones realizadas bajo el sello *Magia de la palabra*.

Al finalizar, los asistentes disfrutaran de una copa de vino y podrán adquirir el libro.

#### **Lugar**: Auditorio Acta de la independencia–Universidad Santo Tomás Carrera 9A \# 63-28

#### **Hora:** 3:00- 5:00 pm.

 
