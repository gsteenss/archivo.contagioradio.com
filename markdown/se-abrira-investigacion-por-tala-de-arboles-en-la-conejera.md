Title: Se abrirá investigación por tala de árboles en La Conejera
Date: 2015-06-17 16:03
Category: Ambiente, Nacional
Tags: Bogotá, Desastre ambiental, Humedales de Bogotá, La Conejera, REserva de Fontanar, Secretaría de Ambiente, Suba
Slug: se-abrira-investigacion-por-tala-de-arboles-en-la-conejera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11291982_10153263102725020_57746706_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4654655_2_1.html?data=lZuilpuZeY6ZmKiak5WJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8afwsffy9eJh5SZopaYy9PaqdToyszOxc6Jh5SZo5jbjdXTtozowtHOjcnJb4a3lIquk9fGs83Z1JDS0JCwpYy30JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Xua Malagón, ambientalista] 

Este martes, los ambientalistas y defensores del humedal La Conejera se manifestaron en contra del arboricidio que cometió la semana pasada las autoridades distritales, donde se talaron **12 árboles, entre ellos 3 pinos** que hacían parte del espacio público, y que constituyen un corredor para el vuelo de las aves, también se afectó animales como **loros, torcazas, picos australianos, lechuzas, halcones, alcaravanes**, además después de la tala se encontró **cinco nidos, huevos y restos de aves.**

Según Xua Malagón, una de las ambientalistas, con el plantón se logró hablar con las autoridades que dieron los permisos  para la tala de los árboles, con ellos se acordó que se va a establecer de manera permanente, un punto de la secretaría de ambiente del distrito en esa zona de la localidad de Suba para realizar una veeduría ambiental, así mismo**, se abrirá una investigación en contra de la constructora** que lleva a cabo el proyecto urbanístico Reserva de Fontanar.

**Fueron tres resoluciones de la Secretaría de Ambiente las que aprobaban la tala de los árboles,** sin embargo, de acuerdo a Malagón, se estudió esos documentos y se identificaron varias inconsistencias. La primera, fue que se declaraba como un espacio privado, una zona que realmente es pública, por lo que se había exigido una revocatoria directa a las resoluciones emitidas por secretaría, y aunque se pidió un tiempo para esclarecer la realidad de ese espacio, se permitió que se continuara con la tala.

Aunque desde hace un año, se está pidiendo un principio de precaución, mientras se establece el impacto ambiental, a la fecha ni siquiera se ha registrado el primer estudio donde se verifique la situación ambiental del humedal y cómo se vería afectado por la construcción.

Pese a todas las inconsistencias que han denunciado los defensores del humedal, “ninguna autoridad se ha pronunciado para suspender la obra”, afirma la ambientalista, quien añade que **la construcción sigue avanzando y se denuncia que está construyendo en las tardes y las noches, lo que es “totalmente ilegal”**, dice Xua.

La comunidad sigue atenta a la situación de la obra y se planea continuar con manifestaciones y plantones frente a las instituciones competentes, hasta que se logre alguna respuesta por parte de las autoridades distritales.

[![11541304\_10153263105385020\_1579390171\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11541304_10153263105385020_1579390171_n.jpg){.aligncenter .wp-image-10199 width="395" height="526"}](https://archivo.contagioradio.com/se-abrira-investigacion-por-tala-de-arboles-en-la-conejera/11541304_10153263105385020_1579390171_n/)[![11212469\_10153263102025020\_1560063605\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11212469_10153263102025020_1560063605_n.jpg){.aligncenter .wp-image-10200 width="368" height="490"}](https://archivo.contagioradio.com/se-abrira-investigacion-por-tala-de-arboles-en-la-conejera/11212469_10153263102025020_1560063605_n/)

[  
](https://archivo.contagioradio.com/se-abrira-investigacion-por-tala-de-arboles-en-la-conejera/11541304_10153263105385020_1579390171_n/)[![Sin título](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Sin-título-1024x491.png){.aligncenter .wp-image-10201 .size-large width="604" height="290"}](https://archivo.contagioradio.com/se-abrira-investigacion-por-tala-de-arboles-en-la-conejera/sin-titulo-7/) [![11419949\_10153263098855020\_1586113643\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11419949_10153263098855020_1586113643_n-300x225.jpg){.aligncenter .wp-image-10202 width="498" height="373"}](https://archivo.contagioradio.com/se-abrira-investigacion-por-tala-de-arboles-en-la-conejera/11419949_10153263098855020_1586113643_n/)
