Title: “Colombia necesita una cultura de paz” Patricia Ariza
Date: 2015-04-07 16:54
Author: CtgAdm
Category: Entrevistas, Paz
Tags: 9 de abril, 9A, Corporación Colombiana de Teatro, Cumbre mundial de artistas por la paz, fernando Vallejo, madres de soacha, Patricia Ariza, Política y paz en Colombia, Víctimas en Colombia
Slug: colombia-necesita-una-cultura-de-paz-patricia-ariza
Status: published

<iframe src="http://www.ivoox.com/player_ek_4320907_2_1.html?data=lZifkp6Ue46ZmKiak5WJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nh9Dg0NLPy8aPssbXxtjW1saPuc%2FVjMjiztnZtsKfxcqY0sbeaaamhp2dh56ob7HV1dfWxc7Fb6LmypKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Patricia Ariza, Organizadora de Cumbre Mundial] 

Uno de los objetivos importantes de la **Cumbre Mundial de Artistas por la Paz**, es conformar un movimiento cultural que incida en la paz y analizar y proponer acciones para otorgarle al proceso de paz la dimensión cultural y artística, afirma **Patricia Ariza, directora de la Corporación Colombiana de Teatro y organizadora de la Cumbre** que este año realiza su tercera versión.

Ariza señala que la institucionalidad no ha tenido en cuenta el aporte que pueden hacer los artistas a la construcción de la paz,  “*ni en el gobierno, ni en la mesa de conversaciones*” se ha planteado la necesidad de que se genere en Colombia la cultura de la paz, puesto que “*el conflicto no solamente es armado y social sino también cultural*”.

Nosotros queremos entrar con un espíritu muy positivo para construir la cultura de la paz que le está haciendo falta a este país. “***A través del arte se puede transformar el dolor** de las víctimas, el arte lo que hace es tramitar el dolor y convertirlo en impulso de construcción de paz*” afirma Ariza y recuerda que es necesario que se tenga en cuenta el gran aporte de los artistas ya que en Colombia “**Hay muchos artistas que están comprometidos con la causa de la paz**”

\[embed\]https://www.youtube.com/watch?t=270&v=8tibvkSfcqU\[/embed\]
