Title: Estos son los avances para la comunidad LGBTI en Colombia
Date: 2015-05-06 18:22
Author: CtgAdm
Category: closet, LGBTI
Tags: Adopción igualitaria, Comunidad LGBTI, Derechos Humanos, Homosexualidad, Igualdad
Slug: estos-son-los-avances-para-la-comunidad-lgbti-en-colombia
Status: published

###### Foto: www.kienyke.com 

 

La adopción igualitaria en Colombia sigue siendo tema de debate, la Corte Constitucional continúa en el análisis de la aprobación absoluta, teniendo en cuenta que hasta el momento solo se permite adoptar a parejas del mismo sexo siempre y cuando el menor sea hijo biológico de alguna de las dos personas, lo que se espera por parte de la comunidad LGBTI es que se elimine cualquier condición que los limite y que se permita la adopción de menores que no cuentan con un hogar permanente.

Hasta hace unos meses las posibilidades de adopción para la comunidad LGBTI eran mínimas, actualmente el panorama es mucho más claro y las posibilidades se han ampliado, pues, en vista de que se siguen violando derechos fundamentales en términos de familia, las reacciones de la comunidad por medio de demandas no dan espera; Jaime Córdoba Triviño un jurista de talante liberal es el conjuez que definirá el desempate.

A la espera de que se defina la situación de adopción igualitaria, se resaltan los avances de los que goza la comunidad LGBTI en Colombia, entre las más importantes están las sentencias: **C-577 de 2011**, que reconoció que una pareja homosexual puede considerarse una familia, **la SU-671 de 2014,** que le permitió a una lesbiana de Medellín adoptar a la hija biológica de su pareja.

Por otro lado, con la sentencia la **C-481 de 1998** se eliminó una de las peores discriminaciones contra gais y lesbianas a quienes se les prohibía ser profesores, la ley de derecho a la identidad que le permitió a un transexual cambiar su nombre de nacimiento por uno que se amoldaba más con su nueva identidad, la **SU-337 de 1999,** en la que reconoció que las personas intersexuales constituyen “una minoría que goza de especial protección del Estado”, mediante la Sentencia **T-876 de 2012,** la Corte dio un paso importante al concederle a una persona su deseo de cambiar su sexo y finalmente la SU-617 de 2014 le abrió la puerta a la adopción consentida, es decir, a la posibilidad de que un homosexual adopte al hijo biológico de su pareja.

De acuerdo a Germán Rincón Pretelt abogado y defensor de la comunidad LGBTI “la adopción y el matrimonio igualitario no tienen reversa”.
