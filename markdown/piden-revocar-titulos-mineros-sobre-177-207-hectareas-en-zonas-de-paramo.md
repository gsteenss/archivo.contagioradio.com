Title: Piden revocar títulos mineros sobre 177.207 hectáreas en zonas de páramo
Date: 2016-06-28 13:08
Category: Ambiente, Entrevistas
Tags: páramos, titulos mineros
Slug: piden-revocar-titulos-mineros-sobre-177-207-hectareas-en-zonas-de-paramo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/carbonoybosques.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Carbono y Bosques] 

###### [28 Jun 2016]

El 27 de junio diferentes voceros pertenecientes a la plataforma política Cumbre Agraria Étnica Campesina y Popular, presentaron una **petición conjunta solicitando caducidad de los títulos mineros que se ubican en las zonas de páramo** y áreas protegidas que aún continúan en estos lugares pese a que en el pasado mes de febrero,[la Corte Constitucional prohibió la minería en los páramos del país.](https://archivo.contagioradio.com/400-tituloa-mineros-seran-retirados-por-fallo-de-la-corte-constitucional/)

Según la organización de Mecanismos de Información de Páramos, en la actualidad las empresas con mayor cantidad de títulos mineros en zonas de páramo son[Anglogold Ashanti Colombia S.A que tiene 41.849 hectáreas](https://archivo.contagioradio.com/anla-y-ministerio-del-interior-adelantaban-consulta-previa-ilegal-en-parque-tayrona/)en el páramos de Santurban, Los Nevados y Chili-Barragan, las empresas Acerías Paz del Río y Minas Paz del Río que tienen entre las dos **112.510 hectáreas** tituladas que se sobreponen en los páramos de Guantiva - La Rusa, Pisba, Rabanal y Río Bogotá y Greystar Rsources tiene **22.848 en el páramo de Santurban.**

De acuerdo con José Santos, vocero de la Cumbre Agraria Étnica Campesina y Popular, después del fallo de la Corte Constitucional el gobierno [debería proceder a derogar los títulos mineros en zonas de páramos](https://archivo.contagioradio.com/el-agua-los-paramos-y-la-vida-le-ganan-un-round-a-los-negocios-en-colombia/), sin embargo esta acción no se esta llevando a cabo por falta de entendimiento al interior de las instituciones, por ejemplo los títulos otorgados en el municipio de Suarez, corregimiento de La Toma, a través de la sentencia 1045A, interpuesta por una acción de tutela de las comunidades, falló desde la Corte Constitucional a favor de la población para que se revoquen los títulos mineros, pero **el gobierno en cabeza de la Agencia Nacional Minera dice que esos actos administrativos no se pueden echar para atrás.**

Santos afirma que es allí en donde esta la importancia de realizar las consultas previas a las comunidades "Las comunidades negras e indígenas le hemos dicho al gobierno que antes de que genere un acto administrativo que le de derecho a un tercero [se debe hacer la consulta](https://archivo.contagioradio.com/balance-ambiental-la-locomotora-arrollo-los-ecosistemas-en-el-pais/), para no generar un hecho causado, porque luego nos vienen **con el cuento de que no pueden derogar ese título minero que han entregado".**

Se espera que las discusiones sobre los títulos mineros entre el gobierno y la Cumbre Agraria se den pronto a partir de **los acuerdos generados en el último paro Agrario,** en donde se establecía el tema minero energético desde el pliego de exigencias.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

###### 
