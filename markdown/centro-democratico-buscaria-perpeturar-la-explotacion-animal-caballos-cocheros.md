Title: Centro Democrático buscaría perpetuar la explotación animal de caballos cocheros
Date: 2017-05-07 15:55
Category: Animales, Nacional
Tags: caballos cocheros, Centro Democrático, Maltrato animal
Slug: centro-democratico-buscaria-perpeturar-la-explotacion-animal-caballos-cocheros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/cocheros-e1494189726637.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La FM 

###### [7 May 2017] 

En el Congreso de la República, se tramita un proyecto de Ley disfrazado de bienestar animal. Así lo denuncian organizaciones animalistas como la Plataforma ALTO, quienes aseguran que el senador Fernando Araujo del Centro Democrático, busca **blindar la explotación animal de los caballos cocheros, sin siquiera garantizar los derechos laborales de quienes viven de esta actividad.**

De acuerdo con un comunicado del Senado, este proyecto de ley "establece que el Gobierno Nacional deberá reglamentar las condiciones mínimas de salud, higiene, sanidad, circunstancias locativas, bienestar y los demás factores que considere pertinentes para prevenir el dolor, sufrimiento  y enfermedades en la vida de los **semovientes que son utilizados en vehículos de tracción animal para fines turísticos,** sin perjuicio de las competencias y facultades de las entidades territoriales”.

Araujo ha asegurado, “No veo una alternativa distinta que reglamentar los coches, que implique sanciones a quienes atenten contra los caballos, esto para buscar conciliar una tradición cartagenera y preservar la vida y salud de los animales”. No obstante, para Mateo Córdoba, integrante de la Plataforma ALTO, no es necesario regularizar para garantizar el bienestar animal.

### ¿Qué buscaría el proyecto? 

La alcaldía de Cartagena ya no será la entidad encargada de la vigilancia de los caballos cocheros, sino Corpoturismo. Y aunque la propuesta parece tener en cuenta el bienestar animal, el único fin sería lucrativo y tanto los trabajadores como los caballos serían los grandes perdedores, según explican desde la Plataforma ALTO.

**“Lo que hay es una retaila supuestamente animalista para justificar la formalización de una actividad económica de la cual el senador sacaría provecho”,** con base en una Ley “que no tiene una sola palabra de derechos laborales”, asegura Córdoba.

Los animalistas que han analizado el proyecto denuncian que la iniciativa legislativa no habla de verdadera protección animal. Lo que se busca es blindar la tracción a sangre, y en cambio no deja abierta la puerta para la sustitución voluntaria, se le da una máscara corporativa a la actividad de los cocheros, y además **abre la posibilidad para que los caballos sean explotados de la misma forma que en Cartagena en ciudades como Cúcuta, Buenaventura, Rioacha, Popayán y Tumaco**; sumado a lo anterior, se habla incluso de que los trabajadores se verían gravemente afectados, ya que se verían obligados a pagar un nuevo impuesto.

**“Lo fundamental es que hay una doble condición de explotación y precariedad, animales humanos y no humanos,** jornadas largas para los animales, y personas en condiciones económicas muy difíciles”, dice el animalista, quien asegura que la propuesta se basa en un falso dilema, pues desde la alcaldía de Cartagena ya se ha avanzado en el bienestar animal de los caballos. Asimismo, ya existe un diálogo propositivo entre los cocheros y los animalistas para llevar a cabo una veeduría respetuosa y mancomunada  en torno a las condiciones dignas de los animales. De echo, **lo que se busca es impulsar un plan piloto de sustitución voluntaria garantizando los derechos laborales.**

<iframe id="audio_18562193" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18562193_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
