Title: La USO completa 30 días de huelga por la defensa del patrimonio
Date: 2020-07-26 09:29
Author: CtgAdm
Category: Actualidad, DDHH
Slug: la-uso-completa-30-dias-de-huelga-por-la-defensa-del-patrimonio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Foto3_David_Pinzón-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde el 25 de junio dos integrantes de la USO se encadenaron en el monumento en representación de las válvulas de pozos petroleros, al frente de la sede de [Ecopetrol](https://archivo.contagioradio.com/trabajadores-de-54-plantas-de-ecopetrol-protestan-contra-la-posible-venta-de-la-empresa-estatal/)en Bogotá, la acción por parte de los dirigentes obreros, "El Machín de la resistencia", es un acto de respaldado por los y las trabajadoras de las 54 plantas de transporte de crudo a nivel nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ellos son **Fabio Díaz, de la SUO - USO,** y **Hernando Silva, perteneciente a la Secretaría de Derechos Humanos y Paz de la SUO,** voceros del sindicado que acatando todas los protocolos de protección determinados por el Gobierno Distrital y Nacional, defienden encadenados sus derechos y el de más de 300 trabajadores que hoy correr el riesgo de quedar desempleados.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/i/status/1280287572581920768","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/i/status/1280287572581920768

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Hernando Silva, como vocero de la USO** señaló que en estos 30 días de protesta han estado a la espera de que el Gobierno Nacional se pronuncie por medio de Ecopetrol, ***"y deje de dar dilación a este tema, esperamos la buena fe por parte de los dirigente de Ecopetrol y que decidan llevar esto a feliz término".***

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **"*Hacemos un llamado al pueblo colombiano para que entre todos fortalezcamos esta defensa,*** *para que se informen y entienda que bloquear las venas de Ecopetrol, terminaría en un colapso total de este sector*"
>
> <cite>**Hernando Silva| Secretaría de Derechos Humanos y Paz de la SUO**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

  
Y agregó que se debe extender este llamado, ***"la venta de Ecopetrol es asunto todos, porqué quién va a sufrir toda esta crisis es el pueblo colombiano, y lo verá en el desangre por medio de más impuestos y reformas tributarias"***. ([Día sin IVA, un insulto a Colombia y una vergüenza mundial](https://archivo.contagioradio.com/dia-sin-iva-un-insulto-a-colombia-y-una-verguenza-mundial/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Razones de la movilización de los integrantes de la USO

<!-- /wp:heading -->

<!-- wp:paragraph -->

Primero, se da luego de la presentación del[Decreto 811](http://www.usofrenteobrero.org/index.php/actualidad/7590-el-decreto-811-del-2020-debe-derogarse-es-inconstitucional), emitido por el Gobierno en el marco del Estado de Emergencia, y que *"auto- habilita al Estado para vender empresas públicas que cotizan en la bolsa, entre ellas Ecopetrol y así generar un alivio financiero al país"*, **acción que según el vocero es inconstitucional** y desconoce no solo lo señalado en la Constitución si no los derechos del sector obrero.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Segunda, el sindicalista indicó que Ecopetrol en el 2013 pasó de ser una sola empresa a convertirse en un conglomerado de empresas una de ellas [CENIT ,](http://www.usofrenteobrero.org/index.php/actualidad/7599-cenit-es-el-corazon-de-ecopetrol) empresa que encabeza el transporte por medio de ductos y poliductos del país, lo que la convierte en la ***"sección más rentable de Ecopetrol y que ahora quiere ser vendida"***.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tercera y ultima razón, Silva la denominó **"masacre laboral"**, que se da como consecuencia del traslado de nómina de Ecopetrol a CENIT, lo que representa no solo posibles despidos para quienes no acaten esta decisión, sino una significativa disminución de los contratos laborales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D222598968850182&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
