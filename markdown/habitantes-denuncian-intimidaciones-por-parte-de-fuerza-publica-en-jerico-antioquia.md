Title: Habitantes denuncian intimidaciones por parte de Fuerza Pública en Jericó, Antioquia
Date: 2018-05-10 12:11
Category: Ambiente, Nacional
Tags: Anglo Gold Ashanti, Antioquia, Cobre, Fuerza Pública, Jericó
Slug: habitantes-denuncian-intimidaciones-por-parte-de-fuerza-publica-en-jerico-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/cosajuca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cosajuca] 

###### [10 May 2018] 

Los habitantes del corregimiento Palo Cabildo, en Jericó, Antioquia, denunciaron que el pasado lunes 7 de mayo, mientras campesinos se movilizaban en contra de la presencia de una comitiva de la multinacional AngloGold Ashanti, integrantes de la Fuerza Pública, específicamente del GOES, **arremetieron hacia las personas que se movilizaban, amenazándolos e intimidandolos con capturarlos**.

De acuerdo con Fernando Jaramillo, integrante de la Mesa Ambiental de Jericó, los campesinos venían marchando, luego de haber logrado que el grupo de personas de la multinacional se retiraran del territorio, **minutos después abría aparecido el GOES en actitud agresiva y fuertemente armados.**

Jaramillo afirmó que la delegación se habría retirado del lugar porque “desde hace aproximadamente 5 años, la empresa AngloGold Ashanti y los campesinos de Palo Cabildo, **llegaron al acuerdo de que la empresa no iba a transitar por ese corregimiento, porque cerca de él**, está la zona de exploración” que la multinacional ha buscado explotar cobre.

### **Los habitantes de Jericó no quieren actividades extractivas en su territorio** 

Los habitantes han rechazado la presencia de esta multinacional y la explotación de cobre en el territorio, **porque afirman que de hacerlo se contaminarían las fuentes de agua y los acueductos municipales** no solo de este corregimiento, sino también de algunos aledaños como Támesis.

El año pasado, la comunidad había interpuesto una tutela en el Consejo de Estado contra el Tribunal Administrativo de Antioquia que frenó una medida del Consejo Municipal de Jericó en donde se prohibía la minería en el territorio. (Le puede interesar:["Habitantes de Jericó tutelaran derecho a prohibir la minería en su territorio"](https://archivo.contagioradio.com/habitantes-de-jerico-tutelaran-derecho-a-prohibir-la-mineria-en-su-territorio/))

 Jaramillo, afirmó que faltan pocos días para que se conozca el fallo del Consejo de Estado sobre este tema, sin embargo, manifestó que esa tutela es producto de la “voluntad popular que se manifiesta en **un rechazo activo a la presencia de las multinacionales y a la pretensión del gobierno Nacional de disponer el territorio que es agropecuario como uno minero**”.

<iframe id="audio_25902049" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25902049_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
