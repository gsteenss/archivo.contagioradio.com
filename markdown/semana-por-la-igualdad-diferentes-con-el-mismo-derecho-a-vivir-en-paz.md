Title: Semana por la igualdad: "Diferentes con el mismo derecho a vivir en paz"
Date: 2015-09-30 15:01
Category: LGBTI, Nacional
Tags: Alcadía de Bogotá, Diversidad sexual, Juan Carlos Prieto, LGBTI, Maia, Naty Botero, ONU, política pública LGBTI, Secretaria de Planeación Distrital de Bogotá, Semana por la igualdad
Slug: semana-por-la-igualdad-diferentes-con-el-mismo-derecho-a-vivir-en-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/lgbti.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### www.elrebelde.com 

<iframe src="http://www.ivoox.com/player_ek_8711002_2_1.html?data=mZyek5WUdo6ZmKiak5aJd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bhwtPOjdXTtozgwpDWydrFsMXVxYqgo5CJdpO4ysvS1MrSuMbnjMjc0JDJsIzhytja0ZDIqdPZxM2ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Juan Carlos Prieto, Secretaria Distrital de Planeación] 

###### [30 Sept 2015] 

Del 24 al 3 de octubre se realiza en Bogotá, la quinta Semana Por la Igualdad, **“Diferentes con el mismo Derecho a vivir en Paz”,** resaltando que la paz no solo se logra con los diálogos en la Habana sino también al interior de la sociedad colombiana cuando se deja de discriminar al otro o la otra por su orientación sexual, como lo afirma** **Juan Carlos Prieto Director de Diversidad Sexual de la Secretaria Distrital de Planeación.

Durante esta semana se tiene como objetivo fomentar una cultura de respeto, libre de discriminación por orientaciones sexuales e identidades de género, a través del intercambio de experiencias y construcciones políticas, pedagógicas, culturales y organizativas, dirigidas a la ciudadanía en general.

La semana se celebra en el marco del cumplimiento **del Decreto 062 de 2014, por medio del cual la Alcaldía Mayor de Bogotá a través de la Dirección de Diversidad Sexual** de la Secretaría Distrital de Planeación, ha desarrollado políticas públicas con el fin de promover el respeto y la igualdad para la comunidad LGTBI.

Actualmente ONU Hábitat y el Estado de Dubái reconocieron que Bogotá posee las mejores prácticas de cambio cultural, logrando que los indicadores de discriminación hacia la población LGBTI disminuyeran notablemente. Según las estadísticas del Distrito, **hace 4 años los índices de discriminación  hacia el movimiento LGBTI eran del 98%, actualmente se han reducido al 69.4%.** Sin embargo, **todavía existen muchas barreras para que las personas de la comunidad accedan efectivamente a los derechos a la salud y al trabajo.**

Para el director de Diversidad sexual, los imaginarios erróneos y negativos que posee la ciudadanía frente a la comunidad y **las posturas de algunos consejales de Bogotá que no creen en la política pública para la comunidad LGBTI,** no han permitido que todos recursos necesarios sean destinados a garantizar los derechos de esta población LGBTI.

"Se necesita que la Diversidad Sexual sea reconocida en los territorios, barrios y calles logrando construir  respeto e igualdad frente a la población LGBTI que ha sido discriminada por su orientación sexual  e identidad de género", expresa Prieto.

**Agenda Semana por la Igualdad **  
[![1196543](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/1196543.jpg){.aligncenter .size-full .wp-image-15032 width="680" height="799"}](https://archivo.contagioradio.com/semana-por-la-igualdad-diferentes-con-el-mismo-derecho-a-vivir-en-paz/attachment/1196543/)  
![1196542](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/1196542.jpg){.aligncenter .size-full .wp-image-15031 width="680" height="799"}  
[  
](https://archivo.contagioradio.com/semana-por-la-igualdad-diferentes-con-el-mismo-derecho-a-vivir-en-paz/attachment/1196546/) [![1196545 (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/1196545-1.jpg){.aligncenter .size-full .wp-image-15030 width="680" height="465"}![1196546](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/1196546.jpg){.aligncenter .size-full .wp-image-15029 width="680" height="725"}](https://archivo.contagioradio.com/semana-por-la-igualdad-diferentes-con-el-mismo-derecho-a-vivir-en-paz/1196545-1/)
