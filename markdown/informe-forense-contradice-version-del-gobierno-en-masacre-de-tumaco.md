Title: Informe forense contradice versión del gobierno en masacre de Tumaco
Date: 2017-10-07 21:23
Category: DDHH, Nacional
Tags: Medicina Legal, Tandil, Tumaco
Slug: informe-forense-contradice-version-del-gobierno-en-masacre-de-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/masacre-tumaco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Luis Alfonso Mena Sepúlveda 

###### 7 Oct 2017 

Un informe del **Instituto de Medicina Legal y Ciencias Forenses** divulgado este sábado en horas de la tarde, luego de la visita de verificación de esa organización, concluyó que 6 campesinos asesinados este 5 de Octubre en el corregimiento de Llorente en la vereda el El Tandíl,  fueron causadas por proyectiles de alta velocidad.

### **Para la medicina forense un proyectil de alta velocidad es equivalente a un proyectil de Fusil** 

El comunicado de cuatro puntos elaborado por expertos de diversas disciplinas ente ellos: odontólogos, radiólogos y médicos forenses; manifiesta “los cuerpos presentan lesiones causadas por proyectiles de alta velocidad” con lo que se desvirtua la versión de que las muertes fueron producto de un ataque con cilindros bomba ya que ninguno de los heridos presenta esquirlas según lo divulgado por Medicina legal.

Un forense consultado por Contagio Radio explica que para la medicina forense un proyectil de alta velocidad corresponde a armas largas o fusiles. De acuerdo con la explicación cuando en un informe de medicina forense ubica la definición “proyectiles de alta velocidad” se da por entendido que fue un arma tipo fusil, además se descartan armas de carga múltiple. Es decir que quedaría desmentida la versión de que las muertes se produjeron por la explosión de cilindros bomba. Ver: [Oficina de la ONU en Colombia condena asesinato de campesinos en Tumaco](https://archivo.contagioradio.com/oficina-de-la-onu-en-colombia-condena-asesinato-de-campesinos-en-tumaco/)

### **Los disparos provienen de una distancia mayor a los 5 metros** 

Por otra parte, el punto 4 del comunicado afirma que los disparos no fueron a corta distancia, sin embargo, los forenses califican la “corta distancia” cuando hay marcas de pólvora o quemaduras, es decir que la corta distancia es lo que popularmente se conoce como “quema ropa” o menos de 5 metros de distancia explica el experto que pidió reserva de su nombre.

El experto afirma que para una persona común y corriente una distancia de 30 o 40 metros es corta, sin embargo no lo es así para la medicina forense que mide la longitud por la cantidad de partículas de pólvora u otros materiales que se impregnan en los cuerpos cuando se dispara a una distancia menor a los 5 metros. Ver:[ 9 personas muertas en medio de protestas por erradicación forzada en Tumaco](https://archivo.contagioradio.com/9-personas-erradicacion-forzada-tumaco/)

En este aspecto los testimonios de los pobladores son claros en afirmar que incluso habían entablado una comunicación directa con la policía en el sector, es decir, estaban muy cerca. Uno de los testimonios difundidos relata que se había logrado un acuerdo de diálogo con la policía pero a la hora pactada los campesinos escucharon la orden de “disparen”.

Según lo pobladores y algunos líderes de la región, luego de este primer comunicado del Instituto de Medicina Legal queda por establecerse quién estuvo al mando de la operación militar y policial, quién dio la orden de disparar y por qué el presidente Santos y Ministro de Defensa sostuvieron la versión de un supuesto ataque con cilindros bomba.

### **El comunicado confirma los nombres y las edades de las víctimas: ** 

Aldemar Gil Guachetá, de 25 años  
Janier Usperto Cortés Mairongo, de 26 años  
Nelson Chacuendo Calambas, de 29 años  
Diego Escobar Dorado, de 31 años  
Alfonso Taicús Taicús, de 32 años.  
Jaimen Guanga Pai, de 45 años
