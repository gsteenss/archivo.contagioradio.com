Title: Pescadores exigen medidas ambientales que restauren la Ciénaga de Bañó, en Córdoba
Date: 2020-05-08 21:35
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: Córdoba, Pesca artesanal, Urrá I
Slug: pescadores-exigen-medidas-ambientales-que-restauren-la-cienaga-de-bano-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Loric.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Ciénaga Lorica / Dinero

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la **Cienaga de Bañó en el corregimiento de Cotoca, en Lorica (Córdoba),** pescadores artesanales alertan que la sequía que se vive en la región amenaza no solo con acabar con su forma de empleo sino que podría terminar con el ecosistema y las especies que han defendido desde hace 20 años, a raíz de la intervención de megaproyectos como la hidroeléctrica Urrá I y su impacto en el río Sinú.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

José Felipe Negrete Ballesta, pescador artesanal relata que previo a la instauración de Urrá I, los pescadores artesanales realizaban un control sobre las aguas del corregimiento, logrando un orden en los horarios de pesca, la cantidad de pescados recolectados y una regulación en la utilización de recursos, lo que permitía que el comercio fluyera sin afectar de forma negativa el ecosistema.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La situación cambió en 1997 cuando el cause del río Sinú fue desviado, secando paulatinamente la ciénaga y reduciendo la población de peces que habitaban en el acuífero, especialmente el bocachico, "no tenemos donde hacer nuestra pesca, no hay pescado porque las ciénagas están secas", relata.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dos décadas más tarde de la entrada en funcionamiento de la central hidroeléctrica, el río Sinú se vio directamente afectado y la arteria fluvial más importante de Córdoba de 350 kilómetros y que baña a 17 municipios decayó en actividades como la pesca, de la que dependen numerosas familias campesinas que viven en cercanías al afluente.  
  
Para contrarestar el daño ambiental, el pescador sañala que han reforestado con plantas nativas y a protegen cerca de 163 especies de aves y 57 de aves acuaticas que habitan en las 303 hectáreas de la ciénaga, sin embargo ni a nivel municipal, departamental o nacional ha llegado algún tipo de ayuda, "han llegado recursos pero no han sido para nosotros, como pescadores artesanales no nos ha llegado nada".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Defender la ciénaga, la misión de los pescadores

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La sequía y el efecto negativo de la hidroélectrica en la [Ciénaga de Bañó](https://twitter.com/elcarlosjuan/status/1246778246416515073) ha afectado al menos a 150 familias de este corregimiento, "ahora nuestros humedales, están secos y es una tragedia, dentro de los mismos pescadores hay quienes han tenido que dejar la pesca y volverse mototaxistas o areneros, todo está parado". [(Le puede interesar: Inicia construcción de Hidroeléctrica que amenaza al Macizo colombiano)](https://archivo.contagioradio.com/inicia-construccion-de-hidroelectrica-que-amenaza-al-macizo-colombiano/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

José Felipe Negrete afirma que lo único que exige la comunidad es una cerca que divida lo privado de lo público pues el poco espacio con agua que aún queda en el territorio está "en manos de terratenientes con negocios de ganaderia extensiva que han acabado con toda la vegetación", además de otros factores negativos como la creciente del río Sinú que trae sedimentos que se acumulan en la parte honda del río, por lo que es necesario un levantamiento topográfico y la rehalitación de caños, riachuelos, quebradas y arroyos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
