Title: No se detiene el exterminio hacia los pueblos indígenas
Date: 2019-11-01 15:38
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Antioquia, asesinato, Cauca, indígenas
Slug: no-se-detiene-el-exterminio-hacia-los-pueblos-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ONIC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ONIC\_Colombia  
] 

Diferentes organizaciones indígenas denunciaron el asesinato de cuatro integrantes de los pueblos indígenas en diferentes partes del país, se trata de Pedro Jamioy y su esposa Rosa Jacanamejoy en Putumayo, Gilberto Domicó Domicó en Antioquia y Edison José Maestre Nieves en Cesar. Sus homicidios se suman al de cuatro integrantes de un equipo de topografía que desarrollaban su trabajo en zona rural de Corinto, Cauca, y a los cinco indígenas que perdieron la vida en la masacre de Tacueyó.

### **Pedro Jamioy y Rosa Jacanamejoy, indígenas del pueblo Kamëntsa**

Cerca de las 6 de la tarde del pasado jueves 31 de octubre se informó del asesinato de **Pedro Jamioy y su esposa, Rosa Jacanamejoy, integrantes del pueblo Kamëntsa en Sibundoy, Putumayo.** Según las primeras informaciones, la pareja de esposos eran adultos mayores, y se dedicaban a la medicina tradicional. Aunque por el hecho fue capturada una persona, se espera que los entes investigadores avancen en el caso para determinar los móviles del hecho.

### **Gilberto Domicó Domicó, guardia indígena embera** 

La Organización Indígena de Antioquia (OIA) denunció el asesinato de **Gilberto Domicó Domicó** el pasado jueves en el resguardo Pollines, del municipio de Chigorodó, Antioquia. Domicó tenía 50 años, era **parte del pueblo Embera Eyábida y hacía parte de la guardia indígena**. La OIA señaló que Domicó fue víctima de ataque con arma de fuego, presuntamente por grupos armados que hacen tránsito por ese territorio del Urabá antioqueño. (Le puede interesar: ["Cinco personas asesinadas y varios heridos deja atentado contra comunidad indígena de Tacueyó"](https://archivo.contagioradio.com/dos-personas-asesinadas-y-varios-heridos-deja-atentado-contra-comunidad-indigena-de-tacueyo/))

### **Edison José Maestre Nieves, integrante del pueblo Wiwa en Cesar** 

La Corporación Mesa Departamental de Derechos Humanos y Territorios del Cesar denunció que hombres armados atentaron contra la vida de **Edison José Maestre Nieves, integrante del pueblo Wiwa en la Sierra Nevada de Santa Marta**. Además del cuerpo sin vida de Maestre, la Corporación aseguró que fue encontrado un panfleto firmado por las Águilas Negras, en el que se amenaza la vida de autoridades y personas del pueblo Wiwa. (Le puede interesar: ["Comunidades del Cauca responsabilizan al Ejército del asesinato de Flower Jair Trompeta"](https://archivo.contagioradio.com/comunidades-del-cauca-responsabilizan-al-ejercito-del-asesinato-de-flower-jair-trompeta/))

### **Territorios de los pueblos indígenas en riesgo** 

Integrantes de la **Asociación de Cabildos Indígenas del Norte de Cauca (ACIN)** informaron que hombres armados atacaron a cuatro personas en la vereda Santa Elena, del Municipio de Corinto en Cauca. Los hombres hacían parte de un equipo de topógrafos que hacían trabajo de terreno, y mientras adelantaban sus labores fueron agredidos, y hurtadas sus herramientas. Posteriormente se supo que otra persona fue víctima también de hombres armados cerca del lugar, pero aún no está confirmado que los crímenes guarden relación.

Aunque las personas asesinadas no hacen parte de los pueblos indígenas, desde la Organización Nacional Indígena de Colombia (ONIC) afirmaron que sus muertes obedecen a una violencia que se instaló en el territorio, y requiere un enfoque distinto al militar. En ese sentido, señalaron que se requieren medidas integrales para atender las diferentes vulneraciones a los derechos humanos que ocurren en los territorios indígenas. (Le puede interesar: ["Militarizar más al Cauca es una propuesta «desatinada y arrogante»: ACIN"](https://archivo.contagioradio.com/militarizar-mas-al-cauca-es-una-propuesta-desatinada-y-arrogante-acin/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
