Title: Judicial
Date: 2014-11-25 15:44
Author: AdminContagio
Slug: judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/HERIBERTO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

JUDICIAL
--------

[](https://archivo.contagioradio.com/piden-nulidad-en-eleccion-del-fiscal-nestor-martinez-por-ocultar-informacion/)  

###### [Las razones para pedir nulidad en elección del Fiscal Néstor Martínez](https://archivo.contagioradio.com/piden-nulidad-en-eleccion-del-fiscal-nestor-martinez-por-ocultar-informacion/)

[<time datetime="2019-01-14T16:19:00+00:00" title="2019-01-14T16:19:00+00:00">enero 14, 2019</time>](https://archivo.contagioradio.com/2019/01/14/)Organizaciones defensoras de derechos humanos entablaron una demanda solicitando la nulidad de la elección del Fiscal Martínez por haber ocultado información cuando fue elegido por la Corte Suprema de Justicia[Leer más](https://archivo.contagioradio.com/piden-nulidad-en-eleccion-del-fiscal-nestor-martinez-por-ocultar-informacion/)  
[](https://archivo.contagioradio.com/allanamiento-juan-carlos-montes/)  

###### [Denuncian allanamiento a residencia de Juan Carlos Montes](https://archivo.contagioradio.com/allanamiento-juan-carlos-montes/)

[<time datetime="2018-12-26T12:50:37+00:00" title="2018-12-26T12:50:37+00:00">diciembre 26, 2018</time>](https://archivo.contagioradio.com/2018/12/26/)Foto: Noticias Uno 26 Dic 2018 Este miércoles trascendió que la residencia de Juan Carlos Montes, quien aparece en el video en que Gustavo Petro recibía dinero, fue objeto de allanamiento por parte del CTI de la[Leer más](https://archivo.contagioradio.com/allanamiento-juan-carlos-montes/)  
[](https://archivo.contagioradio.com/debate-del-siglo/)  

###### [El debate del siglo: ¿Qué piensa el Gobierno de la renuncia del fiscal Nestor Humberto Martínez?](https://archivo.contagioradio.com/debate-del-siglo/)

[<time datetime="2018-11-27T17:13:38+00:00" title="2018-11-27T17:13:38+00:00">noviembre 27, 2018</time>](https://archivo.contagioradio.com/2018/11/27/)En el que se ha calificado como el “debate del siglo”, senadores de la oposición pedirán la renuncia del fiscal Nestor Humberto Martínez[Leer más](https://archivo.contagioradio.com/debate-del-siglo/)  
[](https://archivo.contagioradio.com/que-renuncie-el-fiscal/)  

###### [¡Que renuncie el Fiscal!](https://archivo.contagioradio.com/que-renuncie-el-fiscal/)

[<time datetime="2018-11-13T18:30:35+00:00" title="2018-11-13T18:30:35+00:00">noviembre 13, 2018</time>](https://archivo.contagioradio.com/2018/11/13/)Tras la revelación de audios que prueban el conocimiento de Martínez sobre la corrupción en Odebrecht antes de ser Fiscal, distinta voces piden su renuncia[Leer más](https://archivo.contagioradio.com/que-renuncie-el-fiscal/)
