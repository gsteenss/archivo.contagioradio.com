Title: Santrich podría salir de la cárcel a mediados de Febrero: Defensa
Date: 2019-01-29 14:14
Author: AdminContagio
Category: Nacional, Política
Tags: FARC, JEP, Jesús Santrich, Nestor Humberto Martínez
Slug: jesus-santrich-podria-salir-de-la-carcel-a-mediados-de-febrero-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Jesus-Santrich-en-entrevista-contagio-radio-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Ene 2019] 

Luego de que Estados Unidos no enviara las pruebas en contra de Jesús Santrich a la Jurisdicción Especial para la Paz, Gustavo Gallardo, abogado defensor, señaló que el siguiente paso es esperar un plazo que otorga el Tribunal para que se construyan los alegatos de conclusión y la sección de revisión falle en derecho. Es decir que no se de la garantía de extradición y se proceda con la liberación del integrante del partido FARC.

"Desde el 9 de abril anunciamos que no iba haber la posibilidad de que el Fiscal General de la Nación o la DEA, **pudieran plantear o dar pruebas sobre la culpabilidad de Santrich en algún delito**", señaló Gallardo y agregó que creería que a mediados del mes de febrero tendría que producirse una decisión desde el Tribunal sobre Santrich.

### **¿Qué hay detrás de la captura de Santrich?** 

El abogado defensor de Jesús Santrich, aseguró que en este proceso debe caer una gran responsabilidad sobre Néstor Humberto Martínez, fiscal general de la Nación, porque **"violando abiertamente la Constitución Política Colombiana, procedió con la captura,** sobre la base de un pedido de extradición sin tener la competencia para ello".

Asimismo señaló que el caso sirvió para revelar las vulneraciones a los derechos humanos de las personas que son extraditadas, porque la Corte Suprema Justicia solo hacía un "ejercicio notarial".

De igual manera señaló que las pruebas que presentó públicamente la Fiscalía en contra de Santrich **"no han tenido un avance procesal"** y tampoco tenían el carácter de evidencia, porque eran unos audios que no tenían el recorrido legal lícito para considerarse material probatorio.  (Le puede interesar:["Se venció el plazo, EEUU no aportó prueba en el caso Santrich"](https://archivo.contagioradio.com/se-vencio-el-plazo-y-eeuu-no-aporto-pruebas-en-caso-santrich/))

Frente a la curul que Santrich tenía en el Congreso de la República, Gallardo afirmó que están a la espera del fallo del  Consejo de Estado, sobre la pérdida de investidura, y una vez el integrante del partido FARC salga de la cárcel, si se protegen sus derechos de participación política, retomaría sus labores en ese escenario político.

<iframe id="audio_31934922" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31934922_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
