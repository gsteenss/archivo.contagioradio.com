Title: #AdopciónIgualitaria no afecta psicológicamente a niños y niñas
Date: 2015-01-21 21:45
Author: CtgAdm
Category: DDHH, LGBTI
Tags: Adopción igualitaria, Derechos, LGBTI
Slug: adopcionigualitaria-no-afecta-psicologicamente-a-ninos-y-ninas
Status: published

#### Foto por: Contagio Radio 

Desde el 19 de enero **la Corte Constitucional debate la posibilidad de que las parejas del mismo sexo puedan adoptar.** Esta discusión la abrió un ciudadano que afirma que la norma ya estipulada es discriminatoria incluso desde el propio lenguaje.

Hace pocos días, el presidente de la Conferencia Episcopal, monseñor Luis Augusto Castro,  afirmó que la iglesia católica apoyaría la adopción en caso de que el hijo o hija perteneciera a una de las personas que componen la pareja. Sin embargo, **este 21 de enero se confirmó el rechazo total por parte de la iglesia, a la posibilidad de que una pareja homosexual adopte**. Esto con el argumento de que es una medida de protección a los menores.

Por su parte, la organización Colombia Diversa, defensora de los derechos de la comunidad LGBTI, afirma, luego de varias investigaciones internacionales, que **no se han generado efectos psicológicos nocivos en niños y niñas adoptados por una pareja gay.**

*"La evidencia científica ha hecho reportes sobre el seguimiento de hijos de parejas del mismo sexo desde hace más de veinte años, incluso de parejas homosexuales que criaron niños y niñas antes de que fuera legal la adopción en varias jurisdicciones. Este es el caso de la cohorte de niños y niñas del estudio de Golombok, que empezó a seguirse desde la década de 1970 en el Reino Unido. A estos estudios se suma evidencia de Estados Unidos y Canadá, conducidos con los más altos estándares de rigurosidad científica. Dichos estudios no reportan ninguna diferencia en el desarrollo psicosocial de niños y niñas criados por parejas homosexuales”.*

Por otro lado, **el defensor del pueblo, Jorge Armando Otálora, en entrevista con Colombia Diversa, afirmó su posición de apoyo a que las parejas del mismo sexo puedan adoptar:** “Yo he sido muy claro con eso, yo tengo principios católicos, fui criado en una familia católica, pero tengo claro que por encima de mis principios católicos están los derechos humanos y desde esa óptica siempre he sido partidario de la adopción gay y así han ido mis conceptos que he emitido a la Corte Constitucional”.

Cada vez se unen más voces a favor de la adopción igualitaria. Cabe aclarar que **la Corte Constitucional deberá emitir una decisión el próximo 6 de febrero**.
