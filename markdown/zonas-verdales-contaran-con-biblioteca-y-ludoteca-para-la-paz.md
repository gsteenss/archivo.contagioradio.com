Title: Zonas veredales contarán con bibliotecas y ludotecas para la paz
Date: 2017-02-22 12:28
Category: Nacional, Paz
Tags: Biblioteca por la paz, Zonas Veredales Transitorias de Normalización
Slug: zonas-verdales-contaran-con-biblioteca-y-ludoteca-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/bibliotecas-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [22 Feb 2017] 

Bajo la consiga “la paz se construye a muchas manos” organizaciones sociales como OXFAM, Javerianos por la Paz y COMPAZ lanzaron una iniciativa que pretende **recolectar libros y juegos  para construir bibliotecas en las diferentes zonas veredales**, y de esta forma contribuir desde la cultura al tránsito de las y los guerrilleros a la vida civil.

Además, ya que en los puntos de concentración hay población infantil y madres gestantes, se pretende que las bibliotecas tengan una sección de ludoteca **para que tanto niños como mamás puedan también disfrutar de este lugar.**

**Las bibliotecas serán  construidas en conjunto entre ciudadanos y guerrilleros en cada una de las zonas, al igual que manuales de uso** que establecerán las normas para usar el material pedagógico y didáctico. La primera biblioteca que se construirá será en la zona veredal de Iconozo y hasta el momento  se han recolectado aproximadamente 600 libros. Le puede interesar:["Venga esa mano por la paz"](https://archivo.contagioradio.com/venga-esa-mano-por-la-paz/)

Para Juan David Ojeda, integrante de COMPAZ **“lenguajes como el artístico y el cultural, permiten otros tipos de encuentro y de tejido social”** y esta es una de las formas que pueden tener las personas para entre todos ayudar en la construcción de paz. Le puede interesar:["Inicia campaña de donaciones para guerrilleros de las zonas veredales"](https://archivo.contagioradio.com/inicia-campana-de-donaciones-para-guerrilleros-de-las-zonas-veredales/)

Los libros llevarán una dedicatoria de la persona que lo entrega, con mensajes que busquen tejer lazos de solidaridad hacia los integrantes de la guerrilla de las FARC-EP y entre la literatura más solicitada se encuentra la latinoamericana, novelas, cuentos cómo también libros técnicos sobre agricultura y construcción. **Los libros se pueden dejar en la sede de OXFAM Colombia, en la calle 36 \#16-20  o en Redepaz en la carrera 10 \#19 -65, oficina 905.**

<iframe id="audio_17168711" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17168711_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
