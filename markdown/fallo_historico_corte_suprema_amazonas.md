Title: El precedente que marca el fallo sobre la Amazonía colombiana
Date: 2018-04-06 13:24
Category: Ambiente, Voces de la Tierra
Tags: amazonas, Corte Constitucional, Corte Suprema de Justicia, páramos, Río Atrato
Slug: fallo_historico_corte_suprema_amazonas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Rio-amazonas-e1482197399239.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Viajes Mollar] 

###### [6 Abr 2018] 

El fallo histórico sin precedentes por parte de la Corte Suprema de Justicia, evidencia que la naturaleza debe ser reconocida como un auténtico sujeto de derechos. Así lo ven organizaciones ambientalistas frente a la decisión del alto tribunal sobre la Amazonía colombiana mediante la cual, se reconoce a la naturaleza, en este caso a la selva amazónica, como sujeto de protección por parte del Estado.

"Estamos muy contentos y extendemos toda nuestra felicitación a Dejusticia que acompañó a los **25 niños, niñas y jóvenes que abogaron por la protección de la Amazonía y que viven en los lugares del país más afectados por el cambio climático",** dice Juana Hofman, coordinadora de la Red por la Justicia Ambiental, quien agrega que de esa manera las Cortes están reconociendo "eso" que desde hace años vienen anunciando por los ambientalistas.

### **Primer fallo frente al cambio climático** 

De acuerdo con Hofman, se trata de una decisión que evidencia avances importantes en materia de protección ambiental. En este caso, **fueron las generaciones futuras que se enfrentarán a los efectos del cambio climático  las que alegaron  la equidad intergeneracional**, y para ello pidieron una serie de acciones a través de las cuales se proteja debidamente el pulmón del mundo. Hoy, esas medidas exigidas por los jóvenes son desarrolladas de manera juiciosa en esa sentencia de la Corte Suprema de Justicia, según analizan los ambientalistas. (Le puede interesar: [El 12% de la Amazonía colombiana ha sido transformada)](https://archivo.contagioradio.com/12-la-amazonia-colombiana-ha-transformada/)

La sentencia de la Corte Suprema de Justicia es la primera tutela contra el cambio climático en  América Latina. Allí, el magistrado Luis Hernando Tolosa, trae a colación lo mencionado por la Corte Constitucional en su sentencia del pasado mes de diciembre sobre al Río Atrato. Cabe recordar que allí se reconocía a la naturaleza como titular de la protección, conservación, mantenimiento y restauración a cargo del Estado y las entidades territoriales que son los encargados de la planificación y conservación de la naturaleza.

### **Un precedente para acciones posteriores a favor del ambiente** 

"La justicia con la naturaleza debe ser aplicada más allá de los escenarios humanos", expresa la integrante de la Red por la Justicia Ambiental. En ese sentido, en el reciente fallo se recapitula lo dicho anteriormente por la Corte Constitucional y lo puntualiza en acciones concretas. (Le puede interesar: [400 organizaciones protestan por la Amazonía)](https://archivo.contagioradio.com/movilizacion_amazonia/)

Entre las medidas estipuladas por la Corte Suprema, **los municipios tendrán 4 meses para incluir en sus instrumentos de ordenamiento territorial, un plan de acción contra la deforestación** y medidas de restauración frente al cambio climático. "Que la Corte obligue a los municipios incluir este tipo de acciones y les de un tiempo, es una acción concreta y puntual para materializar los derechos de la naturaleza", dice Hofman.

También se dan **4 meses al Ministerio de Ambiente y al Sistema Nacional Ambiental, para que se formule un plan de acción ante la deforestación.** De esta forma, la Corte está evidenciando que ese fenómeno está aumentando, que el cambio climático es un hecho y que debe existir un valor ético de solidaridad frente a la naturaleza. "Hay un criterio ecocéntrico y biocéntrico", explica la ambientalista.

### **Será clave la veeduría de todo el país** 

Además de las acciones anteriores, en un plazo de **5 meses se debe concretar el pacto intergeneracional por la vida del Amazonas.** Un hecho sin antecedentes, frente a lo cual, Juana Hofman hace un llamado a las organizaciones ambientalistas y a la ciudadanía para que se haga un seguimiento juicioso a ese pacto, ya que se trata de una oportunidad única que abre la decisión sobre esa tutela para poder ejercer acciones concretas frente a la deforestación.

"Teniendo en cuenta que la conservación de la Amazonía es una obligación nacional y global  no podemos olvidarnos del tema. Se debe hacer un seguimiento estricto a las órdenes de la Corte: activismo digital, verificando con derechos de petición sobre lo que se está haciendo desde el gobierno, y todas las acciones necesarias", recomienda Hofman. (Le puede interesar:[Blindaje Ramsar para los Lagos de Tarapoto)](https://archivo.contagioradio.com/lagos_toporapa_ramsar_amazonia/)

Finalmente, dicho fallo podría ser un primer paso para que no solo la Amazonía y el Río Atrato sean sujetos especiales de derecho, sino que también lo sean los páramos del país. **"Estamos en vilo con la protección de los páramos, la delimitación no puede ser la única herramienta**. Estos ecosistemas deben estar en el radar para ser objetos de estas acciones, de acuerdo a los títulos  mineros que hay sobre ellos y en zonas de amortiguamiento. Ojalá haya un giro en torno a la protección ambiental y al ecocentrismo", concluye la ambientalista.

<iframe id="audio_25157359" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25157359_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
