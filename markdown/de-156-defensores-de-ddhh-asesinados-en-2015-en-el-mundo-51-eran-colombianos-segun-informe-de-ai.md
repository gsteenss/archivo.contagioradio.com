Title: De 156 defensores de DDHH asesinados en 2015 en el mundo 51 eran colombianos según informe de AI
Date: 2016-02-25 11:57
Category: DDHH, Otra Mirada
Tags: Amnistía Internacional, Derechos Humanos, Jurisdicción Especial de Paz, Paramilitarismo
Slug: de-156-defensores-de-ddhh-asesinados-en-2015-en-el-mundo-51-eran-colombianos-segun-informe-de-ai
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Amnistia_internacional_contagioradio.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Amnistía Internacional] 

###### [25 Feb 2016] 

El informe "Tus derechos peligran" de 472 páginas la organización de DDHH, Amnistía Internacional presenta un recuento detallado de la situación de los derechos alrededor del mundo en que resaltan que el “**2015 fue testigo de un asalto generalizado contra las libertades básicas de los seres humanos**” y que “**muchos gobiernos infringieron abiertamente el derecho internacional y socavaron intencionadamente** las instituciones destinadas a proteger los derechos de las personas”.

“No sólo peligran nuestros derechos, sino también las leyes y el sistema que los protegen. Más de 70 años de duro esfuerzo y avances en materia de derechos humanos están en la cuerda floja”, ha dicho Salil Shetty, vocera de la organización.

Según el informe en al menos 19 países se perpetraron crímenes de guerra u otras violaciones de las “leyes de la guerra”, en 122 países o más se infligieron torturas u otros malos tratos, en al menos 30 países obligaron ilegalmente a personas refugiadas a volver a países donde correrían peligro, esto en referencia a la política migratoria de países como los que hacen parte de la Unión Europea.

También se afirma que al menos en 113 países se restringió arbitrariamente la libertad de expresión y de prensa, 61 países se encarceló a presos y presas de conciencia, personas que no hacían más que ejercer sus derechos y libertades y 156 defensores y defensoras de los derechos humanos murieron en detención o fueron víctimas de homicidio, siendo Colombia el país en el que más asesinatos se registraron con una cifra de 51 defensores asesinados.

### **El Caso Colombia** 

En el informe se dedican 4 páginas al caso colombiano y se hace un recuento de la situación en torno al proceso de **[conversaciones de paz](https://archivo.contagioradio.com/?s=jurisdicci%C3%B3n+especial+de+paz)**, en el que no se mencionan reparos concretos a lo acordado hasta el momento, como es el caso del acuerdo sobre el Sistema Integral de Justicia, Verdad, Reparación y garantías de no repetición.

En materia de impunidad, Aimnistía Internacional menciona que hay algunos avances en las investigaciones por las operaciones ilegales del DAS, sin embargo, siguen siendo insuficientes. En cuanto a la restitución de tierras AI señala que “al concluir 2015, **sólo 58.500 hectáreas de tierra reclamadas por campesinos, un territorio indígena de 50.000 hectáreas**, y otro de afrodescendientes, de 71.000” han sido objeto de sentencia de devolución, lo que califican como insuficiente al tratarse **de más de 6 millones de hectáreas arrebatadas.**

Uno de los elementos más preocupantes que señala el informe es la situación de defensores de DDHH dado que en el informe general la organización señala que son 156 defensores asesinados en el mundo y para el caso colombiano le da credibilidad al informe de Somos Defensores que indica que en el año 2015 fueron asesinados 51 defensores, de lo que se concluye que del total de asesinatos una tercera parte se dio en Colombia.

Los procesos judiciales contra sectores sociales, la violencia contra las mujeres, la ayuda internacional, el **[paramilitarismo](https://archivo.contagioradio.com/?s=paramilitarismo)** y las **[violaciones de DDHH por parte de las FFMM](https://archivo.contagioradio.com/?s=falsos+positivos)** así como la ampliación del **[Fuero Penal Militar](https://archivo.contagioradio.com/?s=fuero+penal+militar)** fueron parte de los elementos analizados en el informe que encuentran a continuación.
