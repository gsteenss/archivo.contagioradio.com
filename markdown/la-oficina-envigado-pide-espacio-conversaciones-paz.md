Title: 'La Oficina de Envigado' pide espacio para conversaciones de paz
Date: 2016-11-11 16:05
Category: Nacional, Paz
Tags: acuerdos de paz, AUC, Neoparamilitarismo
Slug: la-oficina-envigado-pide-espacio-conversaciones-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [11 Nov de 2016] 

En un comunicado, la dirección colegiada de los grupos armados agremiados de La Oficina de Envigado citando los artículos 22 y 23 de la Constitución de 1991, **solicitan ser tenidos en cuenta para el inicio de una fase exploratoria formal de un proceso de paz con el Gobierno Nacional.**

En el documento aseguran que **respaldan las conversaciones con el ELN y los acordado en La Habana con las FARC**, manifiestan que es momento propicio para participar directamente en la finalización del conflicto armado colombiano. **Piden que Piedad Córdoba y la Iglesia Católica acompañen dicho proceso.**

Por otra parte, la organización argumenta que es necesario lograr una paz urbana **"como complemento necesario al esfuerzo por la paz rural",** y aprovechan para instar a las Autodefensas Gaitanistas de Colombia **"a sumarse a esta petición".**

En marzo de este año, este grupo ya había hecho pública una propuesta, sin embargo, hasta el momento **ni autoridades departamentales ni el Gobierno Nacional se han pronunciado sobre la viabilidad de estas propuestas.**

**Acá puede leer el comunicado completo:**

[Comunicado Oficina Envigado](https://www.scribd.com/document/330775939/Comunicado-Oficina-Envigado#from_embed "View Comunicado Oficina Envigado on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_72706" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/330775939/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-QgHzmT3JGZBu6OxHTyzM&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
