Title: Arhuacos exigen que la Sierra Nevada sea zona libre de minería
Date: 2017-11-14 13:39
Category: DDHH, Nacional
Tags: arhuacos, derechos de los indígenas, Gobierno Nacional, Indígenas arhuacos, Minga por la vida, Movilización Indígena
Slug: arhuacos-exigen-que-la-sierra-nevada-sea-zona-libre-de-mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Indigenas-en-Ciudad-Perdida.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Encolombia.com] 

###### [14 Nov 2017] 

Los indígenas arhuacos que hacen parte de la Confederación Indígena Tayrona, se reunieron el 10 de noviembre con los delegados del Gobierno Nacional para establecer la **ruta metodológica para la instalación de la mesa de negociación en Valledupar, Cesar**. Allí, los indígenas le harán saber al Gobierno sus necesidades como comunidades ancestrales y buscarán llegar a acuerdos para defender la Sierra Nevada.

De acuerdo con el comunicado realizado por la Confederación Indígena Tayrona, las autoridades del pueblo Arhuaco le manifestaron a los delegados del Ministerio del Interior y MAP-OEA la necesidad de abordar en la mesa de negociación las **inquietudes expresas del pueblo Tayrona en el marco de la Minga por la Vida** y que están incluidas en el capítulo étnico de los Acuerdos de Paz enfocados en la Sierra Nevada.

Además, esperan que el Gobierno **reconozca los esfuerzos de más de 400 representantes del pueblo Arhuaco** quienes han manifestado las afectaciones y vulneraciones a los derechos humanos que ponen en riesgo la identidad cultural y territorial de estos pueblos ancestrales. (Le puede interesar: ["Más de 150 arhuacos en hacinamiento tras desalojo en Pueblo Bello, Cesar"](https://archivo.contagioradio.com/mas-de-150-arhuacos-en-hacinamiento-tras-desalojo-en-pueblo-bello/))

### **Exigencias del pueblo Arhuaco a las autoridades nacionales** 

Según la autoridad indígena Leonor Zalabata, en la reunión se precisó la fecha de instalación de la comisión que estará a cargo de tratar la agenda del pueblo Arhuaco que será el 15 de noviembre. Dijo que **las necesidades de los indígenas hacen referencia a la protección de la Sierra Nevada** y la supervivencia de los pueblos que se encuentran en peligro de desaparecer.

Por esto, le van a exigir al Gobierno Nacional que la Sierra  “**debe estar libre de minería** y el Gobierno debe asumir el respeto por la madre tierra que es sagrada”. Además, afirmó que existe un derecho que tienen los indígenas sobre la ancestralidad de ese territorio por lo que la minería significa un riesgo.

Indicó que desde los años 70, se ha reconocido como parte de un conceso social, hasta donde llega el territorio ancestral y que además **la Sierra Nevada ha sido declarada como territorio sagrado por la Corte Constitucional**. Por esto, consideran que el Gobierno Nacional debe cumplir estas normas que reconocen la diversidad étnica y cultural. (Le puede interesar: ["Para el 2040 la Sierra dejará de ser Nevada"](https://archivo.contagioradio.com/nevados-de-la-sierra-nevada-de-santa-marta-desapareceria-en-el-2040/))

Reafirmó que **no ha habido voluntad política para definir el territorio ancestral** que ya ha sido ordenado por la Corte Constitucional. Además, dijo que la ley de origen y la ley arhuaca de protección territorial le da la potestad a los indígenas de crear una visión de política pública para defender sus derechos.

### **Gobierno Nacional ha incumplido de manera sistemática los acuerdos con el pueblo Arhuaco** 

Zalabata manifestó que para ellos **es importante que se respeten los propósitos y la condición de indígenas** de los diferentes pueblos. Por esto, “le vamos a decir al Estado que no nos puede seguir manejando nuestros derechos, necesitamos que se reivindique nuestra condición de pueblos ancestral que tienen derechos sobre el territorio”.

Igualmente, recordó que los recursos que provienen de la Sierra Nevada, **no son solamente para los pueblos indígenas**, sino que hacen parte fundamental para la vida de las personas y los seres vivos del país en general. Dijo que no van a negociar el territorio en la medida que son los dueños reconocidos por las normas y la ley. Además, indicó que el Gobierno Nacional les ha dado “migajas para coptar a los indígenas e involucrarlos en la vida nacional y esto es algo que no vamos a permitir”.

Finalmente, recordó **los derechos fundamentales sobre el territorio de los indígenas no van a ser negociados** y fue enfática en manifestar que el Gobierno Nacional debe ser claro en las propuestas que “deben ser cumplidas”. Por esto, han solicitado que haya acompañamiento internacional de la OEA y las Naciones Unidas para realizar seguimiento y veeduría de los acuerdos que se instalen en la mesa de negociación.

<iframe id="audio_22070176" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22070176_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
