Title: En el mes del teatro llega la fiesta titiritera a Bogotá
Date: 2018-03-22 15:00
Category: eventos
Tags: Bogotá, teatro, Títeres
Slug: titiritera-teatro-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Medieval-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa 

###### 22 Mar 2018 

En el mes del teatro llega la fiesta de títeres a Bogotá y para celebrarlo las seis mejores compañías de titiriteras de la ciudad presentaran en 12 funciones obras donde los principales protagonistas son las marionetas.

Del 23 de marzo al primero de abril en cinco salas de teatro de la capital podrá disfrutar de títeres de guante, varilla, animación a la vista, marionetas, stop motion, cámara negra y animación de objetos entre otras técnicas.

Los asistentes podrán encontrar historias como “La Abuela Chifloreta y los tres cerditos”, de la compañía Baúl de La Fantasía. Una historia que muestra las aventuras de estos personajes en el que se entremezclan algunos de los cuentos clásicos de siempre (Le puede interesar: [FESTA 2018, una fiesta escénica en homenaje al teatro colombiano](https://archivo.contagioradio.com/festa-teatro-bogota/))

Inspirada en el cuento de Jairo Aníbal "Niño El Gato" y combinando el stop motion en cámara negra y la animación de objetos se presenta “Gatos en Fuga”, una obra en la que una tigresa es arrancada del seno de la selva y confinada a un circo de la ciudad, donde se plantea el anhelado regreso contado con la ayuda de otros animales.

Por otro lado la compañía Escénica Vórtice presenta la obra “Donde Habita el Tiempo”. La cual cuenta como los hombres grises, ladrones del tiempo llegan para deshumanizar al mundo, pero Momo un ser capaz de viajar al interior de sí misma y descubrir el poder que tiene. Nos enseña que el poder que ella posee lo tenemos todos.

Con dirección de Héctor Loboguerrero llega la obra “El circo Medieval”, donde cuatro cómicos ambulantes arriban al escenario con una comparsa llena de música en vivo y color, que consigo traen un payaso cantante y bailarín, el gusano contorsionista y los hombres más fuertes del mundo, entre otros personajes.

De igual modo, estarán las obras “Pinsiete” del Teatro Comunidad y “El circo de La Ilusión” de la compañía Paciencia Guayaba. La primera nos presenta una muestra de  música popular y literaria cantada con títeres cómicos y teatro festivo. Por último, la segunda obra nos da a conocer como se recrea el mundo de los actores y las marionetas. La pista de este circo se llenara de ternura y emoción, dejando los mecanismos al desnudo del espectador.
