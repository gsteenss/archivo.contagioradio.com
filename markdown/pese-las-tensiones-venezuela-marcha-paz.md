Title: Pese a las tensiones Venezuela marcha en paz
Date: 2017-04-19 13:06
Category: El mundo, Movilización
Slug: pese-las-tensiones-venezuela-marcha-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/venezuela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: telesur] 

###### [19 Abr 2017]

Tanto opositores como simpatizantes del gobierno de Nicolás Maduro se movilizan hoy por las calles de Caracas con multitudinarias convocatorias. Sin embargo, algunas fuerzas de oposición se mantienen en algunos puntos de caracas. A pesar de ello algunos sectores de la oposición están intentando cambiar las rutas acordadas lo que provocaría un encuentro directo que podría resultar en confrontaciones dada la creciente polarización.

En hechos recientes durante estas semanas ya se han presentado fuertes incidentes afectando por lo menos 3 instituciones estatales según lo informó Pablo Kunich de Alba Tv, lo que demostraría que aunque si hay movilización pacífica por parte de la oposición, también hay actos violentos que no obedecen a las necesidades de movilización pacífica.

Por su parte el presidente Nicolás Maduro activó el llamado “Plan Zamora”, mecanismo de defensa de las FFMM y de Policía para intentar evitar un Golpe de Estado. Según informó durante la última semana se detuvieron algunos efectivos militares como civiles con armamento y entrenamiento que se estaban alojando en algunos hoteles en la capital del país. Adicionalmente denunció que hay militares involucrados en el plan que están huyendo hacia Colombia.

### **Las marchas de este 19 de Abril** 

Como es habitual en una jornada de movilizaciones, tanto de oposición como simpatizantes del gobierno, cada una de las marchas estableció una ruta que fue autorizada por la policía. La movilización de oposición tenía la ruta planificada para movilizarse hacia el Este de la ciudad, sin embargo algunos manifestantes intentan dirigirse hacia el centro de la ciudad rompiendo los mecanismos de seguridad en que se hace contención.

La movilización por parte de quienes respaldan al gobierno se concentran en la avenida Bolívar y en Plaza Venezuela sin ningún tipo de incidente hasta el momento. Sin embargo muchos manifestantes afirman que este tipo de movilizaciones se han mantenido sin visibilidad por parte de los medios internacionales.

Según el comunicador Pablo Kunich, la movilización de hoy demuestra que la gente está respaldando al gobierno y sobre todo los avances en derechos que se han conseguido en los últimos años como la misión vivienda que ha entregado cerca de 1 millón de casas.

<iframe id="audio_18233752" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18233752_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
