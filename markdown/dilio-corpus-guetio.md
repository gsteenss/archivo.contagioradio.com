Title: Asesinan a Dilio Corpus Guetio, miembro de la Guardia campesina en Cauca
Date: 2019-01-29 15:43
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Cauca, Fensuagro, guardia campesina, Lider social
Slug: dilio-corpus-guetio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/WhatsApp-Image-2019-01-29-at-3.05.33-PM-770x400.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Ene 2019] 

Este martes fue asesinado **Dilio Corpus Guetio, integrante de Asocordillera, organización filial de la Federación Nacional Sindical Unitaria Agropecuaria (FENSUAGRO)** y miembro de la guardia campesina. Según las primeras informaciones, el hombre fue atacado con arma de fuego por extraños que se movilizaban en un vehículo, tras salir de su casa en la **vereda Santa Bárbara, del municipio de Suarez, Cauca**.

Según Deivin Hurtado, integrante de la mesa técnica de FENSUAGRO en Cauca, cerca de las 6 am Corpus Guetia salió de su casa a su finca en moto para trabajar en la tierra, en ese recorrido, un vehículo lo alcanzó, golpeándolo para que pierda el equilibrio y propinandole 5 impactos de bala. (Le puede interesar: ["Corinto bajo fuego cruzado de EPL y disidentes de las FARC"](https://archivo.contagioradio.com/conrito-cauca-bajo-el-fuego-cruzado-por-enfrentamientos-entre-disi/))

Corpus Guetio **desarrollaba su labor como miembro de la guardia, haciendo protección territorial y de las comunidades**, mediante esa herramienta de autoprotección que son las guardias campesinas. De acuerdo a la información que se tiene hasta el momento, contra el líder no había ninguna amenaza, pero los miembros de FENSUAGRO intentan recopilar la información necesaria para determinar los posibles móviles del asesinato.

### **Cauca: El departamento más afectado por el asesinato de líderes sociales** 

Como recuerda Hurtado, Cauca ha sido uno de los departamentos más golpeados por el conflicto armado, y después de la firma del Acuerdo de Paz en 2016, es la región con mayor número de asesinatos y amenazas contra defensores de derechos humanos. En ese departamento, diferentes grupos hacen presencia territorial, y se enfrentan, por lo que es difícil determinar sus estructuras; esta situación es un riesgo inminente para comunidades, pues **en tan solo 29 días de 2019 ya ha sido asesinados 4 líderes sociales en el departamento**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
