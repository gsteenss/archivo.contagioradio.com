Title: Contagios de Covid 19 sobrepasan los 3500 en cárceles colombianas
Date: 2020-08-27 10:13
Author: AdminContagio
Category: Expreso Libertad, Programas
Tags: #Cárcel, #covid19, #Expresolibertad
Slug: contagios-de-covid-19-sobrepasan-los-3500-en-carceles-colombianas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/carcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Desde el pasado 24 de agosto, familiares de personas privadas de la libertad se encuentran adelantando un plantón a la afueras del INPEC, en Bogotá, rechazando el aumento de contagios de Covid 19, **que ascienden a más de 3.500 casos**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, y pese a la difícil situación, el presidente Iván Duque, sigue sin tomar acciones reales, que den garantías a la vida de estas personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, el Movimiento Carcelario se encuentra promoviendo un pliego de exigencias, con medidas urgentes que tendría que tomar el gobierno para evitar, lo que ellos denominan, una masacre en los centros de reclusión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del Expreso Libertad estuvimos hablando con Esperanza Bustamante, integrante del Movimiento Corazón Humano, Gloria Silva, integrante del Equipo Jurídico Pueblos y el senador Iván Cepeda, sobre la situación de la población reclusa y la ausencia de acciones por parte del Estado colombiano, para proteger a las y los reclusos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la fecha, dentro de las cárceles se han registrado [más de 24 muertes por contagios del virus 2 de ellos en la Cárcel](https://archivo.contagioradio.com/el-sistema-penitenciario-de-colombia-una-violacion-a-dd-hh/), ocho de ellos en la Cárcel La Picota en Bogotá, 19 en valle del Cauca y tres en Barranquilla.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_55783497" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_55783497_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
