Title: ACA presenta en Bogotá el estatuto del actor
Date: 2015-06-22 12:55
Category: eventos, Nacional
Tags: ACA, actores, estatuto del actor, Fanny Mikey
Slug: aca-presenta-en-bogota-el-estatuto-del-actor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/ACA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [22 jun 2015] 

La Asociación Colombiana de actores y actrices ACA, organización que trabaja en defensa de los derechos de más del mil agremiados en el país, socializará este martes 23 de Junio el proyecto legislativo por medio del cual se crearía la Ley del Actor Colombiano, figura que establece un marco jurídico para el ejercicio de la profesión en el territorio nacional.

La presentación, que tendrá lugar en el Teatro Nacional Fanny Mikey, contará con la presencia de congresistas de distintas colectividades, gestores culturales, actores y actrices, quienes han participado en el diseño de la propuesta que será radicada el próximo 30 de Julio en el Congreso de la República.

El objetivo del encuentro será exponer cada uno de los puntos de la propuesta, así como la justificación de su existencia y su relevancia para garantizar condiciones dignas de trabajo para los profesionales que se desempeñan en escenarios como el teatro, la televisión y el cine en Colombia.

Durante el evento se abrirá un espacio para hacer un balance de las actividades y logros del sindicato al cumplirse un año de existencia, intervenciones que serán escuchadas por los directivos de las diferentes plataformas laborales de los actores: Canales de Televisión, Salas de Teatro, Entidades Culturales, Productoras de Cine y Contenidos Audiovisuales en Internet.

De acuerdo con la agremiación sindical, el trabajo del actor en la actualidad presenta problemas de oferta académica, de tipo laboral como artista, trabajador de la cultura y de tipo profesional como activo principal en la lucrativa industria audiovisual, sumado a la falta de vinculación en los planes de seguridad social que ofrece el Estado, condiciones laborales y remuneraciones justas para todos y todas.

La presentación la podrá seguir en vivo a través de nuestro portal [www.contagioradio.com](https://archivo.contagioradio.com/envivo.html) desde las 9:00 a.m.

**Fecha:** martes 23 de junio de 2015.  
**Hora:** 8:45 a. m.  
**Lugar:** Teatro Nacional Fanny Mikey. Calle 71 \# 10 – 25.
