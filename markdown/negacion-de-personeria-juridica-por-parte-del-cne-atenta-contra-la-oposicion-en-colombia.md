Title: Negar personería jurídica a Colombia Humana es un mal mensaje para la oposición
Date: 2018-08-23 17:27
Category: Nacional, Política
Tags: CNE, Colombia Humana, Petro
Slug: negacion-de-personeria-juridica-por-parte-del-cne-atenta-contra-la-oposicion-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/5b2a6491929fd1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Pais 

###### 23 Ago 2018 

Ante la posibilidad de que el Consejo Nacional Electoral CNE, no otorgue definitivamente la personería jurídica al partido Colombia Humana del que hacen parte el ex candidato presidencial Gustavo Petro y su fórmula vicepresidencial Angela Robledo, diversas voces se han levantado en desacuerdo con una decisión que califican como **antidemocrática, ya que los dejaría sin financiación estatal y  sin derecho a replica como oposición**.

Una de las razones que esgrime el CNE como argumento, radica en que **legalmente se encuentra establecido que la personería jurídica solamente se obtiene presentándose a elecciones legislativas y se debe sobrepasar el 3% del Umbral de votación**. Sin embargo, Jorge Rojas, integrante de la Colombia Humana, refuta tales aseveraciones recurriendo a lo dispuesto en el recientemente creado estatuto de oposición en Colombia.

Para tal fin, Rojas se remite al artículo 24 de la ley 1909, que ordena no solo que el candidato que quedo de segundo en las elecciones presidenciales ocupe una curul en el Senado y su fórmula vicepresidencial otra en la Cámara; sino que además **ambos harán parte de una agrupación política que tendrá todos los derechos para ejercer la oposición**.

Adicionalmente, cita el fallo de exequibilidad de la Corte Constitucional en el que ratifica que **los derechos de la oposición contemplan tanto la designación de tales curules como la representación política que estas implica**, lo que a su juicio demandaría una interpretación democrática por parte del CNE para otorgar la personería jurídica en ambos casos.

"La pregunta es si el CNE en su última sesión, **va a asumir esa responsabilidad democrática o si sencillamente le va a cerrar las puertas al candidato más votado que hay en el senado de la república**" asegura Rojas refiriéndose a los 8 millones de votos alcanzados por el candidato Gustavo Petro.

Aunque desde el movimiento se ha solicitado el reconocimiento al CNE, y este organismo lo ha rechazado, al final de la tarde de ayer ha dicho que **todavía falta una sesión más para definir el asunto** dadas las reacciones que se ha desatado en todo el país por su anuncio inicial, lo que da un margen para que esa posición **sea reconsiderada por el nuevo tribunal electoral que será elegido el próximo 29 de agosto**.

De no presentarse una respuesta positiva, desde la Colombia Humana aseguran que **recurrirán a los mecanismos legales necesarios para defender los derechos en el marco del Estatuto de la oposición**, y demandar garantías nacional e internacionalmente. El CNE "están mandando un pésimo mensaje al mundo sobre la restricción de los derechos a la oposición política en Colombia tal y como ha ocurrido en otros países de la región" puntualiza Rojas.

<iframe id="audio_28050834" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28050834_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
