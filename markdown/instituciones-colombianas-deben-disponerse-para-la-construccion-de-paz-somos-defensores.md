Title: "Instituciones Colombianas deben disponerse para la construcción de paz" Somos Defensores
Date: 2017-09-12 20:02
Category: DDHH, Nacional
Tags: colombia, Somos defensores
Slug: instituciones-colombianas-deben-disponerse-para-la-construccion-de-paz-somos-defensores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [12 Sept 2017] 

El último capítulo del informe “Stop Wars” de la organización Somos Defensores, pone en alerta a Colombia frente a los restos que debe asumir, en términos de derechos humanos, las diferentes instituciones del Estado en los próximos años, de cara a la construcción de la paz, **que reconozca la reparación a las víctimas a través de procesos de justicia y verdad.**

**Las pequeñas experiencias de autoprotección**

Este último capítulo del informe llamado “La amenaza fantasma” contiene y visibiliza las pequeñas estrategias que han usado históricamente diferentes comunidades para protegerse de las agresiones por parte de diferentes sectores del conflicto armado. (Le puede interesar: ["Entre 2009 a 2016 fueron asesinados 458 defenssores de DD.HH en Colombia"](https://archivo.contagioradio.com/entre-2009-y-2016-han-sido-asesinados-458-defensores-de-dd-hh-en-colombia/))

Una de esas estrategias fue la que utilizó la Asociación de Trabajadores Campesinos del Carare, que a través de la organización lucho por la vida, la paz y el trabajo, la experiencia de Mogotes, en Santander, en el año 1999, **también es expuesta como un trabajo de empoderamiento,** en donde la comunidad logra tomar las riendas, a través de la democracia de su territorio y lograr alejar el conflicto armado.

Otra de esas formas de organización que ha mantenido el respeto a la vida, es la guardia indígena que habita en diferentes territorios del país y que han hecho un trabajo de control comunitario y **dialogo para mediar ante la violencia que se asentaba en sus comunidades. **(Le puede interesar: ["Víctimas de masacre de Mapiripan denuncian amenazas de muerte"](https://archivo.contagioradio.com/victimas-de-desplazamiento-en-mapiripan-denuncian-amenazas-contra-su-vida/))

Estas pequeñas experiencias, son para el informe de Somos Defensores, estrategias que deben ser tenidas en cuenta por las instituciones para continuar en el trabajo de la defensa de los derechos humanos y entretejerlas con los mecanismos que actualmente existen para una reacción temprana en caso de riesgos.

### **Garantías, un camino por recorrer** 

Somos Defensores hace un llamado, en el informe, para que tanto la sociedad civil como el Estado, aúnen esfuerzos en transformar escenarios de guerra en regiones de paz, en donde primen las apuestas de interlocución **y una participación institucional que debe estar acorde con las nuevas realidades y contextos.**

Uno de los puntos reajustar que expone el informe son las matrices de riesgos individuales y su utilidad en la protección integral y colectiva, **además habla de la necesidad de poner en marcha una política pública de protección en el post acuerdo. **(Le puede interesar: ["Visita oficial de fiscal Bensouda del CPI es llamado de atención para Colombia")](https://archivo.contagioradio.com/fiscal-cpi-colombia-fatou-bensouda/)

### **Los riesgos de la paz** 

Finalmente el documento evidencia los posibles riesgos a los que se enfrenta la construcción de paz, como el surgimiento de nuevas estructuras armadas, producto de disidencias o del paramilitarismo, que intente un re acaparamiento del territorio colombiano, **sobretodo de aquellas regiones en donde no hay una presencia fuerte del Estado**.

De igual forma expuso que, tras la firma de los acuerdos de paz con las FARC-EP, arreciaron las amenazas, asesinatos y atentados contra defensores de derechos humanos y líderes sociales en Colombia, **un síntoma de que persiste la violencia y el interés porque en el país no haya cambios estructurales de fondo.**

En esta medida, señala la urgencia porque se replanteen los mecanismos de prevención en los territorios y exista un trabajo conjunto entre todas las instituciones a **nivel local, departamental y nacional, que actúen de manera rápida en la defensa de la vida**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
