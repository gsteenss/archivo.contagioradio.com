Title: La costumbre de hacer críticas sin fundamento, el caso de la coca.
Date: 2019-09-09 16:41
Author: A quien corresponde
Category: Opinion
Tags: coca, Cristianos, Religión
Slug: la-costumbre-de-hacer-criticas-sin-fundamento-el-caso-de-la-coca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

*** ***

***Creaturas del Señor, bendigan al Señor***

(Daniel 3, 57)

 

Bogotá, 2 de septiembre del 2019

 

*Reconocer nuestra dependencia de la naturaleza, *

*nos hace más responsables con ella y con la vida.*

 

Estimado

**Hermano en la fe**

Cristianos, cristianas, personas interesadas

 

Una persona de tu entorno religioso, generó un conflicto en una reunión familiar cuando dijo que “los que se *oponen a la fumigación conglifosato son narcotraficantes o cómplices de ellos y que un buen cristiano tiene que respaldar las fumigaciones para acabar con la droga”,* sin argumentos, ni fuentes que respaldaran su afirmación,se limitó a atacar y descalificar a quienes no estuvieron de acuerdo con él.

Discusiones como éstas son frecuentes, en espacios familiares y de amigos, con personas que creen tener razón en todo y que, desde sus convicciones políticas y creencias religiosas,  hablan en nombre de Dios, afirman conocer “la Voluntad de Dios”, y por eso piensan que hay que creerles sin cuestionar nada, sin pedir argumentos de sus afirmaciones; como ellos y ellas les creen a sus líderes.

El tema del narcotráfico es amplio, complejo y polémico; aquí solo tendré en cuenta la afirmación que generó la discusión. Para entendernos es importante aclarar el sentido de las siguientes palabras:

**Coca, hoja de coca**: planta originaria de los Andes, con muchas propiedades[\[1\]](#_ftn1){#_ftnref1} medicinales y alimenticias, contienefibra, proteínas, vitaminas**,** calcio, potasio, magnesio, fósforo, hierro y zinc, es antioxidante y alcaloide, estimula el organismo como el café; los pueblos indígenas la han utilizado ancestralmente en ritos sagrados y la mastican constantemente sin dañocomprobados a la salud.

**La cocaína:** *“la coca tiene componentes, con mayores o menores efectos farmacológicos, uno de los cuales resulta ser el alcaloide cocaína, que, en su forma concentrada o sintetizada es un estimulante con propiedades potencialmente adictivas”[**\[2\]**](#_ftn2){#_ftnref2}.*  Para sacar la cocaína de la hoja de coca se utilizan precursores químicos como el éter, ácido sulfúrico o la gasolina, el resultado de este proceso crea dependencia y daños a la salud.

**Narcótico:** palabra que *“significa ´cosa capaz de adormecer y sedar´, se usa con frecuencia para referirse a todo tipo de drogas psico activas, es decir, aquellas que actúan sobre el psiquismo del individuo, se pueden dividir en: opio, opiáceos y sucedáneos sintéticos”[**\[3\]**](#_ftn3){#_ftnref3}.* Hoy, está aumentando el consumo de las llamadas “drogas sintéticas”, según el Observatorio de Drogas de Colombia (ODC), órgano adscrito a la Dirección de Política de Drogas y Actividades Relacionadas del Ministerio de Justicia y del Derecho, entre el 2010 y septiembre del 2018, se detectaron 32 Nuevas Sustancias psicoactivas. Sustancias que generan “drogodependencia”.

**Narcotráfico**: proceso de producción, distribución y venta de sustancias psicoactivas o drogas ilegales, actividad relacionada con delitos como tráfico de personas, tráfico de armas, lavado de dinero, corrupción pública y social. Todos conocemos el daño ético, social, ambiental y económico generado por el narcotráficoen Colombia.

**Glifosato**: SegúnMonsanto, “*es un ingrediente activo en algunos herbicidas, inhibe una enzima que es esencial para el crecimiento de la plantea. Es un herbicida no selectivo, lo que significa que tiene efectos sobre la mayor parte de las especies de plantas verdes”[**\[4\]**](#_ftn4){#_ftnref4}.* Según Greenpeacce[\[5\]](#_ftn5){#_ftnref5}, genera riesgos a la salud de las personas que se dedican a la agricultura y sus familias, a quienes consumen alimentos contaminados con plaguicidas, entre ellos glifosato e impacta al medio ambiente, porque contamina los suelos, el agua y afecta a otros seres vivos.

Con estas precisiones, es importante afirmar que quienes nos oponemos a las fumigaciones estamos en contra del narcotráfico y de la drogadicción, porque conocemos los daños causados por esta “industria de la muerte” que mata, de diversas maneras,  a consumidores, productores y cadenas de intermediación mientras genera ganancias gigantescas a sectores del poder económico y financiero del mundo, que no se daña, ni se mata. Nos oponemos por razones de salud, sociales, económicas, éticas y religiosas; afirmamos que no somos cómplices del narcotráfico, más aún, el narcotráfico ha amenazado y asesinado a líderes sociales que se oponen a la fumigación. (le puede interesar: [El regreso del glifosato, una amenaza para campesinos en zonas cocaleras](https://archivo.contagioradio.com/el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras/))

“Los gobiernos del mundo”, no quieren reconocer el fracaso de 45 años de guerra contra las drogas, no reconocen que a esta “industria de muerte” la mueve la demanda. ¿Qué están haciendo los países consumidores para acabar con el consumo?  Mientras existe quien consuma y pague habrá alguien que la produzca en algún rincón del mundo.

Estas son algunas razones para oponernos a las fumigaciones con glifosato, no están en orden de importancia:

**En primer lugar, por la ineficiencia.** Las fumigaciones no han disminuidolos cultivos de uso ilícito, la producción y el tráfico de cocaína. Camilo Romero gobernador de Nariño señala, con datos oficiales, que entre 2004 y 2015, el departamento fue asperjado con tres millones setecientos mil (3.700.000) libros de glifosato, sin embargo, en ese mismo periodo las hectáreas cultivadas aumentaron a cerca de cuatro mil (4.000), lo mismo ha ocurrido en otras regiones; las fumigaciones no han disminuido el cultivo, pero sí han generado graves daños sociales y ambientales.

**En segundo lugar, por cuestiones de salud**. Los defensores de las fumigaciones dicen que es inofensivo y exigen pruebas científicas realizadas en seres humano (creo que ninguno de ellos se dejaría fumigar con glifosato o lo consumiría para que el laboratorio analice los efectos); pero los testimonios  de habitantes de regiones frecuentemente fumigadas muestran las afectaciones en niños, niñas, mujeres embarazadas y todo tipo de personas, muchas de ellas recogidos en [los 12 estudios sobre glifosato en colombia](https://www.elespectador.com/noticias/salud/esto-dicen-los-12-estudios-sobre-glifosato-en-colombia-articulo-809532). Uno se pregunta, si es inofensivo ¿por qué Colombia concilió con Ecuador y pagó 15 millones de dólares para que el proceso ante el Tribunal Internacional de la Haya, por las afectaciones causadas por las fumigaciones realizadas en el lado colombiano de la frontera no siguiera? (se comprobó que el viento llevó el glifosato al lado ecuatoriano e hizo daños).  La derrota en ese tribunal era inminente porque se demostraron los daños causados a la salud humana, a los cultivos y al ambiente en general, entre las pruebas presentadas por Ecuador, estaban los estudios del profesor colombiano Daniel Mejía Londoño, a quien el gobiernos colombiano poco ha escuchado.

**En tercer lugar, por cuestiones económicas.**Erradicar una hectárea de coca fumigándola con glifosato cuesta setenta y dos millones (72.000.000), erradicarla manualmente cuesta treinta y seis millones (36.000.000), según datos del mismo Estado. ¿Por qué se insiste en una estrategia el doble de costosa, menos eficaz y dañina para el ser humano y la naturaleza?

**En cuarto lugar, por razones sociales.** La mayoría de cultivadores, lo hacen empujados por el deterioro de sus condiciones de vida, por los costos de producción, de transporte y las dificultades para una comercialización justa de sus productos; las fumigaciones profundizan su crisis al acabar con sus cultivos, con las pocas fuentes de ingresos sin ofrecer alternativas reales generado hambre y destrucción.

**En quinto lugar, por razones ambientales.** Son muchas las evidencias de los daños a bosques, la flora y la fauna, la contaminación de las aguas y la muerte de peces, los daños a todo el ecosistema; además, de los efectos lentos e imperceptibles sobre la tierra, el agua y el aire.  No creo que sea más dañina el agua que el glifosato, como dicen miembros del gobierno, me gustaría verlos tomar glifosato.

**En sexto lugar, por razones éticas o lógicas.** Si las fumigaciones son dañinas para la salud humana y para el ambiente, si son el doble de costosas y menos eficaces para acabar con los cultivos de coca, si el problema no es la hoja de coca sino su transformación en cocaína, si el negocio está determinado por el consumo en Norte América y Europa, si el narcotráfico se acaba cuando acabe la demanda ¿Por qué no se intenta resolver el problema de fondo, ir a la raíz? ¿Por algo muy valioso será?

**En séptimo lugar, por razones religiosas.** Para los creyentes la palabra de Dios debe orientar el actuar y ella nos advierte: *“La creación espera con gran impaciencia el momento en que se manifieste claramente que somos hijos de Dios. Porque la creación perdió su verdadera finalidad, no por su propia voluntad, sino porque Dios así lo había dispuesto; pero le quedaba siempre la esperanza de ser liberada de la esclavitud y la destrucción, para alcanzar la gloriosa libertad de los hijos de Dios”.*

Es decir, que los creyentes alcanzamos la libertad de los hijos e hijas de Dios, liberando la creación de la esclavitud y la destrucción.

 

Fraternalmente, su hermano en la fe,

 

Alberto Franco, CSsR, J&P

<francoalberto9@gmail.com>

 

[\[1\]](#_ftnref1){#_ftn1}<https://www.enbuenasmanos.com/propiedades-de-la-hoja-de-coca>, consultada el 12 de agosto del 2019.

[\[2\]](#_ftnref2){#_ftn2}<https://www.tni.org/es/primer/hoja-de-coca-mitos-y-realidad>, consultada el 12 agosto del 2019.

[\[3\]](#_ftnref3){#_ftn3}<https://es.mimi.hu/medicina/narcotico.html>, consultada el 12 agosto del 2019.

[\[4\]](#_ftnref4){#_ftn4}<http://www.monsantoglobal.com/global/ar/productos/pages/que-es-el-glifosato.aspx>, consultada el 12 agosto del 2019.

[\[5\]](#_ftnref5){#_ftn5} Cf. <https://es.greenpeace.org/es/trabajamos-en/agricultura/glifosato>, consultada el 12 agosto del 2019.
