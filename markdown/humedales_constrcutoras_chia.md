Title: Constructoras estarían rellenando humedales en Chía, Cundinamarca
Date: 2017-09-21 13:14
Category: Ambiente, Entrevistas
Tags: Chía, Humedales, Tinguas
Slug: humedales_constrcutoras_chia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DJI_00014-1200x674.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[SkyscraperCity]

###### 20 Sep 2016 

"**Todo Chía se está convirtiendo en una ciudadela"**, denuncian habitantes de ese municipio quienes en los últimos meses han tenido que ser testigos de cómo por cuenta de la construcción de otro de estos conjuntos de casas de lujo, los pequeños humedales, conocidos como chucuas, han sido tapados y rellenados para edificar, ocasionando graves problemas ambientales para la zona.

Se trata del **proyecto Darién, para viviendas de estrato 4, 5, y 6** como lo asegura Luis Parrado,  presidente de la Junta de Acción Comunal del sector. Según denuncia el vocero de la comunidad, sería un proyecto que va en contravía del Plan de Ordenamiento Territorial (POT) de Chía, por lo cual, la licencia de construcción podría contar con una serie de irregularidades promovidas por la administración municipal.

### L**os daños ambientales** 

La vereda conocida como Fagua, es una de las más grandes de Chía y alberga cerca de 8 mil habitantes, de los cuales unos 2 mil o 3 mil serían los más afectados por esta situación. Parrado señala  que "**están rellenanado y tapando la fuente hídrica, la Chucua de Fagua"**, un ecosistema que recolecta el agua lluvia proveniente de los Cerros Occidentales, la cual es transportada a través de un vallado paralelo a la vía, para desembocar en el río Frío; su flujo de agua es continuo y abundante.

La situación preocupa a los vecinos  puesto que esta situación convierte a una zona segura en una altamente inundable. Además se está destruyendo el **hábitat de tinguas, peces endémicos y árboles nativos que están en peligro por las actividades de esas constructoras**.

Pese a los riesgos y las advertencias de los habitantes, ninguna institución responde. Explica Parrado que se han enviado derechos de petición a distintas instancias como la alcaldía y la CAR Cundinamarca, pero nadie se apersona de la situación, que entre otros, la ha causado empresas y familias que han comprado esos terrenos como los Díaz Amaya, y otra serie de constructoras que tienen especial interés en la zonas.

<iframe id="audio_21019047" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21019047_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div>

<div>

Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

</div>

<div>

</div>

</div>
