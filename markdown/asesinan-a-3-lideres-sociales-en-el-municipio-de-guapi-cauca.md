Title: Asesinan a 2 líderes sociales en el municipio de Guapi, Cauca
Date: 2018-02-10 10:38
Category: DDHH, Nacional
Tags: Asesinato a líderes sociales, FARC, Guapi, Unión Patriótica
Slug: asesinan-a-3-lideres-sociales-en-el-municipio-de-guapi-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/WhatsApp-Image-2018-02-10-at-10.33.57-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [10 Feb 2018]

Un doble asesinato de líderes sociales se presentó en las últimas horas en el municipio de Guapi, Cauca, se trata de Jesús Orlando Grueso y Jhonatan Cundumí Anchino. Los líderes y defensores de derechos humanos, estaban impulsando el programa de sustitución de cultivos ilícitos de manera gradual en su territorio. **Hasta el momento primeras versiones afirman que ELN habría sido el autor de este crimen**.

De acuerdo con Marino Grueso, hermano de Jesús Orlando, los asesinatos se **comentieron el 9 de febrero en horas de la noche**, cuando Jesús realizaba tareas de apoyo a la campaña de las candidaturas de Ancizar Barrios a la Cámara de Representantes por el Cauca, Aida Avella y las postulaciones del la Fuerza Alternativa Revolucionaria del Común. (Le puede interesar: ["Empieza febrero y ya se registran dos asesinatos de lideresas sociales"](https://archivo.contagioradio.com/febrero_asesinatos_lideres_sociales/))

Tanto Jesús Orlando como Jhonatan eran integrantes del Movimiento Étnico y Popular del Pacifico -MOEP, de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana COCCAM y del Movimiento Politico y Social Marcha Patriotica en el Departamento del Cauca y **venía impulsando el proceso de sustitución gradual y voluntaria de cultivos de uso ilícito en el Marco del punto 4 del acuerdo de la Habana. **

Noticia en desarrollo ...
