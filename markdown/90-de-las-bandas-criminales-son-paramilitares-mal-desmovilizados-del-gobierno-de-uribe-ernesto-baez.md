Title: “90% de las bandas criminales son paramilitares mal desmovilizados del gobierno de Uribe”: Ernesto Baez
Date: 2018-04-26 13:47
Category: DDHH, Nacional
Tags: acuerdo de paz, AUC, desmovilización AUC, Ernesto Baez, FARC, paramilitares
Slug: 90-de-las-bandas-criminales-son-paramilitares-mal-desmovilizados-del-gobierno-de-uribe-ernesto-baez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/baez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Avanza Colombia] 

###### [26 Abr 2018] 

En diálogo con Contagio Radio, el ex comandante de las AUC, Iván Roberto Duque, conocido como **Ernesto Baez**, manifestó que la sociedad debe conocer la verdad por parte de los actores armados y reconoció que hubo problemas del proceso de desmovilización de las AUC durante el Gobierno de Uribe como lo fue la reinserción a la vida civil producto de las debilidades institucionales.

Baez enfatizó en que los grupos armados en Colombia tienen la responsabilidad de decirle **“no a la guerra y no a la violencia”.** Dijo que, “en medio de tantas diferencias (…) tenemos la obligación de no sólo luchar por la paz sino que tenemos la obligación moral de pedirle perdón a Colombia por todo este daño que hemos causado”.

### **En el Gobierno de Uribe hubo falencias en el proceso de desmovilización de las AUC** 

Dentro de las afirmaciones de Baez, con relación al proceso de desmovilización de las AUC durante el Gobierno del hoy senador Álvaro Uribe Vélez, afirmó que las cifras de desmovilizados que presentaron las autoridades **no corresponde con la realidad**. (Le puede interesar:["Organizaciones sociales exigen compromisos del Gobierno para el desmonte del paramilitarismo"](https://archivo.contagioradio.com/comunidades-asediadas-por-el-paramilitarismo-solicitaron-su-desmonte/))

Afirmó ademas que, por ejemplo, los menores de edad que entregaron las AUC, fueron recogidos por el Gobierno y llevados a albergues donde **no se les prestó la atención adecuada**. Por esto, indicó que “muchos de los niños, ya hoy mayores de edad, hacen parte de las llamadas bandas criminales”. Esto para Baez fue una de las grandes falencias del proceso de desmovilización.

Adicionalmente enfatizó en las negociaciones de paz no se pueden quedar en los discursos sino que “debe ser un proceso serio que conduzca a los combatientes a una [**reinserción**]** exitosa** a la sociedad colombiana”. Dijo que el 90% de los integrantes de las llamadas bandas criminales “son paramilitares mal desmovilizados por el Gobierno de Uribe”.

### **Es necesario que Jesús Santrich vuelva a la actividad política.** 

En lo que tiene que ver con el proceso de paz que se está llevando a cabo con las FARC, el ex jefe paramilitar indicó que **“es más fácil conseguir un fusil que entregarlo”**. Por esto resaltó la importancia del Acuerdo de Paz “que se debe valorar en su justa dimensión”. (Le puede interesar:["Ex paramilitares que se comprometan con la verdad podrán ingresar a la JEP"](https://archivo.contagioradio.com/exparamilitares-que-se-comprometan-con-la-verdad-podran-ingresar-a-jep/))

Dijo que la libertad de Jesús Santrich es indispensable para que el proceso de paz se lleve a cabo de la manera correcta teniendo en cuenta que “ese episodio vende una idea muy negativa a organizaciones como el ELN que está en transe de una negociación de paz”.

Manifestó que es necesario que el Gobierno Nacional busque crear medidas que **desarticulen** los nuevos grupos paramilitares y grupos armados ilegales que están reviviendo la violencia en el país. Dijo además que “los únicos portadores de la verdad son los actores de primera clase en el escenario espantoso de la guerra”.

Finalmente Baez indicó que le ha propuesto a las FARC “adelantar gestiones para conformar **comisiones de la verdad regionales** para luego procesar toda la información en una sola verdad nacional”. Por esto, manifestó su voluntad de contar la verdad como un gesto necesario y “mínimo” con las víctimas.

<iframe id="audio_25644162" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25644162_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
