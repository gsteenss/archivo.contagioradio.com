Title: Sujetos apuñalaron a oso hormiguero en Flandes, Tolima
Date: 2018-03-13 14:48
Category: Animales, Voces de la Tierra
Tags: CAR, oso hormiguero, violencia contra los animales
Slug: sujetos-apunalaron-a-oso-hormiguero-en-flandes-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/Osos-hormigueros-1024x768-e1520965304460.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: De Perros] 

###### [13 Mar 2018] 

Organizaciones que defienden los derechos de los animales y ambientalistas denunciaron que sujetos aún desconocidos apuñalaron a un oso hormiguero en el municipio de Flandes en el departamento de Tolima. El oso, aunque no murió **se encuentra en graves condiciones** por lo que hicieron un llamado a que se judicialice a los responsables y se aplique la Ley 1774 de 2016, mediante la cual se penaliza el maltrato animal.

De acuerdo con Berta Guarinizo, defensora de los derechos de los animales, el oso fue atacado por una persona “inescrupulosa que **lo apuñaló y lo golpeó** en varias ocasiones”. Dijo que la denuncia fue hecha por una organización que trabaja en Girardot, Cundinamarca y que las autoridades de protección animal atendieron el caso para remitirlo a la Corporación Autónoma Regional.

### **Responsables deben ser judicializados** 

Guarnizo manifestó que quienes recogieron al oso **“no sabían cómo atenderlo ni cómo proceder”**. Informó que las personas lo llevaron a su casa y el animal “estaba agresivo por el dolor que sentía”. Sin embargo, lograron que fuera remitido a la CAR para posteriormente poner las denuncias pertinentes.

Teniendo en cuenta las agresiones que sufrió el oso, la animalista manifestó que se debe **judicializar a quienes cometieron estos actos** y que la ciudadanía debe generar conciencia sobre la importancia de proteger a los animales. Dijo que las autoridades deben comenzar a hacer la investigación pertinente para sancionar con las medidas necesarias al agresor, teniendo en cuenta que en Colombia existe una Ley que castiga el maltrato hacia los animales hasta con tres años de cárcel. (Le puede interesar: ["En riesgo más de 15 especies animales en humedal Tubaguya"](https://archivo.contagioradio.com/en-riesgo-mas-de-15-especies-animales-por-ampliacion-de-planta-de-tratamiento-en-humedal-tibaguya/))

### **Sociedad debe conocer la aplicabilidad de la ley contra el maltrato animal** 

Frente a lo que se debe hacer cuando ocurren estos casos, la ambientalista recalcó que hay un **desconocimiento y poco compromiso** en la aplicabilidad de la ley 1774. Además, “la sociedad también desconoce el proceso que se debe llevar a cabo y la aplicación de la norma”. Dijo que cuando esto ocurre “se debe seguir un debido proceso para involucrar a los entes de control”.

Enfatizó en que la aplicación la ley **es aún muy débil** y se debe ligar a un trabajo articulado entre las autoridades y la ciudadanía. Finalmente, recordó que de acuerdo con dicha Ley “los animales no son objetos sino seres sintientes por lo que tienen unos derechos que deben ser respetados".

<iframe id="audio_24393598" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24393598_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
