Title: Aumentar pie de Fuerza en el Catatumbo no resolverá crisis humanitaria: ASCAMCAT
Date: 2018-08-10 16:38
Category: DDHH, Nacional
Tags: Catatumbo, Iván Duque, Ministros de Iván Duque
Slug: aumentar-pie-de-fuerza-el-catatumbo-no-resolvera-crisis-humanitaria-ascamcat
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Militaress-Boyaca-e1524691374383.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [10 Ago 2018] 

El presidente Iván Duque manifestó que el aumento del pie de Fuerza en el Norte de Santander será una de las medidas para solucionar la crisis humanitaria que vive este departamento. Sin embargo, de acuerdo con Juan Carlos Quintero, vocero de ASCAMCAT, esta disposición **podría llevar a recrudecer el conflicto armado y a la violación de derechos humanos por parte de todos los actores armados**.

Quintero afirmó que en la reunión que sostuvieron diversas organizaciones defensoras de derechos humanos, sectores del movimiento social e instituciones gubernamentales, el presidente Duque "**no se refirió nunca a la grave crisis social humanitaria"** que viven las comunidades del Catatumbo" y que por el contrario el aumento del pie de Fuerza podría profundizar la alarmante situación que ya viven las comunidades.

El vocero de ASCAMCAT, también señaló que, Duque tampoco habló de la masacre de las **9 personas en el Tarra, ni a si hay adelantos por parte de la Fiscalía en el esclarecimiento de estos hechos** y que actualmente en la zona del Catatumbo hay actualmente más de 10 mil unidades de la Fuerza Pública, que no han impedido el aumento de la violencia en el territorio. (Le pude interesar:["¿Dónde estaba el Ejército durante la masacre de El Tarra?"](https://archivo.contagioradio.com/donde-estaba-ejercito-durante-masacre/))

### **¿Qué hay detrás de la militarización?** 

Además, según Quintero la propuesta del aumento de la Fuerza Pública en el territorio revive la preocupación de **la puesta en marcha de megaproyectos que han generado tensiones entre empresarios y comunidades Campesinas e indígenas.**

Algunos de ellos tendrían que ver con la ampliación de cultivos de palma hacia la zona de reserva campesina del  Catatumbo, la ampliación de la vía del Carbón que conectaría a Tibú con la Costa Atlántica y la intensión de permitir explotación de carbón a cielo abierto en la zona baja de esta región.

### **Los compromisos de Duque con el Catatumbo** 

De otro lado, Quintero manifestó que el presidente aseveró que tiene la intención de gestar un diálogo con diversos sectores de la sociedad, en ese sentido se acordaron canales de interlocución entre los que se encuentra como intermediarió el representante de la ONU en Colombia.

Asimismo, entre las peticiones que hicieron las organizaciones defensoras de derechos humanos al gobierno Duque esta la posibilidad de que se le brinde logística y elementos jurídicos necesarios para que sea la **Comisión por la Vida y la Reconciliación la que funcione como interlocutor en el conflicto que hay entre el ELN y el EPL**.

De igual forma exigieron el cumplimiento de los planes de sustitución de cultivos de uso ilícito que ya se venían adelantando en el territorio y que se logre avanzar en el cumplimiento de acuerdos que asumió el Estado en diferentes mesas de diálogos con la comunidad.

<iframe id="audio_27744436" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27744436_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
