Title: FFMM y bancos, son las prioridades de Iván Duque durante la pandemia: Coeuropa
Date: 2020-06-11 21:54
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Iván Duque
Slug: ffmm-y-bancos-son-las-prioridades-de-ivan-duque-durante-la-pandemia-coeuropa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/200328-01-Facebook-Live-Coronavirus-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Iván Duque/ Presidencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones sociales han seguido de cerca la política institucional desplegada por el gobierno de Iván Duque a lo largo de la pandemia del Covid-19, los resultados un incremento en las acciones y represión por parte de la Fuerza Pública, **decretos que han priorizado a los bancos y grandes empresas, además de un incremento de la violencia en las re**giones caracterizada por un aumento en el asesinato de líderes sociales.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la [Coordinación Colombia - Europa - Estados Unidos – CCEEU](https://coeuropa.org.co/158/) advierten que las acciones del gobierno han priorizado a los bancos por encima de otros sectores económicos y de la misma ciudadanía, lo cual, según su más reciente boletín «ha dejado **más de 5,4 millones de personas sin empleo en solo un mes de la emergencia económica y social decretada, aumentando el desempleo a un 32.6%».**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aseguran que los [117 billones](https://id.presidencia.gov.co/Paginas/prensa/2020/Colombia-invierte-cerca-de-117-billones-de-pesos-para-atender-la-Emergencia-Economica-por-la-pandemia-del-covid-19-200527.aspx) de pesos que el Gobierno dice haber destinado para la atención de la pandemia terminaron favoreciendo a los bancos mediante cuotas de intermediación para la entrega a los beneficiarios de subsidios como Ingreso Solidario (6.000 millones) y a través de los intereses recaudados por los préstamos que desembolsan a  la Nación, ya que un porcentaje de las ayudas se está solventando mediante créditos contraídos por el Gobierno con la banca privada (500.000 millones).

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El boletín también alude a las denuncias de corrupción y desviación de los recursos del programa  Ingreso Solidario señalando que: **«más de 16.000 cédulas de presuntas personas destinatarias de estos subsidios resultaron ficticias y en otros casos se descubrió que algunas ayudas habrían llegado a personas acaudaladas».**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Represión y Fuerza Pública**

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por otra parte, el informe resalta la represión de la que son víctimas personas que por necesidad o por salud mental se han visto imposibilitadas para cumplir el mandato de cuarentena obligatoria, aludiendo a la cifra de **más de un millón de multas** que fueron impuestas en el primer mes de  confinamiento obligatorio, según declaró el General Jorge Luis Vargas de la Policía Nacional y a las **cuotas que presuntamente se imponen a los agentes de policía para efectuar al menos 38 comparendos por día.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Adicionalmente, se centra en el endurecimiento del gobierno de Iván Duque en las acciones de las Fuerzas Militares en la erradicación forzada en regiones como Nariño, Putumayo, Catatumbo, Sur de Córdoba, Bajo Cauca, Meta y Caquetá, donde **la represión contra la población campesina que subsiste del cultivo de hoja de coca ha llevado a la ejecución extrajudicial de al menos cuatro líderes campesinos e indígenas»**, señala el boletín. (Le puede interesar: [Ejercito y narcotráfico: verdugos de la sustitución de la Coca](https://archivo.contagioradio.com/ejercito-y-narcotrafico-verdugos-de-la-sustitucion-de-la-coca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Finalmente señalan que las amenazas y asesinatos en contra de los liderazgos en los territorios se han intensificado durante la pandemia, ya que**, "la mayor parte de asesinatos ha sucedido cuando los líderes y lideresas estaban recluidos en sus casas, guardando la cuarentena, mientras sus perpetradores, estructuras paramilitares vinculadas al tráfico de drogas, pudieron desplazarse con libre movilidad**, incluso en áreas urbanas, buscando casa por casa, lista en mano, a sus víctimas», expresa el informe. (Lea también: [Durante 2020 se han registrado 115 amenazas y 47 asesinatos contra defensores de DD.HH.](https://archivo.contagioradio.com/durante-2020-se-han-registrado-115-amenazas-y-47-asesinatos-contra-defensores-de-dd-hh/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
