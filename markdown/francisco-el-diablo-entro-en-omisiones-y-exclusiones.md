Title: Francisco, el diablo entró en omisiones y exclusiones
Date: 2017-09-15 11:42
Category: Camilo, Opinion
Tags: Juan Manuel Santos, Papa Francisco, uribe
Slug: francisco-el-diablo-entro-en-omisiones-y-exclusiones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Diseño-sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Camilo de las Casas 

#### Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) 

###### 15 Sep 2017 

El primer Papa latino, en la anquilosada y bancaria jerarquía vaticana, concentró la mirada de millones de colombianos durante cuatro días.

Francisco un hombre de clase media, según biógrafos,  cuestionado por algunos ante su posición frente a la dictadura argentina. Muy singular, no se hizo llamar Ignacio, como podría haberse nombrado en memoria a Loyola.

Ese sencillo hombre animó a los utópicos en la persistencia por una sociedad justa en lo social y lo ambiental, y en una memoria de la verdad hacia la reconciliación como base de un sentido de leyes y sociedad incluyente, agnósticos, ateos, de izquierda, de centro y más, intentando romper el esquemático modelo de narraciones papales y la vida concordataria que existe en la cultura colombiana.

Entre lagartos de todo tipo, de clériman y púrpura, de corbatas y de todos los verdes, entre ellos los propietarios de Avianca, y los RCN, Caracoles, y los del abecedario,  W y Blu, llamó a la ética mínima que debería orientar a la clase política, a los sectores económicos, al aparato judicial, a la jerarquía de la iglesia, y a los ciudadanos en la construcción de la casa común. Una casa incluyente, justa, tierna, fresca, alegre, con mujeres y jóvenes;  reconociendo la tosudez y valentía de  las víctimas en una superación traumática basada en la verdad, y un proyecto de país con cambios económicos  en coexistencia con otras fuentes de la vida aguas, bosques, animales.

Entre la tensión Uribe y Santos, Francisco se refirió a la cizaña existente. Creo, evidentemente sembrada antes y después del plebiscito con el caprichito del presidente para aplastar a Uribe, y el caprichito uribista de atravesarse a todo lo huela a pax dialogada, mintiendo y encubriendo sus irresponsabilidades.

Ambos, expresando con disfraces la excluyente, ególatra, corrupta, guerrerista y con signos de criminalidad la ambiciosa dirigencia. Muchos de ellos en las graderías VIP que ocuparon conmocionados o pantallando con los mensajes de Francisco y en estos días con Natanyau. ¡Y obvio!, sin asumirse como responsables de las tragedias éticas, sociales, políticas y ambientales del país, entre ellas las de arrojar a millones a la desesperanza, a la indiferencia y a la guerra, para asegurar, ampliar y proteger sus poderes y las de terceros multinacionales.

Ellos, la expresión de la clase dirigente mentirosa e injusta reflejada en los incumplimientos a los acuerdos con la Cumbre Agraria, al Acuerdo del Teatro Colón con la ex guerrilla de las FARC EP, y a los límites absurdos a la mesa con ELN. Y qué no decir de la burla  a las víctimas con falsas respuestas a los fallos internacionales, y por supuesto el quite a las responsabilidades de crímenes contra la humanidad. Y falsamente aplaudiendo y aludiendo al mensaje ambiental de Francisco mientras Uribe y Santos han comprometido la política en el extractivismo sin límites. Poderosos pretendiendo usar a Francisco intentando asegurar su impunidad social con nuevas bendiciones para su modelo excluyente.

El ritual diplomático mostró ese poder eclesiástico con ese poder politico - militar - económico. Una agenda marcada por el episcopado  castrense, Monseñor  Fabio Suescún, íntimo amigo de la familia presidencial,  y su colega de docencia en  el Seminario Mayor en los 80, el cardenal Octavio Ruíz, de las entrañas del antecesor de Francisco, el poderoso reconservador Ratzinger. La narración simbólica de esa simbiosis concordataria origen de fúsiles y escapularios partes responsables del relato de la exclusión lo lograron ocultar

Dudo que el Papa sepa todo lo que es Colombia. Tampoco creo que su Nuncio le haya propiciado una lectura más critica de la propia iglesia católica, de los políticos, empresarios y militares, descubriéndole páginas de horror, selladas en sacro santo silencio o lealtades falsas a una democracia signada de sangre.

La puesta en escena de militares lisiados, omitiendo a los civiles y a los exguerrilleros, que se encuentran en esa misma condición por una guerra prolongada que nunca debió ocurrir, por supuesto, si otra democracia se hubiera cimentado. La trasescena de negación del conjunto de nuestra historia se bendijo con putrefactos olvidos y exclusiones.

Las escenas múltiples legitimaron la verdad oficial excluyente. Esa mentira que ha ido protegiendo celosamente Santos, la cúpula militar y ACORE. La que pretenden asegurar a empresarios, expresidentes,  militares héroes y victoriosos de la guerra, con los partidos de gobierno y el Centro Democrático. Omisiones de verdad en ese escenario papal,  haciendo una verdad incompleta, o amañada a la mentira del poder que ha destruido millones de personas a nombre de dios y la democracia. El relato de la negación y del ocultamiento.

El encuentro con más de 5000 víctimas imposibilitó  esa otras verdades. En Villavicencio se relegitimó la teoría de los dos demonios, dejando a un lado la responsabilidad del Estado. Así se presentaron víctimas de las guerrillas y paramilitares, relatos de ex guerrilleros y exparamilitares, expresión de la tragedia de los empobrecidos, en los que el Estado, sus dirigentes civiles,militares, eclesiástico no tuvieron  nada que ver.

Y qué decir del absoluto silencio de ese martirologio de los Toño Hernández, los Úlcue, los Restrepo, las Ramírez, los Guillard, las Hildegar consecuencia de la persecución eclesiástica combinada en alta ciruguía de represión estatal. El reconocimiento en contexto de la muerte de Monseñor Jaramillo es una verdad en la que sus responsables reconocen esa inaceptable muerte violenta.

Esa otra iglesia católica, denunciante del paramiltarismo de Estado, de las operaciones de enemigos internos de ESMAD, de la fuerza pública narcotizada, quedó al margen público. El osado acto de apostasía, días antes, respaldado por un millar de creyentes exigiendo reconocer la responsabilidad histórica en el uso del púlpito para justificar la violencia de Estado y la cultura de la persecución, o en casos de la participación directa en la guerra, no merecieron una atención. El libreto de la censura estaba hecho por esos sectores de una iglesia jerárquica acomodada a experimentarse absolutamente perfecta y divina en medio de la injusticia..Francisco, el italiano,  es resignificado en pasajes de Rubén Darío. El pasaje del hermano lobo, el que teme a los humanos por el odio, capaz de mentir y matar, es reconciliado en la ternura cuando ambos se desarman y no solo uno. Cuánto falta Francisco, el argentino, para que esos poderes fácticos asuman que la alegría y la libertad serán realidad cuando se asuma la verdad real, toda y completa. Y cuando cese su obsesiva ambición y modelo acaparador,  más allá de la adobada propuesta de inclusión construida en escenas fragmentadas de engañosos espejismos.

La superación de ese trauma social y político es la verdad reconocida y el cambio económico, lo demás es imposibilitar la conexión de lo fracturado, la conservación de lo establecido por un poder malhalado que ha hecho de la casa común una piñata, una mercantilización de todas las vidas. Gracias Francisco, los lobos son otros y poco quieren aportar a la reconciliación que nacen de la verdad y la justicia socio ambiental.

#### [Leer más columnas de opinión de  Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
