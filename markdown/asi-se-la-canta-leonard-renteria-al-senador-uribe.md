Title: Así se la canta Leonard Rentería al Senador Uribe
Date: 2016-09-16 11:29
Category: Cultura, Otra Mirada
Tags: Cultura, Música, paz
Slug: asi-se-la-canta-leonard-renteria-al-senador-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Leonard-renteria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Los primeros días de septiembre en la ciudad de Buenaventura, [ **Leonard Rentería**,](https://archivo.contagioradio.com/a-pesar-de-amenazas-leonard-renteria-seguira-resistiendo-en-su-territorio/) artista, y líder juvenil, cuestionó con argumentos el discurso del Senador Uribe en su apoyo y campaña por el No  al Plebiscito que refrenda los acuerdos de paz entre Gobierno y FARC.

Contagio Radio musicalizó las palabras de Leonard al Senador Uribe
