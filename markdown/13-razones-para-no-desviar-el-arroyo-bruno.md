Title: 13 Razones para no desviar el Arroyo Bruno
Date: 2016-04-15 15:43
Category: Ambiente, Nacional
Tags: arroyo Bruno, La Guajira
Slug: 13-razones-para-no-desviar-el-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Arroyo-Bruno1-e1460752989840.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ashajiraa 

###### [15 Abr 2015] 

De acuerdo con las comunidades de La Guajira, la maquinaria de la empresa El Cerrejón para desviar el arroyo Bruno ya está en la zona, luego de que la Agencia Nacional de Licencias Ambientales diera el permiso, a partir de “estudios amañados”, como lo asegura Danilo Urrea, de Censat Agua Viva.

Esta desviación traería graves consecuencias ambientales y sociales para las comunidades del departamento. Es por eso que en Contagio Radio citamos las razones publicadas en el informe **'La Desviación del Arroyo Bruno: entre el desarrollo minero y la sequía', de Censat Agua Viva – Amigos de la Tierra con apoyo de Sintracarbon,** para evidenciar los daños que podría acarrear esa intervención a una fuente natural de agua, que sería la número 27 que podría verse afectada por la multinacional.

**1. Impacta directamente el río Ranchería:** El Arroyo Bruno aporta alrededor de 0,90 m3 /seg promedio anual de agua al río Ranchería. El desvío de este afluente significaría una pérdida de 4,4 L/seg para el río ranchería, lo que significa más de 11 millones de litros al mes. Es decir, además del desvío el río Ranchería estaría en riesgo de secarse.

**2. Pérdida de diversidad biológica:** Por deforestación y degradación, fragmentación, disminución o desaparición de áreas naturales que albergan numerosas especies de plantas, animales y microorganismos.

**3. Desertificación:** El área de desviación se encuentra en una zona con amenaza por desertificación que sería acentuada con los procesos de deforestación y alteración de dinámicas hídricas.

**4. Cambio climático:** Tanto el desvío del Arroyo como la expansión minera requieren la intervención de una amplia zona, Es decir la pérdida de la capa vegetal y la deforetsatación generarían cambios en el nivel de precipitaciones aumentando la vulnerabilidad climática la región.

**5. Incrementa la crisis de agua en la zona:** El municipio de Albania toma agua del Arroyo Bruno ante los problemas de abastecimiento de su casco urbano. El desvío significaría una menor provisión de agua y por tanto problemas de salud pública para sus más de 26.000 habitantes.

**6. Ordenamiento Territorial:** Tanto el POMCA (Plan de Manejo Ambiental) del Río Ranchería como el POT (Plan de Ordenamiento Territorial) de Albania consideran que el Arroyo Bruno y sus bosques freatofíticos (con capacidad de absorver agua de las capas bajas de la tierra)  son zonas de preservación ambiental por su tarea de conservar la vegetación del desierto.

**7. El Cerrejón ya desapareció dos arroyos en la zona:** Según pobladores locales y los recorridos de la zona, los arroyos La Puente y Tabaco, han sido desaparecidos debido a la actividad de la multinacional en sus cauces.

**8. Hidrogeología:** La zona del Arroyo es un lugar de recarga de acuíferos que si se altera pone en riesgo el abastecimiento de las comunidades que toman agua de los pozos y en general la dinámica subterránea del agua fundamental para el abastecimiento en el departamento.

**9. Falla de Oca:** Esta pasa muy cercana al curso del Arroyo Bruno y tiene gran influencia en las dinámicas hídricas de la zona. Debe primar el principio de precaución ante la falta de certeza del impacto de la actividad de la falla y el cauce “construido".

**10. Control del agua:** La desviación del Arroyo, como la explotación minera implica acaparamiento territorial y de aguas por parte de la empresa. Esto en detrimento del derecho fundamental al agua y por tanto del derecho a la vida de las comunidades.

**11. Desplazamiento de comunidades y vulneración de derecho humano al agua:** Actualmente numerosas poblaciones se distribuyen en las zonas cercanas al Arroyo Bruno. Una de las causas de desplazamientos podría ser los altos índices de escasez hídrica propiciada por los impactos del desvío y la mayor vulnerabilidad territorial por la cercanía a la mina. El desplazamiento además, trae consigo cambios en las prácticas, usos y significados que las comunidades mantienen con esta fuente de agua que al ser desviada desaparecerían.

**12. Expansión de la actividad minera y de sus impactos negativos:** El motivo de desviar el Arroyo es ampliar las áreas mineras, es decir, extender sus impactos negativos a más territorios… más personas… más zonas de vida.

**13. Afecta cosmovisiones de comunidades wayúu y afrodescendientes:** Los pueblos indígenas y afrodescendientes tienen una fuerte relación con el agua que es constitutiva de su cosmovisión. La interacción entre el arroyo y la comunidad significa entonces la pervivencia de prácticas y significados que revitalizan la cultura y permiten otras interacciones asociadas a la medicina tradicional, el sueño como fuente de saber propio y la socialización. El desvío o intervención de cualquier fuente de agua, trastoca estas relaciones poniendo en peligro la integridad cultural de estos pueblos.

[Informe Arroyo Bruno](https://es.scribd.com/doc/308922218/Informe-Arroyo-Bruno "View Informe Arroyo Bruno on Scribd")

<iframe id="doc_39298" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/308922218/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
