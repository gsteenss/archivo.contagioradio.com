Title: Asesinan a líder ambientalista en Cauca
Date: 2017-02-19 17:48
Category: Ambiente, Nacional
Tags: ambientalistas, Asesinado, Cauca, Lider social
Slug: asesinan-a-lider-ambientalista-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Faiver-Cerón-Gómez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [19 Feb. 2017]

En una situación que ha consternado a la comunidad, **fue asesinado este 18 de Febrero Faiver Cerón Gómez,** Presidente de la Junta de Acción Comunal del **Corregimiento de Esmeraldas en Mercaderes, Cauca**. Faiver se destacaba por su compromiso con la comunidad, especialmente en el tema ambiental y había estado oponiéndose al proyecto minero de Esmeraldas por la multinacional minera C.I.S.A.S que explotaría el Cerro de la Campana.

Según primeras versiones, **Faiver fue atacado por hombres armados y encapuchados quienes con múltiples impactos de arma de fuego** acabaron con su humanidad en el punto conocido como Sajandinga,  a 10 minutos de la cabecera municipal, cuando regresaba en moto a su hogar.

Aseguran algunos integrantes de organizaciones que conocían el trabajo de Faiver, que **presumen que su asesinato tenga que ver con el trabajo incansable de este defensor y líder ambiental.** Así mismo, cuenta la comunidad que desconocen de problemas que él haya tenido a nivel personal y que hubieran podido desencadenar en este asesinato. Le puede interesar: [Continúan las amenazas a líderes en el Cauca](https://archivo.contagioradio.com/continuan-las-amenazas-a-lideres-en-el-cauca/)

En horas de la mañana de este sábado, Faiver había participado en un Consejo Comunitario de Gobierno con la Alcaldía y Secretarías de la Gobernación.

Las comunidades de las que hace parte y representaba Faiver se han movilizado desde 2016 en contra de la minería ilegal que acabó con el río Sambingo.

Además el pasado 7 de febrero realizaron una marcha contra la erradicación forzosa de cultivos de coca, que dio como resultado el establecimiento de Mesas con el Gobierno municipal, departamental y nacional para trabajar este tema.

Desde Mercaderes, Cauca, diversas organizaciones y la comunidad en general **han solicitado que se adelanten las investigaciones, se castigue a los responsables** y se brinden garantías a las comunidades, organizaciones y líderes. Le puede interesar: [Asesinan a Luz Herminia Olarte, lideresa social de Yarumal](https://archivo.contagioradio.com/asesinan-a-luz-herminia-olarte-lideresa-social-de-yarumal/)

### **Rectificación** 

En esta nota publicada el 19 de febrero de 2017, manifestamos que la empresa CI S.A.S. era una multinacional, sin embargo y por solicitud de la misma, nos permitimos rectificar que **esta organización no es una multinacional,** sino que es una empresa constituida legalmente en la ciudad de Medellín y que fue comprada recientemente por una familia nariñense. En la actualidad, la empresa tiene domicilio en la ciudad de Pasto **y se dedica a prestar servicios de consultoria e interventoria en geología.**

En cuanto al tema de la posible explotación de El Cerro La Campana, la empresa nos ha manifestado que esta información no es cierta, sino que **en la actualidad se encuentran haciendo un estudio de prospección minera en el cerro de la Campana** en la vereda La Esmeralda, pero que no se va a realizar ninguna explotación en dicho cerro.

Aclaramos que en ningún caso nuestro objetivo es desinformar, ni causar daño o riesgo a las personas integrantes de dicha empresa, razón por la cual y **por respeto a todas las personas que nos leen ofrecemos esta rectificación.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
