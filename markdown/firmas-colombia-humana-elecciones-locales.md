Title: Con firmas Colombia Humana presentará candidatos a elecciones locales
Date: 2019-03-22 14:17
Author: CtgAdm
Category: Nacional, Política
Tags: Alcalde, CNE, Colombia Humana, elecciones
Slug: firmas-colombia-humana-elecciones-locales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-22-a-las-1.56.12-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @jerojasrodrigue] 

###### [22 Mar 2019] 

Gracias a una decisión del Consejo Nacional Electoral (CNE), el movimiento político Colombia Humana deberá recurrir a las firmas ciudadanas para presentar candidatos a las elecciones regionales de este año. **El Consejo ratificó la determinación tras resolver una tutela interpuesta por integrantes del Movimiento,** quienes señalaban la vulneración a su derecho a la participación en política.

El primer fallo contra la personería jurídica del movimiento que lidera Gustavo Petro se produjo el año pasado, cuando el CNE consideró que no cumplía con las condiciones necesarias para alcanzar el reconocimiento; posteriormente, el movimiento interpuso una acción de tutela para que se revisara la decisión, misma que fue contraria a sus intereses. (Le puede interesar: ["Cuatro acciones con las que se quiere atacar a la Colombia Humana"](https://archivo.contagioradio.com/atacar-colombia-humana/))

A propósito del tema, el **precandidato por Colombia Humana para la alcaldía de Bogotá, Jorge Rojas**, afirmó que la decisión le da la razón a quienes habían advertido que el Órgano electoral tomaría una decisión de carácter político y no jurídico, al tiempo que **cerraría definitivamente las puertas para que el movimiento tuviera el aval para presentar candidatos a los comicios electorales de este año**.

Para Rojas, esta decisión fue solo un asunto de interpretación, "porque la norma indica que para tener personería jurídica, un movimiento o partido político debe tener representación en Senado y Cámara, y Colombia Humana tiene senador y representante a la cámara". Pero la norma, en su interpretación más literal establece que para tener personería un movimiento debe llegar al Congreso en el marco de las elecciones legislativas, y tanto Angela María Robledo, como Gustavo Petro lo hicieron gracias al Estatuto de Oposición.

Sin embargo, Colombia Humana se adelantó a la decisión y sabiendo la naturaleza de la misma inició el proceso de recolección de firmas para presentar candidatura a la Alcaldía  de Bogotá en el marco de una convergencia ya establecida con la Unión Patriótica, el MAIS y el movimiento Fuerza Ciudadana. **De 50 mil firmas que se necesitan para presentar la candidatura, Rojas sostuvo que ya lograron 52 mil.**

### **La alianza con sectores de oposición: Claves para elecciones regionales** 

El precandidato a la Alcaldía de Bogotá fue enfático al afirmar que **"la única forma de ganar Bogotá por parte de sectores alternativos democráticos es una convergencia"**, razón por la que se acordó hacer una consulta interpartidista el próximo 26 de junio, en la que esperan la participación de candidatos de la Alianza Verde, Polo Democrático, MAIS, Unión Patriótica y Activista. (Le puede interesar: ["Los tres mecanismos con los que la Colombia Humana defenderá su personería jurídica"](https://archivo.contagioradio.com/los-3-mecanismos-con-los-que-la-colombia-humana-defendera-su-personeria-juridica/))

En dicha convergencia también esperan lanzar una lista conjunta para definir candidatos al Concejo de Bogotá que sea lo más amplia posible y les permita tener un bloque sólido en la corporación; adicionalmente, Rojas indicó que **el modelo de unidad que esperan poner en funcionamiento en la Capital se repita en todo el país**, permitiendo a la oposición avanzar en las corporaciones públicas y los cargos de elección popular.

<iframe id="audio_33629635" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33629635_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
