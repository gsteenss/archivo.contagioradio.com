Title: Paramilitares de las AGC provocan desplazamiento masivo en el Alto Baudó
Date: 2016-02-26 17:32
Category: DDHH, Entrevistas
Tags: Alto Baudó, Autodefensas Gaitanistas de Colombia, Desplazamiento, Paramilitarismo
Slug: paramilitares-de-las-agc-provocan-desplazamiento-masivo-en-el-alto-baudo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/alto-baudo-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: altobaudo-choco.gov] 

<iframe src="http://co.ivoox.com/es/player_ek_10612636_2_1.html?data=kpWjk5ead5ehhpywj5aVaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncbHV08bay9HNuMLmxtiYxsqPsMLnjKa0pZDUttDq0MjO0JDIqdTkzcbnw9LNqc%2Fo0JDaw9jNutChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
 

###### [26 Feb 2016]

Por lo menos 300 paramilitares de las [Autodefensas Gaitanistas de Colombia](https://archivo.contagioradio.com/?s=gaitanistas) están desarrollando una fuerte **incursión en el municipio del Alto Baudó desde el pasado 25 de Febrero y han provocado el desplazamiento de cerca de 200 personas hacia la cabecera municipal**. Las víctimas pertenecen a 4 comunidades afrodescendientes y la comunidad indígena de Geandó, sin embargo, no todas ellas han llegado a la cabecera municipal sino que se han resguardado en otras comunidades.

El señor Ernesto Roa, vicepresidente del **[Coordinador Nacional Agrario](https://archivo.contagioradio.com/en-asamblea-general-la-cumbre-agraria-decide-si-hay-nuevo-paro-nacional/)**, afirma que en la zona de Apartadó, hay un retén paramilitar permanente sobre el Río Baudó con, por lo menos, **40 hombres armados y visibles en una zona bajo jurisdicción de la Fuerza de Tarea Titán y la Brigada XV del Ejército**. Según Roa el resto de la tropa paramilitar se adentró en las comunidades sobre las bocas de otros ríos y obligó al desplazamiento a las comunidades sin que, hasta el momento, se conozca de algún tipo de reacción por parte de las FFMM.

Hasta el momento **la emergencia ha sido atendida por integrantes del movimiento social CNA, la Minga y la Alcaldía Municipal,** mientras que la Defensoría del Pueblo y las propias FFMM afirman que están haciendo todo lo humanamente posible, sin embargo la situación humanitaria podría agravarse porque siguen llegando campesinos a la cabecera municipal y no hay recursos para atenderlos.

Roa, las organizaciones sociales y las comunidades afectadas están exigiendo la realización de una **Misión Humanitaria urgente**, para verificar la situación de las comunidades abandonadas y establecer el número concreto de víctimas, así como verificar los daños causados en las casas e intentar la recuperación de algunos enceres necesarios para la sobrevivencia de la población desplazada.
