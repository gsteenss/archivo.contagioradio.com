Title: Independenzia Fezt: Arte, música y cine por la resistencia y la reconciliación
Date: 2018-12-06 12:48
Author: AdminContagio
Category: Cultura, Otra Mirada
Tags: Cine, independencia fest, Música
Slug: independenzia-fest-arte-cultura-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/46983759_280489902652538_2142791928805064704_n-533x800-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Independencia Fezt 

###### 6 Dic 2018 

En el marco del Festival Independenzia Fezt, que se adelanta en Bogotá, tendrán lugar varios **espacios de diálogo y muestras culturales**, que contarán con la participación de artistas nacionales e internacionales, reunidos al rededor de la **cultura para la paz y la reconciliación en Colombia**.

![46983759\_280489902652538\_2142791928805064704\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/46983759_280489902652538_2142791928805064704_n-533x800.png){.alignnone .size-medium .wp-image-58946 .aligncenter width="533" height="800"}

El jueves 6 de diciembre se adelantaran dos conversatorios, el primero desde las 2 p.m. **"Sacar la voz, mujeres, arte y resistencia"**, con la participación de la cantante chilena **Ana Tijoux**; la Senadora por el partido FARC **Sandra Ramírez**, y la actriz y directora de teatro **Paola Guarnizo**. El encuentro finaliza con la proyección del documental "**Nunca invisibles, mujeres farianas, adiós a la guerra"** en Casa Tea, calle 19 \#4-71 local 405, con aporte voluntario o un juguete no sexista para niños de las ETCR.

![47475648\_191172971826123\_8619033944618172416\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/47475648_191172971826123_8619033944618172416_n-533x800.png){.alignnone .size-medium .wp-image-58947 .aligncenter width="533" height="800"}

En el segundo conversatorio titulado **"Combat rockers, la música y los procesos de resistencia"** se encontrarán desde las 4 p.m. **Carlos Lugo, Martin Batalla y Edson Velandia**, con la moderación de **Alex Arce**, integrante de La Servera Matacera, para compartir sus experiencias sobre el tema, en el **Centro Cultural Gabriel García Márquez**.

Al finalizar el espacio de diálogo, en el mismo auditorio Rogelio Salmona, se proyectará la producción animada **"Black is Beltza"** de **Fermin Muguruza**, cantante, instrumentista, productor musical y uno de los músicos más representativos del rock radical vasco, ex integrante de la banda Kortatu y Negu Gorriak, el valor de la entrada es de 10.000 pesos (Le puede interesar: [Fotogramas en resistencia: un Festival de cine por la memoria y la paz](https://archivo.contagioradio.com/fotogramas-resistencia-cine-memoria-paz/))

![47067097\_1921307964845626\_4689352848343826432\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/47067097_1921307964845626_4689352848343826432_n-502x264.jpg){.alignnone .size-medium .wp-image-58948 .aligncenter width="502" height="264"}

El viernes 7, día del cierre del evento, desde las 3 de la tarde se adelantara la **Asamblea de trabajadores de la cultura** sobre la ley naranja, privatización de la cultura y afectación de la labor creativa; y desde las 7 p.m. el cierre del festival con el concierto que reúne a los artistas **Ana Tijoux, Los Hispanos, Frente Cumbiero, Fermin Muguruza y Skartel, BlackEsteban Martin Batalla, La severa Matacera** en el Auditorio Mayor Calle 23 \#6-19.
