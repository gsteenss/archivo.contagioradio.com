Title: ¿Por qué las mujeres nos deberíamos sentir orgullosas si nos dicen brujas?
Date: 2017-11-29 14:09
Category: Libertades Sonoras
Tags: brujas, feminismo, Libertades Sonoras
Slug: mujeres_brujas_inquisicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/brujas-e1511982423890.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel de la Riva] 

###### [29 Nov 2017] 

Los mitos, leyendas, comentarios despectivos al rededor del términos "bruja" son variados, no obstante hay una historia frente a ese hecho, que tiene que ver con las primeras luchas de las mujeres que decidieron no seguir las normas que se dictaminaban por parte del establecimiento en la época medieval. Feministas como Silvia Federicci aseguran que esta época es un elemento fundacional del capitalismo y que supone el nacimiento de la mujer sumisa y domesticada.

Justamente para reivindicar esos elementos y la memoria de las más de **nueve millones de mujeres que fueron asesinadas al ser tildadas de brujas,** hablamos en \#LibertadesSonoras sobre el tema.

De acuerdo con historiadoras, lmuchas mujeres fueron víctimas de un genocidio en Europa y Estados Unidos durante los sigos XVI y XVII, acusadas de brujería y en ese sentido el colectivo Witch señala que "**la historia oculta de la liberación de las mujeres comenzó con brujas y gitanas,** porque son las más antiguas guerrilleras y luchadoras de la resistencia".

<iframe id="audio_22358854" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22358854_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div>

</div>
