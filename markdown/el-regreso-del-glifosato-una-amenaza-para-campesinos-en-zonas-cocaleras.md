Title: El regreso del glifosato, una amenaza para campesinos en zonas cocaleras
Date: 2019-06-19 16:49
Author: CtgAdm
Category: Ambiente, DDHH
Tags: fumigaciones aéreas, Glifosato, PNIS
Slug: el-regreso-del-glifosato-una-amenaza-para-campesinos-en-zonas-cocaleras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Glifosato-Putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Comunidades en zonas cocaleras rechazaron la reciente decisión del Gobierno de reanudar el uso del herbicida glifosato en fumigaciones aéreas, que comenzará el próximo mes en las regiones de Putumayo, Catatumbo y Nariño. Comunidades de estas zonas aseguraron que esta medida representa **otro incumplimiento por parte del Gobierno con los programas de sustitución de cultivos de uso ilícito**, como quedaron estipulados en el Acuerdo de Paz.

<iframe src="https://co.ivoox.com/es/player_ek_37358303_2_1.html?data=lJygl52XdJShhpywj5WaaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncaPVzdvW0NSPlNDg0IqfpZDas8TZ09SYxsqPp9bg1c7jw8nTtsbnjMnSjajTp8Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según Balvino Polo, vocero de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana (COCCAM), este anuncio del Ministro de Defensa Guillermo Botero ha sido recibida por las comunidades productoras de cultivos de uso ilícito como "un baldado de agua fría" pues ya son **alrededor de 90.000 familias que se han inscrito al Programa Nacional Integral de Sustitución de Cultivos Ilícitos** (PNIS) y esperan pagos por parte del Gobierno.

Más de el 90% de quienes se acogieron han cumplido con la erradicación, sin embargo al menos de 40.000 familias no han recibido un primer pago del Gobierno, según un estudio de las Naciones Unidas del 2018. "El incumplimiento ha sido por parte del Gobierno mientras los campesinos manifestamos toda la voluntad de sustituir los cultivos", afirmó el vocero.

En ese sentido, Polo lamentó la postura gubernamental que "sigue creyendo que la única solución para contrarrestar los cultivos de uso ilícito es a través de la erradicación forzosa o la aspersión aérea como es en este caso específico". (Le puede interesar: ["La sustitución es cambiar una mata por otra, pero el Estado no lo está haciendo: campesinos de Putumayo"](https://archivo.contagioradio.com/la-sustitucion-es-cambiar-una-mata-por-otra-pero-el-estado-no-lo-esta-haciendo-campesinos-de-putumayo/))

### **Los impactos graves del glifosato para campesinos en zonas cocaleras**

Según el vocero de COCCAM, el regreso del glifosato implica el desplazamiento forzado de comunidades, escapando de los impactos nocivos de las aspersiones aéreas. Un estudio de la Organización Mundial de la Salud (OMS) concluyó en 2015 que el glifosato probablemente produce cáncer en las personas expuestas al químico. Por su parte, la Universidad de los Andes encontró que la exposición al herbicida utilizado en la aspersión aérea de cultivos de coca aumenta la probabilidad de sufrir trastornos en la piel y en algunos casos abortos.

Asísmismo, el glifosato también ha demostrado causar impactos graves para los animales. Un estudio reciente de la Universidad Nacional demostró que para los peces nativos de Colombia, el glifosato causa alteraciones en el sistema nervioso, dificultades respiratorias y para algunas especies la muerte.

<iframe src="https://co.ivoox.com/es/player_ek_37358403_2_1.html?data=lJygl52YdJShhpywj5WdaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncbPdxMbfxtSPmsLmyMbgh5enb8bs0crf1tSPqc-f1craw5DIqYza1tLWycbHrdDixtiY25DUs82ZpJiSo6mRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Ricardo Vargas, miembro de la Comisión Andina y experto en tema de fumigaciones y políticas antidrogas, aseguró que las aspersiones aéreas no son exactas y en el pasado han afectado cultivos alternativos, apoyados por organizaciones internacionales como Cooperativa Alemana y USAID.

A pesar de que el presidente Iván Duque manifestó recientemente que las fumigaciones se podrían realizar de manera responsable con el ambiente y las comunidades, Vargas sostuvo que no hay ningún estudio independiente que evidencie la existencia de alguna técnica de aspersión que mitigue los efectos nocivos del químico.

### **Acuerdos con las comunidades**

Desde las regiones, la organización COCCAM convocará a reuniones con las administraciones de los municipios y departamentos para llegar a acuerdos que impidan las fumigaciones con glifosato en sus territorios. Además, denunciarán esta medida durante una visita a la Unión Europea en septiembre, durante la cual se hablará sobre la implementación del Acuerdo de Paz.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
