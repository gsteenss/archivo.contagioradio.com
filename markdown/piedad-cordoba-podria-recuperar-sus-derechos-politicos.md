Title: Piedad Córdoba podría recuperar sus derechos políticos
Date: 2016-08-09 12:57
Category: Política
Tags: piedad cordoba, procuraduria, Sanción disciplinaria
Slug: piedad-cordoba-podria-recuperar-sus-derechos-politicos
Status: published

###### [Foto: eltestigo] 

###### [9 Ago 2016] 

En Octubre de 2010 el Procurador Alejandro Ordóñez destituyó a la senadora liberal por 18 años en el ejercicio de la función pública, teniendo en cuenta pruebas que ya habían sido declaradas ilegales por parte de la Corte Suprema de Justicia. En ese entonces la senado instauró un recurso de súplica ante el **Consejo de Estado que, después de 6 años de trámite, encontró razones suficientes para levantar la sanción.**

Aunque aún hay una sanción por supuesto financiamiento de campañas políticas contra la líder política que la condena con 14 años de imposibilidad para la función pública, también se está a la espera de una decisión por parte de los tribunales. **Si se llegara a fallar a favor de la ex senadora tendría que estudiarse la posibilidad de que su curul fuese restituida**.

El próximo 14 de Septiembre se realizará una audiencia en el mismo Consejo de Estado en el que se presentarán los argumentos de la defensa de la Ex senadora Piedad Córdoba con miras a establecer si hubo o no una conducta ilegal en el proceso por supuesta financiación de una campaña política en 2010.

### **La anulación como "pruebas" por parte de la Corte Suprema de Justicia del material recopilado en la Operación "Fenix" en Ecuador** 

En un comunicado de la Corte leído el 25 de Mayo de 2011 la Corte reiteró que las pruebas usadas en varios procesos, entre ellos el de Piedad Cordoba y Wilson Borja carecen de validez porque no cumplieron los requisitos de la cadena de custodia ni los procedimientos requeridos para la recopilación de pruebas en otros países. La decisión fue tomada en Agosto de 2008.

*1.- La Corporación se abstuvo de hacer calificaciones relacionadas con los aspectos político-militares de la denominada "Operación Fénix" en la cual fue abatido el guerrillero Luis Edgar Devia Silva, alias "Raúl Reyes", centrando su estudio en lo estrictamente jurídico.*  
* *  
*2.- La decisión partió de reconocer que los medios electrónicos que contenían información alusiva a las actividades delictivas del abatido jefe de la insurgencia, fueron encontrados en territorio de la República del Ecuador por el Comando de Operaciones Especiales "COPES" de la Policía Nacional de Colombia, y trasladados a territorio nacional.*  
* *  
*3.- Dentro de este contexto, la Corte precisó que por tratarse de pruebas recogidas en el extranjero, su legalidad estaba condicionada al cumplimiento de los protocolos exigidos en estos casos por la normatividad interna y los convenios internacionales en materia probatoria.*  
* *  
*4.- Señaló igualmente, que dichos presupuestos no fueron acatados, toda vez que en la recolección de la prueba no se siguieron los procedimientos establecidos al efecto por el Código de Procedimiento Penal Colombiano, ni por el Convenio de Cooperación Judicial y asistencia Mutua en Materia Penal, celebrado entre los gobiernos de Colombia y Ecuador, ratificado por el Congreso de la República mediante la Ley 519 del 4 de agosto de 1999.*

### **Las razones de la destitución según la procuraduría**

En el fallo emitido en Octubre de 2010 por parte de la procuraduria se lee...  
*"Está comprobado que la Senadora instó a ese grupo para que fuera hostil contra miembros de partidos políticos y servidores públicos, acordó estrechar relaciones con miras a apoyar un nuevo gobierno con la ayuda de gobiernos de otros países, emitió consejos al grupo subversivo frente al envío de videos de personas retenidas y la entrega de pruebas de vida de los secuestrados a gobiernos extranjeros, dio información a las Farc sobre asuntos diferentes a los relacionados con la liberación de los secuestrados, concedió declaraciones en diferentes actos públicos, colaboró con la defensa de algunos ex jefes y miembros del grupo guerrillero en procesos judiciales seguidos contra ellos y ejerció actos de promoción para favorecer a las Farc"*
