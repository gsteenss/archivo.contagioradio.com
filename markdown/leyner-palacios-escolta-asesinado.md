Title: Asesinan a Arley Chalá, jefe de escoltas del lider Leyner Palacios
Date: 2020-03-04 18:03
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinatos, Bojaya, Leyner Palacios, Masacre de Bojayá
Slug: leyner-palacios-escolta-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Leyner.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En la tarde de este miercoles 4 de marzo, diversas fuentes informaron sobre el asesinato de Arley Hernan Chalá Rentería, quien se desempeñaba como escolta del lider Leyner Palacios que recientemente tuvo que desplazarse forzadamente del departamento del Chocó por amenazas en su contra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las primeras informaciones, Arley Chalá, de 24 años de edad, **fue interceptado en su lugar de residencia, en el barrio La Selva de la ciudad de Cali**, donde falleció debido a la gravedad de las heridas. Chalá estaba adscrito a la empresa de seguridad Guardianes, desde la que prestaba sus servicios como coordinador de seguridad de Palacios, que a su vez se desempeña como secretario de la [CIVP](https://verdadpacifico.org/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Extraoficialmente se ha conocido que en el hecho habría sido cometido por mujeres. Sin embargo hasta el momento no hay una hipótesis oficial de lo ocurrido en contra de Chalar.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Asesinato de Aarley Chalá es un claro mensaje contra Leyner Palacios
--------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para allegados al lider social, e**sta situación es un claro mensaje en contra de Leyner Palacios** dadas las constantes amenazas que ha recibido luego de denunciar la grave crisis humanitaria que afronta Bojayá y en donde se ha denunciado la connivencia de las FFMM con los grupos de tipo paramilitar que operan en la zona. Le puede interesar: [FFMM piden información a líderes sobre operaciones denunciadas](https://archivo.contagioradio.com/derecho-de-peticion-ejercito-exige-que-lideres-demostrar-denuncias/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente Organizaciones de DDHH expresaron su solidaridad con los familiares de Arley Chalá y exigieron investigaciones prontas y efectivas que permitan esclarecer el asesinato, así mismo se manifestaron en solidaridad con la familia de Leyner Palacios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que Palacios fue víctima de una serie de amenazas luego de denunciar estos hechos, además las propias fuerzas militares le pidieron, mediante un derecho de petición que diera la información completa en torno a las unidades militares que tenían relaciones con los grupos paramilitares que operan en la región y a quienes había denunciado. Lea también: [El gobierno está del lado contrario a la ciudadanía: Arzobispo de Cali](https://archivo.contagioradio.com/el-gobierno-esta-del-lado-contrario-de-la-ciudadania-arzobispo-de-cali/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente cabe resaltar que las autodenominadas AGC aseguraron, a través de un video publicado el pasado 15 de febrero, que no son los responsables de los asesinatos de líderes sociales y tampoco de amenazas, razón por la cual se hace más urgente el esclarecimiento de este tipo de acciones en contra de líderes sociales y sus entornos.

<!-- /wp:paragraph -->
