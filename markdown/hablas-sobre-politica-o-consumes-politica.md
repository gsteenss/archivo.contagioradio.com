Title: ¿Hablas sobre política o consumes política?
Date: 2016-08-05 15:24
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Medios de información, politica, proceso de paz
Slug: hablas-sobre-politica-o-consumes-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/hablando-de-politica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: portaloaca.com 

#### **Por:[Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 5 Ago 2016 

[Dejémonos de vainas, en las calles de las ciudades uno no escucha hablando a la mayoría de gente del proceso de paz o de “política”, eso no es así, sencillamente porque si nos fijamos bien, la gente va pegada al teléfono celular, va enclaustrada con audífonos o evitando mirarse en el transporte público, y sin más interacción de carro a carro que pitarse o cerrarse.]

[De otro lado, partamos de la realidad más elemental: en Colombia la mayoría de la gente no habla de política. La gente por lo general, elude el tema, o lo que es peor, considera que una opinión sin fundamento es sinónimo de verdad o incluso, “verdad política” … ¡claro! como “valientes” internautas algunos postean, escriben, ponen carita feliz, echan un madrazo, o redactan cuanta pendejada quieren confundir con política o con algo quizá parecido a una reflexión política.  ]

[Al mejor estilo y de los mismos creadores de la frase “]*[yo creo en dios a mi manera]*[” la gente, sin problema alguno, repite como loro en las ciudades aquellas apreciaciones propagandísticas que partidos o gamonales políticos, de la mano de los medios masivos ya han fabricado para que sean consumidas, porque al final, ¡¡sí!! aquí no se habla de política, ¡¡se consume política!!]

[Esa es la raíz del problema tan tremendo que emerge cuando quieres tratar de exponer diferenciadamente que una cosa es la paz, que otra cosa es el fin de una guerra, que una cosa es la violencia y otra cosa son las violencias, que Santos y Uribe son ideológicamente lo mismo, que el Polo no es izquierda, que Raisbeck no es libertario sino neoliberal, que Peñalosa no es político sino empresario y así etc.]

[Al no]*[hablar de política]*[ sino]*[consumir política]*[, la gente se apropia de slogans partidistas, de slogans empresariales, de slogans mediáticos para fundar opiniones, teniendo como punto de partida (valga la redundancia): nada más que la opinión. El agarrón que ha comenzado entre el Sí y el No, ya está por fuera de una lectura juiciosa de los acuerdos porque la mayoría no lee sobre política, consume política. La mayoría no realiza ¡aunque sea! una lectura de lo que fueron procesos de paz en otros países para por lo menos dar cuenta que este, aunque no es perfecto, es mejor que muchos ¡pero no! aquí muchos se quedan en un par de frases o trinos que consideran suficientes para resumir todo lo que es Colombia.   ]

[Los “valerosos y gamberros” internautas al leer esto saldrían diciendo “]*[yo posteo en mi Facebook y en mi twitter lo que quiera]*[” pero bueno, una cosa es eso, y otra que una opinión desprovista de sentido sea considera como una opinión política. Los consumidores de política, seguirán diciendo “]*[esa es la paz de Santos]*[” con tal de perpetrar la falsa idea de que lo que hizo Uribe con “mano firme corazón grande” mandando a la guerra hijos ajenos, fue modélicamente algo diferente.  ]

[Por eso el problema de la paz en Colombia si sigue en manos de los que consumen política, va a terminar, saltándose la comprensión real, y arribando en un tajante Sí o No, como si ese tipo de determinismos fracasados no los hubiésemos vivido ya en nuestra historia… no le suena ¿realistas vs independentistas? ¿centralistas vs federalistas? ¿liberales vs conservadores? ¿colombianos de bien vs terroristas?  Si medianamente se explicara alguno de estos interrogantes, podríamos concluir, que Santos y Uribe son exactamente lo mismo y que ninguno es redentor del país.]

[Hey, pero si]*[“la]* *[paz de Santos]*[” es un engaño, entonces eso de “]*[corazón grande]*[” ¡las ballenas! Porque si Uribe tiene un corazón grande, entonces que se levanten los muertos de la escombrera en Medellín y lo aplaudan; es que tan faltos de intelecto y tan abandonados están los ultraderechistas, que incluso han promovido la falacia de que “]*[Santos es socialista]*[” ¡¡por favor!! ¿quién puede creer esa acusación tan mamerta hecha contra un íntimo amigo de Tony Blair, un auténtico representante de la oligarquía colombiana? Santos es uno más.]

[Con frases poco profundas no se pueden elaborar argumentos claros, por el contrario, eso empuja a pensar cerradamente, y concluir como algunos despistados que “]*[Si Santos es malo, entonces le creo a Uribe]*[” “]*[o que si Santos y Uribe son lo mismo entonces que se caiga un proceso de acuerdo de paz]*[”. Eso es un absurdo. No se pueden mezclar todos los temas para concluir en determinismos oportunistas, nuestra realidad política es compleja, si tan sólo muchos no consumieran política, sino trataran de comprender profundamente a este país, las opiniones casi siempre tendrían el prefijo: DEPENDE.]

[Por lo pronto, mientras se siga consumiendo política, podríamos engañosamente considerar los acuerdos de la Habana como un disco rayado que se puede poner en todas las fiestas, o, en otras palabras, que lo que se habla en la Habana servirá para arreglar y concluir todo lo que sucede en Colombia. ¡Qué mala maña la que ha tomado mucha gente de “pasar por la Habana” todo lo que se le ocurre! Los que elogian excesivamente, siguen creyendo que el acuerdo nos dará hasta para el bus. Los que critican excesivamente, siguen creyendo que el fin de una guerra, es menos importante que sus narcisas costumbres de no aceptar un bien común.]

[Los acuerdos son indiscutiblemente necesarios, pero no alcanzan para ser el marco explicativo o el marco referencial de todos los problemas que tiene Colombia. El problema de Colombia es el modelo económico y político en el cual está inmersa, un modelo que está basado en la fracasada teoría de Friedman, un modelo que se imparte en algunas universidades de Colombia y que se presenta como infalible, creando ficciones en los estudiantes para cubrir las frustraciones que emergen cuando se niega sistemáticamente la posibilidad de comprender el mundo social; un modelo que finalmente, no se está negociando en la Habana… No obstante, y a pesar de la dura contradicción, eso no quiere decir que los acuerdos no sirvan… ¿si ve? … depende.]

[Al final, eso quiere Santos: hace creer que con el acuerdo puede convencer de que el problema de Colombia era la guerra. Eso quiere Uribe: hacer creer que, oponiéndose al acuerdo, su figura criminal puede limpiarse. No… ya basta, dejemos de consumir política. Si una de tantas guerras tiene la probabilidad de ser acabada, pues que se acabe, de seguro eso no tiene nada de malo. Pero por lo pronto, vote el acuerdo, vote pensando políticamente y no consumiendo cuanta frase sale por los medios masivos de información, que luego de eso, si no estábamos balbuceando, entonces seguiremos, como siempre, trabajando para cambiar el país, para construir la paz… eso es harina de otro costal.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
