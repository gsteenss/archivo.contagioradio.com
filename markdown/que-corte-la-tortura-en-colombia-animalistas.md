Title: Que la Corte, corte la tortura en Colombia: Animalistas
Date: 2017-01-26 13:36
Category: Animales, Nacional
Tags: Animales, Animalistas, Natalia Parra, Taurinos, Toreo
Slug: que-corte-la-tortura-en-colombia-animalistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-No-a-las-corridas-de-toros-18-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [26 Ene. 2017] 

Durante esta semana la Corte Constitucional debate **dos ponencias que podrían acabar o no a las corridas de toros en Colombia**. La primera manifiesta que las corridas deben mantenerse, y de ese modo respetar lo dicho en 2010 por la misma Corte y la segunda, apoyada por los antitaurinos, asegura que las corridas de toros deben ser desmontadas paulatinamente, dado que no representan a la mayoría de los ciudadanos.

Estas ponencias son parte de **una demanda contra el maltrato de los animales, que pretenden penalizar "el rejoneo, coleo, las corridas de toros, novilladas, corralejas,** entre otros actos" que causen sufrimiento a los animales.

Diversas organizaciones siguen esperanzadas en que se dé una noticia próximamente, así lo contó para Contagio Radio **Natalia Parra directora de la Plataforma Alto** “tenemos una alta expectativa, estamos muy pendientes, pues lo que tenemos en nuestro país es una ley incoherente que plantea que está mal el maltrato animal, pero por otro si se trata de espectáculos crueles no está tan mal, o son permitidos”.

Desde el movimiento animalista han estado animando a través de las redes sociales esta posibilidad que se abre en la Corte Constitucional, invitando en Twitter a instar a dicha institución a cortar con este tipo de maltrato “**estamos moviendo un hashtag utilizando la palabra corte** pero en términos de cortar, no de alta Corte y es **\#QueCorteALaTortura".**

### **Continuará la movilización** 

Luego de un Tweet hecho por Enrique Peñalosa, Alcalde de Bogotá en el que aseguraba que “debido a la violencia que se presentó, el próximo domingo no permitiremos manifestaciones en las inmediaciones de la Plaza Santamaría”, Natalia Parra dijo que las actividades de movilización continuarán en la capital.

**“Hemos considerado un poco ofensivas las declaraciones del Alcalde en términos del derecho que tenemos los ciudadanos para movilizarnos** y por eso vamos a estar de nuevo este domingo en la Plaza La Santamaría hasta donde podamos ubicarnos”: Le puede interesar: [“Los animalistas somos pacifistas”](https://archivo.contagioradio.com/los-animalistas-somos-pacifistas-35020/)

Los movimientos animalistas esta vez estarán abanderando **el slogan “si a la lectura, no a la tortura” según Natalia la actividad tiene como propósito “Sentarnos a leer**. Ya que nos tildan de violentos pues vamos a estar es en una posición muy zen, muy de Ghandi, leyendo. Así los taurinos pasen y nos digan cosas, vamos a estar en posición pacífica”.

Ante la posibilidad de una infiltración de este acto, Natalia afirmó que al estar todos sentados, leyendo y de manera pacífica en el espacio destinado para éste “sería muy evidente quiénes son los que quieren sabotear el evento”. Le puede interesar: Caracol Radio: [¿Los Antitaurinos son terroristas?](https://archivo.contagioradio.com/caracol-radio-los-antitaurinos-son-terroristas/)

Por último, Natalia calificó de “ofensiva” la actitud de la Fiscalía en la que ha aseveró que estaría pensando en quitar las penas de cárcel en contra de quiénes maltraten a los animales dejando solamente una multa  “esta es la forma en que la Fiscalía se deshace de una carga en lugar de, como entidad, ponerse las pilas y buscar las formas de atender y de cómo penalizar el maltrato animal. **Pedimos que se respete La Ley 1774”.**

Aunque se preveía que este miércoles la Corte diera a conocer su decisión, los tiempos se han retrasado sin embargo, **la fecha límite para que la Corte Constitucional tome una decisión se vencen el próximo 30 de Enero**, de no darse, Natalia asegura que se volverán a movilizar frente a esta institución para exigir una pronta disposición en el tema del maltrato animal.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
