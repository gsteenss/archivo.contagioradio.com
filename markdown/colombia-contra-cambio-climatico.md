Title: Colombia y 90 países del mundo se movilizarán contra al cambio climático
Date: 2018-09-07 14:53
Author: AdminContagio
Category: Ambiente, Movilización
Tags: 8 Septiembre, Ambiente, Hidroituango, marcha
Slug: colombia-contra-cambio-climatico
Status: published

###### [Foto: @350] 

###### [7 Sept 2018] 

Con el objetivo de llamar la atención sobre la necesidad de liderar acciones contra el cambio climático, este sábado **8 de septiembre** se realizará la movilización global **"Únete por el Clima"**, en la que algunas capitales de Colombia se unirán a los 90 países que hacen parte de la iniciativa.

En Bogotá, los capitalinos podrán participaran en el** 'Septimazo climático-Bogotá resiste',** una caminata propuesta desde la preocupación existente por los altos niveles de contaminación del aire en la ciudad, superada únicamente en ese aspecto por Medellín. Actividad que tendrá lugar el sábado desde la**s 2 p.m.,** con un recorrido que va d**esde el Parque Nacional hasta el Parque de Los Hippies,** ubicado en la carrera. 7ma con calle 61.

Bajo el lema 'Por un mundo sin represas y sin combustibles fósiles', diferentes comunidades del Bajo Cauca antioqueño se congregaran haciendo especial referencia a los conflictos socioambientales generados por **Hidroituango**. Mientras que en Nariño, se realizará la **marcha carnaval de Pasto**, saliendo del Colegio Champagnat y culminando en el Parque Nariño.

A la movilización, que sirve como antesala de la **Cumbre Global de Acción climática del 12 de septiembre en San Francisco, California;** se unirán las capitales de **Atlántico, Meta, Valle del Cauca y Cauca** con actividades que se realizarán en alcaldías, gobernaciones y universidades. (Le puede interesar: ["Campañas políticas deben hablar del cambio climático en el Foro Al Clima con el País"](https://archivo.contagioradio.com/campanas-politicas-deben-hablar-del-cambio-climatico-en-el-foro-al-clima-con-el-pais/))

La movilización a nivel global cuenta con más de 800 acciones confirmadas, con la participación de cerca de **320 grupos** y organizaciones ambientales con las que se buscan visibilizar los efectos nocivos del cambio climático. (Le puede interesar:["Diálogo de Talanoa, una oportunidad para exigir acciones efectivas frente al cambio climático"](https://archivo.contagioradio.com/53671/))

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
