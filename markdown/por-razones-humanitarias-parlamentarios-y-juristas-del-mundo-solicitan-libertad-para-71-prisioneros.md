Title: Por razones humanitarias, parlamentarios y juristas del mundo solicitan libertad para 71 prisioneros
Date: 2015-07-24 09:02
Category: DDHH, Nacional
Tags: adolfo peres esquivel, angela david, benjamin defise, bernardo soares, carcelario, carta, christian juhl, coalicion, ernesto, iva pinheiro, james jordan, larga, marcelo chalreo, marie bundgaard hagensen, mariposas, mark burton, movimiento nacional, noam chomsky, politicos, presos, prisioneras, prisioneros, sahar francis, schulman, vida
Slug: por-razones-humanitarias-parlamentarios-y-juristas-del-mundo-solicitan-libertad-para-71-prisioneros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/PARANOTA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [24 jul 2015] 

La solicitud firmada por académicos, activistas por la paz, parlamentarios, juristas y defensores de Derechos Humanos en el mundo, tiene como objetivo solicitar la libertad por condiciones humanitarias para 71 hombres y mujeres, para quienes sus condiciones de salud se han convertido en un elemento más de tortura a interior de las prisiones Colombianas, como es el caso de Jhon Fredy Gomez, procesado y condenado mientras se encontraba en estado de inconsciencia como efecto del bombardeo tras el cual fue capturado; o Maira Alejandra Hernández, quien cumple 4 años con una fractura en su brazo que no ha sido atendida por las entidades responsables.

A continuación reproducimos

 

####  

#### ***CARTA HUMANITARIA POR LA LIBERTAD DE 71 PRESOS POLÍTICOS COLOMBIANOS*** 

*Julio de 2015*

***Juan Manuel Santos,** Presidente de Colombia*

***Emilio Álvarez Icaza Longoria**, Secretario Ejecutivo de la Comisión Interamericana de Derechos Humanos*

***Ban Ki-Moon,** Secretario General de las Naciones Unidas*

*Su Santidad Papa **Francisco,** Iglesia Católica Apostólica Romana*

***Olav Fykse Tveit,** Secretario General del Consejo Mundial de Iglesias*

* *

*De nuestra mayor consideración:*

*Esta es una petición basada estrictamente en razones humanitarias. Se trata de **setenta y un mujeres** **y hombres colombianos** encerrados en condiciones de extrema crueldad para sus graves problemas de salud, la mayoría de ellos agravados por la nula o deficiente atención medica que el Estado les brinda.*

*Podríamos haber empezado por hablar de los miles y miles de personas que pueblan del peor modo posible las cárceles de Colombia, condiciones tan indignas que la Corte Constitucional Colombiana habló en 1998 de un “estado de cosas inconstitucionales” y de “hacinamiento crítico”  (y todo se ha deteriorado tanto, que en abril de este año, los magistrados María Victoria Calle, Mauricio González y Luis Guillermo Guerrero, advirtieron que el actual sistema penitenciario es **“indigno, cruel e inhumano”**); pero de la situación general carcelaria se ha hablado mucho y hay muchas gestiones en marcha.  No diremos una palabra de todos ellos.*

*También podríamos mencionar la persistente negativa del gobierno colombiano a consentir lo obvio, la existencia de miles de presos por razones políticas; más aún desde que ha reconocido un conflicto histórico por el cual se llevan adelante conversaciones de paz con una de las fuerzas insurgentes y en la que se discuten infinidad de temas entre los que se incluyen los relacionados con las víctimas, las reparaciones y los modos de hacer justicia.  Una vez más apoyamos la salida negociada y pacífica del conflicto y nos pronunciamos por un pronto y exitoso final de dichas conversaciones que ansiamos traigan una paz sustentable para el pueblo colombiano, que será la paz para toda América Latina.*

*El repaso de las condiciones de supervivencia de las **setenta y un personas** que aquí mencionamos (seguros de que hay otras en iguales o peores condiciones o que la situación de alguna de ellas pudo haber mejorado en estos días) nos exime de cualquier reflexión adicional sobre el tema.*

*Por ninguna razón ser humano alguno en ningún lugar de la tierra merece sufrir lo que sufren estas personas.  En el agravio a lo más elemental de su dignidad humana está agraviada la humanidad toda y en nombre de esa humanidad es que venimos a pedir la urgente libertad de estas personas sin ninguna otra consideración que la defensa de la dignidad humana que no solo es arrasada para los que sufren la situación de tortura permanente que entraña esta situación sino también para quienes se ven llevados a realizar estas acciones deshumanizantes.*

*Mientras se tramite su libertad, que insistimos es urgente y prioritaria, **estamos dispuestos a trasladarnos en el menor tiempo posible a cada uno de los sitios de detención** donde estas personas están alojadas para constatar, junto al gobierno colombiano y representantes de las agencias internacionales de preservación de los derechos humanos, las condiciones que más abajo denunciamos en el anexo que acompañamos.*

*Con el respeto y la consideración que su persona y función ameritan:*

***Adolfo Pérez Esquivel**, Premio Nobel de la Paz (Argentina)*

***Noam** **Chomsky**, Filósofo, Lingüista, Pedagogo, Profesor Emérito del Instituto Tecnológico de Massachusetts y Activista Estadounidense (EUA)*

***Angela Davis**, Filósofa, Pedagoga, Profesora Emérito de la Universidad de California, Activista Afroamericana y contra el Encarcelamiento Masivo (EUA).*

***Christian Juhl**, Parlamentario, Representante de la Comisión de Relaciones Exteriores del Parlamento Danés (Dinamarca)*

***Marcelo Chalreo**, Abogado y Presidente de la Comisión de Derechos Humanos, Colegio de Abogados del Brasil - Sección Rio de Janeiro (Brasil)*

***Mark Burton**, Abogado Defensor de Derechos Humanos, Gremio Nacional de Abogados -National Lawyers Guild- (EUA)*

*** Sahar Francis**, Abogada y Directora de ADDAMEER - Prisoner Support and Human Rights Association/ Apoyo a Prisioneros y Asociación de Derechos Humanos (Palestina)*

*** Jose Ernesto Schulman**, Secretario Nacional de la Liga Argentina por los Derechos del Hombre (Argentina)*

***James Jordan**, Activista de Derechos Humanos, Co-coordinador Alianza por la Justicia Global/ Alliance for Global Jusitice (EUA)*

*** Marie Bundgaard Hagensen**, Activista de Derechos Humanos, integrante del Grupo “Solidaridad con Colombia” (Dinamarca)*

* **Ivan Pinheiro**, Secretario General do PCB - Partido Comunista Brasilero (Brasil)*

* **Bernardo Soares**, Unión de Juventudes Comunistas (Brasil)*

* **Benjamin Defise**, Responsable del Grupo América Latina de Bruselas (Bélgica)*

#### 

#### **LISTADO DE PRESOS POLÍTICOS EN CONDICIONES DE EMERGENCIA EXTREMA CUYA LIBERTAD ES IMPERIOSA** 

####  

####  

#### LA INFORMACIÓN HA SIDO PROPORCIONADA POR LOS ORGANISMOS DE DERECHOS HUMANOS DE COLOMBIA, EN SU INMENSA MAYORÍA LOS CASOS YA HAN SIDO PLANTEADOS ANTE INSTANCIAS INTERNACIONALES, INCLUYENDO LAS NACIONES UNIDAS 

#### **      **

##### **      <u>Penitenciaria de Alta Seguridad “La Tramacúa” de Valledupar</u>** 

##### ** ** 

1.  ##### **OSCAR ELIAS TORDECILLA, TD 4665:** Fue capturado el 2 de enero de 2007 cuando aún se recuperaba de una cirugía de trasplante de córnea, conservando en ese momento algo de visión. Inicialmente fue recluido en la Cárcel de Bellavista (Medellín), donde no recibió atención médica, no se le trasladó a tratamiento postquirúrgico ni se le suministró el medicamento (gotas) recetado. Desde el 13 de abril de 2010, se encuentra recluido en el EPCAMSVAL donde ha tenido algunos controles médicos con especialistas. Desde el año 2012 tiene programada una cirugía, pero no se le ha practicado y los exámenes ordenados por el médico internista tampoco se le han realizado; debido a ello, el preso ha perdido totalmente de la vista, ha bajado de peso y presenta constante lagrimeo en los ojos acompañado de líquido verdoso. Además de la ceguera, el preso está afectado por la amputación de su mano izquierda, y es obligado a convivir con presos calificados con mala conducta, a pesar que el patio donde él se encuentra está destinado para el confinamiento de *discapacitados y tercera edad,* situación que hace que el preso esté en contante estado de zozobra e indefensión. Su integridad física-emocional también se encuentra afectada, refiere depresión y vulnerabilidad, por lo que ha solicitado**traslado por acercamiento familiar** a la ciudad de Medellín, donde se encuentra su familia, pero tampoco ha recibido respuesta.

#####  

2.  ##### **ARIS ALBERTO CARRILLO PINTO, TD 6175:** Capturado el 11 de agosto de 2013, con heridas en su pierna derecha causadas en combate; fue intervención quirúrgicamente y le incorporaron en su pierna un tutor (tornillos), pero esto no mejoró su salud, al contrario, por omisión de tratamiento post operatorio, este preso empezó a padecer fuertes dolores, fiebre y drenaje en su pierna de un líquido color amarillo de mal olor, situación que se prolongó por más de un año, por ello, presentó varias quejas e instauró una acción de tutela que fue fallada a su favor, sin embargo, el penal y la empresa de salud no han dado cumplimiento a la tutela, agravando su estado de salud, padeciendo el preso, cuadros febriles, dolor intenso y continuo, y perdida de la funcionalidad de su pierna. el preso fue informado que requiere la amputación de 10 cm de su pierna y recientemente le dieron la orden médica de retiro del tutor y uso de zapato ortopédico para equilibrar la disminución de su pierna derecha, pero tampoco ha sido cumplida esta orden médica. El 3 de febrero de 2015, aproximadamente la 7:00 pm, cuando el preso se encontraba acostado, preparándose para el descanso nocturno, **guardianes del INPEC le vaciaron por debajo de la puerta de su celda un frasco de gas pimienta**, inmediatamente el preso se levantó aturdido con el propósito de protegerse de los efectos del gas, pero en su agonía se cayó de la cama y se lastimó la pierna herida, que enseguida le comenzó a sangrar, el preso a gritos pidió ayuda y atención por enfermería, preguntando sobre qué pasaba, pero no obtuvo respuesta ni le prestaron los primeros auxilios, siendo sometido a pasar el resto de la noche bajo el sufrimiento ocasionado por el intenso dolor en su pierna herida y los efectos del gas. Además del flagelo a su integridad física, el preso se vio afectado con el daño de su caminador, toda vez que su cuerpo cayó sobre éste en el momento del angustioso hecho. Testimonios de varios reclusos comprometen a los guardias **JOSE CARRASCAL y PALLARES,** no obstante de la respuesta de la penitenciaria se ha limitado en afirmar que *“un tubo de gas no cabe por debajo de la puerta”* sin presentar avances para sancionar a los victimarios.

#####  

3.  ##### **EVER SANCHEZ DIAZ, TD 2114, Torre 3:** Padece desgaste de las vértebras 4 y 5, sufre dolores intensos al caminar, al agacharse, al hacer fuerza y al dormir. No puede realizar actividades físicas por el intenso dolor en la columna. Ha sido valorado por médicos y sometido a pruebas de rayos X, pero no ha sido remitido a especialista en ortopedia ni se le ha suministrado copia de su diagnóstico, mientras su estado físico y emocional se deteriora.

##### ** ** 

4.  ##### **ISAÍ MEDINA VERA, TD 5739, Torre 5:** Le fue diagnosticado lesión de hombro izquierdo con fractura en el humero, clavícula, omoplato y costilla. Desde el año 2004 padece ruptura de ligamentos cruzado de la rodilla derecha que lo incapacita debido al fuerte dolor que le produce esta enfermedad, el dolor es más fuerte cuando camina rápido o tiene que subir gradas. Este preso político instauró dos (2) tutelas las cuales reconocieron su derecho a la práctica de la cirugía en la rodilla, desafortunadamente el procedimiento no fue realizado oportunamente, por lo cual más allá de presentar una mejoría, se ocasiona un detrimento en su salud.

##### ** ** 

5.  ##### **CARLOS ALBERTO GUTIERREZ, TD 3011, Torre 9:** Capturado el 24 de diciembre de 2007. Padece la enfermedad de colon irritable, hemorroides y gastritis crónica. Debe estar tomando medicamentos para controlar el dolor que le causa la irritación. También muestra significativa disminución visual. No recibe atención médica oportuna ni dieta adecuada.

##### **<u>Cárcel “La Vega” de Sincelejo – Sucre</u>** 

##### ** ** 

6.  ##### **NANCY ISABEL SALCEDO NAVARRO, TD 12215 – Patio de mujeres:** Capturada en buen estado de salud el 24 de diciembre de 2007, condenada por el delito de Rebelión. Actualmente padece problemas de salud mental, consistente en un cuadro *esquizoide depresivo* *de manejo neurótico*. Ha estado internada en varias oportunidades en este último año en centros psiquiátricos, en la ciudad de Sincelejo, en la Clínica Nuevo Ser y en Barranquilla, en la clínica Reencontrarse. Recientemente estuvo recluida por un largo periodo en la Clínica Nuevo Ser de Sincelejo y trasladada a la Clínica Reencontrarse de la ciudad de Barranquilla. Para su mejoría requiere tomar medicamentos que la sedan y la alejan completamente de la realidad, aun cuando ha presentado leve mejoría su situación de salud puede en algún momento significar un grave riesgo para su vida. Su estado de salud es incompatible con la vida en reclusión.

##### **<u>Cárcel “Las Mercedes” de Montería – Córdoba</u>** 

##### ** ** 

7.  ##### **YAMITH NARVAEZ SABALLET, TD 18789, Patio 2:** Padece de hernia inguinal, no recibe tratamiento médico, está programado para cirugía pero hasta el momento no la han autorizado. Maneja dolor frecuente, sobre todo al orinar. Toma medicamentos para manejo del dolor, pero no lo están suministrando en el centro de reclusión.    ** **

8.  ##### **PEDRO MIGUEL RODELO NOVOA, TD 17378, Patio 6 (Oasis):** Capturado con heridas de guerra en 2011. Al momento de la captura padecía una hernia inguinal y tenía esquirlas en el estómago producto de las heridas de guerra ocasionadas con arma de fuego, por lo que necesitaba cirugía para la extracción de las esquirlas, la cual le fue practicada en 2013 luego insistentes peticiones. Tiene secuelas de cirugía realizada en abdomen, debido a omisión de tratamiento oportuno, teniendo comprometidos varios órganos y parte de los intestinos. Manifiesta que es recurrente un dolor abdominal y que no recibe atención médica.

##### **<u>Cárcel Villa Hermosa de Cali – Valle del Cauca</u>** 

##### ** ** 

9.  ##### **JHON FREDY GÓMEZ:** fue capturado después de un bombardeo efectuado por el Ejército de Colombia en el departamento del Chocó en el año 2011; estuvo en estado de inconciencia durante 5 meses, tiempo en el cual fue procesado y condenado a 5 años de prisión. Durante su reclusión le practicaron 7 operaciones en la cabeza, quedando con heridas en la espalda y en el tórax, los dedos de los pies contraídos, perdida de la memoria, convulsiones periódicas y dificultades para caminar; cumplió 3 años físicos de prisión y descontó pena con estudio y trabajo hasta lograr que le concedieran libertad condicional en septiembre de 2014, sin embargo, el INPEC se ha desentendido de su obligación de prestarle atención médica y darle continuidad a su tratamiento, empeorando su estado de salud, puesto que su familia es de escasos recursos y no tiene como ofrecerle continuidad del tratamiento.

##### ** ** 

10. ##### **MARCO ANTONIO TÁLAGA VELASCO:** Capturado el 11 de enero del año 2013, condenado a 25 años de prisión; tiene una incapacidad física, le amputaron la mano derecha y los dedos anular, corazón, índice y pulgar de la mano izquierda. Su estado actual de salud mental está afectado, como consecuencia del encierro ha presentado profundos trastornos de carácter depresivo, llegando incluso a manifestar intenciones de suicidio.

##### ** ** 

11. ##### **SABEL CASTILLO:** Capturado en 2004 y trasladado en múltiples ocasiones por cárceles de Cali, Popayán y Caldas. En julio de 2012 le diagnosticaron sospecha de glaucoma en ojo izquierdo, confirmado en febrero de 2013 como una alteración glaucomatosa que produce pérdida acelerada de la vista, acompañada de visión borrosa, dolores agudos en los ojos y en la cabeza. Esta situación ha sido empeorada por las pésimas condiciones de higiene en las que se encuentra el penal. Además de estas afecciones, sufre de gastritis crónica y cálculos renales. El INPEC no le brinda la atención ni los medicamentos que necesita y tampoco permite la entrada de los mismos.

<!-- -->

12. ##### **JULIO ENRIQUE MONCAYO**. En marzo cayó gravemente enfermo de neumonía, por ello estuvo hospitalizado dos semanas en el Hospital Universitario del Valle. En este momento está en la cárcel de nuevo, manifiesta que siente mucho dolor y que el INPEC no lo ha llevado a las terapias que ordenaron realizarle en el hospital de la cárcel.

##### **<u>Complejo Penitenciario de Jamundí - COJAM - Valle del Cauca</u>** 

##### ** ** 

13. ##### **GLORIA ÁLVAREZ MESTIZO, TD 1898**: Capturada el 20 mayo de 2013, con heridas de guerra, la más grave de ellas causada por una esquirla de bala que hasta el momento está alojada en la parte frontal derecha de su cerebro. Este fragmento de artefacto le ha producido varios coágulos en el cerebro, que se han manifestado en episodios recurrentes de pérdida de conocimiento y convulsiones. Producto de lo anterior, en tres ocasiones ha estado internada en la Unidad de Cuidados Intensivos del Hospital Universitario del Valle. En agosto de 2013 le practicaron una escanografía neurológica, cuyo resultado mostró que habría sufrido un aneurisma y que tiene alojado en su cerebro un fragmento de metal, lo cual puede ser mortal. El penal no le ha brindado la atención médica ni curaciones a las heridas que presenta. Como consecuencia de la esquirla que tiene en el cerebro, sufre frecuentemente dolores de cabeza muy fuertes, mareos, entumecimiento y hormigueo en las extremidades que le impiden moverse y desarrollar sus tareas cotidianas. Ante esta situación, el penal solo le ha suministrado analgésicos y fenitoma. Se instauró tutela solicitando atención médica en febrero de 2015. En marzo el juez falló a favor de la interna, ordenando atención en un plazo de 48 horas. Hasta el momento no se la han prestado.

##### ** ** 

14. ##### **GLORIA PATRICIA RAMIREZ, TD 853:** Condenada a 55 años de prisión sin existir pruebas que la responsabilicen penalmente. Sin su consentimiento ha sido visitada en varias ocasiones por agentes de la DEA de los EUA que la intimidan y le ofrecen todo tipo de prebendas a cambio de información. Sus familiares luego de visitarla han sido objeto de seguimientos por parte de hombres desconocidos. Antes de la captura estaba en proceso de realizarse una cirugía en el útero por enfermedad, sin embargo, en reclusión no ha recibido ningún tipo de atención a su problema de salud, comprometiendo seriamente su sistema sexual y reproductivo. Se instauró derecho de petición en febrero. Hasta el momento no ha habido respuesta. .

##### ** ** 

15. ##### **MAIRA ALEJANDRA HERNÁNDEZ SÁNCHEZ,** **TD 1761**: Capturada en combate el 22 octubre de 2010, en desarrollo del cual sufrió una fractura en su brazo. Durante los 4 años que lleva en prisión no ha recibido ningún tipo de atención médica y en consecuencia ha perdido totalmente la movilidad del brazo y de la mano, además de soportar cotidianamente fuertes dolores sin lograr atención a ellos. Ha instaurado múltiples recursos y ninguno ha sido suficiente para que el INPEC autorice la operación que requiere. Se instauró derecho de petición en febrero. Hasta el momento no ha habido respuesta.

##### ** ** 

16. ##### **GLORIA DEL CARMEN DORIA GONZALEZ, TD: 851:** Capturada el 27 de octubre de 2010. Ha padecido el aniquilamiento sistemático de sus familiares por parte de grupos paramilitares. El 29 de agosto de 2012 los paramilitares asesinaron a otro de sus hermanos en Riosucio - Chocó y el 27 de septiembre del mismo año asesinaron a su compañero permanente y fue herido otro de sus hermanos, quien falleció posteriormente el 6 de octubre de 2012. El 12 de diciembre de 2012 grupos paramilitares asesinaron a una de sus hermanas en Necocli, Antioquia. Actualmente solo quedan con vida dos hermanas de la prisionera, quienes fueron desplazadas de manera forzada del Chocó por una amenaza de grupos paramilitares quienes les daban 12 horas para salir del pueblo. Estas mujeres sobrevivientes, que están a cargo de los hijos e hijas de los hermanos asesinados, han seguido recibiendo amenazas y no tienen ningún tipo de protección por parte del Estado, por lo que su vida se encuentra en una condición de vulnerabilidad extrema. Se hace urgente y necesario algún tipo de protección para Gloria del Carmen Dorian y su familia.

##### ** ** 

17. ##### **YURANY DELGADO RAMÍREZ, TD: 2880:** Capturada en septiembre de 2014. Desde ese momento tenía varios quistes sobresalientes en los senos que le causaban fuertes dolores. En el tiempo que ha estado en la cárcel, producto del estrés y de las pésimas condiciones, los quistes han crecido y el dolor ha aumentado, siendo tan intenso que a menudo no le permite dormir. Cuando se queja ante el INPEC solo le dan analgésicos. La enfermería del COJAM la remitió a una cita con especialista pero el día en que debía presentarse al centro médico no la llevaron argumentando que no había suficientes guardianes, así que perdió la cita y no ha recibido ninguna atención. En febrero se instauró derecho de petición. Hasta el momento no ha habido respuesta y el dolor se ha incrementado.

##### ** ** 

18. ##### **LUIS ARTURO GARCES BORJA, TD 4133:** Fue capturado el 28 de septiembre de 2000. Al momento de su captura fue víctima de tortura física y psicológica, siendo objeto de aislamiento, incomunicación e interrogatorio durante horas bajo el método de asfixia, colocándole en la cabeza una bolsa plástica untada de jabón. Durante dos días no le permitieron dormir y un coronel llamado Juan Pablo Guerrero insistía en decirle *“colabóreme con la ubicación del campamento y terminamos la entrevista”.* Luego fue legalizado y ha cumplido quince años de presidio, siendo víctima de atentados contra su vida y traslados arbitrarios como represalia por liderar jornadas de protestas y desobediencias pacíficas. Durante su reclusión ha sido asediado por agentes de inteligencia con fines de entregar información sobre la organización a la que pertenece. Ha sido objeto de nuevas torturas en la penitenciaria de San Isidro – Popayán, según su testimonio por órdenes de los tenientes Chacón y Daza. Manifiesta encontrase en riesgo por la existencia de un plan para asesinarle en represalia por su accionar de denuncia y protesta.

##### ** ** 

19. ##### **JHON EDWIN URQUINA, TD: 381:** Por problemas en el hígado le pusieron una malla. Los problemas del hígado ya se solucionaron y requiere urgente que le retiren la malla porque le está oprimiendo el estómago. Envió un derecho de petición en abril pero no ha habido respuesta.

##### **<u>Establecimiento Penitenciario “San Isidro” de Popayán - Cauca</u>** 

##### ** ** 

20. ##### **DIEGO FERNANDO LÓPEZ JARAMILLO, TD 9085:** Capturado el 4 octubre de 2000. Se encuentra en condición de discapacidad por pérdida de sus manos, tiene una prótesis en su ojo izquierdo a causa de una granada de fragmentación que le destrozó el ojo. Presenta cuadros continuos de hipotermia. Reiteradamente ha solicitado que sus prótesis sean enviadas de cárcel de La Modelo en Bogotá, donde estaba recluido, hacia la cárcel de San Isidro en Popayán, donde lleva casi 4 años, pero no ha recibido respuesta. La prótesis del ojo izquierdo está infectada por ausencia de procedimiento de esterilización adecuado ni cotidiano por parte del personal de salud. Ha solicitado intervención a la Procuraduría Regional del Cauca y otras instancias de control, quienes compulsan oficios ante Comité de Ética Médica u otras instituciones, pero su situación no ha sido resuelta. En repetidas ocasiones se le ha negado el permiso de salida de hasta 72 horas, a pesar que cumplir con los requisitos; adicionalmente, su madre ha sido perseguida por agentes del Estado, por lo que se mantiene oculta por temor a ser objeto militar.

##### ** ** 

21. ##### **MIGUEL ÁNGEL GONZÁLEZ:** capturado en Junio de 2013. Al momento de su captura recibía atención médica en el hospital en Santander de Quilichao, debido a heridas en su pierna ocasionadas con arma de fuego. Cuando fue trasladado hacia el centro carcelario en Popayán, mantenía el proyectil al interior de la pierna y no fue tratado por personal médico. Solo hasta finales del año 2013 recibió atención médica y le tomaron radiografías de la pierna. A pesar de las reiteradas solicitudes de atención medica presentadas ante el INPEC y CAPRECOM, no ha obtenido respuesta y el proyectil todavía se encuentra alojado en su pierna, causándole inmovilidad parcial de la pierna.

<!-- -->

22. ##### **SILVIO ARMANDO NENE MENZA:** capturado con heridas en la pierna derecha ocasionadas en combate. Tiene una fractura en el fémur y se encuentra en silla de ruedas. A mediados de 2013, recibió atención médica en la Clínica La Estancia en Popayán, donde le realizaron una cirugía y le enviaron tratamiento con antibióticos, pero cuando fue trasladado de nuevo al centro carcelario no se le suministró el medicamento, la guardia del INPEC no le prestó el cuidado ni quiso enviarlo a enfermería para su atención, por lo que contrajo una infección en la pierna que complicó su salud y los resultados de la cirugía. Para que recibiera el tratamiento adecuado fue necesaria la intervención de su abogado ante la Procuraduría Regional del Cauca. Le realizaron dos cirugías complementarias. En la actualidad se encuentra con movilidad parcial, no resiste estar mucho tiempo en una misma posición, no recibe tratamiento fisioterapéutico que permita mejora en la movilidad, no cuenta con control médico y presenta desde julio de 2014 fuertes dolores en la pierna, sin que le hagan seguimiento al desarrollo de la operación ni reciba atención médica. Su situación de salud es incompatible con la vida en reclusión.

<!-- -->

23. ##### **OVIDIO DAGUA ESCUÉ**, TD: 11005: Presenta pérdida parcial de la vista por esquirla que lastimó su retina y le ocasionó terigio. Antes de ser capturado fue sometido a varias cirugías. Estando detenido solicitó atención médica y debido a la omisión de respuesta interpuso acción de tutela contra el INPEC y CAPRECOM, la cual fue fallada a su favor, por lo que fue llevado a un médico especialista (oftalmólogo) en la clínica La Estancia donde le ordenaron otra cirugía. A pesar de esto un médico general de CAPRECOM al interior del centro penitenciario le manifestó que no tenía que ser intervenido en cirugía, contradiciendo al especialista de la rama y contradiciendo el fallo de tutela que obligaba a brindar la atención médica necesaria al PRESO. Hasta el momento la perdida de vista sigue en proceso sin recibir tratamiento alguno.

##### ** ** 

24. ##### **JERSON MENZA PUYO, TD 98851:** Presenta dolor de columna por agresión que atribuye a personal del INPEC, padece pérdida de movilidad parcial en el brazo izquierdo y no ha recibido ningún tipo de atención o tratamiento médico.

##### ** ** 

25. ##### **LUCIO GOMEZ BRIÑEZ, TD 8825:** Presenta pérdida progresiva de audición y problemas graves en los ojos que le han generado pérdida de visión. No recibe tratamiento ni atención médica.

<!-- -->

26. ##### **CIVILINO UL SECUE**: Al momento de ser capturado por la guardia indígena del Cabildo de San Francisco, fue golpeado y maltratado por sus captores en el tórax y en el área abdominal, lo que agravó su situación de salud, ya que había sido intervenido quirúrgicamente en el abdomen y había quedado pendiente de una segunda operación donde debía ser puesta una malla especial que recubriera sus órganos del sistema digestivo. Desde esa fecha a la actualidad continúa supurando permanentemente pus por un pequeño orificio que tiene en el abdomen. No recibe ningún tipo de atención o tratamiento médico. En abril dirigió carta a autoridades indígenas donde expresa su disposición de suicidarse por la desatención médica de la que es víctima y por el montaje judicial que le han hecho. Los cabildos indígenas se comprometieron a visitarlo el 23 de abril, pero no cumplieron. Ante ello, el 25 de abril se corta el cuello y es sacado del penal desangrándose. Lo suturaron y lo regresaron a su celda. Ha puesto como plazo máximo para que las autoridades indígenas se dirijan a hablar con él el 29 de abril. 


