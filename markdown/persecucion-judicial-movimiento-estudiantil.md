Title: ¿Vuelve la persecución judicial contra el movimiento estudiantil?
Date: 2019-05-03 17:01
Author: CtgAdm
Category: Educación, Nacional
Tags: Cauca, estudiantes, Fiscalía, Judicialización, Lideres
Slug: persecucion-judicial-movimiento-estudiantil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El pasado jueves 2 de mayo, la Fiscalía General de la Nación informó la captura de Andrés de la Cruz Cerón y Lina Isabel Medina Alegría, por ser presuntamente miembros del movimiento Juventudes M-19, así como de Juan Carlos Cuervo Fernandez por integrar el grupo Trocha Colectiva. Para la Unión de Estudiantes de Educación Superior (UNEES), este hecho es una nueva persecución contra el movimiento estudiantil, y la protesta social.

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw) [\#Fiscalía](https://twitter.com/hashtag/Fiscal%C3%ADa?src=hash&ref_src=twsrc%5Etfw) y [@PoliciaColombia](https://twitter.com/PoliciaColombia?ref_src=twsrc%5Etfw) capturaron hace pocos minutos en [\#Popayán](https://twitter.com/hashtag/Popay%C3%A1n?src=hash&ref_src=twsrc%5Etfw) a Andrés De La Cruz Cerón, señalado como jefe de la organización delictiva JM-19 en el [\#Cauca](https://twitter.com/hashtag/Cauca?src=hash&ref_src=twsrc%5Etfw). Es investigado por enfrentamientos con la fuerza pública durante protestas en la Universidad del Cauca [pic.twitter.com/I6mURpCGjo](https://t.co/I6mURpCGjo)
>
> — Fiscalía Colombia (@FiscaliaCol) [3 de mayo de 2019](https://twitter.com/FiscaliaCol/status/1124102293769596928?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
> Lina Isabel Medina Alegría, presunta integrante de la organización JM-19, y Juan Carlos Cuervo Fernández, presunto integrante del movimiento Trocha Colectiva, fueron capturados en [\#Popayán](https://twitter.com/hashtag/Popay%C3%A1n?src=hash&ref_src=twsrc%5Etfw). [\#Fiscalía](https://twitter.com/hashtag/Fiscal%C3%ADa?src=hash&ref_src=twsrc%5Etfw) les imputará concierto para delinquir, terrorismo, daño en bien ajeno y asonada [pic.twitter.com/l6ducyHZfg](https://t.co/l6ducyHZfg)
>
> — Fiscalía Colombia (@FiscaliaCol) [2 de mayo de 2019](https://twitter.com/FiscaliaCol/status/1124093455754461184?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
Carlos Tautiva, vocero nacional de la UNEES y estudiante de la Universidad del Cauca, explica que las capturas se producen luego del proceso de movilización que se llevó a cabo a final del año pasado y en los primeros meses de este año. Los tres jóvenes serán imputados por concierto para delinquir, terrorismo, daño en bien ajeno y asonada por eventos ocurridos en distintas protestas. De acuerdo a Tautiva, por hechos en los que no está clara su autoría, pues aunque en las protestas hubo desorden público, no se sabe si fue provocado por miembros infiltrados de la Policía.

</p>
### **¿Quiénes son los jóvenes capturados?**

Según la información brindada por Tautiva, y un habitante de Popayán (Cauca) cercano a Juan Carlos Cuervo, el jóven es un egresado de la Universidad del Cauca que fue relacionado con desmanes ocurridos en noviembre de 2018, cuando la Policía levantó el campamento estudiantil que se había establecido en el Parque Caldas de la ciudad. Cuervo es padre de familia de una niña, guardián de semillas y hace parte de una comunidad con la que hace años trabaja en la recuperación de un bosque; adicionalmente, ha sido una persona activa en el movimiento estudiantil.

El egresado de la UniCauca es un reconocido líder y gestor cultural, debido a que fue una de las personas que quiso continuar el festival de música rock llamado Rocksistencia que se realizaba en el centro universitario, razón por la cual fundó (en compañía de otras personas) la organización comunal Trocha Colectiva. En ese sentido la Fiscalía lo acusa de cuatro delitos, **señalando que es "presunto integrante de Trocha Colectiva", como si se tratase de un grupo con actividades ilegales.**

Tautiva indicó que a Andrés de la Cruz se lo acusa de ser cabecilla del JM-19 en Cauca, y a Lina Isabel Medina de ser parte del mismo grupo. Ambos tienen cargos por hechos ocurridos el 15 de febrero de 2019, fecha en la que se desarrolló una jornada de movilización; en el caso de Medina, se la acusa de incitar a la destrucción de un cajero electrónico al interior de la Universidad. (Le puede interesar: ["96 heridos por represión de ESMAD durante paro estudiantil en Popayán"](https://archivo.contagioradio.com/movimiento-estudiantil-del-cauca-uno-de-los-mas-golpeados-por-abusos-de-fuersa-del-esmad/))

### **¿Una cacería de brujas?**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FVozUNEES%2Fposts%2F586101848560780&amp;width=500" width="500" height="274" frameborder="0" scrolling="no"></iframe>

En un comunicado la UNEES señaló su preocupación por una posible persecución contra el movimiento social, y los líderes que participaron de el paro nacional que se desarrolló en 2018; por eso, los estudiantes de la Universidad del Cauca se reunirán en una asamblea próxima para aclarar las situaciones al rededor de estas capturas, y tomar las decisiones a las que haya lugar. Entre tanto, los estudiantes han señalado una matriz para estigmatizar el movimiento estudiantil, mediante el señalamiento de infiltraciones por parte de grupos armados en las universidades públicas.

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw) [\#Fiscalía](https://twitter.com/hashtag/Fiscal%C3%ADa?src=hash&ref_src=twsrc%5Etfw) denuncia ante el Ministerio de Educación infiltración de grupos terroristas en universidades públicas del país [pic.twitter.com/8h422VLUVX](https://t.co/8h422VLUVX)
>
> — Fiscalía Colombia (@FiscaliaCol) [2 de mayo de 2019](https://twitter.com/FiscaliaCol/status/1123989098249760770?ref_src=twsrc%5Etfw)

**Actualización**

El pasado viernes en horas de la noche se llevó a cabo la audiencia de legalización de captura en la que un juez determinó la libertad de los tres jóvenes, al no encontrar pruebas suficientes contra ellos.

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<iframe id="audio_35334929" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35334929_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
