Title: Comunidades del Putumayo insisten en plan de sustitución a pesar de erradicación forzada
Date: 2016-11-18 16:59
Category: Ambiente, Nacional
Tags: Sustitución de cultivos de uso ilícito, zonas de reserva campesina
Slug: comunidades-del-putumayo-insisten-en-plan-de-sustitucion-a-pesar-de-erradicacion-forzad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/ejercito-nacional1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ejercito Nacional] 

###### [18 Nov 2016] 

Campesinos de la zona de reserva Campesina de la Perla amazónica del Putumayo, tuvieron en pasados días una reunión con miembros del gobierno para intentar llegar a un acuerdo frente a **la erradicación de cultivos ilícitos que se viene haciendo el Ejército Nacional en esta zona, sin que se haya implementado todavía el acuerdo de paz**, sin embargo, no se avanzó en ninguna propuesta y por el contrario la erradicación continuará afectado el sustento de muchos campesinos en esta región.

En la reunión los miembros del gobierno propusieron a los campesinos que la erradicación se realizará en un periodo de un año, tiempo insuficiente para que las comunidades puedan hacer la sustitución. De igual forma frente a la producción agrícola que propone el gobierno, los campesinos insisten en que no hay forma de comercialización.

“El gran problema que hay en el Putumayo es uno la problemática de vías, de fácil acceso a la parte rural y  la segunda es la falta de comercialización, aparte de esto en medio de un acuerdo de paz **siguen surgiendo grupos armados paramilitares, que tienen a la gente muy asustada y temerosa**” afirmó Jany Silva, miembro de la Zona de Reserva Campesina de la Perla amazónica.

Esta acción de erradicación de cultivos se está realizando  pese a que en el Acuerdo de Paz se pacta parar con este hecho hasta que no se implemente el punto tres sobre sustitución de cultivos ilícitos, Silva afirma que los militares que están a cargo de esta función les han dicho que **“tienen orden de que todo lo que tenga que ver con cultivos hay que erradicarlos y hablan de judicialización y extensión de dominio a los campesinos”.**

“Nosotros como campesinos y organizaciones sociales siempre hemos estado abiertos y conscientes  en que al tema de cultivos ilícitos hay que darle una solución y somos nosotros lo que hemos propuesto alternativas a estos problemas sociales, **el gobierno es imponente sabiendo que no son la solución al problema**” afirmó Jany Silva y agrego que de implementarse los proyectos del gobierno solo se trasladaría el problema porque no hay una solución de fondo.

Entre tanto las organizaciones sociales y campesinas del Putumayo han expresado que seguirán insistiendo en que se dé una negociación que brinde una solución de fondo al problema y esperarán a que se de la implementación de los Acuerdos de La Habana. Le puede interesar:["Gobierno no podrá imponer si política de sustitución de cultivos ilícitos: César Jerez"](https://archivo.contagioradio.com/gobierno-no-podra-imponer-su-politica-de-sustitucion-de-cultivos-ilicitos-cesar-jerez/)

<iframe id="audio_13820776" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13820776_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
