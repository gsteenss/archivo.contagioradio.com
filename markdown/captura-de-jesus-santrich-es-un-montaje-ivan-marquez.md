Title: Captura de Jesús Santrich es un montaje: Iván Marquez
Date: 2018-04-09 18:11
Category: Paz
Tags: acuerdos de paz, FARC, Jesús Santrich
Slug: captura-de-jesus-santrich-es-un-montaje-ivan-marquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/SANTRICH.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Abr 2018]

La Fiscalía General de la Nación capturó Jesús Santrich, integrante del partido político de la Fuerza Alternativa Revolucionaria del Común, FARC, como parte de un proceso en el marco de una investigación que se adelanta en la Corte de Nueva York. **Iván Marquez ha manifestando a través de la cuenta de twitter de este partido, que el hecho se constituye en un montaje.**

<iframe id="audio_25206150" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_25206150_4_1.html?c1=ff6600"></iframe>

En el proceso de captura, la vivienda de Jesús Santrich también fue allanada. Este hecho iría en contravía de lo pactado en los Acuerdos de Paz de La Habana, en donde quedaban suspendidas las órdenes de extradición. Con la excepción de que se comprobara que una vez realizado el proceso de reincorporación se hubiese cometido actos delictivos.

<iframe id="audio_25206338" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_25206338_4_1.html?c1=ff6600"></iframe>

Iván Marquez, afirmó en la cuenta de Twitter del partido FARC que "Bajo montaje y de forma aleve se realizó captura de [~~@~~**JSantrich\_FARC**](https://twitter.com/JSantrich_FARC){.twitter-atreply .pretty-link .js-nav}. Este es el peor momento que esta atravesando este proceso de [~~\#~~**Paz**](https://twitter.com/hashtag/Paz?src=hash){.twitter-hashtag .pretty-link .js-nav}, el gobierno debe actuar e impedir que montajes jurídicos desemboquen en hechos como este que generan una gran desconfianza"

En esa misma vía, Víctoria Sandino afirmó "Esta es una situación muy delicada y peligrosa. Ratifica una vez más lo que hemos venido denunciando en torno a **la inseguridad jurídica y falta de garantías que tenemos los integrantes del partido Farc**".

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10155353484015812%2F&amp;width=500&amp;show_text=false&amp;appId=894195857389402&amp;height=375" width="500" height="375" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Noticia en desarrollo
