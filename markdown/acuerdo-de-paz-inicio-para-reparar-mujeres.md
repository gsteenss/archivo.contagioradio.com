Title: Acuerdo de Paz puede ser inicio para reparar mujeres en Colombia
Date: 2016-11-25 16:13
Category: Mujer
Tags: mujeres, Oportunidades, paz
Slug: acuerdo-de-paz-inicio-para-reparar-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/161021_11_HomenajeMujeres_1800.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alianza Estratégica ] 

###### [25 Nov. 2016] 

A través del tiempo las mujeres han tenido unos impactos diferenciados y desproporcionados en el conflicto armado. Todos estos han tenido consecuencias muy graves como el desplazamiento, empobrecimiento, la falta de salidas y la revictimización de las mujeres.

Marina Gallego directora de la ruta pacifica de las mujeres aseguró que **“las mujeres tienen entre 4 o 5 hechos victimizantes en su proceso que las ha puesto en mayor vulnerabilidad. Esto ha tenido consecuencias muy graves dadas que la desatención del Estado”**.

En cuanto a las posibilidades de empezar a resarcir los daños que a través del tiempo han sufrido las mujeres, Marina manifestó que la paz es un primer avance “**el acuerdo mismo puede ser muy reparador si se aplica cabalmente**. Ahí hay temas que se pueden volver políticas para las mujeres. El acuerdo coayuda a la reparación. Que se termine el conflicto armado es otra manera de reparar sobre todo en la no repetición”.

Otra de las exigencias que a través del tiempo han realizados las mujeres no solo de Colombia, sino del mundo es **lograr la participación política óptima, que para personas como Marina, deja unos espacios para que las mujeres participen “somos más del 52% y no hay una adecuada representación en las instancias donde se toman las decisiones. Es un asunto de la democracia, las mujeres debemos llegar a cargos públicos. El acuerdo deja curules que podrán ser asumidas por mujeres.**

Finalmente Marina afirma que “**hay que transformar las relaciones culturales entre hombres y mujeres y que nosotras debemos empezar a transformarnos par a no permitirlo. Las mujeres tienen que tener las antenas para no aceptar hombres violentos en su vida y para irse o para no comenzarla. Las mujeres tenemos que decir aquí no me meto. Los hombres no cambian ni los cambiamos”. **Le puede interesar: [Mujeres listas y preparadas para trabajar por la paz](https://archivo.contagioradio.com/mujeres-preparadas-para-la-paz/)

[Historia 25 de Noviembre - Día Internacional de No a la Violencia contra las Mujeres](https://www.scribd.com/document/332297665/Historia-25-de-Noviembre-Dia-Internacional-de-No-a-la-Violencia-contra-las-Mujeres#from_embed "View Historia 25 de Noviembre - Día Internacional de No a la Violencia contra las Mujeres on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_75808" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/332297665/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-zWb9LJghFSChOqOoq4kc&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}
