Title: Estos son los 5 puntos que se firmarán en la Habana con el Cese Bilateral
Date: 2016-06-22 16:05
Category: Nacional, Paz
Tags: Cese Bilateral de Fuego, Conversaciones de paz de la habana, FARC
Slug: estos-son-los-5-puntos-que-se-firmaran-en-la-habana-con-el-cese-bilateral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [22 de Junio] 

Según el abogado Enrique Santiago, mañana en la Habana se firmarán 3 acuerdos de gran envergadura en la discusión entre el gobierno y las FARC. Una de ellas el cese bilateral de fuego que ya ha comenzado su implementación pero que solamente culminaría con la firma del acuerdo final que estaría pronto a anunciarse según los avances que se han tenido. Sin embargo el cese bilateral no es el único punto.

### **Dejación de armas** 

El asesor jurídico de la mesa de conversaciones también revela que dentro del acuerdo, que corresponde al punto tres de la agenda o [fin del conflicto](https://archivo.contagioradio.com/las-claves-del-cese-bilateral-entre-el-gobierno-y-las-farc/), se firmarán dos aspectos más. Uno de ellos es que se ha acordado un mecanismo para la dejación de armas por parte de las FARC, proceso en el que está involucrada directamente la Unión de Naciones Suramericanas, **UNASUR y la Organización de las Naciones Unidas** como veedoras y garantes de la efectividad de las medidas en ese aspecto.

### **Medidas de combate al paramilitarismo** 

Uno de los puntos importantes y que ha causado preocupación en organizaciones sociales y de derechos humanos ha sido la **[persistencia del paramilitarismo](https://archivo.contagioradio.com/?s=paramilitarismo)**. En ese sentido el acuerdo contiene una serie de medidas de combate a esos grupos. Habría un momento de ubicación de esas estructuras y luego la garantía de la presencia de la fuerza pública y desarrollo de operaciones especiales.

Además se crea una misión especial de la fiscalía y el CTI que trabajaría en zonas específicas previamente definidas. Como medida de combate a las estructuras paramilitares se pondría en funcionamiento una comisión especial de alto nivel con organizaciones de la sociedad civil de víctimas y derechos humanos para definir políticas públicas de prevención del reclutamiento de niños, niñas y jóvenes.

### **Garantías de seguridad** 

Se crearía una unidad especial de protección parecida a la UNP pero bajo jurisdicción directa del presidente de la república. Habría operaciones de seguridad conjuntas entre el Estado colombiano y delegados de las FARC para el funcionamiento de esas unidades.

### **Amnistía** 

Este punto incluiría lo definido previamente en el acuerdo sobre los menores en el conflicto que tiene que ver con amnistías amplias para integrantes de las FARC que no estén comprometidos con crímenes de Lesa Humanidad o que por su edad no serían imputables pues se considerarían víctimas. [Tampoco serán amnistiables los crímenes relacionados con la violencia sexual](https://archivo.contagioradio.com/?s=cese+bilateral).

El proceso de juzgamiento de los crímenes imputables estará a cargo de la Jurisdicción Especial para la Paz

### **Implementación de la jurisdicción especial de Paz** 

Se definirá el mecanismo de escogencia de los magistrados, 20 de nacionalidad colombiana y 4 de nacionalidad extrajera que harán parte del tribunal así como los órganos de fiscalía y de la sociedad civil para presentar los casos ante ese escenario.
