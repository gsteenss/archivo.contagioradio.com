Title: Mujeres sí a la paz, violencias ni una más
Date: 2016-11-24 15:44
Category: Mujer, Nacional
Tags: genero, mujer, mujeres, paz, violencia
Slug: mujeres-si-a-la-paz-violencias-ni-una-mas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marcha01.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Primicia Diario ] 

###### [22 Nov 2016] 

El tema de las mujeres fue eje central en el acuerdo de paz que se firmó hoy en Bogotá, pues no se puede desconocer que han sido ellas quienes a través del tiempo han sufrido un alto impacto del conflicto en sus vidas, sus cuerpos, sus territorios.

**El número de víctimas mujeres del conflicto armado son alarmantes. Según cifras de la Campaña ‘Violaciones y otras violencias: Saquen Mi Cuerpo de la Guerra’ desde el 2001 hasta el 2009 en promedio, 54.410 mujeres por año sufrieron violencia sexual en Colombia. Es decir, 149 por día, o 6 mujeres cada hora.**

Además de estas encuestas que realizan diversas entidades y organizaciones, la **Corte Constitucional ha manifestado varias veces que la violencia sexual constituye una ‘práctica sistemática, habitual y generalizada’ en medio del conflicto colombiano.**

Diana López integrante de Fondo Lunaria asegura que hay que reconocer la firma de este nuevo acuerdo de paz sin desconocer que muchas de las “cosas consignadas allí han sido una ganancia de las mujeres. Es una lucha, no ha sido gratis, eso es una lucha de las organizaciones de mujeres a nivel nacional que desde que comenzaron los acuerdos y desde mucho antes vienen trabajando”.

Por su parte, asegura Diana que **los Acuerdos de Paz deben velar “por la prevención y la erradicación de todas las formas de violencia contra la mujer, porque para nosotras es vital construir paz desde allí”.**

En cuanto al tema LGBTI, el Fondo Lunaria en voz de Diana, manifiesta que **en la actualidad la sociedad es la que no ha querido reconocer que las personas esta comunidad son sujetos de derechos** y agrega “también le niegan –la sociedad- derechos a las mujeres, y niega cómo ha sido el conflicto, cómo el conflicto ha afectado de manera diferencial a las mujeres y que ellas son las más afectadas”.

Por último, Diana se refirió a las actividades que se van a realizar en la capital **a propósito del Día Internacional de la Eliminación de la Violencia contra la Mujer** e hizo la invitación “Desde hace más de 6 meses hemos preparado una campaña que se llama \#DéjameEnPaz, que pretende visibilizar el trabajo que vienen construyendo las jóvenes de todas las regiones del país por la paz más allá de los diálogos”.

**En la capital realizarán la presentación en el Gabriel García Márquez un evento en el cual expondrán las violencias que más sufren las mujeres jóvenes en Colombia y desde la 1 de la tarde desde el Planetario Distrital cientos de mujeres y hombres se movilizarán bajo la consigna [*mujeres si a la paz, violencias ni una más*] hacia la Plaza de Bolívar para conmemorar esta fecha al son de música y arte. **Le puede interesar: [Durante el 2016, 100 mil mujeres han sido violentadas en Colombia](https://archivo.contagioradio.com/2016-100-mil-mujeres-han-sido-violentadas-colombia/)

**Actividades en Ibague:  Plaza de Bolívar 6 p.m**

**Actividades en Cartagena : Iglesia San Pedro Claver - 4 p.m **

**Actividades Bogotá: Planetario 1:30 p.m para marchar hasta la Plaza de Bolívar**

**Actividades en Puerto Colombia, Atlántico: Estación Ferrocarril de Bolívar - 6:30 p.m **

**Actividades en Cali: Estación Meléndez y Estación Caldas -  3 p.m **

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)
