Title: “Es momento de revivir la unidad de los trabajadores estatales” FECODE
Date: 2015-03-13 18:23
Author: CtgAdm
Category: Movilización, Nacional
Tags: CUT, fecode, Movilización del Magisterio, Salario Mínimo en Colombia, Trabajadores
Slug: es-momento-de-revivir-la-unidad-de-los-trabajadores-estatales-fecode
Status: published

###### Foto: adida.org 

<iframe src="http://www.ivoox.com/player_ek_4211755_2_1.html?data=lZeek5yZeY6ZmKiak5WJd6KlkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8bi1dfOzsrXb8XZjNnfw8fFrsLY0NfS1ZDWqcLgyt%2FO1Iqnd4a1ktOYz9Tarc3d28bQy4qnd4a2lNOYx9GPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Tarsicio Mora] 

El pago de prestaciones sociales, incremento y nivelación salarial, seguro de salud, y el nuevo estatuto docente, son algunas de las exigencias que distintos sectores gremiales que junto con FECODE reclaman al gobierno nacional. Para impulsar estos reclamos, los gremios de trabajadores y educadores convocan a una movilización el 19 de marzo.

32 sindicatos de trabajadores afiliados a la Federación Colombiana de Educadores, empleados del sector salud, transportadores, y estudiantes universitarios, presentarán al gobierno este pliego de peticiones que desde el año 2013 han venido exigiendo sin cumplimiento por parte del mismo.

Tarcisio Mora, responsable de DDHH de la organización, afirma que el Plan Nacional de Desarrollo, “es un portafolio para garantizar que las multinacionales del país y el capital extranjero se puedan beneficiar”, y que no es ninguna solución para resolver los problemas del sector de trabajadores.

Mora agrega que, a pesar de estar instalada una mesa de diálogo por parte del gobierno con todos los trabajadores estatales, “si no se encuentra eficiencia ni respuesta de parte del gobierno, el magisterio está abocado a ir a un paro”.

El próximo 19 de marzo habrá concentraciones de trabajadores y educadores en las capitales de cada departamento del país y en la capital, Bogotá, se dirigirán hasta la Plaza de Bolívar, desde las 10 am.
