Title: En más del 90% de los casos de Restitución de Tierras existen intereses mineroenergéticos
Date: 2018-04-25 15:01
Category: DDHH, Política
Tags: desarrollo minero energético, despojo, Mineria, MOVICE, petroleo, Restitución de tierras
Slug: concesiones-minero-energeticas-en-restitucion-de-tierras-no-han-permitido-retorno-de-victimas-movice
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Restitución-de-tierras-.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [25 Abr 2018] 

El Movimiento de Víctimas de Crímenes de Estado realizó un reporte sobre los problemas del proceso de restitución de tierras en el país **que no ha permitido el retorno** de las víctimas a sus territorios debido a las concesiones de los proyectos minero energéticos. Indican que, a pesar de los fallos sobre restitución, en el 91% de los casos se aprueba la continuidad de los proyectos extractivos.

El reporte lo hizo la organización habiendo analizado **110 de las 144** sentencias de restitución que han sido emitidas “donde se superponen los proyectos o concesiones minero energéticas con el territorio a restituir”. Tuvieron en cuenta el hecho de que los desplazamientos ocurrieron entre 1992 y 2012 “bajo la presión y el asesinato de los habitantes de las comunidades y familiares de las víctimas”.

### **Hallazgos del MOVICE** 

Para empezar, afirman que los argumentos de duración restringida del desarrollo del extractivismo “que puede ser décadas (…) **desconoce la situación de desplazamiento** de las víctimas, por lo que la continuidad de dichos proyectos no les permitirá el uso y goce cabal de su tierra y territorio después de décadas de haber sido expulsadas por la fuerza”.

Además, encontraron que los territorios analizados han sufrido un reordenamiento **producto de los proyectos extractivistas**. Indican que en los casos de despojo que analizaron, “los territorios eran habitados en un 70 % por sujetos de reforma agraria, territorios que en un 92 % cambiaron su vocación agraria a una minera o petrolera”.

Recordaron que los desplazamientos, que tuvieron un beneficio para el desarrollo de proyectos minero-energéticos, se cometieron en **un 69% por los grupos paramilitares,** un 2.7% entre paramilitares y Fuerza Pública, un 11% por grupos guerrilleros y un 17.2% por grupos armados no identificados. Sin embargo, en el 95% de los casos, las víctimas llevan más de una década en situación desplazamiento.

En lo relacionado al cumplimiento de los derechos de las víctimas a la restitución y retorno a sus territorios, el MOVICE alertó que, de los 176 predios restituidos, **en 101 se ha permitido la solicitud de los proyectos extractivos**. El reporte indica que los casos fueron remitidos a la Agencia Nacional de Minería y a la Agencia Nacional de Hidrocarburos “para que se garantice la sostenibilidad y el goce de los derechos de las víctimas a la restitución”. (Le puede interesar:["Cinco propuestas para evitar el fracaso total de Ley de Restitución de Tierras"](https://archivo.contagioradio.com/42667/))

### **Las empresas que aspiran o tienen concesiones mineras o petroleras en procesos de restitución** 

De acuerdo con el MOVICE, mientras las víctimas de despojo **acuden a los procesos de restitución**, “los proyectos mineros y petroleros, en su gran mayoría, mantuvieron su posibilidad de adjudicación o funcionamiento”. Estas son las empresas que han aspirado o tienen concesiones mineras o petroleras en los procesos de restitución son:

Continental Gold Limited Sucursal Colombia, Compañía de Exploraciones Chocó Colombia S.AS, Alianza Minera Limitada, **Anglogold Ashanti Colombia**, Clean Energy Resources, Ecopetrol, El Molino S.O.M, Grantierra Pluspetrol, Minerales de Uraba, Hocol S.A, MinCol, Loh Energy, Perenco Colombia Limitada, Trenaco Colombia S. A.S, FG Mining Group Corporation Ci Ltda., Pisa Proyectos de Infraestructura S.A, Asociación de Areneros de Andalucía-Valle, Exploración Materiales de Construcción Agrícola y Titular GMX Minerales And Coal Ltda, Acuario, Consorcio Optimas Range.

### **Recomendaciones de la organización** 

Frente a este panorama y teniendo en cuenta también que “la Fuerza Pública ha protegido el **desarrollo de las empresas extractivistas** por encima del derecho a la restitución de las víctimas” y que las entidades como la Unidad de Restitución de Tierras no ha solicitado la cancelación de títulos mineros o concesiones a las empresas de extractivismo, el MOVICE realizó una serie de recomendaciones.

En primer lugar, solicitaron a las autoridades que, dentro de los procesos de restitución, “soliciten investigaciones que permitan dar cuenta de **cómo se beneficiaron los proyectos extractivos** con el desplazamiento o despojo de las víctimas”. Además, solicitaron la tipificación del delito de despojo teniendo en cuenta que no se investiga penalmente el fenómeno. (Le puede interesar:["Unidad de Restitución de Tierras mete un mico a la Ley 1448"](https://archivo.contagioradio.com/unidad-de-restitucion-de-tierras-mete-un-mico-a-la-ley-1448/))

Finalmente, agregaron que es necesario que la Comisión para el Esclarecimiento de la Verdad y de la Jurisdicción Especial para la Paz, “direccione como uno de sus énfasis el esclarecimiento de lo ocurrido con el **reordenamiento de los territorios**, donde se investigue la participación de los grandes beneficiarios de las concesiones y proyectos minero energéticos en los territorios afectados por el desplazamiento forzado y el despojo”.

[Restitución de tierras no permite el retorno de las víctimas por concesiones de proyectos mineroenergéti...](https://www.scribd.com/document/377394252/Restitucio-n-de-tierras-no-permite-el-retorno-de-las-vi-ctimas-por-concesiones-de-proyectos-mineroenerge-ticos#from_embed "View Restitución de tierras no permite el retorno de las víctimas por concesiones de proyectos mineroenergéticos on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_20824" class="scribd_iframe_embed" title="Restitución de tierras no permite el retorno de las víctimas por concesiones de proyectos mineroenergéticos" src="https://www.scribd.com/embeds/377394252/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-ruQ55k0hFwllpiH2fLSB&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
