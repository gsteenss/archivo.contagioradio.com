Title: Debe parar Minería de AngloGold Ashanti en Jericó
Date: 2019-07-26 10:40
Author: CtgAdm
Category: Ambiente, Nacional
Tags: acuerdo municipal, Jericó, Minería ilegal
Slug: continuan-actividades-de-mineria-en-jerico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/33156256_1397768400322849_2324060057165103104_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

El día 13 de julio una comisión de la Contraloría General de la Nación inspeccionó las actividades de exploración minera que está desarrollando el proyecto filial minero de Quebradona – Nuevo Chaquiro de la AngloGold Ashanti, que de acuerdo con la comunidad de Jericó, es ilegal debido al incumplimiento de un acuerdo que se había firmado.

La controversia de la exploración radica por el **acuerdo 010 del 2018 del Concejo de Jericó** donde se impuso una medida para la suspensión de actividades mineras en el territorio, cuya vocación económica es agraria. (Le puede interesar:[Nueva derrota en Jericó para AngloGold Ashanti).](https://archivo.contagioradio.com/nueva-derrota-en-jerico-para-anglogold-ashanti/)

El coordinador de la Mesa Ambiental de Jericó, Fernando Jaramillo aseguró que **estas acciones son ilegales**, dado que, el municipio presentó medidas cautelares de suspensión. Esta medida pasó a instancias de CorAntioquia quien la levantó, sin embargo, esto no invalida el acuerdo municipal; únicamente el Tribunal Administrativo de Antioquia puede invalidar o no este acuerdo afirmó Jaramillo.

**La comisión pudo corroborar los incumplimientos de la empresa minera, para hacer seguimiento y detener hasta el momento cualquier tipo de exploración,** aseguró Jaramillo. Asimismo, se pudo evidenciar que no hay medidas frente al proceso sancionatorio del 2016 ante CorAntioquia. Pese a que, se trata de una salida  de aguas subterráneas por la perforación de un pozo exploratorio que se encuentra a 15 metros de una quebrada, dentro de una zona de protección municipal. (Le puede interesar: [Jericó, Antioquia le dice ¡No! a la minería por segunda ocasión).](https://archivo.contagioradio.com/jerico-antioquia-le-dice-no-a-la-mineria-por-segunda-ocasion/)

Dada el incumplimiento por parte de las entidades respectivas, la comunidad tienen claro que deben mantener  continuas acciones de denuncias, defensa y protesta pacífica para la protección de su territorio.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
