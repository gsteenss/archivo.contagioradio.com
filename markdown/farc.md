Title: Fiscal debe dejar de tratar a las FARC como si no hubieran firmado la Paz: Andrés Paris
Date: 2017-06-05 12:37
Category: Entrevistas, Paz
Tags: CSIVI, Implementación de Acuerdo de paz
Slug: farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Farc-ONG-foto-FARC-EP.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/farc-dialogos-de-paz-cuba.gif" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/farc.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/farc.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/farc_en_la_guajira-e1455931913551.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/farc-en-la-guajira.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/FARC-1Conejo_contagioradio.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/farc-paz-gobierno-contagioradio.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/farc-campamento-guerrilla-afp.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Farc-Semana.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/farc_gobierno_en_cuba_0.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/farc.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/farc-mamas-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/farc-mamas.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/farc-en-putumayo.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/farc-en-putumayo-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/farc-en-putumayo-4.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/farc-zonas-veredales.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/FARC-FRENTE-33.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/farc.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/militares-e1506032994531.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/farceln.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/farc-cumple.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/farc-rueda-de-prensa-e1500922588668.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/FARC-Partido.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/Farc-21-960x500.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/farco.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/farc-2-e1518197319974.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/farc.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/farc1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/farc-e1523400908842.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Excomandantes-de-FARC.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [05 Jun 2017] 

El caso de Yimmi Ríos, no es la única irregularidad que se ha presentado con las amnistías durante la implementación del Acuerdo de Paz, de acuerdo con Andrés París, integrante de las FARC-EP y miembro de la CSIVI, estos actos “**hacen pensar será que sectores del Estado y funcionarios del Estado en este caso como el Fiscal, no están enterados que se firmó un proceso de paz**”.

Para París, estos hechos son una muestra de los incumplimientos por parte del gobierno hacia lo pactado que llenan de incertidumbre al proceso de paz y agregó que la actitud del Fiscal general, Néstor Humberto Martínez, es amenazante y una **clara señal de “judicializar” a los integrantes de la guerrilla**, en distintas partes del país, para atenuar el ejercicio que lleva las FARC-EP en la implementación de los acuerdos.

El paso siguiente será la creación de una comisión a la cabeza de Rodrigo Londoño junto con una delegación del gobierno que asegure que los listados vayan con los seudónimos usados en la confrontación acompañados con los nombres propios. Le puede interesar: ["Estas son las alternativas de Santos para salvar el proceso de paz"](https://archivo.contagioradio.com/estas-son-las-alternativas-del-presidente-santos-para-salvar-el-proceso-de-paz/)

### **Los bienes de las FARC-EP** 

El Fiscal General de la Nación, señaló que ya tiene en su poder una lista con todos los bienes y las empresas que harían parte de las FARC-EP, referente a este tema Andrés París manifestó que esas afirmaciones solo buscan interferir y que, en vez de ser comunicadas a la opinión pública, **tendrían que ser expuesta a la comisión encargada de revisar estos puntos.**

“Ya hay avances y adelantos en esta dirección, el Físcal no hace parte de esta mesa, él es un funcionario del Estado, **él tiene que trasmitir estas inquietudes a sus jefes inmediatos y no estar tirando a los cuatro vientos**, con ánimo de bronca y estigmatización contra una de las dos partes que firmó el acuerdo, por qué siguen tratando a los insurgentes como si no se hubiese firmado ningún acuerdo”, señaló París.

### **Paramilitarismo en cercanías a Zonas Veredales** 

Desde que se instalaron las zonas veredales en el país y se dio el repliegue de los integrantes de las FARC a estos lugares, campesinos y organizaciones defensoras de derechos humanos, han expresado una re paramilitarización del territorio. Este fin de semana la **Comisión de paz del Congreso Nacional visitó diferentes zonas y confirmó el riesgo en el que se encuentran los integrantes de las FARC-EP.**

Sobre esta situación y las diferentes alarmas que ya han enviado tanto las FARC-EP como organizaciones sociales, París indicó que “**se están preparando las condiciones para el asesinato masivo de los combatientes de las FARC**” y agregó que la implementación de los acuerdos de paz avanza y una muestra de ello es la creación de la Unidad para el desmantelamiento del paramilitarismo.

Sin embargo, señaló que este y los demás decretos firmados por el presidente Santos tienen muchos “micos” que deberán ser revisados por las organizaciones sociales y mantener en alerta permanente a la sociedad. Le puede interesar: ["Dipaz corrobora control paramilitar en 7 territorios cercanos a Zonas Veredales"](https://archivo.contagioradio.com/dipaz-corrobora-presencia-de-paramilitares-en-zonas-veredales/)

<iframe id="audio_19084150" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19084150_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
