Title: Mujeres caminando hacia la libertad
Date: 2019-03-12 12:39
Category: Expreso Libertad
Tags: Cácerl, Machismo, movimiento carcelario, mujeres
Slug: mujeres-caminando-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/mujer-camina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Psico-Activa] 

###### [8 Feb 2019] 

Doris Suarez es una ex prisionera política que estuvo durante 14 años recluida en el sistema penitenciario de Colombia. Desde allí no solo luchó por la defensa de los derechos humanos de sus compañeras, sino por deconstruir lo que ella misma denomina como el "machismo carcelario".

En este programa del Expreso conéctese con las voces de las mujeres que desde el movimiento carcelario han exigido reformas para garantizar la igualdad en los derechos de  ellas.

<iframe id="audio_33273052" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33273052_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Encuentre más información sobre Prisioneros políticos en][ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)[y escúchelo los martes a partir de las 11 a.m. en] [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
