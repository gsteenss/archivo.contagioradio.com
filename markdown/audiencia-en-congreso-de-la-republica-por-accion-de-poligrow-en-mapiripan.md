Title: Palmera Poligrow consume 1'700.000 litros de agua diarios en cada plantación en Mapiripán
Date: 2016-02-26 10:21
Category: Ambiente, Nacional
Tags: Congreso de la República, mapiripan, Poligrow
Slug: audiencia-en-congreso-de-la-republica-por-accion-de-poligrow-en-mapiripan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Captura-de-pantalla-2015-08-11-a-las-16.51.09.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://co.ivoox.com/es/player_ek_10585152_2_1.html?data=kpWimpqVeZOhhpywj5aZaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncbHVzdLS1MaPlNDgyszf0dyPp9Di1Nrax5CVaZOrmJWdkJWUdIzgytnf0diPqMafwsziw5DJsozXwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Juliana Chaparro, Comisión de Justicia y Paz] 

###### [26 Feb 2016] 

Hoy se realiza en **el Salón Boyacá del Congreso de la República una audiencia sobre el caso de Mapiripán** y las afectaciones ambientales y sociales por el cultivo de palma aceitera producido por la empresa italiana [Poligrow](https://archivo.contagioradio.com/poligrow-palma-mapiripan/).

Una de las mayores tragedias en esta zona del país, tiene que ver con los factores ambientales que a su vez afectan a la población que cuenta con pocas fuentes de agua, y de las cuales **13 se han secado debido a que la [plantación de 15 mil hectáreas de palma aceitera](https://archivo.contagioradio.com/cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia/) acaban con las palmas de moriche y las fuentes hídricas subterráneas de la región, afectando más de 200 familias. **“Las palmas actúan como chupas que absorben el agua que está en el subsuelo”, explica Juliana Chaparro, bióloga y defensora de Derechos Humanos de la Comisión de Justicia y Paz.

“Empresas palmeras tienen permisos de captación de agua más alta que petroleras, **captan más de 1'700.000 litros diarios en Mapiripán”**, dice Chaparro, quien señala que las actividades de la empresa podrían generar sequías como las que conoció Colombia en el año 2014 en Paz de Ariporo, y que ocasionó la muerte de miles de chigüiros.

Además el costo que el agua tiene para la empresa también es irrisorio, la organización de DDHH señala que por cerca de **454 millones de litros de agua que Poligrow consume al año solamente está pagando 6 millones de pesos** en un contrato a 5 años. Es decir, menos de 1 millón de pesos anuales.

Las organizaciones aseguran que, además, la empresa ha prohibido de manera expresa a las comunidades indígenas Jiw y Sikuani la práctica de actividades ancestrales, como la visita a la laguna sagrada de las Toninas, que fue adquirida por la empresa Poligrow para generar una zona franca e instalar plantas extractoras para el procesamiento de palma africana. Una laguna en la que habitan delfines rosados, una especie en peligro de extinción.

[**La acción paramilitar, las problemáticas ambientales y el despojo de tierras**](https://archivo.contagioradio.com/lider-reclamante-de-tierras-en-mapiripan-es-blanco-de-amenazas-por-parte-de-paramilitares/)serán los temas que se tratan en la audiencia, en la que participan habitantes de la región y las víctimas de la Masacre de Mapiripán, junto con congresistas y defensores de DDHH.
