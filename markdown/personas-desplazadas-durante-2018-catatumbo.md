Title: ONU registró 13 mil personas desplazadas durante 2018 en Catatumbo
Date: 2019-05-27 19:20
Author: CtgAdm
Category: DDHH, Nacional
Tags: ASCAMCAT, Catatumbo, lideres sociales, Norte de Santander
Slug: personas-desplazadas-durante-2018-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/chart.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Cultivos-de-Uso-ilícito-en-Norte-de-Santander.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-2-1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio y otros] 

El pasado jueves 23 de mayo, la Oficina del Alto Comisionado de la ONU para los Derechos Humanos en Colombia, presentó su informe anual sobre la situación de Norte de Santander y Catatumbo. En el documento, la Oficina advierte sobre graves violaciones a los derechos humanos que se están cometiendo en la región, al tiempo que resaltó el papel de líderes y organizaciones sociales para evitar que la situación sea aún más difícil.

Según el Comité Internacional de la Cruz Roja (CICR), en el Catatumbo tiene lugar el único Conflicto Armado No Internacional (CANI) entre actores armados ilegales, producto de la confrontación entre ELN y EPL; no obstante, en la región también hacen presencia Fuerzas Armadas y grupos residuales de las FARC que no se acogieron al proceso de paz. Adicionalmente, de acuerdo al Observatorio de Drogas de Colombia (ODC), hasta 2017, en Norte de Santander habían cerca de 30 mil hectáreas de Coca cultivada, siendo el Catatumbo la región con mayor presencia de cultivos de uso ilícito.

(Le puede interesar: ["En colombia hay cinco conflictos armados no internacionales"](https://archivo.contagioradio.com/colombia-conflictos-armados-cicr/))

![Crecimiento en cultivos de uso ilícito en Catatumbo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/chart-300x200.jpeg){.alignleft .wp-image-67828 width="425" height="283"}

![Cultivos de Coca concentrados en Catatumbo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Cultivos-de-Uso-ilícito-en-Norte-de-Santander-291x300.png){.aligncenter .wp-image-67829 width="284" height="293"}

### ONU alerta por desplazamientos forzados, amenazas y asesinatos de líderes sociales

Según lo registró la oficina de derechos humanos de las Naciones Unidas, en 2018 se presentaron por lo menos 116 asesinatos a defensores de derechos humanos; de los cuales 18 ocurrieron en Norte de Santander (10 de ellos en Catatumbo). La cifra reseñada en la región "es 3 veces superior al promedio anual registrado en los 3 años anteriores". Del total de líderes asesinados, la Oficina resaltó que 5 eran miembros de la Asociación Campesina del Catatumbo (ASCAMCAT).

De igual forma, la organización llamó la atención por el desplazamiento de 13 mil personas en eventos masivos; y resaltó que 14 líderes tuvieron que abandonar sus territorios por impulsar el Programa Nacional Integral de Sustitución (PNIS). Adicionalmente, afirmó que en lo corrido del año, han registrado 4 homicidios de defensores de derechos humanos en Norte de Santander. (Le puede interesar: ["Desplazamiento forzado y aumento en la violencia: Las preocupaciones del CICR Colombia"](https://archivo.contagioradio.com/desplazamiento-forzado-cicr-colombia/))

En la presentación del informe, la Oficina reveló que “el 66% de los asesinatos de defensores y defensoras estaría relacionado con la denuncia u oposición al accionar criminal, a los efectos de la violencia a niveles endémicos que afectan a la población en general o al apoyo a la implementación del Acuerdo de Paz, y más concretamente a la sustitución de los cultivos de uso ilícito”.

### **Medidas para proteger líderes sociales no están siendo efectivas**

En el informe se advierte que las medidas de protección para los líderes sociales tienen problemas en tanto su enfoque, y el tipo de respuesta que obtienen quienes han sido amenazados. En primer lugar, el Organismo internacional observó que en 2018 "en muchas ocasiones, las medidas de protección otorgadas a los defensores y defensoras no respondían a los riesgos y complejidades del contexto en el que estos desempeñaban su labor"; a ello se suma el "retraso de más de un mes en el otorgamiento de medidas de protección por la UNP que derivó en el asesinato de un líder comunal" en Norte de Santander.

En segundo lugar, hicieron evidente la necesidad de abordar un enfoque de protección preventivo que adopte medidas de carácter temporal; para ello, indicaron que sería necesario contar con instancias fortalecidas como la Comisión Intersectorial para la Respuesta Rápida a las Alertas Tempranas (CIPRAT) y la Comisión Nacional de Garantias de Seguridad.

### **Líderes de ASCAMCAT denuncian persecución por parte del Ejército**

Recientemente los líderes Juan Carlos Quintero, Guillermo Quintero y Olga Quintero, integrantes de ASCAMCAT, denunciaron la instalación de cámaras vigilando su finca en Teorama, Norte de Santander. Sumado a ello, informaron sobre la aparición de grafitis de las AUC en las paredes cercanas a sus predios; situación que para Olga, prende las alarmas porque "hay denuncias de presencia del paramilitarismo en la región".

Para la líder, también es relevante el hecho de que las cámaras y las pintas aparecieran tras la retirada del Ejército de la zona; pues como lo explicó, cerca de la finca que habita con sus hermanos, la Fuerza Pública había mantenido un asentamiento durante 14 años, pero hace 9 días decidieron abandonar el lugar. No obstante, la activista reconoció que no es la primera vez que aparece una cámara en sus predios, pues el pasado 21 de enero se percataron de otro dispositivo similar que había sido instalado en inmediaciones de la casa.

Ante esta situación el personero de Teorama hizo una verificación del hecho, y Quintero pidió a la Fiscalía dar claridad  sobre los responsables de la instalación de las cámaras, así como de los grafitis que intimidan a líderes de una de las organizaciones más golpeadas en el Catatumbo. (Le puede interesar: ["Más de 240 habitantes huyen de enfrentamientos entre ELN y Ejército en Teorama, Norte de Santander"](https://archivo.contagioradio.com/mas-de-240-habitantes-huyen-de-enfrentamientos-entre-eln-y-ejercito-en-teorama-norte-de-santander/))

### **Líderes fueron clave en 2018 para que la situación de Catatumbo mejore**

Parar finalizar, la Oficina de la ONU que vigila el cumplimiento de los derechos humanos en el país reconoció la "labor incansable de las defensoras y defensores de derechos humanos, muchas de ellas y ellos miembros de organizaciones sociales, Juntas de Acción Comunal o personas que individualmente, que con su esfuerzo y dedicación han visibilizado y contribuido a la mejora en las condiciones de vida en sus comunidades"; todo ello, pese a los graves riesgos que conlleva liderar procesos en esta zona del territorio nacional.

 

<iframe id="audio_36311044" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36311044_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
