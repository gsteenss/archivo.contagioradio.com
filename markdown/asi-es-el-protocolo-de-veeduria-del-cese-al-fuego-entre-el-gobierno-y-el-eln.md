Title: Así es el protocolo de veeduría del Cese al fuego entre el gobierno y el ELN
Date: 2017-09-29 16:56
Category: Nacional, Paz
Tags: ELN, Proceso de paz en Quito
Slug: asi-es-el-protocolo-de-veeduria-del-cese-al-fuego-entre-el-gobierno-y-el-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Etnografo] 

###### [29 Sep 2017]

Según un comunicado conjunto difundido este 29 de Septiembre, el gobierno y el ELN tienen listos los protocolos de verificación del Cese al Fuego. La comunicación especifica que se crea un **Mecanismo de Verificación y Veeduría que funcionará en el nivel local, regional y nacional y estará compuesto por integrantes de esa guerrilla, la Fuerza Pública, la iglesia católica y las naciones unidas.**

Los delgados de las partes para ese mecanismo no portarán armas y gozarán de todas las garantías de seguridad acordadas en el protocolo. La misión del Mecanismo será coordinará las acciones necesarias para evitar incidentes y garantizará el buen funcionamiento del **Cese al fuego bilateral y nacional que comenzará a regir este 1 de Octubre.**

El comunicado conjunto señala que el componente internacional de este mecanismo se encargará de dirimir las dificultades que se presenten y organizar y publicar informes mensuales a las partes y a la opinión pública de manera que se pueda hacer una veeduría efectiva y amplia del cumplimiento de las partes. (Le puede interesar:["Cese Bilateral evitará 165 choques semanales entre Fuerza Pública y ELN"](https://archivo.contagioradio.com/cese-bilateral-evitara-165-choques-semanales-entre-eln-y-fuerza-publica/))

### **Protocolo se aplicará en nivel regional, local y nacional** 

El documento explica que en el nivel regional la ONU y la iglesia católica formularán las recomendaciones necesarias para evitar incidentes y reportarán el resultado de la observación. **En el nivel local los comités diocesanos de veeduría recolectarán y analizarán la información en algunos sectores que ya están acordados.**

### **ONU lista para verficar** 

Adicionalmente explican que cada una de las partes tendrá sus delegados que coordinarán las acciones necesarias con sus instancias a nivel nacional. **También señalaron que solicitaron al concejo de seguridad de la ONU la autorización para esta misión de verificación. **(Le puede interesar:["Cese Bilateral entre Gobierno y ELN es histórico: Alvaro Villarraga"](https://archivo.contagioradio.com/cese-bilateral-entre-eln-y-gobierno-nacional-es-hstorico-alvaro-villarraga/))

Por su parte Jean Arnoult, jefe de la misión de la ONU para la implementación del acuerdo con las FARC, aseguró que están listos para asumir la misión en este nuevo acuerdo entre el gobierno y el ELN, saludó la disposición de la iglesia católica a acompañar este mecanismo y aseguró que el respaldo de la comunidad internacional será vital para este proceso.

[Comunicado Conjunto G0C-ELN](https://www.scribd.com/document/360273141/Comunicado-Conjunto-G0C-ELN#from_embed "View Comunicado Conjunto G0C-ELN on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_3167" class="scribd_iframe_embed" title="Comunicado Conjunto G0C-ELN" src="https://www.scribd.com/embeds/360273141/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Oj3MCHrbbDYhSpD80QcC&amp;show_recommendations=true" width="80%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
