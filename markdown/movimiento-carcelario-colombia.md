Title: Las conquistas y retos del Movimiento carcelario en Colombia
Date: 2018-06-29 09:35
Category: Expreso Libertad
Tags: movimiento carcelario, presos politicos
Slug: movimiento-carcelario-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/pena-muerte-benin_web.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Getty Images 

###### 26 Jun 2018 

El Movimiento Nacional Carcelario en Colombia, ha sido el escenario que ha permitido a los prisioneros y prisioneras del país, gestar espacios de diálogo y concertación sobre la exigencia de sus derechos humanos, con las diferentes instituciones que son responsables de estos centros.

Dentro de sus máximos logros, se encuentra el cierre del patio de mujeres de la Cárcel de Valledupar, más conocida como Tramacúa, o la Guantanamo de Colombia, uno de los centros de reclusión que tiene los mayores índices de violaciones a derechos humanos.

Según el relato de "Chucho Nariño", ex prisionero político que estuvo en esa prisión, la protesta para lograr el cierre de ese patio inició con una huelga de hambre y culminó con una de las intromisiones y arremetidas más fuertes del Escuadrón Móvil Antidisturbios ESMAD. Producto de ello se conformó una de las primeras misiones de derechos humanos, conformada por políticos y organizaciones, que visitó este lugar y que recolectó la suficiente información para cerrar ese patio.

En el siguiente Expreso Libertad, realizamos un viaje  desde el origen, conquistas y retos del Movimiento Nacional Carcelario colombiano.

<iframe id="audio_26784756" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26784756_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
