Title: Mesa Social para la Paz ajusta metodología de cara al inicio de conversaciones Gobierno ELN
Date: 2017-01-20 16:44
Category: Nacional, Paz
Tags: ELN, Mesa de conversaciones en Quito, Mesa Social para la paz.
Slug: mesa-social-para-la-paz-busca-incidir-en-conversaciones-eln-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz-contagioradio-e1476125455134.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [20 Ene 2017] 

Mesa Social para la paz, es una de las iniciativas que desde que se dio a conocer la agenda de díalogos entre el gobierno y el ELN, ha estado generando espacios de encuentro entre los diferentes sectores de la sociedad para poder tener incidencia desde la participación ciudadana en las conversaciones de Quito, **motivo por el cual estarán participando de la instalación de la mesa el próximo 7 de febrero.**

Para Diana Sánchez, vocera de la Mesa Social, el ideal de este escenario es hacer que **“la participación social sea una realidad y que efectivamente influya positivamente en que el proceso entre el gobierno y el ELN avance**”. Para ello diferentes organizaciones en cada una de las regiones del país han venido reuniéndose con la finalidad de tener una agenda social que se pueda presentar tanto al gobierno como al ELN. Le puede interesar:["Participación social será el corazón de proceso ELN-Gobierno: Carlos Velandia"](https://archivo.contagioradio.com/participacion-social-sera-el-corazon-del-proceso-de-paz-eln-gobierno-carlos-velandia/)

De igual forma, la Mesa Social, tiene el ánimo de realizar **100 encuentro territoriales el próximo 4 de febrero** que permita consolidar mucho más el escenario e ir ampliando la participación social. Las discusiones de estos espacios giraran en torno a la construcción de las propuestas locales, regionales y nacionales, y a pensar nuevas formas de vincular a la sociedad en el debate.

Para finalizar Sánchez agrega que “entre los diálogos regionales y nacionales que se den se podrán ir esbozando las problemáticas que se pueden ir acordando para llevar a la mesa en Quito, en clave de **pensar qué tipo de país es el que realmente se necesita construir para que quepan todos y todas**”

<iframe id="audio_16565770" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16565770_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
