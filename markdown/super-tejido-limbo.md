Title: "Súper tejido limbo" una combinación de danza contemporánea y folclórica
Date: 2018-02-21 15:11
Category: eventos
Tags: Bogotá, Cultura, danza
Slug: super-tejido-limbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/image001-2-e1519243053572.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La maldita vanidad 

###### 21 Feb 2018 

Del 22 de febrero al 3 de marzo, la **Compañía Maldita Danza **llega al escenario de **La Factoría L’Explose** con la obra **Súper Tejido Limbo.** Un espectáculo que ha girado por  América  y Europa** **que combina música latina, ruídos sórdidos y voces guturales, poniendo en diálogo la interpretación el movimiento y la música, creada originalmente por Carlos Romero.

Los bailarines Walter Cobos y Jorge Bernal, son los encargados de aportar los movimientos explosivos de **Súper Tejido Limbo,** que** **coinciden plenamente con el contenido, con la inercia del cuerpo que busca moverse. Siempre cercana con los espectadores, los conceptos de transformación derivan en sentencias a media voz: “*Recuerda que morir y vivir siempre es lo mismo”.*

La pieza teatral es producto de una investigación escénica desarrollada a partir del diálogo dado entre distintas danzas tradicionales y contemporáneas como la cumbia, la danza tradicional colombiana, el teatro del noh, la danza butoh y la danza hindú; las cuales contribuyeron a la búsqueda y precisión del movimiento en la apuesta coreográfica.

Los artistas estudiaron formalmente **la cumbia:** su planimetría, sus movimientos y sus intencionalidades. En cuanto al concepto y lo significante en la cultura popular y religiosa, de allí salió la idea del ritual y la temática global de esta pieza **la vida y la muerte**, así como el elemento referente de movimiento: **La gallina.**

**Súper Tejido Limbo **se describe como un ritual de vida, mezcla de diferentes influencias culturales. Un ritual en donde dialogan los cuerpos en representación de lo opuesto – complementario de que procede el universo, o la construcción de universos a través de una danza milenaria en que bailan lo masculino y lo femenino, lo bestial y lo humano en el trascurrir eterno de lo que nace y muere, se une y se separa.

 

De jueves a sábado a las 8:00 p.m.  
Bono de apoyo: \$30.000 General y \$25.00 estudiantes y tercera edad.  
Lugar: Factoría L’EXPLOSE Carrera 25 \# 50-34
