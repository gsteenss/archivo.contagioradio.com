Title: En tres años han sido asesinados 236 integrantes de Juntas de Acción Comunal
Date: 2019-02-05 14:50
Author: AdminContagio
Category: DDHH, Nacional
Tags: juntas de acción comunal, Rutas de prevención
Slug: en-tres-anos-han-sido-asesinados-236-integrantes-de-juntas-de-accion-comunal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Prensa-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: PrensalibreCasanare 

###### 5 Feb 2019 

[Frente a las constantes muertes y amenazas de las que han sido víctimas integrantes de las Juntas de Acción Comunal a lo largo del territorio colombiano, sus dirigentes convocaron a una reunión extraordinaria con el Gobierno Nacional para expresar sus preocupaciones y exigir al Estado el fortalecimiento de una ruta de protección que garantice la vida y la labor de los líderes comunales del país. ]

**Ceferino Mosquera Murillo, presidente de la Confederación Nacional de Acción Comunal** manifestó que durante el año 2016 fueron asesinados 61 integrantes de la organización, cifra que se elevó en 2017 con 84 asesinatos, y que continuó en aumento en 2018 con 91 muertes. Para el presente mes de febrero de 2019 la cifra ya se eleva 7, un medidor de que probablemente este año electoral la estadística pueda aumentar.

[El Presidente admite que las cifras son dolorosas pero señala que es la única forma de que el Estado entienda la magnitud del problema, **"no es que quiera sumar a una estadística, es que se debe reconocer y visibilizar las afectaciones comunales en todos estos hechos que para nosotros son sistemáticos aunque a veces no quieran reconocerlo de esta manera"**, apuntó. ]

[También indicó que tras revisar cifras y constatar que el 85% de las personas que han sido asesinadas no tenían amenazas, la ruta de prevención se enfocará en fortalecer la protección de aquellos líderes comunales que siguen ejerciendo su trabajo y aún no han sido amenazados y de esta forma **lograr proteger **]**colectivamente a todo el liderazgo comunal.** [(Lea también: Colombia: el país con más asesinatos de defensores en el mundo) ](https://archivo.contagioradio.com/colombia-pais-mas-asesinatos-defensores-mundo/)

### **¿Cómo funcionará la ruta de prevención? ** 

[Segun lo explicado por Mosquera Murillo, **los entes del Gobierno se unificarán bajo un mismo proyecto en las zonas rurales y urbanas** de tal manera que quienes "estén trabajando la acción comunal puedan sentir la presencia inmediata del Estado",  para tal propósito se designaran unos primeros responsables que acudirán al llamado de la organización en cualquiera de las regiones, además se acordó que alcaldes y gobernadores  
brinden el apoyo necesario  en dicha materia. ]

[El dirigente señaló que no se tiene claridad sobre qué estructuras son las que están atentando contra la organización, pues a **pesar que la Fiscalía dice haber esclarecido el 53% de las investigaciones, los resultados de las mismas no se han dado a conocer.** Finalmente Mosquera resaltó que a pesar de las amenazas, las Juntas de Acción Comunal seguirán trabajando, "s]eguimos vigentes, la comunidad sabe que los buenos somos más  y a eso le estamos apostando" concluyó .

###### Reciba toda la información de Contagio Radio en [[su correo]
