Title: No pudieron desaparecer la sonrisa de Nydia
Date: 2015-09-02 19:00
Category: Carolina, Opinion
Tags: Desaparición forzada en Colombia, Fundación Nydia Erika Bautista, Nydia Erika Bautista
Slug: no-pudieron-desaparecer-la-sonrisa-de-nydia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/10620739_756671087729059_2859652791548629174_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Erik Arellana 

###### 

**Carolina Garzón Díaz (**[**@E\_vinna**](https://twitter.com/E_Vinna)**)**

[El pasado domingo se cumplieron 28 años de la desaparición forzada de Nydia Erika Bautista, una mujer militante del Movimiento 19 de Abril -M19- que fue arrancada de los brazos de su familia el día que celebraba la primera comunión de su único hijo. ¿Quiénes fueron los responsables? Fueron militares de la Unidad de Operaciones Especializadas del Batallón ‘Charry Solano’ al mando del Coronel del Ejército Álvaro Velandia Hurtado. Ese 30 de agosto de 1987 se llevaron a Nydia Erika, la sometieron a toda clase de vejámenes y finalmente la asesinaron para dejar su maltrecho cuerpo en una fosa común donde aguardó durante tres años, hasta que sus restos fueron finalmente encontrados. Una situación que rara vez ocurre en los casos de desaparición forzada.]

[Seguramente quienes planearon fríamente su crimen y se llevaron a Nydia Erika creyeron que al desaparecerla se desvanecerían con ella sus ideas de cambio, sus sueños de justicia, su lucha, su ánimo y su sonrisa. Pero se equivocaron. La imagen de Nydia Erika Bautista se ha convertido en un símbolo de las más de 45.000 personas desaparecidas forzadamente en Colombia y ha trascendido las fronteras para contar que en nuestro país los desaparecidos sí existen, que sus crímenes siguen en la impunidad y que están siempre presentes en las voces que día a día preguntan ¿Dónde están?]

[Son 45.000 personas, seres humanos, hijos, madres, padres, tíos, abuelos, hijas, sobrinas, primas y amigos a quienes no les dieron elección y se los llevaron lejos de sus seres amados.  Sin duda la historia de nuestro país no puede estar completa si dejamos de lados a quienes están ausentes: a nuestros desaparecidos. Tampoco la paz puede ser posible si esta atrocidad se continúa realizando en la completa impunidad. En este momento el país tiene la oportunidad única de resarcirse con los familiares de las víctimas de desaparición forzada mediante la verdad, pues solamente a través de ella se podrá esclarecer ¿Quiénes los desaparecieron? ¿Por qué? ¿Para qué? ¿Cuál es su paradero?]

[Ha sido un gran mérito de los familiares de los desaparecidos no desfallecer en la búsqueda de sus seres queridos y mantener viva su memoria. No es fácil continuar la vida cuando el corazón se ha partido y desconocemos el paradero de uno de sus trozos. Pero si no fuera por su valor, por la constancia de los familiares y las personas y organizaciones que los acompañan, no sabríamos la verdad sobre este atroz crimen que se ha cometido por años en Colombia y aún sigue ocurriendo. La memoria le ha podido ganar el pulso a la oscuridad que se levanta tras la desaparición forzada y a los criminales que pensaron que el miedo llevaría al olvido.]

[Con gusto decimos: No pudieron desaparecer la sonrisa de Nydia, esa sonrisa preciosa, franca y luminosa que a lo largo de estos 28 años hemos visto también en los rostros de hombres desaparecidos cuando caminaban a su trabajo, hacían ejercicio o llevaban a sus hijos al colegio; en las caras de niñas y niños desaparecidos forzadamente; y en la tez de mujeres que salieron de sus casas y jamás regresaron.]
