Title: Erradicaciones forzadas de coca no contribuyen a la paz
Date: 2016-10-31 17:55
Category: Ambiente, Nacional
Tags: ANZORC, campesinos, coca, Cultivos de coca
Slug: campesinos-del-catatumbo-dicen-no-a-erradicacion-forzada-de-coca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Erradicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Según informaciones entregadas por la Asociación Nacional de Zonas de Reservas Campesinas – ANZORC, **en días pasados el Ejército Nacional hizo presencia en Cerro González, jurisdicción del Zulia en Norte de Santander, para realizar funciones de erradicación.** Hecho que fue impedido por los campesinos cocaleros que hacen parte de ASCAMCAT – Asociación Campesina del Catatumbo.

Por su parte, César Jerez representante de ANZORC cuenta que lo sucedido en la zona fue que los campesinos **“impidieron la erradicación manual y con este acto exigieron la implementación inmediata de un programa de sustitución”.**

El vocero de esta organización aseguró que de **los compromisos que se han adquirido con los campesinos del Catatumbo después del paro del 2013 no se ha cumplido ninguno** “hay un compromiso verbal de parte del presidente Santos de no erradicar ni fumigar en el Catatumbo hasta tanto no se implementé una alternativa vía programa de sustitución…pero no obstante el gobierno no ha querido firmar una solución”.

Así mismo, Jeréz aseveró que **la erradicación afecta a las fincas campesinas** que están sembradas con coca “en la práctica es destruir la economía de una finca generando un impacto directo en la familia e incluso en la economía de la región, es por eso que en el Catatumbo hay una forma de cómo el campesino debe hacer la erradicación”. Le puede interesar: [Comunidades de San Pablo y Cantagallo rechazan la erradicación forzada](https://archivo.contagioradio.com/comunidades-de-san-pablo-y-cantagallo-rechazan-la-erradicacion-forzada/)

Por otro lado, el representante de ANZORC aseguró que “el Gobierno ignora las propuestas del campesinado y está justificando la erradicación proponiendo un modelo asistencial durante un año y al primer año que se pague, el campesino tendría que erradicar y es una burda interpretación del punto 4 de los Acuerdos de La Habana”.

**Las propuestas del campesinado**

Según esta organización, el gobierno nacional desde hace más de 3 años tiene en su poder un conjunto de propuestas para un programa de sustitución para toda la región del Catatumbo.

Cesar Jeréz, aseguró que como **ANZORC han realizado las respectivas denuncias de las fumigaciones, las erradicaciones forzadas como modalidad de violación a los derechos humanos** y agrega que “siempre hemos propuesto la solución de la sustitución de manera voluntaria, concertada, con un enfoque estructural, con sostenibilidad ambiental, sustituyendo los ingresos que se derivan del cultivo de la coca”. Le puede interesar: [Campesinos del Catatumbo rechazan erradicación forzada por parte del Ejército](https://archivo.contagioradio.com/campesinos-del-catatumbo-denuncian-nuevos-incumplimientos-del-gobierno/)

Así mismo esta Asociación asegura que dentro de las propuestas se encuentran planes pilotos de sustitución en varias regiones del país y concluye asegurando que **si el gobierno no realiza una “propuesta amigable y concertada lo que se viene es la confrontación y la movilización”.**

<iframe id="audio_13566070" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13566070_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
