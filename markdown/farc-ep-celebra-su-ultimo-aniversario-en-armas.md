Title: FARC-EP celebra su último aniversario en armas
Date: 2017-05-26 17:35
Category: Nacional, Paz
Tags: acuerdo de paz, FARC-EP
Slug: farc-ep-celebra-su-ultimo-aniversario-en-armas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/X-Conferencia-FARC-EP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo] 

###### [26 May 2017] 

Este 27 de mayo se realizará la conmemoración de los 53 años del asalto a Marquetalia, hecho histórico que da surgimiento a las FARC-EP y que para esta ocasión se juntará con la celebración del tránsito que realiza la guerrilla hacia un movimiento político y la construcción de una Colombia en Paz, en lo que las FARC ha llamado **"el último aniversario en armas"**.

Las 26 zonas veredales y puntos transitorios contarán con actividades culturales a la que también han sido invitados senadores, artistas y la ciudadanía en general, con la intensión de abrir un espacio que permita **relatar la historia del surgimiento de las FARC-EP**, desde las voces de quienes pertenecieron a ella por más de 50 años.

### **La Agenda cultural de la conmemoración de los 53 años de las FARC-EP** 

Entre las actividades más importantes que se realizarán el 27 de mayo se encuentra el recorrido hasta Marquetalia, vereda en Planadas, Tolima, en donde se espera que lleguen personas de todas partes del país. Las FARC-EP ha señalado que las personas que estén interesadas en asistir deberán llevar, **camping, ropa, calzado deportivo, botas pantaneras, abrigos, botiquín de primeros auxilios y linterna**, debido a las condiciones climáticas del país.

### **Zona veredal de Buenos Aires, Cauca** 

<iframe src="http://co.ivoox.com/es/player_ek_18923445_2_1.html?data=kp2mlJiYeJahhpywj5WaaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncYamk7HixcbXaZOmhpewjb_TssKft8rfx8nFsIy3wtfZ0diPlMLoyoqwlYqmddChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En la zona veredal Carlos Patiño en Buenos Aires, Cauca, el Teatro Experimental de Cali (TEC) tendrá una intervención cultural y participará en diferentes actividades. De acuerdo con Lucas, integrante de esta zona veredal, la importancia de esta celebración radica en la difícil situación por la que atraviesa el proceso de paz “el llamado que hacemos es que la implementación más allá que un discurso, e**s algo que vemos en la vida real y lo logramos con la movilización social**”.

La comunidad del municipio la Elvira hará una muestra gastronómica y ha colaborado con la organización del evento y se espera que a esta zona veredal llegue un apróximado de entre 700 personas. Le puede interesar: ["Convocan la creación de un pacto ciudadano por la paz de Colombia"](https://archivo.contagioradio.com/pacto_ciudadano_por_la_paz_colombia/)

### **Zona veredal de Icononzo** 

<iframe src="http://co.ivoox.com/es/player_ek_18923429_2_1.html?data=kp2mlJiYdpqhhpywj5aUaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncarnwsfSzsaPl8Li09Te18qJdqSfu9Tbw5C6qdPZxcbZja7Hs8_jz9_cj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En la zona veredal de Icononzo, en Tolima, se estima que podrían llegar aproximadamente mil personas provenientes de las ciudades como Bogotá e Ibague y que el evento se ha preparado con la colaboración de las y los campesinos de alrededor y las donaciones que han realizado. En los actos culturales estarán presentes artistas populares de la región y artistas de la guerrilla de las FARC-EP.

Isabela Sanroque, integrante de la guerrilla, señaló que este encuentro también tiene como finalidad rescatar la historia que el país no conoce y que ha marcado los relatos de la profundidad olvidada del país “**queremos que, a partir de este aniversario, de la voz de la guerrillerada sepan que pasó**, cómo ha sido este movimiento campesino que ahora emerge también en las ciudades”. Le puede interesar: ["Estas son las alternativas del presidente Santos para salvar la paz"](https://archivo.contagioradio.com/estas-son-las-alternativas-del-presidente-santos-para-salvar-el-proceso-de-paz/)

### **Zona veredal Mesetas** 

<iframe src="http://co.ivoox.com/es/player_ek_18923466_2_1.html?data=kp2mlJiYepehhpywj5aWaZS1lJWah5yncZOhhpywj5WRaZi3jpWah5yncanVysnm0JC0rduZpJiSpJjSaZO3jL_c0MaPusbmxsnOzpCmucbiwpDDy9jYpYzBxtnOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En Buena Vista, Meta, se encuentra la zona veredal de, allí Haidyn Pizón integrante de la guerrilla de las FARC-EP manifestó que pese a que aún no se encuentren construidas en su totalidad, las y los guerrilleros **han realizado adecuaciones para recibir a la ciudadanía durante estos días** y agregó que este 27 de mayo aspiran a que sea el último alzados en armas y estando tan cerca de que concluya el plazo para el día D+180.

“Es difícil hemos tenido las armas durante años y nos ha garantizado la vida, pero ahora pensamos que las garantías políticas **no son solo las armas sino el respaldo del pueblo como tal**” afirmó Haidyn. Le puede interesar: ["Con movilizaciones y pactos políticos haremos frente a decisión de la Corte: Cepeda"](https://archivo.contagioradio.com/con-movilizaciones-y-pactos-politicos-haremos-frente-a-decision-de-la-corte-ivan-cepeda/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
