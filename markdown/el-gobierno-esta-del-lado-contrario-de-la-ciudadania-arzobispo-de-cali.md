Title: El Gobierno está del lado contrario de la  ciudadanía: Arzobispo de Cali
Date: 2020-01-13 11:48
Author: AdminContagio
Category: DDHH, Nacional
Tags: acuerdo de paz, Chocó, iglesia católica, Monseñor Romero
Slug: el-gobierno-esta-del-lado-contrario-de-la-ciudadania-arzobispo-de-cali
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Dario-Monsalve.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @OArquidiocesis  
] 

Tras el [comunicado de diez obispos del pacífico y suroccidente del país](https://archivo.contagioradio.com/obispos-piden-reabrir-dialogos-con-eln-y-garantias-para-lideres-y-comunidades/) sobre las situaciones de derechos humanos que se viven en esos territorios, Contagio Radio habló con Monseñor Darío Monsalve, Arzobispo de Cali y uno de los firmantes del comunicado. Monseñor habló sobre el asesinato de líderes sociales, el proceso de paz con el ELN, el paramilitarismo y la continuidad en la realización de acuerdos humanitarios regionales.

### **El Gobierno se ha puesto al lado contrario de la ciudadanía** 

En primer lugar, Monseñor aseguró que el país están anhelando y buscando, concretamente de parte de grandes sectores sociales, un cambio muy significativo pero "el Gobierno se ha puesto al lado contrario de la ciudadanía".  En ese sentido, sostuvo que es necesario **abrir espacios para la transformación, y "no seguir con el miedo al cambio, porque ese miedo es fatal para el país".**

El Arzobispo de Cali explicó que este miedo se da en dos vías: El miedo al cambio de parte de un sector conservador de la sociedad, y el de quienes quieren imponer un cambio a través del miedo. No obstante, sostuvo que el miedo no tiene asidero en una sociedad que está buscando cambios democráticos, reales e inmediatos.

### **Connivencia entre paramilitares y Fuerza Pública es un tema "antiguo y evidente"** 

Respecto a la connivencia entre sectores de la Fuerza Pública y paramilitares, Monseñor afirmó que "es un tema muy antiguo y muy evidente", recordando que en reuniones que sostenían los obispos de Antioquia y Chocó con el entonces gobernador de Antioquía Álvaro Uribe, los prelados presentaban cartas, hechos y documentos que probaban esas alianzas.

Dichas alianzas continúan, pese a que se realizó el proceso de Justicia y Paz, el cual fue calificado por Monseñor como "Una gran estafa a la verdad, a la verdad de las víctimas", en tanto se limitó a ser solo un "acuerdo carcelario". (Le puede interesar: ["¿Qué pasó con la Ley 975 y el paramilitarismo en Colombia?"](https://archivo.contagioradio.com/que-paso-con-la-ley-975-y-el-paramilitarismo-en-colombia/))

El líder religioso agregó que el paramilitarismo llegó a todas las esferas de la sociedad, haciendo que el país caiga en **la mentalidad del asesinato, "de aniquilar al contrario".** Por esa razón, sentenció que se necesita hacer una revolución ética, psicológica y moral para que el derecho a la vida sea el pilar de nuestra sociedad. (Le puede interesar:["La defensa de la vida, un punto central del Paro Nacional"](https://archivo.contagioradio.com/la-defensa-de-la-vida-un-punto-central-del-paro-nacional/))

### **El Estado no persigue a los hechos sino a los denunciantes** 

Sobre la [respuesta del Comandante de la Fuerza de Tarea Titán](https://archivo.contagioradio.com/derecho-de-peticion-ejercito-exige-que-lideres-demostrar-denuncias/), solicitando a Leyner Palacios que le dé más información sobre sus denuncias respecto a actores armados ilegales que operan en Bojayá (Chocó), el Arzobispo dijo que esa comunicación es una muestra de que ahora **el problema no es que haya denuncias, sino que haya personas que denuncian.**

Siguiendo esta premisa, se explicarían los asesinatos a líderes sociales y defensores de derechos humanos, cuya labor básica es denunciar lo que ocurre en sus comunidades. En ese sentido, Monsalve aseveró que se necesita una gran reflexión nacional, porque enfrentamos "un problema de supervivencia pública".

### **Los acuerdos humanitarios los tendrá que empujar el pueblo en la calle** 

En cuanto a los acuerdos humanitarios, la posibilidad de procesos de paz y de sometimiento de actores armados, el Arzobispo de Cali declaró que se requiere un proceso simultáneo, en el que haya una voluntad del Gobierno. Pero en vista de que el Gobierno parece no creer en esa solución al conflicto, "entonces **al pueblo en la calle le va a tocar empujar por esa voluntad, para que haya procesos de Acuerdo"** y se logre el acogimiento a la justicia de los grupos armados ilegales.

Para avanzar en esta dirección, Monsalve expresó que es necesario acabar con los asesinatos a líderes sociales y firmantes del Acuerdo de Paz, así como que la Corte Penal Internacional le ayude a Colombia, porque la justicia nacional parece que está superada por la realidad del país. (Le puede interesar: ["Asesinan a tres líderes sociales en la noche de este 10 de Enero"](https://archivo.contagioradio.com/asesinan-tres-lideres-10-enero/))

De igual forma, apuntó que se debe continuar con la creación de pactos regionales de convivencia para lograr **lo urgente: La supervivencia de las comunidades en los territorios**, por lo que anunció que la lucha por la existencia y la convivencia pacífica la seguirán dando los obispos del Chocó junto a las comunidades.

### **La patria boba es nada al lado de la patria torpe que tenemos ahora  
** 

Siguiendo con la idea de la convivencia territorial, el Arzobispo de Cali concluyó que era necesaria una visión "muy audaz y sencilla" para **reabrir un mínimo de relaciones con Venezuela.** Agregando que entrar en conflicto con el vecino país "ha sido parte también de esta torpeza política que se apodera del país, yo creo que la patria boba es nada al lado de esta patria torpe que tenemos ahora".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_46590860" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46590860_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
