Title: "No hay garantías de derechos humanos para las mujeres en las cárceles de Colombia"
Date: 2018-05-02 17:13
Category: Expreso Libertad
Tags: colombia, prisioneras
Slug: mujeres-carceles-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Expreso-libertad-mujeres.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo /END 

###### 24 Abr 2018 

En Colombia existen 45 centros de reclusión en donde son ingresadas mujeres, sin embargo, de acuerdo con los testimonios de las ex prisioneras políticas solamente el 10% de estos lugares surgieron con la intensión de albergarlas, los demás fueron centros de reclusión acondicionados para conternerlas. Marcela Moreno, ex prisionera política, relató para los micrófonos del Expreso Libertad, la vida de quienes son llevadas a estos centros, la falta de acompañamiento por parte de las autoridades y la violación incluso a la misma dignidad de la que son víctimas.

![Expreso-libertad-mujeres](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/Expreso-libertad-mujeres-800x600.png){.alignnone .size-medium .wp-image-56999 width="800" height="600"}

<iframe id="audio_25751162" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25751162_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
