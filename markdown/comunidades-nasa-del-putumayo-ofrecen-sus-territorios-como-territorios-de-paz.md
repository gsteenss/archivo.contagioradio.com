Title: Comunidades Nasa del Putumayo ofrecen sus territorios como "territorios de paz"
Date: 2016-06-23 09:49
Category: Entrevistas, Paz
Tags: cese bilateral, Comunidades Nasa del Putumayo, Conpaz, FARC, Territorios de Paz
Slug: comunidades-nasa-del-putumayo-ofrecen-sus-territorios-como-territorios-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/asamblea-nasa-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: justiciaypaz] 

###### [23 Jun 2016]

Las comunidades indígenas Nasa del Pututmayo que realizaron su IV Congreso esta semana en el departamento del Putumayo y definieron que **ponen a disposición sus territorios para que allí se implementen las zonas de concentración de los integrantes de las FARC** en el marco del proceso de paz y de implementación de los acuerdos que se alcancen en la Habana. “Ponemos en disposición  nuestros territorios, en calidad de Territorios de Paz” reza el comunicado.

[Los territorios de las comunidades Nasa](https://archivo.contagioradio.com/movilizacion-de-comunidad-nasa-del-putumayo-detiene-exploracion-petrolera/) abarcan **38 cabildos en los que habitan más de 4000 indígenas**. Ellos y ellas en el congreso del pueblo Nasa, que se realiza cada 3 años, afirmaron la necesidad de avanzar con propuestas concretas y por ello propusieron sus propios cabildos. Según el comunicado es un interés en **aportar en la construcción de la paz y también en avanzar hacia las posibilidades de ejercer autonomía real sobre sus territorios.**

En ese mismo sentido resaltan que esa propuesta tiene “la finalidad de aportar a este momento importante en nuestro país y que nuestros territorios adquieran una categoría jurídica.” Y agregan que la paz es un [compromiso de todos y de todas](https://archivo.contagioradio.com/indigenas-de-putumayo-se-toman-secretaria-de-educacion-exigiendo-inicio-de-clases/) “Creemos en la reconciliación basada en la verdad y la generosidad de todos y todas, por eso nuestros territorios están con sus habitantes listos a **la construcción de la Paz en una democracia profunda de justicia ambiental y social.**”

Aquí el texto completo...

##### *Señor Presidente* 

##### ***JUAN MANUEL SANTOS*** 

##### *Señor* 

##### ***TIMOLEÓN JIMÉNEZ*** 

##### *Comandante Máximo de las FARC EP* 

##### *Cordial saludo,* 

##### *El suscrito gobernador del Cabildo Nasa Kwe´sx Kiwe, amparado por la ley 89 de 1890, la Ley 21 de 1991, convenio 169 de la OIT y el artículo 330 de la constitución política de 1991, en uso de sus facultades legales por medio del presente, les hace saber que la comunidad Nasa kwe´sx kiwe que hace parte de la Red Comunidades Construyendo Paz en los Territorios – CONPAZ, en el marco de la asamblea general  realizada el día 21 de mayo del presente año,  evaluó el proceso de paz en lo relacionado con el punto de tres(3) **<u>fin del conflicto armado en Colombia y la búsqueda de una paz estable y duradera</u>**, acción que comprometen todo el territorio nacional, del cual hacen parte los  territorios ancestrales de las comunidad indígena y en el particular los territorios del pueblo Nasa kwe´sx kiwe “ubicados en el  departamento del Putumayo, municipio de Puerto Asís, corregimiento de Piñuña Blanco.* 

##### *Ponemos en disposición  nuestros territorios, en calidad de Territorios de Paz. Esto con la finalidad de aportar a este momento importante en nuestro país y que nuestros territorios adquieran una categoría jurídica.* 

##### *Creemos en la reconciliación basada en la verdad y la generosidad de todos y todas, por eso nuestros territorios están con sus habitantes listos a la construcción de la Paz en una democracia profunda de justicia ambiental y social.* 

##### *Para mayor constancia se firma en el municipio de Puerto Asís, a los catorce (14) días del mes de junio de 2016* 

##### *Quedamos atentos-as a su respuesta,* 

##### ***Gobernador Nasa Kwe´sx Kiwe*** 
