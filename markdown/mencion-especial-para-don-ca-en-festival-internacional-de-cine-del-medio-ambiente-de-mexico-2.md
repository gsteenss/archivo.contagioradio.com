Title: Mención especial para "Don Ca" en Festival Internacional de Cine del Medio Ambiente de México
Date: 2015-04-13 11:39
Author: AdminContagio
Category: 24 Cuadros
Tags: Documentales, Don Ca, estival Internacional de Documentales Fidocs, Festival Biarritz Amérique Latin, Festival de Cannes, Festival Internacional de Cine del Medio Ambiente de México, Festival Spanisches Film Fest Berlín, H2O Mex, Internacional Visions Du Réel Suiza, José Cohen, Latin American Film Festival VLFF, Lorenzo Hagerman, Macondo, Pathos Audiovisual, Patricia Ayala
Slug: mencion-especial-para-don-ca-en-festival-internacional-de-cine-del-medio-ambiente-de-mexico-2
Status: published

La Ópera prima de Patricia Ayala continúa sumando reconocimientos a dos años de su estreno.

Don Ca, largometraje documental de la directora boyacense Patricia Ayala Ruíz recibió mención especial del jurado del Festival Internacional de Cine del Medio Ambiente de México, que tuvo lugar entre el 22 y el 28 de Marzo.

El documental producido por Pathos Audiovisual, fue reconocido por el  jurado compuesto por Montserrat Valeiras, Angel Pulido y Arturo Pons; por “su retrato profundamente humanista de un personaje tan cotidiano como extraordinario que, sin proponérselo, nos dio una lección de amor y fraternidad en defensa de las comunidades más marginadas”.

<iframe src="https://www.youtube.com/embed/8vhOadgOzmI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El premio a mejor largometraje Documental del Festival, fue otorgado por unanimidad para la co- producción argentino- chilena “Refugiados en su tierra” de Nicolas Bietti y Fernando Molina, “por mostrarnos con una visión tan honesta como descarnada, que no se puede poner precio a la dignidad humana, y que la solidaridad comunitaria y el amor familiar pueden superar hasta a las catástrofes inimaginables” en apreciación del jurado de expertos.

De igual manera, la producción “H2O Mex” de José Cohen y Lorenzo Hagerman recibió Mención especial del jurado por “abrirnos los ojos ante una realidad urgente e ineludible de una catástrofe que parece inevitable, y por mostrarnos que aún estamos a tiempo de enderezar el rumbo y construirnos un futuro mejor”.

El reconocimiento se suma a otros como el premio “Macondo” a Mejor documental 2013, entregado por la Academia Colombiana de las Artes y Ciencias Cinematográficas, y a su selección en festivales como el Internacional Visions Du Réel Suiza 2013, Festival de Cannes 2013 (Selección especial), Visions Du Réel (Competencia Largometraje, Festival Internacional de Documentales Fidocs Chile 2013Competencia Latinoamericana, Vancouver Latin American Film Festival VLFF Canadá 2013 (Competencia de largometraje), Festival Biarritz Amérique Latin 2013, Festival Spanisches Film Fest Berlín 2013 ( Selección Ventana Colombia), entre otros.

##### *Sinópsis:* 

##### *Don Ca es el retrato de un personaje tan rico como complejo, un hombre al que no es fácil clasificar bajo una etiqueta. Heredero de lo mejor y lo peor de la sociedad colombiana, decidió convertir su vida en un manifiesto libertario en el que la felicidad significa desear poco para poseerlo todo. Pero el mundo no perdona, el conflicto acecha y los paraísos se pierden. Casi cuarenta años después de haber tomado una ruta de vida sorprendente, Don Ca se plantea si debe renunciar a su universo, ése que está ubicado en el pacífico colombiano y que huele a selva y a río, pero también a tensión, a dolor y a peligro.* 
