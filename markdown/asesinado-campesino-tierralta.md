Title: Asesinado campesino en Tierralta, Córdoba
Date: 2017-12-24 10:50
Category: DDHH, Nacional
Tags: Asesinados, campesinos, colombia
Slug: asesinado-campesino-tierralta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/Imagen-lider-e1514130350548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Ascaprodesa 

###### 24 Dic 2017 

El cuerpo del campesino Guillermo Javier Artuz Tordecilla, integrande de Asociacion De Campesinos Productores De La Vereda La Ossa ASCAPRODESA, fue encontrado a la orilla del rio a la altura de chapinero en Tierralta, Córdoba en límites con el municipio de Valencia.

Según reporta la asociación Artuz Tordecilla, quien se desempeñaba como productor agricola y encargado de la bodega comercializadora de cacao y miel,  había sido reportado como desaparecido el 22 de diciembre cuando salió de su hogar para el trabajo promediando las 9 de la mañana.

En el reporte se indica que el  Gaula de la policía se encuentra haciendo inspección al cuerpo encontrado, y se afirma que con este nuevo hecho ya son tres los dirigentes gremiales asesinados en Tierralta en lo corrido del mes de diciembre.

Noticia en Desarrollo...

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
