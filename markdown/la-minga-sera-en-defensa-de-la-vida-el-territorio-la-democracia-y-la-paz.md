Title: Avanza la Minga en defensa de la vida
Date: 2020-10-12 08:53
Author: AdminContagio
Category: Actualidad, Nacional
Tags: CRIC, Minga Indígena, Movilización Indígena
Slug: la-minga-sera-en-defensa-de-la-vida-el-territorio-la-democracia-y-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Ptt-2020-10-09-at-12.55.55-PM.ogg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: CRIC

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este sábado 10 de octubre, cerca de 8.000 personas iniciaron la Minga Indígena del sur occidente colombiano por los derechos a la vida, el territorio, la democracia y la Paz. En esta movilización participarán personas indígenas, campesinas y afrocolombianas desde el departamento del Cauca. [(Minga del 10 de Octubre avanza a pesar de la estigmatización](https://archivo.contagioradio.com/minga-del-10-de-octubre-avanza-a-pesar-de-la-estigmatizacion/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ColHumanaEuropa/status/1314555525531602944","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ColHumanaEuropa/status/1314555525531602944

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este 12 de octubre las comunidades tienen previsto llegar a la ciudad de Cali, en **donde esperarán la presencia del presidente Iván Duque en la Plazoleta San Francisco para mantener un diálogo. Sin embargo, señalan que de no presentarse el presidente, continuarán su camino hacia Bogotá, con rumbo final, la plaza de Bolívar. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Guiovany Yule, [miembro del Consejo Regional Indígena del Cauca](https://www.cric-colombia.org/portal/) -CRIC- , aclara que en caso de no lograr establecer un diálogo en Bogotá, desde la Minga, **se hará un juicio político al Gobierno y se presentará una agenda política para la nación**, la cual pretende organizar un proyecto alternativo *“de una nación para la vida que afianza las esperanzas que nosotros hemos venido reforzando desde nuestras organizaciones y poblaciones”* .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma los anuncios por parte de las autoridades indígenas quienes explicaron que en el camino se unirán otras  organizaciones y sectores de diferentes regiones, y aclararon que no se bloqueará la carretera  internacional de la Panamericana. ([Minga indígena se reactivará el próximo 10 de Octubre](https://archivo.contagioradio.com/minga-indigena-se-reactivara-el-proximo-10-de-octubre/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

**Razones que activaron la Minga**
----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Yule resaltó que son múltiples las razones por las cuales sectores indígenas, campesinos y organizaciones sociales, han tomado la decisión de reactivar este ejercicio, el cual quedó pendiente el año pasado, y reafirma las razones que ya se tenían como, la **defensa de la vida, el territorio, la democracia y la Paz.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que esta Minga debe ser de interés nacional, ***"corresponde salir a denunciar y defender la vida ante todas las muertes, genocidios, etnocidios, feminicidios y las masacres"***, y agregó que en esta exigencia por medio de la movilización sociales el Gobierno debe responder a las diferentes denuncias *"ante los múltiples actos de violencia que se están recrudeciendo en los territorios y en las regiones del país".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Derecho al territorio**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**E**n cuanto al territorio, según Yule, las exigencias están relacionadas al cuidado y la protección del territorio y del ambiente que finalmente, ***"es el que ofrece condiciones de vida no solamente a las comunidades sino también a toda la población colombiana".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un aspecto que se relaciona a [reforma agraria integral](https://archivo.contagioradio.com/cinco-claves-para-entender-el-informe-de-michel-forst-sobre-la-situacion-de-dd-hh-en-colombia/)***“en el sentido de que la Constitución orienta con mucha claridad que los gobiernos deben de buscar la forma de entregar, dotar tierras a los sectores que viven en el campo y que se nos debe permitir no solamente el acceso a la tierra sino también condiciones para sembrarla y cultivarla”***.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El miembro del CRIC añade también que otro de los puntos que les convoca a salir a las calles es expresar que desde las comunidades se rechaza el avance de las actividades extractivistas que *“busca entregar los recursos naturales como materia prima a las multinacionales”.*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Derecho a la democracia

<!-- /wp:heading -->

<!-- wp:paragraph -->

En relación a la defensa de la vida, a la luz del desbalance en el sistema de pesos y contra pesos Yule afirma que, *“desafortunadamente hoy la figura presidencial ha concentrado todos los poderes”* , causado así un **detrimento de la democracia y** ***"deteriorando muchos de los derechos fundamentales que los colombianos han conquistado** y que han costado tantas vidas"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este aspecto exige como vocero de ACIN que **se garantice el derecho a la protesta social y a la participación social y política**, la cuál en los últimos meses a sido violentada, criminalizada y censurada, *"la movilización social es un derecho, y como tal debe ser respetado, pero en Colombia históricamente ha sido violentado por el Estado"*. ([Testimonio: Así son las torturas de la Policía a ciudadanos detenidos en la protesta social.](https://archivo.contagioradio.com/testimonio-asi-son-las-torturas-de-la-policia-a-ciudadanos-detenidos-en-la-protesta-social/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desde la Minga exigen el derecho a la paz

<!-- /wp:heading -->

<!-- wp:paragraph -->

Otro de los planteamientos que la Minga busca hacer es la desmilitarización de los territorios pues ***“el gobierno está totalmente equivocado pensando en que solamente puede solucionar los problemas militarizando los territorios, militarizando la vida social y comunitaria de la población”*.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este aspecto en relación al manejo que se le esta dando al trabajo de la fuerza pública, Yule destaca que l**a Minga plantea que se haga una reforma de la doctrina militar,** esto debido, *"a los sucesos en que la fuerza pública ha agredido y violado los derechos de quienes salen a manifestarse". *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y resaltó que además existen argumentos y evidencias suficientes para que se desmonten los grupos paramilitares que aún operan en varios departamentos, y con ello todo lo que tiene que ver con grupos al margen de la ley, y en cabeza del narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a este punto añadió que se deben *"establecer mecanismos de diálogo para que las guerrillas que están todavía con las armas tomen la decisión de reintegrarse a la vida civil”*. (Le puede interesar: [Minga indígena se reactivará el próximo 10 de Octubre](https://archivo.contagioradio.com/minga-indigena-se-reactivara-el-proximo-10-de-octubre/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente otros de los temas de los que se espera mantener diálogo es la implementación, justa, coherente y en coherencia con la realidad de los territorios de Acuerdo de Paz; el cumplimiento del derecho a la consulta previa; el reconocimiento del campesinado como sujetos de derechos y el inicio de las conversaciones entre el gobierno y el ELN para lograr un acuerdo político. 

<!-- /wp:paragraph -->

<!-- wp:audio {"id":91274} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/WhatsApp-Ptt-2020-10-09-at-12.55.55-PM.ogg">
</audio>
  

<figcaption>
Escuche el audio completo de la entrevista de Guiovany Yule

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
