Title: Los asesinatos de líderes sociales son sistemáticos: Ángela María Robledo
Date: 2018-07-24 16:05
Category: Paz, Política
Tags: Alberto Castilla, Angela Maria Robledo, Bancada de oposición, lideres sociales asesinados
Slug: asesinatos-de-lideres-sociales-son-sistematicos-angela-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-24-a-las-3.50.21-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @angelamrobledo] 

###### [24 Jul 2018] 

En rueda de prensa, la Bancada por la vida y por la paz, integrada por miembros de la oposición, se refirió al asesinato de líderes sociales en el país. En ella la representante a la Cámara **Angela María Robledo afirmó que estos crímenes responden a patrones identificables, lo que confirmaría la hipótesis de que es un fenómeno sistemático.**

Según Robledo, en los informes presentados por diferentes organizaciones, se da cuenta de la sistematicidad en los asesinatos, así como en los contextos de los líderes sociales. En su criterio, está deducción exige a la Fiscalía hacer algo más que detener a sicarios, y  tener acciones concretas para el funcionamiento de **"la unidad de investigación de contexto, que no se ha estrenado".**

Por su parte, el Senador del Polo Democrático, Alberto Castilla, señaló que hay móviles claros en el asesinato de líderes sociales entre los cuales están "la sustitución de cultivos \[de uso ilícito\]; la restitución de tierras" y  contra quienes defienden el agua, los territorios y se oponen al sector extractivo.

Castilla también se refirió a la judicialización de líderes sociales, afirmando que es una forma más de atacar a la protesta social, y concluyó manifestando que **"en los últimos meses, solo el Congreso de los Pueblos ha vivido la judicialización de 52 líderes"**, lo que para él representa la muestra clara, de que hay una política de 'encarcelación' contra quienes defienden la democracia. (Le puede interesar: ["Congreso de los Pueblos denunció la captura de cerca de 40 de sus integrantes"](https://archivo.contagioradio.com/congreso-de-los-pueblos-denuncio-la-captura-de-cerca-de-40-de-sus-integrantes/))

**Los 4 puntos claves de la Bancada de Oposición**

Angela Robledo resaltó que la Bancada de Oposición, que lideran los miembros de la Colombia Humana, se constituye con base en la defensa por la vida; respaldar el acuerdo de la Habana y la construcción de una paz estable y duradera; la participación de los territorios en la política; y la agenda legislativa y ejercicio de control político, cuyo primer acto será un debate en el Congreso sobre el asesinato de líderes sociales.

Este anunció se produce tras el asesinato de **Libardo Moreno,** miembro de la Junta de Acción Comunal de la Vereda Las Pilas, Valle del Cauca, ocurrido el lunes a manos de sicarios; y el homicidio de **Kevin Julian León,** joven líder de 16 años en Medellín, Antioquia. Hechos que se suman a los crímenes de** José Osvaldo Taquez Taquez y Horacio Triana Parra.** (Le puede interesar: ["Dos líderes sociales asesinados durante el 20 de julio"](https://archivo.contagioradio.com/lideres-asesinados-el-20-de-julio/))

### **El 7 de agosto nos encontraremos para defender la vida y la paz: Alberto Castilla** 

Por estas razones, los miembros de la Bancada de Oposición invitaron a la ciudadanía a acompañar el proceso legislativo con una **movilización el 7 de Agosto.** El senador del Polo señaló que con la movilización " reclamaremos el derecho a vivir en Colombia, a vivir en libertad y a hacer oposición". (Le puede interesar: ["De la plaza pública al Congreso: La agenda legislativa de la oposición"](https://archivo.contagioradio.com/de-la-plaza-publica-al-congreso/))

######  

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
