Title: Incorporación del enfoque de género: "un hecho histórico que ningún proceso de paz ha logrado"
Date: 2016-07-25 20:15
Category: Paz
Tags: Diálogos de paz Colombia, enfoque de género en los acuerdos de paz, Subcomisión de género
Slug: incorporacion-del-enfoque-de-genero-un-hecho-historico-que-ningun-proceso-de-paz-ha-logrado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Mujeres-afro-e1461609635108.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [25 Julio 2016]

Para Adriana Benjumea, directora de la Corporación Humanas, el acuerdo anunciado ayer desde La Habana demuestra "que es posible lograr un acuerdo de paz en el que se incluyan las demandas de las mujeres en todos los temas (...) **un hecho histórico que ningún proceso de paz ha logrado** (...) poder tener los lentes de género poder decir cómo el conflicto armado ha impactado diferencialmente a las mujeres".

De acuerdo con Benjumea comprender la dimensión del acuerdo, pasa por entender que **las mujeres en Colombia históricamente han sido discriminadas en temas como la tenencia y permanencia de la tierra**,** **han sido excluidas de ser propietarias, y cuando han llegado a serlo se han enfrentado a mayores dificultades para permanecer en sus tierras en condiciones dignas y seguras, por lo que es necesario que la implementación de los acuerdos incluya acciones que ayuden a superar esos obstáculos.

Por su parte Marina Gallego, coordinadora de la Ruta Pacífica de las Mujeres, asegura que las propuestas de las organizaciones de mujeres deben fortalecerse para garantizar que en la implementación de los acuerdos se mantenga el enfoque de género. El primer paso debe ser estudiar lo pactado para apropiarlo y poder establecer articulaciones para que las mujeres incidan de acuerdo a las características propias de cada territorio, en un **acuerdo de paz que debe materializarse en políticas públicas financiadas por el Estado** y no en proyectos aislados sí lo que se quiere es lograr que influya en la cotidianidad.

Para las organizaciones de mujeres es necesario que hayan funcionarios con sensibilidad de género en todo lo que demanda la Justicia Especial para la Paz, teniendo en cuenta que el acuerdo de paz permitirá develar todos los tipos de violencias contra las mujeres en el marco del conflicto armado y que **es necesario que se logren cambios estructurales con incidencia en la cotidianidad tanto a nivel individual como colectivo** "para que no sean las armas lo que se siga imponiendo", como asevera Gallego.

El acuerdo a su vez demuestra la voluntad política tanto del Estado como de las FARC-EP y el **compromiso que cada delegación asumió para escuchar las demandas de las mujeres** colombianas asegura Benjumea y agrega que "el movimiento de mujeres, el movimiento de derechos humanos, las comunidades más empobrecidas de este país y las más excluidas, estamos por el si al proceso de paz (...) lo que tenemos que hacer es informar a los sectores que por desconocimiento han menguado la confianza de un país en paz".

<iframe src="http://co.ivoox.com/es/player_ej_12342459_2_1.html?data=kpeglpeYeZqhhpywj5WbaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncaLY087O0MaPhsbiy9rax8aJdqSfpNTf0tTWpcTdhqigh6eXsoy81tLO0MbXcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="http://co.ivoox.com/es/player_ej_12342492_2_1.html?data=kpeglpeYfZOhhpywj5aVaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5ynca7V087bw5Crpc3gxszch5enb7Pp1caYssbHaaSnhqaxyM7HpYzYxpDZw9iPkdbextfS1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
