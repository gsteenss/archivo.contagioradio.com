Title: Organizaciones de DDHH y comunidades están de luto ante asesinato de Carlos Pedraza
Date: 2015-01-23 20:17
Author: CtgAdm
Category: DDHH
Tags: Amenazas, Carlos Pedraza, Derechos Humanos, Paramilitarismo
Slug: asesinado-defensor-de-derechos-humanos-carlos-pedraza
Status: published

###### Foto: Congreso de los Pueblos 

**[Entrevista con Silvia Oviedo:]** <iframe src="http://www.ivoox.com/player_ek_3991325_2_1.html?data=lJ6mk5iWeY6ZmKiak5aJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8rg187OjbTarcbY0JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El cuerpo sin vida de Carlos Pedraza fue hallado el pasado 21 de enero en la localidad de Gachancipá, cercana a la ciudad de Bogotá. **Carlos era un reconocido defensor de Derechos Humanos**, constructor de alternativas de comercialización justa con colectivos de campesinos, integrante del proyecto Nunca Más y del Congreso de los Pueblos.

Según la información, que ha sido restringida por las autoridades, Carlos fue hallado sin vida, sin pertenencias y con un golpe en la cabeza. A pesar de no tener amenazas directas, los integrantes del Congreso de los Pueblos fueron amenazados el pasado lunes a través de un panfleto firmado por las “Aguilas Negras” al mismo que diversas organizaciones sociales y varios defensores de Derechos Humanos.

Según Silvia Oviedo, integrante de “Trochando sin fronteras”, las autoridades han restringido la información sobre la muerte de Carlos, pero se espera que en las próximas horas se establezca una primera hipótesis y se le entregue a los representantes de la familia.

Durante lo corrido del 2015 ya son varias las amenazas contra defensores de Derechos Humanos y reconocidos líderes políticos como Piedad Córdoba, lo cual eleva las alarmas en cuanto a la necesidad de garantías para ellos y ellas.

Según el Frente Amplio por la Paz, es necesario buscar mecanismos integrales que no se limiten a las medidas de protección para las personas amenazadas, sino que vayan en busca de las personas u organizaciones criminales que están detrás de las amenazas.

[Ver comunicado de la organización Congreso de los Pueblos.](http://congresodelospueblos.org/index.php/pueblo-en-lucha/ultimas-noticias/647-denuncia-publica-por-el-asesinato-de-carlos-alberto-pedraza-salcedo)
