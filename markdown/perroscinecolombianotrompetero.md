Title: "Perros" un drama carcelario
Date: 2017-03-23 16:34
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, colombiano, Perros, trompetero
Slug: perroscinecolombianotrompetero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/harold-trompetero-llega-con-su-pelicula-perros-504386.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Película perros 

###### 23 Mar 2017

Este jueves 23 de marzo se estrena en salas de cine del país la película “Perros” , drama dirigido por el cineasta colombiano Harold Trompetero, quien por segunda vez se aventura a realizar un film alejado de la comedia.

La película cuenta la historia de Misael, un campesino que es condenado a prisión por cometer un crimen pasional. Durante su encierro debe enfrentarse a los diversos maltratos y abusos por parte de los guardias y los reos. En medio del dolor y la soledad Misael encuentra una chispa de felicidad y cariño en ‘Sarna’, la perra del penitenciario.

Antes de su estreno nacional, la película fue presentada en el Festival de cine Independiente de Bogotá Indiebo en 2016; de igual forma “Perros” clausuró la octava edición del Festival Internacional de Cine de Santander FICS en 2016 y, coincidencialmente, en el día de su estreno nacional será la película que inaugurará la versión 2017 del Colombian Film Festival de New York.

<iframe src="https://www.youtube.com/embed/T8gBW3DR1vw" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El largometraje, rodado en la población cundinamarqués de Facatativa, cuenta con las actuaciones del actor colombo-estadounidense John Leguizamo, acompañado por la actores colombianos Maria Nela Sinisterra, Álvaro Rodríguez, Hernán Méndez y Ramiro Meneses, y la actriz mexicana Adriana Barraza, nominada a un Oscar de la Academia por su actuación en la cinta Babel.
