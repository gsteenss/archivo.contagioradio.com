Title: Defensores de DDHH del Tolima amenazados ante intención de voto por Gustavo Petro
Date: 2018-05-25 12:15
Category: DDHH, Nacional
Tags: Aguilas Negras, Amenazas a defensores de Derechos Humanos, elecciones, paramilitares
Slug: defensores-de-ddhh-del-tolima-amenazados-ante-intencion-de-voto-por-gustavo-petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/amenaza-copy.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Organizaciones denunciantes en Tolima] 

###### [25 May 2018] 

Diferentes organizaciones sociales del Tolima denunciaron haber recibido amenazas de muerte por parte del grupo paramilitar las **“Águilas Negras”**. El panfleto fue enviado a defensores de derechos humanos, ambientalistas, sindicalistas y mujeres donde fueron declarados “objetivo militar” si apoyan en las elecciones presidenciales al candidato Gustavo Petro.

De acuerdo con la comunicación, las organizaciones recibieron el panfleto vía correo electrónico. Ante esto, manifestaron que, **con anterioridad**, varias organizaciones han recibido amenazas por parte de grupos paramilitares “sin que hasta el momento se tomen medidas de protección por parte de la Unidad Nacional de Protección”.

### **Organizaciones pidieron a la ciudadanía no dejarse intimidar en los comicios presidenciales** 

Hicieron responsable al presidente Juan Manuel Santos, las Fuerzas Militares y la Policía Nacional **“de lo que pueda suceder a cualquiera de los integrantes”** de los colectivos denunciantes. Afirmaron que han “colaborado y proporcionado números telefónicos y correos electrónicos de las respectivas amenazas para agilizar las investigaciones respectivas”. Sin embargo, “hasta el momento no se tiene conocimiento de captura o resultados contundentes de las investigaciones por parte de la Fiscalía General de la Nación”.

Con esto en mente, invitaron a la ciudadanía a **“no dejarse intimidar”** y ejercer su derecho al voto este 27 de mayo. Indicaron que debe haber una unión que garantice una transición “consciente y democrática, hacia una era de paz donde el Estado, y quienes lo gobiernen, garanticen los elementos fundamentales que históricamente la ciudadanía colombiana ha exigido”. (Le puede interesar:["Águilas Negras amenazan de muerte a mujeres líderes, congresistas y periodistas"](https://archivo.contagioradio.com/aguilas_negras_amenzas_mujeres/))

A lo anterior se suma el mensaje de **“solidaridad y apoyo”** que enviaron al candidato de la Colombia Humana “por los múltiples señalamientos, estigmatizaciones y atentado del cual ha sido víctima en el proceso de campaña”. Ante los hechos, exigieron al alcalde de Ibagué y al gobernador del Tolima “garantizar la integridad de las organizaciones de defensores de derechos (…) y medios comunitarios del departamento que también fueron amenazados”.

Finalmente, le exigieron a la Fiscalía General de la Nación investigar y esclarecer los hechos y que el Gobierno Nacional **otorgue las medidas de protección** necesarias para garantizar “el trabajo, la integridad y honra de las personas y organizaciones de defensores de derechos humanos, sociales, sindicales, ambientales y de mujeres a nivel nacional”.

[A Me Naz as Dirigente s Social Esto Lima](https://www.scribd.com/document/380169496/A-Me-Naz-as-Dirigente-s-Social-Esto-Lima#from_embed "View A Me Naz as Dirigente s Social Esto Lima on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_45689" class="scribd_iframe_embed" title="A Me Naz as Dirigente s Social Esto Lima" src="https://www.scribd.com/embeds/380169496/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-RwzxUtdWbKageXgO3tau&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7727272727272727"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
