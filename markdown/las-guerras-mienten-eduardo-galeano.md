Title: "Las guerras mienten" Eduardo Galeano
Date: 2016-09-03 10:00
Category: Cultura, Otra Mirada
Tags: 76 años, Eduardo Galeano, Eduardo Galeano paz
Slug: las-guerras-mienten-eduardo-galeano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/galeano.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La mañana On line 

El **3 de septiembre de 1940**, el mundo recuerda un aniversario más del escritor, periodista y pensador uruguayo **Eduardo Germán Hughes Galeano**, quien desde las letras propuso una nueva perspectiva política, social y cultural para los pueblos del mundo y en particular para aquellos que hacen parte de América Latina.

En esta oportunidad y con particular ocasión del **aniversario 78 de su nacimiento**, retomamos uno de sus discursos sobre la necesidad de para la guerra, enviado con motivo de la realización de la *Marcha Mundial por la paz y la no violencia*, que tuvo lugar en octubre de 2009.

"Ninguna guerra tiene la honestidad de confesar: Yo mato para robar.

Las guerras siempre invocan nobles motivos, matan en nombre de la paz, en nombre de dios, en nombre de la civilización, en nombre del progreso, en nombre de la democracia y si por las dudas, si tanta mentira no alcanzara, ahí están los grandes medios de comunicación dispuestos a inventar enemigos imaginarios para justificar la conversión del mundo en un gran manicomio y un inmenso matadero.

En Rey Lear, Shakespeare había escrito que en este mundo los locos conducen a los ciegos y cuatro siglos después, los amos del mundo son locos enamorados de la muerte que han convertido al mundo en un lugar donde cada minuto mueren de hambre o de enfermedad curable 10 niños y cada minuto se gastan 3 millones de dólares, tres millones de dolares por minuto en la industria militar que es una fábrica de muerte.

Las armas exigen guerras y las guerras exigen armas y los cinco países que manejan las naciones unidas, los que tienen derecho de veto en las Naciones Unidas resultan ser también los cinco principales productores de armas.

Uno se pregunta ¿Hasta cuando? ¿Hasta cuando la paz del mundo estará en manos de los que hacen el negocio de la guerra?

¿Hasta cuando seguiremos creyendo que hemos nacido para el exterminio mutuo y que el exterminio mutuo es nuestro destino?

¿Hasta cuando?"
