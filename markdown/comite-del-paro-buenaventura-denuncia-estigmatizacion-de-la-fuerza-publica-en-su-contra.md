Title: Comité del Paro de Buenaventura denuncia estigmatización de la Fuerza Pública en su contra
Date: 2020-12-07 22:04
Author: CtgAdm
Category: Actualidad, DDHH
Tags: buenaventura, desempleo, Fuerza Pública
Slug: comite-del-paro-buenaventura-denuncia-estigmatizacion-de-la-fuerza-publica-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/129279040_2499598840345223_3973520976494353699_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comite del Paro Cívico de Buenaventura

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Comite del Paro Cívico de Buenaventura** denuncia que funcionarios de la Alcaldía y Policía del municipio rechazan al movimiento y se niegan a compartir espacios que involucren a sus integrantes. Denuncian que esta no sería la primera ocasión en la que miembros de dichas instituciones les rechazan por hacer parte de la organización.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El episodio se dio el pasado 6 de diciembre cuando una delegación del Comité acompañó al gremio de mototaxistas que se reuniría con funcionarios de la Alcaldía y la Policía para mediar entre ambas partes en medio de una continúa problemática por la reglamentación de los motorizados. Sin embargo, la reunión nunca se dio cuando funcionarios y uniformados vieron que los integrantes del Comité del Paro acompañarían la reunión.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según **Maria Miyela Riascos, integrante del Paro Cívico de Buenaventura,** ya son varias las ocasiones en las que la Fuerza Pública del municipio ha optado por rechazarlos, al expresar que otro de estos hechos se dio en 2019 cuando notó que habían dos personas sospechosas a las afueras de su casa y al llamar a la Policía su respuesta fue que **"no asistirían a a su casa porque hacía parte del paro cívico".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe resaltar que los miembros del Paro Cívico han sido amenazados o su vida ha sido puesta en riesgo en ocasiones anteriores, tal es el caso de personas como el padre **John Reina, Carlos Tobar, Danelly Estupiñán, Yency Murillo Sarria y Mary Cruz Rentería.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "A todas luces se ve una persecución, una estigmatización, un rechazo hacia al movimiento del Paro Cívico que no hace otra cosa distinta a la defensa de la vida del territorio y la cultura, no toleramos el abuso de la fuerza publica en la población de Buenaventura, lo que ha hecho que nos vea como enemigos"
>
> <cite>María Miyela Riascos</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

**"Se supone que la Alcaldía surge de nuestro movimiento, pero nosotros dejamos la libertad al alcalde que consiguiera los secretarios de despacho y resulta que nos están desconociendo**" afirma la la integrante del Comité por lo que señala, se realizará una evaluación para exigir el cambio de aquellas personas "que no son afines a la comunidad", resalta además que dichas actitudes de los funcionarios no cuentan con el respaldo del alcalde, Víctor Vidal. [(Lea también: Atentado contra Alcaldía de Buenaventura no es una casualidad)](https://archivo.contagioradio.com/atentado-contra-alcaldia-de-buenaventura-no-es-una-casualidad/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desempleo siempre ha existido, incluso antes de la pandemia

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Con respecto a la situación de los mototaxistas, señala que en la actualidad, este sector está compuesto por cerca de 2.500 a 3.000 personas que trabajan entre jóvenes y adultos para garantizar la alimentación de sus hogares, y pese a que reconocen que deben mejorar su documentación y reglamentación, esta es una forma de empleo en medio de una crisis económica que azota al país que ha sido rechazada por las autoridades.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la Alcaldía señalan que los mototaxistas deben realizar un censo que permita saber con exactitud la cantidad de personas que conforman el sector y así facilitar la consecución de los documentos y el cumplimiento de ciertos requisitos de ley, sin embargo, se siguen decomisando motos e impidiendo su labor sin ofrecerse alternativas de empleo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Líderes sociales del municipio como Orlando Castillo han resaltado que ad portas de la pandemia, cerca del 80% de la población carecía de ingresos regulares, mientras la mayoría de la gente vivía del rebusque o del ‘día a día', por su parte Riascos advierte que si antes del Covid-19 la pobreza multimodal se ubicaba cerca a un 37% en la actualidad esta estaría cerca al 70%.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Una de las principales razones de esta carencia de empleo se deriva del abandono estatal y la imposibilidad de sus habitantes de alcanzar mayores niveles educativos; hoy dichos factores sumados a la pandemia han llevado a que la mayor pérdida de empleos ocurriera en los sectores de transporte y logística, servicios administrativos y construcción.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Inseguridad persiste en Buenaventura

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Miyelas Riasco asegura que pese al aumento del pie de fuerza en el municipio, la inseguridad es evidente en los puertos de embarque y cabotaje donde robos y extorsiones llevaron a que del 16 al 22 del pasado mes, más de 8.000 trabajadores y 90 embarcaciones paralizaran el transporte de víveres, mercancías y personas hasta que se ofrecieran soluciones. [(Lea también: Comunidades siguen clamando por un acuerdo humanitario)](https://archivo.contagioradio.com/comunidades-reiteran-necesidad-de-un-acuerdo-humanitario/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Están siendo atracados y extorsionados, todos los días roban a sus pasajeros y los tiran al agua a la hora de cargar y descargas los barcos (...) hay un tema de abandono social y estatal y las posibilidades que se le dan a las personas son pocas o casi nulas" explica la integrante del Comité quien agrega que prohibirles o reprimir su fuente de ingresos conduce a que **"la gente engorde las cadenas de delincuencia".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, a lo largo del año se llamó la atención sobre [las amenazas que han surgido](https://www.justiciaypazcolombia.com/amenazan-a-lideres-y-lideresas-para-imponer-microtrafico/) a través de estructuras criminales herederas del paramilitarismo, que buscan amedrentar a los líderes y lideresas que se oponen a la vinculación de menores de edad a redes de** microtráfico**. [(Lea también: Preservemos la vida ante la pandemia: Espacio humanitario a actores armados en Buenaventura)](https://archivo.contagioradio.com/preservemos-la-vida-ante-la-pandemia-espacio-humanitario-a-actores-armados-en-buenaventura/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A propósito, el pasado 6 de diciembre en horas de la noche, en la Vereda El Credo del Corregimiento de Cisneros, de Buenaventura, fue asesinado el líder Social **Joaquín Antonio Ramírez quien hacía parte del Consejo Comunitario Pacífico Cimarrones de Cisnero.**

<!-- /wp:paragraph -->

<!-- wp:image {"id":93871,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/129279040_2499598840345223_3973520976494353699_n.jpg){.wp-image-93871}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"justify"} -->

Joaquín, de 69 años fue hallado al interior de su vivienda con múltiples heridas de bala. Suceso que eleva el registro de líderes sociales asesinados a 286 en lo corrido del 2020 [(Lea también: Las enseñanzas de 'don Temis' son símbolo de resistencia en el barrio Isla de la Paz)](https://archivo.contagioradio.com/las-ensenanzas-de-don-temis-son-simbolo-de-resistencia-en-el-barrio-isla-de-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
