Title: Muere otro interno de cárcel de Villavicencio, se estudia si está relacionado con Covid-19
Date: 2020-04-11 15:18
Author: AdminContagio
Category: Actualidad, DDHH
Tags: #Recluso, Villavicencio
Slug: muere-otro-interno-de-carcel-de-villavicencio-se-estudia-si-esta-relacionado-con-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Cárcel-de-Villavicencio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Inpec {#foto-inpec .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este sábado 11 de abril se conoció que otro interno de la cárcel de Villavicencio, en Meta, falleció por problemas respiratorios, actualmente aún se estudia si el hombre tenía el virus del Covid-19. Además de esta persona, autoridades de la ciudad confirmaron que tres internos más del penal están hospitalizados con síntomas relacionados con el virus, pero también se espera la confirmación mediante resultados de laboratorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La situación sanitaria en la cárcel de Villavicencio

<!-- /wp:heading -->

<!-- wp:html -->

> Hago un nuevo llamado al Gobierno Nacional ante los graves hechos que se conocen sobre contagios de coronavirus en la cárcel de Villavicencio: [pic.twitter.com/CN3ZW6eRiC](https://t.co/CN3ZW6eRiC)
>
> — Iván Cepeda Castro (@IvanCepedaCast) [April 11, 2020](https://twitter.com/IvanCepedaCast/status/1249010473044975616?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

Contagio Radio comunicó el pasado viernes 10 de abril sobre la muerte de un hombre de 63 años de edad el 7 de abril, luego que fuera dejado en libertad y presentara síntomas el primer día del mes. El Instituto Nacional Penitenciario y Carcelario (INPEC) informó en un comunicado que el hombre había dado positivo en la prueba de Covid-19, y añadió que pondría en marcha medidas para prevenir nuevos casos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, este domingo se conoció de un segundo caso, de un hombre con síntomas relacionados con el virus que perdió la vida y aún se estudia si efectivamente padecía el Covid-19. Adicionalmente se supo que tres internos más están hospitalizados con dolencias que responderían al virus, pero aún no han sido confirmadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los dos internos que fallecieron estaban en el mismo pabellón, adicionalmente, según se denunció por [redes sociales](https://www.facebook.com/jhon.leon.1213/videos/1303125419882334/), hay un brote de tuberculosis en el penal que no ha sido tratado adecuadamente, lo que mantiene el riesgo sobre la población recluida. (Le puede interesar: ["A pesar de exigencias de Movimiento carcelario muere primera persona por COVID 19"](https://archivo.contagioradio.com/a-pesar-de-exigencias-de-movimiento-carcelario-muere-primera-persona-por-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La situación puede extenderse a otros centros de reclusión

<!-- /wp:heading -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/928797384222868/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/928797384222868/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->
