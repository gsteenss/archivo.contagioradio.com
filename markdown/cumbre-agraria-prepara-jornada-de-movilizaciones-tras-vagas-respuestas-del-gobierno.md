Title: Cumbre Agraria prepara jornada de movilizaciones tras "vagas respuestas del gobierno"
Date: 2015-06-30 17:20
Category: Movilización, Nacional
Tags: 2013, ANOZRC, Asociación Nacional de Zonas de Reserva Campesina., César Jeréz, Cumbre Agraria, Gobierno Nacional, Juan Manuel Santos, Movilización, Paro Agrario
Slug: cumbre-agraria-prepara-jornada-de-movilizaciones-tras-vagas-respuestas-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/CUMBRE1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimundo 

<iframe src="http://www.ivoox.com/player_ek_4706949_2_1.html?data=lZydmJ6YfY6ZmKiakpuJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9bhw9fSjcbLtsLmysaY0tfJtMLmwpDa0dvNsMruwsjW0dPJt4zo08bgjc7Sp9bh0dHWz87Jso6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [César Jeréz, Asociación Nacional de Zonas de Reserva Campesina, ANZORC] 

###### [3o Jun 2015]

“**Se trató de una reunión de trámite donde no hubo respuestas concretas de parte del gobierno**”, dice César Jeréz, integrante de la Asociación Nacional de Zonas de Reserva Campesina, ANZORC, al referirse a la reunión que sostuvieron la semana pasada voceros de la Cumbre Agraria con el presidente Juan Manuel Santos.

El objetivo de la reunión con el gobierno nacional, era conocer **el porqué de los incumplimientos a los acuerdos** realizados tras las movilizaciones del paro agrario del 2013. Fue por eso, que desde la Cumbre Agraria, se presentó la situación de falta de compromiso del gobierno, respecto a temas sobre derechos humanos y garantías, recursos económicos e infraestructura social y la ruta de negociación.

**“El gobierno respondió con propuestas de reunión y comisiones”** además, el presidente Santos, dijo que “existen los recursos para iniciar proyectos agroproductivos, pero la cifra destinada a estos proyectos tendrá vigencia cada año”, es decir que se estaría rompiendo uno de los acuerdos pactados, como lo afirma Jeréz.

Tras la reunión, se estableció un plazo de dos semanas para empezar los diálogos, mientras tanto, y debido a la **“vaga respuesta del gobierno**”, como lo señala el integrante de ANZORC, varias organizaciones indígenas y las mesas regionales, ya realizan los **preparativos para una nueva jornada de movilizaciones** por la falta de compromiso de Santos.
