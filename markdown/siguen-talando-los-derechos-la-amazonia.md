Title: Siguen talando los derechos de la Amazonia
Date: 2019-04-05 18:00
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Amazonía, Corte Suprema de Justicia
Slug: siguen-talando-los-derechos-la-amazonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-58.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [5 Abr 2019] 

Un año después de que la Corte Suprema de Justicia reconoció a la** **Amazonía colombiana como sujeto de derechos y ordenó al Estado a formular mecanismos para frenar la deforestación, las acciones del Gobierno han sido mínimas. Así lo denunció la organización Dejusticia y los accionantes de la tutela, quienes hoy solicitaron al **Tribunal Superior de Bogotá que declare el incumplimiento de las órdenes de la Corte Suprema por parte del Gobierno y de los demandados.**

Las cuatro órdenes del alto tribunal fueron las siguientes: formular un **plan de acción para contrarrestar la deforestación** en la Amazonía; crear el **Pacto Intergeneracional por la Vida del Amazonas**; **actualizar e implementar los Planes de Ordenamiento Territorial **de los municipios de la Amazonía; y finalmente, realizar un plan de acción por parte de las **Corporaciones Autónomas Regionales** pertinentes. (Le puede interesar: "[El precedente que marca el fallo sobre la Amazonía colombiana](https://archivo.contagioradio.com/fallo_historico_corte_suprema_amazonas/)")

El plazo para cumplir estas órdenes venció en agosto y septiembre de 2018 y según Gabriela Eslava, investigadora de Dejusticia y representante legal de los accionantes, las medidas que el Gobierno ha tomado han sido insuficientes. El último reporte del Instituto de Hidrología, Meteorología y Estudios Ambientales (IDEAM) indicó que la tasa de deforestación en el país aumentó durante el último trimestre de 2018 y que el **75% de la tala de árboles se concentró en la Amazonía**. (Le puede interesar: "[Deforestación sigue aumentando en la Amazonía colombiana](https://archivo.contagioradio.com/deforestacion-siguen-aumentando-la-amazonia-colombiana/)")

Eslava señala que no hay voluntad política por parte del Gobierno para entender la dimensión de la emergencia ambiental a pesar de los estudios científicos que evidencian las afectaciones que ocasionará el cambio climático en un futuro cercano. "**Se les olvida que frente eso datos, hay personas que van a sufrir esos efectos**", manifestó. La investigadora agrega que la deforestación de la Amazonía también se trata de la vulneración de derechos  fundamentales de los colombianos **al agua, al aire puro y a un ambiente sano**, tal como lo reconoció la Corte Suprema en la sentencia.

Sin embargo, la política del nuevo Gobierno genera dudas entre los ambientalistas sobre el nivel de compromiso del presidente Iván Duque con la conservación de la Amazonía. Por un lado, el Gobierno no volvió a convocar talleres con los accionantes y las comunidades de la región, como lo había hecho el Gobierno anterior, para generar propuestas para el plan de acción y el Pacto Intergeneracional por la Vida.

Además, el Plan Nacional de Desarrollo (PND) que propone el Presidente no busca reducir el número de hectáreas deforestadas sino mantenerlo constante durante los próximos cuatro años, determinación que Dejusticia indica va en contra de la sentencia de la Corte Suprema, aludiendo que "si se conserva el ritmo actual de deforestación, Colombia perdería, a 2020, más de 800.000 hectáreas de bosques: aproximadamente cinco veces el tamaño de Bogotá".

Por tal razón, la organización creó una [petición](http://chng.it/ZbZhcMr8)en la plataforma Change.com para que el Gobierno cambié la meta de deforestación planteada en el PND "por una que enfrente la grave desaparición del bosque". (Le puede interesar: "[El Plan Nacional de Desarrollo, un respaldo al sector minero-energético](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/)")

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
