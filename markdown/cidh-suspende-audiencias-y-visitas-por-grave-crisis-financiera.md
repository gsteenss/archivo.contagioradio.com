Title: CIDH suspende audiencias y visitas por grave crisis financiera
Date: 2016-05-24 14:27
Category: DDHH, El mundo
Tags: CIDH, Comisión Interamericana de Derechos Humanos, crisis financiera CIDH
Slug: cidh-suspende-audiencias-y-visitas-por-grave-crisis-financiera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/COMISION-CIDH-e1501085604514.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Nuevo Herald ] 

###### [24 Mayo 2016 ]

De acuerdo con el más reciente comunicado emitido por la Comisión Interamericana de Derechos Humanos CIDH, el organismo enfrenta una extrema crisis financiera que tendrá serias consecuencias para el cumplimiento de sus funciones. Los **contratos del 40% de su personal vencen el próximo 31 de julio**, y los menos de US\$5 millones con los que cuenta durante este año, no son suficientes para renovarlos. La actual crisis es sólo una muestra del desfinanciamiento que viene enfrentando la CIDH desde hace años y que no ha sido resuelta pese a las alertas.

La CIDH advierte con profundo pesar que se ha visto obligada a **suspender las visitas previstas para este año, así como los Períodos de Sesiones número 159 y 160** que estaban programados para julio y octubre, pues pese a los esfuerzos de su Secretaría Ejecutiva no han llegado las donaciones que estaban previamente conversadas; por lo que extiende el llamado tanto a los países miembros como a los observadores, a que realicen aportes financieros urgentes y de libre disposición.

La suspensión de las sesiones tiene un impacto directo en el avance de las denuncias que cursan por violaciones a los derechos humanos, que sin duda **se retrasarán y podrían llegar a un punto incompatible con el derecho de acceso a la justicia**, dejando a miles de víctimas en un estado de indefensión inminente.

"La CIDH espera que la próxima Asamblea General de la OEA, a realizarse en junio, adopte una decisión histórica y trascendental, que refleje el compromiso de los Estados con la defensa de los derechos humanos en la región. Esto significa **aumentar de forma radical el presupuesto del fondo regular de la OEA y asignar a la CIDH y al Sistema Interamericano de Derechos Humanos en general los recursos necesarios** para el cumplimiento del mandato asignado por los propios Estados", asevera en el comunicado.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
