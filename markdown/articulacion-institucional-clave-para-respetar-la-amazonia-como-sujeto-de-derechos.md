Title: Articulación institucional: Clave para respetar la Amazonía como sujeto de derechos
Date: 2019-10-22 19:33
Author: CtgAdm
Category: Ambiente, Judicial
Tags: Amazonía, Ambiente, Corte Suprema de Justicia, Tribunal Supremo de Bogotá
Slug: articulacion-institucional-clave-para-respetar-la-amazonia-como-sujeto-de-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Amazonía-1-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @InfoAmazoniaEsp  
] 

Desde el 15 de octubre hasta el 12 de noviembre, el Tribunal Superior de Bogotá realizará una serie de audiencias de seguimiento a la Sentencia 4360 de 2018, que declaró a la Amazonía como sujeto de derechos. La semana pasada se realizaron 3 audiencias y este martes se desarrolló una más, en la que según los participantes, se evidenció la falta de articulación institucional para cumplir con la protección del territorio y su recuperación. (Le puede interesar: ["El precedente que marca el fallo sobre la Amazona colombiano"](https://archivo.contagioradio.com/fallo_historico_corte_suprema_amazonas/))

### **En contexto: ¿Qué significa reconocer a la amazonía como sujeto de derechos?** 

En fallos recientes, algunas cortes han reconocido que la naturaleza y otras formas de vida distintas a la humana también son sujetos de derechos, es decir, gozan de una protección especial que debe ser asegurada por el Estado. Este ha sido el caso de algunos ríos como el Cauca, o según la sentencia 4360 de 2018 de la Corte Suprema de Justicia, la Amazonía. (Le puede interesar: ["Autoridades indígenas piden que la Amazona sea declarada libre de concesiones minero-energéticas"](https://archivo.contagioradio.com/autoridades-indigenas-defensa-amazonia/))

Para la **profesora de la Universidad de la Amazonía Mercedes Mejía Leudo**, la sentencia significa que las instituciones deben dar medidas para planificar dentro de sus procesos cómo se va a proteger la Amazonía y restaurar el daño que se ha cometido sobre el territorio. **La decisión fue producto de una acción de tutela interpuesta por un grupo de 25 jóvenes**, preocupados frente a la deforestación de la región y demás problemas asociados a esta situación. (Le puede interesar: ["Con el Amazona está en juego la supervivencia de la humanidad"](https://archivo.contagioradio.com/amazonia-supervivencia-humanidad/))

Según explicó Mejía, la sentencia ordenaba a las instituciones relacionadas con la Amazonía a comenzar un proceso **de programación institucional para frenar la deforestación**, generar un ordenamiento territorial orientado a restaurar los daños contra el territorio, creaba un pacto intergeneracional por la vida del Amazonas, y daba un término de 5 meses para la concreción del mismo. (Le puede interesar:["Arde el Amazona: 72.843 focos de incendio registrados en 2019"](https://archivo.contagioradio.com/arde-la-amazonia-72-843-focos-de-incendio-registrados-en-2019/))

### **¿Qué significa la audiencia para los interesados en proteger la Amazonía?** 

**Jesús Medina, uno de los 25 jóvenes accionantes en la tutela** que terminó con el reconocimiento de la Amazonía como sujeto de derechos declaró que las audiencias de seguimiento que realiza el Tribunal Superior de Bogotá son un paso importante en la protección de esta región porque "significa preguntar qué está pasando con los bosques", y es una cuestión de "índole nacional". En esa medida, es fundamental tener claridades sobre cuáles son los recursos que se destinan para acabar con la reforestación, y una oportunidad para que las entidades vinculadas con el desarrollo de la sentencia expliquen lo que han hecho para cumplir con la misma.

No obstante, Medina aseguró que la conclusión tras las audiencias que se han desarrollado es que **"las instituciones no han tenido una articulación armónica"** en aras de evitar la deforestación, y restaurar el bosque. En esa medida, dijo sentirse "insatisfecho" por el cumplimiento del Gobierno, y resaltó que durante las audiencias se esté pidiendo a las entidades compromiso en términos de políticas públicas para cumplir con la sentencia. (Le puede interesar: ["Plan de Gobierno para frenar deforestación en Amazona ha sido ineficaz"](https://archivo.contagioradio.com/plan-de-gobierno-para-frenar-deforestacion-en-amazonia-ha-sido-ineficaz/))

Según explicó el Ambientalista, el problema puede rastrearse en que no es una sola entidad la encargada de proteger este territorio, sino que se involucran ministerios, autoridades ambientales, gobiernos departamentales y municipales, ello significa que se requiere "una articulación fuerte y armónica", para cumplir con los puntos ordenados en la sentencia. Adicionalmente, Medina dijo que debía tomarse en cuenta la participación de la fuerza pública para detener la deforestación, pero que su actuación esté ligada al respeto de los derechos humanos para evitar que sean los campesinos los encontrados culpables de un problema con causas estructurales.

### **Audiencias continuarán hasta noviembre, pero no todas las personas pueden participar** 

Medina lamentó que la participación de la audiencia no esté abierta al público en general, porque los magistrados que la citaron, solicitaron con anterioridad a quienes estuvieran interesados en asistir que enviaran un memorial pidiendo acceder al evento. No obstante, dijo que se puede hacer seguimiento constante a lo que ocurra mediante las redes sociales de la organización DeJusticia, y a través de las transmisiones en vivo que realice el Tribunal. (Le puede interesar: ["La Amazona está perdiendo 293 hectáreas cada día"](https://archivo.contagioradio.com/amazonia-perdiendo-hectareas/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43539845" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_43539845_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
