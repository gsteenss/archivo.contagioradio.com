Title: Menos impuestos para las empresas, menos salario para los trabajadores
Date: 2018-12-21 17:47
Author: AdminContagio
Category: Economía, Nacional
Tags: CUT, salario minimo
Slug: menos-impuestos-para-las-empresas-menos-salario-para-los-trabajadores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Du6M_DhWsAArSEi-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @AliciaArango 

###### 20 Dic 2018 

Gobierno, empresarios y algunas centrales de trabajadores llegaron a un acuerdo el pasado jueves para fijar el incremento del salario mínimo para 2019 el cual se estableció en **\$828.116,** un aumento de 6% que a pesar de estar un **2,73 %** por encima de la inflación, no es suficiente para solventar las necesidades de las familias colombianas, tal como lo resaltó la CUT,  organización que no firmó el acuerdo.

[**Diogenes Orjuela**, presidente de la **Central Unitaria de Trabajadores (CUT)** aseguró que, una vez más el presidente Duque rompió sus promesas de campaña pues el aumento hecho al salario mínimo de ninguna manera es sustancial, tal como anunció cuando era candidato, a pesar de ello, la medida fue respaldada por la Confederación ]de Trabajadores de Colombia (CTC) y la Confederación General del Trabajo (CGT).[  
  
Orjuela manifestó que la CUT y la Confederación de Pensionados de Colombia (CPC) continuarán exigiendo el retiro de la nueva reforma tributaria y seguirán consolidándose con diferentes organizaciones sociales, además de unirse para pronunciarse sobre **el alza de los trabajadores del Estado y la reforma pensional** del próximo año.   
  
El presidente de la central obrera afirmó que Colombia necesita un alza de salarios para en primer lugar “mejorar las condiciones de vida de la gente” y en segundo lugar para garantizar un mayor consumo que permita el crecimiento de la economía, una postura que según el dirigente, el Gobierno parece no entender.  
  
El líder sindical también explicó que para comienzos de febrero, esperan convocar a una reunión nacional de organizaciones sindicales y sociales con el fin de responder al cumplimiento de los acuerdos de paz y hacer frente a las políticas de un Gobierno que “cada vez les da más motivos para realizar un paro nacional” del cual aún no se tiene fecha pero se espera sea, según el líder sindical: contundente y efectivo.   
]

###### Reciba toda la información de Contagio Radio en [[su correo]
