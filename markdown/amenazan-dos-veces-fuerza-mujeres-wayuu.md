Title: En una semana amenazan dos veces la organización Fuerza Mujeres Wayúu
Date: 2019-05-12 13:53
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Aguilas Negras, amenaza, Fuerza Mujeres Wayúu, Wayuu
Slug: amenazan-dos-veces-fuerza-mujeres-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-5.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Esta semana el movimiento Fuerza Mujeres Wayúu denunció que en ocho días han recibido dos amenazas por parte de grupos armados ilegales. Adicionalmente, los integrantes del movimiento denunciaron que tras poner en alerta a las autoridades por estos hechos, las mismas indicaron que no había riesgo y desestimaron las amenazas. (Le puede interesar: ["Duque, atienda primero la crisis humanitaria en La Guajira: Mujeres Wayúu"](https://archivo.contagioradio.com/guajira/))

Jackeline Romero, integrante del movimiento recordó que hace un año recibieron un panfleto que hacía mención de su Organización y otras más; en ese momento, denunciaron la situación pero no recibieron respuesta. Posteriormente,  finalizando el mes de abril hubo una amenaza más específica y directa contra Fuerza Mujeres Wayúu en la que había nombres de algunos de sus líderes; el martes 7 de mayo la amenaza se reiteró mediante un panfleto de las Águilas Negras en la que amenazan a 6 integrantes de la Organización.

> En tan solo 8 días, el Movimiento de Fuerza de Mujeres Wayuu ha recibido 2 amenazas directas hacia sus integrantes. Desde [@ONIC\_Colombia](https://twitter.com/ONIC_Colombia?ref_src=twsrc%5Etfw) le exigimos al gobierno de [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) que brinde de manera URGENTE [\#GarantiasParaLaVida](https://twitter.com/hashtag/GarantiasParaLaVida?src=hash&ref_src=twsrc%5Etfw). [@luiskankui](https://twitter.com/luiskankui?ref_src=twsrc%5Etfw) [@MiguelWayuu](https://twitter.com/MiguelWayuu?ref_src=twsrc%5Etfw) [@renacientes](https://twitter.com/renacientes?ref_src=twsrc%5Etfw) [@ONUHumanRights](https://twitter.com/ONUHumanRights?ref_src=twsrc%5Etfw). [pic.twitter.com/AECevMWUeN](https://t.co/AECevMWUeN)
>
> — Organización Nacional Indígena de Colombia - ONIC (@ONIC\_Colombia) [11 de mayo de 2019](https://twitter.com/ONIC_Colombia/status/1127233389277458432?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Romero afirmó que esa situación tiene al Movimiento en extrema alerta y preocupación, pero lo que más los preocupa es "la apatía de las autoridades y su estigmatización frente a este caso". Como la misma Activista lo señaló, tras hacer pública su denuncia, fueron abordados por miembros de la SIJIN, Gaula y Policía quienes les indicaron que el grupo Águilas Negras no existe, y por lo tanto, solo tienen que estar pendientes.

Ante esta situación, los integrantes de la Organización se preguntan cómo es posible que se desestimen las amenazas; teniendo en cuenta que sea que existan o no las mencionadas Águilas Negras, la realidad es que están amenazando y asesinando líderes sociales en el país. (Le puede interesar: ["Águilas Negras amenazan a William Solano, periodista que denuncia la corrupción en Buga"](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-william-solano-periodista-que-denuncia-corrupcion-en-buga/))

### **Gobierno ha hecho caso omiso de las formas de protección propuestas por las comunidades**

Fuerzas Mujeres Wayúu es una organización social que ha acompañado a víctimas de paramilitares y militares en la región Atlántica, allí ha trabajado con mujeres sobre el impacto de actividades extractivas y sus efectos en las comunidades. Por lo tanto, para Romero es sorpresivo que la Fiscalía avance rápidamente en investigaciones sobre atentados contra el Presidente (como el que afirmaron que ocurriría en Cauca si Duque asistía a la Minga por la Vida), pero no en amenazas contra líderes.

Aunque la Orgaización tiene asignado un esquema de protección colectivo asignado por la Unidad Nacional de Protección (UNP),  Romero recordó que hace más de 10 años trabajan en pro de la creación de sistemas de protección que no se limiten a la entrega de una camioneta, chalecos antibalas o celulares; y se piense la seguridad desde las comunidades, con enfoques étnicos y territoriales. (Le puede interesar: ["Gobierno guarda silencio ante amenazas de Águilas Negras a dirigentes de izquierda"](https://archivo.contagioradio.com/gobierno-guarda-silencio-ante-amenazas-aguilas-negras-dirigentes-izquierda/))

<iframe id="audio_35748699" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35748699_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
