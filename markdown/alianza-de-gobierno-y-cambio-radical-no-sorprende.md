Title: Alianza de Gobierno y Cambio Radical no sorprende
Date: 2020-01-27 19:03
Author: CtgAdm
Category: Nacional, Política
Tags: acuerdo de paz, Gobierno
Slug: alianza-de-gobierno-y-cambio-radical-no-sorprende
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Cambio-Radical-y-Gobierno.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @EfrainCepeda {#foto-efraincepeda .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:audio -->

<figure class="wp-block-audio">
<audio controls src="https://www.ivoox.com/alejo-vargas_md_47010435_wp_1.mp3">
</audio>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

Desde los primeros días del año se ha anunciado que Cambio Radical está en conversaciones con el Gobierno para dejar su 'independencia' y ser partido de Gobierno. La clave para que el acuerdo se dé estaría en el apoyo a algunos proyectos legislativos que tiene en mira Cambio Radical, y la llegada formal de algunas fichas del partido a Ministerios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para el profesor y analista político Alejo Vargas, este ingreso hace parte de una reconfiguración de fuerzas que está buscando mayor gobernabilidad mediante un aliado de vieja data. Esta situación no debería afectar la implementación del Acuerdo de Paz más de lo que ha sido hasta ahora. (Le puede interesar: ["Cambio Radical y Centro Democrático se unen "para hacer trizas los acuerdos" FARC"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-unen-para-hacer-trizas-los-acuerdos-farc/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿El retorno de la mermelada?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Algunos han señalado las recientes reuniones entre Gobierno y representantes de Cambio Radical como el posible regreso de la mermelada: Cambiar puestos en el Gobierno por votos en el Congreso. Vargas considera que el término no es correcto, pues en todas las sociedades modernas se forman coaliciones políticas, que tienen representación en el Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según explicó, Duque intentó hacer un gobierno de partido: nombrando a discreción en todos los cargos a personas cercanas a su partido. No obstante, el modelo hizo aguas y ahora se está intentando recomponer para sacar adelante proyectos que son de mutuo interés.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, Vargas señaló que tampoco es extraño el movimiento de Cambio Radical, porque desde antes, parte de su bancada votaba en concordancia con el interés del Gobierno. Situación que se asemeja a la del partido de La U o Liberal. (Le puede interesar:["Adhesión de liberales a Duque es un "suicidio asistido del partido"](https://archivo.contagioradio.com/adhesion-de-liberales-a-duque-es-un-suicidio-asistido-del-partido/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las leyes que Cambio Radical está interesado en impulsar**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Germán Vargas Lleras, líder natural de Cambio Radical, ha dejado ver algunos de sus intereses legislativos como la reforma pensional, a la justicia o la salud. En especial, se ha referido al interés que tienen en aprobar la inhabilidad por 4 años para ocupar cargos públicos a quienes son jefes de los entes de control (Fiscalía, Producaduría y Contraloría).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una iniciativa que para el profesor Alejo Vargas es buena idea, para evitar que dichos entes se conviertan en plataformas para cargos de representación, pero que en este caso podría ser leído como una sanción transitoria contra alguno de los actuales directores, que tendría en miras una candidatura presidencial.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La implementación legislativa del Acuerdo de Paz**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por último, Vargas se refirió a los proyectos que son claves para la implementación legislativa del Acuerdo de Paz: La reforma política electoral y las curules de paz. Sobre la primera, señaló que era una prioridad nacional, porque nuestro sistema electoral aún no es garantista para todas las fuerzas políticas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Tenemos una rama electoral que no funciona como tal, un Consejo Nacional Electoral que es dependiente de los partidos políticos" y en el que las fuerzas que están en el Congreso buscan proyectarse, mientras la "Registraduría tiene una dependencia muy fuerte del ejecutivo", sostuvo. (Le puede interesar: ["Propuesta del Gobierno sobre Curules de Paz busca desconocer lo acordado en La Habana"](https://archivo.contagioradio.com/curules-gobierno-desconocer-acuerdo-habana/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, y tomando en cuenta que este tipo de reformas tiene más probabilidades de ser aprobadas en años no electorales, 2020 y 2021 serán años propicios para dar esta discusión e intentar crear "una corte electoral que sea independiente de los partidos, en los que se elija magistrados y las reglas de juego se aplique para todas las fuerzas".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, resaltó la importancia de permitir la participación política de las víctimas mediante la aprobación de las 16 curules consignadas en el Acuerdo de Paz. Este proyecto es objeto de una disputa jurídica que debería dirimir la Corte Constitucional, pues se habría aprobado en 2019, pero la mesa directiva del Senado consideró que no había los votos suficientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta disputa es una muestra de la oposición que tiene el Proyecto en el legislativo, sin embargo, Vargas concluyó que es una oportunidad para que los sectores afines a la paz se organicen y orienten la discusión del Proyecto en el sentido de su aprobación.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
