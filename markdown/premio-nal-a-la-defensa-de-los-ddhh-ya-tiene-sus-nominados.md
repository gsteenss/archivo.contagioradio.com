Title: Se revelan los nominados al Premio Nacional de DDHH
Date: 2017-07-18 13:37
Category: DDHH, Nacional
Tags: Derechos Humanos, Premio de Derechos Humanos
Slug: premio-nal-a-la-defensa-de-los-ddhh-ya-tiene-sus-nominados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/premio-defensores-derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 jul. 2017]

Ya se conocen **las personas, procesos organizativos y organizaciones nominadas a la sexta versión del Premio Nacional a la Defensa de los Derechos Humanos – 2017**, un galardón entregado por la Organización Diakonia y que es considerado como un homenaje para reconocer el trabajo que realizan los defensores y defensoras de DD.HH. en Colombia.

Desde todos los lugares del país viajarán a Bogotá el próximo 9 de septiembre en el marco del Día Nacional de los DDHH en Colombia, para recibir los galardones y homenajes por la labor de la vida y los derechos en el país.

### **“Toda una vida” en defensa de los derechos humanos** 

**Son 3 las categorías en las que se entregan los galardones, la primera es “*****Toda una vida”*** en la que un defensor o defensora es premiado por su valor, perseverancia en la defensa de los derechos humanos. Para poder ser tenido en cuenta se debe contar con más de 30 años de trabajo.

**En la edición pasada, la galardonada fue Fabiola Lalinde,** quien desde 1984 comenzó una búsqueda incesante por encontrar a su hijo, un joven militante desaparecido por miembros del Ejército en Riosucio, Caldas. Le puede interesar: [Estos son los ganadores del premio Nacional a la Defensa de los Derechos Humanos](https://archivo.contagioradio.com/estos-son-los-ganadores-del-premio-nacional-a-la-defensa-de-los-derechos-humanos/)

Este año están nominadas 13 personas de lugares como Valle del Cauca, Antioquia, Arauca, Santander, Bolívar, Quindío, Cartagena de Indias y Bogotá. Algunos nombres postulados fueron Elsa Alvarado y Mario Calderón del Centro de Investigación y Educación Popular CINEP – Programa Por La Paz (In Memoria) o María Teresa Arizabaleta de la Ruta Pacifica de Mujeres.

### **Defensor o defensora del año** 

En esta categoría que pretende otorgar un reconocimiento público a quienes durante el último año se han dedicado a dar testimonio de lo que significa defender los derechos humanos en Colombia el año anterior ganó Francia Elena Márquez Mina.

**Para este año fueron nominados catorce hombres y diez mujeres. Veinticuatro personas que durante el último año se han destacado** por abanderar luchas en pro de los derechos de sus comunidades.

Samuel Arregocés del Consejo Comunitario Negros Ancestrales de Tabaco en el Municipio de Hato Nuevo, departamento de la Guajira, Enrique Chimonja Coy de la Comisión Intereclesial De Justicia Y Paz – CIJP en el Valle del Cauca, Adriana María Arboleda Betancur de la Corporación Jurídica Libertad en Antioquia y Milena Quiroz Jiménez de la Comisión de Interlocución del Sur de Bolívar, Centro y Sur del Cesar en Bolívar, son algunos de los nombres en el listado.

### **Experiencia Colectiva** 

Son diversas las iniciativas y las experiencias colectivas que han sido acumuladas en Colombia y de la cuales muchas organizaciones de nivel comunitario, ONG, colectivos o acompañantes dan cuenta de ello. Le puede interesar: [Cinco mujeres reciben el Premio de Derechos Humanos 2016](https://archivo.contagioradio.com/cinco-lideresas-galardonadas/)

**Este año en las dos subcategorías que existen en esta nominación se encuentran los nombres de 21 organizaciones** que son procesos sociales comunitarios y 15 que están en el nivel ONG, colectivo u ONG acompañante.

### **Jurados Nacionales e Internacionales para escoger a los y las ganadoras** 

Como todos los años para la escogencia de los y las ganadoras **se contará con la presencia de personalidades reconocidas en los derechos humanos, investigadores,** miembros de la academia, la cooperación internacional, las Iglesias, generadores de opinión pública, columnistas de prensa, artistas y docentes, de Estados Unidos, Europa, América Latina y Colombia. Le puede interesar: [Entregan en Bogotá el premio Franco Alemán de Derechos Humanos](https://archivo.contagioradio.com/se-entrego-en-bogota-el-premio-franco-aleman-de-derechos-humanos-en-colombia/)

Conozca el listado completo aquí: http://bit.ly/2tCMPrK

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
