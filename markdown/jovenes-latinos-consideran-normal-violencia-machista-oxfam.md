Title: Jóvenes latinos normalizan la violencia machista: OXFAM
Date: 2018-07-25 16:06
Category: Movilización, Mujer
Tags: Informe, Oxfam, Rompiendo Moldes, Violencia contra mujeres
Slug: jovenes-latinos-consideran-normal-violencia-machista-oxfam
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/violencia-de-genero.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Jul 2018] 

Datos revelados en el informe "Rompiendo Moldes", de la organización OXFAM, registran que 7 de cada 10 jóvenes en El Salvador, Guatemala, Honduras, Nicaragua, República Dominicana, Cuba, Bolivia y Colombia "piensan que la responsabilidad de ser manoseadas o acorraladas es de las mujeres por la ropa que usan", y 6 de cada 10, creen que celar es una muestra de amor.

Según la ONG internacional, un 65% de los hombres encuestados para la realización del informe considera "que cuando una mujer dice 'no' a una relación sexual, en realidad quiere decir 'si'". Según el informe, esto se debe a la idea del "amor romántico" nocivo, que gracias a los imaginarios y normas sociales presentes en el continente, reproducen la violencia machista.

El "amor romántico" se refiere a conductas negativas que asumen las relaciones amorosas como vínculos de autoridad y no de compañerismo afectuoso; lo que justifica situaciones como el control de la forma de vestir de la pareja, el acceso a los mensajes de sus celulares o exigir las claves de sus redes sociales.

### **"77 % de las y los jóvenes coincide en que todas las mujeres deben de ser madres": OXFAM** 

Algunas de las cifras que muestra el informe revelan las diferentes formas en que se evidencia las conductas machistas presentes en la juventud latinoamericana; una de ellas son los roles que se asignan según el género, pues el 56% de los jóvenes entre 15 y 19 años, cree que lo mejor es que el hombre sea el sustento económico de la familia y que las mujeres cuiden a los hijos.

También la violencia se expresa en las relaciones íntimas, pues el 61% de los hombres se enoja si su pareja no desea tener relaciones sexuales. Y hay una violencia contra la diversidad de sexo y género, pues el 70% de los encuestados "supone que sus amigas y amigos piensan que las lesbianas no deberían mostrar su orientación sexual en la calle".

Sin embargo, OXFAM cree que hay evidencias de que la sociedad está cambiando y la base de ese cambio radica en la población juvenil, puesto que "las voces más críticas hacia las desigualdades de género se identifican principalmente entre las mujeres de entre 20 y 25 años".

### **Colombia no es ajena al fenómeno del machismo** 

Adicionalmente, el documento se refiere a las conductas de acoso callejero, que tienen una tasa de aceptación alta en Colombia, pues "el 95% de los hombres colombianos admiten que para sus amigos el acoso callejero -en forma de piropo- es normal, en cambio, ocho de cada diez hombres guatemaltecos lo consideran violencia".

El informe "Rompiendo Moldes: Transformar imaginarios y normas sociales para eliminar la violencia contra las mujeres", fue presentado el 25 de julio en el Hotel Holiday Inn de Bogotá, por la experta en derechos de las mujeres Damaris Ruiz de Nicaragua y las periodistas Jineth Bedoya y Viviana Bohórquez de Colombia.

El informe concluye con un llamado al Estado, para que además de los avances en términos normativos que favorezcan la equidad de género, se adecuen los presupuestos de tal forma que las leyes cuenten con un sustento financiero. De igual forma, OXFAM indicó que "el camino es el de la construcción social y la deconstrucción de los imaginarios y normas sociales que solo reproducen más desigualdad". (Le puede interesar: ["Estado falla en la protección de derechos de las mujeres y las niñas"](https://archivo.contagioradio.com/estado-ha-fallado-en-la-proteccion-de-los-derechos-de-las-mujeres-y-las-ninas/))

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
