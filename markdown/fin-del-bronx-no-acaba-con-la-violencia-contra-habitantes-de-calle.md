Title: Fin del Bronx no acaba con la violencia contra habitantes de Calle
Date: 2018-05-29 16:49
Category: Nacional
Tags: Bogotá, Bronx, Habitantes de calle
Slug: fin-del-bronx-no-acaba-con-la-violencia-contra-habitantes-de-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/WhatsApp-Image-2018-05-29-at-2.21.26-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Fray Gabriel Gutiérrez 

###### 29 May 2018 

A dos años de la intervención en lo que se conocía como el sector de El Bronx en el centro de Bogotá, defensores de los habitantes en situación de calle advierten sobre la grave situación que aún padecen y los alcances reales de la intervención realizada por la administración de Enrique Peñalosa.

Fray Gabriel Gutiérrez, asegura que de los objetivos con los que se justificó el operativo en la zona como la recuperación de los niños que se decía que estaban allí, la detención de las bandas criminales que operaban en el lugar y la recuperación de los habitantes de calle,  aun no se tienen resultados tangibles, contrario a la recuperación de los terrenos por la Administración Distrital, único objetivo que el sacerdote valora como cumplido.

Para el defensor, a pesar del trabajo que hacen los centros de integración social en el apoyo, acompañamiento y recuperación de esta población, su capacidad esta desbordada "yo no creo que ellos hayan atendido a los 16 mil habitantes de calle que nosotros tenemos en esta ciudad" añadiendo además que "cada día la situación más fuerte y más violenta contra esta población".

Frente a lo anterior, Fray Gabriel asegura se debe a que se pensó en la intervención, más no en lo que vendría después de la misma "no hubo una construcción de proyectos frente a este fenómeno, de que haríamos con estas poblaciones, a no ser el único factor que ha intervenido en estas poblaciones es la violencia" que en su criterio es sistemática y ha cobrado la vida por lo menos a 20 habitantes de calle en lo que va de 2018.

El que las poblaciones no estén visitando el Bronx, asegura el sacerdote, no se debe a la atención que se les presta sino por la violencia que se aplica contra ellos por parte la fuerza pública, la comunidad, los comerciantes,  lo que ha conllevado a que los sobrevivientes del Bronx se desplacen a otros lugares como el caño de la sexta, el Parque 3er milenio, la zona de Puente Aranda o el Santa Fé.

"El otro asunto que nos preocupa es que si hay una percepción de inseguridad en Bogotá le están achacando esa inseguridad a los habitantes de calle, escondiendo los verdaderos orígenes y causas de la inseguridad", añadiendo que muchos por cuestión de su adicción al pegante y otras sustancias no son capaces de atacar a nadie.

Desde el trabajo social, Fray Gabriel anuncia que seguirán trabajando por que se declare una crisis humanitaria y los sistemas de alerta temprana por parte de la defensoría del pueblo "donde a los habitantes de calle se les escuche para que podamos revisar esas políticas públicas y busquemos una salida, que se humanice la calle y que se le de un tratamiento más humanitario a esta población".

<iframe id="audio_26244658" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26244658_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
