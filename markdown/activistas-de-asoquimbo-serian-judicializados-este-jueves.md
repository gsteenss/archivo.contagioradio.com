Title: Activistas de Asoquimbo serían judicializados este jueves
Date: 2016-06-22 16:54
Category: Ambiente, Otra Mirada
Tags: ASOQUIMBO, EMGESA, Miller Dussán
Slug: activistas-de-asoquimbo-serian-judicializados-este-jueves
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Miller-Dussan.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Somos el Río 

###### 22 Jun 2016 

Asoquimbo denuncia que el investigador de esa organización, Miller Dussán y la presidenta, Elsa Muñoz Ardila, quienes han liderado la lucha en contra del proyecto hidroeléctrico El Quimbo de Emgesa, serían judicializados  por la Fiscalía 22.

De acuerdo con la denuncia, son acusados de perturbación del orden público, en el marco de las protestas en la zona de implementación de la represa, donde **han exigido a la empresa y al gobierno, medidas de reparación para las 1500 familias víctimas de este megaproyecto,** que han quedado en la absoluta pobreza, según denunció hace algunos meses Miller Dussán en los micrófonos de Contagio Radio.

El investigador de Asoquimbo, ha expresado que esta medida, significa la criminalización de la protesta social legítima. “**Las movilizaciones de las familias afectadas por la construcción de la represa son legítimas y están amparadas constitucionalmente.** Imputar cargos por las protestas es una medida desproporcionada".

La decisión se toma tras una demanda interpuesta por la multinacional Emgesa, por la que se espera que este jueves los integrantes de Asoquimbo sean juzgados sobre las 11:30  de  la mañana en una audiencia que se llevará por el supuesto delito de **“obstrucción de vías públicas que afecten el orden público”**, contemplada en el Código Penal.

Frente a esta situación se ha pedido la intervención de la Corte Interamericana de Derechos Humanos, de manera que haya garantía en el proceso que se adelanta en contra de los activistas y afectados por la construcción de la represa.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
