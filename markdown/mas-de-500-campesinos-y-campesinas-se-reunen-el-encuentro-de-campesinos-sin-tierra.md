Title: Se reactiva el movimiento de  Campesinos Sin Tierra en Santander
Date: 2015-03-16 18:00
Author: CtgAdm
Category: Movilización, Nacional
Tags: Bogotá, campesinas, campesinos, Independencia, José Antonio Galán, María Antonia Santos, Plan Nacional de Desarrollo 2014 - 2018, PND, Rebelión comunera
Slug: mas-de-500-campesinos-y-campesinas-se-reunen-el-encuentro-de-campesinos-sin-tierra
Status: published

###### Foto: Alberto Castilla 

<iframe src="http://www.ivoox.com/player_ek_4222241_2_1.html?data=lZeflJeYdY6ZmKiakpuJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkYa3lIquk9iPqMaflpWdjcjFsdHZ1M7b0diPvYzXwtLdx9jNssLnjNjSjdfJaaSnhqeu0MrSb8bgjKrbxdrJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Polidoro Guaitero, líder campesino] 

Este 16 de marzo, más de 500 campesinos y campesinas desterrados, se reúnen para realizar el  Encuentro de Campesinos Sin Tierra de sur de Santander, con el objetivo de **reactivar este movimiento y compartir expresiones de lucha por la vida y la tierra,** además de celebrar la conmemoración de la Rebelión Comunera que cumple 234 años.

[![Sin Tierra 4 - Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Sin-Tierra-4-Contagio-Radio-202x300.png){.wp-image-5988 .alignleft width="234" height="347"}](https://archivo.contagioradio.com/actualidad/mas-de-500-campesinos-y-campesinas-se-reunen-el-encuentro-de-campesinos-sin-tierra/attachment/sin-tierra-4-contagio-radio/)

La tierra, el agua, el sol y el aire, son los ejes sobre los cuales se desarrollará el evento. Polidoro Guaitero, uno de los líderes convocantes para el encuentro, señala que se espera generar **“un proceso de empoderamiento, y ‘descontentamiento’**- a lo que añade irónicamente- estamos contentos los sin tierra, los desempleados; estamos contentos con todo el plan de guerra, con los que nos expropiaron las tierras y ahora estamos contentos con lo que nos roban la minería en Colombia, la educación, y la religión”.

Los desterrados y desterradas, realizarán una marcha por la tierra, desde el monumento Los Capuchinos hasta el Parque la Independencia, y allí, harán una **ofrenda campesina a José Antonio Galán y Manuela Beltrán, que rompieron el edicto; y María Antonia Santos,** quien lideró tuvo la primera guerrilla en Colombia en defensa de los españoles.

“**El edicto de la marcha es el PND,  donde no está claro la parte de las tierras y la paz en Colombia”** ya que, para el líder campesino, es claro que el Plan Nacional de Desarrollo ‘2014-2018’, es más un plan de presupuesto para la guerra y las multinacionales y no para los colombianos.

![Sin tierra 2 - Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/Sin-tierra-2-Contagio-Radio-213x300.png){.wp-image-5987 .alignright width="173" height="243"}

**El próximo 6 de abril en Bogotá, se llevará a cabo una asamblea nacional para celebrar el primer año del paro campesino,** “Será una marcha por el desarrollo rural de Colombia”, afirma  Polidoro Guaitero.
