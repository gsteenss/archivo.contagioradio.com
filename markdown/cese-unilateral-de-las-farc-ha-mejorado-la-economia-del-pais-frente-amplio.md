Title: Frente Amplio reclama pasos para un cese bilateral definitivo
Date: 2015-02-10 17:18
Author: CtgAdm
Category: Movilización, Paz, Política
Tags: cese al fuego unilateral FARC-EP, Conversaciones de paz en Colombia, paz
Slug: cese-unilateral-de-las-farc-ha-mejorado-la-economia-del-pais-frente-amplio
Status: published

###### Foto: minuto30 

El comunicado del Frente Amplio por la Paz, luego de la reunión sostenida por algunos de sus representantes con integrantes de la delegación de paz de las FARC en la Habana, este lunes. Según el Frente Amplio, el **cese unilateral ha tenido resultados en la economía de las regiones y en la tranquilidad de los habitantes** en las zonas de conflicto.

Piedad Córdoba, vocera de la organización que hace veeduría al cese unilateral de las FARC, afirma que de esa reunión **tenía conocimiento el señor presidente de la República, el alto comisionado de paz Sergio Jaramillo** y además fue informada por la delegación de paz de esa guerrilla.

El objetivo del encuentro fue **recibir información sobre el proceso de conversaciones e intercambiar puntos de vista sobre cese unilateral desarrollado por la guerrilla desde hace 53 días**.

En el comunicado de 4 puntos, el Frente Amplio por la Paz resalta que hay un cumplimiento estricto del compromiso de las FARC para no desarrollar operaciones ofensivas y FARC “**Se ha constatado el efecto altamente benéfico y humanitario que el cese unilateral ha tenido para el país, evitando nuevas víctimas y daños irreparables**”.

De la misma manera la guerrilla informa que se mantienen en su decisión del cese unilateral a pesar de las operaciones de las FFMM que “incluyen ataques a unidades guerrilleras, operativos contra sus comandantes, operaciones psicológicas que invitan a la desmovilización de las estructuras que se mantienen en cese al fuego y copamiento de posiciones estratégicas y bombardeos”.

Por esas razones el Frente Amplio reclama que el gobierno asuma también comportamientos consecuentes y recíprocos que permitan mantener el cese unilateral y que de pasos para un cese bilateral, igualmente,  anuncia que se reunirá con el presidente Juan Manuel Santos una vez sus delegados se encuentren en Colombia.

Anexamos video de la rueda de prensa ofrecida por el Frente Amplio desde La Habana

\[embed\]https://www.youtube.com/watch?v=zZ9gSZCKNuc\[/embed\]
