Title: Consejo de Seguridad de la ONU llama a un cese al fuego global
Date: 2020-07-07 21:07
Author: CtgAdm
Category: Actualidad, DDHH
Tags: ACNUDH, Agresiones por parte del ejército, asesinato de líderes sociales, Consejo de Seguridad de la ONU, Covid-19, Ejército Nacional
Slug: consejo-de-seguridad-de-la-onu-llama-a-un-cese-al-fuego-global
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/EcXlKIEWsAAG6JT.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Consejo de Seguridad de la ONU/ FLick

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El Consejo de Seguridad de las Naciones Unidas consciente del grave efecto que ha tenido la pandemia de COVID-19 en todo el mundo, especialmente en los países asolados por conflictos armados, exigió el cese general e inmediato de las hostilidades a nivel global, y una pausa humanitaria a las actores armados durante al menos 90 días consecutivos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la evidencia de cómo las condiciones de violencia e inestabilidad que se dan en las situaciones de conflicto pueden complejizar la pandemia y a su vez, la pandemia puede exacerbar las situaciones de conflicto, la ONU reiteró un llamado que ya había hecho con anterioridad y que busca facilitar el acceso humanitario en diversas poblaciones en riesgo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En Colombia, la exigencia se da en medio de un incremento de la violencia en épocas de aislamiento por parte de grupos armados ilegales y del paramilitarismo en las regiones rurales del país. Según la Misión de Verificación de la ONU entre enero y junio se verificó **un total de 32 homicidios de líderes sociales, además de otros 47 casos en proceso de verificación, Mientras organizaciones como la Misión de Observación electoral señala que durante cuarentena se asesina a un líder cada tres, otras organizaciones como [Indepaz](https://twitter.com/Indepaz) hablan de 156 líderes o defensores de DD.HH. asesinados en 2020, 75 al menos en medio del confinamiento.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, entre el 27 de marzo y el pasado 26 de junio han sido asesinados 15 excombatientes en varias regiones del país y 33 en lo que va corrido del año. La ONU también destacó que el desplazamiento masivo y el confinamiento forzado también fueron evidentes y que tan solo en el mes de mayo, más de 45.000 personas fueron sometidas a confinamiento forzado, el 70% de ellas en la región del Catatumbo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el senador Iván Cepeda, durante la pandemia se ha evidenciado **un recrudecimiento en el uso de la fuerza, no solo por parte de los grupos armados ilegales, sino también de la Policía y del Ejército** así que considera coherentes los llamados de la comunidad internacional, por lo que señala que el Gobierno debe acoger estas medidas no como una sugerencia sino como una orden.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Defensoría del Pueblo también dio prioridad a las alertas tempranas en época de cuarentena, con un total de 12 alertas, 10 de ellas de carácter inminente en **Valle del Cauca, Norte de Santander, Chocó, Caldas, Antioquia, y Bolívar.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El llamado de la ONU también compete a los Ejércitos

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El anunció se da no solo en medio de esta apreciación hecha por el Consejo de Seguridad de las Naciones Unidas sino que remite a las recomendaciones de el Alto Comisionado de las Naciones Unidas para los Derechos Humanos en el país (ACNUDH) de restringir el uso del Ejército en situaciones relacionadas con la seguridad ciudadana. [(Lea también: Operativo de erradicación en Putumayo deja dos campesinos muertos y tres heridos)](https://archivo.contagioradio.com/en-operativos-de-erradicacion-en-putumayo-deja-dos-campesinos-muertos-y-tres-heridos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esto a propósito de las recientes denuncias de violación hechas contra niñas menores de edad por parte de integrantes del Ejército en lugares como Risaralda y Guaviare sin mencionar el aumento de las labores de erradicación forzada por parte de la Fuerza Pública en diversas regiones del país y que han llevado a la confrontación entre integrantes del Ejército y campesinos que exigen se cumpla con lo pactado en el acuerdo con relación a la sustitución voluntaria. [(Lea también: Agresión sexual del Ejército contra menor embera es un ataque a los 115 pueblos indígenas del país: ONIC)](https://archivo.contagioradio.com/agresion-sexual-del-ejercito-contra-menor-embera-es-un-ataque-a-los-115-pueblos-indigenas-del-pais-onic/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dichos sucesos han dado pie a una serie de denuncias con relación al exceso de la fuerza por parte del Ejército, según denuncias de la Asociación Campesina de Catatumbo (ASCAMCAT), militares abrieron fuego contra un asentamiento en la vereda Vigilancia, zona rural de Cúcuta, provocando la muerte del campesino **Emerito Digno Buendía** durante el mes de mayo, además del asesinato a manos del Ejército del joven **Alejandro Carvajal** en medio de protestas campesinas por las operaciones de erradicación.[Lea también: Ascamcat denuncia posible ejecución extrajudicial de Salvador Durán en Teorama)](https://archivo.contagioradio.com/ascamcat-denuncia-posible-ejecucion-extrajudicial-de-campesino-en-teorama/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El llamado se da tan solo un día después de conocerse una petición firmada por 96 congresistas de Estados Unidos que han pedido a la administración Trump, se ejerza presión sobre Iván Duque para que refuerce la protección a los liderazgos sociales y en general a las poblaciones rurales del país. [(Lea también: Congreso de EE.UU. pide que Trump presione a Duque para proteger a líderes sociales)](https://archivo.contagioradio.com/congreso-de-ee-uu-pide-que-trump-presione-a-duque-para-proteger-a-lideres-sociales/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ELN propone un cese al fuego bilateral

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como una respuesta al llamado del Consejo de Seguridad de las Naciones Unidas, la guerrilla del Ejército de Liberación Nacional, ELN, hizo la propuesta de pactar un cese bilateral de fuego al gobierno nacional. La propuesta fue hecha pública a través de un comunicado difundido este 7 de Julio.

<!-- /wp:paragraph -->

<!-- wp:image {"id":86486,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/EcXlKIEWsAAG6JT-881x1024.jpg){.wp-image-86486}  

<figcaption>
Comunicado del ELN

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->
