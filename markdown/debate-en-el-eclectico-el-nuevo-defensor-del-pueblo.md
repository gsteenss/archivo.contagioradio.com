Title: Debate en el Ecléctico: El nuevo defensor del Pueblo
Date: 2016-08-23 17:41
Category: El Eclectico
Tags: Carlos Negret, debate, Defensor del pueblo
Slug: debate-en-el-eclectico-el-nuevo-defensor-del-pueblo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Carlos-Negret.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Para hablar sobre el reciente nombramiento de Carlos Negret como nuevo defensor del pueblo, invitamos a Carlos Guevara, coordinador del programa Somos Defensores y quien ha trabajado como defensor de derechos humanos desde hace 15 años. Además, nos acompañaron en cabina Carolina Laverde, integrante de la Unidad de Trabajo Legislativa del representante a la Cámara por el Polo Democrático Alirio Uribe; y Pedro Sánchez Medina, codirector nacional de juventudes del Partido de la U.

El debate comenzó abordando el hecho de que la Cámara de Representantes haya escogido a Carlos Negret por encima de Andrés Santamaría y Caterina Heyck, quienes tienen más experiencia que Negret en el trabajo con derechos humanos. Hubo una discusión respecto a la forma en la que se hacen estos nombramientos en las entidades de control del Estado.

Los panelistas también discutieron sobre la sensación que les generaba el que la elección haya tenido lugar tan solo seis días después del nombramiento de la terna, y de que los candidatos hayan tenido tan poco tiempo para presentarle a los representantes sus planes de trabajo. También se hizo mención a la controvertida votación en la que aparecieron más votos que votantes.

Para finalizar, los panelistas abordaron el papel que tendrá la Defensoría del Pueblo en un escenario de posacuerdos y de las expectativas que las organizaciones defensoras de derechos humanos tienen de la gestión de Carlos Negret.

<iframe src="http://co.ivoox.com/es/player_ej_12641898_2_1.html?data=kpejlpacfZmhhpywj5WVaZS1kZiSlaaXeI6ZmKialJKJe6ShkZKSmaiRdI6ZmKiasNrJutCfpcrTx9PXs9OfxcrZjbXZqcPg0IqfpZCoqcPV1cqYx9OPic2fpsjZh6iXaaKtxNnW1sjTcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
