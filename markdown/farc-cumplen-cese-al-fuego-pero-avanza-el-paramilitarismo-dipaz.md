Title: FARC cumplen Cese al Fuego pero avanza el paramilitarismo: DiPaz
Date: 2015-08-28 12:25
Category: Entrevistas, Paz
Tags: Cese al fuego, cese unilateral, DIPAZ, ELN, FARC, Gobierno Nacional, Justicia y pazç, proceso de paz
Slug: farc-cumplen-cese-al-fuego-pero-avanza-el-paramilitarismo-dipaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ [ Foto:] [colombiaporunapazestableyduradera.blogspot] 

<iframe src="http://www.ivoox.com/player_ek_7661270_2_1.html?data=mJujk5ebdI6ZmKiak5aJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisLmxJDQ19LUsMbijMjS1cqPpc2fx9rSydSPtMbm0JDO2MbSvsKfxtGY0sbWpc7dzc7hw9fNt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [ Abilio Peña, Secretario de Dipaz] 

###### [28 Ago 2015] 

El balance realizado por DiPaz sobre la veeduría que realizaron al Cese unilateral de las FARC-EP, coincidió con el informe del Frente Amplio por la Paz y el CERAC en que el último mes fue el menos violento en 40 años de conflicto, pero que la presencia y las acciones **paramilitares siguen en aumento.**

Abilio Peña, secretario de Dipaz y defensor de derechos, indicó que en este mes de cese al fuego hubo **cinco personas asesinadas por estructuras paramilitares**, las cuales están presentes en distintos territorios del país, como Buenaventura y el departamento del Meta.

Este hecho, que ha sido reiterado en los distintos informes sobre seguimiento al cese unilateral, es problemático en vista de que “*de no resolverse esta situación los diálogos en la Habana y el ELN se pondrían en riesgo*” por las garantías que tendrían las guerrillas en un eventual post conflicto.

El informe destaca también cuatro acciones armadas por parte de las FARC, pero también constantes bombardeos y ametrallamientos por parte del ejército en distintas regiones, hechos denunciados por la delegación de paz de las FARC y constatados por las comunidades.

Frente a la muerte de Genaro García, el secretario de Dipaz dijo “el reconocimiento es la posibilidad de un correctivo, de que no se va a repetir” pero agrega que e**l Estado y los medios de comunicación también deben reconocer su participación en la guerra**.

Esta veeduría que continuará por la prolongación al cese unilateral, fue realizada en coordinación con las regiones, en base a testimonios de miembros de las comunidades y también usando fuentes oficiales, explica Peña.

[Carta DiPaz veeduría Circular Español](https://es.scribd.com/doc/276717476/Carta-DiPaz-veeduri-a-Circular-Espan-ol "View Carta DiPaz veeduría Circular Español on Scribd")

<iframe id="doc_13160" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/276717476/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
