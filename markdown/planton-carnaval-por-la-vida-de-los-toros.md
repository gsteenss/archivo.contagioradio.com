Title: Plantón carnaval por la vida de los toros
Date: 2015-07-09 14:46
Category: Animales, Voces de la Tierra
Tags: Bancada Animalista, Concejo de Bogotá, Consulta Antitaurina, consulta popular, Natalia Parra, Plataforma Alto
Slug: planton-carnaval-por-la-vida-de-los-toros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11133691_10153083290045020_2974320563924837454_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4750105_2_1.html?data=lZyikpaUeY6ZmKiakpeJd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidToxpDjy8rWssbnjNjSjcnJpsLoxpDS0JDJsIy30NPgx8%2FTb8XZjKfcydTYaaSnhqaejdHFb8Tjz9iah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Natalia Parra, Plataforma ALTO] 

Este viernes **10 de julio a la 1:00 de la tarde se realizará un [plantón carnaval](https://www.facebook.com/events/1465767240400834/) a la entrada del Concejo de Bogotá**, con el objetivo de solicitar a los concejales la realización de la consulta popular y demostrar si aun existe o no arraigo frente a las corridas de toros.

Ese día está convocada la instalación de las sesiones extraordinarias, para la votación del Concepto por parte del Concejo para la realización de una Consulta Popular. Es por eso,  que desde las organizaciones ciudadanas que hacen parte de Bogotá sin toreo, se piensa **pedirle al Concejo de Bogotá un pronunciamiento favorable a la Consulta Antitaurina**.

“Esta vez no vamos a llegar con el tema de la sangre, vamos a celebrar la vida de los toros, para decirle al consejo que los toros tiene dolientes”, dice Natalia Parra, directora de la Plataforma ALTO y secretaria de la bancada animalista.

Durante el evento, se quiere romper una serie de mitos que los taurinos han impuesto sobre la consulta popular, afirmando que esta es inconstitucional y que podría quitar los curules a los concejales.

De ser aprobado este instrumento de participación ciudadana en las elecciones del 25 de octubre los bogotanos y bogotanas, podrán dar su opinión frente a la pregunta **¿Está usted de acuerdo, si o no, con que se realicen corridas de toros y novilladas en Bogotá D.C.?**
