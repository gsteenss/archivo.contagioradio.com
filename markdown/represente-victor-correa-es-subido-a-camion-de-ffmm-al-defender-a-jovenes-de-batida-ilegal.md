Title: Representante Víctor Correa se subió a camión de FFMM para defender a jóvenes de batida ilegal
Date: 2015-08-10 11:29
Category: DDHH, Nacional
Tags: batidas del Ejército, Ejército Nacional de Colombia, Polo Democrático, reclutamiento de jóvenes, Víctor Correa
Slug: represente-victor-correa-es-subido-a-camion-de-ffmm-al-defender-a-jovenes-de-batida-ilegal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/retención-Victo-Correa.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [10 Ago 2015] 

Esta mañana, a través de su cuenta de Twitter, el representante a la Cámara del Polo Democrático Alternativo, Víctor Correa, denunció que fue detenido y llevado en un camión del Ejército Nacional, tras defender a unos jóvenes que estaban siendo reclutados en una batida ilegal en Medellín.

Correa, le decía  los militares que lo que estaban haciendo con los jóvenes se trataba de una acción ilegal, teniendo en cuenta que  las batidas en calles o lugares públicos quedaron prohibidas por orden de la **Corte Constitucional.**

El representante dijo a través de twitter que fue llevado en el camión del Ejército a la IV Brigada en Medellín.

Por su parte, la Fundación Comité de Solidaridad con Presos Políticos, denunció que durante, este fin de semana se realizaron batidas ilegales en **Bogotá y Medellín,** "muchos de estos chicos ya se encuentran en **Granada**”, dice la denuncia.

\[embed\]https://youtu.be/SrIJGGjHF8Y\[/embed\]  
[![twit 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/twit-1.png){.wp-image-12002 .size-full .aligncenter width="579" height="276"}![twit 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/twit-2.png){.wp-image-12003 .size-full .aligncenter width="584" height="193"}](https://archivo.contagioradio.com/represente-victor-correa-es-subido-a-camion-de-ffmm-al-defender-a-jovenes-de-batida-ilegal/twit-2/)[![twit 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/twit-4.png){.wp-image-12006 .size-full .aligncenter width="585" height="184"}](https://archivo.contagioradio.com/represente-victor-correa-es-subido-a-camion-de-ffmm-al-defender-a-jovenes-de-batida-ilegal/twit-4/)[  
](https://archivo.contagioradio.com/represente-victor-correa-es-subido-a-camion-de-ffmm-al-defender-a-jovenes-de-batida-ilegal/twit-3-2/)[  
](https://archivo.contagioradio.com/represente-victor-correa-es-subido-a-camion-de-ffmm-al-defender-a-jovenes-de-batida-ilegal/twit-3/)
