Title: 600 Indígenas de la zona rural de Buenaventura bloquean via Cali-Buenaventura
Date: 2015-06-03 15:23
Category: Comunidad, Nacional
Tags: ACIVRP, Administración Distrital, Asociación de Cabildos Indígenas del Valle del Cauca y Región Pacífico, buenaventura, Cali, Chirimía Estéreo, colombia, comunidades indígenas, ESMAD, Fernando Chirimía, Policía Nacional
Slug: 600-indigenas-de-la-zona-rural-de-buenaventura-bloquean-via-cali-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/IMG-20150603-WA0006.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4590680_2_1.html?data=lZqmkpucdI6ZmKiak5WJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy6xtfbw9PIs4y3yc7fy9KJh5SZoqnOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Entrevista: Fernando Chirimía] 

##### [3 Jun 2015] 

Desde las 5:30 A.M. de este miércoles 3 de Junio, cerca de 600 indígenas de la **Asociación de Cabildos Indígenas del Valle del Cauca y Región Pacífico** **ACIVRP**, iniciaron un bloqueo en la vía que conduce de **Buenaventura a Cali**, por los incumplimientos de la **Administración Distrital** de **Buenaventura** a algunos acuerdos pactados previamente.

**Fernando Chirimía** encargado de la comunicación de las comunidades, de la emisora **Chirimía Estéreo,** asegura que desde el **18 de Mayo** **ACIVRP** tiene pendiente una mesa de concertación con la **Administración Distrital**, para acordar temas que benefician a las comunidades de la Jurisdicción de **Buenaventura**, pero, debido al incumplimiento del alcalde en esta fecha, la asociación toma la decisión de salir a protestar pacíficamente para que sean tenidos en cuenta.

La mesa de concertación se lleva a cabo desde el año **2009**, desde ese año, según **Chirimía**, el gobierno ha hecho propuestas positivas sobre los proyectos que se refieren a cultura, territorio, educación y deporte, pero hasta el día de hoy no se ha evidenciado ningún avance. A finales del mes de Junio, se cerrará la ley de garantías y la preocupación de las comunidades es que se cierre antes de que sean incluidas en este recurso de ley.

Los indígenas exigen la adecuación de viviendas, en especial para cinco comunidades que están actualmente en condiciones de vulnerabilidad, para darle vía libre a un proyecto de reubicación ya acordado, se necesita como último paso la firma del alcalde. En cuanto a educación, se necesita la construcción de escuelas y asignación de profesores, el contrato ya está firmado, pero la administración todavía no ha realizado el desembolso a las dos instituciones que lo solicitan.

**Fernando** afirma que hasta el momento no existe presencia del **ESMAD** por la buena comunicación que se ha tenido con la **Policía Nacional**, en cuanto a la respuesta de la **Administración Distrital**, las comunidades aceptarán únicamente una respuesta positiva, de lo contrario no levantarán la protesta hasta que cumplan los acuerdos.
