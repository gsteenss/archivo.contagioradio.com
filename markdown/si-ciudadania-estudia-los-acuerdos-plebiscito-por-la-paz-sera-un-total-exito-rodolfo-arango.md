Title: "Si ciudadanía estudia los acuerdos Plebiscito por la paz será un total éxito" Rodolfo Arango
Date: 2016-07-19 16:07
Category: Entrevistas, Paz
Tags: plebiscito por la paz, refrendación acuerdos
Slug: si-ciudadania-estudia-los-acuerdos-plebiscito-por-la-paz-sera-un-total-exito-rodolfo-arango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/plebi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

###### [19 de Jul] 

Después de la aprobación de la Corte Constitucional al plebiscito como mecanismo para refrendar los acuerdos de paz entre el gobierno y las FARC-EP, se establecieron las condiciones para que este proceso se lleve a cabo, se dispuso que **el plebiscito sea de carácter vinculante** y se determinó** la participación de personas públicas o partidos políticos en las campañas del si o el no**.

"Si la ciudadanía, si la población no se deja arrastrar por la demagogia o por el miedo, sino por el contrario **se tranquiliza, se empodera, estudia los acuerdos, escuchas las posiciones y participa activamente**, será un total éxito, va a ser algo muy importante para la vida democrática del país" afirmo Rodolfo Arango.

En principio el **carácter vinculante** se refiere a que si gana el sí en el plebiscito por la paz, el presidente de la República, en este caso Juan Manuel Santos, **podrá incorporar los acuerdos en la Constitución a través de proyectos o actos legislativos  que posteriormente el Congreso debatirá**, de acuerdo con el análisis del abogado y constitucionalista  Rodolfo Arango, esto significa que finalmente será el Congreso quien incorpore a la legislación  y al orden jurídico los acuerdos por vía de debate, a través de una reforma constitucional que se desarrollaría en 3 sesiones.

Sobre la decisión de la Corte referente a la **participación de funcionarios públicos y partidos políticos** o contenidos que promuevan un movimiento social o grupos significativos de ciudadanos, Arango considera que **sí se permitiría que funcionarios públicos participen de las campañas del sí o el no al plebiscito**, sin embargo afirma que lo que se quiere evitar es que se mezclen las campañas electorales con las campañas del plebiscito desde los partidos políticos, [para que no exista una promoción de candidatos ni una instrumentalización del proceso](https://archivo.contagioradio.com/cuatro-millones-quinientos-mil-deberan-votar-si-en-plebiscito-para-la-paz/).

Frente a la posibilidad de que **gane el No al plebiscito por la paz**, el constitucionalista cree que "el panorama **no es tan catastrófico,** ya que en algunas ocasiones las FARC-EP han señalado que ellos[no están dispuestos a volver al monte y empuñar las armas](https://archivo.contagioradio.com/la-mejor-herramienta-para-votar-si-al-plebiscito-es-la-alegria-angela-maria-robledo/)", hecho que generaría que se lleve a cabo una re negociación de las condiciones de los acuerdos.

 <iframe src="http://co.ivoox.com/es/player_ej_12274562_2_1.html?data=kpefmZmZepOhhpywj5eWaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncbPjxdTZyNSPhdPVz8zch5enb6LiwtHW1dnFcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
