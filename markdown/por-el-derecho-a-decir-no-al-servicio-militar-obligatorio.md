Title: Por el derecho a decir: 'NO al servicio militar obligatorio'
Date: 2016-02-29 12:53
Category: Hablemos alguito
Tags: batidas del Ejército, Objeción de conciencia en Colombia, reclutamiento forzado
Slug: por-el-derecho-a-decir-no-al-servicio-militar-obligatorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/servicio_militar_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### <iframe src="http://co.ivoox.com/es/player_ek_10616509_2_1.html?data=kpWjk5uZdJqhhpywj5WVaZS1kZuSlaaXdI6ZmKialJKJe6ShkZKSmaiRdI6ZmKiastTWb8bgjMnS1MrHrNCfwpDRx8jNtozC0JDOzpDXqdPqysjW0ZDRrc3d1cbfjdTGsMrbwtnc1JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Hablemos Alguito, Objeción de conciencia] 

##### [29 Feb 2016.]

La Acción Colectiva de Objetores de conciencia (ACOC), nace a partir de la iniciativa de jóvenes de diversas organizaciones con el objetivo de trabajar sobre el derecho a la objeción de conciencia al servicio militar obligatorio, reusando el uso de niños, jóvenes o adultos por parte de cualquier ejército legal o ilegal, en particular el realizado por el Ejército Nacional.

De acuerdo con Nicolás Rodríguez y Luis Carlos Tolosa, integrantes de ACOC, el reclutamiento forzado es toda acción adelantada por grupos armados para incorporar en sus filas personas en contra de su voluntad, siendo el Ejército regular el mayor responsable de estas acciones, debido a su tamaño y prácticas con cifras que sobrepasan los 400.000 reclutamientos por año, superando ampliamente a las realizadas por agrupaciones paramilitares y guerrilleras.

La falta de garantías para acceder a la educación media o superior, aseguran los activistas, hace que muchos jóvenes deban cumplir hasta 6 o incluso 12 meses más de prestación de servicio que los bachilleres que son incorporados al culminar sus estudios. Cifras de la Defensoría del Pueblo indican que en el año 2013 fueron reclutados 10823 soldados campesinos,  22.258 soldados bachilleres y 75000 soldados regulares (de manera obligatoria).

Para el presente año, el Ejército nacional debe cumplir con una “cuota” de 87.000 nuevos reclutas en el primer contingente, adquirida con la firma del llamado ´Plan Colombia´, cantidad que según Rodríguez y Tolosa es la “cifra pública” que maneja el ejército y que corresponde a una disminución de la originalmente establecida, con la que se buscaba conformar ejércitos de 500.000 hombres y mujeres.

Los miembros de la iniciativa objetora de conciencia, afirman que el 30% del personal que tiene el ejército, compuesto únicamente por soldados regulares, está siendo utilizado para integrar los batallones megamineros, que protegen los intereses de particulares a expensas de la vida de algunos de los jóvenes más pobres del país, razón que motiva la existencia de la ACOC.

Uno de los primeros casos de 2016 es el del joven indígena [Daniel Morera Pamo](https://archivo.contagioradio.com/joven-indigena-le-gano-la-pelea-al-servicio-militar-obligatorio/), perteneciente al resguardo Palma Alta de Natagaima, Departamento del Tolima, quien estuvo 20 días retenido ilegalmente en el Batallón Mecanizado Silva Plazas de Duitama, donde fue obligado a raparse, cambiar su ropa habitual por un camuflado y botas, y recibir instrucción militar, pese a que de acuerdo con la Ley 48 de 1993 “los miembros de comunidades indígenas están exentos en todo tiempo de prestar el servicio militar”.

El joven Morera y su madre, la señora Flor Pamo, compartieron en [Hablemos Alguito](https://archivo.contagioradio.com/programas/hablemos-alguito/), la experiencia por la que tuvieron que pasar para hacer valer sus derechos y como gracias a su fuerte convicción y al apoyo de la ACOC, lograron superar uno de los episodios más difíciles de su vida.

 
