Title: Jorge Iván Ramos, excombatiente de FARC es asesinado en Bolívar
Date: 2020-08-30 10:34
Author: CtgAdm
Category: Actualidad, Nacional
Tags: asesinato, bolivar, excombatientes, Excombatientes de FARC, FARC
Slug: jorge-ivan-ramos-excombatiente-de-farc-es-asesinado-en-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/jorge-ivan-ramos-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 29 de agosto en horas de la tarde **fue asesinado el excombatiente de las FARC, [Jorge Iván Ramos](https://www.youtube.com/watch?v=CMmLCTt0aY8), en la vereda Palmachica**, en la Serranía de San Lucas, en el municipio de Santa Rosa, al Sur del departamento de Bolívar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El partido FARC denunció que como firmante de paz, Jorge Iván Ramos, **era miembro del Consejo Nacional de los Comunes del Partido Fuerza Alternativa Revolucionaria del Común**, e Integrante del Consejo Político Territorial Magdalena Medio, y titular de la Junta de Direccionamiento Estratégico del Pnis.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1299830266945515521","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1299830266945515521

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por medio de un comunicado el [Espacio Territorial de Capacitación y Reincorporación](https://archivo.contagioradio.com/por-amenazas-y-riesgos-excombatienes-de-ituango-se-desplazan-a-mutata/) Juan Carlos Castañeda, informó que **Ramos se desplazó el 28 de agosto de la vereda donde habitaba debido a la necesidad de recolectar información sobre las coordenadas para la entrega de tierras** en el marco de los Acuerdos con el Gobierno Nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este trayecto fue, ***"retenido, despojado de sus pertenencias, amarrado y vilmente asesinado por hombres armados pertenecientes al Ejército de Liberación Nacional (ELN)"***, cerca a la vereda Palmachica en el municipio de Santa Rosa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Jorge Iván Ramos, **fue comandante del Frente 37 de las FARC que operaba en los [Montes de Maria](https://archivo.contagioradio.com/informe-recopila-casos-de-campesinos-victimas-de-capturas-masivas-en-montes-de-maria/), en dónde fue conocido como Mario Morales**; su asesinato es el segundo registrado desde la Firma del Acuerdo de Paz en el 2016, en donde pierde la vida un alto mando de la extinta guerrilla.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto luego de que **el pasado 14 de mayo del 2019, fuese asesinado por sicarios Wilson Saavedra,** excombatiente del Frente 21 de la columna Víctor Saavedra Ramos, en el Valle del Cauca. ([Asesinan a Wilson Saavedra ex comandante de FARC que aportó a la paz](https://archivo.contagioradio.com/asesinan-a-wilson-saavedra-ex-comandante-de-farc-que-aporto-a-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El comunicado del ETCR también señala que Ramos, *"**era un hombre de pasos firmes en la construcción colectiva de una paz estable y duradera,** era comprometido desde su inicio en la lucha por un país equitativo, desde el sentir mismo del campesino sin tierra, desposeído y sin las garantías sociales y económicas".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez hicieron un llamado a la comunidad internacional y nacional para que se garantice la seguridad de los y las firmantes de paz, *"es inadmisible que hechos como estos sigan sucediendo a lo largo y ancho del territorio nacional con la [mirada cómplice del Gobierno](http://www.indepaz.org.co/informe-presencia-de-grupos-armados-en-colombia-2018-2019/), seguimos exigiendo que se cumpla con lo pactado en el Acuerdo de Paz".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el asesinato de Jorge Iván Ramos la cif**ra de firmantes de paz que han sido asesinados llega a 225, acción a la que se suma el desplazamiento, hostigamiento y violencia** constante que sufren los excombatientes y sus familias en todo el territorio nacional. ([En dos años del Gobierno Duque se ha derrumbado institucionalidad](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
