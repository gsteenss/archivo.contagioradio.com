Title: ¿Qué le pasaría al planeta si se extinguen las abejas?
Date: 2016-11-16 14:54
Category: Animales, Nacional
Tags: abejas, animales en peligro de extinción, FAO, polinizadores
Slug: que-pasa-si-se-extinguen-las-abejas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/abejas-e1479325565637.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Youtube 

###### [16 Nov 2016] 

Las abejas no solo son ahora una especie en peligro de extinción como lo ha decidido en Estados Unidos, el Servicio de Pesca y Vida Salvaje, si no que además sin estas polinizadoras, la vida en el plantea también podría extinguirse.

“**Sin los polinizadores, muchos de nosotros no podríamos consumir café**, chocolate o manzanas, entre otros alimentos de nuestra vida diaria” explica Simon Potts, vicepresidente de la IPBES y profesor en la Universidad de Reading en el Reino Unido). Aunque animales como las abejas son **polinizadores de la vida,** pues son un proceso viviente de restauración ecológica de bosques la situación de las abejas en el mundo es alarmante.

Diversos estudios han evidenciado que los monocultivos y los pesticidas serían los principales causantes de esta situación, pese a que de las **100 especies de vegetales que proveen el 90% de los alimentos en 146 países, 71 son polinizadas por abejas,** como lo señala la Organización de las Naciones Unidas para la Agricultura y la Alimentación, FAO.

### La situación de las abejas en el mundo 

En Estados Unidos para 1988 existían registros de cinco millones de colmenas, sin embargo, hoy queda apenas la mitad. Según un informe del Programa de las Naciones Unidas para el Medio Ambiente, PNUMA, **en los últimos años los apicultores chinos enfrentaron pérdidas de colonias “inexplicables”** y los criadores egipcios informaron del colapso varias colonias. Por otra parte, en Europa se presentaron pérdidas de hasta un 25% desde 1985.

En Colombia la situación no parece mejorar. Según Sputnik Francisco Silva, representante legal y fundador de Apisred, empresa colombiana dedicada al desarrollo de la apicultura, este año se han reportado más de 3.000 colmenas que desaparecieron en el departamento de Quindío por fumigaciones. En Santander del Sur sucede lo mismo y en Cundinamarca, específicamente en el municipio de Guasca, apicultores denuncian que las** fumigaciones indiscriminadas** con fuertes químicos tóxicos a los cultivos de papa en el páramo grande, están acabado con la vida de más de 150 colmenas de abejas.

“De no revertirse esta situación lo que sigue es la muerte de la humanidad. Nos vamos a quedar sin alimentos. Si en el mundo no se emprende una campaña para defender a los polinizadores, corremos nosotros peligro de desaparecer”, concluye Silva.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
