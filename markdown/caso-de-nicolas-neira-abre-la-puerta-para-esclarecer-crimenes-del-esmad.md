Title: Caso de Nicolás Neira abre la puerta para esclarecer crímenes del ESMAD
Date: 2017-08-08 16:14
Category: DDHH, Entrevistas
Tags: ESMAD, nicolas neira, yuri neira
Slug: caso-de-nicolas-neira-abre-la-puerta-para-esclarecer-crimenes-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/NICOLAS-NEIRA-ANIVERSARIO-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Seryosem] 

###### [08 Ago. 2017]

Luego de conocerse la **aceptación de responsabilidad por parte del mayor (R) Julio César Torrijos en el encubrimiento del asesinato de Nicolás Neira, su padre Yuri Neira** manifestó que de no haber sido por el ataque que han hecho contra la Fiscalía General de la Nación, contra el Fiscal General y los fiscales delegados de derechos humanos, no habrían llegado hasta donde lo han hecho luego de 12 años dado que “el interés de la justicia ha sido tapar los hechos y los responsables”.

### **¿Qué busca Yuri Neira luego de tantos años de luchar por justicia?** 

Asegura Neira que luego de 12 años del asesinato de su hijo de 15 años de edad sigue buscando que el autor material que ya está identificado acepte los hechos, pero además espera que delate a las otras personas que estuvieron ahí. Le puede interesar: [Sin Olvido - Nicolas Neira](https://archivo.contagioradio.com/nicolas-neira/)

“El ex mayor de la Policía ya está delatando a quiénes dieron la orden para encubrir la situación, qué oficiales, que son Coroneles y Generales de la República y a su vez qué Ministro de Defensa les autorizó porque **ese año del 2005 en todo el país el Escuadrón Móvil Antidisturbios –ESMAD- trató a toda la población a patadas a agredirlos**, a asesinarlos y el primero que cayó fue Nicolás Neira”.

Para Neira, **no solo deben ser culpados los integrantes de las fuerzas policiales sino también quien fue el mandatario desde 2002 a 2010,** al que llama como “el narcotraficante número 82, porque él es el máximo responsable, hasta allá pretendo llegar yo”, haciendo referencia al ahora senador Álvaro Uribe Vélez.

A la fecha por el asesinato de Nicolás Neira han sido destituidos e inhabilitados para la función publica por 10 años, el Subteniente Edgar Mauricio Fontal Cornejo y el teniente Julio César Torrijo Devia, capturado tiempo después con 103 kilos de cocaína.

### **Yuri Neira exige justicia para el caso de su hijo desde el exilio** 

Todas las amenazas, atentados, disparos, el allanamiento, las detenciones y la tortura de las que ha sido víctima Yuri Neira se encuentran en investigaciones, que hasta la fecha no han tenido ningún avance más que la relación en los expedientes.

“Incluso hubo una fiscal que me sindicó a mí de ser célula de la guerrilla, de fabricar explosivos y de construir armas. Luego la echan de la Fiscalía y sigue por ahí litigando. Los policías que me detuvieron me colocaron una demanda porque supuestamente, yo solo contra 6 policías, logré ocasionarles lesiones personales de los cuales uno tuvo más de 10 días de incapacidad. El fiscal del caso me llamó a conciliar y palabras textuales le dije, yo no concilio con asesinos, voy hasta las últimas consecuencias”.

Pese a todas esas situaciones Neira dice que él sigue adelante, que ha sufrido las consecuencias por hablar claro, por decir las cosas, por eso manifiesta que pese a que **tiene miedo es capaz de regresar al país y estar al frente denunciando** y gritando.

### **El ESMAD en cifras** 

Según el Centro de Investigación y Educación Popular (CINEP) entre 2002 y 2014 se registraban 448 casos en los que el ESMAD había agredido a personas, de los cuales 137 presentaron heridas, 91 personas fueron detenidas, 107 amenazadas, 2 sufrieron agresiones sexuales y 13 ejecuciones extrajudiciales.

Otras cifras entregadas por el representante a la cámara Alirio Uribe aseguran que **durante el 2016, cerca de 682 personas fueron víctimas de agresiones por parte del ESMAD,** razón por la cual Uribe del Polo Democrático y Ángela María Robledo del Partido Verde, han solicitado el desmonte del ESMAD.

Sin embargo, el 30 de Junio de 2016, el presidente Juan Manuel Santos aseguró que aumentaría el número de integrantes de este escuadrón, pues con la firma de la paz las movilizaciones, protestas y alteraciones de orden público que habrá que controlar serán más. Le puede interesar: [El prontuario del ESMAD](https://archivo.contagioradio.com/el-prontuario-del-esmad/).

<iframe id="audio_20235920" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20235920_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
