Title: Víctimas exigen ser nuevamente el centro de la construcción de la paz
Date: 2018-04-06 13:29
Category: DDHH, Nacional
Tags: 9 de abril, acuerdo de paz, conflicto armado, Gaitán, Redepaz, víctimas del conflicto armado
Slug: victimas-exigen-ser-nuevamente-el-centro-de-la-construccion-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/gaitan2-e1523037294144.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Sintraemsdes Medellín] 

###### [06 Abr 2018] 

Organizaciones sociales celebrarán el 9 de abril el **día nacional de la memoria y la solidaridad con las víctimas del conflicto armado con una movilización** en Bogotá. Así mismo, la familia Gaitán a invitado a participar de la celebración ecuménica por los 70 años del magnicidio de Jorge Eliecer Gaitán.

### **“Las víctimas pasaron de estar en el centro de los acuerdos a la periferia”** 

Luis Emil Sanabria, director de Redepaz, indicó que “queremos destacar las condiciones en las que se encuentra **el proceso de reparación integral** y la situación frente a la re victimización y asesinatos de los líderes sociales”. Para esto, en la Plaza de Bolívar harán una actividad con ataúdes “que representan los más de 240 líderes sociales asesinados en los últimos dos años en Colombia”.

Sanabria recordó que la situación que viven las víctimas registradas en el país es preocupante en el sentido que tan sólo **el 10% de ellas han recibido alguna indemnización**. Afirmó que “en Colombia se tiene conocimiento de cerca de 9 millones de víctimas y tan sólo han sido reparadas 800 mil de ellas”.

Además, indicó que “no hay recursos suficientes para atender las solicitudes de las víctimas como tampoco hay un programa de atención a las víctimas que se implemente **con fuerza y respaldo territorial**”.  En el tema de retornos, confirmó que “hay un estancamiento en los procesos de retorno individual y colectivo, mientras que sólo el 4.5% de las tierras despojadas han sido restituidas”. (Le puede interesar:["Jóvenes víctimas del conflicto armado le pide a Petro, De la Calle y Fajardo que formen una gran coalición"](https://archivo.contagioradio.com/jovenes-le-piden-a-petro-de-la-calle-y-fajardo-que-formen-la-gran-coalicion/))

La movilización saldrá a las **9:00 am del 9 de abril desde el Planetario** en el centro de Bogotá donde harán un acto simbólico. Seguidamente caminarán por la calle 26 para llegar a la carrera décima y subir por la calle 13 hasta llegar a la Plaza de Bolívar.

### **Familia Gaitán recordará 70 años del asesinato del caudillo liberal ** 

Al mismo tiempo de esta actividad, defensores de los derechos humanos vinculados a la iglesia católica y la familia de Jorge Eliecer Gaitán, recordarán la memoria del caudillo liberal **asesinado el 9 de abril de 1948**. Este año recordarán 70 años del magnicidio “con una ceremonia ecuménica e interreligiosa” donde participarán quienes encuentran en Gaitán “a un hombre que encarnó los deseos de justicia social para Colombia”.

Según la familia Gaitán, se ha dialogado sobre **“la responsabilidad de la iglesia** en el genocidio y memoricidio del que ha sido víctima el Movimiento Gaitanista y sectores del partido Liberal”. Recordaron que “en el mes de septiembre celebramos un acto público de pedido de perdón del que se derivaron propuestas de colaboración de cara a la recuperación de la Casa Museo Jorge Eliécer Gaitán como construcción de la verdad histórica".

La conmemoración ecuménica se realizará en la **Universidad Libre de Colombia**, Casa Museo Rafael Uribe Carrera 5 \# 9-09 a las 12:00 m. Esta ceremonia “estará antecedida de una audiencia privada de la familia Gaitán con la Comisión de Esclarecimiento de la Verdad”.

<iframe id="audio_25157244" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25157244_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
