Title: Arranca diálogo de campesinos del Paro Nacional con el Gobierno
Date: 2017-10-26 15:52
Category: DDHH, Nacional
Tags: campesino, cumplimiento de acuerdos, paro de campesinos, paro nacional indefinido
Slug: continua-paro-nacional-indefinido-de-campesinos-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/paro-nacional-e1509051134914.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Directa] 

###### [26 Oct 2017 ] 

En rueda de prensa las organizaciones sociales y campesinas, que instalaron hace tres días el paro nacional indefinido, **hicieron un balance de la protesta** y aseguraron que en la tarde este jueves se realizará una primera reunión con el Gobierno Nacional en la que se discutirá la continuidad de la protesta y se definirá si se instalan o no mesas de diálogo regionales.

Los integrantes de la MIA le recordaron a la sociedad colombiana que **“la paz no es el silencio de los fusiles**, sino la garantía de la vida y el mejoramiento de las condiciones sociales, políticas y económicas de colombianas y colombianos, que es lo que despliega los seis puntos del Acuerdo Final de Paz”.

### **Así va el paro en Norte de Santander** 

Además, rechazaron las intimidaciones y las agresiones que se han presentado en Norte de Santander “en donde **alrededor de 200 efectivos del ESMAD agredieron** con fuerza desmedida y armas no convencionales a los campesinos agrupados en el borde de la carretera en el punto de concentración conocido como la Y de Astilleros, municipio de Zulia”. (Le puede interesar:["Gobierno a responder con hechos a campesinos en paro nacional indefinido"](https://archivo.contagioradio.com/comunidades-y-organizaciones-redactaron-pliego-de-peticiones-para-el-gobierno-nacional/))

### **Así va el paro en el Cauca** 

Denunciaron que, en el Cauca, “**se han presentado sobrevuelos intimidatorios de helicópteros** en el punto de concentración conocido como El Descanso, municipio de Caldono, en donde al menos mil efectivos del ESMD se agrupan al lado de los manifestantes”.

Indicaron que en los diferentes puntos de movilización del país, en los que se encuentran 5 en el Cauca, 2 en Antioquia, 5 en Norte de Santander, uno en Guaviare y uno en el Sur de Bolívar, ya se han movilizado más de **20 mil personas** de forma permanente.

### **Campesinos continúan firmes con el paro nacional indefinido** 

Con respecto a la presencia de la fuerza pública en los territorios, afirmó que, en algunas zonas, los dirigentes han conversado con la Policía y el ESMAD para evitar que suceda cualquier tipo de enfrentamiento. Argumentó que, en el Catatumbo, acordaron y **firmaron un acuerdo de voluntades de no represión**, en donde “los campesinos se comprometen a no bloquear y los Policías se comprometen a no agredir a la gente”.

Además, enfatizó que en el Catatumbo hay una presencia constante de miembros de las Naciones Unidas que ha ayudado a **garantizar el respeto por los derechos humanos** de las personas que se encuentran realizando la movilización. Esta delegación, “tiene como objetivo facilitar el diálogo y evitar los actos violentos por parte de manifestantes y Fuerza Pública”. (Le puede interesar:["Así será el paro nacional a partir de este lunes"](https://archivo.contagioradio.com/23-de-octubre-hora-cero-para-el-paro-nacional/))

### **Investigaciones por asesinatos en movilización de 2013 no han avanzado** 

En entrevista con Contagio Radio, Pablo Téllez, representante de Ascamcat, indicó que la movilización social también está dirigida a que el Gobierno Nacional cumpla con los acuerdos pactados **que se firmaron en 2013 con diferentes comunidades del país**. Allí, se había acordado la mejora en las condiciones de vida de las comunidades y al día de hoy no se ha cumplido ni siquiera con el mejoramiento vial de algunos territorios.

De igual forma, indicó que los **asesinatos de 4 campesinos en Ocaña** en el marco de las movilizaciones, aún no se han esclarecido y las investigaciones para encontrar a los responsables no han iniciado. Para exponer esto, los campesinos se encuentran reunidos con representantes del Gobierno Nacional, quien está realizando un balance del cumplimiento de los acuerdos.

### **Campesinos esperan respuestas por parte del Gobierno Nacional** 

Los campesinos indicaron que es necesario que el Gobierno Nacional cumpla con los acuerdos pactados y **de respuesta al pliego de peticiones** que redactaron el 23 de octubre. En la reunión que esperan tener, “vamos a estar pendiente de la respuesta que nos den, para avanzar en la instalación de las mesas de negociación en cada departamento”. (Le puede interesar: ["Policía sigue actuando de forma criminal en Tumaco"](https://archivo.contagioradio.com/policia-sigue-actuando-de-manera-criminal-en-tumaco/))

Finalmente, expresaron la voluntad que tienen en 17 departamentos para acogerse a la **sustitución voluntaria de cultivos ilícitos,** teniendo en cuenta que el  Gobierno Nacional les debe brindar garantías para continuar con su actividad agrícola. También, le pidieron a las autoridades que actúen de manera inmediata ante la falta de seguridad que tienen los líderes sociales en el país, debido al aumento en el número de asesinatos contra ellos y ellas.

<iframe id="audio_21714717" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21714717_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
