Title: Rafael Alberti, "Retornos de una sombra Maldita"
Date: 2015-10-28 12:30
Category: Viaje Literario
Tags: Generación del 27, Poesía Española, Rafael Alberti, Retornos de una sombra maldita
Slug: rafael-alberti-retornos-de-una-sombra-maldita
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/rafaelAlberti-e1446059587447.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  <iframe src="http://www.ivoox.com/player_ek_9194044_2_1.html?data=mpamlpWYeI6ZmKiakpmJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsLawsrZjabQpsbm1c6SlKiPaZOms8rh0dfSs9SfxcqY19PFb9Tjzsffw5Cxpc3YytnOh5eWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [ Rafael Alberti, "Retornos de una sombra Maldita"]. 

##### [29, Oct 2015]

Rafael Alberti, fue un poeta y dramaturgo español, nacido en el puerto de Santa María, en Cádiz, en 1902, considerado como uno de los grandes poetas del panorama literario español y último sobreviviente de la Generación del 27  en lo que al verso se refiere.

Ganador del Premio Nacional de Literatura en 1925 y del Premio Cervantes en 1983. Durante la guerra civil militó activamente en la política y dirigió varias revistas de orientación comunista, lo que le representó vivir en el exilio hasta el año de 1977.

Entre sus obras más importantes se cuentan Marinero en Tierra, Sobre los Ángeles, Cal y Canto y Sermones y Moradas. Falleció en Madrid el 28 de octubre de 1999, hoy recordamos su obra poética con "Retornos de una sombra Maldita".
