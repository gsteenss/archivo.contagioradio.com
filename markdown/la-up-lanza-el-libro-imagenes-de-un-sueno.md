Title: La UP lanza el libro Imágenes de un Sueño
Date: 2016-11-16 12:43
Category: DDHH, Nacional
Tags: Bogotá, Imagenes de un sueño, Lanzamiento del Libro, Unión Patriótica
Slug: la-up-lanza-el-libro-imagenes-de-un-sueno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/thousands-murdered-union-patriotica-e1473968718811.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [16 Nov. 2016] 

Así se titula el libro que lanzará este miércoles 16 de noviembre el Movimiento Político de la Unión Patriótica de la mano del Comité Permanente por la Defensa de los Derechos Humanos – CPDH.

**Este libro, tuvo todo un largo trabajo para llegar a su lanzamiento y recogió el esfuerzo de varios autores que investigaron archivos fotográficos e históricos para poder llevar a buen término esta iniciativa.**

Aida Avella, vocera de la Unión Patriótica manifestó que ***Imágenes de un Sueño* busca resaltar el trabajo hecho por muchos anónimos que han sido pieza importante en la historia de este movimiento y del país** “en este libro pesó mucho el trabajo de Alejandra Gaviria, hija de Francisco Gaviria asesinado en Medellín… y son los intereses que tienen los jóvenes para reivindicar y hacer conocer la vida no solo de sus padres, sino todo el proceso que se vivió con la Unión Patriótica”. Le puede interesar: [La Unión Patriótica quiere demostrar que hay otra forma de gobernar](http://bit.ly/2fXR6Dq)

**Imágenes de un Sueño como su nombre lo indica, es la representación de los sueños de muchos colombianos que querían y quieren otro país** “un lugar donde haya un mínimo de democracia que no se representa en echar un día un poco de votos en una urna, sino el bienestar de los ciudadanos. Cuando los ciudadanos tienen bienestar, el país puede decir que hay una democracia” aseguró Avella.

En Imágenes de un Sueño los lectores podrán encontrarse con la expresión de alegría por el nacimiento de este movimiento en la década de los ochenta, el auge de su proyecto político, el esfuerzo de las campañas electorales y la experiencia en sus gobiernos populares.

Aida Avella concluye diciendo que “**este libro es un gran aporte, no solamente para los jóvenes sino para las generaciones que no han nacido y que tienen que saber que eso que hicieron – asesinar a quien piensa distinto- nunca volverá a repetirse”. **Le puede interesar: [Si no se acaba con el paramilitarismo “yo creo que se va a repetir el caso de la Unión Patriótica”](https://archivo.contagioradio.com/el-paramilitarismo-tiene-una-fuerza-tan-grande-que-yo-creo-que-se-va-a-repetir-el-caso-de-la-union-patriotica/)

**Datos del lanzamiento:**

Fecha: miércoles 16 de noviembre a las 6 p.m.  
Lugar: Auditorio Teresa Cuervo  
Carrera 7ª con calle 28  - Bogotá

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
