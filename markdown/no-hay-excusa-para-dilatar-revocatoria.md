Title: "No hay excusa para seguir dilatando la revocatoria" Unidos Revocamos a Peñalosa
Date: 2017-09-06 13:25
Category: Nacional, Política
Tags: Bogotá, Comité revocatoria, Consejo Nacional Electoral, Firmas revocatoria, revocatoria Peñalosa
Slug: no-hay-excusa-para-dilatar-revocatoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/revocatoria_Peñalosa-e1504722871558.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica] 

###### [06 Sept 2017] 

Tras la orden del Tribunal de Cundinamarca a la Registraduría Nacional de certificar las firmas recogidas para la revocatoria del alcalde de Bogotá, el comité promotor de esta acción"unidos revocamos a Peñalosa", **calificó la decisión como oportuna y necesaria** en vista de las constantes demoras y obstáculos que, según ellos, han afectado el proceso.

Si bien la decisión del tribunal** da un plazo de 10 días a la Registraduría para la certificación de las firmas**, la continuidad depende del Consejo Nacional Electoral que debe avalar las cuentas de financiación, que ya fueron presentadas en Julio por el Comité. En esto el CNE se ha demorado más de lo esperado y no tiene un límite de tiempo que los obligue a acelerar esta clase de procesos.

Carlos Carrillo, integrante del Comité Unidos Revocamos a Peñalosa, dijo que **“el CNE está lejos de fallar en derecho, es uno de los más corruptos”.** Sin embargo, calificó la decisión del Tribunal como “una de las tantas que nos han favorecido en este proceso”.

Carrillo dijo que Peñalosa y sus abogados “han dilatado las cosas en varias oportunidades para salvarse de la revocatoria”. (Le puede interesar: ["Revocatoria a Peñalosa podría realizarse en noviembre"](https://archivo.contagioradio.com/revocatoria-a-enrique-penalosa-podria-realizarse-en-noviembre/))

En repetidas ocasiones el comité para la revocatoria ha dicho que el CNE no es un organismo que actúe en concordancia con la ley sino que por el contrario “**son magistrados que tienen intereses económicos y políticos** y son cercanos al alcalde Peñalosa por lo que siempre han buscado favorecerlo”.

### **Las cuentas están claras** 

Ante el argumento de que el comité promotor se excedió en los topes monetarios para financiar su campaña, Carrillo aseguró que **ellos han entregado sus cuentas en los tiempos establecidos** y que, con los 110 millones de pesos que gastaron, cumplen con los topes que fija la ley de manera individual y general para este tipo de procesos.

Sin embargo, cuestionaron el hecho de que la Fundación Azul, que se opone a la revocatoria, **“no haya revelado sus financiadores y no haya hecho pública sus cuentas”.** Carrillo aseguró que, “la ciudadanía debe saber cuánto le cuesta la publicidad multimillonaria de Peñalosa para hacer populismo limpiando postes”. (Le puede interesar: ["Proceso de revocatoria no se ha suspendido": Comité Revoquemos a Peñalosa"](https://archivo.contagioradio.com/proceso-de-revocatoria-no-se-ha-suspendido-comite-revoquemos-a-penalosa/))

### **¿Por qué Darcy Quinn sabe de una decisión que el CNE no ha tomado contra Gustavo Merchán?** 

Carrillo fue enfático en manifestar que hay** una campaña mediática para deslegitimar el proceso** que adelanta el comité de la revocatoria y para favorecer al alcalde. Según él, “periodistas como Darcy Quinn, que es esposa de un empresario beneficiado de los negocios de Peñalosa, tienen información sobre decisiones que va a tomar el CNE sin que se haya reunido la sala plena”.

Para el Comité Unidos Revocamos a Peñalosa no sería extraño que el CNE acudiera a acciones jurídicas **“para dilatar aún más la revocaria”**. Dijo Carrillo que es posible que se abra una investigación contra Gustavo Merchán pero que por el momento este no ha sido notificado como dicen algunos medios de información.

Finalmente, invitó a la ciudadanía a la **movilización del 14 de septiembre en el Planetario de Bogotá** donde exigirán que se cumpla con el proceso para que los bogotanos puedan decidir en las urnas si quieren que el Enrique Peñalosa continúe siendo el alcalde de Bogotá.

<iframe id="audio_20730628" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20730628_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
