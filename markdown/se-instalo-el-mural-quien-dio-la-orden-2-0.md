Title: Se instaló el mural «¿Quién dio la orden? 2.0»
Date: 2020-09-30 12:37
Author: PracticasCR
Category: Actualidad, Nacional
Tags: ¿Quién dio la orden?, Ejecuciones Extrajudiciales, falsos positivos, JEP, MOVICE
Slug: se-instalo-el-mural-quien-dio-la-orden-2-0
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Mural-Quien-dio-la-orden-.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Instalacion-Quien-dio-la-orden.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Quien-dio-la-orden.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Quien-dio-la-orden-1.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Mural-Quien-dio-la-orden-1-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Hoy miércoles se realizó desde las 9 de la mañana, **el lanzamiento del mural «¿Quién dio la orden? 2.0» por parte de organizaciones de víctimas pertenecientes a la Campaña por la Verdad.** El mural se instaló sobre la carrera 7° con calle 63, donde se ubican las instalaciones de la Jurisdicción Especial para la Paz -JEP-. (Lea también: [Piden a Corte Constitucional garantizar el derecho a preguntar ¿Quién dio la orden?](https://archivo.contagioradio.com/piden-a-corte-constitucional-garantizar-el-derecho-a-preguntar-quien-dio-la-orden/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":90704,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Mural-Quien-dio-la-orden-1-1-1024x512.jpeg){.wp-image-90704}  

<figcaption>
Mural «¿Quién dio la orden? 2.0» ubicado en la Cra. 7 con calle 63 en Bogotá

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

El mural es una segunda versión de la ya conocida muestra  «¿Quién dio la orden?», en el que se reúnen más rostros de presuntos victimarios y responsables de ejecuciones extrajudiciales como los militares **Henry Torres Escalante, José Joaquín Cortés, Fabricio Cabrera, Diego Tamayo, Miguel David Bastidas, Juan Pablo Rodríguez, Mario Montoya Uribe y Publio Hernán Mejía.** (Le puede interesar: [MOVICE se afirma en favor de la tipificación del paramilitarismo como delito](https://archivo.contagioradio.com/movice-se-afirma-en-favor-de-la-tipificacion-del-paramilitarismo-como-delito/))

<!-- /wp:paragraph -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Instalacion-Quien-dio-la-orden.mp4">
</video>
  

<figcaption>
Instalación del mural en la Cra. 7 con calle 63 Bogotá. 30 de sept. 2020

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Justamente, **frente a estos dos últimos militares (Mario Montoya Uribe y Publio Hernán Mejía); organizaciones** **solicitarán formalmente ante la JEP  la exclusión de dicha jurisdicción, pues según las víctimas su contribución con la verdad del conflicto ha sido nula.** (Le puede interesar: [Leyner Palacios representará el legado de las víctimas ante la Comisión de la Verdad](https://archivo.contagioradio.com/leyner-palacios-representara-el-legado-de-las-victimas-ante-la-comision-de-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La petición será elevada por organizaciones defensoras de Derechos Humanos y representantes de víctimas que hacen parte del Espacio de Litigio Estratégico; mediante dos **solicitudes de apertura de incidente de incumplimiento**, una figura que contempla la Ley Estatutaria de la Jurisdicción Especial para la Paz; **cuando los agentes estatales que se han sometido a esta justicia, para obtener beneficios a cambio de verdad, no cumplen a cabalidad con este propósito.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según esta disposición **dicho incumplimiento, se concretaría en la exclusión de los militares y la pérdida de beneficios como libertad transitoria o penas más bajas** de las que contempla la justicia ordinaria.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
