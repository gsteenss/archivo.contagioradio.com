Title: Efren Zúñiga, docente y líder de Asoinca fue torturado y asesinado en Piendamó, Cauca
Date: 2018-04-16 12:15
Category: datos, Paz
Tags: Asesinatos, ASOINCA, Cauca, Efren Zúñiga, lideres sociales
Slug: efren-zuniga-docente-y-lider-de-asoinca-fue-torturado-y-asesinado-en-piendamo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Indígenas-del-Norte-del-Cauca-e1464380469118.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Los mundos de Hachero. ] 

###### [16 Abr 2018] 

La organización Asoinca y la Mesa por la defensa  de los derechos humanos, **de la vida y el territorio denunciaron el asesinato de Efren Zúñiga**, en el municipio de Piendamó, Cauca, el pasado 14 de abril, luego de haber sido torturado y según testigos, obligado a cavar su propia tumba. Asoinca afirmó, que con el asesinato de Efren, se completa la aterradora cifra de 43 líderes sociales y sindicalistas asesinados en el Cauca en lo que lleva el año 2018.

Los familiares del líder social señalaron que el docente salió de su casa a negociar una finca al municipio de Piendamó, lugar en el cual fue encontrado su cuerpo con un tiro en la cabeza. **Así mismo aseguraron que al parecer Efren fue torturado y obligado a cavar su propia tumba**.

**Siguen matando a los líderes en el Cauca**

De acuerdo con Tito Torres, presidente de Asoinca, en lo corrido de este año han sido asesinados 43 líderes sociales, sindicalistas y defensores de derechos humanos en el Cauca, que a su vez, evidencia **“la permanente y sistemática violación a los derechos humanos direccionados a quienes pertenecen al movimiento social en Cauca”.**

Asimismo, dijo que detrás de estos actos se encuentran **estructuras paramilitares** que se han apropiado del territorio, pese a las múltiples denuncias que se han hecho a las autoridades del aumento de este actor armado en el Cauca. (Le puede interesar: ["No hay freno en el asesinato a líderes sociales"](https://archivo.contagioradio.com/continuan-los-asesinatos-de-lideres-sociales-en-el-pais/))

Frente a las medidas que toma el gobierno para defender a la comunidad Torres afirmó que, “el gobierno hace caso omiso a las peticiones que se hacen frente los hechos de asesinatos de líderes sociales”, y no existe una respuesta clara por parte de las autoridades para encontrar una solución que garantice la vida de los líderes.

<iframe id="audio_25395929" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25395929_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
