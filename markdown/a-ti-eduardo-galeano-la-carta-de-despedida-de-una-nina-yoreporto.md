Title: A ti, Eduardo Galeano... La carta de despedida #YoReporto
Date: 2015-04-26 12:06
Author: CtgAdm
Category: Nacional, yoreporto
Tags: #YoReporto, Eduardo Galenao, Laura Rudas
Slug: a-ti-eduardo-galeano-la-carta-de-despedida-de-una-nina-yoreporto
Status: published

###### Foto: [drugstoremag.es]

A ti, Eduardo Galeano, que te hiciste eterno, inmortal...

*“When he shall die,*  
*Take him and cut him out in little stars,*  
*And he will make the face of heaven so fine*  
*That all the world will be in love with night*  
*And pay no worship to the garish sun.”*  
*― William Shakespeare                                  *

Son pasadas las once de un infortunado trece de abril y la penumbra asalta inclemente mi morada con la misma destemplanza con que invade implacable mi alma.

El luto lo llevo a flor de piel, lamento tu muerte, la siento como si tu ausencia horadase un abismo insondable en mi ser y, si bien mi razón entusiasta me recuerda con tenacidad una y otra vez que tu muerte es ilusoria, que tu genialidad y tu esencia quedaron plasmadas en tus letras, que lo que escribiste con tinta quedará grabado eternamente mientras existan la historia y la raza humana, no puedo evitar preguntarme el porqué de ésta desazón en lo profundo de mi pecho.

No logro evadir la pequeña gota de sentimiento trágico que amenaza constante con resbalar por mi mejilla, empañando la veracidad de mi afirmación sobre la eternidad de tu ser que se manifiesta en tus palabras y fue hasta después de rumiar durante un tiempo el dolor que me abrumaba, que descubrí la razón de mi pesar.

Lamento tu muerte porque, pese a que alcanzaste una conquista inverosímil de mis sensaciones, siempre me cautivó la idea de conocer al intrépido autor de tan maravillosos bosquejos de nuestra realidad y en tu ausencia, solo me queda conocer la estela que dejaste tras de ti.

Me aterra saber que dejaste la pluma, tu arte llegó a un punto estático, no más cambio, no más movimiento, no más ideas surgirán de tus líneas, por lo menos no más ideas delineadas por ti. Nos dejaste un obsequio, un legado, un principio, una semilla, ahora el movimiento depende de los que quedamos aquí.

Considero tu muerte una pérdida para el universo, siento no haber podido experimentar el placer de conocerte frente a frente, pero te agradezco inmensamente el trozo de ti que dejaste impreso.

Leerte me permitió visitar gratuitamente millares de escenarios, abrir mis horizontes, viajar a través de situaciones y realidades, ser testigo de injusticias, apreciar la seducción de la utopía y disfrutar de la estación en donde inicia la travesía rumbo a las soluciones; conseguiste asombrarme, fascinarme, deslumbrarme, entusiasmarme, desconcertarme, aturdirme, intimidarme y obligarme a cuestionar. ¡Todo esto haciendo acopio únicamente de 27 letras y 5 diágrafos!

Tus trazos están grabados a fuego en mi memoria, porque revolucionarios valientes sin medida, críticos nobles, honorables soñadores, poetas y escritores sabios como tú...  nunca mueren.

Laura Rudas
