Title: Movimientos populares piden impeachment contra Temer
Date: 2016-12-09 16:27
Category: El mundo, Otra Mirada
Tags: Brasil, Impeachment, Temer
Slug: movimientos-populares-impeachment-temer
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/30672532274_dc1eb4dcc8_z.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Brasil de Fato 

##### 9 Dic 2016 

15 organizaciones de la sociedad civil y un grupo de cuatro juristas de Brasil, presentaron este jueves ante la Cámara de diputados una **solicitud para que el presidente no electo Michel Temer sea objeto de un impeachment por su presunta vinculación en la comisión del delito de responsabilidad**, tras las denuncias públicas del exministro Marcelo Calero.

La actual solicitud se basa además en **las declaraciones del entonces secretario de gobierno Geddel Viera Lima**, quin denunció que lo habrían presionado a liberar las obras de un emprendimiento en el estado de Bahía, región Nordeste de Brasil, construcción que había sido embargada por el Instituto de Patrimonio Histórico y Artístico Nacional (Iphan).

La petición de someter a un impeachment al presidente interino, s**e deberá tramitar de manera paralela al proceso que busca abrirse desde el Partido del socialismo y libertad (Psol), presentado el pasado 28 de noviembre** con argumentos similares a los de la nueva petición. Le puede interesar:[Golpe de estado en Brasil: mal paga el diablo a quien bien le sirve](https://archivo.contagioradio.com/golpe-de-estado-en-brasil-mal-paga-el-diablo-a-quien-bien-le-sirve/).

Los testimonios suministrados por los ex integrantes del gabinete ministerial, provocaron la salida de Geddel y pusieron en un punto álgido las relaciones al interior del gobierno provisional, situación que según los partidos de la oposición refuerza la ilegitimidad del actual mandato y la necesidad de que este sea revocado en el menor tiempo posible.

A pesar de haber sido agendados, el grupo representativo de ciudadanos y juristas **no fue recibido por el presidente de la Cámara de diputados Rodrigo Maia** como tampoco por ninguno de los diputados de la mesa directiva, razón por la cual el documento tuvo que ser radicado ante Wagner Padilha, secretario general de la mesa.

La actitud evasiva del presidente de la Cámara, fue recibida por los portadores de la petición como una respuesta desfavorable a la manifestación de la sociedad civil. Se espera que Maia realice un pronunciamiento oficial sobre la solicitud de impeachment y se refiera a las razones de su ausencia a la cita programada con anterioridad.
