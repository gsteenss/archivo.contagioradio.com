Title: La Muestra Internacional Documental de Bogotá abre convocatorias
Date: 2017-05-11 16:19
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Documental, MIDBO
Slug: midboconvocatoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/midbo-e1494540899210.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:MIDBO 

###### 11 May 2017 

En su décima novena versión,  la **Muestra Internacional Documental de Bogotá MIDBO 2017** que tendrá lugar desde el 30 de octubre hasta el 5 de noviembre, abre sus convocatorias para **realizadores, directores, productores nacionales e internacionales**.

Las categorías de la muestra para este año incluyen una dedicada al Documental Nacional, donde serán seleccionadas alrededor de **30 obras** que harán parte de la Competencia nacional, Panorama Nacional y Espejos para salir del horror, en esta última se recibirán documentales cuya temática sea el **conflicto armado, la resistencia de las comunidades y la imaginación del posconflicto**.

En la categoría **“Miradas Emergentes”** podrán participar directores y productores nacionales e internacionales, con la única condición de ser **estudiantes vigentes de un programa de estudio en el campo audiovisual**.

En la convocatoria internacional **“Otras Miradas”** podrán participar directores, productores y exhibidores extranjeros de cualquier nacionalidad **cuyos documentales hayan finalizado a partir de enero de 2016**. Le puede interesar: [5 documentales sobre la importancia del agua.](https://archivo.contagioradio.com/documentales-importancia-agua/)

En esta versión de MIDBO, se realizará la **cuarta versión el Seminario Internacional pensar lo real “Acción Documental”**, un espacio en donde se reflexionará acerca de los documentales que traten el desplazamiento, los refugiados, la lucha por el territorio o la amenaza de los recursos ecológicos. Los interesados podrán participar con sus ponencias ingresando a la página www.midbo.co

**El Plazo para la postulación de trabajos será hasta el 16 de junio** a través de la plataforma Movibeta, donde se podrá iniciar la inscripción en las diferentes categorías y conocer a profundidad los requisitos para la participación.

<iframe id="audio_18635499" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18635499_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
