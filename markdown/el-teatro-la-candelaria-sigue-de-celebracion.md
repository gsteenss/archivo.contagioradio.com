Title: El teatro La Candelaria sigue de Celebración
Date: 2016-08-11 14:47
Category: eventos
Tags: "Soma Mnemosine" teatro la Candelaria, Julio Mario Santodomingo, teatro en bogota
Slug: el-teatro-la-candelaria-sigue-de-celebracion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/soma-nmosine.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Teatro La Candelaria 

##### 10 Agos 2016 

La celebración por los 50 años del tradicional Teatro La Candelaria de Bogotá continúa. La siguiente parada de la agrupación, fundada por el maestro Santiago García, será el Teatro Mayor Julio Mario Santodomingo para presentar en doble función sus obras "Soma Mnemosine" y "Camilo".

<iframe src="https://www.youtube.com/embed/Svob1VDA3p4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La primera, es una propuesta de teatro performático nacido del trabajo de laboratorio en grupo, que parte de la indagación de la corporalidad tanto en estado de dolor como de júbilo, abordando como tema central el cuerpo personal y social, sometido a la  violencia  y refugiado en la fiesta como  espacio de resistencia. Se trata de una secuencia-cuerpo-persona-cuerpo-sociedad.

<iframe src="https://www.youtube.com/embed/H5DFe4RlFwo" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La segunda, es una combinación de teatro, elementos de performance, danza, video y música en vivo, para narrar las vivencias, pasiones, conflictos y convicciones que rodearon la vida del sacerdote Camilo Torres, encarnado en la piel de 12 actores y actrices diferentes, mostrando a las nuevas generaciones parte de la rica personalidad de uno de los impulsores de la sociología en Colombia.

Las presentaciones de "Soma Mnemosine" serán el 12 y el 13 agosto, mientras que "Camilo" el 19 y 20 del mismo mes, bajo la dirección de la maestra Patricia Ariza.
