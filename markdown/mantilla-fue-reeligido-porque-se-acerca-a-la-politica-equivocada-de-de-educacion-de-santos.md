Title: Reelección de Mantilla: "anti-democrática y lamentable"
Date: 2015-03-26 17:28
Author: CtgAdm
Category: Educación, Nacional
Tags: Congreso de la República, Derecho a la eduación en Colombia, educacion, gina parody, Ignacio Mantilla, Juan Manuel Santos, Mario Hernanadez, Ministerio de Educación, Universidad Nacional
Slug: mantilla-fue-reeligido-porque-se-acerca-a-la-politica-equivocada-de-de-educacion-de-santos
Status: published

##### Foto: [fotos.starmedia.com]

<iframe src="http://www.ivoox.com/player_ek_4268993_2_1.html?data=lZejmp6dd46ZmKiak5eJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcLi1c7ZzsaPqdSf08rSzsrLrcXjjMjcz9SPtsbX1dTfjcnJb83VjLqbjbPFp8rjz8bZj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mario Hernández, ex-candidato a la rectoría de la Universidad Nacional] 

<iframe src="http://www.ivoox.com/player_ek_4268999_2_1.html?data=lZejmp6dfY6ZmKiak5mJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbVxMjW0dPJt4zYxpDZw5DHs87pz87Rw8mPuc%2Fd18rf1c7YpdPdwpDh1MbXb9PZxtHSxcjNaaSnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Oscar Londoño, representante estudiantil de la sede de Bogotá de la Universidad Nacional] 

**“Quedó muy claro, que el gobierno ya había tomado la decisión de reelegir a Ignacio Mantilla como rector de la institución**, de hecho,  la ministra no estuvo en la presentación de los candidatos para la realización de la consulta” es la reacción del médico y profesor, Mario Hernández, quien había sido escogido por los estudiantes como rector de la Universidad Nacional, pero que ahora, se muestra indignado tras la decisión del Consejo Superior Universitario, que reeligió a Ignacio Mantilla.

“**La comunidad universitaria es la que debe tomar las decisiones, pero el gobierno de turno está imponiendo su visión**”, expresa el médico.

Para Hernández, Mantilla fue reelegido ya que era el candidato más cercano a la política de educación superior de Santos, una **política equivocada donde las universidades públicas no tiene autonomía y se rigen bajo un modelo de auto-sostenibilidad.**

“La propuesta del presidente santos es equivocada para la sociedad, lo que proponen es que los pobres entren mayoritariamente a obtener títulos técnicos y tecnológicos para que trabajen, mientras que unos pocos pobres pueden entrar a una universidad con créditos, y una pequeña franja entre a las universidades ricas, para ser ministros, eso reproduce la desigualdad en la sociedad colombiana”, expresa Hernández.

Ese mismo descontento lo expresan los estudiantes y otros actores de la universidad, quienes ya han empezado a movilizarse en rechazo a la elección del Consejo Superior. “Por segunda vez consecutiva el Consejo toma una decisión contraria  a la comunidad universitaria, se elige a quien ha contribuido a la crisis de la universidad, que tiene un déficit de más de \$130 mil millones, **es una decisión lamentable y antidemocrática**”, afirma Oscar Londoño, representante estudiantil de la sede de Bogotá de la institución.

Pese al descontento de la comunidad universitaria, los estamentos ya entraron en procesos de movilización y organización para lograr cambiar “las reglas de juego de la Universidad Nacional”, tanto Hernández como Londoño, aseguran que se va a entrar en un proceso de generación de un **congreso universitario** que permita hacer una transformación interna, con el **objetivo de construir democracia.**

Por otro lado, se planea un proceso de construcción colectiva con otras instituciones públicas para gestionar **una nueva Ley de educación superior, que sería tramitada en el Congreso de la República.** Esta, iría en contra del Plan Nacional de Desarrollo (2014-2018), que para el vocero de la Federación Médica Colombiana, se trata de plan donde “se sigue pensado que la educación es un asunto de mercado”.

Para Londoño, **en el PND no existe una medida de rescate financiero para las universidades públicas**, más bien, se plantea profundizar la privatización a través del “fortalecimiento del ICETEX, se promueven programas como ‘ser pilo paga’ a través de los cuales, se naturaliza que solo unos pocos pueden acceder a una educación superior”, argumenta el representante estudiantil.

Los estudiantes aseguran que la movilización apenas comienza. Los trabajadores iniciarían **asamblea permanente el próximo 13 de abril,** es decir que, como dice el universitario “se van a  agudizar los conflictos con la comunidad”, ya que tanto para Oscar como para el profesor Mario Hernández, esta decisión del Gobierno, es contraria a la supuesta vocación de paz, **"el modelo de educación actual no funciona para la paz”.**
