Title: Nombramiento de Jorge Tovar, una burla a la dignidad y memoria de las víctimas
Date: 2020-05-21 10:00
Author: AdminContagio
Category: Nacional
Tags: Jorge Tovar, memoria, MOVICE
Slug: nombramiento-de-jorge-tovar-una-burla-a-la-dignidad-y-memoria-de-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Jorge-Tovar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto:  Jorge Tovar Vélez

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la designación de **Jorge Rodrigo Tovar Vélez, hijo del paramilitar “Jorge 40”, como Director de Víctimas del Ministerio del Interior**, organizaciones sociales y víctimas del conflicto armado han señalado que la decisión es una burla a las víctimas, una muestra del negacionismo que se ha propiciado en otras instituciones y pone en duda la credibilidad del Gobierno en materia de Verdad, Justicia, Reparación y Garantías de No Repetición.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El abogado, Jorge Rodrigo Tovar Vélez, llega al cargo de coordinador del Grupo de Articulación Interna para la Política de Víctimas del Conflicto Armado, que debe promover y hacer seguimiento a la reparación de la población víctima de infracciones del Derecho Internacional Humanitario o de las violaciones graves y manifiestas a las normas internacionales de los Derechos Humanos, ocurridas con ocasión del conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, organizaciones como el [Movimiento de Víctimas de Crímenes de Estado (MOVICE)](https://twitter.com/Movicecol)consideran que dicho nombramiento es "una afrenta a la dignidad y a la memoria de las más de 21.000 de víctimas fatales" que según el Centro Nacional de Memoria Histórica ha dejado el paramilitarismo en Colombia y contra sus familiares quienes continúan esperando garantías de verdad, justicia y reparación integral por parte del Estado.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Entre 1996 y 2005, las Autodefensas Unidas de Colombia a través del Bloque Norte, comandado por "Jorge 40", cometieron 333 masacres en los departamentos de Atlántico, César, Magdalena y Guajira, con un total de 1.573 víctimas, y Aunque en la actualidad Rodrigo Tovar Pupo cumple una condena en Estados Unidos no fue acusado por dichos delitos sino por el de narcotráfico.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, víctimas del conflicto como Leyner Palacios, sobreviviente de la masacre de Bojayá, señalan que como una muestra de reconciliación previa, primero debería darse el reconocimiento de dichos delitos cometidos por el paramilitarismo que no han aflorado por lo que "la dignidad de las víctimas con ese nombramiento se ve atropellada de manera abrupta".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque las organizaciones han manifestado que conocen la labor de contribución que ha adelantado Tovar con víctimas del Estado y asumido un discurso de paz y reconciliación en foros, señalan que les preocupa la objetividad y conflicto de intereses que pueda tener al desempeñar su cargo. [(Le puede interesar: Organizaciones retiran archivos del Centro de Memoria ante políticas negacionistas del director)](https://archivo.contagioradio.com/organizaciones-retiran-archivos-del-centro-de-memoria-ante-politicas-negacionistas-del-director/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La responsabilidad de "Jorge 40" en el conflicto colombiano

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el Observatorio Nacional de Memoria y Conflicto del Centro Nacional de Memoria Histórica, los hechos victimizantes asignados al Bloque Norte de las Autodefensas Unidas de Colombia bajo el mando de Rodrigo Tovar Pupo alias “Jorge 40” suman cerca de 1’020.332 millones de agresiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Jurisdicción Especial para la Paz (JEP), ha expresado que “Jorge 40” tuvo la oportunidad de aportar a la verdad en el proceso de Justicia y Paz, sin embargo fue excluido del mismo por no contribuir con la verdad suficiente y los compromisos que adquirió al ingresar a dicho marco jurídico, pese a ello en medio de sus declaraciones **reconoció participar en más de 600 hechos victimizantes, incluidas masacres, desplazamientos y secuestros.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> . Los procesos de reconciliación no puede ser otra práctica de imposición, esta solo es posible con la garantía de los derechos de las víctimas y con propuestas emanadas de las mismas. - MOVICE

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Nombramiento de Jorge Tovar Se perpetua el negacionismo

<!-- /wp:heading -->

<!-- wp:paragraph -->

El nombramiento de Jorge Rodrigo Tovar Vélez, señalan desde sectores de oposición, se suma a otros hechos negacionistas que han surgido durante el gobierno Duque como la posición asumida por el Centro Nacional de Memoria Histórica dirigido por Dario Acevedo quien ha afirmado en ocasiones anteriores "que en Colombia no existió un conflicto armado, sino un ataque terrorista al Estado". [(Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino)](https://archivo.contagioradio.com/con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte , algunas organizaciones advierten que el nombramiento obedece a una jugada política que además de promover el negacionismo y asegurar el silenciamiento de parapolíticos y extraditables que podrían aportar al esclarecimiento de la verdad y de los responsables del conflicto. [(Le puede interesar: Centro de Memoria y Fedegan ¿una alianza para exonerar a responsables del conflicto?)](https://archivo.contagioradio.com/centro-de-memoria-y-fedegan-una-alianza-para-exonerar-a-responsables-del-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a estos hechos, organizaciones de DD.HH. incluidas el MOVICE, exigen el retiro de Jorge Rodrigo Tovar del cargo pues advierten que no pueden "ser cómplices de procesos de reconciliación que perpetuen el silencio, el olvido y la impunidad".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
