Title: Mesa Ecuménica lanza propuesta en respaldo a los acuerdos de paz
Date: 2016-10-10 15:36
Category: Nacional, Paz
Tags: acuerdos de paz, Mesa Ecuménica por la paz
Slug: mesa-ecumenica-lanza-propuesta-en-respaldo-a-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Mesa-Ecuménica-e1476131458378.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 de Octu 2016]

La Mesa Ecuménica por la paz expuso una propuesta de cinco puntos en donde **respaldan los acuerdos firmados entre las FARC-EP y el gobierno Nacional, evidencian la necesidad de establecer el Pacto Social con las organizaciones y el movimiento social e iniciar con una pedagogía que supere la polarización.**

Frente al primero punto argumentan que es necesario defender los acuerdos de la Habana para visibilizar y poner en el centro a las víctimas, de igual forma consideran que se pueden hacer [ejercicios de complementación de los acuerdos que no signifique dilatar un acuerdo definitivo de paz.](https://archivo.contagioradio.com/debemos-construir-una-paz-con-etica-que-reivindique-posibilidad-de-la-diferencia/)

Sobre el “Pacto Social”, piensan que este debe ser **la garantía de la reconciliación del país, hacía una etapa de transición que a su vez supere la polarización**. Un pacto que para la Mesa Ecuménica no puede ser entre élites sino que debe incluir a todos los sectores políticos y sociales colombianos.

En otro punto la propuesta habla del enfoque de género y de la necesidad de **dejar de hablar de ideología de género desde los sectores religiosos** que apoyaron esta premisa y abordarlo desde el enfoque de derechos humanos, es decir desde la inclusión y la equidad, que reconozca la participación de la mujer y la diversidad sexual.

Finalmente compartieron una serie de iniciativas que desarrollarán como el envío de una carta al presidente Santos pidiendo una participación directa en el diálogo y conformación del pacto social, referente a los grupos armados afirmaron que **harán parte de las vigilias en los campamentos de las FARC-EP, como una medida de acompañamiento y protección a los guerrilleros** concentrados en zonas seguras. De igual forma, la Mesa Ecuménica hará parte como organización civil, de la mesa de diálogos con el ELN que podría tener inició hoy.

“Es un momento histórico para que **las comunidades de fe nos unamos para apoyar no solamente los proceso de paz sino la reconciliación en el país**” afirmó Amparo Beltrán Acosta, miembro de  la mesa ecuménica por la paz.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
