Title: Grupo paramilitar “Los Caparros” decreta paro armado en el Bajo Cauca
Date: 2020-11-21 10:44
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Bajo Cauca, Los Caparros
Slug: grupo-paramilitar-los-caparros-decreta-paro-armado-en-el-bajo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Panfleto-Los-Caparros.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Los-Caparros-decretan-Paro-Armado-en-Bajo-Cauca.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El grupo paramilitar **“Los Caparros”** difundió un panfleto en el que advierte sobre el decreto de **un paro armado para la subregión del Bajo Cauca,** entre **el viernes 20 de noviembre a las 6 de la tarde y el lunes 23 de noviembre a las 6 de la mañana.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto como retaliación a la reciente baja en combate de **Emiliano Alcides Osorio Macea,** **alias “Caín” o “Pilatos”**, máximo líder de **“Los Caparros”**, en medio de combates con el Ejército Nacional. (Lea también: [En el Bajo Cauca Antioqueño la violencia se está tomando el poder](https://archivo.contagioradio.com/bajo-cauca-antioqueno-violencia-gobierno/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":93102,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Panfleto-Los-Caparros.jpg){.wp-image-93102}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

Según información difundida por algunos medios de comunicación el Mayor General Juan Carlos Ramírez, comandante de la Séptima División del Ejército afirmó que conocían de la existencia del panfleto y se encontraban en tareas de verificación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Oscar Yesid Zapata, vocero del Proceso Social de Garantías -[PSG](https://twitter.com/GarantiasPSG)-; señaló que era bastante factible que el panfleto fuera autentico y que se hubiese difundido por parte de “Los Caparros”, **ya que estas acciones de amedrentamiento a la comunidad son constantes por parte de este grupo y habitualmente se agudizan como represalia contra la comunidad, cuando altos mandos de estos grupos ilegales son abatidos.  **

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Nosotros podemos tranquilamente decir que el Bajo Cauca quien manda, quien ejerce el poder, el control y el uso de las armas son los grupos paramilitares”
>
> <cite>Oscar Yesid Zapata, vocero del PSG</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Oscar Zapata, también señaló que la población está muy atemorizada y que el miedo y zozobra se han apoderado de las comunidades quienes están ante una sensación de **“*total orfandad por parte del Estado*”.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “La comunidad en general ha asumido un silenciamiento por el miedo; porque han entendido que lamentablemente, si quieren sobrevivir en su territorio tienen que recurrir a eso”.
>
> <cite>Oscar Yesid Zapata, vocero del PSG</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El vocero del PSG también expresó que el Bajo Cauca es una región totalmente desatendida por el Estado, cuya única presencia se limita a movilizar efectivos de la Fuerza Pública, que a juzgar por la facilidad con la que operan los grupos paramilitares en la zona, pareciese haber consolidado nexos y alianzas que son los que permiten la movilidad de estos grupos. (Le puede interesar: [«Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Responsabilizamos a Iván Duque, como cabeza de este Gobierno, de todo lo que pueda ocurrir y de lo que ya está ocurriendo en el Bajo Cauca porque ha sido advertido de toda esta situación en diversos escenarios”
>
> <cite>Oscar Yesid Zapata, vocero del PSG</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Sobre estas problemáticas de violencia que se han perpetuado por años y aún persisten, la Comisión de la Verdad desarrolló justamente el día de hoy, un “Diálogo para la No Repetición” en la región del Bajo Cauca.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComisionVerdadC/status/1329590284418736135","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComisionVerdadC/status/1329590284418736135

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
