Title: Las víctimas no olvidan los horrores de la operación Orion
Date: 2016-10-18 15:52
Category: Sin Olvido
Tags: Antioquia, Comuna 13, ejercito, Medellin, Mujeres Caminando por la Verdad, operación orion, paramilitares
Slug: las-victimas-no-olvidan-los-horrores-de-la-operacion-orion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/jesus-abad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Jesús Abad Colorado 

##### [18 Oct 2016] 

[El 16 de octubre se cumplieron **14 años de la Operación Orión**, intervención armada adelantada en la **Comuna 13 de Medellín**, con la excusa de recuperar por parte del Estado territorios controlados por las llamadas milicias urbanas de las FARC-EP, el ELN y los Comandos Armados del Pueblo CAP.]

[Contrario a ese propósito, y con el antecedente de la llamada]***Operación Mariscal, ***ejecutada[ el 21 de mayo del 2002 por la incursión de 900 efectivos militares que dejaría como saldo 9 civiles muertos y 37 heridos; la]***Operación Orión*** se convertia en un nuevo episodio fatal para el[ accionar del Estado y su articulación con las fuerzas ilegales de las AUC, particularmente del Bloque Cacique Nutibara.]

[**Más de un millar de uniformados del ejército, la policía, el DAS y del CTI fueron desplegados por la zona**, restringiendo el ingreso a los medios de comunicación, organismos humanitarios y la evacuación de la población civil. De acuerdo al informe presentado por el Centro de Investigación y Educación Popular, CINEP, barrios como El Corazón, 20 de Julio, Las Independencias, El Salado, Nuevos Conquistadores y Belencito fueron los más afectados.]

[La intervención, que oficialmente estaba progamada para 4 días y que terminó prolongandose durante todo el mes de noviembre e inicios de diciembre, **facilitó el posicionamiento de las AUC en el territorio**, afectando directamente a la población civil de manera indiscriminada, vulnerando en varias oportunidades sus Derechos Humanos.]

[Según el informe "**Comuna 13: Una política de Agresión Estatal**" del CINEP, se estima que en la operación **355 personas fueron detenidas de manera arbitraria; 170 fueron judicializadas por cargos como terrorismo, secuestro, concierto para delinquir**; y acciones que sirvieron como estrategia en las labores de “reconocimiento” para la posterior **ejecución de civiles por parte de integrantes paramilitares**.  ]

[En la conmemoración de los 14 años, la organización **Mujeres Caminando Por La Verdad**, continuan denunciando la **falta de voluntad por parte del Estado en el cumplimiento de sus derechos como víctimas**, el cierre absoluto de la escombrerera y la arenera (lugares en los que se encuentran los cuerpos de las víctimas) y la implementación por parte de la Fiscalía General de la Nación del Plan Integral de Búsqueda, entregado en julio de 2015.]

[Durante la Jornada de Memoria y Resistencia, que tuvo lugar el 14, 15 y 16 de octubre, se realizó una Audiencia Pública de Rendición de Cuentas sobre los “Avances en el reconocimiento de los derechos de las víctimas de la comuna 13”, la **caravana de la memoria de las Mujeres Caminando por la Verdad**, la Feria Agroecológica e intercambio de experiencias y la acción colectiva de **Cuerpos Gramaticales**, una denuncia de las consecuencias de la guerra a través de la palabra, la música y el arte.]
