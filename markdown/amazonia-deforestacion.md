Title: Amazonía habria perdido 19.600 millones de árboles en los últimos meses
Date: 2020-06-09 22:09
Author: CtgAdm
Category: Actualidad, Ambiente
Tags: amazonas, deforestación, violencia
Slug: amazonia-deforestacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/amazonas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 9 de junio la Fundación para la Conservación y el Desarrollo Sostenible de la Amazonía, señaló que la deforestación en este departamento, supera las 75.031 hectáreas taladas, agregando que **tan solo en enero y abril del 2020 se tumbó la misma extensión que en todo el año pasado**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Antonio Nobre investigador del Instituto Nacional de Pesquisas da Amazonía** (INPA), señaló que el Amazonas bombea 20,000 millones de toneladas de agua cada día hacia la atmósfera, regulando así las lluvias en la gran mayoría de Suramérica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sobre esta crítica situación la docente de la **Universidad de la Amazonía, Mercedes Mejía Leudo**, señaló que es preocupante que año tras año y pese a los esfuerzos de organizaciones, y la disposición de recursos donados por la comunidad internacional, "*la deforestación en el Amazonas aumente en vez de disminuir*".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En la Amazonía gobierna "la ley del más fuerte"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Asímismo señaló que existen otros factores que ponen en riesgo este ecosistema, "***la ilegalidad en los territorios cada vez es más amplia y la presencia estatal escasa, por no ser pesimista y decir que nula en muchos territorios"***, situación que según Leudo, amplia la brecha de lo que podría llamarse la ley del más fuerte.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Resaltando como ejemplo, "las burbujas contra la deforestación, una estrategia del Ejército en sinergia con diferentes instituciones gubernamentales para detener la tala, y que a la fecha no ha disminuido los números".

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Al ver ese tipo de acciones uno se pregunta, **¿si todos los ciudadanos estamos confinados, entonces cómo es posible que sigan culpando al ciudadano de a pie de estas deforestaciones masivas?** ¿quién es el verdadero responsable?*"

<!-- /wp:quote -->

<!-- wp:paragraph -->

Sobre la gestión de las Fuerzas Militares en los territorios, Leudo señaló que es poco lo que se puede esperar de estas iniciativas, *"cuando estos grupos pensandos para detener el extractivismo están dejando de lado su función principal y se dedican exclusivamente a la erradicación forzada, mientras millones de especies de animales y árboles están siendo arrazados"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo agregó,que los intereses concretos en el territorio , se resumen en la cantidad de recursos que se encuentran en la Amazonía, *"agua, [minerales](https://www.justiciaypazcolombia.com/solidaridad-con-jerico-y-tamesis-en-riesgo-por-megamineria/), oxígeno y todo lo que se quiere para el desarrollo de la vida, esta en los ojos de quienes tienen como único objetivo lucrarse a costa de la [vida](https://archivo.contagioradio.com/continua-el-genocidio-silencioso-del-pueblo-embera-chami-de-caldas/)"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para la docente, *"el mismo Gobierno aprueba y respalda todo el proceso de explotación, hoy son proyectos ganaderos de la mano de préstamos de bancos internacionales, **desafortunadamente tenemos un Gobierno corporativo que une sus decisiones a lo que las corporaciones quieran**"*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Todo lo relacionado al extractivismo en el Amazonas sigue estando a la orden del día"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por su parte **Andrés Echeverry, integrante de [Censat](https://archivo.contagioradio.com/reflexiones-ambientales-en-tiempos-de-cuarentena-iii-agua-escasez-o-desigualdad/)Agua Viva** , señaló que existen generalidades alrededor del tema de la deforestación, así como del aumento de los puntos de calor producto de las incendios intencionales, *"que han subido de manera ridícula en tan corto tiempo".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y explica la gravedad de la deforestación, diciendo que ésta es de tal magnitud que **equivale a 5 veces la ciudad de Medellín incluyendo sus ardedores rurales**, *"Colombia ha deforestado 1.7 millones de hectáreas, equivalentes a 19.600 millones de árboles, solo en los últimos meses; cada hectárea [deforestada](https://archivo.contagioradio.com/las-practicas-turisticas-que-mas-danan-los-ecosistemas-segun-pueblos-indigenas-de-la-sierra/) puede incluir cerca de 6 mil especies distintas"*.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Es un mito vendido que la deforestación esté ligada a los cultivos de uso ilícito

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para Echeverry este tipo de acciones, como la deforestación justificada en los cultivos de uso ilícito, *"lo único que ocultan es otro tipo de acciones como la ganadería y el acaparamiento de tierras en el país, **lo que aumenta la desigualdad y la criminalización a las poblaciones; dando tratamiento de guerra a la deforestación**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y señaló que en Colombia la lucha contra las drogas se confunde con la lucha contra la deforestación; y finalmente destacó de que es importante analizar a quiénes son los verdaderos deforestadores en Colombia, *"no tengo duda que los campesinos ubicados en zonas protegidas, los indígenas que habitan las selvas y las comunidades afro que habitan los litorales, no son los culpables de este circulo vicioso de intereses en el Amazonas".*

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
