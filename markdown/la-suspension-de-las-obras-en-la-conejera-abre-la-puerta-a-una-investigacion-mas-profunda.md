Title: Suspenden obras de construcción en el humedal "La Conejera"
Date: 2015-03-10 18:07
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, Bogotá, Humedal, La Conejera, Roberto Sáenz, Suba
Slug: la-suspension-de-las-obras-en-la-conejera-abre-la-puerta-a-una-investigacion-mas-profunda
Status: published

##### Foto: [humedalesbogota.com]

<iframe src="http://www.ivoox.com/player_ek_4194970_2_1.html?data=lZamlp6bdI6ZmKiakpmJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmrcaY1drXtMbi1M6SpZiJhpTijMnSjdHFt4zjw9fO1ZDJsozAwpCw0dPJrsbmwoqfpZDFptPZjNHOjdXZcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Roberto Sáenz, Concejal de Bogotá] 

La decisión del Tribunal Administrativo de Cundinamarca, sobre la suspensión provisional parcial de las obras de la Constructora Praga Servicios Inmobiliarios junto al humedal La Conejera, en la localidad de Suba **“***abre las puertas a que se genere una discusión mucho más profunda  sobre los  límites del humedal y los procedimientos ilegales que antecedieron al otorgamiento de la licencia***”,** afirma el concejal de Bogotá, Roberto Sáenz.

Por daños al medio ambiente y la acción popular presentada por la Personería de Bogotá, el **Tribunal suspendió las obras en la Conejera**, que llevaban el 4% construido. “*La comunidad ha demostrado que hubo vicios de forma y fondo al dar la licencia***”**, dijo el concejal.

De acuerdo a Sáenz, la Secretaria de Ambiente ya firmó un plan de manejo ambiental que afectaría las construcciones de ese sector, lo que haría que los constructores deban modificar la licencia de construcción, y además la Curaduría deba volver a estudiar el proyecto.

Con este fallo, se demuestra que **el predio tiene una evidente presencia de biodiversidad protegida históricamente por la comunidad del sector**, por lo que los defensores de La Conejera, han presentado más documentos que permitan ampliar la investigación sobre los mapas que autorizaron la construcción ilegal.

Finalmente, Roberto Sáenz, resaltó que “*la comunidad cuenta con varios argumentos jurídicos sociales y ambientales para que se suspendan permanentemente las obras”,* además, *“la Contraloría ha insistido en que se tiene una discusión muy de fondo con respecto a la tradición historia del humedal*”.
