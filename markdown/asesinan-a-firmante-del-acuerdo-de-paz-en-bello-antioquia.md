Title: Asesinan a firmante del Acuerdo de Paz en Bello, Antioquia
Date: 2020-05-09 13:40
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #AcuerdoDePaz, #Excombatiente, #Exprisioneropolítico, #Farc, #WilderMarin
Slug: asesinan-a-firmante-del-acuerdo-de-paz-en-bello-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Wilder-Marin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 8 de mayo fue encontrado el cuerpo sin vida de **Wilder Daniel Marín, ex prisionero político, gestor de paz y firmante del Acuerdo de La Habana**. Los hechos ocurrieron en el muncipio de Bello, Antioquia cuando la Policía confirmó la muerte de una persona que había sido incinerada y envuelta en un colchón.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Alejandro Pérez, miembro de la dirección del Partido FARC en Antioquia, la última información que se tuvo de Marín, es que salió el pasado 8 de mayo de su vivienda. Posteriormente, e**n vista de que no aparecía, su madre activa el protocolo de búsqueda**. (Le puede interesar: ["Asesinatos a la paz"](https://partidofarc.com.co/farc/2020/04/23/asesinatos-a-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Wilder es el 27 firmante del Acuerdo de Paz asesinado en Antioquia
------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con información de sus allegados Marín, de 22 años, se encontraba estudiando contaduría en el SENA. De igual forma, en su tránsito al proceso de reincorporación hacía parte de la Cooperativa Multiactiva Tejiendo Paz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Marín también se destacó como gestor de paz en el proceso de diálogo entre la entonces guerrilla de las FARC y el gobierno de Santos. El Partido FARC, en un comunicado de prensa, asegura que con **Wilder son 27 los firmantes de la paz asesinados en Antioquia.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1258968283195334656","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1258968283195334656

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En el mismo documento, exigieron al presidente Iván Duque que frene el exterminio en contra de las y los excombatientes. En consecuencia recordaron que en Colombia desde la firma del Acuerdo, han asesinado a 196 personas. (Le puede interesar:[¡Que pare la matanza de reincorporados de FARC!](https://archivo.contagioradio.com/que-pare-la-matanza-de-reincorporados-de-farc/))

<!-- /wp:paragraph -->
