Title: ♫ Podcast "Operación Cierre"
Date: 2019-07-29 12:16
Author: CtgAdm
Category: Expreso Libertad, Uncategorized
Tags: Expreso Libertad
Slug: podcast-operacion-cierre-expreso-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Operación-Cierre-Radionovela-Ver-2-1.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Formato-Podcast-_-Banner.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Facebook-Banner.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Le invitamos a que escuche "Operación Cierre", radionovela escrita y producida por Contagio Radio, en la que se relata la historia de 5 mujeres, de diferentes sectores sociales, que se encuentran en la Cárcel La Tramacúa, uno de los centros de reclusión más violentos en Colombia.

<div>

Juntas deciden poner fin a las múltiples violaciones de derechos humanos de las que son víctimas y emprender un camino por lograr el cierre de la Torre 9 de esta cárcel.

</div>

<div>

</div>

<div>

</div>

<iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/656317952&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true" width="100%" height="300" frameborder="no" scrolling="no"></iframe>
