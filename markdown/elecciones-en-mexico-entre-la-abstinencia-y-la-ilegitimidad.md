Title: Elecciones en México: entre la abstención y la ilegitimidad
Date: 2015-06-09 15:12
Category: El mundo, Política
Tags: Ayotzinapa, Derechos Humanos, elecciones, Enrique Peña Nieto, gilberto lopez, guerrero, mexico
Slug: elecciones-en-mexico-entre-la-abstinencia-y-la-ilegitimidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Captura-de-pantalla-2015-06-09-a-las-14.55.04.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: HiperTextual 

<iframe src="http://www.ivoox.com/player_ek_4617380_2_1.html?data=lZuemZicdI6ZmKiak5iJd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic3ZxMjW0dPJt4zZz5C6h6iXaaKt2c7Q0YqWh4zZz9nfx5DQpYzVw9jhy9PJssTdwpDmjdHFb8rgxszW1pKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Gilberto Lopez, Comisión Ética y Política de México] 

###### [9 jun 2015] 

El pasado fin de semana se llevaron a cabo elecciones generales en México, para definir la conformación del parlamento y las gobernaciones. Sin embargo, la jornada que debió registrarse como una fiesta de la democracia, no pudo evitar ser un reflejo de la tensa calma que vive el país centroamericano.

La abstinencia fue la más alta registrada en la historia mexicana, alcanzando un 55%. También dejó huella la campaña por anular los votos, marcando en el trajeton frases contra el gobierno de Enrique Peña Nieto, o los partidos que postularon candidaturas. En Oaxaca, Chiapas y Guerrero, las protestas ciudadanas evitaron la instalación de hasta un 50% de las mesas electorales, y dejaron como saldo al rededor de 100 personas detenidas.

En Tixtla, Guerrero, las acciones de la población casi lograron anular las elecciones. La presidenta del Instituto de Participación Electoral de Guerrero, IEPC, Marisela Reyes, afirmó que "la quema de más del 20% de las casillas electorales anula todo el proceso". Sin embargo, el gobierno nacional emitió un parte de tranquilidad, e ignorando las manifestaciones de mexicanos y mexicanas, militarizando y reprimiendo las expresiones de desacuerdo, aseguró que "el 99,5% de las casillas instaladas están funcionando correctamente". El presidente Enrique Peña Nieto hizo un llamado a que se "reafirme la democracia".

\[caption id="attachment\_9900" align="alignright" width="352"\][![Foto: Desinformemonos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Mexico.png){.wp-image-9900 width="352" height="191"}](https://archivo.contagioradio.com/elecciones-en-mexico-entre-la-abstinencia-y-la-ilegitimidad/mexico-2/) Foto: Desinformemonos\[/caption\]

Para Gilberto Lopez, de Comisión Ética y Política de México, ese era precisamente el objetivo del partido de gobierno, el PRI. "La clase política logró realizar su ritual electoral, y las cosas en México siguen exactamente igual que antes de las elecciones; Desapariciones forzadas, más de 150 mil muertos y 1/2 millón de desplazados internos y externos, la vida en un país neoliberal a ultranza y sometido a los Estados Unidos que cada vez se hace sentir más a través de la versión mexicana del Plan Colombia, el Plan Mérida".

Es por este motivo que, para Lopez, lo ocurrido el fin de semana en México fue "sin retórica alguna, es una farsa electoral", en la que el partido de derecha adquirió el 59% de los votos. "Mexico está sufriendo una de las más grandes crisis de legitimidad y credibilidad en los partidos políticos, y el proceso fue, por decirlo menos, patético".

El llamado que realiza la Comisión Ética y Política de México es lo importante que podría ser la construcción de un poder popular desde abajo, desde la posibilidad de tener formas organizativas horizontales que no dependan de líderes providenciales ni de partidos que se ven acogidos por el poder. "Todo esto para nosotros significa la acción política del día a día, y no de un día; Si se trata aquí de luchar toda la vida por la construcción de un poder distinto, de un poder popular diferente, como decían los zapatistas, que mande obedeciendo; eso que ya se le olvidó a la izquierda institucionalizada", concluyó.
