Title: Reconocimiento al CPDH por sus Escuelas de Paz en clave de justicia transicional
Date: 2015-08-28 12:46
Category: DDHH, Nacional
Tags: cpdh, justicia transicional, justicia transicional en colombia, paz
Slug: reconocimiento-al-cpdh-por-sus-escuelas-de-paz-en-clave-de-justicia-transicional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/CPDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo CPDH ] 

<iframe src="http://www.ivoox.com/player_ek_7661173_2_1.html?data=mJujk5abd46ZmKiak5iJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbX0NPcxc7Rrcbi1dSYw9GPp9HYyZDd0dePt9bnjMrgxdrJsMLnjMnSjdXFvozZz5DQzsbaqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Diego Martínez, director del CPDH] 

###### [26 Ago 2015] 

[En el marco del Día Internacional de la Asistencia Humanitaria, la Oficina de la ONU para la Coordinación de Asuntos Humanitarios, OCHA, otorgó un premio al Comité Permanente por la Defensa de los Derechos Humanos, CPDH, en **reconocimiento a las Escuelas de Paz** que esta organización impulsa en regiones como Putumayo, Cauca, Neiva y Bogotá.]

[Diego Martínez, director del CPDH, afirma que las **Escuelas de Paz son experiencias cuyo énfasis es la formación en torno al derecho a la justicia comunitaria**, como un esfuerzo que busca transformar el imaginario social que conecta temas de justicia con lo estrictamente penal, en estos momentos de álgido debate sobre justicia transicional en la Mesa de Conversaciones de La Habana.]

[Martínez asegura que **“Si hay algo central en los procesos de diálogo es el reencuentro, la restauración y la reparación a la victimas en clave de justicia”**, por lo que es necesario pensar formulas creativas que desde prácticas de justicia comunitaria, restaurativa y reparativa, adelantadas por pueblos indígenas, campesinos y afro descendientes, permitan consolidar un sistema integral de justicia, verdad, reparación y garantías de no repetición para la víctimas.]

[Todavía son muchas las preguntas de las comunidades respecto al sistema de justicia que se construirá para la transición a la democracia, en todo caso, Martínez insiste en que en las experiencias de justicia comunitaria de las Escuelas de Paz se pueden hallar las **claves para un modelo de reconciliación y reencuentro que posibilite la democratización en los territorios** en los que los tejidos sociales se han afectado por causa de la guerra. ]
