Title: General Villegas recognizes the murder of Dimar Torres while the Ministry of Defense hides the truth
Date: 2019-04-29 12:36
Author: CtgAdm
Category: English
Tags: Catatumbo, Dimar Torres, Extrajudicial execution
Slug: general-villegas-recognizes-the-murder-of-dimar-torres-while-the-ministry-of-defense-hides-the-truth
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Diseño-sin-título-e1556470066779.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[General **Diego Villegas**, head of the Vulcano Task Force present in Catatumbo assumed responsibility for the former]{lang="EN"}’[s Farc combatant **Dimar Torres** assassination. Torres was in the process of reincorporation and while General Villegas asked the facts would be investigated **the Minister of Defense** instead, tried to cover up the fact stating that Dimar died in the middle of a struggle.]{lang="EN"}

[Villegas declared:]{lang="EN"}“[**This kind of unpleasant events should not happen, I'm not the one that could officially state what happened there but I'm not calm with it nor will I never let this injustice go unpunished on my watch. Everyone has to respond for what they do** ".]{lang="EN"}

[Despite that, we have to bear in mind that who is asking community for forgiveness is also the one investigated for an alleged extrajudicial execution.]{lang="EN"}

> "No mataron a cualquier civil; mataron a un miembro de la comunidad, y lo mataron miembros de las fuerzas armadas", Brigadier General Diego Luis Villegas Muñoz, Fuerza de Tarea Vulcano, sobre homicidio de Dimar Torres. (Cortesía Periodismo Joven El Tarra). [pic.twitter.com/NIEqetSwYA](https://t.co/NIEqetSwYA)
>
> — Helena Sánchez (@helenasanchezt) [28 de abril de 2019](https://twitter.com/helenasanchezt/status/1122330922924290048?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**[Peace commission in Catatumbo]{lang="EN"}**

[Members of the Senate Peace Commission also have visited the Catatumbo region particularly the community of Campo Alegre. After its tour, the Commission concluded that the death of Dimar was an extrajudicial execution caused by a member of the Army. The latter was in ]{lang="EN"}“[**non-missional action, not ordered and made illegally**". The evidences suggest that there was an intention to conceal and remove the corpse with the complicity of third parties.]{lang="EN"}

> Luego de su visita ayer a la zona donde fue asesinado Dimar Torres, exmiliciano de Farc, [@ComisiondePaz](https://twitter.com/ComisiondePaz?ref_src=twsrc%5Etfw) concluye que se trata de ejecución extrajudicial, intento de desaparición, y crimen contra el Acuerdo y el proceso de paz. [pic.twitter.com/cYFWK6Ypkn](https://t.co/cYFWK6Ypkn)
>
> — Iván Cepeda Castro (@IvanCepedaCast) [28 de abril de 2019](https://twitter.com/IvanCepedaCast/status/1122476476492845058?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
["The murder of Dimar Torres by the hands of the National Army constitutes a State crime against peace. The public prosecutor]{lang="EN"}’[s office must promptly establish the intention and material criminal responsibilities of these very serious acts against the peace of Colombia," declared **Senator Antonio Sanguino**. ]{lang="EN"}

[The investigators indicated that the versions about rape or mutilation are still under probe and asked the coroner to deliver the official report on the case as soon as possible, likewise to verify whether or not there are other graves with human remains in the zone.]{lang="EN"}
