Title: "Colombia si cuenta con los recursos para financiar la paz": Libardo Sarmiento
Date: 2016-07-18 14:01
Category: Entrevistas, Paz
Tags: FARC, Gobierno, proceso de paz
Slug: colombia-si-cuenta-con-los-recursos-para-financiar-la-paz-libardo-sarmiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/paz-e1468867771732.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corp. Humanas 

###### [18 Jul 2016]

En total, la paz en Colombia demanda **la inversión de 61 mil millones de dólares, de los cuales 44 mil millones se deben invertir en la implementación de los acuerdos y 17 mil millones para la reparación de las más de 8 millones de víctimas** que ha dejado el conflicto armado, explica Libardo Sarmiento, analista económico quien asegura que el país si cuenta con los recursos para financiar el posconflcito, así como los tuvo para aumentar el sueldo de los congresistas un 8%.

“Sino hubiera recursos no les hubieran subido el salario a los congresistas que hoy ganan 29 millones mensuales, además no se trata de una cifra muy alta, lo que se necesita es voluntad política y el compromiso de la comunidad internacional”, dice Sarmiento.

Esos  61 mil millones de dólares, **no deben  ser financiados con mayores impuestos a la clase media colombiana o con la profundización del modelo extractivista**, como se ha planteado desde el Ministro de Minas y Energía señalando que el fracking es necesario para la cubrir los costos del posconflicto.

De acuerdo con el analista económico, **son todos los sectores políticos y empresariales los que deben aportar para la paz porque estos mismos han contribuido con la guerra,** así mismo se debe tener en cuenta el aporte con el Plan Paz Colombia, y el que se hace desde el Fondo Fiduciario para el Posconflicto de Naciones Unidas, el Fondo Fiduciario de la Unión Europea y otros fondos como los del Banco Mundial que han anunciado su compromiso de la construcción de la paz del país.

Para Libardo Sarmiento, se trata entonces de un proceso de reconstrucción que podría tardar unos 10 años, y en los primeros 5, será cuando deba realizarse el mayor esfuerzo para lograr obtener los primeros 3 mil millones de dólares para el **Fondo Colombia en Paz que cuenta con cinco planes de financiación internacional.**

**"Resolver el problema económico es de voluntad política y de fuerza popular, todo concluido en una Asamblea Nacional Constituyente”**, dice el analista, quien indica que es esencial que se combata la corrupción, “un cáncer en toda la administración pública y la cultura colombiana que debe ser atacado y erradicado del país”, así mismo, “se debe acabar con los gastos improductivos del Estado, debe haber una reforma tributaria que toque los grandes capitales, y se debe fortalecer la veeduría ciudadana, por lo cual la Contraloría tendrá un papel fundamental".

<iframe src="http://co.ivoox.com/es/player_ej_12261851_2_1.html?data=kpefmJaceZKhhpywj5mWaZS1kp2ah5yncZOhhpywj5WRaZi3jpWah5ynca3dw8bfxtSPl8Lmzs7S0NnTaZO3jMbbw9HNt9XVjMrQ0dOJh5SZo5jay8jTcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
