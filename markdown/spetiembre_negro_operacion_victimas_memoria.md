Title: Víctimas conmemoran los 20 años de la operación "Septiembre Negro"
Date: 2017-12-08 15:47
Category: DDHH, Nacional
Tags: Curvarado, paramilitares, septiembre negro
Slug: spetiembre_negro_operacion_victimas_memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/cacarica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: Contagio Radio] 

###### [8 Dic 2017] 

Del 8 al 10 de diciembre las comunidades víctimas de la operación “Septiembre Negro” realizarán el Festival de la Memoria y la Verdad, un escenario de encuentro para reafirmar sus derechos, la dignidad y la necesidad de construcción de paz, **luego de 20 años de que se diera el masivo desplazamiento,** producto de más de 143 asesinatos y desapariciones forzadas y del manto de impunidad que hay sobre estos hechos.

En Septiembre de 1996 inició la operación “Septiembre Negro”, perpetrada por paramilitares y militares en los departamentos de Chocó y Antioquia, que provocó el desplazamiento de un número aún desconocido de comunidades y de acuerdo con la información recolectada por la **Comisión Intereclesial de Justicia y Paz, el asesinato y la desaparición de 143 personas.**

Los territorios que recorrió esta macabra incursión fueron las cuencas del Curvaradó y Jiguamiandó, en el municipio del Carmen del Daríen - Chocó, Piedeguita y Mancilla, en el municipio de Río Sucio -Chocó, Larga Tumarado, en los municipios de Turbo y Río Sucio y en Dabeiba. Sin embargo, pese al dolor y el miedo, **las familias han ido retornando a sus territorios, previamente ocupados por monocultivos de palma, plátano y ganaderia** extensiva que llegaron detrás de los paramilitares.

Este fin de semana se espera que participen más de   200 personas de diferentes territorios y departamentos del país que estarán en espacios de encuentro en este Festival de la Memoria y la Verdad que recorrerá 2 zonas humanitarias: Nueva Esperanza, en Jiguamiandó, Las Camelias, en Curvaradó y que finalizará con una marcha de antorchas hasta Brisas.

El Festival, también contará con **escenarios culturales, muestras artísticas, conversatorios sobre la implementación** de los Acuerdos de Paz y el reconocimiento del territorio en donde quedarán ubicadas dos sedes de la Universidad de Paz.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
