Title: Miguel Ángel Beltrán y su defensa del pensamiento crítico
Date: 2019-03-26 11:59
Category: Expreso Libertad
Tags: pensamiento crítico, presos politicos, Profesor Miguel ángel Beltrán
Slug: miguel-angel-beltran-pensamiento-critico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/miguel-angel-beltran-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 19 Mar 2019 

En este programa del Expreso Libertad, **Miguel Ángel Beltrán**, catedrático de sociología de la Universidad Nacional y **víctima de un falso positivo judicial**, habló de la importancia de construir una academia crítica que explore las causas del conflicto armado y sus responsables.

Beltrán fue capturado en el año 2009, en México, cuando se encontraba realizando sus estudios posdoctorales. Allí el gobierno de ese país, según el profesor, lo detuvo cuando se encontraba haciendo una diligencia, **esposándolo, cubriendole el rostro y más tarde, tras haber sido torturado**, se encontraba en la Base Militar de CATAM en Colombia.

Además, habló de sus vivencias siendo recluso en la Cárcel La Modelo, donde al igual que la población reclusa **fue víctima de violaciones a sus derechos, y de tratos crueles e inhumanos**.

<iframe id="audio_33739951" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33739951_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
