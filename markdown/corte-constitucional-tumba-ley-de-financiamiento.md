Title: Corte Constitucional tumba Ley de Financiamiento
Date: 2019-10-16 18:57
Author: CtgAdm
Category: Economía, Política
Slug: corte-constitucional-tumba-ley-de-financiamiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/23_08_2015corte_constitucional-e1473979535347.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corte Constitucional] 

Con seis votos a favor y tres en contra, este miércoles la Corte Constitucional tumbó por completo la Ley de Financiamiento impulsada por el Gobierno Duque que buscaba recaudar 7.1 billones en impuestos, para el Presupuesto General de la Nación del 2019 y que además según advirtieron algunos sectores, otorgaría exenciones a grandes empresas. Esta decisión se tomó al considerar que durante su trámite en el Congreso hubo errores que la hicieron inconstitucional.

Aunque la Corte tomo esta decisión, a su vez resaltó que la Ley de Financiamiento se caerá a partir del 2020, es decir, tendrá validez durante los meses restantes del 2019 hasta enero del próximo año. [(Le puede interesar: Carrasquilla se equivocó de fórmula para reducir el desempleo](https://archivo.contagioradio.com/carrasquilla-equivoco-reducir-desempleo/))

A consideración de la mayoría de los magistrados, cuando la Cámara de Representantes votó el texto de la ley, el pasado 19 de diciembre del 2018, no se conocían en su totalidad las modificaciones que había realizado el Senado la noche anterior, violando así principios claves de una ley como el de publicidad, consecutividad y deliberación. [(Lea también: Ley de Financiamiento, un regalo de navidad para los empresarios)](https://archivo.contagioradio.com/ley-de-financiamiento-un-regalo-de-navidad-para-los-empresarios/)

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
