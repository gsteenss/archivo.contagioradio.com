Title: Inicia fase pública de conversaciones de paz con ELN
Date: 2016-03-30 11:12
Category: Nacional, Paz
Tags: Caracas, ELN, Gobierno Nacional, paz, proceso de paz
Slug: se-anuncia-inicio-fase-publica-de-conversaciones-de-paz-con-guerrilla-del-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/conversacion-eln-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Notimerica] 

###### [30 Mar 2016] 

Después de [[tres años de contactos entre el gobierno y la guerrilla del ELN](https://archivo.contagioradio.com/los-momentos-cruciales-de-la-fase-exploratoria-eln-gobierno/)], hoy en Caracas, Venezuela, se anunció el inicio de la fase pública de la mesa de conversaciones de paz con esa guerrilla.

De acuerdo con la información que se ha conocido, seis serían los países garantes: **Cuba, Noruega, Chile, Venezuela, Brasil y Ecuador.** Además, la [[agenda de negociación](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/)] estaría conformada por seis puntos: participación de la sociedad; democracia para la paz; víctimas; transformaciones para la paz; seguridad para la paz y la dejación de las armas; y garantías para el ejercicio de la acción política.

Por su parte, diferentes sectores de la sociedad colombiana, celebran este anuncio. La represente a la Cámara por el Partido Verde, Angela María Robledo, asegura que "Si se acaba la guerra gana Colombia", agregando que “**Con la paz habrá más educación, mayor apoyo al campo y oportunidades para las nuevas generaciones”.** Además, la representante ve muy positivo que en estas negociaciones **la sociedad civil tendría una participación mucho más amplia,** como de los puntos que ha propuesto la guerrilla.

<iframe src="http://co.ivoox.com/es/player_ek_10998849_2_1.html?data=kpWmm52ceJqhhpywj5WcaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncbPZ0dfS1crSuMLi1cqYo9PLqc3VjLfcxNHJqNCf0NXW0MaPt9DW08qYxs6Jh5SZopbZ0czTt4zb0MfWj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Angela Robledo, Alianza Verde] 

Por su parte, desde las organizaciones de víctimas como la Red CONPAZ, Comunidades Construyendo Paz en los Territorios, se saluda el anuncio y afirman que así como sucedió con el proceso de paz en La Habana, las víctimas harán llegar sus propuestas de paz a la mesa con el ELN.

<iframe src="http://co.ivoox.com/es/player_ek_10998757_2_1.html?data=kpWmm52beZihhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaTDr7WuvJDXs8PmxpDTw9jJb9GZpJiSpKbGsMrXwpDd1NTHqdTjjMnSjdXFvoy5rbOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Rodrigo Castillo, CONPAZ] 

Finalmente, Luis Eduardo Celis, analista del conflicto armado en Colombia,  celebra el inicio de la fase pública de conversaciones, y asegura que “**Muy seguramente se podría abrir un canal de comunicación entre los  proceso de paz con las FARC y el ELN**", a su vez, señala que “**Sería muy importante que el cese al fuego y de hostilidades que se pacte entre el gobierno y FARC incluya al ELN”.**

######  

<iframe src="http://co.ivoox.com/es/player_ek_10998658_2_1.html?data=kpWmm52aeZmhhpywj5WcaZS1k56ah5yncZOhhpywj5WRaZi3jpWah5yncariysjWw5DKpdTZjNWSpZiJhqLWzc7Qw5DIqYzX0NPjx9fXpcTd0NPS1ZDIqYzkwt%2BYxdTSb6bAr5Caj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luis Eduardo Celis, Analista ] 
