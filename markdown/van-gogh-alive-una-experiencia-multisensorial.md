Title: "Van Gogh alive", una experiencia multisensorial
Date: 2016-07-29 15:00
Category: Cultura, eventos
Tags: Exposición Van Gogh Bogotá, Los girasoles Van Gogh, Van Gogh, Van Gohg Alive
Slug: van-gogh-alive-una-experiencia-multisensorial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/12523822_269094880091586_4089663957169021343_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: El espectador 

##### [29 Jul 2016] 

[Con más de  mil dibujos y 900 pinturas llega a Bogotá “Van Gogh alive”, una exposición de arte que busca maravillar a los  asistentes con lo mejor de la obra del artista neerlandés Vincent Van Gogh; evento organizado  por las compañías  Grande Exhibitions y Tyrona Eventos.]

[Además de apreciar “La noche estrellada” y “Los girasoles” entre otras grandes obras,  los asistentes podrán observar cómo el arte de pintar y la tecnología se conjugan en una combinación de gráficos en movimiento, sonidos de calidad y multipantallas, las cuales les permitirán visualizar las pinturas en el techo y piso, todo esto  gracias a la tecnología Sensory 4.  ]

[![Van-Gogh-Alive](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Van-Gogh-Alive.png)

[La exposición "Van Gogh Alive" estará en Bogotá hasta el 24 de octubre en Cafam de la Floresta, de ahi partirá al Valle del Cauca, en  Cali, desde el 8 de noviembre hasta el 20 de diciembre en el Centro de Convenciones Alférez Real. Boletería disponible a partir de los  38 mil pesos para el público general y  23 mil pesos para niños y adultos mayores en los puntos y página web de  TuBoleta.]
