Title: Cuba apuesta por la energía solar como alternativa al petróleo
Date: 2017-01-20 12:14
Category: El mundo, Otra Mirada
Tags: Cuba, energías renovables, parques solares
Slug: cuba-energia-solar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/paneles-solares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:NQ Norte 

##### 20 Ene 2016 

Con el propósito de reducir su dependencia a los combustibles fósiles, **el gobierno de Cuba inició la construcción de 59 parques solares fotovoltaicos**, estructuras que permiten el aprovechamiento de la energía solar de manera eficiente y de paso reduce los impactos ocasionados al ambiente por la extracción y combustión del petróleo y sus derivados.

De acuerdo a lo publicado por el diario oficial Granma, son las Brigadas del Ministerio de la Construcción (MICONS), las encargadas de iniciar los trabajos de movimientos de tierra y edificación, proyectando que **para finales de 2017 estén construídos por lo menos 33 de los parques** para ser sincronicados al sistema energético nacional.

Con la primer fase de construcción es que **aporte un promedio de 59 megavatios**, equivalentes a la mitad de lo producido por una planta eléctrica convencional, en lo que catalogan como "una de las fuentes renovables más prometerodas para la nación y **un objetivo esencial en el propósito de cambiar la matriz energética**" sustentada en el petróleo.

En la actualidad **Cuba obtiene un 4 % de su electricidad de fuentes renovables y proyecta llegar al 24 % en el 2030**, con estrategias como la construcción de siete parques eólicos que fueron anunciados el pasado mes de septiembre en la región oriental, que sumarán al sistema una potencia total de 750 megavatios. Le puede interesar:[Salud, educación y asistencia social prioridades en cuba](https://archivo.contagioradio.com/economia-cuba-2017/).

En promedio, anualmente **la isla recibe más de 1.800 kilovatios por metro cuadrado de radiación solar**, lo que permite augurar que el país se convierta en una potencia enérgetica a partir del uso de energías alternativas que eviten crisis como la presentada por la reducción en el suministro de crudo subsidiado proveniente de Venezuela.
