Title: 8M: Un día de lucha por los derechos de las mujeres
Date: 2019-03-08 13:10
Author: ContagioRadio
Category: Movilización, Mujer
Tags: Activismo feminista, feminismo, marcha
Slug: lucha-derechos-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/53469073_10217689137717352_8361430628066918400_n.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/8M-2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: José Rocamora] 

###### [8 Mar 2019] 

Este jueves, al igual que cada 8 de marzo después de 1911, las mujeres conmemorarán la multitudinaria marcha de 1857, cuando trabajadoras textiles reclamaron en Nueva York (EE.UU), mejores condiciones salariales para ellas, y el fin del trabajo infantil. En Colombia esta conmemoración incluirá una **marcha de antorchas** que se desarrollará en diferentes capitales, entre otras actividades con las que buscarán reclamar el **goce efectivo de sus derechos.**

Como lo recuerda **Jenifer Vanegas Espejo, una de las mujeres que organizó la conmemoración que se realizará en Colombia,** esta fecha es especial porque recuerda diferentes hitos en la historia de la lucha por los derechos de las mujeres que ocurrieron en marzo; entre ellos el de 1.857, y uno de los más recordados, **el de 1.908, cuando más de 100 mujeres trabajadoras en una fábrica de algodón fueron incineradas mientras participaban de una huelga para mejorar sus condiciones laborales**.

Esta lucha que ha durado cerca de siglo y medio, en Colombia sigue clamando por los derechos básicos de las trabajadoras como la pensión para madres sustitutas del Instituto Colombiano de Bienestar Familiar (ICBF), la seguridad social de las trabajadoras domésticas, y la paridad salarial. De igual forma, clamarán por la vida de defensoras y defensores de derechos humanos que han perdido la vida en el desarrollo de su labor, y para que se proteja a quienes tienen su vida en riesgo por cuenta de amenazas.

### **¿Cómo está la garantía de derechos para las mujeres?** 

Vanegas sostiene que este Gobierno ha implementado políticas regresivas en materia de derechos para las mujeres, por ejemplo en el marco del Plan Nacional de Desarrollo (PND) que profundiza el desarrollo del Estado desde una visión únicamente masculina, razón por la que los derechos de las mujeres tiene menos importancia. (Le puede interesar: ["Colombia por debajo del promedio mundial y regional en paridad política"](https://archivo.contagioradio.com/colombia-debajo-promedio-mundial-regional-paridad-politica/))

Uno de los puntos en los cuales el PND afecta a las mujeres tiene que ver con el respeto a los territorios, pues como lo explica la activista, "es una amenaza para las mujeres líderesas en los territorios, porque son las mujeres quienes lideran la defensa den los recursos naturales"; adicionalmente, agrava la ya difícil situación laboral de las mujeres al entorpecer el derecho a la pensión. (Le puede interesar:["Realizan el primer tribunal a la justicia patriarcal en Colombia"](https://archivo.contagioradio.com/justicia-patriarcal-mujeres/))

### ** "La situación nos llama a articularnos, a estar unidas"** 

Para hacer frente al PND y a las diversas violencias de las que son víctimas las mujeres, Vangeas afirma que es necesaria la unión y la articulación, razón por la que este viernes el movimiento feminista organizó una Marcha de Antorchas en Bogotá que permita a todas las mujeres salir de sus trabajos y encontrarse en el Centro Nacional de Memoria Histórica para caminar a través del barrio Santa Fé, acompañando la lucha de las trabajadoras sexuales por el respeto de sus dignidad, y finalizando en la Plaza de Bolívar.

![8 de marzo, mujeres](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/53469073_10217689137717352_8361430628066918400_n-534x715.jpg){.alignnone .size-medium .wp-image-62881 width="534" height="715"}

Otras ciudades también han organizado diferentes eventos como conversatorios, talleres y coloquios que terminan de igual forma en marchas de antorchas, razón por la que Vanegas espera que este viernes sea histórica, y sean muchas las mujeres que en todo el país salgan a luchar por el feminismo "en sus prácticas cotidianas más allá de la academia". (Le puede interesar: ["Informe revela aumento en violencia contra las lideresas en Colombia"](https://archivo.contagioradio.com/62024/))

> [\#UnidasSomosMás](https://twitter.com/hashtag/UnidasSomosM%C3%A1s?src=hash&ref_src=twsrc%5Etfw)  
> Amigas y amigos de [@RutaPacificaM](https://twitter.com/RutaPacificaM?ref_src=twsrc%5Etfw), compartimos las acciones políticas y simbólicas que nuestro movimiento pacifista y feminista realizará el 8 de marzo en 10 lugares del país. [\#8M](https://twitter.com/hashtag/8M?src=hash&ref_src=twsrc%5Etfw). [\#NingunaGuerraEnNuestroNombre](https://twitter.com/hashtag/NingunaGuerraEnNuestroNombre?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/ml3EzYKfV8](https://t.co/ml3EzYKfV8)
>
> — RutaPacíficadelasMujeres (@RutaPacificaM) [1 de marzo de 2019](https://twitter.com/RutaPacificaM/status/1101516086728683521?ref_src=twsrc%5Etfw)

###### <iframe id="audio_33204063" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33204063_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
