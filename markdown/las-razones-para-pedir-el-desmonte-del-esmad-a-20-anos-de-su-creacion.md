Title: Las razones para pedir el desmonte del ESMAD a 20 años de su creación
Date: 2019-11-26 18:05
Author: CtgAdm
Category: Nacional, Paro Nacional
Tags: Desmonte del ESMAD
Slug: las-razones-para-pedir-el-desmonte-del-esmad-a-20-anos-de-su-creacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/ESMAD.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-26-at-3.28.02-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Organizaciones defensoras de derechos humanos plantean una vez más la necesidad de desmontar el Escuadrón Móvil Antidisturbios (ESMAD) de cara al aumento en los abusos de fuerza y recrudecimiento de la violencia que se han evidenciado con el homicidio de Dilan Cruz, estudiante que ejercía su derecho legítimo a la protesta, y casos históricos de la misma trascendencia como el de **Nicolás Neira, asesinado en 2005**, ambos por integrantes de esta fuerza policial.

Para el abogado defensor de derechos humanos, Germán Romero, existen tres temas claves por los cuales hablar de un desmonte del ESMAD, la primera relacionada al carácter sistemático y reiterado en el "método, uso de armas, municiones y forma en que se ejerce el control en las manifestaciones pacíficas", **tanto a nivel urbano como a nivel rural**, lo que ha devenido en graves violaciones de DD.HH. [(Lea también: Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo)](https://archivo.contagioradio.com/se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo/)

En segundo lugar, han sido varias las condenas y llamados de atención por parte del Consejo de Estado que ha exigido al ESMAD iniciar cursos de derechos humanos para sus integrantes, un requerimiento que según el abogado, se hace urgente con el asesinato de Dilan y  "no ha surtido ningún efecto sino que por el contrario se ha profundizado".

Como tercer elemento, Romero afirma que el ESMAD **"ha sido instrumentalizado como un agente de ataque a la manifestación social"**, evocando ejemplos como el de Tunja, Boyacá que durante el pasado 21N se movilizó sin la presencia del escuadrón y la jornada se desarrollo de forma tranquila. [(Le recomendamos leer: El prontuario del ESMAD) ](https://archivo.contagioradio.com/el-prontuario-del-esmad/)

Agrega que esta unidad de la Fuerza Pública se convirtió en una opción por parte del Estado para optar por la salida policial a la inconformidad social, antes que atender las demandas de los ciudadanos, **"todavía nuestra clase dirigente es incapaz y no tiene la madurez suficiente para enfrentar una sociedad moderna"**.

\[caption id="attachment\_77189" align="aligncenter" width="999"\]![ESMAD](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-26-at-3.28.02-PM.jpeg){.size-full .wp-image-77189 width="999" height="547"} Foto: Contagio Radio\[/caption\]

### "El ESMAD está mandado a recoger"

Esta unidad de la Policía Nacional nació en 1999 comenzando con 19 unidades, cifra que en 2018 aumentó hasta llegar a tener 3.580 agentes en todo el país. Según el Manual 070 de Autoprotección contra el ESMAD, 20 personas han sido asesinadas por lo que se supone es una fuerza no letal,  sin embargo el ESMAD no reconoce ningún de estos casos, que en su mayoría han afectado a sectores campesinos, indígenas y estudiantiles.

De acuerdo a las cifras del libro 'Elogio a la bulla protesta y Democracia en Colombia", que citan a la Defensoría del Pueblo, durante e**l Paro Agrario de 2013 se interpusieron 103 quejas en contra de policías del ESMAD,** lo que equivale al  82% de todas las quejas presentadas ese año con relación a la actuación de la Policía en dicho paro mientas que, según la Procuraduría, entre 2007 y marzo de 2017, se recibieron 150 quejas disciplinarias en contra de esta fuerza policial.

Dichas cifras contrastan con las del **Centro de Investigación y Educación Popular (CINEP)** que en 2017 registró un total de 540 victimizaciones y acciones represivas en su mayoría a manos del ESMAD. Se espera que en las próximas semanas, Temblores ONG  haga el lanzamiento “Silencio Oficial: un grito aturdido de justicia en el marco de los 20 años del ESMAD” que pueda aportar nueva información sobre la violación de DD.HH.

Romero hace énfasis en que casos como el del asesinato de Dilan Cruz no son aleatorios, **"los manuales y procesos de formación del ESMAD están diseñados para tomar esta actitud y esta forma de reprimir  la movilización**", afirma, haciendo referencia a la evidencia que existe de agentes de la Fuerza Pública que al iniciar disturbios, infiltrar la manifestación, provocan a los estudiantes, se autoinfligen heridas y detonan granadas para justificar el ataque a las personas.

Este accionar grupal, afirma el abogado, se evidencia en casos como el del estudiante Jhony Silva, estudiante de la Universidad del Valle asesinado en 2005 por una unidad de este escuadrón, que estaba al mando del entonces capitán Gabriel Bonilla, quien llegó a comandar el ESMAD a nivel nacional y más tarde se convirtió en comandante de la Policía Metropolitana en Ibagué, ascensos que dan a entender que al interior de la institución "quienes cometan violaciones a los DD.HH. son premiados". [(Le puede interesar: Indignación en las calles por asesinato de Dilan Cruz) ](https://archivo.contagioradio.com/indignacion-en-las-calles-por-asesinato-de-dilan-cruz/)

Por tanto agrega que no es un problema de procedimiento, se trata de que exista un tránsito de una solución violenta a una solución dialogada en la que, no solo basta con el desmonte sino con la judicialización  de los responsables de los cerca de 1.200 casos que están siendo investigados que involucran al ESMAD. [(Lea también: Caso de Nicolás Neira abre la puerta para esclarecer crímenes del ESMAD)](https://archivo.contagioradio.com/caso-de-nicolas-neira-abre-la-puerta-para-esclarecer-crimenes-del-esmad/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44903697" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44903697_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
