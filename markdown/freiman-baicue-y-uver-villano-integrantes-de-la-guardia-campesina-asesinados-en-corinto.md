Title: Freiman Baicue y Uver Villano, integrantes de la guardia campesina asesinados en Corinto
Date: 2019-06-11 13:16
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Cauca, Corinto, Fensuagro, lideres sociales asesinados
Slug: freiman-baicue-y-uver-villano-integrantes-de-la-guardia-campesina-asesinados-en-corinto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

Organizaciones sociales y defensoras de derechos humanos denunciaron el pasado lunes que en horas de la madrugada, hombres armados asesinaron a **Freiman Baicue y Uver Ney Villano,** líderes sociales integrantes de la **Asociación de Trabajadores Campesinos Zona de Reserva Campesina del Municipio de Corinto (ASTRAZONAC),** en Cauca. Los hechos habrían ocurrido mientras los dos hombres departían en un sitio de comidas en el centro del municipio.

**Andrés Martínez, presidente de ASTRAZONAC,** señaló que fueron asesinados por hombres que llegaron en moto, y sin mediar palabra dispararon contra los integrantes de la guardia campesina. Martínez también sostuvo que aunque en el territorio se producen amenazas de manera sistemática contra la vida de defensores de derechos humanos y miembros de procesos sociales, no conocían de riesgos para la vida de Baicue o Villano. (Le puede interesar: ["Con diálogos para la no repetición, Comisión para el Esclarecimiento de la Verdad promoverá defensa de líderes sociales"](https://archivo.contagioradio.com/con-dialogos-para-la-no-repeticion-comision-de-la-verdad-promovera-defensa-de-lideres-sociales/))

El Presidente de la Asociación Campesina manifestó que en Corinto han visto **amenazas firmadas por las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) y las Águilas Negras;** adicionalmente las AGC en ocasiones hacen presencia en el territorio y marcan las paredes con sus mensajes. No obstante, resaltó que son las instituciones estatales las encargadas de investigar estos hechos y capturar a los autores materiales e intelectuales de este hecho; aunque no se ha recibido comunicación oficial alguna hasta el momento.

### **Aumenta la cifra de defensores de derechos humanos asesinados durante el gobierno Duque**

Tras el homicidio de [Julían Quiñones](https://archivo.contagioradio.com/asesinan-julian-quinones-lider-denuncio-corrupcion-covenas/), algunas organizaciones hacían un recuento de 174 defensores de derechos humanos asesinados desde el 7 de agosto de 2019, número que se elevaría nuevamente este martes. Pero más allá de los dígitos, la ASTRAZONAC destacó que **Freiman Baicue y Uver Ney Villano eran líderes que le apostaban a la defensa del territorio y de los derechos humanos**, razón más que suficiente para exigir al Gobierno la protección de su vida, y la respuesta judicial a quienes atentaron contra la misma.

<iframe id="audio_36966211" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36966211_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
