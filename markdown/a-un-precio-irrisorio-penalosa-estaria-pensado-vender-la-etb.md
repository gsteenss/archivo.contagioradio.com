Title: A un precio "irrisorio" Peñalosa estaría pensado vender la ETB
Date: 2016-01-04 16:04
Category: Economía, Nacional
Tags: Enrique Peñalosa, ETB, Gustavo Petro
Slug: a-un-precio-irrisorio-penalosa-estaria-pensado-vender-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/VENTA-ETB.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[lacordinadora.wordpress.com]

###### [3 Enero 2016] 

Han pasado solo 3 días de la posesión de Enrique Peñalosa como nuevo alcalde Bogotá, y ya ha generado varias dudas frente a sus propuestas que **ya plantean una posible privatización de la ETB, vendiéndola en mil millones de dólares.**

En el discurso de Peñalosa ante los concejales de Bogotá en la primera sesión de este 2016, nuevamente demostró su interés en vender la ETB y otras empresas públicas del distrito, "Las empresas de telecomunicaciones europeas que eran gigantescas, con gobiernos socialistas, podrían comprar la empresa de teléfonos de Bogotá con la caja menor de un mes".

Ante esta posibilidad, Clara López, presidente del Polo Democrático Alternativo, aseguró que la privatización de estas empresas públicas implicaría **“subirles los impuestos a los bogotanos porque estas empresas son tan buen negocio y entregárselas al sector privado es reducir los ingresos** desde el concepto de utilidades lo que implica aumentar los recursos por la vía de los impuestos, y de otra parte privatizar siempre implica que favorecer a las personas más vulnerables de la sociedad se vuelva un imposible por los altísimos costos”.

Pese a que en campaña, Enrique Peñalosa había dicho que no se venderían las empresas del distrito, **lo cierto es que no lo descarta, pues ha indicado que esta empresa está en un mercado que representa un constante riesgo** y por lo que deberá analizar la posibilidad de venderla, pues según él “una empresa como la ETB tiene unos riesgos inmensos de competencia mundial”.

Por su parte, el exalcalde de Bogotá, Gustavo Petro, señaló a través de sus redes sociales, que **los mil millones de dólares por los que se piensa vender la ETB es un precio “irrisorio”,** pues no se está teniendo en cuenta el crecimiento que esta empresa obtuvo en los últimos años, ya que pasó de **siete años decreciendo en sus ingresos, a registrar un ascenso desde 2013 obteniendo 713.526 millones** de pesos en el primer semestre del 2015, elevando las metas para finales de 2015 en \$1,8 billones en ingresos.

“Traer ahora un discurso de hace 20 años para privatizar la ETB es literalmente pensar que la administración pública es un espacio simple para hacer grandes negocios de particulares”, expresó Petro.
