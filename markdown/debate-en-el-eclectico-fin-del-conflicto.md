Title: Debate en El Ecléctico: Fin del Conflicto
Date: 2016-09-17 11:00
Category: El Eclectico
Tags: acuerdo de paz, colombia, Fin del conflicto
Slug: debate-en-el-eclectico-fin-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/proceso_de_paz_2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe id="audio_12893239" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12893239_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El senador **Antonio Navarro**, excombatiente del M19, aseguró que “el tiempo de los grupos guerrilleros ya pasó; son un arcaísmo, un dinosaurio”. Este pronunciamiento lo hizo durante el **debate de El Ecléctico sobre el punto 3 del acuerdo de paz, el concerniente al Fin del Conflicto**. Además, dijo también que “las Farc se dieron cuenta de lo mismo que nosotros (el M19) hace 26 años: que armarse no tiene sentido”.

En este debate estuvieron también **Luis Felipe Olarte** y **Daniel Arce**, integrantes de las juventudes del **Centro Democrático**, quienes manifestaron que el partido reconoce que este acuerdo tiene muchas cosas buenas pero están convencidos de que se puede lograr un mejor acuerdo y por esa razón invitaron a votar NO en el plebiscito.

**Juan David Torres**, estudiante de economía en la Universidad Javeriana y columnista de El Ecléctico, señaló que “a pesar de los sapos que hay que tragarse con este acuerdo, los colombianos no tendremos que seguirnos tragando el sapo más grande: la guerra”. **Angélica Antequera**, estudiante de Relaciones Internacionales en la Universidad del Rosario y cofundadora de Hablemos de Paz UR, dijo estar convencida de que el hecho de que las Farc se desarmen y participen en política es el primer paso para la construcción de un país en paz.
