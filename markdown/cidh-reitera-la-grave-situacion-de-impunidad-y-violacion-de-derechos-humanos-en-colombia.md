Title: CIDH reitera la grave situación de impunidad y violación de derechos Humanos en Colombia
Date: 2016-03-22 18:29
Category: DDHH, Nacional
Tags: CIDH, Ejecucuciones extrajudiciales, Paramilitarismo
Slug: cidh-reitera-la-grave-situacion-de-impunidad-y-violacion-de-derechos-humanos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cidh-colombia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH] 

###### [20 Mar 2016]

Según el último Informe de la Comisión Interamericana de Derechos Humanos, que contiene un capítulo sobre Colombia, el 100% de las denuncias quedan en la Impunidad, persisten la violación de derechos humanos por todos los actores del conflicto armado y hace un llamado al esclarecimiento del paramilitarismo que existe en el país.

El informe del CIDH que cuenta con 362 puntos, hace un compendio de la situación actual de derechos humanos y expone cifras frente el estado de la violación de derechos. Por ejemplo reporta un total **de 230 denuncias por ejecuciones extrajudiciales en el 2015, que se suman a las 5.763 reportadas entre el 2001 y el 2010.**

Además, el informe devela que los crímenes cometidos por las Fuerzas Armada en el periodo de gobierno anterior pertenecían en un 90% al Ejercito Nacional, mientras que en el actual gobierno el 46,7% se atribuyó a la Policía Nacional y 51,7 % al Ejercito Nacional. Referente a la desaparición forzada, la CIDH expone que en lo corrido del año 2015, **la justicia colombiana reporta 3.400 casos de desaparición, de los cuales 59 correspondían a desaparición forzada y solo se han proferido 35 sentencias por este delito.**

A su vez, manifiesta que la falta de esclarecimiento de las dinámicas, composición y estructuras de antiguas autodefensas, surgidos después de la desmovilización de organizaciones paramilitares, constituyen un obstáculo no solo para garantizar los derechos de las víctimas, sino para frenar la impunidad que persiste con el proceso de paz llevado a cabo en el periodo del ex presidente Álvaro Uribe Vélez.

Por otro lado, el CIDH genera una serie de consideraciones al mecanismo del marco de justicia transicional que se está llevando a cabo en materia de garantías y reparación a las víctimas del conflicto armado, en primera instancia hace un llamado a la superación de la impunidad,  para lograr una paz duradera y en segundo lugar evidencia que debe aplicarse un marco normativo que garantice la verdad, justicia y reparación a las víctimas del conflicto armados por parte de todos los actores del conflicto armado.

La Corte Interamericana de Derechos Humanos, después de generar este balance, afirma que las observaciones realizadas en periodos pasados aún persisten y que por este motivo, permanecerá en constante supervisión de los avances del proceso de paz y del reconocimiento de los derechos humanos en el país.

Consulte aquí el informe sobre Colombia

[Informe Anual CIDH, Capítulo Colombia](https://es.scribd.com/doc/305665670/Informe-Anual-CIDH-Capitulo-Colombia "View Informe Anual CIDH, Capítulo Colombia on Scribd")

<iframe id="doc_36267" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/305665670/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="70%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
