Title: Comisión Ética por la verdad tiene la mira en la Construcción de Paz en Colombia
Date: 2018-11-20 16:56
Author: AdminContagio
Category: DDHH, Nacional
Tags: colombia, Comisión Ética, comunidades de paz
Slug: comision-etica-por-la-verdad-tiene-la-mira-en-la-construccion-de-paz-en-colombia
Status: published

###### [Foto:] 

###### [20 Nov 2018] 

Desde el 15 al 19 de noviembre Colombia recibió a la Comisión Ética, conformada por diversas personas defensoras de derechos humanos a nivel internacional, que recopiló información para la construcción de memoria sobre el conflicto armado, y expresó su preocupación frente a **temas como la implementación de los Acuerdos de Paz y la garantía a los derechos de la ciudadanía en el país**.

Esta Comisión Ética, que surgió en el año 2006, viene trabajando en la reconstrucción de casos de violencia durante el conflicto armado, como una etapa previa a la documentación de la Comisión de la Verdad y ahora ha logrado coordinar un diálogo directo con esta instancia y está integrada por Mirta Acuña de Varavalle, del proceso de las Madres de la Plaza de Mayo, línea fundadora, Eduardo Natchman, , de Hijos e Hijas por la Identidad y la Justicia contra el Olvido y el Silencio; Pilas Sánchez González y Andrés Redondo, de la comunidad Santo Tomás, Madrid; Vicenta Font, de la Taula Catalana por la Paz y los Derechos Humanos en Colombia; y Mary Bricker-Jenkins del Movimiento de los pobres, Tica Font, de  Taula Catalana por la Paz y los Derechos Humanos en Colombia y Yolaima Salderon

### **Las preocupaciones de la Comisión Ética** 

Luego de tener reuniones con diversos sectores de la sociedad, la Comisión señaló que hay grandes preocupaciones frente al avance de Colombia en la construcción de paz, uno de ellos, de acuerdo con Tica Font, son los asesinatos y amenazas a líderes sociales en el país, sin que **hayan medidas por parte del gobierno para garantizar su vida mientras continúa la impunidad.**

Además, para la comisionada "hay una parte de esta sociedad que no quiere que esto cambie, que procura que todo siga como estaban antes", conformada por la élite, "beneficiaría económicamente del conflicto del país", en hechos como el despojo de tierras, y otra que tiene una gran desconfianza frente a las transformaciones que ya se han venido gestando. Razón por la cual consideró que uno de los mayores retos que debe afrontar Colombia es el reconocimiento entre personas. (Le puede interesar: ["Jalón de orjas a la JEP por parte de víctimas del General Montoya"](https://archivo.contagioradio.com/jalon-orejas-jep/))

Otro de los retos para la Comisión, tiene que ver con que el gobierno Nacional tenga voluntad de seguir en la mesa de diálogos con el ELN y lograr condiciones mínimas que permitan ir **desescalando conflicto armado que propenda por la protección de la población civil.**

### **Gobierno debe garantizar recursos para la implementación de los Acuerdos de Paz** 

Otra de las preocupaciones, es la llegada de Duque al poder y su compromiso con la implementación de los Acuerdos de Paz, que para Font se verá reflejada en el presupuesto que se destine a organismos como la Comisión de la Verdad, y de esta forma garantizar su operación, y señaló que en caso dado de que esto no suceda, **la comunidad internacional y los países garantes tendrán la responsabilidad de respaldar estos organismos.**

Asimismo aseguró que las organizaciones de víctimas deberán generar un apoyo a todo el Sistema de Verdad Justicia Reparación y No repetición, cuando se intente deslegitimar el trabajo que estas instancias realicen, porque finalmente su trabajo girará en torno a la transformación social. (Le puede interesar: ["Organizaciones de DD.HH y la Corte Penal Internacional, temen por modificaciones a la JEP"](https://archivo.contagioradio.com/organizaciones-de-dd-hh-y-la-corte-penal-internacional-temen-impunidad-por-modificaciones-a-la-jep/))

Frente a la Jurisdicción Especial para la Paz, la comisionada expresó que esperan que **no se modifique este Tribunal con la apertura de una sala aparte para el juzgamiento de militares**, que está próximo a definirse en la Cámara de Representantes .

### **"Hay serenidad" ** 

Font manifestó que durante su visita notó que sí ha sentido un cambio social en Colombia, "en los primeros años que yo venía aquí, el miedo estaba muy instaurado en la piel de todas las personas. Hay miedo en ciertas zonas, pero hay otras en donde el miedo se ha relajado y **han empezado a abrir su corazón y a encontrarse con los otros en su humanidad"**.

Yolaima Salderon, también integrante de la Comisión, afirmó que tras la visita su percepción sobre Colombia es la de un pueblo que verdaderamente le ha apostado a la paz, "lo están haciendo muy bien, en el sentido de que han logrado juntar a víctimas y victimarios desde su ser humano y hablar de lo que pasó, promoviendo una reconciliación que no esta solo en el papel sino que apuesta por relacionarse de forma diferente" aseveró.

###### Reciba toda la información de Contagio Radio en [[su correo]
