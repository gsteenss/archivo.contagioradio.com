Title: ONIC suspende consulta previa por modificaciones de Gobierno a Ley de Tierras
Date: 2017-05-23 18:26
Category: DDHH, Nacional
Tags: acuerdo final, CSIVI, ONIC
Slug: onic-suspende-proceso-de-consulta-previa-tras-incumplimiento-de-gobierno-en-proyecto-de-ley-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ONIC-e1460134357679.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [23 May 2017] 

Tras el anuncio que hizo la Comisión de Seguimiento e Implementación de los Acuerdos de paz (CSIVI), sobre las modificaciones hechas al proyecto de Ley de Tierras, por el Gobierno Nacional, la Organización Nacional Indígena de Colombia (ONIC) manifestó su decisión de **suspender los procesos de consulta previa sobre las discusiones relacionadas con las normas del Fast Track,** en la Mesa Permanente de Concertación, hasta no tener garantías de dialogo con la CSIVI.

Los indígenas señalaron que rechazan cualquier “**pretensión de incluir en los acuerdos en esta norma beneficios a grandes propietarios** a través del otorgamiento de derechos de uso” y la actuación del viceministro de Participación para la Igualdad de Derechos del Ministerio del Interior, Luís Ernesto Gómez, debido a la desinformación y falta de palabra que los indígenas consideran ha tenido frente a los diálogos. Le puede interesar: ["Gobierno traicionó el consenso en torno a la ley de Tierras: CSIVI" ](https://archivo.contagioradio.com/gobierno-traiciono-el-consenso-en-torno-a-ley-de-tierras-csivi-farc-ep/)

La ONIC manifestó que en el proceso de la implementación de los Acuerdos de Paz, **“no ha existido la voluntad política genuina de parte de la CSIVI para incluir a los pueblos indígenas** y el desarrollo del Capítulo Étnico y expresaron que el gobierno Nacional ha vulnerado el derecho de estas comunidades a participar al negarse de un lado, a radicar en la mesa la totalidad de las normas para ser consultadas, y del otro, a incluir las propuestas orientadas a salvaguardar los derechos de los pueblos indígenas.

De igual forma, afirmaron que continúan con la voluntad de la construcción de paz para el país y mantienen su disposición para el diálogo e informaron que han avanzado en la revisión de las **5 propuestas normativas presentadas en la mesa**, dando por sentado que estas normativas han cumplido previamente con la concertación en el espacio de la CSIVI, como se pactó en los Acuerdos de Paz.
