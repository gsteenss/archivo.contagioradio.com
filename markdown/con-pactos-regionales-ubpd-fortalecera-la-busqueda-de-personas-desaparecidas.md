Title: Con pactos regionales UBPD fortalecerá la búsqueda de personas desaparecidas
Date: 2020-12-09 15:24
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Meta, personas desaparecidas, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: con-pactos-regionales-ubpd-fortalecera-la-busqueda-de-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Directora General de la [Unidad de búsqueda de personas dadas por desaparecidas](https://www.ubpdbusquedadesaparecidos.co/actualidad/pacto-por-la-busqueda-de-las-personas-desaparecidas-en-meta-anuncia-directora-de-la-unidad-de-busqueda/), Luz Marina Monzón Cifuentes, anunció que se realizarán **pactos regionales para fortalecer la búsqueda de personas dadas por desaparecidas** en el marco del conflicto armado en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

«Es nuestro propósito fortalecer el Estado social de derecho, por medio de la legitimidad de las instituciones dentro y fuera del Sistema Integral de Verdad, Justicia, Reparación y No Repetición, que se logra a través de la respuesta articulada y coordinada a las víctimas, de la garantía a los derechos y participación de familiares víctimas de desaparición en la búsqueda de sus seres queridos», señaló la directora. (Le puede interesar: [UBPD recuperó 24 cuerpos en acción humanitaria en Caldas](https://archivo.contagioradio.com/ubpd-recupero-24-cuerpos-en-accion-humanitaria-en-caldas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Monzón indicó que el primero de los pactos regionales se suscribirá en Villavicencio, departamento del Meta**. Allí, la suscripción del pacto se realiza en alianza con el alcalde de Villavicencio, Juan Felipe Harman Ortiz, agencias de cooperación internacional, organizaciones de derechos humanos y quienes continúan en la búsqueda de sus familiares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, también se espera que hagan parte las autoridades locales y regionales, autoridades indígenas y afrodescendientes, campesinos, movimientos sociales, sectores económicos, gremios, iglesia, academia y la ciudadanía. **«Se requiere de un esfuerzo mayor como sociedad colombiana, en el que, de manera articulada, amplia y diversa, invitemos a todo el departamento para poner en primer lugar a las personas dadas por desaparecidas y a sus familias»**, indicó la Directora.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Meta, segundo departamento más afectado por la desaparición forzada
-------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con el Centro Nacional de Memoria, a la fecha se han registrado 113.442 personas que continúan desaparecidas, de las cuales el 63,9% son víctimas de desaparición forzada, el 20,7% fueron secuestradas y el 15,3% reclutadas de manera ilegal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El Meta es uno de los nueve departamentos donde se concentra el 60% de las desapariciones de personas registradas en Colombia**. Monzón afirma que el departamento ocupa el 6.6% del total de desapariciones forzadas, 10% de casos de reclutamiento y el 4% de los secuestros en el país, **«cifras aterradoras si se tiene en cuenta que en ese departamento habita el 2% de la población colombiana».** (Le puede interesar: [UBPD reunió a víctima de desaparición forzada con su familia después de 35 años](https://archivo.contagioradio.com/ubpd-reunio-a-victima-de-desaparicion-forzada-con-su-familia-despues-de-35-anos/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
