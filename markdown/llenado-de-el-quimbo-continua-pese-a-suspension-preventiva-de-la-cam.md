Title: Llenado de El Quimbo continúa pese a suspensión preventiva de la CAM
Date: 2015-07-06 14:21
Category: Ambiente, Nacional
Tags: Agencia Nacional de Licencias Ambientales, ANLA, ASOQUIMBO, Bogotá, CAM, El Quimbo, EMGESA, Gobernación del Huila, Huila, Miller Dussán, Ministerio de Ambiente, Río Magdalena
Slug: llenado-de-el-quimbo-continua-pese-a-suspension-preventiva-de-la-cam
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11696290_1141824982501176_5931153056671580492_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.pitalitonoticias.com]

<iframe src="http://www.ivoox.com/player_ek_4728301_2_1.html?data=lZyfmpiUdY6ZmKiak52Jd6Kkl5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkM3Zz8bR0ZDIqYy5zZC%2B187RptCfxNTb1s7SucKf0crgx5DFb9Tp1NXS0NjNaaSnhqeg0JDUtsbqxtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Miller Dussán, ASOQUIMBO] 

###### [6 de julio 2015] 

El llenado del embalse El Quimbo deberá ser detenido luego de que la Corporación Autónoma Regional del Alto Magdalena, CAM, interpusiera una medida preventiva al demostrarse que **EMGESA no realizó en su totalidad el aprovechamiento forestal,** poniendo en riesgo la calidad de las aguas del Río Magdalena por contaminación. A pesar de ello, desde EMGESA se anuncia que la **hidroeléctrica estará lista para el mes de septiembre.**

Pese al anuncio de la CAM, Miller Dussán, investigador de ASOQUIMBO, asegura que **el  llenado de la represa continúa y “toda la biomasa, la madera que no se sacó, está flotando**”. Es por eso que desde ASOQUIMBO se le exige a la gobernación del Huila, se use las herramientas constitucionales para detener esa acción de la empresa.

“El gobernador del Huila debe garantizar que se cumpla la ley e impedir que se siga llenando la represa; quiero ver un **gobernador fortalecido que use la fuerza pública en favor de los intereses territoriales**,” expresa Dussán.

A las 4 de la tarde de este lunes, se entregará una tutela ante la Corte Constitucional para suspender el llenado de El Quimbo, esperando que esta vez la justicia no solo sirva a las multinacionales.

Cabe recordar que la **Gobernación del Huila ya reconoció que no se han entregado las 5.200 hectáreas de tierras** a los campesinos y campesinas afectadas por la hidroeléctrica, quienes además se quedarían sin trabajo con el llenado, ya que se ocasionaría la muerte de 30 mil toneladas de peces, afectando uno de los renglones más importantes de la economía de esa región del país. [Ver: LLENADO ILEGAL DEL EL QUIMBO PROVOCARÍA 54 “ARMEROS” Y ACABARÍA DOS MUNICIPIOS](https://archivo.contagioradio.com/llenado-ilegal-del-el-quimbo-provocaria-54-armeros-y-acabaria-dos-municipios/)

El investigador de ASOQUIMBO  señala que el procedimiento para dar aval al llenado se realizó ilegalmente, teniendo en cuenta que los abogados de EMGESA asesoraron la resolución sobre asuntos ambientales de la ANLA para aprobar llenado de El Quimbo.

**El próximo viernes 10 de julio desde las 9 de  la mañana, se va a realizar una jornada de movilización en Bogotá y en todos los municipios del Huila** con el fin de exigir la renuncia del Ministro de Ambiente, Gabriel Vallejo, y del director de la ANLA, Fernando Iregui. En la capital del país, la protesta se desarrollará frente al Ministerio de Ambiente y en el Huila en todos los parques centrales municipales.  
 
