Title: El homenaje a Violeta Parra de gira por Colombia
Date: 2016-08-01 15:37
Category: Cultura, eventos
Tags: Cultura, Homenaje violeta Parra Bogota, Música, Violeta Parra
Slug: el-homenaje-a-violeta-parra-de-gira-por-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Violeta-Parra-PNG.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Museo 'Violeta Parra' 

###### [1 Ago 2016] 

La canción de **Violeta Parra** vuelve a sonar en la gira **'Color Violeta'**, un sentido agradecimiento-homenaje que le hace su compatriota Carmen Prieto Monreal, actriz y cantante chilena, que trae al país una puesta en escena que combina la música con algunas narraciones en primera persona de la cantautora y folclorista latinoamericana.

Con el acompañamiento del guitarrista y arreglista Galo Ugarte Pavéz y del pianista Carlos Román Mella, la artista austral interpretará durante una hora varias de las piezas musicales y poéticas más representativas en la carrera de Violeta Parra, recorriendo en 8 presentaciones 7 escenarios del país.

[![Carmen Prieto Monreal](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Violeta-Parra.jpg){.aligncenter .size-full .wp-image-27105 width="768" height="384"}](https://archivo.contagioradio.com/el-homenaje-a-violeta-parra-de-gira-por-colombia/violeta-parra/)

'Color Violeta', arranca en el Teatro del Colegio Hispanoamericano de Cali este 1ro de Agosto, el 2 en el Teatro Imperial de Pasto, el 3 en el Teatro de la Universidad de Caldas en Manizales, el 4 en el Auditorio de la Universidad del Norte en Barranquilla, el 6 en el auditorio Cajamag de Santa Marta, el 7 en el Teatro Pablo Tobón Uribe de Medellín y finalizará el 10 de Agosto en el Auditorio Old Mutual de Bogotá.

La gira es organizada por la Dirección de Asuntos Culturales del Ministerio de Relaciones Exteriores de Chile, a través de sus proyectos culturales en el exterior y la organización a cargo de la Agregaduría Cultural y de Prensa de la Embajada de Chile en Colombia.
