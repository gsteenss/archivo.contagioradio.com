Title: Familia de Victor Jara exige justicia a 40 años del asesinato del cantautor
Date: 2016-06-14 17:59
Category: El mundo, Judicial
Tags: juicio crimen Victor Jara, Pedro Pablo Barrientos, Victor Jara
Slug: familia-de-victor-jara-exige-justicia-a-40-anos-del-asesinato-del-cantautor
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Mural_Victor_Jara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Mintpressnews 

##### [14 Jun 2016] 

Después de mas de 40 años, inició en una Corte Federal de Orlando, Florida, **el juicio civil en contra del teniente (r) Pedro Pablo Barrientos**, acusado de ser el autor del asesinato y tortura del cantautor chileno Víctor Jara, en el Estadio Nacional en el incio de la dictadura de Augusto Pinochet con el respaldo del gobierno de Estados Unidos.

El ex militar, radicado en los Estados Unidos desde el año 1989, debe afrontar el proceso **por la comisión de delitos de lesa humanidad** estipulada en el marco de la ley de protección de víctimas de tortura de ese país, a la que acudieron  Joan Turner Jara, viuda del cantante, y sus dos hijas Amanda Jara y Manuela Bunster.

Y fue precisamente la esposa del músico quién rindió testimonio este lunes, describiendo explícitamente cómo recuerda el día que vió el cadáver de su marido y la manera en que afectó su vida “**Siento que fue, literalmente, el fin de mi primera vida \[…\] Cambió mi vida, y la vida de mis hijas, para siempre**”, manifestó la ex bailarina de origen británico.

De acuerdo con Nelson Caucoto, abogado de las demandantes en Chile con el proceso que se adelanta en Norteamérica, "pudiera permitir reforzar la solicitud de extradición que la justicia chilena requirió del Sr. Barrientos para que sea traído y enfrente los tribunales" añadiendo que durante el proceso **se ha revelado que el militar ocultó información para obtener la nacionalidad estadounidense**.

A pesar que la solicitud de extradición fue presentada en enero de 2013 y que desde 2012 la Corte de Apelaciones de Santiago inició el enjuiciamiento de Barrientos junto con otros siete acusados por el homicidio de Jara, **acciones dilatorias de la defensa han impedido que se comience el proceso de sentencia**, razón por la cual la familia decidió acudir a la ley en mención.

Durante el proceso, que puede tomar cerca de dos semanas, se escucharán **20 personas que atestiguaron en la causa criminal**, quienes también a consideración del abogado Caucoto "sirvan para acreditar el daño causado a la familia del músico" añadiendo que al militar "nada saca con irse a esconder a Estados Unidos y no enfrentar los tribunales chilenos porque también se le va a perseguir desde el área civil”.
