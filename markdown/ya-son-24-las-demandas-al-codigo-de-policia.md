Title: Ya son 24 las demandas al Código de Policía
Date: 2017-01-30 12:34
Category: DDHH, Otra Mirada
Tags: código de policía, Fuerza Pública
Slug: ya-son-24-las-demandas-al-codigo-de-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/código-de-policía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo  Particular] 

###### [30 Ene 2017] 

A partir de hoy comienza a regir el nuevo Código de Policía que ya tiene 23 demandas aceptadas y  en proceso de estudio en la Corte Constitucional, a la que se le suma la carta del representante Alirio Uribe en la que exige que se respete la actividad de los vendedores ambulantes y los **derechos constitucionales que tienen al trabajo, el mínimo vital y la propiedad privada.**

En la carta el Congresista pide que se aplique la figura **jurídica de excepción de inconstitucionalidad** al artículo 140 del Código de Policía, porque atenta contra estos derechos fundamentales que deben ser garantizados por el Estado:

“Existe una amplia jurisprudencia que ha abordado la tensión entre el espacio público y los derechos al trabajo, la dignidad humana y el mínimo vital de los vendedores informales, en la cual se ha afirmado reiteradamente que ante la implementación de políticas de recuperación del espacio público s**e debe proteger los derechos de los trabajadores informales”.**

De igual forma, el representante también lleva en curso otra demanda por las altas multas que penalizan la actividad de los vendedores ambulantes por ocupar espacios públicos, sin que se tenga en cuenta que “hacen parte de un grupo poblacional **que se encuentra en una condición de debilidad la cual se centra en su precariedad económica**”. Le puede interesar:["Nuevo Código de Policía otorga poderes exorbitantes a los Uniformados"](https://archivo.contagioradio.com/nuevo-codigo-de-policia-otorga-poderes-exorbitantes-a-los-uniformados/)

En este punto la Procuraduría General de la Nación, ya emitió un fallo en el que señala que “las sanciones establecidas en los artículos del código mencionado, aplicables a vendedores informales son desproporcionadas y resultan contrarias a la Constitución, motivo por el cual **la propia Procuraduría solicita que las declare inexequibles**”

Las otras demandas que se han presentado giran en torno a la  facultad que tiene la Policía para  conducir a una persona a un Centro de Atención Inmediata por su protección, si está ebrio, bajo la influencia de sustancias psicoactivas en espacios públicos,   **el ingreso a las viviendas sin una orden de captura previa**; disolver una manifestación cuando haya una “alteración de la convivencia” y el trámite para pedir permiso a la hora de realizar una protestar con 48 horas de anticipación siempre que **se trate de un fin “legítimo”,  sin especificar qué lo es y qué no.** Le puede interesar:["Código de Policía tiene vicios de procedimiento y vulnera DDH: Iván Cepeda"](https://archivo.contagioradio.com/codigo-de-policia-tienes-vicios-de-procedimiento-y-vulnera-ddhh-ivan-cepeda/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
