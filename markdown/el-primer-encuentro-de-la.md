Title: Víctimas de la Desaparición Forzada de Latinoamérica desarrollan encuentro en Colombia
Date: 2017-03-31 12:09
Category: DDHH, Nacional
Tags: Red Latinoaméricana sobre Desaparición Forzada, Víctimas de desaparición forzada
Slug: el-primer-encuentro-de-la
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [31 Mar 2017] 

Del 2 al 6 de abril se realizará en Colombia, el primer encuentro de la Red Latinoaméricana sobre Desaparición Forzada, que busca generar un intercambio de experiencias para aportar al caso colombiano, **en términos de la búsqueda, localización, identificación, restitución con vida, o en caso de fallecimiento de la entrega digna de los cuerpos a sus seres queridos.**

En el evento participaran organizaciones como las Madres de la Plaza de Mayo, H.I.J.O.S del Sur de la Plata, la Fundación de Antropología Forense de Guatemala, el Centro de Derechos Humanos Fray Juan de Larios, el **Movimiento de Fuerzas Unidas por Nuestros Desaparecidos**, la Fundación para la Justicia y el Estado Democrático de Derecho, Desaparecidos Justicia A.C Queretaro, Comité de **Migrantes Desaparecidos de El Salvador y el comité de Familiares Desaparecidos de Honduras**.

De acuerdo con Pablo Cala defensor de DDHH del Colectivo Orlando Flas Borda, lo más importante que se podría aprender en este encuentro es **“la nueva unidad de búsqueda como una instancia, que quite el chip de la judicialización y priorizar la búsqueda de los desaparecidos”** y agrega que también se hace énfasis en la autonomía que necesita la Unidad de Búsqueda de Desaparecidos, que, tras los debates de la JEP, quedó supeditada como organismo.

Este encuentró se realizará en diferentes territorios del país, el evento central se llevará acabo el 5 de abril y será un foro público en el Cementerio Central de Villavicencio llamado **“Sitio de Conciencia y Memoria”** que abordará temas como los nuevos contextos de la desaparición forzada, la lucha contra la impunidad y los retos para las víctimas en el post conflicto. Le puede interesar: ["JEP mantiene abierta la posibilidad para la impunidad: WOLA"](https://archivo.contagioradio.com/jep-mantiene-abierta-la-posibilidad-para-la-impunidad-wola/)

Previamente ya se habían intentado construir espacios como este, ejemplo de ello es la experiencia de FEDEFAM, **Federación Latinoamericana de Familiares de Desaparecidos**, que logró realizar la convención contra la desaparición forzada en las Naciones Unidas. Le puede interesar: ["Corte revisará constitucionalidad de modificaciones del Congreso al Acuerdo Final de Paz"](https://archivo.contagioradio.com/corte-revisara-constitucionalidad-de-modificaciones-del-congreso-al-acuerdo-final-de-paz/)

<iframe id="audio_17887945" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17887945_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
