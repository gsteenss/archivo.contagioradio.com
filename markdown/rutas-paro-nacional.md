Title: Estas serán las rutas del Paro Nacional en Bogotá
Date: 2018-11-08 13:08
Author: AdminContagio
Category: Educación, Movilización
Tags: educacion, estudiantes, Movilidad, Paro Nacional, Trabajadores
Slug: rutas-paro-nacional
Status: published

###### [Foto: @CarlosAmayaR] 

###### [8 Nov 2018] 

Este miércoles, estudiantes, organizaciones sindicales y profesores realizarán una **jornada de paro nacional** con la que buscan que el Gobierno retire su Ley de Financiamiento y asigne mayor presupuesto para superar la crisis de la educación superior pública que afronta el país. En Bogotá, **la movilización tendrá 4 rutas que llegarán a puntos distintos de la Capital**.

Según **Cristian Guzmán, integrante de la Unión Nacional de Estudiantes de la Educación Superior (UNEES)**, la movilización de hoy no buscará masividad sino contundencia; sin embargo, las jornadas de marchas y plantones se tomarán las principales capitales del país con la participación de las 61 Instituciones de Educación Superior (IES) públicas que tiene el país, y de sindicatos como la CUT y Fecode.

Esta **será la primera movilización en la que se articularán otros sectores como el de los trabajadores**, puesto que además de presupuesto para la educación, los estudiantes pedirán que el Gobierno retire la Ley de Financiamiento, que consideran nociva, en tanto busca recursos provenientes del "bolsillo de los colombianos", sin hacer claridad en qué se gastará es dinero.

Por ejemplo, Guzmán tomó el caso de la educación superior, puesto que el Gobierno ha dicho que no está dispuesto a invertir más recursos en este sector, pero sí financiará el programa Generación E, que tiene un costo por año de 900 mil millones de pesos. (Le puede interesar: ["Con movilizaciones estudiantes presionaran al Gobierno para negociar"](https://archivo.contagioradio.com/movilizaciones-estudiantes/))

### **El movimiento estudiantil no está desgastado, lo están golpeando para desgastarlo** 

Guzmán señaló que no es cierto que el movimiento estudiantil esté desgastado, más bien, lo que ha ocurrido es que **lo quieren desgastar "golpeando"** con intervenciones del ESMAD en las IES, el uso de la fuerza por parte de agentes externos (como en la Unipamplona) y mediante la amenaza de suspensión o cancelación de semestres académicos. Por esta razón pidió que se respete la movilización estudiantil, que es un derecho consagrado por la Constitución colombiana.

El activista pidió a los rectores de todas las IES públicas del país dialogar con la comunidad estudiantil, y entablar procesos de concertación sobre la base de los pliegos de exigencias locales. Adicionalmente, el estudiante solicitó que apoyarán las movilizaciones y el paro, pues **lo que de este proceso se logre, será en beneficio de la educación superior pública del país;** y por el contrario, si se levanta el paro sin encontrar soluciones estructurales, el otro año los estudiantes tendrían que volver a movilizarse por las mismas razones.

Por último, Guzmán sostuvo que las jornadas de protesta, cómo la que se llevó a cabo en la Universidad Nacional, son de carácter pacífico, encabezadas por estudiantes y **lo único que buscan es garantizar el "derecho a la educación del pueblo colombiano"**. (Le puede interesar:["Reunión con Consejo Superior de la UPTC definirá continuidad del paro de sus estudiantes"](https://archivo.contagioradio.com/reunion-con-consejo-superior-de-la-uptc-definira-continuidad-del-paro-de-sus-estudiantes/))

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUNEES.COL%2Fposts%2F249490932390170&amp;width=500" width="500" height="737" frameborder="0" scrolling="no"></iframe>

### **Estas serán las rutas de la movilización en Bogotá** 

Universidad Distrital: Todas las facultades llegarán a la sede de la Calle 40 con carrera 7ª (frente a la Universidad Javeriana), y saldrán por la Avenida Caracas hasta la Calle 100.

Universidad Nacional: Saldrán a las 2 p.m. por la Carrera 30 hacía el norte, para encontrarse con la Universidad Distrital en la Calle 100.

Universidad de Cundinamarca, UniMinuto y CUT: Saldrán a las 4  de la tarde por la Autopista Sur y llegarán hasta el parque principal de Soacha.

UPN y otros sindicatos: A las 2 de tarde saldrán por la Carrera 7ª hasta el Planetario Distrital, y se concentrarán definitivamente en la Plaza de Bolivar.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
