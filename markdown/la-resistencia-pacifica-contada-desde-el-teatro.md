Title: La resistencia pacífica contada desde el Teatro
Date: 2016-05-27 15:46
Category: Cultura, eventos
Tags: Bartleby el escribente, teatro en bogota, Teatro Quimera
Slug: la-resistencia-pacifica-contada-desde-el-teatro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/DSC_0631.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Teatro Quimera 

##### [27 Mayo 2016] 

Bajo la dirección del maestro Jorge Prada, el Teatro Quimera de Bogotá presenta del 19 al 28 de mayo 'Bartleby, el escribiente', adaptación teatral de la novela corta de Herman Melville, autor de obras tan importantes de la literatura estadounidense como Moby Dick (1851).

Desde hace aproximadamente 10 años y con más de 300 funciones, la compañía teatral viene presentado este montaje en diferentes temporadas, convirtiéndolo en una pieza emblemática de su repertorio, con la que en 2007 obtuvo la tercera mejor votación en el Festival de Teatro de Bogotá entre 150 obras participantes.

La versión teatral, realizada por  el dramaturgo español José Sanchis Sinisterra, cuenta la historia de un pálido, humilde y silencioso escribiente llamado Bartleby, inmóvil y como varado en una oscura oficina, quien no opone resistencia a las acometidas de su “perseguidor”, el Abogado, su jefe, más razones que una breve frase:“Preferiría no hacerlo”.

La realidad del escribiente, es reflejo de cómo los problemas se pueden confrontar desde la resistencia pacífica, representación que en palabras del maestro Prada, "puede revelarnos cosas muy importantes en momentos como los que actualmente vivimos con el proceso de paz, como podemos hacer que cambie (la realidad) sin recurrir a la violencia".

'Bartleby, el escribiente', es protagonizada por el mismo Prada en el papel del abogado en compañía del maestro Fernando Ospina como su abnegado subordinado, con la paticipación de Diego Zamora y Liliana Cortés como tramoyistas. Compartimos algunos minutos con el director para contarnos más sobre la producción y la trayectoria de más de 30 años del Teatro Quimera.

**Temporada:** 19 al 28 de mayo de jueves a sábado

**Lugar:** Teatro Quimera Cl. 70a \#Calle 70 No. 19 - 40

**Valor entrada:** Precios: General: \$15.000 - Estudiantes: \$10.000
