Title: Estas son las razones de la movilización en Bogotá
Date: 2016-02-25 11:17
Category: Movilización, Nacional
Tags: Alcaldía de Bogotá, Movilizaciones en Bogotá, Plaza de Bolívar
Slug: razones-de-la-movilizacion-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Movilizaciones-ADE-e1456416787217.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Renovación Magisterial 

<iframe src="http://co.ivoox.com/es/player_ek_10571274_2_1.html?data=kpWimZaWe5Whhpywj5WUaZS1kZWah5yncZOhhpywj5WRaZi3jpWah5yncabn1cbgjdjTsozgwtiY1Mbes8%2FZ1JDRx5DQpYzh0NvWzs7epcTdhqigh6eXsozZz5Cv0czTuIa3lIquk5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [William Agudelo, ADE] 

###### [25 Feb 2016]

Desde las 10 de la mañana, educadores, centrales obreras y sindicatos del distrito, se movilizan desde la carrera 30 con calle 26 hasta la Plaza de Bolívar, donde **se entregará a la administración de Enrique Peñalosa un pliego de peticiones,** exigiendo condiciones dignas para los diferentes funcionarios del sector público.

Uno de los ejes centrales de la manifestación es la educación, de acuerdo con William Agudelo, presidente de la Asociación Distrital de Educadores de Bogotá, la ADE, una de las organizaciones que lidera la marcha, son varias las exigencias que los profesores le harán al distrito. **Los maestros piden mejoras en los salarios, no privatización del patrimonio público y mejorar las condiciones para la jornada única de los colegios.**

“Los profesores estamos por la defensa del patrimonio público”, dice Agudelo, quien añade que aunque Enrique Peñalosa había anunciado en su campaña que construiría **50 colegios, la cifra ya disminuyó a 20,** y estos serían entregados a privados.

Así mismo, denuncian que a los docentes les están pagando salarios mínimos y en cambio, les han exigido trabajar más horas tras haberse establecido la jornada única. Además los alumnos deben estudiar en condiciones de hacinamiento pues son hasta 50 estudiantes por salón. **"Estamos buscando que haya una jornada única en condiciones dignas para los alumnos”,** dice el presidente de la ADE.

Otras razones de la movilización tienen que ver con el **alza en la tarifa de Transmilenio; una posible privatización del Sena, la ETB y el Acueducto de Bogotá; disminución en los precios de la gasolina y los impuestos,** entre otros. La jornada del día de hoy se realiza como acción preparatoria para  el Paro Nacional  programado para el próximo 17 de marzo.

La ruta de movilización inició en la Carrera 30 con Calle 26, de ahí continua por la AV El Dorado hasta la carrera 13, luego tomarán la 24 calle para subir hasta la carrera séptima, y finalmente llegar hasta la Plaza Bolívar.
