Title: 180 familias del Pueblo Arhuaco serían desalojadas en Pueblo Bello, Cesar
Date: 2017-07-12 18:12
Category: DDHH, Nacional
Tags: Pueblo Arhuaco, Pueblo Bello, Sierra Nevada de Santa Marta
Slug: pueblo-arhuaco-desalojado-pueblo-bello
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/desalojo-pueblo-bello-cesar-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pueblo Arhuaco] 

###### [12 Jul 2017] 

Por lo menos 180 familias del Pueblo Arhuaco que habitan la Sierra Nevada de Santa Marta, enfrentan hoy una amenaza de un **tercer desalojo por parte del ESMAD en la plaza central del municipio de Pueblo Bello en el departamento del Cesar**. Además de los integrantes del Pueblo Arhuaco otras familias campesinas, en su mayoría desplazados, también serían desalojados.

Los indígenas, entre los que se cuentan más de 170 niños y niñas, arribaron al parque central de “Pueblo Bello” luego de dos desalojos forzados realizados por el ESMAD en el predio conocido como Tikumukin, que es parte del territorio ancestral de la comunidad y es uno de los **sitios sagrados sobre los cuales este pueblo debe ejercer el control para garantizar su supervivencia,** como lo explica Mamo (autoridad tradicional) Arwavikungumu.

Ya desde el 12 de Marzo de este año, la comunidad decidió ocupar el predio que fue asignado de manera inconsulta por el INCORA a un tercero, a pesar de estar entre los límites del territorio ancestral Arhuaco. Según las comunidades indígenas era necesario retomar el control del sitio sagrado, **sin embargo el 31 de marzo el ESMAD arremetió en una primera oportunidad contra los indígenas** obligándolos a abandonar el sitio sagrado momentaneamente. [Lea también: Sierra Nevada amenazada por 348 títulos mineros.](https://archivo.contagioradio.com/sierra_nevada_de_sana_marta_peligro_mineria_/)

Según Juan Vallejo, vocero de la comunidad arhuaca, **"nos han pedido que nos devolvamos a los territorios** que están alejados de la parte urbana sin darnos ninguna solución".  Él manifestó que han sido tratados como invasores porque "no han entendido que lo que estamos buscando es recuperar los espacios sagrados de la comunidad".

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/WhatsApp-Video-2017-07-12-at-17.58.38.mp4\[/KGVID\]

### **Pueblo Arhuaco exige protección para Tikumukin** 

Luego de ese primer intento de desalojo y pasando por encima de una directriz del Ministerio del Interior, en que se aseguraba que la alcaldía no tenía competencia para realizar la diligencia, el 4 de Julio se ejecutó el desalojo en que, según el relato de la comunidad,  **sin presencia de defensoría o alguna institución del Estado y con motosierras y tractores, el ESMAD destruyó lo que había construido la comunidad, casas, centros sagrados, lugares comunitarios y siembras de pan coger**. [Le puede interesar: Colombia ha perdido el 92% de la Sierra Nevada de Santa Marta](https://archivo.contagioradio.com/colombia_pierde_sierra_nevada_santa_marta/)

Luego de que las autoridades tradicionales se reunieran con las autoridades municipales , Vallejo manifestó que no les han dado ninguna solución. Van a seguir intentando crear una ruta de atención donde las comunidades indígenas exigen atención humanitaria de emergencia y un lugar de alojamiento temporal, **mientras que se resuelve definitivamente la situación del sitio sagrado en que ellos y ellas deben habitar y sostener según sus usos, costumbres y tradiciones.**

\

<iframe id="audio_19787438" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19787438_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
