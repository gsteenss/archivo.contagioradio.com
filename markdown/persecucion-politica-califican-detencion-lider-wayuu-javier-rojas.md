Title: Como persecución política califican detención de líder Wayuú Javier Rojas
Date: 2019-04-04 17:22
Author: CtgAdm
Category: Comunidad, Nacional
Tags: asociación shipia wayuu, Guajira, Instituto Colombiano de Desarrollo Rural, Wayuu
Slug: persecucion-politica-califican-detencion-lider-wayuu-javier-rojas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Javier-Rojas-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Asociación Shipia Wayuú] 

###### [04 Abr 2019] 

Comunidades rechazaron la detención de **Javier Rojas Uriana, representante legal de la Asociación Shipia Wayuú,** ocurrida el pasado 31 de marzo por presuntos hechos de corrupción cometidos en el marco de un convenio entre la Asociación y el extinto [**Instituto Colombiano de Desarrollo Rural **(Incoder). La Asociación rechazó las acusaciones y asegura que el proceso jurídico se trataría de una **persecución política** al líder indígena quien ha realizado denuncias en contra del Estado por más de 10 años.]

De acuerdo con la Dirección Seccional de la Guajira, la Fiscalía adelanta una investigación sobre la presunta apropiación de por lo menos **\$1.000’000.000** de un convenio acordado entre la Asociación Shipia Wayúu y el Incoder para la realización de un proyecto de fortalecimiento de las economías tradicionales de 200 familias indígenas pertenecientes a la Asociación. El ente investiga el **posible desvío de los recursos** y **presuntos irregularidades en la ejecución y liquidación de dicho convenio**.

Un juez de control de garantías dictó medida de aseguramiento en contra de Rojas, en su calidad de representante legal de la organización, y actualmente se encuentra detenido. La Fiscalía imputó a Rojas los delitos de [peculado por apropiación, contrato sin cumplimiento de los requisitos legales y falsedad ideológica en documento público.]

### **Los argumentos de la defensa** 

**Carlos Balcazar, abogado de Rojas**, asegura que los recursos se usaron para comprar [lanchas con equipos de pesca, alrededor de 800 chivos y la construcción de apriscos y un centro de acopio de la Asociación Shipia. El defensor sostiene que la Fiscalia señala a la organización por un aporte en especie que el ente investigativo caracteriza como sobrevalorado.]

[En el convenio de **\$2.223’000.000**, el Incoder aportó **\$1.448’000.000** mientras que la Asociación realizó un aporte en especie, que en esta instancia consistía de mano de obra y conocimiento ancestral sobre tradiciones Wayuú como la pesca artesanal, el tejido y el pastoreo de chivos. A este aporte se designó un valor de **\$775’000.000**, acordado en el convenio. Sin embargo, el abogado sostiene que tal valorización está siendo cuestionada por la Fiscalía.]

Por otro lado, Balcazar manifestó que si bien se debería respetar el proceso legal, "la imputación de cargos en contra de Rojas y lo que se viene es preocupante". El abogado indicó que "**hay mucho interés para que Javier salga del camino**" y que desde el inicio de su labor como defensor de derechos humanos, el líder ha sufrido amenazas por su liderazgo. De hecho, Rojas actualmente cuenta con una esquema de la Unidad Nacional de Protección por la gravedad de las amenazas en su contra.

Como integrante de Shipia Wayuú, Rojas [ lleva más de 10 años defendiendo los derechos de su pueblo, en temas como agua potable, nutrición y atención a la niñez. En 2015, el líder presentó una demanda ante la** Comisión Interamericana de Derechos Humanos (CIDH)** contra el Estado colombiano por la mortandad de niños Wayuú a causa de la desnutrición y falta de servicios médicos. En consecuencia, la CIDH otorgó medidas cautelares a 9 mil mujeres de esa etnia. "**Javier Rojas es una piedra en el zapato del Estado colombiano**", aseguró el abogado. ](Le puede interesar: "[CIDH cobija a 9 mil mujeres Wayuú con medidas cautelares](https://archivo.contagioradio.com/cidh-cobija-a-9-mil-madres-gestantes-y-lactantes-con-medidas-cautelares/)")

Frente la detención de Rojas, las autoridades indígenas de Manuare han pedido que su caso sea juzgado por Sistema Normativo Wayuú, porque los delitos fueron supuestamente cometidos en territorio indígena, los presuntamente afectados pertenecen a la etnia y los delitos relacionados a [recursos de administración pública son de competencia de la justicia indígena.]

<iframe id="audio_34099182" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34099182_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
