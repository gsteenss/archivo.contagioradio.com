Title: AngloGold Ashanti anuncia que detiene el proyecto La Colosa en Cajamarca
Date: 2017-04-27 17:52
Category: Ambiente, Voces de la Tierra
Tags: anglogold, ashanti, Cajamarca, Mineria
Slug: anglogold-ashanti-anuncia-que-detiene-el-proyecto-la-colosa-en-cajamarca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Catapa 

###### [27 Abr 2017] 

La victoria de las comunidades de Cajamarca, Tolima, en la consulta popular del pasado 26 de marzo que evidenció un NO contundente a la minería contaminante,  muestra ya sus primeros frutos. La multinacional Anglo Gold Ashanti **informó por medio de un comunicado que decide detener las actividades en La Colosa.**

“Diversas razones que van desde lo institucional, lo político y particularmente lo social con la reciente consulta, nos obligan a tomar **la desafortunada decisión de detener todas las actividades del proyecto y con ello el empleo y la inversión**, mientras se le da certeza a la actividad minera en el país y en el Tolima”, dice el comunicado.[(Le puede interesar: Cajamarca dice NO a la minería)](https://archivo.contagioradio.com/si-se-pudo-pueblo-de-cajamarca/)

Los ambientalistas y habitantes de Cajamarca, celebran este comunicado, pero aseguran que no bajarán la guardia, pues el paso a seguir para que realmente se evidencie que la multinacional y el Estado respetan la decisión de la comunidad, **es que los títulos mineros no sigan vigentes pues hasta que estos no sean retirados por parte del gobierno**, **permanece el riesgo para los ecosistemas de esa zona del país.**

En su comunicado la Anglo Gold Ashanti dice que se detiene “la generación de empleo en la región, el impulso a los proyectos de fortalecimiento agrícola, ganadero y comercial en Cajamarca, el trabajo conjunto con las comunidades y autoridades municipales para promover proyectos en salud, educación, servicios públicos e infraestructura, entre otros, necesarios para cualquier municipio en Colombia”.

Ante ello, los ambientalistas recuerdan que la consulta popular logró todo lo contrario pues **“por ahora están a salvo la economía campesina y sus trabajos, además de las imprescindibles fuentes de agua y la disponibilidad de alimentos que siempre han sido las banderas de las comunidades”.** [(Le puede interesar: Gobierno miente decisión de consulta popular debe cumplirse)](https://archivo.contagioradio.com/gobierno-miente-decision-de-consulta-popular-debe-cumplirse/)

![colosa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/colosa.png){.alignnone .size-full .wp-image-39799 width="756" height="854"}

###### Reciba toda la información de Contagio Radio en [[su correo]
