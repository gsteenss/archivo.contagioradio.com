Title: Consejo de Seguridad de la ONU verificará situación de DD.HH en Colombia
Date: 2017-05-04 16:52
Category: Nacional, Paz
Tags: Acuerdos de paz de la Habana, Consejo de Seguridad ONU
Slug: consejo-de-seguridad-de-la-onu-verificara-situacion-de-dd-hh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/consejodeseguridadonu-e1493934251735.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Panorama] 

###### [04 May 2017] 

Desde el 3 de mayo, está en Colombia el Consejo de Seguridad de las Naciones Unidas con la finalidad de constatar los avances en la implementación de los Acuerdos de Paz. Se espera que con la visita los integrantes del Consejo **se enteren con mayor profundidad de la situación que afrontan los defensores de derechos humanos, la acción del paramilitarismo** y la necesidad de que inicie la segunda misión de la ONU.

En la pasada visita el Consejo de Seguridad hizo serias recomendaciones referente a la **preocupación por los asesinatos de defensores de derechos humanos y la falta de garantías de seguridad** para la reincorporación de los integrantes de las FARC-EP a la sociedad civil. Le puede interesar:"[José Yatacué, integrante de las FARC-EP asesinado en Toribio, Cauca"](https://archivo.contagioradio.com/jose-huber-yatacue/)

Ambas situaciones continúan presentándose al interior del país, en donde hace apenas una semana **asesinaron a Diego Rodríguez, defensor de derechos humanos en el Cauca,a dos integrantes de la guerrilla de las FARC-EP**, beneficiarios de la ley de Amnistía y la familia de otro integrante de esta guerrilla que se encuentra en una zona veredal.

De acuerdo con Mariela Kohon, integrante de la organización Justicia por Colombia, los llamados internacionales generan una mayor presión sobre el accionar del gobierno para crear un “**clima donde se den las garantías jurídicas, de seguridad y socioeconómicas que se necesitan para que el proyecto avance**”.

La segunda misión de la ONU tendría como tarea verificar la reincorporación social, económica y política de los integrantes de las FARC-EP, las garantías de seguridad de este proceso de tránsito y de las comunidades más afectadas, para **evitar la continuación del accionar paramilitar que se ha venido denunciado en los diferentes territorios.** Le puede interesar: ["En Chocó, indígenas y afrodescendientes marchan contra el paramilitarismo"](https://archivo.contagioradio.com/en-choco-indigenas-y-afrodescendientes-marchan-contra-el-paramilitarismo/)

<iframe id="audio_18507342" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18507342_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
