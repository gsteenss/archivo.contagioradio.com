Title: En Octubre han sido asesinados 24 palestinos en territorios ocupados en Cisjordania
Date: 2015-10-13 14:41
Category: DDHH, El mundo
Tags: Agencia de medios alternativos Israel, Conflicto medio oriente, Derechos Humanos, Emisora de derechos humanos, Franja de Gaza, Israel, Jerusalen, Palestina
Slug: en-octubre-han-sido-asesinados-24-palestinos-en-territorios-ocupados-en-cisjordania
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Rtve.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:rtve.com 

###### [13 oct 2015]

**Desde el 1 de octubre la violencia en territorios ocupados han cobrado la vida de 31 personas**: 7 israelíes y 24 palestinos, entre los que se incluyen una decena de agresores abatidos a tiros por las fuerzas de seguridad. Varios dirigentes políticos han planteado como respuesta a esta ola de violencia **desplegar policías en los barrios** del Este de Jerusalén para impedir la libertad de movimientos de los atacantes.

Esta ola de violencia se hace aún más crítica ya que distintos grupo aprueban estos hechos “Hamás bendice los heroicos ataques ocurridos esta mañana en la Palestina ocupada”, además que "Instamos a los jóvenes a continuar y matar a todo israelí en todas partes cuando los vean", generando así mayor tensión entre la población civil.

Saeb Erekat, secretario general de la Organización para la Liberación de Palestina (OLP), indicó que en Ramala que los palestinos "tienen derecho a defenderse", en medio de la oleada de violencia que vive la zona y **denunció lo que denominó "ejecuciones extrajudiciales de niños palestinos"** por parte de Israel.

El día de hoy Jerusalén **sufrió la jornada más sangrienta de ataques entre Israel y Palestina** en la denominada “**jornada de la ira**” convocada por los palestinos este martes contra el aumento de visitas de judíos a la Explanada de las Mezquitas.

En el hecho se presentó **un ataque con armas de fuego** en el que al menos tres israelíes han muerto, dos en ataques de jóvenes armados y otro que falleció atropellado por un conductor árabe en la Ciudad Santa. Por otra parte 30 israelíes resultaron heridos en un total de cuatro ataques, si se incluyen los dos cometidos por jóvenes palestinos en la ciudad de Raanana, cerca de Tel Aviv.

 
