Title: Guardia campesina en Caquetá, una apuesta por la vida y el territorio
Date: 2018-12-04 15:01
Author: AdminContagio
Category: Comunidad, Paz
Tags: Caquetá, etcr, guardia campesina
Slug: guardia-campesina-caqueta-el-pato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/WhatsApp-Image-2018-12-03-at-3.39.07-PM1-e1543946616941-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AMCOP 

###### 4 Dic 2018 

En la **Zona de Reserva Campesina Cuenca del Río Pato y Valle de Balsillas**, zona de colonización en el Caquetá, hombres y mujeres de diferentes edades, se graduaron de la **escuela básica Humberto Moncada**, en temas de territorio, organización social, derechos humanos, resolución de conflictos y cuidado del ambiente, que **servirá como impulso a lo que será la Guardia Campesina de la zona**.

La escuela, que lleva el nombre de uno de los primeros líderes desaparecidos en la región, **formó durante 128 horas a los pobladores provenientes de las veredas que conforman el territorio**, culminando su proceso el pasado 1ro de diciembre en acto que tuvo lugar en la vereda Mira Valle.

![guardia campesina](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/WhatsApp-Image-2018-12-03-at-3.39.07-PM-1-800x449.jpeg){.alignnone .size-medium .wp-image-58887 .aligncenter width="800" height="449"}

Mauricio Quesada uno de los coordinadores aseguró que con la iniciativa de formación “empezamos nuevamente a recorrer el territorio y junto con la olla comunitaria volvimos a movernos desde las veredas de Balsillas hasta Mira Valle, recorriendo lugares principales en la región y siempre con un plato de comida, de nuestros propios productos, al que quisiera y pudiera llegar a las formaciones”.

La apuesta pedagógica esta enmarcada en el **proyecto de reincorporación Ambientes para la Paz, Vida digna y la reconciliación**, y es impulsada por la **Asociación Municipal de colonos del Pato, AMCOP,** organización desde la cual aseguraron que esta figura tiene años de funcionamiento en el territorio desde los primeros colonos que debían generar vínculos de confianza mutua para protegerse ante las adversidades; y que poco a poco se fueron reforzando con la creación de las juntas, las asociaciones y con ellas las normas de convivencia.

La escuela se desarrolló retomando aspectos importantes para la vida campesina, en cuanto al desarrollo de la confianza y la unidad, “**Quisimos empezar con este grupo de impulso para legitimar esta figura civil de seguridad del territorio y de la vida, que tiene como objetivo velar por los intereses y la autonomía campesina,** por la implementación integral de la reforma agraria” así lo manifestó otro de los integrantes y participantes al encuentro.

Al espacio también llegaron diferentes actores de la fuerza pública, la ONU, asociados de la zona baja del pato e integrantes del Espacio Territorial de Capacitación Reincorporación (ETCR) Oscar Mondragón así como de comunidad en general que dio su apoyo a la creación y socialización de la figura en el territorio.

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio. 
