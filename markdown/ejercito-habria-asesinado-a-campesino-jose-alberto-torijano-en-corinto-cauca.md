Title: Video comprobaría que Ejército habría asesinado a campesino en Corinto, Cauca
Date: 2017-09-29 16:30
Category: DDHH, Nacional
Tags: Cauca, Corinto, Ejército Nacional
Slug: ejercito-habria-asesinado-a-campesino-jose-alberto-torijano-en-corinto-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/militares-e1506032994531.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Confidencial 

###### [29 Sep 2017]

Según información entregada por el Movimiento Marcha Patriótica, José Alberto Torijano, coordinador de la guardia campesina de Corinto en Cauca habría sido asesinado por integrantes del ejército que habían llegado a esa zona del Cauca. Además, en los hechos **habrían resultado heridos Jhonatan Hernandez y Alexander Guevara.**

**José Alberto era coordinador de la Guardia Campesina del corregimiento de Río Negro** y además era integrante del Movimiento Marcha Patriótica que tiene amplia presencia en el departamento. Los otros jóvenes serían integrantes de la comunidad que acudieron al llamado de movilización tras la retención de los dos pobladores.

La denuncia relata que los hechos se presentaron en la madrugada de este jueves cuando los militares pertenecientes al **batallón de alta montaña Número 8 arribaron a la zona rural del municipio, en la vereda Media Naranja**, en medio del operativo habrían detenido a dos integrantes de la comunidad y ante el reclamo de los habitantes del sector abrieron fuego.

Cristian Hurtado de la Comisión de Derechos Humanos de Marcha Patriótica dijo que lo que les habían contando los campesinos "es que desde la parte alta donde estaba un grupo de militares, comienzan a disparar ráfagas y justo en ese momento los militares que estaban hablando con los campesinos comienzan a disparar a los pies de la gente. Nos han dicho que los militares estaban consumiendo marihuana y algunos sustancias psicoactivas".

### **José Adalberto falleció camino a la clínica** 

Cuando los campesinos se percataron de la crítica situación, llevaron a José Adalberto a Corinto, pero debido a la gravedad de las heridas se decidió trasladarlo a Cali, pero en el camino falleció. Además de José, otros 3 campesinos resultaron heridos.

"Pese a que los campesinos les decían a los militares que había población civil herida ellos  - los militares -continuaron disparando".

Las personas que resultaron heridas producto de los golpes que les propinaron con las culatas de las armas, dos ya fueron dadas de alta y 1 más fue remitida a la Clinica Valle de Lili para agilizar su recuperación.

### **¿Qué pasó con los militares?** 

En un primer momento y ante la situación presentada cerca de 700 campesinos se reunieron en la zona para rodearlos e impedirles la salida de la zona, hasta tanto arribó una comisión de la personería y el CTI que realizó el levantamiento del cadáver y estaría iniciando una investigación de los hechos.

"El grupo de militares se retira a la base militar. La versión que está manejando el Ejército como siempre es que fueron atacados y respondieron en legitima defensa. Estamos solicitando que este grupo de mas o menos 100 militares sean retenidos y se proceda al tema de la investigación para dar con los responsables".

Este es el video difundido en la cuenta de Facebook de Andrés Gil, dirigente de Marcha Patriótica

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/22005985\_368802033553477\_5042588327614611456\_n.mp4\[/KGVID\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
