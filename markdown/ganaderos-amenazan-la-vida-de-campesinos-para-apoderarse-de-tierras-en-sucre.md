Title: Ganaderos amenazan la vida de campesinos por apoderarse de tierras en Sucre
Date: 2015-06-04 15:57
Category: DDHH, Nacional
Tags: baldios, cordoba, David Andrés Gomescásseres, despojadores de tierras, INCODER, MOVICE, Movimiento de víctmas de crímenes de Estado, paramilitares, Restitución de tierras, Sucre
Slug: ganaderos-amenazan-la-vida-de-campesinos-para-apoderarse-de-tierras-en-sucre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/DSC09654.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [elpueblo.com.co]

<iframe src="http://www.ivoox.com/player_ek_4596532_2_1.html?data=lZqmmJqXdo6ZmKiak5WJd6Kml5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi8LiwsnS1NTXb8LhxtPO3MbSb83VjNvWxsaPqMafxMba0srXrc%2Fj1JDdw9fFb8Lk0MnS1MbWt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ingrid Vergara, MOVICE Sucre] 

En el departamento de Sucre, en la Villa de San Benito de Abad Ciénaga de Sispataca, el Movimiento de Víctimas de Crímenes de Estado, denunció que el pasado 21 de mayo, **cuatro  hombres armados ingresaron a ese lugar y rompieron la cerca que protegía los cultivos, y los destruyeron.**

La comunidad enfrenta una fuerte lucha contra **ganaderos que se han apropiado de los baldíos,** por lo que los campesinos hoy viven **una grave crisis alimentaria,** sumado a que tradicionalmente, las comunidades viven de la pesca pero la minería ha contaminado el río y las especies de peces han desaparecido.

Los pobladores aseguran que **los ganaderos han contratado un grupo de hombres para intimidar a la comunidad reclamante de tierras.** De hecho, muchos campesinos dicen que estas personas las contrataron "para darle por la cabeza a dos o tres campesinos y a los que están detrás de ellos". Un situación preocupante para Ingrid Vergara, integrante del MOVICE, quien asegura que **la vida de líderes campesinos y miembros del MOVICE está en peligro.**

De acuerdo a la integrante del MOVICE, en la región de la Mojana y el San Jorge, la organización viene acompañando procesos que tiene que ver con baldíos y playones que pertenecen al Estado, según el mismo INCODER lo ha afirmado, sin embargo, esas tierras que deben estar a disposición de la población, están en poder de ganaderos de Sucre y Córdoba, quienes **han usado la violencia, asesinando familias enteras, destruido viviendas y  cultivos de los campesinos, para apropiarse de los baldíos de forma ilegal.**

Aunque el pasado mes de mayo se llevó a cabo una audiencia en defensa de la comunidad en San Jorge, los puntos a los que se había comprometido las autoridades no se han cumplido; en cambio, han aumentado las acciones violentas que atentan contra la integridad de los **campesinos, que además han sido acusados por allanamiento a la propiedad privada.**

Así mismo, según Vergara, a pesar de que existen investigaciones donde se vincula a varios de los ganaderos que estarían detrás de estas acciones y asesinatos, **como Nicolás Bula, del departamento de Córdoba** que se apoderó de baldíos, y como el anterior director del Incoder de Sucre, **David Andrés Gomescásseres**, aun así los procesos no avanzan y "las autoridades locales no resuelven la situación", por lo que se ha pedido que los casos se radiquen en una unidad de derechos humanos.

Cabe recordar, que los campesinos han identificado a los hombres que los han atacado, y según el MOVICE, serían parte de una estructura paramilitar en la zona. El problema es que no se ha prestado atención a las investigaciones y **hay poco acompañamiento del Estado para visibilizar situaciones que sucedieron a manos del Bloque Mojana.**
