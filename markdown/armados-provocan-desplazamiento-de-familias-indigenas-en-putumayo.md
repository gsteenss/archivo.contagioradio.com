Title: Armados provocan desplazamiento de familias indígenas en Putumayo
Date: 2015-05-11 13:36
Author: CtgAdm
Category: DDHH, Nacional
Tags: ACIN, Consejería Nasa del Putumayo, Gran Tierra Energy, Orito, Paramilitarismo, Pueblo Nasa, Putumayo
Slug: armados-provocan-desplazamiento-de-familias-indigenas-en-putumayo
Status: published

###### foto: [putumayocomunicacionescu.wordpress]

En la noche del pasado 7 de Mayo un grupo de 15 hombres vestidos de camuflado y portando armas largas, incursionó en un asentamiento del pueblo indígena **Nasa en la vereda Bellavista del Municipio de Orito en Putumayo**. Según la denuncia de la comunidad, los armados se dirigieron a la casa del profesor indígena, lo amenazaron y lo obligaron a huir con múltiples ráfagas de fusil y el lanzamiento de granadas. Tras la huida del profesor incendiaron la casa indígena.

Las familias del **Pueblo Nasa** se vieron obligadas a desplazarse de su sitio de habitación y hasta el momento solo algunas de las familias han podido retornar a pesar del miedo y la zozobra.

De este caserío hacen parte cinco familias, 6 mujeres, 5 hombres, 11 niñas y niños, algunas familias hacen parte del grupo de víctimas que están siendo reparadas por la masacre del Nilo, Cauca, en un proceso de acompañamiento entre la **ACIN y la Consejería Nasa del Putumayo**; a su vez esta parcialidad hace parte del cabildo Nasa Tkuymatewe'sx.

Desde hace algunos meses las comunidades habitantes de ese territorio han manifestado su preocupación por la línea sísmica que la empresa **Gran Tierra Energy**, pretende atravesar de manera inconsulta. La comunidad también denunció que existen indicios de posibles ataques violentos que desde el pasado 2 de Mayo se podrían presentar por la decisión de la comunidad de no permitir la entrada de las empresas a sus territorios por las graves afectaciones que puede causar la actividad extractiva de petróleo o minerales.
