Title: Mujeres cocaleras entregan 14 puntos mínimos para sustitución de cultivos
Date: 2017-05-18 16:54
Category: Mujer, Nacional
Tags: coca, cocaleras, Cultivos de coca, hoja de coca, Putumayo, sustitución
Slug: mujeres-cocaleras-entregan-14-puntos-minimos-para-sustitucion-de-cultivo-de-coca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/familias-cocaleras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [18 May. 2017] 

*“Usted la coca la echa, una libra o un kilo, en un bolso y sale dos o tres horas y le echa (camina). Una no se cansa. Pero échese un bulto de yuca o de plátano a ver…no lo carga ni diez metros y lo bota, porque no se aguanta. En cambio,* ***uno de mujer hasta cuatro kilos de coca carga”*,** así lo asegura una mujer cocalera de Putumayo a propósito de las realidades que viven y a las que podrían verse expuestas de no realizar una sustitución de cultivos de coca con enfoque de género.

Luego de un encuentro de dos días, celebrado el pasado mes de marzo en Putumayo, las mujeres cocaleras de Caquetá, Cauca, Meta, Nariño y Putumayo realizaron la entrega de **14 puntos que consideran son los más importantes al implementar los puntos 1 y 4 del Acuerdo de Paz**. Le puede interesar: [Familias Cocaleras ya tienen Plan de Sustitución de Cultivos](https://archivo.contagioradio.com/familias-cocaleras-ya-tienen-plan-de-sustitucion-de-cultivos/)

Las mujeres han pedido que se haga pedagogía de lo acordado en La Habana, para que de tal manera todas las comunidades directamente involucradas puedan tomar las decisiones y posiciones de acuerdo a lo pactado entre el Gobierno y las FARC, pero **también teniendo en cuenta sus realidades diarias y su condición de mujer**. Le puede interesar: [Erradicación de cultivos de uso ilicito van en contra de Acuerdo de Paz](https://archivo.contagioradio.com/erradicacion-de-cultivos-hecha-por-el-gobierno-iria-en-contra-de-acuerdo-de-paz/)

### **Las mujeres cocaleras no son narcotraficantes** 

Así mismo, aseguran que no permitirán que se les de trato de narcotraficantes a quienes tienen relación con los cultivos de coca y actividades relacionadas y solicitan que “las mujeres que están en las cárceles dentro y fuera del país, por su relación con la economía de la coca, sean beneficiarías de excarcelación y cesación de procesos penales”. Le puede interesar:["Comunidades del Putumayo insisten en plan de sustitución a pesar de erradicación forzada"](https://archivo.contagioradio.com/comunidades-del-putumayo-insisten-en-plan-de-sustitucion-a-pesar-de-erradicacion-forzad/)

Insistieron en que es necesario **promover y apoyar la participación de las mujeres en todos los espacios de toma de decisiones, **para que de tal modo también puedan fortalecerse como organizaciones de mujeres y apoyar la transferencia de herramientas de incidencia en política, tal y como lo establece el punto 2 del Acuerdo de Paz. Le puede interesar: [Mujeres cocaleras insisten en la sustitución de cultivos con enfoque de género](https://archivo.contagioradio.com/mujeres-cocaleras-insisten-en-la-sustitucion-de-cultivos-con-enfoque-de-genero/)

Otro de los puntos importantes que destacan las mujeres, es que los procesos de sustitución de cultivos tengan en cuenta a todas las mujeres que se encuentran inmersas en este tipo de economía y que con ello **se haga la respectiva titulación de tierras y la promoción e impulso de una economía territorial** “desde la economía del buen vivir”.

### **La sustitución de cultivos de uso ilícito no se puede improvisar** 

Por lo cual consideran que **se debe financiar proyectos pilotos de transformación de alimentos que incluya la gestión y el pago del registro ante el INVIMA** para ensayar algunos productos, dentro de los cuales mencionan el chontaduro, la sacha inchi, la moringa, pimienta, cacao y la piña.

De modo que se apoye el emprendimiento social, cultural y económico de las mujeres, de modo que se garantice el enfoque diferencial.

Por último, hicieron una llamado al Estado para que **no inicie la sustitución de cultivos hasta tanto hayan sido cumplidas las demandas que las mujeres** cocaleras y raspachinas exigen y que además también se encuentran consignadas en el Acuerdo de Paz.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
