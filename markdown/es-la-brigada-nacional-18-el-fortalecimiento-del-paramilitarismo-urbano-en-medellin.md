Title: ¿Es la Brigada Nacional 18 el fortalecimiento del paramilitarismo urbano en Medellín?
Date: 2019-01-30 16:38
Author: AdminContagio
Category: DDHH, Educación
Tags: Antioquia, neoparamilitares
Slug: es-la-brigada-nacional-18-el-fortalecimiento-del-paramilitarismo-urbano-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Amenazas-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Aguacatehumanotv 

###### 30 Ene 2019 

[Con la aparición de panfletos y pendones atribuidos a un grupo denominado Brigada Nacional Anticomunista o Brigada 18, el pasado 22 de enero amenazando  a estudiantes de la **Universidad de Antioquia**  señalándoles de hacer parte de la guerrilla, los campus de las universidades estarían atestiguando la consolidación de una expresión urbana de violencia que ya se ha visto en esta región desde inicios de la década de los noventa.]

Para el **analista de conflicto urbano, Luis Fernando Quijano** se trata de una expresión urbana que ha cobrado fuerza tras el atentado ocurrido en Bogotá contra la Escuela de Cadetes. El experto advierte que no solo se han difundio panfletos y mensajes neonazis en las calles sino que por medio de las redes sociales dichos grupos pueden “atraer a más adeptos” que compartan sus mismos intereses y pasen de la propaganda a un frente de guerra urbano contra el ELN.

[Quijano sugiere que grupos como la Brigada Nacional 18 **podrían ser utilizados como “una punta de lanza” por sectores de la institucionalidad **tal** **como ya se ha visto con anterioridad con grupos como Los Pepes, para  perseguir a quienes defienden la implementación de los acuerdos de paz y abogan por el proceso de paz con el ELN, un escenario en el que quienes corren más riesgo son los defensores de derechos humanos y los líderes sociales. [(Lea también: Con panfletos y carteles amenazan a estudiantes de la U. de Antioquia)](https://archivo.contagioradio.com/con-panfletos-y-volantes-amenazan-a-estudiantes-de-la-u-de-antioquia/)]

[De igual forma, señala que expresiones de odio como las que se presentaron durante las marchas del pasado 20 enero y que fueron difundidas a través de redes sociales como "te quitas esa camiseta o te pelamos", son una forma de presión que se busca imponer por parte de un sector de ciudadanía que considera el camino de la guerra como el más propicio y que fortalecen el accionar de dichas organizaciones.]

<iframe id="audio_32002779" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32002779_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
