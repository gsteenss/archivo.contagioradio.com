Title: Exparamilitares dicen sí a la verdad
Date: 2020-09-28 15:22
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Álvaro Leyva, Carlos Antonio Moreno Tuberquia, Exparamilitares, Macaco, Martín Llanos
Slug: exparamilitares-dicen-si-a-la-verdad-para-reparar-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Exjefes-paramilitares-responde-a-Alvaro-Leyva.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Los exparamilitares Carlos Mario Jiménez, en tiempos de guerra alias “Macaco”; Carlos Antonio Moreno Tuberquia y Héctor Germán Buitrago, en tiempos de guerra alias “Martín Llanos” ; dirigieron al ex ministro Álvaro Leyva, sendas cartas en las que **expresaron su compromiso con la verdad y las víctimas del conflicto armado, dando respuesta a la misiva de Leyva en la que los invitaba a continuar trabajando por las víctimas y la paz de Colombia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Héctor Germán Buitrago, quien lideró las Autodefensas Campesinas del Casanare -ACC-, también conocidas como “Los Buitragueños”, habló en nombre suyo y de su padre Héctor José Buitrago, alias “El Viejo” y su hermano Nelson Orlando Buitrago, alias “Caballo”, también comandantes del grupo;  expresando que accedieron al Sistema Integral de Verdad, Justicia, Reparación y No Repetición **–**SIVJRNR**-**, y que esperan comparecer ante la Jurisdicción Especial para la Paz **-**JEP**-**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, **hicieron hincapié en la importancia de que la verdad no solo se dé en las instancias de sometimiento, sino que sea un ejercicio «*público, de cara a las víctimas y al alcance de todo el país*».**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **«**\[Si la verdad\] no se conoce, si no llega  a las víctimas, si no se trasciende no puede haber reconciliación, ni mucho menos reparación. La verdad pública es la puerta que conduce a un nuevo país, la verdad ayuda a superar la actual violencia, \[…\] la verdad airea, la JEP la fortalece, la impulsa y permite que las víctimas puedan hacerle sentir a sus deudos, vivos y muertos, que su sacrificio ha sido compensado****»****
>
> <cite>Aparte de la carta dirigida por los exjefes paramilitares Hector Germán, Hector José y Nelson Buitrago</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Adicional a la misiva firmada por los Buitrago, **el exjefe paramilitar Carlos Mario Guerra, alias “Macaco” también dirigió una carta en la que manifestó su deseo de que se le escuche en la JEP para dar a conocer todos los hechos del conflicto que le constan como autor y así poder expresar su arrepentimiento y deseo de reparación con las víctimas como garantía de no repetición.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> No busco venganza, ni daño para nadie, así la sociedad colombiana esté llena de cómplices de la violencia. En algún momento pude haber sido víctima del conflicto, pero como victimario que fui asumo mis responsabilidades. Es mi obligación comparecer. Pienso en Colombia, \[…\] en las víctimas, y en la no repetición.
>
> <cite>Carlos Mario Guerra, exjefe paramilitar de las AUC.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Frente a la solicitud formal de Carlos Mario Guerra, la JEP resolvió rechazar su sometimiento a esa solicitud argumentando que el Tribunal competente era Justicia y Paz. No obstante, aún queda la posibilidad de someterse bajo la figura de tercero civil, como lo han intentado otros excomandantes de las AUC, como Salvatore Mancuso. (Le puede interesar: [La frustrante JEP](https://archivo.contagioradio.com/frustrante-jep-mancuso/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A las dos cartas anteriores, se sumó la comunicación dirigida por **Carlos Antonio Moreno Tuberquia**; quien en un mensaje expresó las causas que lo empujaron al conflicto siendo apenas un niño de 14 años, reclutado forzosamente por la guerrilla del EPL.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Moreno Tuberquia denunció que tras la desmovilización de ese grupo armado, **se ejecutó un plan para ponerlo a él y a sus compañeros de insurgencia, en manos y al servicio del extinto jefe paramilitar Carlos Castaño**. Hecho que, según Moreno, fue orquestado por el «*Gobierno de turno*» en el que fue tan evidente la injerencia, que se utilizaron «*helicópteros de la Gobernación de Antioquia*».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El exparamilitar, también denunció el exterminio del que fueron víctimas muchos de los excombatientes que depusieron las armas en el Acuerdo de Santafe de Ralito, firmado por las AUC con el gobierno de Álvaro Uribe, señalando que **entre los años 2005 y 2008 fueron «*asesinados más de 3.000 desmovilizados*» sin que el Estado hubiese investigado nada al respecto.** (Le puede interesar: [«Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> El proceso de Paz con las autodefensas hubiera sido exitoso, pero la extradición de los comandantes de las AUC para ponerles la mordaza y así impedirles el relato de la verdad al país en los tribunales de Justicia y Paz, junto con la persecución a los desmovilizados para asesinarnos y evitar que llegáramos a contar los hechos cómo sucedieron, fue el caldo de cultivo para que muchos nos reorganizáramos. \[…\] Cómo y por qué terminé en el Clan del Golfo es algo que quisiera explicar en detalle.
>
> <cite>Carlos Antonio Moreno Tuberquia, exjefe paramilitar y miembro del Clan del Golfo</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Salvatore Mancuso y Rodrigo Londoño ya habían acogido el llamado de Leyva

<!-- /wp:heading -->

<!-- wp:paragraph -->

En su momento el exjefe paramilitar Salvatore Mancuso también expresó su voluntad de aportar a la verdad mediante una [carta](https://www.justiciaypazcolombia.com/carta-de-salvatore-mancuso-a-alvaro-leyva/) remitida el exministro Leyva; como también lo ha hecho desde su sitio de reclusión en Estados Unidos, buscando su acogida ante la Jurisdicción de Paz. (Lea también: [La JEP le cierra la puerta a Salvatore Mancuso](https://archivo.contagioradio.com/la-jep-le-cierra-la-puerta-a-salvatore-mancuso/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el mismo sentido, Rodrigo Londoño reafirmó en su momento su deseo de contribuir con la Paz a través de una carta dirigida a Leyva acogiendo su llamado. Asimismo, él y otros excomandantes de FARC pidieron perdón a las víctimas, en particular, aquellas afectadas por el delito de secuestro. (Lea también: [FARC pidió perdón a las víctimas en un gesto de paz](https://archivo.contagioradio.com/farc-pidio-perdon-a-las-victimas-en-un-gesto-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
