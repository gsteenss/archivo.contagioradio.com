Title: Hay que salir masiva, enérgica y creativamente para contrarrestar el miedo
Date: 2019-11-19 15:57
Author: CtgAdm
Category: Movilización, Paro Nacional
Tags: Allanamientos, DDHH, derecho a la protesta, Militarización, movilización paro, Paro Nacional
Slug: hay-que-salir-masiva-energica-y-creativamente-para-contrarrestar-el-miedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/paro-nacional-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Frente al aumento de la **militarización y a los allanamientos irregulares** en varias ciudades días previos al **paro nacional del próximo 21 de noviembre**, defensores de Derechos Humanos y organizaciones sociales que apoyan esta movilización se reunieron en la Asociación Distrital de Educadores para respaldar y defender el derecho a la protesta social y hacer un llamado al gobierno de Duque para que ofrezca las garantías que permitan que la marcha se desarrolle de manera pacífica.

### **Militarización, una respuesta al paro ** 

La orden del pasado lunes 18 de noviembre, emitida por el Comandante General de las Fuerzas Armadas,  decreta el acuartelamiento de primer grado antes del paro nacional y que las tropas se concentren "en máximo estado de alerta". Esta medida, que entró en vigor el pasado lunes 18 de noviembre, a las 6 de la mañana, supone la disponibilidad de las tropas ante cualquier requerimiento.

**Andrés Felipe Valencia, abogado defensor de Derechos Humanos**, advirtió que “La militarización desatada en las calles de Bogotá hace prever que el gobierno Nacional está constituyendo una tragedia contra los dirigentes sociales y los miembros del movimiento social”.

En esa misma línea, en el encuentro, los defensores de derechos humanos subrayaron que las Fuerzas Militares no son un cuerpo civil que establezca diálogo e interlocución ante la inconformidad social, sino que están para desarrollar un tipo de fuerza letal que hace parte del conflicto armado.

### **Allanamientos irregulares** 

A lo largo de esta mañana, 19 de noviembre, se producieron **varios allanamientos irregulares en distintas ciudades del país como** Bogotá, Cali y Medellín, en contra de defensores de derechos humanos, integrantes de colectivos artísticos y estudiantiles (Le puede interesar: "[Allanamientos a colectivos y jóvenes promotores del Paro Nacional](https://archivo.contagioradio.com/allanamientos-a-colectivos-y-jovenes-promotores-del-paro-nacional/)")

Asimismo se denunciaron las capturas de dos personas, que según la Fiscalía, entre el material probatorio que recopilaron durante los allanamientos tenían elementos como tuercas, puntillas, bufandas o pañoletas, overoles, pasamontañas, afiches, botellas de vidrio, que servirían para generar disturbios. No obstante, defensores de derechos humanos dejaron en claro que estos materiales se encuentran en el hogar de cualquier persona y que eso no los hace delincuentes.

**Karen Vanegas,** delegada de Ciudad Movimiento e integrante del **Congreso de los Pueblos** denunció que “las detenciones arbitrarias han sido a personas que lo único que han hecho es promover el paro nacional como el ejercicio al derecho a la movilización y a la libre asociación. Ese tratamiento de criminalización a la protesta social y a la movilización nos ponen en estado de alerta”.

### **“Frente al miedo, la tarea es colectiva, serena y multitudinaria”** 

En la rueda de prensa también se hizo un llamado a la comunidad nacional e internacional y a la Defensoría del Pueblo para que contribuyan a **defender el derecho a la protesta social**. Asimismo, se pidió al Coordinador Nacional del Sistema de Alertas Tempranas que se emita una alerta para proteger el derecho al paro.

“**Estamos convocando a toda la ciudadanía a que se movilice sin temor**. Las organizaciones sindicales y sociales convocamos a que se respete la movilización pacífica, la protesta social y que este gobierno garantice la seguridad para aquellos que reclaman sus derechos con justa causa” declaró Leonardo Arguello Sanjuan, presidente del Sindicato de trabajadores de ETB.

Igualmente, **Ángela María Robledo**, lideresa política, se muestró optimista ante el incremento de las movilizaciones en Colombia y lo que pueda comportar este paro nacional del 21 de noviembre: “quizás esto puede alentar a mediano plazo a una manifestación más duradera con resultados contundentes como parece que lo va a tener Chile” y aseguró que “**frente al miedo, la tarea es colectiva, serena y multitudinaria**”.

### **Recomendaciones para marchar** 

** **Durante el encuentro, defensores de derechos humanos también proporcionaron algunas recomendaciones para los ciudadanos que van a salir a marchar:

-   Al inicio de la marcha, identificar dónde están ubicadas las personas defensoras de derechos humanos, que suelen distinguirse con carnets o con chalecos. De esta manera, ante cualquier situación de alerta, pueden dirigirse a ellas.

<!-- -->

-   Si se tiene la sospecha de que hay infiltrados en la marcha, es apropiado fotografiar, tomar un registro digital o avisar a alguno de los/as defensores/as de DD.HH.

<!-- -->

-   Se recomienda tener el número de contacto en físico, ya que la policía, en ocasiones, y de manera irregular e ilegal puede hacer confiscación de éstos.

<!-- -->

-   En caso de detención arbitraria, deben identificarse, gritando su nombre completo y su cédula, de tal forma que quiénes le rodeen sepan a quién se están llevando y que puedan advertir a algún/a defensor/a de derechos humanos.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
