Title: Nuevo round entre promotores de la revocatoria a Peñalosa y el CNE
Date: 2018-04-05 13:21
Category: Movilización, Nacional
Tags: Alcaldía de Bogotá, Enrique Peñalosa, revocatoria
Slug: nuevo-round-entre-promotores-de-la-revocatoria-a-penalosa-y-el-cne
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/revocatoria_Peñalosa-e1504722871558.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unidos Revocaremos a Peñalosa] 

###### [05 Abr 2018] 

Un fallo del Tribunal Administrativo de Cundinamarca podría resucitar el proceso de revocatoria contra el alcalde Enrique Peñalosa, debido a que le ordena al Consejo Nacional Electoral un plazo de **15 días para que de un veredicto sobre los dineros que financiaron la campaña contra la revocatoria y finalmente las presente a Registraduría General de la Nación**.

De cumplirse los plazos, ordenados por el Tribunal, aún se tendrían los tiempos estipulados para convocar a una revocatoria. Sin embargo, estaría sujeta a que se realice en la fecha estipulada para la segunda vuelta de las elecciones presidenciales el  17 de junio. (Le puede interesar: ["No hay excusa para seguir dilatando la revocatoria: Unidos Revocaremos  Peñalosa")](https://archivo.contagioradio.com/no-hay-excusa-para-dilatar-revocatoria/)

De acuerdo con Carlos Carrillo, vocero del comité promotor de la revocatoria, esta noticia es recibida con “moderado optimismo” para quienes continúan trabajando en la activación de este mecanismo y aseguró que poco a poco se **ha ido vetando jurídicamente los obstáculos que ha puesto el CNE para no dejar realizar la convocatoria**.

“El Tribunal obliga al CNE a que diga si se violaron los topes o no, y después de eso debe emitir su decisión, no le da cabida a ninguna otra estrategia” afirmó Carrillo **y agregó que sí el CNE no cumple con emitir su fallo en los 15 días, cometerá prevaricato.**

### **Los impedimentos del CNE para permitir la revocatoria** 

Desde el 20 de junio de 2017 se entregaron las más de 470 mil firmas a la Registraduría, que fueron avaladas para continuar con el proceso, no obstante, un mes antes, el Consejo Nacional Electoral anunció que había irregularidades en las cuentas de dineros de los comités de la revocatoria a Peñalosa.

Han pasado 9 meses, y el CNE continúa sin manifestarse, por un lado, sobre cuáles son esas irregularidades en las cuentas y por el otro sobre si finalmente da vía libre o no a la revocatoria, **hecho que para los comités se ha convertido en la mejor herramienta para frenar el proceso democrático y favorecer los intereses de Peñalosa**.

<iframe id="audio_25133420" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25133420_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
