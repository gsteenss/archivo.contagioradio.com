Title: ¿Cuál es el temor a que rockero chavista toque en Rock al parque?
Date: 2017-05-12 12:47
Category: Cultura, Nacional
Tags: Bogotá, Paul Gillman, Rock al parque, Venezuela
Slug: paulgillman-rockalparque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/DSC3551-copy.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Globovivsión 

###### 12 May 2017 

La decisión del Instituto Distrital de las Artes IDARTES  de **retirar del cartel oficial del Festival Rock al parque** 2017, al veterano rockero venezolano **Paul Gillman**, ha sido interpretada desde diferentes colectivos como una censura abierta por la simpatía que ha manifestado el artista hacia el **chavismo** en su país.

Tras conocer la decisión, el colectivo de educación y comunicación alternativa Brecha difusión, compartió una comunicación en la que rechaza la posición del Instituto, encabezado por el alcalde Enrique Peñalosa, asegurando que desde esa administración su participación fue leída como "**un factor que radicalizaría las consignas anticorrupción que abandera la actual revocatoria en la capital**" Le puede interesar: [Enrique Peñalosa: El alcalde del conflicto](https://archivo.contagioradio.com/enriquepenalosarevocatoria/) .

En el texto, el colectivo expresa además que el veto al rockero, no esta exento de la influencia que los grandes medios de información en Colombia tienen sobre la imagen  de lo que se vive en Venezuela, a quienes en su opinión **no les convendría que se abra un espacio donde miles de jóvenes podrían escuchar una versión diferente de la realidad**.

La noticia del retiro del artista, la dio a conocer el portal de comunicación Noisey, donde se menciona que IDARTES había justificado su decisión asegurando que el nivel musical de Gillman no era suficiente para estar en el Festival y que su invitación se dio por una supuesta petición del público.

Sin embargo, desde el mismo portal musical se referencia la posible presión que pudo haber ejercido la campaña que personajes como el productor **Julio Correal**, uno de los fundadores del evento, adelantaron en redes sociales para que se cancelara la presentación del músico.

Para Brecha Difusión, la actitud del Instituto de justificar que el escenario de Rock al parque deba ser apolítico, **resta importancia a la conciencia crítica y acción proactiva** "parece que es mejor dejarle a la juventud el mensaje facilista expresado en canciones como cuatro babys del condecorado Maluma" aseguran.

En sus redes sociales, el colectivo compartió algunas de las letras de Gillman con la intención de visibilizar que **"no son doctrinarias"** y que por el contrario "estimulan el conocimiento, la historia critica y la lucha contra toda forma de opresión", añadiendo que **del 100% de su discografía tan sólo el 10% habla de la revolución** “no chavista, no madurista, ni bolivariana” demuestra la necesidad en ese entonces de un cambio frente a todas las injusticias.

"El otro 80% son las letras que más temen, es la historia oral de las luchas venezolanas, sus mitos y su tradición", afirma el comunicado reiterando que a los grandes medios le temen a "nuestra historia común desde el Río bravo hasta la Patagonia, lo que nos hace latinos, lo que les imposibilita su poder"
