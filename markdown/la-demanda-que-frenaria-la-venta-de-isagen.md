Title: La demanda que frenaría la venta de ISAGEN
Date: 2016-01-18 11:56
Category: Economía, Nacional
Tags: demanda contra venta de isagen, ISAGEN, senadores demandan por venta de isagen
Slug: la-demanda-que-frenaria-la-venta-de-isagen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Venta-de-ISAGEN.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Corporación Nuevo Arco Iris 

###### [18 Enero 2016 ] 

[Senadores de las bancadas del Polo Democrático, Partido Verde, Centro Democrático y Partido Liberal junto con la Red de Justicia Tributaria y el Sindicato Nacional de Trabajadores de ISAGEN, radican este lunes una demanda ante el Tribunal Administrativo de Cundinamarca contra el [[proceso de venta de ISAGEN](https://archivo.contagioradio.com/que-se-pierde-con-la-venta-de-isagen/)]. Argumentan que **se violaron los derechos a la libre competencia, se ocasionó un grave detrimento patrimonial y se afectó gravemente la moral administrativa**.]

[De acuerdo con la Red de Justicia Tributaria **el proceso de venta de la generadora de energía puede aún detenerse**, pues la entrega del dinero no se ha efectuado y tanto el Gobierno como la firma canadiense Brookfield, tenían suficiente información sobre los procesos judiciales en curso y el Ministerio de Hacienda pese a conocer las dificultades de la venta, a última hora cambió las reglas de juego.]

[Además de esta acción judicial, los senadores adelantan una **recolección de firmas para una moción de censura contra el ministro de hacienda Mauricio Cárdenas**, luego de que más de 70 de ellos solicitaron desistir de la venta e interpusieron sus demandas ante el Consejo de Estado. ]
