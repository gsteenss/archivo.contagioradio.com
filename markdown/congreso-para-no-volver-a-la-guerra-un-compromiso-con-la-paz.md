Title: Congreso para no volver a la guerra, un compromiso con la paz
Date: 2018-04-23 15:05
Category: DDHH, Paz
Tags: Comandante Lola, Congreso para no volver a la guerra, Guatelama, Salvador
Slug: congreso-para-no-volver-a-la-guerra-un-compromiso-con-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/congreso.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso Para que no Vuelva la Guerra] 

###### [23 Abr 2018] 

Este 25 de abril, en Medellín, iniciará el Congreso para no volver a la Guerra, una iniciativa de diferentes organizaciones, que pretende visibilizar otras experiencias de procesos de paz en el mundo, con sus aciertos y desaciertos. Para Alejandro Toro, director de Avanza Colombia, esta será una oportunidad en la que las personas podrán reflexionar en torno a los otros tipos de violencia que **se dan con el conflicto y que por supuesto continúan haciendo parte de la realidad social del país**.

"Queremos poner el foco por ejemplo en lo que pasó en el Salvador, por eso viene alguien de este país, a decirnos que no es solo la burbuja emocional de firmar, sino qué sucedió luego en este país hoy sumido en homicidios y las pandemias de las Maras" afirmó Toro.  (Le puede interesar:["Organizaciones sociales señalan como "falso positivo judicial" captura masiva en Valle y Nariño"](https://archivo.contagioradio.com/valle_cauca_narino_lideres_sociales_capturas/))

En este escenario también estará participando Alba Estela Maldonado, más conocida como "La Comandante Lola", una de las primeras integrantes del Ejército Guerrillero de los Pobres, en Guatemala, y que posteriormente se desempeño en una ardua lucha por llevar la paz a su país.

Maldonado afirmó que en el caso particular de Colombia y los Acuerdos de Paz firmados con la FARC, debe intensificarse el trabajo con la organización popular o social, "creo que se logró avanzar en los últimos años en la creación y **cosolidación de organizaciones populares y eso es lo importante que tiene Colombia**" y agregó que finalmente el cumplimiento de los mismos dependerá de la movilización social.

Este foro se da en medio de la conmemoración de los 10 años que cumple la organización de Avanza Colombia, trabajando por iniciativas en torno a la no repetición del conflicto armado. Las inscripciones para acceder ya fueron cerradas, sin embargo, cada uno de los paneles será transmitido vía straming.

<iframe id="audio_25575636" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25575636_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
