Title: Líderes sociales serán protagonistas del Festival de Cine por los Derechos Humanos
Date: 2019-08-21 13:00
Author: CtgAdm
Category: Cultura
Tags: Audiovisual, Cine, Derechos Humanos, festival de cine, lideres sociales
Slug: lideres-sociales-seran-protagonistas-del-festival-de-cine-por-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Untitled-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Por : N[[ataly Pinzón Sierra]

[No se pierda la sexta versión del **Festival Internacional por los Derechos Humanos**, un espacio que desde el 2013 llega al país con su mensaje inspirador “El Cine Nos Une”, con el objetivo de promover el campo audiovisual en nuestro país.]

[En esta ocasión, serán 200 películas de selección oficial, largometrajes, documentales, cortometrajes, ficciones y animaciones las que harán parte de la selección además de 50 invitados de ámbito nacional e internacional, entre los que estarán actores, directores de cine, expertos en derechos humanos y líderes sociales, los cuales darán al público conferencias y talleres que  buscarán sensibilizar y despertar el interés del público sobre los derechos humanos partiendo bajo la premisa de que el cine incentiva la creatividad de las personas y las acerca de forma solidaria para la construcción de una memoria colectiva. ]

**"El cine nos une con todas estas historias maravillosas, nos une como país, la idea del festival es que nos ayude a humanizarnos un poco más con todas las víctimas, defensores y defensoras de derechos humanos, que todos los días se levantan a trabajar por las comunidades, por salvar el planeta." afirma Diana Arias, directora del festival.**

Además, agregó que en el marco del festival se realizarán talleres de herramientas básicas de producción audiovisual en alianza con UNITEC dirigido a líderes sociales y público en general, con el fin de visibilizar las labores que desarrollan dentro de sus comunidades. [(Le puede interesar: Cine en los barrios, el otro Festival en el Festival)](https://archivo.contagioradio.com/cine-en-los-barrios-el-otro-festival-en-el-festival/)

[El Festival Internacional por los Derechos Humanos se tomará al país del 23 al 29 de agosto, ofrecerá 10 talleres de formación audiovisual, que puede consultar][[aquí]](http://cineporlosderechoshumanos.co/talleres-de-formacion/)[. Serán varios escenarios entre el municipio de Soacha, Cundinamarca, y ciudades como Bogotá, Medellín, Barranquilla, Cartagena y Pereira donde tendrán espacio las proyecciones y demás actividades de este festival. ]

\[embed\]https://www.youtube.com/watch?v=r45EuYuQnhs\[/embed\]

[Si desea conocer la programación por ciudad o municipio, las películas que se proyectarán y demás actividades  lo invitamos a que ingresé a la página oficial del][[Festival  Internacional por los Derechos Humanos.]](http://cineporlosderechoshumanos.co/programacion-oficial/)

<iframe id="audio_40236245" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40236245_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
