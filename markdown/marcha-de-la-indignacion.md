Title: Estos son los puntos de movilización de marcha de la indignación del 12 de Octubre
Date: 2017-10-11 19:18
Category: Movilización
Tags: acuerdo de paz, congreso de los pueblos, Derecho a la salud, educacion, marcha patriotica
Slug: marcha-de-la-indignacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [11 Oct 2017]

En el marco de la Semana de la Indignación que desde el lunes y hasta el próximo viernes, pero con actividades durante todo el mes, este 12 de Octubre, se realizará la marcha de la indignación con tomas de ciudades, plantones, actos culturales, marchas de antorchas y varias acciones en las que los habitantes de las decenas de ciudades manifestarán su indignación frente a temas diversos como **la corrupción, el derecho a la salud, la educación, la vivienda digna, el incumplimiento en la implementación del acuerdo de paz entre otras.**

Desde el Comando Unitario de Paro, una plataforma que reúne a organizaciones sindicales, campesinas y civiles se están convocando a cientos de actividades en el marco de la **Marcha de la Indignación.** En redes sociales se ha posicionado la etiqueta **\#MeIndigna **

**Bogotá:** 9:00 AM en el Centro Administrativo Distrital hacia la plaza de Bolívar

**Medellín:** La movilización sale de la Universidad de Antioquia a las 9:00 Am hacia el centro administrativo la alpujarra

**Bucaramanga** en la Universidad Industrial de Santander, plantón de indignación a partir de las 6 PM Marcha de Antorchas

**Cali:** En la estación del ferrocarril a las 9:00 AM hasta la plazoleta de la gobernación

**Barrancabermeja** plantón y galería de la memoria en Parque Camilo de 8:00 am a 5:00 pm

**Neiva:** 8:00 Am en la Universidad Sur Colombiana hacia el parque del amor y la amistad de donde saldrán a las 9:00 AM en compañía de los pilotos y de allí al Aeropuerto Benito Salas

**Ibague:** 9:00 Am de la sede del Sindicato de Maestros del Tolima hasta la plaza de la gobernación y en la noche proyección de cine película “la toma de la embajada”

**Monteria:** Sale de la Plaza Rojas del Barrio P 5 y llega al parque de la 27 desde las 8:00 AM corrupción

**Puerto Asís:** Plantón en la Esmeralda a las 9:00 AM

En Otra Mirada a partir de las 8 de la mañana podrán encontrar el cubrimiento especial de toda la jornada. [Lea también: todas las actividades de la Semana de la Indignación](https://archivo.contagioradio.com/inicia-la-semana-de-la-indignacion-en-colombia/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
