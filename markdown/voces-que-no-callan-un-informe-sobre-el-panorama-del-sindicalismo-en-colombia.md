Title: 'Voces que no callan' un informe sobre el panorama del sindicalismo en Colombia
Date: 2016-11-29 01:09
Category: DDHH, Nacional
Slug: voces-que-no-callan-un-informe-sobre-el-panorama-del-sindicalismo-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/sindicato.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Doralnews] 

###### [29 Nov 2016] 

El 28 de Noviembre la Escuela Nacional Sindical presentó en Bogotá el informe Voces que no Callan, que da cuenta del panorama para las organizaciones sindicales y proporciona **un balance sobre las violaciones a los derechos humanos que enfrentan los sindicalistas colombianos durante el período del 2010 al 2015.**

Dicho informe hace un recorrido por las violaciones de derechos humanos a sindicalistas de sectores campesinos, étnicos, educativos, minero energéticos y de mujeres, evidenciando cifras alarmantes de **más de 3.000 hechos victimizantes en contra de esta población, y lo más preocupante, una impunidad por parte de los entes gubernamentales cercana al 100%.**

Según la investigación de la Escuela Nacional Sindical, dichas violaciones fueron distribuidas así: 186 homicidios, 101 atentados, 22 desapariciones forzadas, 2.093 amenazas, 293 hostigamientos, 191 desplazamientos forzados y 89 detenciones arbitrarias, de las cuales **712 fueron contra mujeres y 2.304 contra hombres.**

![tabla\_nota](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Tabla_Nota.jpg){.alignnone .size-full .wp-image-33098 width="935" height="510"}

Se hace especial énfasis en que ha habida una progresiva especialización de la violencia antisindical que **“cada vez extiende más su radar para estigmatizar, perseguir y eliminar aquellos que participan de manera activa en la defensa de los derechos laborales** desde la dirigencia de los sindicatos”.

El documento señala que las amenazas con un de 79,1% de ocurrencia, son una de las violaciones más persistentes a lo largo del estudio, **“lo cual sugiere una intencionalidad de sostener un ambiente de temor e incertidumbre”.**

Si bien durante 2013 se evidencia una disminución gradual de las violaciones en términos cuantitativos como resultado de la estrategia de denuncia por parte de las organizaciones sindicales, la Confederación Sindical Internacional en el año 2015 incluyó a **Colombia como el cuarto país con más riesgos para el ejercicio de derecho de asociación sindical.**

Por otra parte en el marco de la ley 1448 la administración actual reconoció la violencia antisindical como un fenómeno histórico y sistemático, dio inició a un proceso de **reparación diferencial para uno de los colectivos más victimizados en el marco del conflicto colombiano.**

### **¿Cuáles son los sectores más afectados?** 

El documento revela que los sectores de educación con un 42,3%, agricultura, caza y pesca con 13,8%, minas y canteras con 11,6% y otros servicios con un 10,3% han sido los más azotados por el fenómeno antisindical. Menciona que algunos sindicatos educativos en particular como **Adida, Sutev y Educal, y otros como Fensuagro, USO, CUT, Sinaltrainal, Anthoc, Sintramienergética, Asogras y Sintraunicol,** han sido en particular fuertemente golpeados por la violencia.

De igual forma destaca que los departamentos donde más se concentran **casos de violaciones son** **Antioquia, Valle y Santander con un 20,1%, 18,8% y 7,9% respectivamente,** y los departamentos en donde la violencia fue más dura contra dirigentes sindicales son Valle, Santander y Bolívar.

### **El sector rural y de mujeres los más afectados** 

Durante los 5 años estudiados, se cometieron 416 violaciones de derechos fundamentales contra trabajadores del sector de la agricultura, caza y pesca, **150 amenazas, 132 hostigamientos, 64 detenciones arbitrarias, 28 homicidios y 17 atentados de los cuales 59 fueron contra mujeres rurales.**

Además durante el mismo periodo se registraron 712 casos de violencia contra mujeres sindicalistas, entre ellos 13 asesinatos, 9 atentados, 546 amenazas, 93 desplazamientos forzados, 35 hostigamientos y 8 detenciones arbitrarias.

El informe aclara que si bien “hubo un incremento de la participación de las mujeres en el escenario sindical, especialmente en los sectores educación, agricultura, salud, las empresas municipales y oficiales y otros servicios comunales”, **dicho protagonismo fue proporcional a la violencia desplegada contra ellas.**

### **La impunidad, un lugar común** 

Sobre este tema puntual, el informe de la ENS denuncia que si bien el Gobierno Nacional destinó recursos humanos, económicos y técnicos para fortalecer la capacidad investigativa de la Fiscalía “la superación de la violencia antisindical y la impunidad aún no se ha logrado, por el contrario, **sigue siendo un asunto pendiente de resolver, a lo que se suma otro factor de riesgo: la criminalización de la acción sindical”.**

El estudio denota que en la actualidad la impunidad por el delito de homicidio es del 87%, y **la impunidad por amenazas tiene el más alto índice: 99,8%.** Señala también que la Fiscalía “no informa con precisión el estado de las investigaciones en todos los casos de asesinatos cometidos contra sindicalistas”, pues sólo ha informado de **1.545 casos, mientras que el seguimiento realizado por la Escuela indica que se han cometido al menos 3.093 homicidios.**

De los 1.545 casos únicamente 971 están activos y 554 en etapa preliminar, es decir tan sólo en 417 casos hay avances en investigativos.

### **¿Quiénes han sido los responsables de estas violaciones de Derechos Humanos?** 

El informe muestra que en la mayoría de los casos al no haber una exhaustiva labor investigativa, se desconoce a los responsables, sin embargo, resalta que casi en la totalidad de los casos en que se ha conocido a los responsables **los paramilitares continúan siendo los principales victimarios, con el 69,1% de los casos documentados.** Siguiéndole a estos, los organismos del Estado, **“miembros de la fuerza pública, policía y ejército actúan con la lógica de la sanción y represión de la actividad sindical”.**

Por último el informe presenta unos puntos propuestos desde los **resultados arrojados por el estudio, pero que también recogen distintas apuestas de organizaciones sindicales** para la conformación de una agenda de paz nacional:

-   Impulsar el debate sobre las violaciones en materia de derechos humanos.
-   Persistir en la necesidad de la superación de la violencia antisindical y la impunidad.
-   Impulsar iniciativas de reconstrucción de su memoria histórica y promover su difusión.
-   Activar el desbloqueo del proceso de reparación colectiva al sindicalismo.
-   Construir propuestas en clave de garantías para la acción y la protesta sindical.

###### Reciba toda la información de Contagio Radio en [[su correo]
