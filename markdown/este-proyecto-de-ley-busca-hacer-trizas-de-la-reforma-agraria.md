Title: Este proyecto de ley busca hacer trizas la Reforma Agraria
Date: 2019-06-18 18:19
Author: CtgAdm
Category: Comunidad, Política
Tags: Ley de Reforma Agraria, Senado
Slug: este-proyecto-de-ley-busca-hacer-trizas-de-la-reforma-agraria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/REFORMA-AGRARIA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

[La Comisión Quinta del Senado votará esta semana el proyecto de ley 003, una iniciativa que modifica la Ley 160 de 1994, conocida como la Ley de Reforma Agraria, y que **la oposición indica daría vía a la imposición de un modelo extractivista, a través de la minería y la agroindustria en el sector rural del país**.]

Según Sergio Martínez, abogado de la Comisión Colombiana de Juristas, hay cuatro puntos de la norma que generan preocupación, entre ellos, el desconocimiento del campesinado como sujeto político y de derechos; la debilitación de la Reforma Rural Integral (RRI); legitimación del despojo de tierras; y la eliminación de la función social de la propiedad. (Le puede interesar: "[Las razones para que campesinos le digan al Congreso: \#NoJodanElCampo](https://archivo.contagioradio.com/campesinos-congreso-nojodanelcampo/)")

### **Los derechos de los campesinos serían desconocidos**

Sobre el primer punto, Martínez explicó que el artículo 1 de la norma elimina la concepción de la agricultura fundada en una economía campesina, familiar y comunitaria y al contrario, se reduce esta actividad a la apuesta agroindustrial. "Se plantea un balance entre la agricultura agroindustrial, los sectores mineros y el turismo, pero **esto dejaría de lado ese enfoque diferencial, teniendo en cuenta que no todos los sectores rurales están en igualdad de condiciones en términos de garantías de derecho**", afirmó.

A este punto, se suma que el artículo 2 establece la creación de una nueva apuesta de ordenamiento territorial, Zonas Prioritarias para la Producción de Alimentos (ZPPA), que el Abogado indica desconocería la autonomía de las entidades territoriales para tomar decisiones sobre sus territorios, teniendo en cuenta que el Ministerio de Agricultura y Desarrollo Rural sería la institución encargada de declarar estas zonas.

### **Hace trizas del Acuerdo de Paz**

Dentro del Acuerdo de Paz, en el Punto 1, quedó planteado la RRI, una forma alternativo de desarrollo y bienestar territorial que incluye a las personas excluidas, como camino para alcanzar la reconciliación nacional. En primer lugar, el artículo 10, numeral 2, permite que mineros puedan acceder a predios del Fondo de Tierras, originalmente destinados a beneficiarios de la reforma agraria.

En segundo lugar, el artículo 34 permite cambiar la entrega de los bienes del Fondo de Tierras, a iniciativas agroindustriales impulsadas, por ejemplo, de las ZPPA. Mientras tanto, **el artículo 29 determina que personas extranjeras también puedan acceder a predios del Fondo de Tierras**.

### **Vía libre del despojo de tierras**

Según el Abogado, el artículo 7  "termina por sanear y legitimar las distintas formas de despojo jurídico, administrativo y judicial que se han dado en el marco del conflicto social y armado, al permitir que se acredite la propiedad con resoluciones de adjudicación, títulos privados o sentencias judiciales sin evaluar el contexto de violencia o los vicios que puedan contener estas  
formas de adquirir el dominio".

Esta norma **desconoce las presunciones de ilegalidad establecidas por el artículo 77 de la Ley de Víctimas, "una burla desde nuestra punto de vista a los derechos y la reparación integral a las víctimas.**

### **Elimina la función social de la propiedad**

El artículo 13 del proyecto propone **la renuncia del Estado respecto a la recuperación de predios** porque le da un carácter residual a la explotación administrativa. Además, el artículo 19, numeral 2, dice que cualquier ocupante que acapare tierra sin cumplir con la función social de la propiedad pueda probar su explotación económica con firmar un contrato de arrendamiento o aparcería.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
