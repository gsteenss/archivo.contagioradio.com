Title: Lucha contra la corrupción, la desigualdad y la defensa de la paz: coincidencias entre Petro, De la Calle y Fajardo
Date: 2018-04-01 19:55
Category: Nacional, Política
Tags: Angela Maria Robledo, Gustavo Petro, Humberto de la Calle, Iván Duque, Partido Liberal, Sergio Fajardo
Slug: coincidencias_entre_fajardo_de_la_calle_petro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/DZBPfQ_WkAAUKsA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter @jovenesconpetro] 

###### [31 Mar 2018] 

Esta semana Claudia López, Ángela María Robledo y Clara López, fórmulas vicepresidenciales de Sergio Fajardo, Gustavo Petro y Humberto de a Calle, respectivamente, **se reunirán atendiendo el llamado de los jóvenes reunidos en el colectivo "Primero Colombia",** desde donde se ha pedido a los tres candidatos una gran coalición antes de las votaciones del próximo 27 de mayo.

Se trata de un café que se tomarán este martes las tres candidatas a la vicepresidencia, y el con el cual los jóvenes, esperan que se tejan caminos jurídicos y políticos para lograr dicha alianza de cara a las presidenciales.

"En mi caso es darle continuidad a una invitación que ha hecho reiteradamente Gustavo Petro, vamos a ver si este es el momento", dice Ángela María Robledo, fórmula de Petro, quien agrega que **será clave este diálogo entre tres mujeres que han tenido apuestas comunes en el marco de la defensa de la paz, la igualdad y la lucha contra la corrupción.** Tanto la excongresista Robledo, como Claudia López y Clara López, han aceptado la invitación.

### **Liberales también se la juegan por una coalición** 

En medio de la incertidumbre en la que se encuentra actualmente el Partido Liberal, un sector de ese partido, también clama por la alianza entre los tres candidatos. "**Más allá de los egos la prioridad debe el momento histórico que vive Colombia, la coalición tiene que ser en torno a las ideas.** Son varias las coincidencias", dice Andrés Guzmán, integrante del Partido Liberal. "Colombia necesita un equilibrio ambiental, económico, social", añade.

Guzmán, reconoce que el Partido Liberal no pasa por un buen momento, teniendo en cuenta que algunos congresistas electos liberales, han evidenciado su intensión de unirse a la campaña del candidato por el Centro Democrático, Iván Duque.

"El Partido Liberal no puede ser una camarilla que decida por el grueso poblacional que busca una alternativa. **No puede haber congresistas del Partido que le estén apostando a Iván Duque. Eso es un si sentido, cuando como partido se ha creado una ruta a la paz y a reforzar los acuerdos"**, expresa e integrante de ese partido político, y por eso concluye que los liberales deben retomar la visión de centro "que es la visión del equilibrio".

<iframe id="audio_24930277" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24930277_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
