Title: Grave situación de salud de presos en Cárcel de Cómbita, Boyacá
Date: 2016-12-19 14:57
Category: DDHH, Nacional
Tags: presos cárcel Cómbita, presos politicos
Slug: grave-situacion-de-salud-de-presos-en-carcel-de-combita
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Preso-Politico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Macondo] 

###### [19 Dic 2016] 

Los presos políticos del país continúan denunciado la crisis carcelaria y las condiciones inhumanas a las que son sometidos en las diferentes prisiones del país, en esta ocasión durante los días 13 y 14 de diciembre se presentó un **brote de fuertes dolores estomacales, diarrea e infecciones intestinales en más de 100 reclusos de la Cárcel de Cómbita, en Boyacá.**

De acuerdo con Gloria Silva, abogada de equipo jurídico Pueblos, estos hechos se deben principalmente a la **pésima calidad de la alimentación  y comida que se le da a los reclusos**, situación que ha sido demandada por esta organización “previamente ya habíamos presentado una denuncia contra la empresa privada que presta el servicio de alimentación porque no hay ningún tipo de control por parte de quienes se encargan de hacer las auditorias correspondientes”.

Los presos han señalado que muchos alimentos están descompuestos a la hora de ser servidos, de igual manera la abogada indicó que “el problema de fondo sigue sin resolverse y sin ser atendido por las autoridades de control que recae en la privatización de los servicios” generando que los **derechos fundamentales de los reclusos se conviertan en un negocio en manos de entidades privadas.**

De otro lado, por estaS fechas las cárceles además son más visitadas debido a las festividades de fin de año y ya familiares de los presos han expresado que se ven sometidos a diferentes ultrajes, malos tratos por parte de los policías del INPEC. Le puede interesar: ["Por falta de atención han muerto 6 personas en la cárcel La Picota"](https://archivo.contagioradio.com/por-falta-de-atencion-oportuna-han-muerto-6-personas-en-la-carcel-la-picota/)

**Esta misma situación se presenta en el 100% de los centros de reclusión** y hasta el momento no hay alguna entidad que responda por la crisis carcelaria, no obstante diferentes organizaciones defensoras de derechos humanos, han estado haciendo seguimiento a las diferentes sentencias para que se respeten los derechos de las personas recluidas. De igual forma, el próximo año se presentará un  proyecto de ley entre diferentes representantes a la Cámara y el Ministerio de Justicia para intentar mejorar las condiciones de vida de los presos.

<iframe id="audio_15180248" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15180248_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
