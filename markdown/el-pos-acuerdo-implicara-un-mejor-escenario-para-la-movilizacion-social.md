Title: El pos acuerdo implicará un mejor escenario para la movilización social
Date: 2016-08-26 13:00
Category: Nacional, Paz
Tags: conversaciones de paz, ELN, FARC, INDEPAZ, Movilización social, pos acuerdo
Slug: el-pos-acuerdo-implicara-un-mejor-escenario-para-la-movilizacion-social
Status: published

###### [Foto: eluniversal] 

###### [26 Ago 2016]

El escenario del pos acuerdo será un aporte importante para que las exigencias de diversos **sectores sociales tomen más fuerza y no se expongan a la estigmatización** que siempre tacha la movilización como “infiltrada por la guerrilla”. Además seguramente en lugares como Caquetá y Putumayo la arremetida de las empresas se va a encontrar con una fuerte oposición por parte de las comunidades campesinas e indígenas.

Para **Camilo González, ex comisionado de paz y director de INDEPAZ**, los argumentos que han sido manifestados por los opositores de la paz, son “delirantes” y lo que quieren acabar con los esfuerzos de paz para seguir haciendo “[su propia guerra”.](https://archivo.contagioradio.com/?s=oposici%C3%B3n+a+la+paz) En referencia a lo afirmado por Francisco Santos, González afirma que **en toda la historia reciente no se han presentado unos niveles tan bajos de presión electoral**.

Frente al anuncio por parte del Ministerio de Defensa de la reducción del número de las **Zonas Veredales Transitorias y campamentarias**, el director de INDEPAZ señala que lo que se está dando ya es un ajuste para poner en marcha tanto el mecanismo de verificación, como la movilización de los integrantes de las FARC a las zonas en donde dejarán las armas en manos de la verificación internacional.

INDEPAZ asegura que uno de los trabajos fuertes que se vienen son el de pedagogía de los acuerdos para alcanzar y rebasar el umbral definido y luego el trabajo para la implementación de los acuerdos. Por ello una de las primeras tareas que asumen desde esa organización es la de la jornada de 24 horas de lectura de los acuerdos y luego una serie de talleres en las regiones para seguir haciendo campaña pero también explicando **los alcances del [acuerdo firmado entre el gobierno y las FARC.](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)**

<iframe src="http://co.ivoox.com/es/player_ej_12677035_2_1.html?data=kpejmZyUd5ahhpywj5acaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZCrs8_uhqigh6aVsMbnhpewja7SqMbkwt-ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
