Title: Chile: Los efectos de un modelo económico fallido
Date: 2019-10-21 17:55
Author: CtgAdm
Category: El mundo, Entrevistas
Tags: Chile, Metro, Piñera, protestas
Slug: chile-los-efectos-de-un-modelo-economico-fallido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/74428421_2495675590701794_8095273841196007424_n-e1571700528781.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Guillermo Jaramillo Castillo] 

Latinoamérica gira la mirada sobre Chile tras completar 5 días de movilizaciones ciudadanas en rechazo de un modelo económico que se había tomado como plan a seguir en el continente, pero que ha demostrado su fracaso, con políticas financieras erradas. En respuesta al \#Cacerolazo cívico, el presidente Piñera ha decretado un toque de queda con más de 10.500 disponibles de la Fuerza Pública, que han dejado un saldo de violaciones de derechos humanos con más de 10 personas asesinadas, más 1.900 detenciones arbitrarias y el temor por revivir hechos de la dictadura de Pinochet.

### **La razones del pueblo en Chile para levantarse  
** 

El profesor universitario de comunicación política y residente en Chile, Juan Carlos Pérez, explicó que el pretexto para las protestas fue el alza en el servicio de transporte, "pero se está incubando desde el fin de la dictadura una serie de problemáticas que se han ido acrecentando con el tiempo". Uno de los problemas que acarreó la transición a la democracia fue la expectativa de cambio social, pero según Pérez, la dictadura dejó una serie de condiciones que dificultan este proceso, como establecer una forma binominal que le dio peso a la derecha en el Congreso e impedir el cambio en la Constitución.

Ello generó que el Congreso pudiese modificar asuntos claves de la economía chilena en favor de las personas, entre ellas, el sistema de pensiones, que debido al envejecimiento de la población y la falta de medidas para sostener salarios dignos, ha generado condiciones "miserables" entre quienes ya no pueden trabajar. Sumado a ello, el Profesor señaló que los partidos que tomaron el poder tras la dictadura y supuestamente se ubican en la centro izquierda, encontraron un sistema político y económico 'amarrado' a las condiciones de Pinochet, y por otra parte, "olvidaron su rol transformador, y se adaptaron al sistema".

No obstante, Pérez afirmó que en los últimos años el pueblo chileno se ha ido despertando, y en 2011 una serie de manifestaciones estudiantiles que reivindicaban el derecho a la educación tomaron la batuta de otras reivindicaciones. En ese momento, **"los movimientos sociales tomaron mayor fuerza, pero los gobierno de Piñera y Bachelet no supieron leer, o dar respuesta a las demandas".** (Le puede interesar: ["¿Por qué Ecuador se vuelca a las calles?"](https://archivo.contagioradio.com/ecuador-tras-protestas/))

> [pic.twitter.com/cOeqwxQ0jD](https://t.co/cOeqwxQ0jD)
>
> — Carlos Fonseca (@carlosfonzeca) [October 21, 2019](https://twitter.com/carlosfonzeca/status/1186407019638657024?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Las marcha del viernes: Más que una subida del metro** 

Respecto a la reciente agitación social, el Profesor señaló que las protestas por el aumento en el costo del metro tuvieron que ver con una lucha muy puntual de "un colegio público que estuvo un año y medio en lucha, y que ha tenido conflicto con las autoridades, y una forma de expresar su rechazo fue convocar una evasión masiva del metro". Ante esta manifestación, las autoridades reprimieron a jóvenes de entre 12 y 14 años, por lo que las personas se unieron contra el alza en el sistema de transporte y contra la violencia de la Policía.

Pero una vez más, Pérez aclaró que las protestas tienen más razones: **"Chile es un país desigual, está muy endeudado, la clase media está muy endeudada porque paga cerca del 50 al 60% de su sueldo en deudas"**, mientras la imagen que se vende del país al exterior es que es una potencia económica. Posteriormente, el viernes las manifestaciones se salieron de control porque "la gente quemó el metro y se comenzó a genera una serie de disturbios".

Adicionalmente, el Académico señaló que en los sectores más desposeídos de la periferia de Santiago de Chile, algunas personas encontraron la oportunidad de comenzar saqueos e incendios, lo que generó una situación aún más compleja. Ante este conjunto de situaciones, el gobierno de Sebastián Piñera emitió un decreto que congeló la tarifa del metro, "pero sabemos que no va a ser suficiente, es mucho más complejo". (Le puede interesar:[¿Levantar el paquetazo, o buscar la salida de Lenín Moreno en Ecuador?](https://archivo.contagioradio.com/levantar-el-paquetazo-o-buscar-la-salida-de-lenin-moreno-en-ecuador/))

### **"El modelo (económico) fracasó"** 

Pérez recordó que durante la dictadura chilena, quienes se hicieron cargo de la economía fueron los llamados "Chicago Boys", hombres entrenados en Estados Unidos por Milton Friedman, que posteriormente establecieron un sistema de libre mercado y una economía igualmente dependiente de la exportación de materias primas. **Según el Profesor, "este modelo se implantó y ha sido muy difícil de desmontar" hasta que finalmente "el modelo fracasó".**

En primer lugar, porque se permitió que el mercado controlara la salud, la educación e incluso el agua, que está concesionada, y manejada por el sector privado; y en segundo lugar, porque la coyuntura de guerra económica entre China y Estados Unidos tomó por sorpresa a una economía que depende de la exportación de cobre y otros insumos. No obstante, Pérez resaltó que también gracias a la coyuntura, sectores que antes no se manifestaban ahora están pensando en reformas para responder a las demandas del pueblo. "Ya se están escuchando voces que claman por una reforma constitucional, por una asamblea constituyente y ponerle freno a esta intervención del mundo privado", concluyó el experto.

### **¿Cómo está Chile en este momento?** 

Luego de las movilizaciones de la semana pasada, en medios de comunicación y redes sociales, la ciudadanía denuncia el asesinato de **más de 10 personas, cerca de 1.900 detenidos y más de 10 mil efectivos de las Fuerzas Militares movilizados** en medio de un toque de queda. Según el presidente Piñera, todo ello, debido a "una guerra" en la que se enfrenta a un "enemigo poderoso (...) con el único propósito de producir el mayor daño posible".

Siguiendo esta línea, el Presidente de la Asociación de Oficiales de la Armada en Retiro (ASOFAR), declaró que están listos para enlistarse nuevamente a las FF.MM. porque tienen **"la preparación y la vasta experiencia de lo que es organizar y dirigir un país frente a un convulsionado momento"**. (Le puede interesar: ["Lenin Moreno anuncia que recuperará el orden en Ecuador, desplegando las FFMM"](https://archivo.contagioradio.com/lenin-moreno-anuncia-que-recuperara-el-orden-en-ecuador-desplegando-las-ffmm/))

Un habitante de la ciudad de Los Andes, en Chile, que no quisó que apareciera su nombre, dijo a Contagio Radio que **"todos sabemos que cuando salieron los militares en el 73 desapareció gente, asesinaron gente y se está repitiendo"**. Por lo tanto, la gente ha rechazado el toque de queda y la declaración de guerra hecha por Piñera, no obstante, dijo que los manifestantes no tenían miedo y seguían saliendo a las calles aún en poblaciones que son muy 'tranquilas' como Los Andes.

Respecto al trabajo de los medios de comunicación, el ciudadano chileno sostuvo que tanto los diarios, como la televisión y la radio no están informando adecuadamente sobre la violación a los derechos humanos que se están cometiendo por parte de la Policía, y afirmó que se concentran en mostrar los saqueos, provocando una situación de pánico. Ante ello, destacó el papel de las redes sociales como plataformas para compartir las realidades de lo que está pasando en Chile.

Por su parte, el profesor Pérez dijo que el pasado domingo 20 de octubre diferentes organizaciones sociales, estudiantiles y de fondos de pensiones convocaron marchas para intentar dar una dirección política a la protesta, intentado tener fuerza "para generar un petitorio concreto". Adicionalmente, Pérez indicó que también estaría por verse cómo se organizarán los partidos de izquierda para lograr tener "algún espacio desde el Senado o la Cámara para generar algún tipo de presión y generar cambios".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43333758" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43333758_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
