Title: “La paz sin el ELN sería una paz incompleta”
Date: 2015-08-25 11:00
Category: Entrevistas, Nacional, Paz
Tags: Conversaciones de paz gobierno - ELN, ecuador, ELN Nicolas Rodríguez Bautista, FARC, Juan Manuel Santos, paz
Slug: la-paz-sin-el-eln-seria-una-paz-incompleta
Status: published

###### Foto: cablenoticias 

<iframe src="http://www.ivoox.com/player_ek_7593128_2_1.html?data=mJqmlZaWfI6ZmKiakpuJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkMKf0cbnjdjNsozZzZDSztOPt8bmhqigh52opYzpz8aY0sbeb8rixNTa0tHJuMKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Victor de Currea Lugo, analista Internacional] 

<iframe src="http://www.ivoox.com/player_ek_7593016_2_1.html?data=mJqmlZWVeo6ZmKiakpyJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdfVz8jS1ZDFb8LbxtPRw5DIqYzX0NPjx9fXpcTd0NPS1ZDHs8%2BfxtGYydTGrcbmz9SYxdTQs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Nicolás Rodríguez Bautista, ELN] 

 

###### [25 Ago 2015] 

Nicolás Rodríguez Bautista, miembro del Comando Central del Ejército de Liberación Nacional anunció públicamente, a través de la Radio Nacional Prensa Libre, que la agenda de **conversaciones con el gobierno colombiano ha avanzado en un 85% o 90%** y reafirma su intención de construir un proceso de **negociación responsable y de cara al país.**

El profesor Víctor de Currea Lugo, analista internacional y estudioso del proceso con el ELN, insiste en la necesidad de reconocer que este anuncio público es de gran importancia para la mesa de negociaciones, dado que es el primer comandante del ELN y como tal es **una voz autorizada que impide que el proceso sea viciado por comentarios o falsos rumores.**

Para el analista la **posición del gobierno nacional frente a los falsos positivos judiciales, puntualmente el caso de los 13 jóvenes detenidos**, ha llamado la atención del ELN, se cuestionan la dejación de armas y las posibilidades de participación política, en un escenario de detenciones arbitrarias y genocidios como el ocurrido con la Unión Patriótica.

Se espera entonces que el gobierno de Juan Manuel Santos y el Ejército de liberación Nacional logren acuerdos puntuales al respecto y pueda instalarse la mesa de negociaciones en su fase pública, **que deberá acoplarse con lo avanzado hasta ahora en la mesa de conversaciones con la guerrilla de las FARC.**

Vea también[Conversaciones con el ELN  han avanzado en un 85%.](https://archivo.contagioradio.com/hay-avances-en-un-85-de-agenda-de-paz-con-gobierno-eln/)
