Title: 101 años del natalicio de monseñor Oscar Romero
Date: 2018-08-15 15:59
Category: Otra Mirada
Tags: El Salvador, Monseñor Romero
Slug: 101-anos-monsenor-romero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Romero-101-e1534361508819.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 15 Ago 2018 

Monseñor Oscar Arnulfo Romero, Arzobispo salvadoreño, nació el 15 de agosto de 1917 en Ciudad Barrios en El Salvador. Formado como sacerdote a los 24 años en Roma, el 4 de abril de 1942, emprendió una ardua labor de la mano de las comunidades más pobres, impulsando durante 20 años movimientos apostólicos y obras sociales.

El 15 de octubre de 1974 fue nombrado Obispo de la Diócesis de Santiago de María, lugar que padecía la represión contra los campesinos organizados. Allí Romero comprendió que debía asumir una postura política para defender a quienes conformaban su iglesia. Fue un hombre que defendió la idea del derecho de los pueblos a organizarse y demandar justicia y paz. En 1975, La Guardia Nacional asesino a 5 campesinos, lo cual, fue denunciado por Monseñor.

Pese las represiones e injusticias presentadas contra la población y contra él, fue nombrado Arzobispo de San Salvador, el 3 de febrero de 1977. Por su labor fue calumniado y acusado de ser un revolucionario marxista que incitaba a la violencia, posteriormente fue amenazado y finalmente asesinado el 24 de marzo de 1980, mientras celebraba una misa en la Capilla del Hospital La Divina Providencia.

La comisión de la verdad en 1993 encontró culpable al Escuadrón de la muerte y a grupos de la extrema derecha dirigidos por el Mayor Roberto D’Abuisson, fundador del partido Alianza Republicada Nacionalista ARENA. En el 2000, la Corte Interamericana de Derechos Humanos hizo responsable al Estado salvadoreño, solicitando reparación. Hasta 2009, el Estado reconoció su responsabilidad, luego de más de treinta años.

Su lema fue Sentir con la Iglesia, con ello, estuvo en favor de la justicia y la paz, era mediador de conflictos, creando una oficina para la defensa sobre los derechos humanos, dio refugio a campesinos en persecución e impulso al Seminario Orientación y la Radio YSAX.

Monseñor fue emblema para América Latina, por ello, en su centésimo primer aniversario natalicio, la iglesia católica realizara una Solemne Eucaristía, en la Catedral Metropolitana. El 14 de octubre, el Papa Francisco realizara la canonización del arzobispo junto al papa Pablo VI, una vez más a través de este evento se cumplirán las palabras del arzobispo por medio de esta beatificación, afirmo: “No creo en la muerte sin resurrección, si me matan resucitaré en el pueblo salvadoreño”, resucitara no solo en el pueblo salvadoreño sino que también en el pueblo católico latinoamericano.
