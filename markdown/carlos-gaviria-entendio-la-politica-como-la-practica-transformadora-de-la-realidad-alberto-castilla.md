Title: "Carlos Gaviria entendió la política como la práctica transformadora de la realidad" Alberto Castilla
Date: 2015-04-01 20:34
Author: CtgAdm
Category: Nacional, Política
Tags: Alberto Castilla, carlos gaviria, division, Magistrado, partido, polo, unidad
Slug: carlos-gaviria-entendio-la-politica-como-la-practica-transformadora-de-la-realidad-alberto-castilla
Status: published

##### Foto: La Opinión 

<iframe src="http://www.ivoox.com/player_ek_4296341_2_1.html?data=lZemmJiYdY6ZmKiakpyJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmpMbfztTXb6jV187fy8aPqc%2FoxtPRy4qnd4a2lJDZw5DUs82ZpJiSo6nYrcTVjMjcz9SPsMKf0deSpZiJhZLX1c7Qj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Castilla, Senador] 

"**Carlos Gaviria fue un liberal a carta cabal**, un demócrata demostrado en el sentido de reconocer la diversidad, los Derechos de las minorías, y como organizaciones sociales que luchamos por el reconocimiento como sujetos políticos, reconocemos su labor", afirma el Senador Alberto Castilla.

Para este líder campesino, una de las tareas inaplazables que, como homenaje a Gaviria, debe realizar el **Polo Democrático Alternativo**, es trabajar por la unidad. Fue "un señor dedicado a que tuviéramos **buenas relaciones al interior del partido**, a que trabajáramos realmente por cambios y transformaciones en este país".

Afirma el Senador Castilla que hasta sus últimos días, Carlos Gaviria mantuvo su empeño porque "se resuelvan las diferencias que existen en los sectores del Polo; y el Polo como un homenaje debe desprenderse de intereses particulares y convertirse en el partido del sentir nacional y las luchas populares, el partido de la paz, el partido de la unidad, que es lo que quería el doctor Carlos".

Es por este motivo que Alberto Castilla concluye que quienes "entendemos que la política significa la práctica transformadora de la realidad, nos duele y lamentamos la muerte del doctor Carlos Gaviria".
