Title: Las razones para que asesinato de Dylan Cruz sea tomado por la Justicia Ordinaria
Date: 2020-04-24 18:04
Author: AdminContagio
Category: Actualidad, Judicial
Tags: Corte Suprema de Justicia, ESMAD
Slug: las-razones-para-que-asesinato-de-dylan-cruz-sea-tomado-por-la-justicia-ordinaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Homenaje-a-Dylan-Cruz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: {#foto .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este jueves 23 de abril la Corte Suprema de Justicia ordenó a la Sala Disciplinaria del Consejo Superior de la Judicatura que revise la decisión que llevó el caso del asesinato de Dylan Cruz a la Justicia Penal Militar. La decisión fue posible gracias a una acción de tutela que interpuso la familia del joven, esperando que sea la justicia ordinaria la que avoque conocimiento del caso.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **En contexto: La discusión sobre quién investigará el caso de Dylan Cruz**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 18 de diciembre el Consejo Superior de la Judicatura determinó que el caso del joven Dylan Cruz, que perdió la vida en medio de una protesta producto del disparo de un agente del Escuadrón Móvil Antidisturbios (ESMAD) debía ser investigado por la Justicia Penal Militar porque el hecho había ocurrido " **en el marco de un acto del servici**o" que cumplía el agente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras conocerse la decisión, desde la [Fundación Comité de Solidaridad con los Presos Políticos (FCSPP)](http://www.comitedesolidaridad.com/es/content/corte-suprema-de-justicia-ordena-revisar-decisi%C3%B3n-sobre-el-asesinato-manos-del-esmad-de) señalaron que disparar a una persona mediante el uso desproporcionado de la fuerza no era una acción del servicio. En su momento, Contagio Radio señaló que se podría acudir a la acción de tutela para buscar una decisión diferente a la que se había producido.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fernando Rodríguez, abogado de la Fundación y representante de la Familia de Dylan sostiene que gracias a una acción de tutela, fue posible que se ordene nuevamente la revisión sobre la jurisdicción en la que se adelantará la investigación del caso. (Le puede interesar: ["Caso de Dilan Cruz es enviado a la Justicia Penal Militar"](https://archivo.contagioradio.com/caso-de-dilan-cruz-es-enviado-a-la-justicia-penal-militar/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Significa la posibilidad de cambiar de jurisdicción una pérdida de tiempo para el proceso?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Rodríguez señala que lo ideal es que el caso estuviera en la justicia ordinaria desde el principio, porque si llegara a regresar a esta, habrá interrupciones y lo que ha avanzado la justicia militar no podrá ser usado después. Sin embargo, recuerda que la jurisdicción ordinaria ya había recaudado elementos probatorios que no van a perderse en sus primeras etapas y podrían ser usados más adelante.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque el abogado reconoció que fueron varias las órdenes y solicitudes que el despacho militar adelanto mientras la investigación estuvo en su jurisdicción, explica las razones de la familia para buscar que el mismo pase a manos de la jurisdicción ordinaria.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las razones para pedir que el caso de Dylan Cruz esté en la justicia ordinaria**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Rodríguez explica que la justicia penal militar es de excepción, es decir, para casos especiales en los que se juzgan consecuencias de hechos en los que se ven involucrados uniformados, y se necesita un cumplimiento estricto de dos elementos: que el presunto responsable esté vinculado a la Fuerza Pública, y que los hechos ocurran cuando se está en cumplimiento de una operación legítima.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En caso tal de que haya dudas sobre la legitimidad de esta operación, la misma debería ser investigada por la justicia ordinaria. Para el abogado y la familia de Dylan Cruz, esta legitimidad está cuestionada porque él no estaba en una confrontación con el ESMAD, y murió producto de un desconocimiento de los protocolos de acción de este cuerpo en el marco de la protesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, Rodríguez afirma que la justicia penal militar se ha mostrado históricamente parcial porque "allí existe todavía el tema de la jerarquía militar, entonces un capitán difícilmente va a condenar a un coronel". Por lo tanto, para el experto, hay un tipo de 'solidaridad' mal entendida por parte de quienes juzgan este tipo de actos que terminan afectando a civiles.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lo que viene para el caso**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Rodríguez manifestó que un elemento relevante para que la justicia ordinaria asuma la competencia del caso tiene que ver con tomar en cuenta los testigos del asesinato de Dylan, porque sus testimonios relatan un escenario diferente al presentado por los uniformados que declararon: señalando que no había una confrontación y no era necesaria una intervención como la que hizo el ESMAD.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese orden de ideas, el abogado espera que el Consejo Superior de la Judicatura valore esos testimonios, y dada la operación de la justicia por la pandemia, se presume que una decisión final sobre la jurisdicción que deberá tomar conocimiento del caso se produzca en dos meses. (Le puede interesar: ["Se teje impunidad por posibilidad que Justicia Penal Militar asuma caso de Nicolás Neira"](https://archivo.contagioradio.com/justicia-penal-militar-podria-asumir-el-caso-de-nicolas-neira/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
