Title: Cuba sale de la lista de países terroristas de EEUU
Date: 2015-05-29 12:50
Category: El mundo, Política
Tags: Barack Obama y Raúl Castro, Cuba, Cuba y EEUU, Cuba ya no está en lista de países terrorristas, Estados Unidos, Fidel Castro, Lista de países terroristas, Raul Castro, Reestablecimiento relaciones Cuba EEUU
Slug: cuba-sale-de-la-lista-de-paises-terroristas-de-eeuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/cuba-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Posta.com.MX 

###### [29May2015]

**EEUU ha sacado a Cuba de la lista de países terroristas**, en la que estaba **desde 1982,** por la estigmatización por parte de los gobierno sucesivos de Estados Unidos. El gesto supone un paso más** **para el restablecimiento total de las relaciones diplomáticas.

El **14 de Abril**, dos días después del encuentro entre Barack Obama y Raúl Castro en la Cumbre de las Américas en Panama, **Obama anunció su intención de sacar al país de la lista de países patrocinadores del terrorismo.** Para ello, dio un plazo de 45 días para que el congreso revisara la decisión y se pronunciara de manera definitiva sobre el asunto.

Esta mañana, el **Congreso** se pronunció así: "La rescisión de la designación de Cuba como país patrocinador del terrorismo **refleja nuestra valoración de que cumple los criterios estatutarios para ello".**

Cuba había sido acusada de **patrocinar las guerrillas Latinoamericanas,** y en el último tiempo de apoyar a miembros de las **FARC** y de dar refugio a perseguidos del grupo armado Vasco, conocido como **ETA.**

Estar en la **lista negra significa ser objeto de sanciones y de bloqueos económicos** como el de brindar ayuda económica a terceros países a través de mecanismos como el Banco Mundial, así como impedir el normal desarrollo diplomático del país. La lista se revisa anualmente. Actualmente están países como **Sudán, Irán o Siria**.

La inclusión de Cuba en la lista hizo parte del bloqueo económico de la isla que **no puede recibir ayudas económicas** a través de países que mantengan relaciones diplomáticas con EEUU o a través del Banco Mundial.

**La Isla aplaude la decisión puesto que consideraba injustificable seguir siendo parte de esta lista** y se consideraba esta decisión como parte de la política de represión aplicada desde EEUU contra Cuba.
