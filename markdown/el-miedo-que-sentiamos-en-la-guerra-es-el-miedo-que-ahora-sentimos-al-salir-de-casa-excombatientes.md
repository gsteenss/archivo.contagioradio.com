Title: El miedo que sentíamos en la guerra, es el miedo que ahora sentimos al salir de casa: excombatientes
Date: 2020-03-09 18:45
Author: CtgAdm
Category: Actualidad, Paz
Tags: asesinato de excombatientes, comision de la verdad, excombatientes
Slug: el-miedo-que-sentiamos-en-la-guerra-es-el-miedo-que-ahora-sentimos-al-salir-de-casa-excombatientes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/49641836812_5f9190626f_c.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: excombatientes/ Comisión de la Verdad

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/mauricio-jaramillo-sobre-exterminio-excombatientes-reincorporacion_md_48754055_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Mauricio Jaramillo | Integrante del Consejo Político Nacional del Partido FARC

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

Este 9 de marzo tuvo lugar el primer 'Espacio de Escucha' en la [Comisión para el Esclarecimiento de la Verdad (CEV),](https://twitter.com/ComisionVerdadC) una dinámica que hace parte del mandato de escucha plural de la institución y que **surge como respuesta a los homicidios de 186 excombatientes denunciados por el Partido FARC, que reunió a delegados de los Espacios Territoriales de Capacitación y Reincorporación y familiares de las víctimas para abordar y denunciar esta problemática.** [(Le puede interesar: Comenzó aporte de FARC al esclarecimiento de la verdad](https://archivo.contagioradio.com/aporte-farc-esclarecimiento-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El evento convocó diversos testimonios incluidos los de Armando Aroca, excombatiente de Puerto Guzmán (Putumayo); Luz Marina Giraldo, esposa del líder y excombatiente asesinado Alexander Parra; y el de José Torres, padre de Dimar Torres, excombatiente Farc asesinado por miembros del Ejército en Norte de Santander. [(Le recomendamos leer: Alexander Parra, representante ambiental y excombatiente es asesinado en ETCR)](https://archivo.contagioradio.com/alexander-parra-representante-ambiental-y-excombatiente-es-asesinado-en-etcr/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Estamos convencidos de que la guerra no es el camino. Pero hoy sentimos el mismo miedo que teníamos cuando combatíamos. Aunque tengo miedo estoy aquí y seguimos firmes en el cumplimiento del acuerdo” - Luz Marina Giraldo

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Dichas voces permitieron conocer de primera mano los problemas que se han presentado durante el proceso de reincorporación, la relación de los asesinatos con el cumplimiento del acuerdo y la forma en que las muertes de los firmantes de la paz han impactado negativamente las dinámicas de las poblaciones y los espacios territoriales donde habitan los y las excombatientes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el Partido FARC ha denunciado el homicidio de 186 excombatientes, según el último informe de la Misión de Verificación de Naciones Unidas en Colombia, el número total de asesinatos de excombatientes FARC es de 173 personas, a los que se suman 14 desapariciones y 29 atentados. **El año 2019 fue el más violento para los excombatientes desde la firma del Acuerdo de La Habana, con 77 asesinatos, que en comparación a los 65 registrados en 2018 y 31 en 2017, revelan un aumento de las agresiones.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

“No podemos caer en la ligereza de unos funcionarios del Estado de desvalorizar o quitarle el significado a las vidas que se están perdiendo”, manifestó a su vez el comisionado Saúl Franco, que también se refirió con preocupación a la "apatía de la sociedad frente a los asesinatos de excombatientes". [(Lea también: Un cacerolazo para amplificar la voz de casi 200 excombatientes asesinados)](https://archivo.contagioradio.com/cacerolazo-mplificar-voz-casi-200-excombatientes-asesinados/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Necesitamos voluntad política y de paz por parte del Gobierno" afirman excombatientes

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Para Mauricio Jaramillo, integrante del Consejo Político Nacional del Partido FARC, los asesinatos o por lo menos los testimonios que fueron presentados por la Comisión, han ocurrido en su mayoría a lo largo de los cerca de dos años del gobierno de Duque, por lo que señala lo importante que es tomar medidas en este momento. "Colombia lo que necesita para prosperar es voluntad de paz y tomar un camino que no sea el de la guerra, esa fue la manifestación de quienes hoy intervinieron: la desazón de que esa voluntad no existe por parte del Gobierno", argumenta. [(Le puede interesar: Astrid Conde Gutiérrez, tercera firmante de la paz asesinada en Bogotá)](https://archivo.contagioradio.com/astrid-conde-gutierrez-tercera-firmante-de-la-paz-asesinada-en-bogota/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Nosotros estamos dispuestos a continuar, estamos trasegando por estos momentos en los que decimos que la implementación, no aparece por ningún lado, hacemos grandes esfuerzos en los espacios territoriales para buscar recursos",** agrega el líder político, señalando que no solo se trata de una petición de los excombatientes sino de las mismas comunidades que se suman al clamor de vivir en un país sin violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El espacio, propiciado por la CEV es el primero de una serie de encuentros que permitirán escuchar todas las voces, de todos los sectores de la sociedad y sus aportes para consolidar el Acuerdo de Paz, "el compromiso de la Comisión con la verdad incluye la reconciliación y la no repetición, lo que nos obliga a interpelar el presente y el trabajo que tenemos que hacer tiene que ver con la construcción de un futuro donde se respete la vida y dignidad de todos los ciudadanos de este país", manifestó la comisionada Lucía González durante el evento.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
