Title: Ex-general golpista gana las elecciones en Nigeria
Date: 2015-04-02 18:48
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Buhari, Ex general Buhari es elegido presidente de Nigeria, Nigeria cambia de gobierno
Slug: ex-general-golpista-gana-las-elecciones-en-nigeria
Status: published

###### Foto:Vistazo.com 

**Muhammadu Buhari**, líder de la oposición ha **ganado las elecciones en Nigeria**, tras **16 años de gobierno del Partido Democrático Popular**, que ha sumido al país en una grave crisis económica debido a la **extensa red de corrupción que había establecido**.

Desde 1999 gobernaba el **PDP**, hasta que se conformo una coalición formada por los 4 principales partidos de la oposición para poder alcanzar el poder.

A diferencia de las elecciones de **2011, que finalizaron con fuertes enfrentamientos** entre opositores y militantes del partido vencedor, esta vez el **líder saliente** ha **reconocido la** **victoria de la oposición**, felicitando al PDP por su campaña electoral. Este es un gesto importante para la paz en el país y en la propia región.

**Buhari** ex-general del ejército, fue presidente del país tras **encabezar un golpe de estado entre 1984 y 1985**.El ex-general se ha presentado a las elecciones hasta tres veces, 2007, 2010 y 2013, perdiendo en todas ellas. Ha sido **calificado de dictador por organizaciones de DDHH**, y tiene fama en el país de ser implacable con la corrupción.

**Nigeria**, es la primera economía de África y el país más poblado del continente, también es el país que más refugiados acoge de las guerras de los grandes lagos, como las de Congo, Somalia o Ruanda.

El país actualmente enfrenta distintos **conflictos armados**, el de mayor importancia es el de la milicia islamista de carácter extremista **Boko Haram**, que en los últimos años ha tomado fuertes posiciones en el noreste, y la **amenaza de los rebeldes en el delta del Níger**.

El **presidente entrante** ha declarado que en unos meses **va a acabar con la amenaza de Boko Haram y a pacificar el país**. También ha felicitado a los observadores internacionales por su labor en la verificación de las elecciones, que según los diplomáticos han sido totalmente transparentes.
