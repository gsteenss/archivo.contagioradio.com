Title: Emergencia sanitaria en las prisiones Colombianas
Date: 2015-04-19 09:49
Author: CtgAdm
Category: DDHH, Hablemos alguito
Tags: Crisis carcelaria colombia, Emergencia humanitaria salud presos Colombia, ley 1709, Muertes en prisones Colombianas debido a sistema sanitario, Solución emergencia sanitaria prisiones colombia
Slug: emergencia-sanitaria-en-las-prisiones-colombianas-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/descarga-7.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Pulzo.com 

###### **Entrevista con [Ingrid Saavedra], abogada defensora de DDHH del Comité de Solidaridad de Presos Políticos:** 

<iframe src="http://www.ivoox.com/player_ek_4369990_2_1.html?data=lZijm56ddI6ZmKialJ2Jd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic7Z08zS0MjNpYznwtPW1sbWrcKfxtOYzsbXb9HmytjW0dPJt4y30NHcz8fNpc%2FV1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La situación del **sistema sanitario en las prisiones de Colombia está provocando una emergencia humanitaria** de impredecibles consecuencias para la población reclusa pero también para la población que se encuentra en libertad. Sólo en el último año han **fallecido 257 personas dentro de las prisiones y de ellas 57 relacionadas con enfermedades sin atención médica.**

La ausencia de cobertura sanitaria y la difícil situación de habitabilidad debido al hacinamiento que es la principal problemática actual del sistema penitenciario es la mayor causa del agravamiento de la situación de los enfermos.

Según Ingrid Saavedra, abogada defensora de DDHH del Comité de solidaridad de Presos Políticos,la situación de emergencia se deriva de la decisión del **gobierno de 2009, cuando decidió que la salud de los presos pasaba a formar parte de la Seguridad social y no del propio INPEC,** como funcionaba anteriormente.

Fue entonces cuando se decidió que la EPS **CAPRECOM** debía de hacerse responsable de la cobertura sanitaria de las prisiones. CAPRECOM en el momento de ausmir las responsabilidades **se encontraba en  quiebra, y sin ningún tipo de recursos**, ya ha quedado demostrado a través de los distintos informes y por el propio gobierno que la EPS no puede asumir las necesidades sanitarias de la población privada de la libertad.

Con el tiempo CAPRECOM dejó de asumir sus responsabilidades y casi todos los casos donde había una emergencia fueron asumidos mediante **tutelas o por interposición de medidas cautelares emitidas por la CIDH.**

La conclusión final es que la EPS no estaba ni está preparada para asumir la salud penitenciaria en el país, pero además es culpable de el agravamiento de la enfermedad de la población reclusa durante su funcionamiento.

Es importante analizar la situación sanitaria desgranando la problemática por partes, comenzando por las **enfermedades consecuencia de la propia situación precaria de habitabilidad, alimentaria y de sistemática violación de los DDHH** en los centros de privación de la libertad, pero también brindar de ejemplos de la propia cotidianidad de la salud y la problemática.

Comenzando por las enfermedades **derivadas por la desnutrición**, entre ellas todas las de orden gastrointestinal o las referentes a todo el sistema digestivo son la orden del día y los informes hablan de que casi la totalidad de los reclusos las padecen, así como las enfermedades musculatorias procedentes de la escasa movilidad dentro del centro.

En general todas las enfermedades que una **persona privada de la libertad tenía antes de entrar se agravan dentro.**

Centrándonos en casos concretos, la peor situación es la de los **enfermos mentales o toxicodependientes que son en total 2117** entre todas las penitenciarias del estado, que no reciben ningún tipo de asistencia y además se les recetan medicamentos sin prescripción médica o psiquiátrica.

También son los heridos de guerra que la gran mayoría continua con las **heridas de guerra** hasta que acaban produciendo **enfermedades infecciosas, teniendo que realizarse amputaciones.**

CAPRECOM no ha desplegado **ningún tipo de política en prevención** o atención primaria de tal manera no existe ningún tipo de prevención en enfermedades de **VIH o de detección temprana provocando una autentica epidemia** que se extiende a la población en libertad a través de las relaciones sexuales entre los reclusos y sus propias parejas en libertad.

Para la población con **discapacidad tampoco existe ningún tipo de adaptación en su movilidad, con lo que su enfermedad se agrava** debido a que no hay ningún tipo de espacio adaptado con el objetivo de poder desarrollar capacidades que impidan su propio estancamiento.

Otra de las peores situaciones es la de las **mujeres embarazadas** que por ley deberían salir en libertad por un periodo de hasta 4 meses para poder dar a luz, pero por el contrario **el estado no lo está permitiendo y dan a luz en prisión sin que haya ningún tipo de especialista** provocando enfermedades tanto para la madre como para el neonato.

Las **enfermedades crónicas en fase de terminación** son otra grave problemática, en las que el **estado debería actuar declarando la libertad** para las personas en situación terminal. **Esto no sucede** hasta el punto de que muchos de ellos mueren dentro del propio centro.

Ejemplos de **negligencias médicas** como la del preso **Jesús Velandia León que falleció en el hospital por un cáncer que no tuvo ningún tipo de atención medica** y que podría haber sido curado si hubiese sido atendido.

Por último la situación precaria de las prisiones provoca episodios constantes de **violencia,** principalmente provocada por los **excesivos castigos y las violaciones de los DDHH y la torura sistemática** a la que son sometidos los reclusos por el INPEC provocan graves daños en la salud de los reclusos.

Para el Comité de Solidaridad de Presos Políticos la solución pasa por **conformar un fondo estatal para la salud de los presos** y de que su gestión sea estatal y que la entidad garantice los derechos de la salud a la población privada de libertad.

Los informes emitidos por la ONU, la OMTC y la Comisión Colombiana contra la Tortura dejan claro la urgente necesidad de que el estado Colombiano disponga de las medidas necesarias para garantizar la salud en prisión, a su vez se han realizado visitas hospitalarias pero no existen actualmente estadísticas que monitoreen la situación de los enfermos dentro de las cárceles.
