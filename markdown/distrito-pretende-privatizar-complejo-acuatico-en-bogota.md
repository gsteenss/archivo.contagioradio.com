Title: ¿Distrito privatizaría Complejo Acuático en Bogotá?
Date: 2017-08-15 13:13
Category: Economía, Nacional
Tags: Alcaldía de Bogotá, Complejo acuático, Deporte, Enrique Peñalosa, privatización complejo acuático
Slug: distrito-pretende-privatizar-complejo-acuatico-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/complejo-acuático-e1502819084554.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Clavados Bogotá] 

###### [15 Ago 2017] 

El concejal por Bogotá del partido Alianza Verde, Antonio Sanguino, denunció que el **IDRD va a empezar a cobrar por el uso de la infraestructura del complejo acuático** en la ciudad de Bogotá, afectando a 24 clubes que hacen uso del escenario y a más de 4 mil niños y jóvenes nadadores se verían afectados con la medida a partir del 1 de septiembre.

El Complejo Acuático fue construido en el 2005 durante la administración de Luis Eduardo Garzón como escenario público para la práctica de natación. En la ciudad, **es el único lugar que hay diseñado para el entrenamiento de clavados**. Con esta medida de privatización lo que se estaría haciendo, según el concejal, es construir escenarios con “recursos públicos que son puestos al servicio de los ciudadanos a cambio de un pago”.

Según Sanguino, “al director del IDRD se le ha ocurrido cobrar por el **uso de la infraestructura lo que significa la privatización de un escenario público**”. Indicó además, que el cobro que se hace deberá ser pagado por los deportistas y sus familias quienes ya pagan por el total de la formación deportiva. (Le puede interesar: ["Terreno para orquesta filarmónica de Bogotá se convierte en parqueadero"](https://archivo.contagioradio.com/alcaldia-de-bogota-hara-parqueaderos-donde-iba-a-ser-sede-de-orquesta-filarmonica/))

Sanguino manifestó que para el uso de las instalaciones del complejo acuático ya existe una tabla de cobros donde los clubes de natación de la ciudad **“tienen que pagar una mensualidad que está por encima de los 200 mil pesos”**. Ahora con esta medida, “se está excluyendo a los deportistas de más bajos recursos para que puedan realizar la práctica deportiva”.

Si bien no es claro que se hará con los dineros recogidos con el cobro, el concejal manifestó que **“seguramente dirán que es para mantenimiento de la infraestructura, pero esto se hace por medio de los impuestos que se pagan al IDRD”**. Además, aseguró que en Bogotá hay escenarios deportivos abandonados porque “hay una mentalidad de que lo público se le debe cobrar a los ciudadanos”. (Le puede interesar: ["Nuevo intento de vender la ETB no sería aprobado en el concejo"](https://archivo.contagioradio.com/nuevo-intento-de-penalosa-por-vender-la-etb-no-seria-aprobado-por-el-concejo/))

### **Proyecto de Peñalosa para construir complejo de recreación en el norte de Bogotá no fue aprobado por el concejo de Bogotá** 

A mediados de junio, **Enrique Peñalosa propuso crear un centro deportivo en el barrio El Retiro específicamente en la calle 82 con carrera 11.** Allí iba a construir piscinas y canchas de fútbol para que se beneficiaran los habitantes y la población flotante del sector.

Esta propuesta fue bastante polémica en la medida que varios sectores de la ciudadanía manifestaron que **Bogotá tiene necesidades más urgentes** y se trataba de un proyecto que no beneficiaba a todos los bogotanos y en especial a los capitalinos con bajos recursos.

El concejal Sanguino manifestó que “en este proyecto de valorización, el alcalde intentó que el concejo le aprobase un cobro para este parque y otras obras, pero la presión ciudadana y la oposición de sectores del consejo **lograron que este proyecto se hundiera en el consejo**”.

<iframe id="audio_20348453" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20348453_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
