Title: Juan Carlos Losada no ha recibido notificación sobre anulación de su curul
Date: 2015-11-04 18:59
Category: Nacional, Política
Tags: Alberto Yepes, Anulación de curul de Juan Carlos Losada, Concejo de Estado, Congreso de la República, Consulta Antitaurina, Juan Carlos Losada, Partido Liberal, Patarroyo, Proyecto de Ley 172
Slug: juan-carlos-losada-aseguro-que-no-ha-recibido-notificacion-sobre-anulacion-de-su-curul
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/JUAN_CARLOS_LOSADA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [commons.wikimedia.org]

###### [4 Nov 2015]

Frente a la posible desacreditación como representante a la Cámara por el Partido Liberal, **Juan Carlos Losada, aseguró que aún no le ha llegado ningún tipo de  notificación,** pese a que según diversos medios, la decisión ya se tomó en la Sección Quinta, con ponencia del magistrado Alberto Yepes, quien habría encontrado que la votación del candidato por el Partido Liberal estuvo mediada por irregularidades.

Cabe recodar que el representante, ha trabajo desde su curul por los derechos del ambiente y los animales, impulsando diferentes iniciativas en pro de la defensa de la naturaleza.

Así mismo, es de resaltar que las propuestas desde el movimiento animalista para defender la vida de todos los seres sintientes, han sido debilitadas por el Concejo de Estado, que suspendió la consulta popular antitaurina, dio vía libre a **Manuel Elkin Patarroyo para que continúe experimentando con primates, y ahora estaría cancelando la curul del representante Losada, quien es el autor del proyecto de Ley 172 que busca la penalización del maltrato animal.**

COMUNICADO

No he sido notificado de ninguna decisión del Consejo de Estado sobre la anulación de mi elección como Representante a la Cámara por Bogotá.

No obstante, ante las informaciones difundidas por diferentes medios de comunicación me permito señalar que:  
1. Si efectivamente esa es la decisión de la más alta corporación en lo contencioso administrativo, la acataré como corresponde en un Estado de Derecho. [  
2. Ejerceré, luego de hacer las consultas jurídicas correspondientes, los recursos que quepan contra dicha decisión.  
3. Aún por fuera del Congreso seguiré trabajando por las causas que he defendido desde la Cámara, buscando siempre ennoblecer el ejercicio de la política, poniéndola al servicio de los mejores intereses de nuestros conciudadanos y de una sociedad con valores que privilegie el respeto por la vida y por el entorno que la hace viable.  
4. Quiero dar gracias a todos los que votaron por mí a conciencia y a quienes me han acompañado como Representante.]{.text_exposed_show}

<div class="text_exposed_show">

Ustedes cuentan conmigo.

</div>
