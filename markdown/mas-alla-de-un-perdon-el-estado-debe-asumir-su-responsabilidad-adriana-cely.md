Title: "Más allá de un perdón, el Estado debe asumir su responsabilidad": Adriana Cely
Date: 2016-05-19 13:56
Category: Entrevistas, Mujer
Tags: feminicidio, Rosa Elvira Cely, Secretaria de Gobierno, Violencia contra la mujer
Slug: mas-alla-de-un-perdon-el-estado-debe-asumir-su-responsabilidad-adriana-cely
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/rosacely-110506-14-de-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [19 May 2016] 

“Nadie se imagina la afectación tan grande de la familia, esto es una falta de respeto, es algo que uno no cree. Mi sobrina tiene 16 años y está completamente consternada, mi mamá está afectadísima. **Es más complejo manejar esta situación incluso que lo que sucedió hace 4 años, esto es inaudito, es algo indigno, algo que no tiene razón de ser”**, expresa Adriana Cely, hermana de Rosa Elvira Cely, ambas revictimizadas por la institucionalidad, esta vez, por la Secretaría de Gobierno de Bogotá.

Después de la resolución que expidió la Secretaría en donde culpa a Rosa Elvira Cely de su asesinato, violación y tortura, ni la Secretaría de Gobierno ni las demás entidades responsables, han pedido disculpas personalmente a la familia de la víctima. Adriana Cely exige que **más allá de un perdón, la institucionalidad debe asumir su responsabilidad en el feminicidio de su hermana** y se deben implementar todas las acciones necesarias para enfrentar los distintos tipos de violencias contra las mujeres.

La demanda interpuesta contra la Fiscalía, la Secretaría de Salud y la de Gobierno inició su trámite desde agosto del año pasado.  En el proceso han existido dos conciliaciones en donde ninguna de las entidades demandadas ha querido conciliar, argumentando que se prestaron todos los protocolos de forma debida.  La única respuesta hasta el momento ha sido por parte de la Secretaría de Gobierno que culpó a Rosa de su propio crimen, y **aunque se ha anunciado una rectificación, la familia aún no conoce el documento.**

La abogada Luz Stella Boada contratada en enero de este año por el secretario Miguel Uribe Turbay, no era la primera vez que conocía el caso, incluso ella estuvo en los anteriores intentos de conciliación. **Al finalizar abril la familia ya conocía esa resolución emitida por la Secretaría de Gobierno, sin embargo, dieron un poco de tiempo esperando que se pronunciaran, pero no pasó nada**, hasta que decidieron hacerlo público. De no ser así, probablemente nunca Uribe Turbay hubiera dicho algo, y mucho menos Peñalosa hubiera enviado un tuit exigiendo la rectificación.

¿Si así se maneja un caso emblemático, cómo serán los de otras mujeres?, es una de las principales preguntas, pero sobre todo preocupaciones de Cely, quien ha reiterado en diferentes ocasiones que más allá de la reparación económica, el feminicidio de su hermana debe servir para que el Estado tome acciones urgentes para que no se siga permitiendo este tipo de crímenes, analizándolos como un problema estructural y no como un problema exclusivo de una administración distrital. “**A nosotras no nos repara un dinero. Un dinero no me devuelve a mi hermana, ni una mamá a mi sobrina, lo que se debe hacer es analizar desde donde empiezan los errores**”.

Para Adriana, en manos del alcalde Enrique Peñalosa está investigar qué fue lo pasó, pues no se trata de la culpa exclusiva de una funcionaria sino de todo un equipo que debería estar al frente un caso emblemático que no puede firmar y responder cualquier persona.

"[Estamos siendo violentados institucionalmente](https://archivo.contagioradio.com/rosa-elvira-cely-6-veces-victimizada/). No me arrepiento de haber denunciado al Estado porque creo que es una forma de visibilizar la violencia, esto no es solo por mi hermana sino por las miles de mujeres que son víctimas de todo tipo de violencia, y **el Estado tiene que responsabilizarse de todas las muertes de todas las mujeres de este país**”.

### “La vida nos cambió un 100%” 

Detrás de este caso, está también el drama de una familia de escasos recursos que tuvo que enfrentarse a un Estado que 4 años después no se responsabiliza por el contexto de desamparo estatal en el que sucedió este crimen, pero tampoco se responsabiliza frente a las más de mil mujeres que fueron asesinadas en Colombia durante el  2015, o las más de 16 mil denuncias por abuso sexual del año pasado.

**Adriana Cely, asegura que la vida de ella, de su madre y la de su sobrina tuvo un cambio del 100%.** Su mamá, una mujer de casi 70 años ha sufrido bastante, pues carga con el peso de haber visto morir a dos de sus hijos de manera violenta. Actualmente padece serios problemas de salud que se agravan con actos como el de la Secretaría de Gobierno, además no cuenta con una pensión a pesar de haber tenido que trabajar durante toda su vida para sacar sola adelante a sus hijos.

Para Juliana, la hija de Rosa Elvira, tampoco ha sido fácil. En medio de la fortaleza que le ha servido para defender los derechos de las niñas, ha sufrido durante estos 4 años, pues **extraña a Rosa Elvira en fechas tan especiales, como su cumpleaños o el día de la madre.**

Por su parte, Adriana nunca esperó centrar su vida en la defensa de los derechos de las mujeres. Pero ahora, en medio del dolor, se prepara profesionalmente como trabajadora social para continuar con su labor en la Secretaría Distrital de la Mujer donde intenta acompañar a las mujeres víctimas de violencia, y cuyo trabajo es el único sustento de su familia.

**Cely lucha desde el comienzo por lograr una reparación colectiva impulsado la creación de un centro de atención sobre violencia sexual,** y aunque a veces se desmotiva pues ve que “las leyes siguen en un papel”, asegura que la fortaleza la lleva en su hermana quien fue la que la puso ahí, “todos los días me levanto por Rosa Elvira y todas las mujeres de este país, siento que hago algo por la memoria de mi hermana. **Tengo la fortaleza que me dejó mi hermana, ella no pudo haber muerto en vano.** Seguiremos luchando y trabajando por un futuro diferente”.

El próximo **28 de mayo se llevará a cabo una vigilia a las 7:00 de la noche el Parque Nacional y el 29 de mayo se realizará una movilización que iniciará en el Planetario de Bogotá** hasta el Parque Nacional donde se harán diferentes actividades culturales en memoria de Rosa Elvira y todas las mujeres víctimas del machismo y la indiferencia estatal.

<iframe src="http://co.ivoox.com/es/player_ej_11590946_2_1.html?data=kpaim5WdeJehhpywj5edaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5yncaLY087O0MaPh8bg2pCaja3Jts7Vz8aYtNTXpYy3xtHmj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
