Title: Zona Veredal Carlos Perdomo pide garantías de seguridad
Date: 2017-05-15 13:14
Category: Nacional, Paz
Tags: FARC, Zona Veredal Carlos Perdomo, Zonas Veredales
Slug: zona-veredal-carlos-perdomo-pide-investigar-hechos-delictivos-en-caldono-atribuidos-a-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/cms-image-000059364-e1494868212484.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ConfidencialColombia.com] 

###### [15 mayo 2017] 

Los miembros de la Zona Verdal Carlos Perdomo le hicieron un llamado a la comunidad en general para que, por medio de acciones conjuntas, **se garantice la seguridad de las Zonas Veredales** y sostuvieron que, ante el aumento de la inseguridad en Caldono, se debe dejar de relacionar a los miembros de las FARC como los causantes de hechos delictivos.

En un comunicado a la opinión pública, la Zona Veredal de Transición y Normalización Carlos Perdomo, pidió que las autoridades tradicionales de Caldono, Cauca y los miembros de las ZVTN se unan para garantizar la seguridad y la implementación de los acuerdos de paz.

En el comunicado, Los miembros de la Zona Veredal Carlos Perdomo, aclararon que "**todos los miembros están cumpliendo estrictamente el acuerdo de Cese al Fuego y Hostilidades Bilateral y Definitivo y la Dejación de Armas". **Aseguraron además que "no se ha presentado ningún incidente grave que involucre a los integrantes de la ZVTN y que la totalidad del armamento de que dispone la guerrilla en esta zona está registrado, marcado y controlado la ONU, donde no se ha presentado ninguna novedad".

El pasado 29 de abril, en el sitio Pescador, murió un ex-miliciano registrado en la Zona Veredal. Ante esto, las FARC aclararon que "**esta persona había desertado el día anterior, por tanto no podía actuar como miembro de nuestra organización".** Además, establecieron que "sus acompañantes, así como el armamento implicado allí, no pertenecen a las FARC-EP, ni están relacionados con la Zona Veredal CARLOS PERDOMO".

Las Farc aclararon que no han autorizado "a ningún guerrillero ni miliciano, a cobrar impuestos ni desarrollar ninguna actividad de economía de guerra o acción militar. Garantizamos que quienes actúen en nombre de las FARC-EP para extorsionar, atracar o intimidar de cualquier forma a la población, **no pertenecen a nuestra organización revolucionaria".**

Algunos miembros de la Zona Veredal Carlos Perdomo se encuentran **realizando tareas de pedagogía de paz** socializando los acuerdos que se hicieron entre el gobierno y las Farc.

###### Reciba toda la información de Contagio Radio en [[su correo]
