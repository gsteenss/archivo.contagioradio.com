Title: Organizaciones señalan como "Inadmisible" manejo de información sobre secuestro de periodistas ecuatorianos
Date: 2018-04-12 17:15
Category: DDHH, Nacional
Tags: colombia, Disidencias FARC-EP, ecuador
Slug: organizaciones-senalan-inadmisible-manejo-informacion-secuestro-periodistas-ecuatorianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/nosfaltan3ES2-master1050.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: The New York Times] 

###### [12 Abr 2018] 

En un comunicado de prensa, diferentes organizaciones defensoras de la libertad de expresión en Colombia, pidieron al grupo disidente "Frente Oliver Sinesterra de las FARC-EP"  la liberación inmediata de los trabajadores de El diario El Comercio. Asimismo, afirmaron que **es "inadmisible" el manejo de la información pública que tanto el gobierno de Colombia, como de Ecuador,** le han dado a este hecho relacionado con el paradero de las personas.

Las organizaciones Fundamedios, Fundación para la Libertad de Prensa, Reporteros sin Frontera y el Comité para la Protección de Periodistas, señalaron en el documento que en primera instancia, durante estos 16 días, las autoridades ecuatorianas han manifestado que el secuestro se dio en territorio colombiano, a su vez las autoridades colombianas afirman lo contrario, es decir que las personas se encuentran en territorio ecuatoriano.

**Situación que evidencia una evasión por parte de los dos gobiernos de sus responsabilidad, mientras emiten información contradictoria** a los medios de comunicación. En ese mismo sentido aseguraron que el gobierno Ecuatoriano no puede pretender ser la única fuente de información.

### **Las acciones militares ** 

Otra de las preocupaciones de las organizaciones, tiene que ver con las acciones militares que se pueden estar adelantando para salvar a las 3 personas secuestras. Razón por la cual señalaron que así como el gobierno Ecuatoriano declaró que no ha realizado operaciones ofensivas, **el gobierno Colombiano debe dar un informe detallado sobre las operaciones militares contra las disidencias de las FARC-EP**, en la región, durante estos últimos 16 días.

De igual forma instaron a los gobiernos, para que en el marco de la Cumbre de las Américas, también se de un informe de todas las acciones emprendidas en busca de la liberación de **Javier Ortega, periodista, Paúl Rivas, fotógrafo y Efraín Segarra, conductor**.

Otra de las solicitudes que hacen, a elevan la Comisión Interamericana de Derechos Humanos, para que cobije este caso con medidas cautelares y brinde protección a los integrantes de El diario El Comercio.

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
