Title: Mujeres cocaleras de todo el país se reúnen para trabajar propuestas de sustitución
Date: 2017-03-16 17:27
Category: Nacional, Paz
Tags: coca, Cultivos de coca, mujeres cocaleras
Slug: mujeres-cocaleras-de-todo-el-pais-se-reunen-para-trabajar-propuestas-de-sustitucion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/cocaleras-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: sputniknews] 

###### [16 Mar. 2017] 

Este 17 y 18 de marzo en Puerto Asís, Putumayo, se estará llevando a cabo un **encuentro internacional de mujeres cocaleras en el que las mujeres de diversos lugares del país estarán discutiendo** sobre lo qué ha significado la hoja de coca en sus vidas, qué les ha facilitado, qué problemas han tenido y cómo van a enfrentar la sustitución de cultivos que se encuentran consignados en los Acuerdos de Paz.

**Luz Caicedo integrante de la Corporación Humanas**, una de las organizaciones convocantes a este encuentro, manifestó que **“las mujeres que cultivan coca han estado haciendo un trabajo en un contexto caracterizado por la exclusión**, por los problemas que tiene el agro colombiano, además de la dificultad para poder dedicarse a cultivos lícitos debido a que no hay vías de comunicación ni políticas para los cultivadores”.

Este encuentro en el que **participarán más de 100 mujeres de todo el país dentro de las que se encuentran cerca de 70 mujeres cocaleras**, también trabajará el tema del poder adquisitivo casi nulo con el que cuentan las mujeres rurales “en general son los hombres los que manejan el dinero del hogar y los que determinan los gastos, sin contar que las mujeres tienen largas jornadas que deben cumplir sin tener un poder adquisitivo” relata Caicedo. Le puede interesar: [Erradicación de cultivos de uso ilicito van en contra de Acuerdo de Paz](https://archivo.contagioradio.com/erradicacion-de-cultivos-hecha-por-el-gobierno-iria-en-contra-de-acuerdo-de-paz/)

En el lugar contarán con la presencia de invitadas internacionales de países como Bolivia para “**conocer cómo ha sido la participación de las mujeres en el proceso de sustitución de cultivos en Bolivia.** Sustitución que ha resultado del hecho que la hoja de coca es un bien ancestral y que su uso no debe ser destinado para cultivos de uso ilícito, sino para productos como aromáticas, harinas, entre otros. Para dejarlo de ver como un mercado ilícito” añadió Caicedo. Le puede interesar:["Comunidades del Putumayo insisten en plan de sustitución a pesar de erradicación forzada"](https://archivo.contagioradio.com/comunidades-del-putumayo-insisten-en-plan-de-sustitucion-a-pesar-de-erradicacion-forzad/)

Finalmente, luego de este encuentro en el que las mujeres cocaleras podrán escucharse y aprender de diversas experiencias del país y de Bolivia, podrán  aprovechar para **discutir las oportunidades que les da el Acuerdo de Paz.**

“Queremos aprovechar que en el Acuerdo de Paz quedó claramente establecido que **las mujeres somos participantes activas de la construcción de posconflicto y que también somos participantes activas y decisivas** en la determinación de los planes de desarrollo que van a sustituir los cultivos ilícitos” afirmó Caicedo. Le puede interesar: [Familias Cocaleras ya tienen Plan de Sustitución de Cultivos](https://archivo.contagioradio.com/familias-cocaleras-ya-tienen-plan-de-sustitucion-de-cultivos/)

<iframe id="audio_17605901" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17605901_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
