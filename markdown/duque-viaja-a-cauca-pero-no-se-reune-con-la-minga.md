Title: Duque viaja a Cauca pero no se reúne con la Minga
Date: 2019-04-09 20:06
Author: CtgAdm
Category: Comunidad, Política
Tags: CRIC, Iván Duque, Minga Nacional
Slug: duque-viaja-a-cauca-pero-no-se-reune-con-la-minga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/WhatsApp-Image-2019-04-09-at-2.49.01-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC] 

Pese a que hoy era el día en que se daría el esperado encuentro entre los pueblos que integran la Minga del suroccidente del país y el presidente Duque, la reunión no tuvo lugar pues aunque el mandatario llegó a **Caldono, Cauca**, este manifestó no tener garantías de seguridad ante la solicitud que hacían las comunidades de realizar  el concilio en la plaza principal para el debate público.

Miles de mingueros, entre ellos numerosos ancianos, niños y mujeres, esperaron al presidente que desde las 10:00 arribó al departamento del Cauca, sin embargo la silla destinada para el dirigente permaneció vacía, dicho lugar, argumentan, fue elegido pues el diálogo no debía ser una reunión a puerta cerrada con las autoridades, sino con todos los mingueros.

Desde el **Consejo Regional Indígena del Cauca (CRIC)** aseguran que se le brindaron todas las garantías de seguridad al presidente, sin embargo desde la Presidencia insisten en que el debate debía realizarse en un espacio cerrado, indicando a Duque que las condiciones de la plaza para el encuentro no eran óptimas, pues esperaban un encuentro con únicamente cerca de 120 personas. [(Le puede interesar: La Minga vive y cumplió con su palabra, ahora solo falta la del Gobierno)](https://archivo.contagioradio.com/la-minga-cumplio-con-su-palabra-ahora-solo-falta-la-del-gobierno/)

> La minga continúa esperando al presidente de la república Ivan Duque, quién ha dicho qué se reunirá con un grupo pequeño para debatir. La respuesta de la minga fue continuar esperando para el debate en la plaza pública.
>
> — CRIC Colombia Cauca (@CRIC\_Cauca) [9 de abril de 2019](https://twitter.com/CRIC_Cauca/status/1115673193774309376?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
"Nosotros volveremos a nuestros territorios y haremos una junta directiva para realizar y evaluar, habrá una movilización, esto crecerá más, no solo era el movimiento indígena, había muchos movimientos sociales que estaban pendientes de lo que iba a suceder"  asegura **Neis Oliverio Lame, consejero del CRIC** quien además agrega que la Minga está abierta al diálogo para una próxima reunión, el hecho de que haya quedado una silla vacía no creo que eche lo acordado para atrás, creo que si el presidente  lo hace  daría una muestra al país de falta de palabra y responsabilidad " añadió el mandatario indígena.

 

[Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
