Title: Campesinos de Tumaco y gobierno logran acuerdo sobre sustitución de cultivos
Date: 2017-04-04 16:31
Category: Movilización, Nacional
Tags: COCCAM, programas de sustitución de cultivos, Sergio Jaramillo
Slug: campesinos-de-tumaco-y-gobierno-logran-acuerdo-sobre-sustitucion-de-cultivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Cultivos-de-coca-e1468887460603.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partitcular] 

###### [04 Abril 2017] 

Tras dos semanas de bloqueos y movilizaciones, los campesinos de Tumaco llegaron a un nuevo acuerdo con el gobierno Nacional que ampliará el plazo para que se pueda realizar un censo **mucho más específico de los pequeños y medianos cultivadores que entrarán a formar parte del programa de sustitución de cultivos ilícitos.**

El censo deberá llegar a las zonas más recónditas de Nariño y tomar datos de cuantos cultivos tiene cada campesino, cuáles son sus condiciones económicas y sociales en las que se encuentran las familias. Sobre esta información se crearán los planes de sustitución.

Posteriormente cada comunidad o vereda hará una **asamblea y junto con ellas, trabajaran los comités tripartitos, para hacer verificación y monitoreo** a los planes de implementación del gobierno.

Al mismo tiempo será el gobierno el encargado de **coordinar interinstitucionalmente la implementación de las estrategias de sustitución y erradicación** con el propósito de evitar incidentes que afecten a las comunidades. Le puede interesar:["Plan de sustitución voluntaria no se está cumpliendo: COCCAM"](https://archivo.contagioradio.com/plan-de-sustitucion-voluntaria-no-se-esta-cumpliendococcam/)

Otro de los compromisos adquiridos por el gobierno es retirar de la zona a las Fuerzas Militares y Policiacas que estaban erradicando, hasta tanto no haya comenzado el programa de sustitución. A su vez, la Oficina de Derechos Humanos del Alto Comisionado de las Naciones Unidas, contribuirá al seguimiento de las investigaciones sobre los hechos de **violencia que ocasionaron heridas a civiles y la muerte del integrante de la Policía Bayron Fernando Reicalde**, como consecuencia del bloqueo de la vía al mar. Le puede interesar: ["Un policía muerto y más de 10 campesinos heridos durante bloqueos en Tumaco"](https://archivo.contagioradio.com/un-policia-muerto-y-mas-de-10-campesinos-heridos-durante-bloqueos-en-tumaco/)

De acuerdo con Iván Rosero, vicepresidente de la Junta de Acción Comunal de Llorente, se fijaron plazos de cumplimiento para estos acuerdos, sin embargo, afirmó que de no ser este el caso, volvería a darse un choque entre ambas partes **“los que pagan en últimas las consecuencias de los incumplimientos son los campesinos y los policías que son gente del pueblo, no se desea un enfrentamiento**”.

\[caption id="attachment\_38807" align="alignnone" width="725"\]![COCCAM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/co.jpg){.size-full .wp-image-38807 width="725" height="929"} COCCAM\[/caption\]  
<iframe id="audio_17964353" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17964353_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
