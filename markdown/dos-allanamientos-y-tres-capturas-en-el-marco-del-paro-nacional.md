Title: Dos allanamientos y tres capturas en el marco del Paro Nacional
Date: 2020-01-21 09:55
Author: CtgAdm
Category: DDHH, Nacional
Tags: Allanamientos, Bogotá, Paro21E, ParoNacional
Slug: dos-allanamientos-y-tres-capturas-en-el-marco-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/allanamientos-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

La Red Popular de Derechos Humanos (REDHUS) Bogotá reportó a tempranas horas de este 21 de enero, dos allanamientos por parte de la Policía a las viviendas de integrantes del movimiento social, activos en las diferentes jornadas del Paro Nacional vividas en el 2019. (Le puede interesar: <https://archivo.contagioradio.com/las-rutas-de-movilizacion-para-el-21e/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según Isabel Fajardo, integrante de REDHUS las intervenciones se presentaron cerca de las 7:30 am, en las casas de David Ravelo y Jaime Hernando Olarte, "creemos que se pueden estar configurando falsos positivos judiciales como ocurrió días antes de la primera jornada de movilizaciones el año pasado".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La defensora informó que los jóvenes recibieron acompañamiento jurídico por parte de abogados de diferentes organizaciones quienes hacen seguimientos a los allanamientos, uno de los colectivos allí presentes es la Comisión Colombiana de Juristas (CCJ). (Le puede interesar: <https://www.justiciaypazcolombia.com/nuevo-protocolo-de-la-alcaldia-mayor-para-la-movilizacion-y-la-protesta-social/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la posibilidad de posibles arrestos hacia estos jóvenes Fajardo manifestó, *"se pueden presentar capturas, pero no conocemos a profundidad el objetivo de la diligencia*; *las autoridades podrían estar haciendo un seguimiento a través de redes sociales, existe un patrón de seguimiento de personas que han estado acompañando jornadas de movilización"* .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente Fajardo informó que a pocas horas de iniciar la jornada de paro, se han identificado varias zonas críticas de movilización e intervención de la fuerza pública, una de ellas es la localidad de Kennedy donde se registran tres arrestos a marchantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Asimismo añadió, "en el Portal Suba se desconocen los protocolos presentados por la Alcaldesa, y se hace uso de la fuerza por parte del ESMAD. No nos reportan heridos aún, pero sí ***hacemos un llamado a la administración para que se cumplan los protocolos porque se evidencia que el ESMAD está disparando a los cuerpos de las personas***".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Último reporte

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de las intervenciones realizadas a las vivienda de gestores del Paro Nacional, cerca de las 12 de medio día se reveló un nuevo caso de allanamiento a la vivienda y joven .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la versión de su hermana quien estuvo presente en medio del evento, policías llegaron hasta la puerta de su casa y solicitaron abrir la puerta de la vivienda y al ver la respuesta de los jóvenes no fue inmediata deciden ingresar a la vivienda de manera forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Nos encontrábamos en un segundo piso, sumado a la desconfianza que nos dio la presencia de estos uniformados, tardamos más de lo normar en abrir la puerta, a pesar de esto forzaron y dañaron parte de esta ... luego ingresaron al apartamento señalandonos como principales sospechosos"*, contó la joven.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicional a esto en el testimonio también manifestó que luego del allanamiento cerca de las 3 de la tarde, cuando se encontraban en el barrio El Recuerdo ( carrera 38a \# 25b- 56), fueron abordados por el conductor de un vehículo particular quien si realizar algún tipo de identificación solicitó documentos de identificación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"nos preguntó sobre nuestra ocupación, mientras que al tiempo llegaba una van con aproximadamente 10 polícias "*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asímismo la joven señaló que algunos minutos después el conductor del vehículo particular se identificó como Subteniente de la Polícia Fernández, quién según el testimonio sería el responsable de la captura del hermano de esta joven, bajo el delito de concierto para delinquir .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Cerca de las 3:37pm leyeron a mi hermano las causales de su captura , y Fernández me indicó que lo llevarían a la URI de Puente Aranda, solicité el número de Fernández para tener su contacto, cuando intenté comunicarme algunos minutos después el número estaba errado"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente la joven indicó que fue contactada por el subintendente al rededor de las 4:10 pm, para informarle que su hermano ya no iba ser detenido en la URI de Puente Aranda, sino que se trasladaría a la Avenida el Dorado con 75.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

" Luego de esta llamada Fernández me llama insistentemente solicitando información que me negué a dar"; según defensores de derechos humanos que han hecho acompañamiento a este caso, siendo las 10:00 pm de 21 de enero no se ha recibido algún tipo de contacto por parte del joven detenido o de los oficiales que realizaron su captura.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:html -->  
<iframe id="audio_46797875" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46797875_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
