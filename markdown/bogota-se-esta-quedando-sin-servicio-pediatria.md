Title: Bogotá se está quedando sin servicio de pediatría
Date: 2017-02-09 18:28
Category: DDHH, Nacional
Tags: Bogotá, Hospitales de Bogotá, Pediatria, Salud
Slug: bogota-se-esta-quedando-sin-servicio-pediatria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/marcha_reforma_a_la_salud-fa-o066.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [9 Feb. 2017] 

Continúa la grave crisis del sector salud en Colombia y en especial para la red de hospitales públicos de Bogotá, este miércoles médicos, enfermeras, pacientes y familiares realizaron un plantón como forma de rechazo ante la decisión de la Secretaría de Salud Distrital de **trasladar la unidad de pediatría del Hospital Simón Bolívar, medida que pondría en grave riesgo a muchos niños y niñas** que hacen uso de dicha unidad.

Además del traslado de la unidad de recién nacidos, las mujeres se verán también afectadas, pues **la orden también conlleva a cerrar los servicios de ginecología y obstetricia en dicho hospital. **Le puede interesar: [Sistema de salud pública agoniza en Bogotá: Defensoría del Pueblo](http://Sistema%20de%20salud%20pública%20agoniza%20en%20Bogotá:%20Defensoría%20del%20Pueblo)

Según el secretario de Salud de la capital, no se está hablando de un cierre sino de un traslado de estos servicios al Hospital de Suba. Sin embargo, para los manifestantes **esta es una decisión que afecta la salud de los pacientes y que pondrá barreras de acceso a los servicios. **Le puede interesar: [Congresistas piden a Fiscalía investigar a Secretario de salud de Bogotá](https://archivo.contagioradio.com/congresistas-piden-a-fiscalia-investigar-a-secretario-de-salud-de-bogota/)

Para **Clemencia Mayorga de la Mesa Nacional por la Salud** esta medida es absurda puesto que "el Hospital Simón Bolívar es el hospital de alto nivel de complejidad, donde los pacientes críticos tienen todos los servicios, mientras que el Hospital de Suba es de mediana complejidad y además está la barrera geográfica a quienes les va a costar mucho trabajo ser trasladados o llegar por sus propios medios".

### **Subredes lesivas para la salud** 

Ante la propuesta hecha por la Secretaría de Salud de Bogotá al Concejo Distrital, que busca reorganizar el sector salud de la ciudad, mediante la fusión de las Empresas Sociales, la creación de nuevas instituciones, el reordenamiento de organismos como el Consejo Distrital de Seguridad Social en Salud Ampliado y la Red Integrada de servicios de salud, Clemencia aseguró que es un proyecto nefasto.

**"El traslado de la unidad de pediatría del Hospital Simón Bolívar es parte de todo un plan y en donde los servicios de pediatría están siendo gravemente afectados.** Esto está sucediendo también en Kennedy (...) a nosotros nos ha llegado información de que las mamás están llegando al hospital y en la puerta les dicen que allí no hay pediatría y que tienen que ir hasta el Hospital El Tintal, lo que genera unas distancias y unos tiempos que pueden ser muy riesgosos".

Dice Clemencia que el Hospital El Tintal no puede reemplazar otros como el de Kennedy, puesto que en El Tintal no hay cirugía pediátrica lo que se traduciría por ejemplo en que si un paciente niño o niña tiene una enfermedad y requiere de la intervención de cirugía pediátrica debe llegar al Tintal, esperar una ambulancia, ser llevado a Kennedy y allí operarlo y casi que de las salas de cirugía los vuelven a llevar al Tintal, donde no lo ve de nuevo su cirujano.

**"Estamos hablando entonces que hoy los niños y las niñas tienen muchas más barreras, muchas más demoras y esto los puede poner en riesgo**" recalcó Clemencia.

### **Acciones por hacer** 

Como lo que está en juego en este caso es la vida de cientos de niños y niñas, Clemencia asegura que ya han comenzado a realizar diversas acciones para que se defienda el derecho a la salud "lo que hemos pedido en el Concejo es que se tomen las decisiones para solucionar este tema de inmediato, máxime cuando en Marzo las infecciones respiratorias en Bogotá se agudizan por lo que requerimos que todos los servicios de pediatría estén listos y abiertos".

Clemencia concluye diciendo que **"en Bogotá tenemos un problema con la improvisación que se ha dado con las redes y con el cierre de camas de pediatría**" situación que ha hecho que la Mesa Nacional de Salud esté adelantando una acción jurídica ante el Tribunal Contencioso Administrativo de Cundinamarca para frenar estas acciones.

Según cifras de la Secretaría Distrital de Salud se han cerrado entre 50 y 100 camas de pediatría en el último año.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
