Title: 'Ser Pilo Paga' otro golpe al derecho a la educación
Date: 2016-03-16 15:15
Category: Educación, Nacional
Tags: Observatorio de la Universidad Colombiana, ser pilo paga
Slug: ser-pilo-paga-otra-mermelada-del-gobierno-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/SPP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### ["Foto: MinEducación ] 

###### [16 Mar 2016 ]

De acuerdo con la investigación que durante diez meses adelantó el Senador del partido de la U Armando Benedetti, existen irregularidades en la aplicación del programa 'Ser Pilo Paga', relacionadas con la clasificación de los beneficiarios y el nivel de deserción. **Del total de 321 jóvenes beneficiados en Antioquia, 129 no pertenecen al Sisben** y de 572 desertores que registra el Icetex el Ministerio de Educación sólo reconoce 22.

'Ser Pilo Paga' [[ha sido criticado por organizaciones estudiantiles y de profesores](https://archivo.contagioradio.com/balance-de-la-educacion-superior-en-colombia-durante-2015/)] como el 'Observatorio de la Universidad Colombiana' que argumenta que con los recursos estatales destinados para los 40 mil bachilleres beneficiarios del programa durante 2015, se hubiera podido financiar la permanencia de **por lo menos 500 mil alumnos en las universidades públicas que ya alcanzan un déficit superior a los \$16 billones**.

Pese a que el Gobierno nacional argumenta que 'Ser Pilo Paga' fue creado para financiar la educación superior de los jóvenes empobrecidos, la denuncia del Senador Benedetti podría constatar que **se están beneficiando estudiantes de condiciones económicas favorables** y no aquellos que no cuentan con los recursos necesarios para acceder a estudios superiores en **beneficio de las universidades privadas con ánimo de lucro y en detrimento de las universidades estatales** cada vez más desfinanciadas.

Entretanto desde el Ministerio de Educación se asegura que no hay ningún estudiante que pertenezca al programa 'Ser Pilo Paga' que no esté inscrito en el Sisben con un puntaje menor a 57 puntos, "lo que puede suceder es que las personas están mintiendo frente a las encuestas que focalizan los subsidios", asevera la viceministra de educación superior; sin embargo, **resulta desconcertante que existan plataformas de acceso público en las que se puede ver quién pertenece o no al Sisben** y que el Icetex no haya verificado la pertenencia de los beneficiarios del programa.

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/)]]
