Title: Senado aprobó prohibición de plásticos en San Andrés
Date: 2019-06-11 17:58
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Plataforma Alto, San Andrés, Senado
Slug: senado-apruebe-prohibicion-de-plasticos-en-san-andres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Captura-de-pantalla-123.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de Noticias UN] 

A pesar de los esfuerzos de los gremios del plástico, la plenaria del Senado aprobó un proyecto de ley que busca prohibir la [comercialización y el uso de materiales plásticos en San Andrés y Providencia. La iniciativa ahora **espera conciliación entre las cámaras del Congreso y sanción presidencial**.]

Según Natalia Parra, vocera de la plataforma animalista ALTO, el proyecto se encontró con muchos obstáculos por el camino en la legislatura dado que los gremios de plástico argumentaban a los congresistas que la mejor opción para abordar el problema descontrolado de basura de plásticos en la isla era con una planta de incineración.

Sin embargo, como lo sostuvo Parra, esta medida contamina el aire. "En la Unión Europea y en Estados Unidos, **ese tipo de planta se está desmontando y se están desestimulando por el material particulado que generan** y porque no son una solución definitiva". (Le puede interesar: "[Advierten que proyecto para prohibir plástico en San Andrés corre peligro](https://archivo.contagioradio.com/advierten-que-proyecto-para-prohibir-plastico-en-san-andres-corre-peligro/)")

### **La importancia del proyecto de ley**

A pesar de que el archipiélago previamente impidió la entrada de bolsas de plástico, la vocera afirmó que las iniciativas para prohibir el uso de plástico tendrían que ser respaldadas por un marco legislativo para ser respetadas.

Como un ejemplo, citó al caso de la administración de Santa Marta, que prohibió el plástico y ahora está siendo demandada por el gremio de plástico. "Si la decisión de Santa Marta fuese amparada por la ley, la demanda de Acoplásticos no sería tan fuerte", afirmó la vocera.

### **El futuro del proyecto**

El Congreso tendrá que resolver algunas diferencias entre el texto aprobado por la Cámara de Representantes y el Senado como el plazo para la implementación de la prohibición y una proposición para impedir que los barcos que entren a la isla dejen su basura ahí.

Finalmente, esperan que este proceso sea resuelto antes del 20 de junio y luego el proyecto de ley sancionado por el Presidente Iván Duque. Además, pretenden extender esta prohibición de plásticos al Caribe y la costa Pacífica.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
