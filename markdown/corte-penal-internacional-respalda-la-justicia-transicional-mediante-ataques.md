Title: Corte Penal Internacional manifiesta su respaldo a la JEP
Date: 2019-02-20 14:22
Author: AdminContagio
Category: Judicial, Paz
Tags: Corte Penal Internacional, jurisdicción especial para la paz
Slug: corte-penal-internacional-respalda-la-justicia-transicional-mediante-ataques
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/8539515634_fd60a35001_k-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [20 Feb 2019] 

En medio de la fuerte campaña de desprestigio en contra de la Jurisdicción Especial para la Paz (JEP) que han emprendido ciertos sectores políticos, la **Corte Penal Internacional (CPI) **se pronunció en defensa de este mecanismo de  justicia transicional, argumentando que **es necesario que la institución mantenga un marco legal definido**.

Según la declaración de James Stewart, vicefiscal de la CPI, el tribunal internacional reconoció a la **JEP como el mecanismo clave de justicia transicional conforme con el acuerdo de paz.** (Le puede interesar: [Las falacias del fiscal Néstor Humberto Martínez para objetar la JEP](https://archivo.contagioradio.com/falacias-para-objetar-jep/))

Al respeto de las preocupaciones del Fiscal Nestor Humberto Martínez, Stewart manifestó que es **el deseo de la CPI "que la Fiscalía General de la Nación y la JEP puedan establecer conjuntamente una distribución eficiente de trabajo y sinergias.**" Si la JEP no pueda mantener un marco legal definido, la CPI tendría que "considerar sus implicaciones."

Las declaraciones de James Stewart:

“La Fiscal de la Corte Penal Internacional, Fatou Bensouda, se ha referido siempre a la Jurisdicción Especial para la Paz en Colombia, también conocida como “JEP”, desde la perspectiva de sus propias responsabilidades de evaluar si las autoridades nacionales se están ocupando de los crímenes del Estatuto de Roma, como los crímenes de lesa humanidad y los crímenes de guerra, de manera genuina.

Hemos aceptado que las medidas de justicia transicional que Colombia decidió adoptar podrían, si fueran implementadas y aplicadas adecuadamente, cumplir con los objetivos de rendición de cuentas, disponibilidad de recursos para las víctimas y contribución a la disuasión de crímenes futuros, previstos en el Estatuto de Roma.

Hemos, por lo tanto, apoyado a la JEP, como el mecanismo clave de justicia transicional adoptado de conformidad con el acuerdo de paz. Notamos asimismo ciertas preocupaciones planteadas recientemente por el Fiscal General de la Nación, por ejemplo respecto a la importancia de llevar a los autores directos a rendir cuentas. En este sentido, es nuestro deseo que la Fiscalía General de la Nación y la JEP puedan establecer conjuntamente una distribución eficiente de trabajo y sinergias. A tal fin, observamos la importancia de que la JEP tenga un marco legal definido. De no ser el caso, esto podría constituir un revés y tendríamos que considerar sus implicaciones."

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
