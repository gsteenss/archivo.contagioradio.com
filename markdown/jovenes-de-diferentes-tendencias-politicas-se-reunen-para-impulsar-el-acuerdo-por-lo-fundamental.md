Title: Jóvenes de diferentes tendencias políticas se reúnen para impulsar el Acuerdo por lo Fundamental
Date: 2018-05-29 19:23
Category: Nacional, Política
Tags: elecciones 2018, Gustavo Petro, Iván Duque, jovenes
Slug: jovenes-de-diferentes-tendencias-politicas-se-reunen-para-impulsar-el-acuerdo-por-lo-fundamental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/photo4931645659208067053.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [29 May 2018] 

Jóvenes de la capital, integrantes de diferentes sectores políticos o abanderados por la construcción de paz en el país, se reunieron ayer en frente al monumento del Almirante Padilla, para generar un diálogo que permita la construcción de apuestas en apoyo a la candidatura de Gustavo Petro, de cara a la segunda vuelta.

Los jóvenes aseguraron que su máxima intención no es sumar adeptos al "Petrismo", s**ino trabajar en mantener a salvo los Acuerdos de Paz**, continuar en la presión para que se implementen y asegurar un camino para la justicia y el cambio social en el páis, lo que se ha denominado como el Acuerdo por lo fundamental. En esa vía, en esa primera reunión participaron jóvenes que votaron tanto por Petro como por De la Calle y Fajardo.

De acuerdo con José Antequera, uno de los promotores de este escenario, "muchos jóvenes votaron por Petro, y mucho otros también lo hicieron por la coalición Colombia, por Fajardo y De la Calle".  En ese sentido, el escenario sirvió para ratificar que hay un llamado generacional para que no se regrese en el tiempo, **cuando Uribe era presidente, situación que podría darse si Duque llega a la presidencia, aseguró Antequera**.

Frente a  la polarización en segunda vuelta, Antequera manifestó que no se puede confundir politización con polarización, **"parte de lo que estamos intentado hacer, es irradiar una cultura política diferente, que permita dialogar mucho más"**. De igual forma, sobre a la abstención que podría darse, Antequera aseguró que hay una crisis de representación muy antigua que se podrá superar en la medida en que exista un gobierno que efectúe el retorno de la confianza de las personas hacia las instituciones políticas.

Dentro de esas actividades que los jóvenes planean llevar a cabo se han encuentros con jóvenes de otras ciudades del país que tienen las mismas intenciones, abrazatones, besatones, campañas en redes sociales y demás actividades que propicien estas discuciones. (Le puede interesar:["Votos de Fajardo y De la calle el escenario de disputa para las elecciones 2018"](https://archivo.contagioradio.com/votos-de-fajardo-y-de-la-calle-el-escenario-de-disputa-para-elecciones-0218/))

<iframe id="audio_26260291" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26260291_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
