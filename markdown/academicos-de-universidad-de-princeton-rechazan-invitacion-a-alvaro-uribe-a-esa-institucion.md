Title: Académicos de Universidad de Princeton rechazan invitación a Alvaro Uribe a esa institución
Date: 2016-02-23 17:46
Category: Nacional, Política
Tags: Alvaro Uribe, Operaciones ilegales del DAS, Paramilitarismo, Universidad de Princeton
Slug: academicos-de-universidad-de-princeton-rechazan-invitacion-a-alvaro-uribe-a-esa-institucion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/uribe-princeton-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ]

###### [23 Feb 2016] 

Según un grupo de 100 académicos, la universidad habría realizado una invitación al Senador [Álvaro Uribe](https://archivo.contagioradio.com/?s=uribe) a una cátedra en la Universidad de Princeton durante esta semana. Para los maestros la manera en que se ha publicitado la charla y la forma en que se dará no da pie a debate abierto sobre las posturas de Uribe e **impide que se evalúen aspectos como las diversas violaciones a los Derechos Humanos que caracterizaron su gobierno.**

En la misiva los académicos señalan que la invitación El anuncio inicial alabó los logros del gobierno de Uribe en relación con el crecimiento económico, la seguridad, la reducción de la pobreza y la disminución en el número de personas desplazadas internamente, pero se obviaron temas como las[interceptaciones ilegales](https://archivo.contagioradio.com/operacion-estimulo-provoco-la-muerte-de-6-integrantes-del-movice/), el escándalo de la parapolítica y los crímenes que tienen [en prisión o prófugos de la justicia varios de los funcionarios del ex alto gobierno.](https://archivo.contagioradio.com/con-luis-alfonso-hoyos-aumenta-numero-de-uribistas-profugos-de-la-justicia/)

Los firmantes de la carta aseguran que es decepcionante que el Sr. Uribe, sea  tratado como un invitado de honor en la **Universidad de Princeton, puesto que es una institución que pretende valorar los derechos humanos, la justicia y la democracia.** Además recordaron otras ocasiones en las que se han presentado objeciones a las visitas de Álvaro Uribe a universidades como Georgetown, Harvard y Columbia.

La carta culmina con la invitación a que se avalúen visitas como la de Uribe puesto que proviene de un país en guerra y con una serie de denuncias por graves violaciones a los DDHH, puesto que una charla cerrada al debate,  se tomaría como una **postura política a favor de gobiernos que han actuado contra el Estado Social de Derecho.**

Esta es la carta y las firmas de académicos

##### Dear Dean Rouse and President Eisgruber, 

##### As scholars who are from Colombia, do research on the country and/or are concerned with its political events, we write this letter to protest the invitation that Princeton University’s Woodrow Wilson School has extended to former president Alvaro Uribe Vélez this week. We object to the deceptive and biased announcement that has been used to publicize his talk. And we are concerned that the format of his public lecture will not be conducive to the open exchange of views about his very contested legacy and his current open denunciation of the Colombian peace process. 

##### The initial announcement praised the achievements of Mr. Uribe’s government concerning economic growth, security, poverty reduction and the decrease in the number of internally displaced people. Without denying some of these achievements, we wish to draw attention to other, less salubrious details of his reign. The Uribe government was accused of multiple human rights violations as a result of military campaigns against innocent civilians, systematic political persecution of members of the opposition and journalists, illegal tapping of communications by the state’s intelligence agency, collusion of hundreds of members of Uribe’s political coalition with paramilitary armed groups, and corruption among high-ranking officials, including ministers of Uribe’s cabinet charged with bribing Congressional members to vote in favor of a constitutional change to allow Uribe to stand for reelection. 

##### These events are being investigated by the Colombian judicial system, which has issued multiple criminal convictions of members of Uribe’s closest circle. They are of public knowledge—they have been widely documented by Colombian, US and other countries’ main newspapers, TV news channels and social media. Furthermore, they have been the subject of research by leading academics, as well as investigators from global and Colombian human rights defenders (including Human Rights Watch and Amnesty International). 

##### It is disappointing that Mr. Uribe will evidently be treated as a guest of honor at Princeton University, an institution that claims to cherish human rights, justice, and democracy. We regret that the Woodrow Wilson School, an institution committed to rigorous scholarship and critical thinking, has announced Uribe’s presence in a way that overlooks Mr. Uribe’s government’s abuses of human rights and the weakening of the democratic process, and is silent on the scholarship that highlights its legacies. 

##### This echoes other objections from other American universities, where academics and students have opposed previous visits of Uribe, such as Harvard, Columbia, and Georgetown. 

##### For the record, therefore, we feel it important to register some of the illegal and unethical actions for which Mr. Uribe’s government has been denounced. What follows was extracted from the letter that many Colombians and Colombianists addressed to Harvard University in 2010, when Mr. Uribe was invited to the Kennedy School right after leaving office: 

##### “As has been consistently denounced by a diverse number of human rights groups, scholars, community leaders, journalists and individuals inside Colombia and elsewhere, here is a brief list of some of the ethical and legal problems that taint Mr. Uribe’s legacy today. (Source: Human Rights Watch): 

##### - The frequent extrajudicial killings of civilians attributed to the Colombian Army, which the United Nations special rapporteur on extrajudicial executions recently described as “systematic.” The Attorney General’s Office is reported to be investigating cases involving more than 1,700 alleged victims in recent years. Uribe refuses to acknowledge the magnitude of the problem. 

##### - Recent scandals over widespread illegal surveillance and wiretapping by the national intelligence service, which answers directly to Uribe. The surveillance focused almost entirely on major opposition political figures, Supreme Court justices investigating the infiltration of paramilitary mafias in the Colombian Congress, as well as on journalists, trade unions, and human rights organizations. (Please note that one of the people signing this very letter was a direct victim of these criminal acts of surveillance, along with his family). 

##### - The repeated verbal attacks and intimidation of critics by Uribe and senior administration officials, who often try to link legitimate human rights work, journalism, or union activity with the brutal left-wing guerrillas of the Revolutionary Armed Forces of Colombia (Farc). 

##### - Ongoing anti-union violence, with the offenders rarely brought to justice. Colombia has the highest rate of killings of trade union members and leaders in the world. More than 2,700 are reported to have been killed since 1986, according to data collected by the National Labor School (Escuela Nacional Sindical or ENS). The ENS recorded 49 such killings in 2008, up from 39 in 2007. It recorded 20 of these killings in 2009 as of mid-June. Ninety-six percent of the killings remain unsolved. 

##### Moreover, after his presidential period ended earlier this year, the intensity of the accusations against Uribe’s administration increased in unprecedented ways, marked by the ongoing criminal investigations led by the country’s Supreme Court against some of his closest aides. Politicians closely allied with the violent project of paramilitary warlords were consistently named by Mr. Uribe to public office during his two terms in office. Even worse, today more than a hundred members of Congress are involved in criminal investigations for their links with paramilitarism, all of them were members of President Uribe’s coalition. The corrupt mechanisms he resorted to obtain his re-election as President in 2006 have pushed several among his ministers and close collaborators to face imprisonment.”. 

##### This letter urges you to be mindful about the invitation of guests from countries with war-torn conflicts and open political wounds. Their speaking presence is not a neutral act, though Princeton should be a place for debate across partisan lines and should be committed to critical thinking and debate. Is that what former President Uribe has been invited to participate in? If not, we are concerned that Princeton be a tool for politicians and governments seeking to legitimate violations of human rights and weakening democracy and the rule of law. 

##### Sincerely, 

##### Maria Paula Saffon Sanín, Ph.D., Race and Ethnicity Fellow, Society of Fellows in the Liberal Arts, Lecturer, Politics Department, Princeton University 

##### Juan F. González Bertomeu, J.S.D.; Professor of Law, ITAM University, Guest, Politics Department, Princeton University 

##### Jeremy Adelman, Henry Charles Lea Professor of History, Director of the Global History Lab, Princeton University 

##### Mark Beissinger, Henry W. Putnam Professor of Politics, Director of the Institute for International and Regional Studies (PIIRS), Princeton University 

##### Miguel A. Centeno, Professor of Sociology and International Affairs, Princeton University 

##### Diana Andrade-Melgarejo, Ph.D Candidate, History Department, Princeton University 

##### Robert Karl, Assistant Professor, Dept. of History, Princeton University 

##### Susan Stewart, Avalon Foundation University Professor of the Humanities and Director, Society of Fellows in the Liberal Arts, Princeton University 

##### Gabriela Nouzeilles, Professor, Department of Spanish and Portuguese, Princeton University 

##### Christophe Litwin, PhD, Princeton Society of Fellows in the Liberal Arts, Princeton University 

##### David Minto, Fund for Reunion-Cotsen Postdoctoral Fellow, Society of Fellows in the Liberal Arts, Princeton University 

##### Tony Grafton, Henry Putnam University Professor of History, Princeton University 

##### Thomas Hare, William Sauter LaPorte ’28 Professor in Regional Studies, Princeton University 

##### Teresa Davis, PhD Candidate, History Department, Princeton University 

##### Mary Harper, Executive Director, Society of Fellows in the Liberal Arts, Princeton University 

##### Alessandro Giammei, Cotsen Postdoctoral Fellow in the Society of Fellows and Lecturer in the Council of the Humanities and French and Italian, Princeton University 

##### Tineke D’Haeseleer, Mellon-Cotsen East Asian Postdoctoral Fellow, Society of Fellows in the Liberal Arts, Princeton University 

##### Deborah Yashar, Professor of Politics and International Affairs, Princeton University 

##### Iwa Nawrocki, PhD Candidate, History Department, Princeton University 

##### Jessica R. Mack, PhD Candidate, Department of History, Princeton University 

##### Andrea Oñate-Madrazo, Ph.D Candidate, History Department, Princeton University 

##### Matthias vom Hau, Fung Global Fellow, Institute for International & Regional Studies, Princeton University 

##### Jon Elster, Robert K. Merton Professor of the Social Sciences, Columbia University 

##### Mary Roldán, Ph.D., Professor and Chair, Dorothy Epstein Professor of Latin American History, Department of History, Hunter College, City University of New York/ CUNY Graduate Center 

##### Andrés Fabián Henao Castro, Assistant Professor, Department of Political Science, University of Massachusetts Boston 

##### Esteban Restrepo Saldarriaga, Associate Professor, Law School, Universidad de los Andes 

##### Miguel Gualdrón, PhD Student, Philosophy, DePaul University, Chicago 

##### Renata Segura, Associate Director, Conflict Prevention and Peace Forum, Social Science Research Council 

##### Oscar Javier Parra Vera, MSc Candidate in Criminology and Criminal Justice, University of Oxford 

##### Lina Britto, Assistant Professor, Department of History, Northwestern University 

##### Sofia Jaramillo, Carnegie Mellon University, Master Student of Public Policy and Management 

##### Victor M. Uribe-Uran, Associate Professor of History and Law, Chair, Department of History, Florida International University 

##### María del Rosario Acosta López, Associate Professor, Philosophy Department, DePaul University 

##### Sara Koopman, Assistant Professor, Department of Geography, York University 

##### Catherine C. LeGrand, Associate Professor, Department of History, McGill University 

##### Paulina Pardo, Ph.D. Student, Department of History of Art and Architecture, University of Pittsburgh 

##### Miguel García-Sánchez, Ph.D. Associate Professor, Department of Political Science, Universidad de los Andes 

##### Carolina Maldonado-Carreño, Ph.D. Associate Professor, Department of Psychology, Universidad de los Andes 

##### Andres Molano, Assistant Professor, School of Education, Universidad de los Andes 

##### Juan Carlos Rodríguez Raga, Ph.D., Associate Professor, Department of Political Science, Universidad de los Andes 

##### Rodolfo Arango, Professor, Departament of Philosophy, Universidad de los Andes 

##### Margarita Fajardo, Assistant Professor, Sarah Lawrence College 

##### Santiago Virgüez Ruiz, Research Assistant, Department of Political Science, Universidad de los Andes 

##### Tatiana Alfonso Sierra, Ph.D. Student, Department of Sociology, University of Wisconsin-Madison, Visiting Professor, Universidad de Los Andes 

##### Alejandra Azuero Quijano, SJD candidate, Harvard Law School, and Ph.D. Student, Department of Anthropology, University of Chicago 

##### Silvia Otero Bahamon, Ph.d. Candidate, Political Science, Northwestern University 

##### Natalia Ramirez-Bustamante, SJD candidate, Harvard Law School 

##### Jacob N. Kopas, J.D. Harvard Law School, PhD Student Political Science, Columbia University 

##### Ana María Ibáñez, Dean, Economic Department, Universidad de los Andes 

##### Camilo Castillo, Candidato a Doctor en Derecho, Universidad del Rosario 

##### Nathalia Hernández Vidal. Ph.D. student, Department of Sociology, Loyola University 

##### Javier Revelo-Rebolledo, Ph.D. Candidate, Department of Political Science, University of Pennsylvania 

##### Jose Rafael Espinosa, Student, The Harris School of Public Policy, Master of Public Policy, University of Chicago 

##### Pilar Riaño-Alcalá, PhD, Professor, University of British Columbia, Vancouver 

##### Winifred Tate, Assistant Professor of Anthropology, Colby College 

##### Ricardo López, Associate Professor of History, Western Washington University 

##### Ana Maria Bidegain, Professor, Florida International University 

##### Libardo José Ariza, Associate Professor, Law School, Universidad de los Andes 

##### Camila de Gamboa Tapias, Ph.D. in philosophy, Binghamton University, Associate Professor, Law School, Universidad del Rosario 

##### Ana Arjona, Assistant Professor, Department of Political Science, Northwestern University 

##### Teo Ballvé, Assistant Professor, Peace & Conflict Studies Program, Department of Geography, Colgate University 

##### Juana Dávila, Ph.D. Candidate, Department of Anthropology, Harvard University 

##### Jose Luis Venegas Ramírez, Student, Law and Philosophy Departments, Universidad de los Andes 

##### Alex Fattal, Assistant Professor, Department of Film-Video and Media Studies, Pennsylvania State University 

##### Jorge González Jacome, S.J.D. 2013, Graduate Fellow, Writing Workshop Teaching Assistant, Harvard University 

##### Julieta Lemaitre, Associate Professor, Law School, Universidad de los Andes 

##### Marco Palacios, DPhil. Oxon, Profesor-investigador, El Colegio de México 

##### Roosbelinda Cárdenas, Assistant Professor, Hampshire College 

##### Antonio Barreto Rozo, Associate Professor, Law School, Universidad de los Andes 

##### Lina Beatriz Pinto García, PhD Student, Department of Science & Techology Studies, York University 

##### Kristina Lyons, Assistant Professor, Feminist Science Studies, Department of Anthropology, Latin American and Latino Studies, and the Science & Justice Research Center, University of California, Santa Cruz 

##### Marisol de la Cadena, Professor of Anthropology, UC Davis 

##### Jose Antonio Ramírez-Orozco, Ph.D. Student, Urban Planning, Columbia University 

##### Alexander Huezo, Ph.D. Candidate, Geography, Florida International University 

##### Albert Berry, Professor Emeritus of Economics, University of Toronto 

##### Jean E. Jackson, Professor of Anthropology Emeritus, MIT 

##### Elisabeth Jean Wood, Professor of Political Science & International and Area Studies, Yale University 
