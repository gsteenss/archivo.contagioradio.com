Title: En riesgo acuerdos municipales contra minería en Huila
Date: 2019-02-12 18:41
Author: AdminContagio
Category: Ambiente, Judicial
Tags: Huila, Tribunal Administrativo del Huila
Slug: riesgo-acuerdos-municipales-mineria-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/mineria-e1457467593828-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [12 Feb 2019] 

El pasado 6 de febrero, el juez Séptimo Administrativo del Huila decidió anular tres acuerdos municipales que prohibían la minería de alto impacto y la construcción de hidroeléctricas en Timaná, Oporapa y San Agustín, **resultando en un total de cinco acuerdos** [**anti-mineros**]** declarados inválidos en ese departamento**.

El juez Tito Alejandro Rubiano Herrera, falló en primera instancia en favor de la demanda por parte del Ministerio de Minas y la Agencia Nacional de Minería, y determinó que **los concejales habían extralimitado su competencia, porque las decisiones sobre esta actividad lícita, de utilidad pública y de interés social, le correspondía a los legisladores, no a los entes territoriales**. Actualmente, la defensa prepara su apelación ante este fallo para el 19 de febrero.

Asimismo las comunidades, sus abogados y los concejos municipales cuestionaron el fallo del juez Séptimo Administrativo del Huila dado que en 2017 el mismo organismo defendió el acuerdo de San Agustín ante una interposición del gobernador del Huila, Carlos Julio González, sobre la legitimidad de este acto administrativo.

El 27 de septiembre de 2017, el Tribunal Contencioso Administrativo del Huila, Sala Quinta de Decisión, declaró infundada la observación presentada por el Gobernador del Huila y, en consecuencia, “**declaró válido el Acuerdo No. 016 del 10 de junio de 2017** por medio del cual se dictan unas medidas para la protección de los recursos naturales y defensa del Patrimonio Ecológico del Municipio de San Agustín Huila”. En ese momento, el Tribunal avaló su decisión con la sentencia de la Corte Constitucional T-445 de 2016.

Sin embargo, las decisiones posteriores han fallado en contra de este precedente, dejando sin piso jurídico los acuerdo municipales, primero de Pitalito en septiembre de 2017 y luego el de Altamira en noviembre. A consecuencia, las comunidades de otros siete municipios, **Acevedo, Elías, El Agrado, Íquira, La Plata, Argentina e Isnos,** actualmente manifiestan su preocupación, también podría anular los acuerdos municipales emitidos en estos territorios.

### **El medio ambiente en riesgo** 

En el Plan Maestro de Aprovechamiento del Río Magdalena,  la compañía estatal china Hydrochina planteó **la construcción de siete represas desde el norte al sur del departamento**, que se suman a otros dos, Betanía y El Quimbo, actualmente en funcionamiento. Oscar Reyes, abogado y integrante de la Asociación de Afectados por el Proyecto Hidroeléctrico El Quimbo (Asoquimbo), aseguró que las comunidades, junto con las organizaciones ambientales, temen la catástrofe que podría resultar de un sismo en la zona de represa.

En el caso de El Quimbo, las comunidades han alertado sobre la falta de estudios y [pruebas pilotos que demuestren cuales podrían ser las consecuencias en la ocasión de un sismo de alto impacto. Las comunidades aseguran que de presentarse el fenómeno telúrico con el resquebrajamiento de la represa sería "]una catástrofe que quizás pueda ser mayor a la que ocurrió en Ituango" sostuvo Reyes.

Reyes agregó que el Ministerio de Minas tiene planificado otorgar títulos mineros para la explotación de petróleo y gas en un bloque territorial, entre el Huila y el Putumayo; además, la explotación de gas natural a través del fracking continúa como una posibilidad en el departamento.

### **El debate legal sobre los acuerdos municipales** 

Para Reyes, estas últimas decisiones en contra de los acuerdos municipales se pueden explicar en parte por la sentencia de la Corte Constitucional T-445, emitida en octubre del año pasado. Esta sentencia decidió sobre el futuro de las consultas populares, sin embargo dejó claro que el Estado tiene competencia exclusiva del subsuelo y por lo tanto tiene "efecto vinculante para casos similares."

Dado que la Corte Constitucional es la máxima autoridad sobre disputas constitucionales, "se están plegando todos los jueces a esta sentencia para anular estos acuerdos," explicó Reyes. Sin embargo, existe otra sentencia de la sala plena del Consejo de Estado que defiende los acuerdos municipales con el principio de "[**rigor subsidiario**," avalado por la Ley 99 de 1993 y el Artículo 313 de la Constitución Política, que sostiene que los entes territoriales "conocen la realidad de su territorio y por lo tanto, bajo esa competencia constitucional, son ellos que deben proteger el medio ambiente."]

Reyes sostiene que los procesos que se han presentado frente los tribunales administrativos, son de carácter ordinario y por tal razón, los jueces de esas entidades deben respetar el precedente vertical que crea el Consejo de Estado.

### **La lucha para la defensa de los territorios sigue** 

Frente a la anulación de los acuerdos de Timaná, Oporapa y San Agustín, la defensa prepara actualmente una apelación en que el Tribunal Administrativo del Huila revisará la decisión del juez Séptimo. Si la decisión sigue siendo desfavorable para la defensa, ellos consideran la posibilidad de radicar una tutela por el desconocimiento de[ "precedentes y la norma constitucional sustantiva".]

Adicionalmente, las comunidades planean presionar al Congreso para que[ reglamenten la relación entre el Gobierno Nacional y los órganos territoriales en las decisiones sobre la energía, la minería y el ambiente, como lo ordenó la Corte Constitucional el año pasado.]

Reyes manifiesta que se tienen "[que agotar todos los recursos jurídicos posibles en el orden interno" Sin embargo, no descarta la opción de iniciar acciones internacionales contra el Estado colombiano en la Corte IDH y en cambio, asegura que ya se ha "adelantado en la Comisión de Derechos Humanos varias discusiones en el tema de participación ciudadana y protección del medio ambiente". Según Reyes, en dos o tres años es "muy probable" que se comience un proceso legal ante este organismo internacional. ]

<iframe id="audio_32483138" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32483138_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
