Title: Modificaciones a Jurisdicción Especial de Paz abrirían la puerta a Corte Penal Internacional
Date: 2017-01-26 17:51
Category: Entrevistas, Paz
Tags: Cadena de Mando, FARC-EP, Jurisdicción Espacial de Paz
Slug: modificaciones-a-jurisdiccion-especial-de-paz-abririan-la-puerta-a-corte-penal-internacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/pazcalle.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paz a la Calle] 

###### [26 Ene 2017] 

El abogado Enrique Santiago se refirió a los debates sobre la implementación de los acuerdos de paz y las alarmas por parte de organizaciones sociales que han expresado la falta de participación de las víctimas del conflicto armado y modificaciones de los puntos pactados en La Habana, **que podrían alterar la esencia de los acuerdos y dar paso a la impunidad.**

### **Víctimas y Jurisdicción Especial para la Paz** 

De acuerdo con el Abogado Enrique Santiago, tanto la Juridiscción Especial para la Paz, como el texto del acto legislativo contemplan la participación de las víctimas. Además, hay un principio general  que da la posibilidad a **las víctimas de recurrir a cualquier resolución de la Jurisdicción Especial** para la paz cuando está implique una vulneración de los derechos fundamentales de las mismas y de esta manera interpelarla.

Aclara que en estos momentos no se está tramitando en Cámara la ley que regula de forma completa la JEP, por ahora, lo que se está debatiendo son las introducciones de principios básicos que deben ser constitucionalizados y en referencia a las sanciones restaurativas, Enrique Santiago afirmó que **los jueces deben escuchar a las víctimas sobre la sanción propuestas a los victimarios, si estas así lo desean. Le puede interesar: "**

Sin embargo, hizo un llamado a reflexionar sobre el sistema procesal que contempla que todas sus resoluciones puedan ser recurridas por cualquier motivo, hecho que podría llevar a que la interposición de recursos se utilice por aquellos que “no quieran que funcione el sistema, quieran colapsarlo, **continuar con la impunidad,  paralizar la Jurisdicción Especial para la Paz  y que no arroje como resultado ninguna sentencia**”.

### **Agentes del Estado y la Cadena de mando** 

En el Acuerdo especial para la paz quedo claramente establecido un sistema de responsabilidad de cadena de mando, con un **matiz diferenciado entre fuerza pública y guerrilleros, civiles u otras personas que sean sometidos a la Jurisdicción Especial para la Paz. **Le puede interesar:["Organizaciones sociales exigen justicia para todos en implementación de JEP"](https://archivo.contagioradio.com/organizaciones-sociales-exigen-justicia-para-todos-en-implementacion-de-jep/)

Para Enrique Santiago, en el caso de la configuración de la cadena de mando de la guerrilla de las FARC-EP, hay una mención express con la aplicación del derecho internacional, no obstante hay pronunciamientos recientes de expertos internacional  y de la Corte Penal Internacional mostrando sus dudas frente a las **normas que se están aplicando a la cadena de mando de la Fuerza Pública.**

“Respecto a mi punto de vista esa **cadena de mando presenta serias deficiencias,** debe dejar claro que la responsabilidad por cadena de mando debe aplicarse en función del control efectivo y no solo en términos formales de responsabilidad. En segundo lugar la **cadena de mando debe tener en cuenta tanto el conocimiento positivo del hecho que tenga el superior como la negligencia consiente, es decir la omisión**”.

### **¿Qué viene para la Jurisdicción Especial para la Paz?** 

Con este panorama y las probabilidades de que la Jurisdicción Especial para la Paz quede coja, el abogado Enrique Santiago aconseja recapacitar ya que “**si se acoge un régimen laxo de responsabilidades se acabe rindiendo cuentas en la Corte Penal Internacional**”  y allí no entraría en funcionamiento las sanciones restaurativas de la Jurisdicción Especial para la Paz. Le puede interesar: ["No hay voluntad política para desmontar el paramilitarismo"](https://archivo.contagioradio.com/no-hay-voluntad-politica-para-combatir-el-paramilitarismo/)

<iframe id="audio_16673857" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16673857_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
