Title: Empresa Cerrejón desviará una de las principales fuentes de agua de La Guajira
Date: 2016-04-04 15:22
Category: DDHH, Nacional
Tags: carbón, El Cerrejón, Guajira, Mineria
Slug: empresa-cerrejon-desviara-una-de-las-principales-fuentes-de-agua-de-la-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Diario del Norte 

###### [4 Abr 2016]

**Corpoguajira y la Agencia Nacional de Licencias Ambientales, ANLA, dieron vía libre para que la empresa El Cerrejón** pueda desviar el arroyo Bruno, que nace en la reserva natural de los Montes de Oca y provee de agua a las familias indígenas Wayúu que habitan a orillas del río, y quienes a su vez, rechazan este tipo de intervenciones pues aseguran que de esta forma se continúa acabando con las comunidades que actualmente atraviesan una de las más graves crisis humanitarias por la falta de agua y alimentos.

El arroyo Bruno corre 26 kilómetros, luego se encuentra con el río Ranchería, (desviado por esa empresa). **En ese contexto, El Cerrejón planea cambiar el recorrido del río por 3.6 kilómetros y moverlo 700 metros al norte** cercano a la carretera principal. De no ser así, según el Cerrejón, se dejaría de producir 3 millones de toneladas de carbón anuales, y la región dejaría de recibir \$3,7 billones de regalías.

Sin embargo, pese a que La Guajira es uno de los departamentos que genera mayor cantidades de regalías, las comunidades indígenas no han sido los principales beneficiarios de la actividad minera de El Cerrejón, en cambio han sido los más afectados pues la minería ha absorbido toda el agua que la población necesita para vivir, **este año ya se cuentan 25 niños y niñas indígenas muertos por la falta de agua, que a su vez repercute en la falta de alimento.**

Angélica Ortíz, vocera de Fuerza Mujeres Wayú,  cuenta que el arroyo Bruno es una de las principales fuentes de agua del departamento, pero podría desaparecer como sucedió con el arroyo  de Aguas Blancas, que **la misma empresa desvió en 1998 y que había prometido que se recuperaría, pero han pasado 18 años y el arroyo está totalmente seco**, cuenta Ortíz.

Las comunidades indígenas denuncian que las consecuencias del desvío podrían ser nefastas. Primero porque el arroyo podría secarse totalmente, profundizando aún más la falta de agua en La Guajira, a su vez  se generaría afectaciones frente a la espiritualidad de las comunidades indígenas que mantienen un vínculo muy importante con el arroyo que para ellos es sagrado y además **comunidades como Alvania, algunas de Rioacha y Maicao, entre otras se verían gravemente afectadas por la falta de agua.**

Según la vocera de Fuerza Mujeres Wayúu, **en el año 2014 las comunidades denunciaron la desaparición de 17 fuentes por las actividades mineras de la empresa** El Cerrejón, que únicamente realizó el proceso de consulta previa con una comunidades.

“Es como si cortarán la vena aorta, este hecho repercute en otros nacimientos de agua. El mayor responsable es el gobierno que sigue dando permisos para que lleguen a desbaratarnos la casa... O tenemos agua para la vida o tenemos agua para la minería”, concluye Angélica Ortíz.

<iframe src="http://co.ivoox.com/es/player_ej_11042477_2_1.html?data=kpadlpeYe5ihhpywj5aYaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncaLiyIqwlYqlfc3dxMaYsdfYrdufjpDD0cjJtsKfxcqYqNrJttvVjLLizMrWqdSfuMbmh6iXaaO1joqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Angélica Ortíz, Fuerza Mujeres Wayúu] 

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
