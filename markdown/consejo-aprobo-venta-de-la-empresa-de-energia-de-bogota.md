Title: Concejo aprobó venta de la Empresa de Energía de Bogotá
Date: 2016-11-09 17:14
Category: Economía, Nacional
Tags: Alcaldía Enrique Peñalosa, Concejo de Bogotá, Venta de la EEB
Slug: consejo-aprobo-venta-de-la-empresa-de-energia-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/eeb.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Portafolio] 

###### [9 Nov de 2016] 

Después de 11 horas de debate, el pasado 8 de noviembre el Concejo de Bogotá aprobó la venta del 20% de la participación que tiene el distrito en la Empresa de Energía de Bogotá. Con 31 votos a favor y 12 en contra, **el cabildo autorizó al alcalde Enrique Peñalosa para que adelante ese negocio, por el que espera recibir \$3,5 billones.**

Roberto Hinestrosa, presidente del Concejo dijo que la decisión tomada “es el respaldo de la corporación a la administración del alcalde Enrique Peñalosa, **para que adelante las obras viales y de infraestructura que la ciudad necesita”. **Le puede interesar: [Cuando EEB va a dar mayores utilidades van a vender el 20%](https://archivo.contagioradio.com/cuando-eeb-va-a-dar-mayores-utilidades-van-a-vender-el-20/).

Frente a ello, concejales de la coalición de gobierno, particularmente del Centro Democrático, insistieron en la necesidad de **“definir cuáles serán los proyectos que realmente se financiarán con esos dineros”** y propusieron que sean sólo tres la Avenida longitudinal de oriente y las troncales de las avenidas Ciudad de Cali y Boyacá.

Quienes se opusieron desde el anuncio de la venta, denunciaron **obras como la longitudinal ponen en riesgo la reserva Thomas Van der Hammen**, pues la alvcaldía aún no ha presentado estudios serios sobre el impacto ambiental que eso generaría. Le puede interesar: [¿Qué le espera a la reserva forestal Thomas van Der Hammen?](https://archivo.contagioradio.com/reserva-thomas-van-der-hammen-y-su-futuro/).

Incluso, durante la mañana de hoy fue tendencia el Concejo de Bogotá porque **4 concejales de la Alianza Verde votaron a favor de la venta lo que provocó la reacción de los congresitas de esa bancada** puesto que el Partido se ha declarado en oposición a la intervención de la reserva Thomas Van der Hammen, y es claro que el presupuesto por la venta de la EEB tendrá destinación directa a dicha intervención.

### **Quienes dieron el sí a la venta:** 

Julio Cesar Acosta Acosta (Cambio Radical)

Edward Arias Rubio (Alianza Verde)

David Ballén Hernández (Partido de la U)

Dora Lucía Bastidas Ubaté (Alianza Verde)

Roger Carrillo Campo (Conservador)

Jose David Castellanos (Cambio Radical)

Ricardo Correa (Partido de la U)

Nelson Cubides (Conservador)

Diego Devia (Centro Democrático)

Gloria Elsy Díaz (Conservador)

Jorge Durán Silva (Liberal)

Andrés Forero (Centro Democrático)

Germán García (Liberal)

Cesar Alfonso García (Cambio Radical)

Rolando González García (Cambio Radical)

Luz Marina Gordillo (Liberal)

Juan Felipe Grillo (Cambio Radical)

Armando Gutiérrez González (Liberal)

Roberto Hinestrosa (Cambio Radical)

Pedro Julián López Sierra (Cambio Radical)

Hosman Martinez (Alianza Verde)

Diego Molano Aponte (Centro Democrático)

Nelly Patricia Mosquera (Partido de la U)

Maria Clara Name (Alianza Verde)

Daniel Palacios (Centro Democrático)

Emel Rojas (Movimiento Libres)

Javier Santiesteban (Centro Democrático)

Horacio José Serpa (Liberal)

Rubén Darío Torrado (Partido de la U)

Jorge Eduardo Torres (Alianza Verde)

Yéfer Yesid Vega (Cambio Radical)

Además, en el debate no sólo se aprobó la venta de la empresa de energía, también fue aprobado un cupo de endeudamiento por 5.1 billones, que según Peñalosa, serán destinados a obras como la troncal de transmilenio por la **carrera 7ma, la construcción de 25 colegios, la restauración de hospitales y el inicio de una fase del metro.**

Por otra parte, la ley establece que si se “genera un hueco en las arcas de la administración producto de una decisión de estas características, **se debe garantizar la fuente de recursos que llenará ese vacío”** y hasta el momento no ha habido aclaraciones ni pronunciamientos sobre el tema por parte de Peñalosa.

### **Estas son las vías que pretenden construir con los 3.5 billones de la venta de la EEB:** 

\* Avenida Longitudinal de Occidente - ALÓ (De Chusacá a límites de Bogotá): \$4 billones 710.186 millones

\* Avenida Longitudinal de Occidente – ALÓ (De los límites con Soacha a límites con Chía): \$1 billón 761.156 millones

\* Calle 13 (Límites de Bogotá – NQS por la troncal Américas): \$2 billones 10.777 millones

\* Avenida ferrocarril del sur (Av. Villavicencio – Av. de las Américas): 1 billón 6.632 millones

\* Troncal Avenida Ferrocarril del Norte (Av. Congreso Eucarístico – Límite de Bogotá con Chía): \$1 billón 759.215 millones

\* Troncal Av. Boyacá (Yomasa – Av. Guaymaral): \$4 billones 218.242 millones

\* Troncal Av. Ciudad de Cali (Av. Bosa – Calle 170): \$2 billones 515.252 millones

\* Av. José Celestino Mutis (Límites de Bogotá con Funza – Carrera 7): \$3 billones 92.929 millones

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
