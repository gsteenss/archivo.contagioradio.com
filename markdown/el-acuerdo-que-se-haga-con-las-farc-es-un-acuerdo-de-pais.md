Title: "El acuerdo que se haga con las FARC es un acuerdo de país"
Date: 2016-08-01 18:16
Category: Nacional, Paz
Tags: Diálogos de paz Colombia, proceso de negociación FARC, proceso de paz eln
Slug: el-acuerdo-que-se-haga-con-las-farc-es-un-acuerdo-de-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/paz-e1468867771732.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [1 Ago 2016] 

Este domingo a través de su cuenta en Twitter, el ELN aseguró que absolutamente toda la comandancia y la militancia considera que **con el 'plebiscito exprés' no queda bien hecha la paz**; así mismo, que el afán de Juan Manuel Santos es ganarle el pulso al senador Uribe usando el proceso de paz como medio para lograrlo y que quienes voten por el si serán asimilados como aliados del Presidente, mientras que quienes voten por el no serán vistos como seguidores de Uribe.

De acuerdo con el analista Luis Eduardo Celis estas afirmaciones deben ser debatidas en el escenario político, pues **el esfuerzo de lograr una paz negociada entre el Gobierno y las FARC-EP es un propósito de nación con un amplio respaldo ciudadano**, por lo que el ELN "se equivoca al asegurar que todo este esfuerzo es un tema que sólo compete a Álvaro Uribe, a Juan Manuel Santos y a las FARC", por el contrario es un proceso con un respaldo de diversos sectores políticos y organizaciones sociales.

"Afirmar que el plebiscito es una cosa que se hace a las carreras, que se hace en medio de la polarización Santos-Uribe es desconocer la realidad política del respaldo ciudadano al proceso de paz", más de cuatro años y medio de negociaciones demuestran que no es un proceso express, asegura el analista y agrega que puede que el ELN considere que ésta no es la paz que quieren, pero deben resolver el debate que tienen con el Gobierno e **iniciar el proceso de negociación para que digan qué les gusta, qué no y qué proponen** en aspectos como la refrendación.

<iframe src="http://co.ivoox.com/es/player_ej_12414952_2_1.html?data=kpehk5mdeZOhhpywj5aUaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYp8nTb6TZzc7gh5enb6LiwtHW1dnFb9HjzYqwlYqliNXdxNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
