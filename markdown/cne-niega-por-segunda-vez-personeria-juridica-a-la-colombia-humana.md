Title: Colombia Humana presentará candidatos por firmas tras decisión del CNE
Date: 2018-12-21 18:20
Author: AdminContagio
Category: Nacional, Política
Tags: Colombia Humana, Consejo Nacional Electoral, Jorge Rojas
Slug: cne-niega-por-segunda-vez-personeria-juridica-a-la-colombia-humana
Status: published

###### [Foto: Twitter Gustavo Petro] 

###### [20 Dic 2018] 

Militantes de la Colombia Humana podrán quedar sin la posibilidad de elegir candidatos de su movimiento político en las siguientes elecciones regionales de 2019, tras la decisión del Consejo Nacional Electoral (CNE) de **negar a la colectividad la personería jurídica este jueves.**

La sala plena del organismo electoral manifestó en un comunicado que la organización política del excandidato presidencial y senador Gustavo Petro no cumplió con los requisitos establecidos por el artículo 108 de la Constitución Política y el artículo tres de la Ley Estatutaria 1475 de 2011 porque no postuló candidatos ni obtuvo votos en las elecciones legislativas.

Debido a la decisión del CNE, Colombia Humana no podrá presentar candidatos para las elecciones regionales del siguiente año. **Tampoco podrá recibir financiación pública para sus campañas ni acceder a espacios de medios de comunicación.**

Jorge Rojas, precandidato a la Alcaldía de Bogotá del movimiento Colombia Humana, tildó el fallo como "profundamente injusto" debido a que restringe el derecho democrático y la expresión política de los mas de 8.000.000 de ciudadanos que votaron por Petro en las elecciones presidenciales. [Le puede interesar Cuatro acciones para atacar a](https://archivo.contagioradio.com/atacar-colombia-humana/) Colombia Humana

Rojas manifestó que la decisión del CNE hace parte de **"una operación para sacar a la Colombia Humana del escenario electoral"**. Además de negar los derechos políticos dos veces por el CNE, el movimiento político también ha sufrido de "miles de amenazas, asesinatos, separación de sus familias, robos de información, intentos de apresar a sus dirigentes, centenares de millones de dólares de multas," escribió Petro en su cuenta de Twitter.

### **Colombia Humana presentará candidatos por firmas** 

Sin embargo, representantes de la Colombia Humana han escogido la opción de recolectar firmas para participar en los comicios regionales del siguiente año. En anticipación del fallo de la CNE, la Colombia Humana inició una campaña de recolección de firmas para lanzar un candidato a la Alcaldía de Bogotá. Actualmente, cuentan con 12.000 signaturas reunidas y esperan obtener 138.000 más antes del 15 de marzo.

<iframe id="audio_30964974" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30964974_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
