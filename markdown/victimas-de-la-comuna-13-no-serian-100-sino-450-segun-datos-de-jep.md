Title: Víctimas de la Comuna 13 no serían 100 sino 450 según datos de JEP
Date: 2020-04-04 00:12
Author: AdminContagio
Category: Actualidad, Paz
Tags: Comuna 13, UBPD
Slug: victimas-de-la-comuna-13-no-serian-100-sino-450-segun-datos-de-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Movilización-Comuna-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Se han hecho populares los descubrimientos realizados por la Jurisdicción Especial para la Paz (JEP) en el cementerio de[Dabeiba](https://archivo.contagioradio.com/comunidad-dabeiba-jep-verdad/), Antioquia, en el que han encontrado víctimas de ejecución extrajudicial. La exhumación de los cuerpos en el lugar fue posible gracias a la solicitud de medidas cautelares que realizó el Movimiento Nacional de Víctimas de Crímenes de Estado [(MOVICE)](https://movimientodevictimas.org/avanza-la-solicitud-de-medidas-cautelares-la-jep-realizara-audiencia-sobre-hidroituango/) sobre 16 lugares en los que se presume que hay cuerpos de víctimas de desaparición forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dicha solicitud fue presentada en 2018 ante la JEP, que asumió competencia en septiembre de ese año para cobijar los [16 lugares](https://archivo.contagioradio.com/piden-proteger-16-lugares-jep/) que estaban en departamentos como Antioquia, Sucre, Santander y Caldas. Contagio Radio habló con Gustavo Salazar, magistrado de la Jurisdicción que trabaja sobre la solicitud en Antioquia, revisando los casos de Dabeiba, Ituango y la Comuna 13.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Contagio Radio: A principios de marzo la JEP le ordenó a Empresas Públicas de Medellín (EPM) entregar toda la información sobre Hidroituango para analizar ese caso. ¿Podría explicarnos cómo avanzan en la zona?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gustavo Salazar: Tenemos tres aspectos a determinar: cuántos desaparecidos hubo en la zona, allá nos hemos dado cuenta que al igual que en la Comuna 13 no hay unas cifras consolidadas oficiales, hay unos datos más o menos robustos pero nos son consolidados por el Centro Nacional de Memoria Histórica, y otros por parte de la Fiscalía. Entonces, necesitamos determinar el universo de víctimas de desaparición Forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El segundo paso es tener el inventario consolidado de los puntos donde la Fiscalía ha realizado exhumaciones, es decir, que prospecta cuerpos y los saca de la tierra. Y en tercer lugar, dónde se ha realizado prospección y búsqueda. (Le puede interesar: ["Así funcionará la Unidad de Búsqueda de personas Desaparecidas"](https://archivo.contagioradio.com/asi-funcionara-la-unidad-busqueda-personas-desaparecidas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Eso en relación con los lugares irregulares de inhumación. Las comunidades, sobre todo las del Cañón del Río Cauca, tenían sus propios cementerios y ahí hay tres cementerios comunitarios particularmente importantes: La Fortuna en Buriticá, Orobaco en Sabanalarga y Barbacoas en Peque. Estos cementerios no son legales, pero tienen cierto orden sobre el lugar en el que la gente inhumaba sus seres queridos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esos cementerios están inundados por la represa de Hidroituango, y para inundar la zona EPM promovió el traslado administrativo, que es una figura legal, para lo cual contrata a la Universidad de Antioquia. Se realiza el traslado administrativo de los tres cementerios. Los cuerpos son llevados a la Universidad, y es en la [audiencia del 8 y 9 de octubre de 2019](https://twitter.com/Movicecol/status/1151495222205079552) cuando encontramos datos que no coinciden en relación con el traslado administrativo, porque **esta figura no se puede utilizar con cuerpos que hayan muerto de manera violenta**, y es finalmente lo que encontramos la segunda semana de marzo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **CR: La primera vez que la JEP solicitó información a EPM recibió pocos documentos, ahora la obligaron a entregar todo sobre Hidroituango. ¿Qué implicación tendría no hacerlo?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

GS: EPM tiene que entregar información, aquí nadie está exento del cumplimiento de obligaciones impuestas por los jueces. Precisamente hay un [escrito de EPM](https://twitter.com/EPMestamosahi/status/1237502603233288194/photo/1) en ese sentido, y el Auto AT-025 de 202, le dice a EPM que la JEP cuenta con toda la potestad para: en primer lugar, solicitar la información que considere pertinente en relación con el caso de medida cautelar, y en general con aquello que está relacionado con el conflicto. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Segundo, para solicitar esa información a terceros civiles, y no solamente a intervinientes/comparecientes. Y tercero, que la Jurisdicción considera que tiene toda la potestad legal y constitucional, por el mandato de protección a las víctimas, para tomar medidas que involucren a terceros. Claro, la posibilidad de tomar medidas cautelares tiene un nivel de exigencia en la medida en que hay que hacer un balance de necesidad, de urgencia, de pertinencia y demás. Pero hasta el momento no se han tomado medidas que afecten a EPM.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido nosotros nos manifestamos, y EPM sacó un comunicado en el que considera que va a cumplir las órdenes emitidas por la Jurisdicción en el tiempo y la manera determinadas en el Auto de Referencia. (También le puede interesar:["En 2019 Unidad de Búsqueda de Desaparecidos llegará a 17 territorios"](https://archivo.contagioradio.com/unidad-de-busqueda-de-desaparecidos/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **En muchas partes de Colombia el conflicto que ha afectado a las poblaciones tiene que ver con la operación empresarial. ¿El caso de EPM da pautas sobre cómo sería la investigación de la JEP sobre la relación entre empresas y conflicto?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

GS: No, de manera específica en este caso no estamos buscando responsabilidades personales ni empresariales, en el trámite actual de medidas cautelares estamos centrados en la protección de los derechos de las víctimas de desaparición forzada. La jurisprudencia internacional señala que estas víctimas tienen el derecho fundamental a la verdad. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De hecho, el derecho a la verdad surge como una necesidad de respuesta a las víctimas de desaparición forzada, la verdad para ellas está anclada a dos respuestas específicas: Qué sucedió con el familiar, cuál fue la suerte de la persona por el que se pregunta; y cuál es el lugar en el que se encuentra su cuerpo, si fuere posible ubicarlo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y una vez que fuera posible ubicarlo, exhumarlo, identificarlo y entregarlo de manera digna a las víctimas. Entonces, **en este momento nosotros no estamos centrados en encontrar responsabilidades**, ya EPM tiene una serie de obligaciones que responder, una serie de documentos que allegar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si hay responsabilidades específicas, lo determinará la sala o sección que avoque conocimiento sobre eso, en su momento y de la manera que determine. (Le puede interesar: ["Unidad de Búsqueda comienza piloto de identificación de 2.100 cuerpos en Norte de Santander y en Nariño"](https://archivo.contagioradio.com/unidad-busqueda-piloto-narino-norte-santander/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **CR: ¿Qué se puede esperar de la acción de la Jurisdicción sobre La Escombrera y la Arenera?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

GS: La segunda semana de marzo proferimos un [auto sobre la Comuna 13](https://twitter.com/JEP_Colombia/status/1238228074195451904), el sitio de la Escombrera o la Arenera es un sitio de búsqueda, pero el tema es los desaparecidos de Comuna 13 posiblemente enterrados en esos lugares. Nosotros hicimos prospección en zona alta de la Comuna 13 en 8 sitios donde no encontramos nada, pero **tenemos 3 sitios marcados con alta probabilidad** en los que esperamos adelantar procesos de prospección y exhumación, si es el caso.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hay que ser pausado y discreto con las posibilidades de hallazgo porque es una zona ampliamente alterada en los últimos años, los testigos tienen dificultades para identificar y reconocer sitios, y además, grandes zonas fueron rellenadas con escombros, o extracción de material. (Le puede interesar: ["Los crímenes de la inteligencia militar llegaron a la JEP"](https://archivo.contagioradio.com/los-crimenes-de-la-inteligencia-militar-llegaron-a-la-jep/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, de la Comuna 13 hay que entender el Jardín Cementerio Universal, que está conectado con la medida cautelar que se tomó, porque todo indica que las personas desaparecidas o de muerte violenta con enorme frecuencia eran enterradas allí por proximidad geográfica. Entendiendo que el Jardín Cementerio queda en la parte baja de la Comuna 13.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que hemos encontrado es un altísimo porcentaje de cuerpos no identificadas que yacen en el Cementerio Universal (cerca de 1.200 personas). Un grupo de esas personas no identificadas fue trasladado al laboratorio de osteología de la Universidad de Antioquia, 138 de manera específica. Esos cuerpos fueron examinados y **en 59 de ellos encontramos posible causa de muerte violenta**, básicamente con proyectil de arma de fuego, y encontramos algunos casos de desmembramiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto es muy grave y señala una alta probabilidad -eso queremos corroborar en el tiempo-, de que estos 59 cuerpos puedan corresponder a algunas personas víctimas de desaparición forzada, y posiblemente de víctimas de la Comuna 13.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el auto que expedimos sobre el caso hay un conclusión contundente: Normalmente las autoridades habían señalado que **el número de víctimas en la Comuna 13 estaba entre los 96 y 114**, que fue lo que nos dijeron en las audiencias del 8 y 9 de octubre de 201**9. Al momento vamos en un número aproximado de 450 desaparecidos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y regreso al punto, un elemento central de la búsqueda de lugares es tener un inventario del universo de víctimas a buscar, tener un inventario de los lugares donde hubo exhumación y tener un inventario de los lugares donde tendrían que hacerse las búsquedas. (Le puede interesar: ["Víctimas ya cuentan con observatorio sobre avance de JEP"](https://archivo.contagioradio.com/victimas-ya-cuentan-con-observatorio-sobre-avance-de-jep/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estamos muy avanzados, eso es una tarea supremamente dispendiosa porque la información está dispersa, está desorganizada y hay incoherencias que tenemos que ir cubriendo. Esa labor está avanzada pero no hemos terminado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto sigue al mediano y largo plazo, pero yo creo que se nos irá todo el año en la consolidación de cifras, en la ubicación de familiares, en la determinación de sitios y en la prospección. Los procesos de identificación seguramente se nos alargarán, pero estamos haciendo todo lo posible para que sean lo más acelerado posible.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
