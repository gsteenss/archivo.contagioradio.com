Title: Los riesgos de la industria militar para la construcción de paz
Date: 2016-12-01 19:14
Category: DDHH, Paz
Tags: Gobierno, militares, multinacionales, paz, seguridad
Slug: 33206-2
Status: published

###### Foto: coyuntura.blogspot.com 

###### [1 Dic 2016] 

En Colombia existen al menos 1.229 convenios de cooperación entre las fuerza militares y diversas multinacionales por un **valor de 2,57 billones de pesos**. Según una respuesta del Ministerio de Defensa,  a una investigación que preparó el equipo del senador Iván Cepeda, en la que se resalta que más de **68.255 personas cumplen la tarea de cuidar al sector minero energético, de infraestructura y vial.** Pero además, estas empresas cuentan con su propia estrategia de seguridad privada.

Frente a este panorama, el más reciente informe de International Institute for Nonviolent Action, titulado "La fuerza invisible en Colombia", analiza el rol e impacto de las Empresas Militares y de Seguridad Privada, EMSPs, frente a los derechos humanos. Allí concluye que **la falta de supervisión pública efectiva, sobre la seguridad privada de las empresas, **genera la violación de los derechos de las comunidades que no quieren permitir el ingreso de, por ejemplo, las multinacionales extractivistas.

La investigación dirigida por la Dra. Leticia Armendáriz analiza los diferentes contextos en los que estas EMSPs actúan en el país. De acuerdo con el informe,  dos son las estrategias que usan las multinacionales para proteger su actividad frente a las acciones rebeldes de sabotaje de oleoductos, secuestros y otras amenazas asociadas al conflicto: "**estableciendo alianzas estratégicas, a menudo de forma pública y manifiesta, con grupos paramilitares de extrema derecha, o bien, contratando servicios de seguridad privada".**

Pese a que se ha evidenciado que existen víctimas de estos sistemas de seguridad de las empresas, la Unidad para la Atención y Reparación Integral a las Víctimas que es responsable de registrar y dar asistencia y reparación a las víctimas de la guerra, no tiene un registro específico a la cantidad de víctimas causadas por actividades relacionadas con la industria militar y de seguridad privada.

Por lo anterior, el informe asegura que "**Colombia es un estado territorial con una enorme y activa industria de EMSPs**, incluyendo grandes EMSPs estadounidenses que han operado bajo el Plan Colombia contra el tráfico de drogas, como también exportando personal de seguridad privada colombiano a estados en situación de conflicto o post conflicto, como Iraq o Yemen". En ese sentido, recomienda al gobierno implementar las medidas necesarias para que en el proceso de desmovilización los exguerrilleros no ingresen al mercado de la seguridad, pues se pone en riesgo la construcción de paz del país.

<iframe id="doc_20340" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/332948350/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-vp4XP6oqSte99HAPpM1o&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>
