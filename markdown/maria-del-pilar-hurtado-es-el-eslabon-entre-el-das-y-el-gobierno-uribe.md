Title: “María del Pilar Hurtado es el eslabón entre el DAS y el Gobierno Uribe”
Date: 2015-01-31 21:36
Author: CtgAdm
Category: DDHH, Judicial, Política
Tags: Alvaro Uribe, chuzadas, das, Interpol, Maria del Pilar Hurtado
Slug: maria-del-pilar-hurtado-es-el-eslabon-entre-el-das-y-el-gobierno-uribe
Status: published

###### **Foto: Contagio Radio** 

"María del Pilar Hurtado no puede cargar sola sobre sus hombros la responsabilidad histórica, política e institucional. Ella no puede aceptar que muchos de su subalternos que ya fueron condenados se conviertan en los chivos expiatorios de algo que nació en la casa de Nariño bajo la dirección del entonces presidente Álvaro Uribe Vélez y su entorno presidencial". Afirma Luis Guillermo Perez de la FIDH.

Las víctimas de las interceptaciones ilegales por parte del Departamento Administrativo de Seguridad entre el año 2002 y 2008, convocaron a una rueda de prensa para pronunciarse sobre la **captura de la ex-Directora del DAS, María del Pilar Hurtado, este sábado 31 de enero en Ciudad de Panamá.**

Pérez Casas, expuso el proceso que ambas organizaciones llevaron a cabo  para solicitar la extradición de la ex-Directora del DAS desde Panamá, así como para que la oficina de la **Interpol Internacional emitiera la circular roja a la ex-funcionaria.**

El Senador Iván Cepeda hizo un llamado al ex-presidente Uribe a la serenidad y a que afronte la justicia. “**Comprendemos su angustia por la situación que va a afrontar, todos los esfuerzos desesperados que hizo en complicidad con el ex-presidente Martinelli se han venido abajo. El trabajo serio que hemos hecho ha logrado que hoy la señora Hurtado este en Colombia**”. Para el Senador Cepeda, la información que tiene María del Pilar Hurtado es fundamental, siendo el eslabón perdido entre el DAS y el Gobierno de Álvaro Uribe.

Por su parte, el precandidato a la alcaldía, Hollman Morris expuso que las víctimas de las interceptaciones ilegales son las primeras en **exigir garantías para que se le proteja la vida a María del Pilar Hurtado, pero tiene que contarle la verdad al país**: quien le daba las ordenes de seguir y amenazar a la oposición política del gobierno Uribe.

La audiencia de imputación de cargos en contra de Maria del Pilar Hurtado se llevó a cabo el sábado 31 de enero a las 2 de la tarde en el Tribunal de Bogotá.

#### Fragmento de la intervención de Luis Guillermo Perez de la FIDH 

\[embed\]https://www.youtube.com/watch?v=7\_BkwTQ6B\_o\[/embed\]  
 

#### Fragmento de la intervención de Iván Cepeda 

\[embed\]https://www.youtube.com/watch?v=kXJmGtMq2k0&feature=youtu.be\[/embed\]  
 

#### Fragmento de la intervención de Hollman Morriz 

\[embed\]https://www.youtube.com/watch?v=8fgBfbpq\_6Y\[/embed\]  
 
