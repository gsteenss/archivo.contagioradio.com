Title: Es urgente solucionar la situación de los presos de las FARC: ONU
Date: 2017-07-14 15:30
Category: DDHH, Nacional
Tags: FARC, Misión ONU, presos politicos, presos políticos de las FARC
Slug: situacion-de-presos-politicos-de-las-farc-deteriora-construccion-de-paz-mision-onu-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/mision-onu-e1500056661395.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Misión ONU Colombia] 

###### [14 Jul 2017]

La Misión de las Naciones Unidas en Colombia, por medio de un comunicado de prensa emitido el 13 de julio, **hizo un llamado para que se dé una solución a la situación de los presos políticos de las FARC** que se encuentran aún detenidos en centro penitenciarios a pesar de la aprobación de la ley 1820 de amnistía e indulto.

Hay que recordar que aún permanecen recluidos en centros penitenciarios **más de 2 mil integrantes de las FARC** a quienes no se les ha solucionado su situación jurídica. En repetidas ocasiones, ellos y ellas han manifestado que los jueces de control de garantías y el Gobierno Nacional, no han cumplido con lo establecido con la ley de Amnistía pactada en el acuerdo de paz. (Le puede interesar: ["Amnistía a integrantes de las FARC se ha incumplido en un 50.4%"](https://archivo.contagioradio.com/amnistia-a-integrantes-de-las-farc-se-ha-incumplido-en-50-4/))

La Misión de la ONU en Colombia es la encargada de iniciar la verificación de la reincorporación a la vida civil de los integrantes de las FARC. Por esto manifestaron que “la detención de los miembros de las FARC en las cárceles por más de seis meses después de la aprobación de la Ley de Amnistía y dos semanas después de la dejación de armas individuales, **socava el proceso de reincorporación y la consolidación de la paz”**.

Adicionalmente, la Misión aseguró que a la situación de los detenidos se suma **“la inseguridad de los miembros de las FARC fuera de las Zonas Veredales”**. Manifestaron que hay un aumento en los casos de amenaza y homicidio “contra ellos y sus familiares”. (Le puede interesar: ["FARC pide a organizaciones internacionales trabajar por libertad de presos políticos"](https://archivo.contagioradio.com/42965/))

Finalmente, instaron a que los mecanismos institucionales, que están involucrados en el proceso de amnistía, **“actúen con responsabilidad y celeridad  para poner fin a una situación que debilita la construcción de la paz”**.

[UNMC Comunicado Julio 13 - 2017](https://www.scribd.com/document/353782382/UNMC-Comunicado-Julio-13-2017#from_embed "View UNMC Comunicado Julio 13 - 2017 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_37382" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/353782382/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-pvoT7H2jBFtjcQKrC0jM&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7055599060297573"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
