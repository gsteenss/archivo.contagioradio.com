Title: Inicia en Bogotá el Encuentro de las Américas para frenar el cambio climático
Date: 2015-09-21 14:01
Category: Ambiente, Nacional
Tags: Alcaldía de Bogotá, ambientalistas, Animalistas, cambio climatico, Comisión de Justicia y Paz, COP20 Paris, Encentro de las americas cambio climático
Slug: inicia-en-bogota-el-encuentro-de-las-americas-para-frenar-el-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/cerros-orientales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: bogotá.gov.co 

<iframe src="http://www.ivoox.com/player_ek_8514558_2_1.html?data=mZqelpqZfI6ZmKiak5eJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2FdxM7OjcrSb6PjyNThh6iXaaKljMrZjarSp9bZz9nf0ZDIqYzgwtiYo9KJh5SZop7fy8jFt4zkwtfOjcvWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Hernando Maldonado] 

###### [21 sep 2015]

El acto de instalación del “Encuentro de las Américas por el cambio climático” fue presidido por el Alcalde de Bogotá Gustavo Petro, quien **resaltó el esfuerzo que se realizará en este encuentro por buscar un punto común de ideas y acciones entre organizaciones sociales y gobiernos locales** para presentar en París, en la cumbre del COP20. [Objetivos del encuentro de las Américas](https://archivo.contagioradio.com/bogota-teje-estrategias-para-enfrentar-y-frenar-el-cambio-climatico/).

En el evento hicieron presencia el Ministro de Ambiente colombiano, y el ministro para asuntos ambientales de Bolivia, así como distintos representantes de organizaciones de México, Guatemala, Salvador, Panamá, Brasil, quienes expondrán en las distintas jornadas las **experiencias y estrategias que hay en sus países**, para mitigar el cambio climático.

Así mismo se adelantarán actividades en otras ciudades de Colombia, como Manizales, Medellín y Cali, en donde al igual que en Bogotá se **discutirán las responsabilidades que tienen todos los sectores tanto ricos, como pobres**, frente al calentamiento global.

**Tres días de actividades:**

La agenda de este encuentro incluye la participación de sectores, sociales, políticos y religiosos. La problemática del agua, emisiones de CO2 y otros agentes contaminantes, alternativas ambientales, serán algunas de las discusiones que se llevarán a cabo y de las cuales también se planea tejer estrategias de acción.

**Mediante la cultura también se buscarán estrategias para crear conciencia climática**, caminatas por senderos ecológicos, día sin carro, exposiciones de arte, movilización en bici, son algunas de las actividades con las que la ciudadanía también podrá participar.

Para estos eventos se espera la participación de alrededor de veinte mil personas de distintas organizaciones sociales y ciudadanos comunes.

###  [Agenda "Encuentro de las Américas Frente al cambio climático](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/agenda-encuentro-frente-al-cambio-climatico.pdf)

 
