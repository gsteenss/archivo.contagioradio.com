Title: Amenazan de muerte a defensores de Derechos Humanos en el Bajo Atrato
Date: 2015-04-20 14:34
Author: CtgAdm
Category: Comunidad, Nacional
Tags: ASOPROBEBA, Casa Castaño Gil, Comisión de Justicia y Paz, Desalojo, Desplazamiento forzado, Paramilitarismo, Pedeguita y Mansilla, Sor Teresa Gómez, tortura
Slug: amenazan-de-muerte-a-defensores-de-derechos-humanos-en-el-bajo-atrato
Status: published

###### foto: contagioradio.com 

“La próxima vez que vuelva a ver a los gringos en el territorio, los voy a sacar amarrados y a arrojar en un lugar apartado y desolado”, afirmó Baldoino Palacio, en una reunión en la que participaban varios integrantes de **ASOPROBEBA**, organización que sirve como estrategia para la apropiación ilegal de tierra en el Bajo Atrato, en los territorios de las comunidades negras, y en los que recientemente se dio un caso de tortura y desplazamiento forzado a familias propietaria de predios en el Consejo Comunitario de **Pedeguita y Mansilla**.

En la denuncia realizada por la organización Comisión de Justicia y Paz, se hace referencia a que la amenaza está dirigida a integrantes de la propia organización y a los acompañantes internacionales  de varias organizaciones que también hacen presencia en la región.

Según la denuncia, esta amenaza es la tercera amenaza después de la agresión que sufrió la familia **Benitez Triana** que se vió obligada a desplazarse tras una incursión de hombres armados y ASOPROBEBA que torturaron y desalojaron a los integrantes de la Familia.

[Ver nota relacionada](https://archivo.contagioradio.com/integrantes-de-asoprobeba-y-encapuchados-armados-agreden-y-desalojan-familia-en-choco/)

Para Justicia y Paz, lo más preocupante del hecho es que ninguna de las denuncias ha surtido efecto a pesar de la identificación clara de los autores tanto de las amenazas, como del desalojo y la tortura, nisiquiera las autoridades locales de policía, testigos y primera autoridad que recibió la denuncia.

Cabe recordar que la Asociación ASOPROBEBA nació como parte de la estrategia de control social y económica de la llamada **Casa Castaño Gil**, luego del desplazamiento y despojo violento de predios colectivos
