Title: Asesinan a Erlin Undagama, docente embera del Chocó
Date: 2020-10-16 19:15
Author: AdminContagio
Category: DDHH, Nacional
Tags: Asesinato de indígenas, Líder Indígena
Slug: asesinan-a-erlin-undagama-docente-embera-del-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Mesa Permanente de Diálogo y Concertación de los Pueblos Indígenas del Chocó, denunció que el miércoles 14 de octubre, aproximadamente a las 7 de la noche, **fue asesinado Erlin Forastero Undagama, docente y gobernador del Pueblo Embera oriundo del Resguardo Jurubida-Chory del municipio del Alto Baudó en el Chocó.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1317110311078559744","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1317110311078559744

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho ocurrió en la cabecera municipal Pie de Pató, cuando el gobernador embera se encontraba con varios miembros del resguardo cobrando el beneficio del programa “Más familias en acción” y fue sacado violentamente por hombres armados, quienes le propinaron tres disparos con arma de fuego en su rostro, causándole la muerte. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia, Erlin Undagama, de 30 años de edad, habría sido asesinado por el grupo paramilitar de las Autodefensas Gaitanistas de Colombia. (Le puede interesar: [Contra viento y marea la minga sigue exigiendo diálogo con Duque en Bogotá](https://archivo.contagioradio.com/7-000-indigenas-de-la-minga-buscan-un-dialogo-con-duque-en-bogota/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La mesa indígena del Chocó lamentó **“profundamente como el silencio y el dolor se apodera de nuestros territorios y nos toca sepultar a los nuestros sin tener alguna respuesta contundente del estado colombiano”.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, exigen a los grupos armados excluir a los pueblos indígenas de los enfrentamientos y a respetar el derecho a la vida en los territorios. (Le puede interesar: [Exgobernador indígena Fredy Güetio y su esposa fueron asesinados en Suarez, Cauca](https://archivo.contagioradio.com/exgobernador-fredy-guetio-esposa-asesinados-suarez-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También exigen al gobierno nacional y local que se investiguen los hechos para dar con los responsables y a implementar estrategias que detengan el asesinato y la violación de los derechos de los miembros de los resguardos indígenas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El asesinato del gobernador se suma al registro **de más de 269 indígenas que han sido asesinados desde el año 2016 y los más de 167 durante la presidencia de Iván Duque con fecha de corte en junio de este año**, según [Indepaz.](http://www.indepaz.org.co/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
