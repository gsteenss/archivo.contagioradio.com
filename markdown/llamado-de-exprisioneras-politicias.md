Title: ¡Que el camino a la paz, no nos cueste la vida! llamado de exprisioneras políticias
Date: 2020-03-19 15:38
Author: AdminContagio
Category: Expreso Libertad, Programas
Tags: Amenazas, Astrid Conde, Expreso Libertad, exprisioneras politicas, paramilitares, paz en colombia
Slug: llamado-de-exprisioneras-politicias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/2511_dibujo_mujeres-731x540-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Con el asesinato de **Astrid Conde**, han aumentado las amenazas en contra de las exprisioneras políticas que iniciaron su camino hacia la paz, con su proceso de reincorporación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Amenazas que provienen algunas de grupos paramilitares, con mensajes claros de estigmatización hacia estas mujeres. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los abogados Juan David Bonilla y Margarita Sánchez que acompañaron tanto a Astrid en su proceso de reincorporación, como a las mujeres que actualmente trabajan en ella, afirmaron que se necesitan garantías urgentes para salvaguardar la vida de quienes le apuestan a la construcción de un camino diferente.

<!-- /wp:paragraph -->

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F2251364168505206%2F&amp;width=600&amp;show_text=false&amp;height=338&amp;appId" width="600" height="338" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>

<!-- wp:paragraph -->

Ver[otros programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
