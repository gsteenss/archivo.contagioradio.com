Title: MOE denuncia inscripciones atípicas de cédulas en más de 93 municipios
Date: 2015-08-21 17:37
Category: DDHH, Entrevistas, Judicial, Política
Tags: Alvaro Uribe, Centro Democrático, CNE, Compra de votos, corrupción, Elecciones 2015, Fuerzas militares, Misión Observación Electoral, oscar ivan zuluaga, trasteo de votos
Slug: moe-denuncia-trasteo-de-votos-en-mas-de-43-municipios-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/votos_comprados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### pulzo.com 

<iframe src="http://www.ivoox.com/player_ek_7297456_2_1.html?data=mJemmZmZeo6ZmKiakp6Jd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkbC5jMnS0NrSp8rVjNnfw9jYqdCfxcqY2NTYs9SfxtOYx9GPtMKZpJiSo6nXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejandra Barrios, directora MOE] 

###### 21 ago 2015

La directora de la **Misión de Observación Electoral Colombia, MOE**, Alejandra Barrios presentó cifras sobre irregularidades en las campañas electorales para las elecciones del próximo 25 de octubre.

Según cifras de la MOE, en más de **93 municipios de Colombia se han presentado inscripciones atípicas de cédulas**. Por otro lado, se habla de la candidatura de 41 militares inscritos al Centro Democrático, partido que ha utilizado logos de la policía y las fuerzas militares en sus campañas.

Estos casos se han duplicado este año, Barrios indica que en promedio de cada 100 mil habitantes en un municipio o ciudad, lo normal es la inscripción de 72 cédulas, sin embargo, de acuerdo con datos de la MOE, **hay 73.6% de inscripciones de cédulas atípicas por cada 100 mil habitantes.**

Alejandra Barrios señala la existencia de irregularidades en regiones que cuentan con la presencia de empresas mineras legales. En estos lugares se ha encontrado un vínculo directo entre intereses políticos e intereses económicos, ya que se ha evidenciado mayores índices de trasteo de votos.

**Hace algunos meses la MOE  presentó estos datos ante el Consejo Nacional Electoral, CNE, para que se tomen las medidas necesarias,** sin embargo, al día de hoy no se han dado respuestas.

**41 militares como candidatos por el Centro Democrático**

En 2014, Álvaro Uribe Vélez y Oscar Iván Zuluaga presentaron una propuesta en donde los militares y soldados pudieran votar, sin embargo, esta propuesta fue archivada. Sin embargo, hoy el Centro Democrático cuenta con 41 candidatos a alcaldías, gobernaciones, concejos y asambleas:

Lista de candidatos militares:

**BOGOTÁ:**

***Concejo de Bogotá:***

Coronel Carlos Julio Castro

Sargento Mayor Fernando Campos

*** Edil:***

Usaquen: Coronel Jesús María Vivas

Usaquen; Hija de suboficial Angie Tafúr

Rafael Uribe Uribe: Agente Víctor Julio Parada

Engativa: Intendente Juán Crisóstomo Sierra

Puente Aranda: Sargento Primero José Teófilo Montiel

Puente Aranda: Sargento mayor José Antonio Contreras

Ciudad Bolívar: Sargento Víctor Manuel Saenz

**CUNDINAMARCA:**

***Concejo:***

Zipacón: Mayor Kevin Mantillana

San Francisco: Sargento Mayor Domingo Eliecer Sánchez

Pacho: Sargento Mayor Saulo Navas Barreto

***Alcaldías:***

La Mesa: General Manuel Guzmán

Chía: Coronel Hector Ignacio Correo

Yacopí: Coronel Pedro Felipe Méndez

San Francisco: Coronel Hector Contreras Lamprea

Villeta: Mayor Leonora Barragán

Puerto Salgar: Técnico jefe Zheir Piñeros

Simijaca: Coronel José Lara

Zipacón: Sargento Primero Bernardo Gonzáles

***Asamblea Departamental***: Coronel Ricardo Pinzón

**BOYACÁ:**

***Alcaldía de Tunja***: Sargento Luis Enrique Hernández

***Consejo:***

Saboyá: Sargento Jaime Sanchez

Puerto Boyacá: Sargento Mayor Alfonso Noé Cárdenas

Duitama: Esposa de oficial: Tatiana Peña de Cabrales

Sogamoso: Hija de oficial María Claiudia Quijano

Tunja: Agente José Joaquín Camacho

**TOLIMA:**

***Consejo de Ibagué***: Hijo de Suboficial Jhon Ferleny Torres

**CAUCA-BOLÍVAR Y SANTANDER:**

***Cauca Gobernación***: General Leonardo Barrero

***Bolívar***: ***Concejo de Cartagena***

Almirante Gabriel Arango Bacci

**Santander**: Sargento Ricaurte Mejía

**Quindío:**

***Alcaldía de Finlandia***: Sargento Javier de Jesús Castro

***Concejo:***

Córdoba: Teniente Fidel Pérez

Armenia: Hija de suboficial Ángela Marín

***Asamblea Departamental***:

Sargento Carlos Arturo López

Sargento Pedro Martínez

**ATLÁNTICO**:

**Gobernación**: Profesional de la Reserva Manuel Díaz Jimenez

***Edil Barranquilla***: Sargento Mayor Pedro Soto

***Asamblea Departamental***: Dragonante Algemiro Alvarado

**META:**

***Concejo:***

Villavicencio: Teniente Coronel Orlando Muñoz

Cumaral: Teniente Coronel Inocencio Ortiz

Granada: Agente Enrique Calzada

***Gobernación:***

Soldado Bachiller Hernán Gómez

***Alcaldía Guamal:*** Coronel Critobal Lozano

***JAL Villavicencio***: Sargento Primero Eusebio Abreo.
