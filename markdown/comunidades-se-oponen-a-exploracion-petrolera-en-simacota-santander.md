Title: Campesinos de Simacota se oponen a destrucción que ocasionaría empresa Parex
Date: 2017-04-18 13:11
Category: Ambiente, Voces de la Tierra
Tags: derrame de petróleo, Exploración Petrolera, Parex, petroleo
Slug: comunidades-se-oponen-a-exploracion-petrolera-en-simacota-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/empresa-parex-contaminacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comunidad  Simacota] 

###### [18 Abr. 2017] 

La **empresa Parex** con presencia en el municipio de Simacota, departamento de Santander está afectando a las comunidades, así lo aseguró Reinaldo Duarte, representante legal de la Corporación Colectivo para la Defensa Integral del Ambiente y las Fuentes (COLDIMAFH), luego del **derrame de ACPM en un caño y de la intención de explorar y explotar hidrocarburos en sus territorios.**

“La empresa entró a los territorios y les prometió a las comunidades que no iba a hacer contaminación y ha sido todo lo contrario, todas las aguas las están contaminando y nosotros en el Bajo Simacota no tenemos acueducto, solo los aljibes que nos dejó la sísmica de hace 6 años” dijo Duarte.  [Le puede interesar: La polémica detrás del proyecto petrolero en Guamal, Meta](https://archivo.contagioradio.com/la-polemica-detras-del-proyecto-ecopetrol-guamal-meta/)

Luego de conocerse la denuncia el pasado miércoles, en la que un carrotanque sufrió un accidente y regó 5800 galones de ACPM en una quebrada de la zona hasta llegar al Río La Colorada, las comunidades ahora deben enfrentar la decisión de **Parex de construir vías de acceso, líneas de flujo y líneas eléctricas para realizar la actividad petrolera en el territorio.**

![WhatsApp Image 2017-04-18 at 10.33.28 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-18-at-10.33.28-AM.jpeg){.wp-image-39332 .aligncenter width="415" height="607"}

Esta decisión, que además iría acompañada de la **constitución de una servidumbre de carácter permanente,** con lo que se limitará el derecho de dominio sobre el territorio que será intervenido en la vereda Aguablanca, del municipio de Simacota, ha hecho que las comunidades se declaren en oposición.

![WhatsApp Image 2017-04-18 at 10.33.29 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-18-at-10.33.29-AM.jpeg){.wp-image-39331 .aligncenter width="415" height="658"}

Parex que adelantaría la obra como ejecutor de Ecopetrol, cuenta con licencia de la Agencia Nacional de Hidrocarburos – AHN -, sin embargo, según denuncia Duarte, **esta alcanzaría un territorio que es privado “ayer se metieron a la brava y le tocó a la comunidad reunirse e ir y sacarlos de allá**. No están dañando solamente el agua sino un bosque que tengo de 20 hectáreas en las que tengo cantidad de especies animales”.

Según el representante de COLDIMAFH, en ese bosque hay monos aulladores, hay chacharos – una especie de cerdo -, tinajos – también conocidos como guagua- y que están en vía de extinción.

**“Están en mi finca, van a ubicar una nueva plataforma que está encima del nacedero de agua y van a contaminar** todo el resto de caños que tenemos aquí en la región y para ellos no pasa nada (…) nos van a dejar sin agua” relató Duarte.

Con la decisión de realizar exploración petrolera en esta zona, cuenta Duarte, no solamente estarían **afectando a las 200 familias que hacen parte de estas comunidades, sino también a los animales y la agricultura**, es decir, se estaría hablando de afectar zonas de pequeña ganadería.

![WhatsApp Image 2017-04-18 at 10.33.31 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-18-at-10.33.31-AM.jpeg){.wp-image-39333 .aligncenter width="415" height="565"}

**Los campesinos han manifestado que defenderán el agua de la zona a como dé lugar** “la empresa sigue insistiendo en entrar, violan todos los derechos a ellos no les interesa nada, les interesa es buscar el petróleo y ya” concluyó Duarte.

Este jueves está prevista una reunión de las comunidades del Bajo Simacota con la Alcaldía, en la que los pobladores sostendrán que **no están dispuestos a permitir la construcción de nuevas plataformas o afectación de bosques o nacimientos de agua. **[Le pude interesar: Comunidades de 8 municipios del Meta rechazan proyecto de Ecopetrol](https://archivo.contagioradio.com/comunidades-8-municipios-del-meta-rechazan-proyecto-petrolero-ecopetrol/)

[Bajo Simacota](https://www.scribd.com/document/345532774/Bajo-Simacota#from_embed "View Bajo Simacota on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_52435" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/345532774/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-29FlJ5eA3FwIH982kYZL&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7066666666666667"><iframe id="audio_18211151" frameborder="0" allowfullscreen="allowfullscreen" scrolling="no" height="200" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18211151_4_1.html?c1=ff6600"></iframe></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
