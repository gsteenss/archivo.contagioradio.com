Title: Iván Cepeda denuncia falta de garantías por parte de la Fiscalía en caso Uribe
Date: 2020-11-05 18:49
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Caso Uribe, Fiscal Barbosa, Gabriel Jaimes, Iván Cepeda
Slug: ivan-cepeda-denuncia-falta-de-garantias-por-parte-de-la-fiscalia-en-caso-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Gabriel-Jaimes-Francisco-Barbosa-e-Ivan-Cepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El senador **Iván Cepeda quien está acreditado como víctima en el proceso que cursa en contra del expresidente Álvaro Uribe Vélez** por los delitos de fraude procesal y soborno, **denunció falta de garantías por parte de la Fiscalía,** ente que recientemente asumió el caso remitido por la Sala de Instrucción de la [Corte Suprema de Justicia](https://twitter.com/CorteSupremaJ).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cepeda aseguró que **el fiscal delegado para el caso, Gabriel Jaimes** —**designado por el Fiscal General Francisco Barbosa**— **no cuenta con la independencia e imparcialidad que le exige el cargo,** y que “*son varios los sucesos que se han hecho públicos que alertan sobre dudosas actuaciones que habría realizado en investigaciones que están a su cargo*”; por ejemplo las de su intención de archivar casos, que luego arrojaron condenas por parte de la Justicia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> **“El Fiscal Gabriel Jaimes, está siendo objeto de una investigación por supuestos manejos irregulares en su labor como Fiscal.”**
>
> <cite>Senador Iván Cepeda, víctima en el caso Uribe.</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por otro lado, el congresista hizo referencia a que en medio de las audiencias del caso, el fiscal Jaimes, “*cuestionó sin fundamento la validez de lo actuado ante la Corte Suprema de Justicia, diciendo que la forma en que fue vinculado Uribe Vélez, afectaba su derecho al debido proceso*” buscando “*desvirtuar aspectos fundamentales del acervo probatorio*” que hay en contra del expresidente.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esto, Cepeda señaló que **“*mientras la Fiscalía actúa con apremio en defensa de los derechos de Uribe Vélez, los cuales, sin fundamento, considera vulnerados por la Corte Suprema de Justicia, no ha actuado con la misma celeridad en otras investigaciones relacionadas con el caso, ni en defensa de los derechos de las víctimas*”.** **(Lea también:** [¿Quién defiende a las víctimas?](https://archivo.contagioradio.com/quien-defiende-a-las-victimas/)**)**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1324364973989126146","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1324364973989126146

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Cepeda también expuso que **desde el pasado 18 de agosto** le solicitó a la Fiscalía que le informara acerca de las investigaciones y las actuaciones relacionadas con la compulsa de copias ordenada por la Corte Suprema de Justicia, mediante la cual resolvió la situación jurídica del exsenador Uribe Vélez y sin embargo, **esa solicitud no ha sido resuelta por el ente acusador.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Iván Cepeda mostró inconformismo desde que la Fiscalía asumió el caso

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde que la Fiscalía asumió la competencia del caso Uribe, **el senador Cepeda mostró serios reparos** sobre la falta de independencia de este ente, por la persona que lo presidia; y cuestionó la labor que pudiera adelantar en el caso **debido a los presuntos sesgos que pudiese tener el Fiscal Barbosa por su cercana amistad con el presidente Iván Duque y en general con miembros del Partido de Gobierno.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto llevó a que una vez la Corte remitió el expediente, Cepeda recusara al Fiscal Barbosa para que se apartara del caso; recusación que fue resuelta negativamente a las pretensiones del congresista, por la misma Corte Suprema. (Le puede interesar: [Iván Cepeda recusará a fiscal y llevará caso Uribe a instancias internacionales](https://archivo.contagioradio.com/ivan-cepeda-recusara-a-fiscal-y-llevara-caso-uribe-a-instancias-internacionales/)**)**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Luego de la celebración de las audiencias en las que intervino la Fiscalía, **Cepeda sostuvo que la tesis del Fiscal Jaimes era «*absurda y peligrosa*» y que la intervención del funcionario le había dado la razón en tener dudas de la actuación que tendría la Fiscalía en el caso pues tenían «*sobrados argumentos para haberlo recusado por su parcialidad*».** 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esa oportunidad Cepeda también señaló que el Fiscal Jaimes, **«***no pudo disimular que su objetivo es anular lo actuado \[por la Corte Suprema\] y garantizar a como dé lugar la impunidad de Uribe***».**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
