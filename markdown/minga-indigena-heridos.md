Title: Minga Indígena reporta 41 indígenas heridos por ESMAD y Fuerza Pública
Date: 2017-11-06 01:13
Category: Movilización, Nacional
Tags: CRIC, La Delfina, Minga Indígena, ONIC
Slug: minga-indigena-heridos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/Minga-indigena.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [06 Nov 2017] 

En el reporte la Minga Indígena de 22 departamentos del país, denuncia que ya son 41 indígenas los que han resultado heridos en los diferentes puntos de concentración. Según las heridas han sido provocadas por el ESMAD, el Ejército y la Policía que han hecho uso desmedido de la fuerza, y habrían dado un tratamiento e guerra la protesta social que se adelanta desde el pasado 30 de Octubre.

Las autoridades indígenas denunciaron que además de las acciones de agresión física contra los participantes de la Minga Indígena también se está presentando casos de judicializaciones ilegales, persecución y estigmatización. Tal es el caso de LEONARDO FLOR quien sigue detenido “a pesar de su haber sido judicializado vulnerando el debido proceso y con fundamento en un arsenal probatorio débil” señalan.

Otra de las situaciones que denuncia la Minga Indígena se dio en el departamento de Risaralda con la amenaza de judicialización contra “MARTÍN SIAGAMA, RODRIGO NACAVERA, OTONIEL QUERÁGAMA, ALBERTO WAZORNA Y CÉSAR QUERÁGAMA”, quienes son señalados de ser  “los responsables de las cosas que están pasando en la Minga \[por lo que\] quedan en la lista de judicialización”. Esta situación se presentó cuando hombres movilizados en una camioneta lanzaron gritos e insultos contra los participantes de la Minga. [Lea también Aumenta Crisis por violación a los derechos humanos a la Minga](https://archivo.contagioradio.com/aumenta-crisis-por-violacion-a-los-derechos-humanos-en-minga-indigena/)

### **Amenazan con quemar y reducir a cenizas al CRIC** 

Una de las situaciones que más preocupa a los lideres de la Minga Indígena sería la amenaza contra el CRIC que habría sido proferida por Andrés Felipe Pérez, quien sería el líder del grupo “líder civil contra el CRIC” y que contaría en sus filas con un número mayor a las 100 personas. Esta persona, según la denuncia, dio plazo hasta el 7 de Noviembre para que se acabe la movilización, de lo contrario  “procederemos a saquear, destruir y reducir a cenizas las instalaciones y vehículos del CRIC”

Otra denuncia se ha presentado en el resguardo Kokonuko que ha sido asediado por integrantes del ESMAD, además a emisora “Renacer Kokonuko” - a la que pertenecía Efigenia Vásquez, periodista asesinada- ,  ha sido blanco de diversos ataques por parte de la fuerza pública y ha resultado afectado un periodista y su familia dado que habitan en la casa en la que funciona el medio de comunicación.

### **La Delfina es un punto crítico de la Minga Indígena** 

Allí, durante todo el fin de semana se han producido fuertes choques entre los indígenas e integrantes de la Fuerza Pública, en los que además de las denuncias por la acción del ESMAD, se ha recopiilado material que prueba la participación de efectivos del ejercito en represión a la protesta. Una de las advertencias de las comunidades es que allí también se han presentado heridos por arma de fuego y si no se evita esa situación se estaría en riesgo de un asesinato por parte de la fuerza pública.

Por estas y otras situaciones las comunidades indígenas que hacen parte de esta jornada de protestas hacen un llamado al gobierno a que retire a las unidades del ESMAD que han estado presentes y que se garantice la judicialización de los responsables de las agresiones, así como se eviten nuevos daños a la vida y a la integridad de las personas que ejercen el legítimo derecho a a protesta.

###### Reciba toda la información de Contagio Radio en [[su correo]
