Title: Amenazan promotores de consulta popular en Cajamarca, Tolima
Date: 2019-05-24 11:03
Author: CtgAdm
Category: Ambiente, DDHH
Tags: Anglogold Ashanti, Cajamarca, La Colosa
Slug: amenazan-promotores-de-consulta-popular-en-cajamarca-tolima
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Cajamarca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

El pasado 15 de mayo, promotores de la consulta popular en Cajamarca, Tolima, que en el 2017  prohibió la minería en el territorio, recibieron amenazas a sus correos electrónicos firmadas por las Águilas Negras en que fueron señalados de impedir el desarrollo en el municipio.

Las amenazas fueron dirigidas a las organizaciones Colectivo Socio-Ambiental Juvenil de Cajamarca (Cosajuca), Comité Ambiental en Defensa de la Vida, Conciencia Campesina y Unión Campesina de Los Alpes, Tolima (UCAT) así como el Presidente del Concejo Municipal de Cajamarca. (Le puede interesar: "[Cajamarca no está arrepentida de la consulta popular](https://archivo.contagioradio.com/cajamarca-no-esta-arrepentida-de-la-consulta-popular/)")

Robinson Mejía, integrante de la organización Cosajuca, afirmó que estas agresiones se dan mediante una campaña de estigmatización que han emprendido la Alcaldía y AngloGold Ashanti por medio del cual los acusa de generar desempleo en el municipio. El ambientalista sostuvo que este discurso ahora lo están usando grupos armados ilegales para convertirlos en su objetivo militar.

El ambientalista indicó que no es la primera vez que estas organizaciones reciben amenazas de las Águilas Negras, hechos que reflejan un aumento en violencia en contra de líderes sociales y ambientales en todo el país.

"No sabemos como se van a desarrollar las cosas porque vemos en el país que cada día hay una escalada en violencia en contra de las personas que defienden los derechos humanos y el territorio. Esperamos que eso no sea el caso de nosotros pero estamos muy atentos de seguir denunciando y más que todo seguir trabajando", afirmó Mejía.

### **La lucha para la defensa del agua sigue**

A pesar de los resultados contundentes de la consulta del 2017, cuando el 97% de votantes en Cajamarca manifestaron su desacuerdo con la actividad minera en el municipio, la empresa AngloGold Ashanti aún no han renunciado los títulos mineros para construir una mina de gran escala de explotación de oro, conocida como La Colosa.

Según Mejía, la empresa estaría reuniéndose con líderes comunitarios, financiando campañas de algunos candidatos a la Alcaldía para las elecciones de octubre y buscando el remplazo al director de la Corporación Autónoma Regional del Tolima con el fin de generar condiciones más favorables para reanudar operaciones en 2020.

Frente a estos hechos, las organizaciones ambientales se unirán a la marcha nacional en contra del fracking el próximo 7 de junio en Ibagué para rechazar nuevamente el desarrollo de proyectos extractivistas en el territorio.

<iframe id="audio_36282238" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36282238_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
