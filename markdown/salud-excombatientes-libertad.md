Title: El derecho a la salud de los excombatientes fuera de prisión
Date: 2018-04-11 10:15
Category: Expreso Libertad
Tags: FARC, presos politicos, Salud
Slug: salud-excombatientes-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/permanence-medicale.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:a3com 

###### 3 Abr 2018 

Luego de la firma del Acuerdo de Paz en La Habana, los prisioneros políticos que salieron de las cárceles del país estaban a la espera de que el derecho a la salud que se les negó por tanto años en prisión, por fin fuera una realidad. En este viaje del Expreso Libertad el testimonio de Julio Marquetalia y la compañía de los abogados Claudia Aguirre y Fabio Diaz, relatarán el drama de la reincorporación y la salud de los prisioneros políticos.

###### <iframe id="audio_25230860" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25230860_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
