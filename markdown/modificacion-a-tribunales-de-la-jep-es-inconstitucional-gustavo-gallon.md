Title: Modificaciones a la JEP serían inconstitucionales: Gustavo Gallón
Date: 2018-11-02 11:33
Author: AdminContagio
Category: DDHH, Paz
Tags: Centro Democrático, Gustavo Gallón, JEP, militares
Slug: modificacion-a-tribunales-de-la-jep-es-inconstitucional-gustavo-gallon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Dqd0SgzWwAA8CQH-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jep] 

###### [02 Nov 2018] 

Como una reforma que podría ser inconstitucional calificó Gustavo Gallón, director de la Comisión Colombiana de Juristas, la modificación realizada a la JEP con la inclusión de los 14 magistrados a la diferentes salas. Además, señaló que este hecho **descalifica el accionar de los juristas actuales y su idoneidad a la hora de llevar los casos que se presenten.**

Para el abogado, este cambio es "desatinado" y si bien esta propuesta tuvo el respaldo de la mayoría de las fuerzas políticas, exceptuado al Polo Democrático y FARC, la medida introduce una división en la percepción de la ciudadanía en relación con la JEP, **acerca de que hay unos jueces legítimos y otros ilegítimos.**

"Significa admitir que los jueces actuales de la JEP son inconvenientes, son los jueces de la FARC, fueron escogidos por un procedimiento que no se debía y que los nuevos jueces si serán los escogidos por el procedimiento adecuado, sí serán creíbles y de confianza de los militares" señaló Gallón; y agregó que este mismo hecho podría conllevar a una estigmatización de los magistrados y una desnaturalización del Tribunal.

Asimismo, aseguró que esta posible modificación a la que aún le faltan 7 debates en el Senado, no se puede permitir, porque "no fue lo que se pactó" en el Acuerdo de Paz y por lo tanto sería inconstitucional, **debido a que implica una re estructuración de la Jurisdicción Especial para la Paz.** (Le puede interesar: ["Cuatro preocupaciones de la Corte Penal Internacional sobre cambios a la JEP"](https://archivo.contagioradio.com/cuatro-preocupaciones-corte-penal-internacional-cambios-jep/))

### **Descalificación de la comunidad internacional es torpe** 

Gallón afirmó que "el argumento y punto de partida de la descalificación sobre la integración de la JEP, por el hecho de que en su selección hayan intervenido personas de otros países en nombre de la comunidad internacional, es inaceptable" manifestó el abogado y aseveró que en ese mismo sentido la solución que le ha brindado es **"torpe"**.

<iframe id="audio_29798378" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29798378_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
