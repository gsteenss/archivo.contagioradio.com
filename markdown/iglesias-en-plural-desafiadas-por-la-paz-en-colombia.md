Title: Iglesias en plural desafiadas por la paz en Colombia
Date: 2015-08-06 11:11
Category: Abilio, Opinion
Tags: Abilio Peña, dialogos de paz, Iglesia, Joan Brown, La Habana, proceso de paz, Rochester Minessota
Slug: iglesias-en-plural-desafiadas-por-la-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Farc-ONG-foto-FARC-EP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Kavilando] 

#### **[Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)-[~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### [6 Ago 2015] 

En distintos  momentos de la historia de Colombia,   las  iglesias, en plural,  han asumido un compromiso directo por la paz de nuestro país, mas siempre los gobiernos  y aún hasta las  mismas insurgencias habían interlocutado predominantemente con la Iglesia Católica o solicitado su exclusiva mediación.

En esta oportunidad  se ha sucedido un  cambio significativo. El protagonismo crecientes de  otras iglesias,  además de la católica, ha hecho que se conviertan en interlocutor necesario de las partes en diálogo y en motor importante de movilización social inspirada desde la fe.

La palabra de las iglesias  ha resonado con fuerza en sus coopartes internacionales, particularmente en el llamado que han dirigido  al gobierno, a la guerrilla de las Farc-Ep, a pactar un cese bilateral  al fuego  que detenga el desangre  de la población civil,  de soldados, de  guerrilleros y las afectaciones ambientales  que como se demostró en el cese de mas de cinco meses respetado por la insurgencia, evitó tantas muertes y daños innecesarias.  Así mismo llama al gobierno de Colombia y a la Guerrilla del ELN a iniciar los diálogos formales, pues sin las demás insurgencias los acuerdos de paz no serían completos.

En este movimiento creciente de los creyentes abogando por la paz, varias acciones significativas han sucedido; 150 miembros   de iglesias motivados por el Diálogo Eclesial por la Paz en Colombia -Dipaz- incluída la católica,   hicieron hincapié a las partes  en las vidas humanas preservadas en la decisión  tomada por las Farc de un cese unilateral de cinco meses, y llamó con vehemencia  a pactar un cese bilateral al fuego que evite  mayores  daños irreparables.

También   la articulación  "Comunidades Basadas en la Fe (Religiones, Espiritualidades y Organizaciones Sociales de Fe)", convocaron a una vigilia en la plaza de Bolívar con un llamamiento similar,  y un significativo número de congregaciones, iglesias, líderes eclesiales llamaron también públicamente “a parar la Guerra y a pactar un cese Bilateral al Fuego”.

Por su parte,  114 religiosas de la comunidad Franciscana de nuestra señora de Lourdes de Rochester Minessota, en Estados Unidos,  dirigieron carta al gobierno de Colombia y a la Guerrilla de las Farc-EP alentando  también  a que se pacte el cese  bilateral al fuego,  reconociendo los avances en  los diálogos y recordando que  los acuerdos deben estar inspirados por la justicia, para evitar que se repitan los ciclos de violencia que han desangrado a Colombia. Con claridad profética manifestaron que “Todas las vidas  son sagradas como lo ha recordado el Papa Francisco en su reciente Enciclica Laudato Si. La de la población civil,  la  de la naturaleza, la de  los soldados y policías y  la de los guerrilleros.  El gobierno debe cambiar su posición de negarse a un acuerdo entre las dos partes por un cese al fuego  en pro de la vida y de la mayor legitimidad de los diálogos que se vienen adelantando.    Llamamos  con la fuerza del Evangelio, en  presencia San Francisco de Asís, de Santa Clara, todos constructores de paz,   a desescalar la confrontación y a pactar un cese bilateral al fuego”.

Entre las firmantes de la carta se encuentra la hermana  Joan Brown, recientemente premiada por el gobierno de los Estados Unidos como una de las doce personas que,  inspiradas en la fe,  han protegido a las comunidades y al medio ambiente de los efectos del cambio climático. Esa fuerza moral en la sociedad estadounidense se compromete con la construcción de la paz con justicia en nuestro país y da fuerza a esa corriente de plurales confesiones que genera opinión e inciden en favor del proceso.

Es especialmente significativo, entonces, que de un lado las Farc-Ep hayan invitado a las iglesias a ser veedoras del cese al fuego unilateral por ellos decretado, que haya manifestado su deseo de reunirse con el papa Francisco en su visita de septiembre a Cuba, y que el gobierno haya respondido positivamente la carta de los líderes religiosos mundiales.  Esperamos que las iglesias en plural  y  las organizaciones eclesiales no sean inferiores a la importante responsabilidad histórica que se les ha puesto en las manos.
