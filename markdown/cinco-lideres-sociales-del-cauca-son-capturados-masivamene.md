Title: Cinco líderes sociales del Cauca son capturados masivamente
Date: 2015-12-15 13:17
Category: DDHH, Nacional
Tags: Cauca, Falsos positivos extrajudiciales, marcha patriotica
Slug: cinco-lideres-sociales-del-cauca-son-capturados-masivamene
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Presos-politicos-2-e1519746085716.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.websur.net]

<iframe src="http://www.ivoox.com/player_ek_9733401_2_1.html?data=mpyglZmUdY6ZmKiak5aJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8rixNSYzoqnd4a1pcnS1MrXb9TjxM7OzsrXb8XZzZCww9rHpYzn0NOYxcbUuNbmwsnc1ZDRpdTd18aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Heriberto Angulo] 

###### [15 Dic 2015] 

**La Red de Derechos humanos del Putumayo, Baja Caucana y Cofanía Jardínes de Sucumbios Ipiales Nariño, denucia** la judicialización y captura masiva de Giovani Zambrano, Arnulfo Chamorro y Aurora Ordóñez, líderes sociales afiliados a Fensuagro y al movimiento político y social Marcha Patriótica.

De acuerdo con la denuncia, el pasado 13 de diciembre las fuerzas militares de la Brigada 27 de Selva y miembros de la policía  retuvieron a Giovavani Zambrano Salinas sin orden de captura,  justificando que tenía “requerimiento jurídico por parte de la Fiscalía”. Al lugar llegó además un helicóptero de la fuerza pública lanzando gas lacrimógeno en medio de la comunidad, afectando a las mujeres, niños y niñas.

Por la circunstancia de los hechos la policía desiste de la retención y el traslado de Zambrano, aunque aseguran que en cualquier momento lo capturarían y lo procesarían.

Horas más tarde en la vereda el Morro en Cauca en un operativo conjunto entre el ejército y la policía nacional capturan a los líderes sociales Arnulfo Chamorro y a su compañera sentimental Aurora Ordoñez.  Ese mismo día, en el Municipio de Piamonte la policía realiza otra captura contra Libardo Pérez junto a Judith Marcela Suárez, sin embargo los dos últimos ya fueron dejados en libertad.

Además de las capturas al día siguiente miembros de la **SIJIN llevan a cabo un** allanamiento en las oficinas de Asociacion Municipales De Trabajadores De Piamonte del Norte del Norte del Cauca ASITRACAMPIC, donde se encontraba documentos de la organización.

Ante esta situación, las organizaciones sociales de las que hacen parte estos defensores y defensoras de derechos humanos, exigen garantías de protección a sus derechos humanos y al debido proceso. Finalmente se solicita al defensor Regional del pueblo “mediar para que secén las persecuciones y seguimientos de los Defensores de los DDHH y líderes de la Región que se encuentran   expuestos por estos hechos”, concluye la denuncia.
