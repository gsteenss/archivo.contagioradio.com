Title: El 17 de octubre se reanudará Audiencia contra General Montoya
Date: 2018-09-24 12:41
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: General Montoya, JEP, víctimas
Slug: el-17-de-octubre-se-reanudara-audiencia-contra-general-montoya
Status: published

###### [Foto: Contagio Radio] 

###### [24 Sept 2018] 

Luego de dos semanas de la primera cesión de sometimiento del General Montoya, el próximo 17 de octubre se retomara este proceso, en el que las víctimas esperan que Montoya reconozca su aporte a este Tribunal **no solo en los casos de las ejecuciones extrajudiciales, sino también en otros hechos como la Operación Orión**.

 En este segundo escenario la defensa de Montoya deberá informar a la sala, los procesos penales que se han adelantado o adelantan en su contra y presentar los hechos jurídicamente relevantes, las personas vinculadas, la clasificación jurídica atribuida y el estado de las actuaciones. (Le puede interesa[r: "Montoya diga la verdad: exigen la verdad"](https://archivo.contagioradio.com/montoya-diga-la-verdad-exigen-victimas/))

De acuerdo con el abogado Manuel García, representante de víctimas de Montoya, el problema que presenta este sometimiento es que "**claramente los casos por los que se presentará el General es un universo ínfimo de la realidad que podrían verdaderamente** existir", razón por la cual, según el abogado, es importante que las víctimas que se sintieron lesionadas por el accionar de Montoya, hagan llegar sus poderes y solicitudes de reconocimiento, para ingresar al proceso.

### **La ritualidad de la JEP** 

El abogado García señaló que la JEP es un laboratorio en donde se esta aprendiendo al mismo tiempo en el que se desarrollan las acciones, sin embargo señaló que ese no puede ser un motivo **para no ser críticos frente a cómo se han venido manejando los procedimientos. **

Referente al reconocimiento de las víctimas, adelantado por la Sala de Definición de Situaciones Jurídicas de la JEP, García manifestó que en la primera audiencia no se alcanzó  ni siquiera reconocer a las víctimas que ya estaban en la Justicia Ordinaría, lo que es "desconcertante" **debido a que tendrían que haber sido las primeras en ser llamadas por la JEP. **(Le puede interesar:["Los 10 casos por los que debería responder el General Montoya ante la JEP"](https://archivo.contagioradio.com/casos-responder-montoya-ante-jep/))

García aseguró que de continuar presentándose esta situación con las víctimas, quienes según el Acuerdo de Paz están en la centralidad de los procesos, como representantes de las víctimas sentarán una voz de protesta y colaboración ante la JEP, para evidenciar cómo podría darse una mejor forma de proceder con estos casos.

<iframe id="audio_28822546" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28822546_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)] 
