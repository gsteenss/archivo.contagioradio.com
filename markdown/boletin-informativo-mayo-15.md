Title: Boletín Informativo Mayo 15
Date: 2015-05-15 17:19
Author: CtgAdm
Category: datos
Tags: Actaulidad informativa, contagio radio, Noticias del día
Slug: boletin-informativo-mayo-15
Status: published

*[Noticias del Día: ]*

<iframe src="http://www.ivoox.com/player_ek_4502653_2_1.html?data=lZqdlJuZd46ZmKiakpuJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfkpqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**Dos estudiantes chilenos** fueron asesinados durante las manifestaciones que se realizaban en ese país **el jueves 14 de mayo**. El hecho generó rechazo tanto al interior del movimiento estudiantil como en la sociedad chilena, que se volcó a las plazas de las principales ciudades en horas de la noche para hacer **"velatones" por los estudiantes** caídos. Habla **Melissa Sepulveda**, **ex-presidenta de la FECH**.

-Detenidos los principales responsables de **golpe de estado fallido en Burundi**. El miércoles en la mañana **al menos tres generales del ejercito** oficial de la nacion africana **provocaron un golpe de estado**, impidiendo el retorno del presidente **Pierre Nkurunziza**. El intento de golpe de estado se fraguó cuando el presidente decidió volver a presentare a las elecciones **por tercer mandato**, siendo esta decisión ilegal deacuerdo con la constitución del país.

-Este jueves, nuevamente **el Consejo de Estado frenó la venta de la empresa ISAGEN**, que estaba prevista para el próximo martes, y dictaminó **medidas cautelares mínimo durante 15 días,** mientras se soluciona de fondo la demanda que busca defender el patrimonio del país. Habla **Mario Valencia**, **Subdirector y analista del Centro de Estudios del Trabajo, CEDETRABAJO**, una de las organizaciones que demandó la venta de **ISAGEN**.

-53 días de protestas contra el **proyecto minero "Tía María"** en la región de **Arequipa en Perú** dejan **5 muertos y más de 200 heridos**, mientras el gobierno continúa sin pronunciarse sobre la concesión minera que rechaza el segundo departamento más grande del país y todos los sectores sociales de las poblaciones afectadas. Habla **Rocio Silva** defensora de DDHH en Peru.

-**Fueron declarados inocentes 8 campesinos capturados durante el paro agrario**, acusados de "rebelión". La defensa a cargo del **Comité Permanente por la Defensa de los Derechos Humanos** y la **Red de Derechos Humanos del Putumayo**, logró demostrar que las acusaciones de la fiscalía se basaban en montajes judiciales. Habla **Yury Quintero**, de la **Red de Derechos Humanos**.

-Balance positivo en el **4to aniversario del 15-M en España**. El 15 de Mayo de 2011 una manifestación convocada por distintos colectivos de base **protestó contra el sistema electoral Español**, por repartir injustamente las votaciones ente los dos grandes partidos y **dar pie al bipartidismo**. Esa misma noche cientos de personas decidieron espontáneamente acampar en las plazas de los ayuntamientos de las principales capitales, conformando el movimiento conocido como **"Indignados"**.
