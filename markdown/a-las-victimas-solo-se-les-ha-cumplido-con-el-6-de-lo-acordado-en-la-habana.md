Title: A las víctimas solo se les ha cumplido con el 6% de lo acordado en La Habana
Date: 2018-04-09 13:27
Category: DDHH, Nacional
Tags: acuerdo de paz, conmemoración víctimas, día de la solidaridad con las víctimas, víctimas
Slug: a-las-victimas-solo-se-les-ha-cumplido-con-el-6-de-lo-acordado-en-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [09 Abr 2018] 

En el marco del Día de la Conmemoración de la Solidaridad con las Víctimas del Conflicto Armado, tanto las víctimas como las organizaciones sociales desarrollarán diferentes **actividades en todo el país**. Recordaron que las garantías de verdad, justicia, reparación y no repetición son esenciales para la consecución de la paz, pero estas no están siendo cumplidas como se había estipulado en el acuerdo de paz.

De acuerdo con Erik Arellana, hijo de Nydia Erika Bautista, víctima de desaparición forzada e integrante del Movimiento de Víctimas de Crímenes de Estado, hay un **incumplimiento por parte del Gobierno colombiano** en lo que tiene que ver con la implementación de los acordado con las FARC. Las víctimas habrían pasado de ser el centro a la periferia.

### **Incumplimiento a las víctimas es casi total** 

Arellana, afirmó que a las víctimas sólo se les ha cumplido **con el 6% de los acordado** y esto “es un incumplimiento casi total”. Recordó que las víctimas de crímenes de Estado “hemos pasado a la marginalidad, hemos sido amenazados y el aumento en los índices de asesinatos de líderes sociales es preocupante”.

A pesar de esta situación, las víctimas en el país continúan luchando por la visibilidad y el fortalecimiento en aras de continuar con el reclamo **de derechos y garantías** fundamentales para la construcción de la paz. El integrante de MOVICE, indicó también que “en la síntesis de verdad, justicia y reparación, está la base de la paz”. (Le puede interesar:["Víctimas exigen ser ser nuevamente el centro de la construcción de la paz"](https://archivo.contagioradio.com/victimas-exigen-ser-nuevamente-el-centro-de-la-construccion-de-la-paz/))

Adicionalmente, dijo que “los reclamos a la verdad **son de una verdad integral** pues queremos que todos los actores que participaron en el conflicto, incluso los terceros cuenten la verdad para que podamos transitar a otra situación”. Recalcó que hay un derecho irrenunciable para las víctimas que es el de la justicia por lo que es tan importante el sistema de justicia transicional.

### **Congreso de la República solo ha revictimizado ** 

En materia de reparación, las víctimas han sido claras en que ha habido “un **Congreso que ha obstaculizado los derechos**”. En temas como los procesos de retorno como medida de reparación, muchas víctimas “no han podido retornar a los territorios y no han podido exigir la restitución sin estar amenazados”.

Arellana hizo énfasis en que el Congreso de la República “no ha dejado hablar a las víctimas, **las ha agredido** y les han negado hechos comprobados como la masacre de las bananeras”. Por esto, recordó que han sido las víctimas las que han estudiado el conflicto armado por lo que esperan que “este año el Congreso tenga una actitud diferente para poder reclamar nuestros derechos”.

<iframe id="audio_25200722" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25200722_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper" style="text-align: center;">

</div>
