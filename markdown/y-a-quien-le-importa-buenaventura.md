Title: ¿Y a quién le importa Buenaventura?
Date: 2015-06-02 13:02
Category: Carolina, Opinion
Tags: buenaventura, Derechos Humanos, Desplazamiento, paramilitares, pobreza, secuestros
Slug: y-a-quien-le-importa-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/buenaventura-gabriel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### [**[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/)- (**[**@**](https://twitter.com/E_Vinna)**E\_vinna)**] 

###### [Junio 2 - 2015] 

El domingo 31 de mayo a las 10:00 a.m Buenaventura, Valle del Cauca, sufrió un apagón que se ha prolongado por días. La causa parece ser el ataque a una torre de energía y no se ha podido arreglar porque más de dos días después, las autoridades no han logrado llegar al lugar donde se encuentra la torre. Este es un cuadro ilustrativo de la presencia estatal en nuestro país, donde las autoridades ni siquiera llegan a una torre de luz que suministra el servicio a cerca de 400.000 habitantes.

La oscuridad nuevamente se apodera del puerto, pues no es la primera vez que un evento de esta magnitud sucede. Sin embargo, ¿cómo puede vivir sin luz el principal puerto marítimo del Pacífico Sur sin que el Gobierno y los empresarios alcen la voz?, la respuesta es sencilla: porque, al igual que el capital, el servicio de luz distingue a los ricos de los pobres. En la penumbra de Buenaventura la Sociedad Portuaria Regional permanece intacta, pues es el único lugar que sí tiene el servicio de energía; por eso ni al Gobierno, ni a los empresarios, les importa que los habitantes vivan uno, dos o más días en la completa oscuridad.

La falta de luz se trasforma en un crítico problema social en una zona como Buenaventura, donde se necesita del servicio para potabilizar el agua, para tener ventilación en un lugar cuyas temperaturas pueden sobrepasar fácilmente los 30° y la humedad es del 75%. Sin energía los comercios han perdido millones de pesos en ventas, las neveras no funcionan y la escasa comida que hay se echa a perder al poco tiempo. Incluso los Centros de Salud y Hospitales ven restringida su actividad por la falta de luz. Pero eso no le importa al Gobierno mientras la Sociedad Portuaria Regional esté en pleno funcionamiento.

Duele lo que pasa en Buenaventura porque lo que sucede hoy con el servicio de energía es lo mismo que pasa diariamente con los derechos humanos de los habitantes del puerto, pobres en su gran mayoría, pues, como mencionó en un reciente especial del periódico El Espectador, basado en las cifras de la Cámara de Comercio de la ciudad, “el 68,34% de las familias bonaverenses obtienen menos de dos salarios mínimos, cifra que se vuelve todavía más crítica cuando tenemos en cuenta que el tamaño de los grupos familiares del municipio es relativamente alto, (las mujeres tienen en promedio cuatro o cinco hijos).

La situación de Buenaventura es tan grave que hasta el Comité contra la Tortura de las Naciones Unidas emitió el pasado 15 de mayo una recomendación específica al Estado colombiano con base en su preocupación por la situación de violencia en el puerto: “El Comité también expresa su preocupación por los graves abusos perpetrados contra civiles por parte de los grupos armados surgidos tras la desmovilización de organizaciones paramilitares, y que incluyen desapariciones forzadas, asesinatos, violencia sexual, reclutamiento de menores, amenazas y desplazamiento forzado. Preocupan especialmente las informaciones que señalan a estos grupos como responsables de los múltiples secuestros y asesinatos registrados durante los últimos años en el municipio de Buenaventura.”.

La cruenta realidad de la población es un secreto a voces. Organizaciones no gubernamentales, como la Comisión de Justicia y Paz, que acompaña a comunidades en el municipio, realiza constantes denuncias sobre la presencia paramilitar y los intereses económicos que promueven la violencia desenfrenada en el puerto más militarizado del país. Sin embargo, parece que en Colombia a nadie más le importa lo que pasa en Buenaventura, ni que falte la luz, ni que sigan funcionando las “casas de pique”.
