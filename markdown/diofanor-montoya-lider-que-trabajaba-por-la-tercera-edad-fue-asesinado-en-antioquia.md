Title: Diofanor Montoya, líder que trabajaba por la tercera edad fue asesinado en Antioquia
Date: 2019-04-30 14:11
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Diofanor Montoya, lideres sociales asesinados
Slug: diofanor-montoya-lider-que-trabajaba-por-la-tercera-edad-fue-asesinado-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Diofanor-Montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Análisis Urbano] 

**Diofanor Montoya,** campesino de 64 años quien lideraba programas que beneficiaban a personas de tercera edad fue asesinado el pasado 27 de abril en el municipio de Maceo, Antioquia mientras realizaba labores agrícolas en su hogar, testimonios de la población apuntan a que las labores que realizaba Diofanor lo convertían en una persona fundamental para su comunidad.

**Óscar Yesid Zapata, vocero del Nodo Antioquia de Coeuropa** informa que según los datos recopilados, hombres armados llegaron hasta la residencia de Diofanor y dispararon en varias ocasiones contra el líder, huyendo de la zona.

Declaraciones del secretario de Gobierno de Maceo, **Julian David Urrego**, apuntan a que las personas que cometieron el homicidio "iban detrás de otra persona", una hipótesis que según Óscar Yesid es "una forma de justificar no solo el homicidio contra el líder sino contra cualquier persona",  a pesar de ello hasta entonces no se tiene conocimiento que Diofanor hubiera sido amenazado con anterioridad.

### **Continúan los asesinatos sistemáticos en Antioquia**

El pasado viernes 26 de abril también fue asesinado en  la vereda Bajo Inglés de Ituango al Norte de Antioquia,  el campesino Andrés Mauricio Roa de 30 años de edad quien hacía parte de la Junta de Acción Comunal de la vereda Mandarín. : [(Lea también: fue asesinado Joaquín Jaramillo, fiscal de Junta de Acción Comunal en Sonsón, Antioquia)](https://archivo.contagioradio.com/fue-asesinado-joaquin-jaramillo-fiscal-de-junta-de-accion-comunal-en-sonson-antioquia/)

A pesar que las investigaciones aún no apuntan hacia un responsable, Zapata señala que en la zona existe un grupo armado que se hace llamar los **Herederos del Clan Isaza, además de integrantes de  las** Autodefensas Gaitanistas de Colombia quienes estarían cobrando más fuerza en la región y podrían obtener más poder en los próximos comicios locales.

Zapata indica que a pesar de las diversas advertencias que han dado a conocer sobre los factores de riesgo que existen contra defensores de DD.HH. y líderes en la región debido a la fuerte presencia de estas estructuras paramilitares, estas no han sido tenidas en cuentas por el Estado.

<iframe id="audio_35164179" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35164179_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
