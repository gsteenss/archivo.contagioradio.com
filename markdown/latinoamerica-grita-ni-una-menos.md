Title: Latinoamérica grita ¡Ni Una Menos!
Date: 2016-10-20 15:53
Category: El mundo, Mujer
Tags: #Niunamenos, feminicidio, mujeres
Slug: latinoamerica-grita-ni-una-menos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vivasnosqueremos-e1476995458607.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:@edgardotlsur] 

Bajo el lema **¡Ni una Menos!** Miles de mujeres en diferentes papises de Latinoamérica como Chile, México, Ecuador, Guatemala entre otros, salieron a las calles a **exigir que acaben los feminicidios y cualquier tipo de violencia de género**. Las movilizaciones se convocaron después de que se conociera el caso de Lucía Pérez, una joven de 16 años de edad víctima de feminicidio en Argentina. Le puede interesar: ["El mundo grita, ni una menos vivas nos queremos"](https://archivo.contagioradio.com/el-mundo-grita-ni-una-menos-vivas-nos-queremos/)

Con frases como **¡El machismo mata!, ¡Disculpen la molestia pero nos están asesinando! O ¡Vivas nos queremos!**, las mujeres del mundo expresaron el dolor y la rabia de morir “por ser mujer”, a su vez exigieron a sus gobiernos que se creen políticas y programas que prevengan la violencia y sean seguros y de fácil acceso para las mujeres.  Le puede interesar: ["Enfoque de género no se negocia"](https://archivo.contagioradio.com/enfoque-de-genero-no-se-negocia/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]

\
