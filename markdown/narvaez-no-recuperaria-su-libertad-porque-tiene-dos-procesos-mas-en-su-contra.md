Title: Los procesos por los que Narváez no recupería su libertad
Date: 2016-12-21 11:21
Category: Nacional, Política
Tags: chuzadas, Claudia Julieta Duque, das, José Miguel Narváez
Slug: narvaez-no-recuperaria-su-libertad-porque-tiene-dos-procesos-mas-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/jose_miguel_narvaez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [21 Dic. 2016] 

A pesar que el magistrado Parra del Tribunal de Bogotá resolvió una tutela presentada por la defensa del ex sub director del DAS **José Miguel Narváez, éste no quedaría en libertad porque tiene por lo menos dos procesos pendientes por los crímenes cometidos por el DAS** en contra de Claudia Julieta Duque por tortura psicológica y por el magnicidio de Jaime Garzón.

Para el abogado Luis Guillermo Pérez, del Colectivo de Abogados José Alvear Restrepo, más que **la posibilidad de que Narváez recupere su libertad, la decisión del magistrado es “una mala señal para las víctimas** y es bastante negativa para la sociedad colombiana, la institucionalidad y la democracia” más aún cuando contradice una decisión anterior que ampliaba la pena.

Agrega el abogado que además la Fiscalía no ha querido hacer investigaciones de contexto, lo que hubiese tenido que hacer cuando **se habla de crímenes tan graves como que a la hija de Claudia Julieta Duque la hallan llamado para amenazarla con descuartizarla**, o que a Hollman Morris le llegara una corona fúnebre a su casa, o que a Piedad Córdoba la hallan tratado de vincular con las AUC para desprestigiarla luego de una orden impartida por el DAS.

Otra de las preocupaciones del abogado y de las víctimas gira en torno a las demás investigaciones contra ex funcionarios del DAS Jorge Noguera, quien tiene una investigación en curso por parte de la Corte Suprema de Justicia. "El caso de Narváez no puede ser un precedente para otras investigaciones" afirma el abogado Pérez.

Por su parte la periodista Claudia Julieta Duque afirmó que "alertar a las autoridades para que no dejen en libertad al señor José Miguel Narváez que tiene otros procesos pendientes en su contra". Además **las víctimas han manifestado reiteradamente su descontento porque no se avanza de manera eficaz en las investigaciones y en los procesos contra los responsables de las operaciones ilegales del DAS, que no se limitaron a las escuchas ilegales.**

<iframe id="audio_15266079" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15266079_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
