Title: Zonas de Reserva Campesina listas para la implementación de Acuerdos
Date: 2016-12-05 16:20
Category: Nacional, Paz
Tags: Implementación de Acuerdos, sustitución de cultivos ilícitos, zonas de reserva campesina
Slug: zonas-de-reserva-campesina-listas-para-la-implementacion-de-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Campesino_5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Planeta Tierra] 

###### [5 Dic 2016] 

Durante el fin de semana se llevó a cabo en Curumaní Cesar, el V Encuentro de Zonas de Reserva Campesina, en el que **participaron alrededor de 1500 personas** y construyeron distintas propuestas sobre temas de **acceso a la tierra, mujer, ambiente, sustitución de cultivos y construcción de Paz.**

Carmenza Gómez presidenta de la Zona de Reserva Campesina del Cauca resaltó que dentro del mismo evento, se realizó el I Encuentro de Mujeres de las ZRC en el que “más de 500 mujeres tuvieron la posibilidad de **discutir y conformar mesas de trabajo para dar inicio a una plataforma de lucha de las integrantes de las Zonas de Reserva”.**

Por otro lado, Gómez señaló que **“es la primera vez que las instituciones se hacen participes de los encuentros”** y algunas como la Agencia Nacional de Tierras, Parques Naturales, delegados de UCRA, la Unidad de Restitución de Tierras, el comandante del ejército responsable de tema de tierras y Eduardo Díaz, delegado para el tema de sustitución de cultivos, hicieron parte de las mesas de trabajo.

Delegaciones de los 7 nodos regionales participaron y debatieron junto funcionarios de la institucionalidad, quienes adquirieron el compromiso de asignar recursos en el 2017, para los **planes de desarrollo de las 6 ZRC constituidas hasta el momento y otras 7 que ya cumplieron con toda la documentación",** aseguró Carmenza Gómez.

De igual forma, la presidenta manifestó que “el encuentro logró recolectar y actualizar información, se concluyó que ya son cerca de **70 las ZRC y hay muchas comunidades interesadas en conformarse como ZRC”.**

Las Zonas Veredales Transitorias también estuvieron presentes en el encuentro, ya que varias están dentro o muy cercanas a las ZRC, se planteó que en estos lugares **“es donde primero deben implementarse los acuerdos,** construirse vías, alcantarillados y **hacerse proyectos productivos”** indicó Gómez.

### **¿Y la sustitución de cultivos?** 

Respecto al tema de cultivos ilícitos, se conformó la COCAN o Coordinadora Cocalera, en la cual “se dejó planteado el principio que **la sustitución debe ser manual, concertada y sin afectar los cultivos de pan coger de las comunidades ni su integridad”** puntualizó la lideresa. Le puede interesar: [“Gobierno no podrá imponer su política de sustitución de cultivos ilícitos”.](https://archivo.contagioradio.com/gobierno-no-podra-imponer-su-politica-de-sustitucion-de-cultivos-ilicitos-cesar-jerez/)

“El gobierno debe atacar a las cadenas altas, no a los medianos y pequeños productores de hoja, estamos dispuestos a sustituir cultivos, pero **debe haber voluntad política y garantías para los campesinos”** asevero Carmenza Gómez.

<iframe id="audio_14637116" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14637116_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
