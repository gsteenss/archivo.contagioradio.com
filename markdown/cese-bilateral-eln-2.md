Title: Inicia el cese al fuego bilateral Gobierno-ELN
Date: 2017-10-01 12:15
Category: Nacional
Tags: cese fuego, ELN, Gobierno
Slug: cese-bilateral-eln-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/cese-bilateral-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: 

###### 1 Oct 2017 

A las 00:00 de este domingo 1 de octubre entró en vigencia el **decreto 1590 de 2017** por medio del cual se decreta el **cese al fuego bilateral y temporal de carácter nacional en el marco del acuerdo alcanzado entre el Gobierno de Colombia y el Ejército de Liberación Nacional -ELN**, contemplado inicialmente hasta el 9 de enero de 2018.

El decreto presenta como objetivo principal **mejorar la situación humanitaria de la población, suspender acciones ofensivas y evitar incidentes armados entre la fuerza pública e integrantes del ELN.** Un cese al fuego que estará sujeto al cumplimiento de los términos acordados en los protocolos pertinentes.

Para tal fin, ordena la **suspensión de operaciones militares ofensivas y operativos policiales**, sin perjuicio al cumplimiento de las funciones de la fuerza pública de garantizar las condiciones necesarias para el ejercicio de los derechos y libertades públicas en el país, y sus acciones estarán enmarcadas en el proceso de paz como mandato presidencial, en busca de garantizar el derecho constitucional a la paz.

Adicionalmente, el acuerdo establece el [Mecanismo de Veeduría y verificación (MV&V)](https://archivo.contagioradio.com/asi-es-el-protocolo-de-veeduria-del-cese-al-fuego-entre-el-gobierno-y-el-eln/), compuesto por tres instancias: **nacional, regional y local**, con el propósito de prevenir e informar sobre incidentes que pudieran presentarse en cumplimiento del cese. Mecanismo que estará integrado por **representantes del Gobierno Nacional - Fuerza Pública, el  ELN, la Iglesia Católica y una misión política con observadores no armados de la ONU**.

Para garantizar la seguridad de los integrantes del MV&V, el gobierno dispondrá de la **Unidad Policial para la Edificación de la Paz (UNIPEP)**, y el Ministro de defensa deberá establecer los lineamientos a la fuerza pública para el cumplimiento de lo dispuesto en el decreto incluida la designación de su representación en el mecanismo de veeduría, y la coordinación que fuese necesaria entre las fuerzas militares y de policía.

Del mismo modo, se notifica que los recursos necesarios para la implementación de los compromisos establecidos en el decreto, y los demás que surjan de las negociaciones, **provendrán del Fondo de programas especiales para la paz.** Mientras que **la coordinación con las autoridades nacionales, departamentales o municipales se canalizaran por medio de la oficina del Alto Comisionado para la Paz**.

Finalmente se advierte que el Cese bilateral, **podrá terminarse anticipadamente si se presenta un incumplimiento grave que determine la mesa**, con previo informe del Mecanismo de Veeduría y Verificación, así mismo **podrá prorrogarse si desde la Mesa de negociaciones se llega a ese acuerdo**.

[Decreto 1590 Del 29 de Septiembre de 2017](https://www.scribd.com/document/360397032/Decreto-1590-Del-29-de-Septiembre-de-2017#from_embed "View Decreto 1590 Del 29 de Septiembre de 2017 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_91666" class="scribd_iframe_embed" title="Decreto 1590 Del 29 de Septiembre de 2017" src="https://www.scribd.com/embeds/360397032/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-OPylwS6v6doZH6juNxPO&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6545718432510885"></iframe>
