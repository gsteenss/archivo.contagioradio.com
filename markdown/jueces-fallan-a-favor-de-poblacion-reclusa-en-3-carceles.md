Title: Jueces fallan a favor de población reclusa en 3 cárceles
Date: 2020-04-24 17:42
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Comisión de Justicia y Paz, covid19, pandemia
Slug: jueces-fallan-a-favor-de-poblacion-reclusa-en-3-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/mujeres-en-las-carceles.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Los jueces de las ciudades de Tunja, Manizales e Ibagué fallaron a favor de tres tutelas interpuestas por la Comisión de Justicia y Paz en **defensa y garantía de la vida de la población reclusa de estos centros penitenciarios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cada una de estas acciones cuenta con plazos distintos para que se cumplan las garantías y solicitudes. Algunas de ellas son el acceso al **agua potable y la entrega de elementos de protección como tapabocas**, antibacterial, entre otros.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel de Cómbita-Boyacá
------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La población privada de la libertad, en la cárcel de Cómbita, afirma que tienen acceso al agua **dos veces al día durante 15 minutos.** Razón por la cuál han creado un sistema conformado por baldes que les permita recoger el líquido y llevarlo a sus celdas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta situación, según la tutela "pone en un estado de alto riesgo la salud de los reclusos". Asimismo, el Juez de Familia de la ciudad de Tunja afirma que ese hecho **"vulnera la dignidad" al no tener acceso al agua en condiciones óptimas.** (Le puede interesar: "[Por crisis carcelaria piden medidas cautelares ante CIDH](https://archivo.contagioradio.com/por-crisis-carcelaria-piden-medidas-cautelares-ante-cidh/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Razón por la cual ordena al INPEC que proceda en el mantenimiento correspondiente de los filtros de agua y demás instalaciones del sistema de tratamiento de agua para el consumo humano.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma le ordena a la misma institución que disponga todo lo necesario para que el suministro sea suficiente y constante durante todo el día. En ese sentido, el Juez dio un plazo de 48 horas, después de emitido el fallo para que se proceda a su cumplimiento.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel de Coiba-Ibagué
----------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La población reclusa de esta cárcel alegaba las pésimas condiciones en términos de salubridad que tiene este penal. Además del máximo riesgo en el que se encontraban, debido al posible contagio de Covid 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, el pasado 8 de Abril, la Personería realizó una visita a este centro de reclusión. En ella pudo constatar que no hay evidencia que el personal del establecimiento **haya recibido información o educación sobre las recomendaciones del Covid 19.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma confirma que el establecimiento no cuenta con un procedimiento de aislamiento, no evidencia acciones de limpieza, desinfección y recolección de residuos. Como tampoco cuenta con suministro de agua, ni de insumos para el lavado de manos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esa situación el Juez de Familia de la ciudad de Ibagué ordena al INPEC que implemente alternativas para dar solución al abastecimiento del agua de forma continua y permanente. Asimismo, **insta a las autoridades correspondientes a cumplir con los protocolos** y lineamientos para el control del Covid 19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En razón de ello, ordena suministrar a todas las personas recluidas, el personal administrativo y la guardia del INPEC, los elementos necesarios para el lavado de manos y los objetos que ayuden a mitigar el riesgo de contagio como tapabocas o mascarillas quirurjicas. También ordena las acciones de **limpieza y desinfección del centro de reclusión**, todas estas disposiciones en un plazo de tres días.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente ordena al director del centro penitenciario que en un plazo de seis meses procedan a iniciar y culminar gestiones que permitan adecuar la infraestructura de la red interna del sistema hidrosanitario.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cárcel de Manizales
-------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para el caso de la población reclusa de la Cárcel de Manizales, los presos denunciaban la ausencia de protocolos en el tratamiento del Covid 19. Situación a la que se suma la vulneración al derecho a la salud que viene exigiendo estos reclusos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esta situación, el Juez laboral de la ciudad de Manizales, ordena al director del INPEC, el brigadier Mojica y al director de este centro de reclusión, Neveth Londoño **elaborar un plan conjunto para la prevención, control y manejo del Covid 19.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo suministrar elementos sanitarios y de aseo a la población privada de la libertad de Manzales "durante y hasta tanto se supere la pandemia". De igual forma ordena supervisar y vigilar que el consorcio de salud a cargo de prestar este servicio a la población reclusa de esta ciudad, cumpla con las instrucciones de control del virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los tres fallos también señalan la notificación a las respectivas Procuradurías y Defensorías del pueblo de las correspondientes ciudades. Ello con la intención de verificar el cumplimiento de las acciones en las tres cárceles del país. (Le puede interesar: "[Rumbo a un genocidio carcelario](https://www.justiciaypazcolombia.com/rumbo-a-un-genocidio-carcelario/)")

<!-- /wp:paragraph -->
