Title: La JEP es una oportunidad única para resarcir los derechos de las Víctimas: MOVICE
Date: 2017-07-07 12:47
Category: DDHH, Nacional
Tags: ACORE, Jurisdicción Espacial de Paz, MOVICE
Slug: la-impunidad-en-crimenes-de-estado-es-del-90-alejandra-gaviria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/victimas-movice-en-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 Jul 2017] 

Según Alejandra Gaviria, integrante del Movimiento de Víctimas de Crímenes de Estado (MOVICE), la Jurisdicción Especial de Paz es una herramienta sumamente importante que le permitiría al país saldar una deuda histórica en el cumplimiento de los derechos de las víctimas “tenemos en Colombia un conflicto de décadas que ha generado una gran cantidad de **víctimas que no han visto satisfechos sus derechos, la impunidad en el país supera el 90%”** agregó Gaviria.

De acuerdo con  Gaviria, los ataques del General en retiro Jaime Ruíz vocero de la Asociación de Oficiales Retirados de las Fuerzas Militares (ACORE) **son el reflejo de un país que no conoce la verdad del conflicto armado y que demuestra la necesidad de un Sistema de Verdad Justicia y Reparación** que garantice los derechos de las víctimas.

Durante la audiencia, **el General Ruíz señaló que los miembros de la Fuerza Pública están enfrentando una guerra jurídica** y son víctimas de los colectivos de abogados defensores de las víctimas de crímenes de Estado. (Le puede interesar: ["Las víctimas de crímenes de Estado tienen derecho a ser escuchadas"](https://archivo.contagioradio.com/las-victimas-de-crimenes-de-estado-tenemos-derecho-a-ser-escuchadas/))

Para Gaviria el debate sobre la creación de este Sistema de Verdad, Justicia y Reparación “hiere muchos cayos e intereses” debido a que garantizar los derechos de las víctimas a la verdad, justicia y reparación **“significa romper unos privilegios, romper cierto manto de impunidad** y romper prácticas que de alguna forma han beneficiado a sectores que han estado comprometidos con la guerra” afirmó Alejandra.

### **La esperanza de las víctimas en el Sistema de Verdad Justicia y Reparación** 

Desde el inicio de los debates sobre los acuerdos de paz, las víctimas de crímenes de Estado han encendido las alarmar por la falta de consulta en los proyectos de ley sobre el Sistema de Verdad, Justicia y Reparación, **manifestado que no hay participación ni centralidad de las víctimas en esta implementación**.

De igual forma frente a los comentarios del General Ruíz, Gaviria aseguró que para que estos hechos no se repitan **debe conformarse una Comisión de la Verdad para conocer quiénes y por qué motivos estuvieron en las acciones dentro del Conflicto armado**. (Le puede interesar:["Víctimas de crímenes de Estado piden que se desclasifiquen archivos militares"](https://archivo.contagioradio.com/victimas-de-crimenes-de-estado-piden-que-se-desclasifiquen-archivos-militares/))

“Vimos cómo fue atacada una persona que defiende a las víctimas de crímenes de Estado y eso solo es posible en un país que no conoce la verdad, por eso es que necesitamos una Comisión de la Vderdad, para que aquí no puedan venir personas a nombre de sus intereses personales a **decir que no existieron nunca falsos positivos o que Colombia no es el país con mayor número de desplazados**” aseguró Gaviria.

### ** El reconocimiento a las víctimas de Crímenes de Estado** 

Gaviria expresó que es importante valorar el ejercicio que realizó la Corte Constitucional al convocar y escuchar las diferentes voces de quienes han sido afectadas directamente por el conflicto armado y manifestó que pese a los señalamientos de ACORE, **las víctimas siempre han demostrado tener esperanza y persistencia en la lucha por la defensa de sus derechos**.

“Nosotros como víctimas de crímenes de Estado tenemos las cualidades de la esperanza y la persistencia, para nosotros nunca ha sido fácil que se reconozca varias cosas: primero que existe una criminalidad de Estado, **segundo que el actuar de agentes del Estado a dejado víctimas y tercero que no se han reconocido nuestros derechos**” afirmó Alejandra.

<iframe id="audio_19682961" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19682961_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
