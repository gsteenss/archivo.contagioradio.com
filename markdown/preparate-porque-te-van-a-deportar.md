Title: "Prepárate porque te van a deportar"
Date: 2016-11-17 14:37
Category: DDHH, El mundo
Tags: Deportados, Donald Trump, Estados Unidos, Hilary Clinton, Latinos en Norte America, Latinos en USA, Votaciones
Slug: preparate-porque-te-van-a-deportar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/niños.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Unimedios Agencia de Noticias] 

###### [17 Nov. 2016] 

Luego de las elecciones llevadas a cabo el pasado 8 de Noviembre en Estados Unidos, las voces de protesta en este país no se han detenido. **Cientos de movilizaciones y actos simbólicos han hecho que el mundo, conozca el descontento de un amplio número de personas que no votaron por el actual presidente electo, Donald Trump.**

Francisco Herrera, líder comunitario en San Francisco en Estados Unidos manifestó que las señales de protesta continúan en todas las escuelas para “repudiar” las elecciones, y precisó que es necesario recordar “que ni siquiera voto la mitad de la gente y eso es un mensaje de que la gente ya no está creyendo en el proceso electoral”.

**Cerca de 30 ciudades en todo Estados Unidos, han continuado con las marchas y las diversas expresiones de repudio al presidente electo** que ha amenazado con expulsar a millones de latinoamericanos, ha dicho que creará muros para no permitir la entrada de latinos y musulmanes y perseguirá a familias por su orientación sexual.

Por su parte, luego de pasados 9 días de las presidenciales en USA, el candidato electo Donald Trump ha empezado a dar a conocer algunos nombres que podrían acompañarle en su mandato, para el cual se posesiona en enero de 2017.

Stephen Bannon ha sido una de esas personas que Trump ha designado para que trabaje con él, más exactamente como consejero presidencial en todo las labores que vaya a realizar el presidente electo. **Diversos grupos de organizaciones civiles ya han mostrado su rechazo a la designación de Bannon pues lo acusan de ser un extremista.**

Para Francisco, tras todos los anuncios hechos por Donald Trump **la población se encuentra como en una “resaca,** porque mucha gente que votó en contra de Hilary Clinton por cuestión de aborto, porque la gente se sintió traicionada por las políticas elitistas (…) hubo mucha confusión y **ahora que se están viendo ataques raciales y el peligro de deportar gente, muchas iglesias, pastores evangélicos, se han dado cuenta que su propia membresía está en peligro y se arrepienten de su voto”. **Le puede interesar: [Donald Trump no es nuestro presidente: latinos en usa](https://archivo.contagioradio.com/trump-no-es-nuestro-presidente-latinos-en-usa/)

“Prepárate por que te van a deportar”

Así lo aseveró Herrera, quien contó para Contagio Radio que **en las escuelas los niños latinos están siendo víctimas de continuos ataques y violencias** “(…) a una niña de 9 años le dijeron en la escuela: ¿eres latina? Pues prepárate porque te van a deportar”.

**Según cifras otorgadas por Herrera, solo en los últimos 8 días, más de 300 casos de ataques de violación a los derechos humanos, de violencia contra personas latinas** “incluso se está viendo la violencia entre los mismos latinos que tienen papeles o que llevan mucho tiempo aquí – en Estados Unidos – contra personas que acaban de llegar” agregó Francisco.

**Continuarán las movilizaciones**

En la actualidad organizaciones sociales y líderes, están realizando diversas actividades encaminadas a que la movilización social se convierta en acciones que redunden en la protección de latinos, comunidad LGBTI e ilegales que están en Estados Unidos.

Se pretende que a mediano plazo pueda lograrse la legalización de cientos de latinos e intentar frenar acciones que puedan perjudicar a la sociedad que habita en USA “nos estamos preparando para protegernos, también estamos  haciendo que la gente empiece a legalizarse y por último hacerse ciudadanos para poder participar” concluyo Francisco Herrera. Le puede interesar: [“Xenofobia no debe tener un lugar en la política estadounidense”: WOLA](https://archivo.contagioradio.com/xenofobia-no-debe-tener-un-lugar-en-la-politica-estadounidense-wola/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
