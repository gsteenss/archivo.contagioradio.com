Title: Se abre la posibilidad de justicia para víctimas de Santiago Uribe
Date: 2017-06-12 18:21
Category: DDHH, Nacional
Tags: 12 apostoles, Santiago uribe
Slug: se-abre-la-posibilidad-de-justicia-para-victimas-de-santiago-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Santiago-Uribe-Centro-Tampa-e1497306434666.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Eje 21] 

###### [12 Jun 2017] 

Tras la decisión de la Fiscalía General de la Nación de dejar en firme el llamado a juicio a Santiago Uribe, se abre la puerta para que más de **500 víctimas del grupo paramilitar los 12 Apóstoles, puedan tener acceso a la verdad, justicia y reparación**.

De acuerdo con Daniel Prado, abogado representante de la familia de Camilo Barrientos, se espera "una decisión en derecho en el caso de Santiago Uribe y que las contrapartes se abstengan de ejercer cualquier tipo de presión contra la administración de justicia” por ello pedirán que **el proceso se radique en un sitio diferente a Antioquía para evitar posibles presiones de los jueces**. Le puede interesar:["Santiago Uribe habría financiado voluntariamente grupos paramilitares"](https://archivo.contagioradio.com/santiago-uribe-habria-financiado-voluntariamente-grupos-paramilitares/)

Santiago Uribe está siendo investigado por su participación en el grupo paramilitar los 12 Apóstoles y el homicidio de Camilo Barrientos, sin que esto lo exonere de otras investigaciones que está realizando la Fiscalía por el resto de asesinatos cometidos por el grupo paramilitar que **operó en la década de los 90's en Yarumal y otros municipios cercanos. **Le puede interesar: ["Caso Santiago Uribe no es cuestión de pruebas sino de poder"](https://archivo.contagioradio.com/verdades-de-los-12-apostoles-en-juicio-santiago-uribe/)

“Nosotros llevamos más de 16 años buscando que estos hechos que han estado en la impunidad salgan a la luz pública y que el Estado Colombiano tome cartas en el asunto, en esa medida recibimos la decisión **con mucha alegría y esperando que la administración de justicia siga** actuando imparcialmente y se valore el material probatorio que hace parte del proceso” manifestó Prado.

Sin embargo, pese al avance en términos de justicia, el abogado lanzó una alerta sobre la reactivación de las actividades paramilitares en la región de Antioquia **“la presencia paramilitar allí es una forma de intimidación contra la población que vivió en su momento los crímenes cometidos por los 12 Apóstoles**”, afirmó Prado.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
