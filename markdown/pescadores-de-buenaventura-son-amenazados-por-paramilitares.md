Title: Pescadores de Buenaventura que están en Paro Cívico son amenazados por paramilitares
Date: 2017-05-19 12:52
Category: DDHH, Movilización
Tags: buenaventura, Paro cívico, Pescadores, Pescadores artesanales
Slug: pescadores-de-buenaventura-son-amenazados-por-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/marcha_buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Blu Radio] 

###### [19 May. 2017] 

A propósito del Paro que vive Buenaventura, los pescadores, quienes hacen parte del Comité Cívico del Paro, aseguraron que están siendo amenazados por las autodenominadas “Autodefensas Gaitanistas de Colombia”, intimidaciones que ponen en riesgo la vida de los pescadores “si decís algo te mato, si pones la denuncia te mato y ahora ya no salimos a pescar(sic)” relató **Manuel Bedoya, Presidente de la Organización de Pescadores Artesanales en Buenaventura.**

Según lo cuenta Bedoya, para poder pescar se han tenido que movilizar a sitios más lejanos, generando sobrecostos en su labor, dado que deben gastar más gasolina. Sumado a esto dice que las condiciones de seguridad se complican “allá a donde vamos no hay la protección de la Armada, la Policía no puede ir hacia donde vamos a producir la alimentación de este país y parte de Buenaventura, ya casi ni comemos pescado por el alto costo del combustible y de los implementos”.

### **Buenaventura no le cree a los Ministros "no vivimos de buenas intenciones"** 

Ante la comitiva del Gobierno que se ha desplazado hasta Buenaventura, el vocero de los pescadores dijo que **ya no le creen a “estos ministros porque ya nos la hicieron una vez”.  **Le puede interesar: [“Si no cerramos el puerto, el Gobierno no nos presta atención” habitantes de Buenaventura](https://archivo.contagioradio.com/si-no-cerramos-el-puerto-el-gobierno-no-nos-presta-atencion-habitantes-de-buenaventura/)

Bedoya se refiere al Ministro de Ambiente, Luis Guillermo Murillo quien fue nombrado el pasado 26 de abril y a quien hace 3 años lo nombraron como gerente para la resolución de los problemas de Buenaventura, pero una vez nombrado ministro se dejaron quietas las negociaciones con las comunidades de ese departamento.

“No le creemos al señor Ministro a pesar de que es nuestro hermano y nuestro paisano, porque **él ya mostró el cobre frente a la cuestión de la posición de toda la gente de Buenaventura** y en este momento a la posición del pacífico colombiano (…) él está es por lavarle la cara al señor presidente de la República, no más, el presidente tiene muy buenas intenciones, pero de eso no vivimos”.

### **Las actividades del paro continuarán** 

Hoy se realizará la quema de recibos de Hidropacífico que según las comunidades de Buenaventura es otro de los males que aquejan a la ciudad “porque ese otro monstruo atracador que nos viene saqueando, quitando la plata. **No hay agua, mal servicio. Ya se han gastado como 170 mil millones para hacer unos tanques**, pero no saben cómo hacer para seguir sacando billete en medio de la corrupción” añadió Bedoya. Le puede interesar: ["Gobierno hace oídos sordos al paro del Chocó"](https://archivo.contagioradio.com/gobierno-hace-oidos-sordos-al-paro-en-choco/)

### **Insistimos en la declaratoria de Emergencia Social y Económica** 

Dice el líder que el paro no se levantará y que por el contrario continuarán exigiendo al presidente de la República declare la emergencia social, económica y ecológica en Buenaventura ante las graves situaciones que está viviendo la población.

**“No tenemos hospitales, se nos está muriendo la gente, no tenemos agua y no tenemos educación.** Hace más de 1 año se formó la mesa del pacto por la educación y hace más de 1 año que no hablan de ese pacto ya firmado y acordado por la comunidad, entonces tenemos toda la razón”.

Adicionalmente, los integrantes del paro han asegurado que hasta el momento no han llegado a acuerdos, porque la exigencia del Gobierno es levantar los puntos de encuentro que han estructurado en el marco del paro y “eso no lo vamos a hacer porque si se levantan los puntos de encuentro la gente se va y aquí tenemos que ser radicales o somos o no somos” declaró Bedoya. Le puede interesar: [Inicia el paro cívico cultural de Buenaventura](https://archivo.contagioradio.com/inicia-el-paro-civico-cultural-de-buenaventura/)

### **Solicitudes especiales de los pescadores artesanales** 

Adicional al pliego de peticiones que ha presentado el Comité del Paro Cívico de Buenaventura, los pescadores artesanales han asegurado que **requieren mayor seguridad para poder trabajar en la pesca ya que les están robando los buques y las canoas,** así mismo instan al Ministerio de Hacienda a poner en marcha la política pesquera.

“Le estamos diciendo al Gobierno nacional que no le dé garrote a la gallinita de los huevos de oro que tiene en este país que somos los trabajadores de Buenaventura” concluyó Bedoya.

<iframe id="audio_18792432" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18792432_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
