Title: Fracking encuentra las puertas cerradas en San Martín
Date: 2016-09-02 17:14
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Ambiente, fracking, Movilización
Slug: san-martin-se-alista-para-tercera-movilizacion-contra-el-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/fracking-e1472854044186.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Silla Vacía] 

###### [2 Sep 2016] 

San Martín es uno de los municipios del país donde se tiene pensado extraer petróleo con base en la técnica del Fracking, una actividad que la comunidad quiere impedir a toda costa, y por ello desde diciembre de 2015, la población viene movilizándose. Ahora preparan **la semana contra la segunda jornada nacional contra el fracking en Colombia y además se alistan para convocar una consulta popular**.

**“Pese a las movilización y la oposición de la comunidad no se ha logrado establecer un diálogo con el gobierno y la empresa ConocoPhillips.** “Hemos intentado acordar talleres de discusión que terminan siendo charlas dictadas por el gobierno y las petroleras donde las voces críticas no tienen posibilidad de participar”, denuncia Carlos Andrés Santiago, vocero de al **Corporación Defensora del Agua, Territorio y Ecosistemas, CORDATEC.**

De manera reiterada esa organización ha manifestado al Ministerio del Interior, la Agencia Nacional de Hidrocarburos y el PNUD, así como la Defensoría del Pueblo, la manera en que el Gobierno Nacional ha incumplido y violado los acuerdos a los que se ha llegado con la comunidad de San Martín. Además denuncian que “la empresa ha llegado a los habitantes a hacer socializaciones repartiendo lechona, con regalos, grupos de danza, orquestas y grupos musicales como la época de la colonia, para saquear nuestros recursos”, asegura Santiago.

“El gobierno y al empresa están pasando por encima de las comunidades, están violentado nuestro territorio, están violentando nuestro derecho a decidir sobre nuestros recursos, sobre nuestra agua, sobre nuestro territorio”.

Así mismo, desde CORDATEC aseguran que pese a que el **alcalde de San Martín,** Saul Celis, ha dicho que apoya a la población, está que **recibe incentivos por 1300 millones de parte del Ministerio de Minas** para impulsar la explotación de hidrocarburos. Así mismo, la Administración Municipal ha dado inicio desde hace varias semanas a una estrategia para señalar, estigmatizar e intimidar a quienes hacen parte del movimiento en contra del Fracking.

De acuerdo con la información de Carlos Andrés, ya hay contratos firmados para más de 7 bloques petroleros para el fracking, y además hay más de 20 bloques ya asignados, no solo en San Martín, Cesar, si no también Cundinamarca, Tolima, Boyacá, Santander, Meta, es decir que **“no es solo la lucha de un municipio sino de todo el país en el que se necesita que se declare la moratoria”.**

La actividad del próximo 24 de septiembre inicia con un conversatorio con invitados nacionales e internacionales y el domingo 25 de septiembre se realizará la tercera movilización desde las 2 de la tarde.

San Martín es uno de los municipios del país donde se tiene pensado extraer petróleo con base en la técnica del Fracking, una actividad que la comunidad quiere impedir a toda costa, y por ello desde diciembre de 2015, la población viene movilizándose. Ahora preparan la semana contra la segunda jornada nacional contra el fracking en Colombia y además se alistan para convocar una consulta popular.

“Pese a las movilización y la oposición de la comunidad no se ha logrado establecer un diálogo con el gobierno y la empresa ConocoPhillips. “Hemos intentado acordar talleres de discusión que terminan siendo charlas dictadas por el gobierno y las petroleras donde las voces críticas no tienen posibilidad de participar”, denuncia Carlos Andrés Santiago, vocero de al **Corporación Defensora del Agua, Territorio y Ecosistemas, CORDATEC.**

De manera reiterada esa organización ha manifestado al Ministerio del Interior, la Agencia Nacional de Hidrocarburos y el PNUD, así como la Defensoría del Pueblo, la manera en que el Gobierno Nacional ha incumplido y violado los acuerdos a los que se ha llegado con la comunidad de San Martín. Además denuncian que “la empresa ha llegado a los habitantes a hacer socializaciones repartiendo lechona, con regalos, grupos de danza, orquestas y grupos musicales como la época de la colonia, para saquear nuestros recursos”, asegura Santiago.

“El gobierno y al empresa están pasando por encima de las comunidades, están violentado nuestro territorio, están violentando nuestro derecho a decidir sobre nuestros recursos, sobre nuestra agua, sobre nuestro territorio”.

Así mismo, desde **CORDATEC** aseguran que pese a que el alcalde de San Martín, Saul Celis, ha dicho que apoya a la población, está que recibe incentivos por 1300 millones de parte del Ministerio de Minas para impulsar la explotación de hidrocarburos. Así mismo, la Administración Municipal ha dado inicio desde hace varias semanas a una estrategia para señalar, estigmatizar e intimidar a quienes hacen parte del movimiento en contra del Fracking.

De acuerdo con la información de Carlos Andrés, ya hay contratos firmados para más de **7 bloques petroleros para el fracking, y además hay más de 20 bloques ya asignados,** no solo en San Martín, Cesar, si no también Cundinamarca, Tolima, Boyacá, Santander, Meta, es decir que “no es solo la lucha de un municipio sino de todo el país en el que se necesita que se declare la moratoria”.

La actividad del próximo 24 de septiembre inicia con un conversatorio con invitados nacionales e internacionales y el domingo 25 de septiembre se realizará la tercera movilización desde las 2 de la tarde.

<iframe src="https://co.ivoox.com/es/player_ej_12760149_2_1.html?data=kpekmJWVeJqhhpywj5WdaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZClssXmhqigh6adt4zHwtPhy8bLs4ampJCwsbeohbW5pJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 

######  
