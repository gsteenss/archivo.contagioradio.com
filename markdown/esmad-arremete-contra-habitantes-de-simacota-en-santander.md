Title: ESMAD arremete contra habitantes de Simacota en Santander
Date: 2017-07-28 15:31
Category: Ambiente, DDHH
Tags: ESMAD, Mineria, Parex, petroleo, Simacota
Slug: esmad-arremete-contra-habitantes-de-simacota-en-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/simacota-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Comunidades Simacota] 

###### [28 Jul 2017] 

Los habitantes de Simacota en Santander, denuncian que el ESMAD, el día jueves 28 de julio, **arremetió en contra de ellos como respuesta a las protestas por los incumplimientos de la multinacional Parex**. Según la comunidad, continúan haciendo exploraciones de petróleo en el territorio sin mediar con ellos y ha dejado sin trabajo a muchas personas.

Reinaldo Duarte, integrante del Colectivo para la Defensa de las Fuentes Hídricas y el Medio Ambiente COLDIMMAFH, manifestó que **“la comunidad ha venido presentado problemas con la empresa porque los están dejando sin trabajo”**. Él fue enfático en que la empresa lleva personal de trabajado de otras partes del país afectando a las personas del municipio que trabajaban con la empresa. (Le puede interesar: ["Campesinos denuncian daños ambientales por derrame de ACPM en Simacota, Santander"](https://archivo.contagioradio.com/campesinos-denuncian-danos-ambientales-por-derrame-de-acpm-en-simacota-santander/))

De igual manera, dijo que, ante los reclamos de los trabajadores, **“la empresa llama al ESMAD y ellos llegan a golpear a la gente”**. Según Rivera, “como la empresa dice que es paro, entonces ahí está el ESMAD”. Ante esto, Rivera indicó que varias personas resultaron heridas por el uso de balas de goma o perdigones y “la gente se retiró porque el ESMAD tenía el control del territorio”.

### **Nadie Responde** 

La situación del d**año ambiental y social que ha generado la empresa Parex en Simacota no es nueva**. En Abril pasado, se presentó el volcamiento de un camión cargado con ACPM en un caño del municipio y los habitantes denunciaron que la limpieza del caño no se había hecho bien y el petróleo estaría llegando a los ríos generando un daño ambiental.

Según Rivera, “ya se hizo la aclaración de los daños ambientales ante el consejo pero nadie ha demandado a la empresa y eso se va a quedar así porque el alcalde está con la empresa”. Dijo que **Parex no reconoce que su actividad está generando problemas con el ambiente**, “ellos dicen que no sacrifican una gota de agua para sacar un barril de petróleo”. (Le puede interesar:["Campesinos de Simacota se oponen a destrucción que ocasionaría empresa Parex"](https://archivo.contagioradio.com/comunidades-se-oponen-a-exploracion-petrolera-en-simacota-santander/))

Finalmente, Rivera manifestó su preocupación por los habitantes de **Simacota a quienes les han violado sus derechos laborales y ambientales**. “Somos los olvidados, nadie nos escucha, yo fui a la presidencia en abril y dijeron que acá no pasa nada”. Rivera indicó que el ESMAD continúa en su territorio y que el alcalde va a realizar un consejo de seguridad el día de hoy.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
