Title: "vamos a ver con qué nos sale el gobierno". Asociación de Camioneros
Date: 2015-03-12 19:35
Author: CtgAdm
Category: Economía, Nacional
Tags: Asociación de Transportadores de Carga, Camioneros, Paro de Transportadores
Slug: a-15-dias-de-paro-camionero-vamos-a-ver-con-que-nos-sale-el-gobierno-asociacion-de-camioneros
Status: published

###### Foto: Cesar Pachon 

<iframe src="http://www.ivoox.com/player_ek_4205616_2_1.html?data=lZedl5uVeo6ZmKiakpqJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhYyllpDRh6iXaaK4wtiYxsqPtMLm0JDQw9LNs8%2FZ09SYh5eWusLh0NiYw5DaqdOfxNTbjdbZaaSnhqamjdPTt4yhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alfonso Medrano, Asociación Colombiana de Camioneros] 

El paro camionero ya ha dejado su primer saldo de heridos en departamentos como Nariño y Boyacá. Las organizaciones de transportadores de carga en Colombia tendrán este jueves 12 de marzo una nueva reunión con el Ministro del Interior, buscando establecer una mesa de negociación que solucione los problemas de este sector.

El presidente de la Asociación Colombiana de Camioneros de la seccional Boyacá, Alfonso Medrano, indica que el 98% de los vehículos de carga del país han cesado sus actividades. En los diferentes puntos de concentración del paro, los camioneros han enfrentado los ataques del ESMAD.

El líder social espera que el gobierno plantee soluciones a los problemas de fletes, gasolina y chatarrización que están sufriendo los camioneros hace más de 4 años. "Nosotros hemos trabajado por este país, lo tenemos donde lo tenemos. No será mucho, pero, si no hubiese sido por el sector transporte, Colombia tampoco habría progresado"

También, hizo un llamado a colombianos y colombianas para que estén al tanto del paro, pues afirma que los medios de comunicación no lo han visibilizando. "Tenemos un sistema periodístico arrodillado a un gobierno, porque priman los intereses económicos frente a los intereses de su profesión, que es informar".
