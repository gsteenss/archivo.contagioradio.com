Title: Propuestas de la sociedad civil son claves para destrabar mesa ELN-Gobierno
Date: 2017-01-10 13:50
Category: Entrevistas, Paz
Tags: ELN, Mesa de diálogos Quito
Slug: propuestas-de-la-sociedad-civil-son-claves-para-destrabar-mesa-eln-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [10 Enero 2017] 

El próximo 12 de enero se dará el encuentro entre las delegaciones  del ELN y el gobierno, para intentar proseguir con el proceso de paz, motivo por el cual, **diferentes organizaciones redactaron una carta con propuestas para destrabar el proceso**, en ella exigen que se lleve a cabo la  liberación de Odín Sánchez y que cumplan puntos pactados como los son los indultos.

Frente a este punto, las organizaciones y personas que firman la carta proponen que además de los dos gestores de paz que ya hay, **se designen otros dos, debido a los impedimentos jurídicos que han imposibilitado dar los indultos**, y reafirmaron la necesidad de que se realice un proceso de conversaciones más cerrado: “**no es saludable ni conveniente la negociación a través de los micrófonos… una fase exploratoria debe ser, por definición, discreta**”.

De acuerdo con Monseñor Monsalve estas propuestas surgen como iniciativas por “**superar las dificultades que han ido surgiendo desde el instante en que se anunció la fase pública de estas conversaciones**”. Le puede interesar: ["12 de enero, nueva fecha para reunión entre delegaciones del ELN y gobierno de Colombia"](https://archivo.contagioradio.com/12-de-enero-nueva-fecha-para-reunion-entre-delegaciones-del-eln-y-gobierno-de-colombia/)

Frente a la reunión y las expectativas que hay de ella, Monsalve afirma que después del 12 de enero se sabrá si, tanto el gobierno como el ELN “lograron ver más allá de los obstáculos que se han puesto, que parecieran inamovibles” y de las necesidad del país para que se frene la guerra. A su vez, advirtió que es importante estar pendientes del comienzo de la implementación de la Habana “que aún es vulnerable” y añadió que esta reunión tiene un carácter importante para el futuro inmediato del país, motivo por el cual se espera que “**no se vayan por las ramas de los temas sino que se mire el trasfondo de un dialogo entre el Estado y una organización Subversiva**”.

Para finalizar Monseñor Monsalve señalo que como sociedad el reto más grande que se tiene es pensar como aunar esfuerzos para asumir y responder a lo que se viene en términos económicos, políticos, no solo a nivel interno sino también externo.

Además Monseñor Monsalve se refirió a la amenaza que recibió y señalo que ya hay una investigación en curso por parte de la Fiscalía, que pretende  ir tras la pista de los responsables de este hecho, y afirmó que “su vida ha seguido bajo normalidad”.

<iframe id="audio_16110923" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16110923_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
