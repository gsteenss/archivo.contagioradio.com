Title: 30 años de prisión pagará José Miguel Narváez por homicidio de Jaime Garzón
Date: 2018-08-14 12:52
Category: Nacional, Política
Tags: Jaime Garzon, José Miguel Narváez
Slug: condena-narvaez-homicidio-garzon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Diseño-sin-título-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cablenoticias 

###### 14 Ago 2018 

El Juzgado Séptimo Penal Circuito Especializado de Bogotá, profirió sentencia de primera instancia en contra de **José Miguel Narváez**, exsubdirector del DAS, **condenándole a la pena principal de treinta 30 años de prisión**, como determinador responsable del delito de homicidio agravado, del humorista y periodísta Jaime Garzón.

En rueda de prensa, el abogado defensor de Derechos Humanos Sebastián Escobar, aseguró que "**José Miguel Narváez lo que hizo fue transmitir a las AUC el mensaje de altos mandos militares** y sembrar la idea del asesinato. Por eso es condenado como determinador del crimen"

Por su parte, el abogado Luis Guillermo Pérez, del Colectivo de Abogados Jose Alvear Restrepo, afirmó que "p**rimero se intentó judicializar a Jaime Garzón presentándolo como guerrillero**, como no pudieron, se optó por su asesinato. En esta ocasión **José Miguel Narváez no podrá escapar de su condena**". (Le puede interesar: [Jaime Garzón conciencia crítica de una generación](https://archivo.contagioradio.com/jaime-garzon-conciencia-critica/))

La sentencia firmada por el juez Ricardo Mojica Vargas, incluye una **pena accesoria de inhabilidad para el ejercicio de los derechos y funciones públicas** por un lapso de diez años, la cual cumplirá simultáneamente con la pena principal de prisión y que se niegue a Narváez **la suspensión condicional de la ejecución de pena y la prisión domiciliaria**.

Adicionalmente, se ordenó el **pago de quinientos salarios mínimos mensuales** legales vigentes a favor de las personas reconocidas como la parte civil: la compañera permanente de Jaime, Gloria Cecilia Krong y sus hermanos Marisol, Alfredo y Manuel Garzón Forero para lo cual tendrá un mes a partir de la ejecutoria de la sentencia

Una de las decisiones controversiales de la sentencia, se relaciona con la indicación de **abstenerse de declarar el homicidio como de lesa humanidad**, a lo que desde el Colectivo de Abogados José Alvear Restrepo responden que "el crimen de Jaime Garzón Forero es un crimen de Estado y crimen de lesa humanidad. La evidencia probatoria apuntaba a ello. Al no reconocerlo, **el Juez revela un desconocimiento del expediente**".

**Segunda condena por homicidio de Jaime Garzón**

Con el anunció de la Condena a José Miguel Narváez, y la proferida contra Carlos Castaño en 2004, como co-autores del delito de homicidio agravado, se conocen dos de los perpetradores del magnicidio de Jaime Garzón.

Narváez, quien fue Subdirector del Departamento Administrativo de Seguridad del DAS entre el 1 de junio y el 25 de octubre de 2005, **actualmente se encuentra retenido en el centro carcelario "La Picota"**.

<iframe id="doc_44196" class="scribd_iframe_embed" title="1395-7 Jaime Hernandogarzon Forero Sentencia 031" src="https://www.scribd.com/embeds/386198799/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-9ss6IwCmaR97kEnwmwgT&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.75"></iframe>

Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
