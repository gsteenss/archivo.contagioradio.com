Title: Condenan a exmilitar Garzón Garzón por secuestro extorsivo y tortura
Date: 2017-07-19 15:18
Category: DDHH, Nacional
Tags: M-19, militares, Palacio de Justicia
Slug: condenan-a-exmilitar-garzon-garzon-por-desaparicion-forzada-y-tentativa-de-homicidio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/sargento-garzon-garzon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Partícular ] 

###### [19 Jul de 2017] 

El Juzgado sexto Penal del circuito especializado de Bogotá condenó a 11 años al Sargento (r) Bernardo Garzón Garzón, al Capitán (r) Camilo Pulecio Tovar y al Teniente (r)Gustavo Arévalo Moreno por tentativa de homicidio, secuestro extorsivo y tortura **en el caso de Guillermo Marín, ex militante del movimiento M- 19.**

### **¿Quién es Guillermo Marín?** 

Guillermo Marín, era activista y defensor de derechos humanos en el barrio Corinto en Bogotá, en el año 1986.  El 8 de abril de ese mismo año, Guillermo, junto con otros líderes sociales y religiosos se encontraban en una reunión preparatoria que tenía como objetivo **presentarle un informe de la situación de derechos humanos y desaparición forzada al Papa en su visita a Colombia.  **(Le puede interesar: ["Luego de 30 años víctimas de Palacio de Justicia continúan sin reparación integral"](https://archivo.contagioradio.com/luego-de-30-anos-familiares-del-palacio-de-justicia-siguen-sin-reparacion-integral/))

Sobre las 9:00 p.m, Marín fue raptado cuando salia de la sede de la Juventud Trabajadora de Colombia, por cuatro hombres que lo amordazaron y ataron de piernas y manos, posteriormente fue trasladado a otro vehículo, tipo furgón que lo llevaría al batallón Charry Solano **en donde fue torturado y sometido a  tratos crueles e inhumanos por más de 48 horas, mientras era interrogado.**

Durante los actos de tortura los victimarios le dan a entender a Marín que hacen parte del B2, luego de dos días del secuestro,  es amordazado, metido en costales y trasladado al parque Florida donde recibe dos disparos. (Le puede interesar:["Controversia por placa que honra a militares por retoma de Palacio de Justicia"](https://archivo.contagioradio.com/concejo-de-bogota-deberia-reconocer-la-memoria-de-las-victimas-del-palacio/))

En un manuscrito el Sargento (r) Bernardo Garzón Garzón, afirmó que el Coronel Iván Ramírez Quintero, al mando del Batallón Charry Solano, fue quién ordenó al Capitan Camilo Pulecio y al Téniente Mejía Lobo **realizar las capturas de Marín y otro integrante del M-19 y relata cómo se le ordena ir al Hospital de la Hortúa,** lugar en donde estaba Marín después de haber sido encontrado por una patrulla de Policía, en el Parque de la Florida.

"Esa noche debían despedir a los pacientes (matar) y botarlos en partes diferentes. Es así como el Capitán Pulecio lleva en un Nissan al señor Hernández Niño y lo matan en la antigua autopista norte; el Teniente Mejía Lobo en otro Nissan junto con el Cabo Primero Gasca y el Sargento Bermúdez llevan al batallón dos costales de empacar arroz y sacan de la camioneta al señor Marín, lo meten en los costales y se dirigen al Parque La Florida, allí lo bajan y una vez en el suelo proceden a dispararle y creyéndolo muerto se alejan hacia el batallón (...) Como el señor Marín sobrevivió y efectuó las denuncias, el Coronel Ramírez me mandó en comisión para la III brigada al B-2. La camioneta la reformaron como para pasajeros (...) El Coronel Ramírez ordenó buscar nuevamente al Señor Marín para cogerlo y matarlo, pero este señor se asiló en la embajada de Francia y salió del país".

### **Sentencias a los militares condenados** 

El 9 de marzo de 2009, 19 años de la declaración de Bernardo Garzón, el Fiscal 11 de Derechos humanos y Derecho Internacional Humanitario, **dictó apertura de la investigación, ordenando la vinculación de Garzón, Camilo Pulecio, Gustavo Arévalo, y Carlos Mejía Lobo.** (Le puede interesar: ["Coronel (R) Edilberto Sánchez pagará 40 años de prisión por desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/coronel-edilberto-sanche-condenado-por-palacio-de-justicia/))

De igual forma el 12 de noviembre de 2010 la Fiscalía General de la Nación y la Unidad de Derechos Humanos y Derecho Internacional Humanitario, profirieron una resolución de acusación en contra de **Camilo Pulecio Tovar y Gustavo Arévalo Moreno, como presuntos autores responsables de los concursantes delitos de Homicidio agravado** en grado de tentativa, secuestro extorsivo y tortura, conforme a la adecuación dada en el acápite correspondiente a la providencia.

Este caso llevaba en proceso 31 años y de acuerdo con el abogado Daniel Prado, quien maneja el caso de Marín, serían muchos más los casos de desaparición forzada en los que estaría involucrado Garzón Garzón.

### **Bernardo Garzón Garzón y  el caso de Nydia Erika Bautista** 

En el año 1996 Bernardo Garzón, se retracta de las declaraciones que había hecho ante la Procuraduría, haciendo que su testimonío pierda validez. Entre las declaraciones que había realizado Garzón, señalaba cómo había sido desaparecida **Nydia Erika Bautista por integrantes del Batallón Charry Solano.**

Para Yaneth Bautista, hermana de Nydia Ekika, con la sentencia proferida por el Juzgado, el testimonio inicial de Garzón **retoma fuerza y abre la puerta para que continúen investigaciones hacía los más altos rangos al interior del Ejercito.**

"Esta sentencia se suma a otras proferidas en años anteriores, sobre los casos de Amparo Torres también desaparecida por la misma Brigada, sobre el caso del Palacio de Justicia y en conclusión para nosotros como víctimas lo que queda claro es que ahora la Corte Suprema de Justicia y la Fiscalía General deben adelantar investigaciones sobre el General Iván Ramírez Quintero y el General Hurtado" afirmó Yaneth.

**[Bernardo Garzón](http://coeuropa.org.co/168/) fue capturado el 30 de enero de 2014 y estuvo privado de la libertad durante dos años y 7 meses**, sin embargo, fue dejado en libertad por vencimiento de términos en julio de 2016. En otras declaraciones Bernardo habría manifestado ser el responsable de otras desapariciones como las de Irma Franco (1985), Antonio Hernández Niño, Amparo Tordecilla (1989), Carlos Uribe (1989) Oscar William Calvo, Ángela Trujillo y Alejandro Arcila (1985), Nydia Erika Bautista de Arellana, Cristóbal Triana, Bertil Prieto Carvajal, Francisco Luis Tobón, Blanca Emilia Mahecha Marín Rosalba Hurtado Ospina, María Yaneth Muños (1987), José Cuesta (1988).

<iframe id="audio_19896929" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19896929_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
