Title: Paramilitares amenazan a organización de familiares de las FARC
Date: 2017-09-30 17:40
Category: DDHH, Nacional
Tags: Amenazas, AUC, FARC, lideres sociales, paz
Slug: familiares-farc-amenazas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/paramilitares-tolima-AUC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Pulzo 

###### 30 Sept 30 

Integrantes de la organización Legados Tejiendo Vida, Paz y Memoria, integrado por familiares de miembros del partido Fuerza Alternativa Revolucionaria del Común FARC, denunciaron a través de un comunicado una serie de amenazas que habrían sido proferidas por un grupo denominado como A.U.C.

De acuerdo con la información, los hechos habrían tenido lugar el 28 de septiembre cuando Isaioa Villacob, recibió en su cuenta personal, un correo electrónico enviado desde la cuenta auccapital@outlook.es. En el texto aparece consignada una amenaza contra lo que denominan fundaciones fachada de guerrilleros "para seguir robando al pueblo y la gente de bien".

Adicionalmente, en el correo electrónico declaran objetivo militar a todos los integrantes de tales fundaciones y sus colaboradores, asegurando que los tienen identificados y ubicados, amenazando con asesinar a sus hijos y familiares e incluso cometer actos de violación en su contra, si siguen "metiendo las narices donde no deben".

Los firmantes, quienes aseguran que la misma advertencia llegó a la misma hora a las cuentas de Facebook y de Isaioa Villacob y Oriana Lugo, piden al gobierno nacional se implementen herramientas de protección para líderes y líderesas sociales y defensores de Derechos Humanos, así como a la Fiscalía tomar acciones que permitan identificar y judicializar a los responsables. (Le puede interesar: [Fallece preso político de las FARC que había sido nombrado gestor de paz](https://archivo.contagioradio.com/47066/))

Por último, instan a las organizaciones sociales, movimientos de Derechos humanos y a la sociedad en general a rechazar el acciones de los grupos paramilitares y sectores políticos que los amparan, asegurando que pese a los amedrantamientos continuaran generando acciones de memoria para fortalecer la unidad entre familiares de los integrantes de FARC y en pro de la construcción de paz con justicia social.

[Denuncia Pública2](https://www.scribd.com/document/360314153/Denuncia-Pu-blica2#from_embed "View Denuncia Pública2 on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_26659" class="scribd_iframe_embed" title="Denuncia Pública2" src="https://www.scribd.com/embeds/360314153/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-5irAElYY6MOLv4O5PsMW&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
