Title: Los retos para las mujeres de la instancia de género de la CSIVI
Date: 2017-07-28 13:12
Category: Mujer, Nacional
Tags: CSIVI, enfoque de género, feminismo, mujeres, Mujeres afro
Slug: los-retos-las-mujeres-la-instancia-genero-la-csivi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/CSIVI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Twitter Alirio Muñoz 

###### 28 Jul. 2017 

Se conocieron los nombres de las lideresas que harán parte de la instancia de alto nivel de género de la Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Final –CSIVI- quienes velarán por el cumplimiento del enfoque de género en los acuerdos de paz. **Mujeres que fueron escogidas luego de un amplio proceso de selección** en el de manera rigurosa se seleccionaron los perfiles más idóneos.

**Victoria Sandino integrante de la guerrilla de las FARC** y quien hizo parte de la construcción del enfoque de género de los Acuerdos, manifestó que esa instalación fue muy emotiva e interesante en la que las mujeres de diversos territorios llegaron para comprometerse con la paz de Colombia.

“Estas mujeres tienen una hoja de vida intachable, **son muy reconocidas en el movimiento social de mujeres, en las organizaciones sociales** y en sus territorios fundamentalmente porqué ese es un componente fuerte. Estamos hablando de 4 mujeres del territorio, una que es víctima”.

### **¿Cuáles son los retos de estas mujeres?** 

Son innumerables los retos a los que se enfrentan las mujeres integrantes de esta instancia, en un primer momento deberán conocer el estado en el que se encuentra la implementación del Acuerdo, para que de esa manera puedan realizar el seguimiento a lo que pasa con el “Fast Track”, la participación política, la ley de reforma rural integral, entre otros y como las mujeres están allí.

En palabras de Sandino “**las mujeres tendrán que estar haciendo lobby, también tendrán que estar haciendo seguimiento a la parte legislativa**, pero también en lo que tienen que ver con la constitución del plan marco que estaremos próximamente en debate y sobre todo el seguimiento en los territorios para que las mujeres estén vinculadas a todos los espacios de discusión, participación y toma de decisiones para todos los programas de la implementación”.

### **¿Cómo fue el proceso de elección?** 

Para poder escoger a estas lideresas se establecieron unos criterios de participación para que fueran presentadas las mujeres a nivel territorial. **Fueron más de 800 organizaciones, por lo cual fue necesario realizar 32 asambleas departamentales** para postular las candidaturas a través de consenso de las organizaciones y votaciones.

En este proceso **estuvieron presentes como garantes la Procuraduría, así como ONU Mujeres** que son parte del mecanismo de acompañamiento internacional y el Servicio Nacional de Aprendizaje – SENA -. Le puede interesar: [Las mujeres tejen paz desde su territorio.](https://archivo.contagioradio.com/las-mujeres-tejen-paz-desde-los-territorios/)

Por cada región se realizó la selección de 3 personas y de esas posibles se buscaba que una fuera víctima. De todas esas mujeres se realizó la revisión de las hojas de vida, de sus trayectorias y las de sus organizaciones, de modo que alcanzaran un porcentaje estipulado para estar dentro de las seleccionadas.

### **Las mujeres afro pueden estar tranquilas, las representaremos bien** 

Luego del comunicado entregado por varias organizaciones de mujeres afrodescendientes en el que aseguraban se sentían excluidas de la CSIVI y que las mujeres seleccionadas no las representaban, **Sandino aclara que todo el proceso fue transparente** y que las mujeres pueden estar tranquilas porque incluirán sus voces en las decisiones que sean tomadas.

“Las mujeres afro participaron en los procesos territoriales y nacionales, había mujeres muy reconocidas pero la puntuación no alcanzó. **Las mujeres que hicieron el comunicado no ganaron en el Cauca e impugnaron la elección,** pero analizamos desde la CSIVI y con el informe de la Procuraduría que no había razón de la impugnación”.

Por lo que manifiesta que, si bien las mujeres afro tienen todo el derecho de expresar sus sentimientos “no tienen razón de lo que expresan en este comunicado que han dado a conocer, no está fundado sobre realidades”. Le puede interesar: [Mujeres afro se sienten excluidas de la instancia de género de la CSIVI](https://archivo.contagioradio.com/mujeres-afro-se-sienten-excluidas-de-la-instancia-de-alto-nivel-de-genero-de-la-csivi/)

Y recalcó que **las 7 mujeres seleccionadas están allí a nombre de todas las mujeres colombianas** porque van a recoger todo el acuerdo y el enfoque tal cual como se logró en La Habana entre el Gobierno y las FARC.

“Vamos a lograr este acuerdo desde la diversidad, con las mujeres indígenas, afros, desde los territorios, las campesinas y con todas las que hemos sentido de manera directa los efectos de este conflicto, entonces vamos a estar allí, a escuchar sus voces y cuenten con las mujeres de las FARC”.

<iframe id="audio_20050009" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20050009_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
