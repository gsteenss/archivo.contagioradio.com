Title: Líder indígena Adalberto Espitia completa 19 días desaparecido en Bajo Cauca Antioqueño
Date: 2015-10-22 14:14
Category: DDHH, Nacional
Tags: 29 desaparecidos en Zaragoza, Adalberto Antonio Espitia Espitia, Asamblea zonal de autoridades, Bajo Cauca, ONIC, Organización Indígena de Antioqueño
Slug: lider-indigena-adalberto-espitia-completa-19-dias-desaparecido-en-bajo-cauca-antioqueno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/adalberto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elcolombiano 

<iframe src="http://www.ivoox.com/player_ek_9131718_2_1.html?data=mpagk5yVfI6ZmKiak5eJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkIa3lIqupsnJtozdz8mSpZiJhaXbxtPOjabIpc3Wxtfh0ZCpt9Hd1c7OjcjTsdHgxtnOjZadb8WZpJiSo6nFt4zYxpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Aída Suárez Santos, Org. Indígena Antioquía] 

###### [22 oct 2015] 

De **2006 a 2012 han desaparecido en el Bajo Cauca Antioqueño 29 indígenas, de los cuales 26 han desaparecido en Zaragoza**, Antioquía. El último caso registrado es el del líder indígena y Cacique **Adalberto Antonio Espitia Espitia**, perteneciente al pueblo Senú de Antioquia quien completa **19 días desaparecido,** visto por última vez en la Asamblea Zonal de Autoridades Indígenas.

Según indican algunos participantes de esta asamblea de derechos humanos, realizada en conjunto entre la Organización Indígena de Antioquia y la Defensoría del Pueblo Regional, el Cacique **participó de manera normal y no manifestó tener algún tipo de amenazas** que pusieran en riesgo su integridad física.

Aída Suárez Santos, presidenta de la Organización indígena de Antioquía, indica que esta **zona presenta una “situación tensa de conflicto”**, que se ha puesto en conocimiento de distintas organizaciones gubernamentales y de derechos humanos.

Según Suárez, en el Bajo Cauca **“confluyen distintos grupos armados”** como la guerrilla del ELN y paramilitares, que tienen intereses mineros, de dominio territorial, entre otros, “van generando una serie de acciones que influyen, repercuten y **afectan en el proceso organizativo de la zona**” como la “educación, organización y proyectos que se ejecutan en la zona”.

La persistencia de las actividades mineras y el hecho de que la región del Bajo Cauca Antioqueño es un corredor estratégico tanto militar como para intereses económicos agrava la situación de las comunidades indígenas que habitan el territorio y que luchan por ejercer su derecho a la soberanía sobre sus resguardos.
