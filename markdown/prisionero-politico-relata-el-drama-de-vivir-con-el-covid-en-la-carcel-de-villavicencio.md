Title: Prisionero político relata el drama de vivir con el COVID en la cárcel de Villavicencio
Date: 2020-04-20 18:51
Author: CtgAdm
Category: Actualidad, DDHH
Tags: carcel, covid19, INPEC, prisioneros, Villavicencio, virus
Slug: prisionero-politico-relata-el-drama-de-vivir-con-el-covid-en-la-carcel-de-villavicencio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/carcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El Movimiento carcelario del centro penitenciario de Villavicencio denuncia que solo se han hecho 10 pruebas de Covid 19 entre la población reclusa, a pesar de tener más de 31 posibles contagios. Además las personas están siendo aisladas en la misma cárcel **sin el equipo suficiente para detener los contagios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con José, prisionero político que está en este centro de reclusión, las personas que se encontraban en la celda donde inició el brote fueron trasladadas a otra parte de la cárcel. Sin embargo, afirma que por cada celda hay aproximadamente **60 personas, lo que implica que el riesgo de contagio es máximo.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pretenden aislar el COVID 19 en un patio con 483 personas

<!-- /wp:heading -->

<!-- wp:paragraph -->

El patio en el que queda ubicada esa celda tendría cerca de **4**83 **personas,** que no pueden guardar la distancia sugerida para evitar la transmisión del virus por los altos niveles de hacinamiento, pues la cárcel tiene una población total de **1.600 personas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Motivo por el cuál están solicitando de forma urgente que se realicen más pruebas del virus Covid 19, ya que hasta el momento, s**olo se han realizado 10,** que no permitirían dimensionar el estado actual de la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este lugar ya fallecieron tres reclusos que dieron positivo a la prueba. Igualmente se tiene confirmación del contagio de 6 guardias del INPEC y dos personas que servían alimentos en el comedor de este penal. (Le puede interesar: [Emergencia por COVID llegó al INPEC y tampoco hay protocolo](https://archivo.contagioradio.com/emergencia-por-covid-llego-al-inpec-y-tampoco-hay-protocolo/)")

<!-- /wp:paragraph -->

<!-- wp:heading -->

No hay atención médica
----------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tanto la guardia del INPEC como los reclusos aseveran que solo cuentan con una jefe de enfermera durante la mañana para atender a las personas. Es decir que por la noche "las personas corren el riesgo de morir por falta de atención médica".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, expresan que las personas enfermas solo están siendo tratadas con acetaminofen. Razón por la cuál están exigiendo brigadas médicas de 24 horas y medicamentos para tratar el virus. (Le puede interesar: ["Decreto 546 de 2020 de excarcelación para evitar contagio de covid-19 en cárceles"](https://www.justiciaypazcolombia.com/decreto-546-de-2020-de-excarcelacion-para-evitar-contagio-de-covid-19-en-carceles/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

"El presidente nos condenó a pena de muerte"
--------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Asimismo señala que tras la firma del decreto 546 que buscaba descongestionar las cárceles, estos lugares siguen con hacinamiento. Ello debido a que esta normativa solo contempló penas alternativas para **el 5% de la población reclusa.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"En medio de está pandemia estamos pidiendo que se saque otro decreto en el que no se discrimine sino que se tenga en cuenta el derecho a la igualdad" asegura José. (Le puede interesar: "[Decreto presidencial no resuelve hacinamiento ni emergencia por COVID](https://archivo.contagioradio.com/decreto-presidencial-no-resuelve-hacinamiento-ni-emergencia-por-covid/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además advierte que, hasta el momento, en la cárcel de Villavicencio solo han salido 4 personas producto del decreto. Mientras que los patios siguen sin tener medidas de aislamiento seguro o elementos como tapabocas, agua y jabón.

<!-- /wp:paragraph -->
