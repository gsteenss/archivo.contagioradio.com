Title: Red pública de salud del Cauca está lista para recibir a maestros: Asoinca
Date: 2017-01-26 18:17
Category: Educación, Nacional
Slug: asionca-vuelve-a-paro-porque-gobierno-sigue-incumpliendo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/asoinca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASOINCA] 

###### [26 Ene 2016]

Desde esta semana el gremio de profesoras y profesores del Cauca asociados en ASOINCA volvieron a declarar paro indefinido puesto que el gobierno sigue incumpliendo los acuerdos alcanzados tras dos ceses de actividades de 2016. En ellos el gobierno se comprometió a **entregar la administración de los recursos de la salud del magisterio en ese departamento a la unión temporal** creada por ellos pero hasta el momento no han sido girados los recursos.

Miguel Burbano, vicepresidente de ASOINCA aseguró que no se entiende por qué en agosto de 2016 fue tan fácil que el vice ministro de educación se comprometiera con la entrega de los recursos para que los maestros lo administraran y ahora menciona una serie de trabas, "los recursos de la salud pueden devolverse a la red pública y para ello está lista la unión temporal. ([Le puede interesar: Maestros del cauca marchan hacia Bogotá](https://archivo.contagioradio.com/1800-docentes-del-cauca-viajan-a-bogota-para-continuar-paro-indefinido/))

Burbano también resaltó que **Fiduprevisora ya corroboró que el sistema propuesto por los profesores funcionaría y sería efectivo**, además está lista la red pública del departamento para llevar a cabo tanto el contrato como la atención en salud del gremio magisterial.

Según Burbano en una reunión que se debe realizar este jueves, tendrá que definirse la permanencia del paro o su suspensión que estará supeditada a la **definición de un plazo fijo para el desembolso de los recursos y así facilitar la contratación de los servicios** para los profesores a través de la red pública del departamento.

El jefe sindical también señala que en esta situación hay un parte de **responsabilidad del ministerio de salud, del ministerio de educación y de las directivas de FECODE,** que deberían garantizar la gestión de los acuerdos con el gobierno nacional en el nivel central, puesto que esta fue una de las exigencias y acuerdos en el paro nacional de profesores que se registró a mediados de 2016.

###### Reciba toda la información de Contagio Radio en [[su correo]
