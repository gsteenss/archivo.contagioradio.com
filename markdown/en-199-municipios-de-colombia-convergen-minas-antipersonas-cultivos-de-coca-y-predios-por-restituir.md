Title: Para 2021 territorio colombiano estaría libre de minas
Date: 2016-04-29 17:01
Category: Nacional, Paz
Tags: desminado, El Orejón, general rafael colon
Slug: en-199-municipios-de-colombia-convergen-minas-antipersonas-cultivos-de-coca-y-predios-por-restituir
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/minas-colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Séptima División ] 

###### [29 Abril 2016 ]

Son 199 los municipios de Colombia que presentan alta afectación por minas antipersonas, cultivos de coca y predios por restituir, afirma el General Rafael Colón, delegado por el Gobierno para el desminado humanitario en El Orejón que ya entra en su segunda fase, y que **se espera sea completamente descontaminado en mayo de este año**.

De acuerdo con el General, tras la firma del acuerdo de paz en La Habana, se espera que el equipo coordinado por la ONG 'Ayuda Popular Noruega' e integrado por miembros de las Fuerzas Militares, las FARC-EP y el Gobierno, entre a **20 municipios en los que el conflicto ha sido intenso**, entre ellos La Uribe, Vista Hermosa e Ituango, para iniciar procesos de desminado humanitario.

Teniendo en cuenta que en estos municipios también hay cultivos ilícitos y solicitudes de restitución de tierras, Colón insiste en que **todas las políticas públicas se deben articular para acompañar el proceso de desminado humanitario** a fin de que en 2021 todos los 199 territorios estén descontaminados.

El desminado humanitario en El Orejón, vereda del municipio de Briceño en la que viven 85 campesinos, se pactó en marzo del año pasado en el marco de los Diálogos de Paz y ha contado con la participación de las comunidades con quienes se han construido mapas en los que se señalan **cuatro zonas de peligro por presencia de minas antipersonas**, dos de las cuales restan por ser descontaminadas.

En esta vereda la presión paramilitar ha amenazado la eficacia del proceso, los pobladores han denunciado **persecución y hostigamiento contra quienes han cooperado**, y de acuerdo con el General se han presentado incidentes por disputas de control territorial, en este sentido le corresponde a MinDefensa trabajar para fortalecer las condiciones de seguridad que requiere el desminado.

Colón concluye asegurando que el plan piloto en [[El Orejón](https://archivo.contagioradio.com/?s=el+orejon+)] fue un mecanismo adoptado para **desescalar el conflicto y para demostrar cómo los actores antagónicos pueden trabajar en equipo**, aprendizajes que se aplicarán los 199 municipios dónde se identificaron minas antipersonas, "el desafío que se viene ahora es multiplicar el esfuerzo por 500 veces".

<iframe src="http://co.ivoox.com/es/player_ej_11382649_2_1.html?data=kpagmpeaeJqhhpywj5abaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncajZz8rfw9GPlsLawsrZjajTsIa3lIqvldORaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
