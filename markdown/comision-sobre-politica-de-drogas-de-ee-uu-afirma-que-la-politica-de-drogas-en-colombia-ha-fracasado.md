Title: Política de drogas en Colombia ha fracasado: Comisión del congreso de EE.UU.
Date: 2020-12-02 12:00
Author: AdminContagio
Category: Actualidad, Paz
Tags: Erradicación con glifosato, Estados Unidos, Glifosato en Colombia, Iván Duque
Slug: comision-sobre-politica-de-drogas-de-ee-uu-afirma-que-la-politica-de-drogas-en-colombia-ha-fracasado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-02-at-10.20.10-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Mientras en Colombia comunidades y organizaciones han rechazado las políticas antidrogas del Gobierno de Duque especialmente frente a la reactivación de las aspersiones con glifosato, la **Comisión sobre Política de Drogas del Congreso de los Estados Unidos publicó el informe que analiza** a profundidad la ejecución de este plan en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Algunas de las conclusiones del informe dan cuenta de la muy baja efectividad del Plan Colombia, la baja efectividad de la erradicación forzada y la fumigación y hace recomendaciones sobre la necesidad de priorizar la confiscación de insumos químicos para combatir el procesamiento del alcaloide.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente señala la comisión del congreso que usar tropas para erradicar pequeñas áreas de coca es un gasto perdido y que una estrategia que podría ayudar a resolver el problema es el desarrollo territorial a través de los PDET´s o zonas priorizadas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Informe del congreso de EEUU es un revés para el gobierno de Duque

<!-- /wp:heading -->

<!-- wp:paragraph -->

Como es sabido el gobierno de Iván Duque ha querido priorizar la erradicación forzada de cultivos de usos ilícito por la vía de la fumigación y por la acción directa de las FFMM. Ahora, con este informe del congreso de los EEUU, tendría que evaluar estas acciones y comenzar a implementar puntos centrales del acuerdo de paz como la restitución o la asignación de tierras, así como los planes de desarrollo territorial.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1334139546489991169","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1334139546489991169

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**El informe compuesto por 117 páginas será entregado este 3 de diciembre y ha recopilado el trabajo de funcionarios del gobierno estadounidense, diplomáticos de todo el mundo** y expertos en problemas y soluciones frente al control de drogas y el papel que juega Estados Unidos en Latinoamérica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El documento de la Comisión sobre Política de Drogas, recoge el trabajo investigativo de los últimos 18 meses en materia de  políticas antidrogas en Colombia, México y Centroamérica, sin desconocer en el caso de las 16 hojas dedicadas a Colombia como los últimos 20 años los Gobierno han sorteado este tema. ([Desconocimiento e incumplimiento del Gobierno con la sustitución de cultivos de uso ilícito](https://archivo.contagioradio.com/coccam-exige-garantias-para-las-familias-que-trabajan-por-la-sustitucion-de-cultivos/))

<!-- /wp:paragraph -->

<!-- wp:image {"id":93556,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-02-at-10.20.10-AM-edited.jpeg){.wp-image-93556}

</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

**Según el informe en los últimos años los cultivos de uso ilícito en Colombia han aumentado y que pese a las décadas de esfuerzos apoyados por Estados Unidos para erradicar los cultivos**, *"La cantidad de coca cultivada alcanzó un récord de 212.000 hectáreas en 2019, incluso cuando el país intensificó sus esfuerzos, erradicando más de 100 hectáreas”.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Destaca los rubros entregados a Colombia para este fin, los cuales suman un valor de **11.600 millones de dólares desde el 2000, de los cuales 10.000 millones fueron destinados al Plan Colombia** siendo este *“el programa de ayuda bilateral más grande y de mayor duración en este hemisferio”*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a esto, **afirman que el trabajo no se basa solo con romper la cadena de narcotráfico, implica también desarrollo social,** que va de la mano con alternativas de vida para las comunidades, la disminución del conflicto interno, y a la atención integral a las comunidades víctimas del narcotráfico. (["Con políticas del uribismo seguiremos entregando los territorios a la ilegalidad"](https://archivo.contagioradio.com/con-politicas-del-uribismo-seguiremos-entregando-los-territorios-a-la-ilegalidad/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Colombia no puede asegurar la paz y controlar el tráfico sin abordar simultáneamente la ausencia de seguridad y desarrollo en vastas regiones del país"*
>
> <cite>**Report Of The Western Hemisphere Drug Policy Commission**  
> </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La Comisión sobre Política de Drogas da cuenta de esto indicando que, incluso **algunos de los programas sociales para la sustitución de los cultivos de uso ilícito en el país encabezados por la La Agencia de los Estados Unidos para el Desarrollo Internacional - [Usaid](https://www.usaid.gov/es/colombia)- , no ha sido tan efectiva**, *"debido a la persistencia del conflicto armado con afectaciones a la población civil como desplazamientos forzados y masacres"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Violencia territorial que se ve reflejada **en [el mas reciente informe del Instituto de Estudio y Desarrollo para la Paz](http://www.indepaz.org.co/informe-de-masacres-en-colombia-durante-el-2020/)(Indepaz), en las 77 masacres en el 2020,** con corte al 22 de noviembre de 2020; y el asesinato de 251 lideres y lideresas sociales, muchos de ellos y ellas defensores del Acuerdo de Paz, y opositores de la erradicación forzada. ([«Cifras de asesinatos a líderes sociales son manipuladas por el Gobierno»](https://archivo.contagioradio.com/cifras-de-asesinatos-a-lideres-sociales-son-manipuladas-por-el-gobierno/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El objetivo del informe de la Comisión sobre Política de Drogas es formular estrategias que prioricen a las comunidades

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Comisión hace diferentes recomendaciones generalizadas a los países latinoamericanos mencionados en el informe, y esto suma cerca de cinco recomendaciones tipificadas por país, en el caso de Colombia **una de las recomendaciones el desarrollar programas de asistencia policial y antinarcóticos basados en acuerdos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un programa que según el informe debe contar con un énfasis en el fortalecimiento de las instituciones que da como resultado la reducción de las tasas de impunidad y aumentar las de la confianza en el estado.[Comunidades siguen clamando por un acuerdo humanitario](https://archivo.contagioradio.com/comunidades-reiteran-necesidad-de-un-acuerdo-humanitario/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado indican que fuerzas armadas como la Policía Nacional, *"ha tenido problemas para encontrar y capacitar nuevos reclutas, especialmente en las zonas rurales*", esto a pesar de que dependen del Ministerio de Defensa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y enfatizan en que cualquier **esfuerzo de erradicación,** ***" ya sean aéreos o manuales, deben centrarse en los productores de coca** **a gran escala, (..) y consulta con los líderes locales, para evitar que los traficantes regresen a esas áreas"***. ([ESMAD y Policía antinarcóticos realiza erradicación forzada en Resguardo Nasa](https://archivo.contagioradio.com/esmad-y-policia-antinarcoticos-realiza-erradicacion-forzada-en-resguardo-nasa/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Enviar trabajadores y fuerzas de seguridad áreas remotas para eliminar pequeñas parcelas de coca es un esfuerzo inútil y, en última instancia, inútil”*
>
> <cite>**Report Of The Western Hemisphere Drug Policy Commission**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El documento a travez de las recomendaciones pretende también que Estados Unidos facilite la asistencia a los desmovilizados en las zonas de posconflicto con programas de desmovilización y reintegración en estas regiones. ([Piden a ONU frenar la “embestida” del Gobierno contra el Acuerdo de Paz](https://archivo.contagioradio.com/piden-a-la-onu-parar-la-embestida-del-gobierno-y-su-partido-contra-el-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al tiempo que vincula de designación de tierras y la construcción de carreteras como una forma popular y muy visible de demostrar el progreso, además de *"proporciona empleo inmediato mientras se afianzan los esfuerzos a más largo plazo"*, y brindar los mecanismos para que el campesinado pueda sacar sus productos, recibir atención medica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lista de **recomendación y acciones que llegarán al Gobierno este 3 de diciembre justo luego de que el ministro de Defensa, Carlos Holmes Trujillo señalara que van a *"erradicar 130.000 hectáreas de cultivos ilícitos*** *(en este 2020), que es la cifra más alta de la historia y ya tenemos el número de toneladas de incautación más alta de la historia”*, y cuando las comunidades claman por qué no retorne el glifosato a su territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al tiempo que se reabre el debate del regreso de la aspersión aérea con glifosato en 104 municipios de Colombia, como una estrategia supuestamente efectiva en las disminución de los cultivos de uso ilícito para el Gobierno, pero como una sentencia de dolor, enfermedades y atropello a los Derechos de las comunidades. ([Aspersión aérea con glifosato, una estrategia fallida que se repite](https://archivo.contagioradio.com/la-aspersion-aerea-con-glifosato-no-es-una-estrategia-efectiva/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello este miércoles, comunidades, organizaciones y procesos comunitarios ante el desacatado nuevamente al fallo que frenó las audiencias para el reinicio de las aspersiones aéreas con glifosato en municipios como, **Cáceres, Antioquia; Jacinto, en Cauca; Barrancabermeja, Santander,** entre otros exigen la participación en estos debates y dar cuenta de los graves impactos de este un herbicida de amplio espectro.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Conozca el informe Completo :*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://drive.google.com/file/d/1vRIoz7xX7WH7wctk-zHak9U3E6hw-o3B/preview" width="640" height="480"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
