Title: yo reporto
Date: 2019-01-27 18:49
Author: AdminContagio
Slug: yo-reporto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/yo-reporto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

\[text\* your-name placeholder"Nombre\*"\]

\[email\* your-email placeholder"e-mail\*"\]

\[textarea your-message placeholder "Reporte o Noticia"\]

Subir archivo  
\[file file-931\]

Al enviar estoy aceptando que estoy de acuerdo con los [Términos de uso](https://archivo.contagioradio.com/terminos)

\[submit "Enviar"\]

1  
Contagio Radio - Yo reporto "\[your-subject\]"  
Contagio Radio <contagioradio@contagioradio.com>  
contagioradio@contagioradio.com  
De: \[your-name\] &lt;\[your-email\]&gt;  
Asunto: \[your-subject\]

Cuerpo del mensaje:  
\[your-message\]

\[acceptance acceptance-115\]

--  
Este e-mail se ha enviado vía formulario de YO REPORTO desde Contagio Radio (https://www.contagioradio.com)  
Reply-To: \[your-email\]  
\[file-931\]

Contagio Radio "\[your-subject\]"  
Contagio Radio <wordpress@contagioradio.com>  
\[your-email\]  
Cuerpo del mensaje:  
\[your-message\]

--  
Este mensaje se ha enviado desde un formulario de contacto en Contagio Radio (http://contagioradio.com)  
Reply-To: diana@decemedialab.com

Gracias por tu mensaje. Ha sido enviado.  
Hubo un error intentando enviar tu mensaje. Por favor inténtalo de nuevo más tarde.  
Uno o más campos tienen un error. Por favor revisa e inténtalo de nuevo.  
Hubo un error intentando enviar tu mensaje. Por favor inténtalo de nuevo más tarde.  
Debes aceptar los términos y condiciones antes de enviar tu mensaje.  
El campo es obligatorio.  
El campo es demasiado largo.  
El campo es demasiado corto.  
El formato de fecha es incorrecto.  
La fecha es anterior a la más temprana permitida.  
La fecha es posterior a la más tardía permitida.  
Hubo un error desconocido subiendo el archivo.  
No tienes permisos para subir archivos de este tipo.  
El archivo es demasiado grande.  
Se ha producido un error subiendo la imagen  
El formato de número no es válido.  
El número es menor que el mínimo permitido.  
El número es mayor que el máximo permitido.  
La respuesta al cuestionario no es correcta.  
El código introducido es incorrecto.  
La dirección de correo electrónico que has introducido no es válida.  
La URL no es válida.  
El número de teléfono no es válido.
