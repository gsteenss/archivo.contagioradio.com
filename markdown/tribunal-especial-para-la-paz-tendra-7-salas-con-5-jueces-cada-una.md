Title: Tribunal especial para la paz tendrá 7 salas con 5 jueces cada una
Date: 2015-10-01 11:20
Category: Nacional, Paz
Tags: Conversaciones de paz de la habana, ELN, enrique santiago, FARC, Iván Márquez, Juan Manuel Santos, jurisdicción especial para la paz, Timoleon Jimenez
Slug: tribunal-especial-para-la-paz-tendra-7-salas-con-5-jueces-cada-una
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/delegación_paz_farc_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: rcn 

###### [01 Oct 2015]

El jefe de la delegación de paz de la Guerrilla de las FARC, Iván Márquez afirmó a través de su cuenta en Twitter que el acuerdo sobre la Jurisdicción Especial para la Paz tiene 75 puntos y un catálogo de sanciones restaurativas y agregó que **tomar el comunicado leído en la Habana el pasado miércoles como el acuerdo es defraudar a la opinión pública y generar desconfianza.**

[![ensanro\_Twitter\_contagioradio (2)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ensanro_Twitter_contagioradio-2.jpg){.aligncenter .wp-image-15057 width="406" height="252"}](https://archivo.contagioradio.com/tribunal-especial-para-la-paz-tendra-7-salas-con-5-jueces-cada-una/ensanro_twitter_contagioradio-2/)

Respecto del acuerdo, el abogado Enrique Santiago, asesor externo para la elaboración del Sistema Integral de Verdad, Justicia, Reparación y No Repteción, afirmó que es “*Sorprendente que el Gobierno pretenda sustituir el Acuerdo "Jurisdicción Especial para la Paz" por el "Comunicado" sobre el mismo*”.

[![ensanro\_Twitter\_contagioradio (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ensanro_Twitter_contagioradio-1.jpg){.aligncenter .wp-image-15058 width="396" height="136"}](https://archivo.contagioradio.com/tribunal-especial-para-la-paz-tendra-7-salas-con-5-jueces-cada-una/ensanro_twitter_contagioradio-1/)

Días atrás el mismo abogado reveló en Contagio Radio que el acuerdo sobre JEP contará con un tribunal independiente conformado por **7 salas que a su vez estarán integradas por 5 magistrados cada una** y que los organismos de acusación como la fiscalía, la Comisión de Acusaciones de la Cámara y organizaciones de DDHH y de víctimas tendrán que presentar ante ese tribunal los hechos materia de investigación por crímenes cometidos en el marco del conflicto armado, lo cual **incluye ex presidentes. Ver** [Acabar con la impunidad es perseguir, investigar y sancionar](https://archivo.contagioradio.com/acabar-con-la-impunidad-es-investigar-es-perseguir-y-es-sancionar/)

Por su parte Timoleón Jiménez, comandante de la guerrilla de las FARC aseguró que dio la orden a todos los integrantes de esa organización la suspensión de los cursos militares y dedicar sus esfuerzos a la formación política y cultural. Luego del anuncio **Pastor Alape, integrante de la delegación de paz afirmó que la orden no es para sorprenderse porque “viene la paz”.**

[![timoleon](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/timoleon.jpg){.aligncenter .wp-image-15063 width="479" height="163"}](https://archivo.contagioradio.com/tribunal-especial-para-la-paz-tendra-7-salas-con-5-jueces-cada-una/timoleon/)
