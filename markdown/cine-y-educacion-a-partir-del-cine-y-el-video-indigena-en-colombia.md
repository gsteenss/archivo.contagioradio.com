Title: Cine y educación a partir del cine y el video indígena en Colombia
Date: 2015-09-10 12:14
Category: Cultura, eventos
Tags: Angélica María Mateus Mora, Cine y educación a partir del cine y el video indígena en Colombia, Universidad Nacional de Colombia
Slug: cine-y-educacion-a-partir-del-cine-y-el-video-indigena-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/5690049w.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo EFE 

###### [10 Sept 2015​]

La tarde de este jueves la Universidad Nacional de Colombia acogerá la Conferencia "Cine y educación a partir del cine y el video indígena en Colombia", impartida por la Doctora Angélica María Mateus Mora, de la Universidad François-Rabelais de Tours.

El evento académico, organizado por el Instituto de Investigación en Educación y la Maestría en Educación, Linea Lenguajes y Literaturas de la U.N., se desarrollar a partir del panorama de la historia de la representación cinematográfica y audiovisual del mundo indígena en Colombia, plasmado en el trabajo investigativo de la conferencista.

**Fecha:** Septiembre 10

**Hora:** 2:00 p.m.

**Lugar:** Salón 3043 del Edificio Manuel Ancizar.

**Sobre la conferencista:**

Angélica María Mateus Mora es doctora en estudios cinematográficos y audiovisuales por la Universidad Sorbonne Nouvelle – Paris 3. Desde el año 2012 es miembro del laboratorio ICD (Interactions Culturelles et Discursives) de la Universidad François-Rabelais de Tours, donde es Maître de Conférence del departamento de español.

Autora del libro "*L'Indien dans le cinéma et l'audiovisuel latino-américains : images et conflits"*, publicado por la editorial l’Harmattan, en la colección "*Champs Visuels*" en  el año 2012 en París. Un año más tarde este libro fue traducido al español en la editorial La Carreta Editores, dando origen a la colección "*La Carreta Cinematográfica*" de Medellín.

Ha publicado en diversas revistas entre ellas: *L’Ordinaire Latino-américain*, *Cinéma & Cie. International Film Studies Journal, Revista CUHSO Cultura-Hombre-Sociedad* y *CinémAction.*
