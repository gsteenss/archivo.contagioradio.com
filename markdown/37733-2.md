Title: Suspendida licencia ambiental para Torres de Energía en Cundinamarca y Boyacá
Date: 2017-03-14 16:56
Category: Ambiente, Nacional
Tags: Corredores Torres de Energía, Empresa de Energía de Bogotá, Tabio
Slug: 37733-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/torres-energía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Extrategia] 

###### [14 Mar 2017] 

La intriga en la que se encontraban las comunidades de Cundinamarca y Boyacá, por la posible construcción de un tendido de 260 torres de energía que pondría en riesgo reservas forestales, ocho cuerpos de agua, predios de actividad agrícola y pecuaria, llegó a su fin. La ANLA **a través del auto 00279 del 10 de febrero de 2017, suspendió la licencia otorgada a la Empresa de Energía de Bogotá** –EEB– para el proyecto “Construcción de la línea de transmisión CHIVOR-CHIVOR II – NORTE – BACATÁ A 230 KV”.

Aunque integrantes de las comunidades expresaron que serían tres las veredas directamente afectadas, Río Frío occidental, Llano Grande y Salitre, los ecosistemas se interconectan entre sí afectándose importantes corredores bióticos y franjas de conectividad. Desde 2014, **las comunidades vienen manifestando con plantones y movilizaciones en plazoletas principales, su rechazo al proyecto.**

El corredor de 7,5 kilómetros de longitud propuesto para el proyecto, y que la compañía considera es el más indicado para desarrollar **el proyecto intervendría 4 kilómetros que hacen parte de una importante reserva forestal de Sabana.**

Patricia de Bedout ambientalista y veedora del municipio de Tabio, señala que  “los campos electromagnéticos son altísimos. Lo que está ahí ya está, no hay por qué hacer modificaciones; ¿a cuento de qué quieren acabar con la fauna y la flora?”.

\[caption id="attachment\_37738" align="alignnone" width="940"\]![Corredores Torres de Energía](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Torres_Energía_Tabio.jpg){.size-full .wp-image-37738 width="940" height="494"} Corredores Torres de Energía - ANLA\[/caption\]

Por último, Alexander Fonseca líder comunitario, explica que algunos daños serían afectaciones a los acueductos que abastecen de agua el municipio, fragmentación de los bosques de la reserva forestal como incendios, cambios micro climáticos, erosión del suelo, efectos en la salud humana, transformación del paisaje, **electrocución de aves y el peligro para especies silvestres como Tigrillos, que están en vía de extinción.**

###### Reciba toda la información de Contagio Radio en [[su correo]
