Title: Cine con dulce sabor en Siembra Fest
Date: 2016-10-03 17:16
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, colombia, Pelicula Siembra, Perros, Siembra Fest, Siempre viva
Slug: cine-con-dulce-sabor-en-siembra-fest
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/14484630_874329846034840_5148582999866995180_n-e1475532555123.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Siembra Fest 

###### 3 Oct 2016 

<iframe id="audio_13167006" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13167006_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En su viaje por Cundinamarca, el festival de cine itinerante al campo **Siembra Fest**, **llega a Villeta para su cuarta edición**. Un recorrido que busca llevar a las zonas rurales de los 116 municipios del departamento, una oferta cultural desde el cual se reconoce la importancia del campo y los campesinos de Colombia.

Como lo viene haciendo desde 2012 en poblaciones como Anolaima y Guaduas, el Festival llegará con las **51 producciones nacionales que conforman la selección de películas** que buscarán llevarse alguno de los reconocimientos que entrega el evento que tendrá lugar entre el **4 y el 8 de octubre,** donde además se presentarán como películas invitadas** **Alias maria, El silencio del rio, 2 mujeres y una vaca y Tierra en la lengua.

**Alexandra Peña, Productora del Festival**, asegura que desde la organización de Siembra, se apuesta por la **desentralización**, teniendo en cuenta que en ciudades como Bogotá se construyen "muchos espacios culturales, artisticos y cinematográficos", lo que no ocurre en todas las zonas rurales de Cundinamarca, donde son escasos estos escenarios, particularmente aquellos que sirven para la exhibición de películas nacionales.

Lo que propone el Festival es **intervenir en espacios públicos y crear escenarios alternativos de exhibición** que sirvan para promover el encuentro del lenguaje cinematográfico con algunas manifestaciones del arte y la cultura popular surgidas en cada uno de los municipios, invitando a que las **comunidades participen activamente en la construcción de su propia imagen**.

Otro de los objetivos del Festival, es aportar a la formación de públicos "**para que la gente empiece a cualificar la mirada, empiece a entender de que se trata el cine colombiano**", y simultáneamente "armar una red con los realizadores, con los productores, para entender como la gente esta percibiendo las películas", afirma la productora.

Uno de los invitados principales a esta edición, será el director colombiano **Ruben Mendoza** (Tierra en la lengua; Memorias de calavero), con algunas de sus producciones en las cuales ha tenido una mirada especial para el campo, y los jurados **Pablo Mora, Camila Loboguerrero, Gustavo Corredor, Marta Rodríguez y Oscar Andrade.** Adicionalmente el director **Miguel Urrutia** dictará un taller de cine recursivo que se realizará en el marco del Festival.

El Festival de Cine Colombiano al Campo es organizado por la Fundación Dialekta, con el apoyo del Ministerio de cultura, Proimágenes, Gobernación de Cundinamarca, Alcaldía de Villeta, el SENA , ANAFE, Impulsos Films y la Cinemateca Distrital de Bogotá.

<iframe src="https://www.youtube.com/embed/VuGCQqJGPwA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
** **

**La selección de películas.**

Estas son las producciones en competencia de Siembra Fest 2016:

**Cortometraje ficción**

Forastero  
La jaula  
Elefante  
La justicia de Rubén  
La lluvia  
Muñeca tuerta  
Utopía  
La vía láctea

**Cortometraje documental**

Heridas  
La esperanza Kankuama  
Bocagrande  
Cartas a Leo Kopp  
Categoría V  
Cien años más viejo  
Resiliencia  
Las migajas del Ácimo  
Reverdecer  
Shamuy Shampachista  
Un susurro en el espacio  
Ciudad invisible

**Animación**  
En vuelo  
Mitú

**Largometraje ficción**  
Canción de Iguaque  
Perros  
Siembra  
Siempreviva

**Largo documental**  
Retornan  
Quintín Lame, raíz de pueblos  
Barbas Bremen, una multinacional papelera  
Chocolate de paz  
La trocha

**Concurso Nuevos Creadores**

Cortometraje ficción  
Barco de papel  
El escape  
El quimérico espectáculo de los Bizzanelli  
Elemento  
La fortaleza del silencio  
Mia  
Pescao'  
Tiza China  
Bunic  
Lo que esconde la tierra  
Volver

**Cortometraje documental**  
El morro  
We'pe Çe - Luz de páramo  
Romelia  
Respirar el agua  
Cenizas celestes  
Kilómetros  
Mama  
Uaia, hijos de la tierra  
Una canoa sin río

 
