Title: La fotografía, una herramienta de transformación socio ambiental en Cundinamarca
Date: 2018-09-21 12:46
Author: AdminContagio
Category: eventos
Tags: Ambiente, Cundinamarca, Fotografia
Slug: fotografia-transformacionl-cundinamarca
Status: published

###### Foto: Fundación Arts Collegium- Alexandra Prieto 

###### 21 Sep 2018 

Partiendo de su reflexión sobre el papel que tiene el arte y los artistas frente al conflicto en Colombia, **Claudia Ruíz, diseñadora industrial y pintora** radicada en Guasca, Cundinamarca, inició un proyecto en 2010 materializado en la **Fundación Arts Colegium**, que ha venido ampliando sus proyectos en ocho municipios del Departamento.

Utilizar la fotografía como una herramienta para la transformación, surgió inicialmente por inspiración de **Luis Benito Ramos, pionero del reportaje gráfico en Colombi**a, oriundo de ese municipio y uno de sus valores patrimoniales, con el tiempo se ha ido direccionando a temáticas relacionadas lo **ambiental y social**.

"Nos empezamos a dar cuenta que la fotografía atraía mucho a los chicos, y para nosotros siempre ha sido un pretexto tomar esos espacios con un público cautivo para hablarles de muchas cosas, para que piensen y reflexionen sobre lo que son y su relación con su entorno, el lugar donde viven, pero sobre todo mirarse hacia adentro" asegura Ruíz.

**Mujeres de Guatavita cuentan su proceso de empoderamiento en fotografías**

En Guatavita, la Fundación viene trabajando con la **Asociación de Mujeres emprendedoras de Guatavita AMEG**, un grupo de 25 mujeres de la vereda Carbonera Alta "lo que hicimos fue abrir un espacio porque ellas querían contar su historia a través de la fotografía" trabajos que serán presentados **este sábado 22 de Septiembre en una exposición pública en la Plaza Central del municipio**, en el marco del Festival de El dorado.

<iframe src="https://www.youtube.com/embed/LfQrSQFTXHQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Ruíz explica que las integrantes de AMEG son un grupo de mujeres "que se unieron para **transformar sus vidas, desde algo mucho más que la figura femenina que cumple con las funciones de madre, esposa y ama de cas**a" añadiendo que su iniciativa constituyó un "acto revolucionario" para la época en que se conformó, al involucrarse en temas como el manejo y consecución de recursos, limitados al género masculino.

La exposición incluye el trabajo realizado por las mismas mujeres de AMEG al tomar la cámara y registrar sus entornos y quehaceres cotidianos, así como el proyecto fotográfico inspirado en  la tradición de las mujeres del campo denominado **"Sabias montañeras"** realizado por dos jóvenes de Guasca, quienes también hicieron parte de los talleres.

**En Guasca, la naturaleza es la inspiración**

Inicialmente **en Guasca el proyecto se enfocó en la fotografía de naturaleza,** otro de sus valores patrimoniales, fruto del cual se elaboró el libro "[El ABC de la biodiversidad en Guasca](http://Inicialmente%20en%20Guasca%20el%20proyecto%20se%20enfocó%20en%20la%20fotografía%C2%A0de%20naturaleza,%20otro%20de%20sus%20valores%20patrimoniales,%20fruto%20del%20cual%20se%20elaboró%20el%20libro%20%22El%20ABC%20de%20la%20biodiversidad%20en%20Guasca%22%20a%20partir%20de%20las%20piezas%20capturadas%20por%20los%20jovenes%20a%20quienes%20se%20sumaron%C2%A0alguno%C2%A0de%20sus%20vecinos%20de%20Sopó.)" a partir de las piezas capturadas por los jóvenes a quienes se sumaron alguno de sus vecinos de Sopó.

En el marco de la celebración del mes del patrimonio, **este 30 de septiembre se realizará un exposición donde una serie de fotografías**, entre las que se encuentran algunas que hacen parte del libro, **se van a disponer en lugares por los que esta programado el trazado de la cicloruta que recorrerá los monumentos patrimoniales del municipio**.

**Los nuevos proyectos y retos de trabajo**

Tras establecer relaciones con la Alianza con Conservación Internacional Colombia, la Fundación Arts Colegium ha empezado a trabajar en el tema de las **medidas de adaptación al cambio climático en alta montañ**a, involucrando a Guasca, Guatavita y Sesquilé.

Según explica Ruíz, el trabajo de los jóvenes de la Fundación consistirá en **generar desde las salidas al campo material audiovisual, cartillas, para circular por las redes y medios,** que aporten a la elaboración de proyectos piloto en los municipios que hacen parte del proyecto y que constituyen un área importante de conservación en el Departamento.

 "Si algo nos ha pasado en estos años es que las salidas de campo para los chicos son super determinantes, **una cosa es lo que puedan contar del lugar en donde viven y otra cosa es que vayan y lo huelan, lo escuchen y lo vean, te mojes y te caigas**, es muy bello la verdad que es una experiencia muy linda" manifiesta la artista.

<iframe id="audio_28773567" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28773567_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
