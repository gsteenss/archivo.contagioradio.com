Title: "Águilas Negras tienen acceso a inteligencia militar" David Flórez
Date: 2015-03-12 17:18
Author: CtgAdm
Category: DDHH, Nacional
Tags: Aguilas Negras, Amenazas, Bombardeo, conflicto, David Florez, ejercito, Iván Cepeda, Paramilitar, paz, Sufragio
Slug: aguilas-negras-tienen-acceso-a-inteligencia-militar-david-florez
Status: published

###### Foto: Internet 

<iframe src="http://www.ivoox.com/player_ek_4200194_2_1.html?data=lZedkpadeI6ZmKiakpyJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmhqigh52Vq9bdzcbgjbPJq9PV1JDhy8rSqc%2BfwsjQx9jTb8KfytPhx9HNq8bixM7OjdLNsMrowteSlJePcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [David Flórez, vocero de Marcha Patriótica] 

El martes 9 de marzo el Bloque Capital y Bloque Sur de las **Aguilas Negras**, enviaron tres panfletos y sufragios con amenazas de muerte en contra de 19 líderes y lideresas políticos y sociales de Colombia, entre los que se encuentran **Iván Cepeda, Gloria Flórez y Gustavo Petro (entre otros), contra el colectivo de abogados José Alvear Restrepo**, y contra las comunidades indígenas que reclaman la "liberación de la madre tierra" en el departamento del Cauca.

Para una de las personas amenazadas por este grupo paramilitar, el abogado David Flórez, responsable de DDHH y Comunicaciones de la Marcha Patriótica y vocero nacional de esta organización, los sufragios son "*una retaliación por parte de los sectores militaristas que se oponen a los avances del proceso de paz*", y no es casual que se presenten el mismo día en que se da a conocer la decisión de cesar los bombardeos por parte del Ejercito sobre campamentos de la insurgencia. Esto daría cuenta, según **David Flórez,** de que estas amenazas provienen de inteligencia militar, o de grupos que tienen acceso a fuentes militares.

Por otra parte, señala que el anuncio sobre el **cese de bombardeos por parte del Ejercito** es "un paso muy importante, una muy buena noticia", pero aún insuficiente. "Nos parece que es necesario avanzar en más gestos: el cese unilateral que ha decretado la insurgencia, el acuerdo sobre des-minado, muestran una voluntad muy fuerte por parte de la guerrilla que no corresponde con el hecho de que el gobierno simplemente deje de bombardear", afirmó. Además, preocupa que en sus declaraciones, el presidente Juan Manuel Santos haga un llamado a arreciar la guerra contra el ELN, ya que "*para que la paz sea integral debe ser con todas las insurgencias y debe involucrar al conjunto de la sociedad*".

Para el vocero de Marcha Patriótica, es importante que se reconozca que los bombardeos no sólo afectan a la insurgencia sino al conjunto de la sociedad colombiana, y que las acciones ofensivas de tierra y agua por parte del Ejercito también han estado encaminadas a perseguir a la población rural de todo el país.

Señala que los **empadronamientos y registros continúan**, y que en sólo dos meses que van corridos del 2015, la Marcha Patriótica ha registrado **6 "ejecuciones extrajudiciales",** sin que el Ejercito ni el gobierno realicen las investigaciones y correctivos pertinentes. "Ha pesar de que se han documentado estos casos y se han hecho reuniones con la Fiscalía, al día de hoy no aparece ni un solo responsable".

En este sentido, el vocero indica que es necesario que cesen las hostilidades también contra los líderes políticos en Colombia.

Finalmente, afirma que las diferentes personas amenazadas por el **paramilitarismo** "nos mantenemos en nuestro ideal de transformar este país, nos mantenemos firmes en nuestro trabajo, en nuestras convicciones, en nuestros sueños, esto no nos va a detener, no nos amilanar."

 [![IMG-20150310-WA0006](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/IMG-20150310-WA0006.jpg){.aligncenter .wp-image-5806 width="549" height="731"}](https://archivo.contagioradio.com/ddhh/aguilas-negras-tienen-acceso-a-inteligencia-militar-david-florez/attachment/img-20150310-wa0006/) [![IMG-20150310-WA0004](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/IMG-20150310-WA0004.jpg){.aligncenter .wp-image-5805 width="546" height="728"}](https://archivo.contagioradio.com/ddhh/aguilas-negras-tienen-acceso-a-inteligencia-militar-david-florez/attachment/img-20150310-wa0004/)

\[caption id="attachment\_5804" align="aligncenter" width="540"\][![Aguilas Negras 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/IMG_20150310_124904.jpg){.wp-image-5804 width="540" height="721"}](https://archivo.contagioradio.com/ddhh/aguilas-negras-tienen-acceso-a-inteligencia-militar-david-florez/attachment/img_20150310_124904/) Amenaza-Aguilas-Negras-Contagio-Radio\[/caption\]
