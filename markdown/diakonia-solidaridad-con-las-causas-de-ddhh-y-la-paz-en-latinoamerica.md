Title: "Diakonia" solidaridad con las causas de DDHH y la paz en Latinoamérica
Date: 2015-09-15 16:40
Category: DDHH, Hablemos alguito
Tags: Cesar Grajales directo Diakonia Colombia, Diakonia, Premio Nacional a la Defensa de los Derechos HUmanos
Slug: diakonia-solidaridad-con-las-causas-de-ddhh-y-la-paz-en-latinoamerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/diako-e1442355448859.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_8324551_2_1.html?data=mZiflpqZdY6ZmKiakpaJd6Kkloqgo5eXcYarpJKfj4qbh46kjoqkpZKUcYarpJKxy9fJp9Xj05DRx5CorcLf0NPWw5Cns83jzsfWw5DJsoy8wsfZx9LTt4zVzcziy9nTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Fundada en 1966 como una iniciativa de solidaridad, cooperación y ayuda humanitaria desde la fé Cristiana impartida por un grupo de iglesias libres de Suecia, Diakonia, (del griego "Diakonos" que significa servicio), trabaja en 35 países del mundo con el respaldo de la cooperación oficial del Estado Sueco y aportes de diferentes comunidades del mundo.

Diakonia dirige su labor a la cooperación para el desarrollo, entendido como la superación de estructuras injustas que someten a las personas y comunidades a pobreza, exclusión y violencia, con valoración por la diversidad de raza, preferencia sexual y género desde una perspectiva de derechos humanos.

Durante los años 7o, la Fundación se ubicó muy cerca de los defensores de derechos durante las dictaduras en Sudamérica y en los conflictos armados centroamericanos, donde existen estructuras muy inequitativas de distribución de la riqueza, de apropiación de la tierra entre otras.

Paraguay, Perú, Bolivia Guatemala, Honduras, Nicaragua y Colombia, son los países latinoamericanos en los que Diakonia hace presencia con personal local y trabajadores suecos, y un programa especial en Cuba de menor tamaño.

La relación de Diakonia con Colombia, inicia a mediados de los años 70 y con personal de trabajo permanente desde el año 1997, apoyando trabajos de atención humanitaria, trabajos relacionados con la construcción de paz, defensa, promoción y protección de Derechos Humanos, justicia y reparación integral para las víctimas del conficto colombiano.

**Cesar Grajales**, Director de la Fundación en Colombia y  **Catalina Vasquez,** comunicadora social, compartieron en "Hablemos Alguito", algunos detalles del trabajo de Diakonia en Latinoamérica y en Colombia, así como de la entrega del [4to Premio Nacional a la defensa de los Derechos Humanos](https://archivo.contagioradio.com/estos-son-los-ganadores-del-4to-premio-nacional-a-la-defensa-de-los-derechos-humanos/), impulsada por la Fundación como reconocimiento a líderes y líderesas de ese trabajo en nuestro país.
