Title: "Semper solus" letras y sueños en "A la Calle"
Date: 2015-07-13 12:51
Category: Sonidos Urbanos
Slug: semper-solus-letras-y-suenos-en-a-la-calle
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/10485000_631357900302050_3002249821325390441_n.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### <iframe src="http://www.ivoox.com/player_ek_4720362_2_1.html?data=lZyfkpiado6ZmKial52Jd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bh0crfjdjTsNbnjIqfmtjNqc7k08qY1dTQpdSZk56ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Semper solus en: "A la Calle"] 

***\`\`Escribir para salvarse, Reír para no rendirse, Leer para explorar el mundo\`\`***

"Semper Solus" es un colectivo de escritoras  cotidianas,  entregadas a todo tipo de historias  
sin temor para expresarse en prosa y verso. Empezaron en agosto de 2014 con un blog  y ahora comparten sus experiencias por Facebook, donde publican escritos propios expresando sus ideas desde textos cargados de diferentes personajes inspirados en sus emociones y posturas frente a la realidad social y cultural. Una combinación de arte, literatura y cine, entre otras manifestaciones que hacen parte de  la imaginación humana.

Click para seguir el [Blog](http://yeswritergirls.blogspot.com/), [Facebook](https://www.facebook.com/theforeverwriters/timeline) y[Twitter](https://twitter.com/SemperSolus1) de "Semper Solus"
