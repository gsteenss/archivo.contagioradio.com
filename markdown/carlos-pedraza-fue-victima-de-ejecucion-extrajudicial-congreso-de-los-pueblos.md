Title: "Carlos Pedraza fue víctima de ejecución extrajudicial" Congreso de los Pueblos
Date: 2015-01-29 16:15
Author: CtgAdm
Category: DDHH, Movilización
Tags: Carlos Pedraza, congreso de los pueblos, ejeccucion extrajudicial
Slug: carlos-pedraza-fue-victima-de-ejecucion-extrajudicial-congreso-de-los-pueblos
Status: published

En rueda de prensa sobre el asesinato de Carlos Alberto Pedraza,licenciado de Universidad Pedagógica Nacional e integrante de movimiento social Congreso de los Pueblos, Medicina Legal amplió el informe forense sobre la desaparición forzada y asesinato del líder social y defensor de DDHH.

<iframe style="border: 0; outline: 0;" src="http://cdn.livestream.com/embed/contagioradioytv?layout=4&amp;clip=pla_4f856550-94ae-4e39-bc2c-a2cde635ff53&amp;height=340&amp;width=560&amp;autoplay=false" width="560" height="340" frameborder="0" scrolling="no"></iframe>

<div style="font-size: 11px; padding-top: 10px; text-align: center; width: 560px;">

</div>
