Title: Ex coronel Diego Tamayo guardó silencio sobre ejecuciones extrajudiciales de Soacha
Date: 2019-07-23 10:21
Author: CtgAdm
Category: Judicial, Paz
Tags: Álvaro Diego Tamayo Hoyos, Ejecuciones Extrajudiciales, JEP, jóvenes de soacha
Slug: diego-tamayo-ejecuciones-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/A-las-afueras-de-la-audiencia-de-Diego-Tamayo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MAFAPOBOGOTA  
] 

El pasado viernes se llevó a cabo la comparecencia voluntaria del ex teniente coronel Álvaro Diego Tamayo Hoyos ante la Jurisdicción Especial para la Paz (JEP). Tamayo Hoyos, fue comandante del Batallón de Infantería No. 15 Francisco de Paula Santander (BISAN 15) entre 2007 y 2008, y **su testimonio podría otorgar verdad plena sobre las ejecuciones extrajudiciales contra jóvenes en Soacha;** pero en la audiencia se limitó a decir lo que ya se sabía de estos casos.

Aunque la audiencia fue la primera de muchas que se producirán gracias al sometimiento de Tamayo ante la JEP, las víctimas se sintieron insatisfechas por la verdad aportada por el uniformado. **Sebastián Bojacá, abogado integrante de la Comisión Colombiana de Juristas (CCJ)**, explicó que las víctimas no veían voluntad para esclarecer los hechos porque lo dicho por Tamayo obedecía a "una histórica contada desde la perspectiva de los militares".

### **Lo que quieren saber las madres, lo que puede contar Diego Tamayo**

El Abogado indicó que las Madres de Soacha quieren saber quiénes, cómo y cuándo se dieron las órdenes para asesinar a sus 17 hijos; esa información no podría tenerla un soldado raso, cuya función es cumplir órdenes y no impartirlas. En su lugar, los más altos rangos de las Fuerzas Militares sí podrían dar razón de los hechos exactos que permiten asesinar civiles, y presentarlos como muertos en combate; ese sería el caso de Tamayo.

Bojacá señaló que el  ex Teniente Coronel llegó en 2006 al Batallón, con sede en Ocaña (Norte de Santander), y fue designado comandante en 2007. En el transcurso de este año, Tamayo eligió a todos los integrantes de su estado mayor, quienes también han sido señalados de participar en las ejecuciones; el uniformado tendría responsabilidad en los hechos porque debió supervisar a sus comandados y comunicar a sus superiores si se detectaban hechos irregulares, pero nunca informó, según el Abogado.

La CCJ, en un comunicado afirmó que "según la información legalmente obtenida, (el coronel Diego Tamayo) buscaba los recursos para pagar los tiquetes de traslado y así mismo pagar el *costo de las víctimas* y **conocía cómo se iba a llevar ese modus operandi, cuándo se hacía la entrega por parte de un militar a la tropa de la víctima,** cómo se procedía el acto de homicidio en completo estado de indefensión”. (Le puede interesar: ["Madres de Soacha rechazan 'verdades a medias' de militares en la JEP"](https://archivo.contagioradio.com/madres-soacha-rechazan-verdades-medias-jep/))

> Las víctimas salen defraudadas de versión voluntaria ante la JEP del ex Tte. Cnel. Álvaro Tamayo, que comandó el BISAN15, unidad responsable de 17 ejecuciones extrajudiciales. Para las familias, no aportó verdad más allá de lo ya revelado en justicia ordinaria [\#TamayoDigaLaVerdad](https://twitter.com/hashtag/TamayoDigaLaVerdad?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/uhvLB48sEi](https://t.co/uhvLB48sEi)
>
> — Comisión Colombiana de Juristas -CCJ- (@Coljuristas) [July 20, 2019](https://twitter.com/Coljuristas/status/1152369353318420480?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Tamayo había afirmado su voluntad de verdad, ¿qué pasó?**

En el marco del caso que se adelantaba contra Tamayo en la justicia ordinaria, el ex Comandante había afirmado que tenía intención de otorgar verdad plena; razón por la que el Defensor de las víctimas no se explicó el cambio de parecer de Tamayo, pues tampoco ha denunciado amenazas contra su vida. Pese a esta situación, el Abogado concluyó que esperaban que en otras audiencias se rompiera el pacto de silencio que parece imperar entre presuntos grandes responsables de ejecuciones extra judiciales, garantizando así el derecho de las víctimas a la verdad, justicia, reparación y no repetición.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
