Title: Las palabras con las que Martin Luther King desafió al racismo
Date: 2019-01-15 10:21
Author: AdminContagio
Category: DDHH, Otra Mirada
Tags: Derechos raciales en EEUU, I have a dream, Martin Luther King
Slug: las-palabras-con-las-que-martin-luther-king-desafio-al-racismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/luther-king.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [15 Ene 2019]

Cerca de las 4 de la tarde del 28 de agosto de 1963, 200.000 personas agolpadas frente al monumento de Abraham Lincoln en Washington D.C., escucharon al reverendo Martin Luther King, elevar una plegaria a manera de discurso por los derechos civiles para la comunidad Afroamericana en los Estados Unidos. Palabras que pasaron a la historia por su célebre mensaje "I have a Dream" (Tengo un sueño).

Cuando la muchedumbre se disponía a volver a sus hogares, tras más de 8 inclementes horas de calor, que durante la jornada acompañó a los manifestantes mientras escuchaban una a una las intervenciones musicales y de los oradores invitados, al Dr King le correspondió el cierre del evento.

Al subir al podio instalado frente a la efigie del decimosexto presidente de los Estados Unidos, el mismo que tomo las banderas de la abolición de la esclavitud en esa nación, Martin Luther King, se dirigió a los asistentes en tono similar al utilizado por sus antecesores al micrófono, con palabras que denotaban la tristeza y desolación resultante de siglos de sometimiento y desigualdad racial.

En discursos anteriores, el pastor bautista y líder del movimiento pro derechos raciales, se había referido a su "Sueño" de una nación igualitaria para negros y blancos. La cantante de Gospel, Mahalia Jackson, presente en el lugar, le sugirió a King que utilizara la misma reflexión onírica de la vida para motivar a los exhaustos asistentes; dejó de lado los papeles , improvisando uno de los discursos más recordados de la historia.

#### *"Hoy les digo a ustedes, amigos míos, que a pesar de las dificultades del momento, yo aún tengo un sueño. Es un sueño profundamente arraigado en el sueño "americano".* 

#### *Sueño que un día esta nación se levantará y vivirá el verdadero significado de su credo: "Afirmamos que estas verdades son evidentes: que todos los hombres son creados iguales".* 

#### *Sueño que un día, en las rojas colinas de Georgia, los hijos de los antiguos esclavos y los hijos de los antiguos dueños de esclavos, se puedan sentar juntos a la mesa de la hermandad.* 

#### *Sueño que un día, incluso el estado de Misisipí, un estado que se sofoca con el calor de la injusticia y de la opresión, se convertirá en un oasis de libertad y justicia.* 

#### *Sueño que mis cuatro hijos vivirán un día en un país en el cual no serán juzgados por el color de su piel, sino por los rasgos de su personalidad.* 

#### *¡Hoy tengo un sueño!* 

#### *Sueño que un día, el estado de Alabama cuyo gobernador escupe frases de interposición entre las razas y anulación de los negros, se convierta en un sitio donde los niños y niñas negras, puedan unir sus manos con las de los niños y niñas blancas y caminar unidos, como hermanos y hermanas.* 

#### *¡Hoy tengo un sueño!* 

#### *Sueño que algún día los valles serán cumbres, y las colinas y montañas serán llanos, los sitios más escarpados serán nivelados y los torcidos serán enderezados, y la gloria de Dios será revelada, y se unirá todo el género humano.* 

#### *Esta es nuestra esperanza. Esta es la fe con la cual regreso al Sur. Con esta fe podremos esculpir de la montaña de la desesperanza una piedra de esperanza. Con esta fe podremos trasformar el sonido discordante de nuestra nación, en una hermosa sinfonía de fraternidad. Con esta fe podremos trabajar juntos, rezar juntos, luchar juntos, ir a la cárcel juntos, defender la libertad juntos, sabiendo que algún día seremos libres.* 

#### *Ese será el día cuando todos los hijos de Dios podrán cantar el himno con un nuevo significado, "Mi país es tuyo. Dulce tierra de libertad, a tí te canto. Tierra de libertad donde mis antecesores murieron, tierra orgullo de los peregrinos, de cada costado de la montaña, que repique la libertad". Y si Estados Unidos ha de ser grande, esto tendrá que hacerse realidad.* 

#### *Por eso, ¡que repique la libertad desde la cúspide de los montes prodigiosos de Nueva Hampshire! ¡Que repique la libertad desde las poderosas montañas de Nueva York! ¡Que repique la libertad desde las alturas de las Alleghenies de Pensilvania! ¡Que repique la libertad desde las Rocosas cubiertas de nieve en Colorado! ¡Que repique la libertad desde las sinuosas pendientes de California! Pero no sólo eso: ! ¡Que repique la libertad desde la Montaña de Piedra de Georgia! ¡Que repique la libertad desde la Montaña Lookout de Tennesse! ¡Que repique la libertad desde cada pequeña colina y montaña de Misisipí! "De cada costado de la montaña, que repique la libertad".* 

#### *Cuando repique la libertad y la dejemos repicar en cada aldea y en cada caserío, en cada estado y en cada ciudad, podremos acelerar la llegada del día cuando todos los hijos de Dios, negros y blancos, judíos y cristianos, protestantes y católicos, puedan unir sus manos y cantar las palabras del viejo espiritual negro: "¡Libres al fin! ¡Libres al fin! Gracias a Dios omnipotente, ¡somos libres al fin!"* 
