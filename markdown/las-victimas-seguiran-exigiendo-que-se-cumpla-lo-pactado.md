Title: Las víctimas seguirán exigiendo que se cumpla lo pactado
Date: 2017-11-24 21:15
Category: DDHH, Nacional
Tags: acuerdo de paz, conflicto armado, víctimas
Slug: las-victimas-seguiran-exigiendo-que-se-cumpla-lo-pactado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/WhatsApp-Image-2017-11-24-at-12.31.24-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [24 Nov 2017] 

Luego de un año de la implementación de los Acuerdos de Paz, las víctimas continúan a la espera de que se ponga en marcha el Sistema Integral para la Justicia Verdad y Reparación, y con esta, que se activen las comisiones creadas para esclarecer la verdad sobre los crímenes cometidos durante el conflicto armado, **además, siguen exigiéndole al Congreso de la República y al gobierno que cumpla con lo pactado y la centralidad de las víctimas en este proceso**.

Luz Marina Cuchumbe, víctima de la Fuerza Pública, afirmó que para las víctimas este primer año de los Acuerdos de paz ha sido **difícil principalmente porque “es una mentira”** que estén en el centro de la implementación como se había pactado y por la actuación de algunos congresistas que han puesto todo tipo de trabas para dilatar los debates. (Le puede interesar: ["Un año retador para el enfoque de género en el acuerdo de paz"](https://archivo.contagioradio.com/las-mujeres-continuan-apostandole-al-enfoque-de-genero/))

“Los senadores y por quienes hemos votado para que nos representen, no quieren que haya un proceso de paz, **nosotras como mujeres somos las más interesadas en construir la paz**, somos los que han perdido familiares y somos los que cada día luchamos por esta paz, y ahora vemos cada día que el proceso no quiere ser implementado” afirmó Luz Marina.

Sin embargo, Cuchumbe expresó que las víctimas siguen con esperanza “como víctima yo no voy a perder **la esperanza hasta el último momento, y ver que estos acuerdos lleguen a buen puerto**”. De igual forma, Luz Marina manifestó que la defensa de los Acuerdos de Paz es camino que seguirán las víctimas del conflicto armado y aportando en la construcción de una Colombia en paz.

Finalmente, Luz Marina hizo un llamado a toda la ciudadanía para que presione al Congreso y continúe en la defensa de los acuerdos de paz, **“claro que la paz es muy cara y la guerra es barata**, pero la paz debe ser una apuesta por los más de 8 millones de víctimas que hay en el país”.

<iframe id="audio_22332201" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22332201_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
