Title: En octubre cerca de 400 inmigrantes perdieron la vida cruzando el Mediterráneo
Date: 2015-11-05 11:51
Category: El mundo, Política
Tags: 400 inmigrantes perdieron la vida cruzando el Mediterráneo, crisis migratoria, españa, Grecia, inmigrantes, Italia, La Organización Mundial para las migraciones (OMI), personas que han llegado al continente europeo a través del Mediterráneo
Slug: en-octubre-cerca-de-400-inmigrantes-perdieron-la-vida-cruzando-el-mediterraneo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Muerte-de-Inmigrantes-en-octubre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### foto: patilla.com 

###### [4 nov 2015]

La Organización Mundial para las migraciones (OMI) informó que **aproximadamente 400 inmigrantes perdieron la vida cruzando el Mediterráneo en el mes de octubre**, lo más alarmante es que en tan solo tres días del mes de noviembre 18 han muerto en medio de la misma la travesía.

**Con este reporte se eleva la cifra de personas  que han muerto tratando de llegar a Europa por mar en 2015 a 3.406,** que suponen  más del 72% del total de migrantes muertos en el mundo en lo que va del 2015.

Por otro lado, **en el mes de octubre 218.000 realizaron la travesía marítima para llegar a las costas de Grecia, Italia o España,** unos 28.000 lo hicieron este mismo fin de semana sin importar las complicaciones del tiempo y el aumento de peligro.

La semana pasada los guardacostas griegos informaron que se presentaron siete hundimientos de embarcaciones precarias, el **hecho dejó 106 cuerpos sin vida, e**l resto de ocupantes se encuentran en paraderos desconocidos y se determina que el número de personas que han llegado al continente europeo a través del Mediterráneo es de 760.976 según los últimos datos oficiales.
