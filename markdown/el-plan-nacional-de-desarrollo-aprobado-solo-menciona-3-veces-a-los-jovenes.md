Title: El Plan Nacional de Desarrollo aprobado solo menciona 3 veces a los jóvenes
Date: 2015-05-15 11:22
Author: CtgAdm
Category: Economía, Nacional
Tags: heidy sanchez, joven, jovenes, juco, juventud, juventud rebelde, liberales, Plan Nacional de Desarrollo, PND, primer empleo, progresistas, tejuntas, vamos por los derechos
Slug: el-plan-nacional-de-desarrollo-aprobado-solo-menciona-3-veces-a-los-jovenes
Status: published

###### Foto: NotMendoza 

######  

##### <iframe src="http://www.ivoox.com/player_ek_4501181_2_1.html?data=lZqdk5acdY6ZmKiak5uJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fsdHO0JCypcTd0NPOzpDIqYy4xtjO1NfTsM3jjMbd1NTGpcXjjNjcztSPscbixM7c0MaPd46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Heidy Sánchez, JUCO] 

######  

###### [15 may 2015] 

Las y los jóvenes en Colombia consideran que el Plan Nacional de Desarrollo 2014-2018 "Todos por un nuevo país", que se aprobó la 1ra semana de mayo del 2015, trata a la juventud de manera subsidiaria, como elemento agregado para satisfacer intereses que no les corresponden.

Muestra de ello sería la ausencia de una política pública para la juventud que no la conciba como objeto sobre el cual se define, sino como actor de decisión de su futuro. El PND aprobado, por el contrario, menciona de manera fragmentada a los jóvenes, y sólo lo hace en los artículos 86 y 103.

En el primero, como un artículo que da continuidad al programa Colombia Joven, que el gobierno de Juan Manuel Santos viene impulsando desde su primer mandato. Sin embargo, según explica Heidy Sánchez, de la Juventud Comunista Colombiana, este es un espacio de burocrático que lejos de apoyar las iniciativas juveniles, las restringe y burocratiza. "Colombia Joven es bastante cerrado, casi del bolsillo del gobierno nacional. La mayoría de los jóvenes ni siquiera saben que ese espacio existe", señala.

Por otra parte, aunque desde el año 2012 se ha trabajado en un estatuto de ciudadanía juvenil que concibe formas de participación amplias y diversas para los y las jóvenes, hasta la fecha no se ha implementado, ni se proyecta su ejecución en el PND.

Aunque se menciona el incremento del empleo, no logra ubicarse a los jóvenes como población especial en sus objetivos. Se mantiene la ley de "Primer empleo" que ofrece salarios de entre 800 y 1 millón de pesos, no brindan condiciones dignas laborales ni de vida, y solo beneficia a las empresas que la promueven. La líder comunista asegura que a través del "banco de iniciativas encaminado al fortalecimiento del capital social" se buscaría mantener las Cooperativas de Trabajo Asociado, que en su momento la Corte Constitucional declarara ilegales.

"Nosotros sabemos que no van a poderse coordinar de la mejor forma una serie de proyectos de los jóvenes en el país", afirma Heidy.

Un segundo momento en que se menciona a los jóvenes, es para fortalecer la atención integral al Sistema de Responsabilidad Penal Adolescente (SRPA). "Para este sistema el joven siempre ha sido quien no es niño ni adulto, pero que requiere una serie de restricciones para que pueda ser un adulto conforme a las normas pre-establecidas. En esa medida, cuando se habla del hecho de 'ser joven', se cae en la lógica de estigmatización incluso en la criminalización de la vida juvenil, que no permite el libre desarrollo de la personalidad". Es bajo este argumento que Heidy explica que se necesita modificar el Código Penal y del Código de Infancia y Adolescencia, y que se entiende que al no hacerlo, el PND no fomenta la participación real, sino que es un PND restrictivo y punitivo para la juventud.

El tercer momento en el que se habla de la juventud es en el artículo 103, y se hace -paradójicamente- como parte de una enumeración de los sectores que requieren políticas públicas con enfoque diferencial: mujeres, niñas y niños jóvenes y mayores indígenas.

Para la joven comunista, la construcción de la paz en colombia debe permitir que se abran espacios reales y concretos para la juventud, y es por esto que buscan participar en el Consejo Nacional de Paz. En él, insisten en la necesidad de crear un ministerio para la Juventud y una política pública clara e integral que le acompañe.

En el mes de agosto, diversas plataformas juveniles entre las que se encuentran Tejuntas, las Juventudes Liberales, los Jóvenes Progresistas, Vamos por los Derechos, la Juventud Rebelde, la Juventud Comunista, y Laboratorios por la Paz, entre otras organizaciones, realizarán un Encuentro Nacional Juvenil, para construir de manera conjunta lineas de trabajo y acción de las y los jóvenes en Colombia en torno a la paz.
