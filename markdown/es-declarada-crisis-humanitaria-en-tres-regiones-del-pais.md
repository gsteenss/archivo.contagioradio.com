Title: Es declarada crisis humanitaria en tres regiones del país
Date: 2020-02-11 17:30
Author: CtgAdm
Category: DDHH, Nacional
Tags: Catatumbo, ELN, violencia
Slug: es-declarada-crisis-humanitaria-en-tres-regiones-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/EQcxk5aWsAMiJ2T.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->  
\[audio src="https://co.ivoox.com/es/juan-carlos-quintero-sobre-situacion-catatumbo\_md\_47573229\_wp\_1.mp3"\] [](https://co.ivoox.com/es/juan-carlos-quintero-sobre-situacion-catatumbo-audios-mp3_rf_47573229_1.html "Juan Carlos Quintero, sobre situacion en catatumbo")  
<!-- /wp:html -->

<!-- wp:paragraph -->

El programa Ruta de Prevención y Protección a Defensores y Defensoras de los Derechos Humanos del Nororiente colombiano, **declaró crisis humanitaria en el territorio** debido a las situaciones que amenazan, atentan y vulneran el goce pleno de los derechos fundamentales contra habitantes del Catatumbo, Bajo Cauca Antioqueño, Nordeste Antioqueño, y sur de Bolívar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Juan Carlos Quintero, directivo de la Asociación Campesina del Catatumbo (ASCAMCAT), "*es lamentable que muchas situaciones sean responsabilidad del Ejército Nacional, específicamente la fuerza de tarea de Vulcano del Catatumbo , fuerza de brigada 30 y la Fuerza de Despliegue Rápido (Fudra)"* y señaló los hechos mas relevantes que se han venido presentando.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Crisis humanitaria en Catatumbo

<!-- /wp:heading -->

<!-- wp:paragraph -->

En esta zona el primer hecho del 2020 se registró el 31 de enero, cuando el líder social Jhonny Abril fue abordado por miembros del Ejercito Nacional en la vereda Motilandia; quienes solicitaron la identificación y una requisa a los ocupante del vehículo de seguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Programa, Abril accedió, pero solicitó la presencia de la Policía, autoridad facultada para hacer estos procedimientos, *"transcurriendo 3 horas sin justificación alguna para detenerlo y a sus acompañantes, al llegar la Policía valiéndose de sus cargos, armamento y vulnerabilidad del líder intimidaron a su escolta con arma de fuego a la altura del pecho"*.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AscamcatOficia/status/1223778171889356801","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscamcatOficia/status/1223778171889356801

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Otro de los hechos evidenciados fueron en los municipios de Ábrego, La Playa y Hacarí, en donde hay confrontación armada entre ELN y EPL, *"esto ha causando situaciones de desplazamiento forzado de 45 familias, 236 personas de la vereda La Arenosa del municipio de Ábrego".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/catatumbol281/status/1227298508711059461","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/catatumbol281/status/1227298508711059461

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Asimismo el asesinato del campesino Efrén de Jesús Pabón vicepresidente de la JAC de la vereda El Totumito, que elevó a tres en número de líderes asesinados en lo que va corrido del año en Norte de Santander.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El caso más reciente denunciado, fue el 10 de febrero sobre la 1:00 am en la vereda San Gil corregimiento de Puente Real, Municipio de San Calixto dónde miembros del Ejército ingresaron a la casa de la docente de la escuela, *"sacaron a su esposo e hijo de forma agresiva, les quitaron sus celulares y posteriormente arribaron a una finca de la vereda y capturaron 2 campesinos, uno fue golpeado"*, ante la reacción de la comunidad el campesino fue liberado a las 5:00 am del siguiente día.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Bajo Cauca Antioqueño

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el municipio de El Bagre, Antioquia durante el mes de enero 14 personas han sido asesinadas, tres desaparecidas entre ellas dos menores de edad, en veredas como Puerto Jobo, Villa El porvenir y Las Brisas. (Le puede interesar: <https://archivo.contagioradio.com/amenazas-asesinatos-y-desplazamientos-a-educadores-en-colombia/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Es de notar que estos hechos violentos se enmarcan dentro de una guerra entre los diferentes grupos armados que hacen presencia en el territorio y reyes autodefensas gaitanistas y narcotráfico"*, destacó el comunicado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Nordeste Antioqueño

<!-- /wp:heading -->

<!-- wp:paragraph -->

En esta región el primero de febrero "*el Ejército ingreso a la vereda la Cooperativa y de manera ilegal hicieron allanamientos*" sañaló el informe. Acto que se repito el 7 de febrero en la vereda Altos de Manila, municipio de Remedios Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"En horas de la madrugada se dieron allanamientos y capturas por parte de miembros del Ejército y Policía Nacional, en los que capturaron tres campesinos entre ellos un menor de edad"*, según la Red, la comunidad desconoce las causas que motivaron su detención.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente uno de los casos mas alarmantes según el directivo de Ascamcat, se dió el pasado 9 de febrero al empezar a circular un panfleto de la guerrilla del ELN que anuncia un paro armado de 72 horas.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/InteligenciaEs/status/1226981062938701824","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/InteligenciaEs/status/1226981062938701824

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Antes de este comunicado la Ruta de Prevención había pedido activar el mecanismo extraordinario de protección el 13 al 16 de febrero en algunos departamentos del Norte de Santander, pero tuvieron que suspenderlo hasta que no se detenga este paro armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Con gran preocupación informamos lo que ocurre pues nos más que las comunidades son quienes se ven directamente afectadas por este accionar de grupos armados que hacen presencia en la región"*, concluyó el comunicado e hizo un llamado a autoridades nacionales e internacionales defensoras de derechos humanos a prestan atención a estos territorios.

<!-- /wp:paragraph -->
