Title: FMI propone pensiones menores al salario mínimo
Date: 2015-03-26 10:52
Author: CtgAdm
Category: Economía, Nacional
Tags: FMI, Ley 100, Línea de pobreza en Colombia, OCDE, Pobreza en Colombia, Reforma Pensional
Slug: fmi-propone-pensiones-menores-al-salario-minimo
Status: published

###### Foto: cincominutos.com 

<iframe src="http://www.ivoox.com/player_ek_4263744_2_1.html?data=lZejlZyYeI6ZmKiak5iJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nlsbX0NLS0MnFp8rjz8rgjcnJsIy6rq6Y1dTSb9Xmwtjb0cjMpcXV1JDmjdPTb9PZ1NXc0JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Economista Eduardo Sarmiento] 

El **Fondo Monetario Internacional, FMI**, recomienda a Colombia que se eleve el **cobro del IVA, que se amplíe la base tributaria y que se haga una reforma pensional**. Para el economista Eduardo Sarmiento, estas recomendaciones son trasnochadas y no corresponden a las necesidades del país, puesto que el mismo FMI estaba pronosticando un crecimiento económico que Colombia no alcanzó.

En la posible reforma pensional lo único que se va a garantizar es que se amplíen los niveles de pobreza puesto que el empleo y la cotización base no alcanzan para cubrir el margen de pensionados, con **la Ley 100 los fondos privados entraron a reconocer solamente el 20% del salario** para el pago de pensiones y el resto lo destinan al sistema financiero.

Sarmiento señala que el sistema propuesto por el **FMI y la OCDE** ya fracasó y ahora lo que recomiendan es hacerlo más oneroso y que afecte aún más a la clase trabajadora. Por ello es necesario que se garantice un sistema público de pensiones para quienes devengan un salario inferior a 1.5% del SMLV y otro sistema privado para quienes ganen más de esta suma.

Por otra parte Sarmiento explica que el margen de pobreza, que según cifras oficiales, llegó al 28%, lo cual ya es escandaloso, bajó del 30% pero por un cambio de fórmula y de metodología del cálculo, es decir, la línea de pobreza bajó de estar en un ingreso de 220.000 a tan solo 180.000 pesos mensuales.
