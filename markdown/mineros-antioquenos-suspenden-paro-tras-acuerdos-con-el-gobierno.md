Title: Mineros antioqueños suspenden paro tras acuerdos con el gobierno
Date: 2016-09-29 17:19
Category: Movilización, Nacional
Tags: Antioquia, Corte Constitucional, Gobierno, mineros, paro
Slug: mineros-antioquenos-suspenden-paro-tras-acuerdos-con-el-gobierno
Status: published

###### Foto: [altamanzana.wordpress.com]

###### [29 Sep 2016]

La creación de un decreto construido por los mineros y el gobierno, por medio del cual se crea una **mesa minera** con el objetivo de solucionar las demandas de los manifestante, permitió que terminen los[10 días de paro](https://archivo.contagioradio.com/36-heridos-deja-represion-por-parte-de-la-fuerza-publica-hacia-mineros-en-antioquia/) que llevaban miles de mineros ancestrales de los municipios de Segovia y Remedios en Antioquia.

[Durante el martes y miércoles ese sector se reunió con delegados Ministerio de Minas y Energía, la Agencia Nacional de Minería, Congresistas, gobierno departamental, secretario de minas de la gobernación de Antioquia, los líderes de los mineros, y la empresa Gran Colombia Gold.]

Teniendo en cuenta que el gobierno ha recibido órdenes en materia legal, por parte de la Corte Constitucional y del Consejo de Estado, donde se le exige al gobierno reformar o expedir un nuevo Código de Minas donde se incluya la mediana y pequeña minería, el **decreto establece comisiones de trabajo mediante las cuales los mineros esperan que se concreten soluciones a sus demandas y se formalice su actividad,** como lo explica Rubén Darío Gómez, secretario general de la Confederación Nacional de Mineros de Colombia, Conalminercol.

“El gobierno reconoció que hay minería ancestral, con base en eso **se  adelantarán los procesos de formalización**”, dice Gómez, quien agrega que por medio de esa mesa minera se buscará gestionar un **proyecto de Ley  que modifique el Código de Minas**, de acuerdo a las órdenes que ha dado la Corte Constitucional.

El próximo 4 de octubre se convocará la mesa con el gobierno y el 6 de octubre empezarán las negociaciones y el trabajo en las comisiones.  Sin embargo, los mineros, aseguran que si esa estrategia no da resultados y el gobierno les incumple **los paros serán una constante.**

<iframe id="audio_13116089" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13116089_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
