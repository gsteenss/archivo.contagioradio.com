Title: Lo que valen nuestros votos
Date: 2015-09-16 06:00
Category: Carolina, Opinion
Tags: Compra de votos, Cundinamarca, democracia colombiana, elecciones colombia, MOE, trashumancia
Slug: lo-que-valen-nuestros-votos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/image.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto ETC.ADN] 

#### **[Carolina Garzón Díaz](https://archivo.contagioradio.com/carolina-garzon-diaz/) - ** [**@E\_vinna**](https://twitter.com/E_Vinna) 

###### [15 de Sep 2015]

[A cinco semanas de las elecciones locales en Colombia el panorama democrático se ve turbio. La Misión de Observación Electoral -MOE- reveló hace unos días que en 204 municipios del país la violencia y la intervención ilícita en el proceso pueden afectar la calidad de las elecciones. Según esta organización de la sociedad civil “hoy en día el riesgo está concentrado en nodos locales de municipios que se encuentran, por igual, en las distintas regiones del país”, es decir, en todo el territorio.]

[Los dos factores que analiza la MOE para medir el riesgo son violencia y fraude electoral. En cuanto al primero, se refiere a las repercusiones que tiene en las elecciones y en los resultados las violaciones a la libertad de prensa, el desplazamiento forzoso, los atentados contra candidatos y partidos políticos, así como la presencia y acciones de grupos armados ilegales. Factores que son más evidentes en el área rural y que han afectado la calidad de elecciones anteriores.]

[Por su parte, el fraude electoral tiene como variables, según la MOE, “el comportamiento atípico de votos nulos, tarjetones no marcados, dominio electoral y variaciones en la participación de los votantes en cada municipio”. Pero creo que no hay que ir muy lejos para ser testigo de los delitos y la corrupción de los candidatos y sus campañas. Precisamente en Cota, Cundinamarca, el Consejo Nacional Electoral acaba de anular la inscripción de 4.279 cédulas por trashumancia.]

[En miles de barrios y pueblos hay grandes fiestas, bazares, asados y baile con los que se cambia el voto por un chorizo, una cerveza o un plato de arroz con pollo. La eterna historia. No tenemos la menor idea de cuánto vale (no cuánto cuesta) nuestro voto y lo que significa.]

[Nuestra democracia seguirá siendo débil mientras consideremos el voto como un objeto de subasta y no como el derecho que tanta lucha y sangre nos costó conseguir. Por su supuesto no todo está en manos del ciudadano que vota, otro tema que corroe nuestro proceso electoral es la corrupción administrativa y la falta de transparencia de las campañas electorales que se venden a sus patrocinadores.]

[Sin embargo, yo votaré por edil, concejal y alcalde, y así mismo invitaré a quienes conozco a votar de forma informada, consciente y crítica. ¿Cómo? Investigando a los candidatos, conociendo sus planes de gobierno, escuchando sus entrevistas, haciéndoles preguntas por las redes sociales y exigiéndoles qué rindan cuentas sobre el dinero de sus campañas y sus patrocinadores.]

[Voy a votar, no porque crea que estas elecciones son el mayor ejercicio de democracia y de participación, sino porque debemos demostrar nuestro compromiso tanto en las acciones mínimas como en las más grandes. En un país diferente optaría por la abstención como un símbolo de protesta ante la corrupción, la ineficiencia y los pésimos manejos de lo público que han tenido los mandatarios locales, pero en Colombia abstenerse es como sumarle un voto a los que hacen con el país lo que les viene en gana.]

[Ser ciudadanos no es únicamente tener una cédula, es ejercer nuestros derechos y hacerlo en consonancia con nuestros deberes. La invitación en este momento es a que nos pongamos en la tarea de conocer a los candidatos, que nos acerquemos a las urnas el 25 de octubre y a que nos apoyemos en iniciativas como la MOE para realizar denuncias y documentar los casos de delitos y acciones violentas que afectan la calidad de las elecciones. Las soluciones no serán inmediatas, pero es mejor usar la energía que gastamos quejándonos e indignándonos por redes sociales, en acciones concretas que algún día podrán hacer la diferencia.]
