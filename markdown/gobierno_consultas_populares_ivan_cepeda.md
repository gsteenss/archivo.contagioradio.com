Title: "Gobierno quiere asfixiar consultas populares" Iván Cepeda
Date: 2017-09-26 12:52
Category: Ambiente, Entrevistas
Tags: consultas populares, Ministro de Minas
Slug: gobierno_consultas_populares_ivan_cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/Consulta-Popular-Minera-en-Ibagué.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Comité Ambiental del Tolima] 

###### [ 26 Sep 2017] 

Son **31.103 personas las que han salido a las urnas de 8 municipios del país para decirle no al desarrollo de proyectos minero-energéticos** que, de acuerdo con sus denuncias, están atentado contra la vida de la naturaleza y las comunidad. Sin embargo, son varias las estrategias del gobierno para impedir que esas consultas se hagan efectivas en los territorios.

Mediante un proyecto de Ley el gobierno busca poner trabas y debilitar el desarrollo de consultas, asimismo se  están interponiendo distintas demandas contra dichos resultados. También Corte Constitucional se encontraría revisando la sentencia T-445/16, con el objeto "de establecer limites el alcance de las consultas populares a nivel territorial", según denuncias publicadas por el abogado Rodrigo Negrete.  [(Le puede interesar: Denuncian cruzada del gobierno contra las consultas populares)](https://archivo.contagioradio.com/46783/)

Esa discusión fue la que se desarrolló en el marco del debate de control político impulsado por los congresistas Iván Cepeda, Alberto Castilla y Alirio Uribe quienes invitaron a diferentes instituciones gubernamentales. Como lo temían los congresistas, **el Ministro de Minas Germán Arce no asistió al debate, pero si estuvieron Carlos Andrés Cante, viceministro de esa misma cartera y Silvana Habib,** presidenta de la Agencia Nacional de Minería.

Los legisladores que llamaron al debate señalaron que el gobierno nacional debe respetar el ejercicio democrático de las comunidades. "El Gobierno quiere asfixiar las consultas sin resolver problemas de fondo", asegura Iván Cepeda, y agrega que lo que se debe hacer es "desentrañar la estrategia que tiene el gobierno, que viene maquillando sus planes contra los resultados de las consultas populares".

Por su parte, el Representante Alirio Uribe señaló que la respuesta del Ministro de Minas “desconoce el carácter democrático del Estado, favorece el autoritarismo y la centralización, niega las garantías que requieren estos mecanismos de participación ciudadana y **van en contravía del Acuerdo de Paz que propende por la participación ciudadana en los territorios”.**

### **Consecuencias del modelo extractivista** 

Además, el senador Cepeda recordó que después de Brasil Colombia es el país más peligroso para los ecologistas toda vez que **37 defensores del medio ambiente fueron asesinados en 2016** y persisten amenazas contra quienes promueven y participan en las consultas populares.

Según los congresistas "el modelo de desarrollo que se fundamenta en la inversión multinacional para la extracción de petróleo, gas carbón y otros recursos no renovables, niega el cambo climático, pone en peligro el agua como recurso vital para la supervivencia, va en contravía de un modelo productivo en el campo y abre las puertas para el clientelismo y la corrupción"

Los congresistas solicitaron al gobierno nacional respetar el mandato ciudadano expresado en las consultas populares, adoptar las medidas para hacer cumplir sus resultados, suspender toda actividad de titulación hasta que se expida la ley que regula el procedimiento de concertación entre la nación y el territorio y se garantice la participación ciudadana y declarar la caducidad de los contratos adjudicados irregularmente en parques nacionales, páramos y en los municipios que prohibieron la minería por mandato de consultas populares.

<iframe id="audio_21103698" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21103698_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
