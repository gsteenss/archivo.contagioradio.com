Title: FARC turns in information related to 276 cases of disappeared persons
Date: 2019-08-21 09:00
Author: CtgAdm
Category: English
Tags: FARC, peace process, Unit for the Search of Disappeared Persons
Slug: farc-turns-in-information-on-276-disappeared-persons
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Unidad-de-Búsqueda-de-personas-dadas-por-desaparecidas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: @UBPD] 

[The FARC political party turned in a report to the Unit for the Search of Disappeared Persons (UBPD by its Spanish initials) that documents 276 cases of people disappeared as part of their commitment to help advance the functioning of the System of Truth, Justice, Reparation and Non-Repetition.]

[The report was carried out by 71 members of the FARC party — 38 men and 33 women who in their process of reincorporation are tasked with the search of disappeared persons. Since 2017, the group collected information associated with 276 cases in 15 provinces of the country — 64% represent members of illegal armed groups, 28% civilians, 1% members of the Armed Forces and 7% weren’t identified.]

### **72% of cases occurred between 1997 and 2007** 

[The information was handed over with the help of the National Institute of Legal Medicine and Forensic Sciences and the International Committee of the Red Cross, tracing a roadmap that allowed for the documentation of cases in]Antioquia, Chocó, Tolima, Valle del Cauca, Nariño, Cauca, Huila, La Guajira, Cesar, Norte de Santander, Arauca, Meta, Caquetá and Guaviare[ between 1981 and 2016.]

[“The Unit trusts that this report is the first step for many who make part of the FARC to advance the search, identification and dignified turning in of persons disappeared and that there will be more ex-combatants that will turn in information,” said the Unit’s director, Luz Marina Monsón.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no" data-mce-fragment="1"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
