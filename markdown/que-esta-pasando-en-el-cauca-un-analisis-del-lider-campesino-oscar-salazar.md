Title: ¿Qué está pasando en Cauca? análisis del líder campesino Óscar Salazar
Date: 2019-08-28 18:23
Author: CtgAdm
Category: Comunidad, Nacional
Tags: amenazas contra líderes sociales, campesinos Cauca, Cauca, Militarización de territorios
Slug: que-esta-pasando-en-el-cauca-un-analisis-del-lider-campesino-oscar-salazar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/campesinos-cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo particular] 

[El asesinato de 9 [líderes]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0} [sociales y el reporte de]{.css-901oao .css-16my406 .r-1qd0xha .r-vw2c0b .r-ad9z0x .r-bcqeeo .r-qvutc0}475 amenazas contra defensores  defensoras de derechos en lo que va del 2019 en Cauca, los continuos ataques contra comunidades afro e indígenas, la proliferación de grupos armados como las autodenominadas Autodefensas Gaitanistas de Colombia (AGC) disidencias o el Cartel de Sinaloa e irónicamente el numeroso pie de fuerza de la región, que contará próximamente con 400 nuevos efectivos, reflejan la conflictividad que vive el departamento. ]{.css-901oao .css-16my406 .r-1qd0xha .r-ad9z0x .r-bcqeeo .r-qvutc0}

El líder campesino, Óscar Salazar, quien fue víctima de un atentado el pasado 17 de agosto, se refiere a la grave situación del Cauca y advierte que estos ataques, **"están golpeando el movimiento social en lo más sensible que tiene que son los hombres y mujeres formados que asumen un liderazgo en su territorio"**.

Salazar sostiene que es necesario analizar las políticas que hacen que la región del Cauca llegue a situación, comenzando por evaluar la "políticas anticultivo que tiene el Estado, las que impiden desarrollar programa contra las drogas ilícitas", cuya producción y mercancía generan gran tensión en dicho territorio.

> **"Tenemos que ir más allá de plantear el análisis entre los malos y los buenos y los buenos son de malas y caen bajo los los malos, esa simpleza no puede seguirse dando"**

Otra de las problemáticas que deben entrar a ser analizadas, señala el líder social es precisamente la exclusión del campesinado como sujeto de derechos, las políticas minero energéticas que se vienen implementando en los territorios y que "conllevan la destrucción del ambiente" y el surgimiento de proyectos hidroeléctricos, que promueven la privatizacion del agua.

Este contexto, " el no cumplimiento de los acuerdos de paz, la ruptura de los diálogos con el ELN y la doctrina militar operando, generan un caldo letal para las comunidades donde los lideres sociales somos carne de cañón de esa situación", concluye Salazar.  [(Le puede interesar: Denuncian nuevas amenazas contra Maydany Salcedo, lideresa de Cauca)](https://archivo.contagioradio.com/amenazas-maydanysalcedo-lideresa-cauca/)

### Cauca, geográficamente estratégica 

"Si usted quiere pasar de China a Brasil, por este corredor llega", expresa el líder campesino, destacando la conectividad y posición geográfica del Cauca y su cercanía a la selva de la Amazonía y al litoral del Pacifico como una ruta trascendental para el tráfico legal e ilegal. [(Lea también: Asesinato del profesor Orlando Gómez en Cauca es un atentado contra la educación)](https://archivo.contagioradio.com/orlando-gomez-atentado-educacion/)

Otro factor clave que sugiere el líder para explicar la situación del departamento, es la composición demográfica y cultural, citando que gran parte de la población del Cauca vive de la agricultura, práctica que **"vive una situación de arrinconamiento y exclusión para las comunidades que sobreviven de ello"**.

Como conclusión Salazar destaca que fruto de la Minga Nacional por la vida, se logró que el Estado adquiriera un compromiso de escribir una política publica para el campesinado y las pueblos afro e indígenas, una oportunidad que debe ser aprovechada para poner sobre la mesa estas problemáticas y encontrar alternativas para que sean resueltas.

**Síguenos en Facebook:**  
<iframe id="audio_40608740" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40608740_4_1.html?c1=ff6600"></iframe>  
<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
