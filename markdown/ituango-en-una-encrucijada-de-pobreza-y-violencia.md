Title: «Ituango en una encrucijada de pobreza y violencia»
Date: 2020-08-05 22:04
Author: AdminContagio
Category: Actualidad, Nacional
Tags: campesinos, Hidroituango, Ituango, Plan de Sustitución de Cultivos de Uso ilícito, PNIS
Slug: ituango-en-una-encrucijada-de-pobreza-y-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/DESPLAZAMIENTO-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este martes se dio a conocer una denuncia por parte de la Asociación de Campesinos de Ituango señalando que **un «*fuerte contingente militar»* había arribado a las veredas La Filadelfia y El Aro del municipio de Ituango, Antioquia** desde el pasado sábado 1° de agosto con órdenes de adelantar operaciones de erradicación forzada de cultivos de uso ilícito. (Le puede interesar: [Esperamos que en Mutatá encontremos la paz: excombatientes de Ituango](https://archivo.contagioradio.com/esperamos-que-en-mutata-encontremos-la-paz-excombatientes-de-ituango/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el proceso organizativo campesino, **en estos operativos «*prima el uso y exceso de fuerza por parte de la tropas*» lo que ha generado graves consecuencias en la población civil, reportándose cuatro personas heridas y la activación de armas de fuego** que presuntamente han sido utilizadas contra la comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente la Asociación de Campesinos, que pertenece a la Asociación Nacional de Zonas de Reserva Campesina –ANZORC-, expone y rechaza el hecho de que tras **casi 4 años desde la firma del Acuerdo de Paz el Estado no haya cumplido con lo dispuesto en el Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito -PNIS-**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y es que, pese a que **el punto 4 del Acuerdo brindó un carácter prevalente a la sustitución voluntaria** de las plantaciones de las que se derivan estupefacientes, relegando la erradicación forzosa a una acción residual que se reserva al Gobierno Nacional únicamente cuando no sea posible, agotadas todas las instancias, avanzar en un acuerdo de sustitución voluntaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las comunidades afirman que contrario al Acuerdo, **el Gobierno mediante el uso de la Fuerza Pública ha priorizado la erradicación forzada, pese a la disposición que tienen los campesinos para asumir la sustitución voluntaria** siempre que se les brinden **garantías de subsistencia, dignidad y seguridad**; condiciones que según ellos no son promovidas por el Estado, cuya única presencia en los territorios es la militarización que lleva a la confrontación armada con otros grupos al margen de la ley y a la vulneración de los derechos humanos de los habitantes en los territorios lo que tiene a **«*Ituango en una encrucijada de pobreza y violencia*».** (Le puede interesar: [Más de 450 familias desplazadas en Ituango por enfrentamientos armados](https://archivo.contagioradio.com/mas-de-450-familias-desplazadas-en-ituango-por-enfrentamientos-armados/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La Fuerza Pública no es el único actor institucional que vulnera los derechos de las comunidades en Ituango

<!-- /wp:heading -->

<!-- wp:paragraph -->

A las denuncias contra la Fuerza Pública, se suma la que realiza el [Movimiento Ríos Vivos](https://riosvivoscolombia.org/) que agrupa a las víctimas y afectados por el desarrollo del Megaproyecto Hidroeléctrico Ituango más conocido como Hidroituango, donde afirman que **la Procuraduría Regional de Antioquia se presta para desconocer el derecho de los miembros del colectivo y favorecer los intereses de Empresas Públicas de Medellín -EPM-.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En un comunicado, Ríos Vivos expone que la Procuraduría no está haciendo veeduría al proceso en el que se están **ejerciendo presiones con advertencias de suspensión de apoyos económicos en medio de la pandemia a personas que perdieron su vivienda** y cláusulas de presión para el pago de indemnizaciones injustas. Las respuestas del Procurador Regional Luis Fernando Bustamante, para atender estos llamados, han sido nulas frente a estos llamados según el Movimiento. (Lea también: [Ríos Vivos denuncia intimidaciones por parte de EPM y autoridades de Ituango](https://archivo.contagioradio.com/rios-vivos-denuncia-ituango/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RiosLibresAnt/status/1289238759725129728?s=08","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RiosLibresAnt/status/1289238759725129728?s=08

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
