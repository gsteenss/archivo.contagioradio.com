Title: En Córdoba ya no solo los persiguen por ser líderes sociales, también por tener  síntomas de Covid-19
Date: 2020-03-28 13:58
Author: CtgAdm
Category: Actualidad
Tags: cordoba, Persecución a líderes sociales
Slug: cordoba-persiguen-lideres-sociales-covid-19
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/FotoAsociaciónMinga-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Líderes Sociales /Minga

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La lideresa Yina Paola Sanchez Rodríguez del municipio de Montelibano Córdoba denuncia que desde el 24 de marzo y debido a una mala asesoría médica en el centro de atención el corregimiento de Tierradentro, **fue señalada erroneamente con Covid-19 sin que se le hicieran las pruebas adecuadas**, pese a ello, el anunció causó conmoción y ha generado una serie de estigmatizaciones, incluso hasta ser considerada blanco de los grupos armados de la zona. [(Lea también: En audiencia se denunció la grave situación de DD.HH. que vive Córdoba)](https://archivo.contagioradio.com/en-audiencia-se-denuncio-la-grave-situacion-de-dh-que-vive-cordoba/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la Policía aseguraron que hasta que no se hicieran los exámenes que determinan si se es positivo, no podrían realizar una evacuación, ante dicho rechazo, relata la lideresa, este sector de la población acudió a un grupo armado, presuntamente el Clan del Golfo para amenazar a la defensora de DD.HH. y expulsarla del territorio. [(Lea también: Acatando cuarentena en su hogar, asesinan a dos líderes indígenas en Valle del Cauca)](https://archivo.contagioradio.com/cuarentena-asesinan-indigenas-valle/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Pánico e intolerancia usada contra los líderes

<!-- /wp:heading -->

<!-- wp:paragraph -->

De este modo, comenzó una persecución que ha incluido mensajes de texto y llamadas, estigmatizando no solo a ella si no a su familia, a quienes ya no los dejan comprar en las tiendas del corregimiento por temor. [(Lea también: Las amenazas que enfrentan líderes como Marco Rivadeneira en Putumayo)](https://archivo.contagioradio.com/marco-rivadeneira-el-lider-que-hasta-su-ultimo-dia-defendio-la-sustitucion-voluntaria-de-cultivos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Temo que por estos señalamientos puedan permitir el accionar de los grupos al margen de la ley, no sé hasta dónde puede trascender este tipo de intolerancia**", expresa Yina Sánchez. [(Lea también: Agresiones contra líderes sociales incrementaron un 49% en el primer semestre del 2019)](https://archivo.contagioradio.com/agresiones-contra-lideres-sociales-incrementaron-un-49-en-el-primer-semestre-del-2019/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Relata que por motivos laborales tuvo que viajar a Bogotá de donde regreso con un cuadro gripal y fiebre por lo que ha decidido cumplir con el aislamiento preventivo sin embargo el diagnóstico apresurado causó conmoción en la zona, "es un tema de intolerancia y pánico por lo que vive el mundo entero por la pandemia del Covid-19" [(Lea también: Lideresas sociales guardan dolores emocionales que se deben atender: Limpal)](https://archivo.contagioradio.com/lideresas-sociales-guardan-dolores-emocionales-que-se-deben-atender-limpal/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde que regresó de la capital, la lideresa acudió al corregimiento de Puerto Libertador donde le realizaron otro seguimiento de prevención por parte de la Secretaría de Salud, tras solicitar y realizar todos los protocolos para que se realice la prueba del Covid, Yina permanece al tanto de la situación pese a no tener todos los síntomas mínimos que se han dado en quienes han contraido el virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De cara a los señalamientos, Yina afirma que adelantará todas las denuncias pertinentes contra quienes dieron inicio a esta persecución e involucraron a los grupos armados que hacen presencia en el corregimiento, pues el día de hoy las persecuciones son contra ella, pero mañana puede ser contra cualquier otra persona. [(Le puede interesar: Asesinan a la lideresa Carlota Salinas en San Pablo, Bolívar)](https://archivo.contagioradio.com/asesinan-a-la-lideresa-carlota-salinas-en-san-pablo-bolivar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Hasta la fecha en el departamento de [Córdoba](https://www.justiciaypazcolombia.com/?s=C%C3%93RDOBA) no se ha confirmado ningún caso de Covid-19, sin embargo desde el pasado 16 de marzo se decretó toque de queda entre las 7:00 p.m. y hasta las 6:00 a. como forma de evitar la propagación del virus.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
