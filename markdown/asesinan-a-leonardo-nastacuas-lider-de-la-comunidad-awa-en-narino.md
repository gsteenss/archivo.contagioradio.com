Title: Asesinan a Leonardo Nastacuas, líder de la comunidad Awá en Nariño
Date: 2019-01-13 16:28
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Asesinato de indígenas, nariño, Pueblo Arhuaco
Slug: asesinan-a-leonardo-nastacuas-lider-de-la-comunidad-awa-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Captura-de-pantalla-2018-12-04-a-las-5.21.28-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 13 ene 2019 

[El pasado sábado 12 de enero fue confirmado el asesinato de Leonardo Nastacuas Rodríguez, líder indígena del resguardo de Cuascuabi del pueblo Awá, en el municipio de Ricaurte, Nariño, en lo que va del 2019 ya han sido reportados dos homicidios contra miembros de la comunidad.  
] [  
Reportes preliminares señalan que varios desconocidos ingresaron en la vivienda del   
líder indígena **Leonardo Nastacuas Rodríguez** y dispararon con armas de  fuego contra su humanidad ocasionándole la muerte. [(Le puede interesar: Comunidad indígena Awá denuncia amenazas a sus líderes)](https://archivo.contagioradio.com/unidad-indigena-del-pueblo-awa-denuncia-amenazas-a-sus-lideres/)  
  
Mientras el caso está siendo investigado por unidades de inteligencia de la Policía Nacional, la víctima de 36 años y padre de una niña de dos continúa en la  a morgue del hospital de Ricaurte, Nariño, lugar en el que será entregado a su familia. [(Lea también: Gobernador indígena Awá, víctima de atentado en Barbacoas, Nariño)](https://archivo.contagioradio.com/gobernador-indigena-awa-victima-de-atentado-en-barbacoas-narino/)  
  
**El 2018 cerró con la muerte de 34 integrantes de la etnia, la cual ha reiterado a los actores armados que hacen presencia en la región que se respete su territorio**, así como sus vidas, un llamado que también han realizado con anterioridad hacia el Gobierno para que garantice la integridad de los miembros del cabildo.   
]

###### Reciba toda la información de Contagio Radio en [[su correo]
