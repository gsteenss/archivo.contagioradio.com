Title: Los aportes de la cultura a la construcción de Paz
Date: 2016-09-09 17:18
Category: Cultura, Otra Mirada
Tags: 2° Festival de Cine por los Derechos Humanos, cine y derechos humanos, Patricia Ariza directora teatro, teatro, teatro la candelaria
Slug: los-aportes-de-la-cultura-a-la-construccion-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/CULTURA1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio radio] 

###### [9 Sept 2016] 

¿Cuántas tamboras habrá callado la guerra? ¿cuántas flautas añoran volver a las bocas de sus dueños? ¿cuántas plazas se quedaron sin teatreros? ¿Cuántas canciones murieron con sus cantores? El conflicto armado en Colombia no solo ha desplazado, desaparecido, torturado y asesinado, el despojo de la vida ha significado la expropiación de las tradiciones y el entierro de las raíces. Pero también ha demostrado que **la cultura esquiva al olvido y para muchos y muchas, se ha convertido en trinchera desde donde resistir o desde donde exorcizar las tristezas**.

Son diversos los escenarios que le apuestan, a partir de los lenguajes de la cultura, a defender y visibilizar los derechos humanos, **la pintura y el cine, han encontrado caminos para  contar los miedos y las alegrías**. La Cultura se ha transformado en vehículo de reivindicaciones y en el lugar de encuentro para las historias y la reconstrucción de memoria, lo que no se podía nombrar ahora recobra voz.

¿Qué tiene el arte y la cultura que le facilita a las personas expresar sus vivencias? Tal vez sea **la cultura la excusa para encontrarnos con el otro** y empezar en conjunto a tejer un país en donde quepan todos los sueños, desde las casas en el aire hasta los tambores del Putumayo, sin miedos ni tristezas, todo al son de la alegría**.**

<iframe id="audio_12846263" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12846263_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
