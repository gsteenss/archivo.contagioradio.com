Title: Comunidad de Gachantiva dispuesta a quitar el freno a su Consulta Popular
Date: 2018-02-12 16:24
Category: Ambiente, Nacional
Tags: consulta popular, Gachantiva, Registraduría Nacional
Slug: comunidad-de-gachantiva-dispuesta-a-quitar-el-freno-a-su-consulta-popular
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Gachanti567.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikipewdia] 

###### [12 Feb 2018] 

Habitantes del municipio de Gachantiva, en Boyacá le solicitaron a la Registraduría Nacional que les responda cuánto cuesta realizar la consulta popular en su territorio para concertar con el gobierno departamental y la alcaldía municipal, **acciones que les permitan recaudar el dinero y llevar a cabo la consulta**.

De acuerdo con Clara Ángel, integrante del comité promotor por el no en la consulta, este mecanismo fue suspendido por el anuncio del Ministerio de Hacienda de no dar los recursos para que se ejecute, sin embargo, **afirma que detrás de esas declaraciones, están los intereses del gobierno y el respaldo a la minería**.

“El gobierno ha visto que en todas partes donde se hace consulta popular siempre gana el no a la minería y el gobierno está interesado en apoyar la minería a toda costa, entonces ahora se inventó que no tienen plata” afirmó Ángel. (Le puede interesar: ["Sancionan a Min Hacienda por impedir consulta popular en Córdoca, Quindío"](https://archivo.contagioradio.com/consulta_popular_quindio_mauricio_cardenas_minhacienda/))

### **La extracción de cemento en el territorio acabaría con el habitad** 

Las dos empresas que, de acuerdo con la comunidad, están interesadas en realizar extracción de cemento en el territorio son Cementos Tequendama y Sumicol, que tendrían ya los títulos mineros. En una reunión de socialización del proyecto, le informaron a la comunidad que se **explotarían 60 mil toneladas mensuales de piedra caliza al mes y que se realizarían hueco de 160 metros de profundidad.**

Clara Ángel afirmó que de ponerse en marcha estos intereses, no solo se acabaría el vertimiento de agua para una de las ciudades turísticas más importantes como Villa de Leyva, sino que también se afectarían los corredores ambientales que c**omunican el Santuario de vida y flora Iguaque con la Serranía del Peligro**, debido a que los lugares en donde se encuentran los títulos abarcan estos caminos ecológicos. (Le puede interesar: ["El Cármen de Chucurí seguirá en lucha para lograr Consulta Popular"](https://archivo.contagioradio.com/carmen_de_chucuri_consulta_popular/))

De igual forma, la vocera afirmó que el 86% de la población es de tradición agrícola en el municipio, razón por la cual la afectación al territorio acabaría con la producción de miles de familias, hecho que se extendería a otros municipios como Ráquira, Sáchica y Sutamarchan, por los afluentes del río que nacen la mayoría en la Serranía del Peligro.

<iframe id="audio_23735333" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23735333_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
