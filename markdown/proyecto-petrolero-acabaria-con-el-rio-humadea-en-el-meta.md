Title: Proyecto petrolero acabaría con el Río Humadea en el Meta
Date: 2017-01-25 11:30
Category: Ambiente, Nacional
Tags: Ecopetrol, Meta, Río Humadea
Slug: proyecto-petrolero-acabaria-con-el-rio-humadea-en-el-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/rio-humadea.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: portalguamal] 

###### [25 Ene 2017]

El río Humadea es uno de los más limpios y conservados del pie de monte llanero y por ello se ha constituido como uno de los principales destinos turísticos y fuentes de agua limpia con el que cuenta la región de la Orinoquía colombiana, **sin embargo la pretensión de Ecopetrol y REPSOl es construir los proyectos TRONGON I y LORITO I** para la extracción de petróleo en los alrederores de la rivera del "Humadea".

Otro de los agravantes es que el municipio de Guamal en el Meta, ha sido considerado y declarado como una zona especial de conservación del **parque natural de Sumapaz**, del que también se provee de agua la ciudad de Bogotá. Según la comunidad **el 60% de su territorio no se debería tocar con proyectos extractivos porque se está afectando el ecosistema de páramo y su zona de amortiguamiento**. ([Lea también La pelea de la USO por mejores condiciones laborales](https://archivo.contagioradio.com/uso-se-toma-instalaciones-de-ecopetrol/))

Por si esto fuera poco el Concejo Municipal y la alcaldía han respaldado las acciones de la empresa que además han sido inconsultas y no se han socializado. Según el relato de uno de los habitantes el **Concejo emitió una resolución en la que cambia la naturaleza de la vereda Pio XII de tradición agropecuaria a zona de ampliación urbana**, situación que facilita el trabajo de construcción que realiza Ecopetrol. ([Le puede interesar: Los atencedentes de Ecopetrol en la Orinoquia](https://archivo.contagioradio.com/en-paujil-caqueta-habra-consulta-popular/))

Además, en esa misma vereda se ha encontrado que existe un acuífero subterraneo considerado uno de los mayores encontrados en la región, que también estaría en peligro pues **Ecopetrol le compró la finca a la fundación a la que pertenecía y que pretendía construir un proyecto de vivienda de interés social** que no fue aprobado por la autoridad municipal.

Según Edgar Humberto Cruz, por estas razones la comunidad debe pronunciarse de manera urgente pues está en riesgo la vocación agrícola del municipio de Guamal así como una de las principales y pocas fuentes hídricas que le quedan a la Orinoquía. La convocatoria se está realizando a las **8 de la mañana desde la Vereda Pio XII y la finca “El Amparo” hacia el parque central de municipio**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
