Title: Muchas familias de apellido Samboní
Date: 2016-12-19 11:58
Category: Opinion, Tatianna
Tags: feminicidio, Uribe Noguera, Yuliana Samboní
Slug: muchas-yuliana-samboni
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/madre-de-yuliana-samboni-es-una-situacion-muy-dolorosa-530204.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: colombia.com 

#### **Por [Tatianna Riveros](https://archivo.contagioradio.com/tatianna-riveros-2/)- [@[tatiannarive]

##### 19 Dic 2016

No leí la entrevista del hermano de Uribe Noguera. Tampoco he sido capaz de revisar las pruebas infiltradas en los medios sobre el caso de Yuliana, nombre que por cierto no debió ser revelado. Fotos, vídeos, historia clínica y hasta notas escritas a mano circulan en las redes y en las noticias.

No voy a dedicar estas letras a Rafael, ni a Francisco ni a Catalina para no correr con la mala suerte de que crean que los defiendo, pero no les han podido comprobar ni han aceptado su participación en los hechos. Por eso y porque no me interesa tampoco posar de correcta.

Este escrito es para familias como la de Yuliana, que viven en situación de vulnerabilidad: no solo de una élite que se cree con el poder suficiente para acabar con la unión, la tranquilidad, el amor y regocijo de un hogar, sino de una sociedad que alienta a la venganza , a la violencia, a devolverle el sufrimiento que sufren a otros.

Familias como las de Yuliana hay muchas, desplazadas víctimas de la violencia, del conflicto armado, de la indiferencia, de la falta de oportunidades y de la falta de atención. Fallamos con el estado social de derecho, fallamos en el ideal de que todos somos iguales ante la ley. Fallamos creyendo que era un caso de opinión pública y que todos teníamos el derecho a juzgar, a culpar, a hablar, a medir y a concluir. Sólo las dos familias pueden hablarnos del dolor que están sintiendo. Sólo la Fiscalía debería hablarnos de las pruebas del caso. Sólo un juez debería decirnos quién o quiénes son los culpables, sólo ese mismo juez deberá decirnos cuál es la pena. Nosotros, somos espectadores y hoy somos participes de ese juego, somos culpables de la desinformación, de la falta de seriedad y la falta de rigor.

Nos quedamos en la indignación, en la réplica del odio, en la difusión del chisme, de la mentira, de subir el rating de los noticieros y de hacer populares a quiénes "desenmascaran" el caso.

¿Acaso a la familia de Yuliana le molesta una entrevista a la familia del victimario? ¿Acaso ellos pregonan el mismo odio que estamos viendo en redes o que incluso vimos en la Clínica donde estaba interno Rafael? ¿Acaso la familia de Rafael ha negado la responsabilidad de su hijo, hermano, cuñado, cercano?

La lección que nos da la familia de Yuliana va mucho más allá de todo lo inimaginable y debe ser el punto de partida para iniciar un cambio, no porque sea la única, al contrario y para nuestra vergüenza hay muchas familias con el apellido Samboní. Y por el "apellido" me refiero a que son muchas las que viven su tragedia. No he visto a la familia de Yuliana pidiendo pena de muerte, castración química y cadena perpetua para Rafael, ellos sólo claman Justicia.

Familias enteras claman justicia no sólo por crímenes sin resolver sino por reclusos sin condenar. Familias enteras viven con un salario paupérrimo, viven en zonas de invasión, familias enteras viven en silencio el drama de tener un victimario entre ellos, sienten la ira y el desprecio de los demás.

La pregunta sincera es ¿qué estamos haciendo por esas familias Samboní y todas las demás?

Este, mi espacio , está dedicado hoy para extender un saludo y un abrazo inmenso para Juvencio, para Nelly , para Nicol Sofía, para toda la familia de Samboní, que sepan que no están solos. Pero no es revictimizando a esta familia que debemos acompañarlos, es exigiendo celeridad, serenidad y conciencia. Es pedir respeto por los unos y por los otros.  
Seamos garantes de un proceso, seamos vigilantes, seamos ciudadanos veedores.

Seamos críticos pero no de los que confundan odio, que de esos ya tenemos hasta políticos.

Que no nos cegue la sed de venganza, que la pasión no nos haga perder la razón, que no condenemos a alguien porque "todo apunta a que" "todo parece que" "eso siempre es que". Que no perdamos el rumbo, porque a diario lo estamos buscando.
