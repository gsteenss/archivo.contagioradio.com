Title: Las 26 demandas sobre el Código de Policía que debería resolver la Corte Constitucional
Date: 2017-07-31 13:01
Category: DDHH, Nacional
Tags: código de policía, Corte Constitucional, Policía Nacional
Slug: las-26-demandas-sobre-el-codigo-de-policia-que-deberia-resolver-la-corte-constitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/codigo-de-policia-e1501524057795.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [31 Jul 2017] 

A partir del primero de agosto comenzarán a regir las sanciones económicas y terminará el proceso pedagógico de las normas que contempla el Nuevo Código de Policía. Sin embargo aún hay 26 demandas presentadas ante la Corte Constitucional por parte de la ciudadanía y de organizaciones sociales que **afirman que el código le otorga poderes adicionales a la Policía Nacional.**

Según el Representante a la Cámara, Alirio Uribe, **“se presentaron en un principio 60 demandas contra el Código de Policía y algunas fueron rechazadas”**. Sin embargo, ya hay fallos que regulan temas como la posibilidad de que los Policías ingresen a las viviendas sin una orden judicial, la invasión al espacio público y las multas y sanciones a los vendedores ambulantes.

### **Sobre lo que ya ha fallado la Corte Constitucional** 

Este organismo judicial, estableció que la Policía podrá ingresar a las viviendas sin una orden judicial siempre y cuando **existan las condiciones que permitan este ingreso** y un juez evaluará los motivos para entrar al inmueble y si estos se encuentran dentro de los parámetros del Código de Policía. (Le puede interesar: [6 cambios que la Corte Constitucional le ha hecho al nuevo Código de Policía"](https://archivo.contagioradio.com/corte_constitucional_cambios_codigo_policia/))

Igualmente, frente a la invasión del espacio público y de los vendedores ambulantes, la Corte manifestó que la **Policía debe respetar los derechos de los vendedores** y las multas sólo se pueden hacer efectivas en el momento que las alcaldías dispongan de mecanismos de reubicación.

Ante esto, según Alirio Uribe “**en muchas zonas de Bogotá hay desalojos forzados de los vendedores ambulantes** y el Código incluye sanciones a los Policía que cometan abusos contra ellos”.  Él ha manifestado en repetidas ocasiones que es necesario que se respeten los derechos constitucionales al trabajo que tienen los vendedores ambulantes ante la implementación de políticas de recuperación del espacio público.

### **¿Qué le falta a la Corte por fallar?** 

La Corte Constitucional aún no se ha pronunciado frente a las demandas interpuestas con relación a temas como la **reglamentación a la protesta, los derechos de los habitantes de calle, la intervención del espacio público de los adolescentes**, la disolución de una manifestación cuando haya “alteración a la convivencia” y el trámite para pedir permiso para realizar una protesta con un “fin legítimo”, entre otras. (Le puede interesar: ["Corte Constitucional restringe los "súper poderes" del Código de Policía"](https://archivo.contagioradio.com/corte-constitucional-restringe-los-super-poderes-del-codigo-de-policia/))

El Representante explicó que se debe garantizar el derecho a realizar protestas instantáneas en la medida que en las ciudades “**las personas se molestan por ejemplo con Transmilenio y se lanzan a la vía a protestar”**. Igualmente, manifestó que cuando se anuncia con anterioridad la realización de una protesta “no es para que la Policía reprima a los manifestantes sino para que se tomen medidas que garanticen que la movilización se va a hacer sin que se afecten los derechos de terceros”.

Uribe expresó que se ha insistido en que el Código de Policía debe cambiar para que la **Policía se acerque más a la ciudadanía** y para que la norma no “ponga a la Policía a pelear con todo el mundo”. Indicó que en los barrios popular hay estigmatización contra los jóvenes que son vistos como delincuentes. (Le puede interesar: [Ya son 24 demandas al Código de Policía"](https://archivo.contagioradio.com/ya-son-24-las-demandas-al-codigo-de-policia/))

El Representante hizo una invitación a que los ciudadanos lean el código de Policía para que **conozcan sus derechos y deberes y así mismo puedan hacer un control social** de las responsabilidades que tienen los Policía al momento de hacer cumplir la ley.

### **¿Qué puede hacer un ciudadano si siente que la aplicación del código de policía viola sus derechos?** 

Aunque se ha manifestado que las multas o sanciones impuestas por los Policías una vez entre en vigencia el código de Policía, serán atendidas en primera instancia por las inspecciones de policía, que hasta febrero solamente en Bogotá eran 63, los ciudadanos tendrían varias opciones para contrarrestar lo que algunos llaman el **"poder extra de los miembros de la Policía".**

Una de las posibilidades es **acudir ante las inspecciones de Policía con las querellas sobre la sanción que le haya sido impuesta.** Se espera que las "comparenderas" que todavía no se conocen, estén conectadas con las inspecciones para que, una vez sean impuestas, se puedan resolver los conflictos que se presenten.

Otra de las opciones **es la segunda instancia** que esta a cargo de autoridades distritales especiales. Sin embargo, estas autoridades no estarían definidas por lo que es necesario que se reglamente de manera clara esa posibilidad.

Por último, los ciudadanos también podrán apelar a las **demandas disciplinarias contra los funcionarios**, o levantar cargos penales cuando el uso de la autoridad afecta la integridad de los ciudadanos o ciudadanas.

<iframe id="audio_20086329" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20086329_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
