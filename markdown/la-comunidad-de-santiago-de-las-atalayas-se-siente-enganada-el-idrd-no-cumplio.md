Title: La comunidad de Santiago de las Atalayas se siente engañada, el IDRD no cumplió
Date: 2019-07-10 10:51
Author: CtgAdm
Category: Comunidad
Tags: Alcaldía, Bogotá, bosa, IDRD
Slug: la-comunidad-de-santiago-de-las-atalayas-se-siente-enganada-el-idrd-no-cumplio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/SANTIAGO-DE-LAS-ATALAYAS-BOSA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

 

###### [Foto: Google Maps] 

Los habitantes del barrio Santiago de las Atalayas en Bosa-Bogotá, denuncian que la remodelación hecha por la Alcaldía Distrital al parque de la comunidad, está incompleta. Según la comunidad tanto el gobierno Distrital como el IDRD prometieron un espacio que contaría con canchas de fútbol, patinódromo y adecuaciones a los senderos peatonales, pero lo único que se ve es un pequeño arreglo en la zona de juegos infantiles.

José María Arboleda, párroco de la Iglesia Juan Pablo II señala que “le venden a la gente la idea de un parque completo y salen al final con una intervención mínima, ese es el problema de la comunidad”. (Le puede interesar: ["A pesar de protestas, Alcaldía adelanta obras en el humedal Tibabuyes")](https://archivo.contagioradio.com/a-pesar-de-protestas-alcaldia-de-bogota-adelanta-obras-en-humedal-tibabuyes/)

### **La comunidad está siendo engañada e ignorada** 

Entre algunas de las dificultades que expresan los habitantes se encuentra la eliminación de una carpa que es empleada para múltiples actividades, en las que participan ancianos, niños e integrantes del barrio. Sin embargo, el proyecto prometido por la Alcaldía no cuenta en sus planes ni con la carpa ni con un espacio semi cubierto.

Previamente a la remodelación del parque se presentaron reuniones entre el IDRD y la comunidad y el Padre Arboleda expone que inicialmente se realizó una encuesta en donde el 64% de la comunidad manifestó el deseo de contar con un espacio para actos culturales y religiosos, la entidad distrital no está teniendo en cuenta las necesidades de la comunidad.

Asimismo, Arboleda menciona que la comunidad se organizó en compañía de la iglesia en mesas de trabajo, de las cuales hacen parte todos los habitantes del barrio, con el fin de sentar una voz de protesta y exigirle tanto a la Alcaldía Distrital como al IDRD, el cumplimiento de la intervención pactada.

El pasado domingo 7 de julio se realizó un plantón alrededor de la carpa del barrio Santiago de las Atalayas, a pesar de ello la comunidad deberá retirar la carpa antes del próximo 9 de agosto a petición de la Alcaldía Local de Bosa, por su parte el IDRD no se pronunció, debido a la ausencia de la entidad distrital la comunidad teme que no se realicen más adecuaciones al parque.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38150745" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38150745_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
