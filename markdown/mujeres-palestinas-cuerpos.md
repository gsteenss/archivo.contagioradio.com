Title: La tierra, el cuerpo y las mujeres palestinas
Date: 2017-11-10 15:11
Category: Onda Palestina
Tags: Apartheid, BDS, Palestina
Slug: mujeres-palestinas-cuerpos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Palestina.org 

###### 10 Nov 2017 

Las mujeres palestinas mantienen formas de resistencia cuyo origen está fuertemente arraigado a la tierra. La relación cuerpo territorio tiene que ver con una forma de comprender las corporalidades atravesadas por los conflictos y opresiones que ocurren en los sitios habitados.

En relación a la violencia sexual que afrontan las mujeres palestinas en las tierras bajo ocupación, existe una reflexión hecha por las feministas palestinas Nadera Shalhoub-Kervorkian, Sarah Ihmoud y Suhad Dahir-Nashif, donde señalan que: “Como feministas palestinas, afirmamos que el imaginario del movimiento sionista de conquista y colonización del cuerpo palestino es inseparable del proyecto de conquista y colonización de la tierra palestina y erradicación de la presencia indígena”.

En respuesta a esto, existe un cuidado mutuo que procura velar por la seguridad de los cuerpos de las mujeres. Es decir, existe una profunda resistencia basada en el afecto y preocupación mutua entre hermanas, amigas y madres frente a la violencia contra sus cuerpos-territorios, que lleva a cabo el hipermasculinizado ejército sionista.

Por ello, Nadera, Sarah y Sujah concluyen señalando que “Como feministas preocupadas por la seguridad de los cuerpos y las vidas de las mujeres, la continuidad de nuestro pueblo y de nuestras generaciones futuras, hacemos un llamamiento local e internacional a las feministas para que se unan a nuestra lucha, desafíen la cultura de impunidad de los asentamientos coloniales y alcen sus voces contra los crímenes en curso del Estado israelí”.

En esta emisión de Onda Palestina además tendremos información de las acciones en contra de la celebración de los 100 años de la declaración de Balfour, hablamos con los creadores de la historieta Aisha: La última refugiada, tocamos el tema del boicot cultural a propósito del patrocinio de un festival de danza por parte de la embajada israelí en Bogotá y escuchamos mucho rap tanto solidario con palestina como hecho por palestinos.

<iframe id="audio_21975516" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21975516_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio.
