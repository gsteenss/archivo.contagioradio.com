Title: Desplazamiento y amenazas de grupos armados en zona rural de Buenaventura
Date: 2020-01-07 16:58
Author: CtgAdm
Category: DDHH, Nacional
Tags: AGC, buenaventura, Chocó, ELN, paramilitares
Slug: desplazamiento-y-amenazas-de-grupos-armados-en-zona-rural-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/zona-rural-de-buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio  
] 

Desde el pasado 2 de enero, comunidades del Río Raposo en la zona rural de Buenaventura (Valle del Cauca) fueron advertidas sobre una intención del Ejército de Liberación Nacional (ELN) de tomar el control de todo el afluente, lo que significa enfrentarse a la Columna Jaime Martínez, de las disidencias de las FARC, que domina la parte baja del Río. Según advierte en un comunicado el **Consejo Nacional de Paz Afrocolombiano (CONPA)**, las disidencias respondieron enviando más armados al territorio, lo que generó el desplazamiento de 20 personas por temor a los enfrentamientos que puedan presentarse.

### **Un territorio colectivo amenazado por intereses armados** 

El Río Raposo hace parte de la zona rural del Distrito Especial de Buenaventura, su titulación es de carácter colectivo, y pertenece a **cerca de 2.300 personas que integran 13 comunidades negras del Consejo Comunitario del Río Raposo.** La Columna Jaime Martínez hace presencia en la zona baja del Río hace más de 2 años, mientras que el ELN hace presencia intermitente en la zona alta del afluente desde finales de 2018.

Según denuncia el comunicado del CONPA, el pasado 2 de enero sobre las 3 de la tarde **integrantes del ELN llegaron a la comunidad El Tigre, ubicada en la zona baja del Río, para comunicar su intención de tomar el control de todo el lugar.** Incluso, una lancha que subía en ese momento por el afluente fue atacada a tiros, por supuestamente pertenecer a la Columna Jaime Martínez, teniendo el motorista (su único ocupante) que dar la vuelta y saltar de la embarcación con rumbo desconocido.

"En esta misma comunidad, varias personas de otras veredas, que estaban visitando a sus familiares y bañando en el río, fueron retenidas y posteriormente las dejaron ir", afirma el Consejo. Posteriormente, en la madrugada del 3 de enero, por la comunidad de Cocalito subieron dos lanchas con personas que, al parecer, integran las disidencias de las FARC en la zona. (Le puede interesar: ["Atentan contra Carlos Tobar, integrante del Comité del Paro Cívico por Buenaventura"](https://archivo.contagioradio.com/atentan-contra-carlos-tobar-integrante-del-comite-del-paro-civico-por-buenaventura/))

A raíz de estos hechos, el mismo día, **se desplazaron 20 personas desde la comunidad de Guadualito hacía la comunidad de Cocalito, por temor a posibles enfrentamientos** entre las dos estructuras armadas que se disputan la zona. Según se advierte en el comunicado, "es posible que situaciones similares se estén presentando o puedan presentarse en otros ríos de Buenaventura". (Le puede interesar: ["Buenaventura: ¿La ciudad que encarna el Estado fallido?"](https://archivo.contagioradio.com/buenaventura-la-ciudad-que-encarna-el-estado-fallido/))

### **Buenaventura, Bajo San Juan y Bajo Calima: Alerta por vulneraciones de derechos  
**

En una alerta emitida por la Comisión Intereclesial de Justicia y Paz se denuncia la llegada de cinco hombres armados que se presentaron como paramilitares el pasado 1 de enero, en el muelle de Docordó, cabecera municipal del Litoral San Juan, Chocó. Los armados advirtieron que iniciarían el control territorial, ello pese a que el muelle se encuentra a 150 mts. de la plaza central de Docordó, donde horas antes se había posesionado el Alcalde en medio de presencia de la Infantería de Marina y Policía Nacional.

Adicionalmente, la Organización afirmó que **"los presuntos paramilitares anunciaron que tomarían control territorial en el Bajo San Juan y el Bajo Calima en Buenaventura,** Valle del Cauca y el Litoral San Juan, departamento del Chocó". (Le puede interesar:["Control paramilitar de AGC se extiende desde frontera con Panamá, Bajo y Medio Atrato"](https://archivo.contagioradio.com/no-es-solo-bojaya-bajo-atrato-y-dabeiba-blanco-de-operaciones-paramilitares/))

Otra vulneración contra la población civil en Buenaventura tuvo lugar el pasado sábado 4 de enero, cuando la líder del paro cívico de la Ciudad, **María Elena Cortés, fue atacada por sicarios que se desplazaban en moto en el barrio la Transformación.** La Líder logró salir ilesa del atentado gracias a que, aparentemente, se trabó el arma del hombre armado. (Le puede interesar:["Del paro cívico a gobernar Buenaventura, los retos que asume Hugo Vidal"](https://archivo.contagioradio.com/del-paro-civico-a-gobernar-buenaventura-los-retos-que-asume-hugo-vidal/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
