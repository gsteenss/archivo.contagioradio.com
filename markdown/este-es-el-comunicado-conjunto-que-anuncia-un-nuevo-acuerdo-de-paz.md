Title: Este es el comunicado conjunto que anuncia un nuevo acuerdo de paz
Date: 2016-11-12 18:39
Category: Nacional, Paz
Slug: este-es-el-comunicado-conjunto-que-anuncia-un-nuevo-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/mesa-social-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [12 Nov 2016]

Este sábado desde La Habana se anunció un nuevo acuerdo de paz entre el gobierno y las FARC EP que estará disponible en las próximas horas. A continuación compartimos el comunicado conjunto leído por los garantes.

Comunicado Conjunto \#4

La Habana, Cuba, 12 de noviembre de 2016

El Gobierno Nacional y las FARC-EP atendiendo el clamor de los colombianos y colombianas por concretar su anhelo de paz y reconciliación, hemos alcanzado un nuevo Acuerdo Final para la terminación del Conflicto armado, que integra cambios, precisiones y aportes de los más diversos sectores de la sociedad y que revisamos uno a uno.

La construcción de una paz estable y duradera, objetivo al que responde este nuevo Acuerdo, debe ser el compromiso común de todos los colombianos que contribuya a superar la polarización y que recoja todas las expresiones políticas y sociales.

Invitamos a toda Colombia y a la comunidad internacional, siempre solidaria en la búsqueda de la reconciliación, a acompañar y respaldar este nuevo Acuerdo, y su pronta implementación para dejar en el pasado la tragedia de la guerra. La Paz no da más espera.

Al finalizar el día, los colombianos podrán consultar en la página www.mesadeconversaciones.com.co http://www.mesadeconversaciones.com.co/ un documento en el que se señalan las modificaciones y los nuevos elementos. La integración total de los textos del nuevo Acuerdo Final para la Terminación del Conflicto y la Construcción de una Paz Estable y Duradera quedará disponible en próximos días.
