Title: 2015 año con la tasa más alta de CO2 en la historia
Date: 2016-10-24 17:52
Category: Ambiente, Nacional
Tags: Ambiente, co2, Polución
Slug: 2015-ano-la-taza-mas-alta-co2-la-historia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/contenido.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contenido] 

###### [24 de Oct 2016] 

Anualmente la Organización Mundial de Meteorología publica los niveles de CO2, sin embargo según el último reporte, **el año 2015 tuvo el nivel más alto registrado en la historia,** que alcanzó una medida de 400 partes por millón.  Cabe resaltar que esta cifra había sido alcanzada en diferentes países pero no permanecía durante el año, a diferencia del 2015.

Con **esta cifra los niveles para el 2016 de CO2, podrían incluso superar las anteriores, prueba de ello es el fenómeno del niño**, que para este año ya afectado a diferentes países como Haití o Cuba.  Los científicos aclararon que la disminución de gas carbónico no se da de inmediato y que por el contrario se demora varías generaciones en desaparecer.

El **aumento en los niveles de CO2 se deben en gran medida a las pocas estrategias o alternativas para el cuidado del ambiente** que cada vez más es deteriorado, produciendo que los fenómenos naturales sean mucho más fuertes, al igual que el cambio climático.

De igual manera el informe presentado por la Agencia Internacional de Energía señalo que **6.5 millones de personas mueren en el mundo a causa de la contaminación energética**,  a su vez el documento también indica que Asía y África son las regiones con mayor tasa de mortalidad por polución.

**Las conclusiones de ambos informes, infieren que debe establecerse medidas inmediatas que disminuyan la producción de CO2 y de contaminación energética**, para a su vez que los daños de los fenómenos naturales sean menos graves y se genere una menor mortandad por la contaminación del planeta. Le puede interesar: ["La defensa del ambiente y los derechos humanos en Colombia"](https://archivo.contagioradio.com/la-defensa-del-ambiente-y-los-derechos-humanos-en-colombia/)
