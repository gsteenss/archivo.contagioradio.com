Title: Al pliego del Paro Nacional se suma la negociación del salario mínimo
Date: 2019-12-06 16:46
Author: CtgAdm
Category: Nacional, Paro Nacional
Tags: negociación, Paro Nacional, salario minimo, SIndicatos
Slug: al-pliego-del-paro-nacional-se-suma-la-negociacion-del-salario-minimo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Comité-Nacional-del-Paro-e1575668558169.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @cutcolombia  
] 

Cumplidos 15 días desde que inició el Paro Nacional, integrantes del Comité Nacional que lo convocó hablaron sobre los horizontes inmediatos del mismo, las fechas de movilización y encuentro que están por desarrollarse, así como las próximas convocatorias a movilización. Adicionalmente, se refirieron al **inicio de la discusión sobre el aumento de salario mínimo**, otro reclamo que se suma a una agenda que tiene entre sus principales puntos, los asuntos económicos que afectan a las y los trabajadores.

### **"Seguiremos insistiendo en la movilización y en la conversación"** 

Diógenes Orjuela, presidente de la Central Unitaria de Trabajadores (CUT), aseguró que desde el Comité se sentían felices ante el tamaño de la movilización que se vió en diferentes ciudades del país el pasado 4 de diciembre, y anunció que seguirán insistiendo en "la movilización y la conversación". En ese sentido, recordó el encuentro de organizaciones sociales que se desarrolla en el auditorio León de Greiff de la Universidad Nacional este Viernes 6 de diciembre y el próximo sábado 7 de diciembre, en el que se decidirán temas relacionados con el Paro.

Posteriormente, el domingo 8 de diciembre se realizará el gran concierto \#UnCantoxColombia, con el que artistas en Bogotá y todo el país, recordarán las razones por las cuales el país está en paro. Orjuela señaló que para el día martes, las centrales obreras presentarán su propuesta única sobre el aumento del salario mínimo y realizarán un plantón frente a las instalaciones de Compensar de la Carrera 94 con Autopista Norte. (Le puede interesar: ["Nueva reforma tributaria significará ‘crecimiento económico’ para los de siempre"](https://archivo.contagioradio.com/nueva-reforma-tributaria-significara-crecimiento-economico-para-los-mismos-de-siempre/))

En esa misma semana, el Comité **invitará a realizar una gran toma de Bogotá con cacerolazos para protestar contra la reforma tributaria en el Congreso**, que ya avanzó en su primer debate. (Le puede interesar:["La defensa de la vida, un punto central del Paro Nacional"](https://archivo.contagioradio.com/la-defensa-de-la-vida-un-punto-central-del-paro-nacional/))

### **Las maniobras del Gobierno para empezar a negociar sobre el salario mínimo** 

Respecto al inició de la discusión sobre el aumento del salario mínimo para el próximo año, el Presidente de la CUT sostuvo que el Departamento Administrativo Nacional de Estadística (DANE) le quitó a Planeación Nacional la medición de la productividad, "que es lo que aporta el trabajo al crecimiento de la economía". Según explicó, mientras Planeación tenía un sistema de medición mundialmente generalizado, el DANE usa el sistema de medición de la Organización para la Cooperación y el Desarrollo Económico (OCDE), "en el que al final, los trabajadores le salimos debiendo dinero a los empresarios".

De esta forma, mientras en el sistema de Planeación la productividad laboral está sobre el 3%, con el DANE  no llega al 1%, y mientras la productividad general supera el 1%, de acuerdo a la nueva medición, esta cifra se ubica en 0,39%. La tensión entre estas cifras es de relevancia, puesto que la medición de productividad laboral, así como la relación entre la productividad de los trabajadores y la del capital son factores a tomar en cuenta para determinar el aumento del salario mínimo.

Respecto a este último factor, Orjuela manifestó que era de vital importancia porque siempre la productividad de los trabajadores es más alta que la del capital, pero se toma en cuenta el promedio, de igual forma, pidió que se considerara la inflación para sectores de menores ingresos, que es la inflación más alta, y no el promedio general. El último factor que los sindicatos buscarán que tenga incidencia en la definición del salario para el próximo año es el aumento el poder adquisitivo de la población, buscando a su vez, un aumento en el consumo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
