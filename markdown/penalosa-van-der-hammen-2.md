Title: Peñalosa desconoce dos siglos de investigación sobre Van Der Hammen
Date: 2018-11-01 13:13
Author: AdminContagio
Category: Ambiente, Nacional
Tags: Bogotá, Enrique Peñalosa, Reserva Thomas Van der Hammen, sabana de Bogotá, Universidad Nacional
Slug: penalosa-van-der-hammen-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Reserva-Tomas-Van-der-Hammen-e1457630338748.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: bogotá.gov.co] 

###### [19 Oct 2018] 

El **Instituto de Ciencias Naturales (ICN) de la Universidad Nacional** afirma que, dadas sus condiciones físicas actuales, la Reserva Thomas Van Der Hammen es "el único sitio que permite el mantenimiento de la vital conexión entre los cerros orientales y la parte occidental incluyendo el río Bogotá", argumentando que con la intervención de la Reserva, **la Administración Peñalosa está desconociendo la relevancia de este lugar para el equilibrio ecológico de la sabana.**

En un comunicado, el ICN recuerda que en los estudios sobre ese ecosistema de los años 1801, 1962, 1965 y 2010, se señalaba **"la importancia de declarar y conservar esta zona de protección de la Sabana de Bogotá"**. En el documento se hace referencia a más de 50 investigaciones que respaldan esta afirmación, algunos realizadas por el mismo científico cuyo nombre lleva la Reserva.  (Les puede interesar:["¿Ya hay una decisión de la CAR sobre la Reserva?"](https://archivo.contagioradio.com/ya-hay-una-decision-de-la-car-sobre-la-reserva-van-der-hammen/))

El Instituto sostiene que dadas las condiciones actuales de conservación de la zona, el estado del suelo, que fue lecho del lago que abarcaba parte de la Sabana de Bogotá y la ocupación del terreno por parte de campesinos y floricultores; **la Reserva es el único sitio que permite mantener la conexión vital entre los cerros orientales y el occidente de la Sabana**, incluyendo el río Bogotá.  (Les puede interesar: ["Sin garantías para participación ciudadana avanza proyecto sobre Reserva"](https://archivo.contagioradio.com/sin-garantias-para-participacion-ciudadana-avanza-proyecto-sobre-reserva-van-der-hammen/))

Aunque el Instituto reconoce que Bogotá tiene problemas de movilidad, y es necesario pensar en vías que puedan descongestionar la principal salida vehicular por el norte; dicho desarrollo no puede desconocer una zona de la que hacen parte el sistema de humedales (Torca, Guaymaral y La Conejera), 514 especies de plantas inventariadas, 47 ejemplares de mariposas, y que además conserva un inmenso valor cultural y arqueológico, en tanto allí habitó la cultura Muisca.

Por lo tanto, los tramites de realinderación, recategorización y sustracción que adelanta la administración distrital encabezada por Enrique Peñalosa con el fin de urbanizar la Reserva, **desconocen los resultados de más de 200 años de investigación que sustentan la necesidad de preservar dicho ecosistema.** (Les puede interesar: ["La terquedad de Peñalosa no pudo acabar con conectividad de la Reserva"](https://archivo.contagioradio.com/la-terquedad-de-penalosa-no-pudo-acabar-con-conectividad-de-la-van-der-hammen/))

\[caption id="attachment\_57874" align="alignnone" width="730"\][![Estado de la Reserva](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-01-a-las-11.36.25-a.m.1-730x354.png){.wp-image-57874 .size-medium width="730" height="354"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-01-a-las-11.36.25-a.m.1.png) Reserva T.V.D.H.\[/caption\]

\[caption id="attachment\_57871" align="alignnone" width="654"\][![Foto: @Aidaespanol](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-01-a-las-11.38.18-a.m.-796x515.png){.wp-image-57871 width="654" height="423"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/Captura-de-pantalla-2018-11-01-a-las-11.38.18-a.m..png) Proyecto de Transformación de la Reserva. Foto: @Aidaespanol\[/caption\]

> El Instituto de Ciencias Naturales de la Facultad de Ciencias de la Universidad Nacional de Colombia informa a la opinión pública sobre La Reserva Thomas van der Hammen y la redefinición de sus linderos [pic.twitter.com/4Q36UcAwYh](https://t.co/4Q36UcAwYh)
>
> — Instituto de Ciencias Naturales - UN (@ICN\_UN) [30 de octubre de 2018](https://twitter.com/ICN_UN/status/1057068589650534400?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
