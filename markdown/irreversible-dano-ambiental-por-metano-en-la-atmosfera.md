Title: Irreversible daño ambiental por metano en la atmósfera
Date: 2020-07-28 08:12
Author: PracticasCR
Category: Actualidad, Ambiente
Tags: Ambiente, comunidades amazónicas, daño ambiental, Metano en Antártida
Slug: irreversible-dano-ambiental-por-metano-en-la-atmosfera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Metano.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Fuente-MAAP.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Aventure Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una investigación presentada por la revista Proceddings of the Royal Society, científicos descubrieron la primera fuga de gas metano en Cinder Cones, un sitio en el mar de la Antártida a una profundidad de 10 metros. La hipótesis apunta a que el gas podría comenzar a gotear a medida que los océanos se calientan por el calentamiento global y de ser así el daño ambiental sería irreversible. (Le puede interesar: [Niñas y niños exigen sus derechos ambientales ante la ONU](https://archivo.contagioradio.com/ninos-y-ninas-exigen-sus-derechos-ambientales-ante-la-onu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Se cree que debajo del mar de la Antártida hay grandes cantidades de metano y por lo general, microbios lo consumen antes de que este pueda salir a la superficie; el problema es que el crecimiento de los microbios es muy lento en esta zona de la Antártida y podría significar la filtración del gas en la atmósfera. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este momento la liberación de metano en regiones de permafrost es uno de los puntos de inflexión clave que preocupan a los científicos pues cuando estas capas se derriten, liberan muchas toneladas de metano y dióxido de carbono. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ahora, con la aparición de la COVID-19, todas las expediciones se han cancelado y los científicos no podrán llegar a Cinder Cones y determinar cuál podría ser el daño ambiental hasta que la emergencia sanitaria se dé por terminada.

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Qué implica que haya metano en la atmósfera?
---------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los investigadores afirman que la aparición de este gas es un misterio pero probablemente no sea por el calentamiento global ya que en donde se encontró la fuga no ha sido significativo el calentamiento. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, los expertos explican que este fenómeno ocurre solamente cuando un impacto global particular se vuelve imparable.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entonces, que no sea causa del calentamiento global, no significa que no haya daño ambiental pues actualmente la concentración de metano en la atmósfera es considerablemente alta siendo uno de los gases mas generadores de efecto invernadero y esto en consecuencia de actividades que el humano realiza desmedidamente como la deforestación y la extracción de combustibles fósiles, a las que el gobierno se niega a poner un alto. (Le puede interesar: [Más de 300 grupos indígenas de Brasil podrían desaparecer por el Covid-19](https://archivo.contagioradio.com/mas-de-300-grupos-indigenas-de-brasil-podrian-desaparecer-por-el-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

No solo el medio ambiente es víctima de estas actividades
---------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Proyecto de Monitoreo de la Amazonia Andina (MAAP) “hubo un gran incremento después del acuerdo de paz. En el primer vistazo al 2020, estimamos una deforestación de 76.200 hectáreas hasta junio, concentrado en un “**arco de deforestación**” al noroeste de la Amazonía colombiana".

<!-- /wp:paragraph -->

<!-- wp:image {"id":87303,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Fuente-MAAP-1024x801.jpg){.wp-image-87303}  

<figcaption>
Fuente: MAAP

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

La preocupación es que según el Ministerio de Ambiente y Desarrollo Sostenible 53,4% de los bosques naturales del país yacen en territorios étnicos, el 46% son resguardos indígenas y el 7,3% son territorios de comunidades afrocolombianas, y solamente en la Amazonía hay más de 26 etnias indígenas, según SINIC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe recordar que la destrucción del territorio también representa la destrucción de la identidad de los pueblos indígenas ya que para ellos la naturaleza no es simplemente una fuente de materias primas sino su forma de ver y conectarse con el mundo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En un conversatorio realizado por la Comisión de la Verdad, según Mario Alejandro Pérez, en el documento "Conflictos ambientales en Colombia: inventario, caracterización y análisis", en los territorios étnicos donde hay presencia de grupos armados, los conflictos ambientales en cuanto a sectores mineros y de hidrocarburos son altos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, el compromiso por parte del gobierno para proteger a las comunidades y los territorios no se ha evidenciado. En febrero se expidió el Decreto 328 por el cual se fijaron los requisitos para proyectos piloto sobre fracking, sin embargo el gobierno incumplió con las condiciones puestas por el Alto Tribunal pues no contemplaban los impactos no resueltos o compensados y los futuros daños ambientales que podrían generar.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

José Felipe Cuero Cuero, líder de la Comisión Interétnica de la Verdad del Pacífico (CIVP), expreso que “los impactos de la minería ilegal en los territorios colectivos del pacífico caucano (la deforestación, la sedimentación y la contaminación de las fuentes hídricas con mercurio) son irreversibles y se siguen replicando”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Si desea leer la investigación completa de fuga de metano](https://royalsocietypublishing.org/doi/10.1098/rspb.2020.1134)

<!-- /wp:paragraph -->
