Title: “Nuestros desaparecidos no están enmarcados como víctimas del conflicto armado” CONPAZ
Date: 2015-10-19 11:11
Category: Uncategorized
Tags: Acuerdo sobre desaparecidos, Conpaz, Conversaciones de paz de la habana, ELN, FARC, Humberto de la Calle, Juan Manuel Santos
Slug: nuestros-desaparecidos-no-estan-enmarcadas-como-victimas-en-el-marco-del-conflicto-armado-conpaz
Status: published

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_9074364_2_1.html?data=mpWklpiaeI6ZmKiakp6Jd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nktbZ1Nnf0diPqMbnwtXO1MrHrcXj1JDb0ZDJt9WZpJiSo5bSb8bizsbfxcbIpdSfxNTa0ZDaaaSnhqaxj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Rodrigo Castillo, CONPAZ] 

###### [19 Oct 2015]

135 organizaciones y **comunidades que componen la red CONPAZ**, afirman que el acuerdo sobre los mecanismos de búsqueda de personas desaparecidas es una oportunidad para el reconocimiento de este flagelo, pero que no puede dejar de lado la responsabilidad estatal en muchas de las desapariciones ni tampoco desligarse de los derechos a la verdad, la justicia y la reparación integral.

Por esa razón, explica Rodrigo Castillo, no se puede dejar de lado la realidad de que muchas de las víctimas de desaparición forzada no han tenido nada que ver con el conflicto “*Nuestros desaparecidos no están enmarcadas como víctimas en el marco del conflicto armado*” y por no puede haber un ocultamiento de la responsabilidad estatal en las despariciones.

En el comunicado público de 10 puntos las comunidades que integran CONPAZ resaltan también que este acuerdo funcionara su y solo si hay un reconocimiento por parte de las estructuras responsables de los crímenes “*La negación de los responsables  como ha ocurrido en el caso del Palacio de Justicia, en La Escombrera, en Putumayo y en Meta no contribuye al deseo y los derechos de nosotros las víctimas.*” Señalan.

Rodrigo Castillo, representante legal de la organización agrega que es importante que se reconozca que las víctimas tienen derechos, como todas y todos, a una sepultura digna, sean o no combatientes, que las familias tienen derecho a la verdad y a la dignidad, por ello **proponen que cada uno de los sitios en donde se encuentren desaparecidos se convierta en un monumento a la memoria de las víctimas y un homenaje a la verdad.**

Conozca aquí el comunicado público

[Carta Pública CONPAZ Acuerdo sobre UBPD](https://es.scribd.com/doc/285915704/Carta-Publica-CONPAZ-Acuerdo-sobre-UBPD "View Carta Pública CONPAZ Acuerdo sobre UBPD on Scribd")

<iframe id="doc_65789" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/285915704/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="505" height="505" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
