Title: Fuerte represión contra plantón en aeropuerto de Bogotá
Date: 2019-12-07 13:57
Author: CtgAdm
Category: Movilización, Paro Nacional
Tags: aeropuerto, Bogotá, El Dorado, ESMAD
Slug: fuerte-represion-contra-planton-en-aeropuerto-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/ELM_fwSWoAAsDkP.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto]@MoisesAlvaro\_ 

A través de múltiples cuentas en twitter, viajeros, manifestantes y ciudadania en general han hecho visibles las detenciones y ataques por parte de la Policía Nacional y el Escuadrón Móvil Antidisturbios (ESMAD) a manifestantes pacíficos que se encontraban en el aeropuerto El Dorado, con carteles expresando su  respaldo al Paro Nacional. (Le puede interesar:[En veinte años el ESMAD ha asesinado a 34 personas](https://archivo.contagioradio.com/en-veinte-anos-el-esmad-ha-asesinado-a-34-personas/))

> Mucho cuidado con lo que está pasando en el Aeropuerto El Dorado de Bogotá ahora mismo: jóvenes fueron a manifestarse en silencio y con carteles, a lo que la policía se los está llevando... Sigue la represión [\#PlantónEnElDorado](https://twitter.com/hashtag/Plant%C3%B3nEnElDorado?src=hash&ref_src=twsrc%5Etfw)  
> [pic.twitter.com/DLeFLuHqP3](https://t.co/DLeFLuHqP3)
>
> — Juanse Morales (@juansemo) [December 7, 2019](https://twitter.com/juansemo/status/1203376443281022976?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Los manifestantes quienes  habían coordinado este plantón días atrás para las 11:00 am, denunciaron además de los múltiples arrestos  vulneraciones a su intimidad, " se habían coordinado varias personas para hacer un plantón en el aeropuerto a las 11, pero al parece en el grupo de Telegram que hicieron, habían infiltrados, de allí pudieron detener a 4 personas que estaban liderando esta movilización"  denunció Waldir representante de la Unión Nacional de Estudiantes de la Educación Superior (UNEES).

En el plantón se esperaba una concentración de aproximadamente 1000 personas que hacían parte del grupo de Telegram. Hasta ahora se conoce que son 15 las personas detenidas, entre ellas 6 mujeres, una de estas de nacionalidad extranjera.

De igual forma se registra que hasta el momento estas personas no han sido informadas sobre la razón de su detención ni tampoco los cargos; según información suministrada a defensores de DDHH  por Soledad Granada integrante de Paz  a la Calle, y quien también fue retenida en las últimas horas en el aeropuerto, las 15 personas son conducidas al Centro de Traslado por Protección (CPT) .

Noticia en desarrollo...

> <div style="padding: 16px;">
>
>  
>
> <div style="display: flex; flex-direction: row; align-items: center;">
>
> <div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;">
>
> </div>
>
> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
>
> <div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;">
>
> </div>
>
> <div style="background-color: #f4f4f4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;">
>
> </div>
>
> </div>
>
> </div>
>
> <div style="padding: 19% 0;">
>
> </div>
>
> <div style="display: block; height: 50px; margin: 0 auto 12px; width: 50px;">
>
> </div>
>
> <div style="padding-top: 8px;">
>
> <div style="color: #3897f0; font-family: Arial,sans-serif; font-size: 14px; font-style: normal; font-weight: 550; line-height: 18px;">
>
> Ver esta publicación en Instagram
>
> </div>
>
> </div>
>
> <div style="padding: 12.5% 0;">
>
> </div>
>
> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
>
> <div>
>
> <div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);">
>
> </div>
>
> <div style="background-color: #f4f4f4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;">
>
> </div>
>
> <div style="background-color: #f4f4f4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);">
>
> </div>
>
> </div>
>
> <div style="margin-left: 8px;">
>
> <div style="background-color: #f4f4f4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;">
>
> </div>
>
> <div style="width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg);">
>
> </div>
>
> </div>
>
> <div style="margin-left: auto;">
>
> <div style="width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);">
>
> </div>
>
> <div style="background-color: #f4f4f4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);">
>
> </div>
>
> <div style="width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);">
>
> </div>
>
> </div>
>
> </div>
>
>  
>
> [Urgente difusión! DDHH](https://www.instagram.com/p/B5xvxlzlTuM/?utm_source=ig_embed&utm_campaign=loading)
>
> Una publicación compartida por [Desinformémonos Colombia](https://www.instagram.com/desinformemonos_colombia/?utm_source=ig_embed&utm_campaign=loading) (@desinformemonos\_colombia) el <time style="font-family: Arial,sans-serif; font-size: 14px; line-height: 17px;" datetime="2019-12-07T16:06:34+00:00">7 de Dic de 2019 a las 8:06 PST</time>
>
> </div>

<p>
<script async src="//www.instagram.com/embed.js"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
