Title: Hidrosogamoso “huele a podrido”
Date: 2015-01-15 18:52
Author: CtgAdm
Category: Ambiente, Resistencias
Tags: Destacado, hidrosogamoso, La Playa, protesta
Slug: hidrosogamoso-huele-a-podrido
Status: published

**Infecciones en la piel, caída del cabello, nausea y vómitos, son algunas de las enfermedades que se producen tras la puesta en funcionamiento de las compuertas de Hidrosogamoso y la variación constante de los niveles del río.**

Según explican los campesinos, la apertura de las compuertas de la represa provoca la variación de los niveles del río y el pudrimiento de material orgánico, generando gas metano y ácido sulfhídrico, gases altamente nocivos para la población que está sometida a su inhalación constante. Según la información aportada cerca del 15% de la población infantil se está viendo afectada con diversas enfermedades.

Por esta razón, cerca de 70 familias del municipio La Playa, protestan hoy, de manera pacífica, y exigen la reubicación, dado que el caserío está ubicado a tan solo 800 metros de la pared de la represa y la zona es de alta sismicidad, según lo advierte la propia licencia ambiental otorgada a la construcción.

Clara, una de las campesinas, denuncia que hay que evitar un desastre, puesto que en la zona hay cerca de 37 fallas geológicas, y a 70 kilómetros se ubica uno de los nidos sísmicos más activos del mundo. “Un temblor puede quebrar las paredes de la represa y causar una tragedia”, advierte clara.

Otra de las preocupaciones es que ante la protesta pacífica de los campesinos, la policía, la SIJIN y otros organismos de seguridad han hecho presencia e intentan impedir que los campesinos instalen pancartas a pesar de no estar bloqueando ninguna de las vías.

####  
