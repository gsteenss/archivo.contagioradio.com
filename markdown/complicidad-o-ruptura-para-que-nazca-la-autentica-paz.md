Title: Complicidad o ruptura para que nazca la auténtica paz
Date: 2017-01-28 06:30
Category: Camilo, Opinion
Tags: acuerdos proceso de paz
Slug: complicidad-o-ruptura-para-que-nazca-la-autentica-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### Por [Camilo de las Casas ](https://archivo.contagioradio.com/camilo-de-las-casas/) 

La Pax Neoliberal se sostiene sobre la base de una democracia que asegura la inversión privada con una simulada inclusión social y una política asimétrica moldeada a instancias del Banco Mundial y el gran peso de las corporaciones

**Los llamados procesos de paz con las FARC EP, el ELN, y el que se debería abrir con el EPL**, pueden ser el comienzo de una eventual fase de sensibilización y politización ciudadana para ser parte de otro proyecto democrático, en medio de la Pax neoliberal. La condición de lo nuevo es asumir esa realidad cultural y espiritual neoliberal que excita la fragmentación del sujeto individual y el sujeto social para reiventar la otra paz, aquella que es democracia social, cultural, económica, ambiental participativa y deliberativa.

La Pax Neoliberal pretende garantizar la conducción militar policial a la seguridad inversionista planteada desde el gobierno de Pastrana y diseñada en su última fase de la seguridad democrática como control social territorial.

**Las Zidres, la política minero energética y los límites a los derechos ambientales** acompañadas de código de policía, una liberalización de los derechos sexuales cuando es conveniente, la inclusión de los derechos de los animales, de las iglesias son parte de lo estratégico y fragmentario.

La cultura ciudadana afectiva se mueve entre núcleos de identidad en la música, el cine, la bici, los animales, los bosques, el agua, la farra, la anticorrupción, el fútbol, todo ello sin conexión afectiva pero si efectiva en el inconsciente  del ascenso social del complejo de pirámide con cualquier medio en el ejercicio de poder.

Esa cultura ciudadana es distante de los reconocimientos propios de los mecanismos de represión y falsa libertad heredada en núcleos familiares, en mundos doctrinales e ideologizados político religiosos. Esas falsas seguridades están distantes de las almas de los pueblos,  y legitiman las nuevas formas de paramilitarismo y o grupos armados que  protegen  la riqueza legal e ilegal o amasada para asegurar el acaparamiento y la inversión extranjera rural.

**Esas son las reglas de juego de la construcción de una sociedad hacia la paz**, esos son los dilemas o se reconoce la Pax neoliberal sus rasgos culturales y espirituales o será imposible el cambio.

<div dir="auto" style="text-align: justify;">

Los alcances de la mesa de La Habana y lo que ojalá se logre en la mesa de Quito son bisagras para un eventual renovación de la cultura política colombiana signada por la violencia de las élites, la corrupción, el maniqueísmo judeo cristiano (católico, protestante y los diversas iglesias evangélicas) y al afrontamiento del desarrollo de la política y la economía con el paramilitarismo de Estado y el paramilitarismo privado.  
La Comisión de la Verdad, la Jurisdicción Especial de Paz y en general el Sistema Integral son parte de un escenario nuevo, que aún, con los recortes que se ha asegurado el ejecutivo con el legislativo, posibilitara, dependiendo de los actores sociales que se involucren, la comprensión y deconstrucción de los motivos y los límites de la rebelión armada  y en particular la deslegitimación del terror del establecimiento como mecanismo de cimentación de la verdad y de la justicia.  
Si ese escenario abierto es creativamente asumido, la vuelta a la ética del buen existir como cimiento de la política supondrá rupturas en el modo de ser en su conjunto, citando a M Mafessoli, para que sea posible lo nuevo.  
Actuar sin previamente ver, hará inviable lo nuevo. Trump es otra forma de Obama o Clinton más privatizada, es la expresión del rompimiento de las fronteras de las formalidades de Estado y sin matices el corporativismo en el sentimiento nacionalista, que se muestra capaz de satisfacer osadamente las necesidades de trabajo, de ingreso y ascenso. Ver esa realidad supone más que reconocer una crisis del modelo global un reacomodamiento económico en el  mundo de la imagología del que habla Kundera.  
Ese factor económico cultural es muy cercano a nuestra realidad que puede mostrarse más hipócritamente pecata pero obsesivamente ambiciosa de poder castigar, vengar,  ascender, rezar, gozar en evasión de la propia tragicomedia.  
La identificación de las motivaciones reales de los diversos actores del cambio en un proyecto de país incluyente sería la expresión de una madurez política que aún es distante por el enanismo y la mirada estrecha que sigue carcomiendo lo nuevo e imposibilitando la ruptura cultural espiritual. Sin la transparencia de conciencia en el ejercicio de poder como poder conciente dichas motivaciones son fácilmente seducidas por un ejercicio de la política tradicional.  
Mentes abiertas a la autocrítica y la críticas en las llamadas izquierdas y sus líderes, a su formas y modos de ejercicio del poder, a su laxitud hacia la corrupción, al amasijo de riqueza a costa de la política en tierras y otro tipo de acumulación, a la hipocresía con puñaladas traperas y falsas lealtades al establecimiento al que dicen cuestionar son parte del orden armonioso por reestablecer.  
Sin un rompimiento de las exclusiones basadas en prejuicios ideologistas, sin una actitud de servicio frente a lo excluido y ubicación de sus mecanismos de complicidad con el sistema por encima de los egoprotagonismos es poco proable esperar que la creatividad que interconecte el alma individual de los seres humanos con los otros sintientes y las fuentes de vida abran un proyecto de país del nuevo pacto eco social y de derecho.  
El nacimiento de lo nuevo, de lo que está dormido y o subygado o consentido por la mayoría de la sociedad para que el poder consciente nazca, sea real y medible parte de ver la realidad cómo es del yo y del yo intersubjetivo ese es el punto base para la participación real deliberante en el Acuerdo Final y la mesa del ELN.

</div>

Es un reto de 2017 la conciencia convergente en que los liderazgos jóvenes y de  adultos negados, sean escuchados y asumidos con respeto, por aquellos que pretenden en el movimiento social de mujeres y género, de ambientalistas y animalistas, de derechos humanos y de paz y las agrupaciones políticas crecientes lo nuevo, sin rupturas a partir de asumir lo real nada podrá ser nuevo, será imposible la otra paz, la de la nueva democracia.

#### [**[Leer más columnas de opinión de Camilo de las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)**] 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
