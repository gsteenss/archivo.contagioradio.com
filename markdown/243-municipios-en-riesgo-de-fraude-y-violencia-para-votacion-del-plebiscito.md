Title: 243 municipios en riesgo de fraude y violencia para votación del plebiscito
Date: 2016-08-12 13:48
Category: Nacional, Paz
Tags: Mapa de riesgo electoral plebiscito paz, plebiscito por la paz, refrendación acuerdos de paz
Slug: 243-municipios-en-riesgo-de-fraude-y-violencia-para-votacion-del-plebiscito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Votaciones.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PMI Colombia ] 

###### [12 Ago 2016] 

De acuerdo con la Misión de Observación Electoral, 243 municipios colombianos están en riesgo por factores de violencia y fraude para la votación del plebiscito por la paz. De ellos, 53 están en riesgo extremo, 83 tienen riesgo alto y 107 riesgo medio. La organización llama la atención de las autoridades estatales para que estén vigilantes y recomienda la presencia de observadores internacionales, teniendo en cuenta que **en departamentos como Chocó, Arauca, Cauca, y Putumayo más de la mitad de los municipios presentan riesgos electorales**.

El equipo técnico de la Misión, conformado por la CODHES, la FLIP, el CERAC, la Fundación Paz y Reconciliación, la Pastoral Social, la Fundación Forjando Futuros y Transparencia por Colombia y las Universidades de los Andes, Rosario, Externado y Javeriana, tuvo en cuenta 13 variables que agrupó en dos categorías: fraude electoral y riesgos de violencia, relacionados con la presencia de bandas criminales, ELN y FARC-EP, así como la **incidencia de cultivos ilícitos, minería ilegal, violencia política y social, y el riesgo por desplazamiento masivo y violaciones a la libertad de prensa**.

Según Camilo Vargas, coordinador del observatorio político de la MOE, es necesario que la Corte publique la totalidad de la sentencia para que se resuelvan [[todas las dudas que hay frente al plebiscito](https://archivo.contagioradio.com/las-preocupaciones-en-torno-al-plebiscito-por-la-paz/)] y en esa medida haya mayor confianza. A los comités de las campañas tanto a favor como en contra, Vargas los invita a que **sean leales con el debate y busquen combatir los mitos frente al proceso de paz** para que la ciudadanía pueda tomar una decisión seria e informada.

El coodinador asegura que **la sociedad debe dejar de lado las pasiones que despiertan las explicaciones facilistas y desinformadas de lo que va a pasar en el posconflicto**, informarse de las fuentes directas para lograr evaluar con objetividad los acuerdos a los que se han llegado, es decir, hacer una lectura juiciosa que lleve a una reflexión que no despierte odios y confrontaciones sino que esté a la altura de un escenario democrático.

<iframe src="http://co.ivoox.com/es/player_ej_12527193_2_1.html?data=kpeilJyVfZShhpywj5aXaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncaTVzs7Z0ZC6pdPbwtiSlKiPkbC5joqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
