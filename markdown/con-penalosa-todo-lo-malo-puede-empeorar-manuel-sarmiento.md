Title: Con Peñalosa todo lo malo puede empeorar: Manuel Sarmiento
Date: 2018-02-02 14:32
Category: Política
Tags: Bogotá, inseguridad en Bogotá, Movilidad, Trasnmilenio
Slug: con-penalosa-todo-lo-malo-puede-empeorar-manuel-sarmiento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Enrique-Peñalosa-e1461273574548.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Lafarge Holcim Foundation] 

###### [02 Feb 2018] 

A partir de hoy empieza a regir en Bogotá la restricción por tres meses para parrillero hombre de las motos, **incrementa en \$100 pesos** el Sistema Integrado de Transporte Público, Enrique Peñalosa anunció que, a partir de agosto, comienzan las obras para incluir Transmilenio por la calle séptima y las denuncias por hurtos continúan aumentando. Estas medidas y situaciones han sido controversiales en la ciudadanía y especialmente en el concejo de Bogotá.

### **Restricción del Parrillero hombre afecta la movilidad** 

Si bien la restricción del parrillero hombre busca disminuir los índices de atracos y va dirigida a mejorar la seguridad en la ciudad, el concejal Manuel Sarmiento manifiesta que es una medida que **obliga a los usuarios de moto** a subirse a Transmilenio.

Indica que la medida es completamente **arbitraria** “pues perjudica a cientos de miles de ciudadanos que utilizan su motocicleta con el respectivo pasajero para transportarse”. Además, enfatiza en que es improvisada debido a que cuando Peñalosa estaba en campaña “dijo que la medida no servía para resolver los problemas de seguridad”.

Ahora, “después de que crece la inconformidad por la inseguridad, sale con esta medida improvisada cuando solamente **5 de cada 100 hurtos** que se hacen cada día en la ciudad son cometidos por pasajeros que van en motocicleta”, recalcó. (Le puede interesar: ["En febrero se vence el plazo para revocar a Enrique Peñalosa"](https://archivo.contagioradio.com/en-febrero-se-venceria-el-plazo-para-revocatoria-de-penalosa-en-bogota/))

### **Concejo ha propuesto estrategia integral para superar la inseguridad** 

Sarmiento recordó que en los debates que se realizan en el concejo de Bogotá, se ha planteado la necesidad de crear una estrategia integral que resuelva los problemas de seguridad de fondo. Indicó que “el alcalde Peñalosa ha enfocado su estrategia en el **aparato represivo** que es un asunto que se debe mejorar”.

Para él, no es suficiente con solamente fortalecer las instituciones como la Policía. Esto lo argumenta poniendo el ejemplo de que Bogotá es la ciudad con **menor cantidad de Policías** por habitantes en el país y “a pesar de eso tiene las tasas más bajas de homicidio, en Medellín hay más Policías y su tasa de homicidios es superior”. Esto quiere decir que “más pie de fuerza no significa que se reduzcan los niveles de inseguridad”.

Por esto, es importante “que la política integral incluya aspectos como en la infraestructura urbana donde **funcione bien el alumbrado público** y no genere condiciones de inseguridad como lo que va a suceder con el metro elevado”. Enfatizó en que hay que mejorar las condiciones sociales de las personas, “en una buena política de seguridad se debe incluir todo tipo de intervenciones que van desde lo policial hasta la salud pública”.

### **“Bogotá en movilidad está muy mal”** 

Ayer, el Distrito anunció que, a partir de la segunda semana de agosto, se empezará a realizar las obras necesarias para construir el **Transmilenio por la carrera séptima**. Ante esto, el concejal indicó que, en una ciudad de 8 millones de habitantes, “Transmilenio no puede ser la columna vertebral de la movilidad”. (Le puede interesar:[" Administración de Peñalosa aumentaría \$15 la tarifa de parqueaderos"](https://archivo.contagioradio.com/penalosa_aumento_tarifa_parqueaderos/))

En varias ocasiones ha manifestado que ese sistema no tiene las capacidades suficientes para ser el **eje estructural de la movilidad**. Por esto, hace referencia a los diferentes estudios que se han realizado y los cuales han sugerido que “lo que tiene que hacer Bogotá es construir una red de metros que se vuelvan la columna vertebral”.

Sin embargo, Enrique Peñalosa ha insistido en que “Transmilenio hace lo mismo que un metro, pero es muchísimo más barato”. Frente a esto, Sarmiento ha dicho que “la forma como se planteó Transmilenio **fue un negocio para los operadores privados** que han hecho millonarias ganancias a costa de un pésimo servicio y una de las tarifas más caras del continente”.

Finalmente, recordó que a partir de hoy los usuarios de este sistema deberán pagar **\$2.300 pesos** por un sistema que no está planteado para satisfacer las necesidades de los bogotanos. Dijo que “con Peñalosa se aplica el principio de que todo lo malo puede empeorar” teniendo en cuenta los graves problemas que ha tenido Bogotá y que no han sido resueltos.

<iframe id="audio_23522471" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23522471_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
