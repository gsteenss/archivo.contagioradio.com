Title: Hace 65 años Gabo escribió sobre el abandono al Chocó
Date: 2019-04-17 08:00
Author: AdminContagio
Category: Cultura
Tags: Chocó, Crónicas Chocó, Gabriel García Márquez
Slug: hace-62-anos-gabo-escribio-sobre-el-abandono-al-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/gabo2_620x460.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Revista Dinners 

En 1954, cuando aún se desempeñaba como periodista para el diario el "El Espectador", Gabriel García Márquez escribió una serie de mini-crónicas sobre el Chocó, uno de los Departamentos históricamente más pobres y olvidados del país.

En sus textos de viaje, el futuro nobel de literatura expone varias de las problemáticas que hoy poco y nada se han solucionado para las comunidades afros, indígenas y mestizas, especialmente algunas relacionadas con las vías de comunicación y los medios de transporte, prometidos desde esos tiempos por los gobiernos de turno.

**Una familia Unida sin vías de comunicación**

Es difícil llegar a Quibdó. Pero es más difícil salir. En una palabra, es ese el problema del Chocó, la causa de su situación actual, el origen primario del conflicto que acaba de sortearse y al mismo tiempo la fuente de esa férrea unidad y de ese sólido patriotismo de los chocoanos. Si se penetra un poco más a fondo, se comprende que la gente del Chocó quiere a su tierra y está aferrada a ella en esa forma radical y definitiva, porque están acostumbrados a saber que son una sola familia. Desmembrar al departamento habría sido, literalmente, dispersar una antigua y extensa casa de 100.000 parientes.

**Política: un problema doméstico**

El negocio de hotel es allí un mal negocio. En toda la sección, desde las bocas del Atrato hasta las del San Juan, no hay un solo pueblo con restaurante: cada viajero come donde el primo de su tía, y duerme donde su cuñado o donde el cuñado de su cuñado.

Cualquier chocoano raizal que explore su ascendencia en más de dos generaciones, encuentra la manera de ser pariente de su vecino. Y si no lo es, tampoco importa; porque tarde o temprano tendrá la oportunidad de ser su compadre. Esa es la clave del ya proverbial nepotismo del Chocó. El pacto político que acaba de suscribirse es, por eso, más que un pacto político, una reconciliación familiar.

**La carretera embotellada**

A través del tiempo esa familia ha venido creciendo, fortaleciéndose, a pesar de que para conservar su unidad, ha sido preciso caminar muchos kilómetros abrir muchas trochas en las selvas, recorrer enormes distancias en canoas bajo esos aguaceros primarios que parecen anteriores a la creación. Lo alarmante es, que la nación en toda su historia no haya contribuido en forma efectiva a estimular, con adecuados medios de comunicación, esa unidad chocoana que tan útil y ejemplar puede ser para el resto del país. Lo único que se ha hecho es una carretera interna, teóricamente de Yuto a Istmina, que muy probablemente es la única carretera del mundo que en realidad no sale de ninguna parte, ni llega a ninguna parte. El embotellamiento del Chocó es crítico en tal extremo, que hasta su única carretera interna es una carretera embotellada.

**Fantasías del mapa**

El tramo de Quibdó a Yuto aparece trazado en los mapas. Esa es otra especulación cartográfica: ni un solo árbol se ha derribado para explorar el terreno. El trayecto se recorre en lancha de motor desde Quibdó, subiendo por el Atrato, hasta el lugar en que la carretera sale al encuentro del río, dos kilómetros más arriba de la población de Yuto, y en la ribera opuesta. Con el correr del tiempo, es posible que allí se forme otro pueblo, alrededor de las dos oscuras y frescas enramadas en las que se vende -por otro pacto familiar- gallina guisada, en una, y pescado frito en la otra. Pero mientras ese pueblo no se forme y se le ponga el nombre de Yuto para conciliar la realidad con los mapas, hay que admitir la verdad: la carretera de Yuto a Istmina no pasa ahora ni ha pasado nunca por Yuto.

**La tierra milagrosa**

A todo lo largo de aquellos 75 kilómetros estrechos y empedrados, divididos longitudinalmente por una cresta de hierba que es como una anticipación natural de la cinta blanca en las carreteras asfaltadas, no hay un solo centímetro, pero ni siquiera un milímetro de tierra civilizada. Esa carretera es una incursión de piedra en la selva virgen, a través de una vegetación insaciable y poderosa. Allí no hay nada que sembrar. Nada que criar. Son tierras inexploradas de las que extrajeron los constructores de la carretera huesos de fósiles marinos, a 80 kilómetros del mar. Es enteramente sensato pensar que si se le hubiera ocurrido a alguien sembrar en esa tierra una mata de plátano, las frutas habrían crecido hinchadas con granos de platino. Sin embargo, la realidad indica que ni siquiera esos plátanos fabulosos podrían ser llevados al mercado más cercano antes que empezaran a podrirse.

**En carne y hueso**

De no ser por la incomodidad de los trasbordos, el viaje por la única carretera interna del Chocó sería una reposada experiencia humana. Allí viaja el vendedor de baratijas que habla de política y de la manera de prevenir las mordeduras de serpiente. Viaja la maestra de escuela, que atiende en una remota aldea del San Juan una escuela con 123 niños, muchos de los cuales recorren varios kilómetros en canoa para asistir a las clases. Allí viaja el indio que está siendo catequizado por los protestantes y cuyo idioma tiene 24 matices de las vocales y verbos que condensan toda una oración castellana. Ese indio, que vende gallinas y no acepta el pago sino en billetes de a peso, porque no entiende el régimen monetario, es tal vez una de las únicas ocho mil personas que con una sola palabra pueden decir: "Voy de Quibdó a Condoto, y almorzaré en Istmina". Los misioneros protestantes que adelantan investigaciones lingüísticas en el San Juan, aseguran haber recogido diez mil términos, para un diccionario indígena que definirá diez mil.

En el camión que conduce de Yuto a Istmina, entre el baratillero, la maestra y el indio, viajaba un hombrecillo de 60 años, destrozado por el paludismo, que decía en voz alta para que lo oyeran los periodistas: "Esta es la gente más honrada del mundo. En esta cartera llevo treinta mil pesos en valores declarados y a nadie se le ha ocurrido robármelos". Era verdad. Al preguntársele quién. era, el hombrecillo, muy cordial, muy cordial, muy ceremonioso, se presento a mucha honra:

-Soy el correo nacional.

**El revés de la teoría**

El trasbordo de la lancha al camión, no es el único que se hace en el viaje de Quibdó a Istmina. Hay que hacer un segundo trasbordo, "en Cértegui", según se dice. Pero también es esa una manera de decir las cosas, sencillamente porque la carretera de Yuto a Cértegui, que no pasa por Yuto, tampoco pasa por Cértegui. Pasa por un sitio donde ya se está formando un pueblo, a tres kilómetros de la verdadera población de Cértegui, en cuya gallera se jugó hace veinte años una libra de platino, y hoy es una polvorienta aldea tirada al margen de la carretera. Al revés de lo que ha ocurrido siempre, en el Chocó son los pueblos los que tienen que pasar forzosamente por las carreteras y no las carreteras por los pueblos.

**Un solo puente**

De la ribera izquierda del río Cértegui, donde se interrumpe la carretera, hay que atravesar en canoa a la ribera derecha, donde se toma el camino que definitivamente conduce a Istmina. Allí está el único puntal para un puente que no se construyó jamás, nadie sabe por qué. Aunque acaso sea para que el Chocó siga siendo la gran paradoja administrativa, el departamento que tiene mayor cantidad de ríos, y que sin embargo no le ha costado nada más que un puente interior a la nación: el puente de Las Ánimas, construido en 1942.

**"Como me lo contaron"**

La razón para que una carretera que fue construida para desembotellar a Cértegui no pase por Cértegui, es cosa muy difícil de averiguar. Los habitantes de la región, acostumbrados a las más inexplicables y curiosas maniobras administrativas, aseguran que Cértegui se quedó sin carretera porque el ingeniero constructor se bañaba desnudo dentro del perímetro urbano y el inspector de policía le llamó la atención. Como represalia el ingeniero dejó a Cértegui sin carretera.

Esa es la historia, como la cuentan en el pueblo. Pero los informantes más serios insisten en que todo eso es pura literatura folclórica y que lo que realmente ocurrió fue que a última hora se modificó el progreso y se desvió la carretera por donde el puente fuera menos costoso y más fácil de construir. De eso hace muchos años y todavía el puente no ha sido construido.

**Istmina ¡Por fin!**

Al final de la carretera, Istmina es una calle larga, o más exactamente, el último tramo de la carretera con deterioradas casas de madera a lado y lado, hasta la orilla del río San Juan. Un pueblo de gente sencilla, cordial y hospitalaria, que desayuna con leche condensada, porque no hay otra, y que sólo come carne de res durante quince días. En una eminencia, a espaldas de un busto del general Uribe que nadie se explica cómo sobrevivió a la violencia, hay un colegio de misioneros con seminaristas que cantan en el coro y al atardecer salen a pasear por el pueblo en bicicleta y a tomar fotografías.

\* \* \*

El acueducto, construido por el Instituto de Fomento Municipal, surte a la población desde una pileta al aire libre, en la que además del agua se deposita toda clase de desperdicios. Donaldo Lozano asegura que hace tres meses abrió el grifo de su casa y por él salió el hollejo de una serpiente. Aunque en Istmina saben que Donaldo Lozano vive en olor de exageración, desde hace tres meses nadie ha vuelto a beber el agua del acueducto.

**La bolsa de valores**

El comercio de Istmina es de una variabilidad imprevisible. El miércoles de la semana pasada un paquete de cigarrillos nacionales costaba treinta centavos, como en todo el país. El viernes costaba ochenta. La modificación de los precios se debía a que la avioneta de "Seraco", que anda, de pueblo en pueblo como un saltamontes providencial, no pudo transportar un cargamento de cigarrillos, que le aguardaba en Buenaventura. La avioneta debió de volver el martes. Si tampoco en esa ocasión pudo transportar la carga, todo Istmina debe de estar fumando cigarrillos extranjeros, que ya deben ser mucho más baratos que los nacionales.

**La despensa perdida**

La consecuencia dramática del embotellamiento del Chocó está en el Baudó, la prodigiosa e inútil despensa donde el plátano se pudre y el arroz se pierde porque no hay cómo sacarlos a los mercados. Nada se logra con llevar los productos a Puerto Pizarro, en el océano Pacífico, porque el único barco que pasa por allí, "El Ciudad Quibdó", demora un mes en la travesía. Varios kilómetros al norte se quedaron esperándolo "los locos del Baudo", ese puñado de intrépidos colonizadores que sobrevivieron comiendo cucarachas marinas.  
No hay un mercado en el mundo a donde los suculentos plátanos del Baudó -de los cuales se conocen ocho variedades- puedan llevarse en menos de una semana. Y el plátano empieza a podrirse cinco días después de recolectado.

**A lomo de hombre**

El camino del arroz es largo y sangriento. Pero como los habitantes de Istmina, Condoto, Andagoya y Tadó, tienen que alimentarse de cualquier cosa para no comerse su tierra de oro y platino, el camino del arroz sigue utilizándose. Hay que remontar la carga, en canoas, hasta las cabeceras del río Pepe. Allí lo espera una cuadrilla de hombres de carga, de bestias humanas que lo transportan, en largas y dramáticas jornadas a través de la selva, agarrándose como gatos a las faldas de la montaña virgen, hasta la carretera de Istmina. Cada cargador, sobreviviente de los tiempos heroicos de la Colonia, devenga dos pesos por jornada. Un saco de arroz comprado a quince pesos en el Baudó, llega a Istmina -80 kilómetros aproximadamente- costando 28 pesos, si se ha contado con la suerte de que no llueva demasiado.

**¿Dónde está el Pacífico?**

Si es difícil viajar al Chocó desde cualquier lugar del país, es más difícil viajar dentro del Chocó. En Quibdó se habla de Bahía Solano como de una leyenda: se dice que es la ensenada natural más hermosa y aprovechable del país, y que puede ser la fantástica puerta del Pacífico. Pero muy pocos de quienes lo dicen conocen a Bahía Solano, sencillamente porque para recorrer el centenar de kilómetros que la separan de Quibdó, se necesitan hasta cuatro días, por la ruta del pian y el paludismo.

Incluso los pueblos del Pacífico están incomunicados entre sí. Bahía Solano ignora a qué saben las naranjas de Cupica, a pesar de que en el mapa figuran a una pulgada de distancia. Desde las bocas del SanJuan hasta Punta Ardita, en la frontera de Panamá, no hay una sola oficina telegráfica. En octubre del año pasado, un visitador de la policía que viajaba a Juradó tuvo que dormir en Bahía Solano. El agente postal le pidió el favor de llevar al agente postal de Juradó un pequeño envoltorio, protegido con papel encerado. Era el correo de 6 meses.

###### Gabriel García Márquez para El Espectador, Septiembre de 1954. 
