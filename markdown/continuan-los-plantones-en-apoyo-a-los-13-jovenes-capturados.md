Title: Continúan los plantones en apoyo a los 13 jóvenes capturados
Date: 2015-08-06 11:39
Category: Movilización, Nacional
Tags: 13 jóvenes capturados, atentados Porvenir, Congreso de loa Pueblos, Falsos Positivos Judiciales, Fiscalía General de la Nación, persecución política, procuraduria
Slug: continuan-los-plantones-en-apoyo-a-los-13-jovenes-capturados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Planton-Fiscalia1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: comosoc.org 

<iframe src="http://www.ivoox.com/player_ek_6152927_2_1.html?data=l5ailJ6We46ZmKiakpuJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Di1c7bh6iXaaO1wtOYzsbXb8LXxM7c0MrXb8XZjMbQ0dLUpYa3lIqvk8bRrcbi1dSYw5DQs9SfkpiYzIqnd4a2lJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Fernanda Espinosa, Congreso de los Pueblos] 

“**En medio de la tristeza e indignación por los señalamientos y detenciones injustas, se vive una posibilidad de seguir hablando con música**”, dice Fernanda Espinosa, integrante del Congreso de los Pueblos, quien participó en el plantón que se realizó este jueves a las 8 de la mañana frente a la Fiscalía General de la Nación.

Con esta manifestación continúa el acompañamiento a los 13 jóvenes capturados, que se han vuelto un símbolo de las persecuciones hacia los defensores y defensoras de derechos humanos, lo que se ha catalogado como **“falsos positivos judiciales**”; pero a la vez se han vuelto una representación de la **dignidad y fortalecimiento del movimiento social** en Colombia.

“El plantón también es por los compañeros de Marcha Patriótica, el movimiento indígena, y en general la persecución colectiva a todo aquel que piensa distinto y sueña con un país diferente”, dice Espinosa quien agrega que el caso de los 13 líderes sociales detenidos es una “evidentemente acción de persecución a su trabajo y militancia política”.

La integrante del Congreso de los Pueblos, asegura que se debe seguir presionando y acompañando a los 13 capturados para que no se pierda la visibilidad del caso, teniendo en cuenta que ya se ha demostrado que los jóvenes **nada tienen que con las bombas de Porvenir.**

“**Libertad, libertad son inocentes” y “Que pensar no nos cueste la vida y la libertad**”, son algunas de las arengas que se escucharon durante este plantón para decir “no más casos de falsos positivos judiciales”. Además, cada persona llevaba una cinta tapando su boca con palabras como cárcel, persecución, estigmatización, “porque con esos elementos nos quieren callar”, explicó Fernanda Espinosa, quien añade que estas acciones continuarán en los próximos días.

Cabe recordar que a los 13 jóvenes se les dictó medida de aseguramiento en centro penitenciario, sin embargo, [los abogados apelaron la decisión de la juez](https://archivo.contagioradio.com/cuales-son-los-argumentos-de-los-abogados-para-que-los-13-jovenes-no-vayan-a-la-carcel/)72 de garantías, y la Procuraduría General de la Nación pidió que se examine nuevamente la medida ya que no se tuvo en cuenta los argumentos de todos los abogados y **no se respetó su derecho de defensa.**
