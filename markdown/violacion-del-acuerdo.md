Title: Denuncian captura con fines de extradición de integrante de las FARC que buscaba atención médica
Date: 2017-02-25 15:36
Category: Nacional, Paz
Tags: FARC, Movimiento de zonas de reserva campesina
Slug: violacion-del-acuerdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/WhatsApp-Image-2017-02-08-at-5.01.16-PM-e1486591460330.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Contagio Radio 

###### [25 Feb 2017] 

**La Policía habría capturado a un guerrillero enfermo de Hepatitis B, que se dirigía desde la Zona Veredal Transitoria de Normalización,** en el Gallo, Córdoba, hacia la ciudad de Medellín, con el fin de recibir tratamiento médico. Según las FARC esto es una provocación y una violación del acuerdo en el literal H del punto 6.1.9 del Acuerdo definitivo.

Según la denuncia, **Julio Enrique Lemos Moreno, conocido en la guerrilla como "Nader",** e identificado con la cédula, 71'985.826, miembro del Frente 58, fue capturado de manera irregular, y además trasladado a Bogotá, con fines de extradición.

"Es una situación que ha generado indignación y confusión en las bases guerrilleras, que han demostrado compromiso con el cumplimiento de los acuerdos. Se cree que esto último es el objetivo de ciertos sectores para impedir el avance de la paz Es considerada una provocación dirigida a romper el proceso de paz.

En este sentido el CSIVI debe apersonarse, junto a los países garantes para cumplir mecanismos establecidos", dice la denuncia.

Frente a esta situación Iván Márquez, Jefe de la Delegación de Paz de las FARC-EP, dijo que se trata de "un saboteo al Acuerdo de Paz el traslado a Bogotá con fines de extradición de Nader guerrillero enfermo capturado por la policía". Así mismo señaló que **"En lugar de capturarlo y judicializarlo debieran brindarle asistencia médic**a", pues lo que ese tipo de actos generan es "llenar de desconfianza a la guerrilla".

Finalmente en su cuenta en Twitter, citó las palabras de Humberto de La Calle "**sería un acto de perfidia desarmar una guerrilla para luego incumplir.** La Implementación es sagrada".

**La guerrilla y los veedores del proceso solicitan liberación** de Julio Enrique Lemos. También hacen un llamado a la veeduría ciudadana, para que estas irregularidades no vuelvan a ocurrir.

Las FARC además reiteran que las ZVTN no se encuentran en condiciones óptimas para su funcionamiento, y por ello no hay manera de realizar tratamientos médicos en esas zonas, dadas **las precarias condiciones que además han provocado que guerrilleros de las diferentes regiones se encuentren se enfermen.**

###### Reciba toda la información de Contagio Radio en [[su correo]
