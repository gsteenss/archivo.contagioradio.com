Title: Campesinos exigen ser reconocidos en la Constitución Política
Date: 2016-04-08 15:36
Category: eventos, Nacional
Tags: Campesinos Colombia, Coordinador Nacional Agrario, El campesinado a la Constitución
Slug: campesinos-exigen-ser-reconocidos-en-la-constitucion-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesinos-boyaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo -Contagio Radio ] 

###### [8 Abril 2016 ] 

Este viernes en la Universidad Nacional sede Bogotá diversas organizaciones sociales se dan cita para discutir sobre el **reconocimiento político del campesinado**, sus propuestas de ordenamiento territorial y participación democrática, así como sus apuestas para el fortalecimiento de la economía campesina, todo ello en perspectiva de mujer y en el marco del foro 'El campesinado a la Constitución'.

Según afirma Ricardo Herrera, actual presidente del 'Coordinador Nacional Agrario', este evento se da en el marco de las acciones políticas, legislativas y de movilización social, puestas en marcha para presionar el reconocimiento del campesinado colombiano como sujeto de derechos, a través de una reforma constitucional que afirme su territorialidad y el **enfoque diferencial de sus derechos económicos, sociales, políticos y culturales**.

Con este propósito se ha radicado una [[iniciativa legislativa](https://archivo.contagioradio.com/proyecto-de-ley-busca-garantizar-derechos-del-campesinado-en-colombia/)] que busca que los **más de 12 millones de campesinos colombianos sean reconocidos en la Constitución Política**; así mismo, las organizaciones campesinas planean la realización de un referendo por el agro, que además de permitir su reconocimiento político, fortalezca la producción agrícola, teniendo en cuenta las afectaciones que les han representado los TLC.

De acuerdo con Herrera el objetivo de este foro es intercambiar experiencias con líderes campesinos y sociales de diferentes regiones del país y del mundo, así como con sectores académicos estudiosos de estos temas en América Latina, con el propósito de fortalecer las luchas campesinas en Colombia y alentar la **movilización nacional de la que participarán comunidades de distintas regiones en el primer semestre de este año**.

Participaran de este foro el Senador Alberto Castilla e integrantes del Instituto de Estudios Ambientales de la UNal, del CINEP, de 'Planeta Paz', de la 'Cumbre Agraria, Campesina, Étnica y Popular', de 'FIAN Internacional', de 'OXFAM', del 'Grupo Semillas', de la 'Comisión Colombiana de Juristas' y de 'CENSAT'; así como invitados internacionales de la Embajada de Bolivia en Naciones Unidas y de CLOC-Vía Campesina, e investigadores de México y Ecuador.

<iframe src="http://co.ivoox.com/es/player_ej_11095808_2_1.html?data=kpadm5qcdJmhhpywj5WdaZS1k5iah5yncZOhhpywj5WRaZi3jpWah5yncbPdxMbfxtSPjMbm08rfw4qWh4zk08rgy8nJstXZjMnSzpCJdpi30NTfxs7SpcXj05C7w8jNs8%2FVzZCuj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
