Title: Enrique Peñalosa anuncia la venta de la empresa pública ETB
Date: 2016-04-29 10:54
Category: Economía, Nacional
Tags: Empresas Públicas, Peñaloza, Venta ETB
Slug: enrique-penaloza-anunciala-venta-de-la-empresa-publica-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/penalosa-vende-etb.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Handout photo 

El alcalde **Enrique Peñalosa anuncio la venta de la empresa pública ETB** durante la presentación del Plan de Desarrollo para Bogotá, ante el Concejo de la capital.

Según el alcalde de Bogotá la empresa pública ETB se encuentra en un estado de devaluación y pierde ingresos, mientras que el sector de telecomunicaciones crece. Sin embargo, los estados financieros de la ETB  **[reportan utilidades por 370 mil millones, crecimiento exponencial del patrimonio de 25.64% y un billón de pesos de ganancias para el distrito](https://archivo.contagioradio.com/etb-le-aporta-al-distrito-cerca-de-un-billon-de-pesos/), **esta información se encuentra publicada en el último informe financiero en la página web de la empresa.

De acuerdo con el concejal Hollman Morris, son múltiples las acciones que se han emprendido para desprestigiar a la empresa pública ETB, [sin tener en cuenta que es la única empresa que lleva fibra óptica a todas las regiones](https://archivo.contagioradio.com/a-un-precio-irrisorio-penalosa-estaria-pensado-vender-la-etb/)del país y que reportó en lo corrido del año  un 16% de incremento en sus acciones.

Con la venta de la ETB saldrían afectados no sólo los trabajadores de la compañía sino gran parte de la población estudiantil de la ciudad, esto debido a que la empresa durante los dos últimos años, le ha **representado a la Universidad Distrital Francisco José de Caldas ingresos de entre 7 mil y 10 mil millones de pesos**, así como la posibilidad de contar con el servicio de internet gratuito en 34 puntos de la ciudad, en los que se encuentran  bibliotecas y colegios públicos, siendo la ETB la única empresa que oferta internet de 150 megas.

\[caption id="attachment\_23361" align="aligncenter" width="618"\][![razones para no vender la ETB](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/RAZONESPARANOVENDERETB.jpg){.wp-image-23361 .size-full width="618" height="1177"}](https://archivo.contagioradio.com/enrique-penaloza-anunciala-venta-de-la-empresa-publica-etb/razonesparanovenderetb/) Razones para no vender la ETB\[/caption\]  
<iframe src="http://co.ivoox.com/es/player_ej_11355897_2_1.html?data=kpagl5qcfZihhpywj5WaaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncanjzdHaw9OPkdDm087gjZKPh9DixMrXw9GPlNPjyNfS1c7XuMLnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Noticia en desarrollo
