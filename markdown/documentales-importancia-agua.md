Title: 5 Documentales sobre la importancia del agua
Date: 2017-03-22 17:47
Author: AdminContagio
Category: 24 Cuadros
Tags: Agua, Cine, conservación, Documental
Slug: documentales-importancia-agua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/campesino-Yingtan-territorio-desertico-China_CLAIMA20120630_0017_19.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Caminando por la luna 

###### 22 Mar 2017 

En el día mundial del agua, recordamos algunas producciones que han abordado el tema desde el documental, particularmente centrados en la ambición de algunos sectores por obtener su propio beneficio económico, pasando por encima del bienestar colectivo de cientos de miles de personas en todo el mundo.

**Agua, una crisis apunto de desbordarse**

![agua](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/maxresdefault.jpg){.wp-image-38105 .aligncenter width="370" height="208"}

Un país biodiverso como Colombia, con abundantes ríos y costas en dos océanos, además de innumerables riquezas naturales, se encuentra en dificultades para el abastecimiento de agua dulce. Más que por el cambio climático, la contaminación de los ríos y el deterioro del medio ambiente, son los factores que están poniendo en riesgo la riqueza hídrica existente.

Un documental que aborda la problemática del agua en Colombia desde distintas perspectivas y también propone algunas soluciones. La producción contó con la dirección de José Orlando Castillo, tras varios meses investigación y de entrevistas a distintos especialistas, con el único fin de hacer visible las verdaderas causas que están acabando con los ríos y las fuentes de agua en el país.

**Un mundo sin agua**

![agua](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/post-un-mundo-sin-agua.jpg){.wp-image-38099 .aligncenter width="367" height="240"}

Un documental sobre los problemas relacionados con malas prácticas en la privatización del abastecimiento de agua, principalmente el impuesto por el Banco Mundial a países en vías de desarrollo.

La producción muestra las problemáticas de ciudades como El Alto en Bolivia,Dar es Salaam en Tanzania, y Detroit en Estados Unidos, que tienen en común las dificultades del acceso al líquido vital por cuenta de la privatización por parte de empresas, públicas y privadas y los diferentes conflictos sociales producidos por los altos costos y avaricia de quienes están detrás del negocio del agua.

**Nuestro sueño de Agua**

** ![el-sueno-del-agua3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/el-sueno-del-agua3.png){.wp-image-38111 .aligncenter width="415" height="228"}**

Nuestro Sueño de Agua sigue a tres mujeres y sus comunidades. La crisis mundial del agua las afecta a ellas y a los niños, quienes pasan muchas horas al día recolectando agua en lugar de trabajar, cuidar de la familia o asistir a la escuela.

El documental está enfocado en las historias de mujeres en Haití, Perú y Kenia y su lucha diaria por encontrar agua limpia y segura. Sus historias son representativas de las 663 millones de personas en todo el mundo que carecen de acceso a agua potable, para ayudar a crear conciencia sobre esta situación.

**Flow: Por amor al agua**

![agua](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/6484.jpg){.wp-image-38107 .aligncenter width="399" height="155"}

En el documental se expone el tema de la privatización y monopolización del agua en muchos lugares del mundo, tanto en los servicios de distribución como del recurso en su estado natural. Le puede interesar: [La radiografía del derecho fundamental al agua en Colombia](https://archivo.contagioradio.com/la-radiografia-del-derecho-fundamental-al-agua-colombia/).

La producción fue proyectada en la sede de Naciones Unidas para concienciar a sus representantes de los problemas relacionados con el agua que hay en el mundo y de la necesidad de proteger a la población de los intereses económicos de unos pocos.

**La sed del mundo**

![agua](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/lased-comp.png){.wp-image-38100 .aligncenter width="370" height="245"}

La sed del mundo, hace un viaje por diferentes partes del planeta como Kenia, Congo, Camboya, Palestina, Senegal o China, para visibilizar los problemas que se viven en estos lugares por cuenta de la escasez de fuentes cercanas de agua potable, la contaminación de los afluentes y los conflictos que allí se viven.

Con la estética característica del director del documental Home, la producción utiliza diferentes imágenes aéreas y entrevistas muy descriptivas de las situaciones por las que pasan las comunidades que sobreviven ante las difíciles condiciones.

**Tapped**

![agua](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/81pCX2exF9L._SL1500_.jpg){.wp-image-38102 .aligncenter width="345" height="484"}

Una producción sobre la industria de comercialización del agua en botella, un negocio que se estima recauda 800 mil millones de dólares en todo el mundo. El mayor problema que expone el documental radica en que el 80% de las botellas que se consumen en un país de las dimensiones de Estados Unidos no se reciclan, situación que se replica en otros países del mundo en un promedio del 50%.

Muchas de las compañías que trabajan en este tipo de negocio, obtienen el recurso de manera ilegal, hasta prácticamente agotar las fuentes hídricas que explotan, además de realizar una fuerte campaña mediática para validar la pureza de su producto frente al agua que puede consumirse en estado natural o del grifo en cada hogar.

 

 
