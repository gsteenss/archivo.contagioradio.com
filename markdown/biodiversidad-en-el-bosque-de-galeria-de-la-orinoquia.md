Title: Biodiversidad en el bosque de galería de la Orinoquia
Date: 2019-05-14 14:05
Author: CtgAdm
Category: Voces de la Tierra
Tags: biodiversidad, Fauna, libro, Orinoquía
Slug: biodiversidad-en-el-bosque-de-galeria-de-la-orinoquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Biodiversidad.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:@magiasalvajecol 

En el marco de la Feria Internacional del libro de Bogotá se presentó la publicación "**Biodiversidad en el bosque de galería de la Orinoquia colombiana**", fruto del trabajo interdisciplinar de varios académicos del país que se dieron a la tarea de caminar, investigar y documentar la diversidad de especies que se encuentran en estos ecosistemas y advertir sobre los peligros que amenazan con extinguirlas progresivamente.

Para hablar sobre el libro, primero de 3 tomos dedicado a la fauna, y sobre la biodiversidad en Colombia y el mundo, en Voces de la tierra nos acompañaron **Luis Alberto Núñez Avellaneda**. Biólogo, máster en ecología y doctor en Biodiversidad y Conservación. Profesor del programa de biodiversidad de la Universidad Nacional y **Gelys Mestre Carrillo**, matemática y magister en matemática aplicada. Profesora de la Universidad de la Salle, coeditores junto a María Isabel Castro y Lucía Cristina Lozano.

<iframe id="audio_35814310" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35814310_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en Contagio Radio 
