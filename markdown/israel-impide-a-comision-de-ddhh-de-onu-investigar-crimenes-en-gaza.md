Title: Israel impide a Comisión de DDHH de ONU investigar crímenes en Gaza
Date: 2015-02-03 18:47
Author: CtgAdm
Category: El mundo, Judicial
Tags: Comisión de DDHH ONU Palestina, Crímenes de guerra Israel, Gaza, Israel, Palestina
Slug: israel-impide-a-comision-de-ddhh-de-onu-investigar-crimenes-en-gaza
Status: published

###### Foto:Informador.fmx 

<iframe src="http://www.ivoox.com/player_ek_4033436_2_1.html?data=lZWglZmXeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRjdTmwsrZjdPNqcjVjMaYzsaPp9DhytjWh6iXaaOnz5DRx5DQpYzDr7qY09rJb8ri18rg1s7LpYzg0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista con Nicola Hadwa] 

Israel ha **negado el acceso a los miembros de la Comisión de Derechos Humanos de la ONU que debe investigar las acusaciones de crímenes de guerra** perpetrados por el ejército israelí en el contexto de la operación "Margen protector" contra Gaza.

El ministerio de exteriores del estado israelí ha afirmado que "...Dado que la Comisión no es una comisión investigadora sino una comisión que, de antemano, ya ha sacado sus conclusiones (...) se ha decidido no colaborar con ella".

También el portavoz del ministerio Emmanuel Nahshon desestima la comisión afirmando que "...Estamos hablando de una comisión totalmente ilegítima, que fue nombrada por un organismo anti-israelí y con un mandato claramente contrario a Israel..."

Por el momento **la comisión se encuentra en Jordania a la espera de poder por la frontera sur entre Egipto y Gaza**, aunque para ello es necesaria la autorización de Israel.

La propia comisión ha sido creada con el fin de esclarecer infracciones al derecho internacional humanitario y la convención de Ginebra; dicha misión, ha sido ratificada por más del 90% de Estados de la ONU.

Para todas las organizaciones de derechos humanos son claras las violaciones de derechos humanos y los crímenes de guerra cometidos por el país hebreo en las repetidas incursiones militares en Gaza, pero sobre todo en la última.

Una de las voces que afirma la necesidad de que una comisión juzgue dichos crímenes es **Nicola Hadwa , miembro del Comité de solidaridad de Chile con Palestina y de la comisión internacional para el retorno.**

Hadwa afirma que **es innegable que Israel cometiera crímenes de guerra contra población civil ya que esta todo totalmente reportado**, e incluso la opinión pública pudo verlo en vivo y en directo por los distintos medios de comunicación.
