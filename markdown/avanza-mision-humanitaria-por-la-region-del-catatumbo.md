Title: Avanza misión humanitaria por la región del Catatumbo
Date: 2018-05-17 14:57
Category: DDHH, Nacional
Tags: Catatumbo, Misión Humanitaria
Slug: avanza-mision-humanitaria-por-la-region-del-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Faro_del_Catatumbo-e1526587021569.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Esacademic] 

###### [17 May 2018] 

En el segundo día de la misión de verificación de derechos humanos que recorre la región del Catatumbo, los integrantes denunciaron que se han percatado del abandono al que ha sido sometido este territorio por parte del Estado, **la crisis humanitaria por los constantes enfrentamientos entre grupos armados y los desplazamientos masivos que se han provocado**.

Wilmer Franco, delegado de ASCAMCAT, afirmó que  han solicitado al gobierno nacional, que no se militarice el territorio como respuesta a la violencia, sino que se trabaje por una salida dialogada, esto debido a que **tienen información referente a que se habría aprobado el traslado de 12 mil integrantes de la Fuerza Pública al Catatumbo**.

Frente a los hostigamientos de los cuales fue víctima la misión el pasado 15 de mayo, aún no se ha establecido que grupos armados estarían detrás de los hechos. La misión se encontraba visitando el corregimiento de Mesitas en el municipio de Acarí y de Villanueva, en el municipio de San Calixto, cuando se presentaron los disparos. (Le puede interesar: ["Comisión de verificación fue víctima de hostigamientos en el Catatumbo"](https://archivo.contagioradio.com/comision-de-verificacion-fue-victima-de-hostigamientos-en-el-catatumbo/))

El objetivo de este recorrido, de acuerdo con Franco es, que a través de esa misión humanitaria, convocada por la Comisión por la vida, la paz y la reconciliación, se visibilice la situación por la que están pasado. El próximo lugar de llegada será la vereda de La Fortuna, en el municipio de San Calixto.

<iframe id="audio_26037314" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26037314_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
