Title: Eduin López, alcalde de Buenos Aires Cauca sale ileso de atentado
Date: 2020-01-26 14:43
Author: CtgAdm
Category: DDHH, Nacional
Slug: eduin-lopez-alcalde-de-buenos-aires-cauca-sale-ileso-de-atentado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Buenos-Aires-Cauca-LA-FM-RCN.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Diseño-sin-título-4.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Archivo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 26 de enero, **Eduin López Sánchez** alcalde del municipio de Buenos Aires, al norte del departamento del Cauca, fue **ataque atacado con arma de fuego mientras se movilizaba en su vehículo**. (Le puede interesar: <https://archivo.contagioradio.com/asesinato-de-hernando-herrera-y-john-fredy-vargas-enlutan-al-pais/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho se dio cerca de las de 10:00 am **cuando el mandatario salía de reunirse con la comunidad de la zona en compañía de su asesor Pablo César Peña y un escolta**, y fueron interceptados por hombres armados que dispararon contra el vehículo blindado en el que se movilizaba, sobre la vía que conduce del corregimiento La Balsa a Santander de Quilichao.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según algunos testimonios, **Eduin López logró evadir el ataque al refugiarse en un retén del Ejército que se encontraba en la zona,** allí los uniformados lo trasladaron junto con su asesor hacia el área urbana de Jamundí al sur del Valle del Cauca. (Le puede interesar: <https://www.justiciaypazcolombia.com/operaciones-paramilitares-de-las-agc-en-jiguamiando/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El secretario de Gobierno del Cauca, Luís Angulo, informó que el alcalde y sus acompañantes se encuentran ilesos, y agregó que a**ctivará protocolos de seguridad que permitan garantizar la vida del mandatario, y llegar a identificar y procesar a los autores de este hecho**.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
