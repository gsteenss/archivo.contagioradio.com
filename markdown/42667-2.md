Title: Cinco propuestas para evitar el fracaso total de ley de restitución de tierras
Date: 2017-06-22 12:57
Category: Ambiente, Entrevistas
Tags: Forjando Futuros, Ley de Tierras, microfocalización, Restitución de tierras
Slug: 42667-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Restitución-de-Tierras-e1510251256744.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Informador] 

###### [22 Jun. 2017] 

Luego de 6 años de haber sido proclamada la **ley de restitución de tierras en Colombia, la norma ha sido calificada como un fracaso contundente**. Según cifras de la organización Forjando Futuros al 8 de Junio habían sido presentadas 103.892 solicitudes ante la Unidad del Gobierno que trabaja ese tema, de las cuales han sido negadas 26.052.

En contraposición han sido restituidas o compensadas 4.670 sentencias a favor de las víctimas, es decir, en todo este tiempo solo el 4.5% del total de las sentencias han sido resultas. Le puede interesar: [Organizaciones proponen acciones para fortalecer la restitución de tierras](https://archivo.contagioradio.com/organizaciones-proponen-acciones-para-fortalecer-la-restitucion-de-tierras/)

Según **Gerardo Vega Medina, integrante de la organización Forjando Futuros, pareciera que “la norma fue creada para negar el derecho de las personas** para que reclamen sus tierras. Esto parece un plan piloto. La ley es buena pero el proceso de implementación ha sido un fracaso. Ha fracasado la implementación”.

### **Cinco propuestas de Forjando Futuros para que ley 1448 no fracese totalmente ** 

Es necesario derogar la microfocalización, dado que en algunos casos, el Ministerio de Defensa ha demostrado falta de voluntad para indicar en qué predios se podría dar el trámite de restitución y en qué predios no. Según Vega muchas solicitudes están represadas por ausencia de microfocalización. Son cerca de 26.000 casos.

“En Urabá hay un número importante de solicitudes, muy pocas tramitadas porque no hay la microfocalización del Ministerio de Defensa. Allí están represadas la mayoría de solicitudes de restitución” recalca Vega.

Por otro lado, dice el funcionario de Forjando Futuros que debe haber una restitución administrativa es decir **“si no hay oposición a la restitución solicitada ¿para qué llevarla a un juez? Bastaría con una resolución de Gobierno** para devolverle el predio las personas” y recuerda que a la fecha de las más de 4.500 solicitudes, el 72% no ha tenido opositor.

El tercer elemento sería la** restitución colectiva de los territorios,** haciendo referencia a la posibilidad de que cuando predios son colindantes y tiene un mismo despojador, el juez puede resolver varios casos con una sola sentencia.

“Ya hay ejemplos de ellos, en Montes de María se han resuelto en una sola sentencia 58 casos, porque los predios eran colindantes, los victimarios eran los mismos y porque los hechos fueron en una misma época” añadió Vega.

Si se hiciera de esa manera de los 6 millones 500 hectáreas de tierras que hay que restituir en Colombia, el 55% podría ser de manera colectiva. En Urabá hay más de 2.800 reclamantes de tierras y los hechos de violencia se produjeron en la misma época y por una misma persona según varias confesiones de paramilitares, por lo que "esos casos pueden resolverse en una misma sentencia” dijo Vega. Le puede interesar: [Unidad de Restitución de Tierras estaría desacatando tutela sobre microfocalización](https://archivo.contagioradio.com/unidad-de-restitucion-de-tierras-estaria-desacatando-fallo-de-tutela-sobre-microfocalizacion/)

###  

### **Es necesario garantizar la seguridad de los reclamantes de tierras** 

A la fecha **en Urabá han sido asesinados 17 reclamantes de tierras,** razón por la cual el integrante de Forjando Futuros asegura que la cuarta propuesta es que el Gobierno desmantele a los grupos paramilitares y la quinta es que la Unidad Nacional de Protección – UNP - garantice la vida de los líderes y lideresas. Le puede interesar: [Atentan contra Argemiro Lara, líder de restitución de tierras en Sucre](https://archivo.contagioradio.com/atentan-contra-argemiro-lara-lider-de-restitucion-de-tierras-en-sucre/)

**“Estamos solos, porque no tenemos siquiera respuesta de la UNP,** eso hace que haya riesgo, peligro contra los reclamantes y contra las organizaciones que llevamos la representación legal. El Gobierno debe dar soluciones de fondo” recordó Vega.

### **Empresarios y paramilitares otro problema para poder restituir tierras** 

Otro elemento que es mencionado por Vega y que asegura ha hecho que la ley no tenga resultados positivos es el asesinato, el desplazamiento, las amenazas contra los líderes y los reclamantes de tierras.

“Porque persisten y allí hay grupos financiados por empresarios en territorios como el Urabá chocoano. **Hay palmicultura, banano y ganadería.** La participación de grupos empresarios que han financiado grupos paramilitares y que esos grupos están allí lo han probado muchas sentencias” ratifica Vega.

### **El proceso de restitución ha perdido legitimidad** 

Así lo aseveró Vega, quien añade que **la gente le teme a comenzar el proceso por los riesgos que corren sus vidas** y por la falta de garantías en que su caso va a ser resuelto “si tengo solo un 4% de probabilidad de que me resuelvan el tema mejor me quedo como estoy y no pongo en riesgo la vida” dice que le cuentan los campesinos. Le puede interesar: [Unidad de Restitución de Tierras mete un mico a la ley 1448](https://archivo.contagioradio.com/unidad-de-restitucion-de-tierras-mete-un-mico-a-la-ley-1448/)

Y concluye que faltó mayor divulgación y más oficinas para poder tramitar las solicitudes “no se puede seguir siendo especialistas en sacar buenas normas, pero no cumpliéndolas. **Estamos ante una buena norma, que la han enredado en el procedimiento,** pero además por las vías de facto, de hecho, han hecho imposible que se logren avances importantes en la implementación”.

![Tierras](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Tierras.jpeg){.alignnone .size-full .wp-image-42681 width="600" height="960"}

<iframe id="audio_19415548" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19415548_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
