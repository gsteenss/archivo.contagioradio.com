Title: Ejército Popular de Liberación también quiere sumarse a la paz
Date: 2017-10-04 17:47
Category: Nacional, Paz
Tags: conversaciones de paz, Ejército Popular de Liberación, ELN, EPL, FARC, Juan Manuel Santos
Slug: ejercito-popular-de-liberacion-tambien-quiere-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/Ejercito-Popular-de-Liberación-e1527526839809.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EFE] 

###### [04 Oct 2017 ]

En una carta fechada este 2 de Octubre y firmada por la comandancia del Ejército Popular de Liberación, EPL, los comandantes de esa guerrilla **afirmaron que estarían dispuestos a adelantar conversaciones de paz con el gobierno de Juan Manuel Santos** y aseguran que esa misiva es una respuesta a comunicaciones del gobierno que ha manifestado la necesidad de establecer espacios para construir paz.

En la comunicación abierta el EPL hace un recuento de varias situaciones por las que atraviesa el país en relación con las movilizaciones sociales, los incumplimientos a varios sectores sociales por parte del Estado, la crisis de la justicia y de las instituciones en general y aseguraron que podrían hacer propuestas para solventar algunos de estos puntos.

### **Cuáles podrían ser los puntos de una agenda de paz con el EPL** 

En la comunicación los integrantes del EPL señalan lo que podrían entenderse como una posible **ruta de conversaciones** en las que señalan la participación popular como una de las principales banderas, así como las garantías de participación y problemas de orden social.

“Como rebeldes tenemos propuestas, en ese sentido estamos dispuestos a discutir y acordar con el gobierno una agenda mínima que destaque el delito político y el carácter beligerante de la guerrilla colombiana, las garantías para la participación popular en el debate nacional que proponemos, así como las salidas a los acuciantes problemas de la nación, como son el empleo, la salud, la educación, el bienestar social, el desarrollo productivo y tecnológico, la protección del campo, entre otros.”

**Además solicitaron la derogatoria de la Directiva del Ministerio de Defensa No 015 del 22 de abril de 2016 que los califica como Bacrim**, así como el desmonte del ESMAD y lo que llamaron la criminalización de la protesta social.

En la carta los integrantes del EPL señalan que han observado las conversaciones de paz con las FARC y con el ELN y por ello también están dispuestos a entrar en un proceso “Asumimos con seriedad y decisión este nuevo reto que nos demanda la historia”.

“Aspiramos a que la apertura de estos diálogos, contribuya de manera positiva a dar salida política al conflicto, que tal como la sociedad entera lo reclama, pasa por el real y efectivo compromiso del gobierno en la solución de los grandes problemas y demandas del pueblo colombiano.” **Aún se desconoce si hay en marcha unas conversaciones formales**, sin embargo se esperaría un pronunciamiento por parte del gobierno nacional.

###### Reciba toda la información de Contagio Radio en [[su correo]
