Title: "Pedimos participación directa, voz y voto en el nuevo Pacto Social": Conpaz
Date: 2016-10-03 12:30
Category: Nacional, Paz
Tags: Asamblea Nacional Constituyente, plebiscito por la paz, Red CONPAZ
Slug: pedimos-participacion-directa-voz-y-voto-en-el-nuevo-pacto-social-conpaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/conpaz.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CONPAZ 

###### 3 Oct de 2016 

La red de Comunidades Construyendo Paz en los Territorios, CONPAZ, emitió dos comunicados dirigidos al presidente Santos y al ELN, en los que **expresan su compromiso firme en la construcción de Paz a pesar de los resultados del plebiscito**, y piden ser incluidos en la construcción del Nuevo Pacto Ético Social.

La petición que hacen al presidente Juan Manuel Santos es clara, participación directa, voz y voto calificado de las comunidades más azotadas por el conflicto armado, social y político, en la **conformación de una constituyente que ponga a dialogar a todos los sectores implicados,** teniendo en cuenta los resultados del día de ayer en el plebiscito por la paz.

“Hemos sufrido la guerra y [debemos ser tenidos en cuenta en ese Pacto](https://archivo.contagioradio.com/perdonamos-y-decimos-si-al-plebiscito-victimas-de-la-chinita/). **No se trata solo de un Pacto solo social y político, también  ético  por la Vida y la libre expresión Digna y el respeto a las vidas**, quizás en un nuevo marco para la profundización nacional.”

CONPAZ saluda la decisión del presidente Santos de “no reversar lo logrado y mantener el cese bilateral del fuego”, y a su vez la afirmación de las FARC-EP de duplicar sus esfuerzos y aportes para una paz estable y duradera. **Recalcan la importancia de iniciar un [diálogo político para resolver los asuntos de la paz](https://archivo.contagioradio.com/colombianos-y-chilenos-invitan-a-conversar-sobre-el-plebiscito-de-paz/), y manifiestan que “debe incluirnos a todos los que somos parte del país”.**

Llaman la atención sobre el riesgo que corre la paz si el nuevo escenario de discusión se reduce a un pacto político de agrupaciones y partidos políticos, pues debe incluir a la sociedad organizada, a las víctimas y sobre todo, debe partirse de lo ya acordado.

En otro comunicado dirigido a Nicolás Rodríguez Bautista**,** Comandante del Ejército de Liberación Nacional, expresan de igual manera su interés de seguir participando activamente en la construcción de Paz e instan al ELN a **asumir el reto de seguir construyendo el camino hacia la paz, involucrándose desde una “perspectiva de participación transformante” en este nuevo pacto social.**

Afirman el derecho a la paz en una nueva democracia, manifestando que “independientemente de los resultados del día de ayer, creemos que se abren nuevos retos y desafíos para nuestros derechos a la Verdad, a la justicia y las garantías de no repetición sean una realidad”

<iframe id="audio_13163857" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13163857_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [bit.ly/1nvAO4u](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [bit.ly/1ICYhVU](http://bit.ly/1ICYhVU) 
