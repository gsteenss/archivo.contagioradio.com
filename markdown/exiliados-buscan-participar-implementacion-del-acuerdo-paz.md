Title: Exiliados colombianos exigen que "se detenga el genocidio político"
Date: 2016-11-25 15:43
Category: Nacional, Paz
Tags: Exiliados, FARC, Gobierno, Paramilitarismo, paz, víctimas
Slug: exiliados-buscan-participar-implementacion-del-acuerdo-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/2016-02-21-17.55.16.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colombianos en España 

###### [28 Nov 2016] 

Las y los Constituyentes exiliados políticos perseguidos por el Estado Colombiano que residen en Europa se dieron cita este sábado en Madrid, España, para para hacer un llamado al gobierno colombiano para que se **"detenga el genocidio político y se implementen el nuevo acuerdo de paz"** firmado entre el gobierno y las FARC.

La reunión extraordinaria se realizó también para establecer los planteamientos sobre los cuales las y los exiliados de España, Suiza, Bélgica Alemania e Inglaterra, pedirán **una reunión con la comisión de paz y el gobierno colombiano** para paticipar activamente en la implementación del acuerdo.

Nolasco Presiga, exiliado en Bélgica, afirma que se busca crear una organización como ente jurídico que reivindique la posibilidad del retorno, exigiendo sus derechos a la jusitica, verdad,  reparación económica y garantías de no repetición y seguridad, para que puedan retornar al país.

“**En estos momentos no existe una verdadera garantía por parte del Estado colombiano quien no reconoce el exilio político** como víctimas del terrorismo de Estado y además, está claro que las estructuras paramilitares se encuentran vigentes operativa, económica y militarmente en las cinco regiones de nuestro país y es un tema que el gobierno no quiere discutir más cuando se sigue revictimizando a la población, desplazando, persiguiendo, asesinando a los líderes y a los que quieran regresar al país”, asegura Jorge Freytter Florián, vocero  de “Constituyentes exiliados políticos perseguidos por el Estado Colombiano”.

También **se discutió una posible interlocución como víctimas del Estado en la mesa de diálogos entre el gobierno y el ELN **en Quito, Ecuador. Un plan estratégico instando a las Farc-ep y al gobierno colombiano pero también a la mesa de conversaciones que se abre con el ELN para que el exilio político tenga una protección no solo nacional a su regreso sino internacional que debe traducirse en que los exiliados que voluntariamente deseen regresar al país no pierdan su estatus de exiliado o refugiado durante un tiempo determinado, como lo explica Freytter.

Cabe recordar que la organización viene **exigiendo la inmediata puesta en funcionamiento y participación de las comisiones de la verdad y esclarecimiento y desarticulación del paramilitarismo** para exponer casos emblemáticos del exilio político.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
