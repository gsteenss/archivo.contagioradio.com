Title: Salir a las calles, lo único que queda para detener las reformas
Date: 2020-02-03 19:01
Author: CtgAdm
Category: Entrevistas, Movilización
Tags: Alicia Arango, Reforma laboral, Reforma Pensional
Slug: salir-a-las-calles-lo-unico-que-queda-para-detener-las-reformas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/paro-nacional-contra-reformas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Antes de dejar su cargo como ministra de trabajo, Alicia Arango (nueva ministra de interior) concedió una entrevista a Caracol Televisión en la que dió muestras de lo que incluiría una reforma laboral y pensional. Estas reformas, sumadas a la llegada de La U y Cambio Radical al Gobierno dejan un panorama de probables recortes a los derechos laborales, ante lo que quedaría solo el camino de la movilización para evitarlos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Desde el punto de vista institucional está claro que ellos van a implementar esas reformas: Daniel Libreros**

<!-- /wp:heading -->

<!-- wp:paragraph -->

**El profesor universitario y analista político Daniel Libreros,** destacó que el acuerdo entre Gobierno, Cambio Radical y La U ya había funcionado cuando se tramitó la reforma tributaria (Ley de Desarrollo Económico), lo que le permitió tener mayoría en el Congreso. Al tiempo, reseñó que la nueva Ministra de Interior dejó los parámetros de lo que será una reforma laboral y pensional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A estas reformas se las denominó el paquetazo, parte de los motivos del paro, que el Gobierno se enfocó en desmentir y decir que no existía, pero que "significa la flexibilización del trabajo y recortes a los derechos pensionales". Para el profesor, dichas reformas son exigencias del capital transnacional, del Banco Mundial, la Organización para la Cooperación y el Desarrollo Económicos (OCDE) y de los propios gremios del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si a ello se suma la potencia parlamentaria que tendría Duque para aprobar las propuestas con la adhesión de Cambio Radical y La U, Libreros concluyó que **"lo único que queda es la calle, porque desde el punto de vista institucional está claro, a mi entender, que van a implementar esas reformas".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, el Analista dijo que lo que queda en la calle es tratar de **organizar más el paro nacional: Con más fuerza y capacidad política.** (Le puede interesar:["Solidaridad: la fuerza que ha vencido la violencia en el Paro Nacional"](https://archivo.contagioradio.com/solidaridad-la-fuerza-que-ha-vencido-la-violencia-en-el-paro-nacional/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"Lo que está en cuestión es la forma de toma de decisión, de organización y resistencia popular "**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Libreros declaró que el problema que se presentó en el Encuentro Nacional de Organizaciones Sociales fue una división respecto al método, la convocatoria y la forma de tomar decisiones. En su opinión, el problema fue que el movimiento sindical quiso imponer una forma de tomar decisiones desde arriba, lo que es insuficiente para toda la explosividad que ha tenido este momento social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esa razón, señaló que **"lo que está en cuestión es la forma de toma de decisión, de organización y de [resistencia popular](https://www.justiciaypazcolombia.com/urge-mesa-distrital-de-seguimiento-al-ejercicio-del-derecho-a-la-protesta-comisiones-de-verificacion/)"**. (Le puede interesar: ["Las demandas de organizaciones defensoras de DD.HH. al Gobierno, en el Paro Nacional"](https://archivo.contagioradio.com/las-demandas-de-organizaciones-defensoras-de-dd-hh-al-gobierno-en-el-paro-nacional/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Camino a una democracia directa**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para encontrar respuestas a la pregunta sobre la forma, Libreros acudió a ejemplos como el Parlamento de los Pueblos, en Ecuador, donde todas las etnias del país tienen la posibilidad de discutir y llegar a acuerdos. Para el Profesor, esta es una posible respuesta a la crisis de la democracia representativa, que responde más al interés de las empresas que al de los ciudadanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esa razón, concluyó que es el momento de que el país discuta sobre cómo abrir el camino a una democracia directa, y menos una democracia representativa, buscando integrar al máximo a movimientos como los que están gestado el paro nacional. (Le puede interesar: ["Alianza de Gobierno y Cambio Radical no sorprende"](https://archivo.contagioradio.com/alianza-de-gobierno-y-cambio-radical-no-sorprende/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
