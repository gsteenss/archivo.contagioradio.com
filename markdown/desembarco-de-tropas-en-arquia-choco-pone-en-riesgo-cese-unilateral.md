Title: Desembarco de tropas en Arquía, Chocó, pone en riesgo cese unilateral
Date: 2015-01-27 16:13
Author: CtgAdm
Category: DDHH, Paz
Tags: Aida Avella, cese al fuego unilateral FARC-EP, paz
Slug: desembarco-de-tropas-en-arquia-choco-pone-en-riesgo-cese-unilateral
Status: published

###### Foto: pulzo.com 

##### [Aída Avella ] 

<iframe src="http://www.ivoox.com/player_ek_4006603_2_1.html?data=lZWdmJuUd46ZmKiakpmJd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhYa3lIqupsnFb6LqxtHZw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

A través de las redes sociales, **Pastor Alape**, integrante de la delegación de paz de las FARC, denunció que hay un “riesgo inminente de combates” puesto que desde el pasado 22 de enero se está presentando **desembarco de tropas en la zona de Arquía, Chocó**, sitio donde se dio la liberación del Gral Alzate en Noviembre de 2014.

\[caption id="attachment\_3742" align="aligncenter" width="468"\][![Pastor Alape](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/alape.jpg){.wp-image-3742 width="468" height="337"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/alape.jpg) Tuits enviados por Pastor Alape\[/caption\]

Según la propia denuncia del comandante guerrillero, desde el momento de la liberación del General Alzate se está **presentando una situación de zozobra y confinamiento** de la población por cuenta de la alta presencia militar.

Aida Avella, directora de la Unión Patriótica, e integrante del Frente Amplio por la Paz, afirma que a las fuerzas militares no les queda nada bien estar atacando a una guerrilla que está en tregua y pidió que se suspendan de inmediato estas acciones ofensivas, puesto que se está poniendo en riesgo el **cese unilateral que se ha cumplido a cabalidad** por parte de la guerrilla de las FARC.

Adicionalmente, Avella, denunció que en varias regiones del departamento del **Meta se continúan realizando operaciones ofensivas** y que son varias las denuncias que se reciben de los campesinos de esa región.
