Title: El viejo comedor de Úrsula Iguarán
Date: 2019-08-05 13:04
Author: CtgAdm
Category: Comunidad, Paz
Tags: cacarica, Comisión de Justicia y Paz, festivales de las memorias
Slug: bogota-fue-el-escenario-de-una-cena-por-la-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Festivales-de-las-memorias-cena-bogota-4.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Festivales-de-las-memorias-cena-bogota-3.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Festivales-de-las-memorias-cena-bogota-5.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Festivales-de-las-memorias-cena-bogota.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Festivales-de-las-memorias-cena-bogota-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/festival-de-las-memorias-cacarica.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/festival-de-las-memorias-cacarica-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/cena-festival-de-las-memorias.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/CENA-FESTIVAL.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

El pasado 24 de julio, en una casa del centro de Bogotá, se realizó un encuentro que solamente es posible en la Colombia del 2019. Detrás de unas puertas de cristal, víctimas del conflicto armado, ex integrantes de estructuras paramilitares, docentes de universidades públicas y privadas, ex combatientes de FARC, ex militares y líderes sociales, se reunieron entorno a unos pequeños baúles y marcos, para reconocer y decir la verdad, y de esta forma honrar la vida.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/cena-festival-de-las-memorias-1024x576.jpg){.aligncenter .size-large .wp-image-71937 width="1024" height="576"}

### **El camino de la memoria ** 

Desde las 7 de la noche y provenientes de diferentes latitudes, rostros afros, indígenas, blancos, mestizos, jóvenes, viejos y diversos, cruzaron las puertas verdes de una casa colonial. Pudo haber sido una casa en Macondo ,y el salón al que llegaron, el comedor de Úrsula Iguarán, con un puesto para cada uno de  las y los invitados.

Algunos de los asistentes ya se conocían previamente, otros se referenciaban por historias que habían llegado a sus  oídos, y aunque no se conocieran, al final tenían dos cosas en común: haber vivido de alguna forma el conflicto armado en Colombia y tener la plena voluntad por no permitir que el horror de la violencia regresara a los territorios del país.

Sin embargo, este encuentro no inició acá. De hecho, lleva un buen cúmulo de pasos detrás. Desde febrero de este año, la **Comisión Intereclesial de Justicia y Paz**, lanzo la iniciativa  "Festivales de las Memorias", espacios que están destinados a crear confianzas entre quienes han sido afectados por el conflicto armado y quienes fueron los responsables, a partir de el reconocimiento de las memorias sanadoras y el derecho restaurador.

Además, también buscan tejer una memoria incluyente, motivo por el cuál estos escenarios han contado con una participación transgeneracional,  a múltiples voces, en donde se reconocen las diferentes vivencias de las y los integrantes de las comunidades y sus apuestas.

Los dos primeros festivales se realizaron en la región del Bajo Atrato, específicamente en Cacarica y Alto Guayabal  y son estos lugares los que permitieron que a la casa de puertas verdes, hombres y mujeres llegaran con una voluntad renovadora para no permitir que la historia se repita y asistir a una cena de verdades.

### **El comedor de Úrsula ** 

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Festivales-de-las-memorias-cena-bogota.jpg){.aligncenter .size-full .wp-image-71649 width="1000" height="563"}

Los platos estaban servidos, Úrsula dispuso que esa noche se repartieran altas dosis de la pócima de Mélquiades contra la peste del Olvido, y de esta manera evitar que quienes estaban allí perdieran el rumbo, las fuerzas y la esperanza.

Entre las mesas habían magistrados de la Jurisdicción Especial para la Paz e integrantes de la Comisión de la Verdad. Habían personas que alzaron sus fusiles por más de 20 años y otros que actuaron como defensores de derechos humanos y junto a todos ellos, unos rostros blancos llamaron la atención.

Eran las y los líderes sociales que llegaron hasta la fría Bogotá para traer noticias amargas.  Viajaron desde lugares remotos como El Naya, en el departamento del Valle del Cauca, la Zona de Reserva Campesina de la Perla Amazónica, en Putumayo, el Litoral del bajo San Juan, en el Chocó y Curbaradó y el Alto Guayabal, en el Bajo Atrato.

Viajaron desde allá para denunciar las situaciones de riesgo que afrontan sus comunidades, como confinamientos por enfrentamientos entre grupos armados, reclutamiento a menores de edad, controles territoriales, amenazas a sus vidas y la falta de garantías a los derechos humanos del gobierno de Iván Duque.

Las y los asistentes prestaron suma atención a cada palabra, algunos movieron la cabeza en señal de tristeza, los rostros más jóvenes se sorprendieron con la realidad de la otra Colombia, mientras que los rostros blancos se reafirmaron en la defensa de la vida de sus comunidades y pidieron apoyo para frenar a quienes insisten en la guerra.

### Los regalos de Úrsula 

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/festival-de-las-memorias-cacarica-1-1024x683.jpg){.aligncenter .size-large .wp-image-71654 width="1024" height="683"}

Desde que iniciaron los "Festivales de las Memorias", se han escogido dos símbolos para recoger los recuerdos y la verdad, estos serán llevados a los distintos lugares de Colombia en donde se realicen más festivales. El primero de ellos son los marcos o porta retratos, que convocan a las y los desaparecidos.

El segundo símbolo son los baúles que contienen secretos, y que recogen las preguntas de quienes fueron afectados en medio del conflicto armado y las respuestas  por parte de los distintos responsables de los hechos.

En esta ocasión, en el comedor de Úrsula se repartieron 3 baúles, el primero fue entregado a un ex miembro de las Autodefensas Unidas de Colombia, el segundo fue entregado a una integrante del partido político FARC y el tercero lo recibió un ex militar comprometido con la verdad.

Cada cual con su baúl se comprometió a intentar dar respuestas a las preguntas de las comunidades y a devolverlo en otro espacio similar al comedor de Úrsula. Asimismo, tras haber escuchado a las y los líderes sociales, las tres personas expresaron su voluntad por no permitir que las prácticas del terror se apoderen de Colombia.

Sin embargo, también advirtieron que aquellas personas que están intentando buscar y reconstruir la verdad, desde sectores que tuvieron competencia en la guerra, están siendo víctimas de amenazas y persecuciones.

### **La casa de puertas verdes** 

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Festivales-de-las-memorias-cena-bogota-4.jpg){.aligncenter .size-full .wp-image-71646 width="900" height="506"}

Tras la entrega de los baúles y los marcos, los aplausos llovieron dentro del recinto. Una a una las personas fueron saliendo del salón, algunos sellaron sus encuentros con una fotografía, tal vez sea la foto que más tarde adorne uno de los porta retratos. Otros se quedaron en las esquinas de la casa conversando, más tarde fueron saliendo tras las puertas verdes hacia sus destinos, no sin antes asegurar su participación en las próximas cenas de la verdad.

Úrsula, ordenó que se recogieran los trastes y regresó a su cama, en dónde tuvo una larga charla con la muerte. Le contó los detalles de la velada, e incluso le recomendó que se abrigará, porque Macondo esa noche había reencarnado más fría.

Esa noche no llovió en Bogotá y por algunas horas, entre las sombras de la guerra que persiste en asesinar, el negacionismo y las trizas del dolor, una ventana se abrió hacia el futuro de la Colombia en paz, un país que se encuentra y se reconoce, uno que se esta tejiendo desde todas las latitudes para las generaciones venideras.
