Title: ELN entrega tres menores, y denuncia que estos fueron reclutados por el Ejército
Date: 2019-12-25 10:00
Author: CtgAdm
Category: DDHH, Nacional
Tags: ejercito, ELN, niños, policia, reclutamiento
Slug: eln-entrega-tres-menores-y-denuncia-que-estos-fueron-reclutador-por-el-ejercito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/615274_1.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-Pantalla-2019-12-25-a-las-13.07.45.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivo] 

Este lunes 23 de diciembre el Frente de Guerra Oriental (FGO) del Ejército de Liberación Nacional (ELN), entregó en la zona rural de Arauca a tres menores de edad, entre los 14 años y 15 años. Los menores fueron dados a la misión humanitaria encabezada por el obispo de esta región, Jaime Abril quien estuvo acompañado por la Defensoría del Pueblo.

En un comunicado entregado por los integrantes del ELN, denuncian que los 3 menores fueron entrenados por el Ejército y luego puestos en las filas del grupo guerrillero, ¨m*ediante labores de inteligencia logramos descubrir dentro de la población a 3 menores de edad, que fueron entrenados por militares colombianos para infiltrarse al ELN, estos fueron detenidos por nuestras unidades guerrilleras y entregados posteriormente*¨.

Asímismo aclararon que los tres menores fueron reclutados a los 10 años, dos de ellos en  Aguazul,Casanare, y el tercero en Yopal; actualmente tienen 14 y 15 años. (Le puede interesar: [FARC-EP anuncian que no reclutarán más menores de edad](https://archivo.contagioradio.com/farc-ep-anuncian-que-no-reclutaran-mas-menores-de-edad/))

### *¨El Estado colombiano recluta niñas y niños para la guerra¨ ELN *

En el mismo comunicado afirmaron que desde hace muchos años el Estado ha dispuesto sus puestos de mando, como batallones y estaciones de Policía, para acercarse a la comunidad, y despertar en los jóvenes un interés a pertenecer a estos grupos, *¨a los menores de edad los disfrazan con uniformes militares y hasta se maquillan con los mimetizajes de las tropas de asalto y se inducen a la guerra¨.*

Declararón también que en estos eventos van haciendo un tamizaje de los jóvenes, destacando a aquellos que cumplan con un perfil que llene las necesidades para cumplir tareas en la guerra, ¨Es *política oficial del Estado colombiano el reclutamiento de niñas y niños, para ponerlos a trabajar como agentes de inteligencia o espías de las Fuerzas armadas y de Policía¨, *señalan en el documento.

De igual forma aclararon que el reclutamiento de menores de edad es evidente en algunas regiones que cumplen un patrón como, ¨p*resencia guerrillera, en barrios populares, organizaciones sociales y en las escuelas y colegios, con trayectoria de lucha¨*.  (Le puede interesar:[Los cambios en el sistema de reclutamiento militar en Colombia)](https://archivo.contagioradio.com/militar-reclutamiento-colombia/)

Por último expresaron que, *¨El ELN no realiza reclutamientos como hace el Estado y que el ingreso a nuestras filas es un acto voluntario y consciente, mediado por un proceso de selección¨; e hicieron* una invitación a organizaciones internacionales defensores de Derechos Humanos a verificar esta situación, *¨Exigimos que se cancele el reclutamiento de menores de edad en Colombia¨.*

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
