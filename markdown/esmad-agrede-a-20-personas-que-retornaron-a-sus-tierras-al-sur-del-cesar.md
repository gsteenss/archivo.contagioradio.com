Title: 20 personas heridas deja agresión de ESMAD a familias en hacienda Bellacruz
Date: 2015-07-01 13:16
Author: AdminContagio
Category: Nacional, Resistencias
Tags: 14 de febrero de 1996, Avianca, bienestar Familiar, Carlos Arturo Marulanda, cesar, Davivienda, Defensoría del Pueblo, Derechos Humanos, Desplazamiento forzado, despojo de tierras, ESMAD, Germán Efromovich, Hacienda Bella Cruz, ICBF, Mr Inversiones, retorno
Slug: esmad-agrede-a-20-personas-que-retornaron-a-sus-tierras-al-sur-del-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/desalojo-del-14-d-feb-061.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[colombiaespasion.org]

<iframe src="http://www.ivoox.com/player_ek_4710303_2_1.html?data=lZyekpiUd46ZmKiak5qJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcXZzcvcjbfTqNOZpJiSo6nLucbuhpewjabXs8TdwsjWh6iXaaOnz5Cww9LUqdTdz8aYxsqPiMbn0dHO3MbIs9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe src="http://www.ivoox.com/player_ek_4710317_2_1.html?data=lZyekpiVe46ZmKiakp2Jd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTBoqmYw8zWqcXZjMaYxcbRtMbnytPOxtSPtdbZjM7b1srSuMKf08rh0dfSpdOfwpC1w8jNqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Adelfo Rodríguez, Asociación Campesina de Desplazados al Retorno, ASOCADAR.] 

###### [1 Jul 2015] 

Desde las 5 de la mañana de este martes, **260 familias retornaron a la Hacienda Bellacruz, en La Gloria, al sur del Cesár, tras 19 años de haber sido despojados de sus tierras por grupos paramilitares usados por empresarios** para adquirir sus predios. Ahora la comunidad que intenta restablecerse, es víctima de agresiones por parte del ESMAD que sin contar con una orden legal intenta desalojarlos.

Son  **60 mujeres, 120 niños y 180 hombres, los que han decidido volver a sus territorios, exigiendo justicia** y una repuesta por parte del gobierno sobre el despojo de tierras y las fosas comunes donde están enterradas familias enteras, víctimas de empresarios de Davivienda, MR Inversiones LTDA y Avianca, cuyo presidente, Germán Efromovich, tiene el mayor número de terrenos, que le fueron vendidos por el empresario Carlos Arturo Marulanda, exministro en gobierno de Samper .

De acuerdo con Adelfo Rodríguez, integrante de la Asociación Campesina de Desplazados al Retorno, ASOCADAR, **Germán Efromovich, usó los baldíos para sembrar palma aceitera y piña,** cultivos que han secado las fuentes hídricas, dejando a los campesinos “acorralados” sin donde pescar ni sembrar.

A manos de paramilitares y empresarios, la comunidad vive “una situación muy crítica por monocultivo de palma aceitera… **las empresas se encargaron de nuestras familias,  las mataron y desaparecieron**”, cuenta Adelfo, afirmando además que "los empresarios y despojadores acabaron la generación de empleo y en los empleos que ofrecían los sueldos eran irrisorios”.

“Sabemos que va a haber una arremetida fuerte”, expresó el martes Rodríguez a través de los micrófonos de Contagio Radio, palabras que cobran vida este miércoles cuando los campesinos, desplazados forzosamente el** 14 de febrero de 1996**, están siendo agredidos por el ESMAD, acompañados por hombres encapuchados, según manifiesta la comunidad.

**20 personas aproximadamente han resultado heridas, entre ellas un joven al que se le dispararon en una pierna, niños y niñas heridas y una mujer embarazada**. Además, de robar algunos implementos a la comunidad.

La comunidad hace un llamado para que desde las instituciones gubernamentales como la **Defensoría del Pueblo** y otras organizaciones defensoras de derechos humanos hagan un acompañamiento al campesinado, teniendo en cuenta que no han recibido ningún tipo de ayuda desde el gobierno para que el proceso de retorno se desarrolle con las garantías necesarias.

**“Nosotros nos somos invasores, estamos recuperando nuestras tierras,** Bienestar Familiar y la alcaldía nos están proponiendo negociar con la empresa, pero no tenemos nada que mediar, **Incoder es el que tiene que resolver ese problema**”, expresa Adelfo.
