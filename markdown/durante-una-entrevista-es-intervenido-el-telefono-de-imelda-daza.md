Title: Durante una entrevista, es intervenido el teléfono de Imelda Daza
Date: 2017-01-31 12:26
Category: DDHH, Nacional
Tags: imelda daza, interceptaciones ilegales, voces de paz
Slug: durante-una-entrevista-es-intervenido-el-telefono-de-imelda-daza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/imelda_Daza.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: paréntesis Cali] 

###### [31 Ene 2017] 

En horas de la mañana fue intervenida la línea telefónica de la vocera del **Movimiento Voces de Paz, Imelda Daza**, durante una entrevista al aire con Contagio Radio sobre la Jurisdicción de Paz y su avance en el Congreso de la República . La líder social, quien en varias ocasiones ha visto amenazada su integridad, manifestó al equipo que **por unos segundos escuchó a través de su celular una voz masculina que la insultaba con tono amenazante.**

Imelda Daza, también sobreviviente del genocidio de la Unión Patriótica, fue víctima recientemente, en Mayo de 2016, de **un atentado en su contra mientras estaba en las instalaciones del Sindicato Unitario de Trabajadores de la Industria de Materiales** para la Construcción –SUTIMAC– en la ciudad de Cartagena.

SUTIMAC, puso en conocimiento de la Defensoría del Pueblo las imágenes capturadas por las cámaras de seguridad, donde se logra ver el rostro del agresor y el momento en que ingresa y sale de la sede portando un arma. Hasta el momento no se ha resuelto el caso, y la **versión de las autoridades es que se trató de un intento de robo de una cadena de oro** a uno de los escoltas.

Por último la vocera de Voces de Paz, hace un llamado a las organizaciones defensoras de Derechos Humanos, a los medios de comunicación y sobre todo, al Gobierno Nacional para que **vele por la seguridad de los integrantes de movimientos y organizaciones sociales que vienen siendo hostigados,** amenazados y asesinados, desde el inicio de la fase de implementación de los acuerdos.

> Esto pasó mientras [@Imeldadaza](https://twitter.com/Imeldadaza) era entrevistada por [@Contagioradio1](https://twitter.com/Contagioradio1) <https://t.co/dleG5evYaC> [\#MujeresXLaPaz](https://twitter.com/hashtag/MujeresXLaPaz?src=hash) [pic.twitter.com/3qHnV6Gj48](https://t.co/3qHnV6Gj48)
>
> — Contagio Radio (@Contagioradio1) [31 de enero de 2017](https://twitter.com/Contagioradio1/status/826511420397350917)

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
  

<iframe id="audio_16745927" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16745927_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
