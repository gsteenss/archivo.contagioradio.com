Title: Los riesgos para la Ley de tierras de no implementarse vía Fast Track
Date: 2017-07-24 11:58
Category: Nacional, Paz
Tags: Fast Track, Ley de Tierras
Slug: peligra-ley-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Restitución-de-Tierras-e1510251256744.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Informador] 

###### [24 Jul 2017] 

El Ministro del Interior, Guillermo Rivera afirmó la semana pasada que hay posibilidades de que la ley de Tierras, uno de los puntos más importantes de los acuerdos de paz, no sea tramitada vía Fast Track sino a través de legislación ordinaria, afirmación que para el senador Iván Cepeda, **es preocupante porque se abre la puerta a modificaciones y cambios en la esencia de los acuerdos.**

Para el senador, ésta posibilidad vendría acompañada por una intención de frustrar la posibilidad de una “auténtica ley de tierras” con relación a los acuerdos de paz y modificar los mecanismos que allí se establecen para que grandes empresarios y latifundistas puedan fortalecer un modelo en el que “**el acceso de tierras esta únicamente a disposición de los grandes propietarios y no de los campesinos y comunidades rurales más pobres**”.

De igual forma, Cepeda manifestó que aún se desconoce de dónde van a provenir los terrenos que alimentarán el fondo de tierras creado en el acuerdo de paz, que debería contar con más de **tres millones de hectáreas, y que tampoco se han fortalecido las vías para hacerlo posible**. (Le puede interesar:["Periodo legislativo será decisorio para implementación de Acuerdos de Paz"](https://archivo.contagioradio.com/inicio-de-periodo-legislativo-sera-decisorio-para-implementacion-de-acuerdos-de-paz/))

En apreciación del Senador si la Ley de Tierras llegará a quedar fuera del Fast Track, se estarían irrespetando e incumpliendo los acuerdos de paz, por tal razón hizo un llamado a organizaciones sociales y al movimiento popular para que “**salga a defender el derecho a la tierra**”, y anunció que en las próximas semanas citara una audiencia de control político para saber qué proyecto de ley se van a pasar al Congreso y si cumplen o no con la esencia de los pactado en La Habana.

### **El estado de las tierras en Colombia** 

De acuerdo con el más reciente informe de la organización OXFAM, Colombia es uno de los países con una de las mayores brechas en la tenencia de tierra, donde** el 1% de la población ocupa el 80% de las tierras del país, mientras que el otro 99% restante ocupa solo el 19% de tierras.**

De igual manera, el informe “Radiografía de la desigualdad: lo que dice el último censo agropecuario sobre la distribución de tierra en Colombia” reveló que las grandes fincas del país contienen extensos territorios improductivos u ocupados por la ganadería extensiva, con un área insignificante dedicada a cultivos agrícolas que casi siempre están dedicadas a la agroexportación. (Le puede interesar: ["El 1% de propietarios ocupa más del 80% de las tierras en Colombia"](https://archivo.contagioradio.com/el-1-de-propietarios-ocupa-mas-del-80-de-tierras-en-colombia/))

<iframe id="audio_19966936" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19966936_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
