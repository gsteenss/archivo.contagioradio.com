Title: Rousseff presentará su defensa ante Comisión liderada por la oposición
Date: 2016-04-26 15:59
Category: El mundo, Otra Mirada
Tags: Comisión Senado Brasil, Dilma Roussef impeachment, Golpe de estado Brasil
Slug: rousseff-presentara-defensa-ante-comision-liderada-por-la-oposicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Senado-Brasil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Conclusión.com.ar 

###### [25 Abr 2016] 

Fue elegida por parte del Senado Federal Brasileño, la comisión compuesta por 21 miembros titulares con representación en la Cámara Alta y 21 suplentes, que tendrá la tarea de evaluar el pedido de juicio político contra la mandataria Dilma Rousseff, con el que se ["rompería la base de la democracia"](https://archivo.contagioradio.com/impeachment-rompe-la-base-de-la-democracia-rousseff/) pasando por encima de la decisión de 54 millones de ciudadanos.

Con la elección este martes de los senadores Raimundo Lira del disidente PMDB como presidente, y Antonio Anastasia del opositor PSDB como relator, la comisión deberá escuchar durante 10 días los argumentos de acusación y a la defensa de Rousseff, situación que dificultaría mucho más la permanencia de Rousseff en su cargo.

Adicional a la ventaja alcanzada por la oposición en el proceso de juicio, está  la situación legal de más de un tercio de los 21 miembros de la comisión, quienes enfrentan procesos de investigación por el Supremo tribunal, cuatro de los cuales figuran en la lista de políticos denunciados por el caso "Lavacoches", relacionado con el desvío de fondos de la estatal Petrobras.

**Lo que viene en el proceso**

Luego de escuchar las partes, la comisión deberá **presentar un dictamen aproximadamente el lunes 9 de mayo**, luego de esto deberá realizarse una votación que aprobará o archivará el caso por mayoría simple, de presentarse la primera, pasará a ser votado en plenaria donde requerirá la mitad más uno de los votos (41 de los 81 parlamentarios).

Si se presenta consenso por parte del Senado, la mandataria será separada de su cargo durante los 180 días que puede demorar el proceso de destitución ante lo cual Michel Temer asumiría la presidencia del país.
