Title: "Hay que descarrilar la locomotora minera": Ambientalistas
Date: 2017-08-08 13:49
Category: Ambiente, Voces de la Tierra
Tags: ambientalistas, defensa del territorio, locomotora minera, modelos de desarrollo
Slug: hay-que-descarrilar-la-locomotora-minera-ambientalistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/mineriaypaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [08 Ago 2017] 

Diversos grupos de ambientalistas se reunieron en Fusagasugá, Cundinamarca el pasado sábado 05 de agosto, para **fortalecer los movimientos que buscan la defensa de los territorios** frente al modelo extractivista que los ambientalistas califican como un amenaza y que "se ha impuesto en el país". Ellos y ellas han venido desarrollando propuestas como alternativas hacia un modelo de desarrollo diferente.

Según Renzo García, miembro del Comité Ambiental del Tolima , delegaciones de 12 regiones se reunieron para fortalecer los espacios de unidad de los defensores del ambiente como respuesta a las afirmaciones del Gobierno Nacional, quien en repetidas ocasiones ha dicho que **“las consultas populares son un estorbo para procesos extractivos”.** (Le puede interesar: ["Ibagué se convierte en la primera ciudad capital de Colombia en prohibir la minería"](https://archivo.contagioradio.com/ibague-se-convierte-en-la-primera-ciudad-capital-en-prohibir-la-mineria/))

De igual forma, García manifestó que están desarrollando **propuestas para descarrilar la locomotora minera** que “está pasando por encima de las comunidades, afecta el medio ambiente y la soberanía alimentaria de los territorios”. Como alternativa, buscan promover propuestas ecoturísticas “para explotar las riquezas del suelo colombiano y no del subsuelo”.

De igual manera, han tenido en cuenta las alternativas que se proponen en ciudades como Londres donde, de acuerdo con García, **“hay 2 millones de habitantes que hacen avistamiento de aves”**. El ambientalista recordó que Colombia es uno de los países con mayor diversidad de especies de aves y esta actividad serviría para “generar un bienestar colectivo dirigido hacia un nuevo modelo de desarrollo”. (Le puede interesar:["La propuesta de Pijao como alternativa tras consulta popular"](https://archivo.contagioradio.com/la-propuesta-de-pijao-como-alternativa-a-la-mineria-tras-consulta-popular/))

### **Grupos ambientalistas enviarán carta a la Corte Constitucional** 

Dentro del grupo de los ambientalistas está la iniciativa de enviar una carta a la Corte Constitucional para que **“modifique los criterios de la mesa de trabajo que estudia el impacto de la minería”**. De igual forma, en la carta promueven que haya una participación de investigadores y profesionales que estén conectados con los territorios y conozcan las dinámicas que ocurren allí.

De igual forma, le entregarán una carta al grupo de trabajo de Derechos humanos de las Naciones Unidas para que **se protejan los derechos colectivos al medio ambiente y la defensa de los territorios**. Estas iniciativas van encaminadas a no quedarse “en una postura de rechazo al modelo minero energético del país sino a aportar ideas hacia la construcción de un nuevo modelo de desarrollo”.

<iframe id="audio_20229369" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20229369_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
