Title: Transformación del modelo energético para la construcción de paz con justicia social y ambiental
Date: 2016-09-01 10:09
Category: Opinion
Tags: Mineria, plebiscito por la paz, proceso de paz
Slug: transformacion-del-modelo-energetico-para-la-construccion-de-paz-con-justicia-social-y-ambiental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/mineriaypaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gabriel Galindo 

#### **Por [CENSAT Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### 1 Sep 2016 

A un día de anunciarse la firma de los acuerdos de la Habana entre el gobierno colombiano y las FARC, organizaciones sindicales y movimientos sociales instalaron formalmente la Mesa Social Minero Energética y Ambiental por la Paz bajo el empeño de construir paz desde los territorios, impulsando la permanencia y el buen vivir sin extractivismo.

La instalación se llevó a cabo el 25 de agosto en el Auditorio Gonzalo Jiménez de Quesada en Bogotá con la presencia de hombres y mujeres de diversos lugares del país, trabajador@s de empresas del sector energético afectados por represas, minería y emprendimientos petroleros, defensores de derechos humanos y ambientales, la Ministra de Trabajo Clara López, y la Delegada de la Unión Europea para Colombia y Ecuador.

La apuesta de establecer este diálogo entre trabajadores y afectados en Colombia es histórica, pertinente y trascendental, pues más allá de construir propuestas en conjunto entre los sectores participantes, se busca interactuar con los planificadores de la política minero energética en Colombia con el propósito de garantizar transformaciones orientadas a una política minero energética que respete la vida en el territorio y atienda  los desafíos del progreso y el buen vivir tanto del campo como la ciudad.

Estas discusiones han estado presentes en las luchas de los movimientos populares y de trabajadores desde hace décadas. De hecho, la Mesa Social se instaló en un día emblemático: el 25 de agosto, cuando se conmemoraba el aniversario 65 de la creación de Ecopetrol, la empresa nacional petrolera surgida gracias a las luchas de los trabajadores petroleros.

Más aún y más recientemente, la construcción de este espacio se fundamenta en los debates y discusiones que se sostuvieron a lo largo y ancho del país durante la Segunda Asamblea por la Paz, Territorios con Energía Construyendo Paz para la Nación, realizada en el año 2015, y organizada por la Unión Sindical Obrera, el Ministerio de Trabajo y la Universidad Nacional de Colombia, y que consistió en la realización de 13 asambleas regionales y 53 asambleas subregionales por la paz, en las que participaron más de 10.000 personas y 1.800 organizaciones. Finalmente, este ejercicio deliberativo culminó con la reunión de más de 1500 delegados y delegadas de todo el territorio nacional durante 2015 en Bogotá.

A grandes rasgos estos espacios han concluido que el futuro más próspero para el país está en virtud de la transformación del modelo minero energético, donde se priorice el tránsito energético, se promocione la soberanía y autonomía energética e hídrica, la gestión comunitaria del agua y la energía, la garantía de derechos de los trabajadores y de los afectados, la reparación histórica de los afectados, el fortalecimiento de las empresas públicas, el derecho al agua y a la energía, la promoción de alternativas energéticas, el reconocimiento de los derechos de la naturaleza, la redistribución de utilidades y la preservación de las fuentes hídricas. Esas son, entre otras, las propuestas y apuestas de este espacio de diálogo social que además propone herramientas como la creación de una comisión nacional de represas y comisión de la verdad ambiental

Estas demandas están presentes en las múltiples manifestaciones que organizaciones defensoras de los territorios, de las aguas, de trabajadores, étnicas, campesinas y ambientalistas realizan a lo largo y ancho del país, contra un modelo minero – energético, que desconoce la rica diversidad ambiental, social y cultural, la importancia de nuestras montañas, ciénagas, mares y sábanas, nuestra riqueza hídrica. Estos movimientos regionales y nacionales que se expresan creativamente desde la Guajira hasta el Putumayo, y desde el Chocó hasta la Amazonía demandan un cambio al modelo extractivo despojador los territorios de los pueblos ancestrales, como también lo ha venido planteando la Cumbre Nacional Campesina, étnica y Popular.

Dar trámite a estas propuestas, y otras que emanen de la MSMEA por parte del gobierno colombiano, es la ruta de construcción de paz en los territorios. Ello significa atender las causas estructurales que han dado origen a varios de los conflictos armados. Así se evitaría la generación de más conflictos socioambientales que no son convenientes ni rentables para el país, a partir de respetar los derechos de la naturaleza humana y no humana como parte de la construcción de una verdadera paz integral, con justicia social y ambiental.

Los planes de vida, planes de permanencia, entre otros, debe ser apoyados y respetados por el Estado Colombiano, decidir entre todos es la ruta adecuada para la construcción de paz con justicia social y ambiental, por tanto, es necesario sumarse a las convocatorias de participación ciudadana como la consulta popular de Ibagué y a la vez rechazar la criminalización y ola de asesinatos de la que están siendo víctimas los defensores de los derechos de los trabajadores y defensores de la naturaleza.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
