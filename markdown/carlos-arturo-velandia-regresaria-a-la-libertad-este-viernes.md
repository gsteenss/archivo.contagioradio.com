Title: Carlos Arturo Velandia regresaría a la libertad este viernes
Date: 2016-07-27 17:37
Category: Nacional
Tags: Carlos Arturo Velandia, ELN, gestor de paz, paz
Slug: carlos-arturo-velandia-regresaria-a-la-libertad-este-viernes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Libertad-Carlos-Velandia.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MCB medios comunitarios 

###### [27 Jul 2016] 

Carlos Arturo Velandia y  Antonio Bermúdez Sánchez fueron designados como gestores de paz por la presidencia de la República, lo que significa que en un plazo máximo de 2 días Velandia regresaría a la libertad tras la suspensión de las órdenes de captura, **lo que no significa que continúe el proceso judicial en su contra.**

La figura de gestor de paz le da la posibilidad al gobierno de suspender órdenes de captura o incluso penas, como una forma de intercambio en la medida en que Velandia y Bermúdez se comprometan con el gobierno nacional a asesorarlo para el **acercamiento con grupos insurgentes, avanzar en escenarios de pedagogía de paz y la asesoría en la implementación de los acuerdos.**

Esas laborales, según explica Franklin Castañeda, abogado de Velandia, deberán someterse a la entrega de informes mensuales a la Oficina del Alto Comisionado para la Paz. Tareas soportadas por unas cartas de compromiso con el gobierno firmadas por los recién designados como gestores de paz.  Se trata entonces de “**una medida política con la que se busca continuar con el papel que ha jugado Carlos Velandia en el acercamiento con la guerrilla”.**

Sin embargo, como continúa el proceso, Castañeda asegura que la defensa del gestor de paz se prepara para enfrentar nuevos procesos en su contra, pero lo importante con esta medida del gobierno es que se podrá evitar otras ordenes de captura.

De acuerdo con el abogado, “**la investigación de la Fiscalía General en contra de Carlos Arturo Velandia tiene muchos vacíos** y además procedimentalmente tiene varias irregularidades pues en el momento en que se realiza su captura los plazos y términos procesales estaban prácticamente agotados”.

Por el momento, **este 18 de agosto a la 1:30 de la tarde se realizará un nuevo juicio en el caso de Velandia,** pero tras la medida se evitará que este entre 2 a 3 años privado de su libertad, “mientras los abogados ejercen su defensa y además no se priva al país de su labor como gestor de paz”, señala Franklin Castañeda.

<iframe src="http://co.ivoox.com/es/player_ej_12358976_2_1.html?data=kpegl52de5ehhpywj5WbaZS1lZuah5yncZOhhpywj5WRaZi3jpWah5yncafmwtPYzs7Sb6TV1NnOh6iXaaOlxsnOh5enb6LW0MzOxtSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
