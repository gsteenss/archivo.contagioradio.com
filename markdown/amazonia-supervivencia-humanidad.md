Title: Con la Amazonía está en juego la supervivencia de la humanidad
Date: 2019-08-26 13:26
Author: CtgAdm
Category: Ambiente, DDHH
Tags: Agua, Amazonía, Incendio, Selva
Slug: amazonia-supervivencia-humanidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Amazonía.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-26-at-9.35.56-AM-1-e1566850696141.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @sanchezcastejon  
] 

Se cumplen más de 20 días desde que iniciaron los incendios en la Amazonía, situación que pone en riesgo a más de 350 pueblos indígenas de Brasil y Bolivia, así como la supervivencia misma de la humanidad. Según expertos, esta selva, considerada el pulmón de la tierra, **es también una de las grandes productoras de agua, pero las llamas estarían afectando el ciclo hidrológico,** cuya recuperación es difícil y requiere de un apoyo contundente.

La **profesora de la Universidad de la Amazonía y coordinadora de la Mesa para la Defensa del Agua y el Territorio del Caquetá Mercedes Mejía,** resaltó que esta región "es la responsable del equilibrio del clima mundial". Por lo tanto, cada hectárea perdida (hasta el pasado 23 de agosto se calculó que se habían perdido 471 mil hectáreas) significa poner en riesgo especies de flora y fauna, así como la posibilidad de supervivencia de la gente que habita en la selva. "Tanto de los indígenas que están en contacto, como de los no contactados y los que están en aislamiento voluntario", concluyó la experta.

### **Tras los incendios habrá problemas de hambre y sed** 

![Región de la Amazonía](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-26-at-9.35.56-AM-1-e1566850696141.jpeg){.aligncenter .size-full .wp-image-72574 width="676" height="606"}

Mejía resaltó que las personas que están en el área de influencia de la Amazonía se verán afectadas por el incendio, pues el incremento de la temperatura en la selva genera reacciones químicas y cambios súbito en el ambiente de la región; a ello se suman los efectos del humo "y lo que queda tras los incendios que se va desplazando, y afecta otras áreas". Según la profesora, **ambos hechos generarán hambre, por la escasez de alimentos que suelen consumir los habitantes de esta región.**

En cuanto al agua, la experta enfatizó que **"el Amazonas le provee 20 mil millones de metros cúbicos de agua al ambiente a diario", un ciclo hidrológico que cambiará**, y afectará la cantidad de líquido que emana de esta zona tropical. Mejía afirmó que incluso, algunos ríos que nacen en la selva ya han visto la reducción de sus caudales producto de un proceso de evaporación que se ha acelerado. (Le puede interesar:["Arde la Amazonía: 72.843 focos de incendio registrados en 2019 "](https://archivo.contagioradio.com/arde-la-amazonia-72-843-focos-de-incendio-registrados-en-2019/))

### **El modelo económico no permitiría que se de la recuperación de la Amazonía** 

> La selva del Amazonas es considerada el bosque tropical más grande del mundo produce el 20% del oxígeno en la atmósfera terrestre. El pulmón del planeta está en estado de emergencia.  
> [\#PrayForTheAmazon](https://twitter.com/hashtag/PrayForTheAmazon?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/LzyhXzvmiV](https://t.co/LzyhXzvmiV)
>
> — Marlop (@anelmarlop) [August 22, 2019](https://twitter.com/anelmarlop/status/1164353784685875201?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
La profesora indicó que según estudios realizados en determinados puntos de la Amazonía colombiana, **la recuperación tras un incendio puede tomar entre 40 y 50 años, pero ello ocurre "si no hay más presión sobre la selva".** Esta presión, que también sería la causante de los incendios, proviene de la intención de ampliar la frontera agrícola para agronegocios y ganadería extensiva, así como debido a las altas temperaturas.

En ese sentido, Mejía concluyó que **bajo el actual modelo económico y la globalización, es difícil que se de una recuperación de la selva**; "este es un tema que debería ser el de primer importancia a nivel mundial, porque es la supervivencia de la raza humana", finalizó. (Le puede interesar:["Incendios en Amazonas amenazan a 350 pueblos indígenas de Brasil y Bolivia"](https://archivo.contagioradio.com/incendios-en-amazonia-amenazan-a-350-pueblos-indigenas-de-brasil-y-bolivia/))

<iframe id="audio_40562168" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_40562168_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
