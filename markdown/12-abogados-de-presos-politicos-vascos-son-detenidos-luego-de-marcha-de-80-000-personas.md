Title: 12 abogados de presos políticos vascos son detenidos luego de marcha de 80.000 personas.
Date: 2015-01-15 18:14
Author: CtgAdm
Category: El mundo, Paz
Tags: Destacado, españa, presos politicos, vascos
Slug: 12-abogados-de-presos-politicos-vascos-son-detenidos-luego-de-marcha-de-80-000-personas
Status: published

###### Foto: laizquierdadiario.com 

**Un día después de la gran marcha, el estado español detiene a 12 abogados ligados al movimiento independentista vasco y los libera al día siguiente.**

Diversas organizaciones sociales y colectivos del movimiento independentista vasco, así como familiares y amigos de los presos políticos realizaron una macromarcha a favor de los derechos de los presos y en contra de la política de dispersión que aplica el estado español en su contra que impide, como indica el derecho internacional, que el propio preso pueda cumplir la pena en la prisión más cercana a su lugar de origen.

Esta medida dispersa a los presos políticos por cárceles de todo el estado alejándolos de su hogar, afectando también a los familiares, que tienen que recorrer cientos de kilómetros para poder realizar las visitas en la cárcel.

Las consecuencias de la gran marcha no se han hecho esperar. En lo que se ha conocido como la operación  “Jaque Mate”, el estado español ha detenido a 12 abogados del movimiento independentista y ha allanado varias sedes de organizaciones sociales, así como la del sindicato abertzale LAB.

Los detenidos han salido en libertad con cargos al día siguiente. Además los letrados estaban en un hotel de Madrid a la espera de la celebración del macrojuicio político contra la izquierda abertzale.

Muchos de estos abogados han conseguido desestimar la “doctrina Parot”, en el tribunal de derechos humanos de Estrasburgo. Hay recordar que la doctrina impedía solicitar excarcelaciones por beneficios penitenciarios antes de cumplir la pena integra (máximo 30 años).

Diversas organizaciones sociales de abogados de derechos humanos así como voces del mundo de la política y del periodismo han expresado su preocupación por las detenciones, entendiéndolas como una venganza o como un mero acto de represión por parte del estado español contra la izquierda abertzale.
