Title: General (r) Rito Alejo del Río comparecerá ante la JEP en diciembre
Date: 2020-10-31 21:29
Author: AdminContagio
Category: Actualidad, Judicial
Tags: JEP
Slug: general-r-rito-alejo-del-rio-comparecera-ante-la-jep-en-noviembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/rito-alejo-del-rio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: Canal 1

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el próximo 11 de diciembre a las 8 a.m, la Sala de Reconocimiento de la Verdad de la Jurisdicción Especial para la Paz -JEP- programó la versión voluntaria del general (r) Rito Alejo del Río en el macro del caso 004 referente a la ‘situación territorial de la región del Urabá. Con su testimonio, la JEP espera que Del Río pueda aportar en la construcción de verdad tras el conflicto armado que se vivió y se vive en el Bajo Atrato.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A finales de septiembre de 2017, el general (r) Rito Alejo fue dejado en libertad luego de ser beneficiado por la Justicia Especial para la Paz; tras pasar nueve años detenido, de un total de 25 años y 10 meses, por la muerte del campesino Marino López cuando dirigía la Brigada XVII, que durante cuatro días desplegó la incursión militar conocida como "Operación Génesis” en febrero de 1997.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras por aire se coordinaron ametrallamientos y bombardeos, por tierra, fuerzas combinadas militares e irregulares -paramilitares- realizaron quemas de casas, saqueos, amenazas de muerte y posteriormente asesinaron a Marino López, tales sucesos además ocasionaron el desplazamiento de 3.000 personas de Cacarica y de otras 7.000 del Bajo Atrato. [(Lea también: Juez exige cumplimiento inmediato de medidas de reparación a comunidades en Cacarica, Chocó)](https://archivo.contagioradio.com/incidente-de-desacato-obliga-al-estado-a-cumplirle-a-las-comunidades-de-cavida/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El uniformado había sido investigado por la Fiscalía no solo por este crimen sino por otros casos como la masacre de Mapiripán en Meta en 1997, cometida en julio del mismo año por parte de las Autodefensas Unidas de Colombia -AUC-, o el asesinato de Jaime Garzón. [(Lea también: Víctimas piden a la JEP hacer públicas declaraciones de militares comparecientes)](https://archivo.contagioradio.com/victimas-piden-a-la-jep-hacer-publicas-declaraciones-de-militares-comparecientes/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Organizaciones defensoras de DD.HH. han denunciando la alianza que existió entre Del Río y el paramilitarismo, pese a que el uniformado en retiro lo ha negado y ha afirmado que no va a aceptar verdad ni responsabilidades pese a que se ha establecido que tenía conocimiento de las actividades que estaban realizando los paramilitares, con colaboración de las Fuerzas Armadas y no hizo nada por evitar las acciones violentas. [(Lea también: Cinco casos por los que la JEP pone la lupa a crímenes en Urabá y Bajo Atrato Chocoano)](https://archivo.contagioradio.com/cinco-casos-los-la-jep-pone-la-lupa-crimenes-uraba-atrato-chocoano/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ahora al acogerse a la Jurisdicción Especial para la Paz, aceptó acudir cada que sea llamado por cualquiera de los organismos que hacen parte del Sistema Integral de Justicia, Verdad, Reparación y No Repetición. Es necesario resaltar que al interior del caso 004, según la JEP se ha vinculado a 249 personas en servicio activo, retirados y separados parte del Ejército Nacional, exintegrantes de las FARC-EP y terceros civiles. [](https://archivo.contagioradio.com/el-despojo-de-tierras-en-el-bajo-atrato-sigue-vigente/)[(Lea también: Urabá fue laboratorio de lo que pasó entre militares y paramilitares en el resto del país: Coronel (R) Velásquez)](https://archivo.contagioradio.com/uraba-fue-laboratorio-de-lo-que-paso-entre-militares-y-paramilitares-para-el-pais-coronel-r-velasquez/)</a>

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto, las comunidades han expresado en el pasado que para hacer justicia es necesario apelar a un derecho restaurador que permita un encuentro digno entre afectados y los afectadores, lo que ha motivado su deseo de verdad para la construcción de paz en sus territorios. [(Lea también: Víctimas de Rito Alejo del Río esperan toda la verdad)](https://archivo.contagioradio.com/victimas-rito-alejo-del-rio/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Víctimas exigen verdad de Rito Alejo del Río

<!-- /wp:heading -->

<!-- wp:paragraph -->

Voceros de organizaciones que acompañan a las víctimas en estos procesos, aseguran que aunque el general (r) Rito Alejo tienen otras causas por las que responder ante la Justicia, esta citación de la JEP se da puntualmente para que declare por hechos ocurridos en el marco del conflicto como homicidios, desapariciones forzadas y torturas ocurridos en 6 municipios de Antioquia y 4 del Chocó ubicados en la región de Urabá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los reclamos de las víctimas recaen principalmente en la participación de Rito Alejo en la Operación Génesis, en la que fue hallado responsable por la Justicia Ordinaria por violar su deber de garante en el homicidio de Marino López.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, para las víctimas el aporte que tendría que realizar el exmilitar en su comparecencia ante la JEP, radica en que acepte la participación del Bloque de las Autodefensas “Elmer Cardenas” en dicha operación; la responsabilidad que corresponde a las autoridades civiles que gobernaban ese territorio en esa época y también la de las empresas que llegaron a la zona luego de los desplazamientos forzados que tuvieron lugar en esa región.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
