Title: Alerta en el Bajo Cauca por el asesinato de tres mineros y el reclutamiento de menores
Date: 2019-12-10 16:29
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Bajo Cauca, ELN, grupos paramilitares, reclutamiento de jóvenes, Tarazá, UC
Slug: alerta-en-el-bajo-cauca-por-el-asesinato-de-tres-mineros-y-el-reclutamiento-de-menores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Bajo-Cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía de Tarazá] 

[En la noche del 08 de octubre, **tres personas fueron asesinadas en la vereda de San Antonio, zona rural de Tarazá**, Bajo Cauca. Fueron llevadas por la fuerza hasta una cancha deportiva, donde los asaltantes lanzaron una granada de fragmentación e hicieron varios disparos que les ocasionaron la muerte. Los nombres de las víctimas eran José Albeiro, Germán Betancourt y Remberto Lucas Flores y se dedicaban a la minería artesanal. ]

[Los asesinos, identificados como integrantes de la banda criminal Los Caparros, una disidencia de los paramilitares, generaron terror en la población y provocaron el desplazamiento de más de 150 personas desde el sector de San Antonio hacia el corregimiento de El 12, zona rural de Tarazá.][(Lea también: Aumenta control por parte de grupos ilegales en Tarazá Antioquia)](https://archivo.contagioradio.com/enfrentamientos-de-grupos-armados-en-taraza/)

[Para Oscar Yesid Zapata, vocero del Nodo Antioquia de la Coordinación Colombia Europa Estados Unidos (CCEEUU), no hay una razón clara de los asesinatos. Sin embargo, hay que mencionar que “en ese territorio convergen grupos paramilitares y grupos guerrilleros que se pelean por el control de la zona”. ]

[La estigmatización de la minería artesanal y la ausencia estatal en el territorio, han generado un ambiente de preocupación para las personas que viven de estas actividades. “Eran pequeños mineros, no ejercían ningún tipo de labor ilegal. **Creemos que la criminalización de la minería artesanal permite que sucedan este tipo de situaciones”.**]

[Por otro lado, la muerte de los tres mineros y el desplazamiento de la población rural del municipio de Tarazá, demuestra que “al ser un territorio minero, el negocio de la extracción de minerales sirve para financiar estos ejércitos de la muerte. El problema de la zona es que está consolidada por esos grupos armados y no por el Estado y cada vez se están fortaleciendo más”.]

### **El problema del control de la tierra en el Bajo Cauca** 

[Estas muertes se suman a las de tres personas —dos conductores de camiones de carga y una mujer—, asesinados en los que va corrido del año. Como menciona Zapata, “nosotros hemos advertido acerca de la situación que se viene presentando en el Bajo Cauca, los factores de riesgo que tienen que presenciar las comunidades, y el Estado no aparece”. ]

[“Lamentablemente, **en el Bajo Cauca el poder territorial lo ejercen las estructuras paraestatales y otros grupos armados** que hacen presencia en la zona” afirmó. Esto se debe a la posición estratégica de los territorios dentro de las rutas del narcotráfico y la riqueza mineral del territorio. ]

[La minería ilegal en la zona, junto al constante conflicto entre los distintos grupos armados que reclaman su control, son una situación constante para los habitantes del lugar. “Al ser un territorio minero, este negocio sirve para financiar estos ejércitos de la muerte. El problema de la zona es que está consolidada por grupos armados y no por el Estado y cada vez se están fortaleciendo más”.]

### **El reclutamiento de menores en la zona** 

[Para Zapata, una de las problemáticas más graves es el reclutamiento de niños, niñas y adolescentes por parte de los distintos grupos armados que ejercen control territorial en las zona rural de Tarazá.  Esta violencia está “escalando en diversos sectores de la población, no solo en la población adulta”. ]

[La marcada ausencia estatal en el territorio y el temor generalizado en la población civil, ha permitido “la continuidad o el ejercicio de la escuela delincuencial de todas esa estructuras en las zonas. **No hay un Estado que esté interesado en acabar de una vez y para siempre todos estos actos de violencia en el Bajo Cauca”.**]

[“Todos los grupos están tratando de fortalecerse en número, recurriendo al reclutamiento” —afirma Zapata—. El ELN, los Caparros y los grupos paramilitares son los que están reclutando. Uno se pregunta por qué reclutan ante la mirada de todas las autoridades sin que hasta el momento surja una disposición por parte del Estado por reducir este fenómeno, y por qué no hay un Estado que busque brindar todas las garantías a estas comunidades”.]

### **Derechos humanos en el Bajo Cauca** 

[Es preocupante la situación que enfrentan los habitantes del Bajo Cauca —y el departamento en general— debido a una nueva ola de violencia. Como explica el vocero del Nodo Antioquia, “lamentablemente con el gobierno Duque vamos en retroceso en materia de derechos humanos. Primero, preocupado por el tratamiento militar y de guerra hacia la protesta social. Segundo, **en Antioquia, por ejemplo, la Policía ha ejercido formas de violencia física y psicológica contra mujeres que se manifestaban para exigir sus derechos”.**]

[Esto es tangible en las cifras. En el departamento, según el Observatorio de Derechos Humanos de la Fundación Sumapaz y la Corporación Jurídica Libertad, “se han reportado 24 homicidios contra líderes sociales en Antioquia en lo que lleva del año”.][(Le puede interesar: El líder social Humberto Londoño, es asesinado en Tarazá Bajo Cauca)](https://archivo.contagioradio.com/el-lider-social-humberto-londono-es-asesinado-en-taraza-bajo-cauca/)

[Para Zapata, es indispensable llevar a cabo acciones concretas para que el Gobierno Nacional “nos vincule para construir un plan de derechos humanos. Hasta que nos den la posibilidad de una construcción de políticas públicas que beneficien a los defensores y defensoras de derechos humanos podremos lograr algo”. ]

### **El conflicto en otras zonas del departamento** 

[Al mismo tiempo que los asesinatos en la zona rural de Tarazá, **se reportó la quema de tres vehículos de transporte —incluido un bus intermunicipal— en el municipio de Valdivia**, ruta que conecta Antioquia con la Costa Caribe.]

[Según los testigos, el ataque habría sido efectuado por el ELN, quienes sacaron a los pasajeros y le prendieron fuego a los vehículos. No se reportaron heridos ni víctimas. El ataque es una respuesta a los operativos que se han llevado a cabo en la región, específicamente en el Bajo Cauca, para capturar a cabecillas de dicha guerrilla.]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
