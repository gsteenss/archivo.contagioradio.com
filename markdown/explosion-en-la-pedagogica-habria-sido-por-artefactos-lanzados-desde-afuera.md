Title: Explosión en la Pedagógica habría sido por artefactos lanzados desde afuera
Date: 2019-09-25 22:44
Author: CtgAdm
Category: DDHH, Nacional
Tags: ESMAD, universidad pedagogica, UPN
Slug: explosion-en-la-pedagogica-habria-sido-por-artefactos-lanzados-desde-afuera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/ESMAD-pedagógica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @ExhamPriory\_] 

Según varios relatos de estudiantes de la Universidad Pedagógica Nacional que presenciaron el momento de las protestas, **el artefacto que explotó al interior del campus habría sido lanzado desde afuera**. Las denuncias señalan que no se trataría de una “papa bomba” sino de elementos tipo granada, que dejaron un saldo no menor a 5 personas heridas, de las cuales ninguna estaba participando en las protestas.

Una de las estudiantes relató que el momento fue de mucho caos, mucho dolor y mucha sangre: “Siendo aproximadamente las 4:45 pm se escuchó una explosión muy fuerte al frente de uno de los edificios de la universidad (…) al parecer, dos artefactos tipo granada (…) f**ueron lanzados desde afuera de la universidad y explotaron hiriendo a estudiantes que no estaban encapuchados**. Dentro de los estudiantes hay una persona que al parecer perdió la pierna”.

En el relato que fue allegado a Contagio Radio por una de las testigos del hecho, la estudiante expresa su preocupación por las versiones que se pueden presentar en los medios de información: “Quizá en los medios de comunicación van a salir a decir que fue que a los encapuchados se les explotaron los explosivos, pero eso no es así (…) todos coinciden en que dos artefactos los lanzaron desde afuera y explotaron dentro”.

Otro testigo de los hechos, quien se encontraba en otro punto de la protesta, señaló que a la hora de la explosión ya los disturbios se estaban calmando cuando dos artefactos fueron lanzados desde afuera y que **incluso el personal del ESMAD, que estaba afuera del campus, se reía viendo a las personas heridas**. Adicionalmente señaló que la policía ingresó sin autorización al campus en lo que calificaron como un allanamiento, más que un operativo de verificación de la situación de seguridad.

### **Universidad Pedagógica y Javeriana se manifestaron contra la violencia en la protesta**

Por su parte la Universidad Pedagógica emitió un comunicado en el que señala que rechazan los actos de violencia y advierten que están tomando medidas para el control del ingreso de personas a las instalaciones. Para algunos estudiantes este comunicado es incompleto, dado que no se refirieron a la entrada de personal de la policía hacia las 7:30 de la noche y tampoco hacen alusión al uso de la fuerza por parte del ESMAD.

De igual forma, la Facultad de Ciencias Sociales de la Pontificia Universidad Javeriana, emitió un comunicado rechazando el uso de la fuerza por parte del ESMAD  contra de una protesta que era pacífica, y en contra de estudiantes de la Universidad de Cundinamarca, la Universidad Distrital, El Hospital San Ignacio y la misma Javeriana. (Le puede interesar: ["Nuevamente el ESMAD olvida que tiene que cumplir protocolos de intervención"](https://archivo.contagioradio.com/esmad-olvida-que-tiene-protocolos-intervencion/))

Noticia en desarrollo...

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
