Title: Prácticas militares habrían provocado incendio en cerros orientales de Bogotá
Date: 2016-02-02 16:36
Category: Ambiente, Nacional
Tags: Bogotá, cerros orientales, incendios forestales
Slug: practicas-militares-habrian-provocado-incendio-en-cerros-orientales-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/incendio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gert Steenssens 

<iframe src="http://www.ivoox.com/player_ek_10304927_2_1.html?data=kpWgkpmddpihhpywj5WaaZS1kZiah5yncZOhhpywj5WRaZi3jpWah5yncbHmhqigh6aVp9XdxMbgjdLNsMrowtfS1ZDMpcPmhqigh6aopc%2Bf0dfc2NTHpcXjjM7bxcrSqMrjjMrbjcjJto6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [2 Feb 2016.] 

De acuerdo con la Mesa Ambiental de los Cerros de Bogotá, el incendio en los cerros orientales de la ciudad, habría sido ocasionado por **militares del Batallón de Logística del Ejército, donde se suele realizar prácticas de tir**o, lo que implica la disposición de material explosivo en la zona de reserva de los cerros, “lo que podría llevar a pensar que el incendio  empezó por esa actividad de los militares”.

Las autoridades dudaban que el problema se hubiera iniciado por la acción de manos criminales, debido a que se trata de un lugar custodiado por la fuerza pública. **El incendio consumió el campo de entrenamiento.**

Según la información de la Mesa Ambiental de los Cerros de Bogotá y la Red Nacional del Agua, el primero de febrero en horas de la mañana, habría iniciado el incendio forestal en la parte alta en la localidad de San Cristóbal, en el barrio Aguas Claras, dentro de la zona de reserva forestal de los cerros orientales, en las inmediaciones al Batallón del Ejército, y que **se propagó por la presencia de especies no nativas como los  retamos espinoso**, una especie invasora que favorece los incendios forestales por su facilidad para arder y multiplicar el fuego.

Según los habitantes, el incendio persiste debido a que no existen vías ni capacidad de respuesta institucional que permita un buen manejo y la preservación de la reserva forestal.

Además este incendio en esa zona de la capital, ubicado en inmediaciones del río Fucha, preocupa a los habitantes pues **podría afectar el abastecimiento de agua de por lo menos cinco localidades de la ciudad,** como se ha afirmado desde la Red Nacional del Agua.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
