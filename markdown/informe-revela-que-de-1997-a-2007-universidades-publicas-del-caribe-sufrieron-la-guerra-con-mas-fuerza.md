Title: Informe revela que de 1997 a 2007 universidades públicas del caribe sufrieron la guerra con mas fuerza
Date: 2020-12-02 09:18
Author: AdminContagio
Category: Actualidad, Nacional
Tags: comision de la verdad, conflicto armado, universidad públicas del Caribe
Slug: informe-revela-que-de-1997-a-2007-universidades-publicas-del-caribe-sufrieron-la-guerra-con-mas-fuerza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/conflicto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Brujula Internacional

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes 30 de noviembre se llevó a cabo, ante la Comisión de la Verdad, la entrega del informe **'Conflicto en el campus: una generación que no aprendió a rendirse', el cual presenta** los impactos que sufrieron las universidades públicas del Caribe colombiano durante el conflicto armado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El [informe](https://www.youtube.com/watch?v=9bCWpfrzRw8&ab_channel=Comisi%C3%B3ndelaVerdad) fue realizado por los comités de investigación de las universidades de Cartagena, Atlántico, Sucre, Magdalena, Córdoba y Popular del Cesar, con el Centro Internacional para la Justicia Transicional (ICTJ).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la entrega del informe, ex actores de la guerra, excomandantes paramilitares y excombatientes de las FARC-EP reconocieron las afectaciones que causaron a víctimas del conflicto de las universidades públicas del Caribe. Así lo expresó Édgar Ignacio Fierro, excomandante paramilitar, quien compartió que **«valga la oportunidad para ofrecer todas las disculpas a todos ustedes aquí, para ofrecerles de todo corazón el arrepentimiento que es absolutamente sincero y que espero que lo reciban de corazón».**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComisionVerdadC/status/1333549637446356996","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComisionVerdadC/status/1333549637446356996

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**«**Nos equivocamos**»**, dijo Fierro, conocido como alias don Antonio, ex comandante paramilitar perteneciente del frente 'Pablo Díaz, de Barranquilla, del bloque Norte de las Autodefensas. Asimismo añadió que acudió al escenario porque considera importante la reconciliación y que se base en la verdad y la no repetición. (Le puede interesar: [Exjefes paramilitares y de las FARC-EP piden ser escuchados por la Comisión de la Verdad](https://archivo.contagioradio.com/exjefes-paramilitares-farc-piden-ser-escuchados-comision-verdad/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, Robinson Charris Palencia, representante del comité universitario del Atlántico manifestó que **«este informe de la universidad del Atlántico también evidencia una relación con entes del Estado, incluso infiltración de la misma Policía, que empezaron una política de liquidación del movimiento estudiantil e incluso, asfixiar la universidad pública, a partir de la eliminación de la protesta estudiantil»**.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Entre 1997 y 2007 se dio la mayor violencia en las universidades públicas del Caribe
------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con los informes del Centro Nacional de Memoria Histórica y del Centro Internacional para la Justicia Transicional, entre 1997 y 2007 se dio la mayor violencia en las universidades públicas del Caribe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tatiana Galera, representante del comité universitario de Córdoba, expuso que **«**las personas que fueron asesinadas entre 1995 y el 2005 estaban principalmente vinculados a procesos sociales con comunidades indígenas y defensa de los derechos. Estos hallazgos importantes resaltarlos por el patrón claro entre estas muertes**»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, durante este período la presencia de grupos como el ELN, las FARC y EPL se introdujo en las universidades, fortaleciendo la estigmatización hacia el movimiento estudiantil y las organizaciones sindicales, quienes tuvieron que enfrentarse a que el espacio universitario se convirtiera en un escenario de riesgo. (Le puede interesar: [La Franquicia de las Águilas Negras](https://archivo.contagioradio.com/la-franquicia-de-las-aguilas-negras/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, esta estigmatización construyó un imaginario negativo contra los estudiantes y los colectivo estudiantiles de las universidades que se movilizaban en pro de los derechos de la misma.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En Córdoba y Sucre, las Autodefensas Campesinas de Córdoba y Urabá, (ACCU), y las Autodefensas Unidas de Colombia (AUC)​ «difundieron un discurso antisubversivo que desencadenó una oleada de persecución contra sindicatos de profesores y empleados universitarios» , indica el informe entregado a la Comisión. Muchos de estos hechos terminaron con la vida de muchos miembros de las universidades públicas, tal fue el caso de la Universidad del Atlántico, donde fueron asesinados cerca de 20 estudiantes y trabajadores.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Organizaciones señalan no haber sido invitadas
----------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

A pesar de considerarse un espacio necesario para lograr la paz definitiva en el país, varias organizaciones advirtieron que no fueron convocadas a la entrega del informe, considerando que no se tuvieron en cuenta a las víctimas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante esto, Stalin Ballesteros, coordinador de la Comisión de la Verdad en el Magdalena, explicó que los informes los entregan comités de diferentes universidades, conformados por académicos, víctimas, entre otros actores, con el respaldo del Ictj. «Nosotros como comité estamos en la obligación de escuchar a las víctimas, sin embargo, es necesario entender, que esto es un proceso, que requiere una serie de casos para proteger la dignidad de las víctimas». (Le puede interesar: [Leyner Palacios representará el legado de las víctimas ante la Comisión de la Verdad](https://archivo.contagioradio.com/leyner-palacios-representara-el-legado-de-las-victimas-ante-la-comision-de-la-verdad/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
