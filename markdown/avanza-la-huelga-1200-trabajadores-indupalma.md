Title: Avanza la huelga de 1200 trabajadores de Indupalma
Date: 2018-01-31 15:52
Category: Movilización, Nacional
Tags: Indupalma, Palma, Trabajadores
Slug: avanza-la-huelga-1200-trabajadores-indupalma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/Cultivo-palma-1-face.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Agencia de Información Laboral] 

###### [31 Ene 2018]

Continúa la huelga los más de 1.200 trabajadores de Indupalma que denuncian la tercerización laboral y descuentos injustificados en sus salarios. El día de ayer se estableció una mesa de conversaciones entre los directivos de la empresa y los sindicatos que abordo el primer punto de las exigencias de los **trabajadores sobre el mejoramiento del modelo de contratación**.

De acuerdo con Andrey Piñeres, vocero de la huelga y trabajador en la empresa, los directivos de Indupalma aceptaron que existe corrupción dentro del modelo de contratación de las cooperativas, “la comisión negociadora manifestó directamente que fortalecer el modelo no era la solución y fuimos muy claros y enfáticos en decir que la única posibilidad **de llegar a levantar esta acción política sería por medio de un acuerdo de formalización**”.

En este sentido los directivos de la empresa aceptaron la necesidad de hablar de formalización de la contratación pero que por la cantidad de personal este tema no sería tan fácil de abordar. Sin embargo, afirmaron que se pensara en ese proceso de transición**. El día de mañana se continuará con los avances en esta mesa**.

Los trabajadores anunciaron que la huelga no se levantará hasta que existan las garantías de la transición en los modelos de contratación y señalaron que el paso que dió la empresa es un paso grande. En este momento los trabajadores continúan en los 5 puntos de entrada de Indupalma. (Le puede interesar:["Trabajadores denuncian abuso laboral por parte de la empresa Indupalma"](https://archivo.contagioradio.com/trabajadores-de-indupalma-tienen-que-pagar-hasta-su-dotacion/))

<iframe id="audio_23481853" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23481853_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
