Title: Hasta el 7 de Julio se recogerán firmas para disminuir salario de Congresistas
Date: 2017-02-03 14:46
Category: Movilización, Nacional
Tags: iniciativa ciudadana, Referendo disminuir salario congresistas
Slug: hasta-el-7-de-julio-se-recogeran-firmas-para-disminuir-salario-de-congresistas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La razón] 

###### [3 Feb 2017] 

Desde el 29 de diciembre de 2016, la Registraduría Nacional dio vía libre para que se conforme el comité que impulsa el referendo que busca **disminuir el salario de congresistas**. Se deben recolectar un **millón 900 mil firmas en un plazo de 6 meses, hasta el 7 de julio,** en todo el territorio colombiano y sí la Registraduría a avala las firmas, el proyecto pasará a ser debatido en Congreso y luego en la Corte Constitucional.

De acuerdo con Edwin Pabón, vocero de “Contra la Rosca”, una de las organizaciones que recolecta firmas para esta iniciativa se hizo un estudio sobre cuanto ganaban los congresistas comparado al tiempo que trabajaban “ellos tienen un cargo público y nosotros como constituyente primario debemos presionar para que cumplan, **cómo es posible que no asistan a sesiones, que se levanten de sus banquillos cuando debatieron la Reforma Tributaría”**

Sí el proyecto pasa el proceso en la Corte Constitucional, se convocará  a  la ciudadanía a las urnas para que voten el referendo, que será aprobado con **la participación ciudadana es de al menos la cuarta parte que compone el censo electoral y sí la opción del SI obtiene la mitad más uno de los votantes.**

Frente a la recepción que ha tenido esta recolección de firmas entre la ciudadanía Pabón afirmó que ha sido muy bien acogida “**la gente se acerca y creen que es algo bueno, la gente cree que muchos jóvenes podemos cambiar la politiquería”**.

La recolección de firmas en este momento se lleva a cabo en diferentes ciudades del país como **Bogotá, Medellín, Cali, Bucaramanga, entre otras,** que tendrán hasta el mes de julio para entregar el compilado de firmas e iniciar con el proceso de revisión. Le puede interesar: ["Comienza recolección de firmas para disminuir salario de Congresistas"](https://archivo.contagioradio.com/comienza-recoleccion-firmas-reducir-salario-congresistas/)

###### Reciba toda la información de Contagio Radio en [[su correo]
