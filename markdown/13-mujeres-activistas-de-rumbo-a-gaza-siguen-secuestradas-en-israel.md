Title: 13 mujeres activistas de Rumbo a Gaza siguen secuestradas en Israel
Date: 2016-10-06 16:23
Category: El mundo, Mujer
Tags: Gaza, Mujeres Rumbo a Gaza, Palestina
Slug: 13-mujeres-activistas-de-rumbo-a-gaza-siguen-secuestradas-en-israel
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/gaza-e1475788085553.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rumbo a Gaza] 

###### [6 Oct 2016 ]

El pasado miércoles fue asaltada por la Armada de Israel, en aguas internacionales, la flotilla en la que 1[3 mujeres se dirigían a Gaza en protesta contra el bloqueo impuesto por el gobierno Israelí a Palestina.](https://archivo.contagioradio.com/mujeres-rumbo-a-gaza-asaltadas-por-la-armada-de-israel/) **Las activistas fueron llevadas bajo secuestro a zona militar y hasta el momento no se ha resuelto la situación de estas integrantes de la iniciativa Mujeres Rumbo a Gaza.**

María del Río integrante y encargada de comunicaciones de Rumbo a Gaza, informó que el cónsul español y algunas delegaciones de los países de estas mujeres intentaron, durante toda la noche de ayer, comunicarse con ellas, sin embargo no fue posible hasta hoy, a la una de la tarde, hora de España, hablar con algunas de las delegadas.

Se pidió una reunión con el Ministerio de asuntos exteriores para **exigir “como ciudadanas protección y defensa a nuestros gobiernos cuando suceden estos casos de detención fuera de nuestros territorios”**, hasta el momento el consulado español se ha comprometido a realizar asistencia y movilización para la liberación de las activistas sin obtener resultado hasta ahora, afirmó Ríos.

Varias organizaciones sociales y sindicatos se han manifestado en apoyo a las exigencias para la liberación de estas 13 activistas. **En Barcelona y Madrid han realizado plantones y Ríos finaliza comentando que “ha sido más fuerte el apoyo político de estas organizaciones que del mismo Gobierno”.**

Informó que sólo el Zaytouna logró zarpar, pues el otro velero de la flotilla presentó fallas técnicas y no pudo lograr su objetivo. [En costa de Gaza estaban esperando al Zaytouna decenas de personas,](https://archivo.contagioradio.com/ocupacion-palestina-solo-puede-terminar-si-a-nivel-internacional-sumamos-esfuerzos/)se habían instalado carpas para atención médica y transmisión de medios. Denunció que durante esta instalación, l**a Armada de Israel envió drones y bombas sónicas para aturdir a los manifestantes.**

En las últimas horas Carmen Fernández de la unidad de crisis del Ministerio de Asuntos Exteriores y Cooperación, informó a representantes de Rumbo a Gaza que **Sandra Barrilaro una de las retenidas de nacionalidad española, será deportada el próximo viernes en horas de la mañana.**

<iframe id="audio_13210331" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13210331_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
