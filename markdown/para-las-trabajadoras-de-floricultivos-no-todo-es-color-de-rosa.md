Title: Para las trabajadoras de los floricultivos, no todo es color de rosa
Date: 2017-02-15 12:40
Category: Mujer, Nacional
Slug: para-las-trabajadoras-de-floricultivos-no-todo-es-color-de-rosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/trabajadoras_floricultivos-e1487178392677.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IPS] 

###### [14 Feb 2017 ] 

Son muchas las denuncias de los y las trabajadoras de los floricultivos, a diario viven condiciones indignas de trabajo como **jornadas laborales de 16 horas, madres cabeza de hogar que no pueden ver a sus hijos, salarios mínimos sin prestaciones ni reconocimiento de horas extras, enfermedades asociadas al uso de plaguicidas y la tercerización laboral.**

Organizaciones sociales, sindicatos de la Sabana y  la Corporación Cactus, evidenciaron sus preocupaciones por **“las malas condiciones de trabajo y el poco reconocimiento”** que se da a la labor de cientos de mujeres y hombres “que día a día hacen de las flores un negocio exitoso en el comercio internacional”.

Mercedes Bojacá, quien hace parte de la Red Popular de Mujeres de la Sabana y lidera la organización Manos Unidas Madrid, señala que aunque la floricultura ha generado empleo durante 50 años de presencia en Colombia, el “aumento progresivo de los rendimientos de producción exigidos”, ha agravado la salud de los y las trabajadoras. Cuenta que hace 40 años, **la jornada más larga era de 6:30am a 4:00pm y ahora "los turnos son de 5:00pm a 9:00am, con media hora de almuerzo y salarios más bajos".**

### Largas jornadas, salarios bajos y enfermedades 

La líder, asegura que tiempo atrás a cada trabajadora se le asignaban 13 camas de flores, cada una de 4m2, y ahora "le asignan a cada empleada 80 camas y más grandes, eso es demasiado para una sola persona". Muchas de las trabajadoras son madres cabeza de hogar, las extensas jornadas de más de 16 horas y las riesgosas actividades que llevan a cabo, han hecho que **"no puedan dedicarle tiempo a sus hijos y tienen ahora muchas enfermedades respiratorias, de la espalda y la piel"**, puntualiza Bojacá.

De acuerdo con Bojacá, la exposición constante a los plaguicidas hace que las trabajadoras "todo el tiempo tengan brotes en la piel, mareos, vómitos, diarrea e infecciones pulmonares" y frente a eso las directivas no hacen nada, "es muy difícil que una trabajadora pueda pedir permiso para ir al médico, las hacen salir del invernadero a tomar aire y que luego vuelvan a entrar".

Manifiesta que esta situación ha generado mayor desgaste físico y emocional, que cada vez las trabajadoras deben exponerse a actividades de alto riesgo, “asociadas a la exposición a plaguicidas" y las EPS contratadas por las temporales **"parecen estar aliadas con las empresas porque siempre dicen que no tenemos nada, que estamos bien".**

### Otro sector afectado por la tercerización laboral 

Por otra parte, llaman la atención sobre la tercerización laboral y “la histórica represión al derecho de asociación sindical”, mediante despidos constantes, explica Bojacá que las temporales **"cada 15 días contratan y despiden gente y hacen que nuevos empleados trabajen 3 días sin paga y luego les dicen que no son aptos para el cargo"**, resalta también que los dueños de cultivos usan medidas represivas contra quienes “desean organizarse para defender sus derechos laborales, esos que nos arrebataron con la tercerización".

En la misiva, mencionan que ni el sector floricultor ni otros sectores “considerados prioritarios”, se han visto avances o voluntad política para cumplir los compromisos del **Plan de Acción Laboral, firmado entre los gobiernos de Colombia y EE.UU, el cual cumplió 5 años en 2016** y prometía mayor protección a los derechos laborales, fortalecimiento de vigilancia a las empresas, “reduciendo la informalidad y la tercerización laboral y ampliando la protección de la libertad sindical”. ([Le puede interesar: 65.000 madres comunitarias irían a paro indefinido](https://archivo.contagioradio.com/65-000-mil-madres-comunitarias-irian-a-paro-indefinido/))

Por último, exigen al Estado colombiano “una voluntad política seria para el cumplimiento de sus obligaciones de protección a los derechos laborales”, a las empresas floricultoras “respetar los derechos de los y las trabajadoras y en particular el respeto al derecho de asociación” y **saludan a las trabajadoras y trabajadores de las flores en su día.**

###### Reciba toda la información de Contagio Radio en [[su correo]
