Title: Con ausencia del PLRA se establece mesa de diálogo en Paraguay
Date: 2017-04-05 13:16
Category: El mundo, Otra Mirada
Tags: Cartes, Mesa de diálogo, paraguay, Reelección
Slug: paraguay-reeleccion-mesa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Manifestantes-Rodrigo-Quintana-Policia-Paraguay_LNCIMA20170402_0171_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:AP 

###### 05 Abr 2017 

Luego del convulsionado fin de semana que vivió Paraguay por cuenta de la aprobación en el Senado de la reforma constitucional que permitiría la reelección presidencial, **hoy se establece la mesa de diálogo**, espacio en el que no participará el Partido Liberal Radical Auténtico (PLRA).

A pesar del llamado hecho por el presidente Horacio Cartes a diferentes partidos políticos con representación parlamentaria y representantes de la iglesia, varios dirigentes del PLRA han declarado desde el inicio de las manifestaciones que **no se sentarán en ninguna mesa mientras no sea retirado el proyecto de enmienda de la carta constitucional**.

La determinación de los integrantes de esa colectividad, está argumentada en que l**a modificación es "irremediablemente inconstitucional"**, razón por la cual anunciaron que **no tendrán representación** en la reunión con el Presidente, la Conferencia Episcopal Paraguaya y los integrantes de los partidos políticos que deseen participar de la iniciativa.

En una carta enviada este miércoles al Palacio de los López, el presidente de la colectividad Efraín Alegre, hizo un llamado además para que se **esclarezcan las circunstancias en que fue asesinado Rodrigo Quintana**, líder juvenil de la organización, mientras se encontraba en la sede del partido, distante en más de un kilómetro del epicentro de las manifestaciones.

En la misiva dirigida al Presidente, Alegre asegura que contrario a asumir responsabilidades por lo ocurrido "**desde su sector político pretenden transferir responsabilidades, acusando a líderes opositores y a periodistas** de ser instigadores de la genuina indignación ciudadana, que desembocó en la toma del Congreso, abandonado por las fuerzas de seguridad".

Si el proyecto de enmienda sigue su curso, como espera que ocurra Cartes, **deberá pasar a debate en la Cámara de diputados**, presidida por el Partido Colorado al que pertenece el mandatario, audiencias que no tendrían lugar mientras se mantengan las conversaciones en la mesa propuesta. Le puede interesar: [Se mantiene la presión contra reelección en Paraguay](https://archivo.contagioradio.com/paraguay/).
