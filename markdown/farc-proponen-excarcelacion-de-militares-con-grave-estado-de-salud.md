Title: FARC proponen excarcelación de militares en grave estado de salud
Date: 2016-01-18 17:13
Category: Nacional, Paz
Tags: FARC, presos politicos, proceso de paz
Slug: farc-proponen-excarcelacion-de-militares-con-grave-estado-de-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/FARC-e1453154641281.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.elnuevodiario.com.ni]

<iframe src="http://www.ivoox.com/player_ek_10116674_2_1.html?data=kpWek5uae5Whhpywj5WbaZS1kpqah5yncZOhhpywj5WRaZi3jpWah5yncae1s6iY0tfTtNDixtOYx93HpdPXxtHOxc6Jh5SZo5jbjcnJb87dzc7hw9fJt4zZz5DU1MbaqYzZ1NnOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carlos Lozano, Semanario Voz] 

###### [18 Enero 2016] 

Este domingo, la delegación de paz de las FARC le propuso al gobierno nacional, la excarcelación de militares que se encuentren en prisión en grave estado de salud, como un gesto humanitario **acorde al indulto de 30 guerrilleros, de los cuales 16, esta semana saldrán en libertad, según anunció el Ministro de Justicia, Yesid Reyes.**

“Ponemos a consideración del Gobierno una nueva medida de construcción de confianza, con un claro contenido humanitario, consistente en que se proceda también a excarcelar a integrantes de la Fuerza Pública”, anunció públicamente Iván Márquez, el jefe de la delegación de paz de las FARC.

Las FARC-EP hablaron de la excarcelación de miembros de la Fuerza Pública, pero también de **dirigentes sociales y guerrilleros que se encuentren con delicado estado de salud.**

Para el director de semanario Voz, Carlos Lozano, “**es una propuesta interesante, que puede ayudar a generar confianza y tender puentes en la reconciliación de la sociedad”**, y añade que “si se liberan unos insurgentes en condiciones precarias de salud, con la misma consideración se debe sacar a otros de sectores del conflicto, como los militares, que han contribuido con el proceso de paz”.

A su vez, Lozano, aseguró que debe haber mayor voluntad política y de paz, de parte del gobierno nacional, para que iniciativas como la del indulto a 30 guerrilleros no se dilaten tanto, y además, también **debe haber una disposición de parte de la delegación de paz del gobierno para atender las propuestas de las FARC.**

Por su parte, el General en retiro Juan Salcedo Lora, señala que si se trata de una medida que  cobija a los militares, es un gesto humanitario que probablemente reciba el gobierno.

**Nombres de los primeros 17 integrantes de las FARC-EP que saldrán de los centros carcelarios son:**

1.  Maritza Yurani Rueda
2.  Yenny Paola Quitian
3.  Marleny Ropero García
4.  Bibiana Alexandra Romero
5.  Leydy Johana Granados
6.  Sandra Patricia Isaza
7.  Gloria Álvarez
8.  Jeison Orlando Moreno
9.  Jhon Kennedy Ospina
10. Alexander Ramírez
11. Reynaldo Bustos
12. Gabriel Valderrama
13. Aristides Luna
14. Elky Javier Caballero
15. Wilson Antonio López
16. Carlos Antonio Ochoa
17. Mauricio Suárez

   
Reciba toda la información de Contagio Radio en su correo.  [[http://]{.invisible}[bit.ly/1nvAO4u]{.js-display-url}[[ ]{.invisible}]{.tco-ellipsis} ](http://bit.ly/1nvAO4u)
