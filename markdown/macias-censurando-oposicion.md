Title: Ernesto Macías está censurando a la oposición: Jorge Robledo
Date: 2018-10-03 13:12
Author: AdminContagio
Category: Entrevistas, Política
Tags: Alberto Carrasquilla, Bonos del Agua, Ernesto Macías, Jorge Robledo, Moción de Censura
Slug: macias-censurando-oposicion
Status: published

###### [Foto: @SenadoGovCo] 

###### [3 Octo 2018] 

El **presidente del Senado, Ernesto Macías** estaría evitando que se realice el debate en el que se decidiría si Carrasquilla debe o no abandonar su cargo, por su participación en los llamados 'Bonos del Agua'; hecho con el que estaría afectando a la oposición que, tras el debate de control político, ha buscado la **moción de censura del Ministro de Hacienda** y Crédito Público.

Para el senador **Jorge Enrique Robledo**, la actitud que ha asumido Macías es despótica al evitar el debate que podría terminar con la salida del Ministro de Hacienda; y señaló que también es una actuación que censura a la oposición y que es violatoria de la Ley. (Le puede interesar: ["El que la hace la paga no aplica para el ministro Carrasquilla"](https://archivo.contagioradio.com/paga-ministro-carrasquilla/))

Robledo explicó que **el Presidente del Senado no puede tomar la decisión acerca de si se lleva a cabo, o no, el debate sobre la moción de censura**, porque el órgano encargado de tomar esa determinación es la mesa directiva de esa corporación, que actualmente conforma la senadora Angélica López y, que no fue consultada por Macias.

Adicionalmente, porque **las acciones de Carrasquilla siguen teniendo repercusión en el presente:** actualmente "hay procesos en curso en la Procuraduría, la Fiscalía y la Contraloría por los 'Bonos del Agua'". El senador de la bancada de oposición recordó que, **"fue tal el robo que en 2012 el Ministerio de Vivienda puso un denuncio para que se investigue el tema"** de los Bonos del Agua o Bonos Carrasquilla.

### **El autoritarismo está mostrando las uñas: Robledo** 

Robledo dijo que lo mínimo que esperarían es que se pudiera citar al debate para que, de esta forma, todos los senadores expresen su punto de vista, **"incluso que defiendan la corrupción si quieren"**, pero lo que se está viendo es una decisión del 'Duquismo' coartando la libertad de expresión del Congreso.

El senador por el Polo, concluyó que no se puede esperar nada de este proceso, pero vale la pena intentar lo necesario para garantizar el debate; sin embargo, Robledo también indicó que **serían todos los colombianos, quienes deberían reclamar en la calle y redes sociales que el Ministro Carrasquilla responda a los cuestionamientos en su contra**. (Le puede interesar: ["La reforma tributaria de Carrasquilla afectaría al 90% de los colombianos"](https://archivo.contagioradio.com/carrasquilla-afectara-colombianos/))

Estos son los resultados de nuestra encuesta del día:

> ¿Desde el Gobierno y una parte del Congreso están protegiendo a Alberto Carrasquilla de la moción de censura en su contra ...?
>
> — Contagio Radio (@Contagioradio1) [3 de octubre de 2018](https://twitter.com/Contagioradio1/status/1047509450079330305?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
  

<iframe id="audio_29075744" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29075744_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
