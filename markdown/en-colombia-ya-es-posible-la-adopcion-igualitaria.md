Title: En Colombia ya es posible la adopción igualitaria
Date: 2015-11-04 19:04
Category: LGBTI, Nacional
Tags: Adopción igualitaria, Colombia Diversa, Corte Constitucional, LGBTI
Slug: en-colombia-ya-es-posible-la-adopcion-igualitaria
Status: published

###### Foto: colombiagay 

###### [4 Nov 2015]

Con 6 votos a favor y 2 en contra la sala plena de la Corte Constitucional votó a favor de la adopción igualitaria en Colombia, luego de la ponencia presentada por el magistrado Iván Palacios. Según la decisión la posibilidad de la [adopción por parte de parejas diversas](https://archivo.contagioradio.com/?s=adopcion) no afecta a los menores que sean acogidos por parejas del mismo sexo.

Esta decisión se considera como un fallo histórico en favor de los derechos de la comunidad LGBTI en Colombia que desde hace más de 2 años venía buscando a través de diversas acciones políticas y legales por el derecho a adoptar pero también por el derecho de los niños y niñas a tener un hogar.

**La Corte Constitucional resolvió la demanda interpuesta por el abogado Sergio Estrada,**  por la cual  se buscaba que las parejas del mismo sexo pudieran adoptar. Esta vez el argumento no se basó en los derechos de la comunidad LGBTI, sino en los derechos de la niñez a tener un hogar.

El profesor universitario de Medellín, radicó la demanda en contra de los artículos **64, 66 y 68 de la Ley 1098 de 2006,**conocida como la Ley de Infancia y Adolescencia, debido a que el debate esta vez  gira en torno a que los niños y niñas tienen el derecho fundamental a una familia, de manera que se tiene en cuenta la** ley 12 de 1991**, donde se incorpora **la Convención de Derechos del Niño**, allí se argumenta que en la adopción no se debe considerar el sexo de los padres.
