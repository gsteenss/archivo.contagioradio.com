Title: Aguas!  De sequía y acaparamiento
Date: 2015-10-15 16:15
Category: CENSAT, Opinion
Tags: CENSAT, crisis del agua colombia, El Quimbo, Fenómeno del Niño, IDEAM, Ministerio de Vivienda, Regulación de Agua potable
Slug: crisis-de-agua-colombia-sequia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/MG_7928.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Verena Glass] 

#### **[[Censat Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [@CensatAguaViva](https://twitter.com/CensatAguaViva)]** 

###### 15 Oct 2015 

Colombia presenta un preocupante escenario de sequía sin precedentes en las últimas décadas. El Ministerio de Vivienda asegura que 312 municipios en el país tienen riesgo de desabastecimiento de agua, mientras el IDEAM además de declarar alerta roja por el considerable descenso de los niveles del río Magdalena, resalta que las consecuencias del Fenómeno del Niño se extenderán hasta el 2016. En el mismo sentido, otras autoridades ambientales han informado el crítico estado de las fuentes hídricas, algunas incluso tienen riesgo de desaparecer. A este escenario, se suman los más de 3.700 incendios forestales que han arrasado con alrededor de 94 mil hectáreas de bosque en lo que va del 2015.

[Ante esta grave crisis ambiental tanto el gobierno como los medios masivos de comunicación señalan como causantes de la sequía el Fenómeno del Niño, la deforestación, el despilfarro de agua y el cambio climático. En particular, la Comisión de Regulación de Agua potable (CRA) pone en vigencia, en marzo de 2015, la Resolución 710 que castiga económicamente el consumo de agua por encima de ciertos niveles establecidos. Mientras que el gobierno se jacta de haber multado a más de un millón de personas debido al despilfarro de agua; las soluciones, además de paliativas, se han decantado en la ciudadanía con responsabilidades económicas y sociales . De esta manera, se insta al país a un ahorro doméstico de agua y energía mientras sistemáticamente se oculta un proceso de acaparamiento de aguas por parte de las actividades extractivas minero-energético y agroindustrial.]

Ahora bien, según el Estudio Nacional del Agua, presentado recientemente por el IDEAM, la demanda hídrica en Colombia para el sector domestico tan sólo se ubica en un cuarto lugar con un 8.2%, mientras que el sector agrícola con un 46.6% ocupa el primer lugar y el  hidroeléctrico con un 21.5% el segundo lugar en el uso de agua. Es también de resaltar que según el mismo informe, la huella hídrica azul del sector agrícola, es decir la apropiación humana de ríos, lagos y acuíferos que no retorna a sus fuentes, esté concentrada en casi la mitad con cultivos de caña y palma de aceite en el país. Precisamente, en zonas muy golpeadas por la sequía como el Valle del Cauca, las concesiones superficiales y subterráneas de agua están concentradas en el sector cañero con un 64.1% y 87.8% respectivamente.

Por otro lado, se objetará que las hidroeléctricas no representan ningún inconveniente en la disponibilidad de agua, ya que supuestamente no afectan los caudales, dado que la totalidad de las aguas represadas vuelve al afluente. Sin embargo, las hidroeléctricas provocan una gran conflictividad, entre otras cosas por el acaparamiento y concentración del agua, las represas no sólo destruyen el ciclo natural del río y  la pesca, sino que también afectan gravemente la calidad de las aguas por interrumpir el flujo natural del río generando turbidez, concentración de sedimentos y nutrientes, pérdida de oxígeno disuelto, bioacumulación de mercurio y eutrofización, todo ello hace que estas aguas dejen de estar disponibles para el consumo humano y las actividades agrícolas campesinas,. De hecho, el llenado y la puesta en marcha de dos grandes y nuevas represas:  Hidrosogamoso y El Quimbo, ponen en evidencia esta situación. Precisamente, un estudio de la Universidad Santo Tomas de Bucaramanga, liderado por el prestigioso investigador Jaime Puentes, asegura que la calidad del agua en el Río Sogamoso aguas abajo de la represa, no es apta ni para consumo doméstico ni para cultivos.

En este mismo sentido, habría que tener en cuenta que el IDEAM ha advertido que cada año por causa de la extracción minera se vierten a las aguas y suelos alrededor de 205 toneladas de mercurio, siendo necesario reflexionar con profundidad un modelo de desarrollo avante en el país que profundiza la contaminación de los territorios esenciales para el ciclo hídrico.

[Es un contrasentido que las instituciones construyan campañas para que el colombiano de a pie ahorre agua, mientras estas mismas continúan entregando concesiones de aguas a enormes proyectos extractivos, mineros, hidroeléctricos y agroindustriales cuando son estas actividades quienes más demandan y degradan el líquido vital.]

La actual crisis del agua es una oportunidad para pensar en nuevas relaciones con nuestras aguas y para observar con claridad las inequitativas formas de gestión del agua en el país. Como sociedad somos responsables del cuidado de nuestros territorios y consumos, perola responsabilidad de la crisis no puede seguir recayendo exclusivamente en el pueblo, es necesario abordar las reales causas y el devenir histórico de estas crisis.

**¡Aguas para la Vida!**
