Title: ¿Cómo se debe votar la consulta anticorrupción?
Date: 2018-06-06 13:39
Category: Nacional, Política
Tags: Congreso de la República, Consulta anticorrupción, corrupción
Slug: como-se-debe-votar-la-consulta-anticorrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/consulta_anticorrupcion_archivo_el_espectador.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [06 Jun 2018] 

Luego de haber sido aprobada por unanimidad la consulta anticorrupción en el Congreso de la República, el presidente Juan Manuel Santos tendrá una semana para convocar la consulta, que tiene como máximo un plazo de tres meses para realizarse. Razón por cual se descarta la posibilidad de que **esta votación se haga durante los comicios a segunda vuelta para elecciones presidenciales.**

<iframe src="https://co.ivoox.com/es/player_ek_26394357_2_1.html?data=k5ugm5mXeZihhpywj5aXaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncaLiyIqwlYqlfc3dxMaYrtTepc_jhpewjdjTptPZjNHOjajTstTpzdnOjcbSuMrX0Nff19XHrYa3lIqvldOPcYarpJKw0dPYpcjd0JC_w8nNs4yhhpywj5k.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para la representante a la Cámara por el Partido Verde, Angélica Lozano la aprobación por mayoría de los congresistas, significa que escucharon a una ciudadanía que está cansada de los múltiples actos de corrupción que cometen.

Muestra de ello también lo sería el resultado de la primera vuelta electoral, en la que, de acuerdo con Lozano, **las maquinarias fueron derrotadas, “los alternativos, sumados Petro y Fajardo sumamos ya medio país”** afirmó y agregó que incluso ambos candidatos a la presidencia han manifestado su apoyo a esta iniciativa.

### **Cómo se votará la consulta anticorrupción** 

La consulta anticorrupción consta de 7 puntos, que se deben votar separadamente para que el voto sea válido. Es decir que para cada uno de ellos se formuló una pregunta y la ciudadanía votará si o no.

Los siete temas que conforman esta consulta anticorrupción son: primero reducir el salario a los congresistas y altos funcionarios; que se castigue con cárcel a las personas que cometan actos de corrupción y que se les prohíba volver a contratar con el Estado; que los procesos de contratación transparente sean obligatorios en todo el país; la creación de presupuestos públicos con participación ciudadana; que los congresistas deban rendir cuentas sobre su asistencia, votación y gestión; hacer públicas las propiedades e ingresos injustificados de políticos y extinguirles dominio y solo se podrán tener máximo 3 periodos en corporaciones públicas.

Para Lozano estos 7 puntos le devuelven poder de control y veeduría al ciudadano y le recuerdan “a la clase política que se deben rendir cuentas”. (Le puede interesar: ["Aprobada la consulta anticorrupción con 84 votos a favor y cero en contra"](https://archivo.contagioradio.com/consulta-anticorrupcion/))

El representante a la Cámara Alirio Uribe, también señaló que este tipo de escenarios permite que socialmente exista un rechazo a las conductas que van en contra de la democracia como lo son los actos de corrupción, **"aquí la gentre cuando llega a la Alcaldía, a la gobernación, al Congreso, es como si llegaran** a un botín y tuviesen derecho a saquearlo, cuando realmente lo más sagrado que hay en una sociedad son los bienes públicos, para defenderlos y cuidarlos".

<iframe src="https://co.ivoox.com/es/player_ek_26394433_2_1.html?data=k5ugm5mYd5Shhpywj5WdaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
