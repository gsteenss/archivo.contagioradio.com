Title: Muere tercer niño indígena  por confinamiento forzado producto de enfrentamientos
Date: 2020-04-10 19:56
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #SanJuan, #Wounnan, confinamiento, ELN, forzado, niño, paramilitares
Slug: muere-tercer-nino-indigena-por-confinamiento-forzado-producto-de-enfrentamientos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/agua.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

**Falleció Abraham Chiripúa Chamarra**, de dos años de edad, el pasado 10 de abril en la comunidad de **Unión Agua Clara, bajo San Juan, Buenaventura.** Él es el tercer niño indígena que muere por confinamiento forzado producto de enfrentamientos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Abraham pertenecía a la comunidad Nomam, y de acuerdo con la denuncia hecha por la Comisión de Justicia y Paz, **presentaba signos de desnutrición y problemas gastrointestinales**. (Le puede interesar:["Muere bebé indígena Nonam por confinamiento")](https://www.justiciaypazcolombia.com/muere-bebe-indigena-nomam-por-confinamiento/)

<!-- /wp:paragraph -->

<!-- wp:heading -->

El Confinamiento forzado producto de enfrentamientos
----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde el 18 de marzo las comunidades negras e indígenas del litoral Bajo San Juan, vienen denunciando ante el gobierno nacional que se encuentra en confinamiento forzado producto de los enfrentamientos que hay entre la guerrilla del ELN y **grupos criminales herederos del paramilitarismo**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta situación ha impedido que la comunidad logre salir a pescar o recoger la siembra del pan coger para alimentar a sus familias. Asimismo los médicos ancestrales tampoco han podido realizar su labor, debido a que no pueden recoger las hierbas medicinales que necesitan.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Muere tercer niño indígena por confinamiento forzado
----------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 28 de marzo, falleció  **Melbis Chamapuro** de 8 meses. El niño  w**ounaan** d**e l**a comunidad de **Nuevo Pitalito,** en la ribera del río** San Juan**, fue la primera muerte que se produjo en el marco de este confinamiento forzado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente, el martes 7 de abril murió, en la comunidad **Santa Rosa de Guayacán, el niño Dinalder Piraza Moña, de 7 meses**. Hecho al que se suma la reciente muerte de Abraham.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta comunidad indígena junto con 109 más **solicitaron al presidente un Acuerdo Humanitario Global sobre COVID-19 que a la fecha no ha tenido respuesta.** Sin embargo, el pasado 9 de abril, reiteraron su llamado al presidente para que garantiza la vida de los territorios víctimas del conflicto armados y del olvido estatal. (Le puede interesar: " [Alimentación, salud, agua y acuerdo humanitario, exigen 110 comunidades a Duque](https://archivo.contagioradio.com/alimentacion-salud-agua-y-acuerdo-humanitario-exigen-110-comunidades-a-duque/)")

<!-- /wp:paragraph -->
