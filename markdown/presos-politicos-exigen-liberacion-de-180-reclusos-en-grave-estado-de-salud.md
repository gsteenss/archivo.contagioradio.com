Title: Presos políticos exigen liberación de 180 reclusos en grave estado de salud
Date: 2015-10-16 13:33
Category: DDHH, Nacional
Tags: 180 reclusos en grave estado de salud, coalicion larga vida a las mariposas, Diálogos de La Habana, Extradición en Colombia, Fundación Comite de Solidaridad con Presos Políticos, Huelga de hambre en La Picota, hulega de hambre, INPEC, Prisioneros políticos en Colombia
Slug: presos-politicos-exigen-liberacion-de-180-reclusos-en-grave-estado-de-salud
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Presos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Aguijón ] 

##### <iframe src="http://www.ivoox.com/player_ek_9025423_2_1.html?data=mpWfl5mWd46ZmKiak5WJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlNPZ1NTgjdXTsIa3lIquptnNp9DnjMrly8zJsozgysfS1MbHrYa3lIqvldOPqMafkp2djdfJp83p1NTgjcrScYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[ René Nariño, prisionero de guerra] 

###### [16 Oct 2015] 

[Cerca de **1500 presos políticos** completan **4 días de huelga de hambre** en **6 establecimientos carcelarios del país**, como medio de presión para obtener respuestas por parte del Gobierno ante sus **exigencias frente a la necesidad de un servicio de salud digno **en las cárceles, repatriación de prisioneros de guerra extraditados y reforma estructural del Instituto Nacional Penitenciario y Carcelario, INPEC.]

[René Nariño, prisionero de guerra en la cárcel ERON – Picota de Bogotá, asegura que sus exigencias son producto de la ineficiencia en la prestación de servicios de salud al interior de las cárceles, **"Hay enfermos en grave estado de salud que mueren por negligencia en la atención medica".** Es por eso que exigen la ‘liberación humanitaria’ de los cerca de **180 prisioneros de guerra** a nivel nacional que se encuentran **lisiados por heridas en combates y en crítico estado de salud**, así como de aquellas presas políticas en estado de embarazo o de lactancia que son obligadas a vivir con sus hijos e hijas en las celdas de prisión.     ]

[De acuerdo con Nariño, que la salud de los presos políticos no sea atendida se constituye como un  **mecanismo de tortura,** y dada la **sistemática violación a los derechos humanos** sumada a los **altos niveles de corrupción al interior del INPEC**, se hace necesaria la reforma estructural de ese organismo del Estado, bajo los criterios de justicia restaurativa.]

[**"Creemos que ya es hora de materializar los acuerdos de La Habana con gestos de paz"**, afirma  René. Gestos que contemplen amnistías e indultos para delitos políticos y que permitan la salida de prisioneras y prisioneros de guerra mutilados, con problemas de tipo cerebral y falta de órganos que actualmente se encuentran en estado crítico de salud.]

[Debido a que la institucionalidad no se ha manifestado frente a la huelga y las reivindicaciones de los prisioneros políticos, René Nariño indica que proyectan establecer mecanismos de comunicación con las demás cárceles del país para articular una acción más contundente para que finalmente sean escuchados.]
