Title: Lilia García, secretaria del Cabildo Awá fue asesinada en Nariño
Date: 2019-10-16 15:43
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinato de indígenas, Barbacoas, nariño, pueblo Awá
Slug: lilia-garcia-secretaria-del-cabildo-awa-fue-asesinada-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Awá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Imagen de Referencia: Unidad Indígena Del Pueblo Awá Unipa] 

Pese a que los hechos ocurrieron el 13 de octubre, solo hasta hoy se ha informado del asesinato de **Lilia Patricia García**, habitante del territorio ancestral de Watsalpí quien se desempeñaba como secretaria del Cabildo del resguardo indígena Awá  en cercanías a la quebrada Guaguí, parte del corregimiento de Buenavista en Barbacoas, Nariño.

Según la denuncia realizada por la **Unidad Indígena del Pueblo Awá (UNIPA)**, Lilia fue herida por un hombre encapuchado con un disparo de arma de fuego en la espalda mientras se encontraba en el camino que conduce desde la carretera principal del corregimiento de Buenavista, Barbacoas hacia la institución educativa "Los Telembies", lugar en el que estudian niños y niñas que se encuentran en situación de desplazamiento.

Lilia, de  43 años, era secretaria del Cabildo Indígena Awá de Watsalpí y desde hace más de ocho años venía exigiendo a entidades como el INCODER, la constitución legal de Resguardo Indígena, sin que existan avances significativos en el proceso. [(Lea también: Asesinato de Gobernador y líder indígena enluta al Pueblo Awá)](https://archivo.contagioradio.com/awa-luto-asesinato-de-lideres/)

Para la familia García, el asesinato de Lilia es una demostración de la persecución de la que han sido víctimas, pues el 25 de junio del 2015 fue asesinado su hijo de 15 años, quien fue reclutado a la fuerza por el grupo de los “Rastrojos”,  después de estar a cargo del Instituto Colombiano de Bienestar Familiar, entidad que según la denuncia no garantizó su protección. Tras su muerte, Lilia deja 6 hijos menores de edad.

La UNIPA hace responsable de todos los hechos victimizantes que han ocurrido en el territorio al gobierno de Iván Duque debido a la ausencia de garantías en seguridad, protección, salud, educación, alimentación y las continuas amenazas de grupos armados que hacen presencia en la zona como el Frente Oliver Sinisterra.  [(Le puede interesar: Aún no acaba 2018 y ya son 34 indígenas Awá asesinados)](https://archivo.contagioradio.com/indigenas-awa-asesinados/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
