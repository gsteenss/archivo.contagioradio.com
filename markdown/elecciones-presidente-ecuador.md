Title: Ecuador en camino de elegir nuevo presidente
Date: 2017-02-19 10:22
Category: El mundo
Tags: ecuador, elecciones, presidente, Rafael Correa
Slug: elecciones-presidente-ecuador
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/cne-2017-e1487517114175.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Notimundo 

###### 19 Feb 2017 

Con una participación estimada de 12 millones ochocientos mil ciudadanos habilitados para votar, inician las elecciones generales en Ecuador, donde se elegirá el sucesor de Rafael Correa, presidente de ese país durante los últimos diez años. También serán elegidos los 137 legisladores de la Asamblea Nacional y cinco representantes al Parlamento Andino.

Lenin Moreno candidato de Alianza País y vicepresidente de Rafael Correa entre 2007 y 2013, lidera los sondeos de opinión con una intención de voto del 32.3%,  seguido por Guillermo Lasso, candidato por el Movimiento CREO con el 21,5%, Cynthia Viteri del Partido Social Cristiano con el 14% y Paco Moncayo de Izquierda democrática con el 7,7%. (Cifras proporcionadas por Cedatos).

Durante la jornada, que cuenta con la observación internacional de UNASUR, presidida por Pepe Mujica y de la OEA con el ex presidente de República Dominicana Leonel Fernández a la cabeza, también se realizará una consulta popular sobre una iniciativa del Gobierno que busca prohibir a todo cargo público tener bienes en paraísos fiscales.

Los más opcionados

De los 8 candidatos inscritos a la presidencia del Ecuador, las diferentes encuestas y sondeos han posicionado a cuatro de ellos dentro de la preferencia de los electores:

Moreno representa la continuidad de las políticas del gobierno Correa, representada en la Revolución ciudadana, de la que se destacan las apuestas sociales y educativas. El candidato oficialista ha prometido durante su campaña estimular la producción y el trabajo potencializando el sector agrícola y emprender una lucha contra la violencia y el narcotráfico.

Por su parte, Guillermo Lasso, banquero y ex ministro de economía durante el gobierno de Mahuad, recordado por la crisis financiera de 1999; propone un cambio al modelo político, buscando una economía de libre mercado y de competencia; un gobierno donde el sector privado y la sociedad civil tendría un papel protagónico en las políticas de Estado y buscará la incorporación del pais a la Alianza Pacífico.

La candidata y ex diputada Cynthia Viteri, ha prometido incentivar la generación de empleo, la reducción de los costos de producción a las empresas para mejorar su eficiencia y productividad, reducción en algunos impuestos y costos de servicios como la energía eléctrica, se ha mostrado partidaria de la economía de libre mercado.

El cuarto de los 8 candidatos en carrera, es el general en retiro Paco Moncayo,  habla del desempleo como el problema a solucionar y propone una reducción de gastos en el ejecutivo, fortalecer la empresa privada, la dolarización del pais y buscar inversión extranjera y auditar la deuda pública.

Alcanzará la presidencia el candidato que alcance la mitad más uno de los votos, es decir sin incluir los votos nulos o los votos en blanco. O que llegue la 40% de los sufragios y mantenga una diferencia de más de 10 puntos porcentuales con el segundo lugar. De no lograr alguna de estas condiciones se realizará una segunda vuelta el próximo 2 de abril, con los dos que obtengan la mejor votación.

Los electores

En Ecuador existe el voto obligatorio para mayores de 18 años y optativo para mayores de 16 y 65 años, también para los ciudadanos ecuatorianos que habitan en el exterior, los extranjeros que habitan en el país por mas de 5 años, militares y policías y las personas en condición de discapacidad. Le puede interesar: [Crónica desde las calles del Ecuador en campaña.](https://archivo.contagioradio.com/cronica-desde-las-calles-del-ecuador-en-campana/)

Antes de las elecciones de este domingo, votaron las personas privadas de la libertad sin sentencia y las personas que se encuentran en condición de discapacidad, cuya restricción de movilidad los limita a votar desde sus viviendas. Se prevé que la abstención alcance entre el 15 y el 20, mientras que el 35% de los votantes aún se encuentra indeciso.

El presidente Rafael Correa, ejerció su derecho al voto en una escuela de niños al norte de Quito. Durante el ejercició democrático el mandatario hizo un llamado a aceptar los resultados de los comicios, asegurando que las encuestas indican que Moreno alcanzará los votos necesarios para imponerse en la primera vuelta.
