Title: Continúan Movilizaciones en Chile en contra de la reforma a la Educación
Date: 2016-05-26 12:57
Category: Educación, El mundo
Tags: Chile, Movimiento estudiantil, Reforma a la Educación
Slug: continuan-movilizaciones-en-chile-en-contra-de-la-reforma-a-la-educacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/ACE-Chile.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Asamblea Coordinadora de Estudiantes Secundarios 

###### [26 May 2016] 

Diferentes movilizaciones estudiantiles se han presentado en los últimos meses en Chile debido a las reformas a la educación que plantea la presidenta del país Michaelle Bachelet. El día de hoy se llevará a cabo una movilización que no ha sido autorizada **en reclamo a una educación pública, gratuita y estatal.**

Actualmente **9 colegios secundaristas se encuentran en paro** y cese de actividades, de igual forma hay **8 universidades que tienen facultades movilizándose** en apoyo al movimiento estudiantil chileno.

Algunas de las exigencias de los estudiantes se refieren al mondo en el cual se realizaron las reformas educativas, ya que estas **no tuvieron en cuenta las necesidades del movimiento social** y tampoco consideraron sus posturas frente a los cambios que pretende generar. A su vez, el [movimiento estudiantil exige un nuevo modelo de financiación para la educación que redirija los recursos a lugares en donde son más necesarios. ](https://archivo.contagioradio.com/la-gratuidad-de-la-educacion-chilena-no-es-totalmente-cierta/)

De acuerdo con Diego Arraño, vocero de la Asamblea Coordinadora de Estudiantes Secundarios (ACES) "fue necesaria la interrupción de las clases porque nosotros tenemos algo más importante por hacer y es lograr que la educación sea pública y gratuita, venimos con incumplimiento por parte del gobierno desde el 2001, **luego en el año 2011 tuvimos que salir a las calles y frente a la falsa promesa de gratuidad salimos ahora**".

El día de hoy se llevará a cabo [una movilización en las principales calles de Santiago, no aprobada por la Intendencia Metropolitan](https://archivo.contagioradio.com/23-estudiantes-judicializados-y-2-asesinados-en-chile-durante-el-2015/)a, sin embargo la marcha se encuentra convocada por diferentes sectores del movimiento estudiantil como la Asamblea Coordinadora de Estudiantes Secundarios y la Coordinadora Nacional de Estudiantes Secundarios.

<iframe src="http://co.ivoox.com/es/player_ej_11677345_2_1.html?data=kpajmZyXeJahhpywj5WYaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncaXdxszcjabWtsKZpJiSpJbTb46fotjOz8fQqcKfpNTc1MnNssLY0NfOjcnJb6bn1drRy8bSuMbnjLjSj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
