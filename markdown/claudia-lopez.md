Title: "Bla bla bla": los argumentos de Claudia López
Date: 2017-08-04 11:29
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Claudia López, elecciones, politica
Slug: claudia-lopez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Claudia-lopez.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Claudia-lopez1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/claudialopez3-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Claudia-Lopez.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El tiempo 

###### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 03 Ago 2017 

[El marketing político es un éxito, paupérrimo intelectualmente, pero exitoso “¿total aquí quién lee?”...  Nadie podría tumbar una campaña anti corrupción en Colombia a menos que descubra qué hay detrás de ello... más allá de fomentar teorías conspirativas, hay que recordar que en tiempo de campaña por lo general todos son súper independientes y eso sí, todos y absolutamente todas, firman sobre piedra que su lucha contra la corrupción será su bandera más alta, impecable e inmaculada.]

**Eso no es nada nuevo.**

[La virtud de Claudia López, como algunos ya lo sabemos, está en que habla de manera coherente y con los atributos simbólicos de un discurso como regañón.  A mi mente viene el recuerdo de que parte del triunfo de Álvaro Uribe residió en ese discurso que sonaba entre el de un capataz y el de un narco de novela. El caso es que cuando un candidato emerge en Colombia siempre la lucha contra la corrupción es su bandera de campaña. En eso López en teoría y en la práctica se encuentra más que trasnochada.]

[Aquí lo importarte es a quién se le entregan las esperanzas políticas o en quién podemos confiar de manera que sepamos que el próximo o la próxima que suba al poder, no comience a dividir el país a la típica manera mamerta de la derecha, de tratar de convencer, acusar o denigrar sobre la idea de que todo progresismo, toda lucha social, toda resistencia sindical urbana o agraria, toda crítica a esta democracia fallida, todo periodismo alternativo o academia crítica, son en últimas, manifestaciones conexas al servicio secreto del castrochavismo con vínculos norcoreanos… o incluso hasta con los extraterrestres del gerente de Ecopetrol señor Echeverry.  ]

[Claudia López desde hace tiempo viene elaborando ataques ponzoñosos a la izquierda colombiana aprovechando las coyunturas internacionales. Es decir hablando a la vieja usanza, de la típica y trasnochada manera derechista de comparar arbitrariamente contextos indudablemente diferentes, con el fin de asustar incautos o ganar votos de los indecisos a punta de miedos infundados.  ]

[En Colombia los más liberales siempre nos terminan metiendo puñaladas por la espalda, López habla duro y eso está bien, pues no hay manera de inventar que desde la entrevista dada al diario El Tiempo, a López lo único que le preocupa al igual que a todos los liberales (no de partido sino de ideología) es la alianza de la izquierda, es que la izquierda finalmente: les quite votos. Y no vengan a cacarear con que Samuelito o Clarita son izquierda… no no no. Seamos serios, aunque sea una vez ¡carachas!]

[Volviendo al tema, López ya lo ha dicho “La alianza de la izquierda no le conviene al país”… muchos ya hemos cuestionado ¿A cuál país? ¿o al país de quién?  Imagino que no se refiere al país donde más matan líderes sociales, periodistas y sindicalistas. Imagino que no se refiere al país donde la tenencia de la tierra es de tipo "feudal", o imagino que no se refiere al país donde presidente y expresidente están metidos con el escándalo internacional de Odebrecth, o imagino que no se refiere al país con la fosa común urbana más grande del mundo, ni mucho menos al país donde mataban gente inocente para hacerla pasar por guerrilleros. Claro… debe existir un país al que no le convenga la alianza política de izquierda, pero por lo anterior dudo mucho que sea Colombia, pues que yo sepa, el poder siempre ha estado en manos de una derecha, a veces muy liberal, a veces menos liberal, pero al fin y al cabo derecha.   ]

[Nuevamente López vuelve a demostrar con el rifi rafe con Petro en redes, que es una abanderada de la lucha anti izquierda, y antes de que me sindiquen de “petrista” prefiero insistir en que la derecha trasnochada sigue bien representada. López me acuerda de Raisbeck y sus “libertarios”, captando incautos a los que se les recubre de marketing y liberalismo refrito las miiiiiiiiiiiismas posiciones de siempre, pues es más sencillo creer que podemos gobernar a punta de slogans y gritos, que a punta de ideas e inclusión política.   ]

[Esa derecha trasnochada bien representada, quiere respuestas exprés para todo… pues una explicación política y sociológica ¡es obvio! les costaría muchos votos. Sobra ya, esa derecha trasnochada que cuando se le quiere exponer un punto argumentativo y diferencial entonces tilda a la izquierda Colombia de querer llevar el país hacía una Venezuela o simplemente de manera despótica responde tajantemente cuando se les quiere debatir con un: “bla bla bla” o “siguiente pregunta”.   ]

[Hoy que el país necesita, en serio N E C E S I T A dejar de estar pensando en Venezuela y más bien responder por los casi 170 asesinatos por causas políticas entre 2016 y 2017, hoy que necesitamos solucionar la pobreza en el campo, la miseria en las ciudades o pensar en cómo solucionar la desigualdad tan impresionante frente al sistema tributario, jurídico y punitivo del país... no...... López como buena liberal de derecha, sindica y rechaza a la izquierda para aumentar el éxito de su marketing político, pues al final, no podremos ser una Cuba, ni una Venezuela, ni un Mini Estados Unidos, porque sencillamente somos Colombia.]

[En la práctica, López sigue machacando la misma herida que mantiene los problemas en Colombia, es decir la falta de aceptación de la diversidad. Así no le guste a doña Claudia, pues la izquierda, los progresistas, los sujetos críticos existimos. Así diga que no le convenimos al país: existimos y tenemos muy poco “bla bla bla” pero sí bastantes argumentos para asegurar que aquí quien nunca ha dejado el poder ha sido una derecha liberal.]

[Esa derecha disfrazada de liberalismo pela el cobre cuando dice “bla bla bla” a quien le lleva la contraria y trata de exponer su punto. Así muchos se muerdan los codos, no se puede ser tan mamerto como para asociar todas las luchas sociales de Colombia a un complot ruso cubano castrochavista o norcoreano. Que actitud despótica trasnochada la de no querer aceptar que lo que no le conviene al país es seguir negando la amplitud de la diferencia política. Que la derecha existe y que la izquierda también. Así algunos, poco o excesivamente leídos, quieran dar por terminado ese debate arbitrariamente.]

[Aquí no sobra ninguno. Sino que lo que sobra es la manipulación de emocionalidades para que la gente siga sin comprender que sea el modelo que sea no somos Cuba, no somos Venezuela y muchos menos somos Inglaterra o Estados Unidos..... somos Colombia. Con nuestros problemas y por tanto con la capacidad para resolver a nuestra manera, problemas que insisto, son nuestros y difieren sustancialmente de los que tiene otro país.  ]

[Que deje ya la derecha reencauchada en liberalismos bien publicitados de incendiar el país. Que dejen de poner a las masas a mirar problemas ajenos y las preocupen por los propios, que son bastantes y bien graves.]

[Las ideas políticas no son sinónimo de unanimidad. Por eso responder a un contrario con “bla bla bla” deja mucho que decir de una persona inteligente como López, que de seguro sabe muy bien lo que significa la palabra exclusión.  ]

[No convence aquel que se preocupa más por lo que hace el enemigo que por lo que propone a sus amigos. No convence aquel que para fortalecer el marketing de su campaña señala situaciones y contextos que poco tienen que ver con nuestra historia, con nuestra administración, con nuestros berracos problemas. Ahora cogerán de cocheche a Venezuela para aparecer como los más sensatos, para sonar indignados por unos muertos, pero no por otros y con eso ganar más votos... creyendo que aquí nadie lee historia o que solo mira el vacío mental que es RCN o que solo escucha la charla derechosa sabrosa en ocasiones pero fastidiosa y determinista en otras, de Caracol Radio ¡¡No no no no!!]

[No convence aquel que rechaza a la izquierda o a la derecha en un país que necesita mucho más que aplausos cuando todos prometen al unísono luchar contra la corrupción pero luego los vemos en las fotos sociales abrazaditos todas con todos.... ¡dejémonos de publicidad trasnochada! La corrupción es solo un síntoma de una grave enfermedad de la política colombiana; una enfermedad que consiste en creer que ese otro que está en frente mío, que ese otro que tiene algo por decir, que ese otro que tiene ideas que expresar es solo “bla bla bla”. Cuesta reconocer la alteridad, ese otro que a conveniencia es tildado de poco conveniente.  ]

**La corrupción es el síntoma de lo enfermo que resulta pisotear la alteridad señora López… la izquierda existe y conviene siempre y cuando deje de mirar para afuera y piense en Colombia. La derecha existe y conviene siempre y cuando deje mirar para afuera y piense en Colombia. Aquí los que niegan la alteridad son los que sobran.**

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/) 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
