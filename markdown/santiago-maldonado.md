Title: Desaparición de Santiago Maldonado estaría relacionada con su apoyo al pueblo mapuche
Date: 2017-08-31 14:06
Category: El mundo, Nacional
Tags: Argentina, desparición forzada, mapuches, pueblos ancestrales argentinos, santiago maldonado
Slug: santiago-maldonado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/SANTIAGO-MALDONADO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diario Popular ] 

###### [31 Ago 2017] 

Las movilizaciones multitudinarias en Argentina, exigiendo la verdad sobre el paradero del joven Santiago Maldonado, **continúan el viernes 1 septiembre principalmente en la Plaza de Mayo de Buenos Aires**. Este caso completa casi un mes sin que el gobierno de Mauricio Macri asuma alguna responsabilidad por las acciones de la Gendarmería, que según testigos de la comunidad Mapuche, fueron los que se lo llevaron.

La desaparición de Maldonado ha causado gran indignación entre los argentinos en la medida que **recuerda la época de la dictadura de los años 70 cuando las fuerzas militares desaparecían personas indiscriminadamente.** En las calles, los buses y hasta en los estadios de fútbol, han puesto pancartas con la cara de Santiago Maldonado.

**¿Qué pasó con Santiago Maldonado?**

Según algunos periodistas argentinos y defensores de DDHH cercanos al proceso, la desaparición de [Santiago Maldonado](https://archivo.contagioradio.com/argentinos-reclaman-verdad-y-justicia-en-caso-de-santiago-maldonado/)**está relacionada con una serie de factores que se vinculan con la constante lucha del pueblo ancestral Mapuche** por recuperar territorios en la Patagonia.

Desde 2015 este pueblo originario **ha denunciado la venta de territorios por parte del Estado argentino a la empresa italiana Benetton.** La periodista Laura Capote indicó que** **“la resistencia ancestral Mapuche reclama las más 900 mil hectáreas que le fueron vendidas a Benetton donde tienen una crianza de más de 100 mil ovejas que representa la producción del 10% de la lana que necesitan para hacer ropa”.

La periodista manifestó que en enero de este año, en la provincia Chubut en la Patagonia, **creció la criminalización del pueblo Mapuche** donde “las autoridades los tildan de terroristas y delincuentes”. Para junio, fue detenido por las autoridades el líder de la comunidad Facundo Jones Huala quien aún continua preso. Finalmente, el 1 de agosto la Gendarmería realizó un operativo policial contra manifestantes Mapuches que estaban sobre una de las vías de la provincia Chubut  en la Patagonia.

Ella afirmó que este operativo se llevó a acabo a las 4:00 am donde **“la Gendarmería disparó indiscriminadamente contra la comunidad”**. En ese momento, Santiago Maldonado, quien había viajado hacía unos meses a la Patagonia para conocer la causa Mapuche, se encontraba con sus compañeros indígenas quienes intentaron escapar de las balas que disparó la guardia policial.

De acuerdo con la información, Maldonado y los demás se adentraron en uno de los brazos del río Chubut para intentar cruzarlo “pero él no logra cruzar y es ahí donde **la Gendarmería lo saca del agua, lo atrapa, lo golpea y lo monta a un camión donde es visto por última vez”**. A partir de este momento, la policía argentina niega cualquier relación con Maldonado y el Estado desmiente la desaparición del joven argumentando que había huido a Chile.

**Tradicionalmente el pueblo originario Mapuche ha sido estigmatizado**

Capote señaló que a finales del siglo XX **“hubo en Argentina una campaña de exterminio de los pueblos originarios de la Patagonia** y a los únicos que no exterminaron fueron a los Mapuches”. A partir de allí y de acuerdo con ella, los gobiernos de turno han negado la existencia de estos pueblos y le han vendido sus territorios a empresas multinacionales.

La periodista aseguró que es por esto que las reivindicaciones indígenas han sido silenciadas y **la desaparición de Maldonado se da en medio de un discurso cambiante de Mauricio Macri** “quien ha cuestionado la cifra de desaparecidos y ha cambiado el discurso de los derechos humanos en Argentina”.

Finalmente, **manifestó que el Gobierno no tiene voluntad política para realizar las investigaciones pertinentes** en la medida en que es responsable por lo que suceda con Maldonado si fue la Gendarmería quien lo detuvo. Dijo además que las instituciones estatales no quieren que se politice el caso de Maldonado por el costo político que esto conlleva.

Mientras tanto, **los argentinos seguirán exigiendo que se diga dónde está Santiago Maldonado** y que se asuma su desaparición como un caso de desaparición forzada para que no quede como un desaparecido más en democracia.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
