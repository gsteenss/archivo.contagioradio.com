Title: Miles de personas marcharon en España por “Pan, trabajo y dignidad”
Date: 2015-03-24 17:29
Author: CtgAdm
Category: Movilización, Otra Mirada
Tags: CGT-PV Antonio Pérez Collado, Marchas de la dignidad 2015, Marchas de la dignidad 22-m, Represión Marchas de la dignidad 2015
Slug: miles-de-personas-marcharon-en-espana-con-el-lema-pan-trabajo-y-dignidad
Status: published

###### Foto:Arainfo.org 

###### [**Entrevista con [Antonio Pérez Collado], secretario de CGT-PV:**]  
<iframe src="http://www.ivoox.com/player_ek_4257931_2_1.html?data=lZeimZ6XdY6ZmKiakp2Jd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcrgxtiYxsqPtMbm1NTbw9iPscLmxM3O1NTSb8bijKrg0saJh5SZo5bOjcjTsozZzZDZx9LFaZS1jIqylIqcdIatpJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

El sábado en, Madrid, la capital de estado español, **miles de personas marcharon con el lema “pan, trabajo y dignidad”** en **contra de las políticas neoliberales** impuestas por la Troika, el FMI, la UE y el gobierno conservador del Partido Popular.

Es el segundo año consecutivo con esta movilización que continua en pie, y que logra convocar a cerca de **500.000 asistentes**, los cuales llenaron por completo la céntrica plaza del sol y los sectores aledaños del centro de Madrid.

Más de **20 columnas de manifestantes partieron desde sus distintas ciudades a pie** hasta llegar a la capital. Otros llegaron en autobuses fletado por las distintas organizaciones. La marcha había sido convocada por organizaciones sociales de base, movimientos sociales y **sindicatos de base** horizontales como CGT, CNT, LAB, o COS.

Entre las reivindicaciones estaban la **derogación** de las **dos últimas reformas laborales**, la **ley de seguridad laboral** conocida como “Ley mordaza”, las reformas **educativas** como la ya conocida “3+2” o el **pago de la deuda financier**a que los convocantes definieron de “ilegítima”.

Según **Antonio Pérez Collado, secretario del sindicato CGT-PV**, no solo se pretende derogar todos los decretos que atentan contra la dignidad humana, sino que también se **exige la reducción de la jornada laboral para acabar con el fuerte desempleo, la educación gratis, la renta básica de los iguales, y el fin de los desahucios.**

Pare el secretario de CGT-PV, la marcha ha sido un éxito, no solo por la asistencia sino también por la organización que ha sido totalmente eficaz.

La convocatoria del sábado también ha sido **un paso más para convocar una huelga social** de trabajo y de consumo, que están organizando movimientos sociales y sindicatos de base, **al margen de los sindicatos mayoritarios** y de partidos de izquierda tradicionales.

Sin embargo la **represión** no se hizo esperar y la policía nacional atacó el bloque antifascista **deteniendo a más de 14 activistas y apealando a un grupo de menores de edad.** Para Antonio Pérez este es un paso más en la línea represora de la alcaldía de Madrid contra protestas masivas como la de las Marchas de la Dignidad.

La **protesta** no ha finalizado ya que continúa con la convocatoria de **huelga en el sector de la educación** prevista para hoy, y que según cifras del propio sindicato el seguimiento ha sido de **más de un 80% en universidades y colegios**.
