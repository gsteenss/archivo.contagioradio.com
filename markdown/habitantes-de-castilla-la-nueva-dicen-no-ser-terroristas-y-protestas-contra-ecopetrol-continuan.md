Title: Habitantes de Castilla la Nueva dicen no ser terroristas y protestas contra ECOPETROL continúan
Date: 2018-02-13 14:43
Category: DDHH, Nacional
Tags: Castilla La Nueva, Ecopetrol, Meta, trabajadores ecopetrol
Slug: habitantes-de-castilla-la-nueva-dicen-no-ser-terroristas-y-protestas-contra-ecopetrol-continuan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/ecopetrol-e1518546206771.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Comunitaria de Acacías] 

###### [13 Feb 2018] 

Los habitantes de la población de Castilla la Nueva en el Meta se encuentran desarrollando una protesta contra la petrolera ECOPETROL. Argumentan que la empresa **no los contrata y se aprovecha de la producción de petróleo** cuando esta actividad no le deja nada de recursos económicos a la población.

De acuerdo con Bitelio García, líder comunitario de Castilla la Nueva, las protestas comenzaron porque “ECOPETROL tiene empresas aliadas que **traen sus propios trabajadores** desde Barrancabermeja, Putumayo o el Huila y a los de aquí no les dan la oportunidad de trabajar”. Denuncian que hace unos días llegaron 50 profesionales de otros lados.

### **Hace 30 años ECOPETROL pidió profesionalizar a los jóvenes del municipio** 

Estas condiciones laborales las han criticado las comunidades en la medida en que, según ellos, la empresa petrolera les había pedido que los jóvenes debían ser profesionales para trabajar allí. Esto lo hicieron muchas personas que han pedido trabajo y no son contratadas, García manifestó que **“nuestros hijos ya son profesionales y no les dan la oportunidad** entonces ¿qué hacemos?”.

Esta es una preocupación que se suma a los reclamos que han hecho con relación a los daños ambientales y **los retornos en inversión** que se realizan en el municipio. “Aquí en Castilla pasa un tubo madre por debajo del pueblo lleno de petróleo, pero a Castilla no le queda sino ruinas”, manifestó García. (Le puede interesar: ["Ecopetrol hace caso omiso a Procuraduría y comunidad en Guamal"](https://archivo.contagioradio.com/ecopetrol-hace-caso-omiso-a-contraloria-procuraduria-y-comunidad-para-entrar-en-guamal/))

### **Comunidades dicen estar cansadas de las mentiras de ECOPETROL**

En protestas anteriores, las comunidades han manifestado que se les ha prometido un diálogo y esto no ha sucedido. **“Aquí nos han dicho mentiras**, se llevan todos los recursos y al pueblo no le dejan nada”, manifestó. Por esto, las personas han estado de acuerdo con que se realice una protesta pacífica que han intentado mantener.

Han pedido que desde ECOPETROL **sean respetados** y que no sean estigmatizados por unos actos que han cometido personas ajenas a la protesta. Además, se preguntan dónde están los políticos que han ido a hacer campaña en ese territorio pues aseguran que “cuando hay protestas somos delincuentes, pero cuando hay elecciones somos ciudadanos”.

### **Manifestantes rechazaron la protesta violenta** 

Frente a las denuncias que estableció el coronel de la Policía de la región, quien dijo que cerca de 400 jóvenes que no son del pueblo son quienes están protagonizando las protestas de manera violenta, García indicó que no tiene conocimiento de estos jóvenes. Sin embargo, manifestó que **se han presentado disparos** con armas de fuego tanto por parte de quienes protestan como también por parte de los integrantes de la Fuerza Pública. (Le puede interesar:["Ecopetrol debería suspender proyecto Trogón I en Guamal, Meta"](https://archivo.contagioradio.com/46238/))

Informó que efectivamente hubo una persona que se encontraba portando un arma de fuego y que disparó contra algunos integrantes de la Policía “en respuesta a los **atropellos del ESMAD** quienes se le metieron a la casa, como si el señor fuera un terrorista y le sacaron todo”. Rechazó el uso de la violencia en la protesta, pero denunció el uso excesivo de la fuerza del ESMAD.

### Finalmente, los manifestantes rechazaron las **estigmatizaciones de algunos medios de comunicación** en donde se les señala de hacer parte de las disidencias de las FARC. García manifestó que en esa población se ha vivido siempre en paz y que “las FARC nunca se han metido al pueblo”. Aseguró que los únicos problemas que han tenido se han presentado desde que ECOPETROL llegó a sus territorios. 

<iframe id="audio_23736368" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23736368_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
