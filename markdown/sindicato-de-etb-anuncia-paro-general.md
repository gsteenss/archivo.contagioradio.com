Title: Sindicato de ETB anuncia paro general
Date: 2016-05-02 16:49
Category: Movilización, Nacional
Tags: Bogotá, Enrique Peñalosa, ETB, Sintratléfonos
Slug: sindicato-de-etb-anuncia-paro-general
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/etb-1-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: cnn] 

###### [2 May 2016]

El anuncio del Paro General fue hecho por William Sierra tras conocerse la intensión por parte del alcalde Enrique Peñalosa de vender la empresa. Según Sierra, la empresa es un activo primordial de las finanzas de la capital y no se justifica venderla para invertir en educación porque la ETB **le ha entregado a la Universidad Distrital más de 13 billones de pesos** que dejaría de recibir si se concreta la venta.

El jefe del sindicato afirmó que el próximo jueves 5 de Mayo se conocerá la hora cero del inicio del cese de actividades.

Por su parte, el excandidato a la alcaldía y ministro para el postconflicto Rafael Pardo manifestó su desacuerdo con la venta. Así mismo lo han manifestado **diversos sectores sociales que además han recalcado** su respaldo a la empresa como activo de la capital y una de las principales aportantes a las finanzas de la capital. [Conozca 14 razones para no vender la ETB.](https://archivo.contagioradio.com/enrique-penaloza-anunciala-venta-de-la-empresa-publica-etb/)

Desde la misma posesión de Peñalosa se ha despertado una fuerte polémica porque mientras la alcaldía anuncia que la empresa está reportando pérdidas, los integrantes del sindicato afirman que **las inversiones a mediano plazo se están reportando como pérdidas** y [no consideran el potencial competitivo que tiene la empresa](https://archivo.contagioradio.com/etb-le-aporta-al-distrito-cerca-de-un-billon-de-pesos/) en relación con la aplicación de tecnologías como la fibra óptica, siendo la única empresa que está brindando ese servicio en el país.

Según diversos analistas una empresa pública se debe mantener como tal si cumple dos condiciones. Una primera que esté al servicio de las políticas públicas de educación, puesto que tiene la posibilidad de compartir el conocimiento y democratizarlo sin que este derecho esté a merced del capital privado.

Otro de los aspectos es que la empresa está lista para aportar en el servicio de fibra óptica y en la competencia para masificar las nuevas tecnologías de la información y las comunicaciones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
