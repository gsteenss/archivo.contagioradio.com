Title: José Jair Orozco Calvo y Lede María Ortega, dos líderes asesinados el 26 de enero
Date: 2019-01-28 11:46
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: Cartago, El Tarra, Lider social, líderesa
Slug: jose-jair-orozco-maria-ortega
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/Diseño-sin-título-1-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [27 Ene 2019] 

Nuevamente la violencia se hace evidente en Colombia: en un solo día fueron asesinados los líderes José Jair Orozco Calvo en Cartago, Valle del Cauca,  y Lede María Ortega Ortíz en El Tarra, Norte de Santander. Ese mismo sábado, Contagio Radio ya había informado el asesinato de Samuel Gallo en el municipio de El Peñol, en Antioquia. (Le puede interesar: ["Silencian la voz de denuncia de Samuel Gallo, líder comunitario en Antioquia"](https://archivo.contagioradio.com/samuel-gallo-lider/))

José Orozco se encontraba cerca de su casa cuando sujetos armados dispararon en repetidas ocasiones contra su humanidad; aunque personas cercanas a l lugar de los hechos lo auxiliaron rápidamente, el hombre de 52 años falleció en el Hospital San Jorge de Pereira,Risaralda, al que fue trasladado desde Cartago por la gravedad de sus heridas. Orozco era miembro de la Junta de Vivienda Comunitaria de la Urbanización de Vista Hermosa en Cartago, y además lideraba el plan de vivienda comunitaria que se adelanta en su Urbanización.

Por su parte, según medios locales, María Ortega fue asesinada por su expareja, quien en horas de la madrugrada del sábado ingresó a su vivienda en la vereda Mundo Nuevo, cerca al municipio de El Tarra, y le propinó heridas con arma blanca; Ortega fue trasladada hasta el centro de atención hospitalaria de ese Municipio, pero falleció minutos después de su arribo al mismo. La líder pertenecía a la Junta de Acción Comunal de El Tarra, en la que se desempeñaba como secretaria.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
