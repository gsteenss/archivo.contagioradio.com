Title: Familias en Turbo, en riesgo inminente de un desplazamiento masivo
Date: 2016-06-24 15:08
Category: DDHH, Nacional
Tags: Autodefensas Gaitanistas de Colombia, Desplazamiento en Colombia, Turbo, vereda Guacamayas
Slug: familias-en-turbo-en-riesgo-inminente-de-un-desplazamiento-masivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [24 Junio 2016] 

Desde principios de este mes, grupos paramilitares de las Autodefensas Gaitanistas vienen hostigando a las familias que retornaron a la vereda Guacamayas de Turbo, Antioquia, luego de ser desplazadas entre 1996 y 1997, por las Autodefensas Unidas de Colombia. Actualmente, la mayoría de ellas se han visto obligadas a concentrarse en las casas más centrales, **bajo la amenaza de ser agredidos si van a sus fincas a trabajar o salen de la vereda para conseguir alimentos** para los niños que ya no tienen nada para comer, según afirma el señor Julio Correa.

De acuerdo con el poblador, el pasado 7 de junio, 6 hombres armados con fusiles arribaron a la vereda y acribillaron a varias de las familias, les robaron sus celulares y once cabezas de ganado. El día 11 volvieron y **agredieron físicamente a otras de las familias, les destrozaron sus documentos y dispararon contra menores de edad**. Pese a que en la zona hay presencia del Ejército, las comunidades aseguran que no es suficiente, pues no tienen control de toda la vereda, en la que también hay empresarios ganaderos.

El pasado 14 de junio una delegación de estas familias se reunió con altos funcionarios de Gobierno, entre ellos el Ministro del Interior, quien les aseguró que harían una reunión de alto nivel en la zona para atender la situación; sin embargo, las familias están esperando la confirmación de la fecha y la oportuna atención de los gobiernos nacional, departamental y municipal, [[que pese a las denuncias](https://archivo.contagioradio.com/reclamantes-de-tierras-en-inminente-riesgo-de-desplazamiento-por-paramilitares/)] no han brindado soluciones efectivas, por lo que **las familias exigen a autoridades nacionales e internacionales la garantía de condiciones de seguridad para la permanencia digna en su territorio**.

<iframe id="audio_12018873" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12018873_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
