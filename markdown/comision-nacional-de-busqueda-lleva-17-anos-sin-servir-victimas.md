Title: Comisión Nacional de Búsqueda lleva 17 años sin servir: Víctimas
Date: 2017-03-14 14:58
Category: Nacional, Paz
Tags: ASFADDES, desaparecidos, JEP, MOVICE, Unidad de Busqueda
Slug: comision-nacional-de-busqueda-lleva-17-anos-sin-servir-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/victimas-jep.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Voces de Paz 

###### 14 Mar. 2017 

Frente a los mecanismos que componen la **Jurisdicción Especial para la Paz (JEP)** aprobada este lunes, organizaciones de víctimas como el Movimiento de Víctimas de Crímenes de Estado **(MOVICE), han expresado su descontento por la pérdida de autonomía de la Unidad de Búsqueda de Personas Desaparecidas (UBPD).**

Para el MOVICE, **después de 17 años no han encontrado respuesta eficaz por parte de la actual Comisión Nacional de Búsqueda de Personas Desaparecidas (CNBPD)**, y si bien reconocen las arduas labores que otras organizaciones han estado gestionando en la CNPB para que se reconozcan los derechos de las víctimas de desapariciones forzadas, estas no han redundado en la búsqueda efectiva de personas desaparecidas. Le puede interesar: [99% de los casos de desaparición forzada están en la impunidad](https://archivo.contagioradio.com/99-de-los-casos-de-desaparicion-forzada-estan-en-la-impunidad/)

Razón por la cual, **17 años después de creada la Comisión, los familiares deben continuar exigiendo al Estado, organice el aparato estatal para dar con el paradero de las víctimas de desaparición forzada.**

Agregan que **dicha Comisión “no ha dado resultados favorables para las víctimas de desaparición forzada** y en no pocos casos ha sido un escenario irrespetuoso con los derechos de las víctimas y ha evidenciado una falta de compromiso”. Le puede interesar: [Así sería condena de Rito Alejo del Río en las comunidades víctimas](https://archivo.contagioradio.com/asi-seria-condena-de-rito-alejo-del-rio-en-las-comunidades-victimas/)

Es por esto, que el MOVICE ha dicho que “**no se puede generar un paralelismo a la oportunidad que nos da el Acuerdo Final para la terminación del conflicto armado al crear la UBPD,** y de continuar la CNBPD” pues según ellos esa situación será un obstáculo para “esta nueva esperanza que nos da el proceso de paz”. Le puede interesar: [Fiscalía ha suspendido 35 mil investigaciones sobre desaparición forzada](https://archivo.contagioradio.com/fiscalia-ha-suspendido-35-mil-investigaciones-sobre-desaparicion-forzada/)

Estos argumentos también han sido esgrimidos por integrantes del Senado como Iván Cepeda del Polo Democrático quien aseguró que **“en el caso de la unidad de búsqueda de personas desaparecidas ha sido aprobado un artículo que le resta independencia y autonomía.** (…) y se introdujo un artículo que permite la participación de las víctimas de una manera mucho más eficaz”.

Pese a ello, el presidente Juan Manuel Santos, aseveró en su más reciente alocución luego de la aprobación de la JEP, que la Comisión y la Unidad de búsqueda sí funcionarán y “ayudará a que todos los actores del conflicto cuenten la verdad”. Le puede interesar: [Jurisdicción de Paz será una herramienta para las víctimas](https://archivo.contagioradio.com/jurisdiccion-de-paz-sera-una-herramienta-para-las-victimas/)

**Queda por establecerse la finalidad que busca el gobierno al mantener la CNBPD,** sin embargo, una situación clara es que un escenario que no ha funcionado debería terminarse para dar paso a otro mecanismo que cumpla el objetivo de encontrar a los desaparecidos en el que se reconozcan los avances de las víctimas en materia de esclarecimiento de la verdad y de labores para encontrarlos. Le puede interesar: [Cambios en los Acuerdos de Paz no garantizan derechos de las víctimas: Amnistía](https://archivo.contagioradio.com/amnistia-internacional-reconoce-presencia-paramilitar-en-colombia/)

### **Consulte el comunicado del MOVICE** 

[Comunicado Movice](https://www.scribd.com/document/341876714/Comunicado-Movice#from_embed "View Comunicado Movice on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_12597" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/341876714/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-g9Mwiv8Cr5pIUiGQYhs6&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>
