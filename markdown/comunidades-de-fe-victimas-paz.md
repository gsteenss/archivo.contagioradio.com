Title: Comunidades de fé participaron en la maratón radial
Date: 2017-12-13 10:20
Category: Nacional, Política
Tags: colombia, conflicto, paz, víctimas
Slug: comunidades-de-fe-victimas-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/DQ7PK6VXkAALmyI.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Sintonizate con la paz 

###### 13 Dic 2017 

Las comunidades de fe de los territorios víctimas del conflicto armado en Colombia, también hicieron presencia en el marco de la maratón radial  “Con voz tejiendo País”. Eustaquio Polo y Ledis Tuiran líderes de las cuencas de Curvaradó y Pedeguita y Mancilla, comentaron la importancia del perdón y de la justicia en el camino de las comunidades en búsqueda de la paz.

Eustaquio Polo hace parte de las comunidades que habitan en la cuenca del Curvaradó, en Chocó, en la zona de biodiversidad Yuliana, un proceso que surge del desplazamiento por parte de paramilitares en 1997 y que de acuerdo con Eustaquio ha tenido la fuerza suficiente debido a la fe y a la persistencia de las personas para retornar al territorio.

“Gracias a Dios nos fuimos fortaleciendo espiritualmente y orando unos por otros, como lo estamos haciendo ahora” afirmó Eustaquio y agregó que el conflicto ha generado una unión entre las personas para “buscar la salida hacia la paz”.

De otro lado Ledis Tuiran vive en la zona de Biodiversidad La Ceibita, en la cuenca de Pedeguita y Mancilla, señaló que muchos de sus compañeros han sido asesinados mientras que otros están amenazados, sin embargo estas situaciones no han sido obstáculo para que  quienes han sido víctimas del conflicto le apuesten a perdón y la reconciliación.

Además señaló que el papel que han desarrollado las mujeres desde la fe, también ha hecho que los roles de liderazgo cambien y tengan el reconocimiento y respeto al interior de las comunidades, “Hay muchas personas que han puesto su confianza en mi y la verdad esto nace del corazón, uno ve la injusticia y Dios nos da la fortaleza, ser líder, ser madre y padre no es fácil pero acá estamos”.

Finalmente los líderes invitaron a los diferentes sectores de la sociedad a unirse en este tiempo de festividades y calor de hogar a luchar por la construcción de la paz, “queremos dejar unas buenas huellas a nuestras generaciones y es la invitación que hacemos a todos y todas” afirmó Polo.

<iframe id="audio_22637103" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22637103_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="yj6qo ajU">

<div id=":2c4" class="ajR" style="text-align: justify;" tabindex="0" data-tooltip="Show trimmed content">

![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif){.ajT}

</div>

</div>
