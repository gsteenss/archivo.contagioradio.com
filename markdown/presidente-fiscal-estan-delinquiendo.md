Title: Hay quienes creemos que el Presidente y el Fiscal están delinquiendo: Iván Cepeda
Date: 2019-03-27 13:26
Category: Entrevistas, Judicial
Tags: Cepeda, Iván Duque, Nestor Humberto Martínez, uribe
Slug: presidente-fiscal-estan-delinquiendo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-27-a-las-1.15.17-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @IvánCepedaCast] 

###### [27 Mar 2019] 

Este martes se conoció la denuncia que realizó el senador Iván Cepeda sobre el presunto prevaricato cometido por el fiscal Néstor Humberto Martínez y el presidente Iván Duque, al incumplir el mandato del Acto Legislativo 002 de 2017 en el que se señala que **"las instituciones y autoridades del Estado tienen la obligación de cumplir de buena fe con lo establecido en el Acuerdo Final"**. La acción ha suscitado dudas sobre la trascendencia de la misma, y los efectos que podría tener para el Presidente y el Fiscal.

De acuerdo a lo explicado por el Denunciante, esta acción es de carácter penal y se refiere al delito de prevaricato agravado que estarían cometiendo ambos funcionarios cuando "no actuaron de buena fe en distintas circunstancias, pero en particular, con el episodio de las objeciones a la Ley Estatutaria de la Jurisdicción Especial para la Paz (JEP)"; pues como lo han explicado diferentes congresistas, aunque las objeciones fueron presentadas con carácter de inconveniencia (política, social o económica), en realidad se argumentaron de forma jurídica sobre hechos que fueron resueltos y analizados por la Corte Constitucional.

De tal forma que para Cepeda **tanto el Fiscal en sus reparos a la JEP, como el Presidente "están engañando a la sociedad colombiana"**. Sin embargo, como el Senador lo recuerda, los altos funcionarios del Estado son juzgados por la Comisión de Investigación y Acusación de la Cámara de Representantes; conocida como la Comisión de Absolución, pues es un organismo sin independencia jurídica por ser de carácter político.

### **¿Para qué sirve la denuncia si no se espera una acción jurídica?** 

Aunque el Senador reconoció que no se espera una acción sancionatoria por esta denuncia, dijo que la misma sí tiene una función política, "porque evidencia ante la comunidad internacional y la opinión pública que hay personas que creemos que el Presidente y el Fiscal están delinquiendo y violando la Ley". Adicionalmente, la denuncia podría tener acciones legales en el futuro. (Le puede interesar: ["Seis argumentos cuestionan las objeciones de Duque a la JEP"](https://archivo.contagioradio.com/desmienten-objeciones-duque-jep/))

### **Congreso tendrá que decidir sobre las objeciones** 

Mientras se resuelve la denuncia y previendo sus posibles resultados, lo que ocurrirá con las objeciones sean tramitadas por el Congreso de la República y para discutir este tema, el Senado y la Cámara de Representantes ya conformaron sus comisiones accidentales. Sobre la Comisión del Senado, Cepeda señaló que fue elegida de forma inequitativa y parcial porque no responde a los criterios establecidos en la Ley Quinta para este tipo de situaciones, y está compuesta en un 80% por personas que integran partidos de Gobierno.

Por lo tanto, **el Senador no se hace "ninguna ilusión sobre las conclusiones que puede tener esta Comisión"**, y espera que el Congreso inicie las discusiones sobre el tema en la semana del 8 de abril para saber cómo están las fuerzas para tomar una decisión sobre las objeciones. (Le puede interesar: ["Oposición y Gobierno miden fuerzas en el Congreso por Plan Nacional de Desarrollo y JEP"](https://archivo.contagioradio.com/congreso-por-plan-nacional-de-desarrollo-y-jep/))

### **La preocupación de Cepeda por la denuncia de \#LaCalladita ** 

A principios de esta semana el periodista Daniel Coronell denunció que la magistrada de la Corte Suprema de Justicia Cristina Lombana, encargada de investigar al senador Álvaro Uribe por el caso de falsos testigos, no informó que tuvo una relación laboral con Jaime Granados, actual defensor de Uribe. \#LaCalladita, como fue nombrada Lombana por su omisión, señaló que dicha relación había ocurrido hace 20 años, y por eso no la hizo pública.

Ante esta situación, Cepeda dijo que aún no se pronunciaría de fondo, pero afirmó que sí le genera una gran desconfianza el hecho de que la Magistrada "además de tener un rango en las Fuerzas Militares y ser juez en la justicia penal militar, **ha tenido una relación de vieja data" con el abogado Granados.** Lo que significaría para Cepeda y sus abogados una razón para dudar sobre la imparcialidad de la magistrada en sus decisiones sobre Uribe Vélez. (Le puede interesar: ["Magistrada Lombana debería hacerse a un lado en investigación sobre Uribe"](https://archivo.contagioradio.com/magistrada-lombana-deberia-hacerse-lado-investigacion-uribe-defensa-cepeda/))

<iframe id="audio_33779145" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33779145_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
