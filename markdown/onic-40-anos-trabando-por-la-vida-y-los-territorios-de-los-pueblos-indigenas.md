Title: ONIC: 40 años de resistencia histórica
Date: 2020-02-26 17:42
Author: AdminContagio
Category: Comunidad, Entrevistas
Tags: amazonas, indígenas, ONIC, paz, territorio
Slug: onic-40-anos-trabando-por-la-vida-y-los-territorios-de-los-pueblos-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp_Image_2020-02-26_at_11.32.50_AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto:* @ONIC\_Colombia

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Organización Nacional Indígena de Colombia (**ONIC**), conmemoró este 25 de febrero los **40 años de su fundación**, por medio de un encuentro **representantes de las 120 comunidades** indígenas allí congregadas dialogaron sobre los logros y retos durante estos años, así como las metas para el tiempo venidero. (Le puede interesar: <https://archivo.contagioradio.com/2020-oportunidad-unificar-exigencias-sociales-onic/>)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Onic: "Tejiendo historia y sembrando paz"

<!-- /wp:heading -->

<!-- wp:paragraph -->

La organización surgió el 25 de febrero 1980 en Coyaima, Tolima, lugar donde se desarrolló el **Primer Encuentro Nacional Indígena de Colombia**, "*allí nos encontramos y reconocimos como diversos **cerca de mil quinientos hombres y mujeres de distintos pueblos***, *caminamos luego los territorios para volvernos a encontrar 16 meses después y **nombrar con palabra dulce y sabia de mujer, el sueño de la ONIC**"*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1232349089779666945?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Etweet","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1232349089779666945?ref\_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Etweet

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este movimiento se forjó según la organización, gracias a la existencias de organizaciones como, Consejo Regional Indígena del Cauca (CRIC); Confederación Indígena Tayrona (CIT), Concejo Regional Indígena de Risaralda (CRIR), entre otras **organizaciones que motivaron esta lucha**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La ONIC es una iniciativa de los **pueblos actuales para *"reivindicar los pueblos originarios*** *como sujetos de derechos"* en el contexto sociopolítico difíciles en el que es necesario *"organizarse y actuar en unidad* *para permanecer en los territorios".* (Le puede interesar: <https://archivo.contagioradio.com/las-practicas-turisticas-que-mas-danan-los-ecosistemas-segun-pueblos-indigenas-de-la-sierra/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estos retos la Consejera de Derechos Humanos de la ONIC, Aida Quilcué, señaló, *“estamos haciendo memoria, han sido 40 años de luchas conquistadas, **llevando en nuestros hombros y en nuestro caminar a los compañeros asesinados por defender nuestros derechos y nuestros territorios**”*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte el fotográfico Jesús Abad Colorado en la celebración señaló, “*los Pueblos Indígenas, me han enseñado a resistir, a caminar la palabra, a **conectar una cámara con el ojo, el corazón y la conciencia de un país que aún no ha entendido la dignidad y valentía de ustedes**, les agradezco por estar y por resistir ”.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Orientada bajo los principios milenarios de unidad, tierra, cultura y autonomía la ONIC por medio de un comunicado reiteró los temas que han afrontado como organización, *"la participación de los Pueblos Indígenas en la **Constituyente de 1991**; el cruento **conflicto en los años 90** y del que aún somos víctimas; el reconocimiento de los **derechos colectivos como sujetos políticos**,y la participación activa como **actores históricos de paz**"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo reconocieron que no ha sido un camino sencillo, un recorrido que ha costado la vida en los últimos años de cerca de 200 indígenas creyentes de la paz; a pesar de ello reiteraron su apoyo al proceso que vive Colombia, "Sigan contando con nosotros para la paz, nunca para la guerra" señaló el Consejero Mayor de la ONIC Luis Kankui .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente destacaron la ejecución y adopción de la **Ley de Gobierno Propio**, un pacto para volver al Origen, en concordancia con con la tierra y las raíces, encaminada a "40 años más de resistencia". (Le puede interesar: <https://www.justiciaypazcolombia.com/situacion-de-los-derechos-humanos-en-colombia-informe-anual-del-alto-comisionado-de-las-naciones-unidas-para-los-derechos-humanos/>)

<!-- /wp:paragraph -->
