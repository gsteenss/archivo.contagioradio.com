Title: Empresas estatales lideran aplicación del fracking en América Latina
Date: 2016-09-20 13:23
Category: Entrevistas, eventos
Tags: derrames de petróleo, explotación de petróleo, fracking, fractura hidráulica
Slug: empresas-estatales-lideran-aplicacion-del-fracking-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Fracking-Vaca-Muerta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Independencia Energética ORG] 

###### [20 Sept 2016] 

Este miércoles en el marco de la Segunda Jornada Nacional contra el Fracking, se llevará a cabo el "Foro Internacional El Fracking en las Américas y Colombia. Luchas y Resistencias", en el que participaran organizaciones internacionales como el Observatorio Petrolero del Sur y Alianza Latinoamericana frente al Fracking, que presentará un informe regional con el que pretende llamar la atención sobre los **impactos socioambientales de la aplicación de la técnica en distintas naciones latinoamericanas**.

De acuerdo con el investigador Hernan Scandizzo, en América Latina se han generado cambios en los marcos regulatorios para promover la explotación de gas y petróleo con técnicas no convencionales como el fracking; **se han flexibilizado las normas ambientales para hacer posibles estos proyectos**;[ [no ha habido consulta previa a las comunidades](https://archivo.contagioradio.com/mas-de-100-familias-afectadas-por-contaminacion-de-agua-en-barrancabermeja/)]; no se ha aplicado el principio de precaución; ni se ha brindado información pública veraz y completa, acerca de las implicaciones de la fractura hidráulica para que la población pueda tomar posición acertada.

"Vemos que **las empresas controladas por los Estados nacionales son las que tienen un protagonismo particular** en la aplicación del fracking", asegura el investigador y agrega que la mayoría de proyectos se han concentrado en Argentina, en dónde la estatal YPF, junto a Chevron y Petronas ha llegado a perforar por lo menos mil pozos para extraer gas y crudo; mientras que en Chile es la Empresa Nacional de Petróleo quien lidera la aplicación de la técnica, y en México lo hace la empresa Petróleos Mexicanos.

El investigador insiste en que naciones como Argentina, Bolivia, Brasil, Colombia, México y Chile, han tenido "una experiencia muy traumática con la explotación de yacimientos comunes, **altos niveles de contaminación, desplazamiento de pueblos y violación de derechos**" que se cree [[aumentarán con la explotación no convencional](https://archivo.contagioradio.com/?s=san+martin+)], pese a que haya sido presentada por Gobiernos y empresas como una técnica segura y viable para el autoabastecimiento y la exportación.

"En los países en los que hay una fuerte apuesta por los yacimientos no convencionales hay un discurso muy blindado, desde las empresas y las autoridades, diciendo que el fracking es algo seguro, que es la posibilidad del autoabastecimiento y del desarrollo de nuestros pueblos, mientras que en los países donde está ocupando un papel secundario en la agenda, se sincera el discurso y **desde el mismo Estado se reconocen los perjuicios que pueden ocasionar este tipo de explotaciones**", agrega el experto.

A nivel global han habido prohibiciones frente al fracking, por sus nocivos impactos al medio ambiente y a la salud pública, por lo que se insiste en la necesidad de **repensar los modelos de desarrollo vigentes actualmente en América Latina**, en que sean concertados con las comunidades, pues si bien en la mayoría de casos hay determinaciones nacionales, desde los pueblos se deben gestar [[estrategias de resistencia y defensa del territorio para lograr hacer frente](https://archivo.contagioradio.com/arranca-la-segunda-jornada-nacional-contra-el-fracking-en-colombia/)].

<iframe id="audio_12983913" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12983913_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
