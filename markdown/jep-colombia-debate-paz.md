Title: Jurisdicción Especial de Paz incluirá a terceros implicados en la guerra
Date: 2017-02-01 12:08
Category: Nacional, Paz
Tags: colombia, debate, JEP, paz
Slug: jep-colombia-debate-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:  Contagio Radio 

##### 1 Feb 2017

La aplicación de la **Jurisdicción Especial de Paz (JEP), la Comisión de garantías para la búsqueda de personas desaparecidas y la Comisión de la Verdad** plasmadas en el acuerdo entre el gobierno y las FARC, son los temas que **se discuten en el segundo de 4 debates **que regulan el mecanismo de la “Vía Rápida” o “Fast Track”, en la plenaria de la Cámara de Representantes.

**Angela María Robledo**, representante por el Partido Verde afirmó que **será un debate difícil y complejo por que se discuten los temas centrales del acuerdo de paz**. Uno de ellos el que tiene que ver con la JEP, sobre la cual hay proposiciones y cambios en el sentido inicial del acuerdo, por ejemplo, **la cadena de mando para juzgar los crímenes de las FFMM** **y la comparecencia obligatoria de terceros en el conflicto**.

Robledo, aseguró que aunque se ha intentado agregar una serie de beneficios mayores para los integrantes de las Fuerzas Armadas, **este punto iría en contra vía del derecho internacional** y en ese sentido destacó la reciente advertencia por parte de la fiscalía de la Corte Penal Internacional sobre la posibilidad de que **desaparezca la responsabilidad en la cadena de mando**.

Sobre la aprobación del punto que pretende que terceros en el conflicto asistan a la JEP de manera obligatoria, se habían presentado algunos proposiciones que cambiaban el acuerdo y solamente indicaban que empresarios financiadores asistirían de manera voluntaria. En ese aspecto se logra que los tribunales establezcan que terceros sean llamados obligatoriamente una vez se inicie la investigación necesaria por parte de los magistrados. Le puede interesar: [En debates de JEP algunos buscan impunidad](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-2/)

Así mismo, y en la línea de garantizar la bilateralidad, Angela Robledo aseguró que van a hacer una propuesta para que los testimonios aportados por las víctimas en la Comisión de la Verdad, **sean insumos básicos para que se adelanten los procedimientos necesarios en los tribunales de la JEP** y no arriesgar a las víctimas con testimonios en diferentes instancias puesto que ello **podría significar un riesgo de revictimización**.

<iframe id="audio_16776702" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16776702_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
