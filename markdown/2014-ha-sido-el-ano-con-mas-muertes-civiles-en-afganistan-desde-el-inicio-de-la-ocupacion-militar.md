Title: 2014 ha sido el año con más muertes civiles en Afganistán desde el inicio de la ocupación militar
Date: 2015-02-18 19:30
Author: CtgAdm
Category: El mundo, Política
Tags: Afganistán, EEUU, ONU Afganistán, Retirada de tropas EEUU Afganistán
Slug: 2014-ha-sido-el-ano-con-mas-muertes-civiles-en-afganistan-desde-el-inicio-de-la-ocupacion-militar
Status: published

###### Foto:Lahistoriaconmapas.com 

En total según el informe de la **UNAMA (Misión de Naciones Unidas para la asistencia en Afganistán) las muertes han aumentado un 22%** desde que en Diciembre de 2014 comenzara la retirada de las tropas del ejército estadounidense.

**Según las cifras entregadas por UNAMA, 2014 ha sido el año con mas muertes civiles ha causa del conflicto armado que comenzó con la ocupación militar**, calificada de antiterrorista, por parte de una coalición internacional encabezada por EEUU con el objetivo de derrotar al régimen Talibán e instaurar una "democracia".

13 años después del inicio de la guerra, los informes de **Naciones Unidas** inciden en que muy lejos de finalizar el conflicto, las muertes de civiles se agudizan llegando a la cifra máxima este año **con 3.699 civiles muertos en actos de guerra y otros 6.849 heridos**.

Mientras que en años anteriores las muertes se producían por artefactos explosivos y minas antipersona, **este último informe habla de que el 70% de las muertes están relacionadas con combates entre la insurgencia y el ejército Afgano**, sin embargo los heridos continúan proviniendo de actos de sabotaje.

La ONU atribuye mas del 80% de las muertes a los talibanes, quiénes niegan esta acusación y acusan al organismo internacional de **partidista**.

El informe también critica fuertemente la falta de protección a la población civil por parte de las fuerzas gubernamentales, ya que han causado un 20% de las muertes.

Muy lejos de finalizar el conflicto armado, a pesar de las recién entrenadas fuerzas gubernamentales y la nueva policía, los focos insurgentes no han desparecido ni tampoco se han reducido los ataques.

Aunque en palabras de la directora de Derechos Humanos de la UNAMA, Georgette Gagnon, **"Para las mujeres y los niños afganos, la angustia de perder a un esposo y padre en el conflicto a menudo es sólo el comienzo de sus sufrimientos y penurias, a largo plazo, las consecuencias sociales y económicas son devastadoras"**.

La realidad es que niños y mujeres son las principales víctimas del conflicto, en un país donde se vive la pero situación a nivel mundial de violación de derechos humanos de las mujeres y donde, hasta hace poco, era legal la violación por parte del esposo.

**30.000 heridos y 18.000 muertos es el saldo total que dejan estos 13 años de guerra abierta**, un gobierno salpicado por la corrupción, una sociedad donde ahora mismo es impensable el respeto de los derechos humanos y donde la mujer y los niños sufren una especial situación de desprotección y victimización a causa del conflicto armado.
