Title: No es posible hacer Fracking de forma responsable
Date: 2018-08-15 13:09
Category: Ambiente, Nacional
Tags: Alianza Colombia Libre de Fracking, Ambiente, fracking, Iván Duque
Slug: no-es-fracking-responsable
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-15-a-las-12.54.38-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PizarroMariaJo] 

###### [15 Ago 2018] 

Ante el anunció de la ministra de minas y energía, María Fernanda Suarez sobre la posibilidad de permitir que se exploten Yacimientos No Convencionales (YNC) mediante la técnica del fracturamiento hidráulico o 'Fracking', organizaciones sociales se mostraron desconcertadas, pues Iván Duque había prometido en campaña que no iba a permitir que se desarrollara esta técnica en el país.

### **No es posible hacer Fracking de forma responsable: Alianza Colombia Libre de Fracking** 

Carlos Andrés Santiago, integrante de la Alianza Colombia Libre de Fracking, aseveró que no es posible que se haga explotación de YNC de forma responsable, puesto que "sí bien hay adelantos tecnológicos y personas formadas en estas actividades, **hay muchas investigaciones serias que muestran los impactos al territorio, las personas y el ambiente del Fracking".**

Aunque el mensaje fue enviado por uno de los miembros del gabinete de Duque, Santiago señala que es un movimiento coordinado desde el Gobierno Nacional y obedece a un discurso concertado al interior del mismo. Por lo tanto, **si se llegara a aprobar el uso del Fracking por parte del Presidente de la República, estaría incumpliendo a sus promesas en campaña.**

Ante esa posibilidad, el activista sostuvo que la agenda de incidencia del movimiento ambientalista continuará sin cambios, esto, porque que ya hay un Proyecto de Ley radicado en el Congreso con el que se buscará la prohibición del fracking, y que tanto Senadores como Representantes a la Cámara, asuman posiciones políticas en esta materia. (Le puede interesar: ["Radicado Proyecto de Ley que prohibiría el Fracking en Colombia"](https://archivo.contagioradio.com/listo-proyecto-de-ley-que-prohibiria-el-fracking-en-colombia/))

De otra parte, Carlos Santiago afirmó que aunque Colombia aún puede sostener su recurso energético con explotaciones convencionales y sin acudir a los YNC, esta explotación debería contar con una perspectiva de transición energética, que elimine la dependencia de combustibles fósiles.

**"No podemos seguir profundizando el modelo de la re primarización de la economía como ocurre en Venezuela",** en el que su Producto Interno Bruto depende en gran medida de la explotación de hidrocarburos, dejando de lado el desarrollo e impulso de la agricultura, la industria y  las energías renovables, dijo Santiago.

<iframe id="audio_27865995" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27865995_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
