Title: Tortura y violencia sexual contra mujer privada de la libertad en Jamundí
Date: 2019-09-06 16:24
Author: CtgAdm
Category: Mujer, Nacional
Tags: carceles, Cárceles colombianas, cárceles mujeres, Jamundi, la cárcel de Jamundí, tortura, violencia sexual
Slug: tortura-y-violencia-sexual-contra-mujer-privada-de-la-libertad-en-jamundi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/unnamed.gif" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/unnamed.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div>

<div style="text-align: justify;">

Ante la Procuraduría General de la Nación, la Corporación Humanas, denunció las graves violaciones a derechos humanos cometidas por servidores públicos en contra de una mujer privada de la libertad.

Se trata de la interna Tatiana Chaygen Galvis, quien se encuentra recluida en el Complejo Carcelario y Penitenciario de Jamundí – COJAM, en el Valle del Cauca. De acuerdo con la denuncia, el pasado 26 de agosto, la mujer fue golpeada y gaseada por dragoneante Luisa Velasco durante una requisa frente a la cual puso resistencia.

En el audio donde Galvis relata la situación, también señala que luego "fue llevada a la esclusa y fue atacada por un grupo de guardias -hombres y mujeres- quienes la esposaron de manos y pies, la tumbaron boca abajo y la torturaron durante aproximadamente media hora en la cual no cesaron las agresiones físicas, psicológicas y verbales. Asimismo, manifiesta haber sido víctima de violencia sexual en tanto uno de los guardias realizó tocamientos indebidos con su bota sobre su ropa interior".

Además, el documento radicado ante la Procuraduría devela que dichos tratos inhumanos en contra de la mujer, se generan ante la negligencia de las directivas del centro carcelario, y específicamente la funcionaria delegada para los temas de derechos humanos de la cárcel.

Ante tal situación, la Corporación Humanas exige "las investigaciones disciplinarias respectivas y que se tomen las medidas correctivas aplicables y, de acompañamiento y protección necesarias para salvaguardar la vida, integridad y dignidad de Tatiana Chaygen Galvis, quien se encuentra privada de su libertad en el patio 1A de este centro". De igual forma, solicitan que esta denuncia sea tramitada ante la Fiscalía General de la Nación para que inicie una investigación penal.

Cabe mencionar, que la organización hace un llamado para que cese la violencia estructural que aqueja a las mujeres privadas de la libertad en el COJAM, y recuerda que en el año 2015 dichos tratos crueles generaron la muerte de una reclusa.

</div>

<div style="text-align: justify;">

</div>

<div>

[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/unnamed.gif){.wp-image-73235 .alignleft width="248" height="264"}](https://www.mediafire.com/file/ta1eszw9x4igkm4/AUD-20190826-WA0010..mp4/file)Audio de la denuncia: [  
https://www.mediafire.com/<wbr></wbr>file/ta1eszw9x4igkm4/AUD-<wbr></wbr>20190826-WA0010..mp4/file](https://www.mediafire.com/file/ta1eszw9x4igkm4/AUD-20190826-WA0010..mp4/file)

</div>

<p>
[  
](https://www.mediafire.com/file/ta1eszw9x4igkm4/AUD-20190826-WA0010..mp4/file)

</div>
