Title: Salvador Arana el gobernador que financió grupos paramilitares
Date: 2016-10-19 16:34
Category: DDHH, Nacional
Tags: eudaldo diaz, paramilitares
Slug: corte-suprema-ratifico-que-salvador-arana-si-financio-a-grupos-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/salvador-arana-y-paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [19 de Oct 2016] 

**Corte Suprema de Justicia dejó en firme condena a Salvador Arana Sus, ex gobernador de Sucre, condenado a 40 años por el homicidio de Eudaldo Díaz, alcalde de El Roble** y por el delito de concierto para delinquir luego de que desviara 478 millones de pesos para financiar al Bloque Héroes de los Montes de María, entre los años 2001 y 2002.

El entonces gobernador **uso los recursos de 15 contratos del municipio de Tolú, para que fueran cobrados nuevamente por grupos paramilitares identificados como AUC** bajo el comando de alias “Diego Vecino” con documentación falsa para aparentar legalidad. Lea también:["Águilas Negar amenazan a defensores de DDHH de la Costa Atlántica"](https://archivo.contagioradio.com/aguilas-negras-amenazan-a-defensores-de-ddhh-de-la-costa-atlantica/)

La defensa de Arana había interpuesto un recurso de impugnación en contra del falló que lo condeno a 8 años de prisión por su responsabilidad en el delito de peculado por apropiación a favor de terceros en calidad de coautor. **La Corte señaló que “la sentencia proferida en contra de Arana cobró firmeza en vigencia de la legítima y vinculante interpretación del ordenamiento jurídico”.**

De igual forma**, la Corte estableció que fue Arana quién dio la orden a Rodrigo Mercado Pelufo alias “Cadena” para que planeara y ejecutara el asesinado de Eudaldo Díaz**, alcalde del Roble quién públicamente acuso a Arana de tener nexos y financiar con dineros oficiales a las Autodefensas Unidas de Colombia. Lea también: ["Condena por asesinato de Alcalde del Roble, Eudaldo Díaz "es una burla""](https://archivo.contagioradio.com/condena-por-asesinato-de-alcalde-de-el-roble-eudaldo-diaz-es-una-burla/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
