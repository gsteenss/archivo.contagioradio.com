Title: En lo corrido del 2015 han asesinado a 56 civiles en Putumayo
Date: 2015-06-02 14:10
Category: DDHH, Otra Mirada
Tags: capturas irregulares, conflicto armado en Colombia, Fabiola Castilla, FARC, Fuerza Aérea, fumigaciones con glifosato, Mesa Regional de Organizaciones sociales del departamento del Putumayo, Putumayo, Vereda Teteye
Slug: mesa-regional-de-putumayo-suspende-conversaciones-con-gobierno-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/AP131086780241.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [fotos.starmedia.com]

<iframe src="http://www.ivoox.com/player_ek_4585668_2_1.html?data=lZqll5uafI6ZmKiak5iJd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcbnwpC%2Fx8zNs8%2FVzZDRx5C0udXpzsbm0ZDWs87kxpDQ0dPaqdPnwsjW0dPJt4zX0NOYydTGrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yury Quintero, Mesa Regional de Organizaciones sociales del departamento del Putumayo] 

Este martes, la Mesa Regional de Organizaciones sociales del departamento del Putumayo, Bajo Bora Caucana, y Cofanía Jardínes de Sucumbíos, decidió **suspender los diálogos con el gobierno nacional, debido a los últimos hechos presentados donde se evidencia la incoherencia del gobierno**, al querer continuar en la guerra y así mismo, con las fumigaciones aéreas con glifosato, como lo denuncia Yury Quintero, integrante de la Mesa Regional.

Las comunidades de esta zona del país, exigen que el gobierno demuestre **voluntad política y la construcción de garantías reales**, que generen una solución inmediata a la actual crisis en violación de derechos humanos que afronta la región.

“Es lamentable informar que nos hemos visto obligados a suspender los diálogos, porque tenemos una base de datos sobre múltiples vulneraciones a derechos humanos hacia los pobladores”, afirma Quintero, quien señala que en el 2014, en Putumayo se registraron **163 asesinatos de población civil**, y en lo que va corrido **del 2015 van 56 asesinatos,** entre jóvenes, líderes e indígenas. Así mismo, durante estos **últimos 5 meses se han presentado** **68 capturas irregulares** de personas que hacen parte de procesos organizativos. Todos estos hechos se han cometido en zonas altamente militarizadas y otras petroleras.

La integrante de la mesa regional, aseguró que pese a que los delegados del gobierno que están en las negociaciones con las comunidades, hablan de la necesidad de construir paz, lo cierto es que en la práctica “se vive otra cosa”, expresa, teniendo en cuenta que luego de la suspensión del cese al fuego unilateral por parte de las FARC-EP, ha habido **bombardeos y ametrallamientos directamente hacia la población** por parte de la fuerza pública, por lo que también piden un cese al fuego bilateral.

En la madrugada de este martes, la aviación de la **Fuerza Aérea atentó contra la comunidad de la vereda Teteye** ocasionando graves daños a las viviendas y generando temor a la población. Además desde la base militar Quillacinga se dispararon granadas de morteros que explotaron en fincas y patios de las comunidades Los Ángeles y Buenos Aires, poniendo en riesgo la integridad física y psicológica de los habitantes.

[![11349868\_10153233619980020\_506957204\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11349868_10153233619980020_506957204_n-300x225.jpg){.wp-image-9603 .size-medium .aligncenter width="300" height="225"}](https://archivo.contagioradio.com/mesa-regional-de-putumayo-suspende-conversaciones-con-gobierno-nacional/11349868_10153233619980020_506957204_n/) [![11271999\_10153233621050020\_2008858638\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11271999_10153233621050020_2008858638_n-300x225.jpg){.aligncenter .wp-image-9604 .size-medium width="300" height="225"}](https://archivo.contagioradio.com/mesa-regional-de-putumayo-suspende-conversaciones-con-gobierno-nacional/11271999_10153233621050020_2008858638_n/) [![11215954\_10153233622445020\_124632557\_n (1)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/11215954_10153233622445020_124632557_n-1-300x225.jpg){.aligncenter .wp-image-9605 .size-medium width="300" height="225"}](https://archivo.contagioradio.com/mesa-regional-de-putumayo-suspende-conversaciones-con-gobierno-nacional/11215954_10153233622445020_124632557_n-1/)

**“Hay mucho temor, miedo, los niños y niñas están muy asustados**, y la comunidad en general se siente con rabia e impotencia  por las incoherencias del gobierno nacional”, expresa Yury, quien resalta que Fabiola Castilla, representante del gobierno, respondió que se van a ubicar fechas para realizar una verificación en los lugares donde se han presentado las situaciones violentas.

Finalmente, tampoco se entiende porqué **desde el pasado 5 de Mayo no se han detenido las fumigaciones aéreas con glifosato**, lo que también afecta a los pobladores violando su derecho a una economía propia, teniendo en cuenta que los cultivos fumigados en su mayoría son de pan coger.
