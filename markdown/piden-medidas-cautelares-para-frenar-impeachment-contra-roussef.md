Title: Piden medidas cautelares para frenar Impeachment contra Roussef
Date: 2016-04-14 16:41
Category: El mundo, Otra Mirada
Tags: Cámara de diputados Brasil, Dilma Roussef, Dilma Roussef impeachment
Slug: piden-medidas-cautelares-para-frenar-impeachment-contra-roussef
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/dilma.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [14 Abr 2016] 

Ante el Supremo Tribunal Federal, el gobierno de Brasil presentó este jueves una solicitud de medidas cautelares para anular el impeachment (juicio político) que se adelanta en contra de la mandataria Dilma Rousseff desde diciembre del año anterior, en consideración que el delito que se le imputa no constituye motivo para ser separada de su cargo.

La petición anunciada por el representante legal del gobierno José Cardozo, busca contener la decisión tomada el lunes por parte de la Comisión especial parlamentaria de la Cámara, compuesta por 65 diputados, de abrir el juicio político, acción que deberá pasar a la plenaria del Congreso, encargado de tomar una decisión definitiva.

El debate que inicia este viernes en plenaria de la cámara baja con 513 diputados y se votaría el domingo, requeriría que 342 diputados (2 tercios) para que el proceso continúe y 172 para que la iniciativa sea bloqueada, situación que se complica ante la ruptura de varios de los partidos que integran la coalición de gobierno.

A los votos conseguidos por la derecha opositora al interior de la Cámara, se suman los del Partido Laborista Brasileño (PTB), en el que 15 de los 19 diputados se mostraron a favor del impeachment, permitiendo que los parlamentarios que estén a favor de Rousseff puedan votar contra la iniciativa sin que reciban algún tipo de sanción.

De esta manera el PTB sigue el camino del Partido Progresista (PP), del Partido Republicano Brasileño (PRB) y el Partido Social Democrático (PSD) quienes ya habían abandonado al Gobierno presidido por Roussef, mientras que el  Partido del Movimiento Democrático Brasileño (PMDB) tiene el voto dividido y el PTN dio libertad de elección a sus diputados.

"Trabajaré hasta el último minuto contra este intento de golpe" manifestó en rueda de prensa la presidenta, argumentando que la acusación se basa en un informe que "es un fraude" e insto a la creación de un pacto nacional entre todas las fuerzas políticas de salir avante del proceso, sin "vencedores ni vencidos". (Lea también [Golpe de Estado en Brasil: Mal paga el diablo a quien bien le sirve](https://archivo.contagioradio.com/golpe-de-estado-en-brasil-mal-paga-el-diablo-a-quien-bien-le-sirve/))

De ser aprobada la moción, debe ser enviada al congreso compuesto por 81 miembros quienes tendrían hasta 10 días para tomar la decisión de iniciar o no el juicio por mayoría simple. En ese caso la mandataria debe apartarse del cargo por 180 días para atender el proceso y tendría que ser relevada por el vicepresidente Michel Temer, a quien Rousseff considera parte de la conspiración que desde la derecha se fragua en su contra.

Desde la Articulación Continental de Movimientos sociales hacia el ALBA, se ha convocado a Organizaciones Sociales, grupos y comités de solidaridad para que de manera global salgan este viernes a manifestarse frente a las embajadas de Brasil en apoyo a la mandataria, coincidente con las que se realizarán en diferentes ciudades de ese país.
