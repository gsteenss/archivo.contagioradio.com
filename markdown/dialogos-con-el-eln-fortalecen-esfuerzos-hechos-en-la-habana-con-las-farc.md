Title: Diálogos con el ELN fortalecen  esfuerzos hechos en La Habana con las Farc
Date: 2016-10-11 16:36
Category: Nacional, Paz
Tags: acuerdo de paz, Acuerdo de paz de la habana, Alfredo Molano, análisis, conversaciones de paz con el ELN, ELN, proceso de paz
Slug: dialogos-con-el-eln-fortalecen-esfuerzos-hechos-en-la-habana-con-las-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/DialogosELN-e1476221676766.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País] 

###### [11 de Oct. 2016 ] 

Ante el anuncio del inicio de la fase pública de **diálogos** por parte del gobierno de Colombia y la guerrilla del **ELN** –Ejército de Liberación Nacional – hecha en Venezuela el pasado 10 de Octubre, el analista Alfredo Molano Bravo destacó que ésta noticia es muy positiva para el país. Así mismo, aseguró que hay que revisar cómo se dará la participación de la sociedad civil a través de la propuesta de la mesa social para la paz.

Por su parte, Molano manifestó que tal y como está la agenda, lo que se está haciendo es renovar la aspiración de que las personas participen activamente en este proceso con el **ELN**. Pero aseveró que en éste diálogo “hay que tener bajo control el ritmo que debe tener la mesa en miras a que lo que diga la sociedad civil” y añade “si no lo hacen este proceso podría convertirse en una anarquía y salírsele de las manos”.

A su vez, asegura que las organizaciones sociales que están presentes en las regiones serán las que le darán fuerza y orden a las propuestas que se reciban desde la sociedad civil y manifiesta que “seguramente serán consultadas otras fuentes como sociedades conservadoras que no están vinculadas a ellos”.

Así mismo, Molano asegura que no hay que olvidar el daño que la campaña del No le hizo a la posibilidad de refrendar los acuerdos que se hicieron con la guerrilla de las FARC - EP. Lea también [“Dejar de explicar los acuerdos”, la estrategia del centro democrático.](https://archivo.contagioradio.com/dejar-de-explicar-los-acuerdos-la-estrategia-del-centro-democratico/)

Molano destaca que el Premio Nobel que se otorgó la semana pasada al presidente de Colombia, Juan Manuel Santos es una manera de otorgar desde la comunidad internacional un Sí a la paz. Lea también [Premio Nobel es un “espaldarazo” al proceso de paz](https://archivo.contagioradio.com/premio-nobel-es-un-espaldarazo-al-proceso-de-paz/)

De otro lado, reconoce que la mejor manera de apoyar la consecución de la paz es movilizarse y unirse a los actos que los jóvenes realizan por estos días en el país. Lea también [Se sigue moviendo la iniciativa ciudadana paz a la calle](https://archivo.contagioradio.com/se-sigue-moviendo-la-iniciativa-ciudadana-paz-a-la-calle/).

Concluye diciendo que el momento político por el que atraviesa el país es muy interesante e intenso y aseveró que en el proceso de paz con el ELN puede existir una línea roja en la que podría presentarse el mismo problema en el que se ha visto el proceso de las FARC - EP  "esa línea roja a mi manera de ver es la justicia transicional y el tribunal especial, esa es la más roja de todas, y ahí es donde las cosas no son fáciles y donde Uribe está apuntando más duro" aseveró.

Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio[[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU)

<iframe id="audio_13277213" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13277213_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
