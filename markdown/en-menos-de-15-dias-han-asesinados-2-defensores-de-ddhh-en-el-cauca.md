Title: En 15 días han asesinados 2 defensores de DDHH en el Cauca
Date: 2016-11-16 11:59
Category: DDHH, Nacional
Tags: Asesinatos, Caloto, campesinos, Cauca, Lideres, marcha patriotica, paramilitares, Paramilitares en Cauca, paz
Slug: en-menos-de-15-dias-han-asesinados-2-defensores-de-ddhh-en-el-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/cauca-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Nov. 2016] 

En el corregimiento Huasano en Caloto departamento del Cauca, **en días pasados fue asesinado José Antonio Velasco, campesino, defensor de Derechos Humanos, quien era parte de la Asociación Pro Constitución de Zonas de Reserva Campesina e integrante de Marcha Patriótica,** así lo aseguró la Red de Derechos Humanos Francisco Isaías Cifuentes.

Según Cristian Delgado, vocero de esta organización, cerca de las 6:00 de la tarde, el campesino José Antonio Velasco salió de su hogar rumbo a la casa de su mamá y mientras departían recibió una llamada por la que salió en su moto de regreso a la vereda en donde vivía. Según cuentan testigos, detrás de José salió una camioneta Blanca que minutos después se vio de regreso con rumbo a la entrada de la finca La Josefina.

**José Antonio Velasco, fue encontrado mal herido a las 8:00 p.m. con varios impactos de arma de fuego, en el brazo, en el costado izquierdo y en la cabeza a la altura de la cien** “él alcanzó a decirle a las personas que lo encontraron que quienes le hicieron eso eran 3 personas, que 1 era conocido, que 2 no” contó Delgado.

**El vocero de esta organización, afirmó que la esposa de José Antonio recibió una llamada donde le aseguraban “dígale a su marido que no vuelva a aparecer por la zona, porque la próxima no vamos a responder”**. Infortunadamente, José llegó muy  mal herido a la clínica y, días después se optó por desconectarle la respiración artificial por presentar un cuadro complicado y no tener muchas opciones para garantizar su vida.

Igual que en el caso Jhon Jairo Rodríguez Torres, asesinado el pasado 1 de Noviembre, este nuevo hecho sorprendió a las comunidades y familiares, dado que no existían amenazas en contra de José Antonio. Le puede interesar: [[Asesinado en el Cauca Defensor de DDHH de Marcha Patriótica](https://archivo.contagioradio.com/asesinado-en-el-cauca-defensor-de-ddhh-de-marcha-patriotica/)]

Frente a las investigaciones en los dos asesinatos que han tenido lugar en Caloto Cauca, no hay hasta el momento resultados y **según Delgado las autoridades han atribuido de manera errónea estos hechos a situaciones personales o por temas de microtráfico** “las autoridades no han querido reconocer que son actos de paramilitares, han negado la presencia de estos grupos en la zona, buscan señalar otros factores y aunque se comprometen a adelantar reuniones de alto nivel e investigaciones eso no se concreta” puntualizó.

**En la actualidad, el Movimiento Marcha Patriótica capítulo Cauca, se encuentra en diálogos internos para saber cómo continuar o no en este movimiento político y social dado el alto número de defensores asesinados en los últimos meses.**

“Aunque es una situación que preocupa, seguiremos haciendo la exigencia de la garantía a la vida de nuestros compañeros porque creemos que si se brindan dichas garantías es posible avanzar en el tema de la paz, que tanto anhela el pueblo colombiano” aseveró Delgado.

La ONG Somos Defensores, durante el primer semestre de este año dio a conocer un informe en el que mostraron cifras en las que 35 defensores de derechos humanos fueron asesinados en Colombia y 279 sufrieron algún tipo de agresión, siendo Cauca una de las regiones donde más se registraron asesinatos. En este departamento, en total 5 defensores de Derechos Humanos han sido asesinados por su trabajo en la defensa del territorio.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
