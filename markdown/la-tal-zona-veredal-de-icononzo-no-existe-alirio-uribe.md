Title: La tal Zona Veredal de Icononzo no existe: Alirio Uribe
Date: 2017-02-20 15:06
Category: Entrevistas, Paz
Tags: FARC, paz, Zonas Veredales
Slug: la-tal-zona-veredal-de-icononzo-no-existe-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Zona-veredal-icononzo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alirio Uribe] 

###### [20 Feb. 2017] 

**“Es lamentable el abandono del Estado en las Zonas Veredales Transitorias de Normalización”,** esa es una de las conclusiones de la visita realizada por Alirio Uribe, representante a la Cámara y por el senador Iván Cepeda, a la Zona Veredal de La Fila, ubicada en el municipio de Icononzo, departamento del Tolima, en donde se encuentran 300 guerrilleros entre hombres y mujeres.

**En un primer momento, Uribe destacó el optimismo en el que se mantienen los guerrilleros y guerrilleras** pese las difíciles situaciones que deben afrontar a diario “están cumpliendo con lo acordado, que era desplazarse a estas Zonas Veredales, casi 7 mil guerrilleros. Esa es la parte positiva”. Le puede interesar: ["Las FARC nacieron del pueblo y van a ser pueblo"](https://archivo.contagioradio.com/las-farc-nacieron-del-pueblo-36524/)

**Según Uribe, lo que pudieron constatar es que estas Zonas Veredales no existen** “es decir, desde el punto de vista técnico, logístico, no se está cumpliendo lo que dicen los acuerdos que serían unas zonas campamentarias debidamente adecuadas”.

Y añadió que lo que pudieron ver es que **“hay una finca que fue arrendada por el Gobierno y en donde hay dos pequeñas casas.** Afortunadamente los guerrilleros llegaron con plásticos, con madera, con materiales que trajeron del Yarí, y ellos con sus propias manos están armando esta Zona”. Le puede interesar: [Denuncian incumplimientos en obras de zona campamentaria para las FARC en Caldono](https://archivo.contagioradio.com/36370/)

Además, agregó que **en temas de agua, salubridad y salud la situación es dramática,** dado que aún no existen en las Zonas Veredales los programas de reincorporación y reinserción “lamentamos mucho no ver esa presencia del Estado para cumplir lo que está pactado”.

Por último el cabildante realizó nuevamente un llamado al Gobierno para que haga presencia en esta y en todas las Zonas Veredales de manera efectiva, con la infraestructura que se requiere, así como con los mecanismos de verificación.

**“La verdad es que uno llega a una Zona que literalmente no existe. No hay ninguna condición para que se cumpla por lo menos lo que uno lee en los acuerdos** en cuanto a las Zonas Veredales” recalcó Uribe.

**Los calendarios se corrieron**

Para Alirio Uribe, contrario a lo que ha manifestado el Gobierno nacional y el gerente de la Zonas Veredales en cuanto al calendario del proceso de paz, éste ya está corrido. Le puede interesar: [ONU propone "recalendarizar" dejación de armas de FARC-EP](https://archivo.contagioradio.com/onu-propone-modificacion-de-cronograma-por-falta-en-adecuacion-de-zonas-veredales/)

**“Aquí ya no hablamos de lo que quiere el Gobierno o lo que quieren las FARC, yo creo que los plazos están corridos** precisamente por la falta de instalación formal de estas Zonas, y por todos los procesos que hay que hacer en el marco de la dejación de armas”.

**Insistir en la implementación ya**

El representante a la Cámara realizó la invitación a organizaciones sociales, estudiantiles, académicos, empresarios y víctimas a **animarse a ayudar a las personas que están en las Zonas Veredales, porque según él “los acuerdos de paz se deben implementar por la sociedad en su conjunto”. **Le puede interesar: [Inicia campaña de donaciones para guerrilleros de las Zonas Veredales](https://archivo.contagioradio.com/inicia-campana-de-donaciones-para-guerrilleros-de-las-zonas-veredales/)

Y agregó “es importante ir a estas Zonas, conocerlas y mirar que necesitan. Yo creo que necesitan capacitación, formación y ellos lo van a hacer con o sin el Estado. Sería lógico que el Estado garantizará todo pero sino los guerrilleros lo harán con sus propias manos”.

<iframe id="audio_17115404" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17115404_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}
