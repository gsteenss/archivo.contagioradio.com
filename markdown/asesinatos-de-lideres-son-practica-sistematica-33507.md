Title: Asesinatos de líderes sociales son práctica sistemática: Somos Defensores
Date: 2016-12-09 16:05
Category: Nacional, Política
Tags: Fiscal, Fiscal Nestor Humberto, Fiscalía, Somos defensores
Slug: asesinatos-de-lideres-son-practica-sistematica-33507
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/nestor_martinez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ykaly] 

###### [9 Dic. 2016] 

El miércoles 7 de Diciembre se llevó a cabo en el Congreso una audiencia pública por los asesinatos y ataques a líderes sociales, titulada *no más crímenes contra la paz,* en la cual entre otras entidades fue invitada la Fiscalía General de la Nación, en cabeza del fiscal Néstor Humberto Martínez.

**La participación del actual Fiscal suscito diversas posiciones desde los movimientos sociales y organizaciones sociales presentes en el lugar, dado que según él “no se advierte ningún grado de sistematicidad que nos permita establecer certeramente hasta hoy que hay una mano negra, invisible que está afectando a los líderes de derechos humanos”**

**Por su parte, Carlos Guevara de Somos Defensores aseguró que “lo que pasa es que al fiscal le hace falta leer un poquito más nuestros informes de años anteriores para poder decir que esto no es sistemático”.**

Según Guevara no se puede hablar de que lo que está sucediendo no es sistemático cuando en los últimos 5 años han sido asesinados por año al menos 50 defensores de derechos humanos **“no podemos hablar de que es algo asintomático, por supuesto que tienen un fondo y ese fondo comenzó en septiembre de 2014 cuando las víctimas comienzan a ir a La  Habana”.**

Guevara es enfático en manifestar que luego de las visitas de las víctimas hicieron a La Habana se han amenazado a 1000 defensores de derechos humanos y **“la Fiscalía no ha hecho nada”.**

Según Somos Defensores a la Fiscalía “le falta ver para atrás, lo que pasa es que también para la Fiscalía es y ha sido muy difícil reconocer que en este país existen defensores de derechos humanos, líderes sociales (…) y **cada vez que hay una asesinato de estas personas, comienzan las investigaciones con tesis como: lo mataron por estar en una zona donde hay minería o microtráfico o que eran crímenes pasionales”** dijo Guevara. Le puede interesar: [Fiscalía ha suspendido 35 mil investigaciones sobre desaparición forzada](https://archivo.contagioradio.com/fiscalia-ha-suspendido-35-mil-investigaciones-sobre-desaparicion-forzada/)

Para esta organización la situación en la que se encuentra la Fiscalía es preocupante dado que “**le entregaron la entidad a una persona que viene del sector privado y que este tema de lo social y de lo popular le parece superficial y no hay una intención clara de investigar.** (…) este fiscal lo único que le importa son los crímenes que tienen algún tipo de relación  con empresas privadas” asevero Guevara.

En cuanto a la Fiscalía en el postconflicto, según Somos Defensores dicha entidad no “tiene la capacidad técnica de poder responder ni a los crímenes contra defensores ni mucho menos al acuerdo. Aunque se ha preparado la vemos muy biche para enfrentarse a esa etapa y para afrontar todo el tema de verdad que se nos viene” agregó Guevara.

Y Guevara concluye diciendo que las puertas al diálogo están abiertas para construir “estamos absolutamente dispuestos a entregar información detallada, a construir de manera conjunta, el problema es que el fiscal que tenemos ahora es una persona que tiene otras maneras y el tema social  no es una cosa que le llegue mucho al corazón”. Le puede interesar: [Fiscalía niega asesinatos sistemáticos contra líderes y defensores de DDHH](https://archivo.contagioradio.com/fiscalia-niega-asesinatos-sistematicos-en-territorios/)

<iframe id="audio_14809455" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14809455_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
