Title: Demandan decreto que aprobó el aumento del salario mínimo
Date: 2016-01-22 12:19
Category: Economía, Nacional
Tags: CUT, demanda contra salario minimo, salario minimo
Slug: aumento-del-salario-minimo-es-inconstitucional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Salario-mínimo-2016.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Nolasco Présiga. ] 

<iframe src="http://www.ivoox.com/player_ek_10166419_2_1.html?data=kpWemJuYdZqhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLpzsrb1tSPqMbgjNjOzsbWrdCfzoqwlYqliM%2FdztSYx9iPrc%2FX0NPg1s7YucTd0NPOzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Alejandro Pedraza, CUT] 

###### [22 Ene 2016. ] 

[Este jueves la Central Unitaria de Trabajadores CUT, interpuso ante el Consejo de Estado una **demanda contra el decreto que aprobó el aumento del salario mínimo**. De acuerdo con Luis Alejandro Pedraza, presidente de la CUT, esta medida es inconstitucional por varias razones. **Mientras el salario aumentó 7%, los precios de la canasta familiar incrementaron 24%**; así mismo, el alza estuvo por debajo de la inflación establecida para los salarios de bajos ingresos, que es del orden del 7.26%, y finalmente los planteamientos hechos para la recuperación del poder adquisitivo, no fueron tenidos en cuenta en la Comisión de Concertación.]

[Según Pedraza, lo que hizo el Gobierno fue acoger determinaciones de la OCDE y el FMI para “imponer **una decisión contraria a la ley y al derecho de los trabajadores a tener salarios que les permitan acceder plenamente a la canasta familiar**, que es lo que la Corte determina como el ingreso básico para la subsistencia digna”, pues **mientras la canasta familiar cuesta \$1.800.000, el salario mínimo está en \$689.000**. Un hecho que agudiza la pérdida de poder adquisitivo en la población colombiana que ya ha decrecido en un 50%.   ]

[El Consejo de Estado cuenta con 30 días para evaluar sí suspende el decreto y se adelantan nuevos estudios para determinar un alza justa y significativa en el salario mínimo, asegura Pedraza, e insiste en que este **incremento debe tener en cuenta tanto la inflación como la pérdida de poder adquisitivo de la población**, en esa medida debe ser por el orden del 12%, más un porcentaje adicional que permita mantener la capacidad adquisitiva, tal como lo habían planteado las agremiaciones sindicales en la Comisión de Concertación, pues **con el actual modelo “se incrementa la franja de pobres en Colombia y se fortalece la acumulación de riqueza de unos pocos”**.]

[Entretanto distintas organizaciones sociales y gremios sindicales sostienen reuniones para **concertar la propuesta de un paro cívico nacional en el que se presentaran demandas especificas al Gobierno** exigiendo que sean cumplidas y no que pase lo mismo que ha venido sucediendo tras las movilizaciones de distintos sectores con quienes presidencia ha pactado **acuerdos que terminan siendo incumplidos**. “El Gobierno actúa así simplemente porque no pasa del debate en los medios de comunicación o del berrinche de quienes finalmente resultamos afectados”, asevera Pedraza e insiste en que se debe **“hacer un frente común para hacerle saber al gobierno que no puede seguir imponiendo decisiones con el silencio del pueblo colombiano”**.]

[![infografia salario](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/infografia-salario.jpg){.aligncenter .size-full .wp-image-19579 width="716" height="767"}](https://archivo.contagioradio.com/aumento-del-salario-minimo-es-inconstitucional/infografia-salario/)

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio ](http://bit.ly/1ICYhVU)] 
