Title: Persisten las amenazas contra el proceso restitución de tierras
Date: 2016-04-14 18:43
Category: Nacional
Tags: corporación yira castro, Magdalena Medio, Paramilitarismo
Slug: persisten-las-amenaza-contra-el-proceso-restitucion-de-tierras
Status: published

###### [Foto: Contagio Radio ] 

###### [14 Abril 2016]

De acuerdo con la Corporación Jurídica Yira Castro, se ha puesto en marcha una campaña de **estigmatización contra los campesinos reclamantes de tierras en el departamento de Magdalena**, principalmente de las familias de los municipios de Chibolo, Sabanas de San Ángel y Plato, a quienes esta organización ha acompañado en la lucha por la restitución de los predios que fueron arrebatados por grupos paramilitares al mando de Jorge 40, hecho documentado en expedientes judiciales y confesado por jefes paramilitares.

El abogado Francisco Henao, integrante de la Corporación, afirma que en estos municipios los **procesos de restitución de tierras continuamente son obstaculizados, por estructuras paramilitares**, como el 'Ejército Antirestitución' y mandos medios de las 'Águilas Negras' que amenazan a quienes solicitan la restitución de las más de 10.000 hectáreas que fueron despojadas a campesinos en el Magdalena, a inicios de la década del 2000.

Henao asegura que estos paramilitares han promovido diferentes reuniones con quienes se oponen a la aplicación de la Ley 1448 de 2011, entre ellos empresarios, terratenientes, ganaderos y palmicultores. Uno de los encuentros se llevó a cabo el 4 de febrero de 2012 en Valledupar, en él participaron **terratenientes y funcionarios públicos del Cesar que buscaban apoyo económico para el grupo armado anti-restitución**, según refiere la 'Corporación Nuevo Arco Iris'.

De acuerdo con el abogado, 'Sabanas de San Angel', 'Chibolo' y 'Plato', fueron epicentro paramilitar para la consolidación de diferentes bases de operaciones de Jorge 40, quien concreto el pacto parapolítico de Chibolo el 28 de septiembre del 2000, lo que conllevo el **despojo de tierras para mas de mil familias campesinas**, quienes fueron presionadas para ceder o vender sus predios en \$250.000 por hectárea.

Tras el pacto, se desencadeno una furia mediática y política que generó un proceso de desmovilización paramilitar en el Magdalena Medio en el 2006, pero que según Henao, "no fue del todo cierto", pues en esta región aún **"persisten estructuras paramilitares que regulan procesos políticos y económicos, al igual que en ese entonces"** y que son los que están promoviendo reuniones para oponerse a la restitución de tierras, entre otras porque los mandos medios paramilitares no han sido investigados por la Fiscalía.

Entre quienes más se han opuesto al actual proceso de restitución de tierras figuran el actual Procurador Alejandro Ordóñez, la Representante del Centro Democrático María Fernanda Cabal, el actual presidente de Fedegan José Félix Lafaurie, y algunas industrias como Argos S.A, Continental Gold Limited Sucursal Colombia, Exploraciones Chocó Colombia S.A.S, Anglo gold Ashanti Colombia S.A, [[entre otras](https://archivo.contagioradio.com/las-11-empresas-que-se-oponen-a-la-restitucion-de-tierras-segun-estudio-de-forjando-futuros/)], quienes aseguran que la restitución no incluye a los propietarios y terratenientes que adquirieron estos predios en buena fe.

Por su parte Lafaurie afirmó en la llamada audiencia pública contra la Ley de Restitución, realizada en Sabanas de San Ángel el pasado 5 de abril, que "la restitución no es otra cosa sino la cuota inicial del control territorial de ciertos actores armados que creen que pueden volver a hacer de las suyas en el campo y no se los vamos a permitir", lo que de acuerdo con la Corporación "pone en alto riesgo a las comunidades, a las organizaciones de derechos humanos que las acompañan e incluso a las entidades públicas encargadas de adelantar la política de restitución de tierras".

"Es importante señalar, que la buena fe la tienen que probar ante los jueces, y tiene que ser una buena fe exenta de culpa", asevera Henao y agrega que la venta y compra de tierras a través del desplazamiento de más de mil familias del Magdalena Medio, "recae en la ilegalidad", pues fue una **estrategia para adquirir y legitimar predios despojados por paramilitares durante 1996 y 2005**, por medio de testaferros, como Augusto Castro, testaferro de Jorge 40, quien se encuentra en arresto domiciliario.

El abogado concluye aseverando que quienes que fueron violentados y despojados de sus tierras por grupos paramilitares, deben recuperar lo que les pertenecía legítimamente, la **buena fe que argumentan quienes actualmente ocupan sus propiedades debe ser probada ante la corte**, teniendo en cuenta que la violencia ha llevado al desplazamiento de muchos campesinos que se asentaron en tierras baldías.

Por esta serie de estigmatizaciones la Corporación emite el siguiente comunicado:

[Denuncia Por Amenazas](https://es.scribd.com/doc/308698706/Denuncia-Por-Amenazas "View Denuncia Por Amenazas on Scribd")

<iframe id="doc_98395" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/308698706/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
