Title: Gobierno hace oídos sordos al paro en Chocó
Date: 2017-05-12 16:17
Category: Entrevistas, Movilización
Tags: Comunidades del Chocó, Paro Chocó
Slug: gobierno-hace-oidos-sordos-al-paro-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/quibdo-manifestacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Silla Vacía] 

###### [12 May 2017]

Se completan tres días del paro cívico que adelantan las comunidades del Chocó, **sin que el gobierno o alguna institución establezca una mesa de diálogo con los habitantes** que exigen que se cumplan los acuerdos de Agosto de 2016 y garantías por parte de las autoridades para el desarrollo de la protesta pacífica.

“Directamente no hemos recibido ninguna respuesta o comunicación desde el Gobierno Nacional, **de parte nuestra siempre ha estado la disposición de sentarnos** sobre la base de que estén los plenipotenciarios debidamente autorizados por el gobierno y que vengan con la intensión de resolver los requerimientos del Chocó” señaló Emilio Pertúz, intengrante del comité de interlocución del paro.

Frente a la quema de recibos de la luz como mecanismo de protesta y la explosión de un petardo en una de las calles de Quibdó, Pertuz explicó que muchas personas se indignaron con el aumento de las tarifas, sin embargo se controló la situación. Al mismo tiempo **rechazó las acciones de violencia y aseguró que la explosión del petardo no obedece a la tradición de protesta pacífica de los chocoanos** y exigió una investigación para frenar la estigmatizción.

### **Un paro también es una forma de hacer pedagogía y consolidar la unidad** 

Así lo demuestran las divdersas actividades que desarrllan los líderes de la protesta. Por ejemplo: **10 y 11 de mayo fueron de movilizaciones y cierre del comercio y el 12 se realizó una concentración en el parque Centenario**, en Quibdó, con la finalidad de hacer pedagogía con la ciudadanía sobre el pliego de exigencias pactado el año pasado y el comercio estará activo hasta el próximo lunes.

Municipios como Bahía Solano, Nuqui, San Juan, Acandí y Río Sucio, se han unido al paro y se espera que para este fin de semana, **se realicen rutas de visita a barrios y más regiones que permitan reunir un pliego de exigencias más fuerte** y concreto sobre las necesidades del departamento. Le puede interesar:["Comunidades responsabilizan a Santos por incumplimientos en el Chocó"](https://archivo.contagioradio.com/el-presidente-santos-es-el-responsable-de-los-compromisos-asumidos-e-incumplidos-en-el-choco-emilio-pertuz/)

<iframe id="audio_18658336" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18658336_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
