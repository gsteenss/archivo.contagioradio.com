Title: Exfuncionario de la Fiscalía que puso en jaque al paramilitarismo denuncia tentativa de secuestro
Date: 2020-09-21 18:13
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Antioquia, Autodefensas Unidas de Colombia, Fiscalía general, La Terraza
Slug: exfuncionario-fiscalia-puso-en-jaque-al-paramilitarismo-denuncia-tentativa-secuestro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Paramilitarismo / Na-24.net

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Gregorio Oviedo Oviedo, **exfuncionario de la Fiscalía General de la Nación** desde septiembre de 2014 denuncia que fue víctima de una tentativa de secuestro en su contra. Cabe resaltar que Oviedo tuvo a su cargo el operativo del "Parqueadero Padilla" en Medellín, intervención que expuso la forma en que se financiaban en aquel entonces las **Autodefensas Unidas de Córdoba y Urabá (ACCU)**. En el pasado, el exfiscal además fue víctima de tortura por parte **miembros de la inteligencia militar** y declarado objetivo militar por grupos paramilitares.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Según relata Oviedo, el pasado 16 de septiembre, sobre el medio día fue "objeto de una tentativa de retención ilegal** cuando llegaba a mi sitio de residencia por parte de dos sujetos que se movilizaban en un automóvil marca *Skoda*, cuatro puertas", agrega que la persona que intentó hacerlo entrar al vehículo "era un hombre bastante joven, tez blanca y estatura aproximada de 1,75 mts, el conductor, quien no bajó en ningún momento del carro, era una persona un tanto obesa, tez trigueña y con visera".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El exfuncionario de la Fiscalía resalta que desde finales de octubre de 1997 hasta junio de 1998 fue la persona que dirigió el operativo conocido como “El Parqueadero Padilla" que tuvo lugar el 30 de abril de 1998 y que dio a conocer cómo se financiaban en aquel entonces las **Autodefensas Unidas de Córdoba y Urabá (ACCU) y que pasaron a ser Autodefensas Unidas de Colombia (AUC).**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicho allanamiento permitió conocer todo acerca de la contabilidad de las autodefensas desde 1994, **incluyendo disquetes, libros de contabilidad y organigramas de sus estructuras** que evidenciaban los movimientos contables, aportantes, extractos bancarios, comunicaciones sobre operativos militares y nombres de personas que podrían hacer parte del paramilitarismo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese al hallazgo que pudo haber sido clave para desmantelar el paramilitarismo en el país, organizaciones como el [Instituto Popular de Capacitación (IPC)](https://twitter.com/IPCoficial)y la Corporación Jurídica Libertad **han coincidido en que dicha investigación terminó sin consecuencias judiciales.** [(Lea también: Mancuso: "Colombia no conoce la verdad, porque no hubo ni existe interés político")](https://archivo.contagioradio.com/mancuso-colombia-no-conoce-la-verdad-porque-no-hubo-ni-existe-interes-politico/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Antecedentes de perfilamientos durante su paso por la Fiscalía

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que como consecuencia de dicha investigación, **fueron asesinados 14 investigadores del CTI de Medellín** - casos que menciona, continúan en la impunidad - y que según relata, hacían parte de una lista de funcionarios de la Fiscalía declarados objetivo militar, lista que, se decía, era manejada por la banda de sicarios conocida como La Terraza. Oviedo agrega que aunque era la persona que encabezaba la lista, **fue el único sobreviviente**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, relata que el 1 de febrero de 1980, cuando trabajaba como empleado del Juzgado 24 Superior de Bogotá, **"fui capturado por miembros de la inteligencia militar (B2) al amparo del decreto 1923 de 1978, más conocido como El Estatuto de Seguridad, que profirió el presidente Julio Cesar Turbay Ayala"** donde expresa, fue sometido a torturas en la Escuela de Artillería, sur de la ciudad, y luego en las caballerizas de Usaquén donde afirma, fue privado de la libertad durante aproximadamente un mes en instalaciones militares.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Blanco del paramilitarismo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque agrega que fue liberado y el proceso que se siguió en su contra fue objeto de cesación de procedimiento, nuevamente en agosto de 1999 cuando fungía como Fiscal Delegado en el entonces Tribunal Nacional, su nombre apareció en una lista elaborada, por lo que señala "el movimiento paramilitar, con 20 personas mas declaradas objetivo militar, lista que fue difundida y distribuida en todo el país" y que era encabezaba por el sociólogo Alfredo Molano Bravo. [(Le recomendamos leer: Alfredo Molano un caminante de la verdad)](https://archivo.contagioradio.com/alfredo-molano-un-caminante-de-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Resalta que de tal lista fueron asesinados dos congresistas que en ella aparecían, **Jairo Rojas Pulido, parte de la Comisión de Paz de la Cámara de Representantes en septiembre de 2001 y Alfredo Colmenares Chía, quien fue también gobernador del Arauca y representante a la Cámara por el mismo departamento asesinado en octubre del mismo año.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma en agosto de 2000, agrega que el entonces **fiscal general, Luis Camilo Osorio Isaza** le declaró insubsistente en el cargo de fiscal delegado, situación que habría agudizado su situación de inseguridad lo que lo lleva al exilio junto a su familia, abandonando el país desde hace ocho años, pee a ello, afirma la revictimización en su contra ha continuado con el paso del tiempo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La denuncia se da en medio de un contexto en el que se han conocido perfilamientos y seguimientos por parte del Ejército Nacional contra periodistas, líderes políticos, organizaciones sociales e incluso funcionarios de las altas cortes.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
