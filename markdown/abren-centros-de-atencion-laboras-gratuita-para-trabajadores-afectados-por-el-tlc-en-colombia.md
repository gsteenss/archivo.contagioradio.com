Title: Abren Centros de Atención Laboral gratuita para trabajadores afectados por el TLC
Date: 2020-03-04 17:59
Author: CtgAdm
Category: Actualidad
Slug: abren-centros-de-atencion-laboras-gratuita-para-trabajadores-afectados-por-el-tlc-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Juan-Pablo-Marín-García-Cosecha-de-mujeres-scaled.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Luis-Fernando-Echeverría-Aguilar-La-Amarga-Cosecha-Guatemala-2015-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto:* Luis Fernando Echeverría Aguilar - La Amarga Cosecha

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/dan-hawkins-sobre-centros-atencion-laboral-gratuita_md_48575080_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Dan Hawkins | Director de los Centros de Atención Laboral

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

La[Escuela Nacional Sindical](http://www.ens.org.co/) realizó esta semana la apertura de dos de sus seis Centros de Atención Laboral (CAL); espacios donde trabajadores de 5 sectores extractivos impactados por el Tratado de Libre Comercio entre Estados Unidos y Colombia, recibirán asesoría jurídica y psicosocial gratuita.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cerca de 9.000 trabajadores de **sectores como flores, minería, caña, puertos y palma serán acogidos en los CAL** ubicados en las ciudades de **Cartagena, Puerto Wilches, Cali, Facatativá, Villavicencio y Valledupar.** (Le puede interesar: <https://archivo.contagioradio.com/colombia-el-segundo-pais-mas-peligroso-para-defensores-que-trabajan-temas-empresariales/> )

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según **Dan Hawkins, Director de los Centros de Atención Laboral** este espacio fue implementado por la Escuela hace 15 años en las ciudades de Bogotá y Medellín, *"**la nueva fase del proyecto tendrá un enfoque mucho más regional, atendiendo la violación a los derechos laborales de colombianos afectados por el plan de acción laboral Obama - Santos,** firmado  en abril de 2011".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y aclaró que el objetivo de estos espacios será dialogar con los trabajadores, y así *"**lograr que ellos entiendan mucho más sus derechos**, mediante asesoría que den un alto a las vulneraciones y permitiendo que sus denuncias lleguen a entidades que realmente hagan la vigilancia y control de los procedimientos".* 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hawkins afirmó también que estos 5 sectores fueron seleccionados por su trascendencia  histórica de violaciones sistemáticas, *" expertos en DDHH de Estados Unidos y Colombia apoyados por múltiples estudios territoriales permitieron destacar que **estos trabajadores han padecido de intimidación laboral, discriminación sindical, violencia, acoso, además de condiciones laborales muy precarias que afectaron su salud**"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último destacó que los trabajadores que deseen esta atención pueden llegar directamente a la puerta de los CAL en las diferentes ciudades, y agregó, *"**vamos a ofrecer el servicio móvil, que corresponde a brigadas de atención con abogados, trabajadores sociales y psicólogos que irán a los lugares donde ellos viven o trabajan** para ofrecerles la atención gratuita a un mayor número de personas"* .   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Actualmente los Centros de Atención Laboral **abiertos, son Facatativá, Cartagena y Puerto Wilches**, se espera que al finalizar el mes de abril los 6 puntos de atención estén habilitados para acoger a los trabajadores de todo el país afectados por las actividades extractivistas. (Le puede interesar: <https://archivo.contagioradio.com/la-aspersion-aerea-con-glifosato-no-es-una-estrategia-efectiva/> )

<!-- /wp:paragraph -->
