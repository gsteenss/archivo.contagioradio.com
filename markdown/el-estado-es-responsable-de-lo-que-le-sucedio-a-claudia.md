Title: “El Estado es responsable del feminicidio de Claudia”
Date: 2017-04-11 12:45
Category: Libertades Sonoras, Mujer
Tags: asesinato, Claudia Rodríguez, feminicidios, mujeres
Slug: el-estado-es-responsable-de-lo-que-le-sucedio-a-claudia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Feminicidio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [11 Abr. 2017] 

**Sandra Mazo, directora de la organización católicas por el Derecho a Decidir**, manifestó que "el Estado es responsable del feminicidio de Claudia" a propósito del reciente caso sucedido este lunes en Bogotá, capital de Colombia, que cobró la vida de Claudia Johana Rodríguez de 40 años de edad a manos de su expareja sentimental de 42 años.

**"Estamos llenas de rabia, de dolor, por lo que le pasó a Claudia Johana.** Nos muestra la desidia, la dejadez, el olvido y la desprotección en la que están las mujeres (…) increíble que un país como este, hiperlesionado y con tantas normas todavía sigan pasando cosas tan absurdas como estas " añadió Mazo con una voz entrecortada.

Los hechos que conmocionaron a la capital, sucedieron al interior de un centro comercial, lugar en el que trabajaba Claudia y en el que seguramente pensó no estaría en peligro de ser violentada por su expareja. Claudia contaba con protección en su lugar de vivienda para ella y su hijo, resultado de esa relación, sin embargo, **las acciones de laPolicía no fueron suficientes para garantizar la vida de esta mujer.**

**"El Estado es el responsable de lo que le sucedió a Claudia, ella había denunciado.** La responsabilidad es total. Ella tenía que estar protegida, hay omisión del Estado. No se puede condenar a Claudia, aquí había una denuncia. Nos seguimos preguntando cuántas muertes de mujeres necesita este país para que el Estado se dé cuenta que hay que protegernos" dijo Mazo. Le puede interesar: [92 feminicidios en América Latina han sido registrados en menos de 2 meses](https://archivo.contagioradio.com/92-feminicidios-en-america-latina-han-sido-registrados-en-menos-de-2-meses/)

Para la directora de esta organización, es muy triste seguir documentando las dolorosas cifras de mujeres asesinadas sin que se haga nada **“estamos en un sistema patriarcal.** Mientras la nación y los medios no entiendan que la mujer no es un objeto esto no cambiará, está sociedad tiene que entender que hay que construirse de una forma diferente".

Para Mazo, estas violencias son estructurales y mientras no se hable de otras maneras y se construya por ejemplo desde la cultura, la situación seguirá igual y cita **casos como el del cantante de reggaetón Maluma y la condecoración dada por el Gobernador de Antioquia.**

“El lugar que le ponen a las mujeres y que se esté premiando artistas que incentivan la violencia contra las mujeres, todo eso tiene que dolernos e indignarnos porque es una manera de reproducir la violencia hacia nosotras, de cosificar a las mujeres y de justificar las violencias**. A eso hay que ponerle mucho ojo. Ojalá se acabe la indolencia en este país ante la muerte”** recalcó Mazo.

### **¿Qué hacer ante los feminicidios?** 

Aunque dice que no es una pregunta que tenga una respuesta sencilla, Mazo manifiesta que hay que dejar de aplaudir y de reproducir actividades culturales que son denigrantes “porque por mas que sean culturales no quiere decir que las legitimemos y las premiemos, las culturas pueden cambiar”. Le puede interesar: [El feminicidio de Micaela García moviliza Argentina](https://archivo.contagioradio.com/39025/)

Para la defensora de los derechos de las mujeres **hay que seguir trabajando por indignar a la gente** “para mi hay que hacer un movimiento para dejar de escuchar a Maluma, hay que condenar a los reggaetoneros y a todas aquellas expresiones que se llaman culturales que están vulnerando la condición de las mujeres y de las personas”. Le puede interesar:[ ¿La música reproduce el machismo?](https://archivo.contagioradio.com/la-musica-reproduce-el-machismo/)

Y concluye diciendo que también **hay que unir esfuerzos y trabajar por una sociedad más solidaria, menos indiferente** “cuando vean que a una mujer la están agrediendo, la estén vulnerando en sus derechos hay que hacer cadenas de afectos, para que no seamos indiferentes a lo que le está pasando al otro o a la otra y reacciones y condenemos socialmente al violador, al represor. **Hace falta solidaridad en este país para no sentirnos solas en una ciudad o campo que habitamos con miedo y dolor”. **Le puede interesar: [Los retos del 2017 para enfrentar la violencia contra las mujeres en Colombir](https://archivo.contagioradio.com/los-retos-del-2017-enfrentar-la-violencia-las-mujeres-colombia/)

<iframe id="audio_18091523" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18091523_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
