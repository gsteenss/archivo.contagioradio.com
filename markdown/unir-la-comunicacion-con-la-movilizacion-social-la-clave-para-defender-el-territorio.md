Title: Unir la comunicación con la movilización social: la clave para defender el territorio
Date: 2019-05-31 13:30
Author: CtgAdm
Category: DDHH, Educación
Tags: comunicación alternativa, defensa del territorio, Latinoamérica
Slug: unir-la-comunicacion-con-la-movilizacion-social-la-clave-para-defender-el-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Comunicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

En el marco del **Encuentro Andino de  Defensa del Territorio, Comunicación y Movimiento Social**, comunicadores y periodistas de **Colombia, Perú, Bolivia, Ecuador y Guatemala** se congregaron en Popayán para vincular el movimiento social con el periodismo y trabajar en defensa del territorio, a través de nuevas narrativas y herramientas que pueden ser aplicadas desde la comunicación.

Durante cinco días los participantes de este evento trabajaron en el Cauca, realizando procesos de formación, intercambios de experiencias y construcción de historias, según el representante para Colombia de Brotherleague, algunas organizaciones sociales han expresado que deben posesionar más su lucha en la defensa de los derechos, trabajar en propuestas ante nuevos públicos y buscar llegar a nuevas audiencias. [(Lea también: Comunicación para la paz: un desafío aún pendiente)](https://archivo.contagioradio.com/comunicacion-paz-colombia/)

**Gloria Velasco, comunicadora de  Cusco, Perú** señaló que ha llegado a este encuentro esperando compartir experiencias, generar nuevas alianzas y conocer nuevas herramientas que le permitan defender el territorio latinoamericano, "nuestro contexto actual es muy complicado, ahora si defiende el agua y el ambiente, es sinónimo de terrorismo en tu país, nuestras luchas deben ser una, Latinoamerica es una" resaltó.

La comunicadora afirmó que **la forma de visibilizar lo que pasa en los países andinos es a través de las nuevas tecnologías y generando nuevos espacios de comunicación** y nuevos medios alternativos que confronten a los tradicionales.

<iframe id="audio_36776113" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36776113_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
