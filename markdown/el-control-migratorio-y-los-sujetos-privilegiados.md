Title: El control migratorio y los sujetos privilegiados
Date: 2018-05-28 11:41
Category: Eleuterio, Opinion
Tags: Europa, migración, migraciones
Slug: el-control-migratorio-y-los-sujetos-privilegiados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/migrantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Panarria 

#### **Por [Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

#### 28  May 2018 

[Cuenta la leyenda que las columnas de Hércules que aparecen en el escudo español se situaban en el estrecho de Gibraltar, fijando los límites de los territorios españoles bajo el lema “Non plus ultra”. Con la llegada de Colón a América, el imperio español se expande y a estas columnas les acompaña desde entonces el nuevo lema “Plus ultra” (más allá). La primera página de un pasaporte español recuerda también este hecho con el dibujo de las tres famosas carabelas y un mapa con los viajes del no menos famoso almirante. En la misma lógica, la fiesta nacional es desde la Transición el 12 de octubre. Existen 14 monumentos a Cristóbal Colón de construcción reciente en territorio español. Uno de ellos en Barcelona, representa la figura de un indígena arrodillado ante un cura.]

[En Barcelona vive desde hace más de 9 años]**Daniela Ortiz**[, artista y activista social peruana. Daniela nos habla sobre sus investigaciones acerca del control migratorio europeo y sus reflexiones en torno a los sujetos privilegiados. “]*[La identidad nacional española se articula desde el relato del imperialismo y la colonia. Ahora no hablamos de colonialismo que implica una ocupación militar sino de colonialidad, un sistema que mantiene los privilegios de unos pueblos sobre sus excolonias.”]*[Desde que en 1985 entrara en vigor en el estado español]**la ley de extranjería**[, por una exigencia de la UE para poder ser parte de Europa, la población migrante que ya vivía en España pasa a ser ilegal de manera inmediata. El discurso también cambia, ya no se habla de latinos, árabes, africanos sino que se generaliza y encasilla con el término inmigrante. Hubo mucha resistencia contra la ley y mucha represión, incluso una muerte. Esta ley que empieza a ponerse en cuestión ahora, 30 años después. “]*[La gran mayoría de las personas migrantes venimos de excolonias europeas que han sido saqueadas y violentadas durante siglos. El mismo proceso de industrialización en Europa se desarrolla a partir del expolio y la esclavitud en las colonias. Sin embargo, a pesar de que hay elementos culturales que tenemos por la tradición colonial, como la lengua, el castellano en mi caso, no son reconocidos como suficientes para estar integrados.”]*

[Otro caso de actualidad es el de la situación de]**las personas refugiadas**[. Estas sufren una doble situación de agravio, por un lado la represión del sistema de control del Estado y por otra el victimismo con que se les etiqueta desde los medios de comunicación. “]*[Se les pregunta por sus dificultades, sus traumas, sus carencias. Por el contrario a los activistas blancos sí se les pregunta por cuestiones políticas. La izquierda europea blanca acoge, ayuda, asiste, no empodera, no trata de igual a igual como ser humano a los migrantes. Además se habla con naturalidad de repartir a la población refugiada por Europa, lo que implica que no son ellos los que deciden dónde quieren vivir, donde quieren estar. La izquierda habla de los migrantes pero no a los migrantes, ofrecen sus derechos a sus votantes.”]*[ Daniela termina con una reflexión sobre la actitud de la izquierda en Europa sobre la situación de represión que sufren las personas migrantes. ]*[“No hay una crítica desde la izquierda europea a las ONGs, a la industria de los derechos humanos. Desde Latinoamérica, Oriente Medio, África se habla claramente del papel de explotación que las ONGs cumplen en sus territorios. Hay contradicciones y es necesario evidenciarlas y ponerlas sobre la mesa. No quiero juzgar a nivel individual la labor o la intención de políticos o activistas pero se está dando una normalización de los niveles de violencia que está sufriendo en Europa la población migrante.”]*

#### [**Leer columnas de opinión de  Eleuterio Gabón **](https://archivo.contagioradio.com/eleuterio-gabon/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
