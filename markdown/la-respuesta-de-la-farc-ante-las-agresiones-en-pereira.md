Title: La respuesta de la FARC ante las agresiones en Pereira
Date: 2018-02-05 16:48
Category: Entrevistas, Paz
Tags: FARC, Israel Zúñiga
Slug: la-respuesta-de-la-farc-ante-las-agresiones-en-pereira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/farco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC] 

###### [05 Feb 2018] 

**“No nos podrán arrebatar los sueños, ni las esperanzas, eso es motivo de alegría”** con esa frase Israel Zúñiga, conocido también como Benkos Biojó y candidato de la FARC al senado, invitó a la sociedad colombiana a dejar de lado las agresiones y la violencia frente al pensamiento político diferente. De igual forma, invitan a la ciudadanía a a entablar escenarios de debate y diálogo.

Dichas afirmaciones se dan, teniendo en cuenta que el pasado 3 de febrero, Zúñiga denunció en sus redes sociales, que fue retenido durante dos horas, por un grupo de personas que le impedían salir de la sede de Cooeducar, lugar en donde mantenía una reunión con organizaciones sociales. Además, señaló que este acto habría sido orquestado por **“sectores oscurantistas de la sociedad”**.

Según él, “lo acontecido el sábado entorpece y muestra que hay quienes no tienen la voluntad cierta de sobreponer sus intereses a los elementos de la construcción de paz”. Además agregó que durante ese acto se repartieron sustancias psicoactivas para llevar a las personas a comportarse violentamente.

El candidato por el Senado de la FARC, preocupa que estas provecaciones se estén dando desde sectores políticos y además por parte de los medios de comunicación, como sucedió este fin de semana desde la emisora '**Pereira en vivo', a través de la cual se habría incentivado a la ciudadanía para participar en esta actividad**.

La FARC también denunció la respuesta tardía por parte del gobierno y de la Fuerza Pública ante este incidente, en donde no se le estaba solicitando a la Policía que reprimiera a las personas que se encontraban en el lugar, sino que facilitará la salida del candidato al Senado.

### **La construcción de paz pasa por el debate** 

Frente a estos hechos Israel Zúñiga manifestó que tienen la disponibilidad como organización de encarar el debate, “nosotros hemos planteado con mucha claridad que hemos parado el combate y que **el país merece que se pare el combate, pero debe darle continuidad franca y abierta al debate para** que sean las propuestas, los planteamientos de salida y ante todo la reconciliación del país”.

De igual forma aseguró que el partido político está dispuesto a asumir la responsabilidad de sus actos durante el conflicto armado y señaló que abrigan la esperanza de que los demás actores del conflicto armado actúen de la misma forma: “hay que mirar claramente que por vez primera se está dando **finalización a una confrontación con una Comisión de la Verdad, con una JEP** y con una disposición de uno de los actores a coadyuvar a que se establezca la verdad”. (Le pude interesar:["FARC denuncia el asesinato de 3 de sus integrantes en Nariño"](https://archivo.contagioradio.com/farc-afirma-posible-asesinato-de-3-integrantes-por-estructura-del-eln/))

<iframe id="audio_23564283" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23564283_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
