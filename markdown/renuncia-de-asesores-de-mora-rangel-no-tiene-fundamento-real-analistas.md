Title: Renuncia de asesores de Mora Rangel, no tiene fundamento real: Analistas
Date: 2015-03-26 23:17
Author: CtgAdm
Category: Entrevistas, Nacional, Paz
Tags: Alejo Vargas, Centro de Pensamiento de la Universidad Nacional, Delgación de paz de las FARC, Fuerzas militares, General Mora Rangel, paz en colombia, Proceso de conversaciones de paz de la Habana
Slug: renuncia-de-asesores-de-mora-rangel-no-tiene-fundamento-real-analistas
Status: published

###### Foto: minuto30.com 

<iframe src="http://www.ivoox.com/player_ek_4270730_2_1.html?data=lZekkpyXdI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRjMLtjNLixc3Tt4zm1tLc1MrXb8rix9rbxsbIs9SfxtOY1tTWstCfwpDd1MrXqc%2FXysaYxsqPkY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alejo Vargas, Centro de Pensamiento de la UN] 

<iframe src="http://www.ivoox.com/player_ek_4270733_2_1.html?data=lZekkpyXd46ZmKiakpuJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlsbi1tPQy8aPqMaf0MvWxc7FsMbnjMrbjdfJuMrm0IqfpZDFt8bn0NfS1ZDIqYzB0NfOjbfFssjZjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Eduardo Celis, Corp Nuevo Arco Iris] 

Se ha movido mucho rumor y opinión sin fundamento, señala Alejo Vargas, director del Centro de Pensamiento de la Universidad Nacional, puesto que el **General Mora no ha sido retirado del proceso de conversaciones,** sino que el presidente Santos lo ha llamado a acompañarlo en otras labores de manera transitoria, pero que también tiene que ver con el proceso. Vargas señala que las especulaciones y los rumores vienen de sectores que han estado en contra del proceso.

Además, señala Vargas, que los **oficiales que se manifestaron no tenían un nombramiento oficial** sino una delegación por parte de organizaciones de militares retirados. Por ello sería necesario que el propio General Mora se manifestara de cara al caso para aclarar las circunstancias en esta “bola de nieve” que se ha formado, afirma el catedrático.

Sin embargo, parece haber una intensión de **sembrar dudas frente al proceso** con la salida del General Mora del equipo plenipotenciario del gobierno para las conversaciones de paz.

Frente a las declaraciones de **algunos integrantes de la reserva activa de las FFMM** señalan que el retiro de Mora obedece a que existe el peligro de que Colombia se convierta en una Venezuela, sin embargo Vargas reitera que ese tipo de discursos hace parte de una manera de entender el conflicto que cambiará con el avance del proceso.

Por su parte **Luis Eduardo Celis, investigador e integrante de la Corporación Arco Iris,** afirma que los oficiales estén en su derecho de expresar sus opiniones y hacerlas públicas, pero son infundadas puesto que Mora no ha salido del proceso. Además ya hay un grupo de militares activos que garantizan la presencia de las FFMM en la discusión sobre la dejación de armas y el fin del conflicto.

Otro de los argumentos, en **torno a que hay discusiones secretas, tampoco tiene fundamento según Celis,** ya que las partes han venido presentando los adelantos de la mesa de conversaciones y todo se está haciendo de cara a la opinión pública en general.
