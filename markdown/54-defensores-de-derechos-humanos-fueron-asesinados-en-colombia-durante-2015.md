Title: 54 defensores de derechos humanos fueron asesinados en Colombia durante 2015
Date: 2016-01-13 11:49
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, Defensa de derechos humanos, Defensores de DDHH asesinados en 2015, Derechos Humanos, Front Line Defenders
Slug: 54-defensores-de-derechos-humanos-fueron-asesinados-en-colombia-durante-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/defensore-de-derechos-humanos-asesinados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

<iframe src="http://www.ivoox.com/player_ek_10050471_2_1.html?data=kpWdl5WYe5Khhpywj5aXaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5yncZaojMnSyMrSt9DmxtiYxsqPqMbmxsjV0diPrNbhwtPc1ZDKucbm0NOYw9jJt8riwsnc1ZDJsoyhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jim Lowgran, Front Line Defenders] 

##### 12 Ene 2016 

[De acuerdo con el más reciente informe de ‘Front Line Defenders’ **entre enero y noviembre de 2015 en América Latina fueron asesinados más de 87 defensores de [[derechos humanos](https://archivo.contagioradio.com/?s=derechos+humanos)] (DDH)**, 54 de los cuales eran colombianos. Cifra que además de marcar un incremento frente a los 47 hechos reportados durante 2014, posiciona a Colombia por segundo año consecutivo en el deshonroso primer lugar de las **naciones más peligrosas para el ejercicio de esta actividad**.]

[**La mayoría de asesinatos ocurrieron contra quienes defendían los derechos de los pueblos indígenas, del medio ambiente y de la tierra**, y se vincularon con la implementación de mega proyectos, relacionados principalmente con la minería. El segundo grupo más agredido en la región fue el ligado a la defensa de los derechos de las comunidades LGBTI, sufriendo además, hostigamiento judicial, ataques físicos, amenazas, intimidaciones y campañas de difamación.]

[El sector de defensa de DDHH vinculado con el uso de medios de comunicación para la denuncia de casos de corrupción e impunidad, también fue otro de los blancos de ataque durante el 2015, junto al de mujeres quienes denuncian estar expuestas a la **violencia de género, el hostigamiento y la estigmatización**.   ]

[El **clima de total impunidad que ha rodeado las agresiones, amenazas, hostigamientos y estigmatizaciones** de los que a diario son víctimas los DDH ha llevado a que muchos de ellos se escondan o desplacen de sus países de origen, asegura, Jim Lowgran, director de comunicaciones de FLD, e insiste en que **cada día disminuyen las garantías para la actividad de los defensores de DDHH** y las organizaciones no gubernamentales.]

[“Los **gobiernos copian leyes represivas**, como un virus que va de país en país. Por ejemplo en Rusia ahora mismo están atacando a los defensores de derechos humanos creando leyes que limitan las posibilidades de recibir dinero del extranjero o la aprobación de visas para ingresar al país. Una **crisis global en la que los gobiernos no cumplen con su deber de proteger a los defensores de derechos humanos**”, asevera Lowgran.]

[Entre los **principales agresores se destacan los sectores económicos y políticos influyentes** y** **con control en los medios de comunicación, quienes ven en las denuncias que hacen los DDH una amenaza a sus intereses y buscan reprimirlos de cualquier forma, de ahí lo importante que los gobiernos “prioricen la protección a los defensores con menos palabras y más acciones concretas”.]

[La situación a nivel mundial también es alarmante, **durante 2015 fueron asesinados 156 DDH, 20 más que en 2014,** en países como Afganistán, Azerbaiyán, Bangladés, Birmania/Myanmar, Brasil, Egipto, El Salvador, Filipinas, Guatemala, Honduras, India, Indonesia, Iraq, Libia, México, Nicaragua, Pakistán, Siria, Somalia, Sudán del Sur, Tailandia, Turquía y Yemen.  (Leer: [[Balance DDHH en Colombia 2015](https://archivo.contagioradio.com/este-es-el-balance-en-materia-de-ddhh-en-colombia-para-2015/)[)]]]
