Title: "Gobierno debe dejar de dilatar proceso de paz con ELN"
Date: 2016-05-10 13:47
Category: Nacional, Paz
Tags: ELN, proceso de paz, Secuestro
Slug: gobierno-debe-dejar-de-dilatar-proceso-de-paz-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Colombia-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: DiarioUchile] 

###### [10 May 2016]

Frente a la condición que puso el presidente Santos, de cesar el secuestro como forma de financiación  para continuar el proceso de paz con el Ejército de Liberación Nacional, la guerrilla afirma que esa condición **le pone un freno a la paz** y aún no se han referido a la posibilidad de parar estas acciones.

De acuerdo con Víctor de Currea-Lugo, docente de la Universidad Nacional, esta condición  debe ponerse en re-consideración ya que el 30 de marzo, cuando se hizo pública [la agenda entre el gobierno y el ELN](https://archivo.contagioradio.com/conozca-el-acuerdo-de-dialogo-firmado-entre-el-gobierno-y-el-eln/) en ninguna parte **se estipuló que pudiese condicionarse la negociación**. Sin embargo, considera que el ELN si tiene una responsabilidad frente a las personas que ha retenido contra su voluntad y que por ende, debe rendir cuentas por estos hechos.

Por otro lado, el académico señala que "e[l ELN debe acabar independientemente del proceso de negociación de paz, con la práctica del secuestro y debe hacerlo de forma espontanea y por su propia voluntad". ](https://archivo.contagioradio.com/la-postura-de-la-guerrilla-del-eln-frente-al-proceso-de-paz/)

De Currea, expone que la posición asumida por el gobierno frente a la mesa de diálogos con el ELN ha tenido en varías ocasiones **la intención de dilatar, aplazar o torpedear el proceso en sí mismo** y que la falta de reconocimiento del actor con el que hacen la negociación podría bloquear cualquier salida, **esto debido a que en diversas acciones el ELN ha dejado claro que no responderá a las presiones estatales.**

<iframe src="http://co.ivoox.com/es/player_ej_11478918_2_1.html?data=kpahmZ2ddZmhhpywj5WbaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncbfdxNnc1JDIqYy31tffx8aRb63pyNSSlKiPhc%2FVzc7g1saPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
