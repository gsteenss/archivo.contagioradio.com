Title: Asesinan a Giovany Murillas, excombatiente de las FARC que se la jugó por la paz
Date: 2019-05-11 17:21
Author: CtgAdm
Category: Nacional, Paz
Tags: asesinato, Ex-Combatiente, FARC, Murillas
Slug: asesinado-giovany-murilas-excombatiente-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-4.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Este sábado se conoció del asesinato de Gionvany Murillas, excombatiente de las FARC firmante del Acuerdo de Paz, y quien cumplía con su proceso de reincorporación en el departamento de Guaviare. La noticia se conoció gracias a un mensaje de la senadora Sandra Ramírez en su cuenta de twitter, en el que agregó un panfleto de amenaza contra todos los excombatientes del departamento, así como contra algunas organizaciones sociales, por parte de las autodenominadas Autodefensas Gaitanistas de Colombia (AGC). (Le puede interesar: ["Militar implicado señala que Ejército ordenó seguir a Dimar Torres"](https://archivo.contagioradio.com/militar-implicado-senala-que-ejercito-ordeno-seguir-a-dimar-torres/))

> [\#ATENCIÓN](https://twitter.com/hashtag/ATENCI%C3%93N?src=hash&ref_src=twsrc%5Etfw) || Asesinado otro firmante del Acuerdo de Paz. El día de ayer fue asesinado en el departamento del Guaviare Giovany Murillas. El paramilitarismo sigue fortaleciéndose y el Estado es cómplice. ¡Exigimos que se respete la vida de quienes firmamos la paz! [pic.twitter.com/8prBC7KSFa](https://t.co/8prBC7KSFa)
>
> — Sandra Ramírez (@SandraFARC) [11 de mayo de 2019](https://twitter.com/SandraFARC/status/1127227577423282177?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Aunque no hay más información sobre el homicidio de Murillas, a raíz del panfleto la Senadora acusó al paramilitarismo del hecho; y exigió que se respete la vida de quienes firmaron la paz, pues según relatan algunas organizaciones defensoras de derechos humanos, desde la firma del Acuerdo hasta la fecha se cuentan más de 100 excombatientes asesinados. Algunos de los más recientes atentados contra la vida de quienes le apostaron a la paz son los asesinatos de **Dimar Torres**, y el atentado contra  **Carlos Enrique González y Sandra Pushaina,** que terminó en el asesinato de su hijo, [Samuel David](https://archivo.contagioradio.com/samuel-david-hijo-de-excombatientes-fue-victima-de-la-guerra-con-tan-solo-siete-meses/).

> [\#Indignante](https://twitter.com/hashtag/Indignante?src=hash&ref_src=twsrc%5Etfw) Giovanny Murillas, integrante de [@PartidoFARC](https://twitter.com/PartidoFARC?ref_src=twsrc%5Etfw) asesinado ayer en Guaviare,son ya 120 y más de 30 de sus familiares Siguen el exterminio por un Estado incapaz de proteger la vida y la paz.En su honor y los líderes asesinados afirmamos la paz con justicia socioambiental [pic.twitter.com/CAIEPeHpnC](https://t.co/CAIEPeHpnC)
>
> — Comisión Justicia y Paz (@Justiciaypazcol) [11 de mayo de 2019](https://twitter.com/Justiciaypazcol/status/1127264100671684610?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
