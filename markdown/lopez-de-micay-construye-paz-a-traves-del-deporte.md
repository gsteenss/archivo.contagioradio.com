Title: López de Micay construye paz a través del deporte
Date: 2016-08-26 15:28
Category: Nacional, yoreporto
Tags: colombia, Deporte, López de Micay, paz
Slug: lopez-de-micay-construye-paz-a-traves-del-deporte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/ninos_pies_descalzos-e1472242353820.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 

###### [26 Agos 2016] 

El municipio de **López de Micay**, ubicado en el pacífico caucano, históricamente ha sido un pueblo olvidado por el Estado colombiano. Pero desde el inicio de los diálogos entre la insurgencia de las FAR-EP y el gobierno colombiano, sus pobladores ven una **esperanzadora posibilidad de dignificar sus vidas**.

Es así, como desde el mes de Agosto, **indígenas y afrodescendientes han iniciado un campeonato de microfútbol por la Paz Con Justicia Social**, con el objetivo de integrar a las comunidades entorno de las dinámicas de pedagogía para la paz y poder llevar por medio del deporte, un mensaje de reconciliación a cada individuo de la zona.

Las veredas de Noanamito, Guayabal, Belén de Iguana, Isla del Mono, Zaragoza, Barranco, Casas viejas, Playa Bendita y Bugió, se han hecho presentes con delegaciones de **hombres y mujeres**, para particpar hasta el mes de septiembre en el campeonato por la paz con justicia social.

Los consejos comunitarios y cabildos indígenas presentes han **reiterado su apoyo al proceso de paz** y vienen visibilizando su territorio por medio de este tipo de actividades, ya que es un municipio abandonado por el Estado; constantemente vulnerados sus derechos, como el de la salud, la vivienda, educación, el acceso a la tierra y la producción agrícola.

Héctor Buitrago poblador de la zona expresa que "este nuevo ejercicio, es un llamado al pueblo caucano para que apoyemos la posibilidad real de la paz, **para que cambiemos la cultura de guerra que el establecimiento nos ha impuesto**, **por nuevas dinámicas incluyentes y democráticas que visibilicen lo que en realidad queremos ser**; un pueblo que viva en armonía con su territorio, que sea dignó, sin explotación ni despojo"

Mediante estas prácticas que le apuestan a construir un territorio en reconciliación, armonía, dignidad y paz con justicia social, las comunidades de López de Micay le vienen apostado todo al SÍ al próximo plebiscito para **cerrarle las puertas a la guerra**.

##### Por: Agencia de Comunicaciones Prensa Alternativa Cauca 
