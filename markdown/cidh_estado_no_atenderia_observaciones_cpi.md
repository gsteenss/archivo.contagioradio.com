Title: En Colombia hay una impunidad del 97% en violaciones de DDHH
Date: 2017-10-23 20:05
Category: Nacional, Paz
Tags: acuerdo de paz, CIDH, JEP, Nestor Humberto Martínez
Slug: cidh_estado_no_atenderia_observaciones_cpi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cidh-2-e1508781692107.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Oct 2017] 

El Estado afirmó que escucha a la CPI pero que sus observaciones deben estar acordes con el ordenamiento jurídico interno, lo cuál podría significar una violación al estatuto de Roma si no se atienden las advertencias de la Corte Penal.

Esta se convierte en una de las principales preocupaciones que deja la primera parte del Periodo de Audiencias número 165 de la Comisión Interamericana de Derechos Humanos, CIDH, a las organizaciones defensoras de derechos humanos, dado que podría significar que los instrumentos internacionales de defensa de los derechos de las víctimas sean desconocidos por Colombia, afirma Jomay Ortegón, directora del Colectivo de abogados José Alvear Restrepo.

Además, ante la CIDH, las organizaciones advirtieron que, según se ha tramitado la JEP en el Congreso, **no es clara la participación de las víctimas, pero tampoco que los máximos responsables sean judicializados,** "actualmente la arquitectura institucional no pareciera permitirlo, como lo refleja el último documento de la CPI", explica Ortegón. (Le puede interesar:[Cuatro preocupaciones de la CPI sobre la JEP)](https://archivo.contagioradio.com/cuatro-modificaciones-a-la-jep-que-preocupan-a-la-corte-penal-internacional/)

### **Altos índices de impunidad** 

Como un ejemplo de la idea de impunidad en Colombia, las organizaciones afirmaron que con la Ley 975 de 2005, **generó 15.743 compulsas de copias para investigar actores militares, políticos y económicos, pero no hubo ningún resultado, ** lo que, según los defensores de DDHH presentes en la audiencia, podría dificultar la posibilidad de que terceros que hayan contribuido en el los crímenes en el conflicto armado, comparezcan en la JEP.

Otro de los anexos presentados a la CIDH reporta 456 casos, de los cuales el 74% esta en etapa de investigación, el 4,7% archivadas, sobre el 16,2% la Fiscalía no reporta ningún tipo de investigación, solo en 2% hay acusaciones, y el 1,1% cuenta con sentencia, según datos de la propia fiscalía. Esta situación significaría que no hay garantías de No Repetición.

\[embed\]https://www.youtube.com/watch?v=jmfg2NAS0fE\[/embed\]

### **Fiscalía no ha contribuido a la superación de la impunidad** 

De acuerdo con Ortegón, a pesar de que existe un acuerdo de paz, este no es suficiente por si mismo para cambiar las condiciones de riesgo para los defensores de DDHH. Es necesario aprovechar esta oportunidad para hacer una  reingeniería de instituciones como la fiscalía, que "históricamente han sido incapaces de responder frente al reto más importante de investigar las violaciones a los derechos humanos como mecanismo de prevención para que no se sigan cometiendo".

En ese sentido se debe revisar, según las organizaciones, que la fiscalía no tiene cifras propias sobre violaciones a los derechos humanos, que no hay una relación entre las diferentes acciones investigativas, que no haya un trabajo interconectado entre las acciones de investigación y de protección pues se presentan  como si fueran cosas aisladas.

### **Sobre el paramilitarismo y las violaciones a DDHH** 

En el acuerdo de paz se estableció la Unidad para desmantelamiento del paramilitarismo con el decreto 898 del 2017, sin embargo con las observaciones del fiscal Néstor Humberto Martínez se convirtió en institución subordinada y de bajo rango a dicha entidad, lo que amenazaría la autonomía de la Unidad y difícilmente podría lograrse el objetivo por la que fue creada.

 "Aspecto esencial para la viabilidad del acuerdo de paz con las FARC. Teniendo en cuenta el mayor riesgo con el artículo 3 del acto legislativo, pues el fiscal podría seleccionar y priorizar casos que no sean competencia de la JEP", explica la abogada. (Le puede interesar: [Organizaciones colombianas manifestaron ante la CIDH la grave situación que afrontan los líderes sociales)](https://archivo.contagioradio.com/situacion-de-lideres-y-defensores-en-colombia-cidh/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
