Title: El salario en Colombia seguirá siendo mínimo
Date: 2015-12-09 15:34
Category: Economía, Nacional
Tags: CTC propone aumento del 10% en salario mínimo, CUT propone aumento del 12% en salario mínimo, Salario Mínimo en Colombia, Salario Mínimo en Colombia 2016
Slug: el-salario-en-colombia-seguira-siendo-minimo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Salario-mínimo-Colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Youtube] 

###### [9 Dic 2015 ] 

[Desde el pasado lunes y hasta el 30 de este mes sesionará la ‘Comisión Permanente de Concertación de Políticas Salariales y Laborales’ integrada por líderes de las centrales obreras, empresarios y representantes del gobierno nacional para acordar el **incremento salarial del próximo año**, que de acuerdo con la 'Encuesta Salarial Total Rewards 2016' estaría **por el orden de los \$35.440**, un aumento cercano al 5%, cifra que continuaría dejando a **Colombia** como **uno de los países con los salarios mínimos más bajos en América Latina**.]

[La propuesta de la Central Unitaria de Trabajadores CUT, es que el **salario mínimo aumente en un 12%** y la de la Confederación de Trabajadores de Colombia CTC, es que el **incremento sea del 10%**, posturas que se contraponen a las reveladas por la firma ‘Human Capital’ que asegura que el **12% de las empresas** encuestadas **no aumentaría el salario mínimo**, mientras que el **34% lo incrementaría por debajo del 5% **y el **54%** restante lo haría implementando **mecanismos de beneficios y remuneración variable**. ]

[El próximo jueves se espera que el DANE exponga la cifra del PIB de 2015, para que junto con el **índice de inflación del 6.7%** expuesto por el Banco de la República y de la **productividad del -0.5%** estimada por Departamento Nacional de Planeación sirvan de insumos para alimentar la discusión entre los gremios sindicales y empresariales, para que el Gobierno nacional no termine decretando este fin de año el aumento salarial en Colombia.]

Por su parte los organismo internacionales como la OCDE y el [FMI](https://archivo.contagioradio.com/fmi-propone-pensiones-menores-al-salario-minimo/) han promovido la idea de que el salario mínimo en Colombia está por encima de algunos promedios internacionales, [razones que concuerdan con los intereses de los empresarios.](https://archivo.contagioradio.com/el-salario-minimo-en-colombia-es-de-sobrevivencia/) Lo cierto es que ninguna de las entidades consulta a las familias colombianas que señalan que el salario mínimo en nuestro país no alcanza para sobrevivir dignamente.
