Title: Santos recibió el Nobel en nombre de las víctimas
Date: 2016-12-10 12:15
Category: Nacional
Tags: colombia, nobel, paz, Santos
Slug: nobel-paz-colombia-santos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/CzUW_VmXUAAee37.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  

##### 10 Dic 2016 

Con un discurso en el que se refirió a las víctimas del conflicto, a la negociación con la guerrilla de las FARC y a los acuerdos alcanzados durante el proceso de paz en Colombia, el presidente **Juan Manuel Santos recibió la mañana de este sábado el Premio Nobel de Paz**, en ceremonia realizada en la ciudad de Oslo, capital de Noruega.

Durante su intervención el mandatario manifestó que “**Es insensato pensar que el fin de los conflictos sea el exterminio de la contraparte**” refiriéndose a la forma en que se adelantaron las negociaciones con la guerrilla asegurando que “La victoria final por las armas –cuando existen alternativas no violentas– no es otra cosa que la derrota del espíritu humano”.

Durante la ceremonia Santos solicitó a los representantes de las más de 8 millones de víctimas del conflicto armado ponerse en pie para **recibir el reconocimiento por parte de los presentes quienes los aplaudieron de manera sentida**, y se dirigió particularmente a Leyner Palacios, quien perdió gran parte de su familia en la masacre de Bojayá, recalcando que el líder comunitario ya había perdonado a las FARC por tal hecho.

“**El acuerdo de paz en Colombia es un rayo de esperanza en un mundo afectado por muchos conflictos y demasiada intolerancia**”, exaltando además la voluntad de paz de los equipos negociadores quienes se mantuvieron conversando a pesar de no haber logrado una refrendación inmediata en el plebiscito del 2 de octubre, "**Hay una guerra menos en el mudo y es la de Colombia**" aseguró.

El Presidente aseguró que el anuncio del premio Nobel de Paz llegó "como un regalo del cielo" en un momento en que todo parecía estar a la deriva, convirtiéndose en un impulso para continuar en la búsqueda de un nuevo acuerdo, mismo que fue refrendado en el Congreso de la República. “**El sol de la paz brilla, por fin, en el cielo de Colombia” “¡Que su luz ilumine al mundo entero!”** finalizó su intervención. Le puede interesar: D[iscurso que el presidente Santos no pronunciara en Oslo](https://archivo.contagioradio.com/discurso-que-el-presidente-santos-no-pronunciara-en-oslo/)
