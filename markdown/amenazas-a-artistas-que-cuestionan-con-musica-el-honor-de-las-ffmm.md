Title: Más de 3000 mensajes amenazantes contra cantantes de "Militares" ya están en la fiscalía
Date: 2020-07-24 17:26
Author: AdminContagio
Category: Actualidad, Cultura
Slug: amenazas-a-artistas-que-cuestionan-con-musica-el-honor-de-las-ffmm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Entrevista-Reincidentes-CR.mp3" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/WhatsApp-Image-2020-07-23-at-3.10.32-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado [20 de julio](https://archivo.contagioradio.com/estudiantes-de-la-universidad-surcolombiana-exigen-matricula-cero/) se publicó la canción ["Militares"](https://www.youtube.com/watch?v=7ars7VQcNAc), escrita por el grupo Reincidentes Bogotá y Diana Avella, y **que habla sobre los más recientes hechos de violaciones a los DDHH y el DIH que involucran a miembros de las Fuerzas Militares.** Luego del lanzamiento se produjeron amenazas, intentos de censura vía bloqueo de las cuentas y que evidencian seguimientos en contra de los artistas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los autores **Manuel Garzón, Diana Avella, y Hets Mamunt,** señalan que qué el lanzamiento de la interpretación fue promovida días previos, a través de redes sociales y algunos medios de comunicación, por tanto ya varios sectores sabían de su existencia y su contenido. Además indican que **no es casualidad que esta haya sido lanzada en el marco de la celebración del 20 de julio.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Manuel Garzón, coautor de la canción, señaló que i**ncluso antes del lanzamiento a sus redes sociales empezaron a llegar mensajes que descalificaron la canción**, e incluso reconocieron que varios de estos usuarios se identificaban como miembros del Ejército, particularmente el caso de un Capitán y un Mayor.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Aumentaron las reproducciones pero también aumentaron las amenazas

<!-- /wp:heading -->

<!-- wp:paragraph -->

**La canción alcanzó a tener más de 100,000 reproducciones las primeras 24 horas** y junto a esto incrementaban los comentarios principalmente en YouTube , amenazadores e intimidantes, *"incluso recibimos una llamada en la que alertaban que al interior de grupos de WhatsApp asociados institucionalmente con la [Policía Nacional](https://archivo.contagioradio.com/comunidades-claman-por-la-verdad-y-por-acuerdos-humanitarios-que-alivien-el-dolor-de-la-guerra/)y las [Fuerzas Militares](https://archivo.contagioradio.com/congreso-de-eeuu-condiciona-ayuda-militar-a-colombia/) se había dado la orientación de entrar al canal y denunciarlo"* para bloquearlo*.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto según el abogado con la intención de censurar la canción y junto a ella el canal, *"la zozobra aumentó junto con los más de 3.000 comentarios, **mensajes mayormente insultantes, de odio y amenazantes, lo que nos puso en alerta de que podríamos estar en una situación de riesg**o".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estás numerosos mensajes, Garzón agregó que presentaron las respectivas denuncias ante la Fiscalía, Defensoría del Pueblo, Naciones Unidas entre otras instituciones, con el objetivo de defender su libertad de expresión, *"pero **también se activaron cadenas de apoyo de personas qué sabemos que nos cuidan, nos protegen y nos han hecho sentir muy acompañados**".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y señaló que entre los mensajes más preocupantes habían fotografías que hacían parte de sus redes sociales, lo que refleja según Garzón ***"un trabajo de perfilamiento, de estudio de mis cuentas personales de contenido que no tiene nada que ver con la publicación de la canción".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y ante la identificación de integrantes de las Fuerzas Militares dentro de las amenazas a los artistas, señaló que hasta el momento no hay certeza de que efectivamente hagan parte de las FFMM, pero que según, ***"lo que se puede observar en sus redes sociales y cómo se identifican da entender de que efectivamente se trata de uniformados del Ejército".***

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=7ars7VQcNAc","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=7ars7VQcNAc

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:quote -->

> *"V**ale la pena cuestionarnos también el uso que hacen de las redes sociales los integrantes de las Fuerzas Armadas**, claro que ellos hacen parte de la sociedad civil, pero si en sus cuentas aparecen con sus uniformes y se identifican como integrantes de alguna institución este contenido deja de ser privado y se convierte en contenido institucional".*
>
> </p>
> <cite>Manuel Garzón| Abogado y coautor de la canción</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### *"La intención de nuestra canción nunca fue motivar al odio, solo al cambio en el discurso de honor de las FFMM"*

<!-- /wp:heading -->

<!-- wp:paragraph -->

Manuel Garzón, señaló que **la principal motivación de esto fue la indignación**, producto de los hechos recientes en los que se han visto involucrados miembros de las fuerzas militares, *"el regreso de los falsos positivos, la violación de niñas indígenas, asesinato de líderes sociales, estigmatización de la juventud, entre otros casos conocidos públicamente".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**En en la canción no hablamos nada nuevo**, simplemente recopilamos esa indignación que sabemos que es generalizada en ciertos sectores de la sociedad en el derecho a nuestra libertad de expresión".*
>
> <cite>Manuel Garzón| Abogado y coautor de la canción</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y agregó que la intención que tuvieron como artistas, "***nunca ha sido atacar ninguna institución, cómo se nos pretendido señalar, al contrario, estamos convencidos de la necesidad de fortalecer** a partir de la concepción de una nueva fuerza pública para la paz".* (Le puede interesar: [Es momento de redoblar los esfuerzos por la paz: Iván Cepeda](https://archivo.contagioradio.com/redoblar-esfuerzos-paz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado afirmó, que las Fuerzas Militares, no deben ser concebidas como parte de alguna corriente política, sino "*debe ser funcionales a la institucionalidad, al Estado y a la ciudadanía, **más allá de sus intereses políticos e ideológicos**".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Nuestro vídeo Militares iba a ser censurado por hacer apología al terrorismo".

<!-- /wp:heading -->

<!-- wp:paragraph -->

En medio de un discurso en el que se censura, amedrentado o silenciado a quién cuestione a las fuerzas militares, el abogado señaló que dentro de las acciones que se pretendían hacer para evitar que este vídeo fuese conocido, era denunciarlo por hacer *"hacer apología al terrorismo".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Hay que contrarrestar esa información, porque discrepar en Colombia no puede ser una apología al terrorismo"***

<!-- /wp:quote -->

<!-- wp:paragraph -->

A la vez Garzón afirmó que, la historia de Colombia y más de 50 años de una doctrina militar arraigada *"han sido suficientes para que cualquier tipo de expresión en contra de esta institución se convierta en un ataque directo que pone en riesgo la vida y la integridad de quienes hacen los señalamientos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente señaló, que como artistas, están dispuestos a apoyar las investigaciones que se realicen para aclarar y defender su derecho a la libre expresión. Al mismo tiempo que hacen un llamado a las instituciones a tener en cuenta los comentarios que ponen en evidencia esta campaña de odio que se activó en su contra, *" necesitamos determinar si e**fectivamente varios de estos comentarios corresponden a integrantes de las FFMM y determinar en qué punto termina el insulto y comienza la amenaza"**.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:audio {"id":87272} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Entrevista-Reincidentes-CR.mp3">
</audio>
  

<figcaption>
*Encuentre aquí la entrevista completa con Manuel Garzón, integrante de Reincidentes Bogotá*

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:block {"ref":78955} /-->
