Title: Diálogos sobre parto respetado y violencia obstétrica para la construcción de paz
Date: 2019-04-23 11:19
Author: CtgAdm
Category: eventos
Tags: mujer, parto respetado, Salud, Violencia Obstétrica
Slug: dialogos-parto-respetado-violencia-obstetrica-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/violencia-obstétrica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: cinthiapomaski.com 

Ante la preocupación por la violación de derechos sexuales y reproductivos durante la gestación, el parto y el postparto, documentados ampliamente como un problema de salud pública, **este viernes 26 de abril se realizará en Bogotá el Primer Encuentro Intersectorial del Movimiento Nacional por la salud sexual y reproductiva**, un espacio para el diálogo donde se abordará como eje central el tema del **parto respetado y la violencia obstétrica como parte de la construcción de paz en Colombia.**

De acuerdo a las organizadoras, el encuentro "busca recoger la voz de las personas que saben que **cambiar la forma de parir es cambiar la forma como nos relacionamos**; que saben que la gestación, el trabajo de parto, **el parto, el posparto y el duelo gestacional son procesos vitales que transforman la vida de las mujeres y de la sociedad en general**" destacando el rol que estas prácticas puede tener para la construcción de paz desde los vientres.

La premisa se sustenta en que **si estos procedimientos son vividos con respeto, libertad, información y conciencia, puede se parte importante en la construcción de paz**, o por el contrario, **si son rodeados de violencia, de miedo y de frustración pueden ser el origen de dolorosos problemas físicos y emocionales** que no solo repercuten en la mujer y en el recién nacido, sino que igualmente pueden ser **germen de violencia en la sociedad**.

A partir de estos planteamientos, se pretende por medio de un diálogo abierto a todo público, construir **un espacio de reflexión y de construcción entre los diferentes actores que intervienen en estas prácticas** como son las  mujeres y hombres, parteras, doulas, enfermeras, médicos, obstetras, agentes de salud, académicos, artistas, Congreso, Gobierno, organizaciones nacionales e internacionales, entre otros, y permita **avanzar en la garantía del derecho de las mujeres a parir con dignidad**.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/violencia-obstétrica-277x300.jpg){.alignnone .size-medium .wp-image-65278 .aligncenter width="277" height="300"}

El evento definido sobre 3 ejes temáticos: **Parto Humanizado, Violencia Obstétrica y Partería y Doulaismo**, que se desarrollaran en mesas de trabajo y paneles programados para iniciar a las **8 de la mañana** en el **Auditorio Virginia Gutiérrez de Pineda, Edificio de Posgrados de la Facultad de Ciencias Humanas de la Universidad Nacional de Colombia**, sede Bogotá.  Para asistir debe diligenciarse el formulario de inscripción disponible [aquí](https://bit.ly/2JY1Q4E).

###### Reciba toda la información de Contagio Radio en [[su correo]
