Title: Senador Cepeda denuncia complot para vincular su esposa en caso de corrupción
Date: 2019-04-12 15:15
Author: CtgAdm
Category: DDHH, Judicial
Tags: Fiscalía General de la Nación, Iván Cepeda, Jesús Santrich, jurisdicción especial para la paz
Slug: senador-cepeda-denuncia-complot-para-vincular-su-esposa-en-caso-de-corrupcion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Ivan-Cepeda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

El senador Iván Cepeda radicó una denuncia ante la Fiscalía General de la Nación este viernes, a través de la cual, dio a conocer un **presunto complot en el que buscaban sobornar a su esposa Pilar Rueda,** para que ella, actualmente asesora de la Unidad de Investigación de la Jurisdicción Especial para la Paz, incidiera en el proceso del dirigente del partido FARC Jesús Santrich.

Según relato Cepeda, **el pasado 9 de abril, el senador recibió una carta de un abogado anónimo que relató los supuestos hechos**, donde le explica que fue contactado por un colega mexicano en enero para discutir el caso de un cliente que tenía en su posesión propiedades a nombre de Santrich y que estaba interesado en evitar su extradicion. Al contrario, el abogado mexicano aseguraba que los bienes serían irrecuperables.

El litigante colombiano propuso una estrategia que involucraba la participación del dirigente del partido FARC, sin embargo, el abogado mexicano rechazo este plan, asegurando que **si el señor Santrich supiera de las intenciones de su cliente podría tomar acciones para timar los bienes mencionados**.

En seguida, el abogado mexicano propuso un plan alternativo, asegurando que el litigante colombiano tenía que **sobornar a la señora Pilar Rueda par evitar la extradicion de Santrich**. Para realizar este trabajo, tendría **2 millones de dólares a su disposición para persuadirla y sería recompensado con 5 mil dólares de adelantado y 100 mil dólares si lograba su cooperación**. El abogado rechazó la propuesta, lo que disgusto al otro litigante y su cliente.

Después de conocer de la captura del exfiscal de la JEP Carlos Julián Bermeo en febrero, **el abogado colombiano decidió denunciar los hechos al senador Cepeda. "Como quiera que los hechos y las circunstancias en que se dio la detención del fiscal de la JEP podrían tener con lo que aquí he relatado, considero mi deber informarlos a ustedes para que tome las medidas considere pertinentes"**, lee la carta.

[En rueda de prensa a las afueras de la Fiscalía, el senador aseguró que **se está tejiendo "un plan en el que se busca a toda costa involucrar a mi esposa y de manera indirecta, involucrarme a mi también**, en los hechos que han sido conocidos por la opinión pública, con relación al supuesto intento de carteles mexicanos de la droga por dilatar o por malograr la investigación que se hace o el procedimiento que se está adelantando y evitar la extradición del señor Santrich".]

También, señaló que **"si a futuro se llegase a producir algún hecho similar o conexo con el que estoy denunciando, es evidente que aquí hay un montaje por enlodar nuestros nombres** y por intentar también un montaje judicial que pueda relacionar de alguna manera la Jurisdicción Especial para la Paz con los hechos que aquí se describen".

###### [Reciba toda la información de Contagio Radio en ][[su correo][o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][[Contagio Radio] 
