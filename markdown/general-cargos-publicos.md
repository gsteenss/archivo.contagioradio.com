Title: General (r) acusado de falsos positivos no debería ocupar cargos públicos
Date: 2019-01-31 18:39
Author: AdminContagio
Category: DDHH, Paz
Tags: cajar, falsos positivos, General Barrero, JEP
Slug: general-cargos-publicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/general_leonardo_barrero-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia 2020] 

###### [29 Ene 2019] 

**Nancy Patricia Gutiérrez, ministra de interior aseguró que ella es quien lidera el Plan de Acción Oportuno (PAO)**, declaración que se produce luego de que organizaciones sociales se manifestarán contra el nombramiento del General (r) Barrero en este cargo, por estar involucrado en casos de ejecuciones extrajudiciales. (Le puede interesar: ["Gral. (r) acusado de falsos positivos dirige plan que protege a líderes sociales"](https://archivo.contagioradio.com/director-falsos-positivos/))

Para Jomary Ortegón, abogada de el Coletivo de Abogados José Alvear Restrepo, la declaración de la ministra por un lado es buena, porque ya no será Barrero quien dirija el plan para proteger líderes; pero por otro lado sigue siendo mala, pues la Ministra señaló que Barrero será el enlace del Plan con la Fuerza pública y **"una persona que tiene acusaciones tan graves no debería ocupar cargos públicos"**.

La abogada sostuvo que entre las garantías de no repetición, establecidas de forma global, se establece la depuración del sistema público, como una forma de garantizar que los actores estatales que estuvieron mientras se cometían violaciones de derechos humanos no sigan en puestos de poder. Y lo que se ha dicho sobre la labor del General es que continuará prestando asesoría al Ministerio de Defensa, por lo que su trabajo seguiría siendo en torno a la protección de líderes sociales.

### **Este Gobierno parece experto nombrando a personas cuestionadas** 

La abogada concluyó que el rasgo de este Gobierno no ha sido su lucha contra la corrupción y las redes clientelistas, sino que por el contrario, ha sido el nombramiento de personas que no cumplen con los requerimientos técnicos y éticos para ocupar puestos de poder como ocurrió en el caso de Claudia Ortiz, o la designación de Juan Pablo Bieri.

Sin embargo, en el caso del General, Ortegón espera que la JEP revise el informe sobre ejecuciones extra judiciales presentado por el Colectivo el año pasado, y que pueda llamarlo a dar versión sobre los hechos en los que se ve vinculado para aclarar su papel en los hechos. (Le puede interesar: ["Rechazan nombramiento de Brigadier Gral. implicado en falsos positivos a fuerza de tarea"](https://archivo.contagioradio.com/rechazan-nombramiento-brigadier-general-implicado-falsos-positivos-cauca/))

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
