Title: Acusaciones de Aída Merlano deben llegar a la Fiscalía
Date: 2020-02-20 15:51
Author: CtgAdm
Category: Judicial, Política
Tags: Clan Char, Clan Gerlein, Fiscalía General de la Nación, Iván Duque
Slug: acusaciones-de-aida-merlano-deben-llegar-a-la-fiscalia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Diseño-sin-título-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto:

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/david-racero-sobre-denuncia-contra-ivan-duque_md_48221638_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; David Racero | Representante a la cámara

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

El representante David Racero, radicó una denuncia ante ante la Comisión de Investigación y Acusación de la Cámara de Representantes contra el presidente Iván Duque en referencia directa a la versión de Aída Merlano desde Venezuela **quien acusa al mandatario de haber comprado votos para su elección presidencial y atentar contra la vida de la excongresista de la mano de familias del Atlántico como los Char y los Gerlein.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La denuncia radicada, señala al presidente Duque de los presuntos delitos de constreñimiento al sufragante, corrupción al sufragante, tráfico de votos, y tentativa de homicidio, y aunque el representante admite que en el proceso de investigación hay que corroborar modos, tiempos y lugares, expresa la importancia de **escuchar un testimonio de una persona que lleva "30 años trabajando con líderes políticos del país y que por tanto pudo conocer de primera mano la forma en que este círculo operaba”** por lo que de entrada no se debe ignorar su declaración, como se ha pretendido hacer desde la opinión pública en los últimos días.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Las declaraciones de Aída Merlano

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La ex congresista reveló que los clanes Char y los Gerlein **inviertieron al menos 6.000 millones de pesos en la campaña presidencial de Iván Duque quien estaba al tanto** y que incluso se reunió con el empresario Julio Gerlein para “coordinar” la cifra a invertir. Además denunció un plan de asesinato en su contra, según ella, los mismos que la ayudaron a escapar querían asesinarla, acusando directamente a los Char y Gerlein, además acusó al Gobierno de Colombia de estar detrás de un plan contra su vida. [(Lea también: Los 4 partidos políticos inmersos en escándalo de compra de votos)](https://archivo.contagioradio.com/los-4-partidos-politicos-inmersos-en-escandalo-de-compra-de-votos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, indicó que fue testigo de altas sumas de dinero entregadas en las campañas presidenciales de Álvaro Uribe y que Germán Vargas Lleras, recibió más de 15.000 millones de pesos por parte de Odebrecht y Valorcon destinados a la reelección de Juan Manuel Santos en 2014. De igual manera se refirió a la Fiscalía General de la Nación, mencionado que se trata de una institución bajo el control de la familia Char y que **incluso la elección de Néstor Humberto Martínez, como fiscal habría sido amañada.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Racero es enfático en que el presidente Duque tiene responsabilidad sobre los hechos denunciados por Merlano, que de hablar con la verdad, demostraría que la elección de Duque como presidente es ilegítima, "estamos hablando de un fraude electoral de una campaña presidencial, estamso ante una crisis de la democracia y de nuestros sistemas de elección popular".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La denuncia realizada por el representante Racero pasará a un estudio que debe ser delegado a congresistas de la Comisión que estudiarán el caso y darán un concepto, si surte un efecto positivo pasaría a plenaria de Cámara y de ahí podría pasar al Senado, un trámite que si bien puede ser lento, resalta debe llegar a instancias como la Fiscalía a donde también se llevará una petición por firmas para investigar el caso, incluso si esta no es tomada en cuenta por el nuevo fiscal Barbosa

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"El fiscal Barbosa, irresponsablemente ya estaba diciendo que no iba a abrir ningún tipo de investigación, está cerrando un caso más sin abrirlo", expresa Racero. [(Le puede interesar: La llegada de un "camaleón rabioso" a la Fiscalía)](https://archivo.contagioradio.com/la-llegada-de-un-camaleon-rabioso-a-la-fiscalia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El silencio es la garantía de impunidad de los corruptos

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El representante señala que el debate sobre las acusaciones contra el Gobierno y las casas políticas de los Gerlein y los Char **no se ha podido ampliar ya que en general existe un silencio sobre el tema,** "los medios no arrojan nada y eso la evidencia de la familiaridad entre periodistas y clase política, él silencio es la garantía de la impunidad de los corruptos" expresa Racero, agregando que desde el Gobierno no solo se obedeció a una comprar devotos sino a una compra de opinión.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Lo que está demostrado es que el presidente Duque, su grupo del Centro Democrático, las clases poderosas de la costa y medios de comunicación no quieren que se conozca la verdad, están vetando el tema". - representante David Racero

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

El representante reitera además, que este ha sido un tema que no se ha podido llevar a la mesa pues los medios en Colombia son los mismos que "ponen y colocan líderes"; tanto que ya existe un candidato a la Presidencia para el 2022, que sería Alejandro Char, exalcalde de Barranquilla. [(Le puede interesar: El fracaso de la política exterior de Duque a 18 meses de su Presidencia)](https://archivo.contagioradio.com/el-fracaso-de-la-politica-exterior-de-duque-a-18-meses-de-su-presidencia/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre Arturo Char, quien está a punto de ser nuevo presidente del Congreso de la República la Corte Suprema de Justicia ha tomado la decisión de abrir una investigación preliminar por la fuga de Aída Merlano. La Corte, también decidió escuchar su versión por lo que los magistrados buscan el mecanismo jurídico para acceder a la excongresistas actualmente en Venezuela.  

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
