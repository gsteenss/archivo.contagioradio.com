Title: Video | Hidroituango, una bomba de tiempo
Date: 2018-05-17 16:02
Category: Ambiente, Nacional
Tags: afectados por hidroituango, ANLA, Bajo Cauca, EPM, Hidroituango
Slug: video-hidroituango-una-bomba-de-tiempo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/hidroituango.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

La crisis en el Bajo Cauca Antioqueño continúa producto del taponamiento de uno de los túneles de llenado del proyecto hidroeléctrico Hidroituango. Las comunidades han denunciado que viven en zozobra constante y EPM se ha limitado a decir que tiene la situación controlada. Contagio Radio recopiló proceso que ha tenido el proyecto, sus afectaciones a las comunidades, la búsqueda de desaparecidos y la crisis actual que vive la población. Ver: [https://bit.ly/2IH3MN2 ](https://bit.ly/2IH3MN2)
