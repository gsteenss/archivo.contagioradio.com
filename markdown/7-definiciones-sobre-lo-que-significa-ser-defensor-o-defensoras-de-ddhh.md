Title: 7 visiones sobre lo que significa ser defensor o defensoras de DDHH
Date: 2015-12-10 14:07
Category: DDHH, Entrevistas
Tags: Abilio Peña, Alberto Yepes, central unitaria de trabajadores, colectivo de Abogados José Alvear Restrepo, Comisión de Justicia y Paz, Coordinación Colombia- Europa-Estados Unidos, CUT, Defender derechos humanos en colombia, Democracia y Desarrollo, Fabio Arias, Luz Marina Monzon, Minga, Plataforma Colombiana de Derechos Humanos, Somos defensores, Yessica Hoyos
Slug: 7-definiciones-sobre-lo-que-significa-ser-defensor-o-defensoras-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/defensa-de-derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Comisión de Justicia y Paz 

###### [10 Dic 2015] 

Siete defensoras y defensores de derechos humanos, compartieron con Contagio Radio, lo que ha significado para ellos su labor en un país como Colombia que cuenta con uno de los mayores índices de agresiones contra quienes dedican su vida a la construcción de una sociedad más justa.

<iframe src="http://www.ivoox.com/player_ek_9659986_2_1.html?data=mpuim56ceo6ZmKiakpWJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRe4zhwtPS1MbXb8XZjMnSyM7SrdOfzcaYxsrKqc%2FnwpDRx5DQs9Sfpam1qpKlpsrgytSYssqJh5SZo5aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Abilio Peña, integrante de la Comisión de Justicia y Paz:** “Es ser una persona inminentemente comprometida con el dolor del otro y la otra. Surge de una indignación ética por la forma  violenta como el Estado con su poder, con sus alianzas con empresarios, y poderes políticos agrede la humanidad y dignidad de mujeres y hombres en el país".

<iframe src="http://www.ivoox.com/player_ek_9660043_2_1.html?data=mpujkpWYd46ZmKiakpWJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRe4zhwtPS1MbXb8XZjMnSyM7SrdOfzcaYxsrKqc%2FnwpDRx5DQs9Sfpam1qpKwudufrsbfy9PFb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Luz Marina Monzón, abogada:** “Es actuar con responsabilidad frente a un futuro que uno quisiera que fuera distinto a lo que muestra la situación actual de las violaciones de derechos humanos…  a veces uno quiera poder tomar decisiones frente a las necesidades de las víctimas y poder generar un alivio al sufrimiento del otro".

<iframe src="http://www.ivoox.com/player_ek_9660006_2_1.html?data=mpujkpWUeo6ZmKiakpWJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRe4zhwtPS1MbXb8XZjMnSyM7SrdOfzcaYxsrKqc%2FnwpDRx5DQs9Sfpam1qpKlsMPZ09ncjb7JtI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Alberto Yepes, coordinador del observatorio de derechos humanos de la Coordinación Colombia - Europa - Estados Unidos:** “Ante todo un compromiso son la dignidad del ser humanos, con los derechos fundamentales de cada personas sin distinción de raza, sexo, religión o pensamiento político. La defensa de los derechos humanos el fundamento de la paz. Representan el logro más elevado de la civilización, y es el gran indicador sobre el grado de democracia que puede vivir una sociedad, por eso la defensa de los derechos humanos no debería ser una actividad especializada de defensores sino algo que incumbe a todas las personas”.

<iframe src="http://www.ivoox.com/player_ek_9659969_2_1.html?data=mpuim56afY6ZmKiakpWJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRe4zhwtPS1MbXb8XZjMnSyM7SrdOfzcaYxsrKqc%2FnwpDRx5DQs9Sfpam1qpKnpdPg0NiYqdrJuo6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Carlos Guevara, coordinador del programa Somos Defensores: **“Lejos de las definiciones que hagan organizamos como Naciones Unidas, ser un defensor de derechos humanos es una actitud de vida, es trabajar por la vida, por la base de todas las vida que son los derechos fundamentales. Un defensor de derechos humanos ante todo es un hombre o una mujer que da ejemplo desde su actividad y desde su vida misma, da ejemplo de lucha y de entrega de amor.. Se entrega lo que se tiene por hacer este país un lugar mejor”.

<iframe src="http://www.ivoox.com/player_ek_9660051_2_1.html?data=mpujkpWZdY6ZmKiakpWJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRe4zhwtPS1MbXb8XZjMnSyM7SrdOfzcaYxsrKqc%2FnwpDRx5DQs9Sfpam1qpK9qdTnysjOja3TvY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Yessica Hoyos, vocera de la Plataforma Colombiana de Derechos Humanos, Democracia y Desarrollo, y abogada del Colectivo de Abogados José Alvear Restrepo:** “Es mi vida la defensa de los derechos humanos, es una vida llena de riesgos, que tiene mucho temor por lo que pueda ocurrir, pero es una vida llena de esperanza en que realmente se pueda construir un país en paz con justicia social”.

<iframe src="http://www.ivoox.com/player_ek_9660038_2_1.html?data=mpujkpWXfI6ZmKiakpWJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRe4zhwtPS1MbXb8XZjMnSyM7SrdOfzcaYxsrKqc%2FnwpDRx5DQs9Sfpam1qpKupdfdxteYr8bWrY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**Javier Marín, integrante de la Asociación Minga: "**Es un líder social que desde su ámbito actúa para los derechos de las comunidades. No están únicamente en una organización sino en la generalidad de los líderes sociales que hay en el país".

<iframe src="http://www.ivoox.com/player_ek_9660029_2_1.html?data=mpujkpWWfY6ZmKiakpWJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRe4zhwtPS1MbXb8XZjMnSyM7SrdOfzcaYxsrKqc%2FnwpDRx5DQs9Sfpam1qpKqpcPd0JCu1M7Ft46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Fabio Arias, Secretario General de la Central Unitaria de Trabajadores, CUT:** "Es reivindicar una acuerdo mundial que hay sobre los derechos y que desafortunadamente en Colombia seguimos incumpliéndolo”.

 
