Title: El 9 de abril se movilizarán más 70 mil personas en Bogotá
Date: 2015-04-07 16:07
Author: CtgAdm
Category: Nacional, Paz
Tags: 9 de abril, Bogotá, Conversacioines de paz en Colombia, Cumbre Agraria, Cumbre mundial de artistas por la paz, Diego Armando Maradona, ELN, FARC, Frente Amplio por la PAz, José "Pepe" Mujica, Marcha por la Paz, Movilízate por la paz, Partido por la Paz, Ruta de la Marcha por la Paz en Colombia
Slug: en-bogota-se-movilizaran-alrededor-de-70-mil-personas-el-9-de-abril-memuevoporlapaz
Status: published

###### Foto: Contagio radio 

<iframe src="http://www.ivoox.com/player_ek_4320774_2_1.html?data=lZifkpybeI6ZmKiakp2Jd6Knk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Bfo9TU0dmJh5SZopaY1cqPsdDqytHW3MbWaaSnhqae0JDFsNPZxcrR0dePqMafmJWYz87Qb9HZ09jc0MbXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Andrés Álvarez, Equipo logístico de la movilización] 

Organizaciones sociales y populares reunidas en el Frente Amplio por la Paz han realizado amplias convocatorias para movilizarse el próximo 9 de abril en diferentes ciudades del Colombia, entre las que se encuentras **Barranquilla, Bucaramanga, Medellín, Cali, Neiva, Pasto y Bogotá**. La capital del país espera recibir al rededor de 70 mil personas de los departamentos del Meta, Arauca, Guainía, Casanare, Boyacá y Cundinamarca.

Estas delegaciones llegarán a partir de las 5 de la mañana al Centro de Memoria, Paz y Reconciliación, punto de concentración de la movilización en Bogotá. Allí se realizará la instalación de la movilización a las 10 de la mañana, que contará con saludos por parte de las delegaciones regionales y las organizaciones convocantes.

La marcha tomará la calle 26 y tomará la carrera 60 hasta el parque Simón Bolivar, donde se realizará el Concierto por la Paz a partir de las 2 de la tarde. En el evento central estarán presentes el Ex presidente de Uruguay **José "Pepe" Mujica** y el astro del Futbol **Diego Armando Maradona**, quien también jugará el viernes el "Partido por la Paz".
