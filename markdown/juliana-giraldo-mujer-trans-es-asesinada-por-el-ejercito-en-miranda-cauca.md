Title: Ejército asesinó a Juliana Giraldo, mujer transgénero en Miranda Cauca
Date: 2020-09-24 15:18
Author: AdminContagio
Category: Actualidad, Nacional
Tags: asesinato, Cauca, Ejército, mujer trans
Slug: juliana-giraldo-mujer-trans-es-asesinada-por-el-ejercito-en-miranda-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Captura-de-Pantalla-2020-09-24-a-las-2.37.28-p.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 24 de septiembre en zona rural de Miranda, Cauca se registró el asesinato de **Juliana Giraldo, mujer trans, luego de una intervención del Ejército Nacional.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/michelleobandoc/status/1309155512722829315","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/michelleobandoc/status/1309155512722829315

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho se registró sobre las 8:30 am mientras Juliana Giraldo en compañía de tres personas se movilizaban en un vehículo qué fue atacado por integrantes del Ejército, y **en hechos que aún son motivo de investigación uno de los militares activo su arma e impacto un proyectil en la cabeza de Juliana, causando su muerte .**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por medio de un video grabado por uno de los acompañantes de Juliana momentos después de la agresión, se registra como este solicita ayuda, mientras indica que el Ejército había matado a su compañera en un acto en el que tanto ella como los ocupantes del vehículo se encontraban vulnerables.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Ayúdenme, por favor. Este es el que me la mató. No tenemos armas, no tenemos drogas, no tenemos nada"***
>
> <cite>Apartado del video en donde habla uno de los compañeros de Juliana </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Al mismo tiempo, a través de un comunicado de prensa el Comando Específico del Cauca, indicó que **el hecho registrado en Miranda se dio en medio de labores de control militar** del área del batallón de alta montaña número 8 José María Vega.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"En este accionar en donde una mujer resultó muerta a causa de un disparo por parte del integrante de la Fuerza"*, la **unidad militar pone a disposición de la justicia al uniformado involucrado en el asesinato**. ( A[lejandro Carvajal, joven de 20 años asesinado por el Ejército en Catatumbo: ASCAMCAT](https://archivo.contagioradio.com/alejando-carvajal-joven-de-20-anos-asesinado-por-el-ejercito-en-catatumbo-ascamcat/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Testigos y usuarios señalan que que **no se trató de un operativo, ni tampoco de un retén militar** en donde la víctima y sus acompañantes no quisieron colaborar.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JANDR3S__/status/1309181773784518661","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JANDR3S\_\_/status/1309181773784518661

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Esto se corrobora con la versión de otro testigo que señaló que los militares salieron del cañaduzal y sin dar tiempo de reaccionar a las personas que transitaban por el sector, los militares abrieron fuego.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aún **es motivo de investigación establecer** si se trató de un [hecho de homofobia](https://colombiadiversa.org/base-datos/nacional/)por parte de los integrantes del Ejército, que finalizó en un tranfeminicidio.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/redcomunitariat/status/1309201593691185154","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/redcomunitariat/status/1309201593691185154

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Al mismo tiempo, usuarios y organizaciones llamaron a la movilización social en rechazo al accionar violento por parte de las fuerzas armadas contra la población civil en Colombia, uno de estos actos se realizará por medio de un plantón en el Batallón Pichincha en la ciudad de Cali.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Sebas960507/status/1309208605409845249","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Sebas960507/status/1309208605409845249

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
