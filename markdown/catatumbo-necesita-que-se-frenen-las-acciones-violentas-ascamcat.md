Title: "Catatumbo necesita que se frenen las acciones violentas" ASCAMCAT
Date: 2018-04-24 16:26
Category: DDHH, Política
Tags: Catatumbo, ELN, EPL
Slug: catatumbo-necesita-que-se-frenen-las-acciones-violentas-ascamcat
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Catatumbo-Contagio-Radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [24 Abr 2018] 

El gobernador del departamento de Norte de Santander, William Villamizar declaró la Calamidad Pública en la región del Catatumbo, debido al desplazamiento forzado de más de 7.000 mil personas y la creación de **15 refugios humanitarios, producto de los enfrentamientos entre el ELN y el EPL**.

Los habitantes denunciaron que ya está escaseando el alimento en las comunidades debido a que no pueden salir de sus **casas por miedo a quedar atrapados entre el fuego cruzado y porque se ha restringido la movilidad**. De igual forma, los cultivos de los campesinos se estarían perdiendo debido a que no hay cómo sacarlos a la venta.

De acuerdo con Olga Quintero, vocera de ASCAMCAT, el pasado domingo 22 de abril, se realizó una asamblea en la que participaron campesinos, afros e indígenas, con la finalidad de exigir tanto al EPL como al ELN, que cesen las confrontaciones en el territorio y al gobierno que frene la militarización del territorio. (Le puede interesar: ["Gobierno se niega a reconocer conflicto armado en el Catatumbo: Organizaciones Sociales"](https://archivo.contagioradio.com/gobierno-se-niega-a-reconocer-conflicto-armado-en-el-catatumbo-organizaciones-sociales/))

De no ser acatadas estas exigencias, la comunidad ha manifestado que se podrían crear corredores humanitarios para garantizar que tanto alimentos **como productos de uso básicos lleguen a las personas que actualmente se encuentran confinadas**.

### **La responsabilidad del Gobierno en el conflicto del Catatumbo** 

El día de ayer se realizó una reunión en Ocaña, en la que participaron organizaciones sociales, el vicepresidente Oscar Naranjo, el ministro Rivera, los alcaldes de los municipios y la gobernación, en donde se planteó la posibilidad de crear un puesto de mando unificado y de crear una comisión de interlocución **que medie con los grupos insurgentes**. Hasta el momento se habría dado un primer paso con establecer una mediación con la guerrilla del ELN.

Frente al accionar por parte del gobierno, Quintero manifestó la región del Catatumbo ha estado “históricamente” en situación de abandono y olvido por parte del Estado, prueba de ello son los incumplimientos no solo en compromisos a los que se había llegado en instancias como la Mesa Social y Comunitaria, sino también en programas como de sustitución de cultivos de uso ilícito, entre otros.

<iframe id="audio_25601939" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25601939_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
