Title: Congresistas de la Unidad Nacional modificaron la JEP afectando derechos de las víctimas
Date: 2017-03-15 18:09
Category: Nacional, Paz
Tags: crímenes de estado, Jurisdicción Espacial de Paz, víctimas
Slug: congresistas-la-unidad-nacional-modificaron-la-jep-afectado-derechos-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/44df7097-6c8f-4c6d-a6e6-49999afd59ef-e1489618959388.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MOVICE 

######  [15 Mar 2017] 

Luego de que la plenaria de Senado aprobara el proyecto de Ley que regula el Sistema Integral de Verdad, Justicia, Reparación y No Repetición, del que hace parte la Jurisdicción Especial para la Paz, JEP, el Movimiento de Víctimas de Crímenes de Estado, MOVICE, ve con sabor agridulce algunas de las proposiciones realizadas por congresistas de la bancada de la Unidad Nacional y avaladas por el Gobierno, que “modificaron sustancialmente elementos del Acuerdo Final y afectaron de manera negativa los derechos de las víctimas de crímenes de Estado”, aseguran.

### Sobre los financiadores de grupos paramilitares 

**Una proposición realizada por el senador Germán Varón Cotrino y aprobada por el Congreso** rechaza la posibilidad de investigar, juzgar y sancionar la participación directa e indirecta de terceros en la financiación de grupos paramilitares. De acuerdo con el comunicado del MOVICE, esa proposición determina que solo se juzgarán como financiadores del paramilitarismo a los civiles cuya participación haya sido determinante** **en la comisión de crímenes de guerra y de lesa humanidad, reduciendo la autonomía judicial para valorar pruebas. Es decir que la JEP ahora sólo podrá sancionar a quienes se les compruebe una relación directa entre su financiación y la comisión de un crimen concreto.

El MOVICE afirma que este cambio en la JEP, “olvida que la financiación del paramilitarismo en su conjunto garantiza el funcionamiento sostenido de grupos que se dedican sistemáticamente a la comisión de crímenes de carácter internacional y de graves violaciones a los derechos humanos. Sin desmonte del paramilitarismo no habrá paz estable y duradera”. [(Le puede interesar: ciudadanía deberá ser veedora de la JEP)](https://archivo.contagioradio.com/la-ciudadania-debera-ser-veedora-de-la-jurisdiccion-especial-de-paz/)

### Sobre informes de las víctimas a la Sala de Reconocimiento de Verdad y Responsabilidad 

**El Senador Roosvelt Rodríguez,** ponente del Acto Legislativo, niega la posibilidad  de que las organizaciones de víctimas y de derechos humanos puedan presentar informes a la Sala de Reconocimiento de Verdad y Responsabilidad, como insumo para que el Tribunal para la Paz citara a comparecer a los presuntos responsables. La proposición deja de lado el trabajo de las víctimas  frente a la documentación realizada durante décadas por las organizaciones.

### Sobre la responsabilidad de agentes estatales en el conflicto 

Con el texto aprobado establece que sólo los soldados –como actores materiales de delitos- sean juzgados y sancionados, mientras que **los mayores responsables, civiles y altos mandos militares podrán gozar de total impunidad** por los crímenes cometidos bajo sus órdenes.

El MOVICE, resalta que los partidos que promovieron estas proposiciones son quienes más se han visto implicados en las investigaciones por parapolítica y han tenido que ver cómo varios de sus miembros han sido condenados, destituidos o procesados por la Justicia por sus nexos con grupos paramilitares. [(Le puede interesar: ¿Qué viene después de la aprobación d a JEP?](https://archivo.contagioradio.com/los-temas-que-vienen-en-el-tramite-del-acuerdo-de-paz-via-fast-track/))

Aunque reconocen y **saludan la incorporación de las proposiciones que incluyen el enfoque territorial y de género**, la participación de las víctimas en todas las etapas del Sistema Integral, la eliminación de la figura de la sostenibilidad fiscal como requisito para la reparación de las víctimas y la garantía de que esta sea una reparación integral y no sólo pecuniaria; las víctimas de crímenes de Estado exigen que la implementación del Acuerdo Final respete lo pactado entre las partes y lo ya previamente refrendado por Congreso, y reafirman su posición como veedores de lo pactado. [(Le puede interesar: JEP será una herramienta para las víctimas)](https://archivo.contagioradio.com/jurisdiccion-de-paz-sera-una-herramienta-para-las-victimas/)

###### Reciba toda la información de Contagio Radio en [[su correo]
