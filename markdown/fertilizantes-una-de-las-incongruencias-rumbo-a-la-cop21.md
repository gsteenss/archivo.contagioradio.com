Title: Fertilizantes químicos generan más contaminación que todos los carros de EEUU
Date: 2015-10-01 12:03
Category: Ambiente, El mundo, Entrevistas
Tags: Alianza Global para la Agricultura Climáticamente Inteligente, Barcelona, cambio climatico, Conferencia de las Partes, COP21, emisiones de gases de efecto invernadero, fertilizantes, GRAIN, Las Exxons de la Agricultura, Mosaic, Revolución Verde, Yara
Slug: fertilizantes-una-de-las-incongruencias-rumbo-a-la-cop21
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/informe-GRAIN-e1443719316391.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las Exxons de la Agricultura” GRAIN 

“**El uso de fertilizantes químicos durante este año generará, probablemente, ¡más emisiones de gases con efecto de invernadero que el total de emisiones procedentes de todos los automóviles y camiones que circulan en los Estados Unidos!”**, señala el más reciente informe de la organización GRAIN, donde además, se afirma que la única iniciativa intergubernamental para la COP21, que abordaría el tema de la relación entre el cambio climático y la agricultura, está controlada por las compañías de fertilizantes más grandes del mundo.

En el informe titulado **“Las Exxons de la Agricultura”,** se asegura que las grandes multinacionales petroleras deben envidiar la facilidad como **las transnacionales de los fertilizantes han logrado infiltrarse en el desarrollo de las acciones para enfrentar el cambio climático** con relación a la agricultura, que se discutirán en el mes de diciembre en la 21a Conferencia de las Partes (COP21) en París.

De acuerdo con el contenido del documento, entre los fundadores de la Alianza Global para la Agricultura Climáticamente Inteligente creada el año pasado en la Cumbre de las Naciones Unidas sobre Cambio Climático en Nueva York, se encuentran dos de las empresas de fertilizantes más grandes como lo son **Yara de Noruega y Mosaic de Estados Unidos.**

Con la Alianza, las compañías de fertilizantes han logrado “**bloquear cualquier acción importante en el área de agricultura y cambio climático”,** que pueda afectar sus negocios. Además se asegura que actualmente, **el 60% de los miembros del sector privado de la Alianza** aún provienen de la industria de fertilizantes.

Según estudios científicos que cita GRAIN en su informe, “el aumento del 17% de N~2~O (Nitrógeno) en la atmósfera, desde la era preindustrial, es el resultado directo de los fertilizantes químicos”, que se desarrollaron en el marco de la ”Revolución Verde de los años 60 que multiplicó de un modo impresionante el uso de fertilizantes químicos en Asia y América Latina”.

Así mismo, indican que “los científicos también saben que las emisiones de N~2~O resultantes de la aplicación de fertilizantes nitrogenados están en el rango del 3-5% de las emisiones totales, un aumento tremendo respecto al 1% estimado por el IPCC”, es decir, que **el sector de fertilizantes podría ser responsable de hasta un 10% de las emisiones globales de gases con efecto de invernadero.**

Este panorama, implica que si en la COP21 las compañías fertilizantes participan en la formulación de políticas públicas para combatir en cambio climático, es más que probable, que frente al problema de los fertilizantes como uno de los contribuyentes al aumento de la temperatura mundial, no se haga absolutamente nada, como lo advierte GRAIN.

[Grain 5276 Las Exxons de La Agricultura](https://es.scribd.com/doc/283345651/Grain-5276-Las-Exxons-de-La-Agricultura "View Grain 5276 Las Exxons de La Agricultura on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_14110" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/283345651/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Fpt3KXwIJjoMpX7EHxXE&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>
