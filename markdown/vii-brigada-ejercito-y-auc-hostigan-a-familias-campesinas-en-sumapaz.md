Title: VII Brigada Ejército y AUC hostigan a familias campesinas en Sumapaz
Date: 2016-06-29 12:37
Category: DDHH, Nacional
Tags: AUC, Ejército de Colombia, Fundación DHOC, paramilitarismo colombia, Sumapaz
Slug: vii-brigada-ejercito-y-auc-hostigan-a-familias-campesinas-en-sumapaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Sumapaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Espectador ] 

###### [29 Junio 2016 ]

Según el más reciente comunicado emitido por el Comité Ejecutivo Nacional de la Unión Patriótica, durante este mes se han presentado **intercambios de disparos, retenes, destrucción de bienes, saqueos a viviendas, persecuciones y señalamientos contra campesinos **del Centro Duda, en límites entre Meta y Cundinamarca, por parte de miembros de la VII Brigada del Ejército Nacional a quienes los pobladores han visto patrullar vestidos de civil y en compañía de presuntos miembros de las AUC, a juzgar por lo mensajes que han dejado en las paredes de algunas de las casas.

De acuerdo con Soleido Morales, de la Fundación DHOC, por lo menos 600 campesinos de 7 veredas están en peligro por cuenta de los **intercambios de disparos que se dan en inmediaciones de fincas y en horas de la madrugada**. El más reciente ocurrió el pasado 14 de junio, cuando miembros de la VII Brigada saquearon una de las viviendas y dejaron **mensajes escritos en las paredes firmados por las AUC**. Las comunidades denuncian que desde abril se han presentado intercambios así como bombardeos cerca a los nacimientos de agua.

Morales asegura que para las familias es muy preocupante el asedio constante por parte de los militares que han instalado retenes en varios puntos de la región en los que **toman fotografías a algunos de los campesinos y los señalan como colaboradores de la guerrilla**, teniendo en cuenta que en esta zona la presencia estatal es nula y que para ir de una vereda a otra se gastan hasta cinco días en caballo, porque las vías no son óptimas.

La situación ya fue denunciada ante la Fiscalía el pasado viernes, cuando las comunidades emitieron un comunicado a las dos delegaciones en la Mesa de Conversaciones y a diversas organizaciones defensoras de derechos humanos nacionales e internacionales, esperando que las autoridades del orden nacional, departamental y local **garanticen condiciones de seguridad para su vida.**

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
