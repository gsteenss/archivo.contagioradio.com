Title: Un primer vistazo a ‘La Defensa Del Dragón’
Date: 2017-05-18 11:46
Author: AdminContagio
Category: 24 Cuadros
Tags: cannes, Cine, colombia, Premios
Slug: cinecannescolombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/La-defensa-del-Dragón-2-e1495125340236.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Defensa del Dragón 

###### 18 May 2017 

La esperada película colombiana **"La Defensa del Dragón"** que estará en competencia en el Festival de cine de Cannes presentó su primer trailer oficial. Se trata sin duda de una de las principales cartas cinematográficas que tiene el país para mostrar al mundo durante el presente año.

La producción que **será estrenada en salas de cine el próximo 27 de julio**, hará parte de la **quincena de realizadores** del festival y competirá con otros títulos por la tan ansiada Cámara de Oro, galardón otorgado a la mejor ópera prima; reconocimiento obtenido en 2015 por Cesar Acevedo con "La tierra y la sombra".

![La defensa del Dragón 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/La-defensa-del-Dragón-1.jpg){.wp-image-40696 .aligncenter width="531" height="244"}

**Natalia Santa es la primera mujer colombiana que tiene participación en esta categoría dentro del festiva**l; además, la cinta **es la única iberoamericana que participa en la sección de este año**. Esto no deja de ser un hecho relevante, puesto que sin lugar a dudas, abre el camino y habla del importante rol que pueden ocupar las mujeres en el cine colombiano cuando se da la oportunidad de contar nuevas historias.

El argumento de la obra cuenta la historia de **Marcos, Joaquín y Samue**l, 3 personas que de un modo u otro se detuvieron espiritualmente en el tiempo y a través de apuestas en casinos, partidas de ajedrez, caminatas por el centro de Bogotá, acompañan unas charlas que extienden unas experiencias de vida en donde se defiende la cotidianidad y se critica la ciudad convulsa junto con un agitado proceso de modernidad.

<iframe src="https://www.youtube.com/embed/IQPfH-C20ko" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Con su nominación, **La Defensa Del Dragón espera dejar buenas impresiones dentro de la crítica y el público internacional**, que tiene una buena referencia de los títulos colombianos que han pasado por las salas de uno de los eventos cinematográficos más importantes en el mundo.

Otros directores relevantes que se han quedado con este premio en años anteriores son Jaco Van Dormael, Naomi Kawase, John Turturro y Steve McQueen. Le puede interesar: [La muestra internacional documental de Bogotá abre sus convocatorias](https://archivo.contagioradio.com/midboconvocatoria/).
