Title: Fondo de Mitigación de Emergencias desconoce gobiernos locales y beneficia empresas
Date: 2020-03-23 16:17
Author: CtgAdm
Category: Actualidad, Nacional
Slug: fondo-de-mitigacion-de-emergencias-desconoce-gobiernos-locales-y-beneficia-empresas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/descarga.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En los últimos días el presidente Iván Duque ha presentado varios decretos con lo que pretende atender el Estado de Emergencia Económica, Social y Ecológica de Colombia. Entre ellos el [Decreto 444](https://dapre.presidencia.gov.co/normativa/normativa/DECRETO%20444%20DEL%2021%20DE%20MARZO%20DE%202020.pdf)del 21 de marzo que ha sido cuestionado por analistas porque desconoce la gobernanza y la autonomía local y por el contrario beneficia al sector financiero.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/LeonVaLenciaA/status/1242100138644066311","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/LeonVaLenciaA/status/1242100138644066311

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Este decreto habla sobre la creación de el **Fondo de Mitigación de Emergencias** (FOME), y genera las disposiciones en materia de recursos de las regiones, prohíbiendo así a los entes territoriales usar este dinero, que será trasladado a un nuevo fondo nacional donde su destino es buscar el apoyo a la liquidez transitoria de sector financiero. A su vez este será administrado por el Ministerio de Hacienda, encabezado por Alberto [Carrasquilla](https://archivo.contagioradio.com/nueva-reforma-tributaria-significara-crecimiento-economico-para-los-mismos-de-siempre/).

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/harmanfelipe/status/1242107874781913095","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/harmanfelipe/status/1242107874781913095

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Esto significa que recursos del Fondo de Ahorro y Estabilización (FAE) y Fondo Nacional de Pensiones de las Entidades Territoriales (Fonpet), no serán de acceso para los gobiernos locales y el abastecimiento territorial en medio de la crisis, sino del Gobierno Nacional y los intereses que este disponga.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Un fondo Nacional para los Bancos?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Ante la crisis por el Covid-19 el Gobierno Nacional debió contemplar que los entes territoriales pudieran disponer de un porcentaje de los ahorros acumulados en el FAE, una normativa que hubiese favorecido principalmente a los departamentos que son los titulares de estos recursos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recursos ascienden en el caso del Distrito, a cerca de US\$ 64 millones, osea \$263 mil millones. Aclarando que el FAE tiene US \$3.729 millones de dólares, aproximadamente 11 billones de pesos; dinero el cual según el Decreto puede ser usado por la Nación en créditos de hasta el 80% del valor del FAE.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte en el Fonpet, el decreto plantea que los recursos de este fondo podrán ser fuentes del Fondo de Mitigación de Emergencias (FOME). Ante esto la administración de Bogotá ha señalado al Ministerio de Hacienda y Crédito Público que existen excedentes de la cuenta salud que en las actuales circunstancias serían cruciales para financiar las acciones sectoriales y hacer frente a la emergencia del Covi-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Recursos superiores a **\$200 mil millones**, pero que no se han podido disponer debido a una interpretación equivocada sobre la situación de las subredes, las cuales fueron fusionadas y no liquidadas, como lo ha interpretado el Gobierno. (Le puede interesar: <https://archivo.contagioradio.com/empresas-deben-garantizar-bienestar-y-estabilidad-de-trabajadores-cut/>)

<!-- /wp:paragraph -->
