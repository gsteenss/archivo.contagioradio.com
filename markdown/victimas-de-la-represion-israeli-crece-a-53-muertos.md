Title: Aumentan a 53 los palestinos asesinados por el régimen israelí
Date: 2015-10-22 12:34
Category: DDHH, El mundo
Tags: 15 palestinos han fallecido por disparos israelíes en Gaza., 53 palestinos asesinados, Benjamín Netanyahu, Cisjordania, Ejército Israelí, Franja de Gaza, régimen de Tel Aviv, Tel Aviv en la Explanada de las Mezquitas
Slug: victimas-de-la-represion-israeli-crece-a-53-muertos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/1405579926_000_Nic6349994.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: www.laprensa.com.ni]

###### [22 oct 2015] 

Aumenta a 53 la cifra de palestinos asesinados desde el pasado 1 de octubre hasta la fecha por cuenta de los ataques del régimen israelí en territorios ocupados como **la provincia de Al-Quds, Jerusalén y la Franja de Gaza. **

A través de un comunicado emitido por el Ministerio de Salud palestino, se informa **que 37 palestinos han perdido la vida por disparos emitidos por soldados israelíes en Cisjordania, los otros 15 han sido asesinados en Gaza.** A su vez, se denunció que el pasado 14 de octubre, en las ciudades de Cisjordanas de Al-Jalil (Hebrón) y Ramalá, fueron hallados los cuerpos de dos palestinos, uno de ellos habría muerto por inhalar gas tóxico y el otro por disparos generados por los israelíes.

Por su parte, la UNESCO, denunció que **los palestinos han sido atacados en más de 40 ocasiones por agresiones de Tel Aviv en la Explanada de las Mezquitas,** en las que desde el pasado 13 de septiembre se empezó la expulsión de palestinos agrediéndolos con gases lacrimógenos y disparos.

Cabe recordar, que Benjamín Netanyahu anunció que seguirá reprimiendo a la población palestina, y anunció el reforzamiento de las medidas de seguridad en los territorios ocupados.
