Title: Más de 200 profesionales firman "carta a los indecisos" por un cambio en próximas elecciones
Date: 2018-06-03 15:15
Category: Nacional, yoreporto
Tags: colombia, elecciones 2018
Slug: mas-200-profesionales-firman-carta-los-indecisos-favor-del-cambio-proximas-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/indecisos-carta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: las2Orilas] 

###### [03 Jun 2018]

Más de 200 profesionales de la cultura y la ecología firman carta a favor del cambio en la próximas elecciones. Compartimos el texto completo.

"Colombia, Mayo 31, 2018

Carta a los indecisos

Nos convocan hoy los colombianos que no somos nosotros, los que están allá lejos, detrás de los muros invisibles del bienestar de las ciudades, los líderes comunitarios que protegen la cuenca del río que les da de comer, que les permite regar sus sembrados, los niños que esperan ser adoptados por alguien que sea capaz de amarlos sin importar su orientación sexual, los jóvenes que no pudieron entrar a la universidad porque nadie les dio para comprar el formulario de ingreso y mucho menos para la matrícula, los enfermos que no reciben su medicamentos, las madres que no pudieron volver a ver a sus hijos porque fueron enterrados en fosas comunes vestidos de guerrilleros despojados de su identidad y del derecho a ser sepultados dignamente.

Nos convoca hoy la posibilidad de cambiar la eterna historia de 5 familias en el poder quitándonos todo lo que nos pertenece, nuestras tierras, nuestra paz, nuestra educación y todos los derechos fundamentales, los mínimos, los básicos; el derecho al nombre, al médico, a la cosecha, al colegio, al agua, al aire, al árbol, a la montaña. Nos convocan hoy Hidroituango desplomándose, La Colosa contaminando las aguas, el puerto de Tribugá acabando con los manglares, el Cerrejón ennegreciendo la tierra, secando la Alta Guajira, condenando a los indígenas wayuu a la muerte inminente por desnutrición.

Si, también nos convoca la segunda vuelta en elecciones presidenciales enfrentados a una bifurcación de caminos. Como pasaba justo antes de ver morir asesinado a Galán, a Pizarro, a Jaime, hoy vemos otro camino diferente, el único posible en medio de tanto desastre y ese camino es nuestro NO a los políticos corruptos que sólo buscan su propio bienestar y salir de sus cargos con una cuenta multimillonaria en Panamá y varios apartamentos en Abu Dabi.

Nosotros, que hemos ido a las regiones buscando a los maestros de las músicas tradicionales, que hemos subido a la sierra a escuchar a los mamos, que hemos bailado en festivales tradicionales en medio del dolor de la guerra, que nos hemos sentado a ver como se hace un sombrero en cañaflecha o como se mantiene una semilla en el alto Andágueda, que hemos escrito las historias de los excluidos, los abandonados, los disidentes, lo reinsertados, queremos decirles hoy que así como creen en nuestros proyectos, escuchan nuestra música, leen nuestros libros, disfrutan nuestras obras, viven la cultura que día a día nos esforzamos en construir, salvaguardar y no dejar morir, así mismo les pedimos que crean en nuestro llamado a votar el próximo 17 de Junio en contra de los que nos matan y la única manera que ellos no sigan perpetuándose en el poder es votando por Petro. No les pedimos que estén de acuerdo con él, ni con todas su propuestas, les pedimos que le den una oportunidad a un cambio posible en este difícil momento que nunca pensamos que veríamos para contarlo, que voten para que no nos sigan matando.

Ayúdennos a ser las generaciones que lograron cambiar el rumbo, con su apoyo, estamos seguros que podremos!

Los y las artistas, músicos, gestores culturales, teatrer@s, pintores, escultores, comunicadores, periodistas, escritores, poetas, cultores, juglares, actores, bailarines, ambientalistas y ecólog@s nos comprometemos con la paz, y tu?

Firman:

1\. Silvie Ojeda – Realizadora Audiovisual y radial – Radio Mixticius  
2. Jacobo Velez – Músico – La Mambanegra  
3. Pedro Ojeda – Músico - RompeRayo, Los Pirañas  
4. Andrés Gualdrón – Músico – Los Animales Blancos  
5. Urián Sarmiento – Músico – Curupira  
6. Nidia Góngora – Grupo Canalón  
7. Camila Hernandez – Música  
8. Andrés Pascuas – Escritor – creador – gestor  
9. Javier Galavis – La 33  
10. Mateo Ocaña – DJ  
11. Ramiro Velazco - Titiritero del Taller de los Cacharros  
12. Lucía Ibañez – Sonidos Enraizados  
13. Juan Manuel Toro – Hombre de barro  
14. Andrés Aceves – Realizador DJ – Radio Mixticius  
15. Angel Salazar – Rapero – Todo Copas  
16. Damian Ponce – músico  
17. Daia Mutis – Cantante  
18. Mateo Molano – Músico  
19. Santiago Lozano – Compositor  
20. Fernando Torres - Doktor Chiflamicas - La Folka Rumba  
21. Victoria Sur – Cantante  
22. Juliana Ladrón de Guevara – Ralizadora Audiovisual  
23. Jaiver Corrales - Corresponsal en Perú  
24. Vania Otero Gelabert- musico  
25. Camilo Ara – Fotógrafo  
26. Juan Sebastián Rojas – Músico  
27. Walter Pacheco – Productor  
28. Sebastián Carrizosa – Músico  
29. Jaime Ospina – Músico  
30. Jhon Socha – Músico  
31. Edgard Benitez – Músico  
32. Germán Betancourt director de teatro.  
33. Bertha Quintero Medina – Música  
34. Francisco Rojas – Fair Tunes  
35. Dafne Sofía Rojas - Música  
36. Oscar Jimenez - Músico - Amaru Tribe  
37. Piedad Guerrero Coka - performer activista.  
38. Gabriela Sossa - Música - Corrientes y La Voz de la Marimba  
39. Margarita Guevara - Restauradora y conservadora del patrimonio  
40. Juan Pablo Varela - DJ  
41. René Castellanos - Director de cine  
42. Catalina Gutierrez - Musico  
43. Patricia Moreno - Directora creativa  
44. César Medina - Músico  
45. Amy Zuluaga - Gestora Cultural , Lider de Género Mv. Hip Hop  
46. Gonzalo Hernandez - Chef  
47. Mateo Rudas - Realizador Audiovisual  
48. José Miguel Vega - Músico  
49. Leonel Rodriguez - Músico  
50. Mauricio Mendieta - Gestor cultural - Ariari, Meta.  
51. Jose Luis Ruiz - Gestor Cultural  
52. Oscar Andrade Medina - Productor de Animación Digital  
53. Carlos Gutierrez - Gestor  
54. Nicolas Garzón - Músico  
55. Mauricio Bayona - Realizador audiovisual-Los Herederos  
56. Nicolás Vallejo - Editor  
57. Sandra Lopez- Artista visual  
58. Guillo Cros - Músico  
59. Andrés Benavides- Músico  
60. Haida Ramos- Actriz  
61. Santiago Rudas - Músico  
62. Jaime Murillo - Ecólogo  
63. Lucia Reyes -Actriz  
64. Manuela Jara - Bailarina  
65. Alix Lesmes - Gestora Cultural  
66. Valeria Benavidez - Gestora cultural y social  
67. Camilo Cubillos - Músico  
68. Andrea Delgado -Diseñador  
69. Freddy Colorado - Músico  
70. Juan Guillermo Aguilar-Músico  
71. Cesar Herrera - Gestor Cultural  
72. Hugo Moreno - Músico  
73. Milena Zárate - Gestora social  
74. Catalina Avila - Artista  
75. Nicole Tenorio -Vighnesa Natanam y Manusdea Antropología Escénica, bailarina  
76. Manuela Ruiz Reyes - Ambientalista  
77. Angela Olarte - Música  
78. Maggie Alvarado - Productora  
79. Santiago Botero - Contrabajista y pedagogo  
80. Emmanuel Rede - Músico - Afrotumbao  
81. Ana María Arango Melo- Antropóloga  
82. Alejandra Bonilla - Artista  
83. Hugo - Ospina  
84. Daniel Broderick - Músico  
85. Irene Rodríguez - Artista  
86. Santiago Jiménez - Músico - Tristán Alumbra  
87. María Helena Pérez - Productora Audiovisual  
88. Epiara Murillo – Músico  
89. Luis Fernando R. Uyabán = Músico  
90. Vanessa Vivas - Gestora Cultural  
91. Germán Ayala. Antropólogo - Realizador Audiovisual  
92. Carolina Rincón.  
93. Natalia Montes G, actriz  
94. Myriam Ojeda Patiño Directora Artistica  
95. Juan Camilo Cano – Periodista  
96. Erika Antetequera – Periodista  
97. Lina Kathya Rodríguez – Ambientalista  
98. Tania Fuentes – Agro-ecóloga  
99. Paola Marquez -Música  
100. David Riveros - Diseñador, Productor & DJ  
101. Asor Artista multidisciplinar  
102. Milo Cabieles, bailarina  
103. Nicolás Eckardt – Músico  
104. Mercurio Bossio – Músico  
105. Maria Juliana Soto - comunicadora social  
106. David Puerto – Músico  
107. Mauricio López Arenas- politólogo, activista, gestor  
108. Michael Wagner - Músico  
109. Esteban Galindo - Comunicador Social, Productor.  
110. Paola Romero - artista visual  
111. Laura Torres: Gestora cultural - Sonando Sonamos  
112. Paola Duarte Giraldo Actriz  
113. Sara Martelo-Musico clarinetista  
114. Adriana Morales- Música  
115. Carolina Bustos Beltrán – Poeta  
116. Sairi T. Piñeros – Geógrafa  
117. Johan Esteban Peñuela - Musico LaSonocroma  
118. Santiago Ferrer -Musico  
119. Angela Bravo - Directora de Arte  
120. Yeison moreno músico y bailarín  
121. Nathaly Espitia Díaz- comunicadora social- Noís Radio  
122. David mesa - musico y gestor cultural  
123. Julián Mayorga - Músico  
124. Maria Cepeda Castro - Sociologa, defensora de DDHH  
125. Fabian Diaz - Músico y Compositor  
126. Carolina Muñoz-cantante  
127. Jairo Rodriguez - Musico - MitH FotU Records  
128. Denny Ruge Prado "Somos Chibchas"  
129. Sargento Garcia \_ músico, cantante.  
130. Marco Suarez-Cifuentes, compositor e investigador  
131. Víctor Acosta Santamaría – Investigador Científico.  
132. Pablo Canales - Músico y Profesor  
133. Andrea Noriega Andrea Noriega - ecóloga  
134. Daniel Cabanzo Daniel Cabanzo - Compositor - Artista sonoro - Investigador  
135. Juan Manrique- Compositor Musical  
136. Juan Manrique Silva · Artista plástico.  
137. Anariana Tenorio Anariana Tenorio-Platera, joyera y gestora cultural  
138. Rayo Lujan artista escenica  
139. Ana María Ulloa Reyes- Cantante  
140. Diego Marulanda - músico, canta autor  
141. Alexander Correa. Obrero audiovisual  
142. Natalia Avella - pianista y docente  
143. Daniel Osorio Peña - Antropólogo  
144. Susana giraldo- Directora de arte  
145. Erika Castro Becerra - socióloga -Proyecto Guakamayo  
146. Natalia Muriel - Bailarina  
147. Aniyari Zaodiela Carolina Raigoza - pedagoga  
148. Natalia Parrado, musicóloga  
149. Carlos Rosales - Músico Flying Bananas  
150. Laura Barragán Rodríguez - bailarina  
151. Cecilia Silva -cantante.  
152. Mario Alberto Rodriguez Gracia. Músico y Docente.  
153. Alejandra Ortiz - música y maestra  
154. Mauricio Báez Buitrago - Técnico audio y producción musical  
155. Carlos Ramírez, músico, docente.  
156. Julio Orozco - Sociólogo  
157. Carolina Perdomo - Bióloga Marina.  
158. Otoniel Ramirez Moreno-Biologo  
159. Enrique Mendoza - Músico  
160. Monina M Rincón  
161. Ana Maria Paeres Aguirre - Ecologa  
162. Enrique Molano J - musico  
163. Jenny Cárdenas Muñoz Jenny Cárdenas Muñoz - Bióloga  
164. Sonia Barrera - productora audiovisual  
165. Astrid Villa Música - maestra.  
166. Liliana Cifuentes . Fotógrafa  
167. Marta Silvia Villegas. Antropóloga.  
168. Antonia Bustamante. Música/sonidista  
169. MariCristina Guerrero- Gestora Cultural  
170. Juliana López -Diseñadora gráfica  
171. Mariangela Aponte - artista  
172. Felipe Suárez Felipe Suarez - Realizador  
173. Fredy Vallejos - Músico y docente  
174. Natalia Echeverri, periodista-  
175. Cesar Agudelo - Artista Visual  
176. Ivan Medellin – Músico  
177. Alejandro Durán - Músico - El Supersón Frailejónico / Banda de flautas Chicha y Guarapo  
178. Camila Rodríguez, investigadora y directora de Convergencia De Saberes y Acción Territorial  
179. Joanna Gómez - realizadora audiovisual y guionista.  
180. Violeta Cruz - Compositora.  
181. Ariel Arango - realizador audivisual y fotógrafo  
182. Angela Carmona Barón - diseñadora/escritora  
183. Tatiana Meza - Bióloga  
184. Ivan Garcia - Productor Audiovisual  
185. Natalia Gualy - Realizadora Audiovisual  
186. Beatriz Helena Robledo-escritora  
187. Natalia Guzmán- Artista y gestora cultural  
188. Maritza Sánchez - Comunicadora social  
189. Mónica Hernández - Artista Interdisciplinaria  
190. Pedro Garcia-Velasquez - compositor  
191. Lorena Espitia Lorena Espitia docente y activista en pro de los animales.  
192. Constanza Elena Prada Constanza Elena Prada - Hogar temporal de animales  
193. Anabell Palacios Peñaranda. Docente  
194. Mónica Caicedo, docente  
195. Johandris Ospino Barranco - musico  
196. Madeleine Barona - Bióloga - Especialista Ambiental  
197. Dafna Vallejo - Diseñadora gráfica  
198. Sergio Serrano - fotógrafo  
199. Juan Sebastián Rojas - Músico, investigador.  
200. Santiago Martínez Caicedo, realizador audiovisual  
201. Juan Medina - Director, Boogaloop Club  
202. Juan Ignacio Muñoz - Director de Fotografía  
203. Angélica Morales - Realizadora Audiovisual  
204. Ricardo Gallo Ricardo Gallo - Músico.  
205. Lucía Gamba Diana Gamba - Bióloga botánica  
206. Marcela Peñuela - Diseñadora de moda  
207. Felipe Vallejo Felipe Vallejo- Gestor  
208. Juliana Sánchez Editora audiovisual  
209. Ricardo Cubides Historiador  
210. Natalie Adorno - antropóloga  
211. Iván Rickenmann, artista plástico  
212. Casandra Hernandez - Directora de arte y Diseñadora Escénica  
213. Diana Banquez - Publicista  
214. Laura Andrea Castrillón Torres - Productora Musical.  
215. Adiela Osorio, Red de Mujeres Excombatientes  
216. Nicolás Cristancho- Músico - Yurgaki  
217. Marco Fajardo Marco Fajardo - Músico  
218. Milton Piñeros - ingeniero de sonido  
219. Aldemar Ruiz Gallego- Realizador Audiovisual .  
220. David Figueroa Martínez - Arquitecto / Diseñador.  
221. Andrés Buitrago - Cineasta  
222. Angélica Gómez. Semejante trazo.  
223. Juan Carlos Victoria – Músico -Acento Mestizo  
224. Julián Velandia , documentalista  
225. Katheryn Guzmán - Actriz  
226. Gabriel Zea, artista  
227. Alejandro Medina - diseñador  
228. Liliana Guzmán - escritora, libretista.  
229. Cesar Queruz - músico  
230. Johanna Nieto- gestora  
231. Marcia Juzga - Realizadora Audiovisual  
232. Clara García - Realizadora audiovisual - gestora  
233. Che Guerrero Gestor Cultural  
234. Lorena Morris Rincón - Artista plástica  
235. Carolina Torres Topaga - artista escénica  
236. Felipe Quintero - producción radial - stereojoint  
237. Valentina Blando - música y actriz  
238. Rosaura Villanueva - Realizadora audiovisual  
239. Beatriz Sterling - artista y docente.  
240. Ana Carolina Arcila, actriz y docente.  
241. Julián Beltrán Acero, investigador y gestor cultural.  
242. Carlos Rozo Prada. Actor y Director.  
243. María Adelaida Londoño, periodista.  
244. Alejandro Matallana - Realizador Audiovisual  
245. José Salgado Jiménez – Música  
246. Isabel Torres - Cineasta y bailarina.  
247. Victor Avella - Músico  
248. Adriana Alvarado - ingeniera ambiental  
249. Alejandra Ortiz- ingeniera ambiental  
250. Paola Zuluaga Palacios - Realizadora audiovisual  
251. Simon Martinez Abadia - Músico  
252. Diego Gomez - Músico  
253. Elkin Robinson - Músico"
