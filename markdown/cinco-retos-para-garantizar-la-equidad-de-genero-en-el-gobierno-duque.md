Title: Cinco retos para garantizar la equidad de género en el gobierno Duque
Date: 2018-07-04 14:49
Category: Mujer, Nacional
Tags: colombia, Congreso de la República, mujeres, Senado
Slug: cinco-retos-para-garantizar-la-equidad-de-genero-en-el-gobierno-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/mujeres-empoderadas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [04 Jul 2018] 

Las mujeres que llegan al Congreso de la República tendrán que afrontar diversos retos para garantizar la participación de las mujeres en la democracia del país y potencializarla. De acuerdo con Blanca Cardona, directora nacional del área de gobernabilidad democrática del Programa de las Naciones Unidas para el Desarrollo, los desafíos más grandes serán **el fortalecimiento de política públicas de género, el empoderamiento económico de las mujeres y la lucha contra la violencia.**

Estas reflexiones hacen parte de la Cumbre por la Igualdad, un escenario de encuentro de diversas organizaciones tanto estatales como defensoras de derechos de las mujeres, que pretenden generar un balance de los avances o retrocesos frente a la participación de las mujeres en la democracia, **no solamente desde el Congreso de la República, sino también en todos los niveles de empoderamiento.**

En ese sentido, Cardona manifestó que si bien hay avances en términos de la legislación en defensa de los derechos de las mujeres, aún queda un largo camino por lograr que exista paridad a la hora de llegar a los cargos dentro del Congreso de la República. Esto debido a que de 305 mujeres que se inscribieron a las listas de Cámara y Senado, **solo 35 lograron ocupar estas curules.**

### **Los desafíos de estos 4 años para el empoderamiento de las mujeres** 

Cardona aseguró que Colombia necesita con urgencia fortalecer una institucionalidad ejecutiva que permita la puesta en marcha de políticas de género en el país, que cuenten con los recursos suficientes para su manejo. Una de estas políticas que debe ser priorizada, según organizaciones defensoras de los derechos de las mujeres, debe **ser la política pública de mujer rural, que necesitaría un mayor enfoque territorial.**

De igual forma, afirmó que en el país aún existe una gran brecha en la desigualdad salarial, que evita que las mujeres ganen lo justo por el trabajo que realizan y en las mismas proporciones que los hombres. Finalmente, pero no menos importante, **se encuentra la violencia contra las mujeres que continúa cobrando vidas en Colombia**.

Al finalizar este espacio se espera que las congresistas que se desempeñaron en el periodo anterior realicen un pequeño empalme con las mujeres que ahora asumirán este camino en defensa de los derechos humanos de las mujeres. (Le puede interesar:["Estado falla en la protección de los derechos de las mujeres y las niñas"](https://archivo.contagioradio.com/estado-ha-fallado-en-la-proteccion-de-los-derechos-de-las-mujeres-y-las-ninas/))

<iframe id="audio_26895618" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26895618_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
