Title: ELN revela detalles de la muerte de Camilo Torres
Date: 2016-02-19 17:52
Category: Nacional
Tags: camilo torres, camilo torres 50 años, ELN
Slug: eln-revela-detalles-de-la-muerte-de-camilo-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/mensaje-del-eln-camilo-torres.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: video 

###### 19 Feb 2016 

El comandante del ELN, Nicolás Rodríguez Bautista, “Gabino”, explica el contexto de la muerte de Camilo Torres Restrepo, sacerdote, sociólogo y líder político que se vinculó a esa guerrilla y que murió hace 50 años en el sector del Patio Cemento en el municipio de Carmen del Chucurí en Santander. Según “Gabino” Camilo Torres habría caído en combate en medio de primera misión militar que cumplía.

El video, que llegó a los estudios de Contagio Radio y de Colombia Informa y que fue grabado en el sitio de la muerte de Camilo Torres, el sacerdote es presentado como un **líder revolucionario que no encontró en la sociedad de ese momento la manera de cambiar las injusticias a través de la política** y por ello decidió partir a la lucha guerrillera.

Según “Gabino” la intensión de Camilo Torres era “graduarse de guerrillero” para luego partir a los llanos orientales a seguir alimentando la lucha. Relata Gabino que su afán de curtirse en la lucha guerrillera lo llevó al combate el 15 de febrero de 1966 en el que murió, y en el que estaba acompañado de un número reducido de integrantes del ELN, casi todo inexpertos.

En el video organizado con cortas exposiciones, Gabino explica, el contexto en el que se dio la vinculación de Camilo Torres a las filas del ELN y menciona las luchas de los trabajadores petroleros en Barrancabermeja, así como las reivindicaciones estudiantiles que tuvieron su centro en la Universidad Industrial de Santander.

### **El combate de Patio Cemento** 

Gabino explica que la Quinta Brigada al mando del Coronel Álvaro Valencia Tobar, lanzó un fuerte operativo sobre la zona del llamado “Rancho número 1”, el primer campamento del ELN en esa zona. El objetivo del operativo era acabar con el “naciente” ELN. Sin embargo, lo que logró el operativo militar fue el despliegue de la guerrilla hacia las cercanías de la base militar “El centenario” para esperar el regreso de las patrullas del ejército a su sitio de acuartelamiento.

A partir de un nuevo mapa Gabino explica que hacia las 9 de la mañana del 15 de Febrero de 1966, el ejército se reunió en la finca “La Loma” a 4 km del sitio en dónde se preparaba la emboscada de la guerrilla. El paso de un campesino obligó a que la patrulla militar tomara medidas de seguridad excepcionales, disponiendo dos solados cada 10 metros y no 1 cada 20 metros como era la manera regular avanzar.

### **La emboscada** 

Un grupo de 30 guerrilleros se apostaron al los lado del camino. En la cabeza de la emboscada de se ubicaba Fabio Vásquez, y Camilo Torres Restrepo era el tercero en la columna, sin embargo las medidas de seguridad adoptadas por los militares multiplicaron su capacidad de fuego y en 5 minutos pasaron de emboscados a emboscar a la tropa guerrillera, lapso de tiempo en que murieron 5 guerrilleros, entre ellos Camilo Torres.

Gabino relató que un grupo de guerrilleros intentó repeler el ataque de las FFMM y retirar el cuerpo de Camilo Torres pero debieron replegarse al encontrar una tropa multiplicada, puesto que El ELN solamente esperaba 40 militares y la tropa estaba conformada por 80 efectivos que se encontraron en un punto del camino. “Se presentó una discusión” pero tuvo más fuerza la batalla que la voluntad de llevarse el cuerpo de Camilo, según el relato.

“En  término de 8 o 10 minutos se desarrollaron todos estos acontecimientos, que podemos decir, que afectaron sensiblemente la actividad guerrillera porque muere sin duda, el hombre de más impacto, de más relevancia como dirigente popular que era el compañero Camilo", y agrega que a pesar del dolor “Camilo Vive” y sus convicciones cristianas las siguen llevando como un símbolo de la lucha popular y guerrillera.

### **Camilo Torres de combatiente a desaparecido** 

Gabino describe a Torres como una persona sencillo, humanista, humilde y apasionado en sus convicciones políticas, además lo ubica dentro del grupo de desaparecidos de Colombia, “Quisieron desaparecerlo” pero no lo lograron, afirma, y además insta a que el cadáver sea entregado lo más pronto posible a la familia, porque a ellos “les pertenece”.

\[embed\]https://www.youtube.com/watch?v=M-Jr\_sNw8Hw&feature=youtu.be\[/embed\]
