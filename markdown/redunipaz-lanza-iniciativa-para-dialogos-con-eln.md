Title: Redunipaz lanza iniciativa para díalogos con ELN
Date: 2016-04-22 12:03
Category: Nacional, Paz
Tags: Alfonso Insuasty, Negociaciones Gobierno y ELN, Redunipaz
Slug: redunipaz-lanza-iniciativa-para-dialogos-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Comunidades-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [22 Abril 2016] 

La 'Red Universitaria por la Paz' nodo Antioquía-Choco Redunipaz, lidera la construcción de una mesa a través de la que diversos sectores y organizaciones sociales se articularan para participar en [[los diálogos de paz entre el Gobierno y el ELN](https://archivo.contagioradio.com/se-anuncia-inicio-fase-publica-de-conversaciones-de-paz-con-guerrilla-del-eln/)]. La iniciativa **pretende potenciar la participación de la ciudadanía**, teniendo en cuenta el sexto punto de la agenda de negociación.

"Se abre el espacio pero es la sociedad la que debe tomar la iniciativa, no esperar la orden de una mesa, **la sociedad se tiene que mover y ese es el ejercicio que estamos planteado** y que tiene que generarse", dice Alfonso Insuasty, profesor de la Universidad de San Buenaventura e integrante del grupo de investigación Kavilando.

Redunipaz articula grupos de investigación de diversas instituciones y universidades, esta red es **de carácter nacional y desde hace 3 años viene trabajando de forma noda**l teniendo en cuenta la diversidad de las realidades en los territorios.

Dentro de las organizaciones que integran la Red se destaca la 'Cumbre Agraría Étnica y Popular', 'Comosoc', la 'Mesa Intersectorial por la Salud', el 'Movimiento Ecuménico', la Universidad Uniminuto y La Universidad de San Buenaventura.

<iframe src="http://co.ivoox.com/es/player_ej_11268347_2_1.html?data=kpafmJ2XeJihhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncaLgx9Tb1dSPjc%2Fn1sbg1t6PcYzJjLjO0JCmucbiwtvS0NnZtsKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
