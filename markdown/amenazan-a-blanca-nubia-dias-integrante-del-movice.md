Title: Amenazan a Blanca Nubia Dias, integrante del MOVICE
Date: 2016-09-09 18:04
Category: DDHH, Nacional
Tags: amenazas de muerte, defensores de derechos humanos, MOVICE, paramilitares, Paramilitarismo, paz
Slug: amenazan-a-blanca-nubia-dias-integrante-del-movice
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/NubiaDiaz-Colombia-MidiaNinja.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [09 Sep 2016]

El pasado viernes 2 de septiembre en horas de la tarde, la defensora de derechos humanos y de los derechos de las mujeres afro, indígenas y campesinas e integrante del MOVICE, Blanca Nubia Díaz, fue abordada por un sujeto, que portaba un arma blanca. Al dirigirse a Blanca Nubia le arrancó un botón distintivo de la lucha contra la desaparición forzada y en un acto muy intimidante le aseguró “*Si sigues jodiendo te vamos a picar, bien picadita*”.

Blanca Nubia Díaz es miembro activo del [Movimiento Nacional de Víctimas Contra Crímenes de Estado (MOVICE)](https://archivo.contagioradio.com/?s=movice). En varias ocasiones tanto ella, como miembros de su familia, han sido víctimas de constantes amenazas, **algunas de ellas ocurridas en el 2011, 2012 y 2015 en la que sujetos desconocidos les abordaron aludiendo (en tono de amenaza) a la labor desempeñada por la defensora.**

El caso de **Blanca Nubia Días** se remonta a la violación y asesinato de su hija **Irina del Carmen Villero Díaz, en el año 2001 por parte de [grupos paramilitares](https://archivo.contagioradio.com/?s=paramilitarismo)** de la zona de la Guajira, un caso que a todas luces deja en evidencia los tipos de violencia a los cuales han sido sometidas las mujeres en el marco del conflicto y la debilidad institucional en cuanto a la justicia, verdad y reparación.

Esta situación en contra de la señora Blanca Nubia, se suma a las amenazas constantes que reciben los integrantes del MOVICE en el departamento de Sucre, en Valle del Cauca y otras seccionales, en los que se demuestra que aunque se está construyendo un escenario de paz, persisten las amenazas contra los defensores y **defensoras de DDHH, así como para las propias víctimas.**
