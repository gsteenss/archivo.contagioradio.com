Title: En Bolivia Movimientos Populares Mundiales piden unidad entre Iglesia y pueblo
Date: 2015-07-09 17:44
Category: Movilización, Otra Mirada
Tags: Bolivia, encíclica, encíclica Laudato Si, Encuentro Mundial de Movimientos Populares, Evo Morales, Papa Francisco
Slug: en-bolivia-movimientos-populares-mundiales-piden-unidad-entre-iglesia-y-pueblo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/francisco-movimientos-sociales-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Adital 

###### [09 Jul 2015]

El **Encuentro Mundial de Movimientos Populares** en Bolivia le abre las puertas al papa Francisco con el fin de generar acercamientos entre el pueblo y la iglesia en temas ambientales y territoriales, tras ser publicada la Enciclica ecológica. El día de hoy se lleva a cabo un encuentro multitudinario desde las 5:30 P.M que cuenta con la presencia de Francisco y el presidente Evo Morales.

El encuentro Mundial, en su segunda versión, inició el 07 de Julio en **Santa Cruz de la Sierra** y cuenta con la participación de **1.500 representantes de 40 organizaciones**, conformadas por campesinos y campesinas e indígenas que buscan soluciones ante problemas socio culturales y la construcción de la soberanía e identidad de los pueblos, así como la recuperación de su cultura.

Hoy concluirá el Encuentro, en el que serían 10 las demandas que se plasmarán en el documento final, que será entregado al papa Francisco. Evo Morales, presidente de Bolivia, afirma que la responsabilidad de los movimientos populares es grande, pues Francisco sería el primer papa que los y las escucha.

Cabe añadir que el papa Francisco habría elegido este país para la realización del encuentro, pues según él, *¨existe participación democrática de los movimientos sociales y la conformación de un Gobierno que es de los movimientos sociales¨* .
