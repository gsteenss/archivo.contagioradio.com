Title: A pocos kilómetros de base militar binacional funcionaría una pista del narcotráfico en Chocó
Date: 2020-05-29 11:43
Author: AdminContagio
Category: Actualidad, DDHH
Tags: chuzadas, Ejército, Tráfico de drogas
Slug: a-pocos-kilometros-de-base-militar-binacional-funcionaria-una-pista-del-narcotrafico-en-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/frontera-panama.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Archivo Contagio Radio {#foto-archivo-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según información obtenida tras el análisis de la Operación Bastón, que investiga diversos casos de corrupción y vinculación con la criminalidad por parte de integrantes activos de las FFMM, se ha establecido que en medio de la selva del Darien existiría una pista clandestina desde la que despegan aeronaves del narcotráfico, presuntamente con destino a Estados Unidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/a-pocos-kilometro-de-base-militar-binacional-militares-trafican-cocaina/), el lugar que estaría siendo usado para fines del nacrotráfico estaría en zonas controlados por las operaciones de la Fuerza Pública, y coincidiría con los lugares "que desde 1997 han sido epicentro de bombardeos y de tomas de los paramilitares de als Autodefensas Unidas de Colombia (AUC), en los territorios colectivos del Cacarica y el Salaquí".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, señala que serían territorios controlados por las autondenominadas Autodefensas Gaitanistas de Colombia (AGC), "estructura armada que se califica a sí misma como actor político". (Le puede interesar: ["Cacarica, más de dos décadas bajo el asedio paramilitar"](https://archivo.contagioradio.com/cacarica-mas-de-dos-decadas-bajo-el-asedio-paramilitar/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/jdlaverde9/status/1265086606001745921","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/jdlaverde9/status/1265086606001745921

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### En contexto: Operación Bastión, el '*wikileaks*' del Ejército colombiano

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 19 de mayo la Oficina en Washtington para Asuntos Latinoamericanos (WOLA, por su sigla en inglés) realizó un Webinar titulado ["Espionaje Militar Colombiano: Un ataque contra los reformistas del post-conflicto y la prensa libre"](https://www.youtube.com/watch?v=ISZHBORNYXU) en el que, entre otras personas, participó el senador Iván Cepeda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En dicho evento, Cepeda recordó que en 2017, cuando Colombia decide solicitar el ingreso de su Ejército a la Organización del Tratado del Atlántico Norte (OTAN) vino la exigencia de que se hiciera una operación de contra inteligencia para "depurar" la institución porque había informes sobre corrupción y otros hechos. Dicha operación primero se llamó Dante y luego Operación Bastión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el Senador, la misma consistió en 20 misiones que lograron detectar que 16 generales de la república y 250 suboficiales y oficiales estarían involucrados en graves hechos de corrupción, así como tendrían vínculos con organizaciones del narcotráfico, paramilitares e incluso el Ejército de Liberación Nacional (ELN) y las disidencias.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con esta información se produjo un informe de 5 mil páginas "que se ha denominado como el wikileaks del Ejército", pero Cepeda aseguró que cuando el general Nicacio Martínez asumió como comandante del Ejército la operación se paralizó y quienes la llevaban a cabo fueron removidos de sus cargos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Gracias a que medios de información han tenido acceso a algunas de las páginas de la operación, se han conocido de denuncias de corrupción en la Cuarta Brigada del Ejército, acciones de uniformados con distintos grupos armados ilegales y la denuncia de una de las pistas al servicio del narcotráfico que funcionaría entre Salaquí y Cacarica, en Chocó.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Habría connivencia entre armados legales e ilegales en Urabá y Bajo Atrato

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el recuento histórico sobre lo que ocurre en el territorio, la Comisión de Justicia y Paz recuerda que en [2014](https://www.justiciaypazcolombia.com/militares-desconocen-derechos-territoriales-en-cacarica/) se instaló una base de las Fuerzas Militares de Colombia y Panama, que no fue consultada con las comunidades, "mientras como lo refleja hoy, se iban consolidando operaciones aéreas para el tráfico de droga".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, la Comisión explica que las operaciones paramilitares por parte de las autodenominadas AGC se intensificaron en 2016, "logrando un control social territorial casi total que se extiende hacia el norte, hacia Unguía y Acandí, y hacía el occidente con Panamá, donde ejercen control desde hace más de 10 años".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de hacer presencia de forma contundente en el territorio, la Organización manifiesta que la vinculación de sectores militares con la cadena del narcotráfico en el Urabá antioqueño y el bajo Atrato ha sido denunciado hace más de siete años dado el tránsito de elementos necesarios para la cadena de producción de las drogas, "incluso el paso de camiones de cocaína" por retenes militares o policiales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, por el tránsito que hacen tanto insumos como producto final "hasta las riveras del Atrato, donde cruzan, todo ello en medio de la coincidente omisión de unidades del batallón fluvial". Por último, la Comisión señala que algunos cargamentos de cocaína son almacenados en predios despojados de comunidades afro e indíengas que habitan en los territorios de Pedeguita Mancilla, Curbaradó y Jiguamiandó mientras hacen su tránsito al otro lado del Atrato, hacía el occidente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El efecto sobre la población de la operación de los ilegales no se limita al despojo, la Organización Defensora de DD.HH. concluye diciendo que el silencio de líderes y lideresas de las comunidades "es casi total debido al control armado y la fragmentación que ha ido logrando la criminalidad para sus propósitos estratégicos económicos legales e ilegales".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
