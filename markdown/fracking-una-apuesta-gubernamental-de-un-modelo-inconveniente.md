Title: Fracking: una apuesta gubernamental de un modelo inconveniente
Date: 2020-04-14 16:19
Author: AdminContagio
Category: Opinion
Tags: defensa del medio ambiente, fracking, medio ambiente
Slug: fracking-una-apuesta-gubernamental-de-un-modelo-inconveniente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/MG_1267-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#5f6d72"} -->

###### Foto por: Contagio Radio 

<!-- /wp:heading -->

<!-- wp:heading {"level":5,"customTextColor":"#3d4a56"} -->

##### Por: Foro Opina 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Recientemente el **Ministerio de Minas y Energía** publicó el **Decreto 328** de 2020, por el cual se brindan los lineamientos para realizar los denominados Proyectos Pilotos Integrales de Investigación **(PPII)** sobre Yacimientos No Convencionales de hidrocarburos **(YNC)** a través de la técnica de fracturamiento hidráulico, popularmente conocida como *fracking*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este instrumento normativo, fuertemente cuestionado por diversos sectores sociales y académicos por las omisiones a sus recomendaciones y a las directrices de la **Comisión Interdisciplinaria de Expertos** conformada en 2018, figura como un paso significativo en las pretensiones del Gobierno Nacional y las empresas de implementar la técnica en un futuro cercano, a pesar de que el actual presidente manifestó en la campaña electoral que durante su gobierno no se haría uso del fracking.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### En el actual contexto económico y ambiental del país y del mundo, resulta imprescindible preguntarse cuál es la verdadera conveniencia de avanzar en ese propósito gubernamental y corporativo.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Indudablemente, no es para nada conveniente aplicar esta técnica. Al respecto, se enumeran algunos asuntos que deberán tenerse en cuenta antes de acelerar la dinámica de la transición energética en el país.  

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Un primer asunto está referido a los precios internacionales del petróleo**. En efecto, una semana después del anuncio ministerial para emprender los **PPII**, los precios internacionales del petróleo se desplomaron significativamente debido a un desacuerdo entre la **Opep** y Rusia para recortar la producción de crudo y contrarrestar los efectos económicos generados por la pandemia del Covid-19.

<!-- /wp:list -->

<!-- wp:paragraph -->

Para nadie es un secreto que la caída de precios genera consecuencias fiscales en economías como la colombiana, en la cual existe una riesgosa dependencia con respecto a la extracción y exportación de petróleo, situación que se corroboró, entre el 2015 y 2016, cuando fue necesario hacer recortes en el presupuesto de la nación en un margen de 6 billones de pesos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### A pesar de ello, el gobierno demuestra querer preservar este modelo de “desarrollo” celosamente, e incluso, profundizarlo a través de apuestas como el fracking.  

<!-- /wp:heading -->

<!-- wp:list -->

-   En segunda instancia, y relacionado con lo anterior, **la volatilidad de la cotización del crudo y la dependencia de nuestra economía de su venta al mercado internacional no son algo nuevo.** Por ello, carece de lógica que la alternativa para afrontar la caída de precios y los bajos ingresos fiscales sea tratar de extraer más petróleo.

<!-- /wp:list -->

<!-- wp:paragraph -->

El gobierno, en vez de promover un sector tan fluctuante, debería preocuparse con urgencia por implementar acciones para diversificar la economía y la canasta exportadora, de manera que se reduzca la lesiva dependencia con respecto al petróleo y el sector extractivo, tal como lo advirtió en días pasados el presidente de la Bolsa de Valores de Colombia **(BVC)** y como también lo han señalado,  de tiempo atrás, diferentes sectores ciudadanos y académicos, quienes en el debate sobre el fracking, han sumado esfuerzos articuladamente en la plataforma **“Alianza Colombia libre de fracking”.   **

<!-- /wp:paragraph -->

<!-- wp:list -->

-   Significa, entonces, **en tercer lugar, modificar el modelo económico de desarrollo que ha imperado en las últimas dos décadas** y optar por uno menos vulnerable a las circunstancias internacionales y que resulte sostenible en términos ambientales. No es responsable con el planeta ni con el país seguir promoviendo o profundizando la extracción de combustibles fósiles.

<!-- /wp:list -->

<!-- wp:heading {"level":5,"customTextColor":"#243f4a"} -->

##### El **IDEAM** señala que uno de los impactos de los Gases Efecto Invernadero **(GEI)** en Colombia ha sido el incremento de la temperatura media en 1,4°C entre 2005 y 2015, lo que puede llevar a que en 2050 se experimenten consecuencias climáticas y ambientales muy graves e irreversibles.   

<!-- /wp:heading -->

<!-- wp:paragraph -->

Dejar de explotar los recursos del subsuelo obligaría al gobierno a fortalecer, con premura, los esfuerzos en el proceso de transición energética de manera integral; es decir, no solo en función de reemplazar una fuente por otra para evitar el desabastecimiento de combustibles, sino también desde una perspectiva de respuesta social, económica, cultural y ambiental.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque Iván Duque se jacte de que Colombia tenga, supuestamente, la “sexta matriz energética más limpia del mundo”, lo cierto es que las grandes hidroeléctricas están altamente cuestionadas por sus impactos sociales y ambientales, y la generación a partir de fuentes térmicas y fósiles está en camino de fortalecerse utilizando los excedentes de carbón que generen los yacimientos del país, lo que realmente no hace parte de una apuesta o política de generación de energía renovable y, mucho menos, limpia.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Es claro que el contexto actual nos da una nueva muestra de la miopía rentista y extractiva del gobierno; el fracking es una de ellas, donde, ni siquiera, se están tomando adecuadamente las recomendaciones formuladas por la Comisión de Expertos, quienes indicaban que los **PPII** tenían un propósito  de corte investigativo, y no para iniciar labores de exploración como lo ha planteado el Ministerio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esa misma lógica, también se han desatendido las recomendaciones para que los **PPII** favorezcan procesos de gobernanza y espacios de participación ciudadana que eviten la escalada de conflictividades sociales en el territorio y generen un grado favorable de aceptación social (la licencia social) en relación con la técnica y el sector.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta no es una situación menor, de hecho, podría ser el principio y el fin de los propósitos técnicos debido a que en los territorios no se está esperando su llegada y nadie desea que la participación de la ciudadanía sea instrumentalizada para darle visto bueno a los proyectos, luego de que sus condiciones hayan sido predefinidas -*y de manera restringida-* por políticos y empresarios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si se insiste en la apuesta por imponerlos podría activar un estallido social y un auge importante de movilizaciones sociales en varias partes del país. Por tanto, este modelo no es para nada conveniente para el país.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### \***La Fundación Foro Nacional Por Colombia** Es un Organismo Civil no Gubernamental sin ánimo de lucro, creado en 1982, cuyos objetivos son contribuir al fortalecimiento de la democracia en Colombia. Desarrolla actividades de investigación, intervención social, divulgación y deliberación pública, asesoría e incidencia en campos como el fortalecimiento de organizaciones, redes y movimientos sociales, la participación ciudadana y política, la descentralización y la gestión pública, los derechos humanos, el conflicto, la paz y las relaciones de género en la perspectiva de una democracia incluyente y efectiva. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente). 

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### **Contáctenos: 316 697 8026 –  282 2550**

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### Mas[columnas de Foro Opina](https://archivo.contagioradio.com/author/foro-opina/)  

<!-- /wp:heading -->
