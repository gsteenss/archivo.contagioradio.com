Title: "Contra la Corriente de aguas terrosas" el retrato de la esperanza en Colombia
Date: 2018-06-14 07:53
Category: Cultura
Tags: Alexandra Huck, Bajo Atrato
Slug: contra-la-corriente-de-aguas-terrosas-la-historia-de-los-lideres-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/amp-logo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [13 Jun 2018]

“Primero corre el agua río arriba, antes de que a un desplazado en Colombia le devuelvan su tierra; o que a un general lo responsabilicen por sus crímenes” dice el padre de Mariela, la protagonista de **"Contra la Corriente de aguas terrosas"**, de la autora Alexandra Huck, una novela inspirada en las resistencias de las comunidades del Darien Chocoano, que relata el dolor del desplazamiento y el conflicto armado en Colombia.

De acuerdo con Huck, esta novela es un homenaje a muchos líderes que hay en el país, que en medio de un conflicto armado, están luchando por su tierra y por sus derechos. **En ese sentido**, **el libro es el retrato de como Mariela**, el personaje principal, vive un desplazamiento forzado, la violencia previa a este hecho y la fortaleza que la llevará más adelante a iniciar un camino de retorno a su territorio.

Alexandra Huck llegó a Colombia como parte de las Brigadas de Paz Internacional, allí conoció el territorio del Bajo Atrato y Antioquia. "Me siento muy privilegiada por haber podido escuchar los relatos de las personas, pero también haber visto momentos pmuy importantes como los reclamos de justicia y el retorno de las personas al Cacarica" afirmó Huck.

La autora manifestó que su mayor aprendizaje, de esta experiencia, ha sido la esperanza, "es gente que en medio de una situación que parece increiblemente difícil sigue con esperanza, **aún en situaciones en donde no se ve que realmente pueda haber un buen resultado, siguen con su lucha por el territorio y la justicia**".

Para mayor información de las librerías en donde puede encontrar el libro, ingrese al perfil de Facebook de  "Contra la Corriente de aguas terrosas", en donde encontrará la lista de los diferentes lugares, tanto en Bogotá como en el resto del país de dónde lo puede adquirir.

<iframe id="audio_26522387" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26522387_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
