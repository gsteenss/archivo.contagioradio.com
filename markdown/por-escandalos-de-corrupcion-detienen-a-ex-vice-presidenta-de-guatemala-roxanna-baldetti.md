Title: Por escándalos de corrupción detienen a Ex vice presidenta de Guatemala Roxanna Baldetti
Date: 2015-08-21 11:37
Category: El mundo, Otra Mirada
Tags: Comisión Internacional contra la Impunidad, corrupción, Guatemala, Iván Velásquez, Movilización Social en Guatemala, Otto Pérez Molina, Roxanna Baldetti
Slug: por-escandalos-de-corrupcion-detienen-a-ex-vice-presidenta-de-guatemala-roxanna-baldetti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/roxanna_baldetti_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: noticias.emisorasunidas 

###### [21 Ago 2015]

En una acción conjunta entre la **Comisión Internacional contra la Impunidad en Guatemala y la Fiscalía de ese país, se dio la captura de Roxana Baldetti**, quien dimitió de su cargo el pasado mes de Mayo tras abrirse un proceso penal por su implicación en escándalos de **corrupción como el caso de “Linea 21” por el cual hay otras 21 personas detenidas. **

Los cargos imputados contra Baldetti tienen que ver con asociación ilícita, caso especial de estafa, defraudación aduanera y cohecho pasivo. En estos escándalos también estaría **implicado el presidente de ese país y ex militar Otto Perez**, contra quien se abrió un proceso en la Suprema Corte el pasado mes de Junio.

Las acciones contra los altos funcionarios del gobierno han estado precedidas en investigaciones realizadas por la Comisión Internacional contra la Impunidad en Guatemala, CICIG, encabezada por el ex magistrado colombiano Iván Velásquez, principal investigador de la parapolítica en ese país. **Hasta el momento se han dado las capturas de 21 personas, la mayoría de ellos funcionarios del gobierno de Perez Molina**. Lea también [Continúan protestas e investigación por corrupción en Guatemala.](https://archivo.contagioradio.com/continuan-protestas-e-investigaciones-por-casos-de-corrupcion-en-guatemala/)

Desde el pasado mes de Junio miles de personas de diversas organizaciones sociales y **movimientos populares se han lanzado a las calles para exigir el cese de la impunidad y el encarcelamiento** de las personas del gobierno responsables de los escándalos de corrupción, así como la exigencia de un cambio de modelo que ataque la criminalidad de las bandas organizadas que actúan como paramilitares, control del tráfico de drogas y diversas medidas de orden social que tienen que ver con los derechos fundamentales de los ciudadanos y ciudadanas de Guatemala.
