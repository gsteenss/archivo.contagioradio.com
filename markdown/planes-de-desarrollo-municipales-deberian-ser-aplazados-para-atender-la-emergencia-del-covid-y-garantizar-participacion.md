Title: Planes de Desarrollo municipales deberían ser aplazados para atender la emergencia del COVID y garantizar participación
Date: 2020-04-16 09:25
Author: AdminContagio
Category: Actualidad, Política
Tags: Covid-19, participación, Planes de desarrollo
Slug: planes-de-desarrollo-municipales-deberian-ser-aplazados-para-atender-la-emergencia-del-covid-y-garantizar-participacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Planes-de-desarrollo-y-Covid-19.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Felipe Perea {#foto-felipe-perea .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones sociales advierten que si no se emprenden acciones por parte del presidente Duque, loo gobiernos departamentales y municipales tendrían que presentar planes de desarrollo como máximo hasta el 31 de mayo, sin haber tomado en consideración adecuadamente los efectos del Covid-19 en sus territorios. Por esta razón, se solicita una extensión de entre dos y tres meses para hacer la reformulación de los planes y así responder a las necesidades de las personas. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Por qué pedir que se aplace el tiempo para presentar los planes de desarrollo?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Fernando Valencia director de la Corporación Conciudadanía explica que actualmente en departamentos y municipios se están aprobando los planes de desarrollo que deben recoger el programa de Gobierno que inscribieron los alcaldes y gobernadores. El proceso inició el 1 de enero y debe ser entregado antes del 31 de mayo, pero actualmente por el Covid se plantean dos discusiones: La ciudadanía no puede participar en la toma de esta decisión porque los espacios de participación están cerrado, y dichos planes no toman en cuenta los efectos del Covid.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Valencia señala que en primer lugar, la participación está limitada porque asimismo están los concejos, asambleas y Concejos Territoriales de Participación (CTP); y en el segundo caso, la Planes se proyectaron sin tener en cuenta los efectos del coronavirus, y la realidad cambió haciendo que se tengan que cambiar los planes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También señala que los ingresos de los municipios cambiarán, porque no recaudaran el mismo nivel de impuestos, y tampoco van a recibir la mismas transferencias del Gobierno central, por ejemplo, cambian las reglas para el acceso a regalías. Por eso, sostiene que necesariamente deben modificarse las prioridades consignadas en los planes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Valencia advierte que en caso de no permitirse la reformulación, los municipios tendrán planes de desarrollo que no responden a la realidad de los territorios. “Eso sería nocivo porque partimos de diagnósticos equivocados y llegaríamos a metas inalcanzables o que no son prioritarias”, concluye añadiendo que es necesario permitir un reajuste grande, que no obligue a los gobernantes a estar ligados a su plan de gobierno, respondiendo así a las necesidades del momento y no a las planteadas cuando eran candidatos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La propuesta para modificar los planes**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para Valencia, por las razones expuestas, se hace necesario facilitar un periodo especial para idear nuevos planes, que podría estar amparado por un decreto teniendo en cuenta el Estado de emergencia sanitaria que vivimos, garantizando de esta manera diagnósticos locales certeros y la participación de la ciudadanía en la formulación de los mismos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido concluye que se podría pensar en un periodo de dos o tres meses para hacer la reformulación de los planes y que estos respondan a la realidad que tenemos ahora, tomando en cuenta mejores inversiones en sectores que mueven la economía y apoyando a aquellos que se verían afectados por la crisis generada en el marco del Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Le puede interesar: ["La crisis económica no se resuelve con limosnas: Alirio Uribe Muñóz"](https://archivo.contagioradio.com/la-crisis-economica-no-se-resuelve-con-limosnas-alirio-uribe-munoz/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
