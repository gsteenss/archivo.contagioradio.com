Title: Durante el 2016, 100 mil mujeres han sido violentadas en Colombia
Date: 2016-11-21 12:15
Category: Mujer, Nacional
Tags: Asesinatos, feminicidio, feminismo, mujeres, violencia sexual, Violencia Sexual en colombia
Slug: 2016-100-mil-mujeres-han-sido-violentadas-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cybergus] 

###### [21 Nov. 2016] 

**Este 25 de Noviembre se conmemora el Día Internacional de la Eliminación de la Violencia contra la Mujer** creado en 1999 por las Naciones Unidas, como forma de invitar a los gobiernos, las organizaciones internacionales y no gubernamentales a emprender acciones dirigidas a rechazar la eliminación de la violencia contra la mujer y aunar esfuerzos por hallar justicia en los casos que se han presentado en todo el mundo.

**Múltiples son las afectaciones contra las mujeres, que siguen ocupando los titulares de los medios en Colombia y en el Mundo con cifras dramáticas**. Uno de los temas más preocupantes continúa siendo la violencia física y de tipo sexual, realidad que no afecta solo a adultas sino también a niñas.

Por ejemplo, en el Valle al sur de Colombia, **la cifra asciende a más de 2 mil mujeres que han sido maltratadas y abusadas sexualmente. Cada año, según la Mesa por el Derecho de las Mujeres a una Vida Libre de Violencias se registran más de 19000 casos de violencia contra ellas. **Le puede interesar: [Dora Liliana Gálvez, otra víctima de la violencia feminicida](https://archivo.contagioradio.com/dora-liliana-galvez-otra-victima-de-la-violencia-feminicida/)

Dichas cifras lanzan entonces un dramático porcentaje, **en promedio cada día cuatro mujeres son víctimas de feminicidio y el 90% de los casos se encuentran en la total impunidad. Además cada día son 22 las niñas que se convierten en víctimas de abuso sexual. **

Por su parte, el Instituto  de Medicina Legal ha manifestado que **en lo que va del 2016 se tiene registro de 27.681 casos de abuso contra las mujeres en Colombia**, donde la capital y el Departamento del Valle presentan los más altos números.

**Violencia sexual contra menores**

Según el programa Bogotá cómo Vamos, en la capital se han realizado **3.259 exámenes médico legales por delito sexual y de esta cifra  el 86% fueron realizados a niños, niñas y adolescentes.**

En los casos de menores, las cifras también son alarmantes, **del total de 2.789 presuntos casos de violencia sexual contra menores**: 584 en primera infancia (0 a 5 años), 910 en Infancia (6 a 11 años), 1.295 en adolescencia (12 a 17 años). De estos números **el 83% de las víctimas de violencia sexual entre 0 y 17 años son mujeres.**

Según el Fondo de Población de Naciones Unidas **cada día son violadas en Colombia 21 niñas entre los diez y los 14 años.**

**Compañeros sentimentales, los mayores agresores**

Según Medicina Legal, **desde enero hasta septiembre de 2016 se han registrado 100 mil casos de agresiones contra las mujeres en todo Colombia** de las cuales se tiene registro, que es su mayoría han sido cometidas por los compañeros sentimentales de las víctimas.

Aunque las cifras han disminuido con relación al mismo periodo de 2015, estas no dejan de preocupar. Las denuncias continúan en aumento. Le puede interesar: [Medios de información promueven la violencia contra las mujeres](https://archivo.contagioradio.com/medios-de-informacion-promueven-la-violencia-contra-las-mujeres/)

###### Reciba toda la información de Contagio Radio en [[su correo]
