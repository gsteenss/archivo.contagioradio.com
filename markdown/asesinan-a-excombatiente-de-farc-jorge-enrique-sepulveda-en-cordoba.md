Title: Asesinan a excombatiente de FARC, Jorge Enrique Sepúlveda en Córdoba
Date: 2019-06-07 08:40
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinatos de excombatientes, Jorge Enrique Sepulveda
Slug: asesinan-a-excombatiente-de-farc-jorge-enrique-sepulveda-en-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Jorge-Enrique.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo personal] 

En horas de la tarde del pasado 6 junio, fue hallado el cuerpo sin vida de **Jorge Enrique Sepúlveda**, excombatiente de las antiguas FARC quien fue asesinado con arma de fuego en **Bocas de Conejo, vereda Nain, Tierralta, aguas arriba del embalse de Urra 1 en Córdoba.**

Según información de la Fundación Social Cordoberxia, Jorge Enrique era reincorporado a la vida civil del extinto Frente 58 de las FARC-EP y estuvo durante un tiempo en el ETCR de la vereda El Gallo [(Lea también: Asesinan a Wilson Saavedra ex comandante de FARC que aportó a la paz)](https://archivo.contagioradio.com/asesinan-a-wilson-saavedra-ex-comandante-de-farc-que-aporto-a-la-paz/)

El excombatiente actualmente vivía en la vereda Nain, donde asistía a las actividades que la **Agencia para la Reincorporación y la Normalización (ARN)** programaba en el marco de su reincorporación y aguardaba que se le hiciera entrega de su proyecto productivo.

### En Tierralta no hay garantías para los excombatientes

Según **Andrés Chica defensor de derechos humanos de Cordoberxia**, la reincorporación ha sido particularmente difícil en Tierralta, donde los grupos paramilitares derivados de las Autodefensas Unidas de Colombia han amenazado a los reincorporados.

Tal ha sido el nivel de inseguridad en esta región que el ETCR de El Gallo donde vivió Jorge Enrique, fue disuelto por el Gobierno en 2018 debido a que no existían garantías ni proyecto productivos con seguridad territorial para los excombatientes. Con el homicidio de Jorge, este es el segundo caso de asesinatos contra exintegrantes de FARC en Córdoba.

<iframe id="audio_36811213" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36811213_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
