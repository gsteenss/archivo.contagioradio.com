Title: Con una marcha Festival en Pasca dicen "la consulta popular si va"
Date: 2017-08-06 17:06
Category: Ambiente, Movilización
Tags: consulta popular, pasca, petroleo
Slug: pasca-consulta-petrolera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Pasca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:[[@**valentinacmpm**]

###### 06 Ago 2017 

En la misma fecha en que debia realizarse la [consulta popular](https://archivo.contagioradio.com/aprueban_consulta_popular_pasca_petroleo/) sobre actividades petroleras en Pasca, Cundinamarca, hombres y mujeres marcharon por las calles del municipio bajo la consigna "**Pasca se respeta**" como muestra de su voluntad por mantener su derecho a decidir sobre la vocación de sus territorios.

Desde las 9 de la mañana diferentes caravanas locales y de municipios hermanados por la causa, partieron del parque central del municipio, portando carteles y pancartas en el evento denominado **Festivaleando por Sumapaz**, por la defensa del agua, la vida y el derecho colectivo a un ambiente sano.

"La consulta popular de Pasca va porque va. **Lo que se pelea acá es la defensa del páramo más grande del mundo**, el de Sumapaz" aseguraron los participantes quienes marcharon en compañía de organizaciones ambientales de la región y de otros departamentos del país "vamos a cambiar nuestros campos. **¡Nuestras vocaciones productivas NO se cambian!**" (Le puede interesar: [Consulta popular en Pasca sigue viva](https://archivo.contagioradio.com/consulta-popular-en-pasca-sigue-viva/))

\

**La tutela que suspendió la consulta**

“**Si o no están de acuerdo con que se ejecuten actividades exploratorias, sísmicas, perforaciones, explotaciones, producción y transporte de hidrocarburos en el municipio de Pasca**”, era la pregunta aprobada por el Tribunal para la realización de la consulta popular, que fue suspendida por el Consejo de Estado ante la tutela presentada por la **Asociación Colombiana de Ingenieros de petróleos (ACIPET)**

En la acción, la Asociación manifestó que la pregunta propuesta para la consulta dejaría al municipio sin la posibilidad de abastecerse “de los combustibles, gasolina y ACPM a través de carro tanques”, a lo que abogados como Rodrigo Negrete afirmaron que  **“hubo una interpretación errada por parte del consejo de estado y de la Asociación de Ingenieros** porque la pregunta hace referencia al transporte del crudo que sale de la producción de hidrocarburos más no a sus derivados”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
