Title: El riesgo de los acuerdos de paz en elecciones presidenciales 2018
Date: 2017-02-01 13:29
Category: Nacional, Paz
Tags: acuerdo de paz, Centro Democrático, De La Calle
Slug: el-riesgo-de-los-acuerdos-de-paz-en-elecciones-presidenciales-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz64.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [1 Feb 2017] 

Bajo la sombra del No: La paz en Colombia después del plebiscito, es el último informe de la organización internacional Crisis Group, que advierte  la necesidad  de un gobierno estable de transición en el país, que garantice la continuidad de la implementación de los acuerdos de paz, ya que de ganar alguno de los sectores de oposición **“se podría ahogar financieramente  el acuerdo de paz”.**

El documento expone que tal y como sucedió en las pasadas elecciones **“la paz con las guerrillas será de nuevo punto de polarización** entre los partidos políticos y los candidatos a las **elecciones presidenciales y legislativas del 2018**” y que en dado caso de que los ganadores en esta contienda sean los sectores de oposición se impondrían grandes retos de sostenibilidad y voluntad del acuerdo.

### **La oposición y las garantías para la paz** 

En primera medida Crisis Group evidencia que “**la oposición podría ahogar financieramente a instituciones**, programas o políticas propias del acuerdo de paz si llega al poder” y puntos específicos del acuerdo como medidas sobre la reforma rural, el acceso a la tierra e iniciativas comunitarias para la erradicación de cultivos de coca podrían quedar en la cuerda floja o desaparecer de la implementación. Le puede interesar: ["Jurisdicción Especial de Paz incluirá a terceros implicados en la guerra"](https://archivo.contagioradio.com/jep-colombia-debate-paz/)

Frente a esta situación, Crisis Group recomienda que se **fortalezca la pedagogía del Acuerdo de Paz y que se ofrezca una protección inmediata a los líderes y defensores de derechos humanos amenazados**, hasta que se instale el acuerdo sobre garantías de seguridad y recalca la importancia de financiar a las instituciones y comisiones encargadas de las tareas claves durante la implementación.

Otra de las recomendaciones que hace en este punto es continuar con el calendario de fechas para la dejación armas, mantener a las víctimas en el centro del proceso y dar avances en temas como la búsqueda de victimas de desaparición forzada.

### **Panorama electoral 2018** 

De otro lado el informe avizora la puja electoral 2018 en la que consideran que será muy difícil que gane algún candidato que procure velar por la continuidad de la implementación, teniendo en cuenta que habrá un **candidato por Centro Democrático y están las candidaturas de German  Vargas lleras, Alejandro Ordoñez, todos contendores de lo pactado en la Habana** y con fuertes maquinarias electorales.

Sumado a esto se encuentra la percepción social contra el actual presidente Santos y sus medidas regresivas hacia el aumento de impuestos, la división del Polo Democrático y la posible candidatura de De la Calle con el partido Liberal, no obstante, para Crisis Group la **segunda vuelta del 2018 se dará entre el candidato del Centro Democrático y Vargas Lleras**. Le puede interesar: ["En debate de Jurisdicción Especial para la paz, algunos buscan impunidad"](https://archivo.contagioradio.com/jurisdiccion-especial-para-la-paz-2/)

Circunstancia que desde el informe puede ser contrarrestada si  “**durante el próximo año se una implementación exitosa, esta sería la mejor manera de reforzar el apoyo popular** y político, así como de incrementar el costo para la oposición de revertir el proceso de paz”. A su vez expone que podría incluirse de manera más directa a las autoridades locales y regionales en el proceso de dejación de armas y en la planeación de otros aspectos de la implementación del acuerdo, como los planes de sustitución de cultivos ilícitos.

Otro de los actores que tendría fundamental participación en este panorama serían las **comunidades internacionales ya que podrían mantener un diálogo con los sectores de oposición**, presionar  tanto al gobierno como a las FACR-EP a que cumplan lo pactado y denunciar con mayor frecuencia el asesinato de  líderes y defensores de derechos humanos.

### **Para Garantizar los acuerdos de paz** 

Finalmente el informe concluye que es necesario que durante lo que queda del año 2017 se avance en aspectos críticos de la implementación del Acuerdo de Paz para intentar salvaguardar la esencia del mismo. Le puede interesar: ["Modificaciones  a Jurisdicción Especial para la Paz abren la puerta a Corte Penal Internacional"](https://archivo.contagioradio.com/modificaciones-a-jurisdiccion-especial-de-paz-abririan-la-puerta-a-corte-penal-internacional/)

**“Sí los esfuerzos para implementar el acuerdo no sobrepasan estos obstáculos algunos puntos acordados pueden estar condenados al fracaso** antes incluso de que tengan alguna posibilidad de éxito. Si eso sucede, el compromiso de las FARC con la paz, la posibilidad de una negociación similar con el ELN y las perspectivas de enfrentar las causas estructurales del conflicto quedarán en veremos”.

###### Reciba toda la información de Contagio Radio en [[su correo]
