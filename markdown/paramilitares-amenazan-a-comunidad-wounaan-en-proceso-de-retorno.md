Title: Paramilitares amenazan a comunidad Wounaan en proceso de retorno
Date: 2015-12-06 08:44
Category: Movilización, Nacional
Tags: Buenaventura comunidad wounaan, Comunidad Unión Agua Clara, Paramilitares amenazan a comunidad Wounaan, Paramilitares río San Juan, Paramilitarismo en Buenaventura
Slug: paramilitares-amenazan-a-comunidad-wounaan-en-proceso-de-retorno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Vívelo Hoy 

###### [4 Dic 2015]

[De acuerdo con la Comisión Intereclesial de Justicia y Paz, **paramilitares amenazaron de muerte a autoridades Wounaan** pertenecientes a la Comunidad Unión Agua Clara, que [[[reto[r]](https://archivo.contagioradio.com/instituciones-le-incumplieron-a-comunidad-indigena-wonnan-para-el-regreso-a-sus-tierras/)[nó]] ]desde el pasado domingo a sus territorios luego de permanecer cerca de 12 meses en el coliseo de Buenaventura, tras ser desplazados por operaciones de tipo paramilitar en la región. ]

[Sobre las 3 de la tarde del pasado miércoles arribaron al ‘Territorio Humanitario y Biodiverso de la Comunidad Unión Agua Clara’ a orillas del río San Juan, **2 paramilitares de tez morena a bordo de una embarcación y portando armas cortas en la cintura**. 1 de ellos descendió de la embarcación y preguntó agresivamente a los pobladores por el gobernador, al  no obtener respuesta fue hasta su vivienda a buscarlo.]

[Frente al padre del gobernador y otros pobladores que allí se encontraban el **paramilitar** alegó "por qué se desplazaron indios brutos, ahora si voy a mandar a mi gente… me voy pero voy a mandar mi gente… **voy a matar a uno pa’ que no anden jodiendo con esta vaina, ya verán**". Momentos después se acercó a la valla instalada por la comunidad y en la que se cita "Territorio Humanitario y Biodiverso Comunidad Unión Agua Clara, área exclusiva de población civil" e insistió "voy a mandar mi gente".]

[Los 2 paramilitares que permanecieron por un lapso de 15 minutos fueron reconocidos con los alias de "Norman" y "Ricardo", éste último estuvo todo el tiempo en la embarcación. Al retirarse afirmaron que **todavía tienen en lista a líderes de varias comunidades del río San Juan**, entre ellos Mayolo Chamapuro, Edinson Málaga, Ciabel Obispo, Enrique Membache y la autoridad Esmérito Chamarra.]

[ ]
