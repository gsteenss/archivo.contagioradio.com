Title: Un año de la masacre de 15 inmigrantes en la frontera española
Date: 2015-02-06 20:42
Author: CtgAdm
Category: DDHH, El mundo
Tags: Frontera Ceuta y Melilla, inmigracion España, Matanza Tarajal, Tarajal, Valla de la vergüenza
Slug: un-ano-de-la-masacre-de-15-inmigrantes-en-la-frontera-espanola
Status: published

###### Foto:Andaluciainformacion.es 

Se cumple un año de la **muerte de 15 inmigrantes a manos de la guardia civil española en la frontera entre la ciudad española de Ceuta, y Marruecos**.

##### Documental realizado por la Asociación Pro.De.In.Melilla sobre la violación de DDHH en la frontera: 

\[embed\]http://vimeo.com/118330415\[/embed\]

El 6 de febrero de 2014, el grupo de inmigrantes intentaba entrar por la playa del Tarajal (Ceuta) a territorio español. Los inmigrantes habían bajado de la barcaza que los transportaba (patera) e intentaban llegar a la playa, pero fueron recibidos por disparos de goma y material antidusturbios que les impedía llegar.

**Como resultado de la represión policial murieron 15 inmigrantes**. Después de los sucedido el ministro de exteriores compareció ante el congreso **negando,en dos ocasiones, el uso de material antidisturbios**.

Según el informe de varias organizaciones sociales, en el asesinato de los inmigrantes se usaron 145 pelotas de goma y 4 botes de humo, resultando evidente la responsabilidad de la policía y del Estado en la masacre.

A pesar de las pruebas, como los videos de las propias fuerzas de seguridad o los testimonios de los supervivientes, aún **no se ha realizado una investigación judicial definitiva** y hoy se ha conocido que la audiencia nacional ha enviado el caso a los juzgados de la ciudad de Ceuta.

10 ONG's y MIGREUROP han protagonizado un acto de homenaje, d**enunciando la total impunidad y las reiteradas violaciones de derechos humanos por parte del Estado español** en las fronteras del sur. Por otro lado **MIGREUROP** ha matizado que la investigación no ha servido para esclarecer lo que sucedió: *"...no sólo no se ha asumido ninguna responsabilidad por esta tragedia, sino que la investigación en marcha no ha servido para ofrecer una explicación...”***.**

En total son ya 131 personas las que han fallecido intentando entrar en España en lo que va de año y 22.000 en todas las fronteras de los países del sur de Europa[** **durante el 2014.]

A esas violaciones de derechos humanos hay que añadir las devoluciones en caliente consideradas como práctica ilegal de expulsión, sin embargo, en el último proyecto de ley conocido como "**ley de seguridad ciudadana**" o "**ley mordaza**" se ha incluido un apéndice que permitiría la legalización de esta práctica.

Las "**devoluciones en caliente"** consisten **en retornar inmediatamente a los inmigrantes infringiendo la actual normativa de acuerdo a los derechos humanos** que indica que el inmigrante debe acudir a una comisaría de policía donde debe recibir apoyo jurídico y psicológico antes de comenzar el trámite para su devolución.

Este miércoles 11 de febrero España deberá responder ante el **tribunal de derechos humanos europeo por casos de "devoluciones en caliente"**.
