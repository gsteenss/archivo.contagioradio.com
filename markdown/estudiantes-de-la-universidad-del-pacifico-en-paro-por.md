Title: Estudiantes de la Universidad del Pacífico en paro, denuncian mala administración
Date: 2017-03-23 15:33
Category: Educación, Nacional
Tags: Universidad Del Pacífico
Slug: estudiantes-de-la-universidad-del-pacifico-en-paro-por
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Universidad-del-Pacífico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Universidad del Pacífico] 

###### [23 Mar 2017]

Estudiantes de la **Universidad del Pacífico se mantienen en paro desde hace dos semanas**, denunciando las precarias condiciones en las que tienen que asistir a clases; con tan solo 16 docentes de planta, los baños de la institución cerrados y sin servicio de restaurante, los universitarios exigen a la rectoría y vicerectoria que tome acciones urgentes que les permita estudiar con garantías.

De acuerdo con Lizeth Riazo, estudiante de Sociología de esta Universidad, son más de dos mil estudiantes lo que afrontan esta situación, la gran mayoría de ellos pertenecientes a los estratos 1 y 2 y en condiciones de vulnerabilidad. “**Como estudiantes nosotros sabemos que esto se debe a los malos manejos que administrativamente se están llevando**” agregó Riazo.

Sobre la planta docente, los estudiantes señalan que gran parte de los profesores son de cátedra y ocasionales, contando con tan **solo 16 personas para 5 programas, el resto de docentes son de cátedra** y no tienen horas de asesoría, dificultando el proceso de aprendizaje. Le puede interesar: ["Reforma al Sistema de Educación se pasaría vía Fast Track"](https://archivo.contagioradio.com/reforma-al-sistema-de-educacion-se-pasaria-via-fast-track/)

Otra de las denuncias que hacen los estudiantes es que, pese a que ya están en la sexta semana de clase, no se están dando los almuerzos para los universitarios, que además hacen parte de un subsidio que **solo cobija a 241 de ellos**, seleccionados por cumplir requisitos como ser parte de estratos 1 y 2 y estar dentro de poblaciones vulnerables, condiciones que para Lizeth Riazo, son **"ilógicas" cuando la mayoría de los estudiantes cumplen con ellas y necesitan de un subsidio como el almuerzo**.

Referente a este tema, las directivas le informaron a los estudiantes que **hace falta hacer el proceso de licitación** para escoger al operador que preste el servicio, motivo por el cual se encuentra suspendido. Le puede interesar: ["Ser Pilo Paga ha gastado más de 350 mil millones del presupuesto de Educación"](https://archivo.contagioradio.com/ser-pilo-paga-se-llevo-373-290-470-719-del-presupuesto-de-educacion/)

A estas situaciones se suma el cierre de los baños, que según los estudiantes ya lleva varias semanas y que de acuerdo con la Institución se debe a que la Planta de Tratamiento de Aguas Residuales, está saturada, sin embargo, hay un presupuesto de **290 millones que ya se encontraría aprobado para arreglar los baños.**

En horas de la noche los estudiantes radicarán el pliego de exigencias para levantar el plantón, que esperan sea firmado por el recto Víctor Hugo Moreno Moreno y se comprometa a transformar las condiciones actuales de la Universidad, de igual forma, **los estudiantes conformaran un comité veedor de seguimiento al cumplimiento de exigencias**, que de no cumplirse, los estudiantes han expresado retomar el plantón.

###### Reciba toda la información de Contagio Radio en [[su correo]
