Title: Los retos del MAS tras resultados del referendo en Bolivia
Date: 2016-02-26 18:06
Category: El mundo, Política
Tags: Bolivia, Elecciones en Bolivia, Evo Morales, MAS
Slug: los-retos-del-mas-tras-resultados-del-referendo-en-bolivia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/bolivia-evo-saluda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [foto: diariolaposta ] 

###### [26 Feb 2016] 

El Tribunal Supremo Electoral de Bolivia, dio a conocer los resultados de el [referendo reeleccionista](https://archivo.contagioradio.com/empate-tecnico-en-referendo-reeleccionista-en-bolivia/) del 21 de febrero sobre la votación en donde el partido Movimiento Al Socialismo (MAS) perdió por tan solo 130 mil votos de diferencia; los resultados fueron 49% SI y 51% NO , l**o que significaría que en** **la campaña del 2019 para la presidencia de Bolivia, Evo Morales no podrá postularse.**

Ahora el reto para el partido MAS es encontrar a un sucesor que tenga buena aceptación por parte de los electores y que pueda aglutinar los movimientos sociales que han respaldado las políticas de Evo Morales.

Por su parte**, el mandatario agradeció a las comunidades por su apoyo,** y aseguró que va a seguir trabajando firmemente en su itinerario para consolidar el Estado Plurinacional, afirmo que “hemos perdido una batalla, más no la guerra” y reiteró su respeto a los resultados presentados por el TSE.

Adicionalmente afirmó que “la derecha no tiene ningún plan de desarrollo para el país, lo que sería contraproducente para la población boliviana” además recordó que los** resultados del referendo no significan una modificación a la constitución del Estado Plurinacional.**

**Los avances del gobierno del MAS**

Uno de los primeros elementos que evidencian los avances es que **Evo Morales ha sido el único presidente indígena de Bolivia y el que mas tiempo ha durado en el cargo**, y aùn después de 10 años logra mantener una aprobación de más del 57% de la población.

La participación de los movimientos sociales al cambiar la constitución política y en la que le dio la oportunidad al país de “refundar al estado”, también le permitió nacionalizar los hidrocarburos, el mejoramiento del transporte en la capital de este país, ha combatido el analfabetismo y finalmente han recibido mejoras de atención gratuita en centros de salud; lo que muestra un mejoramiento en la redistribución de la riqueza y en general el mejoramiento de la calidad de vida de los ciudadanos.
