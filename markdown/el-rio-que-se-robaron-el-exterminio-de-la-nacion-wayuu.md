Title: “El río que se robaron: el exterminio de la nación Wayúu”
Date: 2016-08-24 10:13
Category: eventos
Tags: crisis en la Guajira, El río que se robaron, Gustavo Guillén, río Ranchería
Slug: el-rio-que-se-robaron-el-exterminio-de-la-nacion-wayuu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/el-rio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Cartel Oficial 

##### [24 Agos 2016] 

Con un conversatorio que tendrá lugar en Bogotá, se estrena este 24 de Agosto el documental **“El río que se robaron: el exterminio de la nación Wayúu**”, un trabajo de investigación del periodista** Gustavo Guillén**, que pone en evidencia la [crisis humanitaria](https://archivo.contagioradio.com/las-propuestas-de-los-wayuu-para-superar-crisis-humanitaria-en-la-guajira/)que vive la Guajira por cuenta de la desaparición del río Ranchería.

La producción audiovisual da cuenta de lo ocurrido con el principal afluente hídrico de la región, retenido por la **represa El Cercado**, una obra que no suministra el líquido vital a las comunidades a pesar de haberse construido con ese propósito, situación que ha derivado en la muerte de miles de niños y adultos de las comunidades indígenas que habitan en uno de los territorios más áridos del país.

La minuciosa investigación de Guillén en el territorio Wayúu sumada al aporte de expertos en diferentes disciplinas, respaldan el contenido del documental, aval que le valió el ser utilizado como **prueba judicial ante la Comisión Interamericana de Derechos Humano**s, para que esta entidad concediera en 2015 medidas cautelares en favor de las comunidades, exigiendo al Estado que proteja la vida e integridad personal de los niños de esta comunidad indígena, garantizando el acceso al agua, a la alimentación y a la salud.

La charla y la proyección de “El río que se robaron: el exterminio de la nación Wayúu”, contará con la presencia de su director desde las 6 de la tarde, en el Aula Máxima de la Universidad Jorge Tadeo Lozano, Cra 4 \#22-61.

<iframe src="https://www.youtube.com/embed/Pw5atUNNTMY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
 
