Title: Gobierno "traicionó" el consenso en torno a ley de tierras: CSIVI FARC-EP
Date: 2017-05-22 13:00
Category: Nacional, Paz
Tags: FARC-EP, Ley de Tierras
Slug: gobierno-traiciono-el-consenso-en-torno-a-ley-de-tierras-csivi-farc-ep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/despojo-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [22 May 2017] 

De acuerdo con Jesús Santrich, integrante del secretariado de las FARC-EP, los cambios que hizo el Gobierno Nacional al proyecto de ley de tierras incumplen los consensos que se habían pactado, **refuerzan el modelo de ZIDRES y pasa por alto la Ley 160 de 1994** con la que se crea el Sistema de Reforma Agraria y Desarrollo Rural, permitiendo que latifundistas accedan a las tierras y que los campesinos continúen sin ellas.

La Comisión de implementación y verificación de los Acuerdos de Paz expresó en un comunicado de prensa, que el gobierno Nacional habría reformado el proyecto de ley de tierras en dos puntos importantes: el primero es que personas con más de **500 salarios mínimos podrían acceder a territorios baldíos y el segundo es que limita la participación de las comunidades** como se había previsto y que posteriormente iba a pasar a consulta previa con los representantes de las comunidades indígenas.

Para Santrich ambas propuestas modifican sustancialmente el acuerdo final de La Habana, “esto está rompiendo el orden normativo que hemos acordado, esto en medio de una situación que estamos padeciendo con la decisión arbitraria no jurídica sino política de la Corte Constitucional”. Le puede interesar: ["Exequibilidad de Ley ZIDRES atenta contra Banco de Tierras por la Paz"](https://archivo.contagioradio.com/exequibilidad-de-ley-zidres-atenta-contra-banco-de-tierras-por-la-paz/)

### **El Blanqueamiento de tierras** 

Hace dos semanas la Contraloría General de la Nación había dado a conocer un informe en el que instaba a la Agencia Nacional de Tierras a que activara mecanismos jurídicos para anular la adquisición de **322 predios baldíos que sumarían 123 mil hectáreas**, y que tendría que ser devueltas al Estado. Le puede interesar: ["Informe sobre acumulación de Baldíos se quedaría sin piso si se aprueba Ley de Tierras"](https://archivo.contagioradio.com/123-mil-hectareas-de-tierras-habrian-sido-adquiridas-irregularmente-contraloria/)

Sin embargo, con las modificaciones al proyecto de ley, los poseedores de tierras podrían continuar con la compra de baldíos sin impedimentos legales, **así en las investigaciones se les indique devolverlas.**

“No se puede hablar de una reforma Rural Integral, teniendo alrededor de 40 millones hectáreas en manos de latifundistas ganaderos y sumarle a ello, la posibilidad a personas que no están cobijadas en el acuerdo de acceder a la tierra, **aun cuando el favorecimiento debería ser para los sectores más empobrecidos, necesitados, para los desplazados**” afirmó Santrich.

Hasta el momento no se conoce ninguna manifestación por parte del gobierno frente a estas modificaciones, sin embargo, **los representantes de las comunidades indígenas a quienes se les iba a presentar el proyecto de ley decidieron levantarse de la mesa** por falta de garantías.

### **La Corte Constitucional y el día D+180** 

Referente a la decisión de la Corte Constitucional y las resoluciones que realizó al Fast Track, Santrich señaló que lo **fundamental es conquistar la paz, como un derecho y un deber de obligatorio cumplimiento** por lo que hace un llamado a la defensa de los acuerdos de La Habana. Le puede interesar: ["Somete al proceso de Paz a más obstáculos"](https://archivo.contagioradio.com/corte-constitucional-somete-al-proceso-de-paz-a-mas-obstaculos/)

De igual modo frente al cumplimiento del día D +180, Santrich afirmó “desde el momento en que la guerrilla de las FARC-EP pasó a los puntos y a las zonas veredales, saco de combate a las armas, las armas ya están registradas, nuestros pasos hacían la legalidad son evidentes, irrefutables, el problema es **qué va a pasar con la seguridad personal, con la seguridad jurídica y la socioeconómica**” y agregó que se debería pensar en una renegociación del calendario.

<iframe id="audio_18830262" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18830262_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
