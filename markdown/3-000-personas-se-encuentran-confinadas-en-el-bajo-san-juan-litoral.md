Title: 3.000 personas se encuentran confinadas en el Bajo San Juan Litoral
Date: 2017-04-21 16:27
Category: DDHH, Nacional
Tags: Chocó, San Juan del Litoral
Slug: 3-000-personas-se-encuentran-confinadas-en-el-bajo-san-juan-litoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [21 Abr 2017] 

**Aproximadamente 3.000 personas que hacen parte de las comunidades que habitan en la zona baja de San Juan Litoral, en Chocó**, continúan confinadas en sus casas debido a la confrontación de grupos armados en sus territorios, mientras que 32 familias salieron desplazadas de la vereda El Carra y se encuentran en el coliseo de Buenaventura.

Mientras tanto, la Fuerza Pública le ha expresado a las comunidades desplazadas que no puede garantizar un retorno a los territorios, debido a que continúan los combates; de acuerdo con Dagoberto Pretelt, representante legal de la comunidad de cabecera del consejo comunitario de Acadesan, **no les ha respondido el por qué aún no se han combatido estas estructuras paramilitares que ostentan tienen el control de esta región** del país.

“**No hay como hacer un retorno digno por la presencia de los actores armados**, las autoridades han dicho que están haciendo su trabajo, que seguirían con pie firme, pero que hay que abstenerse de entrar al territorio del Bajo San Juan”.

Las comunidades que no se han desplazado, están afrontando una falta de abastecimiento, debido a que desde **hace más de 20 días no han podido salir de sus hogares por temor a ser víctimas de estos grupos.** Le puede interesar:["Confinadas comunidades indígenas y afro en el Litoral del San Juan de Chocó"](https://archivo.contagioradio.com/san-juan-choco-paramilitares/)

El pasado 18 de abril, en el sector de la Playa , Río Pichima, municipio de Litoral de San Juan, Chocó, **fueron encontrados los cuerpos sin vida de Anselmo Cárdenas Victoria y su hermano Dalimiro Cárdenas Victoria**, que de acuerdo con testimonios de la comunidad habrían sido retenidos y desaparecidos por paramilitares de la estructura de las Autodefensas Gaitanistas de Colombia, el pasado domingo 16 de abril.

De igual forma Pretelt afirmó que la gobernadora del Valle del Cauca y el Alcalde de Buenaventura habían expresado que “**ya no contaban con los recursos suficientes para atender a la población desplazada”**, razón por la cual el próximo 27 de abril, en Bogotá, una delegación de las comunidades se reunirá con diferentes ministros para que salga una propuesta que brinde condiciones dignas de retorno. Le puede interesar:["600 personas del Río Truandó, Chocó fueron desplazadas por presencia paramilitar"](https://archivo.contagioradio.com/38661/)

La oficina de convivencia para la sociedad civil, se ha encargado de mantener a las 32 familias que se encuentran en el Coliseo de Buenaventura, sin embargo, Pretelt indicó que existe una preocupación con temas como el agua, que se están riesgo de tanques de agua lluvia, las condiciones para dormir y **un brote de gripa que se ha generado y que podría afectar a niños y adultos mayores.**

<iframe id="audio_18275624" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18275624_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
