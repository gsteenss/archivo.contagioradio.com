Title: De la onírica a la política
Date: 2018-11-28 09:44
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Alberto Carrasquilla, Alvaro Uribe, Carrasquilla, Fiscal General, Fiscal Martínez, politica
Slug: de-la-onirica-a-la-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/De-la-onírica-a-la-politica-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#### [Foto: PanAm Post]

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### **9 Nov 2018**

La naturaleza de la realidad política es el antagonismo. No sé qué hace fruncir más el ceño, si esa idea, o irónicamente, el antagonismo a esa idea. Sé que hay grandes esfuerzos por superar este marco comprensivo de la situación, no obstante, en Colombia ni por más liberalismo reciclado que ofrece la cultura educativa la cosa funciona.

[¡Es más! aún si se pensara que la falla fuera la comprensión de la naturaleza de dicha realidad, podríamos explicar e interpretar que en Colombia, mientras el Estado se encuentre bajo un secuestro absoluto por parte de élites amiguistas y familiares, la política seguirá siendo reemplazada por una “administración de problemas sociales”; la democracia seguirá siendo una encuesta que eleva a las instituciones (secuestradas) pero oprime a los actores sociales; el buen ciudadano será premiado y las personas que cuestionen el civilismo, por supuesto serán castigadas.  ]

[Entonces, aquellos que no pertenecemos a esa élite, seremos un universo lleno de particularidades, pero con una equivalencia básica: ser el resto.]

[¿Qué pasa si viene un nihilista y acusa al resto de no querer otra cosa diferente que el poder? Podríamos decir que sí, solo que muchos aún en medio de la fantasía de la horizontalidad, o la fantasía de no querer “parecerse a los poderosos”, paradójicamente se recubren de la moral de los poderosos con el fin de no observar el miedo que sienten… un miedo a perder lo que el amo les ha dado.  ]

[¿Qué haremos con esa corriente intelectual que calienta la onírica de la liberación a tal grado que en caso de que lleguemos a obtenerla, como lo han hecho otros pueblos, siempre se vuelcan a criticar el intento por organizar lo que se ha logrado?  Responder lo elemental: lo político implica el ataque, la composición, la descomposición, la lucha, la organización, la institucionalización (social-legal-valorativa) de poderes construidos colectivamente.]

[La naturaleza de lo político implica el retorno a la composición de un nuevo poder. Eso no gusta mucho a los que se dicen “anarquistas” pero que aún no comprenden por qué Franco terminó fusilando a todos, ganó la guerra, fue enterrado en mausoleo, le hicieron un homenaje hace nos días sin que la Unión Europa dijera nada, mientras a Durruti lo traicionaron desde sus mismas filas y a su tumba en Montjuic solo van unos pocos.]

[Mientras avancen alegres y vitoreadas las falacias sobre la política que la definen como una forma unilateral del conceso de “la mayoría”, mientras se crea ciegamente en el agonismo de los opuestos (los ni ni) sin tener presente el contexto colombiano, mientras los moralismos impriman su sello sobre el comportamiento de los manifestantes simplemente se seguirán montando rabiosas o cívicas obras de teatro, pero sin capacidad de beligerancia política, es decir, aquella capacidad de desestabilizar un régimen como el que tiene secuestrado al Estado colombiano.]

[Lo que hoy hacen por ejemplo Uribe, Carrasquilla y Martínez es la prueba tangible del que llamo yo, el secuestro del Estado Colombiano. Estos tipos y todos los que celebran el “orden por el orden” no son más que radicales institucionalistas, fanáticos republicanistas que pregonan el “orden”, es decir]**su estabilidad** [y no la del resto. Asegurándose por medio de sus aparatos de control, de su moral (ante todo hegemónica) que]**su estabilidad**[ sea vista también como la del resto.]

[En ese orden (y escribir esto no es sencillo) el sueño perjudicial de creer que la democracia es unanimidad necesita ser destruido. La violencia espontánea, rabiosa, eventual, es precisamente la manifestación más clara de continuar persistiendo en ese absurdo sueño; por ende, destruir ese sueño no es una incitación a la violencia como sí, una invitación a la formulación de tesis políticas bien fundamentadas, estudiadas, que logren guiar más allá de la realidad espontánea de los graves hechos que ocurren en el país, hacia caminos de determinación.]

[El principio del cambio, en sí, no es “reventarlo todo”, tampoco es la acción insospechadamente individual; sino que es la consciencia de la acción individual como producto de esas ideas, deseos, prácticas, memoria etc. transferidos por entornos colectivos a los individuos y que retornan interpretados a la escena de lo colectivo para construir el carácter político de una sociedad.]

[Es una onírica la que hoy literalmente se vende en paquetes fabricados por el liberalismo. El último de ellos, envuelto en el neologismo “economía naranja” ese idealismo puro reforzado con ideología liberal, aquella que entroniza a un individuo sin perspectiva de su pasado, que padece de parálisis de la crítica, que se ha convertido en un dócil comelón de proyectos de vida fabricados para él, pero para que crea que los ha concluido auténticamente y le llame “innovación” a la decisión ya tomada por otros… no en vano Descartes decía que es lo mismo tener los ojos cerrados sin abrirlos jamás, que vivir sin filosofía.]

[¿Qué hacer? Por ahora, es urgente la composición de escuelas políticas que por principio teórico se opongan y por principio práctico superen al liberalismo. Esa es una de las mayores necesidades de nuestro momento histórico. Para que las ciudadanías libres no sigan siendo presas de esa libertad envasada en una dura cascara que protege “la institucionalidad por la institucionalidad”, es necesario rechazar el título de ciudadano.]

[Asimismo, recordar a la hora de componer escuelas de formación política, que la colonialidad del poder, el neocolonialismo intelectual se manifiesta en no pocas universidades como ese sentido soterrado de la imposibilidad real de liberación. Es paradójico, pero poseer un título, en ocasiones viene atado a la incapacidad de pensar una libertad por fuera del liberalismo. Mas de una universidad está plagada de mamertos de derecha, de mamarrachos existencialistas o de poetas que se visten de izquierda para cantar a la libertad, eso sí, mientras esta no llegue pronto, porque saben que si llega, tendrían mucho sexo que perder y les tocaría pasar al bando eterno de los defensores de la libertad por la libertad, es decir: los egoístas del más puro material.]

[Por eso las escuelas deben ser libres, libres del liberalismo, compuestas no por ciudadanías libres, pues la simple apreciación de “ciudadanías libres” es una remisión directa a la cárcel ideológica del liberalismo. Cárcel que pasa “como si no existiera”, pero en la que se han encerrado las posibilidades de desestabilizar la jugarreta que tienen montada los poderosos en Colombia.]

[De la onírica a la política, hay la construcción de escuelas que desarrollen la crítica al liberalismo en el grado más alto de la explicación, la comprensión y la interpretación. A la par que solicita metodología apta no solo para los que han cultivado una vida académica, sino también para quienes han cultivado una lucha de base, popular, campesina, indígena. Mientras no se desarrollen escuelas políticas que construyan un antagonismo serio y digno rival del liberalismo, la estabilidad de los que tienen secuestrado al Estado colombiano se traducirá permanentemente en baja resistencia beligerante, en el “aquí no pasa nada” a pesar de tanto, tanto, tanto, tanto, pero tanto inconformismo.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
