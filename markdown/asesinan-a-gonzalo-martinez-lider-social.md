Title: Asesinan a Gonzalo Martínez, líder social en Pechindé Córdoba
Date: 2017-12-21 16:20
Category: DDHH, Nacional
Tags: cordoba, Lider social
Slug: asesinan-a-gonzalo-martinez-lider-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/WhatsApp-Image-2017-12-21-at-4.38.40-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASODECAS] 

###### [21 Dic 2017] 

Fue **asesinado Gonzalo Antonio Martínez, líder social y presidente de la Junta de Acción comunal de la vereda Pechindé**, en Córdoba, el pasado 20 de diciembre, en inmediaciones del corregimiento Piedras Blancas, en Carepa, Antioquía cuando se devolvía con su esposa y su bebé, recién nacido, a su vivienda.

De acuerdo con el testimonio de sus familiares, dos sujetos abordaron el vehículo público campero en el que se transportaba la familia, entre la vereda el Cerro, en Antioquia y la vereda el Llano, en Córdoba. **Allí uno de los sujeto se dirigió a Gonzalo y le dijo dijo “necesitamos hablar con usted”**. (Le puede interesar: ["32 líderes sociales han sido asesinados durante el 2017 en Cauca"](https://archivo.contagioradio.com/aumenta-preocupacion-por-asesinato-de-lideres-sociales-en-el-cauca/))

Gonzalo que tenía en sus brazos a su hija recién nacida, fue obligado por los dos hombres a que la dejara y los acompañara, **posteriormente su cuerpo fue hallado sin vida sobre la carretera con 4 impactos de bala**. Cabe resaltar que las comunidades han denunciado que en este territorio hay control de las Autodefensas Gaitanistas de Colombia.

La muerte de Gonzalo se suma a la ola de asesinatos a líderes sociales en el país, que esta semana ya ha dejado sin vida a 3 líderes más, Alfonso Pérez Mellizo en Cauca y en Putumayo a Nixon Martin Rosero, en el Tigre y Pablo Oviedo, en Puerto Asis.  (Le puede interesar:["Min Defensa pone en riesgo la vida de líderes sociales: Somos Defensores"](https://archivo.contagioradio.com/min-defensa-expone-la-vida-de-lideres-sociales-somos-defensores/))

###### Reciba toda la información de Contagio Radio en [[su correo]
