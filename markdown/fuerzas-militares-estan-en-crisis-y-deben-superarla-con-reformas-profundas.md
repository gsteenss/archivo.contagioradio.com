Title: Fuerzas Militares están en crisis y deben superarla con reformas profundas
Date: 2019-06-25 16:08
Author: CtgAdm
Category: Entrevistas, Nacional
Tags: crisis, Ejecuciones Extra Judiciales, falsos positivos, Fuerzas militares
Slug: fuerzas-militares-estan-en-crisis-y-deben-superarla-con-reformas-profundas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Crisis-de-las-Fuerzas-Militares.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Foto: @GuillermoBotero  
] 

Para el abogado **Reynaldo Villalba, integrante del Colectivo de Abogados José Alvear Restrepo**, **las Fuerzas Militares están en crisis desde hace tiempo porque no responden a las necesidades en seguridad del país** y no se apegan a la Constitución. Entre los hechos que marcan esta crisis se encuentran las denuncias por nuevos casos de ejecuciones extrajudiciales, las directrices en el Ejército expuestas por el New York Times y la 'cacería de brujas' que se vive al interior de las instituciones militares en contra de quienes son considerados posibles informantes de la prensa.

### **Ejecuciones extrajudiciales: ¿errores superados del Ejército?** 

Las ejecuciones extrajudiciales o mal llamados falsos positivos, con las que se hacía pasar personas asesinadas como muertas en combate, cuando en realidad se trataban de ejecuciones, es una realidad que instancias como la Organización de las Naciones Unidas (ONU) ha condenado. El escándalo por estas graves violaciones a los derechos humanos estalló en 2008, y como lo señala Vyllalva, "pensábamos que estaba superada esta situación"; pero nuevas denuncias iniciaron este año, especialmente, con el caso de [Dimar Torres](https://archivo.contagioradio.com/excombatiente-dimar-torres-habria-sido-asesinado-por-ejercito-en-el-catatumbo/).

Posteriormente, el diario New York Times hizo públicas algunas directrices del Ejército con las que se podrían revivir estos asesinatos a civiles. A ello se sumó la reciente investigación de la revista Semana, que también tenía la información del New York Times pero decidió publicarla solo hasta ahora, en la que uniformados del Ejército denuncian que se está haciendo una **'cacería de brujas' al interior de la Institución, para encontrar a quienes filtraron información a ambos medios de comunicación**.

En la publicación, Semana también afirma que tiene evidencias de seguimientos que s**e están haciendo y 'chuzadas' telefónicas contra miembros del Ejército que están entregando sus versiones voluntarias a la Jurisdicción Especial para la Paz (JEP**). A propósito de esta denuncia, Villalba manifestó que esos testimonios en la JEP resultaban valiosos para lograr establecer máximos responsables de estos actos, porque hasta el momento "no hay un solo general condenado por ejecuciones extrajudiciales pese a que se sabe que hubo compromiso en la promoción de políticas que permitieron este tipo de crímenes".

### **Las Fuerzas Militares están en crisis y deben solucionarlo**

Tras esta serie de denuncias ha surgido la idea de que hay una crisis al interior de las Fuerzas Militares; una idea que respalda Vyllalva, al señalar que "cuando no responden a las necesidades institucionales reales, cuando no se apegan a la Constitución y la ley, están en crisis". Y para superarla, se requieren reformas; pues como lo explicó el Defensor de derechos humanos, "se requiere una fuerza pública para la paz", y eso implica "muchas modificaciones". (Le puede interesar: ["Ejército habría torturado, asesinado e intentado desaparecer a Dimar Torres"](https://archivo.contagioradio.com/ejercito-habria-torturado-asesinado-e-intentado-desaparecer-a-dimar-torres/))

Una de esas modificaciones debería incluir el **cambio en el pensum y las materias que reciben los uniformados, pues está debería tener un énfasis en los principios de democracia y derechos humano**s, "o estaremos condenados a repetir la masacre". Adicionalmente, se tendría que **reemplazar las doctrinas que permiten que este tipo de crímenes ocurran como la de seguridad nacional o la de enemigo interno**; pues hacen creer que el enemigo es el sindicalista, o el periodista que denuncia.

### **La Fiscalía ha facilitado la impunidad, y con la JEP podría llegar la verdad**

Villalba afirmó que "la Fiscalía lamentablemente ha jugado un papel fundamental en materializar impunidades", pues aunque ha contado con información suficiente para vincular a altos oficiales en investigaciones sobre ejecuciones extrajudiciales, sólo en casos particulares lo ha hecho. En adición, tampoco han avanzado las investigaciones sobre personas amenazadas, por ejemplo, de los militares que han declarado ante la JEP y están en riesgo. (Le puede interesar: ["Son 1.615 miembros de la Fuerza Pública los que aún no se han reportado ante la JEP"](https://archivo.contagioradio.com/son-1-615-miembros-de-la-fuerza-publica-los-que-aun-no-se-presentan-ante-la-jep/))

En ese sentido, el Abogado lamentó que se esté atacando a quienes están brindado su versión a la JEP, "porque lo que quieren es amedrentar a todos los que están haciendo fila" para declarar ante este tribunal; no obstante, resaltó el trabajo de la Jurisdicción así como el compromiso de los militares, **"porque cuando los implicados están hablando de esa manera es porque no quiere repetir los hechos y porque sienten confianza en el juez que tienen al frente",** es decir, la JEP.

<iframe id="audio_37581308" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37581308_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
