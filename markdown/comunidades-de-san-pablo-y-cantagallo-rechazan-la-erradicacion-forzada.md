Title: Comunidades de San Pablo y Cantagallo rechazan la erradicación forzada
Date: 2015-03-24 21:53
Author: CtgAdm
Category: Movilización, Nacional
Tags: Cantagallo, Erradicación Forzada, Glifosato, protesta campesina, San Pablo, Sur de Bolivar
Slug: comunidades-de-san-pablo-y-cantagallo-rechazan-la-erradicacion-forzada
Status: published

###### Foto: vanguardia.com 

Según la información de la organización **“Congreso de los Pueblos”** el Domingo 22 de Marzo cerca de 400 campesinos y campesinas de los municipios de **Cantagallo y San Pablo, en el Sur de Bolívar** se tomaron algunas de las vías principales de esta región para exigir la suspensión de la erradicación forzada de cultivos de uso ilícito y exigir también que se implementen planes de erradicación voluntaria y concertada.

Los campesinos y campesinas se reunieron el lunes 23 de Marzo con Cristóbal Vanegas, alcalde de San Pablo y con el Comandante Operativo del Magdalena Medio, Teniente Coronel William Arias. Según Juan Hincapie, vocero de los campesinos “la conclusión final de la jornada…, sobre el tema de cultivos de uso ilícito es que, por parte de las instituciones, se comprometen a mantener los cultivos sin erradicar y sin sacar a las personas de los puntos de erradicación, mientras se empieza a concertar con las comunidades y los altos funcionarios del gobierno para llegar a algunos acuerdos muy precisos.”

[![cantagallo2-contagio-radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/03/cantagallo2-contagio-radio-300x219.jpg){.size-medium .wp-image-6402 .alignleft width="300" height="219"}](https://archivo.contagioradio.com/comunidades-de-san-pablo-y-cantagallo-rechazan-la-erradicacion-forzada/cantagallo2-contagio-radio/)

A pesar de esos acuerdos la comunidad se mantendrá en **Asamblea Permanente para esperar el cumplimiento por parte del gobierno nacional**, pero si persiste el incumplimiento los campesinos volverán al bloqueo de las vías.

Cabe recordar que estas comunidades protagonizaron multitudinarias movilizaciones en 1996 en contra de las **fumigaciones con glifosato sustentadas en el Plan Colombia**. Según la denuncia *“En esa ocasión se movilizaron alrededor de 300 mil campesinos que crearon la Coordinadora de Cultivadores de Coca y Amapola”.*

Con información del Congreso de los Pueblos.
