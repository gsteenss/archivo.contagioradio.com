Title: Aplazamiento del juicio de Santiago Uribe es una nueva agresión a las víctimas
Date: 2019-11-08 17:42
Author: CtgAdm
Category: Entrevistas, Judicial
Tags: alvaro uribe velez, Los 12 apostoles, Paramilitarismo, Santiago uribe
Slug: aplazamiento-del-juicio-de-santiago-uribe-es-una-nueva-agrasion-a-las-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Santiago-Uribe.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @sermeca  
] 

El pasado jueves 7 de noviembre se i**nformó sobre el aplazamiento de la audiencia de alegatos finales que se adelantaba en el marco del caso sobre los 12 apóstoles y el asesinato de Camilo Barrientos,** en el que está vinculado Santiago Uribe Vélez. Ante la decisión del juez, abogados de las víctimas pidieron que el aplazamiento no fuera extenso, porque "después  de 25 años, es justo que el país sepa si esta familia (Uribe Vélez) sí tiene relación con la conformación de este grupo paramilitar". (Le puede interesar: ["Inicia audiencia final contra Santiago Uribe en caso los 12 Apóstoles"](https://archivo.contagioradio.com/inician-las-audiencias-finales-contra-los-12-apostoles-y-santiago-uribe/))

### **Un nuevo aplazamiento, ¿más garantías?** 

Antes del inicio de las audiencias de alegatos finales que estaban agendadas para el 6 de noviembre, **el abogado defensor de derechos humanos, Daniel Ernesto Prado**, afirmó que la defensa de Santiago Uribe había intentado aplazar la diligencia judicial alegando que su vocero, Jesús Albeiro Yepes, había recibido el poder para representarlo con solo un mes de anterioridad. No obstante, Prado resaltó que el juez primero Penal Especializado de Antioquia no concedió este aplazamiento, considerando que el vocero conocía el caso, por lo menos, desde diciembre de 2018.

Luego de dos días de audiencias  la Fiscalía pidió que se condenara a Uribe Vélez por los delitos de concierto para delinquir y homicidio, mientras la Procuraduría consideró que solo se lo debía encontrar culpable por el primer delito ya que tenía dudas sobre el vínculo del ganadero con la comisión del segundo delito. Respecto a esta posición, Prado manifestó que esperaban que se acogiera la solicitud de la Fiscalía, tomando en cuenta que "está probado que la mayoría de muertes que se dieron en esta región del país por esta época fueron causadas por el grupo paramilitar de los 12 apóstoles, y si Santiago era la persona que dirigía y daba las órdenes, él debe responder por la muerte de Camilo Barrientos".

La audiencia habría continuado este viernes con los alegatos por parte de la defensa de Santiago Uribe, pero **sus abogados pidieron tiempo para estudiar lo afirmado por Procuraduría y Fiscalía y el juez decidió conceder este plazo**, que según el abogado, podría estar entre los 3 y 4 meses. (Le puede interesar: ["Defensa de Santiago Uribe es más mediática que jurídica: Daniel Prado"](https://archivo.contagioradio.com/defensa-de-santiago-uribe-es-mas-mediatica-que-juridica-daniel-prado/))

### **"La administración de justicia va a condenar a Santiago Uribe, su defensa lo sabe y si no, no pedirían el aplazamiento"** 

Prado aseguró que era insólito que una audiencia como la que se desarrollaba donde se está terminando un juicio se viera interrumpida por un periodo de meses porque la defensa pidió tiempo para presentar sus alegatos, y manifestó que aplazamientos así no ocurren todo el tiempo, "solo ocurre porque es el hermano de Álvaro Uribe Vélez". Al tiempo, el abogado señaló que tenía la seguridad de la condena contra Santiago Uribe, y "la defensa de él lo sabe, si no, no pedirán aplazamiento porque ellos saben que el material probatorio es suficiente para condenarlo".

Pese a su seguridad, Prado expresó que la defensa de Uribe puede en estos meses intentar que se cambie el juez o acudir a otra maniobra para dilatar el proceso pero recordó que las familias de las víctimas están esperando que se respeten sus derechos y llegue pronto y de manera efectiva la administración de justicia. **"No tenemos problemas con que se aplace la diligencia, pero sí que el plazo no sea demasiado extenso porque este país clama por una verdad** y debe haber una decisión judicial sobre este tema que nos permita tener la claridad", sostuvo el defensor de derechos humanos.

Y recordó que han pasado 25 años desde que se denunciaron los hechos por los que está investigado Santiago Uribe y después de ese tiempo, **"es justo que el país sepa si esta familia sí tiene relación con la conformación de este grupo paramilitar**, y si tienen responsabilidad en más o menos 509 crímenes en esta región". (Le puede interesar: ["Una vez más suspenden audiencia contra Santiago Uribe"](https://archivo.contagioradio.com/una-vez-mas-suspenden-audiencia-contra-santiago-uribe/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_44219549" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44219549_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
