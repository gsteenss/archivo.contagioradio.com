Title: Asesinan a dos hermanos campesinos en Bolívar, Cauca
Date: 2020-09-10 14:52
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Cauca, Gerardo Antonio Ruíz, Hermanos Ruíz, Hernán Ruíz, La Yunga
Slug: asesinan-a-dos-hermanos-campesinos-en-bolivar-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Masacres-por-departamento-1.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Asesinato-de-los-hermanos-Ruiz-en-Bolivar-Cauca.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La noche de este miércoles **fueron asesinados los hermanos Gerardo Antonio Ruíz y Hernán Ruíz en el sector de La Yunga, zona rural del municipio de Bolívar, Cauca.** (Le puede interesar: [Bolívar es víctima de 2 nuevas masacres](https://archivo.contagioradio.com/bolivar-es-victima-de-2-nuevas-masacres/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Campesinos que caminaban por el sector, hallaron los dos cuerpos sin vida, los cuales evidenciaban violencia y varios impactos de arma de fuego. (Le puede interesar: [En el Bajo Cauca se registra masacre de 6 personas](https://archivo.contagioradio.com/se-registra-masacre-de-6-personas-en-en-el-bajo-cauca-antioqueno/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según versiones de la población los hermanos Ruíz obtenían su sustento como agricultores en la zona y aún se desconocen los perpetradores del homicidio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La comunidad junto con organismos de socorro, trasladó los cuerpos desde el sitio en el que fueron encontrados, hasta la morgue del hospital local. (Le puede interesar: [Instalan Campamento humanitario por la vida ante la crisis humanitaria en Colombia](https://archivo.contagioradio.com/instalan-campamento-humanitario-por-la-vida-ante-la-crisis-humanitaria-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Ruben723/status/1304100572228976640","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Ruben723/status/1304100572228976640

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Cauca es uno de los departamentos más afectados por la violencia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según registros del Instituto para el Desarrollo y la Paz -Indepaz-, en lo que va corrido del año se han presentado [205 asesinatos](http://www.indepaz.org.co/lideres/) en contra de líderes(as) sociales y defensores(as) de Derechos Humanos, de los cuales **70 se han perpetrado en el departamento del Cauca, es decir más de un 34% de los crímenes.** (Lea también: [Cierre del mes de Agosto fue fatídico para líderes sociales en Colombia](https://archivo.contagioradio.com/cierre-del-mes-de-agosto-fue-fatidico-para-lideres-sociales-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, en el informe sobre masacres, también publicado por Indepaz;  **Cauca es -junto con Nariño- el segundo departamento más afectado en el país con ocho (8) masacres perpetradas**; superado únicamente por Antioquia con doce (12). (Lea también: No cesa la barbarie: 55 masacres en 2020)

<!-- /wp:paragraph -->

<!-- wp:image {"id":89591,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Masacres-por-departamento-1.png){.wp-image-89591}

</figure>
<!-- /wp:image -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
