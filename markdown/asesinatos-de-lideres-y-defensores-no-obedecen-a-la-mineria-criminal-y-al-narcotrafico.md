Title: La Comisión que tendrá la tarea de combatir el paramilitarismo
Date: 2017-02-02 13:14
Category: Nacional, Paz
Tags: Asesinatos, Lideres, Mineria, paz
Slug: asesinatos-de-lideres-y-defensores-no-obedecen-a-la-mineria-criminal-y-al-narcotrafico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/paramilitarismo-comision-de-segurida.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] [La Nación]

###### [02 Feb 2017] 

La **Comisión de Garantías de Seguridad**, que está por instalarse y a la espera de un decreto presidencial, no ha sesionado de manera formal y por lo tanto no tiene una postura definida frente a los recientes asesinatos de líderes sociales y defensores de Derechos Humanos. Así lo explica **Gustavo Gallón, director de la Comisión Colombiana de Juristas e integrante de la Comisión de garantías.** Le puede interesar: [Colombia reportó 85 asesinatos contra Defensores de DDHH en el 2016](https://archivo.contagioradio.com/colombia-reporto-85-asesinatos-contra-defensores-de-ddhh-en-el-2016/)

Estas aseveraciones las hizo Gallón, luego de conocerse la declaración del **Min. Interior que afirma que los asesinatos se deben a hechos relacionados con la minería ilegal y el narcotráfico,** en territorios en los que antes operaban las estructuras de las FARC. Según el jurista, aunque hay hechos que se pueden relacionar, no se puede definir esa postura como la oficial, puesto que falta mucha información en torno a los asesinatos. Le puede interesar: [Denuncian cuatro asesinatos simultáneos en el sur de Córdoba](https://archivo.contagioradio.com/atencion-denuncian-cuatro-asesinatos-simultaneos-en-el-sur-de-cordoba/)

**La Comisión Nacional de Garantías de Seguridad es uno de los escenarios creados en el acuerdo de paz entre el gobierno y las FARC** y que tiene como objetivo principal el desmantelamiento del paramilitarismo, para lo cual debe diseñar un  plan de acción que se coordinaría para su aplicación con las instituciones del Estado. En el plan se incluyen reformas normativas, ajustes sobre inteligencia y supervisión de la seguridad privada. Le puede interesar: [Asesinatos de líderes sociales son práctica sistemática: Somos Defensores](https://archivo.contagioradio.com/asesinatos-de-lideres-son-practica-sistematica-33507/)

**Durante las próximas horas se espera que el gobierno expida el decreto que pone en marcha la CNGS** para que arranque en firme su funcionamiento y se posesionen en ella los ministerios de Justicia, Interior y Defensa, así como la Fiscalía, la Defensoría del Pueblo, los comandantes de las FFMM y de Policía, dos representantes de las plataformas de DDHH y representantes de otras organizaciones políticas.

<iframe id="audio_16796326" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16796326_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]

<div class="ssba ssba-wrap">

</div>
