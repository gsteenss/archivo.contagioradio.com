Title: Venta de papa en peajes no resuelve la crisis estructural del sector que ha quebrado a 116 mil productores
Date: 2020-11-13 18:22
Author: AdminContagio
Category: Nacional
Tags: Papa, Papatón, Paperos
Slug: venta-de-papa-en-peajes-no-resuelve-la-crisis-estructural-del-sector-que-ha-quebrado-a-116-productores
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Paperos.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/mix_14m57s-audio-joiner.com_.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Paperos salen a las carreteras a vender su producto / Foto: Cesar Melgarejo

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la crisis de los paperos en Colombia, que según algunos registros son más de **116 mil productores, la mayoría de los cuales son pequeños, pues el 80% tiene el producto sembrado en una sola hectárea;** la [Gobernación](https://twitter.com/CundinamarcaGob) de Cundinamarca está impulsando una iniciativa para que este puente festivo los paperos puedan vender sus productos en los peajes de algunas de las vías del departamento. (Le puede interesar: [Otra Mirada: Las caras de la crisis económica](https://archivo.contagioradio.com/otra-mirada-las-caras-de-la-crisis-economica/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CundinamarcaGob/status/1327219665135955969","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CundinamarcaGob/status/1327219665135955969

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Pese a que los paperos reconocieron la iniciativa y la consideran un buen primer paso para aliviar la crisis de los productores, cuestionaron la **falta de logística por parte de la Gobernación** para permitir a los campesinos desplazarse a los peajes o disponer de alguna especie de mobiliario como carpas y estantería para que pudieran ubicarse de manera organizada y a cubierto de las inclemencias del clima.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Alex Sierra, productor de papa y co-creador de la Fundación “Alimentando Semillas”, señaló que **la Gobernación no los había tenido en cuenta y que temía que quienes se iban a ver beneficiados con la iniciativa iban a ser los mismos intermediarios que cuentan con camiones para transportar el producto desde las fincas hacía los peajes;** medio con el que no cuentan la gran mayoría de labriegos que cultivan los campos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, invitó a las personas a apoyar la iniciativa de la Gobernación y otras como la **“*Papatón*”**, promovida inicialmente por los campesinos, para contribuir a llevar un sustento a estas familias; al tiempo que pidió a los consumidores adquirir el alimento a un precio justo.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “*Los que vayan a comprar la papita, no vayan a regatear; hay que pensar siempre en todo el esfuerzo y el trabajo que hay detrás de una papa*”
>
> <cite>Alex Sierra, productor de papa</cite>

<!-- /wp:quote -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AlirioUribeMuoz/status/1326138338420592641","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AlirioUribeMuoz/status/1326138338420592641

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Sierra añadió que si bien este tipo de medidas son un buen primer paso para ayudar a los agricultores, es necesario **atender las causas estructurales de la crisis** **de los paperos y agricultores en general**, como intervenir y modificar el funcionamiento de las **centrales mayoristas donde, según él, se especula con los precios,** **perjudicando tanto a los campesinos como al consumidor final quien recibe más caros los productos.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *“Eso es como cuando existía en Bogotá el Bronx, o cuando existía el Cartucho, todo el mundo sabía que existía una “olla” pero nadie hacia nada; lo mismo pasa **en Corabastos, todo el mundo conoce las mafias que se mueven ahí… Los comerciantes que manipulan el precio de los productos en el país entero, que le pagan al campesino el precio que les da la gana y simplemente nadie hace nada**”.*

<!-- /wp:quote -->

<!-- wp:paragraph -->

También recalcó en la importancia en que el Gobierno invierta en el campo e **implemente subsidios**  para los campesinos, la **construcción de vías** que permiten facilitar la salida de los productos del campo hacia la ciudad, **impedir la entrada de productos del agro** que se produzcan en el ámbito nacional desde el exterior y **formar al consumidor para que adquiera los productos directamente del campo evitando el margen de intermediación.**

<!-- /wp:paragraph -->

<!-- wp:audio {"id":92826} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/mix_14m57s-audio-joiner.com_.mp3">
</audio>
  

<figcaption>
Alex Sierra, productor de papa

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small"} -->

Si desea profundizar en este tema le invitamos a ver: *Otra Mirada- Del campo a la ciudad y sin intermediario*

<!-- /wp:paragraph -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=udwdcVcv7jM","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->

<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio">
<div class="wp-block-embed__wrapper">

https://www.youtube.com/watch?v=udwdcVcv7jM

</div>

</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

**  
  
**

<!-- /wp:paragraph -->
