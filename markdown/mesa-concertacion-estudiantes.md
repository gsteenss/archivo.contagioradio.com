Title: Así funcionará mesa de concertación entre estudiantes y Gobierno
Date: 2018-11-02 12:20
Author: AdminContagio
Category: Educación, Movilización
Tags: Educación Superior, estudiantes, Mesa de Concertación, UNEES
Slug: mesa-concertacion-estudiantes
Status: published

###### [Foto: David Guerra] 

###### [2 Nov 2018] 

Luego del primer encuentro en la mesa de concertación, con la que se espera solucionar la crisis de educación superior que enfrenta el país, **estudiantes y representantes del Gobierno sólo llegaron a acuerdos programáticos.** Entre los convenios alcanzados se encuentran la participación en la mesa de garantes externos al proceso, los temas de la próxima reunión y las formas en que las partes participarán en la Mesa.

Durante las 8 horas que duró la reunión, los temas abordados fueron de tipo operativos, acordando la forma en que será abordada la mesa en el futuro. Lo que incluye la emisión de actas sobre los logros alcanzados después de cada reunión, la participación de las partes en la mesa (y de ser necesario, asesorías técnicas para las mismas), y de **garantes en el proceso.** (Le puede interesar: ["Las 3 condiciones para la mesa de concertación con el Gobierno"](https://archivo.contagioradio.com/condiciones-mesa-de-concertacion/))

Tras el encuentro las partes llegaron a algunos compromisos: mientras el **Ministerio de Educación Nacional (MEN)** se encargará de la logística de la mesa, establecer un contacto con **el Ministerio de Hacienda para que también tenga lugar en la negociación**, y lograr que Procuraduría y Contraloría sean garantes de las negociaciones; los estudiantes llevarán propuestas sobre el financiamiento de la educación superior, e invitaran a Alberto Brunori, representante de la Oficina de Derechos Humanos de la ONU, para que se sume en condición de garante internacional.

Otro de los acuerdos logrados entre las partes fue la definición de una agenda de encuentros que se desarrollarán la próxima semana,  empezando el **martes 6 de noviembre a las 9 am;** donde se discutirá la financiación de las Universidades para terminar 2018 y funcionar durante 2019.  Adicionalmente, **se pedirán garantías para la terminación de los semestres académicos, en vista de la suspensión de actividades en varias Instituciones de Educación Superior (IES)**.

**Valentina Ávila, integrante de la UNEES** y quien participó en las reuniones con el Gobierno, sostuvo que también solicitarán al MEN adelantar una **reunión con la Federación Nacional de Departamentos, por ser este el organismo encargado de entregar el billón de pesos proveniente de regalías** para la educación superior pública aprobado por el Gobierno. (Le puede interesar: ["Las 10 exigencias del movimiento estudiantil al Gobierno Nacional"](https://archivo.contagioradio.com/exigencias-movimiento-estudiantil/))

Ávila recordó que el Frente Amplio por la Educación Superior, del que hacen parte la UNEES, Acrees, Fenares y ASPU, tiene puntos en común para exigir al Gobierno. Sin embargo aún no definen un pliego único de exigencias y afirmó que desde las organizaciones, **hay voluntad unitaria para trabajar en Acuerdos que permitan a la educación superior salir de la crisis en la que se encuentra**.

\[caption id="attachment\_57962" align="alignnone" width="667"\][![WhatsApp Image 2018-11-02 at 10.23.39 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-02-at-10.23.39-AM-667x800.jpeg){.wp-image-57962 .size-medium width="667" height="800"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-02-at-10.23.39-AM.jpeg) Acta de Funcionamiento de la Mesa\[/caption\]

\[caption id="attachment\_57961" align="alignnone" width="720"\][![WhatsApp Image 2018-11-02 at 10.23.40 AM](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-02-at-10.23.40-AM-720x790.jpeg){.wp-image-57961 .size-medium width="720" height="790"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/11/WhatsApp-Image-2018-11-02-at-10.23.40-AM.jpeg) Acta de Funcionamiento de la Mesa\[/caption\]

<iframe id="audio_29798431" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29798431_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
