Title: Buenaventura pide 2.5% de los 5 billones que produce el puerto anualmente
Date: 2017-05-30 13:18
Category: Movilización, Nacional
Tags: buenaventura, Movilización, Paro Buenaventura
Slug: fondo-autonomo-seria-la-solucion-al-paro-civico-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/buenaventura-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Plural] 

###### [30 may 2017] 

Ante la negativa del gobierno de decretar la emergencia social y económica en Buenaventura, pobladores le han pedido **al gobierno que apruebe un plan de financiación que recoge las necesidades de los bonaverenses** y el cual piden que sea manejado por ellos mismos. El paro completa 15 días y se plantea como única salida la creación de dicho "fondo autónomo".

Según Manuel Bedoya, presidente de la Organización Nacional de Pescadores Artesanales, **“hemos recogido las necesidades de los pobladores que equivalen a 10 billones y medio de pesos”.** El pliego de necesidades por resolver se lo hicieron llegar al Congreso de la República para que les apruebe el presupuesto que necesitan y así poder hacerle frente a la falta de declaratoria de emergencia social y económica. Le puede interesar:["UNISON le exige al presidente Santos brindar garantías en paro de Buenaventura"](https://archivo.contagioradio.com/unison-le-exige-al-presidente-santos-brindar-garantias-en-paro-de-buenaventura/)

La propuesta de los bonaverenses es crear un **fondo autónomo que, según ellos, “no cuente con la presencia de corruptos y politiqueros”.** Bedoya afirmó que, “el congreso tiene la capacidad de aprobar un presupuesto de 10 mil 500 millones de pesos. Queremos que nos devuelvan el 2.5% de los 5 billones que le entregamos al país cada año”.

En Buenaventura están dispuestos a crear un mecanismo que cuente con la veeduría necesaria para implementar los acuerdos que subsanan las necesidades de los pobladores. **Para esto han dicho que van a crear una fiducia particular** que les permita contar con la administración del dinero y así garantizar que los dineros que recibe este departamento, sean bien invertidos. Le puede interesar: ["Hasta los niños son víctimas del Esmad en Buenaventura"](https://archivo.contagioradio.com/se-recrudece-la-represion-a-paro-civico-de-buenaventura/)

### **A pesar del compromiso del gobierno continúa la represión** 

Desde que se inició el Paro Cívico en el puerto más importante sobre el pacífico colombiano, uno de los sitios neurálgicos por su posición estratégicxa para el puerto ha sido el sector conocido como "La Delfina". Durante los 15 días de paro se han reportado varias **arremetidas de la Fuerza Pública que hace presencia con ESMAD y ejército.**

Sin embargo, pese a la fuerte represión las comunidades negras del Puerto junto a las comunidades indígenas **se mantienen en el cierre nocturno de la vía** y solamente permiten en el paso de transporte de pasajeros de 6 de la mañana a 6 de la tarde.

Este martes se reportó nuevamente, **una agresión del ESMAD a la población desde las 5:30 de la mañana.** Aunque se menciona que hay varios civiles heridos y hasta desaparecidos, aun no se tienen reportes completos de las afectaciones.

<iframe id="audio_18980608" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18980608_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
