Title: Latinoamérica abrió sus puertas para atender crisis migratoria
Date: 2015-09-08 15:26
Category: DDHH, El mundo, Nacional
Tags: Asilo, crisis migratoria, Cuotas, inmigrantes, Latinoamérica, Nicolas Maduro, Ppe Mujica, Refugiados, Siria, Unión europea
Slug: latinoamerica-abrio-sus-puertas-para-atender-crisis-migratoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/inmigrantes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Telemundo33.com 

###### [08 sep 2015] 

Mientras en la Unión Europa se trata de crear estrategias en materia política, económica y social, para brindar refugio a los miles de inmigrantes que buscan entrar a su territorio, con planes como el de “[las cuotas](https://archivo.contagioradio.com/union-europea-definiria-cuotas-de-refugio-a-inmigrantes/)”, Latinoamérica también se une a esta causa y abre sus puertas a algunos de los inmigrantes.

### **Brasil** 

Dilma Roussef fue la primera mandataria latinoamericana en pronunciarse sobre esta situación indicando “**la disposición del Gobierno de recibir a aquellos quienes, expulsados de su patria, quieran venir a vivir, trabajar y contribuir para la prosperidad y la paz de Brasil**".

Desde el 2011, Brasil ha brindado asilo a 2 mil sirios desplazados. Tras la implementación del programa “Siria”, que busca reunificar familias y permitir la entrada legal de sirios sin antecedentes penales, esta vez el país ofreció visas humanitarias a **1.700 personas y otro tipo de documentos a otras 4.000**.

### **Uruguay** 

El expresidente de Uruguay, Pepe Mujica en el 2014 fue uno de los mandatarios que más se movilizó y pidió apoyo latinoamericano para apoyar la situación que viven a diario miles de inmigrantes en Europa.

Uruguay ha recibido hasta la fecha **cinco familias que suman 42 personas** y para finales de este año **espera otras siete familias** para a las que se les dará asilo.

### **Venezuela** 

El Presidente Nicolás Maduro dijo durante Consejo de Ministros que “**Venezuela va a acoger a 20 mil compatriotas sirios**” y dio instrucciones a su canciller, Delcy Rodríguez, para que empiece a realizar las gestiones pertinentes con el gobierno Sirio.

### **Otros:** 

En México la ciudadanía lanzó una petición informal para **que Peña Nieto conceda asilo a 10.000 sirios**. Así mismo Argentina ha recibido a **un centenar de inmigrantes y le prometió asilo a cincuenta familias. Por su parte, **el gobierno Chileno está evaluando la posibilidad de **acoger a entre cincuenta y cien familias**, según afirmó el ministro de Relaciones Exteriores de ese país.
