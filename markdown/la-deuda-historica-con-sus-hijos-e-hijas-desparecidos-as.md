Title: La deuda histórica con sus hijos e hijas desparecidos/as
Date: 2020-11-10 17:36
Author: CtgAdm
Category: A quien corresponda, Opinion
Tags: a quien corresponda, colombia, crimen de lesa humanidad, Desaparición forzada
Slug: la-deuda-historica-con-sus-hijos-e-hijas-desparecidos-as
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

***Por haber practicado la justicia y el derecho, vivirá.*** Ezequiel 33,16. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*¿No habrá manera de que Colombia,  en vez de matar a sus hijos, los haga dignos de vivir? Si Colombia no puede responder a esta pregunta,  entonces profetizo una desgracia:  Desquite resucitará,  y la tierra se volverá a regar de sangre, dolor y lágrimas.* -Elegía a Desquite, Gonzalo Arango 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Querida y contradictoria Colombia, **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un saludo fraterno,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con respeto y aprecio escribo para recordarle, informarle o hacerle tomar conciencia que tiene una “deuda” histórica con sus hijos e hijas desparecidos/as.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Deuda moral, ética, social y humana. Deuda con la verdad, la justicia, la memoria y la dignidad.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desconocer esta “deuda”, minimizarla o retrasar, indefinidamente, su “cancelación” es un atropello brutal a las víctimas y una grave revictimización a sus familiares y amigos. Además, la daña a usted misma, porque pierde credibilidad, “autoridad” y respeto, se desprestigia ante el mundo y ante los suyos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta “deuda” produce daños irreparables a familiares, amigos/as y compañeros/as de trabajo, de militancia política, de luchas sociales, por los derechos humanos y ambientales de las víctimas de desaparición forzada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afecta a familiares y amigos que viven diariamente un infierno indescriptible, el infierno de la incertidumbre y afectaciones humanas, psicológicas, económicas y sociales causadas por la desaparición de sus seres queridos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ellas y ellos están sometidas/os a la permanente, cruel y despiadada tortura de no saber qué pasó son sus seres queridos, si están vivos o no, de no poder darles una digna y humana sepultura… La tortura del sentimiento de culpa por no haber impedido la desaparición, por no haber estado para protegerlos; igualmente, por el sentimiento de culpa al sentir rabia con sus seres queridos desaparecidos por “andar metido en esas cuestiones”: derechos humanos, militancia política, cuestiones sociales o ambientales…

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El trato cruel e inhumano al que son sometidas las víctimas, y la tortura que viven sus seres queridos, hace de la desaparición forzada como un crimen de lesa humanidad.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Le recuerdo, mi querida y contradictoria Colombia, que el artículo 5 de la **Convención Internacional para la Protección de todas las Personas contra las Desapariciones Forzadas**, dice que *“La práctica generalizada o sistemática de la desaparición forzada constituye un crimen de lesa humanidad tal como está definido en el derecho internacional aplicable y entraña las consecuencias previstas por el derecho internacional aplicable”.*  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### ¿Qué significa un crimen de **lesa humanidad**?

<!-- /wp:heading -->

<!-- wp:paragraph -->

El término “Lesa” viene del latín “laesae”, que significa: herir, injuriar, causar daño. El término “Humanidad”, en este contexto, se refiere al conjunto de los seres humanos. El crimen contra la humanidad es el intento de privar a la humanidad de riquezas esenciales, inherentes a la especie humana como especie, como el ser compuesta por diversidad de razas, etnias, concepciones políticas, religiosas, ideológicas… la gravedad del crimen **lesa** **humanidad** [***“****podría concebirse en el triple sentido: de crueldad para con la existencia humana; de envilecimiento de la dignidad humana; de destrucción de la cultura humana. Comprendido dentro de estas tres acepciones, el crimen de lesa humanidad se convierte sencillamente en ‘crimen contra todo el género humano*”.](http://www.javiergiraldo.org/spip.php?article82)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimada Colombia, no tomar en serio el crimen de la desaparición forzada la daña a usted misma, porque la hace ver como una patria-matria a quien le desaparecen sus hijos e hijas y “sigue como si nada pasara”, indolente frente al profundo sufrimiento de sus familiares y amigos/as. La hace como una mala madre, y esto tanto en “casa” como ante la familia de las naciones, con quienes usted intenta quedar muy bien. No olvide sus compromisos solemnes con el mundo y que, aunque quiera desconocerlos o minimizar lo que está pasando, no puede. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le escribo también, para pedirle que vea, analice y tome en serio lo que pasa con tres grupos de sus hijos e hijas, que explico a continuación: 

<!-- /wp:paragraph -->

<!-- wp:list -->

-   **Un pequeño grupo de ellos y ellas,** creó un sistema muy efectivo para adueñarse de la economía y poner la vida, la política, la democracia, los valores que orientan la sociedad y la naturaleza, al “servicio” de los intereses económicos. Ese sistema discrimina, margina, atropella y excluye, por diversos medios, a la mayoría de sus hijos e hijas. Y quienes se oponen o proponen alternativas son calumniados, amenazados, desplazados, exiliados, torturados, asesinados o desaparecidos. Mucho dinero, poder y lujo están marcados con sangre, ilegalidad, corrupción, trampas e injusticias.

<!-- /wp:list -->

<!-- wp:list -->

-   **Otro grupo,** muy numeroso, son víctimas del pequeño grupo: por la violencia socio-política contra quienes se oponen a ellos y no se someten a sus intereses, por el despojo de sus tierras, por la quiebra prevista de pequeñas y medianas empresas, por la entrega de los recursos nacionales a intereses extranjeros con negocios torcidos a cambio de sobornos y dineros bajo la mesa,  porque la salud, la educación, los servicios públicos… fueron convertidos en negocios privados. 

<!-- /wp:list -->

<!-- wp:list -->

-   **Un tercer grupo,** la inmensa mayoría de tus hijos e hijas, son los que aparecen como “indiferentes” o al margen de todo lo que pasa.

<!-- /wp:list -->

<!-- wp:paragraph -->

Creo que lo pueden hacer por dos razones: una, porque andan preocupados/as por conseguir el diario para “sobrevivir”, por las extensas jornadas de trabajo que no alcanza para vivir con dignidad; otra, por el uso, en apariencia muy inteligente, de los medios de información y de las redes sociales, para impedir que vean que sus problemas económicos, de salud, educación, vivienda, servicios públicos, desempleo o subempleos… son causados por el pequeño grupo, que es dueño de la mayoría de esos medios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Éstos y las redes sociales, hacen que la gente mire para otro lado y no vea por qué están viviendo como están viviendo, que no se pregunte por las causas reales de la realidad social. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta realidad explica por qué la mayoría de sus hijos e hijas, son indiferentes con la tragedia humana, social y política de la desaparición forzada, por qué no dimensionan lo que ha pasado, y por qué piensan o dicen que “*si lo desaparecieron… por algo seria”, “en algo andaba metido”, “estaba en el lugar equivocado”*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, la inmensa mayoría de las y los desaparecidos no son de estratos altos, o han sido criticados por el sistema implantado por el pequeño grupo. Esto explica la inoperancia estratégica del Estado para hacer “justicia”, y que intente eliminar, real o simbólicamente, a quienes piensan y son distintos, enviando el mensaje a la sociedad de que “no diga”, “no reclame”, “no busque”, “quédese callado”, “déjele las cosas a Dios”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así, y con otros mecanismos y estrategias, el sistema produce miedo, rabia, impotencia y desesperanza en quienes buscan tanto a sus seres queridos, como a la verdad y la justicia; y así se oculta o esconde a los autores, determinadores y beneficiarios de los crímenes de lesa humanidad, y de manera especial quedan inmunes por una impunidad estratégica.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Querida Colombia, le escribo desde el conocimiento, muy cercano, del drama que viven familiares y amigos de las y los desaparecidos, de los silencios impuestos, de la rabia contenida o a veces expresada, de las profundas rupturas humanas, familiares, sociales, laborales, existenciales, de fe, de sentido… Del drama que, ordinariamente, viven en silencio, con miedo, sin atreverse a expresarlo, a tramitarlo, a buscar o a aceptar una ayuda cualificada. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ellas y ellos me han permitido entrar en sus **sagrarios** de dolor, miedo, angustia, rabia, indignación, culpas y rupturas… Palpar la fuerza profunda de su terca, improbable, incesante y a veces suicida búsqueda de sus seres queridos, de la verdad, de la justicia y de la dignidad. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### La entrada a los **sagrarios** de múltiples víctimas, en Colombia y en el mundo, me ha cambiado la manera de ver y vivir la vida, de ser ciudadano, la manera de comprender y vivir mi ministerio sacerdotal. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta carta está marcada por la contemplación y la vivencia de las experiencias profundas, conmovedoras y transformadoras, preparando con las familiares el encuentro con sus seres queridos: unos pocos huesos. Encuentro querido, soñado, añorado, buscado o temido…, y que despierta una madeja de sentimientos confusos, impredecibles, inimaginables e indescriptibles, que solo quienes lo han vivido pueden comprender. Los demás vemos o percibimos, pero no comprendemos en toda su dimensión.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### **Varias personas me han permitido ser testigo del encuentro privado con los restos de sus seres queridos. Un momento indescriptible, solo se puede contemplar y sentir. Un momento compuesto por infinidad de sentimientos; momento profundamente reparador, restaurador, momento que sintetiza múltiples emociones sagradas. Las miradas llenas de amor y dolor, las caricias más hermosas que he visto, los besos más esperados, las palabras más cortas, profundas y dicientes, los suspiros reparados… han llenado esos momentos. **

<!-- /wp:heading -->

<!-- wp:paragraph -->

Querida y contradictoria Colombia, tiene Ud. el deber y la obligación jurídica, ética, moral humana de “remover cielo y tierra” para que miles y miles de sus hijos e hijas se encuentren con sus seres queridos, para garantizarles la sanadora verdad y la reparadora justicia. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así recuperará su autoridad y su dignidad, será, de verdad, una matria-patria para todos sus hijos e hijas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con respeto,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, JyP francoalberto9@gmail.com

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### **Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.**

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
