Title: Corteros de Caña de Ingenio Risaralda inician huelga #YoReporto
Date: 2015-03-03 16:54
Author: CtgAdm
Category: Nacional, yoreporto
Tags: Corteros de caña del ingenio Risaralda, Huelga de trabajadores, Ingenios azucareros
Slug: corteros-de-cana-de-ingenio-risaralda-inician-huelga-yoreporto
Status: published

Según los directivos sindicales existe respaldo total a la justa huelga de corteros de caña del ingenio Risaralda iniciada el lunes 2 de marzo  contra la persecución sindical y violación a la convención colectiva al rebajar en un tercera parte el corte, es decir a escasos 20m de corte diario que no alcanza en el mes a ganar ni siquiera el pírrico salario mínimo.

Los trabajadores afiliados a Sintrainagro  declararon la justa huelga y se concentran a las afueras de la sede de esta compañía, ubicada en la vía que de La Virginia conduce a Balboa, departamento de Risaralda, denuncian que durante más de un año han sufrido los atropellos del Ingenio Risaralda que insiste en contratar a los corteros de caña mediante contratistas disfrazados de  Sociedades Anónimas Simplificadas SAS.

“Denunciamos públicamente que este tipo de contratación es una forma de tercerizar y con ello el Ingenio Risaralda evade su responsabilidad como empleador de una de las industrias más importantes del país” señalan en comunicado sindical.

Afirman los trabajadores que estar contratado por las S.A.S. significa no tener estabilidad laboral para una actividad que está comprobado es misional porque se corta caña todo el año y  su corte es indispensable para producir azúcar o etanol, argumentos válidos para ser contratados directamente por el Ingenio Risaralda.

Agregan que estar contratados por SAS significa que el derecho a la seguridad social se viola constantemente, cuando un trabajador se enferma o se accidenta el contratista no tiene donde reubicarlo porque su actividad exclusiva es el corte, en cambio el Ingenio el  cual es el principal beneficiario del corte de caña tiene las condiciones para cumplir con la responsabilidad de reubicarlo en cualquier labor distinta al corte.

El informativo sindical señala que en Ingenios como Cauca, Manuelita, Providencia, Castilla, Mayaguez y Pichichi mediante sus filiales cosechas, tienen contratado a término indefinido a los corteros de caña. Por qué estos Ingenios sí pudieron formalizar y el Ingenio Risaralda se niega! Finalizan con un llamado a ASOCAÑA, la cual es accionista de este ingenio a que respete el derecho al trabajo y a la libre asociación, para de esa forma poder luchar por  trabajo digno y estable y por la defensa de la producción nacional en contra de la importación de azúcar.

“Denunciamos públicamente que el Ingenio Risaralda ha fomentado en este año fuertemente la mecanización del corte, como una forma de persecución sindical, hacemos un llamado a los entes gubernamentales a que intervengan a favor del trabajador y sus familias, por el derecho al trabajo, de ser reemplazado los corteros de caña por máquinas y sin ninguna otra fuente de empleo digno, estaremos ante un escenario de desempleo y con ello de violencia, porque las necesidades del pueblo no cesan en este país donde la prosperidad es para algunos y no para todos”, indican los voceros del sindicato.

Con información de Hernán Durango para \#Yo Reporto
