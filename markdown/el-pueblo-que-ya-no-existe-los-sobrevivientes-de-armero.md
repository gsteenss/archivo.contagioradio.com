Title: El pueblo que ya no existe: Los sobrevivientes de armero
Date: 2015-11-13 15:39
Category: Nacional
Tags: 30 anos de Armero, Armero Tolima, Audios sobre Armero, Fosco d'Amelio, RAI Italia, Raquel Llopis, Tragedia de Armero
Slug: el-pueblo-que-ya-no-existe-los-sobrevivientes-de-armero
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/IMAGEN-12375481-2-e1447446822367.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto. El Tiempo 

##### [13 nov 2015]

Una de las tragedias humanas más dramáticas del siglo XX en Colombia, y en la historia nacional, se registro del 13 de noviembre de 1985, cuando el volcán Nevado del Ruíz, erupcionó provocando una avalancha de rocas y lodo que borro del mapa al municipio de Armero, ubicado en el departamento del Tolima.

30 años después, continúan los interrogantes sobre el manejo que se dio antes y después del fenómeno volcánico por parte de las autoridades competentes, la negligencia de quienes debían tomar decisiones, ante la que se ha planteado en múltiples oportunidades fue una "tragedia anunciada" donde perecieron entre 23 y 27 mil personas.

Los testimonios de expertos internacionales y nacionales que manejaron el siniestro, sumados a los de algunos sobrevivientes, nutren el documental sonoro "El pueblo que ya no existe: los sobrevivientes de Armero", una producción que recoge las vivencias  en la memoria de sus protagonistas desde 1985 hasta nuestros días cuando se completan 30 años con muchos interrogantes por resolver.

La pieza sonora producida por Raquel Llopis y Fosco d'Amelio, narra ademas la forma en que los cerca de 4000 sobrevivientes continuaron con sus vidas, cargando la tristeza de sus pérdidas y con el dolor de abandonar lo que un día fue su hogar, por que como dicen muchos de ellos: “la tragedia es lo que viene después”.

<iframe id="audio_9380738" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://www.ivoox.com/player_ej_9380738_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

"El pueblo que ya no existe: los sobrevivientes de Armero", fue emitido entre el 25 y el 29 de mayo de 2015 en la radio pública italiana (RAI-Radio3) dividido en cinco capítulos. Todas las entrevistas (más de 30) fueron realizadas a lo largo de 2014 y 2015 en Bogotá, el Tolima e Italia. Los realizadores lo han publicado bajo  licencia creative commons para ser compartida de manera libre.

El documental se complementa con el artículo “Pertenecer a un lugar que ya no existe”, publicado en enero de este año por la revista española Bostezo. Los invitamos a escuchar y compartir este fragmento de memoria nacional.

[**Sobre los realizadores**]

[Raquel Llopis, es traductora y ha colaborado anteriormente en la producción del documental *L'Impensabile – La prossima eruzione del Vesuvio* (emitido en 2013 por RAI-Radio3), así como en diferentes proyectos audiovisuales.]

Fosco d'Amelio, es geólogo y divulgador científico, además de trabajar como guionista para la radio y la televisión italianas (*Sostiene Bollani*, *L'Impensabile – La prossima eruzione del Vesuvio*, *Il Dottor Djembe*, *Un maledetto imbroglio*).

 
