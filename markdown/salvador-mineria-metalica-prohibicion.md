Title: El pueblo y la iglesia salvadoreña dijeron No a la minería metálica
Date: 2017-04-03 09:05
Category: Ambiente y Sociedad, Opinion
Tags: Mineria, prohibición minera, Salvador
Slug: salvador-mineria-metalica-prohibicion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/17498461_787458251428405_5499028651492014573_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Radio Victoria 

#### **Por: [ Asociación Ambiente y Socieda](https://archivo.contagioradio.com/margarita-florez/)d** 

###### 03 Abr 2017 

La prohibición de la minería metálica aprobada el miércoles por el parlamento salvadoreño incluye varios pasos anteriores que es conveniente comentar. El primero se refiere a la iniciativa que estuvo en el escritorio desde el 2009, y después de un largo proceso finalmente se aprobó. Luego en un hecho relevante el arzobispo de San Salvador en persona encabezó la manifestación ciudadana de la semana pasada para exigir que el congreso aprobara la ley lo cual se logró por una votación a favor de 70 congresistas frente a un total de 84 congresistas.

El poder de la iglesia se jugó a fondo para respaldar y promover la idea que el agua es vida. Este clamor ya lo había hecho público la Iglesia católica en un manifiesto La resistencia frente a la minería metálica en El Salvador publicado en febrero del 2017, en el cual se hace un recuento de la cantidad de asesinatos de ambientalistas y campesinos quienes desde 1990 se habían enfrentado a través de una Mesa contra la minería metálica a la misma corriente continental de incentivos sin medida para la minería 1 y la concesión de muchos títulos a grandes empresas.

El otro hecho es que el movimiento de campesinos, y ambientalistas en el departamento de Cabañas perseguido por su oposición a la mina, logró la no aprobación del estudio de impacto ambiental de El Dorado, mina que tenía título pero a la cual por razones medio ambientales y sociales no se concedió permiso de operación. Por esta razón Pacific Rim demandó al estado salvadoreño ante el CIADI, Centro Internacional de Arreglos de Diferencias Relativas a Inversiones organismos que hace parte del grupo del Banco Mundial. Después de un largo proceso este tribunal le concedió la razón a El Salvador y condenó a la minera con una indemnización de 8 millones de dólares por los gastos en que incurrieron.

El Salvador muestra como la alianza ciudadana entre campesinos, habitantes de localidades y movimiento ambiental puede ir avanzando en presentar alternativas a la minería como fuente priorizada de recursos, y de enfilar sus acciones para oponerse a las licencias ambientales, y a los títulos que se conceden sin siquiera avisar a los ciudadanos. Pero tal vez el punto diferente a otras luchas es que la Iglesia respaldó de frente a su feligresía.

La minería metálica por el momento no tiene permiso en El Salvador, y ojalá sirva para evaluar si es que se necesitan tanto este tipo de metales, que como el oro sigue almacenándose como respaldo económico, y símbolo de lujo y poder.
