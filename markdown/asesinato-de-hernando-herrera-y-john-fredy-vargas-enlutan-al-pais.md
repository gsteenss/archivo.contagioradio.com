Title: Asesinatos de Hernando Herrera y John Fredy Vargas, enlutan al país
Date: 2020-01-26 10:24
Author: CtgAdm
Category: DDHH, Nacional
Tags: Antioquia, lideres sociales, paz
Slug: asesinato-de-hernando-herrera-y-john-fredy-vargas-enlutan-al-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 25 de enero la Fundación Sumapaz denunció el asesinato de Hernando Herrera, líder de la vereda El Brasil zona rural del municipio de Sonsón, al sur de Antioquia, el hecho según la Fundación se presentó el 21 de enero. (Le puede interesar: <https://archivo.contagioradio.com/oposicion-putumayo-cuesta-vida/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la Fundación, Herrera se encontraba en la vereda Río Arriba, cuando un hombre identificado con el seudónimo de *"Tornillo"*, le propinó varios impactos de bala causando así la muerte al líder social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Minutos después la policía inició un operativo para su captura, la cual finalizó con un fuego cruzado en el que el agresor resultó herido y posteriormente falleció en el hospital al que es trasladado por la policía.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El victimario fue identificado como John Jairo Aguirre, actualmente autoridades regionales investigan si el asesinato del líder corresponde a acciones de algún grupo armado presente en la región.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Firmante de la paz asesinado en Pitalito, Huila

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otro lado en las últimas horas se conoció el asesinato de John Fredy Vargas Rojas de 42 años, en el municipio de Pitalito, Huila. (Le puede interesar: <https://www.justiciaypazcolombia.com/temor-a-retaliaciones-de-paramilitares-de-las-agc-obligados-a-salir-de-zona-humanitaria-nueva-vida/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según fuentes locales Vargas estaba saliendo en moto de una reunión con funcionarios de la Agencia para la Reincorporación y la Normalización (ARN) cuando fue agredido por hombres armados quienes le propinaron dos impactos de bala que le causaron la muerte de manera inmediata

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El firmante era oriundo de Puerto Asís, Putumayo, y hacia parte del frente Cacica Gaitana de las Farc, actualmente estaba en proceso de reincorporación a la vida civil junto a su familia. (Le puede interesar: <https://archivo.contagioradio.com/un-partido-no-se-desmorona-por-las-criticas-farc/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el primer mes del año según Indepaz, han sido asesinados 24 defensores de paz, 4 de estos firmantes del acuerdo, incluyendo los dos casos recientes. Los departamentos donde se han evidenciado estos hechos de violencia con mayor frecuencia son Cauca, Antioquia y Nariño.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
