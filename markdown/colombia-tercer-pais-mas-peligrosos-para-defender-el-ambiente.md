Title: Colombia es el segundo país más peligroso para defender el ambiente
Date: 2015-04-20 15:50
Author: CtgAdm
Category: Ambiente, DDHH, Otra Mirada
Tags: Aguilas Negras, Ambiente, Animalistas, Brasil, colombia, Empresas Públicas de Medellín, EPM, extractivismo, Global Witness, Hidroituango, honduras, indígenas, liberación de la madre tierra, Mineria, Movimiento Ríos Vivos
Slug: colombia-tercer-pais-mas-peligrosos-para-defender-el-ambiente
Status: published

##### Foto: [ariadnaensociales.blogspot.co]

<iframe src="http://www.ivoox.com/player_ek_4388002_2_1.html?data=lZilmpWUdo6ZmKiak5yJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9Dg0NLPy8aPqdSfxtGY1crLuc%2FY0JDdw4qnd4a1pdiYz4qnd4a1ktiYy9PXqcjp09SY0sbWpYzg0NiYw9LGcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Tatiana Roa, coordinadora de Censat Agua Viva] 

De acuerdo a un estudio de la organización británica Global Witness**, Latinoamérica es la región más peligrosa para defender el ambiente.** Según su estudio, en 2014 hubo **87 asesinatos contra** defensores de la fauna y la flora en América Latina.

La organización estimó un total de **116 crímenes contra ecologistas de todo el mundo** durante 2014, es decir, que con respecto al 2013, hubo **un aumento del 20%.** El informe señala que **Honduras** es la nación más peligrosa para los activistas, pues entre 2010 y 2014, se registraron **101 muertes.**

De las muertes de América Latina, **29 fueron de Brasil y 25 en Colombia**, mientras que **Honduras tuvo 12**, siendo el país menos poblado.

Billy Kyte, activista de Global Witness, afirmó que **“históricamente ha habido una distribución de tierra muy desigual de América Latina,** lo que ha causado conflictos entre las empresas locales y extranjeras y las comunidades”.

El estudio evidenció que **el 40%** de los defensores asesinados **fueron indígenas** que han muerto mientras defienden sus territorios y el agua.

**El caso colombiano**

Colombia registró uno de los mayores índices de asesinatos contra defensores del ambiente. De acuerdo a Tatiana Roa, coordinadora general de Censat agua Viva, en lo corrido del año, ya han sido públicas varias amenazas contra Líderes de organizaciones ambientalistas.

Por ejemplo, **grupos paramilitares han enviado panfletos** donde se pone en peligro la vida personas que apoyan públicamente acciones como la Liberación de la Madre Tierra, la Movilización de Mujeres Negras del Norte del Cauca por el Cuidado de la Vida y la Defensa de los Territorios Ancestrales, en oposición a los megaproyectos extractivos. [(Ver nota)](https://archivo.contagioradio.com/mineria-amenaza-en-cauca/)

Las comunidades negras e indígenas del norte del Cauca vienen luchando desde febrero por **la Liberación de la Madre Tierra, por lo que** la semana pasada se contabilizaron 6 asesinatos contra indígenas mientras adelantan  el proceso de liberación. [(Ver nota)](https://archivo.contagioradio.com/la-violencia-y-el-terror-regresan-a-las-comunidades-del-norte-del-cauca/)

Otro de los hechos amenazantes ocurridos en el Cauca, sucedió el pasado 10 de febrero, cuando líderes y lideresas socio – ambientales, que trabajan por la protección y defensa del agua, los bosques y las siembras, fueron amenazados por medio de panfletos que desconocidos dejaron en el barrio, Siloé del departamento del Cauca. De acuerdo a la Comisión de Jusiticia y Paz, el panfleto que no está firmado, apareció en medio de “la apuesta comunitaria por una Consulta Popular frente a la Minería que afectaría los intereses de las empresas **CONTINENTAL GOLD LTDA, NEGOCIOS MINEROS, PALMA SOM con traspaso a CGL DOMINICAL SAS”.** [(Ver nota)](https://archivo.contagioradio.com/mineria-amenaza-en-cauca/)

Por otro lado, integrantes del Movimiento Ríos Vivos, han denunciado sobre amenazas y hostigamientos por parte de la empresa de seguridad privada de la obra de **Empresas Públicas de Medellín, EPM, que realiza el** proyecto hidroeléctrico Hidroituango en Antioquia. [(Ver nota)](https://archivo.contagioradio.com/van-mas-de-400-familias-desalojadas-forzosamente-por-hidroituango/)

Hasta los animalistas se han visto afectados y han sido blanco de amenazas por parte de **“Águilas Negras”,** acusándoles de “guerrilleros y defensores del terrorismo”, al hacer parte de diversas protestas sociales y así mismo, afirman que son promotores del comunismo. [(Ver nota)](https://archivo.contagioradio.com/animalistas-ahora-son-blanco-de-amenazas-por-parte-de-aguilas-negras/)

Cabe recordar que en el informe del año pasado de la misma firma, ocupò el quinto lugar de los países con más asesinatos de ambientalistas. entre 2002 y 2013 en el país asesinaron a 52 defensores del medio ambiente.
