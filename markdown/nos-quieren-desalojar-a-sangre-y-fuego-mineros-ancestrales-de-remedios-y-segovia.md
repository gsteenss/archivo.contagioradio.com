Title: "Nos quieren desalojar a sangre y fuego" Mineros ancestrales de Remedios y Segovia
Date: 2019-01-09 17:12
Author: AdminContagio
Category: DDHH, Movilización
Tags: Antioquia, Gran Colombia Gold, mineros, Remedios Segovia
Slug: nos-quieren-desalojar-a-sangre-y-fuego-mineros-ancestrales-de-remedios-y-segovia
Status: published

###### [Foto: Colombia Informa] 

###### [09 Ene 2019] 

Las comunidades de Marmato, en Caldas y Remedios y Segovia, en Antioquia, rechazaron el comunicado de la multinacional Gran Colombia Gold, que pide al Estado colombiano una intervención militar o de cualquier otra fuerza, **para desalojar a los mineros que realizan explotación minera, según la empresa sin los permisos necesarios**, en el territorio.

Jaime Gallego, vicepresidente de la Mesa de Mineros de Remedios y Segovia afirmó que "con esa carta se está declarando la guerra y s**e quiere desalojar a los mineros a sangre y fuego, porque se habla de una intervención militar y policial**", hecho que ha generado temor en la comunidad. (Le puede interesar: ["Luego del paro en Remedios y Segovia han sido detenidos 20 mineros"](https://archivo.contagioradio.com/luego-del-paro-de-segovia-y-remedios-han-sido-detenidos-40-mineros/))

### **"Somos mineros ancestrales"** 

Gallego desmintió el comunicado de la multinacional en la que se afirma que se está realizando minería ilegal y aseveró que la minería que se hace en el territorio es ancestral y tradicional, y que se trabajaba previo a que la Gran Colombia Gold llegara al lugar.

Asimismo, señaló que si bien es cierto que los predios son de la multinacional, existe una disputa legal, debido a que hay una posesión de más de 40 años sobre ese territorio, en el que los mineros han venido realizado la explotación del suelo, razón por la cual serían los dueños de los predios.

"Cuando ellos compraron, ya existía la minería ancestral y tradicional, acá no se está perjudicando ningún título minero, se está trabajando sobre ellos, pero los mineros están buscando una formalización de su trabajo" afirmó Gallego. (Le puede interesar: ["Estos fueron los acuerdos que levantaron la protesta de los mineros en Remedios y Segovia"](https://archivo.contagioradio.com/mineros-de-remedios-y-segovia-logran-formalizacion-de-sus-actividades/))

Además, durante el paro minero del 2018 que mantuvieron los mineros de este municipio, el gobierno nacional se había comprometido a instalar una oficina de formalización para q**ue se caracterice y reconozca** **a los mineros ancestrales.** Igualmente, se acordó reformar el código minero para que se les tenga en cuenta dentro de los procesos que de allí se derivan.

Gallego expresó que en las próximas horas se realizará una asamblea general en la comunidad en donde se tomarán acciones de cara al comunicado de la multinacional, debido a que la exigencia que realizan afectaría a más de 1.500 personas y acabaría con la principal actividad económica de este municipio.

<iframe id="audio_31374657" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31374657_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
