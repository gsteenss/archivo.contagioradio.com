Title: ELN confirma participación en reunión para destrabar el proceso de paz
Date: 2017-01-08 10:50
Category: Nacional, Política
Tags: conversaciones de paz con el ELN, ELN, Mesa de conversación de Quito
Slug: eln-confirma-participacion-en-reunion-para-destrabar-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [8 Enero 2017] 

A través de la cuenta de Twitter del ELN, ELN\_Ranmpal, la guerrilla **confirmó que asistirá a la reunión del próximo 10 de enero**, que servirá como escenario de encuentro con el equipo negociador del gobierno para avanzar en las conversaciones frente a como destrabar el proceso y continuar con la instalación de la mesa en Quito.

En la cuenta la guerrilla del ELN afirmó que “**cumplirá como siempre todo lo acordado y dejará ante el mundo a Santos sin argumentos ni excusas**”. Frase que podría  hacer alusión a la liberación de Sánchez de Oca,  que de acuerdo con esta guerrilla se ha convertido en una condición no pactada para dilatar las conversaciones.

Por otro lado, Monseñor Monsalve indicó que podría ser importante que las conversaciones se realicen en Colombia, debido a la contienda electoral que afrontará Ecuador este año, e hizo un llamado a ambas partes para que se continúe con el proceso e incluso si es necesario se cambie la metodología de los diálogos, a su vez señaló que **“el ELN debe liberar a todas las personas que tienen en su poder”.**

Se espera que el 10 de enero a Ecuador arriben por parte del equipo de diálogos del ELN Pablo Beltrán, Antonio García, Aureliano Carbonell, Bernardo Téllez, Vivian Henao, Consuelo Tapias, Isable Torres, Silvana Guerrero, María Helena Guitrago, Miriam Barón, Gustavo Martínez, Tomas García y Carlos Reyes. Le puede interesar: ["Sin mesa con el ELN no habrá paz completa: Eduardo Celis"](https://archivo.contagioradio.com/sin-mesa-con-el-eln-no-habra-paz-completa-eduardo-celis/)

Mientras que por parte del gobierno, el equipo en cabeza de Juan Camilo Restrepo estará acompañado por el mayor general (r) del Ejército Nacional, Eduardo Herrera Berbel; el economista José Noé Ríos Muñoz y Jaime Avendaño Lamo, funcionario de la Presidencia de la República.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
