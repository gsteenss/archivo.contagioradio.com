Title: San Martín se opone al Fracking a pesar de presencia del ESMAD
Date: 2016-10-18 15:24
Category: Ambiente, Nacional
Tags: Abuso de fuerza ESMAD, defensa del medio ambiente, Fracking colombia
Slug: san-martin-se-opone-al-fraking-a-pesar-de-presencia-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/san_martin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia Ambiental] 

###### [18 Oct 2016] 

Los ciudadanos y ciudadanas de San Martín en el departamento del Cesar, acompañados por la Corporación Defensora del Agua, el Territorio y los Ecosistemas CORDATEC, l**levan a cabo una movilización pacífica en oposición a las operaciones de Fracking a manos de la empresa Conoco Phillips.** En respuesta a sus demandas, el Alcalde municipal Saúl Celis, autorizó la intervención del ESMAD.

Los manifestantes, se encuentran a los costados de la vía que comunica a San Martín con el corregimiento de Cuatro Bocas. Carlos Andrés Santiago, integrante de CORDATEC, señaló que desde la noche del 17 de Octubre efectivos del ESMAD se encuentran en zona urbana de San Martín. **“El Alcalde autorizó la intervención para permitirle a la empresa iniciar exploraciones en el pozo Pico Plata1”** reveló Santiago. Le puede interesar: [ESMAD reprime violentamente movilización en Putumayo](https://archivo.contagioradio.com/esmad-reprime-violentamente-movilizacion-en-putumayo/)

Las comunidades de San Martín vienen protestando pacíficamente y denunciando esta situación, desde Julio, Santiago señala que “se ha intentado dialogar con el Ministerio del Interior, con el Ministerio de Ambiente, con Ministerio de Minas, y ningún ministerio ha entablado comunicación con nosotros”.

Además manifestó, que preocupa a las comunidades la ausencia de apoyo por parte del gobierno departamental, sumado a la negligencia del gobierno municipal. Denunció que tienen “conocimiento que **la empresa Conoco Phillips, solicitó la judicialización de los integrantes de la corporación CORDATEC y el envío del ESMAD”.**

Santiago, señala que “la paz territorial debe vivirse en las comunidades, por quienes nos manifestamos de manera pacifica (…) se deben buscar escenarios de dialogo y no de represión” pero la realidad, es que **“ha llegado una cantidad alarmante de ESMAD, quienes hacen un uso desproporcionado de la fuerza, sin justificación”.** Hasta el momento les ha acompañado la Defensoría del Pueblo y algunas organizaciones defensoras de Derechos Humanos, y extiende la invitación a más organizaciones y medios de comunicación.

Este integrante de la corporación, concluye señalando que mientras se habla de construcción de Paz, **“están siendo vulnerados los derechos fundamentales de defensores de los territorios en todo el país”** a pesar de la sentencia emitida días pasados por la Corte Constitucional. Le puede interesar: [Municipios decidirán su propio modelo de desarrollo.](https://archivo.contagioradio.com/municipios-decidiran-su-propio-modelo-de-desarrollo/)

<iframe id="audio_13374023" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13374023_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
