Title: Universidad de la Sabana se retracta sobre señalamientos a la comunidad LGBTI
Date: 2015-02-16 19:58
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Adopción igualitaria, LGBTI, Universidad de La Sabana
Slug: universidad-de-la-sabana-se-retracta-sobre-senalamientos-a-la-comunidad-lgbti
Status: published

##### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4081292_2_1.html?data=lZWlk5eddo6ZmKiakpeJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkNDnjM3cz9TXqdnpwtHS1ZDXs8%2BfxtPTx9fRs9SZk6iYt5OPl8LWwtPOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Mario Hernández, Federación Médica Colombiana] 

En medio del debate de la adopción igualitaria, este 12 de febrero, la Universidad de la Sabana, emitió un documento a la Corte Constitucional, donde se afirma que **“las personas homosexuales son enfermas**”.

Se trata de un documento de 4 páginas, donde el doctor Pablo Arango, quien es profesor asociado del Departamento de Bioética de la Facultad de Medicina de esta universidad de la Sabana, afirma que las personas con otra orientación sexual, **“se apartan del común, lo que constituye de alguna manera una enfermedad**”. Esto, entre otras de las afirmaciones hechas por la universidad, que han generado todo tipo de polémica.

La Universidad de la Sabana, **“inspirada en la visión cristiana del hombre y del mundo**”, como ellos mismos lo indican en la visión de la institución, señaló en el texto enviado a la Corte Constitucional, que los menores con padres homosexuales, sufren de “trastornos de la identidad sexual, rechazo del compañero o compañera del progenitor homosexual como figura paterna o materna y preferencia a vivir con el otro progenitor” y además que, **“el adoptado está en mayor riesgo de sufrir abusos sexuales”.**

Las principales objeciones ante este documento, se encuentran argumentadas en diversos estudios que reconfirman que la homosexualidad no es una enfermedad, empezando por **la investigación científica de 1973 cuando la Asociación Americana de Sicología dijo que la homosexualidad no es una enfermedad,** así mismo, la OMS, Organización Mundial de la Salud decidió descartar esa condición sexual de su lista de enfermedades.

Respecto a ese tema, el doctor Mario Hernández de la Federación Médica Colombiana, afirmó que "**la homosexualidad no es una enfermedad, es una decisión y una construcción social"** y que ese señalamiento es normal debido a la orientación cristiana de la universidad.

Por otro lado, el mismo **ICBF** determinó, que los niños y niñas no tienen ningún efecto psicológico cuando se desarrollan en un hogar cuyos padres sean del mismo sexo.

Debido a la serie de críticas que recibió la Sabana, la institución decidió retractarse sobre la afirmación de las personas homosexuales son enfermas, aceptando que la homosexualidad fue suprimida de la clasificación de enfermedades por la American Psychiatric Association.

Además el rector de la universidad, Obdulio Velásquez pidió disculpas, y  aseguró que la universidad no avala al profesor sobre ese concepto emitido en el documento y por ende, pedirá a la Corte Constitucional que no tenga en cuenta el artículo para tomar una decisión sobre la posibilidad de que parejas del mismo sexo puedan adoptar.
