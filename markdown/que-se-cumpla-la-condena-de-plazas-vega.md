Title: "Que se cumpla la condena de Plazas Vega"
Date: 2015-06-20 21:05
Category: Judicial, Nacional
Tags: arias cabra, cafeteria, corte suprema, desaparecidos, familia, jaime granados, luís guillermo salazar, Palacio de Justicia, pilar navarrete, plazas vega
Slug: que-se-cumpla-la-condena-de-plazas-vega
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/e-e1472582019512.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4667556_2_1.html?data=lZujmZqZeo6ZmKiakpaJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmstrSjdjJb8TpztXZw5DQpYzX0NPRx9PFb8XZjLXZw9%2FFt4zKxszOh5eWb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Pilar Navarrete, Familiar de desaparecido del Palacio de Justicia] 

######  

###### [21 jun 2015] 

Familiares de los desaparecidos del Palacio de Justicia realizaron un plantón frente a las Corte Suprema este viernes 19 de junio, rechazando la solicitud de libertad para el Coronel (r) Alfonso Plazas Vega, que actualmente estudia la Sala Penal del alto tribunal.

El Coronel (r) fue sentenciado en el año 2010 con ratificación en el 2012 por la desaparición de 11 personas los días 5 y 6 de noviembre del 1985 durante la retoma del Palacio de Justicia. El Tribunal Superior de Bogotá, además, impuso al gobierno y al alto mando de la estructura militar la realización de un acto público de perdón para la comunidad y las víctimas. Sin embargo, en días pasados, el abogado defensor del Coronel (r), Jaime Granados, radicó un recurso de casación ante la sala Penal de la Corte para que se evalúe nuevamente el caso. Actualmente, la Corte Suprema estudia la ponencia presentada por el magistrado Luis Guillermo Salazar, en la que se plantea la necesidad de revocar la condena al considerar que no existe pruebas que relacionen a Plazas Vega con las desapariciones.

Para los familiares de los desaparecidos esto representa una ofensa, en tanto pretende ignorarse la capacidad de mando que tenía el Coronel durante las operaciones de retoma del Palacio de Justicia, pasando por encima de las condenas proferidas en Colombia y en la Corte Interamericana de Derechos Humanos, mientras insisten en la necesidad de que los responsables revelen qué hicieron con las personas desaparecidas, y dónde se encuentran sus restos.

Alfonso Plazas Vega y Jesus Armando Arias Cabrales cumplen condenas de 30 y 35 años de prisión, respectivamente, en el Cantón Norte en Bogotá, la misma instalación militar a la que habrían sido trasladados los 11 civiles que salieron con vida del Palacio de Justicia. Es por este motivo que Pilar Navarrete, esposa de Hector Jaime Beltrán, administrador de la cafetería del Palacio, asegura que "Plazas Vega y Arias Cabrales están detenidos en condiciones que son muy favorables para ellos, y no han contado nada. Realmente lo que nosotros queremos saber es dónde están nuestros familiares. Es lo mínimo que nos merecemos después de 30 años".

A la manifestación frente a las instalaciones de la Corte Suprema se sumaron familiares de más desaparecidos y desaparecidas en Colombia, para acompañar la solicitud y evidenciar la negligencia que existe por parte de la Fiscalía, y el obstrucción por parte del Ministerio de Defensa y de la Procuraduría al momento de investigar las desapariciones forzadas en Colombia.

Los familiares de los desaparecidos preparan la conmemoración de los 30 años de la toma y la retoma del Palacio de Justicia, el próximo mes de noviembre del año 2015, esperando que el Estado colombiano cumpla con las condenas proferidas en su contra.

###### \ 

###### [Fotos: Carmela María] 

   
 
