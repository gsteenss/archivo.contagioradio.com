Title: How Colombia's right is using religion to win votes
Date: 2019-09-12 10:45
Author: CtgAdm
Category: English
Tags: local elections, Miguel Uribe Turbay, Religión
Slug: how-colombias-right-is-using-religion-to-win-votes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-12-at-10.30.44-AM.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Publimetro Colombia] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

Christian churches were highly influential in helping Brazil's Jair Bolsonaro win the presidency. Such was also the case in Colombia last year when they catapulted Iván Duque to the *Casa de Nariño*. Once more, during upcoming local elections on Oct. 27, these groups are taking part in mayoral and gubernatorial races across the Andean country, supporting candidates such as the right-wing, *uribista* candidate running for Bogotá mayor, Miguel Uribe Turbay.

**Are all Christian churches supporting the right?**

The Presbyterian pastor Milton Mejía explained that the religious leaders that tend to support the candidates with more conservative platforms are charismatic, usually part of the pentecostal movement and belong to the Evangelical denomination. These leaders have won some recognition and seek to influence electoral politics for some specific end.

The formula, according to the pastor, comes from other Latin American countries: First, they attack the “gender ideology,” personal drug use policies and the LGBTQIA community. With these arguments, religious groups are joining political sectors and gaining ground in the electoral plane. According to Pastor Mejía, this should worry all citizens because these groups “oppose the development of human rights in all its expressions.”

### **The tyranny of faith**

Pastor Mejía questioned this relationship when he said that “it isn’t healthy for faith to mix with electoral politics.” While pastors may have a responsibility to guide people to vote in a conscientious and informed way, this responsibility does not mean imposing a candidate by force of faith, which would be “a distortion of the Christian faith.”

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
