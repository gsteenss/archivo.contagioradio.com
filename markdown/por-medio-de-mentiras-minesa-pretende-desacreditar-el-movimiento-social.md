Title: "Por medio de mentiras Minesa pretende desacreditar el movimiento social"
Date: 2020-02-20 17:00
Author: CtgAdm
Category: Ambiente, Nacional
Tags: ejercito, Mineria, Minesa, santurbán
Slug: por-medio-de-mentiras-minesa-pretende-desacreditar-el-movimiento-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/EROvpb4VAAAzDXJ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/mayerly-lopez-sobre-agresion-fisica-durante-manifestacion-en_md_48221927_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Mayerly López | vocera del Comité de Saturbán

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Este 20 de febrero la comunidad santandereana hizo presencia en el Club Campestre de Bucaramanga, lugar donde la empresa minera Minesa habría convocado a 500 empresarios nacionales e internacionales para construir alianzas en torno a la ejecución del proyecto Soto Norte el cual interfiere el en el Páramo de Santurbán. (Le puede interesar: <https://archivo.contagioradio.com/paramo-de-santurban/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al mismo tiempo las autoridades de la ciudad enviaron a integrantes del Ejército y la Policía a proteger el Club donde se desarrollaba la reunión, acción que según Mayerly López vocera del Comité de Saturbán ***"reprime el derecho a la libertad y a la la protesta social** todo con el fin de conseguir el apoyo para la obtención de una licencia ambiental que daña y afecta el agua de casi toda Santander"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma se registro la agresión por parte del encargado de seguridad de Minesa hacia una de las mujeres que integraba esta manifestación, *"ella no estaba haciendo nada, más que exigir la protección de páramo, no hay excusa para lo que este señor hizo",* afirmó López y añadió que ante esto, *"la Policía intervino y le dijo al hombre que se retirara inmediatamente".*

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComiteSanturban/status/1230586925595099139","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComiteSanturban/status/1230586925595099139

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Campaña de desprestigio de Minesa

<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComiteSanturban/status/1229742231214489600","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComiteSanturban/status/1229742231214489600

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Minesa_Col/status/1230539248547385350","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Minesa\_Col/status/1230539248547385350

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Een 2019 se filtró un vídeo del presidente de Minesa Santiago Ángel Urdinola, donde le presentaba a diferentes empleado la estrategia comunicativa que usarían en los meses siguientes, acción que según López *"está encaminada a engañar a todos, a hacer ruido en los medios y desviar la atención de las acciones de la empresa, uniéndola a empresarios, políticos y grupos sociales "*. (Le puede interesar: <https://www.justiciaypazcolombia.com/la-lucha-embera-contra-la-mineria-en-el-choco/> )

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el vídeo el presidente de la multinacional afirma que se necesitan 5 puntos para poder avanzar en la aprobación de la licencia, uno de ellos es hacer pensar que las acciones contra el proyecto en Santurbán son acto oportunistas de políticos, *"si los tomadores de decisiones se sienten tranquilos a pesar de que yo allá tenga una marcha ... tiene que pesar que es parte el ejercicio... o que es Petro intentando tumbar el Gobierno de Duque"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "*Urdinola, da ordenas a sus empleados para engañar a la ciudadanía, acompañado de mensajes que mienten diciendo que el título no está en el Páramo, cuando este afecta el Bosque Alto Andino, el cual integra a Santurbán y todo el recurso hídrico que brota de ahí"*.
>
> <cite>Mayerly López - Comite Santurbán</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

A su vez Urdinola afirma que han hecho campañas de análisis a diferentes políticos con el fin de que sus *stakeholders*, osea, sus interesados, se sientan tranquilos y piensen que lo que ocurre con las movilizaciones y protestas se relaciona con acciones políticas entre partidos. (Le puede interesar: [Video completo](https://archivo.contagioradio.com/filtran-video-que-evidencia-campana-de-estigmatizacion-de-minesa-contra-ambientalistas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente la vocera del Comité por la defensa de Santurbán, agregó que *"la falta de reconocimiento por parte del Presidente Duque a las regiones y organizaciones que hemos anunciado ampliamente los impactos que generaría la aprobación de estos proyectos*, *es más que un argumento para movilizarnos este 16 de marzo*". (Le puede interesar: <https://archivo.contagioradio.com/permitir-mineria-en-santurban-es-arriesgar-el-85-del-agua-de-los-colombianos/>)  

<!-- /wp:paragraph -->
