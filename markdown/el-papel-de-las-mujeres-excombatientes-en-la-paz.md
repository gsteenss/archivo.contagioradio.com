Title: Mujeres excombatientes en la implementación de los Acuerdos de Paz
Date: 2017-04-26 10:30
Category: Libertades Sonoras, Mujer
Tags: FARC, genero, Libertades Sonoras, mujeres
Slug: el-papel-de-las-mujeres-excombatientes-en-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/ultima-marcha-de-la-guerrillas-de-las-farc-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [25 Abr. 2017] 

En [[[\#]{._58cl ._5afz}[LibertadesSonoras]{._58cm}]{._5afx}](https://www.facebook.com/hashtag/libertadessonoras?source=feed_text&story_id=10154596141961482){._58cn} hablamos sobre la situación de las mujeres excombatientes, el papel y los liderazgos que han ejercido dentro y fuera de la guerra, así como sus aportes en el proceso de la implementación de los Acuerdos de Paz firmado entre las FARC y el Gobierno Nacional.

En entrevista para Libertades Sonoras, **Victoria Sandino de las FARC-EP**, compartió las experiencias, las apuestas, proyectos y los retos que en la actualidad enfrentan las mujeres de esa guerrilla y **Clara Guerrero de la Red Nacional de Mujeres Excombatientes de la Insurgencia** habló de las experiencias que han ganado las mujeres, luego de haber participado en procesos de paz y de estar por más de 28 años en "la vida civil".

<iframe id="audio_18341505" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18341505_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
