Title: Programa educativo busca atender a 2.500 niños en Tumaco
Date: 2017-12-05 13:43
Category: Nacional
Tags: educación en tumaco, Fundación génesis, primera infancia, Tumaco
Slug: programa-educativo-busca-atender-a-2-500-ninos-en-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/mis-primeros-pasos.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Genesis Fundation] 

###### [05 Dic 2017] 

La Fundación Génesis, junto con la Universidad de los Andes, pondrán en marcha un proyecto educativo para la primera infancia en Tumaco, Nariño. **“Semillas de Apego”** brindará atención psicosocial de manera grupal para los niños y niñas que han sufrido la violencia en ese municipio. Esta fundación ha trabajado por 16 años en diferentes zonas de conflicto con niños de 0 a 12 años.

La Fundación ha trabajado con **100.800 estudiantes** que han participado en los diferentes programas en todo el país. Además, 2.797 docentes se han formado en diferentes metodologías para atender las principales necesidades de los niños y niñas. De igual forma, 26 departamentos han participado de los programas educativos por lo que mil instituciones educativas han sido beneficiadas.

### **En Tumaco buscan atender a 2.500 niños y niñas** 

De acuerdo con Cristina Gutiérrez de Piñeres, directora de la Fundación Génesis, el trabajo en Tumaco cumple 7 años y el programa educativo **beneficia a 2.500 niños y niñas** de 0 a 5 años. Manifestó que “el haber entrado a Tumaco, nos ha permitido conocer las necesidades muy particulares de los niños, las familias y las comunidades”. (Le puede interesar: ["La guerra de Colombia se retrata en la tragedia de Tumaco"](https://archivo.contagioradio.com/la-guerra-de-colombia-se-retrata-en-la-tragedia-de-tumaco/))

Allí, han evidenciado que la mayoría de las personas que habitan en Tumaco, **“han sido víctimas de alguna forma del conflicto armado”**. Por esto, con la iniciativa educativa buscan “sanar las experiencias dolorosas que han tenido muchas familias e incluso las experiencias de nuestros educadores que trabajan diariamente en el programa”. Informó que este programa se extiende a los padres de familia debido a que “se ha demostrado que el trabajo con quienes rodean a los niños ayuda a potencializar el desarrollo de ese niño o niña”.

Gutiérrez enfatizó en que, en Tumaco han hecho una intervención en la zona urbana y rural de manera integral. Sin embargo, **“hay un reto muy grande para llegar a muchas de estas zonas rurales”.** En dos años, la fundación ha pensado atender a cerca de 640 familias teniendo como prioridad los niños y las niñas de estas familias.

### **Hay falta de coordinación entre las instituciones del Gobierno** 

Para garantizar una atención integral dirigida hacia la primera infancia, la Fundación Génesis reconoce que hace falta una coordinación intersectorial entre las instituciones del Gobierno y las diferentes organizaciones que trabajan en territorios como Tumaco. Afirma que “la mayoría de las instituciones educativas de Tumaco **tienen índices de calidad muy bajo,** mostrando así dificultades muy grandes en lo que tiene que ver con las secretarias de educación”. (Le puede interesar: ["Especial: Tumaco llora una masacre en "tiempos de paz"](https://archivo.contagioradio.com/tumaco-llora-una-masacre-en-tiempos-de-paz/))

Finalmente, este programa fortalecerá **“las aptitudes de aprendizaje, la autonomía y la participación** desde la primera etapa de la niñez, bajo un proceso de atención integral que incluye nutrición y el fortalecimiento de talento humano en el sistema educativo y del vínculo entre padres e hijos”.

<iframe id="audio_22479724" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22479724_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
