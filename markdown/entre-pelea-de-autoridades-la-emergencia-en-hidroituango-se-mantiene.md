Title: Entre pelea de autoridades la emergencia en Hidroituango se mantiene
Date: 2018-06-06 15:02
Category: Ambiente, Nacional
Tags: Banco Interamericano de Desarrollo, emergencia en hidroituango, EPM, gobernador de antioquia, Hidroituango
Slug: entre-pelea-de-autoridades-la-emergencia-en-hidroituango-se-mantiene
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/De4luDzXkAA2qVR-e1528309998898.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Twitter Claudia Julieta Duque] 

###### [06 Jun 2018] 

Tras el anuncio de Empresas Públicas de Medellín, quien expresó que la emergencia en Hidroituango **aún no ha sido superada,** continuó la pelea entre las autoridades que ha generado confusión entre la población sobre lo que pueda suceder en el Bajo Cauca Antioqueño. En una carta dirigida a EPM, el gobernador Luis Pérez, dio a conocer el informe que elaboró un grupo de ingenieros estadounidenses.

Los norteamericanos indicaron que puede ocurrir un derrumbe de la montaña "de 10 millones de metros cúbicos" lo que podría generar una ola de **más de 40 metros** que se llevaría consigo a las poblaciones de Valdivia, Cáceres y Tarazá. La gobernación indicó que esta información no fue presentada por EPM a lo que el alcalde de Medellín respondió que la información era confidencial.

### **Rifi y rafe entre autoridades** 

En una carta enviada a la empresa, Pérez manifestó que “jamás los contratistas o los constructores **habían mencionado un problema en la montaña,** o sea que estaban construyendo como gallinas ciegas”. (Le puede interesar:["FLIP alerta sobre información que estaría ocultando EPM y el SIAT en Hidroituango"](https://archivo.contagioradio.com/flip-alerta-sobre-informacion-que-estaria-ocultando-epm-y-el-siata-en-hidroituango/))

Esto teniendo en cuenta que las emergencias que se han presentado se deben a los deslizamientos producto de los **derrumbes las montañas** en donde fue construido el proyecto. Cabe resaltar que esta advertencia la había hecho la comunidad en diferentes oportunidades incluso antes de que empezara la construcción.

Por su parte, el alcalde de Medellín, Federico Gutiérrez, expresó su apoyo a EPM argumentando que l**a empresa ha realizado las acciones necesarias** y suficientes, no sólo para proteger el proyecto, sino también para evitar una posible avalancha.

Sin embargo, el procurador Fernando Carrillo, expuso sus preocupaciones frente a la información entregada por EPM y le pidió **que responda las dudas s**obre lo que está ocurriendo con los túneles, el vertedero, la casa de máquinas, la presa y los planes de contingencia.

### **Ríos Vivos presentó una queja ante el Banco Interamericano de Desarrollo** 

Debido a las constantes denuncias que ha hecho el Movimiento Ríos Vivos y que no han sido escuchadas por la empresa y las autoridades, sus integrantes presentaron una queja ante el BID quien **aportó recursos** para el desarrollo del proyecto. En ella, pidieron que se indague si el Banco cumplió o no sus estándares sociales y ambientales a la hora de invertir en el proyecto. (Le puede interesar:["EPM sigue brindando información confusa: Ríos Vivos"](https://archivo.contagioradio.com/epm-sigue-brindando-informacion-confusa-raiz-los-derrumbes-hidroituango-rios-vivos/))

La queja que fue presentada en Washington, Estados Unidos y en ella manifestaron que los proyectos financiados por esa entidad deben ser **“sostenibles, participativos y respetuosos de la legislación nacional**, lo que no ha ocurrido en Hidroituango”. Afirmaron que “el proyecto no contó con una evaluación de impacto ambiental adecuada, no permite la participación de las comunidades ni el acceso a la información”.

Adicionalmente, recordaron que se han presentado múltiples **violaciones a los derechos humanos** y uso desproporcionado de la fuerza”. Ríos Vivos enfatizó en que se ha puesto en riesgo la vida de miles de personas que han sido evacuadas “de forma improvisada por la crisis de la presa” yendo en contravía de los estándares sociales y ambientales que debe incluir el BID en sus inversiones.

Finalmente, las comunidades afectadas han denunciado que no se les ha permitido salir de las zonas hacia donde fueron evacuadas y enfatizaron en que los riesgos que se han presentado por los deslizamientos de la montaña **fueron advertidos** por ellos por lo que han pedido que se deje que el río Cauca recupere su caudal.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
