Title: El fútbol no es el opio del pueblo
Date: 2016-06-09 12:08
Category: Javier Ruiz, Opinion
Tags: Carlos Caszely, Copa América Centenario, Democracia Corinthiana, fútbol y democracia
Slug: el-futbol-no-es-el-opio-del-pueblo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/DC1-e1465492086644.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

#####  

##### Por: **[Javier Ruiz](https://archivo.contagioradio.com/javier-ruiz/) - [@contragodarria](https://twitter.com/ContraGodarria)** 

##### [09 Jun 2016] 

Vivimos tiempos agitados y es entendible que no debemos ser apáticos e indiferentes con lo que sucede en el país. A su vez, se están jugando el centenario de la Copa América, las finales del fútbol local e inicia la Eurocopa en Francia.

Entonces vuelve a surgir el debate eterno si el fútbol es un “opio del pueblo” en donde este se usa para distraer a las personas de los problemas del país. Eduardo Galeano dijo una vez al respecto “¿En que se parece el fútbol a Dios? En la devoción que le tienen muchos creyentes y en la desconfianza que le tienen los intelectuales” y por esa desconfianza algunos sostienen sin rubor que el fútbol es “pan y circo” creyendo que tienen un tufillo de superioridad moral viendo a los seguidores del fútbol cómo personas poco inteligentes o “castradas mentalmente” por apoyar a un equipo de fútbol.

Lamento decirles a aquellos que ven al fútbol cómo “pan y circo” que no lo es y el fútbol nos ha dado lecciones de democracia y libertad a lo largo de su historia:

Cómo olvidar “[La Democracia Corinthiana](https://www.youtube.com/watch?v=CmqO1Yq7PQI)” liderada por “Sócrates” cómo contrapeso a la dictadura militar que mandaba en Brasil a principios de los años 80 donde nos enseñaron que tu voto es igual a mi voto, que los débiles le pueden ganar a los fuertes y que se puede ganar o perder pero siempre en democracia.

Cómo olvidar al [Start FC](http://www.marca.com/2011/08/09/futbol/futbol_internacional/1312870663.html), aquel equipo conformado por exjugadores del Dinamo de Kiev que le ganaron partidos a equipos conformados por soldados nazis en la Ucrania ocupada en plena Segunda Guerra Mundial y por esa osadía varios jugadores del equipo ucraniano murieron en campos de concentración.

Cómo olvidar al jugador chileno Carlos Caszely que se atrevió a no darle la mano al dictador Pinochet cuando este despedía a la selección chilena que iba rumbo a Alemania a disputar el mundial del 74 ([cuya clasificación a ese mundial fue infame](https://www.youtube.com/watch?v=Fb5KpkSajpw)) y vale recordar que el dictador chileno utilizo el Estadio Nacional de Santiago cómo campo de concentración donde seria torturado y desaparecido el cantautor Víctor Jara. Por ese atrevimiento la madre de Caszely fue secuestrada y torturada por las fuerzas represoras y su desgarrador testimonio sirvió para que los chilenos en 1988 con alegría y sin rencores le dijeran [“NO” a Pinochet](https://www.youtube.com/watch?v=Uo1GMmp_uKo). En ese mundial del 74 Caszely se vería las caras con el maoista Paul Breitner quien le mando una dedicatoria a Pinochet a [manera de golazo](https://www.youtube.com/watch?v=lMWf1BGFpRA). Además, se negó a jugar el mundial del 78 en Argentina por la dictadura de Videla y ayudo económicamente a unos obreros españoles en huelga cuando jugaba en el Real Madrid.

Cómo olvidar a [Oleguer Presas](http://www.futbolypasionespoliticas.com/2012/03/oleguer-vs-salva-la-convivencia.html) y su compromiso por la independencia de Cataluña y por hacerle contrapeso al muy franquista exfutbolista español Salva Ballesta. Y a [Cristiano Lucarelli](http://www.futbolypasionespoliticas.com/2012/05/paolo-di-canio-vs-cristiano-lucarelli.html), el “goleador comunista” del Livorno y admirador del Che Guevara, que también le hizo contrapeso al muy fascista exjugador de la Lazio Paolo Di Canio.

Cómo dejar de lado a Maradona que en el mundial de [México 86](https://www.youtube.com/watch?v=9AQrLxWnXWs)cobró revancha ante los ingleses por la derrota en la Guerra de las Malvinas y por su simpatía por la [izquierda latinoamericana](https://www.youtube.com/watch?v=IdZuDj82o_8) y no nos olvidemos del St Pauli alemán y del Rayo Vallecano español por llevar a la práctica la democracia en el mundo del fútbol.

Y así son muchas las historias y las enseñanzas que nos ha regalado el fútbol de las cuales podemos aprender y no verlo solamente cómo un juego frívolo donde imperan los valores del capitalismo de la eficacia, mérito, riqueza y ganar cómo sea.

Hagámosle caso a Gramcsi, disfrutemos de “este reino de la lealtad humana ejercida al aire libre”
