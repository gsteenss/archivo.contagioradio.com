Title: Discursos guerreristas aumentan la xenofobia contra el pueblo musulman
Date: 2015-11-17 17:24
Category: El mundo
Tags: apoyo de UE a Francia, Atentado en Francia, Estado Islámico, Europa, francia, Guerra contra EI, Obama, UE, Unión europea, Víctor de Currea Lugo
Slug: guerra-contra-el-terror-ha-fracasado-victor-de-currea-sobre-francia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Discurso-de-presidente-Holland.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [peru21.pe]

###### [17 Nov 2015]

Los atentados en Francia que dejaron más de 130 muertos y decenas de heridos, atribuidos al Estado Islámico (EI), **hoy tienen a la Unión Europea y a Estados Unidos, principalmente a Francia bajo un discurso belicista contra la comunidad musulmana,** con expresiones que llaman a la guerra y el odio, como lo asegura Chatherine Merchaise, sindicalista y servidora pública de Francia.

La ciudadana francesa expresa que “siente como el terror está invadiendo las calles”  de Francia, pero aclara que **los discursos de intervención militar y el** llamado constante a la guerra durante las palabras pronunciadas por el presidente Hollande generan mayor terror.

Así mismo, la sindicalista agrega que estos discursos promueven la xenofobia, generando una **“sociedad de sospecha” que solo tendrá como resultado consecuencias mortíferas en la sociedad”.**

"Obama habló primero a los franceses que su mismo presidente ¿Esto qué significa?, Francia es el aliado servil de la política imperialista de EEUU" dice Merchaise. Se escoge a Francia para hacer estos ataques debido a que es “lacayo de Estados Unidos en Europa” y en su comunidad marginalizada, entre árabes, argelinos, marroquís, tunesinos y sirios el EI, encuentra simpatizantes insatisfechos y faltos de oportunidades.

Por su parte, frente al apoyo unánime de la UE sobre el pedido de asistencia militar de Francia, el analista internacional Víctor de Currea  indica que se incurriría en un error ya que la **“guerra contra el terror ha fracasado”,** un “bombardeo no resuelve un problema”, haciendo referencia a la acción armada que ya se inició desde Francia en contra de Siria.
