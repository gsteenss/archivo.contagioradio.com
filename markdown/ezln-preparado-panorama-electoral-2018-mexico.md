Title: EZLN preparado para el panorama electoral de 2018 en México
Date: 2017-01-06 17:03
Category: El mundo, Política
Tags: Congreso Nacional Indígena, Democracia y Pueblos Indígenas, Elecciones México 2018, EZLN
Slug: ezln-preparado-panorama-electoral-2018-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/EZLN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mirada EZLN] 

###### [6 Enero 2017] 

En el marco del Quinto Congreso Nacional Indígena en México, el Ejército Zapatista de Liberación Nacional EZLN anunció a través de sus plataformas virtuales que en **Mayo dará a conocer el nombre de la primera mujer indígena candidata a la presidencia en México.**

Durante el quinto Congreso, **alrededor de 430 comunidades indígenas votaron en una consulta a favor de la creación del Consejo Nacional Indígena** que articulara a los pueblos zapatistas para decidir la candidatura.

Además, el EZLN emitió un comunicado invitando al fortalecimiento de la unión entre pueblos indígenas y la clase trabajadora mexicana, en pro de la vida, la libertad, la justicia y la democracia.

“Para vivir de la única forma que vale la pena vivir, con libertad, con justicia, con democracia. La lucha a la que nos llama y nos invita **el Congreso Nacional Indígena es una lucha por la vida con libertad, con justicia, con democracia, con dignidad”** fue el mensaje transmitido por el EZLN en palabras del subcomandante insurgente Moisés.

Por último, el EZLN resalta a través del comunicado que las causas que motivaron el inicio del Ejército Zapatista hace 23 años, ahora se han agudizado y afectan a toda la población, **“la pobreza, la desesperación, la muerte, la destrucción, no son sólo para quienes poblaron originalmente estas tierras”** puntualizó.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU) 
