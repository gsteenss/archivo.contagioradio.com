Title: OEA impidió entrada de organizaciones sociales a la asamblea general
Date: 2016-06-14 16:38
Category: Nacional, Política
Tags: CEJIL, Derechos Humanos, LGBTI, OEA, Organización de Estados Americanos
Slug: oea-impidio-entrada-de-organizaciones-sociales-a-la-asamblea-general
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/oea-asamblea-contagioradio.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: metrord] 

###### [14 Jun 2016]

Aunque uno de los pilares de la Organización de Estados Americanos, OEA, es que las organizaciones de sociedad civil participen activamente en la toma de decisiones, **ese** organismo notificó que **"no podían ingresar al recinto de los debates" en Santo Domingo,** a cientos de representantes de organizaciones sociales que se trasladaron hasta la República Dominicana para poder ser parte activa de la Asamblea.

Según el Centro por la Justicia y el Derecho Internacional, [CEJIL](https://archivo.contagioradio.com/?s=cejil), **una de las tareas de esta Asamblea es la ratificación y fortalecimiento de espacios de la sociedad civil**, objetivo que se entiende **contradictorio** con la actitud tomada por la mesa directiva de la [OEA](https://archivo.contagioradio.com/?s=oea) cuando se impide el ingreso a esas mismas organizaciones.

Según varios de los que pretendían asistir, la confirmación del impedimento para ingresar al recinto le llegó vía correo electrónico, y además se les invitó a seguir las discusiones a través de los televisores que fueron ubicados a las afueras del recinto. La negativa causó tal indignación que **las personas que se hicieron presentes en Santo Domingo marcharon hasta las puertas del centro de convenciones en donde fueron recibidos por un escuadrón antimotines.**

En varios comunicados públicos realizados por las organizaciones en América Latina, entre ellos organizaciones venezolanas, hicieron un llamado tanto al gobierno de República Dominicana como a la propia OEA para que se respete el derecho a participar y ser miembros activos en la toma de decisiones. “D*esde la sociedad civil exigimos que la OEA no obstaculice nuestra participación y acceso. Éstas fueron las condiciones en las que nos trasladamos hasta el país, con el propósito de ser parte activa del evento, como lo hemos sido durante las últimas décadas.*”

Por si fuera poco, otra de las grandes críticas a la Organización de Estados Americanos, en Santo Domingo, fue que **no se permitió el acceso de las mujeres de trans sexuales a los baños de mujeres en el centro de convenciones**, lo que se convirtió en otra de las situaciones discriminatorias promovidas por ese organismo.

El hecho, altamente criticado por organizaciones [LGBTI](https://archivo.contagioradio.com/?s=mujeres+trans+sexuales) alrededor del mundo, se da dos días después de la **masacre de jóvenes de esa comunidad en una discoteca en Orlando**, Florida EEUU, que dejó un saldo de 50 víctimas fatales y más de 40 heridos.

En esta Asamblea se esperaría que la OEA defina también un plan de salvamento de la [Comisión Interamericana de Derechos Humanos](https://archivo.contagioradio.com/cidh-perdera-el-40-del-personal-por-falta-de-financiacion/), que ha tenido que cancelar su periodo de sesiones programado para Octubre de este año, así como el recorte del 40% de su personal. Una de las principales razones de la crisis económica en la CIDH es que la OEA solamente dedica un pequeño porcentaje de los recursos al SIDH.
