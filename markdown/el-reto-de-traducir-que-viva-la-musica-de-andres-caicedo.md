Title: El reto de traducir "Que viva la música" de Andrés Caicedo
Date: 2016-01-13 13:04
Category: Cultura, eventos
Tags: Andrés Caicedo, Biblioteca luis Angel Arango, Que viva la música
Slug: el-reto-de-traducir-que-viva-la-musica-de-andres-caicedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/caicedo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [13 Ene 2016]

En 1977, año en que Andrés Caicedo decidió acabar con su vida porque consideraba que vivir más allá de los 25 era “una vergüenza”, fue publicada por Colcultura “Que viva la música”, única novela (conclusa) del escritor caleño que se convertiría en una obra de culto, influyente en varias generaciones de lectores y escritores.

El estilo de escritura del entonces perteneciente al que se conoce como “el grupo de Cali” o Caliwood, innovador en las formas y en el uso del lenguaje, la apropiación de los personajes, en su mayoría adolescentes, como relatores de sus propias historias y las múltiples referencias a la música, el cine y la literatura, grandes pasiones de su corta pero fructífera vida.

Todos los elementos que Caicedo utilizó para construir su relato, tanto en sus críticas en “Ojo al cine”, sus cuentos y novela(s), hacen que traducir su obra a otras lenguas resulte ser una tarea que implica destreza y habilidad a la hora de trasladar los espacios, enmarcados en la Cali de los años 70, las alusiones constantes a temas musicales y películas, y por su puesto a la personalidad de los personajes, particularmente en María del Carmen Huerta.

\[caption id="attachment\_19134" align="aligncenter" width="500"\][![que viva la musica](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/tumblr_nraj85W3Cz1sidmsio1_500-e1452708222480.jpg){.wp-image-19134 .size-full width="500" height="237"}](https://archivo.contagioradio.com/el-reto-de-traducir-que-viva-la-musica-de-andres-caicedo/tumblr_nraj85w3cz1sidmsio1_500/) Fragmento "Que viva la música"\[/caption\]

Franck Waynne, traductor literario del francés y del español, fue el encargado de adaptar los textos que componen “Que viva la música”, un trabajo de más de dos años, tiempo que el mismo justifica citando a Anthony Burgess, autor de “A clockwork orange” (llevada al cine por Stanley Kubrick) que “La traducción no es solamente cuestión de palabras: se trata de hacer inteligible toda una cultura”.

Con más de cincuenta obras publicadas en su palmarés, Waynne **visitará Bogotá el próximo 21 de enero**, para hablar acerca de su experiencia y trayectoria profesional, en la que ha traducido textos de autores como Michel Houellebecq, Boualem Sansal y Pierre Lemaitre, Pablo Picasso, Alonso Cueto y Tomás González, obteniendo importantes reconocimientos por varios de sus trabajos.

La conferencia que **tendrá lugar en la Biblioteca Luis Angel Arango desde las 5 p.m**., contará con la participación de Mario Jursich y Sandro Romero Rey, quienes en conversación con Waynne ayudarán a comprender el proceso de traducción de la obra de Caicedo. La **entrada es totalmente libre hasta completar el aforo del recinto**.

Leer [Entre críticas y expectativa se estrenó la película "Que viva la música"](https://archivo.contagioradio.com/entre-criticas-y-aplausos-se-estrena-en-colombia-que-viva-la-musica/)
