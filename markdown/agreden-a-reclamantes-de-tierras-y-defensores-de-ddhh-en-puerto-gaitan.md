Title: Agreden a reclamantes de tierras y defensores de DDHH en Puerto Gaitán
Date: 2015-09-22 18:09
Category: DDHH, Nacional
Tags: Corporación Claretiana, Corporación Claretiana Norman Pérez Bello, INCODER, Jaime León, Mataratón, Paramilitares extorsionan campesinos, porvenir, Puerto Gaitán, Radio de derechos humanos, Titulación de baldíos en Colombia, Victor Carranza usurpa tierras en Puerto Gaitán, Violación a derechos humanos en Puerto Gaitán
Slug: agreden-a-reclamantes-de-tierras-y-defensores-de-ddhh-en-puerto-gaitan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Norman-Pérez-Bello.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Corporación Claretiana Norman Pérez Bello ] 

<iframe src="http://www.ivoox.com/player_ek_8552586_2_1.html?data=mZqilJqceo6ZmKiak5aJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcjmxsnS0JDFb9PZxNHOz8bSuMbnjMnSjdnNqdPmwtiY25DIqcfZz9jc1MrXb8XZjKmxqq2PqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Jaime León] 

###### [22 Sept 2015] 

[250 familias habitantes de las veredas Porvenir y Mataratón en Puerto Gaitán, son constantemente amenazadas y recientemente varios de sus líderes y acompañantes han sido agredidos físicamente, por **invasores "comprados" por la familia Carranza que buscan expropiarlos de sus tierras, mientras que el gobierno nacional hace caso omiso de las denuncias, afirma Jaime León de la Corporación Claretina Norman Pérez.**]

[Las familias que han convivido por más de 40 años en estas veredas, aseguran que quienes buscan despojarlos de sus tierras son **ocupantes sobornados por empresarios y políticos de la región vinculados a estructuras paramilitares asociadas con la compra ilegal de tierras usurpadas**, puesta en marcha por Víctor Carranza y que ahora sus herederos buscan reactivar.]

[Jaime León, acompañante de las familias, afirma que **tras la resolución 6423 de 2014 del Incoder**, sobre titulación de baldíos, que entre otras, ponía freno a las vacunas de 20 mil pesos mensuales con las que paramilitares extorsionaban a los campesinos, las **agresiones físicas y verbales en contra de estas familias campesinas y de defensores de derechos humanos que les acompañan han aumentado**.]

[Son **28 mil** las **hectáreas que están en disputa**, por cuenta de **invasores que, asistidos por "supuestas ONGs", arriban diariamente a la zona, para cercar grandes extensiones y alegar su propiedad**. Pese a que estos amenazan, insultan y agreden con machetes a adultos de la tercera edad,]

[Las **familias campesinas se han articulado con la guardia indígena de Vichada para la defensa de sus vidas y territorios**, adelantando la recuperación de cerca de 15 km que ya habían sido cercados por invasores.]

[El director del Incoder afirma que hasta ahora tienen conocimiento del caso y de acuerdo a León, **la “falta de respeto del Estado” ha sido constante**, pues además ha dilatado las fechas para reunirse con las comunidades, lo que agudiza la problemática pues es claro que **“el Gobierno se está haciendo el que no ve para provocar conflictos por la tierra en esta región”**.]

[Entre las empresas implicadas en estas acciones de despojo se destaca **la filial Colombia Agro de la multinacional Carry**, que **ha venido usurpando tierras en otras zonas de Puerto Gaitán**.]

[Ante la grave situación en materia de derechos humanos de estos “pueblos abandonados en medio del olvido estatal” se estima adelantar una **misión humanitaria en la región en los próximos días, con la participación de diversos sectores sociales**.]
