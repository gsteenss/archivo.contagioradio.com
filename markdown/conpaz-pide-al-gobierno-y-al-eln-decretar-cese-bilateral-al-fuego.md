Title: CONPAZ pide al Gobierno y al ELN decretar cese bilateral al fuego
Date: 2017-06-12 13:23
Category: Nacional, Paz
Slug: conpaz-pide-al-gobierno-y-al-eln-decretar-cese-bilateral-al-fuego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/CONPAZ-e1497291777627.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Jun 2017] 

En una carta enviada al presidente Juan Manuel Santos y a Nicolás Rodríguez Bautista, Comandante Máximo del ELN, las 153 comunidades de la Red Comunidades Construyendo Paz en los Territorios, CONPAZ, **le piden por tercera ocasión a las partes, sellar un acuerdo de cese bilateral al fuego.**

El llamado lo hacen teniendo en cuenta “las hostilidades y confrontaciones armadas entre las Fuerzas Militares de Colombia y el Ejército de Liberación Nacional – ELN –, y en algunos de los territorios con **nuevas formas de operación de tipo paramilitar** con la tolerancia, complicidad e ineficacia”. (Le puede interesar: ["Amenazan de muerte a líder social de Conpaz en Buenaventura"](https://archivo.contagioradio.com/amenazan-de-muerte-a-lider-social-de-conpaz-en-buenaventura/))

Las comunidades denuncian la continuidad de la violencia socio política “a pesar del avance que significa el Acuerdo del Teatro Colón con las FARC EP”. Según ellos, **los acuerdos de la Habana no han mitigado la guerra armada** que “continua destruyendo las posibilidades de la vida digna en los territorios de Colombia”.

CONPAZ ha planteado el cese bilateral del fuego entre el ELN y el Gobierno desde 2014 y aseguran que **“desde Quito solo han escuchado retrocesos y no avances”.** Las comunidades fundamentan sus peticiones argumentando que “el cese bilateral del fuego evitará muertos, heridos de militares, guerrilleros y hasta neoparamilitares”. Igualmente aseguran que con esto los territorios se podrán recuperar de la constante zozobra y las poblaciones podrán recuperar sus siembras. (Le puede interesar: ["Conpaz exige voluntad política del gobierno para implementación de acuerdos"](https://archivo.contagioradio.com/conpaz-exige-voluntad-politica-para-implementacion-de-acuerdos/))

De manera paralela, CONPAZ anunció que las comunidades del San Juan y del Bajo Atrato realizarán una propuesta de Zonas Humanitarias dada la grave situación de violencia que están viviendo. Afirmaron que esperan que las partes logren un acuerdo pronto para **“abrir el camino hacia la construcción de un ambiente hacia la paz.**

[Carta CONPAZ a Santos y ELN](https://www.scribd.com/document/351071626/Carta-CONPAZ-a-Santos-y-ELN#from_embed "View Carta CONPAZ a Santos y ELN on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_77318" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/351071626/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-D8XD3m2hCMv0BrX6U4Uu&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
