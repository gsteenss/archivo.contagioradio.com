Title: COMEVA le está aplicando el “paseo de la muerte” a Carlos Lozano
Date: 2016-04-29 18:57
Category: DDHH, Nacional
Tags: Carlos Lozano, Conversaciones de paz de la habana, Partido Comunista Colombiano, semanario Voz
Slug: en-riesgo-la-vida-de-carlos-lozano-porque-comeva-le-esta-aplicando-el-paseo-de-la-muerte
Status: published

###### [Foto: archivo] 

###### [29 Abril 2016]

Desde hace un año Carlos Lozano, director del semanario Voz y líder del Partido Comunista Colombiano, viene padeciendo un cáncer de colon que le hizo metástasis en el hígado, fue operado y se inició un tratamiento de quimioterapia “Ahí comenzó mi calvario” porque la EPS COMEVA no pagó a tiempo el costo que ya había sido autorizado.

Durante 2016 el Instituto Cancerológico en Bogotá asumió el tratamiento pero ordenó un nuevo medicamento, se trata del panitumumab que debe ser autorizado por la EPS, no solamente para esta sesión de quimioterapia sino para concluir el tratamiento completo. Afortunadamente al finalizar la tarde, el doctor Lozano, a través de su cuenta de Twitter afirmó que ya había sido autorizado el medicamento.

Sin embargo, Lozano también recuerda que son miles y miles de colombianos los que a diario afrontan situaciones parecidas y que no cuentan con el apoyo de nadie y por ello tiene que hacerse una revisión del Sistema de Salud en Colombia que impida que la salud siga siendo un negocio para unas cuantas empresas.

<iframe src="http://co.ivoox.com/es/player_ej_11379292_2_1.html?data=kpagmZ6WfZOhhpywj5aUaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncaTV09Hc1ZCws9vVz9SYj5C3qc7Vz8bfy9SPmtDujoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
