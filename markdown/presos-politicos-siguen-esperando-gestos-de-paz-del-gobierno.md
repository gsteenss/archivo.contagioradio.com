Title: Presos políticos siguen esperando gestos de paz del Gobierno
Date: 2016-01-12 14:30
Category: Paz
Tags: dialogos de paz, Incumplimiento gestos de paz gobierno Colombia, Prisioneros políticos FARC-EP
Slug: presos-politicos-siguen-esperando-gestos-de-paz-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Presos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  El Aguijón ] 

<iframe src="http://www.ivoox.com/player_ek_10050113_2_1.html?data=kpWdl5WVdZShhpywj5aUaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncbHmxtjc1ZDUs82ZpJiSo6nYrcTj1JDgy8zZqc%2Bfxtjdx9fFssXjjMzS1dnTt4zYxpDdw9%2BPqMbgjKzcj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rene Nariño, Prisionero Político FARC- EP] 

###### [12 Ene 2016 ] 

[En los más de 45 días que han transcurrido del anuncio oficial de indulto a presos políticos de las FARC-EP, brigadas de salud y concentraciones en patios, se han agudizado las torturas y violaciones DDHH en su contra y **a la fecha ha habido toda suerte de negligencia, de burocracia, e incluso desinterés y parálisis sobre este tema**” afirma René Nariño.]

[Luego de las protestas en 21 cárceles colombianas, **el Gobierno nacional asumió en noviembre el compromiso de  indultar 30 prisioneros de las FARC-EP**, realizar brigadas de salud en los centros carcelarios para atender los casos más críticos y adecuar patios especiales para la concentración de presos políticos; sin embargo no se ha cumplido con ninguna de las tres medidas pactadas.]

[Aunque el Gobierno colombiano aseguró un plazo de 15 días, según el propio Ministro de Justicia Yesid Reyes, **han pasado 45 días en los trámites que tienen que ver con los tiempos y las dinámicas propias de las instituciones del Estado**. De acuerdo con Nariño, este incumplimiento es muestra de la voluntad de dilación del Gobierno frente a las acciones humanitarias.]

[“Los gestos de paz deberían ser caminos de confianza para la consolidación del proceso de paz. De los gestos anunciados no ha habido ningún avance, no ha habido una sola brigada de salud en ningún centro penitenciario del país, **la situación real a la fecha es de mayor deterioro en la salud de nuestros compañeros y compañeras** y mayor riesgo para todos nosotros”.]

[Nariño concluye que “**ni siquiera en el marco de los Diálogos de La Habana el Estado colombiano se atreve a mover un solo dedo para poder llegar a generar espacios de mayor confianza**” por el contrario lo que provoca son incógnitas frente a lo que estamos y a dónde queremos llegar que es la firma de un tratado y una posible reconciliación en los periodos que vienen”.]
