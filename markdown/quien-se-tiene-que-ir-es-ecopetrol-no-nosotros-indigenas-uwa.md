Title: "Quien se tiene que ir es Ecopetrol no nosotros": indígenas U'wa
Date: 2016-07-26 13:23
Category: DDHH
Tags: asou'wa, Ecopetrol, planta de gas Gibraltar
Slug: quien-se-tiene-que-ir-es-ecopetrol-no-nosotros-indigenas-uwa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/ASOUWA.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Amazon Watch Org ] 

###### [26 Julio 2016]

Desde el pasado miércoles al menos 400 indígenas, entre niñas, niños, mujeres, hombres y adultos mayores permanecen concentrados en la planta de gas de Gibraltar, ubicada en Toledo, Norte de Santander, exigiendo a Ecopetrol que deje de ocupar los territorios que les pertenecen como nación U'wa. A esta movilización **se han sumado cerca de 7 mil pobladores, entre campesinos, indígenas y afros** de Saravena, Arauca, Cubarral, Fortul y Tame.

Pese a que no han habido agresiones por parte de la fuerza pública, las comunidades denuncian que están en inminente riesgo, pues este lunes en la mañana **aterrizaron tres helicopteros con miembros del ESMAD que permanecen en los puntos de concentración.** Así mismo denuncian que los directivos de Ecopetrol han impedido a los trabajadores de la planta salir de las instalaciones y usar sus teléfonos celulares.

Este miércoles los voceros campesinos y las autoridades U'wa, sostendrán una **reunión con funcionarios del alto gobierno y los gobernadores de Santander y Boyacá**, tras la que esperan se implementen acciones concretas que cumplan con sus exigencias, relacionadas con el [[reconocimiento de los títulos coloniales y el saneamiento territorial](https://archivo.contagioradio.com/indigenas-uwa-se-toman-planta-de-ecopetrol-antes-incumplimientos-del-gobierno/)].

Según Vladimir Moreno, presidente de ASOU’WA, pese a la amenaza de desalojo violento emitida por las autoridades, las comunidades planean **mantenerse en concentración hasta que les cumplan sus demandas** "pase lo que pase, así el Gobierno de ordenes a las fuerzas militares, antimonotines, ESMAD o lo que sea (...) no nos vamos a rendir".

<iframe src="http://co.ivoox.com/es/player_ej_12344553_2_1.html?data=kpeglpmZeZShhpywj5WbaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncbfgwsnWz87Wb67j08rb0YqWh4y11NTCh5ebu8Khhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
