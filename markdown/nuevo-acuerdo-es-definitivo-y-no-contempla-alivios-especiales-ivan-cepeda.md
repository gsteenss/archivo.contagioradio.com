Title: Nuevo acuerdo es “definitivo” y no contempla más alivios especiales: Ivan Cepeda
Date: 2016-11-15 13:19
Category: Nacional, Paz
Slug: nuevo-acuerdo-es-definitivo-y-no-contempla-alivios-especiales-ivan-cepeda
Status: published

###### [Foto: contagioradio] 

###### [15 Nov 2016]

El senador Iván Cepeda, quien ha hecho parte de las conversaciones de paz tanto antes de la firma del acuerdo en Septiembre, como después del 2 de Octubre, afirmó que este nuevo acuerdo de paz es “definitivo” y no estará sujeto a más cambios.

El senador aseguró que el mecanismo de refrendación está supeditado a la decisión de la Corte Constitucional con el fallo de tutela interpuesta por el Centro Democrático que abriría la posibilidad al Fast Track.

También hizo la precisión de que en el preámbulo del acuerdo está claramente señalado que es **un acuerdo especial que se deposita en cortes internacionales** y que estará regido por los tratados en materia de Derechos Humanos y Derecho Internacional Humanitario, es decir que las cortes que lo revisen en el futuro tendrán estos tratados como principios rectores y sobre esa base se hará cualquier análisis.

### **El Nuevo Acuerdo y las tierras** 

**Frente al tema de las tierras** el legislador afirmó que se precisa que la propiedad privada no está en peligro y se mantienen todas las disposiciones del acuerdo inicial, es decir que el banco de tierras sigue firme, así como los mecanismos de titulación considerados en el acuerdo inicial, “En términos de tierras se precisan más conceptos frente a titulación” precisó Cepeda.

**Frente a las voces del NO** **que siguen manifestando su oposición al acuerdo de paz**, el senador manifestó que se incluyeron muchas de las exigencias y que la postura del Centro Democrático en esta nueva coyuntura va a definir su vitalidad política. En ese sentido Cepeda aseguró que “Es una mala orientación política seguir oponiendose la paz”.

Por su parte Alirio Uribe, representante a la cámara afirmó que si se llega a dar una refrendación o una **implementación via congreso de la república**, como se presume hasta el momento, por lo menos el 80% de esa institución está alineado con el nuevo acuerdo.

Frente a la refrendación el congresista aseguró que sería un muy buen gesto, **refrendar el acuerdo en el campamento por la paz**, tal y como se ha venido pidiendo a lo largo de este fin de semana a través de las redes sociales. (Lea también [Campamento por la paz evalúa su continuidad hasta la implementación](https://archivo.contagioradio.com/campamento-por-la-paz-evalua-continuidad-en-plaza-de-bolivar/))

<iframe id="audio_13768233" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13768233_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
