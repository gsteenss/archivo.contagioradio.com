Title: El fracking… una técnica agresiva con el medio ambiente
Date: 2015-07-17 12:51
Category: Ambiente, El mundo, Otra Mirada
Tags: Álava en Euskadi, Frackanpada Internacional, fracking, Kaixo Latinoamerica, Subijana, Vitoria-Gasteiz
Slug: el-fracking-una-tecnica-agresiva-con-el-medio-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/fotonoticia_20150218122249_644.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotonoticia 

<iframe src="http://www.ivoox.com/player_ek_4941340_2_1.html?data=lZ6hk5iYdI6ZmKiakpmJd6KnkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRia2fp7eupbCtkqiij5OY19PFb9WZpJiSo57HssrXwpDOydfJt8rqwpDQ0dOPqc2fzsrRy9SPpc7Wysqah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [17 Jul 2015] 

El uso de la energía es consustancial a cualquier sociedad humana. Todas las sociedades han dedicado una parte de su tiempo, esfuerzo y recursos a obtener la energía necesaria para su funcionamiento. Si bien tradicionalmente se ha utilizado prioritariamente energía proveniente de recursos renovables (biomasa, viento, agua) la situación ha cambiado mucho en los dos últimos siglos. Sin embargo, las fuentes de energía primaria más utilizadas en la actualidad (carbón, petróleo, y gas natural) no son recursos renovables, es decir son fuentes energéticas agotables.

Y en algunos casos, como en el petróleo convencional, se puede observar que el pico de producción ya ha sido alcanzado, como han reconocido diferentes agencias internacionales. Con el resto de combustibles de carácter agotable sucederá lo mismo, entre los que también se encuentra el Uranio en un breve período de tiempo, de ahí que hoy los países del Norte y sus multinacionales estén tratando de implementar el FRACKING.

**El Fracking es una técnica que consiste en introducir a gran presión un fluido formado por agua, arenas y productos químicos a grandes profundidades** para que agrieten las rocas que contienen los hidrocarburos no convencionales como el gas y el petróleo, en este caso en territorio del País Vasco. El uso del Fracking genera contaminación de las aguas superficiales y profundas; contamina el aire, consume muchísima agua, contamina el ambiente y daña la salud humana y animal.

Por su parte, desde el lunes 13 al domingo 19 de Julio del 2015 se está realizando una gran **Akanpada Internacional en protesta contra el Fracking**, esta “**Frackanpada Internacional**” como la han denominado sus organizadores, es producto de la inconformidad de mucha gente de los pueblos de Araba, Bizkaia, Gipuzkoa y alrededores, así también de la dinámica de la Plataforma “Fracking Ez” que logró recoger en el año 2014 cerca de 103.000 firmas ciudadanas que registraron en el Parlamento Vasco y planteadas como una Iniciativa Legislativa Popular (ILP) que han servido como base para la nueva ley contra el fracking que reforma las leyes sectoriales de suelo y agua para impedir que se utilice la fracturación hidráulica en el País Vasco.

**La Frackanpada** se está realizando en la población de Subijana de Álava en Euskadi, con la participación de más de 50 colectivos venidos desde distintos lugares del Estado Español y otros países europeos, y en cuyo programa incluye más de 70 actividades que tienen que ver con charlas, conferencias, mesas redondas, documentales, teatro, bailes populares, conciertos y una gran movilización entre Subijana y Vitoria-Gasteiz para este sábado 18 de julio en las horas del mediodía.

Con la nueva Ley se introduce una nueva disposición que no permita la fractura hidráulica en suelo no urbanizable y modifica también la ley de Aguas para prohibir el uso de esta técnica de extracción de gas en aquellos espacios de riesgo medio, alto o muy alto en el mapa de vulnerabilidad a la contaminación de acuíferos. A sí mismo, obliga a someter cualquier proyecto de fracking a una estricta y compleja evaluación ambiental estratégica.

Desde el País Vasco, para Contagio Radio, les informó “**Kaixo Latinoamerica-Hola Latinoamérica**”
