Title: "Ley de Amnistía no puede ser condicionadas" Enrique Santiago
Date: 2018-03-02 13:39
Category: Nacional, Paz
Tags: acuerdo de paz, Corte Constitucional, Ley de Amnistia, Ley de Amnistía e Indulto
Slug: ley-de-amnistia-no-pueden-ser-condicionadas-enrique-santiago
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Enrique-Santiago.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [02 Mar 2018] 

El abogado Enrique Santiago, asesor jurídico durante el proceso de paz aseguró que el fallo de la Corte Constitucional iría en contra del proceso de la ley de Amnistía que no puede ser condicionada y cuestionó la labor de esta institución al asegurar que este órgano debería **funcionar como un revisor de los Acuerdos para ajustarlos a la Constitución Colombiana y no como un legislador que modifica lo pactado**.

Para el abogado, la decisión de la Corte debe ser revisada con más cautela, sin embargo, expone diferentes problemáticas que ponen en tela de juicio lo avanzando en materia de implementación. En primera instancia está el hecho de condicionar a las personas beneficiarias de las amnistías o indultos, “**las amnistías no son condicionadas, eso quedó muy claro en el acuerdo**, y, además, entiendo que ni en la Constitución Colombiana ni en el Derecho Internacional existe alguna norma que obligue a condicionarlas” afirmó Santiago.

En ese sentido afirmó que hay dos condiciones primordiales para iniciar el proceso de Amnistía que son el fin de la rebelión y la dejación de armas, ambas ya cumplidas por la FARC y sus integrantes. (Le puede interesar:["Corte Constitucional restringe Ley de Amnistía e indulto"](https://archivo.contagioradio.com/corte-constitucional-ley-amnistia-e-indulto/))

### **Las condiciones de la Corte Constitucional** 

Una de las condiciones que impuso la Corte es que la libertad de los indultados y amnistiados no sea definitiva hasta que no se compruebe su colaboración con la verdad en los procesos de justicia, hecho que para el abogado Enrique Santiago es **“impropio de cualquier régimen de amnistía”** porque esta tiene efecto de cosa juzgada.

Frente al reclutamiento de menores de 18 años, en el Acuerdo también se estableció que esta conducta también es amnistiable porque no está tipificado como delito en la Corte Penal Internacional. Por lo tanto, **para el abogado se estaría yendo en contra de lo acordado.**

 Sobre la reparación a las víctimas que señala la Corte Constitucional, debe hacerse a partir de la verdad, Enrique Santiago afirmó que la reparación hace parte del cuarto punto del Acuerdo de Paz y no de la JEP, en donde se establece que no hay reparación económica, más allá de las reparaciones simbólicas. Así mismo afirmó que si la Corte condicionara la amnistía a la capacidad de reparación económica que tuviese la persona beneficiaria de este proceso, estaría obviando la reparación integral a las víctimas que debe hacer todo el sistema.

Sobre el poder que se le daría al Congreso para establecer si los amnistiados e indultados cumplen con su labor de aportar a la verdad, Enrique Santiago señaló que **es “poner al lobo a cuidar a las ovejas”**. (Le puede interesar: ["Falta implementar el 82%  de los Acuerdos de Paz en Colombia"](https://archivo.contagioradio.com/falta-implementar-el-82-de-los-acuerdos-de-paz-en-colombia/))

### **El papel de la Corte Constitucional** 

Frente a la acción hecha por el órgano regulador, Enrique Santiago manifestó que la Corte a la hora de decidir sobre las leyes “**está revisando su ajuste a la Constitución y a las normas del derecho internacional** y si han sido incorporadas por el sistema colombiano al ordenamiento interno, o si está pretendiendo legislar”.

En ese sentido, aseguró que si el accionar de esta institución en la primera afirmación, cada argumento que dio debe estar fundamentado, por el contrario, si la acción es legislar estaría realizando una labor que no le compete.

######  Reciba toda la información de Contagio Radio en [[su correo]
