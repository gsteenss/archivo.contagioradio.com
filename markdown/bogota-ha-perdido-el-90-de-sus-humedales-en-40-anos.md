Title: Bogotá ha perdido el 90% de sus humedales en 40 años
Date: 2018-08-15 12:21
Category: Ambiente, Nacional
Tags: Bogotá, Humendales, invierno
Slug: bogota-ha-perdido-el-90-de-sus-humedales-en-40-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Humedal_Cordoba-e1533059032847.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Humedales Bogotá- Emmanuel Escobar] 

###### [15 Ago 2018] 

**Desde hace 40 años, Bogotá ha registrado la pérdida de más del 90% de las hectáreas de humedales** debido principalmente a las construcciones de vivienda y a la falta de políticas que garanticen la protección de estos ecosistemas. Según Daniel Bernal, integrante de Humedales Bogotá, si llegase a extinguirse estos ecosistemas, la capital podría tener inundaciones mucho más fuertes en época de lluvias y veranos más intensos.

Además, señaló que es cada vez más evidente las afectaciones por el detrimento que han sufrido estos ecosistemas, en la fauna y flora, en dónde según Bernal, se ha dejado de hacer avistamientos de aves como el pato zambullidor, mientras que otras **como el cucarachero de pantano, la tingüa bogotana y la tingüa pico verde, han reducido su población de forma alarmante**.

### **Bogotá sin Humedales** 

Bernal también aseguró que la desaparición de los humedales no solo tiene repercusiones en las especies de animales y flora, sino que de igual forma, provocaría daños irremediables en la ciudad, uno de ellos tendría que ver con el aumento de las zonas que se inundan en la capital si continúan las construcciones sobre este habitad, debido a que los suelos se rellenan con materiales y **cuando llegan las lluvias no hay colchones naturales que retengan el agua.**

“Se ha calculado que, si Bogotá perdiera los humedales de Tibabuyes y Jaboque, que son los más grandes de la capital, las inundaciones podrían llegar hasta la carrera 30, debido a que no habría regulación del ciclo hídrico” explicó Bernal. (Le puede interesar: ["Descontaminación del Río Bogotá afectaría Humedal Tibaguya"](https://archivo.contagioradio.com/pretenden-danar-humedal-tibaguya-para-descontaminar-rio-bogota/))

Asimismo, manifestó que los veranos podrían ser mucho más secos porque ya no existirían ecosistemas encargados de regular el drenaje del agua. **La calidad del aire igualmente podría verse afectada,** ya que estos lugares realizan procesos de eliminación de material particulado.

Razón por la cual, desde Humendales Bogotá hicieron un llamado urgente  a que se establezcan políticas de protección a humedales efectivas, que frenen las construcciones sobre estos territorios y a la ciudadanía que proteja estos lugares con acciones básicas como no arrojarles basuras o desechos.

<iframe id="audio_27865592" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27865592_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
