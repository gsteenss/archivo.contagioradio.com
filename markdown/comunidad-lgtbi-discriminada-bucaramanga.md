Title: Comunidad LGTBI víctima de discriminación por parte de grupo de religiosos en Bucaramanga
Date: 2018-09-18 15:34
Author: AdminContagio
Category: LGBTI, Nacional
Tags: Bucaramanga, Discriminación, lgtbi, Sociedad
Slug: comunidad-lgtbi-discriminada-bucaramanga
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/LGBTI-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [18 Sept 2018] 

[La Comunidad LGTBIQ en Bucaramanga, Santander, denunció que el pasado 15 de septiembre fue víctima de un acto de  discriminación en  la biblioteca pública Jorge Valderrama cuando se les negó la entrada al recinto, **luego de que un grupo de religiosos hicieran un plantón con acciones homofóbicas.**]

Los organizadores de la cátedra educativa recibieron una llamada, una hora antes del evento, donde personal de la biblioteca les informó que el espacio se había cerrado, sin ninguna justificación, y que no se les prestaría hasta nuevo aviso.

A pesar de la negativa, y del[ uso periódico del recinto para realizar espacios de lectura sobre diversidad sexual dirigido a los adultos en la Biblioteca, los integrante se dirigieron al lugar en donde se encontraron con un grupo de religiosos que adelantaba  un plantón frente al edificio en el parque cigarras, **manifestando clara oposición al uso de los auditorios por miembros del colectivo LGTBIQ Santander**.]

[Robinson Duarte, miembro del colectivo LGTBIQ Santander, narró que cuando llegaron al lugar encontraron “una horda de religiosos” con pancartas que contenían mensajes cómo **“Oraremos por tu alma pecaminosa”** y “Gays comunistas y socialistas” y cuando intentaron hablar con una de las mujeres que lideraban el plantón, los acusó de sucios, pecadores y de cometer actos aberrantes. (Le puede interesar: [En Colombia persiste la violencia hacia la comunidad LGTBI](https://archivo.contagioradio.com/en-colombia-persiste-la-violencia-hacia-la-comunidad-lgtbi/))]

[Duarte afirmó que el plantón religioso se convocó por medio de una plataforma virtual mexicana donde se recolectaron firmas para impedir que los integrantes del colectivo ingresaran a la biblioteca, “en la plataforma decían que estábamos adoctrinando niños y que formábamos parte de una milicia gay patrocinada por el Gobierno, que **nuestro propósito era vengarnos de la sociedad por no aceptarnos con nuestra sexualidad aberrante”**.]

[También aseguró que la biblioteca era un espacio que no se utilizaba hasta el momento y que la comunidad religiosa lo estaba reclamando únicamente porque era la comunidad LGTBI quienes la estaban usando. Finalmente hizo un llamado a que se respeten los derechos a la diversidad, “hoy pasa en la biblioteca, y si nos vamos a un parque ¿nos van a sacar de allá también?”.]

<iframe id="audio_28683441" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28683441_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).] 
