Title: Cierre de la vía al Llano ha dejado $2 billones de pesos en perdidas
Date: 2019-09-11 14:38
Author: CtgAdm
Category: Economía
Tags: bloqueo, crisis económica, derrumbes, Meta, perdidas economicas, Villavicencio
Slug: afectaciones-cierre-kilometro58-via-llano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Ananas_comosus_Victoria_P1190440.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Vacas_en_el_Llano.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-10-at-3.57.49-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-10-at-3.57.49-PM-1.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-10-at-3.57.49-PM-2.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/20432168175_265c497028_b.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/20432168175_265c497028_b-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/20432168175_265c497028_b-2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/via-al-llano.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Elberth Jimenez] 

Este jueves se cumplen tres meses del cierre del kilómetro 58 de la vía al Llano, el cual según un informe de la Secretaria de Desarrollo Agroeconómico del Meta **ha dejado pérdidas por más de \$2 billones** en la economía del departamento, los sectores más afectados han sido turismo, agricultura, transporte y comercio.(Le puede interesar:[¿Cuándo se establecerán responsabilidades por cierre en vía al Llano?)](https://archivo.contagioradio.com/responsabilidades-cierre-via-al-llano/)

De frente a esta situación, **sectores productivos como el ganadero, arrocero, productores de soya entre otros, enviaron en la primera semana de agosto una carta al Presidente de la República**, donde resaltaban su preocupación, “hasta ahora hemos sido pacientes, pero la gravedad de las afectaciones sociales y económicas está colmando la paciencia de los habitantes del departamento y la región”, esto con la finalidad de lograr agilidad en la apertura de la vía y garantías en el trasporte de sus productos.

### ¿Qué daños se han generado en el sector agricultor del Llano? 

**En el Llanos se produce alrededor del 30% de los productos agrícolas de Colombia,** la lechuga, el tomate, arroz, piña, yuca, soya y plátano son algunos productos que tienen que ser trasportados más de 1.000 kilómetros para alimentar a las familias en el centro del país.

###### ![piñas llanos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Ananas_comosus_Victoria_P1190440-1024x768.jpg){.wp-image-73433 .aligncenter width="400" height="317"}

###### [FOTO: David Monniaux] 

Así mismo, la mayor cosecha de arroz que se da desde julio hasta septiembre, se vio afectada en **incrementos de más de 30% en gastos de trasporte**, esto a pesar de que el Gobierno disminuyó el costo de los peajes y en algunas zonas el precio de la gasolina.**Desde el cierre de la vía, la producción agrícola en los llanos ha caído un 18%**, según cifras del Ministerio de Agrícultura, lo que ha desencadenado afectaciones en diferentes sectores entre ellos el cervecero que cayó en un 80%en esta zona y a su vez afecto los ingresos que tenía el sector salud; **el cual ha dejado de recibir 3.000 millones de pesos**.

(Le puede interesar:[&\#171;El ambiente debe ser incluido en los acuerdos de paz&\#187;, Mario Godinez](https://archivo.contagioradio.com/el-ambiente-debe-ser-incluido-en-los-acuerdos-de-paz-mario-godinez/))

### El sector del turismo uno de los más afectados 

El turismo se ha visto afectado en gran medida por las dificultades del desplazamiento, antes el viajero demoraba no más de 3 horas hasta los Llanos, **ahora el trayecto está en un promedio de 17 horas por la vía que cuenta con mejor infraestructura**. Existen vías alternas que disminuyen un poco la duración del recorrido, sin embargo, no son adecuadas para todo tipo de automóviles pues son carreteras por lo general demasiado angostas y con altos riesgos de accidentalidad. (Le puede interesar: [5 destinos que el turismo puede proteger](https://archivo.contagioradio.com/5-destinos-turismo-puede-proteger/))

![territorio llanos](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/WhatsApp-Image-2019-09-10-at-3.57.49-PM-2-1024x385.jpeg){.aligncenter .wp-image-73442 width="430" height="162"}

###### [FOTO: Contagio Radio] 

> **“Los cálculos indican que 406.281 visitantes han dejado de ir al departamento en el primer semestre de este año”**

Según comunicados emitidos por el alcalde de Villavicencio, Wilmar Babosa, **el turismo ha perdido alrededor de 1.300 millones de pesos diarios,** esto quiere decir que en los últimos tres meses el sector turístico en los Llanos ha perdido más de 120.000 millones de pesos.

En consecuencia, **los ingresos de mayo y junio en comparación con el año pasado en este sector, han caído un 20,67 %**; los hoteles han tenido que reducir su nómina de personal lo que significa alrededor de 1.200 despidos y el cierre de más de 6 hoteles entre ellos el Hotel del Llano, uno de los más tradicionales de la región. (Le puede interesar:[El ecoturismo como posibilidad](https://archivo.contagioradio.com/el-ecoturismo-como-posibilidad/))

Otros centros de recreación como las fincas agro-turísticas, también han tenido que cerrar sus puertas por la falta de clientes. **La cancelación en reservas ha llegado a 93%, y lugares de alto flujo de turistas como el Bioparque Los Ocarros, no ha logrado superar el 5% de visitas.**

### ¿Qué pasa con el mercado de animales de consumo en el Llano?

![animales de consumo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Vacas_en_el_Llano-1024x683.jpg){.wp-image-73434 .aligncenter width="371" height="358"}

###### [FOTO:Carolos Santos ] 

Los Llanos producen alrededor del 60% de la carne de res que se consume en el centro del país, esto significa aproximadamente 10.000 cabezas de ganado que se venden a Bogotá por semana, las que han dejado de ser vendidas o han tenido que incrementar sus gastos de transporte en un 40%.(Le puede interesar:[La ganadería extensiva el gran enemigo del Ambiente en Colombia](https://archivo.contagioradio.com/la-ganaderia-extensiva-el-gran-enemigo-del-ambiente-en-colombia/))

Sectores como el porcicultor, avicultor y piscicultor, también se han visto altamente afectados, teniendo en cuenta que la capital consume más o menos 300 toneladas de carne de cerdo, y cerca de 268 toneladas de pescado. La falta de movilidad no solo ha generado perdida de productos, también sobrecostos de más de 1.000 millones de pesos, y la pérdida de clientes, debido a que la capital ha buscado nuevos proveedores en otras regiones del país.

### **Sola la punta del iceberg …**

El Gobierno desde el cierre de la vía, anunció 72 medidas de mitigación y 600.000 millones de pesos destinados a soportar la crisis, entre ellas se destaca la reducción en los peajes, subsidios de combustible, promoción en las frecuencias aéreas hacia el aeropuerto de Villavicencio y la habilitación de 5 rutas desde y hasta los Llanos.

Sin embargo, esto no representó mayores cambios en el panorama, y luego de más de 180 días de espera, el viceministro de Infraestructura, Manuel Felipe Gutiérrez, anunció la apertura de la vía solo para vehículos de carga, la que se dará en las próximas semanas, y que ha recibido numerosas replicas por parte del gremio transportador, quienes exigieron garantías en su seguridad y la de sus cargamentos. (Le puede interesar:[Acciones populares y tutela frenarían daños ambientales en Guamal, Meta](https://archivo.contagioradio.com/las-acciones-populares-y-tutela-frenarian-danos-ambientales-en-guamal-meta/))

Lo que sucede en el trayecto Bogotá-Villavicencio se suma a la problemática que en los últimos tres meses, ha afectado otras regiones del país, como la  Troncal del Café  en el suroeste antioqueño, vías en caldas bloqueadas por derrumbes, en Risaralda en la vía la Romelia- Variante cerrada por deslizamiento de tierra, y muchas otras vías aproximadamente que presentas fallas geológicas, deslizamiento de tierra, pérdida de la banca, hundimiento de la calzada y caída de piedra, y demás problemas los cuales tampoco han sido atendidos de manera oportuna.

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
