Title: 134 campesinos detenidos en Santander durante el quinto día de Minga Nacional
Date: 2016-06-03 18:03
Category: Paro Nacional
Tags: congreso de los pueblos, Minga Nacional, Páramo de Berlín
Slug: 100-campesinos-detenidos-en-santander-durante-el-quinto-dia-de-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Berlín-ESMAD.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Congreso de los Pueblos ] 

###### [3 Junio 2016]

Según lo denuncia el Congreso de los Pueblos, este viernes el ESMAD arremetió contra el caserío Ranchadero, en Berlín, Norte de Santander, en el que se están movilizando por lo menos 200 campesinos en el marco de la Minga Nacional. De este ataque violento hicieron parte también, integrantes del GOES del Ejército Nacional que tienen sitiado el caserío. **6 personas fueron heridas y otras 134 detenidas, 4 mujeres y 15 menores de edad.** Hasta el momento hay 25 campesinos desaparecidos.

A las 5 de la mañana las comunidades decidieron cierran la vía Cucúta-Bucaramanga, media hora después miembros de la fuerza pública **arremetieron contra el caserío, ingresando violentamente a las casas y negocios, despojando ropa y víveres**, y lanzando gases lacrimógenos. En vista de los ataques de la fuerza pública, para escudarse e impedir que siguieran lanzando gases lacrimógenos, los pobladores decidieron incendiar una tractomula.

El ESMAD rodeó a los campesinos, los dividió en varios grupos, a 52 de ellos los subieron a los camiones de la Policía mientras a los demás **los desvistieron y golpearon en la carretera, les quitaron varias de sus pertenencias, y les pusieron elementos extraños en sus morrales**. Las personas detenidas fueron conducidas a Pamplona, algunos de ellos al batallón García Rovira, informándoles que les imputarán cargos por terrorismo y porte ilegal de armas.

8 de las personas detenidas, lograron evadir a la Policía, están en buen estado de salud y actualmente se encuentran en proximidades del casco urbano del municipio de Mutiscua, pidiendo protección de los organismos de control, pues a ellos también los amenazaron con privarlos de su libertad. **Se presume que en total son cien los campesinos que están detenidos**.

Integrantes de los equipos de comunicación y derechos humanos del Congreso de los Pueblos, denuncian además, que **personal vestido de civil de la DIPOL, movilizado en vehículos particulares les están grabando y tomando fotografías**, desde que inició la Minga Nacional.

Este sábado luego de 30 horas de privación de la libertad y próximos a la legalización de sus capturas, fueron liberados 114 de los campesinos, que continúan **vinculados a una investigación penal** que tendrán que atender en la Fiscalía.

<iframe src="http://co.ivoox.com/es/player_ej_11774716_2_1.html?data=kpakmZmbdZehhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncbXm0MjVw9PIs4znytOYyNfTstXZ08bgj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]][ ] 
