Title: El uribismo sigue culpando a 'las FARC' de lo que pasa en Colombia
Date: 2020-01-29 19:34
Author: CtgAdm
Category: Paz, Política
Tags: acuerdo de paz, Ministra de Interior
Slug: el-uribismo-sigue-culpando-a-las-farc-de-lo-que-pasa-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Min.-Interior-sobre-FARC-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @RevistaSemana {#foto-revistasemana .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/carlos-losada-integrante-farc-sobre-acuerdos_md_47283594_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Carlos Lozada | Integrante FARC

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

En el marco del Foro Colombia 2020, organizado por la revista Semana, la ministra de interior Nancy Patricia Gutiérrez dijo que "el acuerdo con las FARC diría en lo personal que es semifallido", culpando al ahora partido político de ello. En un comunicado, el partido le recordó a la Ministra que las FARC no existen, y por lo tanto, **seguir en la lógica de culparlos por los problemas que enfrenta el país no tiene sentido.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1222475689724665856","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1222475689724665856

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **La continuidad de los asesinatos de líderes sociales y excombatientes, "eso sí puede llamarse gobierno fallido"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador por el partido FARC Carlos Lozada afirmó que este Gobierno llegó con el propósito de impedir la implementación del Acuerdo de Paz y por eso, **su política de Paz con Legalidad ha girado en torno a una visión reduccionista del Proceso**, que lo limita únicamente a la reincorporación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, declaró que no eran extrañas las palabras de la Ministra, siendo una persona cercana al 'ala dura' del Centro Democrático, y una de las lobbistas en el Congreso de las objeciones a la Jurisdicción Especial para la Paz (JEP) entre otros proyectos contrarios a lo acordado en La Habana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, Lozada explicó que la declaración de Gutiérrez por una parte busca contradecir lo que quiere mostrar el Gobierno, respecto a una implementación positiva e integral del Acuerdo; y por otra parte, quiere ambientar la idea de los incumplimientos por parte de FARC "para desarrollar una ofensiva jurídica y política para hacer fracasar el proceso y Acuerdo".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el comunicado, FARC añadió que, en cambio, **sí podía llamarse "Gobierno fallido" a la acción del Estado respecto al constante asesinato de líderes sociales** y de firmantes del Acuerdo, así como a la actuación impune de múltiples bandas armadas en el territorio. (Le puede interesar: ["En Colombia han sido asesinados 170 excombatientes tras la firma del Acuerdo de Paz"](https://archivo.contagioradio.com/en-colombia-han-sido-asesinados-170-excombatientes-tras-la-firma-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Para detener la violencia es necesaria una paz completa**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tal como lo reseñaba al final del Comunicado de FARC, las declaraciones de la Ministra se produjeron el mismo día en que asesinaron al reincorporado Darío Herrera en Ituango. El quinto firmante de la paz que ha perdido la vida por la violencia en 2020. (Le puede interesar: ["Voces difieren de las declaraciones de ONU sobre implementación del acuerdo de paz"](https://archivo.contagioradio.com/voces-difieren-de-las-declaraciones-de-onu-sobre-implementacion-del-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1222532265013661703","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1222532265013661703

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Respecto de esta situación, que Lozada planteó como "una de las amenazas más grandes que tiene el Acuerdo", aseguró que nunca podrá detenerse la violencia hasta que no se culmine el proceso de paz con el Ejército de Liberación Nacional (ELN), se sometan a la justicia las bandas sucesoras del paramilitarismo y se implemente de forma integral lo acordado en La Habana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lozada aseveró que no es suficiente con que se sepan quienes son los responsables de los asesinatos, en su lugar, se deben tomar las medidas necesarias para detener un problema que para ellos es sistemático, y responde también a la ausencia del monopolio de las armas a cargo del [Estado](https://www.partidofarc.com.co/es/actualidad/que-duque-comience-gobernar-715).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **¿Cómo evalúa FARC el proceso de paz?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por último, respecto a la evaluación que hace el partido frente a la implementación del Acuerdo, Lozada sostuvo que la misma no podía limitarse a presentar porcentajes de avances sobre lo que se encuentra estrictamente en el texto. (Le puede interesar: ["Solo el 15% de instancias creadas para cumplir el Acuerdo de Paz están funcionando"](https://archivo.contagioradio.com/solo-el-15-de-instancias-creadas-para-cumplir-el-acuerdo-de-paz-estan-funcionando/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En su lugar, preferían hacer la evaluación política sobre su potencial transformador en la sociedad, en cuyo caso, sostuvo que "el resultado más tangible es esa formidable movilización social y popular que demuestra que **ese Acuerdo dejó de ser entre FARC y el Estado, y cada día más sectores piden su cumplimiento"**, tanto como otras reivindicaciones.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
