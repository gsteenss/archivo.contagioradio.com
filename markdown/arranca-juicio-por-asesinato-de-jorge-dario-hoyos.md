Title: Arranca juicio por asesinato del líder sindical  Jorge Darío Hoyos
Date: 2015-02-02 17:27
Author: CtgAdm
Category: Judicial, Política
Tags: Paramilitarismo, Sindicalismo, UP
Slug: arranca-juicio-por-asesinato-de-jorge-dario-hoyos
Status: published

###### Foto: pulitzercenter.org 

##### [Yessika Hoyos, hija de Jorge D. Hoyos]: <iframe src="http://www.ivoox.com/player_ek_4028652_2_1.html?data=lZWfmpuZdo6ZmKiakp2Jd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjL7S1djNr8KfqdTm0diRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

Homicidio y tentativa de homicidio agravado en concurso con concierto para delinquir son los cargos que afronta el **suboficial de inteligencia del Ejército Fredy Francisco Espitia Espinosa**, asesor de comando en la escuela de Inteligencia **Brigadier Charry Solano**, por el asesinato del sindicalista e integrante de la Unión Patriótica, UP, **Jorge Darío Hoyos, asesinado en Fusagasugá el día 3 de marzo del 2001.**

Este caso, que es considerado de alta importancia histórica, porque **permitiría esclarecer la verdad de la persecución contra el movimiento sindical en Colombia**, lleva 14 años en la impunidad. Para Yessika Hoyos, hija de Jorge Darío, han sido muchos años de búsqueda de la verdad y de justicia y la etapa que se inicia hoy **es un momento de mucha expectativa y de esperanza.**

Yessika Hoyos, hija de Jorge Darío asegura que la solidaridad ha sido un motor fundamental en su compromiso por la defensa de los DDHH. “no he estado sola, he tenido el acompañamiento del Cajar, del MOVICE y del movimiento HIJOS” y además ha orientado su vida a la representación de las miles de víctimas de la estrategia paramilitar.
