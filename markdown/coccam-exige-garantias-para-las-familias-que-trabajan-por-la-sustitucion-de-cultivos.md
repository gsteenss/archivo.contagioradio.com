Title: Desconocimiento e incumplimiento del Gobierno con la sustitución de cultivos de uso ilícito
Date: 2020-01-29 17:27
Author: CtgAdm
Category: Nacional, Paz
Tags: campesinos, cultivos, Cultivos de coca, sustitución
Slug: coccam-exige-garantias-para-las-familias-que-trabajan-por-la-sustitucion-de-cultivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/cultivos_de_coca_-e1470250623934.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Zona Cero

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/deivin-hurtado-sobre-incumplimiento-gobierno-frente-a_md_47057335_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Deivin Hurtado | COCCAM

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

En los últimos meses se ha registrado el asesinato de 59 líderes cocaleros en zonas donde se ejecutan proyectos de erradicación de cultivos de uso ilícito; Hernando Londoño, director del Programa Nacional de Sustitución de Cultivos de Uso Ilícito (PNIS), afirmó públicamente el 24 de enero que *"líderes de sustitución de cultivos no han sido asesinados por pertenecer a esos proyectos, sino por otro tipo de razones , como hacer negocios de la misma hoja de coca"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmación contraria a los registros de organizaciones como la **Coordinadora Nacional de Cultivadores de Coca Amapola y Marihuana (COCCAM)** quienes han documentado diferentes tipos de agresiones hacia las personas que hacen parte de estos proyectos de erradicación.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante estas declaraciones, **Deivin Hurtado integrante de COCCAM**, señaló que desde hace un tiempo se viene planteando desde la organización la necesidad de hacer una asamblea que permita determinar el impacto de los comentarios que han surgido desde Gobiernos anteriores hasta la administración actual en torno a este tema, *"El Gobierno desconoce todas las actividad que viene haciendo encaminadas en el tema de sustitución y ante ello debemos actuar"*, indicó Hurtado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Deivin Hurtado agregó también que el "*Gobierno no tiene una disponibilidad real de poner realmente los Acuerdos en su linea de trabajo **,** porque no se ha venido avanzando en un beneficio de las comunidades"*, resaltó además la preocupación que surge en la comunidad al ver el desconocimiento no solo del director del PNIS sino de toda la institucionalidad ante *"la situación tan grave que viven los líderes sociales y representantes de los territorios".*

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Líderes que según Hurtado *"han perdido su vida apostándole a la sustitución de cultivos de uso ilícito, el cumplimiento de los acuerdos y a la paz"*, de igual forma señaló que el Gobierno está desconociendo los liderazgos de las personas que están siendo asesinadas en el país. (Le puede interesar: <https://www.justiciaypazcolombia.com/nos-siguen-matando-masacre-en-taraza/> )

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para Hurtado, una de las grandes problemáticas que surgen de esta falta de atención es el desconocimiento de algunos tipos de liderazgo, *"hay lideres más reconocidos por su dinámica departamental o institucional, pero también hay unos en los corregimientos y veredas, quienes hacen el trabajo mas complejo porque es donde la violencia esta más fuerte, y son estos los que no son reconocidos"*.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Líderes del programa, que estén en las directivas de la Coordinadora de Cultivadores de Coca, Amapola y Marihuana (Coccam) o que sean miembros de los Consejos Asesores Territoriales (CAT) no han sido asesinados"
>
> <cite> Hernando Londoño , Director del programa de sustitución | El Espectador</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El vocero agregó también que COCCAM entrega periódicamente un informe que señala las acciones que hacen los líderes en la comunidad, trabajo que según él ha sido desconocido mediante una "*dinámica institucional donde se intenta desconocer el liderazgo de los lideres asesinados, acción que dificulta aún más nuestro trabajo".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Avances sin cumplimientos

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por otro lado Hurtado destacó que **el municipio donde más se avanzó en el tema de la sustitución en el Cauca fue Miranda**, **allí aproximadamente 400 familias se vincularon al programa**, donde a pesar de la iniciativa de las comunidades *"el Gobierno pagó solo 2 mesadas de las 12 pactadas, esto pasa en el municipio donde más se ha avanzado en materia de cumplimiento, ahora imaginen el resto"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Situación que afecta los avances en la comunidad, y que según el representante evita que muchas familias decidan acogerse a los programas debido al incumplimiento, *"Por un lado dicen que tienen voluntad y por otro lado están rociando glifosato"*. (Le puede interesar: <https://archivo.contagioradio.com/un-dialogo-sobre-el-impacto-del-conflicto-armado-en-las-poblaciones-campesinas/>)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Ellos no pelean por ser ricos, pelean por sobrevivir"***
>
> <cite> Deivin Hurtado </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Reflejo de esto fueron las cifras entregadas en 2019 por la Oficina de las **Naciones Unidas contra la Droga y el Delito** (UNODC), donde indican que **6.350 familias en 29 municipios encuentran en estos cultivos su forma de supervivencia**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo según el Centro de Investigación y Educación Popular (CINEP) en un informe del 2019 , señalaron que en el periodo entre 2017 y 2018 , han sido asesinados 16 líderes pertenecientes al Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS), esto solo en el departamento de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Allí mismo según el CINEP 40 perdonas han sido desplazados debido al interés de grupos paramilitares como l**os Caparrapos, las Autodefensas Gaitanistas de Colombia, AGC** por perpetuar y mantener el control de la cadena de producción de cocaína en el departamento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente Hurtado afirmó que *"las comunidades siempre han estado trabajando por el tema de la sustitución y no para generar acciones de aspersión con glifosato que pone en riesgo la salud y la vida de sus familias"*, y alertó que de continuar estos incumplimiento seguirán los desplazamientos y posteriormente se generarán nuevas zonas de cultivo de uso ilícito.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Muchas veces las siembras disminuyen en una zona pero los cultivos se concentran en otras, porque lo que necesitan las familias es comer y por eso de desplazan y cultivan en otros lados, ésta lastimosamente es la única forma que encuentran para subsistir en zonas tan apartada y olvidadas por el Estado".*
>
> <cite> Deivin Hurtado </cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
