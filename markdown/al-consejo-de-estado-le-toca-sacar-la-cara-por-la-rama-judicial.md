Title: Al Consejo de Estado le toca sacar la cara por la rama judicial
Date: 2017-10-02 13:32
Category: Mision Salud, Opinion
Tags: Acceso a Salud, Ministerio de Salud
Slug: al-consejo-de-estado-le-toca-sacar-la-cara-por-la-rama-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/consejo-de-estado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Universidad Sabana 

#### Por [[@AndreaCReyesR]](https://twitter.com/AndreaCReyesR)[ de][[Misión Salud]](https://twitter.com/MisionSaludCo) 

Necesitamos que el cumplimiento del derecho fundamental a la salud pase del azar a lo seguro. Y para ello estamos todos llamados a asumir la responsabilidad de velar por nuestra propia salud y la de nuestro entorno, tanto en lo individual como en lo colectivo. Aparte del grano de arena que nos corresponde aportar a cada uno, hoy el Consejo de Estado tiene en sus manos una decisión monumental.

[El][[Decreto 1782 de 2014]](https://www.minsalud.gov.co/Normatividad_Nuevo/Decreto%201782%20de%202014.pdf)[ expedido por el Gobierno Nacional, que da respuesta a la urgencia social de contar con medicamentos biotecnológicos que sean de calidad y con precios que podamos pagar, fue demandado ante el Consejo de Estado a comienzos del año por el gremio de la industria farmacéutica multinacional, Afidro. La demanda acaba de ser admitida y en el transcurso de los próximos días se decidirá si mientras esta se resuelve (proceso que puede tardar de meses a años) se suspende provisionalmente el Decreto. Es decir, el Consejo de Estado tiene que decidir si se mantiene vigente lo ya logrado como país para tener medicamentos biotecnológicos de calidad y con precios asequibles o se suspende mientras  decide sobre la demanda.]

[La decisión que tiene en sus manos el Consejo de Estado, es tan importante como sencilla. Suspender temporalmente el Decreto 1782 de 2014 (específicamente el artículo 9, que es el que ha sido demandado) significa que durante ese tiempo el país renuncia al ahorro de miles de millones de pesos anuales, que fue justamente lo que impulsó su expedición. Para hacernos una idea: en sólo 8 medicamentos][[el ahorro estimado por el Ministerio de Salud es de 300.000 a 600.000 millones de pesos anuales]](https://www.elespectador.com/noticias/salud/biotecnologicos-en-manos-del-consejo-de-estado-articulo-714503)[. Sin duda a todos se nos ocurren necesidades básicas en salud urgentes en las cuales invertir ese dinero.]

[Eso no es todo. Lo más valioso del artículo 9 del Decreto es que genera el mecanismo para que como país nos ahorremos ese dinero sin que sacrifiquemos la salud. El Gobierno expidió este Decreto porque sabe responsablemente que el avance de la ciencia, la rigurosidad técnica y la larga experiencia mundial que se tiene con los medicamentos biotecnológicos permiten tener alternativas de calidad y bajo precio. La impecable argumentación técnica que respalda esta decisión es de dominio público y no se nos puede olvidar que fue fruto de un proceso legítimo de cerca de 10 años entre todos los actores interesados.]

[¿Qué pasa entonces? Quien está demandando al Gobierno es el gremio en Colombia de una de las industrias más poderosas del mundo, cuyo interés principal es comercial: asegurar que sus afiliados mantengan el monopolio farmacéutico de estos medicamentos con sus astronómicos ingresos. Esto puede generar susto en quienes tomarán la decisión, pero el país los respalda en dar a Afidro su justa dimensión. Afidro es solamente uno de los múltiples actores que formamos parte del sistema de salud. Solo uno. Ni más ni menos. Y seguro los demás actores del sistema, incluidos los 49 millones de colombianos restantes y miembros de la industria farmacéutica multinacional que no se sientan representados por esta jugada contra el interés público, coincidimos en que sería una tragedia nacional suspender el mecanismo que en este momento permite aumentar el acceso a medicamentos vitales y a la vez mantener la viabilidad del sistema de salud.]

[En tiempos en que el país requiere que la rama judicial supere las sombras de la corrupción, que el Consejo de Estado priorice la salud sobre el lucro manteniendo vigente el artículo 9 del Decreto se constituye en la muestra de que la rama judicial aún puede volver a ocupar dignamente el rol que los colombianos le hemos encargado de velar por la justicia.]

#### **[Leer más columnas de Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 
