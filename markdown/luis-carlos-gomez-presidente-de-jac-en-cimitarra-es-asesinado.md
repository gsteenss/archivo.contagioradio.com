Title: Luis Carlos Gómez presidente de JAC en Cimitarra es asesinado
Date: 2020-07-30 20:11
Author: CtgAdm
Category: Actualidad, Líderes sociales
Slug: luis-carlos-gomez-presidente-de-jac-en-cimitarra-es-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Asesinato-lider-indigena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 30 de julio sobre las 2 de la tarde **fue asesinado Luis Carlos Gómez,** presidente de la Junta de Acción Comunal, de la vereda Aterrado, ubicada **en el municipio de Cimitarra, Santander.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según información recopilada por testigos de la comunidad **los hechos se presentaron en cercanías del Batallón Cajibio**, el cual se encuentra ubicado en inmediaciones de la vereda, el líder comunal fue agredido con arma de fuego en la estación Cantimplora luego de que saliera de su trabajo. (Le puede interesar leer: [Para proteger a los líderes hay que pasar de voluntad a hechos: Diakonia](https://archivo.contagioradio.com/para-proteger-a-los-lideres-hay-que-pasar-de-voluntad-a-hechos-diakonia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma personas cercanas a Luis Carlos Gómez, señalaron que este no tenía amenazas en su contra, por lo tanto las razones de su asesinato aún son motivo de incertidumbre y miedo al interior de la comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte los líderes comunales de la zona, reclaman fortalecimiento en las [garantías de sus funciones](https://www.justiciaypazcolombia.com/a-pesar-de-las-reiteradas-denuncias-de-la-unipa-ocurre-nueva-masacre-en-el-pueblo-indigena-awa/)y agregan que esté no es la primera situación de asesinato contra los liderazgos del territorio, señalando los recientes homicidios en el corregimiento de Puerto Olaya, donde dos líderes de la comunidad fueron objeto también de ataques armados.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
