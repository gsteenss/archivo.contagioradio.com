Title: DAS no solo chuzaba, también amenazaba, torturaba y asesinaba
Date: 2015-02-03 16:58
Author: CtgAdm
Category: Entrevistas, Judicial
Tags: Actividades ilegales del DAS, das, Maria del Pilar Hurtado
Slug: das-no-solo-chuzaba-tambien-amenazaba-torturaba-y-asesinaba
Status: published

##### [Reynaldo Villalba, abogado CAJAR ]<iframe src="http://www.ivoox.com/player_ek_4033209_2_1.html?data=lZWglZeUfY6ZmKiak5yJd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiKLHjNPcjdjTsNCfxM3i3MbGpYampJDhw9LGrYa3lIqum9OPpc7Zz8bnw8fFaZO3jNnc1NnZtsLWwpDmjcbXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

"Es muy importante que la señora Hurtado cuente los **detalles de lo que ocurría en el DAS, que cuente todo aquello que las investigaciones no han revelado**. Hay situaciones que aún ni sospechamos los colombianos." y otras que son sabidas como las operaciones de **tortura, persecución y asesinatos**, señala el abogado Reynaldo Villalba.

Para el abogado Villalba, los procesos de investigación que se han abierto en torno a las operaciones ilegales del DAS (al rededor de 15), se han desarrollado en torno a las interceptaciones, con condenas por **"concierto para delinquir" y "abuso de autoridad",** pero no frente a otro tipo de delitos cometidos por esta entidad, como **amenazas, torturas y homicidio**. La mayoría  de los acusados se han acogido a sentencia anticipada, **aumentando los niveles de impunidad en el caso del DAS**.

<div>

Para Villalba, si bien es cierto que el plan de persecución provino de Casa de Nariño, y "**es clarísima la responsabilidad de Uribe Vélez**", las declaraciones de Maria del Pilar Hurtado deben permitir que avancen los procesos jurídicos y que se avance en las investigaciones por los demás crímenes cometidos por ese organismo.

Villalba recuerda que hay muchas **más figuras políticas cercanas al entorno del ex presidente Uribe, como el caso del senador Jose Obdulio Gaviria y el propio Bernardo Moreno,** que tienen altos niveles de responsabilidad que hasta el momento no se han establecido.

Sin embargo, en opinión de Villalba, el posible avance de los procesos al interior de la Corte Suprema de Jusiticia, tendrá mucho que ver con la **voluntad política en la Fiscalía General de la Nación** que será la encargada de orientar las investigaciones para el esclarecimiento de las responsabilidades en los crímenes que ya están probados.

</div>
