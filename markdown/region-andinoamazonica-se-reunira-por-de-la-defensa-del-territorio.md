Title: Región Andinoamazónica se reunirá por de la defensa del territorio
Date: 2017-08-04 18:13
Category: Ambiente, Nacional
Tags: Putumayo, Región Andinoamazónica
Slug: region-andinoamazonica-se-reunira-por-de-la-defensa-del-territorio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Imagen1ttt-e1501885667449.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Instituto del Bien Común] 

###### [04 Ago 2017] 

Este fin de semana se realizará en Putumayo el Foro Minero- energético y Ambiental por la defensa de la región Andinoamazónica. Una iniciativa que tiene como objetivo compartir las experiencias de **las comunidades y plantear alternativas y mecanismos para la defensa del territorio y el ambiente**.

Yuli Artunduaga, coordinadora del Comité de Mujeres Andinoamazonicas, indica que el foro servirá para denunciar problemáticas que afrontan comunidades como la de Puerto Leguizamo, que ha quedado en el olvido estatal y que ahora con **la finalización del conflicto armado, enfrenta los intereses de multinacionales que pretenden realizar exploración y explotación petrolera en esa zona.**

De acuerdo con los habitantes, en el corregimiento de Puerto Ospina se está intentado desarrollar el proyecto **Pup 13 que afectaría a comunidades indígenas, campesinas y afro, y que además no han sido consultadas previamente**.  Artunduaga denuncia que Puerto Leguizamo sería el único municipio del Putumayo que no haría parte de las concesiones actuales para realizar explotación y exploración de petrolera.

Se espera que los delegados que asistan al evento **logren unificar criterios en términos de la activación de mecanismos como consultas populares**, entre otros, que impidan que en los territorios se atente contra la vida y el ambiente. (Le puede interesar: ["Camunidades del Putumayo denuncian persecución judicial")](https://archivo.contagioradio.com/comunidades-del-putumayo-denuncian-persecucion-judicial/)

Durante los dos días del foro se tratarán temas como el modelo de desarrollo minero-energético; los proyectos extractivistas que hay en la región andinoamazónica; **la tierra y el territorio; las comunidades y sus derechos ante los proyectos de exploración y explotación minera; y la consulta popular**.

<iframe id="audio_20174626" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20174626_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
