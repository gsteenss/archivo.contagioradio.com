Title: En Gobierno Duque han ocurrido 1.200 violaciones de DDHH contra el pueblo Awá en Nariño
Date: 2020-08-20 15:57
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Asesinato de indígenas, nariño, ONIC, pueblo Awá
Slug: en-gobierno-duque-han-ocurrido-1-200-violaciones-de-ddhh-contra-el-pueblo-awa-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Comunidad-Indígena-Awá-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Comunidad Indígena Awá / Minga

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La Comisión Nacional de Derechos Humanos de los Pueblos Indígenas informó la noche del pasado 18 de agosto el asesinato de tres jóvenes indígenas del Pueblo Awá, de **la organización indígena Camawarí del resguardo Pialapí Pueblo Viejo,** en Ricaurte Nariño. En 2009 la Corte constitucional se pronunció a través del auto 004 advirtiendo que 34 pueblos indígenas estaban en riesgo de extinción, entre ellos el pueblo Awá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los hechos se dan en medio de una serie de denuncias realizadas por el pueblo Awá, que ha advertido cómo la violencia incrementó en medio del aislamiento en sus territorios ubicados en los municipios de **Cumbal, Santa Cruz de Guachavez, Mallama, Ricaurte, Barbacoas, Roberto Payán, Tumaco e Ipiales, en el departamento de Nariño**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El alcalde de Ricaurte, Eder Burgos informó que la nueva masacre ocurrió en el sector El Aguacate” a nueve horas de camino del casco urbano, **"nos preocupa la situación que viene afectando a estas comunidades por la intensificación del conflicto, no encontramos las garantías como ciudadanos de este país para que nos permitan vivir dignamente en los territorios"**, expresó. [(Le recomendamos leer: Atentan contra la vida del líder Awá, Javier Cortés Guanga en Nariño)](https://archivo.contagioradio.com/atentan-contra-la-vida-del-lider-awa-javier-cortes-guanga-en-narino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras a tempranas horas del día se informó que una comisión de verificación se transportaba hasta el lugar de los hechos, el alcalde señaló que las autoridades se encontraban realizando el levantamiento de cuerpos para llevarlos hasta el casco urbano, mientras se planea realizar un consejo de seguridad para escuchar a las autoridades indígenas frente a los hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Horas más tarde, Miguel Caicedo Guanga, gobernador del resguardo Pialapi Pueblo Viejo, **Camawari** Awá confirmó que las personas asesinadas fueron identificada como Lumar Leonel Guanga, John Guanga y Eider Sebastián Guanga, cuyas edades oscilaban entre los 18 y 24 años [(Le puede interesar: Asesinan al líder afrocolombiano Patrocinio Bonilla)](https://archivo.contagioradio.com/asesinan-al-lider-afrocolombiano-patrocinio-bonilla/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asegura que encontraron los cuerpos en estado de descomposición pues al parecer habrían sido asesinados hace 10 días, agregó que no recibió información previa sobre amenazas hacia los jóvenes. y dijo que la comunidad no quiere hablar sobre lo sucedido pues la situación ha intimidado a los habitantes de El Aguacate.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "No podemos estar reportando todos los días muertos por cuestiones de la confrontación armada entre grupos al margen de la ley"
>
> <cite>Eder Burgos</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Violencia contra comunidad Awá va en ascenso

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Desde hace meses los resguardos del pueblo Awá han señalado que **mientras hacen lo posible por cumplir con la medida de aislamiento, grupos armados han puesto en la mira a líderes y lideresas y a la población en general,** el primer hecho en medio de la cuarentena se dió el 24 de marzo cuando hombres armados intimidaron a quienes pasaban frente al centro educativo **“Los Telembies”** en el corregimiento de Buena Vista, realizando disparos en la zona, donde se encuentran comunidades y familias Awá en situación de desplazamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Aunque la comunidad Awá exigió a los diversos grupos armados legales e ilegales que respeten la vida de sus habitantes, reiterando que se abstengan de entrar a sus territorios indígenas** no solo por la actual pandemia sino en respeto de su autonomía, el 26 de marzo en Barbacoas, Nariño sería asesinado Wilder García, indígena Awá perteneciente al Resguardo de Tortugaña Telembí.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el mes de julio en cercanías al corregimiento de Llorente cerca a Tumaco, fue asesinado Rodrigo Salazar, de 44 años quien se desempeñaba como gobernador suplente del Resguardo Indígena Awá de Piguambi Palangala un hecho que fue seguido de un ataque al Resguardo Cuasbíl la Faldada en Barbacoas, donde armados atacaron una vivienda dejando a cuatro personas heridas, entre ellas una mujer menor de edad y otra en estado de embarazo. [(Le recomedamos leer: Rodrigo Salazar, reconocido líder del Pueblo Awá fue asesinado en Llorente)](https://archivo.contagioradio.com/rodrigo-salazar-reconocido-lider-del-pueblo-awa-fue-asesinado-en-llorente/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hechos más recientes ocurridos el 29 de Julio dan cuenta de un grupo armado que irrumpió en la vivienda del exgobernador, **Fabio Alfonso Guanga García**, y **lo asesinaron ante la mirada de sus familiares a escasos metros de su residencia**.

<!-- /wp:paragraph -->

<!-- wp:heading -->

La ONIC alerta sobre crisis humanitaria
---------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

"En los dos años de mandato del presidente Duque constatamos con extrema preocupación cómo los grupos armados ilegales han exacerbado la barbarie en contra de las comunidades y en especial contra los pueblos indígenas", manifestó la Organización Nacional Indígena de Colombia tras conocerse los hechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La ONIC ha advertido que estos sucesos ocurren en medio de un conflicto en el cual participan “quince grupos al margen de la ley”, incluidas las disidencias de las FARC,  las autodenominadas Autodefensas Gaitanistas de Colombia el E-30 Franco Benavides, Los Nuevos Delincuentes, La Gente del Nuevo Orden, Los Contadores y el ELN, grupos que se disputan el territorio y el control sobre los cultivos de uso ilícito. [(Lea también: ONIC denuncia intento de masacre contra comunidad indígena Awá)](https://archivo.contagioradio.com/onic-denuncia-intento-de-masacre-contra-comunidad-indigena-awa/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según el registro del Observatorio de Derechos Humanos de la ONIC, durante el actual Gobierno y con corte al 12 de julio **han ocurrido 1.200 afectaciones a los derechos individuales y colectivos del pueblo Awá.** Hechos por los que exigen al Gobierno y la Fiscalía que adelanten las laborses que permitan dar con los responsables y la comunidad internacional que inste a la protección de los pueblos ancestrales del país. [(Lea también: Pueblo indígena Awá fue víctima de una nueva masacre)](https://archivo.contagioradio.com/pueblo-indigena-awa-fue-victima-de-una-nueva-masacre/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, organizaciones como la [Comisión Interétnica de la verdad del Pacífico](https://twitter.com/VerdadPacifico) condenar el hecho ocurrido contra el pueblo indígena Awá y resaltó que "el Pacífico continuará trabajando por esclarecer la verdad para que avancemos hacia la paz". [(Le puede interesar: Tras asesinato de indígena, Pueblo Awá exige garantías en medio de las balas y el Covid-19)](https://archivo.contagioradio.com/tras-asesinato-de-indigena-pueblo-awa-exige-garantias-en-medio-de-las-balas-y-el-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe señalar que el hecho se registró menos de 72 horas después del asesinato de 8 jóvenes en el municipio de Samaniego, al occidente del departamento. [(Lea también: Nueva masacre, en Nariño fueron asesinados 9 jóvenes)](https://archivo.contagioradio.com/nueva-masacre-en-narino-fueron-asesinados-9-jovenes/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
