Title: Argentinos reclaman verdad y justicia en caso de Santiago Maldonado
Date: 2017-08-30 13:43
Category: DDHH, El mundo
Tags: Argentina, desaparecidos, dia internacional del desparecido, santiago maldonado
Slug: argentinos-reclaman-verdad-y-justicia-en-caso-de-santiago-maldonado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/santiago_maldonado-e1504118532468.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: TN.com] 

###### [30 Ago 2017] 

Se completan **3 semanas de la desaparición  del joven argentino Santiago Maldonado**. Allí, continúan exigiéndole al gobierno y a las autoridades que se avance en la búsqueda pues no quieren revivir lo que ellos llaman “la muerte blanca”.

Según José Schulman, miembro de la Liga Argentina por los Derechos Humanos, el Día Internacional de los Desaparecidos **es una fecha especial en Argentina que está ligada con la historia de las desapariciones** ocurridas en la dictadura de los años 70 conocidas como “la muerte blanca”.

Es por esto que la desaparición de Santiago Maldonado ha sido un hecho que ha reunido la indignación y solidaridad de los ciudadanos argentinos. Para que se continúen los procesos de búsqueda, Schulman indicó que **presentarán una denuncia penal para hacer constancia de los encubrimientos** de los funcionarios de la Gendarmería y la presidencia. (Le puede interesar: ["Unidad de Búsqueda tendrá que encontrar a más de 50.000 desaparecidos"](https://archivo.contagioradio.com/unidad-de-busqueda-tendra-que-encontrar-a-mas-de-50-000-desaparecidos-en-el-pais/))

El día de hoy, cuando se celebra el Día Internacional del Desaparecido habrá una **jornada internacional para que las personas acudan a las embajadas de Argentina**, “al Gobierno le molesta que se dañe su imagen por lo que la solidaridad es muy útil, necesitamos saber dónde está Santiago Maldonado”.

<iframe id="audio_20609547" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20609547_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
