Title: ZIDRES tendrían que ser consultadas con comunidades
Date: 2018-02-23 16:11
Category: DDHH
Tags: Acumulacion de tierras, campesinos, Zidres
Slug: zidres-ponen-en-riesgo-la-labor-de-mas-de-13-millones-de-campesinos-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Despojo-de-tierras-e1518786329996.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Protectionline] 

###### [23 Feb 2018] 

El último documento compes, firmado por el gobierno de Juan Manuel Santos, prendió las alarmas para las comunidades y los campesinos del país, porque aprueba la destinación de 7 millones de hectáreas para la ley ZIDRES, hecho que profundizaría la brecha de la acumulación de tierras en el país y que atentaría **contra la actividad agrícola que desarrolla en el territorio más de 13 millones de campesinos**.

Jennifer Mojica, sub directora de la Comisión Colombiana de Juristas las ZIDRES, esa destinación afectaría la demanda que realizan aproximadamente **13 millones de familias campesinas que necesitan con urgencia que se realice una redistribución** de las tierras en el país para poder cultivar. Además, afirmó que los 7 millones de hectáreas que se destinarían para ZIDRES superan la cantidad de tierra actualmente usada para actividades agrícolas.

El último informe de OXFAM, reveló que en **Colombia el 1% de la población ocupa más del 80% de las tierras**, mientras que el 99% de los cultivos agrícolas que existen solo ocupan el 19% de la superficie, situación que podría profundizarse con la destinación de las hectáreas porque no serían entregadas a los pequeños cultivadores, sino a los grandes proyectos de agroindustria. (Le puede interesar: ["El 1% de propietarios ocupa más del 80% de las tierras en Colombia"](https://archivo.contagioradio.com/el-1-de-propietarios-ocupa-mas-del-80-de-tierras-en-colombia/))

### **Las ZIDRES podrían atentar contra los derechos humanos** 

Mojica manifestó que la investigación que sustenta la destinación de los 7 millones de hectáreas a ZIDRES, iría en contra de los reparos de la Corte Constitucional y del desarrollo de las comunidades, por tres razones:

En primera medida, la Corte Constitucional señaló que las ZIDRES no es una ley de tierras, por lo tanto, no puede afectar los derechos territoriales que tienen las comunidades. El **gobierno primero debe garantizarle a las poblaciones campesinas, indígenas y campesinas las demandas** que han hecho sobre tierras, señaló la jurista.

La segunda medida es que no se pueden hacer ZIDRES en zonas donde haya existido desplazamiento forzado, abandono de tierras por el conflicto armado o despojos, primero debe hacerse la restitución de tierras a las poblaciones afectas y después ZIDRES. Sin embargo, los **7 millones de hectáreas están principalmente en la Altillanura, Córdoba, Antioquia, Bolívar, Sucre y Magdalena**, todos departamentos fuertemente golpeados por la guerra.

El gobierno debe consultar las normas de ordenamiento territorial de los municipios y posteriormente proceder a analizar la implementación o no de las mismas. De acuerdo con Mojica, **ninguna de las tres medidas es tenida en cuenta por el documento COMPES**, dejando a los campesinos y las comunidades en extrema vulnerabilidad.

Mojica aseguró que las ZIDRES no aportan en nada al desarrollo social ni económico de las comunidades, “es solo la forma en la que se ha intentando disimular una herramienta para la concentración de tierras y para dar seguridad jurídica a esas grandes inversiones en extensiones” y agregó que con la misma se **busca legalizar todas las concentraciones, despojos de tierras y expropiaciones que hay en Colombia.**

### **Los riesgos de la ley ZIDRES** 

Para Jennifer Mojica, la ley ZIDRES sitú**a a las comunidades en riesgo de nuevos desplazamientos forzados**, “lo que viene ahorita en los territorios es que empiezan a aparecer compradores, sin que las personas estén vendiendo, lo que empieza a ocurrir es que llegan tramitadores de tierras, finca a finca, seguramente con sumas no despreciables, pero hay que dudar cuando no se está vendiendo y se quiere comprar”.

Igualmente afirmó que el mercado de tierras es muy agresivo y su primer comportamiento es hacer ofrecimiento de tierras positivas, sin embargo, señaló que a quienes se resisten a vender se les somete a amenazas y presiones que pueden llegar al punto crítico de violaciones de derechos humanos. (Le puede interesar: ["Ley ZIDRES profundiza afectaciones a mujeres rurales"](https://archivo.contagioradio.com/ley-zidres-profundiza-afectaciones-las-mujeres-rurales/))

### **Las opciones que le quedan al campesinado** 

Frente a esta situación, una de las posibles situaciones que podría re abrir el debate sobre la acumulación de tierras en el país y las ZIDRES, son los debates e implementación de los Acuerdos de paz, sobre el primer punto que tiene que ver con tierras y en el que se destinaba un banco de tierras para el campesinado y víctimas del conflicto armado.

El paso siguiente en la implementación de las ZIDRES con las consultas municipales, escenarios en donde, según ella, **las comunidades deben manifestar si quieren o no que su territorio** se declare como ZIDRE. De igual forma las poblaciones afro e indígenas debe exigir que se implemente con ellos el mecanismo de consulta previa.

<iframe id="audio_24002103" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24002103_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
