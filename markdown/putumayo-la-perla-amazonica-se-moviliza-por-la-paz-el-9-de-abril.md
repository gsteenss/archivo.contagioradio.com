Title: Putumayo: La Perla Amazónica se moviliza por la paz el 9 de Abril
Date: 2015-04-10 11:34
Author: CtgAdm
Category: Comunidad
Tags: 9 de Abril Putumayo por la paz, ADISPA por la paz 9 de Abril, Perla Amazonica por la paz 9 de Abril
Slug: putumayo-la-perla-amazonica-se-moviliza-por-la-paz-el-9-de-abril
Status: published

###### Foto:Contagioradio.com 

El día 8 de abril desde las 10 am cientos de campesinos de toda la región del **Putumayo y Jardines de sucumbido de Ipiales Nariño** nos dimos cita en cada uno de nuestros municipios para iniciar la **Gran Marcha por la Paz** que se llevara a cabo en la ciudad de Neiva, departamento del Huila el día de 9 de abril, además informamos que hicimos un parada en Mocoa, capital del departamento del Putumayo para hacer el lanzamiento del **Frente Amplio por La Unidad ,la Democracia y la Paz con Justicia Social**, el cual se conforma para salir de la crisis política que esta sufriendo nuestra región.

La Zona de Reserva Campesina del corregimiento Perla Amazónica, municipio de Puerto Asís, departamento del Putumayo hace presencia en la **Gran Marcha por la Paz** con justicia social , que se‏ ha llevado a cabo en la ciudad de **Neiva**.

En total más de **5000 personas han marchado** en la capital del Huila, la **Zona de Reserva Campesina Perla Amazónica** ha participado con mas de **200 personas** que continúan luchando por la defensa de la vida, el territorio y la biodiversidad‏.
