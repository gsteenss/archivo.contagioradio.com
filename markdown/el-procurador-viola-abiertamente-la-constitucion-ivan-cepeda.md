Title: El Procurador viola abiertamente la Constitución: Iván Cepeda
Date: 2016-05-18 15:11
Category: Entrevistas, Nacional
Tags: plebiscito por la paz, proceso de paz, Procurador Alejandro Ordoñez, refrendación acuerdos
Slug: el-procurador-viola-abiertamente-la-constitucion-ivan-cepeda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/procurador_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El País ] 

###### [18 Mayo 2016]

De acuerdo con Iván Cepeda, Senador del Polo Democrático, **el Procurador Ordoñez, viola abiertamente la constitución** al asegurar que los funcionarios públicos que hagan campaña al plebiscito pueden ser investigados. Conforme el artículo 22, **la paz es un derecho de los colombianos y un deber de obligatorio cumplimiento de los servidores públicos**, por tanto explicar los acuerdos no es solamente un derecho sino un deber, y no podría sancionarse a quien este debatiendo públicamente sobre la paz.

Cepeda asegura que "el Procurador viene en una campaña cada vez más fanática" para intentar atajar los acuerdos de paz con el ELN y con las FARC-EP, **"es evidente que está en campaña electoral desde ya, hacia el 2018"** e intenta captar desde ahora a los electores más conservadores de Colombia, razón por la que cada día tiene una propuesta reaccionaria contra el proceso.

"Primero la idea de bombardear los campamentos de la guerrilla a portas un cese bilateral, luego el escándalo que quiso provocar por un supuesto golpe de estado después de que se aprobara en La Habana el artículo para blindar jurídicamente los acuerdos", y ahora pretende que los funcionarios públicos no puedan trabajar por la paz, asevera el Senador.

Para Cepeda, los encuentros que han sostenido Alejandro Ordoñez, Oscar Iván Zuluaga y Álvaro Uribe, dan cuenta de las fuerzas que vienen actuando contra el proceso de paz, que han intentado por todos los medios debilitar el apoyo internacional y ganarse a las fuerzas militares.

El Senador asevera que estas manifestaciones se articulan con el [[proselitismo contra la restitución](https://archivo.contagioradio.com/?s=restitucion+de+tierras+)], que ha buscado darle la razón a quienes usurparon tierras usando grupos paramilitares y a los que han mostrado total complacencia con la estrategia paramilitar, en suma, evidencian que **la extrema derecha quiere mantener el estado de guerra permanente en Colombia, para favorecer los intereses de un sector de las élites terratenientes**.

Los políticos se dividen en dos clases, los que piensan en las próximas elecciones y quienes piensan en las futuras generaciones, "creo sin lugar a dudas que el Procurador pertenece a la primera clase de políticos", concluye Cepeda.

<iframe src="http://co.ivoox.com/es/player_ej_11578330_2_1.html?data=kpaimZ2Xd5Ghhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncarqwtOYpcrUqcXVjJKYstTQs4y4xtLcxdeJh5SZopbhy8jTcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 
