Title: Crece rechazo al llamado acuerdo nacional convocado por el presidente Duque
Date: 2019-05-20 19:41
Author: CtgAdm
Category: Paz, Política
Tags: Cesar Gaviria, Corte Penal Internacional, Defendamos la Paz, JEP
Slug: rechazan-acuerdo-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/acuerdo-nacional-duque-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana] 

Tanto la comunicación de Denfendamos la paz, como la declaración de Cesar Gaviria del Partido Liberal, y una reciente carta de comunidades y organizaciones campesinas del país, expresan su rechazo al acuerdo nacional convocado por el presidente Duque pues se trataría de un pacto que provocaría graves afectaciones al proceso de paz y al Sistema que se ha creado para garantizar los derechos de las víctimas.

### Las razones para rechazar el acuerdo nacional

Una de las razones sería que el primer partido en asistir a la cita fue el Centro Democrático que afirma que lo que se realizaría con el Acuerdo sería una derogación de la JEP, haciendo importantes cambios en materia de extradición, de juzgamiento a terceros implicados en la guerra. Incluso el tema de que **FARC pierda su participación en el congreso hasta que se surta un proceso de justicia en contra de quienes dejaron las armas.**

Otra de las razones por las que se rechaza este acuerdo, es que, hasta el momento no ha convocado a partidos de oposición. En palabras del senador Velasco, “no se puede sacar un acuerdo que tenga que ver con los pactos de la Habana que formó el Estado con las FARC **sin contar con el hoy partido en el que se convirtieron**”. (Le puede interesar: ["Duplicar resultados, la orden a militares en el Gobierno Duque") ](https://archivo.contagioradio.com/duplicar-resultados-a-toda-costa-la-orden-a-militares-en-el-gobierno-duque/)

Por su parte Cesar Gaviria afirmó en un comunicado que el compromiso del Partido Liberal con la paz es indeclinable y consideran que ese acuerdo tiene protección institucional. Además resalta que es necesario que se respeten los fallos judiciales tomados por la JEP en el caso Santrich y **no se pueden usar para hablar de “lecturas de crisis institucional o de choque de trenes”.**

Adicionalmente la carta de varias comunidades asegura que “Ese Pacto quiere desconocer que los incumplimientos son una realidad en nuestros territorios. Algunos de ellos tomados por nuevos criminales que no son propiamente disidencias de las FARC,  que operan con la ineficacia de sectores de la fuerza pública y con el miedo o silencio de alcaldes, tomando nuestras tierras, reclutando a nuestras hijas e hijos, forzándonos al silencio, al desplazamiento y al exilio.” (Le puede interesar: ["No hay garantías de seguridad para las juntas de acción comunal"](https://archivo.contagioradio.com/no-hay-garantias-para-juntas-de-accion-comunal-asesinan-al-lider-benedicto-valencia/))

### **Comunidades apelarán a la Corte Penal Internacional** 

En la carta firmada por más de un centenar de organizaciones aseguran que seguirán enviando comunicaciones a la JEP pero que las orientarán también **hacia la Corte Penal Internacional como un recurso para garantizar justicia y la restauración de sus derechos. **(Le puede interesar: ["¡A la Corte Penal Internacional para exigir protección a líderes!"](https://archivo.contagioradio.com/la-corte-penal-internacional-exigir-proteccion-lideres/))

Para algunas líderes políticos que hacen parte de defendamos la paz, consultados por Contagio Radio, el presidente Duque orientó un acuerdo que es imposible de hacer por as graves afectaciones al proceso de paz, esta situación redundaría en la posibilidad de decretar un estado de conmoción interior para alcanzar los objetivos del uribismo.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
