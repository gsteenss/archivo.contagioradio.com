Title: Declaraciones de Mario Montoya ponen tierra de por medio entre él y soldados
Date: 2020-02-17 18:40
Author: CtgAdm
Category: Entrevistas, Judicial
Tags: Ejecuciones Extrajudiciales, JEP, Mario Montoya
Slug: mario-montoya-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Mario-Montoya-Jep.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Mayor-Cesar-Maldonado.mp3" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Mario Montoya: Verdad Abierta

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/mayor-cesar-maldonado-sobre-declaraciones-mario-montoya_md_47974958_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Mayor César Maldonado | Director de la Fundación Militares por la Reconciliación

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

Sobre las recientes **declaraciones del general (r) Mario Montoya ante la [Jurisdicción Especial por la Paz (JEP)](https://twitter.com/JEP_Colombia/status/1227590318314356741)**quien afirmó que sus subordinados no entendieron sus directrices porque eran de estrato bajo y que por eso cometer ejecuciones extrajudiciales, el mayor César Maldonado, director de la **Fundación Militares por la Reconciliación** señaló que se trató de una posición lamentable que evidencia una forma peyorativa de referirse a los hombres que tuvo bajo su cargo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El mayor señaló que al interior del Ejército no hay hijos de empresarios, ni de políticos o personas acaudaladas, quienes "no van a la guerra sino que la ven a través de la televisión", mientras que aquellos militares que hoy enfrentan un proceso por obedecer órdenes ligadas a los mal llamados falsos positivos, son "soldados quienes deben afrontar el escarnio público sin tener acceso a una segunda oportunidad" .

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Es lamentable que haya tomado esta posición en la JEP al referirse hacia sus soldados, esperábamos más sabiduría".

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### "Declaraciones de Mario Montoya fueron peyorativas"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Agregó que las declaraciones de Montoya no son suficientes para resaltar el humilde origen de quienes conforman las FF.MM. de Colombia, "todos conocemos la procedencia de los soldados de este país, los hijos de los campesinos, los hijos de los tenderos, los hombres de barrio somos lo que realmente engrosamos las filas de las fuerzas armadas", agregando que se trató de declaraciones que además de exponer una posición clasista y peyorativa, pusieron **"tierra de por medio" entre él y los soldados.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Todos conocemos la procedencia de los soldados de este país, los hijos de los campesinos, los hijos de los tenderos, los hombres de barrio somos lo que realmente engrosamos las filas de las fuerzas armadas"

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Maldonado concluye que se esperaba más sabiduría en las palabras dichas por el general durante su audiencia; declaraciones que espera Montoya corrija, pues ha sido demostrado que no solo se trató de soldados quienes incurrieron en las ejecuciones extrajudiciales, también participaron oficiales suboficiales y de alta jerarquía que lo han reconocido ante la justicia ordinaria. [(Le puede interesar: Alfonso Romero Buitrago el capitán que le apuesta a la verdad)](https://archivo.contagioradio.com/alfonso-romero-buitrago-el-capitan-que-le-apuesta-a-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, familiares de víctimas de ejecuciones extrajudiciales consideran que el militar debe ser expulsado de la JEP pues no aportan a la verdad, compromiso que se adquiere al momento de ser acogido en esta jurisdicción, sin embargo antes sus declaraciones, señalan que sus palabras no solamente fueron limitadas, sino que fueron ofensivas. Al respecto, la JEP está evaluando la solicitud de las víctimas, mientras contrasta lo dicho por el general Mario Montoya con otras versiones.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
