Title: Atentado de Daesh podría recrudecer xenofobia
Date: 2017-08-18 14:07
Category: DDHH, El mundo
Tags: Daesh, Estado Islámico
Slug: atentado-de-daesh-podria-recrudecer-xenofobia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/DAESH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Periódico ] 

###### [18 Ago 2017] 

Luego del atentado en Barcelona que dejo como resultado 13 personas muertas y más de 100 heridos, se han activado las alarmas en el continente europeo para evitar que más actos como este se repitan. Sin embargo, **de acuerdo con el analista César Torres del Río otra de las situaciones que se recrudece con este hecho es la xenofobia religiosa**.

Para César Torres, las actuaciones del grupo Daesh, son actos terroristas que tienen como único fin “**utilizar el terror para imponer su visión dogmática de lo que es el islam**” evidenciando que no tiene los medios para imponer ni ideológica ni políticamente su concepción de mundo.

En este sentido, las medidas que se deberían tomar para combatir el terrorismo, según César Torres, no pueden atentar contra la defensa de los derechos humanos “hay que diferenciar entre lo que es una organización típica terrorista como el Daesh **y aquellas que son perseguidas por los regímenes de occidente con el ánimo de aplastarlas**”.

### **La xenofobia tras el combate al terrorismo** 

Los diferentes atentados cometidos por el Estado Islámico, han acarreado medidas como las de Estados Unidos que vetaron el ingreso de personas de países de oriente, acciones que de acuerdo con Torres del Río **“satanizan la sociedad occidental”**.

“Cada vez más cientos de miles de personas ven en la religión un hecho que se vuelve factor peligroso para la estabilidad, al mismo tiempo que miles de ciudadanos están estableciendo distinción entre la visión dogmática del Estado Islamico y terrorista acerca **de lo que debe ser la aplicación del islam y lo que la religión islámica está pretendiendo” afirmó César Torres**. (Le puede interesar: ["Siria, Rusa y el Imperialismo"](https://archivo.contagioradio.com/siria-rusia-y-el-imperialismo/))

Razón por la cual manifestó que las medidas que deben acoger los países deben estar enfocadas y centralizadas no en la cultura ni la práctica de la religión, sino en aquellos que la usan como para infundir terror.

<iframe id="audio_20407467" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20407467_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
