Title: El paramilitarismo sí existe y cometió 550 violaciones a los DDHH en 2016
Date: 2017-05-05 19:36
Category: DDHH, Nacional
Tags: CINEP, Paramilitarismo
Slug: paramilitarismo_si_existe_cinep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Paramilitarismo2-e1461348491716.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  You Tube 

###### [5 May 2017] 

‘El paramilitarismo sí existe', y eso es precisamente lo que demuestra el reciente informe del Cinep, en el que se reportan un total de ** 550 victimizaciones por parte de grupos paramilitares y 833 atribuidos a actores armados no identificados.** El segundo actor más victimizante es la Policía Nacional.

### Paramilitarismo 

El informe lo realiza ‘El Banco de Datos de Derechos Humanos y Violencia Política de Cinep/Programa por la Paz’. Las cifras son alarmantes pues se han presentado incrementos en las violaciones a los derechos humanos por parte de estructuras paramilitares. **Durante 2016 se registraron 395 amenazas; 83 ejecuciones extrajudiciales; 44 personas heridas; 9 personas fueron desaparecidas y 12 más torturadas.**

Uno de los temas que más llama la atención son los panfletos amenazantes, que para el Cinep dan cuenta del fortalecimiento de grupos paramilitares. “Entre las tantas evidencias de la existencia del [paramilitarismo](https://archivo.contagioradio.com/?s=paramilitares), se encuentra la distribución de panfletos amenazantes que tienen una similitud en todo el país, lo cual constituye un indicio de patrón y estructura organizada de carácter nacional”, dice el informe.

El padre Javier Giraldo S.J. hizo un llamado  frente a esta problemática al decir que “**no se puede seguir negando la existencia ni el accionar de los grupos paramilitares** en Colombia”. Por su parte, Alejandro Angulo S.J. cuestionó la capacidad de la justicia para investigar los crímenes contra campesinos y líderes sociales, en el escenario del posconflicto. [(Le puede interesar: 300 pparammilitares impiden la lbre movilizadad en comunidades de Putumayo)](https://archivo.contagioradio.com/300-paramilitares-impiden-la-movilidad-de-comunidades-en-putumayo/)

### Agentes estatales 

Casi al mismo nivel de violaciones a los derechos humanos de los paramilitares se encuentras las de la policía, con un total de 548 victimizaciones durante el 2016, de las cuales 256 personas fueron personas heridas; **196 detenidas de forma arbitraria; 58 amenazadas; 17 ejecutadas extrajudicialmente; 12 torturadas y 9 víctimas de violencia.** [(Le puede interesar: 5 heridos por acción del ESMAD en Llorente, Tumaco)](https://archivo.contagioradio.com/5-heridos-deja-accion-del-esmad-en-el-corregimiento-de-llorente-en-tumaco/)

Por otra parte, de las 833 victimizaciones sin que se tenga datos sobre los presuntos responsables, llama la atención sobre la continuidad de la “guerra sucia” con fines políticos en el país. Los departamentos más afectados por violaciones a los DD. HH. fueron: Boyacá, Cauca,Valle del Cauca, Norte de Santander, Atlántico, Antioquia, Santander y Chocó.

\[caption id="attachment\_40119" align="aligncenter" width="399"\]![Informe Cinep](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/cifras-cinep.png){.size-full .wp-image-40119 width="399" height="529"} Informe Cinep\[/caption\]

### El caso del Magdalena Medio 

Este reporte del Cinep, también visibiliza la grave situación en materia de derechos humanos que se vive en la región del Magdalena Medio. De acuerdo con el Observatorio de Paz Integral del Magdalena Medio (OPI), el Programa de Desarrollo y Paz del Magdalena Medio, pese a las inversiones estatales de conectividad y a las inversiones privadas agroindustriales y mineras la región sigue presentando altos índices de violencia.

Solo las agresiones contra la vida de la población civil en los 31 municipios que cubre el Observatorio de Paz Integral (OPI), muestran un registro de **439 agresiones entre 2014 y 2016, es decir,** 310 muertes violentas de civiles asociadas al conflicto armado y 129 intentos de homicidios con heridos por arma de fuego.

**La mitad de estos crímenes se cometieron en Barrancabermeja, mientras los demás se presentaron en 16 municipios,** principalmente en Aguachica, Sabana de Torres, San Pablo, Puerto Wilches, Puerto Berrío, Tiquisio y Santa Rosa. Lo que muestra que esta región se encuentra en alto riesgo en medio del escenario de posacuerdo que vive Colombia, cuyo territorio es esenciales para los procesos y programas en el marco de la implementación de los acuerdos de paz.

###### Reciba toda la información de Contagio Radio en [[su correo]
