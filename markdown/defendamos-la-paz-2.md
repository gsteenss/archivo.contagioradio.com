Title: Plataforma Defendamos la Paz recogerá firmas para curules de víctimas
Date: 2019-06-17 18:42
Author: CtgAdm
Category: Comunidad, Nacional, Paz
Tags: acuerdo de paz, Defendamos la Paz, Francia Márquez, Juan Fernando Cristo
Slug: defendamos-la-paz-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Captura-de-pantalla-126.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/defendamos-la-paz.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto Twitter:@IvanCepedaCast] 

**La plataforma Defendamos La Paz** manifestó este lunes su intención de proteger la implementación de los acuerdos, en particular la instauración de las curules de víctimas y paz, el fin de los asesinatos de líderes sociales, el respaldo de la justicia transicional y el reconocimiento de la ayuda de la comunidad internacional para la salida del conflicto armado.

En rueda de prensa el ex-ministro del Interio**r Juan Fernando Cristo afirmó que la plataforma recogerá más de 1.000.000 de firmas**, con el fin de presionar a los altos tribunales de solicitar a Presidente Iván Duque y al Congreso, de la promulgación del acto legislativo que creó las circunscripciones transitorias para las víctimas del conflicto en los 16 Programas de Desarrollo con Enfoque Territorial (PDETS) del país.

Cabe recordar que estas **curules fueron aprobadas por la plenaria del Senado en diciembre de 2017** y luego avaladas por un fallo de la Corte Constitucional. Como lo señaló el ex-ministro, hay demandas en el Consejo de Estado y tutelas en la Corte Constitucional pendientes. Lo que esperan lograr con las firmas es que la justicia se pronuncie sobre estos casos. (Le puede interesar: "[Precandidatos a Curules de paz y víctimas protestan en Bogotá](https://archivo.contagioradio.com/precandidatos-a-curules-de-paz-y-victimas-protestan-en-bogota/)")

Además de estas curules, la plataforma también solicitará medidas de protección colectivas para los líderes y las lideresas sociales que actualmente se encuentran amenazados por defender los derechos de sus comunidades y el territorio. A este punto, la lideresa Francia Márquez agregó que los líderes acogieron esta propuesta de Defendamos La Paz, esperando que el discurso se convierta en acciones para realmente proteger la vida de ellos en las regiones.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
