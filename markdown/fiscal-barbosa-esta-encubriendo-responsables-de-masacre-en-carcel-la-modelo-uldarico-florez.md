Title: "Fiscal Barbosa está encubriendo responsables de masacre en cárcel La Modelo" Uldarico Florez
Date: 2020-04-13 13:38
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #motin, carcel, Covid-19, INPEC, Ministra de Justicia
Slug: fiscal-barbosa-esta-encubriendo-responsables-de-masacre-en-carcel-la-modelo-uldarico-florez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/unnamed-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En una alocución el Fiscal general Roberto Barbosa, expresó que detrás de los amotinamientos del pasado 21 de marzo estarían las disidencias de las FARC. Frente a esta afirmación, el defensor de DD.H, Uldarico Florez expresa que **el "Fiscal Barbosa está encubriendo a l**os **responsables de la masacre** en cárcel La Modelo" que son el Ministerio de Justicia y la guardia del INPEC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el abogado, los responsables intelectuales de esta masacre son la ministra de Justicia, **Margarita Cabello** y el el director del INPEC, el Brigadier General Norberto Mujica Jaime y el director del centro penitenciario de la Cárcel La Modelo, Carlos Alberto Incapie.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para el defensor, la responsabilidad de estas autoridades estaba en la puesta en marcha de planes y protocolos para prevenir un posible contagio. Acciones que habrían **calmado el miedo que sintieron los reclusos** y hubiesen evitado la masacre.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a ese panorama, Florez asegura que ya iniciaron acciones legales para que se den respuestas frente a la labor de las autoridades. (Le pude interesar: "[Amotinamiento dejó 23 muertos en cárcel La Modelo](https://archivo.contagioradio.com/amotinamiento-dejo-23-muertos-en-la-carcel-la-modelo/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Envíamos un derecho de petición a la Procuraduría exigiendo que se responda ¿qué acciones disciplinarias se han iniciado al respecto para investigar esta masacre?¿cuáles fueron los dictámenes de medicina legal? y si se acepta o no una comisión de verificación de los hechos".

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Qué hay detrás de los amotinamientos del 21 de marzo?
------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el abogado, la afirmación de Barbosa no solo no tiene sentido sino que evidencia que no se quiere investigar a fondo lo que pasó. En primer lugar, **señala que La Modelo no hay prisioneros políticos,** razón por la cual las disidencias no podrían haber motivado un amotinamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar afirma que las razones para que la población carcelaria protestara ese día son históricas, desmintiendo una vez más la tesis del "plan de fuga". De hecho ese 21 de marzo, la población carcelaria había convocado un cacerolazo p**ara exigir medidas urgentes que evitaran el contagio del Covid 19.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, pese a todos las llamados de la población privada de la libertad, organizaciones defensoras de derechos humanos y organismos internacionales, el pasado 10 de abril se confirmaron las **dos primeras muertes de internos** en la cárcel de Villavicencio y el contagio de un guardia del INPEC en la cárcel Distrital.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esos motivos las organizaciones continúan exigiendo medidas urgentes e inmediatas a Duque y la conformación de una misión de verificación por la defensa de los derechos humanos de la población reclusa. (Le pude interesar: "[Todos tenemos derechos, ellos también-2](https://www.justiciaypazcolombia.com/todos-tenemos-derechos-ellos-tambien-2/)")

<!-- /wp:paragraph -->
