Title: Erradicación forzada provoca múltiples violaciones de DD.HH. en Teteyé, Putumayo
Date: 2020-02-05 14:22
Author: CtgAdm
Category: DDHH, Nacional
Tags: ESMAD, Putumayo, Sustitución de cultivos de uso ilícito, Teteyé
Slug: erradicacion-forzada-violaciones-ddhh-teteye
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/esmad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Fot: Teteyé, Archivo

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Gases lacrimógenos al interior de las casas, bloqueo de caminos de circulación de las comunidades, heridas por represión de la Policía antinarcóticos, violación de los acuerdos de sustitución voluntaria, amenazas e intimidaciones son algunas de las denuncias que habitantes de **Teteyé en Putumayo** hicieron durante la visita de una Misión Humanitaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este tipo de hechos también se estarían presentando en otros sectores como Piamonte, Cauca y Jardines de Sucumbios, Nariño según explica **Yury Quintero, coordinadora de la red de DD.HH. del Putumayo** quien estuvo presente en Nueva Unión, Puerto Asís en la frontera con el Ecuador y el río San Miguel, en el corregimiento Teteyé. Allí se pudo hacer seguimiento a las denuncias y recolectar información que no había sido denunciada o conocida a través de medios y que denota la ausencia de garantías y vulneración de los derechos de los campesinos mediante acciones desmedidas por parte de la **Policía Antinarcóticos y el Escuadron Móvil Antidistrubios (ESMAD) presentes en el corregimiento**. [(Lea también: Reducción de cultivos de uso ilícito es un mérito de las comunidades)](https://archivo.contagioradio.com/reduccion-de-cultivos-de-uso-ilicito-es-un-merito-de-las-comunidades/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Puerto Asís, es uno de los municipios, junto a otros diez como Tibú en Norte de Santander o Tumaco, Nariño recogen el 44% de coca en el país.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

La comunidad no puede circular libremente

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante la misión de verificación, en compañía de la Defensoría, la Gobernación y la Alcaldía encargada, se constató que hay seis personas heridas por golpes y disparos a distancia dirigidos al cuerpo que se han dado entre los enfrentamientos entre campesinos y autoridades. De igual forma preocupa el estado de los niños y niñas de la comunidad quienes no han podido asistir a la escuela.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En Teteyé también se han realizado disparos de gases al interior de las casas, incluida una en donde "se encontraba una mujer embarazada quien tuvo que salir para estar seguro que su bebé estaba bien". La coordinadora alerta además que los caminos por donde transita la gente de una finca a otra en su gran mayoría están obstruidos porque la Policía ha ubicado sus bases, prohibiendo el paso. [(Le puede interesar: Presidente Duque, fumigando no se acaba con el narcotráfico: lideresa Maydany Salcedo)](https://archivo.contagioradio.com/presidente-duque-fumigando-no-se-acaba-con-el-narcotrafico-lideresa-maydany-salcedo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Yury Quintero explica que después de conocer las denuncias se intentó un acercamiento con la Fuerza Pública, pese a que no fue posible, desde la Defensoría se logró llegar a diálogos en la búsqueda de soluciones y mitigar el impacto social que se está causando en la comunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además de verificar la información, desde la red de DD.HH. señalan que la finalidad de esta comisión es generar rutas de atención y soluciones mediante un consejo de seguridad del que surgirá un documento que será entregado al Gobierno para que aborde soluciones inmediatas y elimine la práctica de erradicación en Puerto Asís, lugar en el que se han erradicado 4.293 hectáreas hasta el 2019.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto Quintero señala que la fumigación ni siquiera se está realizando de manera correcta posibilitando que los cultivos de coca crezcan de nuevo, **"es un circulo vicioso del que el único que saca provecho es el Gobierno que considera más rentable pagar a fumigadores que ponerle plata aun programa de sustitución e invertirle al campo del país".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Otras denuncias presentadas desde junio de 2019 por parte de campesinos de Puerto Asís, han permitido conocer que no ha sido posible llegar a un acuerdo concertado e incluso se han dado reiteradas amenazas contra jóvenes campesinos, siendo lo único evidente la ausencia de proyectos productivos que prometió el Gobierno como parte del Acuerdo de Paz. [(Le puede interesar: Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo)](https://archivo.contagioradio.com/se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**A través de la erradicación forzada, el Gobierno invirtió \$80.000 millones para contratar 2.100 personas** que hacen parte de 100 grupos que trabajan en Bolívar, Córdoba, Antioquia, Cauca, Nariño, Putumayo, Norte de Santander, Caquetá, Guaviare y Meta. Dicha medida "no solo viola el Acuerdo de sustitución voluntaria, a su vez genera más desplazamiento y pobreza" denuncia Quintero, por lo que se espera encontrar alternativas para contrarrestar el cultivo de coca [(Lea también: Guerra contra los líderes de sustitución se está tomando al Putumayo)](https://archivo.contagioradio.com/lideres_sociales_putumayo_puerto_asis_asesinatos/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
