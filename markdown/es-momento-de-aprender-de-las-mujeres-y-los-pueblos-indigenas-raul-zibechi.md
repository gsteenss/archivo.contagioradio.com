Title: Mujeres e indígenas, tienen la clave para afrontar crisis del Capitalismo: R Zibechi
Date: 2020-05-17 13:08
Author: AdminContagio
Category: Actualidad, Entrevistas
Tags: Coronavirus, deuda
Slug: es-momento-de-aprender-de-las-mujeres-y-los-pueblos-indigenas-raul-zibechi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/marcha-de-mujeres-el-8-m.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:group -->

<div class="wp-block-group">

<div class="wp-block-group__inner-container">

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->
</p>
###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<p>
<!-- /wp:heading -->

</div>

</div>

<!-- /wp:group -->

<!-- wp:paragraph -->

Este jueves 14 de mayo se llevó a cabo un espacio virtual de diálogo de saberes, impulsado por [Planeta Paz](https://www.youtube.com/channel/UCXM7fnAd4st_jBBg87zDIeA) y que contó con el apoyo de Vortex, Incidem y Contagio Radio. Este primer diálogo títulado Coronavirus desnuda crisis del capitalismo conversaron teóricos y analistas sobre la crisis que se está viviendo, lo que significa para el sistema global y dieron algunas pistas sobre lo que debemos aprender los pueblos de las luchas de los pueblos indígenas y las mujeres.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Luis Jorge Garay, y la crisis del capitalismo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luis Jorge Garay, investigador y analista en asuntos económicos colombiano, sostuvo que la crisis del capitalismo viene de antes y tiene razón en el quiebre de dos paradigmas fundamentales: la meritocracia, que premia al más capaz; y la movilidad ascendente intergeneracional, es decir, que las sociedades avanzan y ascienden en posiciones sociales con el paso del tiempo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ambas razones estructurales, Garay asegura que "el modo de desarrollo del capitalismo en el neoliberalismo ha hecho eclosión con la crisis", y la pandemia es solo una de sus expresiones; "no la última tampoco, probablemente veremos la profundización de la crisis". En ese sentido afirma que el reto de la humanidad en este momento es pensar en caminos para evitar la profundización de la crisis y transformar la sociedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En análisis de Garay, hay dos tendencias claves en el mundo en términos políticos: Las de gobernantes que en medio de la democracia han tomado medidas de corte autoritario, "que podría alimentarlas tendencias de gobiernos ultraderechistas de democracias iliberales", es decir, sin derechos y con menor control ciudadano; o las de gobiernos que parecen encontrar en la pandemia una oportunidad "para avanzar en la búsqueda de un modelo de Estado posbenefactor, posneoliberal y poscapitalista".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El experto reconoce que en medio de esos dos relatos políticos hay multiplicidad de versiones, pero reitera en la necesidad de iniciar un proceso de transición inmediato, o de lo contrario, la crisis desvelada por la pandemia solo será seguida por crisis con multiplicidad de estilos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Iolanda Fresnillo: la crisis de la deuda y el cambio de un modelo insostenible

<!-- /wp:heading -->

<!-- wp:paragraph -->

Iolanda Fresnillo, investigadora especialista en política económica y economía feminista española, centró su intervención en la crisis de la deuda. Estuvo de acuerdo con Garay en que la crisis tiene una característica estructural: es crisis de la democracia, crisis de la deuda...

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y respecto a este tema, recuerda que las políticas monetarias que se pusieron en marcha tras la útlima crisis de la deuda en 2008 que afectó sobre todo a países del norte global, llevó al mundo a niveles de deuda hasta entonces desconocidos, sobre todo en la deuda privada. Ello, debido a que se promovió el endeudamiento de las familias incluso para sustenar el consumo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Lo que signfica una situación inédita: "nunca ha habido una deuda pública tan alta en tiempos de paz, y nunca una deuda privada tan elevada", señala. Adicionalmente, añade que las anteriores crisis de deuda (la de los 80´s en América Latina, por ejemplo) estaban focalizadas en una región particular o en cierto tipo de países, pero esta es una situación generalizada y que afecta a todos los sectores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esa medida, y aclarando que es imposible determinar lo que pasará en el futuro, sostiene que es previsible una situación como la de la década perdida en el continente americano; por lo tanto, afirma como necesario romper con el sistema de deuda actual para evitar que los niveles que ya existen de deuda se sigan profundizando.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Silvia Rivera: el reconocimiento de las mujeres como cuidadoras de la vida

<!-- /wp:heading -->

<!-- wp:paragraph -->

Silviea Rivera, sociologa e historiadora boliviana, retomó a Garay en tanto los elementos estructurales de la crisis, y los explica mediante el ejemplo de los migrantes. En Bolivia, relata, se tenía el imaginario de que la economía avanzaba pero realmente había sectores que salían del país y eran invisibilizados.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ahora que se está dando un retorno masivo, sobre todo desde Chile, el gobierno de facto puso una barrera para evitar el regreso, en parte porque entienden que hay una fragilidad en la economía. Ello quiere decir que los indicadores de Producto Interno Bruto (PIB) o la llegada de remesas no medían realmente la situación de algunas personas que se hace evidente con el retorno al país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Respecto al futuro, la sociologa rercuerda que "lo que nos ha enseñado la crisis es que tras ellas, viene una exacervación de los factores que la provocaron". Pero también asegura que esta crisis tiene el elemento particular de reconocer a las mujeres como ciudadoras de la alimentación y de la vida.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Raúl Zibechi: el momento para aprender de las mujeres y los pueblos indígenas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Raúl Zibechi, investigador social y analista uruguayo, expresa que en las útlimas décadas asistimos a una concentración de la riqueza y el poder que continúa y en estos momentos se está acelerando. Además, los modos de la democracia están ausentes, en tanto los gobernantes toman desiciones autoritarias en medio de estados de excepción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Zibechi fue enfático en que en este momento de la historia vívimos un colapso generacional porque el capitalismo está creando crisis ante las que no tiene la capacidad de responder. A ello se suma el fortalecimiento del autoritarismo de algunos Gobiernos, no obstante, también añade que los movimientos sociales se están haciendo fuertes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El analista uruguayo destaca por ejemplo el trabajo del Consejo Regional Indígena del Cauca (CRIC), que está haciendo una minga hacia dentro, armonizando sus territorios, fortaleciendo la comunidad y cuidándose. Al tiempo, cuidando a otros mediante la propuesta de trueques en los que no se intercambia bajo la idea de equivalencia sino de necesidad, es decir, poniendo de presente el coperativismo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En medio de esa tensión planteada, para Zibechi es claro que no se debe pensar en un choque de trenes o una confrontación, porque es necesario evitar "aquello que nos puede eliminar". En cambio propone que aprendamos de la lucha de las mujeres y de los pueblos originarios, para enfrentar la muerte mediante la multiplicación de la vida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El analista concluye que lo que pase en el futuro dependerá de lo que hagan los pueblos organizados para convertir esta coyuntura en una oportunidad, "no será de los Estados y lo que se haga en términos de crecimiento o desarrollo", finaliza. (Le puede interesar:["Condonación de deuda, control a capitales y emisión productiva: La posible trilogía económica"](https://archivo.contagioradio.com/condonacion-de-deuda-externa-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un espacio para pensar en lo que viene

<!-- /wp:heading -->

<!-- wp:paragraph -->

El diálogo de saberes continuará emitiéndose todos los jueves de 2 a 4, con temas que buscan hacer análisis sobre la coyuntura y el seginficado de lo que está pasando en el mundo respecto a la pandemia. En este [link](https://www.facebook.com/113348540811/videos/914631318986420) puede ver el programa completo desde la emisión que realizó Contagio Radio este jueves. (También lo invitamos a consultar: ["Cinco propuestas de mujeres que le apuestan a la paz"](https://archivo.contagioradio.com/cinco-propuestas-de-mujeres-que-le-apuestan-a-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
