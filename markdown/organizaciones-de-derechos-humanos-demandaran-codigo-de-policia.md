Title: Las tres causales de demanda al Código de Policía
Date: 2016-06-29 17:11
Category: DDHH, Nacional
Tags: Código Policía
Slug: organizaciones-de-derechos-humanos-demandaran-codigo-de-policia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/marcha_bta-25.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: lasillavacia.com] 

###### [29 Junio 2016] 

Las organizaciones que hacen parte de la Coordinación Colombia-Europa-Estados Unidos anunciaron que** demandarán el nuevo Código de Policía** al concebirlo inconstitucional por tres motivos: la forma en la que se aprobó el código, los poderes extraordinarios que le da a esta autoridad y la falta de coherencia con el momento histórico que vive el país con la firma del cese bilateral.

La demanda se presentará dentro de 3 semanas y luego dependerá de los tiempos de la Corte Constitucional para analizarla, sin embargo, una vez que el presidente Santos la sancione entrará en vigencia, hecho que puede darse en cualquier momento.

El Código de Policía, de acuerdo con la Coordinación Colombia-Europa-Estados Unidos **es inconstitucional** por la  forma en la se presentó, debía ser una ley estatutaria  para  tener control previo de constitucionalidad y no se permitió una discusión del proyecto. Además, le otorga [una serie de "facultades extraordinarias a este sector de la fuerza pública](https://archivo.contagioradio.com/nuevo-codigo-de-policia-es-totalmente-arbitrario/)", por ejemplo le permite interponer una serie de sanciones sin algún tipo de control y contiene medidas que atentan contra el derecho a la protesta social o que violentan la libertad de domicilio.

Según Franklin Castañeda vocero de la Coordinación Colombia-Europa-Estados Unidos y director de la Fundación Comité de Solidaridad con los Presos Políticos, "el Ministerio de Defensa y el Congreso de la República decidieron plantearle al país una normatividad que **no contó con el suficiente tiempo de discusión** y que [no está adecuada a los momentos de transito hacía la paz que vive Colombia](https://archivo.contagioradio.com/este-codigo-de-policia-es-para-la-dictadura-no-para-el-posconflicto-alirio-uribe/). Lo que tenemos acá, es una serie de medidas que va a hacer que el Código de Policía no solamente sea contrario a la constitución sino que termine causando una serie de problemas de convivencia entre la policía y la ciudadanía misma".

<iframe src="http://co.ivoox.com/es/player_ej_12069650_2_1.html?data=kpedmJ6aeZGhhpywj5WZaZS1lpeah5yncZOhhpywj5WRaZi3jpWah5yncafmwtPYzs7Sb6TV1NnOh6iXaaOlxsnOjZKPh9Dj08nW0MbHrYa3lIqvldOPh9Dg0NLPy8aRidbm0NXOj6rXuI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
