Title: Transitional justice system grants protective measures for ethnic communities
Date: 2019-09-20 09:33
Author: CtgAdm
Category: English
Tags: Bajo Atrato, Chocó, Justice and Peace Commission, Special Peace Jurisdiction
Slug: transitional-justice-system-grants-protective-measures-for-ethnic-communities
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/JEP-MEDIDAS-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Justicia y Paz] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[The Special Peace Jurisdiction (JEP) implemented precautionary measures for the protection of ethnic communities that live in the humanitarian and biodiversity zones of the Bajo Atrato region, in particular, those in Curbaradó, Jiguamiandó and the Camerú indigenous reservation. These measures prioritize the communities' values, such as their relationship to the land, their ethnic traditions and their participation — all which must be guaranteed by the state.]

[The transitional justice system decided to take action after the report “They come for our lands with blood and gunfire” was presented to the tribunal on December 2018, as part of case 004 which prioritizes human rights violations committed in Choco’s Bajo Atrato region from January 1, 1985 to December 2016. ]

[These are the first, ethnic-focused and collective measures granted by the Special Peace Jurisdiction and are intended to strengthen the communities' self-defense mechanisms. “For us, it’s very significant and historic what just happened, and we hope that these measures can be applied in their totality and that they be for our benefit,” said an inhabitant of one of the humanitarian zones in the Bajo Atrato region.]

[Each one of the institutions that make part of the Joint Commission for Verification — the JEP branch responsible for the measures — such as the Ombudsman’s Office, the Unit of Investigation and Accusation, the Ministry of Defense, the Ministry of the Interior, the National Protection Unit and the Inspector General’s Office were ordered to complete tasks that must be completed. Otherwise, these institutions face penalties.]

### **These are the first JEP measures of its kind**

[“We talked not only about the vulnerability of their rights, but also of their lands, their communities, and their environment, which is an essential part of their lives,” said the lawyer Diana Marcela Muriel, adding that this is a big opportunity for restorative justice.]

[“This is a call for those who were involved in the violation of the community’s rights, for them to appear and commit themselves to tell the truth, but above all else, for them to guarantee the right to non-repetition and to grant a comprehensive reparation,” said Muriel, who represents the communities benefited from this judicial decision.]

[The Justice and Peace Commission, an organization which has accompanied this process, requested that these measures also be granted to cover communities such as those of Cacarica; La Balsita in the municipality of Dabeiba; and the humanitarian zones of Pedeguita, Mancilla, La Larga and Tumaradó, to strengthen their self-defense initiatives.]

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
