Title: Mujeres de Mapiripán resisten al olvido tras 20 años de la masacre
Date: 2017-07-11 13:07
Category: DDHH, Entrevistas
Tags: mapiripan, Masacre de Mapiripan, Meta
Slug: masacre-de-mapiripan-obligo-a-las-mujeres-a-reconstruirse
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/mapiripan-e1456245733494.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [11 Jul. 2017] 

Ad portas de **conmemorar 20 años de la Masacre de Mapiripán en el departamento del Meta,** cometida por paramilitares en connivencia con integrantes del Ejército, las comunidades se organizan para recordar a sus seres queridos que ya no están, para volver a la tierra, para compartir sus luchas y anhelos. Memoria que ha sido en gran parte rescatada por las mujeres.

**Viviana Barrera, víctima de la masacre** dice que mirar hoy el municipio que hace 20 años albergó por más de 5 días el terror de los paramilitares, es darse cuenta que las cosas van de mal en peor pues después de este hecho el pueblo comenzó a decaer.

“Se ha caído por las administraciones que ha habido, pero también por la gente, porque la gente que estaba en el momento de la masacre se fue y la gente que ha llegado no ha ayudado a progresar el pueblo” añade Barrera. Le puede interesar: [Atacan a William Aljure, reclamante de tierras de Mapiripan](https://archivo.contagioradio.com/atacan-william-aljure-reclamante-tierras-meta/)

### **El impacto de la Masacre en las mujeres** 

**Según cifras oficiales, fueron cerca de 511 familias las que tuvieron que salir de Mapiripán para salvar su vida**, de las cuáles no se sabe a ciencia cierta cuántas fueron mujeres, niños o niñas, sin embargo, tal y como lo han denunciado diversas organizaciones de mujeres, el conflicto armado las ha afectado de múltiples formas.

Cuenta Barrera que **seguir en el territorio fue muy difícil pues “muchas quedaron viudas, solas, sus vidas no han sido fáciles,** quedaron con niños y se tuvieron que enfrentar a la vida que no estaban acostumbradas, porque el esposo que tenían les daba todo. En mi caso con mi padre nunca me faltaba nada”.

Ella dice que la masacre de Mapiripán fue un “cambio muy brusco” pues les tocó asumir responsabilidades que, por lo general en el campo, eran asumidas por los hombres **“por ejemplo a mí me tocó asumir la responsabilidad de los negocios de mi papá y eso no es fácil** porque uno no está acostumbrado a eso”. Le puede interesar: [En Mapiripán las mujeres indígenas juegan fútbol por su dignidad](https://archivo.contagioradio.com/mujeresindigenasfutbol/)

Estas situaciones hicieron que personas inescrupulosas se aprovecharan de ellas quitándoles las tierras o asesorándolas mal, de modo que perdieron sus negocios. Así mismo, muchos niños y niñas no pudieron ir a la escuela o a la Universidad por falta de presupuesto en las familias.

### **¿Cómo sueñan las mujeres que hubiera sido Mapiripán sino hubiera sucedido la masacre?** 

Dice Viviana que el municipio que tendrían hoy sería diferente, contaría con más desarrollo, por ser un lugar prospero **“tendríamos calles pavimentadas, la gente que estaba hubiera seguido pendiente del pueblo**, como mi papá que era uno de los mayores comerciantes y era uno de los que se esmeraba porque las cosas funcionaran”.

### **20 años y la justicia sigue ausente** 

Concluye Barrera diciendo que aún después de denunciar todo lo sucedido durante esos 5 días de la masacre **hoy “no se ha hecho justicia, porque todavía no ha habido verdad, no se ha dicho la verdad del porqué pasaron las cosas** y para mi mientras no haya verdad no hay una justicia ¿cómo va a haber un perdón si uno ni siquiera sabe por qué pasaron las cosas? Le puede interesar: [Coronel (r) Plazas Acevedo será procesado por presunta responsabilidad en la Masacre de Mapiripán](https://archivo.contagioradio.com/coronel-r-plazas-acevedo-sera-procesado-por-presunta-responsabilidad-en-la-masacre-de-mapiripan/)

<iframe id="audio_19744405" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19744405_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### 

<div class="osd-sms-wrapper">

</div>
