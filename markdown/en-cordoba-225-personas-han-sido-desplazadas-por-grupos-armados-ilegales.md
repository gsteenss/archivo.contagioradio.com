Title: En Córdoba 225 personas han sido desplazadas por grupos armados ilegales
Date: 2019-11-19 13:24
Author: CtgAdm
Category: DDHH, Nacional
Tags: campesinos, colombia, cordoba, Desplazamiento, Grupos armados ilegales, indígenas
Slug: en-cordoba-225-personas-han-sido-desplazadas-por-grupos-armados-ilegales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/cauca-desplazados.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ContagioRadio] 

La **Oficina de las Naciones Unidas para la Coordinación de Asuntos Humanitarios** (OCHA), presentó un informe que refleja la situación de 225 personas pertenecientes a comunidades indígenas y campesinas de diferentes veredas del departamento de Córdoba que han tenido que desplazarse de sus territorios por amenazas de disidencias  FARC-EP en alianza con el grupo “Los Caparrapos”, según la Defensoría del Pueblo.

Entre las comunidades afectadas se encuentran los resguardos  Emberá Katío en Batatadó, con 67  personas desplazadas, el Resguardo Dochama, indígena en Ibudo el Bosque con 52  integrantes desplazados y el caso más reciente sucedido el  18 de noviembre que ocasionó el destierro de 65 familias integrantes de comunidades indígenas y campesinas en San José de Uré. (Le puede interesar:[Más de 70 familias desplazadas por combates y sin atención estatal en Juradó, Chocó](https://archivo.contagioradio.com/mas-de-70-familias-afectadas-por-combates-entre-grupos-ilegales-en-jurado-choco/))

 

> \[EHP\] Flash Update N° 1 - Desplazamientos masivos en San José de Uré (Córdoba) - <https://t.co/EHn4tVzAMO> [pic.twitter.com/WX6XiMYh0j](https://t.co/WX6XiMYh0j)
>
> — Ocha Colombia (@ochacolombia) [November 16, 2019](https://twitter.com/ochacolombia/status/1195796719465910277?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  

</p>
Ante estos casos José David Ortega integrante de la Asociación de Campesinos del sur de Córdoba (ASCSUCOR), afirmó que los grupos armados tienen una disputa territorial en el sur del departamento desde principios de 2019, donde la Asociación de Campesinos ha denunciando  múltiples problemáticas de seguridad, "desde el mes de marzo de este año, grupos ilegales han desplazado alrededor de 2.000 personas, y desde entonces hay un desplazamiento constante en toda la región". (Le puede interesar: [Antioquia, segundo departamento más peligroso para la defensa de DD.HH.](https://archivo.contagioradio.com/antioquia-segundo-departamento-mas-peligroso-para-la-defensa-de-dd-hh/))

Asimismo, Ortega añadió que el día 12 de noviembre en el corregimiento de Versalles, grupos armados que se identificaron como Nuevo frente 18 de las FARC  ingresaron a un resguardo indígena y desplazaron a  200 personas, "el Gobernador dice que están haciendo la atención humanitaria necesaria; el problema es que el Gobierno nacional y local no busca acciones para que no sigan ocurriendo estos desplazamientos".

**"El Gobierno nacional tiene una idea equivocada de lo que está ocurriendo en Córdoba"**

Ortega hizo también referencia a que, según el Gobierno, todas las problemáticas del departamento están vinculadas con los cultivos de uso ilícito, "eso es una falsedad porque no existe esa cantidad de cultivos que dicen que hay, y creemos que detrás del desplazamiento y las masacres, hay otros intereses. Cuando un grupo armado quiere que la gente cultive coca, permiten que la gente permanezca para cultivar. Sin embargo, dicen que quieren el territorio libre", declaró el líder campesino.

Igualmente Ortega agregó que  Edwin Jose Besaile, Gobernador de Córdoba, criminaliza la protesta de este 21 de noviembre al igual que todas las  acciones sociales: "No creemos que el Gobierno tenga mucha voluntad de cambiar lo que ocurre en los territorios; el conflicto se agudiza y el estado colombiano no implementa el Acuerdo de Paz, vemos un abandono estatal y como los grupos armados se apoderan del territorio". ( Le puede interesar:[En audiencia se denunció la grave situación de DD.HH. que vive Córdoba](https://archivo.contagioradio.com/en-audiencia-se-denuncio-la-grave-situacion-de-dh-que-vive-cordoba/))

Por último el informe concluye con que no solo estas regiones se han visto afectadas por desplazamientos  forzados, también las personas que habitan en el territorio. Esto se debe a que  sus actividades económicas se han detenido por la prohibición que han hecho los grupos armados para transitar entre veredas, colocando artefactos explosivos. Adicional, según la organización, **esta cifra puede aumentar  en el transcurso de los próximos días  así como los riesgos en el departamento.**

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
