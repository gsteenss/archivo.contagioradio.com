Title: Primera Audiencia Regional ante la CIDH sobre criminalización a defensoras del territorio y el medio ambiente
Date: 2015-10-23 14:13
Category: Ambiente, yoreporto
Tags: Audiencias de la CIDH, CIDH, Criminalización, derechos del ambiente, Fondo de Acción Urgente, Movimiento Ríos Vivos, Mujeres Defensoras de los territorios, Territorios
Slug: primera-audiencia-regional-ante-la-cidh-sobre-criminalizacion-a-defensoras-del-territorio-y-el-medio-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/defensores_ambiente_CIDH_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CIDH 

###### [23 Oct 2015] 

El Fondo de Acción Urgente para América Latina y el Caribe, en alianza con organizaciones y fondos de mujeres internacionales, ha convocado a la Primera Audiencia Regional ante la CIDH sobre Criminalización a Mujeres Defensoras de los territorios, el medio ambiente y la naturaleza en las Américas que se realizará hoy 23 de octubre de 2015 a las 6:00 pm hora Washington DC.

La audiencia surge en un contexto en el que la extracción de recursos naturales suscita actos de criminalización aplicados por parte de actores públicos y privados para neutralizar las luchas de las comunidades y organizaciones que defienden sus derechos y sus territorios.

Cuando estas acciones se ejercen contra las mujeres generan en sus vidas impactos diferenciados sobre los cuales llamaremos la atención ante la Comisión, como: la exacerbación de la violencia basada en género, limitación a su participación efectiva, la precarización en el acceso a la tierra y a la vivienda, y el deterioro en su salud física y emocional.

Durante la audiencia se presentarán tres modalidades de criminalización en Latinoamérica que visibilizan patrones y evidencian impactos concretos en la vida de las mujeres.

Judicialización: uso indebido del derecho penal; Estigmatización: señalamiento y daño de la imagen pública de las defensoras; y hostigamiento: amenazas, segregación y violencia física y psicológica.

Por lo anterior hacemos un llamado a los Estados para que emitan instrumentos jurídicos que reconozcan la legitimidad del quehacer de las defensoras, garanticen que el poder judicial no sea instrumento para su represión y promuevan escenarios de rendición de cuentas, concertación y consulta con presencia de las mujeres.

**Por Fondo de Acción Urgente para Yo Reporto**
