Title: “Las víctimas dimos ejemplo de respeto y dignidad” Alejandra Gaviria
Date: 2017-04-10 13:48
Category: DDHH, Entrevistas
Tags: Alejandra Gaviria, Alvaro Uribe, MOVICE, víctimas
Slug: las-victimas-dimos-ejemplo-de-respeto-y-dignidad-alejandra-gaviria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/WhatsApp-Image-2017-04-10-at-11.06.15-AM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movice] 

###### [10 Abr. 2017] 

Luego de la actividad planeada para conmemorar el **“Día Nacional de las Víctimas” en el Congreso**, en la cual se pretendía escuchar testimonios, pero también las propuestas que tienen desde diversos lugares del país las víctimas, **el evento terminó siendo saboteado por cuenta de los uribistas, sin embargo, las víctimas estuvieron en una actitud de disposición y escucha ante la situación.**

 “Las víctimas dieron **ejemplo de respeto, de ganas de trabajar por los problemas que en Colombia se hayan podido tener por este conflicto y eso nos llena de orgullo**, de dignidad, saber que mantenemos nuestra actitud de paz, que es fundamental para poder transitar por estos momentos en nuestro país” relató **Alejandra Gaviria integrante del Movimiento de Víctimas de Crímenes de Estado** y quien estuvo presente en dicho evento.

Según Gaviria, lo que sucedió en la jornada del Congreso es **muestra de todo lo que falta hacer en Colombia y de la falta de compromiso de los congresistas** “al final de la jornada, de 166 representantes de la Cámara solo habían 24 escuchando a las víctimas, y de 102 Senadores solo habían 11, y eso es realmente triste porque refleja un poco la dificultad que nos da a los colombianos escuchar a los otros”.

Ante la actitud de la bancada del Centro Democrático en cabeza del senador Álvaro Uribe, Alejandra Gaviria manifestó que es importante recalcar que había que comprender cuál era el sentido de la sesión “**este domingo no era un debate en el Congreso en el cual alguien daba su punto de vista** y luego el Congresista que se sentía aludido interpelaba, ayer era el momento para escuchar a las víctimas”.

En esta ocasión fue el propio presidente de la Cámara, Miguel Ángel Pinto, quien le manifestó al senador Uribe que debía esperar que las víctimas terminaran sus intervenciones “eso fue lo que le chocó (…) **la perspectiva del movimiento de crímenes de Estado es que todos somos víctimas, hay 8 millones de víctimas, claro que puede hablar, pero respetando su turno** (…)  yo tenía el turno 25 y me hubiera gustado hablar de terceras, pero era mi turno” relató Gaviria.

Para las víctimas, **la actitud que tuvo el senador Uribe es muestra de la misma que se ha interiorizado en Colombia y es la de no escuchar al otro** “y estigmatizar al otro que piensa distinto y por esas actitudes tal vez hemos tenido un conflicto tan largo y por esas actitudes las víctimas de crímenes de Estado hemos sido poco escuchadas, porque nuestras exigencias son distintas. **Lo que nos debe quedar es que debemos aprender a escuchar al otro, respetarnos.** Las víctimas dimos ejemplo”.

### **Las víctimas debían ser la noticia** 

Debido a la situación acontecida en el Congreso, las víctimas resultaron una vez más desdibujadas del plano noticioso, muchas de ellas viajaron de muchos lugares de Colombia – algunos muy lejanos- para **contar con dignidad el tránsito que están realizando de la guerra al posconflicto**, de las apuestas y propuestas que están trabajando desde hace años en sus territorios y de las exigencias que tienen para el Estado. Le puede interesar: [¡Las víctimas de crímenes de Estado tenemos derecho a ser escuchadas!](https://archivo.contagioradio.com/las-victimas-de-crimenes-de-estado-tenemos-derecho-a-ser-escuchadas/)

**“Por respeto, los titulares, las notas, deberían de decir cuál era el planteamiento de cada una de las víctimas, pero si uno ve eso pasó a un segundo plano.** Nadie sabe que proponían las personas que venían de La Guajira, Los Wayúu o la mujer que fue víctima de violencia sexual, por quedarnos en unas peleas que solo buscan la polarización y el odio y ahí las víctimas hemos dicho no nos interesa participar de espacio de rencor de odio (..) estamos trabajando por construir una Colombia con prácticas distintas” aseguró Gaviria.

### **“Las víctimas seguirán trabajando por contar la verdad”** 

Pese a cualquier intención por dividir a las víctimas, Alejandra Gaviria dice que eso no va a suceder puesto que tienen toda la intención de construir “**como víctimas seguiremos trabajando, como lo hemos hecho desde hace 30 años algunas porque el país conozca la verdad** que tenemos para contarle y porque sabemos que es una condición fundamental para transitar y transformar el conflicto”.

Por otro lado, instaron al Estado que **se continúe incluyendo a todas las víctimas del conflicto** “deben saber lo importante que es la participación de nosotros, no solo para nosotras mismas, sino para el país y en ese sentido deben generarse mecanismos y regular nuestras participaciones para que garanticen la amplitud, la diversidad. (…) **Hay que planear bien estos espacios para que no resulten revictimizantes”** dijo Gaviria.

Así mismo, las víctimas continuarán trabajando, articulándose, proponiendo temas con miras al posconflicto y la comisión de la verdad “hay que seguir trabajando por **la implementación de los mecanismos que propone el acuerdo de paz para nuestra participación, esa es la ruta.** Tenemos que ayudar a construir un país desde las propuestas que generan los acuerdos” finalizó Gaviria. Le puede interesar: [Víctimas de Crímenes de Estado piden que se desclasifiquen archivos militares](https://archivo.contagioradio.com/victimas-de-crimenes-de-estado-piden-que-se-desclasifiquen-archivos-militares/)

### **¿Qué hay detrás de la actitud del senador Uribe?** 

“No es razonable que el día que viene gente de la zona rural, personas que no todos los días pueden ejercer el derecho de hablar ante la opinión pública, pues ese día también lo coopten los congresistas, sea cual sea su condición. Lo que vimos ayer (domingo) **en medio de un acto de intolerancia y de agresividad, el caudillo del Centro Democrático** decidió darle una especie de portazo en la cara a las víctimas” dijo el senador del Polo Democrático, **Iván Cepeda**.

Además, agregó Cepeda que lo que hay detrás de la actitud del senador Álvaro Uribe **es mucho temor porque se comienza a vislumbrar la configuración del sistema de verdad,** justicia y reparación “y eso para que seamos claros y directos le provoca pánico a Uribe y a sus seguidores” recalcó Cepeda. Le puede interesar: ["Sectores de Ultraderecha y empresarios le temen a la JEP" Iván Cepeda](https://archivo.contagioradio.com/sectores-de-ultraderecha-y-empresarios-le-temen-a-la-jep-ivan-cepeda/)

<iframe id="audio_18065861" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18065861_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
