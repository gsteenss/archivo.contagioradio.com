Title: Cine gratis, documentales y eventos en la nueva edición de MIDBO
Date: 2017-10-25 11:00
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Cine, colombia, Documental
Slug: midbo-documental-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/22773559_10155320723059335_1152699399_n-e1508952038967.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:MIDBO 

##### 25 Oct 2017 

Del 30 de octubre al 5 de noviembre, se realizará la versión número 19 de la Muestra Internacional Documental de Bogotá (MIDBO), una cita con el cine de lo real que en este 2017 abordará temas como el post-conflicto, la ciencia y el arte.

Más de 90 obras documentales, 30 películas internacionales, 35 documentales colombianos, 19 obras de estudiantes latinoamericanos, retrospectivas, video-instalaciones, obras web, más de 70 horas de programación académica y encuentro con directores integran la programación de la muestra. Entre los títulos más destacados se encuentran las cintas ‘Donkeyote’, ‘Among Wolves’, ‘Arreo’, ‘Atentamente’, ‘Marejada Feliz’ y ‘Amazona’.

Una de las actividades más destacadas de MIDBO será el lanzamiento de Colombia BIO, una producción que recoge el trabajo de varias expediciones científicas en Colombia, que gracias a la cooperación estatal y de organismos internacionales, pudieron aventurarse a investigar en zonas remotas de la geografía nacional. Fruto de esa labor es el descubrimiento de nuevas especies animales, además de la documentación de la flora y fauna de dichos ecosistemas y los documentales ‘La Tierra del Agua’, ‘Andaki’, ‘Tacarcuna’ y ‘El Piñón’ hacen parte de esta selección.

<iframe src="https://www.youtube.com/embed/CVCl8aOkme4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La programación incluye eventos especiales como son la retrospectiva del actor, director, guionista y periodista brasilero Fernando Coutinho y un homenaje al documentalista colombiano Ricardo Restrepo fallecido el mes anterior. También se destaca la proyección del trabajo de Susana de Sousa, quien llega desde Portugal como una de las invitadas especiales. Por su parte, la experta y teórica cubana Susana Barriga, dirigirá el taller de cine y ensayo en la Universidad Javeriana.

Se espera que a esta edición asistan alrededor de unas 10 mil personas. Como dato adicional, muchos de estos eventos son de entrada libre y pueden consultarse en la [página web](http://www.midbo.co/) del festival.
