Title: Gobierno no escuchó a la comunidad, fuerza pública se tomará El Mango, Cauca
Date: 2015-06-26 15:27
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Argelia, Cauca, Cese al fuego bilateral, desplazamiento masivo, el mango, Policía Nacional, resistencia popular
Slug: gobierno-no-escucho-a-la-comunidad-fuerza-publica-se-tomara-el-mango-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/10270302_992987227378461_2333537612817768528_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lizeth Montero 

<iframe src="http://www.ivoox.com/player_ek_4693095_2_1.html?data=lZumlZWdeY6ZmKiakp6Jd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPstCfxtjQ18jMaaSnhqegjcaPsMKfxNTa19PNqMLYhpewjcvZqdPuwpDdh6iXaaO1w9HWxcaPt8ahhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Orlando, vocero de la comunidad El Mango, Cauca] 

###### [26 jun 2015]

En la tarde de este jueves, se llevó a cabo una **reunión entre delegados del gobierno nacional, con voceros del Mango, Cauca,** con “la esperanza de escuchar algún tipo de solución” frente a la situación que vive la comunidad, quienes denuncian ser trinchera de la policía, cuando hay enfrentamientos con la guerrilla. Sin embargo, “**el gobierno es tajante, para la fuerza pública no existe la comunidad”** denuncia Orlando, habitante y vocero de la población de El Mango.

De acuerdo con el vocero, los habitantes insisten en la permanencia de los agentes de la fuerza pública en la periferia de "El Mango", y no en medio de la comunidad, **la posición del gobierno sigue firme en no irse del casco urbano **y de su parte no “hay conciliación ni disponibilidad alguna de negociar”.

La delegación del gobierno, de la que hace parte el comandante de la policía, pidió que los diez representantes de la comunidad fueran a la vereda "La Cumbre", donde se realizaría el encuentro. “**El recibimiento no fue grato, el coronel dijo ‘dejen pasar al que formo la hecatombe’, nos sentimos juzgados**”, expresa Orlando, quien añade que pese al saludo del coronel, decidieron continuar con la reunión, esperando algún tipo de solución o negociación.

La propuesta de la comunidad no fue escuchada por el gobierno,  dificultando encontrar caminos para lograr una solución donde ambas partes queden conformes, **“quieren ocupar "El Mango" como sea”,** señala el habitante del corregimiento. Los pobladores están dispuestos a “enfrentar la situación hasta donde se pueda”, teniendo en cuenta que la militarización de la zona provocaría el desplazamiento de entre **mil y 4 mil pobladores del corregimiento que saldrán hacia el casco urbano.**

Cabe recordar que la población había sacado del casco urbano a la policía, como gesto de resistencia, ante la posibilidad de que los pobladores sean quienes terminen pagando los hechos de la guerra, por lo que **piden el cese al fuego bilateral de manera urgente. [(Ver: ](https://archivo.contagioradio.com/cerca-de-3-mil-desplazados-dejaria-militarizacion-de-el-mango-cauca/)**[Cerca de 3 mil desplazados dejaría militarización de El Mango, Cauca)](https://archivo.contagioradio.com/cerca-de-3-mil-desplazados-dejaria-militarizacion-de-el-mango-cauca/)

**r: **
