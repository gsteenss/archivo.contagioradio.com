Title: Autoridades locales podrán decidir si se desarrolla o no minería en sus territorios
Date: 2016-05-27 23:07
Category: Ambiente, Nacional
Tags: codigo de minas, Corte Constitucional, Mineria
Slug: autoridades-locales-podran-decidir-si-se-desarrolla-o-no-mineria-en-sus-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Megamineria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Elsalmonurbano blog 

###### [27 May 2016]

Alcaldes y gobernadores ahora podrán negarse a la realización de proyectos mineros en sus territorios. Así los decidió esta semana la Corte Constitucional, que derogó el  artículo 37 de la Ley 685 de 200, por medio de la cual se prohibía a las autoridades departamentales y locales impedir el desarrollo de minería.

Se trata de una norma del Código de Minas que impedía a los entes territoriales negarse a proyectos donde se explote el subsuelo, con  lo que el gobierno nacional podía imponer su voluntad y poner en marcha este tipo de proyectos. Ahora los alcaldes y gobernadores podrán hacerlo así el gobierno no esté de acuerdo, de manera que se garantiza la autonomía territorial.

Pese a la ponencia del magistrado Alejandro Linares quien pretendía que se mantuviera esa Ley, se acordó que el artículo del Código vulneraba el derecho que tienen las autoridades locales para decidir sobre sus destinos político y económico.

El artículo se establece como inconstitucional, gracias a la demanda interpuesta por dos alumnos de la Universidad de Antioquia quienes se dieron cuenta que esta Ley estaba en contra de la propia Constitución Política del país.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
