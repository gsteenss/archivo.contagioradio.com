Title: Torres de energía de la EEB en Quindío y Risaralda afectan a 165 familias
Date: 2017-06-17 10:31
Category: Ambiente, Nacional
Tags: Quindío
Slug: torres_energia_quindio_risaralda
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/Foto-de-Nestor-Ocampo2-e1497712584384.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Néstor Ocampo 

###### [17 Jun 20179] 

Desde hace 5 años 165 familias vienen siendo afectadas por la puesta en marcha de un proyecto de torres de energía que **atraviesan 26 veredas de 5 municipios, de Quindío y Risaralda.** Un proyecto que no ha sido consultado ni debidamente informado a los pobladores de esa zona, según denuncian.

**El proyecto se denomina UPME 02-2009 - Armenia.** De acuerdo con líderes ambientalistas de la región, “el proyecto se tomó en junio de 2009 y en febrero de 2012 se adjudicó su ejecución a la EEB (Empresa de Energía de Bogotá) y en el Quindío nos enteramos de todo esto a finales del 2012. No fuimos consultados, ni informados oportunamente”.

La comunidad se encuentra preocupada ya que quedarán expuestas de manera permanente a radiaciones provenientes de los campos electromagnéticos y a los accidentes que son comunes con este tipo de instalaciones. Desde entonces, habitantes, defensores y cuidadores del territorio han manifestado el descontento por el proyecto de transmisión eléctrica de alta tensión.

En ese marco, **desde el 14 de junio llevan a cabo un plantón y campamento Cultural con participación de la comunidad en rechazo a dicho proyecto.** Se trata de un campamento con tiempo indefinido, donde se desarrollan actividades culturales e informativas frente a la problemática.

### La respuesta de la gobernación 

Pese al evidente rechazo de la población, **actualmente el proyecto tiene un avance del 96%.** Por su parte, Carlos Eduardo Osorio, gobernador del Quindío, afirma que la construcción de la torre 21 en Circasia por parte de la EEB, es irreversible.

“Nos toca aceptar que esa torre se necesita para la interconexión. La administración pasada hizo unas peticiones de un dinero para invertir en la reforestación y eso lo concedieron. Mal haría yo en hacer una cosa que no puedo y echar para atrás algo que ya está en firme”, refiere el diario Crónica del Quindío, en donde se publicó las declaraciones del gobernador.

### Razones expuestas por la comunidad 

El recorrido de más de 38 kilómetros a lo largo del cual se impondrá una servidumbre de 32 metros de ancho y se emplazarán **83 torres de entre 30 y 50 metros de altura, ocasionaría graves impactos para la comunidad.**

Según argumentan, dicha energía que se produciría con las torres **podría utilizarse para actividades mineras**. Aunque la EEB afirma que es necesario  llevar a cabo el proyecto porque es posible que más adelante haya desabastecimiento y posibles racionamientos; la comunidad ha investigado y asegura que el Quindío cuenta con una capacidad instalada de 180 kV de los cuales solo utiliza 90 kV en hora pico, de manera que no tiene una demanda represada que se haya demostrado y, además, exporta energía para el departamento del Tolima.

Asimismo les preocupa el impacto ambiental. De acuerdo con las organizaciones ambientalistas **se ve comprometido el Distrito de Conservación de Suelos Barbas – Bremen, donde existen 5 Corredores Biológicos** que fueron creados hace 10 años por el Instituto Alexander von Humboldt. Un complejo natural que se conecta con el Parque Nacional Natural de los Nevados (PNNN), integrando un Sistema Regional de Áreas Protegidas.

Finalmente manifiestan que también se han ocasionado afectaciones al Paisaje Cultural Cafetero (PCC) y afecta al Distrito de Conservación de Suelos “La Marcada” que es un importante sitio arqueológico.

\

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
