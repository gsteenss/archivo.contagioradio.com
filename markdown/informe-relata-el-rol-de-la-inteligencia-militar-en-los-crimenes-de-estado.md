Title: Informe relata el rol de la inteligencia militar en los crímenes de Estado
Date: 2019-12-19 12:17
Author: CtgAdm
Category: DDHH, Nacional
Tags: Briada XX, Charry Solano, Doctrina del Enemigo Interno, Inteligencia Militar
Slug: informe-relata-el-rol-de-la-inteligencia-militar-en-los-crimenes-de-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/inteligencia-militar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

Este 19 de diciembre la Comisión de la Verdad y la Unidad de Búsqueda de Personas Desaparecidas recibieron el informe "BINCI y Brigada XX: El rol de inteligencia militar en los crímenes de Estado y la construcción del enemigo interno(1977-1998) ", que expone las conductas criminales llevadas a cabo por los integrantes del Batallón de Inteligencia y Contrainteligencia Brigadier General Ricardo Charry Solano (BINCI-Charry Solano) y la Brigada 20 del Ejército Nacional. Con el documento **se espera avanzar en el esclarecimiento del papel de la inteligencia militar, en la ejecución de violaciones a derechos humanos en Colombia**.

Los 90 casos recopilados en el informe por la Coordinación Colombia Europa Estados Unidos, la Comisión de Justicia y Paz y el Colectivo de Abogados José Alvear Restrepo, ocurrieron entre 1977 a 1998 y evidencian una persecución por parte del Estado a un grupo de personas, bajo motivaciones políticas, a partir de acciones violentas como **la desaparición forzada, la tortura y ejecuciones extrajudiciales. **[(Le puede interesar: Informe revela la violencia estatal-paramilitar en Viotá, Cundinamarca)](https://archivo.contagioradio.com/informe-revela-la-violencia-estatal-paramilitar-en-viota-cundinamarca/)

### **La doctrina del Enemigo Interno en Colombia ** 

Según el informe, el enemigo interno fue un "eje fundamental para la planeación y ejecución de una política de persecución y eliminación" en contra de sectores de **pensamiento alternativo, oposición política o movilización social**, que no estaban de acuerdo con el gobierno de turno, que con el tiempo se transformó en una estigmatización.

De acuerdo con la abogada Camila Galindo, integrante de la Coordinación Colombia Europa Estados Unidos, en un principio las actuaciones de inteligencia militar se realizaron en el marco de la legalidad del Estatuto de Seguridad, posteriormente, cuando fueron derogadas, surgieron grupos fachada como **La Alianza Americana Anticomunista (Triple A) o el MAS (Muerte a secuestradores)** que sirvieron a los mismos intereses.

El documento también devela que alrededor de la doctrina funcinó toda una red de cerca de 2000 integrantes y que contaba con civiles, inmuebles, vehículos, que se usaban en varias operaciones, identificando así un patrón común de inteligencia y acción que partían desde el control y vigilancia a las personas para indagar sobre su vida personal, horarios, hábitos, lugar de residencia, trabajo, vínculos familiares o amistades cercanas.

Para ello se usaron estrategias como la infiltración de integrantes del Ejército que posteriormente procedían con una retención ilegal de la libertad o secuestro de las personas, con la intensión de obtener **información a través de métodos como la tortura,  y finalmente, terminaban con una ejecución extrajudicial**. [(Lea también: Por primera vez, informes de población LGBTI llegan a una Comisión de la Verdad)](https://archivo.contagioradio.com/por-primera-vez-informes-de-poblacion-lgbti-llegan-a-una-comision-de-la-verdad/)

Galindo aseveró que "cabe resaltar que en esas ejecuciones extrajudiciales, si bien hay  víctimas que fueron integrantes de grupos insurgentes, no se dieron en el marco de combates, sino cuando se desarrollaban otras actividades en donde fueron eran capturados y posteriormente asesinados".

### **El prontuario del Batallón Charry Solano y  la Brigada XX**

El Batallón de Inteligencia y Contrainteligencia Brigadier General Ricardo Charry Solano operó a nivel nacional en Colombia  desde 1962 hasta 1985, a partir de ese momento la Brigada XX asumió sus labores y funcionó hasta que fue disuelta en 1998, producto de las múltiples denuncias de violaciones a derechos humanos, principalmente denunciados por organizaciones defensoras de derechos humanos de Estados Unidos.

Ambos grupos, según el informe, contaron con los medios humanos, técnicos y tecnológicos de la inteligencia adscrita al Ejército para llevar a cabo este fin, y de acuerdo con la Comisión de Justicia y paz, se creó todo un aparato organizado del Estado que "tiene **diferentes conexiones con instituciones gubernamentales o sectores civiles,** para desarrollar operaciones sicológicas, de hostigamientos, desaparición o ejecución".

En ese sentido el informe detalló hechos tan siniestros como vehículos que tenían la función de servir de estaciones móviles, completamente equipados para torturar personas. En el 2017, en fallo de primera instancia, el juzgado sexto especializado de Bogotá condenó a 11 años de prisión a tres integrantes del BINCI por la desaparición forzada de un militante del M-19 que logró sobrevivir a diferentes torturas y un intento de asesinato.

Igualmente la Comisión de Justicia y Paz insistió en que "a pesar de que la Brigada XX operó en la década de los noventa, en la actualidad la mentalidad que justificó ese conjunto de actuaciones en contra de la dignidad humana **persiste en muchas de las expresiones de las Fuerzas Militares y policiales de Colombia**".

### **La tarea de la Comisión de la Verdad y la Unidad de Búsqueda de Personas desaparecidas**

Galindo afirmó que el accionar criminal por parte de esas estructuras de inteligencia militar tuvo muchas implicaciones para el movimiento social, "es incalculable las afectaciones que han tenido las familias y allegados de víctimas de desaparición forzada en sus vidas, sumado al manto de impunidad que se teje en torno a estos crímenes".

Por este motivo las organizaciones esperan que la Unidad de Búsqueda de Personas desaparecidas pueda realizar una articulación con la JEP para hacer un plan de búsqueda de los cerca de **90 casos de desapariciones que se enuncian en el informe. **

Asimismo, esperan que la Comisión de la Verdad efectúe una evaluación de la doctrina militar y acceder a manuales de inteligencia y operación para que su conocimiento permita establecer de qué manera estos desconocieron los estándares de derechos humanos y el Derecho Internacional Humanitario.

Por su parte la Comisión de Justicia y Paz señaló que "este informe visto en prospectiva podrá permitir valorar los efectos nocivos de operaciones de inteligencia que han generado violaciones a derechos humanos bajo marcos doctrinales de enemigos internos. Doctrinas que se han acuñado en bases culturales heredadas de dictaduras del Cono Sur".

De igual forma aseveró que **"en la actualidad persiste la doctrina del enemigo interno que se manifiesta de otras formas**, porque continúa la estigmatización en contra de la oposición política o el pensamiento crítico", hecho por el cual es importante que se avance en el esclarecimiento de las actuaciones violentas de esas estructuras como medida de reparación a las víctimas y garantías de no repetición para la sociedad.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45775555" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45775555_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
