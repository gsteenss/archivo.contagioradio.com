Title: Comité de Solidaridad con los Presos Políticos explica su intervención en caso Monsalve
Date: 2019-10-15 15:57
Author: CtgAdm
Category: Entrevistas, Judicial
Tags: Alvaro Uribe, Corte Suprema de Justicia, Iván Cepeda, Juan Guillermo Monsalve, Testigo
Slug: comite-de-solidaridad-con-los-presos-politicos-explica-su-intervencion-en-caso-monsalve
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Fotos-editadas3-1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

La semana pasada la revista Semana publicó un corto artículo en el que señalaba la ayuda brindada por e**l Comité de Solidaridad con los Presos Políticos (CSPP), en el marco del desarrollo de sus funciones, al testigo Juan Guillermo Monsalve.** Tras la publicación, la defensa de Uribe señaló que demandarán al Comité por esta situación, desde la organización argumentaron que la demanda probablemente saldrá a su favor, porque este caso ya se resolvió en el pasado y corresponde a su actividad misional.

### **En contexto: Acompañamiento a un testigo en riesgo** 

**Juan Guillermo Monsalve Pineda es un testigo relevante en el caso por presunta manipulación de testigos que se adelanta contra el expresidente y senador, Álvaro Uribe Vélez.** Su participación en el caso inició en 2011, cuando por petición suya se entrevista con el senador Iván Cepeda, al que le cuenta que presuntamente los hermanos Uribe Vélez serían los creadores del Bloque Metro de las Autodefensas Campesinas de Córdoba y Urabá (ACCU), así como otros hechos ilegales que los relacionan.

Este testimonio respalda lo dicho por Pablo Hernán Sierra García, exintegrante del Bloque Metro, que también se entrevistó con el senador Cepeda por solicitud propia. En 2012, Monsalve fue víctima de un atentado con arma blanca en la cárcel de Cómbita, en 2013 el senador Cepeda denunció que en la celda de Monsalve se encontró una sustancia altamente tóxica y en 2015,  la Corte Suprema de Justicia (CSJ) ordenó interceptar llamadas de personas cercanas al proceso tras considerar que la familia de Monsalve estaría siendo presionada.

En el marco de estas situaciones, según explicó el presidente del CSPP Franklin Castañeda, **el Comité decidió intervenir para preservar la vida de la familia Monsalve, tomando en cuenta que pasaban por una situación de riesgo.** Ello implicó el traslado de lugar de residencia, y una ayuda provisional para sostenerse por un tiempo en Bogotá. (Le puede interesar: ["Piden respeto a la Corte Suprema por indagatoria a Álvaro Uribe"](https://archivo.contagioradio.com/piden-respeto-a-la-corte-suprema-por-indagatoria-a-alvaro-uribe/))

### **El CSPP activa su protección cuando el Estado no lo hace** 

Continuando con su explicación, Castañeda afirmó que el Comité tiene el papel fundamental de acompañar a víctimas, testigos y personas en riesgo**, "el primer paso de todo acompañamiento es el análisis del caso, la verificación de la información y el riesgo" y posteriormente, la realización de trámites de protección ante el Estado.** Según sentenció, el Estado normalmente "no reacciona a tiempo o de manera suficiente ante los riesgos y por eso, muchas organizaciones tenemos fondos que se otorgan con posterioridad al estudio de cada uno de los casos".

Tras hacer estos estudios de riesgos, las organizaciones defensoras de derechos humanos tienen dos opciones: Activar los mecanismos alternativos dispuestos gracias al apoyo, en el caso del CSPP, de la cooperación internacional, o dejar constancia de que el Estado no activó los mecanismos necesarios cuando fue informado sobre el riesgo. En el caso de Monsalve, el Comité decidió hacer lo primero, tomando en cuenta que la CSJ confirmó una situación de riesgo real para su familia, y que el trámite para su protección se produjo ante la Fiscalía (ente encargado del tema en ese momento, porque la Unidad Nacional de Protección se creó más tarde en ese mismo año) a principios de 2011, pero la respuesta solo llegó hasta julio de ese año.

Sin embargo, el presidente del Comité aseguró que **el acompañamiento a la familia de Monsalve duró hasta que el Estado señaló que el riesgo existía pero no podía brindar protección**. La decisión del CSPP se fundamentó en que los fondos de urgencia para manejar estas situaciones no son lo suficientemente robustos como para brindar acompañamientos por largos periodos de tiempo. (Le puede interesar: ["Uribe ha elegido la vía de la presión mediática y política contra el poder judicial: Iván Cepeda"](https://archivo.contagioradio.com/uribe-ha-elegido-la-via-de-la-presion-mediatica-y-politica-contra-el-poder-judicial-ivan-cepeda/))

### **Corte Suprema de Justicia ya evaluó el caso del Comité con Monsalve  
** 

Cuando la CSJ evaluaba el caso por la presunta manipulación de testigos contra el senador Iván Cepeda, la Corte llamó a Castañeda a declarar como presidente del Comité; al acudir, mostró todos los soportes de las ayudas que brindaron a Monsalve y su familia para demostrar que tenían un carácter legal, "contrario a lo que está sucediendo ahora con el señor Álvaro Uribe Vélez, donde hay un detenido que dice que el abogado le ofreció plata y demás". (Le puede interesar: ["Corte Suprema abre investigación formal contra Álvaro Uribe y Álvaro Prada"](https://archivo.contagioradio.com/corte-suprema-abre-investigacion-formal-contra-alvaro-uribe-y-alvaro-prada/))

En ese momento, la Corte manifestó "(i) que las labores realizadas por el CSPP hacen parte de su misión como organización defensora de derechos humanos; y **(ii) que su intervención está fundada en motivos serios y verificados sobre el riesgo a la vida e integridad personal del testigo Juan Monsalve y su familia".** Por lo tanto, se espera que la demanda que la defensa de Uribe anunció contra el Comité por el apoyo brindado a Monsalve resulte siendo fallada en favor de la organización defensora de derechos humanos.

### **Actualización: El CSPP anuncia que interpondrá denuncias por injuria y calumnia**

Este miércoles 16 de octubre, el Comité anunció mediante un comunicado que emprenderá "las acciones legales pertinentes para que se investigue a las personas que han realizado acusaciones y señalamientos" contra la institución o alguno de sus integrantes. Dichas acciones se emprendieron este jueves 24 de octubre, cuando la Fundación **hizo la solicitud a la Corte Suprema de Justicia de investigar si los senadores Paloma Valencia y Álvaro Uribe Vélez incurrieron en los delitos de injuria, calumnia y hostigamiento contra la Fundación.**

Según explicó el presidente del Comité, Franklin Castañeda, la demanda tiene ocasión porque dichos senadores están utilizando "información de manera dolosa y falsa para generar daño a la Fundación y desviar la atención de la opinión pública de la indagatoria" que rindió el expresidente Uribe ante la CSJ, en el marco de una investigación en la que ahora está formalmente vinculado. De igual forma, Castañeda señaló que los dos congresistas señalados,  tienen que ver con la campaña contra el CSPP porque desde hace meses están usando sus posiciones para afectarlos.

En el caso de **la senadora Valencia, Castañeda aseguró que ha usado su curul para averiguar situaciones que tienen que ver con el Comité** como su estado de cuentas, los fundadores y quienes hacen parte de la Institución. Por su parte**, el senador Uribe y su defensa tienen desde 2014 la información y los recibos sobre el apoyo que recibió el testigo Monsalve** por parte del Comité, y solo recientemente "está mostrándolas al país como supuestas pruebas de una actividad ilícita".

De acuerdo a un comunicado publicado por la organización defensora de derechos humanos, esta campaña de estigmatización ha generado que en **"10 días hemos recibido al menos 140 mensajes calumniosos, difamatorios y amenazantes", poniendo en riesgo su nombre y buena honra, así como su seguridad.** (Le puede interesar:["Hurtan información sensible a tres organizaciones defensoras de Derechos Humanos en Bogotá"](https://archivo.contagioradio.com/robo_informacion_organizaciones_derechos_humanos/))

> El [@CSPP\_](https://twitter.com/CSPP_?ref_src=twsrc%5Etfw) es la primera organización no gubernamental constituida para la defensa de los derechos humanos en nuestro país. Su labor, hace décadas no se circunscribe únicamente a la defensa de presos políticos. [\#LasMentirasDeUribe](https://twitter.com/hashtag/LasMentirasDeUribe?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/UE66TV6c0C](https://t.co/UE66TV6c0C)
>
> — Humanidad Vigente (@HumanidadVigent) [October 24, 2019](https://twitter.com/HumanidadVigent/status/1187366297258078208?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_43149072" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_43149072_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
