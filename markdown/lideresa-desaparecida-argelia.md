Title: Armados desaparecen a lideresa social en Argelia, Cauca
Date: 2018-10-23 15:12
Author: AdminContagio
Category: Nacional
Tags: Cauca, Lideres, secuestros
Slug: lideresa-desaparecida-argelia
Status: published

###### Foto: COCCAM 

###### 23 oct 2018 

Organizaciones denunciaron la desaparición forzada de **María Caicedo Muñoz**, defensora de Derechos Humanos en el municipio de Argelia, Cauca. De acuerdo con el pronunciamiento **la mujer habría sido raptada por un grupo irregular de hombres armados el pasado sábado 20 de octubre en horas de la madrugada**.

Según los denunciantes, la integrante del Comité de Mujeres de la Asociación de Mujeres Campesinas de Argelia AMAR, se encontraba en su vivienda ubicada **en el Corregimiento Sinaí, Vereda Desiderio Zapata**, cuando pasadas las 12 de la noche, los hombres tocaron a la puerta preguntando por María mientras intimidaban a sus hijas.

Acto seguido, **sacaron por la fuerza a la lideresa de su habitación y  sus hijas fueron atadas y amordazas** dejándolas en el piso de la vivienda, advirtiéndoles que en la mañana de ese mismo día la entregarían y que no dieran aviso a las autoridades o les costaría la vida. Según el relato los hombres salieron con Caicedo abandonando el lugar en un vehículo.

Leider Miranda, vocero nacional de la COCCAM, asegura que en ante la falta de pronunciamientos de las entidades gubernamentales frente a la desaparición de la lideresa "**la comunidad de todas formas esta en una búsqueda pero hasta el momento no sabemos nada de la compañera**".

Frente al estado de **Liliana y Zoraida Papamija Caicedo**, hijas de la defensora, Miranda asegura que la comunidad esta acompañándolas en su preocupación por la suerte que pudo haber corrido su madre. Adicionalmente, señala que la Fiscalía y la Policía han manifestado que están **en proceso de recoger las pruebas necesarias para iniciar la búsqueda correspondiente**.

Frente a la situación del Cauca, el vocero asegura que **en sus territorios se mueven diferentes grupos al margen de la ley pese a la presencia de la fuerza pública**, y asegura que los perpetradores de estos crímenes  "esperan es el momento oportuno para poder actuar" particularmente en la noche y la madrugada, **siguiendo a los líderes e identificando los lugares donde habitan y frecuentan**.

Desde la COCCAM y otras organizaciones que tienen incidencia en la región, **responsabilizan al Estado colombiano por la seguridad de los defensores de DDHH**, exigiendo que asuman su responsabilidad en **garantizar la vida e integridad de la lideresa y sus hijas**.

<iframe id="audio_29554821" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29554821_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
