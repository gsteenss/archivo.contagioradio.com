Title: Bancada de oposición rechaza montajes judiciales contra líderes sociales
Date: 2020-12-18 18:17
Author: CtgAdm
Category: Nacional
Tags: #Congresodelospueblos, #Fiscalía, #Judicialización, #LíderesSociales, #Montajesjudiciales
Slug: bancada-de-oposicion-rechaza-montajes-judiciales-contra-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/congreso-de-los-pueblos-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"justify"} -->

Tras las capturas de **Teófilo Acuña, Robert Daza y Adelso Gallo**, integrantes de Congreso de los Pueblos, la bancada de oposición al gobierno rechazó las capturas en contra de los tres líderes sociales. Las y los senadores condenaron la criminalización al ejercicio de la defensa de los derechos del campesinado colombiano.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A los tres líderes sociales se les imputó el delito de rebelión, y según la Fiscalía son colaboradores de la guerrilla del ELN. Sin embargo, la plataforma política Congreso de los Pueblos afirma que este hecho hace parte de l**a persecución al movimiento social y se suma a los más de 249 judicializaciones en contra de integrantes de esa organización.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

¿Quiénes son los líderes víctimas del montaje judicial?
-------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Teófilo Acuña** es un líder social del sur de Bolívar, y sur y centro del César. Acuña se desempeñó como presidente de la Federación de minería del sur de Bolívar y vocero de la Mesa de Interlocución del sur de este mismo departamento y sur y centro del César. Además ha sido el portavoz de la Coordinadora Nacional Agrearia (CNA), la Cumbre Agraria, Étnica y Popular y Congreso de los Pueblos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Adelso Gallo** es un líder oriundo de Saravena, Arauca, su trabajo social se ha organizado en la región de la Orinoquía. Actualmente es miembro de la Asociación Nacional Campesina José Antonio Galán Zorro, ASONALCA. Asimismo, fue vocero del CNA, la Cumbre Agraria, Étnica y Popular y Congreso de los Pueblos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente **Robert Daza** es integrante del Comité de Integración del Macizo Colombiano, CIMA, un proceso de comunidades del Norte de Nariño y el Sur del Cauca que forma parte del CNA. Además es miembro del Congreso de los Pueblos y vocero de la Cumbre Agraria, Étnica y Popular. Actualmente Daza se desempeñaba como parte del equipo de trabajo del senador Alberto Castilla.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Los montajes judiciales una estigmatización al movimiento social
----------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según el comunicado de la bancada de oposición "estos falsos positivos judiciales vienen presentándose desde hace varios años sin ningún fundamento legal, ni ético, ya que en la mayoría de los casos las investigaciones han demostrado que no hay razones para privar de su libertad a quienes han sido judicializados. Si no que esta situación responde a una "cacería de brujas". (Le puede interesar:["Comunicado bancada de oposición"](https://www.facebook.com/CastillaSenador/photos/pcb.3839832086035447/3839832029368786))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los congresistas aseguran que las consecuencias de estas persecuciones al movimiento social son cada vez más graves. Además agregan que las y los líderes sociales no solo deben afrontar la violencia en los territorios; sino que también la **"persecución estatal a quienes se han destacado por su trabajo comunitario".**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, la bancada de oposición solicitó la garantía por parte del Estado y las autoridades correspondientes al pensamiento crítico en el país y se solidarizaron con la plataforma Congreso de los Pueblos y los líderes víctimas de los montajes judiciales. (Le puede interesar: "[Juez absuelve y ordena libertad al líder social Julian Gil](https://archivo.contagioradio.com/juez-absuelve-y-ordena-libertad-al-lider-social-julian-gil/)")

<!-- /wp:paragraph -->
