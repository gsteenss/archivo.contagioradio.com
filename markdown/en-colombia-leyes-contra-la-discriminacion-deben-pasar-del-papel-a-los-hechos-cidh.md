Title: En Colombia leyes contra la discriminación deben pasar del papel a los hechos: CIDH
Date: 2015-10-22 16:50
Category: DDHH, Nacional
Tags: Asociación Nacional de Afrocolombianos Desplazados (Afrodes), CIDH, CIDH 156, Comisión Interamericana de Derechos Humanos, consulta previa en colombia, discriminación racial en Colombia, el Centro de Estudios de Derecho, el Observatorio de Discriminación Racial, el Proceso Afrourbano de Bogotá y el Proceso de Comunidades Negras (PCN), Justicia y Sociedad (DeJusticia), la Conferencia Nacional de Organizaciones Afrocolombianas (C.N.O.A., la Fundación para el Debido Proceso, Minería en Colombia
Slug: en-colombia-leyes-contra-la-discriminacion-deben-pasar-del-papel-a-los-hechos-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/CIDH-afro.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  CIDH 

###### [22 Oct 2015] 

En el marco de las audiencias que realiza la Corte Interamericana de Derechos Humanos en el mes de octubre, las organizaciones que representan a la población afrocolombiana y el gobierno, presentaron datos y estrategias para combatir la discriminación racial en el país.

Temas como la consulta previa, la actividad minera, la discriminación, las aspersiones aéreas con glifosato, la desaparición forzada, la falta de condiciones para estudiar y la violencia sexual fueron tratados durante esta audiencia en el marco del periodo 156 de la Comisión Interamericana de Derechos Humanos.

Participaron la Asociación Nacional de Afrocolombianos Desplazados (Afrodes), el Centro de Estudios de Derecho, Justicia y Sociedad (DeJusticia), la Conferencia Nacional de Organizaciones Afrocolombianas (C.N.O.A., la Fundación para el Debido Proceso, el Observatorio de Discriminación Racial, el Proceso Afrourbano de Bogotá y el Proceso de Comunidades Negras (PCN).

Aunque el gobierno, aseguró que se han creado toda una serie de mecanismos para reconocer los derechos de las comunidades negras sobre sus territorios, las organizaciones sociales aseguraron que el gobierno no protege los territorios de las comunidades de la usurpación, teniendo en cuenta que, del total de las víctimas del conflicto armado el 30% son personas afro.  Señalaron que pese a que es latente la discriminación hacia las comunidades negras, actualmente solo hay una condena por el delito de hostigamiento por razones de raza.

Frente al tema de la minería, la representación de los afrocolombianos denunció que “*la consulta previa un mero trámite sin articulación con los derechos fundamentales*”, teniendo en cuenta que el gobierno  prioriza la actividad minera como locomotra del desarrollo y a su vez, ha generado discriminación, asegurando que estas comunidades están en contra del desarrollo, y además ha “actuado contrario a la realidad de los territorios asegurando que no existen comunidades étnicas en los terrenos donde hay presencia de multinacionales”.

Desde la CIDH se recomendó que los esfuerzos materializados en leyes y normas que ha promovido el gobierno para defender los  derechos de las comunidades, pasen del papel a los hechos y así mismo, indicó que el Estado que reconoce la discriminación, “debe asumir una profunda responsabilidad”.
