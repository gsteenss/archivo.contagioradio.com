Title: 9 personas muertas en medio de protestas por erradicación forzada en Tumaco
Date: 2017-10-05 17:17
Category: DDHH, Nacional
Tags: erradicación de cultivos ilícitos, nariño
Slug: 9-personas-erradicacion-forzada-tumaco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/nueve-muertos-narino.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ASOMINUMA] 

###### [05 Oct 2017] 

[Campesinos de las veredas de los ríos Mira, Nulpe y Mataje, en Tumaco, Nariño, denuncian que cerca de 700 efectivos de la Policía y el Ejército arremetieron contra los manifestantes que se encontraban en la zona de Alto Mira, en la vereda Puerto Rico. De acuerdo con información confirmada por ASOMINUMA **son 9 personas muertas y 18 las que resultaron heridas. En la noche de ayer, fue encontrado el menor de edad que se encontraba desaparecido.**]

[El hecho sucedió en el marco de las movilizaciones para exigir que se frene la erradicación de cultivos de uso ilícito. **Los campesinos aseguran que la Fuerza Pública arremetió con armas de fuego, disparando de forma indiscriminada hacia los manifestantes.** Se trata de manifestaciones que los campesinos iniciaron el pasado 28 de septiembre, exigiendo el cumplimiento de los acuerdos de sustitución de cultivos ilícitos que se habían pactado con el gobierno, en el marco de los acuerdos de paz de La Habana y en rechazo a la erradicación forzada que vienen adelantando las autoridades, por orden presidencial, como lo asegura Diana Montilla, coordinadora de derechos humanos ASOMINUMA. (Le puede interesar:][["Erradicación forzada en el Catatumbo afecta a más de 300 mil familias"]](https://archivo.contagioradio.com/erradicacion-forzada-en-el-catatumbo-afecta-a-mas-de-300-mil-personas/)[) ]

Montilla afirma que de acuerdo con videos y fotos que han enviado los testigos del hecho, se ha establecido por parte de la organización que la mayoría de los fallecidos tienen impactos de arma de fuego en la parte posterior de la cabeza y en la espalda. "Lo que cuenta la comunidad es que en el momento de los disturbios, la comunidad empezó a correr hacia todos lados y es cuando los uniformados abrieron fuego".

[A esas denuncias se suman el reclutamiento de menores de edad, intimidaciones hacia los líderes comunitarios, el despliegue de 5 batallones del Ejército Nacional y dos tropas de la Policía que continúan con la erradicación.]

[Por su parte la Fuerza Pública dio a conocer un comunicado en el que expresa que las personas fallecidas, habrían sido producto de la explosión de cilindros bomba lanzados por un grupo armado identificado como GOA, que sería disidente de las FARC.  Sin embargo, los campesinos han negado esta versión, pues no han llegado registros de personas heridas por este tipo de explosiones, **tampoco hay militares o policías heridos. Además, según la comunidad, no se escuchó ningún tipo de detonación**.]

[Adicionalmente la organización señala que aún no ha sido posible identificar a los militares presentes ya que hacen parte de una fuerza especial integrada por militares de varias regiones, sin embargo la zona está dentro de la jurisdicción de la Fuerza de Tarea Pegaso al mando del General Sergio Alberto Tafur.]

[Hasta el momento se ha confirmado que las personas asesinadas son: Jaime Guanga Pai, Willson Changuendo, Janier Husberto Cortéz, Alfonso Pascal, Diego Escobar, Oscar Iván Ramírez, 3 indígenas continúan sin ser identificados, de igual forma se ha establecido 18 personas heridas, hasta el momento, pero se estima que sean más debido a que los familiares de las víctimas habrían llevado a sus familiares hasta sus viviendas.]

### **¿QUÉ ESTÁ PASANDO CON LAS COMUNIDADES EN ZONA RURAL DE TUMACO?** 

[Según la organización campesina en este momento las personas que están evitando la erradicación forzada de su único medio de sustento están en la zona haciendo un barrido para intentar establecer si hay más personas heridas o asesinadas, pero han asegurado que se mantendrán en la protesta a pesar de la situación que están afrontando.]

[Afirman también que los efectivos de **la Fuerza Pública han asegurado que tienen una orden presidencial de realizar la erradicación forzada** y solamente una nueva orden presidencial hará que se retiren del territorio. (Le puede interesar:][[ "5 heridos deja acción del ESMAD en el corregimiento de Llorente en Tumaco"]](https://archivo.contagioradio.com/5-heridos-deja-accion-del-esmad-en-el-corregimiento-de-llorente-en-tumaco/)[)]

### **La situación actual ** 

En la mañana del viernes, las comunidades han decidido mantenerse organizadas para terminar el censo de las personas que estaban en el cordón humanitario. Mientras tanto la población se encuentra atemorizada debido a que **en la madrugada del viernes helicópteros de la Fuerza Pública sobrevolaron la zona.**

Las comunidades están reclamando acciones urgentes de organizaciones internacionales, la presencia inmediata de las autoridades para que se brinden garantías de vida y seguridad, y además se ha pedido que se haya una misión de verificación de los hechos que establezca las causas de la muerta de los campesinos.

Ante la situación, Jean Arnault, jefe de Misión de las Naciones Unidas en Colombia, indicó que la Misión del ONU lamenta los hechos presentados y recalcó que “los acontecimientos trágicos refuerzan la necesidad de propiciar que los campesinos de las regiones afectadas por el cultivo de coca puedan escapar de la disyuntiva de la extrema pobreza y la ilegalidad”.

<div class="yj6qo ajU">

<div id=":2kq" class="ajR" tabindex="0" data-tooltip="Show trimmed content">

![](https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif){.ajT}

</div>

</div>

<iframe id="doc_14164" class="scribd_iframe_embed" title="Comunicado Alto Mira y Frontera" src="https://www.scribd.com/embeds/360812418/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-oFRk6IsTZDstl6PT1f4I&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_21314675" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21314675_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_21314822" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21314822_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
