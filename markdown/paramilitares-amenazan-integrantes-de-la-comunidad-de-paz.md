Title: Paramilitares amenazan integrantes de la Comunidad de Paz de San José de Apartadó
Date: 2016-12-02 14:24
Category: DDHH, Nacional
Tags: Comunidad de Paz de San José de Apartadó, paramilitares, San josé de Apartadó
Slug: paramilitares-amenazan-integrantes-de-la-comunidad-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/San-José-de-Apartadó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: American Program] 

###### [2 Dic. 2016] 

El pasado 1 de Diciembre, cerca de las 11 de la mañana, **dos integrantes de la Comunidad de Paz de San José de Apartado fueron abordados por sujetos que se movilizaban en una moto y que aseguraron era paramilitares**, así lo asegura una comunicación dada a conocer por esta Comunidad.

Entre los afectados se encontraba Arley Tuberquia, quien es miembro del Consejo Interno de la Comunidad de Paz. **Según la denuncia los paramilitares portaban armas de fuego con las cuales amenazaron e intimidaron a las dos personas y les aseguraron que “ellos eran guerrilleros HP a quienes se les debían matar sin compasión”. **Le puede interesar: [Paramilitares de AGC intimidan Comunidad de Paz y se asientan en territorio colectivo](https://archivo.contagioradio.com/?s=comunidad+de+paz)

Acto seguido, aseguran en la denuncia conocida por Contagio Radio, **los paramilitares requisaron a los dos pobladores y les hurtaron el dinero que portaban** y que según cuentan en la misiva, iba destinado al pago de productos agrícolas de las familias de la Comunidad de Paz.

Gracias a la intervención de los transeúntes que estaban por el barrio Mangolo, lugar donde se desarrolló la intimidación, el desenlace no fue lamentable y **los paramilitares optaron por huir del lugar no sin antes, dice la denuncia, advertirles que “no se denunciara nada o se atenían a las consecuencias”.**

Por otra parte, ese mismo día, Edwin Arteaga, miembro de la Comunidad de Paz, se dirigió a la estación de policía para colocar la denuncia de perdida de las llaves de su vehículo, pero según cuenta el comunicado “fue inmediatamente detenido y señalado de sospechoso por los agentes de la Policía y amenazado por 6 años de cárcel”.

Finalmente, **la Comunidad de Paz de San José de Apartado insta a la solidaridad nacional e internacional para repudiar estos hechos atroces y exigen al Estado Colombiano respuesta frente a estos hechos y salvaguarda de la vida. **

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}

<div class="ssba ssba-wrap">

</div>
