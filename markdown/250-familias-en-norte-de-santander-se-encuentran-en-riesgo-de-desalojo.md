Title: 250 familias en Norte de Santander se encuentran en riesgo de desalojo
Date: 2015-11-10 16:50
Category: Ambiente, DDHH
Tags: Derechos Humanos, Desalojo El canario, Desplazamiento forzado, El canario, MOVICE, Norte de antander, Radio derechos Humanos
Slug: 250-familias-en-norte-de-santander-se-encuentran-en-riesgo-de-desalojo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Desalojos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: José Miguel Gómez 

<iframe src="http://www.ivoox.com/player_ek_9343493_2_1.html?data=mpihlZmdd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRlNPZ1Mrb1sbSb9DmxcrbjcnJb8XZ1MbZ0c%2FTb8Kfk5qdjcvFscrgysbgjcrSb6%2Fj09nSjcnJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rosa Cáceres, vocera de la comunidad] 

###### [10 Nov 2015]

En el **asentamiento de “El Canario”**, Norte de Santander, la Inspección Primera Civil Urbana de Policía, con apoyo de la alcaldesa municipal Donamaris Ramírez **dieron orden de desalojo** de más de 250 familias pobres que llevan viviendo allí por más de 4 años.

En este asentamiento **habitan 500 menores de edad**, **12 personas discapacitadas**, **5 familias son deportadas y el **40% de la población es desplazada**,** entre ellos también hay adultos mayores y** víctimas de minas antipersonas. **

El desalojo se presenta porque la señora **Irma Elizabeth Contreras**, llegó a esa zona asegurando que esos terrenos le pertenecen, pese a que la comunidad lleva más de 4 años habitando esas tierras por las que además pagan impuestos como indica Rosa Cáceres, habitante del asentamiento y vocera de la comunidad.

**Hoy la policía en horas de la mañana hizo presencia** en “El Canario”, pero **no efectúo el desalojo**, sin embargo los pobladores se encuentran en medio de la zozobra y esperan  que el desalojo no se lleve a cabo sin una solución a sus condiciones que como las define Rosa Cáceres son “precarias” y **han subsistido gracias al rebusque**, que además para los servicios básicos como el agua se han visto sujetos a usureros que buscan lucrarse con las necesidades de estas personas y por ejemplo les cobraron cuotas de \$84.000 para instalarles el agua.

**De llevarse a cabo el desalojo, las 250 familias quedarían "en la calle"**, como lo expresa la señora Cáceres, quien añade que lo único que exigen son condiciones de vida digna para poder irse de allí, solicitando vivienda con servicios públicos, salud y educación.

Es por eso que la comunidad solicita el acompañamiento de la Defensoría del Pueblo y como garantes de los derechos humanos, "**se apersonen de a situación que amerita una intervención urgente de todas las entidades del Estado responsables del bienestar, la seguridad y la estabilidad de dichas familias afectadas** y que no pueden quedar desamparadas por una orden que apenas hace dos días les fue notificada de manera abrupta, pues aunque el oficio está fechado del 30 de octubre, tan sólo el pasado sábado 7 de noviembre fue de conocimiento público para la comunidad", dice un comunicado del Movimiento de Víctimas de Crímenes de  Estado, MOVICE capítulo Norte de Santander.
