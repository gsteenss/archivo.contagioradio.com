Title: Colombia entre los 4 países más letales para defensores de DDHH
Date: 2017-12-05 13:16
Category: DDHH, Nacional
Tags: Amnistía Internacional, asesinato de defensores de derechos humanos, defensores de derechos humanos, Derechos Humanos
Slug: colombia-4-paises-letales-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/derechos-humanos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Caraota Digital] 

###### [05 Dic 2017] 

La organización Amnistía Internacional presentó su informe **“Ataques letales pero prevenibles. Asesinatos y desapariciones forzadas de quienes defienden los derechos humanos”.** Según el informe sólo en 2016, fueron asesinados 281 defensores de derechos humanos en el mundo. La organización afirma que Colombia está entre las 4 naciones más letales para los líderes sociales.

El informe contiene 51 páginas y refleja la información recolectada por los investigadores de Amnistía Internacional donde incluyen casos emblemáticos y testimonios de los familiares y amigos de los defensores asesinados en los diferentes países. Con estos testimonios, **identificaron que los defensores sabían que los iban a matar, denunciaron** y los Estados no hicieron nada para protegerlos.

### **Estos son algunos casos emblemáticos en el mundo** 

Por ejemplo, el informe hace alusión al asesinato de Berta Cáceres en Honduras, quien fue asesinada en 2016 **“tras años de amenazas y ataques”**. Xulhaz Mannan de Bangladesh y quien era activista LGBTIQ, murió apuñalado en 2016 y su caso continúa en la impunidad. (Le puede interesar: ["Violencia contra líderes sociales defensores el mayor obstáculo para la paz"](https://archivo.contagioradio.com/a-mayor-posibilidad-de-participacion-mayor-es-la-violencia-contra-lideres-informe/))

En Colombia, **Bernardo Cuero**, “líder afrocolombiano de víctimas de desplazamiento forzado, fue asesinado en junio de 2017. Había recibido amenazas en varias ocasiones desde que fue objeto de desplazamiento forzado en 2000 tras ser perseguido por grupos paramilitares en relación con su labor de defensa de los derechos de la población afrocolombiana”.

Con estos casos, Amnistía Internacional le hace un llamado a los Estados de todo el mundo para que **cumplan con su deber** de “proteger de manera efectiva a quienes defienden los derechos humanos”. Esto, en la medida que la desatención de los defensores  ha dado lugar a que aumenten los casos de asesinatos y desapariciones forzadas “que se podrían prevenir”.

### **La impunidad es un mensaje para los atacantes de defensores** 

Al grave panorama en el cual se encuentran los defensores de derechos humanos en diferentes países, se suma el mensaje negativo que se da a los **perpetradores la falta de investigación de los crímenes**. La organización manifiesta que “cuando las amenazas y los ataques no se investigan ni castigan de forma adecuada, el clima de impunidad resultante erosiona el Estado de derecho y envía el mensaje de que se puede atacar a los defensores y defensoras de los derechos humanos sin que haya consecuencias”. (Le puede interesar: ["Colombia es el segundo país más peligroso para los defensores del ambiente"](https://archivo.contagioradio.com/en-colombia-fueron-asesinados-37-ambientalistas-en-2016/))

Además de esto, el informe refleja que **hay patrones de sistematicidad** en los homicidios de defensores teniendo como referencia la actividad que ellos y ellas realizan. Así, por ejemplo establece que el riesgo ha incrementado para las personas que defienden el medio ambiente, los que luchan por los derechos de la comunidad LGTBIQ, los derechos de las mujeres, los periodistas y los abogados.

Teniendo como referencia los testimonios de las familias y familiares, el informe establece que “muchos de los testimonios describen **cómo las autoridades han ignorado reiteradamente las peticiones de protección** de las víctimas y cómo los atacantes han eludido la acción de la justicia, alimentando una espiral mortal de impunidad”. Por esto, recuerdan que los Estados deben transmitir un mensaje claro sobre la no tolerancia de estas violaciones de derechos humanos.

### **Colombia es uno de los 4 países más peligrosos para los defensores de derechos humanos** 

Amnistía Internacional recalca que la situación para los defensores en Colombia es bastante grave. Indica que, en el país, “las amenazas y los asesinatos contra defensores y defensoras de los derechos humanos, en particular los activistas indígenas, afrodescendientes, de los derechos sobre la tierra y del medio ambiente, **han aumentado desde la firma del acuerdo de paz** entre el gobierno y las Fuerzas Armadas Revolucionarias de Colombia”. (Le puede interesar: ["Instituciones colombianas deben disponerse para la construcción de paz": Somos Defensores"](https://archivo.contagioradio.com/instituciones-colombianas-deben-disponerse-para-la-construccion-de-paz-somos-defensores/))

Menciona que tan sólo en 2017, **han sido asesinados 56 líderes sociales** y defensores de derechos humanos y la mayoría de las muertes están relacionadas con “conflictos locales relativos a tierras, territorio y medio ambiente”. Además, manifiesta que en el país hay una despolitización del trabajo que realizan los defensores por lo que el Estado disminuye sus responsabilidades “como el garante de la vida de los defensores de derechos humanos”.

Ante este panorama, Amnistía Internacional **ha instado a los Estados a dar prioridad, reconocimiento y protección a quienes defienden los derechos humanos**. “Las autoridades deben apoyar públicamente su trabajo y reconocer su contribución al fomento de los derechos humanos”. Dice que las autoridades deben tomar las medidas necesarias para prevenir ataques contra estas personas y “debe llevar a los responsables ante la justicia, investigando los ataques de manera efectiva y enjuiciando los asesinatos y las desapariciones forzadas”.

[Ataques Letales Pero Prevenibles\_ Asesinatos y Desapariciones de Quienes Defienden Los Derechos Humanos](https://www.scribd.com/document/366400074/Ataques-Letales-Pero-Prevenibles-Asesinatos-y-Desapariciones-de-Quienes-Defienden-Los-Derechos-Humanos#from_embed "View Ataques Letales Pero Prevenibles_ Asesinatos y Desapariciones de Quienes Defienden Los Derechos Humanos on Scribd") by [Anonymous 9gchRS](https://www.scribd.com/user/379499957/Anonymous-9gchRS#from_embed "View Anonymous 9gchRS's profile on Scribd") on Scribd

<iframe id="doc_75595" class="scribd_iframe_embed" title="Ataques Letales Pero Prevenibles_ Asesinatos y Desapariciones de Quienes Defienden Los Derechos Humanos" src="https://www.scribd.com/embeds/366400074/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-chNRGE7YvnrQYjabv4fq&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7068965517241379"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

###### 

<div class="osd-sms-wrapper">

</div>
