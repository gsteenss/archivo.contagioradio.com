Title: Víctimas ya cuentan con observatorio sobre avance de JEP
Date: 2020-02-05 11:37
Author: CtgAdm
Category: Entrevistas, Paz
Tags: CCJ, JEP, Observatorio
Slug: victimas-ya-cuentan-con-observatorio-sobre-avance-de-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-18-at-16.03.18.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/julian-gonzales-sobre-observatorio-ante-jep_md_47312947_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Julián González | Abogado Comisión Colombiana de Juristas

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

El próximo jueves 6 de febrero se lanzará el Observatorio sobre la Jurisdicción Especial para la Paz (JEP), una iniciativa de la Comisión Colombiana de Juristas (CCJ) con el apoyo de GIZ cuya finalidad es analizar los resultados de esta institución a través de la mirada de las víctimas. (Le puede interesar: ["Organizaciones de DDHH denuncian vigilancia y hostigamiento con drones"](https://archivo.contagioradio.com/organizaciones-de-ddhh-denuncian-vigilancia-y-hostigamiento-con-drones/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Coljuristas/status/1225048795895009284","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Coljuristas/status/1225048795895009284

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **¿Por qué lanzar un observatorio con enfoque desde las víctimas?**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Julián González, abogado y Coordinador de Incidencia Nacional de la CCJ, explicó que la idea del [observatorio](https://www.coljuristas.org/observatorio_jep/index.php) inició porque aunque había otras iniciativas similares, las mismas estaban enfocadas en informar lo que hacía la Jurisdicción pero **"no tenían un enfoque o línea editorial clara en términos de los derechos de las víctimas"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, la CCJ ya ha publicado seis boletines en los que se hace relatoría sobre los autos que ha publicado la Jurisdicción, haciendo énfasis en aquellos que tienen como tema central las víctimas. González afirmó que "la idea es buscar los conceptos más duros y estructuras más grandes, para volverlos accesibles al público general" pero que al tiempo, sean lo suficientemente rigurosos para las personas expertas interesadas en el tema.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El papel de las víctimas en la JEP**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Una de las [críticas](https://archivo.contagioradio.com/jalon-orejas-jep/) que hacían organizaciones defensoras de DD.HH. a la JEP en las primeras audiencias era que no había suficiente participación para las víctimas. En ese sentido, González señaló que el Observatorio trata de mostrar que si bien la institución hace esfuerzos para mejorar esta situación, aún podría hacer más para garantizar la participación de personas particularmente vulnerables.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo al Abogado de la CCJ, el problema radica en que al principio la JEP era un tribunal de verdad, **"y esos tribunales tienen reducida la participación de las víctimas a ciertos espacios"**. No obstante, resaltó que en los casos 001 y 003 (sobre secuestro por parte de las FARC y muertes ilegítimamente presentadas como bajas en combate) que son los más avanzados, hay más momentos para la participación de los afectados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"La JEP tendrá que mostrar los resultados de su gestión"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

A modo de evaluación, el integrante de la CCJ dijo que el año pasado fue muy complicado por la oposición que enfrentó la JEP desde el Congreso, pero que gracias a parlamentarios y una decisión de la Corte Constitucional, logró tener un marco jurídico completo, lo que significa que podrá avanzar en decisiones. (Le puede interesar:["Presidente Duque sanciona Ley Estatutaria de la JEP"](https://archivo.contagioradio.com/presidente-duque-sanciona-ley-estatutaria-de-la-jep/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, González concluyó que este y el próximo año serán importantes porque los casos más avanzados (001 y 003) tendrán que avanzar más, y **"la JEP tendrá que mostrar los resultados de su gestión", en términos de avanzar en verdad**, el reconocimiento de responsabilidad y la aplicación de sanciones propias.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
