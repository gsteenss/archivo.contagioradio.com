Title: "Lo fundamental será la voluntad política de las instituciones": Luz Marina Monzón
Date: 2017-10-01 22:19
Category: Entrevistas, Paz
Tags: JEP, Luz Marina Monzon
Slug: luz_marina_monzon_unidad_busqueda_desaparecidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/Monzon-JEP-e1506709847691.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ICTJ] 

###### [1 Oct 2017] 

Como un destello de esperanza para las víctimas, Luz Marina Monzón Cifuentes fue seleccionada por el Comité de Escogencia como la persona que llevará la batuta de la Unidad especial para la búsqueda de personas dadas por desaparecidas en el contexto y en razón del conflicto armado. Monzón fue una de las personas recomendadas por las organizaciones de víctimas para desempeñar dicho cargo teniendo en cuenta su trayectoria de más de 20 años en la defensa de los derechos humanos. Ha desarrollado investigaciones sobre el derecho a la verdad y la justicia, asimismo ha trabajado en documentando las luchas de las víctimas de violaciones a sus derechos humanos en el Centro Nacional de Memoria Histórica, así como otras investigaciones en torno al derecho a la justicia en casos de desaparición forzada, ejecuciones arbitrarias, tortura y violencia sexual.

**¿Cómo se siente con este nombramiento por parte del Comité de Escogencia?**

Es una enorme responsabilidad con las víctimas para tratar de dar una respuesta a algo que no ha tenido una respuesta adecuada por muchas décadas. Tengo muchas ganas de poder contribuir a que las cosas cambien, en una experiencia que ha sido muy dura para las víctimas en la búsqueda de sus desaparecidos. Me siento muy agradecida con la confianza de las víctimas.

**¿Cómo ve el papel de las mujeres en la JEP y la construcción de la paz?**

Me siento muy feliz,  realmente creo que finalmente se da una distribución de unos cargos con una perspectiva realmente de inclusión y sobre todo de transformación. Se están implementando unos mecanismos de un proceso de paz que busca transformar el país y se está empezando a transformar lo que no ha estado bien. Una de esas cosas es la no distribución equitativa en el acceso al poder para las mujeres. Yo estoy segura de que esta distribución va a hacer la diferencia.

**¿Cómo unificar un sistema de registro de personas desparecidas, entendiendo las diferentes cifras que se dan desde todas las instancias?**

Ese es uno de los restos más importantes. Se trata de una herramienta fundamental para poder desarrollar todo el mandato de la unidad, y desarrollar la búsqueda de los desaparecidos. Tenemos que saber quienes son, cuántos son, dónde fue, cuándo fue, cómo fue. Eso es fundamental. Siento que a pesar de que en el 2000 la Ley estableció la obligación de establecer un registro nacional de desaparecidos, ese registro no ha cumplido el papel para el cual fue estipulado. La información que está allí sino es incompleta, es imprecisa y en todo caso no está disponible para ser una herramienta de búsqueda.

El reto gigantesco es consolidar los datos con todas las fuentes, la Fiscalía, las organizaciones defensoras de derechos humanos, Medicina Legal, y también la información de las víctimas que puede ser muy importante, para ello debe también existir un método de recopilación de datos para hacer planes de búsqueda que realmente puedan dar resultados. Lo fundamental será la voluntad política de las instituciones para poder consolidar la información.

**¿Cuál es la clave para que haya una articulación entre el trabajo institucional y las víctimas, de manera que exista una recuperación de confianza de las víctimas en la actuación del gobierno?**

Es un gran reto, no solo porque no hay coordinación por la desconfianza entre la sociedad civil y las instituciones, sino que porque lo más lamentable es que no hay una articulación ni siquiera entre las instituciones del Estado, ni dentro de cada institución. Creo que tenemos que plantearnos unos objetivos centrados en el objetivo general de la Unidad, y es que vamos a buscar a los desaparecidos, no vamos a quitarle a nadie las competencias de nadie. Debemos establecer protocolos formales de entendimiento para poder avanzar en encontrar a los desaparecidos.

**Se necesita un andamiaje firme y robusto ¿Cómo se piensa la dirección de la Unidad?**

Hay certidumbre e incertidumbre. La semana pasada el gobierno presentó a las organizaciones sociales una propuesta de lo que sería la estructura de la unidad, con funciones y dependencias. Hasta donde sé, las víctimas y organizaciones no están satisfechas con esa propuesta y tiene un conjunto de observaciones. Al parecer no están de acuerdo porque se trata de una estructura que es más burocrática que técnica, y no hay un equipo lo suficientemente robusto para poder desplegar todas las funciones. Entonces todavía no sé cómo va a quedar la Unidad, y esa es una de las incertidumbres y unas de las dificultades con las que asumo este mandato. No se sabe cómo va a quedar esa institución y cuáles son las herramientas con las que va a quedar, estoy a la espera de poder incidir y hacer que eso funcione de la mejor manera.

**¿El tema de banco de datos genéticos podría ser una apuesta de la Unidad?**

En 2011 se aprobó una Ley que se denomina: Ley de Homenaje. Es una Ley que busca el reconocimiento al tema de desaparición forzada y establece distintos mecanismos para hacerle frente a las expectativas de las víctimas, uno de ellos es el banco genético. Un banco que está bajo la dirección de Medicina Legal. El reto para la Unidad es articular todo lo que hay porque el punto realmente es mirar las herramientas que hay, ver cómo reforzar y potenciar esas herramientas para que se cumpla el propósito de la Unidad. Ese será uno de los elementos claves para la identificación, pero hay que ver cómo está ese banco genético, porque si no tiene información disponible, estamos mal.

**Uno de los casos, por ejemplo es el tema del desarrollo del proyecto hidroeléctrico Hidroituango que inundaría una zona donde se cree que hya fosas comunes. ¿El trabajo de la Unidad de Búsqueda permitirá, que se suspendan ese tipo actividades?**

Espero que si, de hecho una de las cosas que se plantea en la Ley de Homenaje es eso. Medidas para  impedir que se afecten territorios donde se cree que existe desaparecidos y se proteja el lugar para poder hacer las búsquedas, entonces la idea es poder poner en marcha lo que hay. Yo creo que hay muchas herramientas, pero uno siente que no existen porque no funcionan.

**¿Le da miedo la corrupción al interior del Estado?**

No tengo susto, tengo preocupación porque eso realmente es así. Pero es el reto. La idea de estar en este lugar, es tratar que el Estado sea eficaz y honre sus obligaciones y le responda a las víctimas. Hay varios obstáculos pero espero enfrentarlos.

<iframe id="audio_21179404" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21179404_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
