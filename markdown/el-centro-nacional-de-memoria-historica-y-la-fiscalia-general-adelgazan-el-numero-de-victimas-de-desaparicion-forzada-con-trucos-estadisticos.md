Title: El Centro Nacional de Memoria Histórica y la Fiscalía General ‘adelgazan’ el número de víctimas de desaparición forzada con trucos estadísticos
Date: 2020-11-05 16:39
Author: AdminContagio
Category: yoreporto
Tags: CNMH, desaparecidos, Desaparición forzada, Desaparición forzada Colombia, VICTIMAS
Slug: el-centro-nacional-de-memoria-historica-y-la-fiscalia-general-adelgazan-el-numero-de-victimas-de-desaparicion-forzada-con-trucos-estadisticos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/desaparicion-forzada.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: premiodefensorescolombia {#foto-por-premiodefensorescolombia .has-text-align-right .has-cyan-bluish-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:list -->

-   El equipo de [Desaparicionforzada.co](https://colombia.desaparicionforzada.com/) denuncia el intento de las instituciones “de construir un nuevo relato en el que la desaparición forzada pierda peso”
-   Los investigadores consideran que con el actual Gobierno la política respecto a la memoria supone una “desaparición forzada de la verdad”.

<!-- /wp:list -->

<!-- wp:paragraph -->

La estadística tienen muchos atajos cuando se quiere manipular el resultado final de una base de datos. Y el equipo de Desaparicionforzada.co –espacio de investigación impulsado por [Human Rights Everywhere](https://hrev.org/)(HREW)- denuncia que tanto el Observatorio de Memoria y Conflicto (OMC) del Centro Nacional de Memoria Histórica (CNMH), como la Fiscalía General de la Nación (FGN) han alterado las cifras de víctimas de desaparición forzada en Colombia utilizando algunos de esos “trucos”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En concreto, el Centro Nacional de Memoria Histórica, que lleva sin actualizar su información sobre el crimen de desaparición forzada desde 2018, publicó el pasado agosto, a través de la red social Twitter, una cifra de víctimas que “adelgaza” en 4.152 personas el último consolidado ofrecido por el OMC en septiembre de 2018.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si entonces, el OMC registraba 80.472 víctimas de este crimen de lesa humanidad entre 1958 y el 15 de septiembre de 2018, en el trino con motivo del Día Internacional de las Víctimas de Desaparición Forzada, el Centro Nacional de Memoria Histórica hablaba de 76.320 víctimas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fidel Mingorance, investigador de Desapariciónforzada.co, explica que, “aunque el OMC no ha dado ninguna explicación, la operación de adelgazamiento ha consistido en reducir los años del conteo, ciñéndose al periodo entre 1964 y 2016, y quitar de la sumatoria a las 4.133 víctimas de las que no hay constancia de la fecha del crimen”. En todo caso, “lo que recibe el público es una cifra final, 76.230 víctimas, que rebaja sustancialmente el dato anterior, de 80.472 personas”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### [Le puede interesar: ¿Hay un plan para acabar con la memoria de Colombia?](https://archivo.contagioradio.com/hay-un-plan-para-acabar-con-la-memoria-de-colombia/)

<!-- /wp:heading -->

<!-- wp:paragraph -->

Mientras, la Fiscalía General de la Nación ha realizado una operación similar aunque de más calado. Los datos del Sistema Penal Oral Acusatorio (SPOA), que se publican de manera mensual en el portal de datos abiertos de Colombia desde agosto de 2018, iban creciendo de manera acelerada y en abril de 2020 contabilizaba 98.930 víctimas de desaparición forzada entre 1997 y abril de 2020. Justo antes de superar la barrera de los 100 mil casos, la Fiscalía cambió el rango temporal y ahora sólo informa de los casos entre 2010 y 2020, por lo que la cifra global de víctimas de desaparición forzada en Colombia pasa a 34.978 en octubre de 2020. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Estamos asistiendo a un crimen de desaparición forzada de la verdad”, explica Erick Arellana Bautista, miembro de Desaparicionforzada.co e hijo de una víctima del crimen de lesa humanidad. “La operación de camuflaje de la información la podemos enmarcar en toda la ofensiva del Estado para alterar el relato sobre la guerra y sus consecuencias”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Desaparicionforzada.co denuncia esta manipulación de los datos justo cuando la Jurisdicción Especial para la Paz (JEP) analiza si el Centro Nacional de Memoria Histórica alteró la obra ‘Voces para la transformación de Colombia’ sin consentimiento de las víctimas para adulterar el relato.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Francisco Gómez Nadal, miembro de HREW y colaborador de Desapariciónforzada.co, advierte de que  “lo que está ocurriendo en el CNMH y en otras instancias del estado respecto al relato del conflicto es de especial gravedad. No sólo se está dinamitando el proceso de paz en todo lo que tiene que ver con la construcción de verdad o la búsqueda de las personas dadas por desaparecidas en el marco del conflicto, sino que se produce una revictimización al volver a invisibilizar crímenes de lesa humanidad como parte de una estrategia negacionista”. HREW publicó en agosto de 2019 la Cartografía sobre la Desaparición Forzada en Colombia y comparte de forma abierta y permanente los avances de su equipo en la página web:[**www.desaparicionforzada.co**](https://colombia.desaparicionforzada.com/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Para más información o contacto para entrevistas:* [*Erik.Arellana@hrev.org*](mailto:Erik.Arellana@hrev.org)

<!-- /wp:paragraph -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

</p>
<!-- wp:heading {"level":6} -->

###### **Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.**

<!-- /wp:heading -->
