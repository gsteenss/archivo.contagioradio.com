Title: ADEMACOR denunció el asesinato del profesor Carlos Mazo en Córdoba
Date: 2019-08-15 11:31
Author: CtgAdm
Category: Educación, Líderes sociales
Tags: asesinato, cordoba, fecode, profesor
Slug: asesinato-profesor-carlos-mazo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Carlos-Mazo.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @Ademacor] 

[La Asociación de Maestros de Córdoba denunció el asesinato del profesor **Carlos Arturo Mazo Lara**, en la noche de este 13 de agosto en Puerto escondido, Córdoba. Carlos Mazo laboraba en la institución educativa el Silencio. Según la denuncia del sindicato, no se tiene hasta el momento ninguna hipótesis en torno a este homicidio. (Le puede interesar: ["Para los paramilitares la minería sería más rentable que la coca en el sur de Córdoba"](https://archivo.contagioradio.com/paramilitares-mineria-sur-de-cordoba/))]

[El secretario general de Ademacor, Emilio Elis Cogollo, afirmó que ya son 56 los docentes amenazados en el departamento, sin contar lo sucedido en 4 municipios de los cuales no se tiene información. Cogollo aseguró que aunque las denuncias se han realizado a las instancias de control, no se tienen resultados de las investigaciones. (Le puede interesar: ["Procuraduría suspende a Fabio Otero, alcalde de Tierralta, Córdoba"](https://archivo.contagioradio.com/procuraduria-suspende-a-fabio-otero-alcalde-de-tierralta-cordoba/))]

[Elis agregó que las amenazas en contra de los maestros y maestras del país se están quedando en la impunidad y recordó que justamente en Puerto Escondido, donde hace un año, en el mes de Junio de 2018, fue asesinado el educador Washington Cedeño, de 44 años, cuando salía de la Institución Educativa en el corregimiento Sabalito Arriba, hecho que persiste en la impunidad. (Le puede interesar: ["Con asesinato de María del Pilar Hurtado, paramilitares siguen cumpliendo amenazas enCórdoba"](https://archivo.contagioradio.com/con-asesinato-de-maria-del-pilar-hurtado-paramilitares-siguen-cumpliendo-amenazas-en-cordoba/))]

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
