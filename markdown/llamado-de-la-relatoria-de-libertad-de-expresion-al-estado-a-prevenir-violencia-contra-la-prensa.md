Title: CIDH hace un llamado a proteger a periodistas en Colombia
Date: 2017-07-27 13:07
Category: DDHH, Paz
Tags: CIDH, Libertad de expresión, Periodistas, relatoría especial
Slug: llamado-de-la-relatoria-de-libertad-de-expresion-al-estado-a-prevenir-violencia-contra-la-prensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/violencia-prensa-e1501178542597.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero] 

###### [27 Jul 2017] 

A través de un comunicado de prensa, la Relatoría Especial para la Libertad de Expresión, que hace parte de la CIDH, **manifestó su preocupación por la constante estigmatización y violencia contra los periodistas de Colombia**. Le recordaron al Estado que “tiene la obligación de prevenir, proteger, investigar y sancionar la violencia ejercida contra periodistas”.

La Relatoría hizo referencia a 4 casos específicos y manifestó que la situación de la libertad de expresión y el ejercicio del periodismo en Colombia fue objeto de una audiencia pública donde la sociedad civil manifestó que “si bien en 2016 no se registraron asesinatos contra periodistas, persistían otras formas de **agresiones como secuestros, presiones indebidas, hostigamientos y declaraciones estigmatizantes**”. (Le puede interesar: ["Simulación de Saúl Cruz fue un ataque a la libertad de prensa"](https://archivo.contagioradio.com/simulacion-de-saul-cruz-viola-obligaciones-de-libertad-de-prensa-del-estado-fllip/))

### **Casos de estigmatización y violencia ** 

En concreto, el primero caso de violencia contra la prensa al que hizo referencia la Relatoría, fue cuando el 5 de junio de 2017, “**el subsecretario del senado Saúl Cruz denunció ante el plenario del Congreso haber sido víctima de una agresión** del equipo reportero del noticiero Noticias Uno”. Luego del incidente, videos y testimonios “revelaron que el subsecretario había fingido la agresión física”.

Como segundo caso, hizo alusión al secuestro por parte del ELN de **los periodistas holandeses Derk Johannes Bolt y Eugenio Ernest Marie** el 19 de junio. Según la Relatoría, los periodistas fueron liberados el 24 de junio y fueron entregados a una comisión de la Defensoría del Pueblo.

Además, el 14 de julio **Álvaro Uribe, por medio de su cuenta de Twitter, acusó al periodista Daniel Samper Ospina de ser un “violador de niños**”. La organización afirmó que estas acusaciones fueron rechazadas por la Federación de Periodistas y la Fundación para la Libertad de Prensa y “los distintos pronunciamientos alertaron sobre la falta de fundamento de las acusaciones y repararon además en el riesgo que suponían para el comunicador”. (Le puede interesar: ["Álvaro Uribe siempre va a estigmatizar a la persona que revele cosas que él quiere ocultar"](https://archivo.contagioradio.com/alvaro-uribe-estigmatiza-a-periodismo-de-noticias-uno/))

Finalmente, nombró el caso donde la periodista y directora de Noticias RCN y NTN24, **Claudia Gurisatti, denunció una campaña de hostigamiento en su contra** a través de redes sociales. La Relatoría manifestó que esto “podría poner en riesgo su integridad personal”.

En su objetivo de estimular la defensa del derechos a la libertad de pensamiento y expresión, la organización fue enfática en manifestar que **“la actividad periodística debe ejercerse libre de amenazas, agresiones físicas** o psicológicas u otros actos de hostigamiento” e instó nuevamente a que el Estado “implemente medidas integrales de protección, prevención, protección y procuración de justicia”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
