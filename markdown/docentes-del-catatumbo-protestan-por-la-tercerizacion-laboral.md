Title: Docentes del Catatumbo protestan por la tercerización laboral
Date: 2017-01-20 16:20
Category: Educación, Nacional
Tags: Banco de Excelencia, Docentes del Catatumbo, Tercerización laboral docente
Slug: docentes-del-catatumbo-protestan-por-la-tercerizacion-laboral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/IMG-20170120-WA0021.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [20 Ene 2017] 

Decenas de docentes del Catatumbo alzaron su voz de protesta en la ciudad de Cúcuta, rechazando la tercerización laboral de la que han sido víctimas. Llevan trabajando con contratos por prestación de servicios a 10 meses, durante más de 14 años y este año, cuando se supone iban a recibir su nombramiento oficial por el Ministerio de Educación, el Gobierno impuso la iniciativa del Banco de la Excelencia que los excluye.

Algunos docentes manifestaron que dicha iniciativa gubernamental dispone la contratación de 991 maestros y cerca de 50 administrativos nuevos para la región, explicaron que llegarían docentes de otras regiones del país a trabajar en mejores condiciones que las que ellos han venido teniendo y sin tener un acercamiento al contexto que se vive en la región.

“Nosotros quienes trabajamos hace más de 10 años en esta región conocemos la situación que se vive en el Catatumbo (…) algunos tuvimos que vivir **la ola de desplazamientos del 2003 en adelante y hemos estado al lado de la comunidad, nos ha tocado muy duro y ahora nos pueden despedir”, **puntualizó una de las docentes manifestantes.

Esta docente, dijo además que "en 2016 el viceministro aseguró que el banco de la excelencia iba a ser para los decentes que venimos trabajando en el Catatumbo hace varios años, pero nos incumplieron esas promesas (...) **nos cansamos de que nuestros derechos fueran vulnerados, entonces tomamos el ejemplo de los campesinos que salen a las ciudades a protestar”.**

\[caption id="attachment\_34973" align="alignnone" width="1280"\]![Docentes del Catatumbo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/IMG-20170120-WA0024.jpg){.size-full .wp-image-34973 width="1280" height="720"} Docentes del Catatumbo\[/caption\]

### ¿Qué dice el Gobierno? 

Alexander Noguera líder de la planta de personal de la Secretaría de Educación y quien lidera el proceso de vinculación al banco de la excelencia, afirmó que hasta el momento “no hay posibilidad de asociar a los docentes bachilleres al banco del progreso”.

Por último, la docente indicó que todos los docentes afectados y organizaciones sociales se reunirán el **próximo lunes 23 de enero en la Alcaldía de Cúcuta a las 10:00am con funcionarios de la secretaria de educación para aclarar la situación.**

<iframe id="audio_16565551" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16565551_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
