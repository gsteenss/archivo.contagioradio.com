Title: ¿Qué le ha dejado a Colombia 5 años de TLC con Canadá?
Date: 2016-04-28 15:49
Category: Nacional
Tags: Canadá, extractivismo, fracking, TLC
Slug: que-le-ha-dejado-a-colombia-5-anos-de-tlc-con-canada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/PARAMO-DE-SANTURBAN-e1461876510933.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Colectivo de Economía 

###### [29 abr 2016] 

Este jueves, se presentó el informe sobre las afectaciones a los derechos humanos en los territorios de Colombia donde existe inversión extranjera canadiense, en el  marco del TLC con ese país. Allí se evidenció que este tratado ha implicado violaciones a los derechos humanos de las comunidades, **no ha significado un crecimiento relevante en las exportaciones del país y además no se ha promovido el derecho a la asociación sindical, entre otras conclusiones.**

El informe realizado por el Colectivo de Abogados José Alvear Restrepo, el Centro de Estudios del Trabajo, CENSAT Agua Viva, ColJusticia, la Central Unitaria de Trabajadores, la Escuela Nacional Sindical y el Instituto Latinoamericano para una Sociedad y un Derecho Alternativo, se presentó teniendo en cuenta que desde la discusión, hasta la firma del TLC en el año 2011 se sostuvo un prolongado debate producto de las violaciones sistemáticas a los derechos humanos y laborales, en específico a las libertades sindicales, asesinatos, desapariciones forzadas, atentados de muerte, contra sindicalistas en Colombia, por lo que fue necesario que con la firma del tratado se incluyera la protección y garantías de los derechos humanos.

Sin embargo, el informe señala que la realidad es otra, y **no se cumplieron las expectativas frente al crecimiento económico, pero tampoco sobre la garantía de los derechos civiles, ambientales y culturales.**

**Colombia aumentó sus exportaciones a Canadá solo un 12%, mientras que Canadá hacia Colombia lo hizo un 38%** principalmente en productos agropecuarios, industria liviana, productos químicos, y confecciones. Por otra parte, hay una baja presencia de sindicatos dentro de las empresas canadienses, el documento revela que sólo existen **5 sindicatos en 95 compañías** a pesar de que en el Acuerdo de Cooperación Laboral suscrito entre ambos países, la libertad sindical es una prioridad.

Además, en la exposición del informe se hicieron presentes líderes de comunidades donde operan empresas canadienses específicamente del sector minero-energético, teniendo en cuenta que **este TLC está “enfocado casi de manera exclusiva a este sector.** La participación de las exportaciones de bienes minero-energéticos a Canadá entre 2006 y 2014 se ha mantenido constante en un 60%”, dice el documento. Esto ha representado diversos impactos para algunas poblaciones.

Específicamente se denunció que en Sumapaz la empresa Alange Energy, **ha contaminado las cuencas hidrográficas, se han destruido lugares sagrados** para las comunidades y además se **ha privatizado el agua**;  en Puerto Gaitán, Meta, Pacific Rubiales Energy, ha generado la criminalición y judicialización de quienes han defendido sus territorios, como el caso de Héctor Sánchez quien estuvo varios meses en prisión. Así mismo, comunidades indígenas de otras zonas del país donde hay presencia de actividades minero-energéticas por parte de empresas canadienses **denuncian la desarticulación del tejido social, problemas de salubridad, pobreza y desarraigo cultural.**

También concluye que dos empresas canadienses buscan implementar el método no convencional de explotación conocido como **fracking en los departamentos de Cesar y Magdalena,** a pesar de que existe evidencia sobre los daños ambientales de dicha práctica.

A lo anterior se suma que las empresas del sector minero aportan poco al recaudo fiscal y se benefician plenamente de exenciones tributarias. Es decir que pese a las expectativas con del TLC con Canadá, los beneficios para el país han sido pocos y los impactos para las comunidades son múltiples, afectando sus derechos humanos, ambientales y culturales.

[Balance TLC Colombia Canada](https://es.scribd.com/doc/310975071/Balance-TLC-Colombia-Canada "View Balance TLC Colombia Canada on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_93775" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/310975071/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-nRGyir9lMN9kVqN13YLR&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
