Title: Buenaventura volverá a marchar ante incumplimientos del gobierno
Date: 2017-10-20 17:49
Author: AdminContagio
Category: Nacional, Resistencias
Tags: buenaventura, Paro cívico
Slug: buenaventura-vuelve-a-marchar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/buenaventura-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alvaro Miguel] 

###### [20 Oct 2017] 

Una serie de movilizaciones ya se realizan en Buenaventura, debido a los reiterados incumplimientos del gobierno nacional, pese a que hace 4 meses, en el marco del paro cívico en esa ciudad, el Estado se comprometió con una serie de medidas que buscaban sacar de la crisis social y económica en la que se encuentran los habitantes.

Si bien se ha avanzado en la mesa ambiental, según asegura Javier Torres, líder de la comunidad, el resto de las mesas temáticas no han avanzado lo suficiente, debido a que no hay presencia de los representantes del gobierno, como se había prometido.

**"Le decimos al gobierno que el paro está suspendido y no levantado, ya que estamos pendientes hasta que se cumplan los acuerdos", a**firma Torres. Por el momento lo que se adelanta es una serie de movilizaciones para hacer un llamado de atención al gobierno, pero si no se avanza, especialmente en la Ley de fondos para Buenaventura, podría ser posible que nuevamente la población entre en paro.

Este jueves, los pobladores realizaron una nueva marcha en la que participaron cerca de 300 personas exigiendo el cumplimiento de los acuerdos. Asimismo, el comité del paro cívico de hace unos meses prepara otra gran movilización en los próximos días, también el **miércoles 25 de octubre se realizará una reunión **con miembros del Gobierno para revisar los avances de los acuerdos. [(Le puede interesar: ¿Qué ha pasado tras el paro de Buenaventura?)](https://archivo.contagioradio.com/que-ha-pasado-a-dos-meses-del-paro-civico-en-buenaventura/)

<iframe id="audio_21590021" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21590021_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
