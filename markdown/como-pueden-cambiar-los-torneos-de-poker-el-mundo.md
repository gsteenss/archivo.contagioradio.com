Title: ¿Cómo pueden cambiar los torneos de póker el mundo?
Date: 2017-09-09 18:20
Category: Uncategorized
Slug: como-pueden-cambiar-los-torneos-de-poker-el-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/play-886344_960_720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

**[Jugadores bajo una misma bandera](https://es.888poker.com/magazine/poker-world/wsop-1-game-1-flag-uniting-the-world/)**

El póker es un juego que puede ser disfrutado por cualquiera. No importa la clase social ni la procedencia, pues lo único que se necesita es conocer sus reglas. El embajador de 888 Poker Bruno Kawauti resumía bien este concepto en un tête-à-tête que tuvo lugar en el 888Live Barcelona: “No importa de dónde vengamos, todos somos seres humanos. Debería haber una sola bandera”. Esta unión puede verse reflejada en los torneos internacionales de póker, en los cuales los jugadores no prestan atención a los orígenes de sus rivales, sino a sus habilidades.

**Las World Series of Poker**

Por ello, se puede afirmar que esta clase de eventos ayudan a mejorar la comunidad de póker e incentivan un mundo unido. Si observamos uno de los eventos más importantes del mundo del póker, las World Series of Poker (WSOP), veremos un claro ejemplo de [jugadores bajo una misma bandera](https://es.888poker.com/magazine/poker-world/wsop-1-game-1-flag-uniting-the-world/). Desde el año 2006, 1.329 brazaletes de oro de esta competición han sido repartidos en 42 países. Estados Unidos y Canadá son los que obtienen más beneficios económicos por el momento, seguidos de Reino Unido, Francia y Alemania.

Hasta ahora, 131 países han tenido representación en las competiciones de las WSOP, ayudando a crear una harmonía entre territorios. Asimismo, existe cierta similitud entre este sentimiento de unión que genera el póker y el que produce los juegos olímpicos. En ellos, los participantes de cada prueba se reúnen en un certamen único para demostrar que las habilidades no entienden de nacionalidades. Por ello, aunque deseemos que los deportistas de nuestros países venzan, somos capaces de disfrutar las hazañas del resto.

**Los juegos Olímpicos**

Al principio de las [olimpiadas de Río de Janeiro](http://www.notimerica.com/deportes/noticia-imagen-diversidad-cultural-juegos-olimpicos-20160808212859.html), la fotografía de un partido de voleibol entre las selecciones de Alemania y Egipto dio la vuelta al mundo. En ella, se ven dos jugadoras, una de cada equipo. La fotografía dejó claro la variedad y mezcla de culturas que este tipo de eventos acoge.

Los Juegos Olímpicos han ayudado a [mejorar las relaciones entre países](http://www.comillas.edu/es/noticias-comillas/4210-el-deporte-une-paises-y-les-ayuda-en-sus-relaciones-internacionales) durante toda su historia. En 1972, por ejemplo, China y Estados Unidos reestablecieron relaciones diplomáticas gracias a un torneo de tenis de mesa. Del mismo modo, en el 2012 Reino Unido llevó a cabo una campaña llamada GREAT, cuyo objetivo era mejorar la imagen local entre los extranjeros.

**Póker solidario**

Lo cierto es que los deportes no solo ayudan a que los países se acepten y respeten, sino que también aportan su granito de arena a través de eventos solidarios. Este es el caso de Julio Moura, un jugador de póker que dona todos sus premios a hospitales. Otro ejemplo lo protagonizó el jugador de fútbol Neymar, que organizó un torneo de póker benéfico en Barcelona el pasado mes de marzo.
