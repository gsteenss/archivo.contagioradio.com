Title: CIDH registra 14 asesinatos de defensores de DDHH en Latinoamérica en 2017
Date: 2017-02-08 18:43
Category: DDHH, El mundo
Tags: Asesinatos defensores de DDHH, CIDH
Slug: cidh-registra-14-asesinatos-defensores-ddhh-latinoamerica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/IMG-20170118-WA0017-e1484764649814.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CONPAZ 

###### [8 Feb 2017] 

Ya son 14 los asesinatos que contabiliza en el 2017 la Comisión Interamericana de Derechos Humanos (CIDH), y por lo cual expresa su preocupación por la situación de los lideres sociales y ambientalistas, en Colombia, México, Guatemala y Nicaragua.

### Colombia 

En medio de la situación de constantes amenazas, hostigamientos y estigmatizaciones contra los defensores de derechos humanos, **se han presentado crímenes fatales contra siete líderes.** La CIDH hace el recuento de las muertes. El 6 de enero fue asesinado el joven líder indígena Olmedo Pito García, miembro del Movimiento Político y Social Marcha Patriótica en Cauca. El 7 de enero Aldemar Parra García, líder comunitario que luchaba contra el desplazamiento forzado de su comunidad por la destrucción ambiental causada por industriales mineras. El 10 de enero Juan Mosquera, reconocido líder comunitario de la región del Bajo Atrato y su hijo Moisés Mosquera. El 11 de enero fue hallado muerto Jose Yimer Cartagena, líder campesino de Marcha Patriótica Alto Sinú y vicepresidente de ASODECAS. Luego , Emilsen Manyoma Mosquera, lideresa de CONPAZ y su esposo Joe Javier Rodallega fueron asesinados en Buenaventura.  Asimismo, el 26 de enero habría sido asesinada Yoryanis Isabel Bernal Varela, quien trabajaba en la defensa de los derechos de las mujeres indígenas Wiwa en la región de la Sierra Nevada de Santa Marta.

### México 

La CIDH, ha conocido del homicidio el 15 de enero de **Isidro Baldenegro,** reconocido activista ambiental indígena que luchó contra la explotación forestal ilegal del bosque antiguo de la Sierra Madre Occidental en territorio tarahumara/raramuri, en Chihuahua, y ganador del premio medioambiental Goldman en 2005. Baldenegro es el cuarto activista en la comunidad de Coloradas de la Virgen en ser asesinado en los últimos doce meses como represalia por la defensa de bosques y tierras ancestrales. Además, la Comisión fue informada del asesinato de **Juan Ontiveros Ramos,** otro defensor de derechos humanos y ambientales de la misma comunidad que el ganador del Goldman.

### Guatemala 

El 16 de enero fue asesinada **Laura Leonor Vázquez Pineda,** antigua activista del movimiento pacífico de resistencia contra el proyecto minero de San Rafael. Según la información de la CIDH,  la lideresa, habría sido encontrada en su casa con disparos en la cabeza. Adicionalmente, el 17 de enero **fue asesinado Sebastián Alonso Juan,** indígena defensor de los derechos de la naturaleza, en Huehuetenango, durante una protesta pacífica en contra de proyectos hidroeléctricos Pojom I y II.

### Nicaragua 

Tres miembros de la comunidad indígena Miskitu habrían sido asesinados en lo que va del año. De acuerdo con la Comisión, los Miskitu están en una situación particular de riesgo al estar en medio de un conflicto por su territorio, en un contexto de falta de implementación por el Estado de reconocimiento oficial de la propiedad indígena sobre sus tierras ancestrales. Aunque la CIDH ha llamado la atención al Estado para proteger a los miembros de la comunidad, fue asesinada **una familia entera, conformada por Bernicia Dixon Peralta, su esposo Feliciano Benlis Flores y su hijo de 11 años, Feliciano Benlis Dixon.** El triple asesinato se habría dado  en represalia porque la familia había llevado su asunto a las cortes de justicia, a través de lo cual obtuvo legalmente el título de propiedad sobre sus tierras.

Frente a este panorama, la CIDH, le reitera a los Estados su deber de garantizar la defensa de los derechos humanos y de la naturaleza. Asimismo, asegura que los gobiernos deben otorgar las medidas de seguridad necesarias para los líderes sociales en riesgo, y además debe adelantar las investigaciones necesarias para esclarecer esos hechos. Finalmente, reitera que estos crímenes fracturan comunidades y procesos organizativos importantes que se vienen desarrollando en América Latina.

###### Reciba toda la información de Contagio Radio en [[su correo]
