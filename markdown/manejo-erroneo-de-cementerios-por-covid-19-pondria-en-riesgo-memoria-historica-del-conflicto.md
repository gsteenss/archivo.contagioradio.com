Title: Manejo erróneo de cementerios por Covid-19 pondría en riesgo memoria histórica del conflicto
Date: 2020-05-13 01:28
Author: CtgAdm
Category: Actualidad, Memoria
Tags: Cementerios, Covid 19, Desaparición forzada, Ejecuciones Extrajudiciales
Slug: manejo-erroneo-de-cementerios-por-covid-19-pondria-en-riesgo-memoria-historica-del-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Cementerio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Orlando Fals Borda

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de la pandemia del Covid-19 ha quedado en evidencia las deficientes medidas de salubridad pública para atender las sepulturas de los fallecidos por el virus, no solo en los cementerios, también en sus políticas y las medidas de salubridad para las personas que trabajan en estos lugares y quienes arriesgan su vida al entrar en contacto directo con la enfermedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Siguiendo con la ruta de indagación e investigación de los cementerios de Colombia, la Corporación Orlando Fals Borda, ha insistido **en que urge una ruta de intervención para establecer protocolos seguros y una capacitación para los sepultureros en manejo y disposición de los cuerpos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

César Santoyo, presidente de la Corporación señala que los trabajadores de los cementerios enfrentan condiciones precarias y no cuentan con garantías laborales, incluso muchos de ellos no cuentan con una formalidad en su forma de contratación, lo que agrava más su exposición ante el Covid-19 y en las prácticas sanitarias y de conservación de estos espacios físicos. [(Le puede interesar: Comprender fenómeno de los desaparecidos en Colombia aporta a la memoria histórica del país)](https://archivo.contagioradio.com/comprender-desaparecidos-capitulo-historia-colombia/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Trabajadores de cementerios requieren garantías laborales

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En segundo lugar, existe una limitación de inversión frente a materiales higiénicos sanitarios y de bioseguridad pues los municipios no contarían con los recursos para invertir en estas herramientas como dobles guantes, máscaras de filtración N95 como mínimo, botas y batas desechables, más otros insumos como vacunas deHepatitis B, Difteria, Tosferina y Tétanos que cuestan un estimado de \$435.OOO.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la organización estiman que **en cada uno de los 1.122 municipios en Colombia existe un cementerio, por lo que existe cerca de mil personas que están en riesgo permanente** por lo que solicitan a Alcaldías y Gobernaciones que se revise la situación real de cada cementerio y la condición de sus trabajadores que en medio de su labor podrían resultar contagiados de no tomarse las medidas requeridas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Santoyo advierte que existe el riesgo de que en algunas poblaciones locales se dé la posibilidad de que las personas que permanecen sin ser identificadas o no hayan sido reclamadas por sus familiares, sean mezcladas con las personas que fallecieron en el marco de la emergencia del Covid-19, **"lo que sería muy grave para la memoria histórica y los avances que se puedan hacer en casos de desaparición forzada y ejecuciones extrajudiciales**". [(Le puede interesar: Exigen activar mecanismos de alerta para identificar 286 cuerpos en cementerios de Nariño)](https://archivo.contagioradio.com/exigen-activar-mecanismos-de-alerta-para-identificar-286-cuerpos-en-cementerios-de-narino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por solo mencionar un caso, en Tumaco Nariño, según el Colectivo Orlando Fals Borda el número de personas dadas por desaparecidas **en el marco del conflicto podrían llegar a ser más de 150 personas, mientras en 423 cementerios existirían más de 26.000 cuerpos sin identificar, de acuerdo a un informe del Ministerio del Interior.** [(Lea también: En cementerio de Tumaco podrían haber más de 150 personas sin identificar)](https://archivo.contagioradio.com/en-cementerio-de-tumaco-podrian-haber-mas-de-150-personas-sin-identificar/)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "Hemos insistido en todos los escenarios en una política pública que actúe y garantice que los cementerios no solo reposan personas fallecidas sino la oportunidad de superar la etapa de conflicto que tiene que ver con la identificación de personas víctimas de desaparición forzada - César Santoyo

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Muchedumbre y sepelios masivos también violan la cuarentena

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Algunos de los casos que han despertado inquietud frente a este tema han ocurrido en lugares como Bello, Antioquia donde el pasado 7 de abril una multitud violó la cuarentena para asistir al entierro de alias El Oso, jefe de una banda criminal de ese municipio o el pasado 6 de mayo en Tumaco cuando casi 50 personas sin el uso de tapabocas ni guantes se congregaron en el entierro de un joven pese a que los funerales están limitados a la presencia de tan solo cinco personas. [(Le puede interesar: Identificación de personas dadas por desaparecidas llega a tres nuevos territorios)](https://archivo.contagioradio.com/identificacion-de-personas-dadas-por-desaparecidas-llega-a-tres-nuevos-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante la posible evidencia de una inhumación ilegal en medio de la oscuridad en un municipio de Nariño y que fue dada a conocer a la [Corporación Orlando Fals Borda](https://twitter.com/colectivo_ofb), su director advierte que los entierros y accionar clandestino en los cementerios ahora no solo están mediados por la zozobra que causa la presencia de actores armados legales o ilegales, sino que ahora existe el riesgo de que las personas enterradas podrían ser positivos al covid-19, lo que bloquea de algún modo la posibilidad de documentación para identificación a futuro.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
