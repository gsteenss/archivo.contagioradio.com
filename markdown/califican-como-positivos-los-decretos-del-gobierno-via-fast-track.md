Title: Califican como positivos decretos del gobierno vía Fast Track
Date: 2017-05-30 15:58
Category: DDHH, Entrevistas, Paz
Tags: acuerdos de paz, Implementación de Acuerdos
Slug: califican-como-positivos-los-decretos-del-gobierno-via-fast-track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [30 May 2017] 

Con los decretos firmados por el presidente Santos durante este fin de semana, muchas serían las normas que empezarían a regir para el Acuerdo de Paz y que permitirían una implementación más eficaz de la mismas. Entre los decretos se encuentran los que tienen que ver con el tema de **Tierras, desmonte del paramilitarismo y garantías para la participación política.**

### **El asunto urgente de la Tierra en Colombia** 

El punto de Tierras ha sido uno de los más prioritarios durante la implementación de los acuerdos de paz, debido al lento avance que había presentado, el presidente Santos firmó un decreto por el cual se a**doptan medidas para facilitar la implementación de la Reforma Rural Integral** y específicamente el procedimiento para el acceso y la formalización y la creación del Fondo de Tierras.[Consulte acá el decreto 902 de 2017](https://es.scribd.com/document/349887451/Decreto-902-Del-29-de-Mayo-de-2017-Tierras)

Otro decreto urgente en términos de su aprobación para la implementación de los Acuerdos es el denominado Pago por impuestos a través de Obras, que pretende que las **empresas mineras paguen impuestos a través de la construcción de obras en los territorios que han sido afectados por el conflicto armado**. [Consulte acá el decreto 883 de 2017](https://es.scribd.com/document/349889051/Decreto-883-Del-26-de-Mayo-de-2017mineria)

Martínez señaló que frente a este decreto estaría en duda la constitucionalidad o no, debido a las nuevas condiciones expuestas por la Corte Constitucional en donde el presidente no podría expedir nuevos impuestos. Sin embargo, todos los decretos serán estudiados por la Corte que decidirá finalmente si son o no aprobados. Le puede interesar

De igual forma, Santos firmó la creación del **Programa Nacional Integral de Sustitución de Cultivos de uso Ilícito**, un decreto que se hacía urgente para poner en marcha los planes de sustitución que ya se adelantan en algunas regiones del país y establecer los mecanismos por los cuales se llevará a cabo la sustitución. [Consulte acá el decreto 896 de 2017](https://es.scribd.com/document/349889512/Decreto-896-Del-29-de-Mayo-de-2017pnis)

### **Garantías para el tránsito de las FARC-EP a la reincorporación** 

Entre las medidas que se encontraban estancadas se encontraban las que permitían el avance en la reincorporación de las FARC-EP a la vida civil, en ese sentido Santos firmó el decreto que **modifica la estructura de la Agencia Colombiana para la Reintegración de personas y grupos alzados en armas, de forma** tal que ofrezca las herramientas y condiciones necesarias para garantizar este proceso. [Consulte acá el decreto 897 de 2017](https://es.scribd.com/document/349889997/Decreto-897-Del-29-de-Mayo-de-2017angencia)

De igual forma se conformó el Sistema Integral para el Ejercicio de la Política, no obstante Diego Martínez recordó que en el Congreso hay una serie de medidas en materia de **reforma política que deben ir de la mano de las necesidades que tienen los guerrilleros de las FARC-EP**. [Consulte acá el decreto 895 de 2017](https://es.scribd.com/document/349890256/Decreto-895-Del-29-de-Mayo-de-2017-Sistema-Politico)

### **Las víctimas avanzan en la búsqueda de Verdad Justicia y Reparación**

El eje central del acuerdo de paz son las víctimas del conflicto armado en esa medida, una de las preocupaciones que las mismas habían venido manifestando tenía que ver con el **retraso en términos de las normativas que les permiten encontrar Verdad, Justicia y Reparación**.

En este sentido Santos realizó la aprobación del decreto que crea una unidad especial de investigación para el desmantelamiento de las organizaciones y conductas criminales. El segundo decreto que fue aprobado tiene que ver con el **sistema de garantías de seguridad para el ejercicio de la política**, que ofrece mecanismos para la protección de defensores de derechos humanos y comunidades afectadas por el conflicto. [Consulte acá el decreto 898 de 2017. ](https://es.scribd.com/document/349890564/Decreto-898-Del-29-de-Mayo-de-2017victimas)

### **¿Qué viene para la implementación del Acuerdo de Paz?** 

Esta semana en el Congreso de la República, iniciarán los debates sobre la Ley Estatutaria de la JEP, que con las nuevas medidas expresadas por la Corte Constitucional, podría ser alterada, Diego Martínez manifestó que “esperamos que el Congreso no modifique los temas esenciales impuestos en la ley estatutaria porque **es un requisito fundamental para proporcionar seguridad jurídica a los integrantes de las FARC**”.

A su vez, agregó que tiene que haber una voluntad política para aplicar lo acordado, sobre todo de parte del gobierno que ha demostrado “**una falta de institucionalidad”** que debe aplicar una política de paz, en ese sentido Martínez hizo un llamado para que se ponga en práctica el criterio de simultaneidad e integralidad que se encuentran en los acuerdos.

<iframe id="audio_18989551" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18989551_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
