Title: Gobierno a responder con hechos a campesinos en paro nacional indefinido
Date: 2017-10-24 14:41
Category: Movilización, Nacional
Tags: acuerdos de paz, campesinos en el cauca, Cauca, Movilización social, Paro Indefinido, Paro Nacional
Slug: comunidades-y-organizaciones-redactaron-pliego-de-peticiones-para-el-gobierno-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/campesinos-de-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Radio del Sur] 

###### [24 Oct 2017] 

En el marco de la movilización social, que hace parte del paro nacional indefinido, las comunidades y en el Cauca salieron a las calles exigiéndole al Gobierno Nacional **que cumpla los acuerdos ya pactados en diferentes aspectos** y que se respete la vida de los campesinos y los líderes sociales.

De acuerdo con Deivin Hurtado, integrante de la Red de Derechos Humanos del Sur Occidente Colombiano Francisco Isaías Cifuentes, las primeras movilizaciones se están realizando principalmente en el municipio de Patía, Cajibío y en Caldono. Aseguró que las manifestaciones **se están desarrollando de forma pacífica** sobre la carretera Panamericana.

La Red tiene estimado que **3 mil personas están participando de la jornada de movilización**, que se está llevando a cabo desde el 23 de octubre. Sin embargo, denunciaron que ha habido intimidaciones del ESMAD, “más de 50 efectivos del ESMAD estuvieron en Cajibío y las comunidades les manifestaron que van a seguir concentradas exigiendo sus derechos”. (Le puede interesar: ["Así será el paro nacional indefinido a partir de este lunes"](https://archivo.contagioradio.com/23-de-octubre-hora-cero-para-el-paro-nacional/))

### **Comunidades y organizaciones sociales redactaron pliego de peticiones al Gobierno Nacional** 

Paralelamente a las movilizaciones, diferentes organizaciones **redactaron un pliego de peticiones** para el Gobierno Nacional en donde indican que el incumplimiento de los diferentes acuerdos, incluido el Acuerdo de Paz, “pone en riesgo el cierre definitivo del conflicto armado”.

Entre las peticiones, **incluyen la implementación de la Reforma Rural Integral**, una nueva visión para la solución al problema de las drogas ilícitas, la protección y garantías para el respeto a los derechos humanos y las garantías para la participación política de las personas en los territorios.

Deivin Hurtado manifestó que el pliego, **se construyó desde las regiones y recoge muchas de las exigencias de las comunidades**. También, las propuestas se han socializado en asambleas de diferentes territorios teniendo como objetivo que “las comunidades mejoren su calidad de vida a través del cumplimiento de los acuerdos de la Habana”. (Le puede interesar: ["Convocan a paro nacional indefinido a partir del 23 de octubre"](https://archivo.contagioradio.com/convocan-a-paro-nacional-indefinido-a-partir-del-23-de-octubre/))

El pliego de carácter nacional, **“se hizo bajo la buena fe de que el Gobierno Nacional va a cumplir”**. Dijo que las comunidades están dispuestas y abiertas al diálogo para seguir “luchando por los derechos” y que son responsabilidad constitucional del Gobierno. Recordó que la movilización seguirá de forma indefinida y pacífica “a través de las únicas acciones que le ha dejado el Estado a las comunidades que son, lastimosamente, las acciones de hecho”.

[Pliego MIA 23 Oct 2017](https://www.scribd.com/document/362499000/Pliego-MIA-23-Oct-2017#from_embed "View Pliego MIA 23 Oct 2017 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="audio_21661488" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21661488_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="doc_59327" class="scribd_iframe_embed" title="Pliego MIA 23 Oct 2017" src="https://www.scribd.com/embeds/362499000/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-hA2xEcwr9RKg4OrZ6po5&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### **Rec**iba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
