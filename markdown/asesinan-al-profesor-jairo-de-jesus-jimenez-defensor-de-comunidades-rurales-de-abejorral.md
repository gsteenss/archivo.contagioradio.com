Title: Asesinan al rector Jairo de Jesús Jiménez, defensor de comunidades rurales de Abejorral
Date: 2020-04-28 19:09
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Antioquia, Jairo Jesús Jiménez
Slug: asesinan-al-profesor-jairo-de-jesus-jimenez-defensor-de-comunidades-rurales-de-abejorral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Jairo-Jesús-Jiménez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Jairo de Jesús Jiménez/ FECODE

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La noche de este 27 de abril fue hallado sin vida al interior de su casa, Jairo de Jesús Jiménez, rector de la Institución Educativa Zoila Duque Baena del municipio de Abejorral, en Antioquia. Para febrero de 2020, el sindicato de maestros de Antioquia alertó el creciente registro de amenazas y asesinatos a profesores en el departamento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según **Óscar Duque, docente desde 2009, y que compartió años de trabajo con Jairo Jesús Jiménez**, el rector era un "gran líder que siempre laboró en el sector rural", donde vio un espacio para entregar su conocimiento a las personas más desprotegidas del municipio. [(Le puede interesar: Su compromiso con la educación hoy tiene bajo amenaza a profesores del Cauca)](https://archivo.contagioradio.com/compromiso-con-la-educacion-hoy-tiene-bajo-amenaza-a-profesores-del-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque su compañero relata que Abejorral ha sido uno de los municipios que ha permanecido en relativa calma después de la firma del Acuerdo de Paz, información preliminar sugiere que fue atacado con arma blanca mientras las autoridades señalan que hasta el momento se desconocen los móviles y si existían amenazas previas contra Jairo Jesús.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, la **Unión Sindical de Directivos Docentes del Departamento de Antioquia** (USDIDEA) y [FECODE](https://twitter.com/fecode)han exigido que se adelanten las investigaciones necesarias para dar con los responsables del asesinato del rector.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante su gestión en la Institución Educativa Zoila Duque Baena, con el trabajo mancomunado de la comunidad educativa, para 2016 se alcanzó el logro como mejor institución educativa de puntajes en pruebas SABER en básica primaría y a nivel nacional el segundo lugar.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Jairo, relata su compañero, laboró más de 25 años en el magisterio de Antioquia y lo recuerda como la persona que siempre creyó que"la educación era pensar en lo humano, esa fue su gran enseñanza". En el departamento, advierten que 161 maestros denunciaron amenazas durante 2019, y que para febrero, esta cifra llegaba a 20 personas.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
