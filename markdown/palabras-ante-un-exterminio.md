Title: Palabras ante un exterminio
Date: 2018-02-04 16:11
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Amenaza a líderes sociales, Asesinan a líderes
Slug: palabras-ante-un-exterminio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Diseño-sin-título.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 4 Feb 2017 

[Temístocles Machado, Jair Cortés, Liliana Astrid Ramírez, Nixon Mutis, Leidy Amaya, por supuesto estos son solo algunos de los nombres de personas que fueron asesinadas por razones políticas en Colombia, la lista es muy larga y aumenta… más de doscientos asesinatos de líderes sociales en la segunda década del siglo XXI solo pueden resumirse en una frase que no gusta a los defensores de esta democracia de papel: el Estado colombiano, es un estado fallido.  ]

[No es claro, pero de seguro las razones jurídicas y presupuestales que implica reconocer gubernamentalmente que este exterminio de personas es un fenómeno sistemático, mantienen al nobel de paz Juan Manuel Santos y su escudero el ministro de defensa señor Luis Carlos Villegas, soportando con lo poco que les queda de gobierno el argumento barato y falso de que las personas están siendo asesinadas por líos de faldas y que lo que ocurre aquí no es una matanza sistemática.  ]

[No importa si son las multinacionales, los terratenientes, los clientelistas, los funcionarios corruptos y sus familias adineradas los que indirectamente se benefician por esta ola de asesinatos. Para nadie es un secreto que el corazón de la política palpita en las ciudades, pero la sangre, el sudor y las lágrimas continúan siendo derramados en los campos. Si mataran a 5 militantes del centro democrático en Bogotá, el escándalo sería mundial… pero como es la gente más humilde la que muere, como están matando a verdaderos políticos es decir los que trabajan por la comunidad sin sueldo, entonces el olvido, la noticia efímera, o el irrespeto rastrero de negar sistematicidad cuando prestigiosos centros de estudios sociales lo han concluido, sella el marco nefasto de un país protagonista de la crisis política más grave de nuestro continente.]

[Acalladas las palabras, eliminados los cuerpos, solo nos queda en la memoria, la obra de nuestros líderes sociales, que siguen siendo asesinados, ante los ojos de una sociedad que sí observa lo que pasa, pero que se ha vuelto tan indolente, que cree que no tiene responsabilidad o que ni siquiera tiene por qué sentir pena y dolor por tamaña injusticia. Es la noche más oscura de nuestros líderes sociales, es la realidad de un presente que se vuelve una pesadilla, pero que a pesar de todo y tanto, no culmina por empañar ese futuro que ellos vislumbraron para Colombia.]

[Con miedo pero sediciosamente, con cautela pero con determinación, con perseverancia indomable, con llanto rabioso que culmina en una sonrisa quimérica, el mejor homenaje que podemos hacer a nuestros líderes asesinados, el monumento más digno por su injusta muerte, es continuar promoviendo por todos medios y escenarios posibles ese país que nos espera a la vuelta de la luchas. La semilla que yace sobre su cuerpos, es el alimento moral para continuar luchando en nuestros barrios, escuelas, universidades, espacios laborales, sean urbanos o rurales, físicos o simbólicos, sea de día o de noche, y continuar creyendo como ellos lo hicieron hasta el día de su prematura muerte, que este país sí puede cambiar; ellos ahora no están, pero su deseo y su lucha, fueron y seguirán siendo el impulso, seguirán siendo la vocación de orientar la vida misma hacia el sueño indomable de transformar la terrible situación que atraviesa nuestra sociedad. Ese impulso hoy crece, crece y crece, por todos los rincones de la patria.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
