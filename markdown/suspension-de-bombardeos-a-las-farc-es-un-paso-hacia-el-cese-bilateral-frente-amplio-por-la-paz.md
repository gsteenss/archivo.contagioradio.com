Title: Suspensión de bombardeos a las FARC es un paso hacia el cese bilateral: Frente Amplio por la Paz
Date: 2015-03-11 19:18
Author: CtgAdm
Category: Nacional, Paz
Tags: alirio uribe, Angela Maria Robledo, FARC, Fuerzas militares, Humberto de la Calle, Juan Manuel Santos, paz, Procurador Alejandro Ordoñez
Slug: suspension-de-bombardeos-a-las-farc-es-un-paso-hacia-el-cese-bilateral-frente-amplio-por-la-paz
Status: published

##### Foto: pulzo.com 

<iframe src="http://www.ivoox.com/player_ek_4200354_2_1.html?data=lZedkpiZeI6ZmKiakp2Jd6KomZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitPZz9nSjabRtM3d0JDd0dePsMKfscbnjcjJsMbW08aYzsaPqMbXytjWh6iXaaOnz5DRx9GPp8bnxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ángela María Robledo, Representante a la Cámara Partido Verde e integrante del Frente Amplio por la Paz] 

<iframe src="http://www.ivoox.com/player_ek_4200366_2_1.html?data=lZedkpiaeo6ZmKiakpyJd6KmmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitPZz9nSjabRtM3d0JDd0dePsMKfscbnjcjJsMbW08aYzsaPqMbXytjWh6iXaaOnz5DRx9GPp8bnxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe, Representante a la Cámara del Polo Democrático e integrante del Frente Amplio por la Paz] 

Este martes el Presidente Juan Manuel Santos, anunció el **cese de bombardeos contra los campamentos de la guerrilla a de las FARC-EP**, por parte de las Fuerzas Militares, lo que ha generado un sin número de opiniones, críticas y sugerencias  por parte de distintos sectores políticos y sociales.

Desde el Frente Amplio por la Paz, Alirio Uribe, representante a la Cámara, dice estar “**contento con la medida del presidente**, el proceso de negociación va fuerte, y eso debe impulsar el cese bilateral” y resalta la decisión, genera confianza entre las partes,  “combinado con el no reclutamiento y el plan de desminado es un buen mensaje para el proceso de negociación”.

Así mismo, la representante a la Cámara del Partido Verde y también integrante del Frente Amplio por la Paz, Ángela María Robledo, expresó que es “**el gesto de reciprocidad que estábamos esperando de parte del gobierno de cara a últimas medidas tomadas por las FARC”.**

Robledo, resalta que este pronunciamiento es fruto de la comisión de los doce generales que han analizado la estrategia militar en conversaciones con las FARC.

Sin embargo, al representante Alirio Uribe, le preocupa el pronunciamiento de Santos que indica que  el ejército va realizar fuertes operaciones contra el ELN, lo que no contribuiría a la fase preliminar de negociaciones ELN, “**el presidente debió haber omitido esa información y no dar una orden de arremeter mas fuerte contra el ELN**”, dijo el vocero del Frente Amplio por la Paz.

Desde el sector de la oposición, el Procurador General de la Nación, Alejandro Ordoñez, criticó la decisión y afirmó que esta medida es un cese bilateral disfrazado,que se traducirá en la parálisis de la fuerza pública en contra de las FARC, para Ordoñez, se trata de **"un golpe más a la moral de las Fuerzas Militares de Colombia"**.

Frente a esas declaraciones, durante la mañana de este miércoles, Humberto de la Calle, Jefe de la Delegación del Gobierno en la Habana, anunció que se trata de una medida de **construcción de confianza dirigida a continuar en un desescalamiento del conflicto, debido al cumplimiento del cese unilateral de las FARC-EP** y sus decisiones sobre el no reclutamiento de menores, y el plan de desminado, al que también hacía referencia el representante Alirio Uribe.

\[embed\]https://www.youtube.com/watch?v=pNbjQaFPD8Q\[/embed\]
