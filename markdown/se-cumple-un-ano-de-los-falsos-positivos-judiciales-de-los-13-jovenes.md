Title: Se cumple un año de proceso judicial contra los 13 jóvenes
Date: 2016-07-08 11:18
Category: Judicial, Nacional
Tags: Falsos Positivos Judiciales, libertad a lxs 13
Slug: se-cumple-un-ano-de-los-falsos-positivos-judiciales-de-los-13-jovenes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/los-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Somosdignidad] 

###### [8 jul 2016] 

Después de un año del levantamiento de los falsos positivos judiciales a los 13 estudiantes, periodistas y defensores de derechos humanos que serían acusados por promover el terrorismo, la rebelión y agresión a servidores públicos, aún continúa el proceso **de defensa y demanda al Estado**. En conmemoración a este hecho, el 8 de julio se llevará a cabo el conversatorio 'Persecución, Paz y Derechos Humanos'.

De acuerdo con María Angélica Bogotá, vocera del proceso de los 13, y madre de Jhon Fernando Acosta y Lizeth Johanna Acosta, los momentos sufridos durante la captura y retención de las 13 personas fueron muy fuertes, "**ha sido impactante en la parte familiar** y personal, hemos tenido que pasar por señalamientos y [persecución, todo el tiempo vigilados](https://archivo.contagioradio.com/minuto-a-minuto-caso-de-jovenes-capturados-por-explosiones-en-bogota/)".

Las 13 personas víctimas de los[falsos positivos judiciales](https://archivo.contagioradio.com/minuto-a-minuto-caso-de-jovenes-capturados-por-explosiones-en-bogota/)se encuentran en proceso de iniciar una demanda al Estado debido a su captura y las formas en las que estas se llevaron a cabo, desde la imputación de cargos hasta los **señalamientos públicos** que hicieron diferentes autoridades sobre ellos, y continúan en el proceso de juicio por los hechos presentados como "pedreadas" en la Universidad Nacional de Colombia.

El conversatorio contará con la participación de familiares del colectivo 'Libertad a lxs 13', la Asociación de Familiares y Amigos de Presos Políticos, la organización de Presos Políticos de la Cárcel Picota, entre otras organizaciones.

<iframe src="http://co.ivoox.com/es/player_ej_12162771_2_1.html?data=kpeemJebe5Khhpywj5WcaZS1lZuah5yncZOhhpywj5WRaZi3jpWah5ynca7VjKbbyYqnd4a1mtHWxcaPhtDb0NmSpZiJhZKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
