Title: Así implementan la pedagogía de paz las comunidades en Tierra Alta
Date: 2016-08-11 13:22
Category: Nacional, Paz
Tags: ASODECAS, Diálogos de paz Colombia, pedagogía de paz Tierra Alta
Slug: asi-implementan-la-pedagogia-de-paz-las-comunidades-en-tierra-alta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Pedagogía-Paz-Tierra-Alta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [11 Ago 2016] 

Desde principios de este mes la Asociación Campesina para el Desarrollo del Alto Sinú, viene implementando en los corregimientos de Tierra Alta, Córdoba, **acciones pedagógicas en torno al proceso de paz y los acuerdos** a los que han llegado la guerrilla de las FARC-EP y el Gobierno colombiano. Las actividades se han desarrollado en Saiza, Batata y La Osa, y se espera que el próximo 24 de agosto se lleven a cabo en Palmira.

"Como anhelamos que se llegue al fin de la guerra (...) pensamos que ahora vamos a vivir un nuevo capítulo" asegura Luis Carlos Herrera, quien agrega que en los encuentros con las comunidades se han discutido aspectos relacionados con los derechos para las víctimas y la **problemática de titulación de predios que hay en el municipio** y las posibilidades de solucionarla con la implementación de los acuerdos de paz.

De acuerdo con Herrera a las comunidades les preocupa no tener títulos de sus predios; sin embargo, confían en que las **1.700 hectáreas de tierra que fueron apropiadas por una empresa**, en el marco de la Ley Forestal, entren al fondo de tierras y sean tituladas a sus legítimos propietarios y de esta manera haya inversión social para la construcción de colegios, hospitales y vías.

<iframe src="http://co.ivoox.com/es/player_ej_12514946_2_1.html?data=kpeik5mdeJehhpywj5WcaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYpcbWsNDnjK3S1NfJtsKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

 
