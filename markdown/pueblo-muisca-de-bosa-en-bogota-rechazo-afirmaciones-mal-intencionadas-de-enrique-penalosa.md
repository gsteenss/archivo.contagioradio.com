Title: Pueblo Muisca de Bosa rechaza afirmaciones "malintensionadas" de Peñalosa
Date: 2018-02-05 15:19
Category: DDHH
Tags: Bogotá, Enrique Peñalosa, indígenas de bogotá, Indígenas muiscas, muiscas
Slug: pueblo-muisca-de-bosa-en-bogota-rechazo-afirmaciones-mal-intencionadas-de-enrique-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/muiscas-bogota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: IDPAC] 

###### [05 Feb 2018] 

El pueblo Muisca de Bosa en Bogotá emitió un comunicado donde rechaza las afirmaciones del alcalde Enrique Peñalosa quien señaló que “los indígenas son los que tienen la culpa de que las obras estén paralizadas”.

Los indígenas le recordaron al alcalde que se debe respetar el **derecho a la consulta previa** en un territorio ancestral para realizar cualquier tipo de intervención y que sus palabras generan desinformación y ponen en riesgo a la comunidad indígena.

En el comunicado también indicaron que cuando Peñalosa se encontraba desarrollando un recorrido en bicicleta por la localidad, el 3 de febrero de 2018, “hizo comentarios **falsos, malintencionados y ofensivos** que desinforman a la comunidad en general e intenta ponerla en contra de nuestros pueblos”. Allí, Peñalosa culpó a los indígenas y sus “jefes” de ser los culpables de no permitir el inicio de diferentes obras.

Ante esto, los indígenas manifestaron que el pueblo Muisca **tiene autoridades tradicionales y no “jefes”** y el cabildo de Bosa es una “entidad pública de carácter especial reconocida por el Ministerio del Interior desde 1999” y cuyo asentamiento “tiene orígenes milenarios como territorio ancestral Muisca”. Por lo tanto, el pueblo indígena tiene el derecho constitucional a la consulta previa ante cualquier intención de intervención en su territorio. (Le puede interesar:["Con Peñalosa todo lo malo puede empeorar": Manuel Sarmiento"](https://archivo.contagioradio.com/con-penalosa-todo-lo-malo-puede-empeorar-manuel-sarmiento/))

### **Los comentarios señalan a los indígenas de opositores al desarrollo** 

El pueblo indígena Muisca informó que el alcalde los señaló de ser opositores al desarrollo al decir que los “jefes” bloquean la realización de obras en el sector. Esto, para los indígenas **“pone en riesgo la integridad física y cultural de la comunidad”,** a las autoridades tradicionales y amenaza la supervivencia cultural de todo el pueblo Muisca”. Además, sus señalamientos ponen a la comunidad indígena en contra de los demás habitantes del sector.

Ante esto, indicaron que el Alcalde está **desconociendo la Constitución** y las decisiones judiciales como el Consejo de Estado quien falló una tutela orientada a proteger los derechos fundamentales del pueblo indígena. Manifestaron que no es interés de ellos “ser obstáculo para el mejoramiento de la calidad de vida de la población” pero se debe garantizar un desarrollo que integre las formas tradicionales de vida de los indígenas.

Finalmente, le exigieron a Peñalosa que haga una **rectificación pública** y le solicitaron a las autoridades nacionales como la Procuraduría, Defensoría del Pueblo y Ministerio del Interior que adelante las acciones pertinentes para la protección de los derechos del pueblo Muisca.

[Pueblo Muisca Comunicado](https://www.scribd.com/document/370804248/Pueblo-Muisca-Comunicado#from_embed "View Pueblo Muisca Comunicado on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_72690" class="scribd_iframe_embed" title="Pueblo Muisca Comunicado" src="https://www.scribd.com/embeds/370804248/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-8K4n904VTmmTnklakDVP&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
