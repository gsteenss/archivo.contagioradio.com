Title: La falta de implementación de los Acuerdos de Paz impide la paz territorial
Date: 2017-11-17 14:22
Category: Nacional, Paz
Tags: acuerdos de paz, ForumSYD, misiones internacionales, organizaciones internacionales
Slug: la-falta-de-implementacion-de-los-acuerdos-de-paz-impide-la-paz-territorial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/forum-syd2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ForumSYD] 

###### [17 Nov 2017] 

Así lo han evidenciado más de 30 organizaciones internacionales a través de 4 misiones internacionales que han realizado en diferentes territorios. La próxima semana, empieza la quinta y última misión en cabeza de la organización internacional ForumSYD para **establecer mecanismos de incidencia en lo que respecta a la implementación de los Acuerdos de Paz. **

De acuerdo con Carlos Cárdenas, oficial de comunicaciones e incidencia de ForumSYD, las misiones hacen parte de un plan de acciones de incidencia que se propuso desde el primer semestre del año, en una plataforma que se llama **“Espacio de Cooperación para la Paz”**. Allí, las más de 30 organizaciones internacionales, han buscado “crear acciones conjuntas de incidencia en torno a la construcción de paz”.

### **Hallazgos de las misiones internacionales** 

Cárdenas indicó que en las misiones pasadas han podido trazar una línea común en donde es evidente **“el incumplimiento en la etapa de implementación”.** La primera misión, que la realizaron en el Bajo Atrato, evidenció, entre otras cosas, “las condiciones de inseguridad han aumentado debido a la presencia de paramilitares e integrantes del ELN”. (Le puede interesar: ["Organizaciones internacionales apoyan Acuerdos de Paz en Colombia"](https://archivo.contagioradio.com/organizaciones-internacionales-apoyan-acuerdos-de-paz-en-colombia/))

Para estas organizaciones, **“hay una preocupación muy grande frente a la negligencia de la Fuerza Pública** ante la presencia sobre todo de grupos paramilitares”. Cárdenas afirmó que las organizaciones pudieron observar “la cercanía de las bases paramilitares con los batallones y operativos de las Fuerzas Militares”.

Un hallazgo que ha hecho manera transversal es que **“como nunca antes, las comunidades tienen miedo de hablar”**. Cárdenas argumentó que esto se puede evidenciar incluso en espacios de confianza donde las comunidades cuentan con apoyo y acompañamiento internacional desde hace tiempo.

### **Interlocución con el Gobierno Nacional es difícil** 

Cárdenas enfatizó que, en los encuentros con las instituciones gubernamentales, donde comparten los resultados encontrados, **“los funcionarios se sienten limitados** para referirse a las posiciones institucionales”. Por esto, afirma que hay contradicciones con las versiones que brindan las instituciones y las que dan las comunidades en los territorios.

Dijo que es muy recurrente que la Fuerza Pública diga que “no necesitan enviar tropas a algún sector porque ahí todo está tranquilo y uno escucha a las comunidades de esa zona y dicen todo lo contrario”. (Le puede interesar: ["La paz territorial no se siente en los territorios": Misión internacional"](https://archivo.contagioradio.com/la-paz-territorial-no-se-siente-en-los-territorios-mision-internacional/))

Finalmente, Cárdenas indicó que **la quinta y última misión se realizará en el Huila y en Caquetá** donde ya han trabajado con las esas comunidades. Será una visita de 6 días y se reunirán con organizaciones sociales y con organizaciones de mujeres para observar la construcción de paz desde la perspectiva de género. Con estos espacios de intercambio y diálogo buscarán recoger diferentes miradas que incluyan a las comunidades y a las entidades gubernamentales.

<iframe id="audio_22142483" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22142483_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
