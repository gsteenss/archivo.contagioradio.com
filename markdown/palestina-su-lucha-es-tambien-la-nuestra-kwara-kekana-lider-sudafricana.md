Title: "Palestina: su lucha es también la nuestra" Kwara Kekana
Date: 2016-04-26 00:31
Category: DDHH, El mundo
Tags: BDS, Conflicto Israel-Palestina, Israel, Palestina
Slug: palestina-su-lucha-es-tambien-la-nuestra-kwara-kekana-lider-sudafricana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/palestina_17fa4bd5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

###### [26 Abril 2016]

"Tanto los palestinos que vivimos en Cisjordania y Gaza, como quienes estamos en Israel y el 50% que vivimos en diáspora, llevamos una vida de sufrimiento, bajo un régimen de colonización, ocupación y apartheid israelí, con un muro que nos separa entre sí e impide que salgamos a otros lados a buscar salud y educación".

**Puestos de control, robo de tierra, construcción de colonias ilegales, robo de recursos naturales, matanza de palestinos, ataques de colonos y quemas de casas**, son sólo algunos de los vejámenes que día a día tiene que enfrentar el pueblo palestino desde 1967, de acuerdo con Mahmoud Nawajaa, coordinador general del Comité Nacional Palestino de la campaña Boicot Desinversiones y Saciones a Israel BDS.

Según Nawajaa, del mismo modo en que se han creado organizaciones palestinas e israelíes que trabajan por la solución política al conflicto, a partir del cese de "la limpieza étnica del pueblo palestino", cientos de colectivos internacionales expresan su solidaridad con Palestina como parte de "una lucha que hoy día está globalizada" y a través de la que **"gente de todo el mundo nos está dando más y más esperanza para resistir a este régimen de apartheid y ocupación"**.

En otro punto, a 6.390 km, la sudafricana Kwara Kekana, integrante de la mesa directiva de BDS Sudáfrica, asegura que a la lucha de Palestina **"todo el mundo se ha unido, porque es un asunto de justicia"** que sus compatriotas consideran muy cercano, tanto por el sufrimiento que padecieron durante el apartheid sudafricano, como por la experiencia de resistencia que, sumada a la solidaridad internacional, les permitió poner fin al régimen.

"La primera enseñanza que compartimos con los palestinos es que el régimen israelí no va continuar por siempre. El pueblo palestino va a ser libre está pendiente es la cuestión del cuando. **Hoy compartimos las tácticas que utilizamos para aislar el régimen de apartheid sudafricano**, con distintas formas de boicot, desde el cultural y el académico, hasta el económico", agrega Kekana.

"La solidaridad internacional está creciendo todos los días y está logrando resultados que demuestran que nuestras luchas están siempre conectadas. **Hoy  en Sudáfrica vivimos después de la era del apartheid, pero hay un legado en contra del cual la gente sigue luchando** y conecta sus luchas con las del pueblo palestino, así como en Colombia pueden hacerlo otros movimientos, por ejemplo los estudiantiles y feministas", continúa la líder.

Quien desde otras latitudes también trabaja por la autodeterminación del pueblo palestino es el brasilero Pedro Charbel, coordinador de BDS Latinoamerica, quien junto a Kekana y a Nawajaa visitaron Colombia para hacer parte de la [[Semana Contra el Apartheid Israelí](https://archivo.contagioradio.com/mas-de-100-organizaciones-se-solidarizan-con-palestina-en-la-semana-contra-el-apartheid-israeli/)], en la que se entregó al Congreso una carta firmada por **cientos de organizaciones que exigen la no ratificación del TLC entre nuestro país e Israel**, teniendo en cuenta las consecuencias negativas que representa tanto para el pueblo palestino como para el colombiano.

De ratificarse el TLC, Colombia continuaría siendo cómplice de las múltiples violaciones cometidas por el Estado israelí, al que aún le sigue **comprando las armas con las que son atacados los palestinos**. "El TLC es un paso adelante en la normalización de las relaciones de opresión provocadas por Israel", asevera Charbel.

Para los palestinos esta ratificación representaría más espacio para la industria israelí, "desarrollada a partir de la ocupación y del sufrimiento. Significa que Colombia va a dar espacio privilegiado para un régimen que los oprime en todas las zonas de su vida". De acuerdo con Charbel **las tecnologías que se implementado contra el pueblo palestino van a ser ahora ejecutadas contra los colombianos** que trabajan para la garantía de derechos y en clave del proceso de paz.

Las principales demandas de los palestinos consisten en que se ponga fin a la ocupación de los territorios en Cisjordania, Jerusalen del este y la Franja de Gaza, además de lograr la igualdad de derechos, que cese el apartheid contra los palestinos que viven en territorio israelí y el retorno de quienes viven refugiados, según lo demanda la resolución 194 de la ONU.

"Creemos en la resistencia, tenemos la esperanza, somo el pueblo originario de Palestina, tenemos la perseverancia para seguir luchando, es simple no tenemos otra opción tenemos que resistir, nuestras luchas son unidas, **todos los pueblos del mundo tenemos que estar unidos luchando contra todas las formas de opresión** y nosotros les invitamos a hacer parte de nuestra lucha en contra del régimen de apartheid", concluye Nawajaa.

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
