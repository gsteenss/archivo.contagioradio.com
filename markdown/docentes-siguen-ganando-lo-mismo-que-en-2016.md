Title: Docentes siguen ganando lo mismo que en 2016
Date: 2017-05-12 17:38
Category: Educación, Entrevistas
Tags: fecode, Paro Docentes, Paro Educación
Slug: docentes-siguen-ganando-lo-mismo-que-en-2016
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/Abejorral.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UTL Victor Correa] 

###### [12 May 2017] 

Pese a que el Ministro de Hacienda Mauricio Cardenas, haya reiterado que la nivelación salarial se han cumplido, como parte de los acuerdos del año 2015 entre docentes y el gobierno, los maestros señalan que este hecho no es cierto y que **ni siquiera durante este año se les ha hecho el aumento legal de todos los trabajadores en Colombia fijado en el 7%**.

De acuerdo con Diego Echeverri, docente afiliado a Asociación de Institutores de Antioquia (ADIDA) **el Ministro de Hacienda y el Presidente de la República, han expresado que no hay presupuesto para cumplir los compromisos contractuales** y hasta la fecha ningún docente del país ha tenido un aumento en el salario y continúa con el sueldo del año 2016. Le puede interesar: ["Déficit en educación es de 600 mil millones de pesos: FECODE"](https://archivo.contagioradio.com/deficit-en-educacion-es-de-600-mil-millones-de-pesos-fecode/)

En el caso concreto de Abejorral, en Antioquia, desde el pasado 11 de mayo se están realizando asambleas informativas, en los 3 colegios que se mantienen en cese de actividades, con los padres de familia y estudiantes para exponer que el paro no solo se debe a la problemática salarial sino que reúne exigencias frente a la política pública de educación, que para Echeverri, no responde a las necesidades de las comunidades y **“pasa por alto las enormes diferencias que hay entre la educación rural y la educación urbana”**.

“**En términos de infraestructura, de recursos, de capacitación docentes, las inversiones siguen siendo muy precarias en el país**, y eso lo puede ver fácilmente cualquier ciudadano, estamos aquí tratando de comunicarle a toda la comunidad estás problemáticas” expresó Echeverri. Le puede interesar: ["Esta es la Agenda de movilización de FECODE desde el próximo 11 de mayo"](https://archivo.contagioradio.com/asi-sera-el-paro-nacional-de-fecode-a-partir-de-este-11-de-mayo/)

Los docentes de Abejorral tienen programado trasladarse hasta Medellín el próximo **lunes para participar de la movilización social que se registrará en este lugar**, el martes se llevará a cabo la gran toma a Bogotá, en donde se espera que miles de docentes lleguen a la capital y el miércoles se realizará un balance de la movilización.

######  Reciba toda la información de Contagio Radio en [[su correo]
