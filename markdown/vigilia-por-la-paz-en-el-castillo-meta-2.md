Title: La memoria renace en vigilia por la Paz en El Castillo, Meta
Date: 2016-11-01 14:58
Category: Otra Mirada, Reportajes
Tags: El Castillo, Meta, Vigilias por la paz
Slug: vigilia-por-la-paz-en-el-castillo-meta-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vigilia-castillo-meta-15.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 1 Nov 2016 

En un acto espiritual y con el deseo de hacer memoria, aportar a la construcción de paz y exigir la implementacion de los acuerdos, niños, niñas, abuelos, víctimas, organizaciones defensoras de derechos humanos y colectivos juveniles, se reunieron en Puerto Esperanza, caserío del municipio de 'El Castillo,  Meta, lugar que durante largos años fue escenario del conflicto armado.

Hacia las seis de la mañana, más de cien personas iniciaron la caminata hacia la vereda 'La Esmeralda', donde, desde muy temprano hombres y mujeres alistaban el fogón para cocinar los alimentos y disponer la Escuela donde recibieron a los participantes de la ceremonia de Vigilia por la Paz.

![vigilia-el-castillo-meta](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vigilia-el-castillo-meta.jpg){.alignnone .size-full .wp-image-31555 width="1000" height="667"}

Durante el recorrido de más de cinco horas, a través de senderos por donde años atrás hombres del ejército y paramilitares dejaron a su paso, el olor imborrable de la sangre y el miedo, los caminantes observaron hermosos paisajes, caudalosos ríos e imponentes montañas, cargadas de dulces añoranzas de la infancia y recuerdos amargos de la guerra, narrados por quienes han crecido allí y quienes a pesar del dolor, hoy están dispuestos a cambiar el rumbo de la historia.

### **Caminos de Memoria** 

Fueron muchas las historias que en cada tramo del camino provocaban sonrisas y lágrimas entre los presentes. Mientras atravesaban la quebrada 'La Cal', Sandra, una joven campesina de la vereda 'El Dorado', contaba al grupo que cerca de allí, en medio de dos árboles, están enterradas desde hace 10 años, tres personas de la vereda “que no han podido descansar en paz”, pues para poderlas sacar y darles una digna sepultura es necesario que la Comisión Nacional de Búsqueda de personas Desaparecidas se ponga en marcha con la implementación de los acuerdos de la Habana.

![vigilia-castillo-meta-1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-castillo-meta-1.jpg){.alignnone .size-full .wp-image-31509 width="1200" height="800"}

Unas horas más tarde, Gilberto, campesino habitante de la Comunidad Civil de Paz CIVIPAZ, quien perdió una de sus extremidades durante un operativo de las Fuerzas de Despliegue Rápido –FUDRA- del ejército en el año 2002, señaló un lugar del camino llamado por los habitantes de la zona como 'El Bombazo', sitio donde las fuerzas militares arrojaron desde sus helicópteros, cilindros bomba, que dejaron casas totalmente destruidas, graves heridos y cuerpos desmembrados.

En un tramo deforestado del recorrido, la señora Lucía, nacida en Medellín del Ariari y criada en estas veredas, fue desplazada durante el gobierno de Uribe en el año 2004, en medio de lágrimas, relata que en su niñez y juventud “todo esto era llenito de comida, acá crecía lo que usted sembrara, la comida nunca nos faltaba, hasta que llegó la guerra y la tierra, con la gente, se fue muriendo”.

![vigilia-castillo-meta-12](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/vigilia-castillo-meta-12.jpg){.alignnone .size-full .wp-image-31563 width="1000" height="667"}

Durante el trayecto, el padre Henry Ramírez hizo algunas pausas para rememorar a líderes y lideresas asesinadas, torturadas y desaparecidas en distintas zonas, para contar el papel que jugaron dentro de la comunidad y dar una voz de aliento a varios familiares de estas personas que participaban en la caminata.

A medida que iban avanzando, más personas de otras veredas se unían al grupo que rapidamente pasó de cien a quinientos caminantes que llegaron al 'Centro Educativo La Esmeralda'. y fueron recibidos por habitantes de la vereda con mucha afectividad y un plato de sancocho.

Luego de tomar los alimentos en comunidad, se dio inició a la eucaristía oficiada por el padre Henry, quien invito a sentarse junto al altar a familiares de victimas de crímenes del municipio El Castillo y a los presidentes y presidentas de Juntas Comunitarias, Asociaciones Campesinas y Comunidades de Paz.

![vigilia-castillo-meta-8](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-castillo-meta-8.jpg){.alignnone .size-full .wp-image-31516 width="1000" height="667"}

Sin embargo, no se trató de una misa común y corriente, además de los pasajes bíblicos y los rituales propios de las celebraciones ecuménicas, el padre, compartió con los y las asistentes reflexiones en torno a la memoria de los muertos y desaparecidos, del papel de los actores armados en estos crímenes, de la importancia de la organización y la unión entre comunidades, resaltó el papel fundamental del campesinado en la defensa del territorio y la preservación de los ecosistemas por encima de intereses económicos. En general, el padre manifestó el compromiso trascendente que todos y todas debemos adquirir en la construcción de paz en el actual contexto colombiano.

Al terminar la celebración litúrgica, se abrió un espacio para observar algunas presentaciones de danzas folclóricas, cantos al son de acordeones y guacharacas, sentires desde el hip-hop y obras de teatro que recreaban los momentos de conflicto, dejando un mensaje de memoria y esperanza para un nuevo futuro, sin rencores ni desconfianza.

![vigilia-castillo-meta-9](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/vigilia-castillo-meta-9.jpg){.alignnone .size-full .wp-image-31517 width="1200" height="800"}

### **Una luz por la paz ** 

Entrada la noche, el padre Henry junto a integrantes de la Comisión Intereclesial de Justicia y Paz, dieron paso a la entrega de unos jarrones de barro, que simbolizaban el compromiso individual y colectivo con la paz, a las organizaciones que convocaron a la Vigilia. Uno de estos, también fue entregado al Alcalde Eliecer Urrea, quien estuvo durante todo el recorrido, en la ceremonia y pernoctó en la escuela, lo que causó mucha sorpresa a los habitantes del municipio, pues manifestaron que hace veinticinco años ningún alcalde visitaba las zonas altas y, mucho menos, se comprometía con el trabajo en pro de la paz y la vida digna de las comunidades.

Al finalizar la entrega simbólica, se abrió un espacio para aquellos que quisieran expresar sus pensamientos y aportar a esta iniciativa lo hicieran. Uno de ellos fue Julián, un combatiente del Frente 52 de las FARC-EP, quien compartió con los asistentes su sentir, más allá del ser guerrillero, habló desde su sentir como ser humano. Habló del trabajo por la paz que ha adelantado las FARC y del firme compromiso que tienen para dar continuidad a ello, mencionó también la importancia de la unión e invitó a que no se detenga la movilización social en la exigencia de dar celeridad a la implementación de los acuerdos.

Sobre las doce de la noche, con velas encendidas, los participantes se dirigieron hacia un monumento en honor a los muertos y desaparecidos. Se hizo un camino de luz desde el lugar de la ceremonia hasta el mausoleo, acompañado de las palabras del padre Henry y de familiares de quienes cayeron allí. Después de unos momentos de silencio e introspección, se hizo el cierre de la ceremonia quedando el compromiso de continuar en la construcción de la paz, de exigir los acuerdos y de seguir el camino, sin olvido.

El padre Henry, manifestó que los frutos de este evento fueron, el acercar la ciudad al campo, pues los resultados del 2 de Octubre dieron cuenta de la distancia de pensamientos y sentires de los habitantes urbanos con quienes se han enfrentado a la guerra en el espacio rural. La exigencia de mantener el cese al bilateral al fuego para generar confianza en las comunidades, “la necesidad que tiene la población campesina de poderse encontrar directamente con quienes en su momento fueron los actores armados y entablar diálogos” y por último la tarea de recolección de relatos e información para el proceso verdad justicia y reparación de las víctimas de [El Castillo.](https://archivo.contagioradio.com/?s=el+castillo+meta)

\

<iframe id="audio_13571228" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13571228_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
