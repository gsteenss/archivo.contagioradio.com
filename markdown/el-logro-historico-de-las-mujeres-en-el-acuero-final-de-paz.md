Title: El logro histórico de las mujeres en el acuerdo final de paz
Date: 2016-08-26 16:56
Category: Mujer, Nacional, Paz
Tags: acuerdos de paz, aministía, FARC, Gobierno, mujeres
Slug: el-logro-historico-de-las-mujeres-en-el-acuero-final-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/reclusas-e1472247745331.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Nuevo Herald 

###### [26 Ago 2016] 

Las **mujeres que se encuentran en prisión** por delitos menores relacionados con el narcotráfico o con los cultivos de uso ilícito, serán **beneficiadas con la Ley de Amnistía** acordada en La Habana entre el gobierno y la guerrilla de las FARC.

“Se trata de un colectivo muy abandonado de mujeres en situación de pobreza que han sido encarceladas por delitos conexos con el narcotráfico pero que no son delitos violentos. Por primera vez se atiende las recomendaciones que vienen haciendo desde hace más de 4 años los Estados Americanos sobre la necesidad de que haya un tratamiento penal diferenciado”, explica Enrique Santiago, asesor jurídico de las FARC.

Es así como además se escucha las solicitudes de organizaciones defensoras de los derechos de las mujeres en Colombia que hace meses habían propuesto revisar las condenas de las mujeres privadas de la libertad por hechos relacionados con el narcotráfico. La Corporación Humanas había evidenciado en sus investigaciones las consecuencias personales y sociales que tiene la cárcel en mujeres que** incurrieron en delitos de drogas motivadas por la exclusión, marginalidad social y el abandono estatal,** demostrando que esos aspectos se agudizan en la prisión.

Así lo explica Santiago, “No serán personas que pertenecen a las directivas de organizaciones criminales, sino mujeres cuyo encarcelamiento pone a sus familias y sobre todo a sus hijos en una situación de pobreza muy difícil”.

Una decisión que se toma al analizar lo que han sido las políticas penales contra las mujeres por delitos relacionados con drogas, pues en los últimos años  se viene endurecido las condenas contra el eslabón más débil esta cadena, “**siendo tratadas como los grandes narcotraficantes sin serlo, y en cambio se siguen llenando las cárceles y aumentando el hacinamiento”,** señala Adriana Benjumea, directora de la Corporación Humanas.

De esta manera al día siguiente del plebiscito (siempre y cuando sea aprobado el acuerdo final logrado en La Habana) la amnistía sería aplicada para estas mujeres, y todas las demás personas que no hayan cometido delitos de lesa humanidad, sino delitos relacionados con la rebelión.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
