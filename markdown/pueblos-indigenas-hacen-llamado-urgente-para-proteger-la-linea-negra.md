Title: Pueblos indígenas hacen llamado urgente para proteger la Línea Negra
Date: 2020-07-15 17:29
Author: AdminContagio
Category: Actualidad, Nacional
Tags: comunidades indígenas, Línea Negra, ONIC, Sierra Nevada
Slug: pueblos-indigenas-hacen-llamado-urgente-para-proteger-la-linea-negra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Línea-Negra-Corazón-del-Mundo.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Indigenas-de-la-Sierra-Nevada-piden-salvaguardar-la-Línea-Negra.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"textColor":"cyan-bluish-gray","fontSize":"small"} -->

Comunidades indigenas piden proteger la Línea Negra / Foto: Danilo Villafañe

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Los pueblos indígenas Kogui, Wiwa, Kankuamo y Arhuaco de la Sierra Nevada de Santa Marta** hicieron un llamado urgente al Gobierno, instituciones y a la sociedad en general para la **protección y resguardo de la Línea Negra** redefinida recientemente por el Decreto 1500 de 2018.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Línea Negra es la delimitación de un área de interés ecológico y cultural en el Caribe colombiano, con la cual se busca promover su preservación y garantizar el libre acceso a las comunidades indígenas que habitan ese territorio. (Le puede interesar: [Niñas y niños exigen sus derechos ambientales ante la ONU](https://archivo.contagioradio.com/ninos-y-ninas-exigen-sus-derechos-ambientales-ante-la-onu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dicha delimitación redefine el territorio ancestral de los pueblos Arhuaco, Kogui, Wiwa y Kankuamo blindándolo bajo una protección jurídica y legal. **El llamado de estos pueblos indígenas es a salvaguardar esta zona que, según su cosmogonía es el «Corazón del Mundo»**, a través de la consigna: «*1.500 latidos para salvar la Sierra Nevada*».

<!-- /wp:paragraph -->

<!-- wp:image {"id":86816,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Línea-Negra-Corazón-del-Mundo-724x1024.jpg){.wp-image-86816}  

<figcaption>
Línea Negra - Centro de Investigación y Educación Popular ([CINEP](https://twitter.com/CINEP_PPP))

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:quote -->

> **“El corazón del mundo (La Sierra Nevada) es la matriz de todo el universo en donde se engendra la semilla y todos los códigos de la naturaleza”**
>
> <cite>José Mario Bolívar, autoridad del pueblo Wiwa</cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Demanda de nulidad contra el Decreto 1500 que redefine la Línea Negra

<!-- /wp:heading -->

<!-- wp:paragraph -->

No obstante, esta zona es objeto de varios intereses económicos. Según la Organización Nacional Indígena de Colombia –[ONIC](https://twitter.com/ONIC_Colombia)-, **actualmente existen más de 200 títulos y solicitudes mineras que ponen en riesgo la Sierra Nevada y el buen vivir de las comunidades indígenas.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, la ONIC afirma que **en contra del Decreto 1500 de 2018 cursa una demanda de nulidad ante el Consejo de Estado**, la cual fue presentada por Yefferson Dueñas Gómez y respaldada por varios gremios económicos que se están viendo imposibilitados de adelantar sus actividades de minería ante la perimetración y protección de dicho territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El pedido de las comunidades indígenas en relación con este proceso judicial es que sean reconocidas como partes del mismo, ya que son las directamente implicadas con las disposiciones del Decreto y por otra parte que se desestimen las pretensiones de quien demanda la nulidad.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CINEP_PPP/status/1283406508517199872","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CINEP\_PPP/status/1283406508517199872

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Deuda histórica del Estado con las comunidades indígenas

<!-- /wp:heading -->

<!-- wp:paragraph -->

Camilo Niño, secretario técnico de la Comisión Nacional de Territorios Indígenas -[CNTI](https://twitter.com/CNTI_Indigena)-, señaló que **«hoy en Colombia estamos ante un estado de cosas inconstitucional en materia de protección de los derechos territoriales de los pueblos indígenas»**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sustentó su afirmación, entre otras razones, diciendo que el Estado tiene una deuda histórica para garantizar el reclamo que los pueblos indígenas han realizado sobre los territorios para salvaguardar su propiedad colectiva. Aseguró que actualmente **existen 889 solicitudes en ese sentido, algunas de ellas con más de 40 años, sin que el Estado las haya resuelto.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, sostuvo que hay una ausencia de regulación y protección jurídica para el ejercicio de los derechos de las comunidades indígenas. De ahí, la importancia de proteger el Decreto 1500 de 2018 como uno de los pocos instrumentos normativos existentes para la protección de estos pueblos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
