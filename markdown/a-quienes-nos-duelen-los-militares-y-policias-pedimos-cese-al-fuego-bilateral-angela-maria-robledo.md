Title: "A quienes nos duelen los militares y policías pedimos cese al fuego bilateral" Angela María Robledo
Date: 2015-04-15 13:28
Author: CtgAdm
Category: Entrevistas, Paz
Tags: Angela Maria Robledo, Cauca, Cese al fuego, FARC, militares, paz
Slug: a-quienes-nos-duelen-los-militares-y-policias-pedimos-cese-al-fuego-bilateral-angela-maria-robledo
Status: published

##### Foto: Las dos orillas 

#####  

##### <iframe src="http://www.ivoox.com/player_ek_4358482_2_1.html?data=lZiimpmcdo6ZmKiakp6Jd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmopDe187JssbnjNPc1ZDIucbgxtOYztTXb87dzc7hw9fJt4ztjNXczs7HaaSnhqaxw9iPtMbYytLc1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Angela María Robledo, Representante a la Cámara] 

La representante a la Cámara, Angela María Robledo, se pronunció sobre los hechos ocurridos entre el Ejercito y las FARC durante la madrugada del miércoles 15 de abril en la vereda La Esperanza, en que perdieron la vida 10 militares.

Para Robledo, "la situación es muy extraña, y hace falta más información y tener los pies sobre la tierra" para evitar, cómo ocurriera en Ituango en hechos similares, que la información la preliminar hablara de una emboscada por parte de la guerrilla, para confirmar posteriormente que se trató de un enfrentamiento. "Ituango dejó muertos del Ejercito y de la guerrilla, que no son nada diferente a jóvenes colombianos".

"Nos duele profundamente la muerte de estos jóvenes soldados colombianos, y los jóvenes heridos", y es por ese motivo, enfatiza la representante, en que "Insistimos en que se acabe la guerra, y no los esfuerzos por la paz.

Cómo parte del Frente Amplio por la Paz, la representante recuerda que "hemos registrado en muchos territorios en Colombia, en muchos lugares de la Colombia profunda donde se libra la guerra entre hermanos, lo que ha significado parar la maquina de la guerra". Según los estudios, el cese al fuego unilateral "ha implicado evitar la muerte de cerca de mil personas, y las heridas de 4 mil, tanto del ejercito, la policía, como de la sociedad colombiana en general".

Finalmente, la representante hace un llamado a la sociedad colombiana a estar alerta, ante las declaraciones de figuras del Centro Democrático como Fernando Londoño Londoño, que parecieran invitar a las fuerzas militares a realizar un golpe de Estado. "Nos parece un mensaje muy complicado", señala Robledo. "El Centro Democrático quiere presentarse como los únicos que defienden al Ejercito, como los únicos a los que les importa la muerte de militares y policías, y si fuera así, ellos habrían parado la guerra durante los 8 años de gobierno de Alvaro Uribe Vélez.

"A quien le duela Colombia, los militares y policías, lo que debemos pedir es el cese de la confrontación armada y de la guerra", concluyó.
