Title: Paramilitares amenazan a integrantes de la Revista Cultural El Salmón
Date: 2017-03-31 16:33
Category: DDHH, Nacional
Tags: Amenazas, Consulta popular de cajamarca, paramilitares, Revista Cultural el Salmón
Slug: amenazas-periodistas-revista-cultural-el-salmon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/revista-cultural-el-salmon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elsalmon] 

###### [31 Mar 2017] 

Los integrantes de la **Revista Cultural El Salmon** fueron amenazados de muerte, A través de un mensaje de texto firmado por las Aguilas Negras AUC, que los ubica como objetivo militar por** su participación en la difusión de las exigencias de los habitantes de Cajamarca** en el marco de la consulta popular que se realizó el pasado 26 de Marzo y que ordena el cese de la minería en ese municipio del departamento del Tolima.

La primera amenaza se produjo el 25 de Marzo, justo un día antes de la consulta popular en Cajamarca. En ella los paramilitares hacen **una alusión concreta al trabajo de los periodistas** en lo relacionado con el trabajo en medio de la jornada electoral “…*no los keremos ver en Cajamarca ni en ningún lado jodiendo oponiéndose al desarrollo…”* reza el texto enviado.

Una segunda amenaza se produjo este 30 de Marzo. Según la denuncia el director de la revista cultural recibió una llamada en la que expresaron “*que no querían ver a los salmones en Cajamarca y que no siguieran escribiendo contra la Mina en la Revista o sino que accionarían*” según lo relata la **denuncia del Salmón.** ([Le puede interesar Paramilitares amenazan a líderes del Catatumbo y Santander](https://archivo.contagioradio.com/?s=paramilitares))

### **Periodistas también están bajo la lupa paramilitar** 

Esta amenaza contra los integrantes de la **revista cultural el salmón** se da en medio de una fuerte preocupación por el incremento tanto de la presencia como de las actuaciones de los grupos paramilitares que también han arremetido contra la prensa. Según la Fundación para la Libertad de Prensa, durante 2016 se presentaron 216 violaciones a los derechos y **262 víctimas en el gremio de los periodistas.**

Esta situación se suma a las reiteradas amenazas contra defensores de DDHH y líderes sociales en Colombia que no son lejanas a lo que ocurre con integrantes del Movimiento Social y por ello la exigencia de los y las integrantes del Salmon son dirigidas a la investigación, sanción y desmonte de las **estructuras paramilitares que operan en el país**. ([Lea también: Arremetida paramilitar en el chocó](https://archivo.contagioradio.com/paramilitares_amenazan_extorsionan_pobladores_choco/))

###### Reciba toda la información de Contagio Radio en [[su correo]
