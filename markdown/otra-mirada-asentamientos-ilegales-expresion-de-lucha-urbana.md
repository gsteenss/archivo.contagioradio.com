Title: Otra Mirada: Asentamientos ilegales, expresión de lucha urbana
Date: 2020-08-23 10:10
Author: AdminContagio
Category: Otra Mirada, Programas
Tags: Asentamientos ilegales
Slug: otra-mirada-asentamientos-ilegales-expresion-de-lucha-urbana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/asentamientos-i.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Agencia de Noticias UN-

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante la pandemia, se han denunciado múltiples violencias por parte de la fuerza pública a habitantes de asentamientos ilegales en Bogotá y otras partes del país, dejando a muchos en la intemperie en medio de una crisis de salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esto, teniendo en cuenta la situación de hoy en día, es importante ver cómo viven las personas en asentamientos ilegales en medio de conflictos y masacres. (Le puede interesar: [5 niños afro fueron masacrados en el barrio Llano Verde en Cali](https://archivo.contagioradio.com/5-ninos-afro-fueron-masacrados-en-el-barrio-llano-verde-en-cali/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Explicando cómo funcionan estos asentamientos ilegales y los  diversos procesos que se vienen desarrollando dentro de los mismos, estuvieron Ana Erazo, magíster en Estudios Urbanos y concejala de Cali, María Elvira Naranjo socióloga, doctora en Estudios Políticos y Relaciones Internacionales, Yair Mosquera, gestor cultural Charco Azul, Cali, Valle del Cauca y Heiner Gaitán concejal de Soacha.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Comentaron cómo es convivir en estos asentamientos y cómo se puede utilizar y transformar el arte como una expresión de resiliencia y de comunicar lo que se vive en los territorios. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, resaltan lo necesario que es reubicar a las comunidades en un barrio formal sin poner en riesgo esos procesos que se llevaban dentro de los asentamientos y la importancia de denunciar actos de violencia para que la fuerza pública no pase por encima de sus derechos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Igualmente, comparten qué significa para la comunidad la pérdida y asesinato de un niño o un joven al que conocieron y vieron crecer.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente, explican qué acciones se vienen gestionando para que la sociedad misma deje de estigmatizar a estas poblaciones, comprendan que muchos han llegado a esas zonas por culpa del conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si desea escuchar el programa del 18 de agosto: Otra Mirada: [EPM y los derechos a lo público](https://www.facebook.com/contagioradio/videos/814216892316056)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Si desea escuchar el programa completo Haga click aquí](https://www.facebook.com/contagioradio/videos/798769240953782)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
