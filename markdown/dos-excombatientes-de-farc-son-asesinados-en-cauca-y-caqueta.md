Title: Dos excombatientes de FARC  son asesinados en Cauca y Caquetá
Date: 2020-10-25 07:30
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Cauca, excombatiente, FARC
Slug: dos-excombatientes-de-farc-son-asesinados-en-cauca-y-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Asesinados-los-excombatientes-de-FARC-Unaldo-Castillo-y-Yoimar-Jiménez..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 24 de octubre se registrados **dos nuevos asesinatos en contra de excombatientes de Farc,** los hechos fueron reportados desde los departamentos del Cauca y Caquetá.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1320383699100311552","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1320383699100311552

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Marcial Macias, excombatiente de FARC es asesinado en Balboa

<!-- /wp:heading -->

<!-- wp:paragraph -->

La primera víctima fue reconocida como Marcial Macias Alvarado, en el barrio Pampa, municipio de Balboa, departamento del Cauca sobre las 3:40 pm de este sábado. ([«Pese a que se ha avanzado, aún queda un largo camino de compromiso con la paz»: Massieu](https://archivo.contagioradio.com/pese-a-que-se-ha-avanzado-aun-queda-un-largo-camino-de-compromiso-con-la-paz-massieu/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Marcial **Macias tenia 61 años de edad, según familiares, él se encontraba almorzando cuando dos sujetos armados se acercaron y le propinaron tres disparos**, 1 de ellos *"en el hemisferio izquierdo de su cabeza y el otro en el pómulo izquierdo de su rostro"*, según reportó Indepaz.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los impactos fueron fulminantes ocasionándole la muerte de manera inmediata, previo a esto y según también información familiar, la víctima no había recibido amenaza en su contra. Adicional **Marcial Macias, había perteneció al frente 29 de las FARC, de donde se desmovilizo luego de la firmas de los acuerdos de paz en 2016**, desde allí entró a integrar la Cooperativa del Bordo.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Librado Becerra, uno de los excombatientes de FARC, fue asesinado en San Vicente del Caguan

<!-- /wp:heading -->

<!-- wp:paragraph -->

La segunda víctima, fue identificada como L**ibrado Becerra, en la vereda Ciudad Yary de San Vicente de Caguan, departamento de Caquet**a; y fue conocido mediante la información entregada por el presidente de la Junta de Acción Comunal de la vereda Ciudad Yary.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Quien reportó el hallazgo de *"1 cuerpo al parecer de quien en vida se identificaría como Libardo Becerra Valencia (ex Farc), acreditado mediante resolución 004 del 03 de mayo de 2017 en sector rural de los llanos del Yary"*.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1320383699100311552","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1320383699100311552

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Estos dos nuevos asesinatos aumentan la cifra de homicidos contra los y las firmantes de paz, a un número que según el Partido FARC, ya supera los 200 exguerrilleros asesinados desde la firma del Acuerdo de Paz. ([Plataformas de DDHH expresaron su respaldo a la Minga](https://archivo.contagioradio.com/plataformas-respaldo-minga/)[Reincorporados de FARC se movilizarán para exigir el derecho a la vida y la paz](https://archivo.contagioradio.com/excombatientes-farc-movilizaran-rechazo-asesinatos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al tiempo que destacan que estos asesinatos en contra de los excombatientes mientras decenas de personas marchaban **desde el Meta hasta Bogotá,** en una acción por el respeto a la vida a los firmantes del acuerdo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde 2016 han sido asesinados **266 excombatientes, de estos cinco corresponden a mujeres; así mismo 154 de estos asesinatos se han dado en el desarrollo del gobierno de Iván Duque.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**53 sólo durante este 2020, y 18 de estos se encuentran a un desaparecido sus restos**. Asimismo la [peregrinación por la vida de la Paz](https://partidofarc.com.co/farc/2020/10/23/tercer-dia-de-peregrinacion-por-la-vida-y-la-paz/)de los y las firmantes del acuerdo destaca que, los cinco departamentos más afectados por este tipo de hechos son, cauca con 38; Nariño con 28; Antioquia con 25; Caquetá con 22 y Meta con 21.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
