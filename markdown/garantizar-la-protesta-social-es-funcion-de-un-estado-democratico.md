Title: Garantizar la protesta social es función de un Estado democrático
Date: 2019-06-10 18:06
Author: Foro Opina
Category: Opinion
Tags: derecho a la protesta, Marchas, Protesta social
Slug: garantizar-la-protesta-social-es-funcion-de-un-estado-democratico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Marcha-de-estudiantes-4.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: MoMujeresNegras 

##### \*Por la Fundación Foro Nacional por Colombia 

La estrategia del gobierno Duque, aún antes de su posesión oficial, ha sido estigmatizar y criminalizar la protesta social. Desde la voz de su Ministro de Defensa con el respaldo de sectores gremiales y por supuesto de la bancada de gobierno, se alude a la protesta como asunto de orden público que resulta en vandalismo sobre bienes públicos y privados. Esta no es solo una lectura desafortunada y parcializada, sino además autoritaria y ajena al espíritu y al ordenamiento constitucional colombiano.

**Las movilizaciones y protestas son expresiones sociales propias de las democracias. Además de ser vías directas para que la ciudadanía exprese aspiraciones, frustraciones, desacuerdos** o reclamos sobre los asuntos que afectan sus vidas, su ejercicio práctico se apoya en el respeto y el reconocimiento de garantías y libertades como el derecho de reunión y asociación, o de las libertades de expresión y de conciencia. No se puede ignorar que la mayoría de los derechos y libertades se conquistaron gracias a las movilizaciones y protestas de varias generaciones que nos antecedieron y nos legaron la comprensión de la importancia de proteger el “derecho a tener derechos”, a protestar cuando los derechos y libertades son vulnerados o negados y cuando no hay otros medios para expresarlo o hacerse oír.

A un Estado democrático le corresponde garantizar la expresión pública de sectores marginados, minoritarios o de oposición política, con el propósito de que puedan alcanzar y ampliar la realización de derechos individuales y colectivos, en aspectos cruciales como los asuntos económicos, sociales, políticos, culturales o ambientales, y frente la ausencia o la ineficacia de lograrlo por otras vías democráticas.

> Una democracia agoniza cuando no se permite la expresión plural de ideas que reflejan la heterogeneidad social y política.

La Constitución Política colombiana en el artículo 37 reconoce formalmente las movilizaciones y protestas como modalidades legítimas de acción colectiva – entendidas como reunión y manifestación pública – a través de las cuales la ciudadanía se expresa frente a los asuntos que afectan su vida.

De acuerdo con la base de datos de las luchas sociales del CINEP/PPP, **en los años recientes las movilizaciones y protestas se han consolidado como el medio por excelencia para expresar el descontento social y el Estado colombiano** ha optado por privilegiar el orden público por sobre los reclamos y demandas de quienes protestan, frente a los cuales ese Estado poco o nada responde y, por el contrario, genera nuevos motivos para protestar.

La tendencia estatal ha sido enviar mensajes que estigmatizan la protesta y a equipararla con disturbios públicos. Si bien es cierto que la protesta, dada su naturaleza disruptiva, supone ciertas incomodidades para la población que no participa directamente en ellas, o lleva a que las autoridades perciban en su desarrollo algunas dificultades para preservar el funcionamiento cotidiano de las instituciones y de las vías públicas, no se puede concluir que la protesta social se encuentra en el mismo plano jurídico y constitucional que los disturbios públicos, ni se puede restringir al punto de llegar a impedir el ejercicio de este derecho; así lo ha señalado la Corte Constitucional en varias de sus sentencias. Los principales instrumentos internacionales de derechos humanos – incorporados en el ordenamiento jurídico colombiano – hacen un reconocimiento preciso y amplio del derecho a la manifestación pública pues se trata de una expresión propia a la condición humana, funcional para el cumplimiento de los demás derechos.

El rol del Estado debe ser permitir, proteger y facilitar la concurrencia a las movilizaciones y protestas pacíficas. De allí la importancia de contar con un marco normativo y de políticas públicas que se ocupe de abordar temas como el acceso al espacio público, inherente a la movilización y la protesta, que otorgue garantías de seguridad y protección para su ejercicio y prevenga hechos de violencia. Pero además se requiere una respuesta efectiva de las autoridades públicas a las demandas ciudadana; porque protestar no es un deporte: es un acto consciente de expresión social y política que busca la realización de otros derechos y es el medio más directo y amplio para hacer oír voces diversas.

Sin duda en el mundo, ayer, hoy y siempre, ¡se vale protestar!
