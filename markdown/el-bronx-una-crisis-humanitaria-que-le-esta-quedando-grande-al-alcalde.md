Title: "El Bronx una crisis humanitaria que le está quedando grande al alcalde"
Date: 2016-06-15 15:09
Category: DDHH, Entrevistas
Tags: alcaldía Peñalosa, corrupción Policía Nacional, el bronx
Slug: el-bronx-una-crisis-humanitaria-que-le-esta-quedando-grande-al-alcalde
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Bronx.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Behance ] 

###### [15 Junio 2016 ] 

Tras el violento desalojo del Bronx, **los habitantes de calle se han desplazado a barrios del centro capitalino como San Bernardo** en el que las comunidades están alarmadas pues han sido víctimas de constantes robos y ataques, ante la inoperancia de la Policía, según afirma Edgar Montenegro, integrante del proceso social Mesa Centro.

Según Montenegro algunos agentes de la Policía están vinculados con los negocios ilícitos de este sector de la ciudad, esta versión se sustenta en la actual investigación que la misma Policía y la Fiscalía General adelantan contra **siete miembros de la fuerza pública por pagos diarios que recibirían por permitir el microtráfico** y pagos adicionales por evitar los operativos de allanamiento informando a los cabecillas de las bandas conocidas como 'Los ganchos', 'Manguera', 'América', 'Mosco', 'Morado' y 'Nacional'.

Así mismo, uno de los testigos señala en documentos de la Fiscalía que policías de la estación de la 24 recibían dinero por informar si en el sector estaban operando policías de inteligencia, y que todas las bandas **pagaban cuotas semanales a algunos de los agentes que llegaban a los  \$ 450.000**.

De acuerdo con Montenegro en este desalojo no hay ningún carácter de integralidad, lo que hay ahora son cartuchitos por todos lados, en suma, **una crisis humanitaria que le está quedando grande al presidente y al alcalde**, un desastre que no se quiere ver y que se está extendiendo por toda la capital colombiana, particularmente por La Estanzuela Paloquemao y Chapinero.

Pese a que el pasado 2 de abril de 2013 el presidente Juan Manuel Sanos aseguró que en el sector tenían 25 ollas de microtráfico identificadas y que sesenta días después verificarían que ya no existieran, tres años después, **la problemática persiste, se agudiza y los habitantes de los barrios aledaños no saben cómo enfrentarla**.

Las comunidades aseguran que el desalojo y lo que viene sucediendo en el centro de la ciudad, se enmarca en la intención de la actual administración de **poner a disposición de inmobiliarias privadas estos terrenos** que serían adquiridos a "precio de huevo" como afirma Montenegro.

<iframe src="http://co.ivoox.com/es/player_ej_11915008_2_1.html?data=kpamk5qUdJmhhpywj5abaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncabYyMbfjbLTstXZz8rU1NSPcYzE09TQx9jTb9TjxM7OzpCxqdTVjKjS0NnWs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
