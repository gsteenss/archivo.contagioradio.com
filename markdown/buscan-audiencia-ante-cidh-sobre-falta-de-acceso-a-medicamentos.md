Title: Organizaciones buscan audiencia ante CIDH sobre falta de acceso a medicamentos
Date: 2016-03-01 17:54
Category: DDHH, Nacional
Tags: Acceso a medicamentos, CIDH, Salud
Slug: buscan-audiencia-ante-cidh-sobre-falta-de-acceso-a-medicamentos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Medicamentos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Agencia UN 

<iframe src="http://co.ivoox.com/es/player_ek_10644854_2_1.html?data=kpWjlpmceZWhhpywj5WbaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5yncaPp1MjO0JDFucXdxtPQy8aPpc%2FoxpCwq6msb9Tjw9fSjcvFsNXVjMnSjcbHp8bn0JDOjdLJqMqhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carolina Solano, Comisión colombiana de juristas] 

###### [1 Mar 2016] 

Organizaciones de la Sociedad Civil y de la Iglesia Católica de Latinoamérica y el Caribe han solicitado a la Comisión Interamericana de Derechos Humanos una **audiencia temática regional para exponer el drama de la falta de acceso a medicamentos** por parte de las personas de esta región.

De acuerdo con Carolina Solano, abogada de la Comisión Colombiana de Juristas, de las **180 mil tutelas presentadas por los colombianos para acceder a su derecho a la salud**, gran parte de ese porcentaje de esas tutelas se relacionan con la falta de acceso a medicamentos.

Si esta la audiencia es otorgada, los voceros de estas organizaciones expondrían ante el comisionado encargado de los derechos económicos y sociales “la falta de medicamentos necesarios y asequibles en Estados americanos como una negación del derecho fundamental a la salud y la vida, las causas de esta situación, el drama humano que esto genera, especialmente en las poblaciones más vulnerables, **y proponer soluciones a los actores nacionales e internacionales** que lo aborden de raíz para avanzar efectivamente en su superación”.

“La falta de medicamentos no solo es una problemática en Colombia sino de toda Latinoamérica”, dice la abogada, quien agrega que con la audiencia se busca que la CIDH recomiende al **Estado que tiene que tomar medidas para garantizar el acceso a los medicamentos.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
