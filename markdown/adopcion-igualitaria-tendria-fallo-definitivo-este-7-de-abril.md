Title: Adopción igualitaria tendría fallo definitivo este 7 de abril
Date: 2015-04-06 17:36
Author: CtgAdm
Category: LGBTI, Nacional
Tags: adopcion gay, Adopción igualitaria, Angélica Lozano, Convención de Derechos del Niño, Corte Constitucioinal, Derechos de los niños, Homosexualidad, Pretelt
Slug: adopcion-igualitaria-tendria-fallo-definitivo-este-7-de-abril
Status: published

##### Foto: Pulzo.com 

**La Corte Constitucional deberá resolver la demanda interpuesta por el abogado Sergio Estrada,**  por la cual  se busca que las parejas del mismo sexo puedan adoptar. Esta vez el argumento no se basa en los derechos de la comunidad LGBTI, sino en los derechos de la niñez a tener un hogar.

El profesor universitario de Medellín, radicó la demanda en contra de los artículos **64, 66 y 68 de la Ley 1098 de 2006,** conocida como la Ley de Infancia y Adolescencia, debido a que el debate esta vez  gira en torno a que los niños y niñas tienen el derecho fundamental a una familia, de manera que se tiene en cuenta la **ley 12 de 1991**, donde se incorpora **la Convención de Derechos del Niño**, allí se argumenta que en la adopción no se debe considerar el sexo de los padres.

Así mismo, la demanda de Estrada, tiene presente **la Sentencia C-577 de 2011, por la cual la Corte Constitucional, ya había declarado que las parejas homosexuales son familia.** La iniciativa tiene como objetivo, “desomosexualizar” el debate, para así proteger el derecho fundamental de los menores a tener una familia.

De acuerdo al abogado, “si las parejas del mismo sexo son familia y los niños tienen derecho fundamental a una familia, la conclusión es que esa familia puede estar constituida por dos mamás o dos papás”, quien agrega que “el derecho fundamental a tener familia tiene la misma protección que otros como la vida, la libertad de cultos, etc., y la idea es evitar que argumentos políticos o religiosos limiten esa protección”.

Para el profesor, se trata de un paso importante para dar hogar a niños y niñas que no cuentan uno, por tal motivo, es crucial que los magistrados no se dejen llevar por sus fundamentos religiosos y morales, sino que basen su criterio en los derechos de la niñez, explica Sergio Estrada.

**La tutela del profesor se basa en una investigación de más de 5 años,** que tiene el propósito de procurar una protección eficaz de los derechos fundamentales de los niños, allí se resaltan las alarmantes cifras de niños y niñas que están en abandono, teniendo en cuenta que este tema no lo tuvo presente la Corte Constitucional en su anterior fallo, cuando decidió que “**las parejas del mismo sexo solo pueden adoptar cuando la solicitud recaiga en el hijo biológico de su compañero o compañera permanente”**.

Por su parte, **la representante Angélica Lozano **afirmó que “este día sabremos si es verdad esa mentira de que las parejas del mismo sexo somos un peligro para los niños y si acá en Colombia hay ciudadanos de primera y segunda clase” y agregó que **la tutela no puede verse afectada así los magistrados entren en paro,** debido a los últimos escándalos de la Corte por el caso del magistrado Jorge Pretelt.

Cabe recordar que Sergio Estrada, dice no representar a ninguna comunidad LGTBI, ni religiosa, "solo soy un académico".

La demanda quedó en manos del magistrado, Jorge Iván Palacio, y el magistrado Mauricio González Cuervo se  encuentra impedido al haber participado en la formulación de dicha ley, lo que hace pensar que la decisión estaría nuevamente dividida.
