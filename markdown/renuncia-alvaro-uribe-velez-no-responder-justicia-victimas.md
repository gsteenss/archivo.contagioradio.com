Title: Renuncia de Uribe Vélez a su curul puede ser una maniobra para dilatar el proceso y generar impunidad
Date: 2020-08-18 20:27
Author: CtgAdm
Category: Actualidad, Política
Tags: Álvaro Uribe, Caso de los 12 Apóstoles, Corte Suprema de Justicia, Diego Cadena, Senado
Slug: renuncia-alvaro-uribe-velez-no-responder-justicia-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Alvaro-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @AlvaroUribeVel / Álvaro Uribe Vélez

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de una carta dirigida al presidente del Senado Arturo Char, el expresidente Álvaro Uribe Vélez presentó de manera oficial su renuncia a su curul como senador de la República. Con su dimisión se abre el debate sobre si el proceso contra el exmandatario permanecerá en la Corte Suprema de Justicia o si pasa a manos de la Fiscalía, en un acto que varios juristas han considerado como una nueva maniobra dilatoria.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante el anuncio, la representante Ángela María Robledo afirma que el exsenador del Centro Democrático rompió su palabra, pues había expresado que si la justicia lo llamaba, mantendría su investidura como senador. Sin embargo con su renuncia buscaría "ganar tiempo y obstaculizar el proceso de investigación que está en curso". [(Le puede interesar: No queremos entrar en un júbilo ingenuo: Iván Cepeda)](https://archivo.contagioradio.com/no-queremos-entrar-en-un-jubilo-ingenuo-ivan-cepeda/)

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AlvaroUribeVel/status/1295820343060373510","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AlvaroUribeVel/status/1295820343060373510

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que **la intención tras su renuncia, sería la de evitar que la Corte Suprema de Justicia avance en el caso**. No obstante, resalta que desde 2009, el alto tribunal tiene la jurisprudencia de investigar un delito del cual se acusa a un congresista, si este se comete bajo la responsabilidad y en tiempo de su cargo. **Es decir que su fuero no se pierde cuando se trata de actos cometidos como senador.**[(Lea también: La estrategia mediática de los implicados en el caso Uribe Vélez)](https://archivo.contagioradio.com/la-estrategia-mediatica-de-los-implicados-en-el-caso-uribe/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Repercusión en otros casos que vincularían a Álvaro Uribe Vélez

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Daniel Prado, defensor de derechos humanos y apoderado de las víctimas en el caso que también se adelanta contra Santiago Uribe, hermano del exsenador, por el accionar del grupo paramilitar los 12 Apóstoles, señala que en medio del caso, **Álvaro Uribe junto a su abogado, Diego Cadena, buscaban afectar los resultados del fallo del Juzgado Especializado del Circuito Penal de Antioquia y el proceso** en contra del ganadero.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Dicha conducta, explica el abogado, convierte en víctimas de Uribe a los familiares de Camilo Barrientos Durán, asesinado en 1994 y del que se responsabiliza a Santiago Uribe, por lo que **considera "acreditarnos como víctimas del proceso que se adelanta contra Álvaro Uribe Vélez".** [(Le puede interesar: Caso Álvaro Uribe es esperanza para las víctimas que necesitan la verdad)](https://archivo.contagioradio.com/caso-alvaro-uribe-es-esperanza-para-las-victimas-que-necesitan-la-verdad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, **la defensa de las víctimas de la hacienda Guacharacas, llevada a cabo por la Comisión de Justicia y Paz - y caso donde habría sido asesinado extrajudicialmente en 1995, Humberto Mesa Lopera**, jefe guerrillero que lideró la toma a Las Guacharacas, lugar donde advierten que en 1996 nacería el Bloque Metro de las Autodefensas Unidas de Colombia - señala que con su renuncia, el exsenador podría ser llamado a indagatoria y **abriría las puertas para que Uribe Velez fuera vinculado a este y otros procesos en los que ha sido vinculado.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Iván Cepeda anuncia acciones legales contra funcionarios públicos por expresar su defensa a Uribe Vélez a pesar de sus cargos

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En medio del caso, el senador Iván Cepeda anunció que solicitará a la Procuraduría investigar al **director del Centro Nacional de Memoria Histórica, [Rubén Darío Acevedo](https://twitter.com/darioacevedoc), y al gerente para la Atención Integral de la pandemia Covid-19, Luis Guillermo Plata**, quienes desde el momento en que se dio a conocer la decisión de la Corte Suprema de Justicia (CSJ) en relación con la medida de aseguramiento para el expresidente tomaron posición en defensa del ahora exsenador, incumpliendo su deber como servidor público de acatar las decisiones judiciales.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
