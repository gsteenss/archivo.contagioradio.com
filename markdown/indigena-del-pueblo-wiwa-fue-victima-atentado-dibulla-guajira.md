Title: Indígena del Pueblo Wiwa fue víctima de atentado en Dibulla, Guajira
Date: 2018-07-24 13:39
Category: DDHH, Nacional
Tags: Guajira, indigena, Pueblo Wiwa, sicarios
Slug: indigena-del-pueblo-wiwa-fue-victima-atentado-dibulla-guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/pueblo-wiwa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo de Abogados José Alvear Restrepo] 

###### [24 Jul 2018] 

El colectivo de Abogados José Alvear Restrepo denunció que el indígena **José Antonio Mojica Daza, hijo del Mamo Mayor del Pueblo Wiwa, fue víctima de un atentado el pasado 23 de julio**, en el municipio de Dibulla, Guajira, cuando se dirigía a la casa de su padre. Según la denuncia, a pocos metros de la casa había dos hombres en una moto que le propinaron un disparo en la nuca.

De acuerdo con el comunicado, la zona puntual en la que se encontraba Mojica era el Resguardo Kogui Malayo Arhuaco. Una vez se produjo el intento de asesinato, los familiares de la víctima **lo trasladaron al hospital más cercano, mientras que los sicarios emprendieron la huida.**

El Colectivo de Abogados señaló que este hecho se produce 3 días después de que el Estado pidió perdón por su responsabilidad en la masacre **de 16 personas y desplazamiento de cientos más del Pueblo Wiwa**, luego de la intromisión de 200 hombres armados a su territorio durante el año 2002. (Le puede interesar:["Tras 16 años Estado pide perdón por masacre y desplazamiento del Pueblo Wiwa"](https://archivo.contagioradio.com/estado-perdon-wiwa/))

En ese sentido, la organización hizo llamado urgente para que se tomen las medidas necesarias que salvaguarden la vida de los integrantes de esta comunidad, que ha sido objeto de quemas de Unguna y Ushi y explotación de minería ilegal, y aseguró que “estos hechos han sido verificados en dos misiones de investigación de la Fiscalía General de la Nación que ha manifestado su preocupación por la vida de los habitantes de esta comunidad”.

###### Reciba toda la información de Contagio Radio en [[su correo]
