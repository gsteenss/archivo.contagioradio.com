Title: Dos meses de cese unilateral le han traído tranquilidad al campo colombiano
Date: 2015-02-20 20:25
Author: CtgAdm
Category: Entrevistas, Paz
Tags: cese unilateral de las FARC, Conversacioines de paz en Colombia, Frente Amplio por la PAz, paz en colombia
Slug: dos-meses-de-cese-unilateral-le-han-traido-tranquilidad-al-campo-colombiano
Status: published

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4113053_2_1.html?data=lZaelZWZd46ZmKiak5iJd6KolpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpdSfxNTbjabNqMKfotvSztHFb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Aida Avella] 

El Cese Unilateral de la guerrilla de las FARC cumple este **20 de febrero su segundo mes con un 100% de cumplimiento.** Aida Avella, integrante del Frente Amplio por la Paz, afirma que aunque se han presentado algunos casos preocupantes como el de Caloto en Cauca, la acción no se puede calificar como una violación al cese unilateral puesto que el hecho está en investigación.

Aida Avella, afirma que es necesario que se haga un llamado de atención al Ministro de Defensa para que se mantenga un clima que posibilite un cese bilateral y que mantenga las condiciones favorables que ha generado el cese unilateral. Por ello también convoca a que los colombianos y colombianas **comuniquen los beneficios que les ha traído el cese unilateral para hacer visibles las ventajas.**

Por otra parte la líder política llama la atención en torno al **incremento de la presencia paramilitar en varias zonas del país como el Meta y Córdoba**. Avella asegura que es necesario que se tomen decisiones para el combate del paramilitarismo puesto que no podría entenderse que la guerrilla deje las armas mientras que los grupos paramilitares se fortalecen.
