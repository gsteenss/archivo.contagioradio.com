Title: Desalojo del Bronx no tuvo una planeación integral
Date: 2016-08-17 13:19
Category: Nacional
Tags: Habitantes de calle
Slug: desalojo-del-bronx-no-tuvo-una-planeacion-integral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/habitantes2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Semana.com] 

###### [17 de Agost] 

Desde el desalojo del Bronx en el centro de Bogotá, los habitantes de calle se han ido a diferentes barrios de la capital como efecto del desplazamiento interno en la ciudad y **la falta de un programa de re ubicación por parte de la Alcaldía Distrital**, dando paso a diferentes problemáticas como saqueos, extorsiones y enfrentamientos contra la policía.

El último desalojo hacia los habitantes de calle se registro en la madrugada del jueves 17 de agosto, en donde efectivos de la Policía, los sacaron del barrio la Estanzuela, ubicado en la localidad de los Mártires y los trasladaron hacía la carrera 30 con calle 6. Según declaraciones de algunos habitantes de calle, **no se ha generado ningún plan para situarlos en otro lugar y los hogares de paso, como Bacatá, se encuentran llenos**.

De acuerdo con el concejal Hollman Morris, el desalojo del Bronx fue un modelo de intervención de control policial sin un control social, [que para nada tomo en cuenta las repercusiones y la vida del habitante de calle](https://archivo.contagioradio.com/el-bronx-una-crisis-humanitaria-que-le-esta-quedando-grande-al-alcalde/), que es un ser humano y a quien por tanto s**e le deben garantizar sus derechos**, generando así una diáspora de habitantes de calle.

De igual modo el concejal argumentó que en el pasado el desalojo al "Cartucho" generó, en esa misma vía, una multiplicación de ollas como "El Bronx", "El Samber", "5 Huecos", entre otros. Por otro lado, la Defensoría del Pueblo también ha dicho que **la intervención no fue planeada integralmente** y que las consecuencias es la actual situación de Bogotá.

Organizaciones como la Corporación Claretiana Norman Perez Bello a través de un comunicado le exigió a la alcaldía de Peñalosa que **detenga la persecución y desplazamiento en contra del habitante de calle**, que establezca un lugar o espacio para la permanencia de los mismos y que genere un grupo de especialistas aptos para el tratamiento de ellos.

<iframe src="http://co.ivoox.com/es/player_ej_12576282_2_1.html?data=kpeimZuWfJOhhpywj5aZaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5yncanjzdHaw9OPkdDm087gh5enb6Tjz8jSzMbQb7Hm0Mzfx9jNt9XV1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
