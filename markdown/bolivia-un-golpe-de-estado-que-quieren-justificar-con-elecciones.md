Title: Bolivia: Un golpe de Estado que quieren  justificar con elecciones
Date: 2020-03-03 17:59
Author: CtgAdm
Category: DDHH, El mundo
Tags: Bolivia, OEA, Represión
Slug: bolivia-un-golpe-de-estado-que-quieren-justificar-con-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Reuters-scaled.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Bolivia/ Reuters

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/diana-vargas-estudiante-e-integrante-del-feminismo-antipatriarcal_md_48516295_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Diana Vargas | Estudiante e integrante del Feminismo Antipatriarcal en Bolivia

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

Tras conocerse el informe publicado por expertos del Laboratorio de Ciencia y Datos Electorales del Instituto Tecnológico de Massachusetts (MIT por sus siglas en inglés) que reconoció que [Evo Morales](https://twitter.com/evoespueblo) **ganó sin cometer fraude durante las elecciones presidenciales de octubre de 2019,** movimiento sociales denuncian que a tres meses de la salida del mandatario y posterior asentamiento del Gobierno de facto representado por Jeanine Añez y las Fuerzas Armadas, la represión y militarización se han extendido contra medios de comunicación, organizaciones indígenas y mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Diana Vargas, estudiante e integrante del Feminismo Antipatriarcal en Bolivia, expresó su preocupación ante las próximas elecciones del 3 de mayo, señalando que la represión ha impedido hacer una campaña y que no se puede generar un plan para que las elecciones sean limpias o por lo menos no sean militarizadas que es una de las amenazas del gobierno de facto de Jeanine Añez antes durante y después de las elecciones.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "No nos sirve jugar con las urnas y las reglas de la derecha porque en ese tablero no vamos a ganar"

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

**"Se han vivido tiempos de terror, la gente teme salir a las calles"**, explica, Diana al denunciar que a lo largo de los tres meses en los que el Gobierno de facto ha dado un mayor control al Ejército, han ocurrido tres masacres, han sido asesinadas 38 personas y se han registrado más de 800 heridos y un alto número de detenciones que hasta enero superaban las 1.500 detenciones. [(Lea también: Durante gobierno de facto en Bolivia policía habría asesinado 24 personas)](https://archivo.contagioradio.com/durante-gobierno-de-facto-en-bolivia-policia-habria-asesinado-24-personas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**A su vez, más de 5.000 funcionarios de la administración de Evo Morales y sus familiares fueron víctimas de investigaciones arbitrarias y detenciones.** Sumado a estos datos, según el Instituto Nacional de Estadística (INE) de Bolivia, la llegada al poder de Jeanine Añez, también ha afectado otras dimensiones del país, registrando un aumento del 4,83% del desempleo durante el último trimestre de 2019.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "En Bolivia estamos en un cerco mediático"

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

La estudiante resalta las persecuciones que han vivido organizaciones indígenas y simpatizantes del gobierno de Evo, "no se puede hablar en las calles, los policías nos amenazas y nos dicen "ya no está el indio para protegerlos, ¿ahora qué van a hacer?", son los policías, los mismos que tienen sus familias en el campo, quienes han decidido estar del lado de los golpistas".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Diana Vargas afirma que hay mucha gente que aún no reconoce la existencia de un golpe de Estado en Bolivia, y que en gran medida se debe a que la información no se difunde en su totalidad por los canales nacionales, de igual forma, 53 radios comunitarias dejaron de emitir su servicio informativo.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hacia afuera no informan lo que se denuncia aquí, el abuso de poder de la Policía, hombres y mujeres que han denunciado violaciones y torturas hoy están en las cárceles por cargos como sedición o terrorismo", relata la estudiante.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que ante la represión, tampoco existe un proceso que juzgue las acciones de la Fuerza Pública contra la población y aunque organizaciones sociales denunciaron ante la CIDH el actuar del Ejército y la Policía **que estarían actuando junto a grupos paramilitares amparados por la Fuerza Pública para desaparecer y hostigar a personas** que han sido detenidas en medio de la protesta, hasta ahora no se ha hecho una investigación.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ESalinasm71/status/1207084799460085765","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ESalinasm71/status/1207084799460085765

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En cuanto al informe preliminar de la Organización de Estados Americanos en el que afirmó encontrar "irregularidades y que propició la renuncia de Evo Morales, la estudiante sostiene que la institución siempre ha compartido vínculos con naciones interesadas en los recursos de otros países, y que Luis Almagro director de la OEA, ha sido parte de este golpe, a través del informe preliminar presentado usándolo como "punta de lanza" para que militares y policías se volcaran contra el gobierno de Morales. [(Le puede interesar: Diputados de Bolivia alertan sobre persecución judicial del Gobierno de facto en su contra)](https://archivo.contagioradio.com/diputados-de-bolivia-alertan-sobre-persecucion-judicial-del-gobierno-de-facto-en-su-contra/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque la OEA se expreso en contra del reciente informe del MIT, manifestando que se trata de un estudio que no cuenta con un rigor técnico, otros mandatarios como Alberto Fernández de Argentina o Andrés Manuel López Obrador de México han expresado su apoyo a Evo Morales dando legitimidad al informe técnico que demostraría que los comicios electorales del 2019 se desarrollaron sin fraude.

<!-- /wp:paragraph -->
