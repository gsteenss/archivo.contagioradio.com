Title: Denuncian captura de ocho líderes reclamantes de tierras en Guacamayas en el Urabá
Date: 2019-11-28 18:45
Author: CtgAdm
Category: DDHH, Nacional
Tags: Falsos Positivos Judiciales, Guacamayas, Líderes reclamantes de tierras, Restitución de tierras, Urabá Antioqueño
Slug: denuncian-captura-de-ocho-lideres-reclamantes-de-tierras-en-guacamayas-en-el-uraba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/restitucion-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Archivo] 

Organizaciones sociales denuncian que se vienen adelantando capturas ocho lideres reclamantes de tierras en zona rural de Guacamayas en Turbo, Antioquia, **al desconocer sus derechos etnicoterritoriales sobre cerca de 1.542 hectáreas.** Las capturas se dan en medio de un contexto en que grupos paramilitares continúan ejerciendo control sobre la zona y que podrían provocar un desplazamiento masivo de más de 2.000 personas  habitantes del territorio colectivo de La Larga y Tumaradó.

La noche del pasado miércoles, en Orito Putumayo, la familia de Tito David Gómez - líder reclamante quien dejó el Urabá en 2015 como resultado de un intento de asesinato en su contra -  informó al también líder social Julio César León director de la Corporación de Líderes Sociales y Víctimas de Colombia, que Tito había sido capturado por agentes de la Policía.

Los líderes que fueron capturados y quienes son ámpliamente reconocidos por los habitantes del territorio colectivo como líderes sociales son Julio Correa, Ómar Quintana, Sandra Medrano, Víctor Manuel Correa, Fredy Correa, Saulo David y Rosembert Ibáñez. [(Le recomendamos leer: Paramilitares arremeten de nuevo contra reclamantes de Las Guacamayas) ](https://archivo.contagioradio.com/paramilitares-arremeten-nuevo-reclamantes-las-guacamayas/)

### De Víctimas a victimarios de quienes pretenden despojarlos de sus tierras

Según Juan Meneces, líder social y defensor de DD.HH. que ha acompañado el proceso de restitución, esta captura masiva es consecuencia de una contra demanda de quienes en aquel entonces y fruto del despojo, adquirieron los terrenos de forma irregular, "los victimarios colocaron denuncias justificando que en el momento en  que los dueños legítimos de las tierras ingresaron fue sin el acompañamiento de la institucionalidad", explica.

Tras la captura de  Tito, relata Julio César León, el líder fue trasladado hasta Turbo donde permanece sin que la comunidad tenga mayor información de él o los otros líderes, **quienes ya habían sido beneficiarios de restitución de tierras de forma legal.**  "Están secuestrados, no sabemos qué es la vida de estos compañeros, no sabemos qué está pasando ni siquiera sabemos si fue realmente la Policía quien los capturó", agrega Meneces. [(Lea también: Corte Constitucional ordena restitución de tierras a 27 familias en Guacamayas)](https://archivo.contagioradio.com/corte-constitucional-ordena-restitucion-de-tierras-a-27-campesinos-en-guacamayas/)

>  "No nos han permitido hablar con ellos, es preocupante porque quienes tenían los  terrenos  eran grupos paramilitares con documentos falsos y ahora capturan a los campesinos que son sus dueños legítimos"

Con relación a la forma en que fueron capturados, los líderes reclamantes de tierras agregan que la Policía les detuvo bajo el cargo de concierto para delinquir y contactando a las familias exigiendo datos personales y relatando información falsa para poder accederubicar a los líderes a través de sus familias.

### Grupos paramilitares continúan fortaleciendose

Julio César León resalta que en la vereda Guacamayas, los líderes han sido declarados objetivo militar en medio de un fortalecimiento del paramilitarismo en la zona, "estamos presos dentro de nuestros propios territorios, no podemos trabajar, no podemos salir" explica por su parte Juan Meneces.  [(Le puede interesar: Después de 20 años de lucha, campesinos regresan a sus tierras en Guacamayas, Urabá)](https://archivo.contagioradio.com/despues-de-20-anos-de-lucha-campesinos-regresan-a-sus-tierras-en-guacamayas/)

Al respecto, en septiembre de este año, el líder social Yeison Mosquera denunció ser víctima de nuevas amenazas producto de las continuas denuncias que han hecho contra la vulneración de DDHH en Chocó y la incursión de grupos armados que victimizan a las comunidades. ["No nos dejen solos", la petición de líderes sociales del Chocó ante amenazas](https://archivo.contagioradio.com/no-dejen-solos-lideres-sociales-choco/)

Debido a las capturas masivas  realizadas en contra de los reclamantes, otros líderes permanecen escondidos, ante la forma irregular en que se han efectuado los procedimientos, razón por la cual las comunidades están exigiendo asesoría jurídica e intervención de la Defensoría del Pueblo para que se pueda esclarecer la situación y los legítimos dueños de las cera de 1.542 hectáreas puedan regresar a sus viviendas con garantías de seguridad.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_44902765" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44902765_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_44902828" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_44902828_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
