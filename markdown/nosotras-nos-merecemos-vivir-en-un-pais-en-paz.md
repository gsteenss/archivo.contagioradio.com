Title: "Nosotras merecemos vivir en un país en paz"
Date: 2016-03-08 11:08
Category: Mujer, Nacional
Tags: Día internacional de la mujer, proceso de paz, Un Millón de Mujeres por la paz
Slug: nosotras-nos-merecemos-vivir-en-un-pais-en-paz
Status: published

###### Foto: iniciativadebate 

###### [8 Mar 2016] 

En el marco de la conmemoración del Día Internacional de la Mujer Trabajadora, **este martes en el monumento Policarpa Salavarrieta, en el centro de Bogotá, se reúnen ‘Un Millón de Mujeres por la Paz’** para realizar una manifestación simbólica de compromiso con la reconciliación entre las y los colombianos, en torno al proceso de paz que se lleva a cabo en La Habana.

Posteriormente sobre las 5:00pm se realizará un encuentro académico que conmemorará las luchas y apuestas que ha emprendido el Movimiento de Mujeres históricamente en el país.

‘Un Millón de Mujeres por la Paz’, **es una plataforma ciudadana, plural y participativa, que surge como iniciativa desde diferentes lideresas sociales y que busca unir a las mujeres del común,** pertenecientes a diferentes vertientes ideológicas y que crean firmemente en el compromiso de la construcción de paz, con el fin de generar un trabajo mancomunado de empoderamiento que conlleve a replantear la paz desde una visión de género, que implica democracia, justicia, inclusión, derechos humanos, respeto a la diversidad, al ambiente y al patrimonio, plenas garantías para la participación política y social y, desde luego, la eliminación de toda forma de violencia contra la mujer.

De acuerdo con el comunicado de la iniciativa, la conformación de esta plataforma se da a partir del método 10 X 10**: “cada mujer que se sume a este compromiso deberá sumar 10 más, y así en conjunto generar un tejido horizontal,** que propende por el trabajo colectivo y solidario”.

Esta iniciativa se propone tres grandes retos: el primero de ellos va dirigido a la  refrendación de los acuerdos de paz, para lo cual buscan emprender una campaña que permita que a través de un ejercicio pedagógico de sensibilización y del método 10 x10 sumar a Un Millón de Mujeres que incidan y apoyen el mecanismo de refrendación de los acuerdos de paz.

En segundo lugar, anclar el trabajo desde las redes territoriales,  sectoriales  y comunitarias en el ejercicio de Verificación de los Acuerdos de Paz, para a**sumir un rol de veeduría que coadyuve con otros mecanismos de carácter nacional e internacional.**

Y por último, desde el movimiento de Mujeres participar protagónica y activamente en la construcción de la paz, **promoviendo las agendas aplazadas de las mujeres y de la sociedad,  con una decidida participación política,** que lleve  a la construcción de un país incluyente, más igualitario y donde impere la cultura de la paz.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
