Title: Amenazan organización que lidera la sustitución de cultivos en Piamonte, Cauca
Date: 2019-04-17 12:17
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Amenazas contra líderes sindicales, Cauca, Piamonte, Plan de Sustitución de Cultivos de Uso ilícito, PNIS
Slug: amenazan-organizacion-que-lidera-la-sustitucion-de-cultivos-en-piamonte-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/sustitucion_Foto-Programa-de-sustitución-de-cultivos-en-Briceño-Antioquia-Alta-Consejería-Presidencial-para-el-Posconflicto-696x474.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alta Consejería Presidencial para el Posconflicto] 

La **Asociación Municipal Campesina de Trabajadoras y Trabajadores de Piamonte (ASIMPTRACAMPIC)**, organización sindical que ha liderado la sustitución de cultivos en ese municipio caucano, denuncia la aparición de mensajes amenazantes contra la vida de cinco de sus integrantes desde el pasado 15 de abril, firmados por grupos como el Cartel de Sinaloa y las disidencias, en los que responsabilizan al Programa Nacional de Sustitución de Cultivos de Uso Ilícito (PNIS) y a sus promotores por la reducción de coca en la zona y en consecuencia de su método de lucro.

Según Maidani Salcedo, representante legal de ASIMPTRACAMPIC, han recibido reiteradas amenazas desde el 2012, debido a su labor de veeduría, después de firmados los acuerdos estas intimidaciones se volvieron directas. Esta vez los mensajes van dirigidos puntualmente contra Clara Eugenia Cantillo, secretaria de Derechos Humanos de la organización, Eida Ruth Figueroa, secretaria de comunicaciones y contra los líderes sindicales **Amado de Jesús Grisales, Giovani Bastidas y Alexander Burbano Macias, precandidatos a la alcaldía de Piamonte.**

> Denuncia sobre amenazas de muerte a dirigencias regionales de Asimtracampic en Piamonte, Cauca. Demandamos la atención inmediata del gobierno de Iván Duque y la defensa de la vida en el sur de país. [pic.twitter.com/ZugDp933LS](https://t.co/ZugDp933LS)
>
> — AlaOrillaDelRío (@AlaorillaCaq) [16 de abril de 2019](https://twitter.com/AlaorillaCaq/status/1118171324092784640?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
A pesar de contar con el apoyo de las autoridades del municipio, Salcedo advierte que "no necesitamos un pie de fuerza, necesitamos la implementación del PNIS para dar cumplimiento y no darle cabida a que se diga que no sirvió" agregando que el proceso avanza de forma muy lenta, se encuentran apenas en el cuarto ciclo de pago y trabajan en el acompañamiento técnico, sin embargo los retrasos han ocasionado que las familias que decidieron no acogerse al plan de sustitución, reafirmen su posición frente a un Gobierno que no ha cumplido con lo pactado. [(Le puede interesar: Al Bajo Cauca no llega la sustitución pero si la extorsión y el desplazamiento)](https://archivo.contagioradio.com/al-bajo-cauca-no-llega-la-sustitucion-pero-si-la-extorsion-y-el-desplazamiento/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
