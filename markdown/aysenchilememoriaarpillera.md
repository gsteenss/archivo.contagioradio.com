Title: La Arpillera: los desaparecidos de Aysén como arte y memoria
Date: 2017-03-14 12:05
Category: Hablemos alguito
Tags: arpillera, Aysén, Chile, memoria
Slug: aysenchilememoriaarpillera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/unnamed-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio] 

###### [24 Feb 2017] 

<iframe id="audio_17533321" src="https://co.ivoox.com/es/player_ej_17533321_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La Arpillera ha sido un mecanismo de memoria, de resistencia al olvido, ha sido un grito contra la impunidad. Así lo expresan y lo viven las familias de los 12 jóvenes asesinados por Carabineros en Aysén, Chile, entre 1997 y 2002. Tal como lo cuentan los propios familiares y los jóvenes que han querido acompañar este proceso.

Las familias de los 12 jóvenes conocidos como “las víctimas del río Aysén” en Chile, no se cansan de relatar los hechos que les marcaron la vida. 12 jóvenes, alegres, soñadores, entusiasmados por vivir fueron víctimas de un entramado criminal que evidencia que las prácticas de la dictadura militar permanecen en ese país y que incluso han trascendido las fronteras.

***Una figura atada de manos boca abajo, una imagen entre mil colores que plasma el dolor de la muerte a través del rojo…***

Muchos de los jóvenes fueron secuestrados, sometidos a torturas y asesinados con toda crueldad por unidades de las fuerzas de Carabineros que estaban implicados en casos de narcotráfico en la región y que asesinaron, amenazaron y acabaron con las vidas de los jóvenes para encubrir sus crímenes. Además los arrojaron al río Aysén para tratar de encubrir el asesinato con un supuesto suicidio.

***El sol como testigo inmóvil pero también como guardián de la verdad...***

De esta manera, con las mentiras y entramado de impunidad se ha extendido a los largo de estos 15 años. Ver como los testigos se retractaban uno a uno de las declaraciones que descubrían la responsabilidad de Carabineros en los crímenes. Aún más desgarrador fue ver también como algunos testigos fueron asesinados.

La investigación del Estado también fue otro hecho revictimizante. Las madres de los jóvenes no entienden como una delegada de presidencia para esa investigación también concluyó que los jóvenes se suicidaron a pesar de las evidencias. Para el Estado chileno, un joven se ahorcó colgándose de un árbol más pequeño que él y otro se suicidó aun teniendo las manos atadas a la espalda.

Un contra sentido que solamente se puede explicar haciendo conciencia de la crueldad del Estado, haciendo conciencia también del daño que una dictadura pretende hacer inmarcesible. Pero también un contrasentido que, a través de la Arpillera, se resiste al olvido y a la impunidad. Le puede interesar: [Músicos chilenos se unen para salvar el río Maipo](https://archivo.contagioradio.com/maipo-chile-hidroelectrica-musica/).
