Title: La música, tema central del XIII Festival de cine colombiano de Medellín
Date: 2015-08-24 17:30
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: 13 Festival de Cine colombiano de Medellín, Humberto Dorado actor, José Luis Rugeles, La selva inflada película estreno, Nascuy Linares El abrazo de la serpiente, Ruy Folguera compositor cine
Slug: la-musica-tema-central-del-xiii-festival-de-cine-colombiano-de-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Méliès_Mélomane.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [24, ago, 2015]

Salas de cine, teatros, universidades, parques, bibliotecas, casas culturales, estaciones del metro y tranvía, son escenario desde el 24 hasta el 28 de agosto de la edición número 13 del **Festival de Cine colombiano de Medellín**, evento que este año presenta como tema central "la música en el cine".

![Festival cine de Medellín](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/festicine-medallo.jpg){.size-full .wp-image-12693 width="1200" height="323"}

21 películas nacionales, estrenadas en 2014 y 2015, hacen parte de la muestra central del evento, destacando la proyección de las cintas participantes en el Festival de cine de Cannes: ***La tierra y la sombra***, ***Alias María*** y ***El abrazo de la serpiente**.* Durante el evento tendrá lugar el pre-estreno de producciones como *La semilla del silencio*, *NN*, *Tiempo perdido*, *Hombres solos* y ***La selva inflada**,* y 7 cortometrajes colombianos.

[![Humberto Dorado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/dorado.jpg){.size-full .wp-image-12696 .alignright width="218" height="221"}](https://archivo.contagioradio.com/la-musica-tema-central-del-xiii-festival-de-cine-colombiano-de-medellin/dorado/)

Para esta edición, el Festival, rendirá homenaje al actor, guionista y dramaturgo de cine y televisión **Humberto Dorado**, con una muestra retrospectiva de su trabajo en las películas: La estrategia del Caracol, Del amor y otros demonios y Golpe de Estadio. Dorado, participará además en el **Encuentro de Actores**, junto a Julián Román (*La semilla del silencio*), Humberto Arango (*Ella*), Carlos Fernando Pérez (*Gente de Bien*) y Karen Torres (*Alias María*).[  
](https://archivo.contagioradio.com/la-musica-tema-central-del-xiii-festival-de-cine-colombiano-de-medellin/festicine-medallo/)

Simultáneamente, como parte del componente académico del evento, se realizará el **Seminario de Música en el cine**, 11 ponentes en 10 conferencias entre las que se encuentra el **Taller de Talentos cinematográficos, **a cargo del compositor venezolano **Nascuy Linares**, responsable de la banda sonora de "**El abrazo de la Serpiente**"; y desde Los Angeles **Ruy Folguera**, compositor argentino, reconocido por su trabajo en "**El páramo**", "**Corazón de León**", el remake de "El día que la tierra se detuvo" y "Un paseo por las nubes" en Hollywood.

Otra de las apuestas del evento organizado por la Corporación Festival de Cine de Santa Fe de Antioquia, es en lo social con dos iniciativas: "**Pelaos al Cine**", un programa que busca llevar películas y conversatorios a las aulas de clase, con el apoyo de las secretarías de Educación y Cultura Ciudadana de la Alcaldía de Medellín y con el **Taller de prevención de reclutamiento de menores en el conflicto armado**, organizado en alianza con la Secretaría de Gobierno y Derechos Humanos, a partir del argumento de "Alias María" la cinta de **José Luis Rugeles**, quien asistirá en compañía de Karen Torres, protagonista de la película

La entrada a las funciones es gratuita, algunas requieren invitación especial, encuentre en el enlace la [Programación](http://www.festicineantioquia.com/index.php/festicine-colombiano/programacion-festival-de-cine-colombiano-de-medellin) completa del Festival.
