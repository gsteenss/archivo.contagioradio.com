Title: Latinoamérica se une para enfrentar impactos socioambientales de las inversiones chinas
Date: 2017-05-18 06:13
Category: Ambiente, eventos
Tags: China, Derechos Humanos
Slug: latinoamerica_inversiones_chinas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/DADdo6LW0AE_Jii.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Ambiente y Sociedad 

###### [18 May 2017] 

[Las inversiones chinas en América Latina están trayendo consigo nuevas problemáticas socioambientales. Por eso, este 17 y 18 de mayo se analizan en Bogotá, dichos proyectos, para fortalecer las acciones e incidencias frente a inversiones chinas en América Latina y el Caribe.]

[Las principales inversiones en Latinoamérica se ven en Ecuador con el proyecto Panantza San Carlos; en Venezuela con el Arco Minero del Orinoco; Perú, Las Bambas; Nicaragua, con el Canal interoceánico y **en Colombia también las inversiones chinas en el proyecto petrolero de la compañía  Emerald Energ, donde se tienen previstos [43 los bloques petroleros  en Caquetá](https://archivo.contagioradio.com/43-bloques-petroleros-en-caqueta-amenazan-la-amazonia-colombiana/).**]

[En ese contexto, se desarrolla el Taller Regional en el que se reúnen expertos en financiación de banca de desarrollo, para evaluar las reglas que debería cumplir la cooperación de China, para que se garanticen los derechos humanos y de la naturaleza, como lo explica Margarita Flores, directora de la Asociación Ambiente y Sociedad, una de las organizaciones que hace parte de la logística del evento.]

[Con **el taller se busca fortalecer el accionar conjunto de la sociedad civil (comunidades, movimientos y organizaciones sociales, nacionales y locales)** que están siendo afectados por proyectos financiados por banca China y qué realizan seguimiento de los impactos sociales y ambientales de estas inversiones.  Para ello, se abordan temas relacionados con las políticas sociales y ambientales actualmente implementadas por instituciones chinas, el análisis del financiamiento chino presente en la región, así como los proyectos a los que se destina y la identificación de afectaciones.]

[Al final de la jornada, se consolidarán alianzas con China y los aliados de la región y se presentará la **Plataforma Colaborativa de Inversiones Chinas en América Latina.**]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
