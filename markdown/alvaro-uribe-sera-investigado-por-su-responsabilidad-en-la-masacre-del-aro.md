Title: Álvaro Uribe será investigado por la Masacre de El Aro
Date: 2015-10-06 09:32
Category: Entrevistas, Judicial
Tags: alvaro uribe velez, Fiscalía General de la Nación, Iván Cepeda, Las Convivir, los doce apostoles, Masacre del Aro, Santiago uribe
Slug: alvaro-uribe-sera-investigado-por-su-responsabilidad-en-la-masacre-del-aro
Status: published

###### [Foto: lopezdoriga] 

<iframe src="http://www.ivoox.com/player_ek_8822017_2_1.html?data=mZ2flJWVe46ZmKiakpqJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSnhp2eztvFttCfttfWxMqPt8bmhqigh6aVb8ri18rg1s7LpcXjjNXc1JDQpYzBwtjOxdfJb8XZzZCu1NSRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ivan Cepéda, Senador Polo Democrático] 

###### [06 Oct 2015] 

La **Fiscalía General de la Nación** tras examinar las declaraciones del pasado mes de agosto en Miami del desmovilizado paramilitar Diego Fernando Murillo alias “Don Berna", decidió **compulsar copias a la Corte Suprema de Justicia para que investigue al ex presidente y ahora senador Álvaro Uribe Vélez** por su responsabilidad en la planificación y comisión de la Masacre del Aro el 22 de octubre de 1997, así como por la creación y proliferación de grupos paramilitares en el departamento de Antioquia.

El senador del Polo Democrático Iván Cepeda Vargas asegura que “ésta es una decisión acertada y largamente esperada”, pues “Uribe siendo gobernador recibió denuncias por parte de organizaciones de derechos humanos sobre la proliferación de grupos paramilitares en la zona del Aro” que no acató, por el contrario "Los paramilitares estaban a sus anchas en Antioquia, luego de la promoción de las empresas Convivir".

Cepeda afirma que la masacre del Aro ha sido un hecho estudiado desde su ocurrencia y a la fecha está implicado no sólo Álvaro Uribe sino **también su hermano Santiago Uribe, por la creación del grupo paramilitar “Los Doce Apóstoles”** a principios de la década de los noventa, por lo que “se espera que los jueces de la sala penal de la corte suprema de justicia examinen de manera rigurosa el material” y se logre llegar a la verdad.

**El próximo 17 de octubre Santiago Uribe deberá comparecer ante la Fiscalía para declarar frente a las acusaciones por hechos de secuestro**, extorsión y homicidio cometidos por “Los Doce Apóstoles” entre los años 1993 y 1994 en los poblados de Yarumal y Santa Rosa de Osos, región noroccidental del departamento de Antioquia.

La masacre del Aro, ocurrida en 1997 el 22 de Octubre. Según la Comisión Interamericana de DDHH, en ese hecho fueron asesinadas 15 personas. Según declaraciones del paramilitar Francisco Barreto, también asesinado, el gobernador de Antioquia, en ese entonces, habría usado un helicóptero de la gobernación en medio de la masacre y la habría corroborado personalmente.

Se espera que en los próximos días haya una respuesta por parte de la Corte Suprema de Justicia acerca de los fiscales que serán encargados del caso y las fechas en las que se realizarán las indagaciones pertinentes.
