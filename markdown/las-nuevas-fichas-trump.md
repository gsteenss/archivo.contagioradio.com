Title: Las nuevas fichas de Trump
Date: 2016-11-21 15:00
Category: El mundo, Política
Tags: Conflicto Israel-Palestina, Donald Trump, Ocupación israelí
Slug: las-nuevas-fichas-trump
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Donald-Trump.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Nov 2016] 

Además de los posibles nombramientos de **Jeff Sessions** como fiscal general, **Mike Pompeo** como director de la CIA y el general retirado **Michael Flynn** como asesor de seguridad nacional, en las últimas horas Donald Trump reveló a otras tres de sus fichas, **Steve Bannon** quien sería el jefe de estrategia en la Casa Blanca, **Mitt Romney** como secretario de estado y **Mike Huckabee** como embajador de Estados Unidos en los territorios ocupados de Palestina.

Estos personajes tienen en común la afinidad y pertenencia al partido Republicano, al movimiento Tea Party, cercanía a Trump durante su campaña, **fuertes convicciones ideológicas conservadoras, acusaciones de racismo y apoyo al régimen de Israel.**

### **¿Quiénes son estos personajes?** 

**Jeff Sessions** quien ocuparía el cargo de Fiscal General, fue fiscal del distrito sur de Alabama de 1981 a 1993 y luego en 1994. Es senador Desde 1996 y ha sido en varias ocasiones. Para muchos analistas, es una **ficha esencial para el plan de Trump de expulsar a los 11 millones de migrantes ilegales que hay en el país.**

En 1986 durante la presidencia de Ronald Reagan lanzó varias afirmaciones racistas contra la población afro, incluso uno de sus trabajadores cercanos, de ascendencia afro, denunció que una vez le advirtió que debía **“cuidarse de aquello que le decía a la gente blanca”.**

**Mike Pompeo** quien sería director de la CIA, es p**artidario del controvertido programa de recopilación de datos de la Agencia Nacional de Seguridad** y trató de restablecer el acceso a la información que ya había sido recopilada desde que entró en vigor la **Ley Patriota, en octubre de 2001**, hasta finales de 2015.

Se o**pone al cierre del campo de detención de Guantánamo**, a la **regulación de gases de efecto invernadero**, se ha opuesto al **aborto incluso en casos de violación o incesto** y está a favor de las políticas económicas que promueven la producción y comercio de **alimentos modificados genéticamente**.

**Michael Flynn** general retirado de la Armada norteamericana, sería la mano derecha en seguridad del nuevo presidente estadounidense, ocupó la dirección de la Agencia de Inteligencia de Defensa durante el gobierno de Obama. Analistas aseguran que si acepta la oferta para ocupar el cargo **“será un guardián estratégico de un presidente que no tiene experiencia en asuntos militares ni de política exterior”.**

Su posición ante el Estado Islámico ha sido la de proveer **más apoyo militar en terreno**, algo que ha eludido el gobierno Obama y de **cooperar con Moscú como aliado militar**. También ha manifestado** **públicamente que está a **favor de cancelar el acuerdo nuclear con Teherán** e imponer duras sanciones económicas y políticas a la capital de la República Islámica.

**Steve Bannon**, quién sería el jefe de estrategia en la Casa Blanca y que es bien conocido por su ideología de “nacionalista blanco”, fue un veterano de guerra, hizo parte de la junta administrativa de **Goldman Sachs** y fue productor de **Hollywood.**

Es uno de los dirigentes de la c**adena de noticias Breibart, el tercer medio de comunicación conservador más grande de Estados Unidos** y líder del movimiento llamado **“Alt-right”** o “alternativa derecha” que posee un sitio web que publica a diario información marcada por **posturas racistas, antiinmigrantes y misóginas.**

Algunos de los titulares que se encuentran en dicho sitio web son “**No quiero que mis hijas vayan a una escuela con judíos”**, “No me gustan los judíos ni la forma en la que crían a sus mocosos hijos”, **“Los anticonceptivos vuelven a las mujeres locas y poco atractivas”**, “Los derechos de los homosexuales nos han hecho más idiotas” y **“Abolir la esclavitud fue una mala idea”.**

**Mitt Romney** quien durante la campaña de Trump se había destacado como una de las voces más críticas dentro del Partido Republicano y **llegó a calificar al magnate neoyorquino de "fraudulento" y "farsante"**. Está siendo considerado seriamente como **posible nuevo secretario de Estado.**

Es un empresario y político estadounidense, quien fue gobernador del estado de Massachusetts durante el periodo 2003-2007 y **candidato republicano a las elecciones presidenciales de 2012.**

Por último **Mike Huckabee** miembro del partido Republicano, quien fue Gobernador de Arkansas en 1996 y 2007, candidato a las elecciones presidenciales de 2008 y 2016, ha sido postulado como **embajador de los Estados Unidos en Tel Aviv.** Varios medios y analistas aseguran que Huckabee mantiene **estrechas relaciones con organizaciones sionistas y con movimientos como el Consejo Yesha**, que defienden la vía de los a**sentamientos ilegales israelíes en territorios palestinos. **

###### Reciba toda la información de Contagio Radio en [[su correo]
