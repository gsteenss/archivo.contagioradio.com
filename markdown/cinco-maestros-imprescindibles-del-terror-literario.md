Title: Cinco maestros imprescindibles del "Terror" literario
Date: 2015-10-31 11:00
Category: Viaje Literario
Tags: Edgar Allan Poe, Escritores de Terror, Halloween, Historias de Terror, Howard Lovercraft, Koji Suzuki, Mary Shelley, Stephen King, Terror
Slug: cinco-maestros-imprescindibles-del-terror-literario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Terror1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: lookfordiagnosis.com 

###### [19 oct 2015]

Durante el mes de conmemoración del Halloween,  no solo el cine tiene películas que no nos podemos permitir dejar de ver, **la literatura también hace su aporte desde las letras y la imaginación**, convirtiendo la realidad del mundo y su brutalidad en **historias que colocan los pelos de punta**. Incluso muchas de estas películas son inspiradas en libros.

En esta lista encontraremos algunos de los escritores que hicieron del terror algo atrayente para el público y se hicieron maestros en el género, irrumpiendo nuestras sociedades con su forma de vida y sus escritos, es por esto que nosotros recomendamos estos cinco a **los cuales debemos echarles una leída**:

**Mary Shelley**

[![Mary Shelley](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Mary-Shelley.jpg){.aligncenter .size-full .wp-image-15887 width="768" height="432"}](https://archivo.contagioradio.com/?attachment_id=15887)

###### Foto: Biography.com 

Nació en Londres el 30 de agosto de 1797. Fue creadora en 1918 del libro que dio inicio a la ciencia ficción y que aún hoy es uno de los grandes relatos de horror de todos los tiempos, la novela Frankenstein o el moderno Prometeo.

Sobre este libro se dice que Shelley lo escribió por una apuesta, la noche del 16 de junio de 1816, cuando estaba reunida con Lord Byron y otros en una villa en los alrededores de Ginebra y empezaron a relatar cuentos de terror para entretenerse, Mary imaginó entonces a Frankestein inspirada en una pesadilla que tuvo a los dieciocho años de edad. Escribió la novela tras una apuesta con Byron, tal y como narra ella misma en el prólogo de la edición de "Frankenstein" de 1831 “esta obra, un logro más que notable para una autora de sólo 20 años, se convirtió de inmediato en un éxito de crítica y público.”

A pesar de que sus otras obras no tienen la misma fuerza, ni el mismo reconocimiento Shelley terminó un relato de vampiros –El Vampiro-, cuyo protagonista es lord Ruthven, alter ego del propio Byron, relato que sentó las bases para lo que luego constituiría la literatura “vampírica”.

### **Otras de sus obras son:** 

Mathilda (1819)  
Valperga; o Vida y aventuras de Castruccio, Príncipe de Lucca (1823)  
El último hombre (1826)  
Lodore (1835)  
Falkner (1837)

**Edgar Allan Poe**

[![Allan Poe](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Allan-Poe.jpg){.aligncenter .size-full .wp-image-15889 width="1200" height="720"}](https://archivo.contagioradio.com/?attachment_id=15889)

###### Foto: laladronadenoticias.com 

Nació en Boston, Estados Unidos el 19 de enero de 1809, fue un escritor, poeta, crítico y periodista romántico estadounidense, pasó a la historia como uno de los más grandes de la literatura norteamericana

Allan Poe es reconocido como uno de los maestros universales del relato corto, renovador de la novela gótica y sus cuentos de terror son recordados en todo el mundo. Se dice que su amor por las letras apareció a los seis años cuando viajó con sus padres adoptivos a Gran Bretaña por algún negocio de sus padres y Poe se encontró con castillos victorianos, lúgubres y oscuros.

Su vida desde los 17 años giró en torno al alcohol y las drogas como el Opio y el láudano de las cuales se conoce que era consumidor, En 1836 se casó con su sobrina Virginia Clemm de tan solo 13 años, pero debido a una Tuberculosis ella muere en 1847, Allan Poe cae enfermo y a los 40 años de edad muere el 7 de octubre en la ciudad de Baltimore, Estados Unidos.

### **Algunas de sus obras son:** 

Manuscrito hallado en una botella (1833)

La caída de la casa Usher (1839)

Los crímenes de la Calle Morgue (1841)

El gato negro (1843)

**Howard Phillips Lovecraft**

[![lovecraft-main](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/lovecraft-main.jpg){.aligncenter .size-full .wp-image-15891 width="1063" height="657"}](https://archivo.contagioradio.com/?attachment_id=15891)

###### Foto: Holyhelen.com 

Nació en 1890 en Rhode Island Estados Unidos, es uno de los escritores de terror y ciencia ficción más importantes del siglo XX, aunque en su vida nada valorada, su obra después de muerto es ampliamente valorada.

Lovecraft ocupa en el canon de la literatura norteamericana un lugar preeminente: recientemente ha visto publicada parte de su obra en la "Library of América", una especie de salón de la fama. Los relatos de Howard Lovercraft están basados en que "este mundo había estado habitado en tiempos remotos por otra raza, que fue aniquilada y expulsada cuando ejercía la magia negra, pero que sigue viviendo fuera del mundo, estando dispuesta en todo momento a volver a tomar posesión de esta tierra".

Los relatos y novelas de Lovecraft, pueden ubicarse en los límites de la mitología y la fantasía visionaria, son verosímiles, pues a pesar del instinto macabro del autor, un lenguaje detallista, bien utilizado y lento van organizando un pequeño mundo autosuficiente y creíble, incluso posesivo para muchos lectores. Muere el 19 de julio de 1898 en su pueblo natal.

**Algunas de sus obras son:**

 En las montañas de la locura (1931)

El color que cayó del espacio (1927)

Las ratas en las paredes (1924)

El caso de Charles Dexter Ward (1927)

**Stephen King**

[![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/sthephen-king.jpg){.aligncenter .size-full .wp-image-15892 width="2500" height="1673"}](https://archivo.contagioradio.com/?attachment_id=15892)

###### Foto: blastr.com 

Su seudónimo es el de Richard Bachman, nació el 21 de septiembre de 1947, es un escritor Estadounidense, al cual algunos críticos relacionan su producción literaria con un accidente que King presenció cuando niño , en el que uno de sus amigos quedó atrapado bajo los rieles. Esto siempre lo ha negado el escritor.

Las novelas de terror y suspenso de Sthephen King están construidas basándose en una visión constante del mundo y tal vez porque cuentan con una alta dosis de verosimilitud, han sido llevadas a la pantalla grande.

Hacia finales de los años 1970 y hasta fines de los 80\`s King comenzó a desarrollar problemas de adicción, primero al alcohol y posteriormente a otros tipos de drogas. Después de la publicación de Los TommyKnockers en 1987, su familia y amigos decidieron mostrarle los residuos de su estudio para mostrarle el grado de adicción alcanzado: latas de cerveza. Cigarrillo, bolsas de cocaína, botellas de valium,eran algunas de las cosas que había, King Solicitó ayuda y abandonó toda forma de alcohol y drogas hacia fines de esa década.

### **Algunas de sus obras son:** 

Carrie (194)

El resplandor (1977)

El cujo (1981)

Misery (1987)

Insomina (1994)

**Koji Suzuki**

[![Koji Suzuki](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Koji-Suzuki.jpg){.aligncenter .size-full .wp-image-15894 width="700" height="402"}](https://archivo.contagioradio.com/?attachment_id=15894)

###### Foto: claymoorslist.com 

Es un escritor japonés que nació en 1957, graduado de la Universidad de Keio como maestro en literatura francesa, se dedicó a la literatura del horror , además que también se ha dedicado a escribir programas y películas populares en la televisión de su país, además de ser un activo escritor de literatura infantil.

Sus libros son traducidos a varios idiomas, se venden a través de los EE.UU “varias de sus obras han sido adaptadas al cine como Ringu (1991), la cual fue adaptada como el aro en occidente.

Para Koji el miedo verdadero “tiene que estimular la imaginación del adulto. En América y Europa la mayoría de las películas del horror cuentan la historia de la exterminación de espíritus malvados. Las películas japonesas de horror terminan con una sugerencia que todavía sigue existiendo el espíritu. Eso es porque los japoneses no miran a los espíritus solamente como enemigos, sino como los seres que coexisten con este mundo y el nuestro”.

Suzuki demanda nunca haber tenido un encuentro personal con los fantasmas. Él no cree en ellos. De hecho, niega estar interesado en las películas de horror.

### **Algunas de sus obras son:** 

Rakuen (paraíso)

Su trilogía: Ringu (1991); Rasen (1996), y Loop (1998)

Actualmente prepara Edge City,

** **
