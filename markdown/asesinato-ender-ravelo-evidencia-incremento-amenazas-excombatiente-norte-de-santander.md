Title: Asesinato de Ender Ravelo evidencia la ausencia de garantías para excombatientes en Norte de Santander
Date: 2019-12-23 12:27
Author: CtgAdm
Category: DDHH, Nacional
Tags: asesinato de excombatientes, Norte de Santander, tibu
Slug: asesinato-ender-ravelo-evidencia-incremento-amenazas-excombatiente-norte-de-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/bandera-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FARC] 

El pasado 21 de diciembre en Tibú, Norte de Santander fue asesinado con arma de fuego el excombatiente Ender Elias Ravelo mientras se encontraba en un establecimiento comercial. Según directivas del partido Fuerza Alternativa Revolucionaria **han sido asesinados 180 excombatientes durante la firma del Acuerdo y al menos 67 durante este año.**

Según versiones preliminares, los hechos ocurrieron en el barrio Santander, cuando cerca de las 9:00 pm, un hombre que portaba un arma de fuego entró al lugar en el que se encontraba Ender junto a sus acompañantes: su esposa Leidy Arias y su amigo, Luis Alfredo Pérez, al ingresar el hombre habría disparado contra las tres personas para luego huir en una motocicleta con otra persona que aguardaba afuera.

Como consecuencia de los hechos, su esposa y su acompañante resultaron heridos y trasladados a en un centro asistencial de la zona, sin embargo el excombatiente falleció producto de los cuatro impactos de bala causados por el atacante. [(Lea también: Con Wilson Parra, son 169 excombatientes de FARC asesinados desde la firma del Acuerdo de Paz](https://archivo.contagioradio.com/con-wilson-parra-son-169-excombatientes-de-farc-asesinados-desde-la-firma-del-acuerdo-de-paz/))

Sandra Sierra, abogada y defensora de DD.HH. quien ha realizado un acompañamiento a los excombatientes en su proceso de reincorporación en Norte de Santander  relata que Ender tenía un negocio junto a su familia además venía trabajando junto a la Jurisdicción Especial para La Paz y estaba siendo acompañado por la Agencia para la Reincorporación y la Normalización (ARN).

### Precedentes contra la paz en  Norte de Santander

La abogada señala que desde agosto se ha presentado un incremento en la violencia en el departamento, en septiembre, fueron asesinados los excombatientes Milton Urrutia Mora, José Milton Peña Pineda y Arsenio Maldonado en Cúcuta mientras que en **el corregimiento de La Gabarra también en septiembre fue asesinado Fernando Antonio Castro García. **

**"Desde que empezó el proceso los muchachos han manifestado a la ONU  el riesgo, la inseguridad e incumplimiento por parte del Gobierno, tengo una serie personas que han sido amenazados, aquí no hay garantías"** relata la defensora de DD.HH. quien explica que hombres armados en moto, así como el caso de Ender, llegan a las viviendas de los excombatientes a intimidar a sus familias.

Aunque las autoridades aún no han atribuido el asesinato de Ender a ninguna organización ilegal, la Policía indicó que en esta zona opera el frente Juan Fernando Porras del ELN y el grupo armado organizado residual 33, por su parte Sandra Sierra señala que se han recibido amenazas vía Whatsapp por parte de las Águilas Negras. [(Le puede interesar: 66 excombatientes que apostaron a la paz han sido asesinados en 2019](https://archivo.contagioradio.com/66-excombatientes-que-apostaron-a-la-paz-han-sido-asesinados-en-2019/))

De cara a los más de cuatro asesinatos contra excombatientes ocurridos en diciembre, Tulio Murillo, consejero político departamental del Espacio Territorial de Capacitación y Reincorporación (ETCR) de Mesetas Meta, advierte que **esta es una situación que se viene generalizando en Colombia**, pese a que algunos excombatientes continúan en los espacios y otros han regresado con sus familias, otros han decidido permanecer refugiados debido a los constantes asesinatos y amenazas, que relata, se han dado contra los firmantes del Acuerdo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
