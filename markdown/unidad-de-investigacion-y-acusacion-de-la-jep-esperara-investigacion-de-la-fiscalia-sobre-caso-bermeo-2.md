Title: Unidad de Investigación y Acusación de la JEP esperará investigación de la Fiscalía sobre caso Bermeo
Date: 2019-03-01 17:57
Category: Paz, Política
Tags: Anulación de curul de Juan Carlos Losada, Fiscalía, JEP, Unidad de investigación y acusaciones
Slug: unidad-de-investigacion-y-acusacion-de-la-jep-esperara-investigacion-de-la-fiscalia-sobre-caso-bermeo-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/posesion_fiscales_uia3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [01 Mar 2019]

A través de un comunicado de prensa, el director de la Unidad de Investigación y Acusación de la Jurisdicción Especial para la Paz, Giovanny Álvarez, afirmó que frente a la captura realizada por la Fiscalía de Carlos Julian Bermeo, fiscal de apoyo de la unidad, **esperarán las investigaciones correspondientes y de comprobarse su participación condenarán los hechos. **

Álvarez afirmó que la contratación de Bermeo se dio porque cumplía con todos los requisitos para el cargo, "por ejemplo , aparte de ser abogado, el señor Bermeo es especialista en Cultura de Paz y Derecho Internacional Humanitario. Adicionalmente acreditó una maestría en Derechos Humanos y Cultura de Paz".

Asimismo, aclaró que** la Unidad de Investigación y Acusación de la JEP no ha tenido ninguna participación en los procesos en los que se estudian las solicitudes de garantía de no extradición**,  contemplada en el artículo 19 transitorio de la Constitución Política.

Por lo tanto esta Unidad tampoco ha tenido ningún tipo de participación en el proceso de Seuxis Paucias Hernández Solarte, más conocido como Jesús Santrich y** tampoco tiene "ninguna injerencia sobre las decisiones que al respecto se lleguen a tomar por los magistrados que tienen conocimiento de ese caso**".

Álvarez también señaló que confía en que la ley será implacable en el juzgamiento de personas que, con este tipo de actuaciones, empañan la labor de esta Jurisdicción y  aseguró que quedará a la espera de las decisiones judiciales que al respecto se adopten.

Patricia Línares, presidenta del Tribunal afirmó en una rueda de prensa que ninguna conducta que pueda ser tachada, comentida por servidores de la JEP, "será tolerada por la misma" y aseguró que** "serán los primeros en actuar cuando haya una acción indebida, poniendola al conocimiento de las autoridades**".

De igual forma en la declaración de la sección de revisión de la JEP afirmaron que la Unidad de Investigación y Acusación goza de autonomía administrativa para seleccionar a sus integrantes, "en consecuencia la magistratura no tiene injerencia alguna en los nombramientos".

### **Las capturas de la Fiscalía** 

Según la Fiscalía, Bermeo fue capturado junto con 4 personas más, incluyendo el exsenador Luis Alberto Gil, en un operativo adelantado en dos hoteles en el norte de Bogotá. Las primeras versiones del ente investigador aseguran que Bermeo estaría recibiendo 500 mil dólares a cambio de una oferta para incidir en el proceso de extradición de Zeuxis Hernández.

Las otras personas detenidas son Yamit Prieto Acero, Luis Orlando Villamizar y Ana Cristina Solarte Burbano, quien de acuerdo con el portal la Silla Vacía, es la compañera del fiscal Bermeo.

###### Reciba toda la información de Contagio Radio en [[su correo]
