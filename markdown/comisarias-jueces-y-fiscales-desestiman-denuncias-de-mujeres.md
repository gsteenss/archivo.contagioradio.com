Title: Comisarías, jueces y fiscales desestiman denuncias de mujeres
Date: 2017-04-11 14:54
Category: Libertades Sonoras, Mujer
Tags: colombia, feminicidio, genero, mujer
Slug: comisarias-jueces-y-fiscales-desestiman-denuncias-de-mujeres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/000182580W.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Miles Chile] 

###### [11 Abr. 2017]

**En Colombia cada día son asesinadas 4 mujeres** y en esta oportunidad el hecho que enluta al país es el feminicidio de Claudia Johana, de 40 años quien falleció por cuenta de su expareja, quien la sorprendió en su lugar de trabajo para acabar con su vida. Hace 4 meses el país también se estremeció con el asesinato y violación de Yuliana Samboní de 7 años.

Para la **representante a la Cámara, Ángela María Robledo el país se está limitando a registrar lo que sucede con las mujeres**, pero no se va más allá “yo escuchaba al general Pinilla en Bogotá y más o menos en sus declaraciones terminaba diciendo que Claudia se había enamorado de un hombre enfermo, es decir, más o menos ella tuvo la culpa y eso no se puede tolerar”. Le puede interesar: [“El Estado es responsable del feminicidio de Claudia”](https://archivo.contagioradio.com/el-estado-es-responsable-de-lo-que-le-sucedio-a-claudia/)

Este tipo de declaraciones, dice Robledo hacen que **se re victimice a las mujeres que padecen las violencias** “Claudia había hecho denuncias previamente y hay una serie de funcionarios que arrancan por la comisaria, que pasan por los jueces, por los fiscales que desestiman la urgencia, el grito de las mujeres en términos de lo que está pasando en sus casas, en sus camas y permiten que esto vaya escalando”. Le puede interesar: [El feminicidio de Micaela García moviliza Argentina](https://archivo.contagioradio.com/39025/)

Aunque en el país existen muchas leyes con miras a proteger y garantizar la vida de las mujeres, estas no se materializa en acciones reales “está la 1257 y la 1719, pero es **una ley que está en los escritorios de los fiscales, de los jueces, incluso de los mismos comisarios y que no se aplica.** A esto súmele el ambiente cultural que lo que hace es celebrar el machismo que por supuesto habita en hombres y mujeres” agrega Robledo.

Para la representante, lo que sucede en materia de cumplimiento a las leyes y a la educación, es un tema que compete no solo a las mujeres sino también a los hombres “es importante trabajar con niños y niñas para que puedan ver cómo se ejerce una democracia desde la casa”.

Sin embargo, manifiesta que **existe además una ausencia de los Gobiernos** “nunca ningún gobierno, ni en el anterior de Álvaro Uribe que es como la expresión más fuerte del patriarcado y del machismo con su “venga le pego en la cara marica”, hasta un presidente como Juan Manuel Santos que ha ido debilitando la consejería de equidad para la mujer que tiene un presupuesto más bajo que cualquier ONG” relata Robledo.

Y dice, que **lamentablemente los jueces, los fiscales no van a hacer nada, sino que hay que seguir trabajando desde las mujeres** para seguir ganando espacios “hay que seguir en las calles, expresando nuestra indignación, empoderándonos, para conocer las leyes y hacer seguir valiendo nuestros derechos. Porque leyes, contrario a lo que piensa el General Pinilla si hay, lo que pasa es que no hay operadores que las apliquen”. Le puede interesar: [Los retos del 2017 para enfrentar la violencia contra las mujeres en Colombir](https://archivo.contagioradio.com/los-retos-del-2017-enfrentar-la-violencia-las-mujeres-colombia/)

<iframe id="audio_18095199" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18095199_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
