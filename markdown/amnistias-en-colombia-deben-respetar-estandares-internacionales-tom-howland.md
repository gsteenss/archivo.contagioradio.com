Title: “Amnistías en Colombia deben respetar estándares internacionales” Tom Howland
Date: 2016-12-20 15:45
Category: Judicial, Paz
Tags: implementación acuerdos de paz, Ley de Amnistia
Slug: amnistias-en-colombia-deben-respetar-estandares-internacionales-tom-howland
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/tom11.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ifobae] 

###### [20 Dic 2016]

Después de que el proyecto de ley de Amnistía fue presentado, la oficina en Colombia del Alto Comisionado de las Naciones Unidas, en cabeza de Tom Howland expresó que debe tenerse en cuenta que todas las amnistías e indultos que se gesten a partir de los acuerdos de paz **“deben respetar plenamente los estándares internacionales en materia de derechos humanos”.**

El alto comisionado señaló que “**Los beneficios que se otorguen tienen que ser el resultado del cumplimiento de compromisos por parte de los eventuales beneficiarios** de las medidas, no pueden ser el punto de partida ni convertirse en un fin en sí mismos” Le puede  interesar: ["27 de diciembre sera la prueba de fuego para la ley de amnistía: Ivan Cepeda"](https://archivo.contagioradio.com/27-de-diciembre-prueba-de-fuego-ley-de-amnistia-33926/)

Además agrego que **“aplicarse de manera excepcional, condicionada e individualizada y generar el régimen de estímulos requeridos para lograr el funcionamiento adecuado de los mecanismos de verdad**, justicia y reparación que se han acordado en el marco del proceso de paz”. Le puede interesar: ["Ley de amnistía urgente para avanzar en implementación de acuerdos de paz"](https://archivo.contagioradio.com/ley-de-amnistia-urgente-para-avanzar-en-implementacion-de-acuerdos/)

El proyecto que ya pasó el primer debate con **15 votos a favor en Senado y 30 Cámara**, es uno de los primeros mecanismos que se activa, de los acuerdos de La Habana, y hace parte de uno de los compromisos  para que los diferentes miembros de las FARC-EP comiencen a trasladarse a las diferentes zonas veredales.

###### Reciba toda la información de Contagio Radio en [[su correo]
