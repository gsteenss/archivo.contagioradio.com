Title: Ejército detiene y amenaza a campesinos en Pajarito, Boyacá
Date: 2017-01-03 21:29
Category: DDHH, Nacional
Tags: Boyacá, campesinos, Ejército Nacional, Retenidos
Slug: ejercito-detiene-y-amenaza-a-campesinos-en-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Militaress-Boyaca-e1524691374383.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ingenieros Militares] 

###### [3 Enero 2017] 

El pasado lunes 2 de enero **tres campesinos, integrantes de la Junta de Acción Comunal – JAC - de la vereda Sabanalarga en el Municipio de Pajarito, Boyacá, fueron encañonados con fusiles por hombres con uniformes militares, quienes se identificaron como tropas del Ejército Nacional.**

Sin dar mayores detalles a los campesinos, **los amenazaron y con insultos y maltratos les hicieron desmontar de sus caballos para exigirles posteriormente que se quitaran sus botas y sombreros.** Además, según la denuncia conocida, los hombres les pidieron sus documentos de identificación y les hicieron registro fotográfico.

Rodrigo Torres Sanabria, presidente de la JAC, Héctor Julio Pérez Jiménez, tesorero de la JAC  y Pedro Antonio López González, habitante de la vereda, se dirigían por un camino de la zona para verificar lo que había sucedido en unos potreros aledaños, luego de escuchar unos disparos hacia las 8 a.m. del 2 de enero.

Según los afectados, **estuvieron retenidos por cerca de 3 horas** y dice la denuncia “les impidieron – los militares - continuar o regresar a sus casas” por lo que los campesinos tuvieron que dirigirse a una casa cercana.

Hasta el momento no han podido verificar, "si hay personas detenidas o heridos pero les han obligado a permanecer en sus casa" manifiesta la comunicación.

En la **denuncia pública, firmada por la Corporación Social Para la Asesoría y Capacitación Comunitaria -COSPACC- y la Asociación Nacional Campesina José Antonio Galán Zorro -ASONALCA**- dicen que "los militares están entrando a las casas del vecindario, incluso a casas que por las festividades están solas". Le puede interesar: [En total impunidad crímenes cometidos por Ejército Nacional en 2004](https://archivo.contagioradio.com/en-total-impunidad-crimenes-cometidos-por-ejercito-nacional-en-2014/)

Afirman los campesinos que han tenido conocimiento de que **en la zona "se está desarrollando un operativo militar, ha habido ametrallamientos y al parecer hay enfrentamientos entre el Ejército Nacional y guerrilleros del ELN".**

Dada la gravedad de la situación, las organizaciones han solicitado al Gobierno Nacional **respetar la vida e integridad de la comunidad de la vereda Sabanalarga,** así como no realizar allanamientos sin orden judicial en las viviendas de los campesinos.

De igual manera, han exigido al Gobierno que **permita la libre movilidad de los habitantes de las veredas del sector y que se les ordene a las tropas militares, que ante la Comunidad se identifiquen,** dado que son servidores públicos. Le puede interesar: [4 campesinos detenidos en desalojo a predios en Puerto Lleras](https://archivo.contagioradio.com/campesinos-desalojo-puerto-lleras/)

Por último, exigen  a la Defensoría del Pueblo, Personería de Pajarito, demás autoridades y órganos de control, qué estén atentos y den la oportuna protección de los derechos humanos de los habitantes de la vereda Sabanalarga de Pajarito, Boyacá

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio.](http://bit.ly/1ICYhVU)
