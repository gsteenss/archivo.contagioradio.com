Title: Taller de “Comunicación para la paz”
Date: 2015-02-04 22:13
Author: CtgAdm
Category: eventos
Tags: comunicacion para la paz
Slug: taller-de-comunicacion-para-la-paz
Status: published

Contagio Radio invita a participar en el taller **[“Comunicación para la paz]**” en colaboración con la asociación A Seis Manos

Con el objetivo de brindar herramientas y experiencias a quien quiera trabajar en comunicación social, en DDHH, construcción de paz y organizaciones sociales.

La composición del taller son 5 bloques temáticos de los cuales uno es totalmente práctico y se realiza en la emisora contagioradio.com. Los dos primeros bloque son totalmente teóricos e introductorios de la comunicación social y a los medios de comunicación.  El tercero es teórico-práctico sobre el uso y manejo de las redes sociales. El cuarto hace referencia a la experiencia de los comunicadores sociales por la paz en el contexto de las organizaciones sociales y del conflicto armado colombiano en post de una sociedad en paz. Por último un bloque práctico para la realización de comunicación a través de la radio en una emisora.

El taller pretende ser una herramienta para todo tipo de trabajadores de los distintos campos sociales, que tengan como objetivo aprender sobre la comunicación social en un contexto de paz o de conflicto. Siendo éste enfocado en el trabajo de las organizaciones sociales colombianas y sus distintos equipos de trabajo en comunicación. De tal forma se formará a los asistentes en las distintas temáticas referidas a la comunicación para la paz aplicada al contexto colombiano y las necesidades de las organizaciones sociales en su quehacer diario.

**[Organigrama taller:]**

**[Sabado 28 de febrero]**

10:00 a 12:00 **[Bloque I: Introducción a la comunicación social:]**

-   -Introducción a la comunicación social
-   -Principales teorías y tipología de la comunicación social
-   -La comunicación como herramienta educativa
-   -La comunicación como herramienta terapéutica
-   -La comunicación como herramienta identitaria
-   12 a 13:00 Almuerzo

13:00 a 15:00 **[Bloque 2: Introducción a los medios de comunicación social y géneros periodísticos]**

11:30h a 13:30h:

-   -Medios libres y alternativos
-   -Medios de comunicación de masas
-   -Géneros periodísticos
-   -Introducción TV, Prensa y Radio
-   -La radio libre y comunitaria en Suramérica
-   -La radio como herramienta para la transformación social

15:00 a 17:00 **[Bloque 3: Redes sociales]**

-Introducción a las redes sociales web 2.0

-   -Community manager y funciones
-   -Marca personal o Personal branding.
-   -Estrategias en redes sociales
-   -Facebook
-   -Twitter
-   -Herramientas informáticas CM

**[Sábado 7 de marzo]**

**[Bloque 4: Comunicación social para la paz]**

10:00 a 12:00 -Introducción a la comunicación social para la paz

-   -Introducción a los DDHH
-   -Una mirada a los DDHH en Colombia por Danilo Rueda Defensor de derechos humanos de la Comisión de Justicia y Paz.
-   -Periodismo de paz
-   12:00 a 13:00 Almuerzo
-   13:00 a 15:00 -Periodismo de guerra
-   -Dinámicas comunicativas en contexto de conflicto armado
-   -Comunicación social para la paz en organizaciones sociales de Colombia
-   -Estrategias comunicativas de las organizaciones sociales

**[15:00 a 17:00 Bloque 5: Teoría e introdución a  la comunicación en organizaciones sociales]**

-   -Géneros periodísticos en radio libre
-   -Locución
-   -Modulación de la voz
-   -Tipos de programas
-   -Noticiero

**[Sábado 14 de marzo]**

10:00 a 12:00    -**[Práctica en la emisora Contagioradio.com]**

-   -Locución en vivo de los distintos tipos de programas
-   -Producción radial.
-   12:00 a 13:00 Almuerzo
-    13:00 a 15:00 -Modulación de la voz
-             -Tipos de programas
-             -Noticiero

Talleres certificados Para mayor información e inscripciones envíe sus datos completos (nombre y número telefónico) a vocesconsentidos@gmail.com

**[Información:]** 300 618 5188

**[Valor:]** 200.000 mil pesos

**[Lugar:]**A Seis Manos(calle 22 no.8-60)
