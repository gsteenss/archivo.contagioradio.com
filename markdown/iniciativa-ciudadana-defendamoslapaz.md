Title: La iniciativa ciudadana que invita a que #DefendamosLaPaz
Date: 2019-02-20 12:02
Author: AdminContagio
Category: Nacional, Paz
Tags: #DefendamosLaPaz, JEP, paz, víctimas
Slug: iniciativa-ciudadana-defendamoslapaz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Diseño-sin-título3-e1550689696519-770x400-1.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2019-02-20-a-las-11.43.35-a.m..png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PizarroMariaJo] 

###### [20 Feb 2019] 

Antes del plebiscito de 2016 para refrendar los Acuerdos de Paz, grupos que se oponían a lo pactado en La Habana manifestaron sus inconformidades conduciendo a una fuerte polarización del país; dos años después y con el Acuerdo de Paz firmado, persisten los ataques a la posibilidad del cese de la violencia, prueba de ello son los intentos por evitar que la JEP tenga un marco jurídico completo para el desarrollo de sus funciones.

Por esta razón, este miércoles en horas de la mañana ciudadanos, congresistas, intelectuales, l**íderes sociales, activistas e integrantes de ambas delegaciones de la Mesa de Conversaciones de La Habana, se reunieron en el Centro Cultural Gabriel García Márquez** de Bogotá atendiendo al llamado de un encuentro de emergencia de defensores de la paz. (Le puede interesar: ["Las falacias del fiscal Néstor Humberto Martínez para objetar la JEP"](https://archivo.contagioradio.com/falacias-para-objetar-jep/))

En dicha reunión, los asistentes acordaron la creación de **\#DefendamosLaPaz,** una iniciativa que trabajará por la implementación total de los acuerdos; y como lo señala la **representante a la cámara Angela María Robledo**, insistieron en el papel de la ciudadanía para hacer sentir que la paz "no es un asunto entre la guerrilla y los gobiernos únicamente", sino que requiere profundas transformaciones sociales que garanticen el acceso a la tierra, salud, educación y acabar la desigualdad.

Adicionalmente, una de las propuestas que acogieron fue la de r**epetir este primer encuentro en los territorios para que se escuche en las diferentes regiones del país las formas en que se está construyendo paz.** Por su parte, Robledo y los demás congresistas que asistieron se comprometieron a hacer una revisión del Plan Nacional de Desarrollo en su desarrollo de los recursos para la implementación de la paz.

Por último, la Representante añadió que **una de sus primeras acciones tendrá que ver con la sanción de la Ley Estatutaria de la Jurisdicción Especial para la Paz**, buscando que se cumpla a a las víctimas y se garantice su derecho de acceso a la justicia, verdad y reparación. (Le puede interesar: "C[orte Penal Internacional manifiesta su respaldo a la JEP"](https://archivo.contagioradio.com/corte-penal-internacional-respalda-la-justicia-transicional-mediante-ataques/))

> [\#DefendamosLaPa](https://twitter.com/hashtag/DefendamosLaPaz?src=hash&ref_src=twsrc%5Etfw)z Invitamos a toda Colombia a seguir construyendo La Paz. Nos une el mandato constitucional que estipula que “La Paz es un derecho y un deber de obligatorio cumplimiento” [pic.twitter.com/diwruKp8td](https://t.co/diwruKp8td)
>
> — Iván Cepeda Castro (@IvanCepedaCast) [20 de febrero de 2019](https://twitter.com/IvanCepedaCast/status/1098218915153494016?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
