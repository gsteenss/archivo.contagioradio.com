Title: Es asesinado Franco Ojeda exconcejal de Pasto
Date: 2020-10-21 19:46
Author: CtgAdm
Category: Actualidad, Comunidad
Slug: es-asesinado-franco-ojeda-exconcejal-de-pasto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/BuscarlasHastaEncontrarlas-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Mientras se realizaba una nueva jornada de Paro Nacional en todo el país en rechazo a la violencia contra lideres sociales, políticos, defensores y defensoras de Derechos Humanos, este **21 de octubre, fue asesinado el líder político Franco Javier Ojeda** **Delgado**, en la ciudad de Pasto, departamento de Nariño

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1319058052033748993","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1319058052033748993

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según información divulgada por el Instituto de Estudios para el Desarrollo y la Paz (*Indepaz)* **el homicidio de Franco Ojeda se registró en horas de la mañana al ser abordado por hombres armados en el barrio barrio Mariluz**, en la ciudad de Pasto, quienes le propinaron varias impactos de arma de fuego en su cuerpo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Minutos después Ojeda fue trasladado de urgencia al Hospital San Pedro, centro de atención médica al que llegó sin signos vitales. ([En menos de 24 horas dos líderes de Colombia Humana son asesinados](https://archivo.contagioradio.com/en-menos-de-24-horas-dos-lideres-de-colombia-humana-son-asesinados/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/fabioarevalo/status/1319057274539790336","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/fabioarevalo/status/1319057274539790336

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Franco Ojeda era abogado, especialmente reconocido por su trabajo como promotor deportivo desde el año 2000, cuando fue nombrado **Gerente del Deportivo Pasto**, algunos años después fue elegido como concejal de Pasto.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
