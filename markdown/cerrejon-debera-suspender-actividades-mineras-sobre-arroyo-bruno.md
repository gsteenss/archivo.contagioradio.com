Title: Cerrejón deberá suspender actividades mineras sobre arroyo Bruno
Date: 2017-11-29 15:38
Category: Ambiente, Nacional
Tags: arroyo Bruno, Cerrejón, comunidades de la Guajira, Corte Constitucional, Guajira, Wayuu
Slug: cerrejon-debera-suspender-actividades-mineras-sobre-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Arroyo-Bruno2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Censat Agua Viva] 

###### [29 Nov 2017] 

Luego de que la sala plena de la Corte Constitucional decidiera dejar en firme la suspensión de las obras de la multinacional El Cerrejón sobre el arroyo Bruno, las comunidades de la Guajira aseguraron que harán respetar ese fallo teniendo en cuenta que el desvío del río, **atenta contra el derecho al agua.**

En repetidas ocasiones, las comunidades de la Guajira han manifestado que **no fueron consultadas** antes de que se empezará a hacer la desviación del arroyo Bruno, una proyecto que busca realizar la multinacional para expandir la producción de carbón en ese territorio. Para lograr esto, se ha venido denunciando la deforestación de varias zonas para permitir la construcción del canal que desvía el agua.

### **Corte Constitucional ya había ordenado suspender las actividades del Cerrejón** 

La Corte Constitucional **ya había ordenado cesar las actividades de la multinacional** en agosto del 2017. Allí, había decidido ponerle freno a las actividades de la empresa, hasta tanto no se verificara la situación de derechos humanos de la población indígena y afro que hacen parte de las diferentes comunidades de La Guajira. (Le puede interesar: ["Suspendidas las operaciones de Cerrejón para desviar el arroyo bruno en la Guajira"](https://archivo.contagioradio.com/suspendidas_actividades_arroyo_bruno_la_guajira_corte_constitucional/))

En ese momento, la compañía se había comprometido a cumplir con todos los requerimientos legales, ya que había logrado obtener el permiso para intervenir el Arroyo Bruno, gracias a que, en noviembre de 2014, la Autoridad Nacional de Licencias Ambientales, ANLA, **permitió al Cerrejón la modificación del plan de manejo ambiental**.

Un procedimiento para el cual CORPOGUAJIRA se había declarado sin posibilidad de emitir permisos por incapacidad técnica, por lo que la ANLA, **que ya había avalado el desvío**, fue quien terminó realizando “estudios amañados”, que dieron paso a la ampliación de la mina en el Tajo La Puente, como se ha asegurado desde la organización Censat Agua Viva que acompaña a las comunidades.

### **Cerrejón se ha caracterizado por incumplir los fallos de la Corte** 

De acuerdo con Samuel Arregoces, líder de una de las comunidades afrocolombianas y el pueblo Wayuú en la Guajira, el Cerrejón se ha caracterizado por **no acatar las decisiones legales** y ha violado los derechos humanos de manera sistemáticas. “Hoy le estamos demostrando al Estado colombiano y al mundo que Cerrejón no cumple con los estándares que le piden para hacer su minería que no es responsable con el medio ambiente y menos con las comunidades”. (Le puede interesar: ["13 Razones para no desviar el río Bruno"](https://archivo.contagioradio.com/13-razones-para-no-desviar-el-arroyo-bruno/))

Afirmó que esta nueva sentencia **la harán valer** y utilizarán los mecanismos necesarios para que la empresa cumpla con lo que estipula la Corte, “están jugando con la vida, acá el agua es algo vital, las personas están muriendo por falta de agua potable”. Además, dijo que ha habido un trabajo valioso con diferentes organizaciones para evitar que el arroyo Bruno sea desviado.

Arregoces indicó que ya ha sido construido el primer tramo del canal que será utilizado para desviar el agua del arroyo. Allí, varias comunidades ya se han visto afectadas, así como se verán otras poblaciones ubicadas cerca de la fuente de agua.

Para las comunidades, es difícil comprender que la producción de carbón **se mantenga en Colombia** teniendo en cuenta que los países de Europa, a los cuales se les vende el mineral, han decidido dejar atrás su uso. Ante esto, “el Gobierno tiene que pensar en crear nuevas alternativas económicas que no dañen el ecosistema a costillas de nada”.

<iframe id="audio_22358402" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22358402_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper">

</div>
