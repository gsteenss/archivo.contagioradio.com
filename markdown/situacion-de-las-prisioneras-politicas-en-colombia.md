Title: Situación de las prisioneras políticas en Colombia
Date: 2017-07-21 17:45
Category: Libertades Sonoras, Mujer
Tags: FARC, Liliany Obando, mujeres, Prisioneras políticas
Slug: situacion-de-las-prisioneras-politicas-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/carceles-y-mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Diario Femenino ] 

###### [19 de Jul. 2017]

A propósito de la huelga de hambre que llevaron  a cabo mujeres y hombres integrantes de la guerrilla de las FARC durante varias semanas en **\#LibertadesSonoras hablamos de la situación en la que se encuentran las mujeres en las cárceles de Colombia.** Le puede interesar: [La alarmante situación de las cárceles colombianas](https://archivo.contagioradio.com/39173/)

Contamos en el programa con el testimonio de **Liliany Obando socióloga, defensora de derechos humanos y exprisionera política** quien relató su caso de injusta prisión, además de cuál es la realidad que viven las mujeres en las cárceles y la importancia de las acciones que muestran el compromiso de las mujeres con la paz.  Le puede interesar: [En las cárceles de Colombia existe pico y placa para poder dormir](https://archivo.contagioradio.com/en-las-carceles-de-colombia-existe-pico-y-placa-para-poder-dormir/)

Así mismo, entrevistamos a **Victoria Sandino integrante de la guerrilla de las FARC** y constructora del enfoque de género en los Acuerdos de Paz y con José Zamora exprisionero de esa guerrilla, quienes hablan sobre los incumplimientos en las amnistías y las posibilidades para un retorno efectivo de sus integrantes a la vida civil. Le puede interesar: [En Colombia la población en cárceles aumentó 141%  desde el año 2000](https://archivo.contagioradio.com/en-colombia-la-poblacion-en-carceles-aumento-141-desde-el-ano-2000/)

<iframe id="audio_19934316" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19934316_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
