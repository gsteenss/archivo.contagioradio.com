Title: Adopción igualitaria no puede ser llevada a consulta ciudadana: abogado Germán Rincón
Date: 2015-11-05 13:35
Category: LGBTI, Nacional
Tags: Adopción igualitaria, Corte Constitucional, Germán Rincón
Slug: adopcion-igualitaria-no-puede-ser-llevada-a-consulta-ciudadana-abogado-german-rincon
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Adopcion-igualitaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9287029_2_1.html?data=mpelmZWWfY6ZmKiakp6Jd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcXj0cjWh6iXaaOnz5DWydrFsMrowtfWw5DSs4zk1srRx5DXqdOfzdHS2MbIpYzVjMjc0NjZsNXVjMiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Germán Humberto Rincón, Abogado] 

<iframe src="http://www.ivoox.com/player_ek_9288009_2_1.html?data=mpelmpWUfY6ZmKiak5eJd6KmlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhcXj0cjWh6iXaaOnz5DWydrFsMrowtfWw5DSs4zk1srRx5DXqdOfzdHS2MbIpYzVjMjc0NjZsNXVjMiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Ana Leiderman] 

##### [5 nov 2015]

Luego de que la sala plena de la Corte Constitucional colombiana votara a favor la posibilidad de que las parejas del mismo sexo puedan adoptar, han surgido diversas posturas que apoyan la decisión aunque otras buscan retrocederla, pese a que se trató de una medida sobre una protección global, **con impacto positivo en los derechos  de los niños y niñas, y las parejas homosexuales,** explica Germán Humberto Rincón, abogado y activista por los derechos de la comunidad LGBTI.

De acuerdo con el abogado, “hay algunas voces que están diciendo que se llame a una consulta ciudadana para ver qué opinan las personas, pero quiero aclarar que **los derechos humanos no pueden ser objeto de consulta ciudadana**” explicó que personas como la senadora Vivian Morales, que es abogada, saben que no se puede llamar a consulta popular esa decisión.

Respecto a los constantes señalamientos y ataques de parte del procurador Alejandro Ordoñez, el abogado dice “La hoguera medieval  del procurador nos quiere quemar todas las veces que pasa algo a nuestro favor, pero debo decirle que Colombia es un estado laico, y la democracia no es el poder de las mayorías sino el respecto por las minorías, y el único libro sagrado que cobija a **48 millones de personas en Colombia es la Constitución Política, libro que le recomiendo leer para entender lo que pasó en Corte”.**

Por su parte, Ana Leiderman, quien junto a su pareja logró que la Corte Constitucional les concediera la adopción de una niña hija de una de ellas, asegura que la sociedad ha venido avanzado en el respeto de las formas de familia y formas de concebir el amor, "Hay muchas clases de familia, ahora se celebra el día de la familia que es algo más incluyente (…) Somos familias iguales, formamos familias como cualquier pareja".

Ana espera que ahora los funcionarios estatales sean quienes posibiliten el fallo de la Corte, ya que según afirma, "**Las Leyes en Colombia son de papel, ya son los funcionarios lo que deciden poner o no trabas" al momento de hacer cumplir la decisión**, siempre y cuando se deje de pensar en qué es lo que estas parejas “hacen en la cama”, como lo dice Leiderman.

Cabe resaltar que, como indica el abogado Rincón Perfeti, **“hay imaginarios negativos sobre niños y niñas adoptadas por parejas homosexuales que están haciendo daño”**, pero lo cierto es que la orientación sexual de los hijos e hijas criadas por parejas del mismo sexo, no implica que ellos también vayan a ser homosexuales, dice el activista.
