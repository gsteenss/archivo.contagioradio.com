Title: Presidente atiende llamado de Campamento por la Paz
Date: 2016-10-28 17:33
Category: Nacional, Paz
Tags: Campamento por la Paz, Juan Manuel Santos, paz, Plaza de Bolívar, Presidente Juan Manuel Santos, Presidente Santos, Santos
Slug: santos-campamento-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/presidente-santos-paz-a-la-calle.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Presidencia] 

###### [28 Oct de 2016] 

En un comunicado entregado a la opinión pública, **los integrantes del campamento por la paz, ubicado en la Plaza de Bolívar en Bogotá** y en diversos lugares alrededor del país, **dieron a conocer su posición frente a la visita que les realizó en la capital, el presidente de Colombia, Juan Manuel Santos.**

**El primer mandatario escuchó las propuestas y exigencias** que desde hace más de 23 días realizan los campistas y el país entero, de una firma ya a los acuerdos pactados en La Habana.

En medio de su visita, **Santos le dijo al campamento que “con los avances de las negociaciones, se espera que haya un acuerdo en cuestión de días. No habrá necesidad de extender el cese al fuego, porque éste será historia del pasado".**

Durante el encuentro, todas las personas que han estado acampando le pidieron al Presidente que soltara los nudos de una cuerda, una forma simbólica que quiso representar la destraba del proceso de paz.

Así mismo y en medio de las más de 100 carpas que están instaladas en la Plaza de Bolívar en el centro de la capital, las personas que han pasado 26 noches y días allí, le manifestaron su preocupación a Santos y le afirmaron **“llevamos 26 días sin acuerdos. Cada día, cada hora y cada minuto que pasamos sin un acuerdo, aumenta el riesgo de perder el terreno ya avanzado por la Paz”.**

Finalmente, en medio del frío de las 7 de  la noche en la capital, los campistas le reiteraron a Juan Manuel Santos “que **solo levantaremos el Campamento por la Paz cuando nuestras exigencias tengan una respuesta concreta que nos saque de la incertidumbre”. **Le puede interesar: [Amenazados integrantes del campamento por la paz](https://archivo.contagioradio.com/amenazados-integrantes-del-campamento/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
