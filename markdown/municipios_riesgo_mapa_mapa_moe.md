Title: Acuerdo de paz eliminó riesgos para elecciones en 79 municipios
Date: 2018-02-09 13:07
Category: Entrevistas, Política
Tags: Alejandra Barrios, elecciones 2018, MOE
Slug: municipios_riesgo_mapa_mapa_moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/mineria-voto-popular.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: revistaccesos] 

###### [9 Feb 2018] 

En 170 municipios coinciden riesgos electorales tanto por factores indicativos de fraude como por factores de violencia, sin embargo estas elecciones registran el menor nivel de riesgo de acciones violentas, **uno de esos factores tiene que ver con el acuerdo de paz firmado en La Habana que logró eliminar ese tipo de riesgo en 79 municipi**os. Así lo señala la Misión de Observación electoral en su nuevo informe “Mapas y factores de Riesgo Electoral- Elecciones Congreso y Presidente 2018”.

De acuerdo con Alejandra Barrios, directora de la MOE aunque es de resaltar que de 260 municipios en riesgo se disminuyó a 170, son siete  los puntos del país en donde se deben focalizar las instituciones para disminuir los riesgos por fraude electoral o por violencia. Se trata de zonas como el **Catatumbo, el nordeste de Antioquia, el Urabá, Chocó, Nariño y la zona del bajo Putumayo hasta Vichada.**

“Los riesgos electorales pasaron de ser corredores que atravesaban importantes zonas del país a ser regiones específicas en las que se concentran economías ilegales y diferentes actores armados”, dice la directora de la MOE.

A su vez, es importante señalar que en nivel de riesgo extremo aumentó en 14 municipios, pasando de 50 en 2014, a 64 para los procesos electorales que se avecinan. Ante dicha situación, la MOE le solicitó al Gobierno Nacional, a las autoridades electorales y a medios de comunicación, especial atención a **estos 170 municipios de los cuales 64 están en riesgo extremo, 65 en riesgo alto y 41 en riesgo medio.**

![Riesgo\_consolidado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/Riesgo_consolidado-489x600.png){.alignnone .wp-image-51355 .size-large .aligncenter width="489" height="600"}

### **Riesgo por fraude electoral**

Según la MOE en 31 municipios los riesgos por factores indicativos de fraude electoral disminuyen tanto en elecciones de Cámara como en Senado.  Pero se identificó **45 municipios en riesgo extremo por factores indicativos de fraude en Cámara y 34 en Senado.** (Le puede interesar:** **[Los cuestionamientos a las candidaturas presidenciales por firmas según la MOE)](https://archivo.contagioradio.com/?p=45986)

“Estamos viendo una leve disminución del riesgo asociado a las prácticas políticas locales, no obstante, los niveles de municipios con niveles extremos de riesgo aumentaron, lo que implica que las autoridades deben mantener acciones preventivas sobre posibles delitos e irregularidades electorales que se presenten en esos municipios”, explica Barrios.

### **Causas de riesgo en algunas regiones del país** 

Barrios explica que en la región del Catatumbo, de los 40 municipios de Norte de Santander hay **8 con nivel de riesgo extremo por la presencia de grupos armados como el EPL y el ELN,** lo que imposibilitaría que la ciudadanía pueda participar tranquilamente en estas votaciones.

Por otro lado, en la zona del nordeste antioqueño hay factores que prenden las alarmas debido al desarrollo de la actividad minera ilegal, el posible fraude electoral por la manipulación de ciudadanos, y las situaciones que se han presentado con la inscripción de cédulas, entre otras. Mientras que en el Urabá el riesgo de violencia se incrementa especialmente por acciones armadas asociadas a **la disputa por el territorio entre la guerrilla del ELN y grupos neoparamilitares** como las Autodefensas Gaitinistas de Colombia, AGC.

En la parte del departamento del Chocó, por la zona del Río San Juan preocupa la situación de orden público que actualmente se encuentra en amenaza por el reciente paro armado anunciado por el ELN, pues allí hay alto nivel de incidencia de esa guerrilla. Por su parte, el asesinato de lideresas y líderes sociales en la zona del pacífico sur del país,  especialmente en el departamento de Nariño, "**afecta el corazón de la democracia, genera desplazamiento forzado y provoca la desconfianza en las instituciones por su poca capacidad de respuesta",** indica la directora de la MOE.

En la zona desde el municipio de Puerto Asís hasta Vichada, es decir, pasando por lugares como el bajo Putumayo, el sur del río Meta, el río Guavire y el Caguán, el riesgo por factores de violencia se mantiene debido a la presencia de las disidencias de las FARC que hacían parte del frente primero, aunque también preocupa la actividad del narcotráfico presente en estos territorios.

Finalmente, Alejandra Barrios llama la atención de las autoridades para que se actúe frente a los 85 municipios con mayor inscripción  de cédulas que población, y también señala que ahora hay que ponerle la lupa a la elección de los jurados de votación y los testigos electorales. (Le puede intersar: [MOE alerta sobre porcentajes atípicos de inscripción de cédulas en 28 municipios)](https://archivo.contagioradio.com/?p=50488)

###### Reciba toda la información de Contagio Radio en [[su correo]
