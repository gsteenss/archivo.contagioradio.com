Title: Un silencio obligado impera en Santa Isabel tras asesinato de líder campesino Carlos Salinas
Date: 2019-11-12 17:26
Author: CtgAdm
Category: Ambiente, DDHH
Tags: Asesinato de líderes ambientales, Carlos Aldairo Salinas, Tolima
Slug: un-silencio-obligado-impera-en-santa-isabel-tras-asesinato-de-lider-campesino-carlos-salinas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Carlos-Aldairo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

La comunidad del municipio de Santa Isabel, en Tolima, permanece bajo temor y guarda silencio frente al asesinato del líder campesino y ambiental **Carlos Aldairo Salinas**, arriero, guía turístico y gestor de la Ruta del Cóndor quien se desempeñó como un guardián del territorio que habría sido amenazado meses antes de conocerse su homicidio por ejercer su labor de protección del páramo el pasado 9 de noviembre.

Una habitante de Santa Isabel, quien trabajó junto a Carlos durante los últimos años, lo recuerda como un campesino carismático y querido por quienes visitaban el páramo en su compañía, además era considerado como una persona que se oponía a la quema del Valle de Frailejones, la cacería y la tala indiscriminada por lo que asumió el rol de líder ambiental.

Precisamente frente a su labor de denuncia, resalta que el líder campesino había sido intimidado hace dos meses, "nos dijo que temía por su vida porque le habían dejado una nota en su finca que decía 'fuera, aquí no queremos sapos', él tenía miedo". [(Le puede interesar: Carlos Salinas, líder ambientalista y defensor de la Ruta del Cóndor fue asesinado)](https://bit.ly/2Qbitvl)

Hace unos días Carlos se había visto involucrado en una denuncia contra personas que mediante la tala ilegal usaban la madera del páramo para una construcción, hecho que el líder dio a conocer, "lo que él comentó es que la Corporación Autónoma Regional del Tolima incautó la madera y que los afectados, de quienes se desconoce su identidad le hicieron el reclamo".  Frente a ello, **Carlos alertó al inspector de la Policía de Santa Isabel quien tuvo conocimiento del episodio y cuya información sería vital para resolver la investigación que viene adelantando la SIJIN. **

### "No solo se trata de Carlos Salinas"

"**En términos de seguridad, todos estamos anonadados de cómo llegaron dos hombres hasta la finca en que vivía, es una finca que está a 4 horas del Nevado Santa Isabel y a 16 horas en mula del pueblo" r**elata, resaltando la incertidumbre que genera el pensar quién recorrería tal distancia para cometer este acto. Agrega que se ha conocido la presencia de un grupo armado, sin embargo no se sabe quienes son y si estuvieron ligados al asesinato de 'Cejas'.

"No solo es Carlos Aldairo, es un colectivo de viajeros, de mochileros, de ambientalistas, de personas que queremos que el páramo progrese (...) y en la medida en que se desarrolle el turismo se van a minimizar otros factores como la ganadería, la cacería y la presencia de estos grupos" afirma.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
