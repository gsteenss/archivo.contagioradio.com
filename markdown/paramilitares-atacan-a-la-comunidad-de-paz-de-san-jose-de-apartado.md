Title: Sigue asedio de paramilitares contra comunidad de Paz de San José de Apartadó
Date: 2017-02-02 14:05
Category: DDHH, Nacional
Tags: Comunidad de Paz de San José de Apartadó, paramilitares
Slug: paramilitares-atacan-a-la-comunidad-de-paz-de-san-jose-de-apartado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/San-José-de-Apartadó.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [2 Feb 2017] 

La Comunidad de San José de Apartado denunció la grave situación de derechos humanos que afrontan en su territorio debido a la presencia y accionar de paramilitares, que ya han dejado varias víctimas entre ellas el **asesinato de un campesino, la retención de dos familias y el abuso sexual a una menor de edad. De igual forma denunciaron que se están realizando operativos paramilitares con el acompañamiento de la Fuerza Pública. **

Los hechos se han presentado en la región de San José de Apartado, Arley Tubérquia, miembro del Consejo de la Comunidad de Paz de San José de Apartado, informó que paramilitares tendrían retenidos a dos familias de la Comunidad , en la vereda Mulatos, y señaló que “**los han amenazado diciéndoles que van a asesinar a integrantes de la comunidad**”.

Sobre el acompañamiento de organizaciones internacionales que se encuentran con la comunidad, lo que estarían diciendo los paramilitares es que **“con ellos no se van a meter”.** [Le puede interesar:"Cerca de 650 paramilitares ejercen contrrol territorial en Cacarica"](https://archivo.contagioradio.com/650-paramilitares-ejercen-control-en-cacarica-35568/)

Ha esto, se suma el **asesinato del campesino Giovanny Valle Guerra**, en el corregimiento de San José de Apartado, perpetrado según las comunidades por los mismos paramilitares y **el abuso sexual a una menor de edad, que fue herida y amenazada para que no instaurara la demanda.**

La **presencia de los paramilitares ha generado zozobra entre los habitantes de la Comunidad de Paz de San José** de Apartado que además expresaron que desde hace tres semanas vienen denunciando la presencia de estos grupos en sus territorios sin que exista respuesta alguna por parte de las autoridades e instituciones del Estado y piden que se activen los mecanismos necesarios para garantizar sus vidas.

“Estamos en un inminente riesgo de que algunas familias sean desplazadas y otras asesinadas, según lo que han dicho los paramilitares y estamos pidiendo que se activen los mecanismos necesarios **para evitar que se cometan estas muertes**” afirmó Tubérquia. Le puede interesar:["Paramilitares asesinan a dos de Salaquí en el Bajo Atrato"](https://archivo.contagioradio.com/parmilitares-choco-asesinato/)

###### Reciba toda la información de Contagio Radio en su correo o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio.
