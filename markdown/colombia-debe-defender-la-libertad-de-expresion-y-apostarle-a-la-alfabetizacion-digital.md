Title: Colombia debe defender la libertad de expresión y apostarle a la alfabetización digital
Date: 2019-03-01 17:35
Category: DDHH, Política
Tags: Corte Constitucional, Libertad de expresión, redes sociales
Slug: colombia-debe-defender-la-libertad-de-expresion-y-apostarle-a-la-alfabetizacion-digital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/8045443994_50612554fe_k.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: flickr/albita85 

###### 1 Mar 2019

El pasado 28 de febrero la **Corte Constitucional** inició un debate sobre el uso de Internet y el proyecto de ley que buscaría crear normas de funcionamiento y de buen uso sobre las redes sociales, una iniciativa que amenaza con coartar el derecho al anónimato en la web y a la libertad de expresión. A  lo largo del evento también comparecieron representantes en Colombia de **Google y Facebook** quienes destacaron que  sus plataformas no deben ser responsables por lo que suben terceros.

**El periodista y docente de prensa de la Universidad Jorge Tadeo Lozano, Óscar Durán** señala que la propuesta del proyecto de ley promovida por el **senador José David Name,** va en contra de la libertad de expresión pues establece que terceros decidan si un ciudadano tiene o no derecho a esa libertad, lo cual como señala, es sumamente valioso en una democracia.

El docente se mostró a favor de la postura de la **Fundación por la Libertad de Prensa (FLIP),** que estableció en el Tribunal que en el contexto actual es imposible separar la Internet de la libertad de expresión porque permite la difusión de información y opinión. Además resaltó que cualquier tipo de reglamentación sobre la libertad de expresión "va en contra de un derecho que ha costado tanto obtener y que ahora reglamentarlo o condicionarlo es cuestionable".

> ¿Se debe restringir la libertad de expresión en redes sociales?
>
> Con este video la FLIP le explicó a la [@CConstitucional](https://twitter.com/CConstitucional?ref_src=twsrc%5Etfw) que solo los jueces pueden hacerlo en casos puntuales y que sería muy peligroso dejar esa tarea en manos de las plataformas digitales.[\#LibreExpresiónEnRedes](https://twitter.com/hashtag/LibreExpresi%C3%B3nEnRedes?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/pve7DbrNXX](https://t.co/pve7DbrNXX)
>
> — FLIP (@FLIP\_org) [28 de febrero de 2019](https://twitter.com/FLIP_org/status/1101186932481159168?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Durán destaca que desde la academia se debe trabajar en **la alfabetización digital,** haciendo referencia a que el consumidor tenga más criterio frente a los escenarios en redes, "somos una cultura contestataria y eso al final se ve reflejado en las redes sociales  cuando alguien desde el anonimato establece lo que está bien o malo o injura a una persona",  agrega.

El docente advierte sobre lo delicado que sería darle la responsabilidad a un tercero, como a un sector privado, el reconocer o juzgar cuando alguien tiene derecho o no a usar su libertad de expresión, asimismo señala la importancia de comprender que las redes sociales han llegado para quedarse y que **la salida no es reglamentarlas sino entender cómo funcionan y utilizarlas en beneficio de una sociedad más informada, más critica pero más democrática.**

###### Reciba toda la información de Contagio Radio en [[su correo]
