Title: Citaciones y Audiencias: El nuevo capítulo de la JEP
Date: 2018-07-11 16:12
Category: Nacional, Paz
Tags: FARC, General Henry Torres Escalante, JEP, Patricia Linares
Slug: citaciones-y-audiencias-nuevo-capitulo-de-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-11-a-las-2.59.43-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: JEP] 

###### [11 Jul 2018] 

Con la comparecencia del mayor general (r) Henry Torres Escalante del pasado martes, en la que el uniformado reconoció el acta de sometimiento ante la JEP firmada por él y la citación de ex-miembros de las FARC programada para este viernes 13 de julio, iniciaron las audiencias ante la Jurisdicción Especial de paz de los actores armados del conflicto.

Torres Escalante decidió someterse ante el tribunal de paz para resolver el caso en el que se lo acusa de presuntamente participar en una ejecución extrajuidicial cometida en marzo de 2007, cuando era comandante de la Brigada 16 en Aguazul, Casanare. **Aunque Torres pidió perdón a las víctimas, y reconoció que hubo errores que no se debieron cometer, señaló que no acepta los cargos por los que se le acusa.**

El caso de Torres es el primero que involucra el sometimiento de Generales ante la JEP, sin embargo hasta el 9 de julio, y según datos ofrecidos por la misma Jurisdicción de Paz, **2.159 integrantes de la fuerza pública se han acogido ante esta instancia.** De ellos, 965 ya han recuperado su libertad de manera transitoria, anticipada y condicionada.

A pesar de la comparecencia de Torres Escalante, será la Sala de Definición de Situaciones Jurídicas la encargada de determinar si tiene competencia la JEP para juzgar el caso, así como la inclusión del general en esta Justicia Transicional, lo que le permitiría tener beneficios como la libertad.

### **FARC también deberá comparecer ** 

Tal como lo advirtió en un comunicado la Presidenta de la JEP, Patricia Linares, la aprobación de la Ley de Procedimiento de la Jurisdicción de Paz, ya posibilitó que iniciaran las audiencias así como las citaciones a los actores del conflicto para comparecer ante este tribunal. (Le puede interesar: ["Presidenta de la JEP pide al Congreso no dilapidar Ley de Procedimiento"](https://archivo.contagioradio.com/presidenta-de-la-jep-pide-al-congreso-no-dilapidar-ley-de-procedimiento/))

Muestra de ello, es el **llamado a miembros de las FARC a comparecer ante la Sala de Reconocimiento de Verdad, Responsabilidad y Determinación de Hechos y Conductas (SRVR) el próximo 13** de julio a las 2:30 de la tarde. Esta audiencia tendrá como base el informe Nº2 que la Fiscalía le entrego a la JEP denominado "Retención ilegal de personas por parte de las FARC-EP".

Durante esa diligencia, la sala notificará a ex-miembros del secretariado y el Estado Mayor sobre el inicio del caso; abrirá la etapa de "Reconocimiento de Verdad, de Responsabilidad y Determinación de los Hechos y Conductas"; recordará los deberes a los cuales están sometidos; y entregará el respectivo informe de la Fiscalía, así como los anexos del caso a todos los implicados.** **

Sin embargo, queda un halo de duda sobre el futuro de los 2 159 procesos de integrantes de la Fuerza Pública, entre los cuales hay 2 019 del Ejercito quienes de acuerdo con una de las modificaciones presentadas por el Centro Democrático a la Ley de Procedimiento, quedarían congelados hasta que se cree una sala especial al interior del tribunal para Juzgarlos. (Le puede interesar: ["Aún se puede defender el espíritu de la JEP"](https://archivo.contagioradio.com/aun-se-puede-defender-la-jep/))

[Captura de Pantalla 2018-07-11 a La(s) 3.06.24 p.m..Output](https://www.scribd.com/document/383674129/Captura-de-Pantalla-2018-07-11-a-La-s-3-06-24-p-m-Output#from_embed "View Captura de Pantalla 2018-07-11 a La(s) 3.06.24 p.m..Output on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_74621" class="scribd_iframe_embed" title="Captura de Pantalla 2018-07-11 a La(s) 3.06.24 p.m..Output" src="https://www.scribd.com/embeds/383674129/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-uYil4QVy2paRWwp7Juu4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7041373926619828"></iframe>

######  

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
