Title: Pruebas de la Fiscalía no demuestran la culpabilidad de Mateo Gutiérrez
Date: 2017-06-23 12:24
Category: DDHH, Nacional
Tags: DDHH, Juicio Mateo Gutiérrez, Mateo Gutierrez
Slug: pruebas-de-la-fiscalia-no-demuestran-la-culpabilidad-de-mateo-gutierrez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/DC8vuHaXUAEm0tB-e1498238920355.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FCSPP] 

###### [23 Jun 2017]

La Fiscalía acusó al estudiante Mateo Gutiérrez de haber cometido los delitos de terrorismo, concierto para delinquir, hurto y porte de artefactos explosivos. Sin embargo, para la defensa **las pruebas presentadas por el ente acusador no demuestran la responsabilidad del joven** y además, algunos de los materiales probatorios corresponden a hechos posteriores a la captura.

**La defensa de Mateo argumentó que la Fiscalía carece de pruebas que puedan vincular al estudiante con los delitos que se le imputaron**. Por una parte la fiscalía presenta un testimonio de un menor de edad que dice reconocer a Mateo como autor de la activación de una bomba panfletaria en 2015, sin embargo la descripción no corresponde con la de Mateo. (Le puede interesar: "[Se conocerán las pruebas de la Fiscalía en contra de Mateo Gutiérrez](https://archivo.contagioradio.com/hoy-se-conoceran-las-pruebas-de-la-fiscalia-en-contra-de-mateo-gutierrez/)")

El abogado afirmó que “el menor de edad hizo el reconocimiento hace un año y medio de **una persona con características físicas diferentes a las de Mateo”**. En su momento, “el menor de edad dijo que la persona vestía ropa como la de los habitantes de la calle y Mateo nunca ha vestido así”.

Por otra parte la Fiscalía pretende encadenar hecho sucedidos después de la captura del joven lo cual no tendría ninguna fuerza probatoria contra Mateo. “Han agregado nuevos hechos que antes no se había imputado y son hechos que sucedieron cuando Mateo ya estaba detenido”.

Así mismo manifestó que "las bombas panfletarias **no están diseñadas para causar daños y su fabricación y detonación no puede ser considerado un acto de terrorismo**". Según él "estos artefactos son panfletos que se prenden y no causan zozobra ni estragos". Es por esto que para la defensa las acusaciones contra Mateo carecen de fundamento y están seguros que el proceso se desenvolverá a favor del estudiante. (Le puede interesar: ["De Mateo a Mateo: Carta a un preso político")](https://archivo.contagioradio.com/de-mateo-a-mateo-carta-a-un-preso-politico/)

La defensa manifestó que la captura de Mateo se hizo en “un afán por capturar a alguien después de los atentados en la Macarena donde murió un Policía”. Además dijo el abogado que Mateo es un estudiante de sociología de la Universidad Nacional que ha participado movilizaciones sociales. Según Matias, “**la Fiscalía construye álbumes con las personas que participan de estas marchas** para después sindicarlas de terrorismo”.

**El juicio de Mateo tardaría todo el año**

El juicio, en donde la defensa tiene previsto demostrar la inocencia de Mateo, puede tardarse todo el año o más en culminar. Según el abogado “la audiencia preparatoria está prevista para el 1 de agosto y luego viene el juicio oral”. Estableció que **el proceso se puede demorar el resto del año** donde “la Fiscalía quiere relacionar los nuevos hechos cuando Mateo ya estaba prisión y esto hace más largo el proceso”. (Le puede interesar: "[Se conforma red de solidaridad en defensa de Mateo Gutiérrez"](https://archivo.contagioradio.com/red-solidaridad-defensa-mateo-gutierrez/))

En lo que resta del juicio la Fiscalía deberá demostrar 8 hechos nuevos que le adjudican a Mateo utilizando testigos, fotos, videos y demás pruebas. Para el abogado del estudiante la presentación de las pruebas puede tomarse entre 3 o 4 audiencias más.

<iframe id="audio_19439310" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19439310_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
