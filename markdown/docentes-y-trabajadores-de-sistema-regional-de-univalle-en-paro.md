Title: Docentes y trabajadores de sistema regional de Univalle en Paro
Date: 2017-01-24 12:58
Category: Educación, Nacional
Tags: educacion, tercerización laboral, Univalle
Slug: docentes-y-trabajadores-de-sistema-regional-de-univalle-en-paro
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/univalle.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Panoramio] 

###### [24 Ene 2017] 

Docentes y trabajadores de las sedes regionales de la Universidad del  Valle, **entraron a paro** hasta que las directivas hagan efectivo el pliego de exigencias laborales construido y presentado en 2016 y que busca el equilibrio salarial, el fin de la **tercerización y mejoras en el sistema regional que cobija a cerca de 10000 estudiantes.**

Según los datos recogidos por la Asociación Sindical de Profesores y Trabajadores del Sistema de Regionalización de la Univalle, un trabajador del sistema regional gana solo el **30% de lo que gana otra persona en la sede Cali** de la Universidad. Además el sistema regional actualmente acoge al **45% de los estudiantes que acceden a la institución y solamente se le otorga el 5% del presupuesto para su funcionamiento.**

De acuerdo con Carlos Gonzáles, docente de sociología de la sede de Palmira, e integrante de la Asociación Sindical, la problemática radica principalmente en la **forma de contratación y en la desigualdad que existe en los pagos** que se le hace a la planta del sistema regional y a la planta de la sede principal en Cali.

“La contratación que se hace de las plantas en el sistema de regionalización nunca la hecho la Universidad, sino **fundaciones de apoyo que son entes privados en donde el pago de los docentes quedaba en un vaivén sin respetar la escala salarial**” Le puede interesar: ["Educadores marcha en Bogotá en contra de la privatización"](https://archivo.contagioradio.com/estudiantes-madres-padres-y-profesores-marchan-en-bogota/)

Según González estas inconformidades fueron planteadas ante las directivas desde febrero del año pasado y se llegó a un acuerdo, sin embargo después de un año, aún no se ha hecho ningún cambio. A su vez, se **eliminaron las categorías de “medio tiempo” y “tiempo completo”** dejando como único modo de contratación la “hora de cátedra”, afectando, directamente, el proceso de investigación, la atención y el seguimiento a los estudiantes.

De igual forma, en el pliego de exigencias se pide que no haya ningún tipo de represalias en contra de los trabajadores de ASRUV, la firma y el cumplimiento de los acuerdos pactados previamente y ejecutar la nivelación salarias para todos los empleados del sistema de regionalización. **De no cumplirse estos requerimientos  docentes y trabajadores han expresado que las clases no iniciarían el próximo 6 de febrero.**

<iframe id="audio_16621588" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16621588_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
