Title: ¿Carreteras en medio de los parques naturales de Colombia?
Date: 2015-02-11 18:24
Author: CtgAdm
Category: Ambiente, Economía
Tags: Ambiente, Julia Miranda, parques nacionales, Plan Nacional de Desarrollo
Slug: pnd-permitiria-que-se-construyan-carreteras-en-parques-nacionales
Status: published

##### Foto: [www.iisd.ca]

El Plan Nacional de Desarrollo (PND) 2014-2018, ‘Todos por un nuevo país’, daría **vía libre a que se construyan carreteras en medio de los parques naturales de Colombia.**

El texto del PND, incluye un cambio por medio del cual, las trochas informales que hay en los parques, podrían convertirse en carreteras pavimentadas. Hasta el momento, **este punto del Plan, no ha sido consultado con Julia Miranda,** directora de Parques Nacionales, quien no ve con muy buenos ojos esta decisión.

Miranda, asegura que al permitir obras de infraestructura dentro de los parques naturales del país, **se estaría violando la Constitución de 1991 y se generarían daños ambientales.**

Pese al señalamiento de la directora de Parques Nacionales, **en la página 156 del PND dice**: “para garantizar que el mejoramiento de los corredores viales de interés estratégico nacional esté en consonancia con los propósitos nacionales de reducción de la deforestación y de conservación y uso sostenible de ecosistemas estratégicos, se considera necesario que los ministerios y agencias sectoriales, en conjunto con el MADS, la ANLA, los Institutos de Investigación del SINA y Parques Nacionales Naturales, avancen en el desarrollo de criterios de infraestructura verde para la ejecución de proyectos en áreas de especial importancia ambiental.” Este sería el punto que permitiría obras en los parques.

Cabe recordar que en el PND anterior, **había una consigna similar que daba amparo a la realización de carreteras en los parques nacionales** “para viabilizar el mejoramiento de los corredores viales de interés estratégico nacional, se considera necesario modificar la normativa pertinente para permitir la intervención de infraestructura vial, en vías que se encuentren dentro de las áreas de influencia de Parque Nacionales Naturales”.
