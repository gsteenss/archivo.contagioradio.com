Title: Izquierda Vasca celebra la libertad de Arnaldo Otegui
Date: 2016-03-02 11:15
Category: El mundo, Otra Mirada
Tags: Arnaldo Otegui, Bildu, Izquierda Abertzale, País Vasco
Slug: izquierda-vasca-celebra-la-libertad-de-arnaldo-otegui
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/discurso-arnaldo-otegi-tras-salir-carcel-logrono-1456833304513.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elperiodico] 

<iframe src="http://co.ivoox.com/es/player_ek_10644838_2_1.html?data=kpWjlpmcd5mhhpywj5WaaZS1lZWah5yncZOhhpywj5WRaZi3jpWah5yncaru0trWx9fIpYzKwtjQw5DHqc3Zw9fOjdHFb83dw8rf1sbIb8XZjKbf0MbQqNCfsNnSydrNcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Asier Altuna] 

###### [10 Mar 2016] 

Luego de 6 años y medio, Arnaldo Otegui, líder de izquierda Abertzale en el País Vasco salió en libertad tras cumplir una condena en la cárcel de Logroño. El líder político fue acusado de reorganizar el movimiento “Batasuna” supuestamente, bajo las órdenes de ETA. **Hasta el momento de las 5 personas acusadas 4 están en libertad pero el vicepresidente de Izquierda Abertzale sigue detenido.**

A su salida de la cárcel, y con cerca de 2 centenares de personas y medios de comunicación esperándolo, Otegui pronunció un discurso en el que resaltó la situación de las personas inmigrantes, de las personas expulsadas de sus casas, entre otras causas, y también reafirmó que **la paz es un objetivo común y que luchará por ella hasta el final. "el mejor alcalde es el pueblo" concluyó.**

Según Asier Altuna, la libertad de Otegui da un nuevo impulso tanto al partido Izquierda Abertzale como a Bildu, la coalición vasca de las izquierdas, que tendrá en los próximos meses una nueva asamblea en la que **definirán sus objetivos a mediano y largo plazo, así como los candidatos que se presentarán en las próximas elecciones.**

La libertad de Otegui se da en un momento en el que se tejen esperanzas para un eventual proceso de paz en el país Vasco, luego de la salida del gobierno por parte de Mariano Rajoy y el PP, y aunque PSOE y Ciudadanos podrían significar la continuidad de la política de negación de un proceso de paz así como la negación de las **exigencias tanto en Cataluña como en Euzcadi.**
