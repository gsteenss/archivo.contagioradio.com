Title: Ejército pone en riesgo a población de Piamonte, Cauca
Date: 2020-05-10 19:18
Author: CtgAdm
Category: Actualidad, DDHH
Tags: #Cauca, #Sustitución
Slug: ejercito-pone-en-riesgo-a-poblacion-de-piamonte-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Erradicación-de-Cultivos-de-Coca-en-Catatumbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Comisión de Derechos Humanos de la Asociación interveredal de trabajadores campesinos y campesinas de Piamonte Cauca denuncian que las labores de erradicación forzada por parte del Ejército pone en riesgo la vida de la población.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con la comisión, el pasado 6 de mayo, en la vereda Villanueva, en el corregimiento de Yapurá, 20 uniformados hicieron presencia para hacer la erradicación de los cultivos de uso ilícito. Sin embargo, ninguno de ellos contaba con tapabocas o elementos de bioseguridad que protegieran a la comunidad de la pandemia del Covid 19. (Le puede interesar: ["Desconocimiento e incumplimiento del Gobierno con la sustitución de cultivos de uso ilícito"](https://archivo.contagioradio.com/coccam-exige-garantias-para-las-familias-que-trabajan-por-la-sustitucion-de-cultivos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente, el 7 de mayo, en la vereda Playa Rica, en el corregimiento El Remanso, otros 20 uniformados llegaron hacia las 5 de la mañana para continuar con la erradicación. La comunidad denuncia que ese día un intengrante de la Fuerza Pública amedrentó a la comunidad con tiros de fusil al aire.

<!-- /wp:paragraph -->

<!-- wp:heading -->

Los cultivos ilícitos siguen siendo el único sustento de las familias
---------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La Comisión afirma que debido al incumplimiento de los planes de sustitución de cultivos de uso ilícito, las familias siguen dependiendo de ellos para subsistir. (Le pude interesar: "[Comunidades del Cauca se sienten en riesgo ante la escalada de violencia](https://archivo.contagioradio.com/en-cauca-toda-la-comunidad-se-siente-en-riesgo/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido asegura que 945 núcleos familiares se habían acogido a estos planes. Mientras que otras 635, que no alcanzaron a suscribirse afirmaron que realizarían la sustitución de cultivos de uso ilícito de forma voluntaria. Sin embargo, la respuesta desde el gobierno fue negar la posibilidad de nuevas suscripciones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a estas situaciones, la comunidad campesina de Piamonte exige la suspensión inmediata de los operativos de erradicación forzada. De igual forma le pide al gobierno de Iván Duque el cumplimiento de los planes de sustitución de cultivos pactados en los Acuerdos de paz. (Le puede interesar: "[Campesinos heridos por ESMAD en procesos de erradicación forzosa](https://www.justiciaypazcolombia.com/campesinos-heridos-por-esmad-en-procesos-de-erradicacion-forzosa-1/)")

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente le solicita a la presidencia la garantía del derechos a la salud de las comunidades, cumpliendo el decreto 457 de 2020. Razón por la cual insisten en que se acojan las medidas de aislamiento incluso de la Fuerza Pública para evitar contagios del covid 19.

<!-- /wp:paragraph -->
