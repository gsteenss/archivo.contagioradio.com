Title: La Corrupción en las Cárceles de Colombia
Date: 2019-08-03 16:11
Author: CtgAdm
Category: Expreso Libertad
Tags: cárceles colombia, corrupción, Expreso Libertad
Slug: la-corrupcion-en-las-carceles-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/000355000W.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Muchas de las grandes problemáticas que vive la población reclusa en Colombia, son producto de las redes de corrupción que se han tejido en torno a la prestación de servicios para los centros de reclusión y que en escenario muy exacerbados han provocado la muerte de personas.

<div>

</div>

<div>

En este programa del Expreso Libertad el abogado Uldarico Flores y el exprisionero político Chucho Nariño, evidenciaron cómo esas redes de corrupción han permeado desde a la guardia del INPEC hasta las altas Cortes, que continúan sin tomar acciones contundentes frente a este tipo de delitos.

</div>

<div>

</div>

<div>

\[embed\]https://www.facebook.com/contagioradio/videos/2377469369182130/\[/embed\]

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 

</div>
