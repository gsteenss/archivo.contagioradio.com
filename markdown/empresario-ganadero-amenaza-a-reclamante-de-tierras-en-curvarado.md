Title: Empresario ganadero amenaza a reclamante de tierras en Curvaradó
Date: 2016-12-23 13:00
Category: DDHH, Nacional
Tags: Curvarado, zona de biodiversidad
Slug: empresario-ganadero-amenaza-a-reclamante-de-tierras-en-curvarado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/biodiversidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ContagioRadio] 

###### [23 Dic 2016] 

El pasado miércoles 21 de diciembre, sobre las 4:30pm, el reclamante de tierras Eliodoro Polo Mesa,  **fue amenazado cuando regresaba a su Zona de Biodiversidad, por el empresario Alfredo López, quién según testigos de la comunidad, con ayuda del paramilitarismo** despojo ilegalmente a la familia polo de su propiedad.

Eleodoro, reclamante de predios en orillas de la quebrada Bijao en el límite de los territorios colectivos de Curvaradó, y, Pedeguita y Mancilla se encontró con que  López, **ingresó a la fuerza aproximadamente 180 cabezas de ganado a su propiedad y lo amenazó un una escopeta** diciéndole que **“no le fuera a sacar el ganado o le disparaba”**, motivo por el cual el integrante de la familia Polo del Consejo Comunitario se vio obligado a dejar el ganado a la fuerza.

El predio de la familia Polo es una de las experiencias piloto de Zona de Biodiversidad, llamada La Esperanza. El lugar fue despojado bajo presión paramilitar beneficiando al señor Alfredo López, de acuerdo con las afirmaciones de los testigos de la comunidad. Le puede interesar:["300 neoparamilitares transitan por territorios en Chocó sin respuesta del Estado"](https://archivo.contagioradio.com/300-neoparamilitares-transitan-por-territorios-en-choco-sin-respuesta-del-estado/)

A pesar del ejercicio pacífico del derecho de la propiedad de la familia Polo, está ha sido imposibilitada de usar y gozar del territorio, además de estos hechos, este semestre grupos **neoparamilitares han amenazado a la familia**. Le puede interesar: ["Militares ingresaron a la zona Humanitaria de Nueva Vida en Curvaradó"](https://archivo.contagioradio.com/militares-ingresaron-a-la-zona-humanitaria-de-nueva-vida-en-curvarado/)

###### Reciba toda la información de Contagio Radio en [[su correo]
