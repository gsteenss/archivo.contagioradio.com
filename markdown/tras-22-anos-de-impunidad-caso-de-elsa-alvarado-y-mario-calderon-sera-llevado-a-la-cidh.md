Title: Tras 22 años de impunidad, caso de Elsa Alvarado y Mario Calderón será llevado a la CIDH
Date: 2019-05-17 15:56
Author: CtgAdm
Category: DDHH, Memoria
Tags: CIDH, Crimen de Estado, Elsa Alvarado, Mario Calderon
Slug: tras-22-anos-de-impunidad-caso-de-elsa-alvarado-y-mario-calderon-sera-llevado-a-la-cidh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Elsa-Alvarado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

Familiares, amigos y compañeros de los defensores de DD.HH, Mario Calderón,  Elsa Avarado y su padre Carlos, se reunirán este 19 de mayo, para honrar  la memoria de la pareja asesinada por paramilitares en 1997 y para solicitar a la Comisión Interamericana de Derechos Humanos (CIDH) que declare responsable al Estado colombiano por este crimen y otorgue a sus allegados medidas de reparación integral.

### ¿Cómo ha avanzado el caso de Mario Calderón y Elsa Alvarado? 

Sergio Ocazionez, abogado de la Comisión Colombiana de Juristas (CCJ), organización que lleva el caso,  afirmó que se ha avanzado lentamente, al día de hoy solo hay una condena contra uno de los sicarios que participaron en el crimen, mientras el resto fueron asesinados, un práctica que según el jurista es común en el modo de operar de los paramilitares.

Pese a ello existen pruebas que han podido ser aportadas, "hace unos años alias 'Don Berna' dio declaraciones que permitieron esclarecer la participación de agentes estatales en particular del Coronel (r) Plazas Acevedo,  actualmente acogido por la JEP, de ser el facilitador en la logística del crimen", agregando que todo dependerá del aporte a la verdad que realice Plazas Acevedo en este caso y en otros en los que está vinculado como la masacre de Mapiripán y el asesinato de Jaime Garzón".

### Camino a la CIDH

Según el abogado, la intención de acudir a la Comisión Interamericana de Derechos Humanos,  es garantizar los derechos negados y reparar en algo el daño que sufrieron los allegados de las víctimas y aunque advierte que este proceso junto a la CIDH puede ser tardío, esperan que la acumulación de casos como el de Jaime Garzón, por su similitud permita que se den avances. [(Le puede interesar: Declaran crimen de lesa humanidad caso de Mario Calderón y Elsa Alvarado)](https://archivo.contagioradio.com/declarado-crimen-de-lesa-humanidad-caso-de-mario-calderon-y-elsa-alvarado/)

Adicionalmente, este 19 de mayo se realizará una caminata por la memoria que saldrá desde la sede del CINEP a las 9:00 am, "la idea es recorrer algunos puntos simbólicos y de importancia para el trabajo de Mario Calderón y Elsa Alvarado en los cerros orientales", indicó al abogado quien explicó cómo en vida, Mario trabajó por el reasentamiento de comunidades urbanas que protegieran el territorio.

<iframe id="audio_35996407" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35996407_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
