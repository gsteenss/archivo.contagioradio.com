Title: Persiste la deuda de justicia con las víctimas de El Salado
Date: 2020-02-17 19:13
Author: CtgAdm
Category: Memoria, Nacional
Tags: masacre, violencia
Slug: persiste-la-deuda-de-justicia-con-las-victimas-de-el-salado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/el_salado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/juan-carlos-nino-abogado-comision-colombiana-juristas_md_47975305_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Juan Carlos Niño | Abogado Comisión Colombiana de Juristas

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

Este 17 de febrero familiares y sobrevivientes de la masacre de El Salado ocurrida del 16 al 22 de febrero del 2000 en la población de Villa del Rosario-El Salado, Bolivar, conmemoran los 20 años de este hecho; al mismo tiempo organizaciones defensoras de las víctimas destacan los avances y retrocesos en el proceso por la búsqueda de la verdad y la justicia.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Impunidad hacia los responsables de la masacre

<!-- /wp:heading -->

<!-- wp:paragraph -->

Durante casi una semana, **450 paramilitares junto con la participación de 25 integrantes de la 1era Brigada de Infantería de Marina ingresaron a este territorio en Bolívar y cometieron crímenes como homicidios, violaciones sexuales, hurtos, torturas entre otros**. Hoy 20 años después muchos de estos crímenes permanecen en la impunidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello y en materia de responsabilidad **sólo 15 de los cerca de 450 paramilitares que participaron en la masacre han sido condenados**, entre ellos uno de los jefes paramilitares quien se fugó de la cárcel en 2001; por su parte de **los responsables de la Fuerza Pública solo uno ha sido condenado, los demás se encuentran procesos en la Físcalia.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"No se ha dictado medida de aseguramiento ni resolución de acusación contra ellos como coautores del crimen, es un proceso que aún se encuentra en la impunidad "*.
>
> <cite> Juan Carlos Niño, abogado Comisión Colombiana de Juristas </cite>

<!-- /wp:quote -->

<!-- wp:heading {"level":3} -->

### Ni entrega digna de su familiares, ni apoyo psicológico a las víctimas

<!-- /wp:heading -->

<!-- wp:paragraph -->

En marzo de 2019 se realizaron diligencias de exhumación de restos de víctimas que fueron entregados a dos familias en septiembre del mismo año, a pesar de ellos **en la actualidad aún se encuentran pendientes labores de búsqueda en torno a 23 víctimas directas de la masacre**. (Le puede interesar: <https://archivo.contagioradio.com/estado-debe-reconocer-su-responsabilidad-y-dar-la-cara-a-bojaya-y-al-mundo/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo en el 2018 la CCJ denunció que la comunidad de El Salado no recibía atención integral en salud como víctimas, *"debido a las barreras administrativas que imponía el Ministerio de Salud y Protección Social para el cumplimiento de órdenes judiciales*".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante ello el abogado Juan Carlos Niño, coordinador de litigio nacional de la Comisión Colombiana de Juristas dijo, *"nosotros presentamos una acción de tutela ante el incumplimiento en la atención a las victimas esto dio como resultado la sentencia T045 que exige una atención sicosocial a las victimas tanto a las que retornaron como a las que aún siguen fuera"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A pesar de ello el abogado reconoció que *"**aún no se ha cumplido ni desarrollado una implementación para que se desarrolle y se cumpla esta sentencia**"* y agregó que las causas en renovación de convenios por parte del Ministerio eran *"ausencia de presupuesto, dificultades de contratación y falta de cumplimiento de los compromisos asumidos con la comunidad"*. (Le puede interesar: <https://www.justiciaypazcolombia.com/masacre-de-el-salado/> )

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Un espacio que trabaja por El Salado

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el 2014 como iniciativa la CCJ conformó la Mesa Técnica Institucional, espacio conformado por el Grupo de Búsqueda e Identificación de Personas Desaparecidas (GRUBE) de la Físcalia, representantes de víctimas, un equipo forense asesor, la Unidad para la Atención y Reparación Integral a las Víctimas y el Ministerio de Salud.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un espacio que según el abogado de la CCJ para el 2019 tendría que haber reactivado sus labores entorno a la búsqueda, exhumación y entrega digna de los restos de las víctimas. (Le puede interesar: <https://archivo.contagioradio.com/los-crimenes-que-no-existen/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo reconoció que, *"desde la creación de esta Mesa se desatendieron compromisos, se incumplieron acuerdos y recomendaciones técnicas que se dieron por parte de la Comisión afectando principalmente el proceso de exhumación y búsqueda de los desaparecidos".*

<!-- /wp:paragraph -->
