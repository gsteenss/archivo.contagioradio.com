Title: Cárceles colombianas no cuentan ni con ibuprofeno para atender presos enfermos
Date: 2016-05-12 17:59
Category: DDHH, Nacional
Tags: crisis carcelaria, crisis de la salud, INPEC, Ministerio de Justicia, presos politicos
Slug: carceles-colombianas-no-cuentan-ni-con-ibuprofeno-para-atender-presos-enfermos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/crisis-carcelaria-e1463093458368.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Defensoría del Pueblo 

###### [12 May 2016]

Aunque el nuevo Ministro de Justicia, Jorge Eduardo Londoño, **decretó nuevamente emergencia carcelaria en 74 prisiones del país, la situación de los más de cinco mil presos que sufren enfermedades como VIH, cáncer, diabetes y problemas psiquiátricos**, no mejora, y en cambio continúa agudizándose la crisis pues ya ni siquiera se cuenta con ibuprofeno en los centros carcelarios.

Así lo denuncia René Nariño, vocero de los internos de la cárcel El ERON – Picota quien denunció a través de Contagio Radio, que esta situación ha obligado a los reclusos a realizar distintas jornadas de movilización. En La Tramacua de Valledupar desde hace 2 días están en huelga de hambre más de 300 prisioneros; en la Cárcel Doña Juana en La Dorada más de 340 internos ya completan 12 días en huelga; y en La Picota de Bogotá más de 700 internos se encuentran en una jornada desobediencia civil y pacífica, además se tiene planeado que la próxima semana otros 3500 prisioneros de esa cárcel se sumen a esta protesta.

“La situación es bastante grave, **ya no hay ni suministros médicos básicos como el acetaminofén o ibuprofeno, hay médicos, pero como no les han pagado en el 100% de las cárceles del país hay  desatención médica total”**, cuenta Nariño. Es tal  la crisis de salud que se vive el interior de los centros penitenciarios que ni una gripe es tratable, y mucho menos enfermedades más graves como VIH, tuberculosis, cánceres, etc.

Por esa situación, Francisco Ruiz de aproximadamente 65 años de edad, preso político y exconcejal de la Unión Patriótica, falleció la semana pasada por negligencia médica en la cárcel La Modelo. Francisco sufría del síndrome Guillain-Barré, un trastorno en el que el sistema inmunitario del cuerpo se ataca a sí mismo por error, una enfermedad que no fue debidamente tratada durante sus 4 años de reclusión.

Así mismo, René Nariño relata que en la mañana de ayer un interno del patio 4 de la Picota, sufrió un pre-infarto y tuvo que ser auxiliado por los mismos internos que conocían primeros auxilios, pues no había ningún médico que lo atendiera. Además en ese mismo centro carcelario,  en la mañana de este jueves una persona murió por a causa de un ataque perpetrado por otro interno. **“La corrupción del INPEC es transversal a todo el sistema carcelario y a cambio de unas monedas los guardias no observan, no hacen, ni informan nada”** denuncia el vocero de los presos.

"**Cada casa, cada grupo de caciques paga a cada guardián pabellonero de cada patio \$1.500. 000 mensua**l sin incluir el dinero que pagan a cada cuadro de mando. Son 4 pabelloneros guardias por mes. Por 6 millones de pesos la Guardia entra el bazuco marihuana, perico y el trago. También les pagan por el silencio sobre lo que pase dentro de dichos patios. Todo se vende dentro del patio, por supuesto el nivel de degradación es paralelo a lo visto es sectores deprimidos urbanos como el Bronx o la antigua calle del cartucho", señala René Nariño.

Frente a esos múltiples casos, no hay respuesta de ninguna autoridad, pese a la declaratoria de crisis carcelaria, la situación solo se profundiza y para los presos se trata de “anuncios para apaciguar las aguas, pero en la práctica no funcionan de nada.

Desde el Movimiento Nacional Carcelario se hace un llamado urgente para que se expida el **Decreto de Deshacinamiento anunciado por los anteriores ministros de Justicia desde la promulgada y no aplicada Ley 1709. Un decreto que hasta la fecha ha sido incumplido,** pese a que se han declarado 5 emergencias carcelarias.

<iframe src="http://co.ivoox.com/es/player_ej_11518444_2_1.html?data=kpaik52YeJWhhpywj5aXaZS1lJeah5yncZOhhpywj5WRaZi3jpWah5yncbPZz8qYsMbWrYa3lIqvk9SPcYzE087gy9TSqdPjjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
