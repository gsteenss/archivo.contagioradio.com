Title: Si hay voluntad política este año habría Zona de Reserva Campesina en el Catatumbo
Date: 2016-02-23 11:46
Category: DDHH, Nacional
Tags: ANZORC, Catatumbo, Conversaciones de paz de la habana, Cumbre Agraria, FARC, Juan Manuel Santos
Slug: si-hay-voluntad-politica-este-ano-habria-zona-de-reserva-campesina-en-el-catatumbo
Status: published

###### [Foto: contagioradio] 

<iframe src="http://co.ivoox.com/es/player_ek_10543373_2_1.html?data=kpWilpiXe5Shhpywj5WcaZS1kp6ah5yncZOhhpywj5WRaZi3jpWah5yncbTdjM3O25Das83pz9nOxpDUs82ZpJiSo6nYrcTVjMrg1sqPpYa3lIqvk9SPrMLW04qwlYqliMKfu9Tbw5DIqYzGxtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [César Jeréz, ANZORC] 

###### [23 Feb 2016] 

Tras una nueva reunión de los voceros de los campesinos que participaron el paro del Catatumbo, con el gobierno nacional y los garantes del proceso de negociaciones, el ministro de agricultura Aurelio Iragorri, se comprometió con la instalación de la Zona de Reserva Campesina en esa región de Colombia, sin embargo, las amenazas que se enfrentan para la dilación de la decisión siguen siendo las mismas, manifiesta **[Cesar Jeréz, vocero de ANZORC.](https://archivo.contagioradio.com/hay-un-balance/)**

Luego de la reunión **el ex presidente Samper integrante de la Comisión de garantes, afirmó que el gobierno estaba comprometido en la constitución de la ZRC**, pero, según Cesar Jeréz, los enemigos de la puesta en marcha de la ley para la zona siguen siendo los mismos, latifundistas, terratenientes y mafiosos que tienen fuertes intereses en proyectos como la explotación de carbón a cielo abierto en por lo menos 300 mil hectáreas del Catatumbo.

Por su parte,  según el vocero de ANZORC, el gobierno tiene una presión que es el [proceso de conversaciones de paz](https://archivo.contagioradio.com/?s=proceso+de+paz), que “contra todo pronóstico” ha sido utilizado contra las mismas Zonas de Reserva Campesina "como una moneda de cambio". En ese sentido además de la voluntad política del gobierno está la **posible firma de un acuerdo de paz con las FARC**. Por ello, la idea también es [construir un territorio de paz en la región](https://archivo.contagioradio.com/?s=terrepaz).

Una de las conclusiones de la reunión que se desarrolló ayer es que pese al avance de algunas medidas de confianza todavía quedan el 70% de los acuerdos por desarrollar. Según Jeréz también se habló “mucho de los **[cultivos de coca” pero se reconoció la responsabilidad del gobierno al no proveer de programas de sustitución de cultivos de uso ilícito.](https://archivo.contagioradio.com/campesinos-del-catatumbo-denuncian-nuevos-incumplimientos-del-gobierno/)**

Son cerca de 2 años los que han transcurrido desde el paro de los campesinos del Catatumbo que se vieron obligados a cesar sus actividades y suspender el tráfico entre Cúcuta y Tibú durante cerca de un mes para visibilizar su precaria situación, que va desde desabastecimiento de agua hasta la actuación de fuerzas paramilitares en sus territorios. Durante el paro del catatumbo fueron asesinados 4 campesinos en medio de la fuerte represión de las FFMM y el ESMAD.
