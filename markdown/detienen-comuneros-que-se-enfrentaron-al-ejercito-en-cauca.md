Title: Detienen comuneros que se enfrentaron al ejército en Cauca
Date: 2018-01-11 12:57
Category: Movilización, Nacional
Slug: detienen-comuneros-que-se-enfrentaron-al-ejercito-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/cauca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Enraizando: Comunicación y educación popular] 

###### [11 Ene 2018] 

Tras haberse conocido el video en el que cuatro campesinos del Cauca se enfrentaron a integrantes del ejército en medio del proceso de recuperación de la madre tierra, las comunidades indígenas han denunciado la **desaparición de uno de los comuneros**. Han manifestado que ha habido una arremetida por parte del ESMAD contra el pueblo indígena Nasa. Sin embargo en la mañana de este jueves se pudo establecer que el indígena fue detenido por la fiscalía.

Según la página de Facebook de Enraizando: Comunicación y educación popular, los comuneros del Cauca llevan **tres años en el proceso de liberación de la madre tierra** en lugares que “fueron de grandes terratenientes”. En este proceso se han desarrollado diferentes arremetidas por parte de la Fuerza Pública en donde los campesinos han denunciado que “han estropeado alambrados, cortado sembrados y disparado contra los indígenas”.

Precisamente esto es lo que denunciaron el pasado 4 de enero cuando se conoció el video de la confrontación entre los indígenas y los integrantes de las Fuerzas Armadas. “Se cree que no es casual que hace algunos días integrantes del ejercito grabaran un video **después de haber disparado** contra los indígenas y haya desaparecido uno de los liberadores que aparece en el video enfrentando con machete en mano a los soldados.”.

### **Indígenas denuncian creciente militarización de los territorios** 

Teniendo como referencia los hechos que sucedieron el 4 de enero, el pueblo Nasa ha alertado que ha habido una **“tergiversación por parte del brazo mediático** de los intereses mediáticos”. Indicaron además que el Estado Nacional “buscó eco en los comentarios fundados en el desprecio para justificar la creciente militarización que sufren los territorios indígenas”. (Le puede interesar: ["Pueblo Nasa del Putumayo realiza Minga de control territorial"](https://archivo.contagioradio.com/pueblo-nasa-en-el-putumayo-realiza-minga-de-control-territorial/))

Denuciaron que el 9 de enero se volvió a presentar una situación de intimidación por parte de la Fuerza Pública. Indicaron que hubo **“una jornada de represión** donde alternaron gases lacrimógenos, piedras, garrotes e incluso ráfagas de fusil contra la población civil”. Seguidamente y en compañía de la Policía Nacional, agentes del CTI y el ESMAD “capturaron a dos personas que estaban presentes en el momento”.

Adicionalmente, las comunidades indígenas **rechazaron lo expuesto por la Fiscalía General de la Nación** quien indica que “los detenidos habrían atacado a comisión del CTI de la Fiscalía y de la Policía que investigaban los hechos ocurridos el pasado 04 de enero de 2018, en la hacienda Miraflores de Corinto (Cauca), en los que fue agredido personal del Ejército Nacional”.

**Versión de la comunidad sobre el hecho del 4 de enero: **  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcepenraizando%2Fvideos%2F2431890363703060%2F&amp;width=500&amp;show_text=false&amp;appId=894195857389402&amp;height=524" width="500" height="524" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Finalmente, las comunidades indígenas de esta región alertaron que las afirmaciones de las instituciones del Estado **ponen en riesgo a la población civil** y al proceso de liberación de la madre tierra que adelantan los comuneros en el Cauca. (Le puede interesar: ["Indígenas Nasa continúan con la liberación de la madre tierra"](https://archivo.contagioradio.com/indigenas-nasa-continuan-con-la-liberacion-de-la-madre-tierra/))

Además, argumentan que lo expuesto en los medios de comunicación “no solo justifica la desmedida represión sino** más asesinatos”** teniendo en cuenta que los indígenas han sido catalogados de ser parte de grupos armados y guerrilleros.

### **Fiscalía imputará cargos por violencia contra servidor público** 

Según el comunicado de la Fiscalía, en la ciudad de Cali, en un juzgado de control de garantías la fiscalía imputará cargos por violencia contra servidor y daño en bien ajeno. Los comuneros fueron detenidos este 9 de Enero luego de los hechos dados a conocer a través del video que circuló en Facebook y otras redes sociales el pasado 4 de Enero.

Además el ente investigador afirmó que cuando la comisión estaba investigando el hecho presentado en el video, fueron atacados en la hacienda Miraflores en Corinto, Cauca y es cuando se producen las detenciones.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
