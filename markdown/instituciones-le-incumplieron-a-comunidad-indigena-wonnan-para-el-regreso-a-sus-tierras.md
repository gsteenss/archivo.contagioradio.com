Title: Instituciones le incumplieron a comunidad indígena Wonnan para el regreso a sus tierras
Date: 2015-12-01 11:54
Category: DDHH, Nacional
Tags: Comunidad Wounnan, CRIC, Derechos Humanos, Desplazamiento en Colombia, indígenas, ONIC, Radio derechos Humanos
Slug: instituciones-le-incumplieron-a-comunidad-indigena-wonnan-para-el-regreso-a-sus-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Wounnan.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio radio 

###### [30 Nov 2015]

En el marco del segundo Comité de Justicia Transicional realizado durante el 2015 se concertó el retorno de 645 indígenas desplazados de la comunidad Wounaan quienes tras doce meses de estar en el coliseo del distrito de Buenaventura sin condiciones vuelven a sus territorios con tres niños muertos, amenazas a líderes, una pérdida de de identidad cultural en niños y jóvenes y los mayores con serios quebrantos de salud.

Edison Malaga Chirimia, miembro de la comunidad indica que regresan a su territorios tras haber soportado dificultados, pero que esto alegra ya que "es comunicarnos espiritualmente con nuestra madre tierra", además que fue con la ayuda de organziaciones no gubernamentales y la intervención de la Defensoría del Pueblo en el último tramo de la

Sin embargo, este regreso a los territorios no representa garantías ya que los del gobierno "no tienen voluntad política  con las comunidades indígenas", por esto indican en un comunicado que regresan confiando que la alcaldía Distrital y las instituciones encargadas asuman el retorno esto con acompañamiento civil, con la solución inmediata de necesidades básicas como el agua potable, alimentación acorde a las condiciones de retorno y a la necesidad de cada familia según el número de sus miembros, la entrega de los potrillos o canoas familiares indispensables para la movilidad.

Con este panorama la comunidad Wounaan regresa sin garantías, ni "dignidad inmediata" como valora Edison Malanga, hoy "la gente está aquí, haciendo la limpieza, organizando la casita", todo esto con dificultades, pero a la espera de muchas más, si el gobierno no interviene  y cumple de forma urgente.
