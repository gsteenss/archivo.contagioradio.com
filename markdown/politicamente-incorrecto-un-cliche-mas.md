Title: “Políticamente incorrecto”: un cliché más
Date: 2019-06-17 18:07
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: neoliberalismo, neuroliberalismo
Slug: politicamente-incorrecto-un-cliche-mas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/POLIITCAMENTE-INCORRECTO.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[Herido de muerte, **el neoliberalismo observa sus tesis pisoteadas en el suelo.** Ni libre mercado, ni menos Estado, son realidades sostenibles con plenitud hoy en el orden mundial; ni siquiera la derecha latinoamericana (fiel escudera del neoliberalismo) parece defenderlo más, y en cambio, contra todo el pronóstico de su debilucha teoría, más abraza al Estado, más lo necesita, más lo coopta, y continúa siendo la fuente de todo su poder (económico, político y militar).]

[Por supuesto la inequidad que produce, las mentiras sobre la reducción del Estado a su mínima expresión, se desbordan incluso ante aquellos que aún lo dictan como cátedra infalible en las universidades, y que como buenos misioneros gritan “¡viva el libre mercado!” a la vez que aplauden las reformas tributarias o las adjudicaciones de los contratos públicos, con dinero público, del público a las multinacionales… ¿Será que aún no ven el ridículo que hacen en esas cátedras (eso sí todas “anti-socialistas”) pero tan fuera de la realidad?   ]

[Al neoliberalismo su papá y a su abuela, lo dejaron morir en el desierto; hoy las dos naciones promotoras del neoliberalismo, Estados Unidos e Inglaterra, andan construyendo muros, subiendo aranceles, encarcelando niños, cerrando fronteras, saliendo de las comunidades económicas y políticas, inventando cuentos para debilitar a la competencia en el mercado, sancionando económicamente al que no negocia con ellas, perdiendo las guerras de invasión sustentadas en mentiras como les pasó en Siria, instrumentalizando a la ONU, a la OEA mediante el poder de sus Estados, reencauchando la doctrina Monroe etc.  ]

[Lo cierto es que, de la teoría neoliberal, sus postulados originales hoy más parecen la utopía de un anarquista, que cualquier cosa que habite en la realidad nacional o internacional. ¿Qué queda del neoliberalismo? Sin duda algo muy serio: los mecanismos de vinculación social que tuvo habilitados desde 1973 hasta 2019, sus efectos simbólicos en los comportamientos de las sociedades, es decir, su propuesta de cultura.]

[Aún queda mucho de ese fastidioso individualismo, de la competencia sin ética y el lenguaje mercantil del que hablaba el profesor Renan Vega, un lenguaje que se nos metió hasta en la cama y que mostró su fracaso dándole razón a la vieja premisa marxista “]*[la burguesía siempre es superada por la realidad]*[”.  Queda aún mucho de ese]*[neuroliberalismo]*[del que habla el profesor Biagini, un fenómeno que entroniza al yo, segando a los sujetos de todas sus posibilidades humanas o dejándoles ver solo unas pocas, mientras los condiciona a la premisa hegemónica “tiempo es dinero”, que los automatiza, los desprende de todo su pasado y solo les ofrece futuros y sueños que residen en el consumo de mercancías.]

[Sí, en Colombia aún queda el vestigio neoliberal en el lenguaje, esos cuentos de la economía naranja, la barbarie de aquellos que acusan a las víctimas como responsables de su propia muerte, últimamente personajes muy respetados de la sociedad operando bajo el necio principio neoliberal de la individualización de los problemas sociales, “]*[la pobreza es cupa de los pobres” “paren de parir]*[”… (de parir pobres le faltó decir), así como la promoción del “]*[fin de las ideologías]*[” que fue uno de los presupuestos lingüísticos más importantes del neoliberalismo y que produjo su cliché más fanático de todos: “]*[no polarice”]*[.]

[Si bien la realidad se ha encargado de negar las tesis del neoliberalismo, y queda muy poco para su muerte definitiva, aún queda parte de su cultura en los comportamientos, en el lenguaje, produciendo clichés para ser abrazados por sus huérfanos, quienes se aferran al Twitter para disparatar y no creerlo.  ]

> [Entre sus clichés lingüísticos hay uno último modelo que se deriva de esa típica desconexión con el pasado, de la unidimensionalidad con la que percibe la realidad, y que ha criado tantos petulantes que solo piensan por ellos mismos creyendo que el mundo comenzó con ellos. Este cliché se llama ser: “políticamente incorrecto”.]

[Políticamente incorrecto lo adopta desde un youtuber sin mucha prosa, hasta una periodista que ordena “]*[Paren de parir]*[” bajo los aplausos mortuorios del pedazo de cráneo de Hitler. Es una bandera que por el otro lado dice “]*[digo lo que se me da la gana]*[”, claro, esto es viable y normal en la sociedad actual, pero hay que aclarar: ni es política, ni es incorrecto.]

[No es política porque una opinión con una perspectiva que parte de ver la realidad desde una sola ventana de la casa, es unidireccional, unidimensional, no reconoce la totalidad, sino que afirma lo que ve en su particularidad; eso conlleva a muchos a creer que el problema se arregla con limpiezas, con controles natales, con muros, precisamente para no ver eso que “empaña su mundo”.]

[No es incorrecta, porque responde coherentemente con el modelo de una sociedad neoliberal que aún ofrece grandes rasgos de individualismo, que impulsa al darwinismo social como si fuéramos tortugas o pingüinos luchando solo por comer y aparearse. No es incorrecta porque es la actitud que le sirve al sistema, es decir, la de un sujeto que diga “lo que quiere” sin perspectiva política, materializa perfectamente un twitter: pájaros temerosos del suelo, trinando]*[lo que quieren]*[ encima de una rama sin escucharse en sí.  ]

[¿Qué causa eso? Oxigena una libertad de expresión tiránica que le es saludable al discurso democrático totalitario. Un discurso al que le interesa que haya muchos sujetos “]*[políticamente incorrectos]*[” para comprobar lo bien que se vende la desobediencia y lo democrático que puede ser el fascismo del siglo XXI.]

[No, no vengan con otro cuento neoliberal de que son “políticamente incorrectos”. Que de esa vaina nos hablen los líderes sociales, que no adornan sus luchas con clichés, sino que simplemente luchan contra el sistema con acción política, que ponen en riesgo sus vidas, que se alimentan de presente y pasado, condición que creería yo, sí es la base para definir a aquellos que son subversivamente correctos.]

##### [Leer más columnas de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)
