Title: Pequeñas hidroeléctricas ¿una nueva amenaza para el Quindío?
Date: 2018-10-18 15:20
Author: AdminContagio
Category: Voces de la Tierra
Tags: centrales hidroeléctricas, Quindío, Rios Vivos
Slug: pequenas-hidroelectricas-quindio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/descarga-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Crónica del Quindío 

###### 4 Oct 2018 

Las comunidades en Génova, en Quindío, se manifestaron en contra de la posible construcción de por lo menos tres **pequeñas centrales hidroeléctricas** (PCH) en ese municipio, por el temor que en ellos infunde el impacto que podría generar este tipo de proyectos en sus predios y a los ecosistemas naturales de sus territorios, promovidos por empresas como  E-lectrica S.A.S entre otras.

En este Voces de la Tierra, nos acompañaron **Viviana Viera**, habitante de Génova, quien lidera un proyecto agroecológico llamado Ecofinca La María, para hablar de las afectaciones que traerían estos desarrollos y **Tatiana Roa**, ambientalista, investigadora de CENSAT Agua Viva e integrante del Movimiento Ríos Vivos Colombia.

<iframe id="audio_29426794" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29426794_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
