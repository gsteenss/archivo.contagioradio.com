Title: "Árbol de vida", la lucha por salvar el patrimonio de la mineria en Ciudad Bolívar
Date: 2016-09-19 17:51
Author: AdminContagio
Category: 24 Cuadros
Tags: ciudad bolivar, Documental, Globale
Slug: arbol-de-vida-la-lucha-por-salvar-el-patrimonio-de-la-mineria-en-ciudad-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/palo_ahorcado0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Bogota.Gov 

###### [19 Sep 2016] 

El documental “Árbol de Vida” del colectivo audiovisual Caja de Espejos, narra la historia de un Árbol que ha hecho parte de la historia y vida de una comunidad en Ciudad Bolívar y que ahora se ve amenazado por las explotación de minas de Arena.

Aunque muchos le conocen como “El palo del ahorcado”, otros le llaman Abuelo. El Árbol de Vida, que en realidad es un emblemático eucalipto, ha llegado a considerarse parte de la historia del barrio Potosi en Ciudad Bolivar, pero ahora esta insignia y patrimonio de la comunidad se ve amenazado por las empresas de minería de arena que quieren desenraizarlo, desencadenado indignación y lucha en todos aquellos que se resisten a dejarlo morir.

https://www.youtube.com/watch?v=v1yvtQHhFxw

El documental “Árbol de Vida” hasta el momento ha llegado a más de 2 mil espectadores en colegios, universidades, cafés, espacios de colectivos sociales, entre otros y ahora también participará en el festival Internacional de Video documental Globale Bogotá, que se realizará del 19 al 23 de septiembre en la Cinemateca Distrital y que presentará este documental el lunes 19 de septiembre a las 7 de la noche.

Además del trabajo realizado con Árbol de Vida, Caja de Espejos se encuentra en desarrollo de tres proyectos más, entre los que están el documental Abuela, Pai mamita y Semblanza en 35mm.

Por otra parte, también MIDBO, una de las muestras más importantes del país por su trayectoria, escogió a Árbol de Vida para hacer parte de la sección Panorama, en la que estará junto a otros nueve documentales nacionales escogidos entre más de 300 propuestas. Los títulos seleccionados podrán disfrutarse en diversas salas de la capital entre el 24 y 30 de octubre, fecha en que se desarrollará la Muestra.

Para conocer más sobre Árbol de Vida y el resto de proyectos en los que está trabajando el colectivo Caja de Espejos, hablamos con una de sus integrantes, Edna Higuera, quien también es directora del documental:

<iframe id="audio_12971534" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12971534_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
