Title: Amnistía Internacional exige a Israel responder por 20 ejecuciones extrajudiciales
Date: 2016-09-28 15:32
Category: El mundo, Judicial
Tags: Apartheid Israel, conflicto palestina israel, Crímenes de guerra Israel, Ejército de Israel, régimen de Tel Aviv
Slug: amnistia-internacional-exige-a-israel-responder-por-20-ejecuciones-extrajudiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/palestina_nota.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: UrbeSalvaje 

###### 28 de Sep 2016 

Según Amnistía Internacional **por lo menos 20 personas habrían sido asesinadas por las FFMM de Israel desde el 22 de septiembre de 2015 hasta el 6 de septiembre del presente año**, al menos en 15 de estos casos los ciudadanos de Palestina fueron asesinados a tiros deliberadamente, pues no representaban amenaza alguna.

Además se ha denunciado que desde el 1 de octubre de 2015, las fuerzas israelíes han matado a más de 200 palestinos en Israel y en los territorios ocupados. La mayoría de estas muertes, más de 150, se produjeron durante supuestos intentos, o ataques reales de individuos palestinos contra soldados israelíes, policías y civiles.

**En un memorándum emitido el 14 de septiembre la organización expone de forma detallada 20 casos de ejecuciones extrajudiciales, con los cuales no se ha iniciado ningún proceso de investigación.** En el documento se resalta que “la fuerza letal se ha utilizado innecesaria o desproporcionadamente”. Hasta el momento, las autoridades israelíes no han dado respuesta al memorándum emitido por Amnistía Internacional.

En palabras de Philip Luther, director de Investigación y Trabajo de Incidencia para Oriente Medio y el Norte de África de Amnistía Internacional: “Desde la escalada de la violencia producida el año pasado en Israel y los Territorios Palestinos Ocupados, ha habido un preocupante incremento de los homicidios ilegítimos a manos de fuerzas israelíes, fomentado por una cultura de impunidad”.

Hasta el momento **sólo en uno de los veinte casos denunciados, existe una acusación formal contra un soldado, sin embargo, aún teniendo estos indicios no se ha iniciado ningún proceso de investigación**. Habitantes de Palestina han manifestado su indignación no sólo por la inoperancia de las autoridades, también porque los[asesinatos de jóvenes palestinos a manos de las fuerzas israelíes](https://archivo.contagioradio.com/31-palestinos-asesinados-3730-heridos-en-14-dias-de-ataques-en-cisjordania-y-gaza/) continúan y los homicidios siguen en aumento.

Uno de los casos mas representativos es el de **Hadeel al-Hashlamoun**, una joven de 18 años, quien murió asesinada el 22 de septiembre de 2015 en Hebrón. Hadeel recibió un disparo en la cabeza por soldados israelíes mientras transitaba por el *Checkpoint 56* en Hebrón. Ella sostenía un cuchillo, pero la separaba de los soldados una barrera de metal, y en ningún momento representaba una amenaza para los soldados, quienes hicieron uso de la fuerza letal deliberado.

El periódico israelí Haaretz informó que el comandante Judea Brigada del ejército israelí, Coronel Yariv Ben Ezra, había llegado a la conclusión en una revisión inicial del incidente que Hadeel al-Hashlamoun podría haber sido detenida por la soldados y no asesinada. Hasta la fecha Amnistía Internacional no tiene conocimiento de la existencia de cualquier investigación penal sobre su muerte. Afirman que su muerte, al igual que la de los demás ciudadanos palestinos, debe ser investigada como una posible ejecución extrajudicial.

El Dr. Salah al-Hashlamoun, padre de Hadeel, manifestó que “A lo que nos enfrentamos hoy es a una lucha contra la realidad de que Israel puede hacer lo que le plazca sin rendir cuentas ante nadie”.

**La denuncia realizada por Amnistía Internacional evidencia que el sistema judicial militar ha dejado constantemente sin justicia a las víctimas** [palestinas de homicidios ilegítimos](https://archivo.contagioradio.com/en-octubre-han-sido-asesinados-24-palestinos-en-territorios-ocupados-en-cisjordania/) y a sus familias y, por otra, muestra que la conducta del Departamento de Investigaciones Internas de la Policía no ha llevado a cabo un proceso imparcial e independiente frente a los casos denunciados.

Sobre estos hechos, Philip Luther ha manifestado que “La única manera de impedir nuevos homicidios ilegítimos es poner fin a la impunidad que existe para quienes los han cometido en el pasado (...) Israel tiene el deber de investigar de manera exhaustiva, inmediata e imparcial todos los homicidios cometidos por sus fuerzas de seguridad, y de mantener a las familias plenamente informadas”.

<iframe id="doc_38465" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/325678413/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
