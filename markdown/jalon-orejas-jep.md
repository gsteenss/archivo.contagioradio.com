Title: Jalón de orejas a la JEP por parte de víctimas en audiencia de Gral. Montoya
Date: 2018-10-18 15:48
Author: AdminContagio
Category: Judicial, Paz
Tags: falsos positivos, JEP, Mario Montoya Uribe, soacha, víctimas
Slug: jalon-orejas-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-18-at-12.53.37-PM-770x400-1.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 Oct 2018] 

Antes de finalizar la audiencia que terminó con la firma del acta de sometimiento del General (r) **Mario Montoya Uribe** ante la Jurisdicción Especial para la Paz (JEP), **abogados representantes de víctimas y sus apoderados abandonaron la sala por considerar que existieron irregularidades en el proceso,** denunciando que no fueron reconocidos, y no hubo claridad en los procedimientos.

<iframe src="https://co.ivoox.com/es/player_ek_29427380_2_1.html?data=k56hlJyXfJGhhpywj5aWaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncaLiwpC9h6iXaaKlxt-SlKiPscLY08qYxsqPrtDqxtOY2Iqnd4a1pcjhy9LFt4zYxpDSzMrHucTdhqigh6eXsozZ2dnfw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Para **Ana Páez, madre de Eduardo Garzón Páez quien fue víctima de ejecución extra-judicial,** la audiencia fue **"una falta de respeto y una completa grosería"** porque no los reconocieron como víctimas, advirtiendo que aunque fueron citadas a la audiencia, no fueron tomadas en cuenta, y en su lugar sí se escucho atentamente a Montoya.

No obstante, desde las organizaciones sociales que representan a las víctimas, apuntaron que la JEP es un espacio aún en construcción, el cual debe hacer ajustes procedimentales, acompañamiento a las víctimas y debe ser acompañado hasta su perfeccionamiento. (Le puede interesar: ["El 17 de octubre se reanudará audiencia contra General Montoya"](https://archivo.contagioradio.com/el-17-de-octubre-se-reanudara-audiencia-contra-general-montoya/))

Desde la primera audiencia, los abogados representantes de víctimas habían señalado el problema en la indebida notificación del universo de víctimas, y sobre la falta de claridad del procedimiento que se iba a realizar. En esa ocasión **presentaron una solicitud de nulidad para que se aclarara está situación,** sin embargo, el recurso fue resuelto al final del procedimiento.

<iframe src="https://co.ivoox.com/es/player_ek_29427401_2_1.html?data=k56hlJyYdJKhhpywj5aWaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5ynca7Vz9rSzpCrpdPXhqigh6aopYampJDOxNTLpcXjjMrbjdfJtNPZ1Mrb1sbHrYa3lIqvldOPqMaf14qwlYqliMToytLO1ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Según Manuel García, abogado de la Comisión Intereclesial de Justicia y Paz y representante de víctimas, esta situación se produjo con el fin de que se firmara el acta de sometimiento del uniformado ante la JEP. Aunque está firma es importante para el proceso, **más importante aún es que se brinden las garantías jurídicas para las víctimas**, y que haya claridad en el desarrollo de los procesos, para que puedan tener una representación adecuada.

En ese sentido, Páez señaló que la JEP debió informar a los asistentes sobre el procedimiento que se realizaría, de tal forma que supieran quienes son reconocidos como víctimas y así poder solucionar el problema de su representación. Adicionalmente, sostuvo que **en esa instancia hasta ahora se ha escuchado lo que dice Montoya, pero no se ha tenido en cuenta la voz de las víctimas. **(Le puede interesar:["Montoya, diga la verdad: Exigen las víctimas"](https://archivo.contagioradio.com/montoya-diga-la-verdad-exigen-victimas/))

### **"Para tener paz se necesita verdad plena"** 

Páez afirmó que decidió permanecer hasta el final de la audiencia para poder ver a Montoya, y preguntarle ¿por qué habían asesinado a su hijo?, pero **la única respuesta que recibió del uniformado fue una sonrisa.** En consecuencia, sostuvo que la jurisdicción de paz debería escuchar más a las víctimas, tener en cuenta sus pedidos y reconocerlos como tal para poder participar de los procesos.

Desde esa perspectiva, el abogado García señaló que la JEP es un espacio para encontrar verdad, justicia y reparación, por lo tanto es un escenario importante que debería ser apoyado, construido y mejorado a partir de las experiencias que se están teniendo en las audiencias. Afirmación que a la que Páez añadió que, **para estar en paz, debería tenerse garantías de verdad plena y no repetición de los hechos.**

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
