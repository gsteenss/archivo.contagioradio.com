Title: Se mantiene la presión contra reelección en Paraguay
Date: 2017-04-03 14:10
Category: El mundo, Otra Mirada
Tags: Horacio Cartes, paraguay, Reelección, Rodrigo Quintana
Slug: paraguay
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Paraguay-e1488999688754.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Paraguay-provocaron-Congreso-detenidos-AP_LNCIMA20170401_0039_5.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/Manifestantes-Rodrigo-Quintana-Policia-Paraguay_LNCIMA20170402_0171_1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La nación 

###### 03 Abr 2017 

Tras los enfrentamientos del fin de semana que dejaron como saldo 200 manifestantes detenidos, un centenar heridos, un diputado con heridas en la cara y la muerte de un joven activista, continúan en Paraguay las acciones de rechazo a la enmienda constitucional que permitiría la reelección presidencial.

La decisión tomada de manera extraordinaria por 25 senadores el pasado viernes, desató la protesta contra el parlamento de ese país, dejando como consecuencia la sede del legislativo apedreada y envuelta en llamas, manifestaciones que se replicaron en diferentes regiones del país.

En el transcurso de este lunes, varios grupos de jóvenes continúan haciendo presencia en carpas frente a las instalaciones humeantes del Congreso, para recoger firmas que permitan reversar la voluntad de los senadores. Le puede interesar: [Feminicidios en Paraguay continúan en aumento](https://archivo.contagioradio.com/feminicidio-en-paraguay-continua-en-aumento/)

** Asesinato de Rodrigo Quintana**

El presidente de la Juventud Liberal de la Colmena Rodrigo Quintana, fue asesinado por la policía durante los disturbios devenidos de las manifestaciones del pasado viernes contra la aprobación de la enmienda. De acuerdo con los reportes de medios locales, el activista de 25 años fue atacado cuando se encontraba en la sede del Movimiento ubicada a kilómetro y medio del lugar de las confrontaciones.

A pesar de haber sido trasladado al Hospital de emergencias médicas, los doctores no pudieron hacerse mayor cosa para salvar su vida. El reporte médico daba cuenta de 9 heridas de bala encontradas en el cuerpo del líder político provocadas por el mismo número de perdigones utilizados por la policía del Cuerpo Antidisturbios.

Este lunes se conoció que Gustavo Florentín, uniformado que habria disparado contra el jóven y quien declaró que creyó tener balines de goma y no de plomo para cargar su arma, habría abandonado su residencia a pesar de tener una restricción por violencia doméstica emitida desde el pasado 15 de marzo.

**Las decisiones de Cartes**

Ante los hechos ocurridos que captaron la atención internacional, el presidente Horacio Cartes tomó la decisión el sábado de destituir a Tadeo Rojas como Ministro del Interior al igual que la del comandante de Policía Críspulo Sotelo, tras los hechos que concluyeron con la muerte del activista liberal Rodrigo Quintana.

El domingo, y luego del llamado del  Papa Franciso a "buscar una solución política y evitar caer en la violencia", en Venezuela y Paraguay,  el mandatario se mostró conciliador asegurando en alocución televisada  que es necesario armar una mesa de diálogo con todos los partidos con representación parlamentaria y la Conferencia Episcopal para encontrar una solución a las diferencias por la reelección presidencial.

Por el momento, Efraín Alegre, presidente del también conservador Partido Liberal (PLRA) aseguro que mientras Cartes no retire el proyecto de enmienda constitucional no se sentará a dialogar “¡Pero qué fácil hacer un golpe, matar a un joven y pedir sentarse a conversar! Vamos a conversar pero cuando esté todo en orden”, simultaneamente convocó a una nueva protesta “contra Cartes y su proyecto” de reelección para la noche de este lunes.
