Title: Especies silvestres en riesgo por resolución de Minambiente
Date: 2017-09-29 16:37
Category: Ambiente, Voces de la Tierra
Tags: Ministerio de Ambiente
Slug: ministerio-de-ambiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/orquidea-flor-nacional-1024x768-e1506380748278.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/CszHRYhXEAA_v-S-e1474490131839.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: odretours.com 

###### [25 Sep 2017] 

Una resolución del Ministerio de Ambiente, con la que se piensa dar pasos hacia adelante en materia de protección y conservación de las especies de fauna y flora que hay en el país, realmente podría poner en riesgo a varios ejemplares animales y vegetales, según aseguran expertos.

Esa resolución plantea un grave problema respecto a la forma como se lleva a cabo la clasificación de diversas especies. Catalina Reyes, bióloga de la Universidad de los Andes explica que se revisa la forma como se clasifican los ejemplares de fauna y flora, según la resolución expedida por esa cartera, algunos no concuerdan con la clasificación del Comité Coordinador de Categorización de Especies Silvestres amenazadas, cuya misión es actualizar la lista de especies en riesgo en Colombia, y lo que es peor, **tampoco se estaría respetando las directrices de Unión Internacional de Conservación de la Naturaleza.**

Se trata la resolución 1912, expedida por el la cartera ambiental el pasado 15 de septiembre, **"por la cual se establece el listado de las especies silvestres amenazadas de la diversidad biológica colombiana continental y marino costera** que se encuentran en el territorio nacional y se dictan otras disposiciones".

De acuerdo con la bióloga "el principio de la resolución es muy acorde a todos los compromisos adquiridos por Colombia a nivel nacional e internacional respecto a su ordenamiento ambiental", compromisos que obligan a Colombia a estar a la vanguardia en la clasificación de especies vegetales y animales.

Reyes señala que la lista presentada en la resolución no está actualizada, y por ejemplo, respecto a las plantas, las orquídeas (de orden Asparagales) se listan en un orden taxonómico que no corresponde (orden Orchidales). Un problema que puede decantar en obstáculos para  la conservación de una de las plantas más traficadas en el mundo (entre las que está la flor nacional del país), es decir **"están confundiendo peras con manzanas", y con ello se estaría permitiendo que se traficaran especies de orquídeas actualmente en alguna categoría de peligro", **según explica Reyes.

Asimismo sucede con algunos ejemplares de micos que no aparecen enlistados. Por ejemplo "las dos especies de micos nocturnos con los que experimenta Manuel Elkin Patarroyo, lo cual es muy cuestionable", señala.

### **Causas y propuestas** 

Catalina Reyes indica que el problema se da por fallos en los tecnicismos, que dan cuenta de la falta de apoyo a la investigación científica y a la academia. Pese a que diferentes universidades han adelantado estudios en esa materia, estos no son tenidos en cuenta a nivel gubernamental.

"El presidente Juan Manuel Santos asegura que la biodiversidad es uno de los activos de la paz, pero si no la conocemos, es poco lo que podemos hacer para conservarla y mucho lo que se puede estar poniendo en riesgo en **el país más biodiverso del mundo por kilómetro cuadrado**", expresa.

La experta espera que se logre entablar un diálogo con el Ministerio de Ambiente, y especialmente con la Subdirección de Bosques y Ecosistemas, para que desde la academia se de cuenta del problema y los riesgos que puede haber al clasificar mal las especies. "Debe haber una **conexión entre la academia y la instancias ambientales** para que haya una congruencia entre la investigación científica, lo administrativo y lo legislativo".

Por otra parte menciona que **debe haber recursos para exprimir el potencial investigativo de Colombia**, teniendo en cuenta que se trata de un país que tiene la tarea, de cara al mundo, de amortiguar los efectos del cambio climático.

<iframe id="doc_60343" class="scribd_iframe_embed" title="Resolucion 1912 de 2017" src="https://www.scribd.com/embeds/360272080/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-CEY8mFkRiFpejsgTGGhx&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6069986541049798"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

 
