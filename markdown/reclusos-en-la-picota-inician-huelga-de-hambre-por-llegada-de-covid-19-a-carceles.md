Title: Reclusos en La Picota inician huelga de hambre por llegada de Covid-19 a cárceles
Date: 2020-04-12 22:42
Author: AdminContagio
Category: Actualidad, Movilización
Tags: Covid-19
Slug: reclusos-en-la-picota-inician-huelga-de-hambre-por-llegada-de-covid-19-a-carceles
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Huelga-de-internos-en-La-Picota-por-Covid-19.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @CSPP\_ {#foto-cspp_ .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este domingo se conoció que el comandante de la guardia del Instituto Nacional Penitenciario y Carcelario (INPEC) en la cárcel distrital (en Bogotá) dió positivo para Covid-19, adicionalmente se supo que 13 guardianes más están en aislamiento y a la espera de los resultados del examen sobre el virus. Ante la situación, y tras presentarse dos muertes en la cárcel de Villavicencio por el Virus, internos en La Picota emprendieron una huelga de hambre.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Protestas en cárceles del país pidiend**o condiciones para enfrentar el Covid-19

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 20 de marzo se presentó una [protesta a nivel nacional](https://archivo.contagioradio.com/amotinamiento-dejo-23-muertos-en-la-carcel-la-modelo/) en diferentes cárceles, porque los internos exigían condiciones mínimas para enfrentar la pandemia del Covid-19 en los penales. La reacción de las autoridades culminó con, por lo menos, 23 personas asesinadas en la cárcel La Modelo de Bogotá y [múltiples denuncias](https://archivo.contagioradio.com/sin-desinfectantes-ni-tapabocas-poblacion-carcelaria-en-alto-riesgo-por-covid-19/) de abusos en otros centros de reclusión del país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las protestas continuaron, porque los internos denunciaron que aunque se habían restringido las visitas para ellos evitando el ingreso del virus con sus familiares, guardias y funcionarios de las cárceles seguían ingresando sin ningún control a los mismos, pudiendo generar riesgos. A ello se suma el hacinamiento carcelario, que -junto a otras razones- llevó a la declaración del Estado cosas inconstitucional en el sistema penitenciario y carcelario por parte de la Corte Constitucional. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante la situación, el movimiento carcelario solicitó que se expidiera un decreto de emergencia para lograr excarcelaciones de personas que no han sido juzgadas, así como la casa por cárcel para población en riesgo. De tal manera que se evitaran muertes, y se redujera el hacinamiento. (Le puede interesar: ["Ninguna Cárcel en Colombia está lista para afrontar Covid 19"](https://archivo.contagioradio.com/ninguna-carcel-en-colombia-esta-lista-para-afrontar-covid-19-porque/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

No obstante, la Fiscalía se interpuso a la medida que intentó el Ministerio de Justicia y desde la semana pasada se reportaron dos muertes relacionadas con el Covid-19 en la cárcel de Villavicencio, además de 13 personas hospitalizadas por síntomas respiratorios relacionados con la pandemia. (Le puede interesar: ["Muere otro interno de cárcel de Villavicencio, se estudia si está relacionado con Covid-19"](https://archivo.contagioradio.com/muere-otro-interno-de-carcel-de-villavicencio-se-estudia-si-esta-relacionado-con-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El Covid-19 llegó a las cárceles de Bogotá**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El secretario de seguridad del Distrito, Hugo Acero, confirmó que el comandante de guardia de la Cárcel Distrital dió positivo en el examen de Covid-19, asimismo, manifestó que 13 guardianes más están siendo valorados por sospechas de la enfermedad. Aunque Acero dijo que los reclusos no tuvieron contacto con el comandante, hay preocupación porque otras personas sí lo hayan tenido y aún no presentan síntomas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, y previendo el riesgo biológico (dado el hacinamiento de los penales), reclusos en la cárcel La Picota iniciaron una[huelga de hambre](http://www.comitedesolidaridad.com/es/content/huelga-de-hambre-en-c%C3%A1rceles-de-la-costa-caribe) indefinida ante la llegada del virus a las cárceles y la demora del Gobierno en promulgar un decreto que alivie la situación en los centros de reclusión.(Le puede interesar: ["Más de 80 reclusos de cárcel de Palogordo están en huelga de hambre"](https://archivo.contagioradio.com/mas-de-250-reclusos-de-carcel-de-palogordo-estan-en-huelga-de-hambre/))

<!-- /wp:paragraph -->

<!-- wp:html -->

> [\#AHORA](https://twitter.com/hashtag/AHORA?src=hash&ref_src=twsrc%5Etfw) ? Los privados de la libertad de la cárcel Picota, en la ciudad de Bogotá, anuncian huelga de hambre indefinida ante la llegada del Covid-19 a las cárceles, y la demora del Gobierno Nacional en promulgar decreto sobre prisión domiciliaria.  
>   
> 1/2 ⬇️ [pic.twitter.com/nTIaTHJaXI](https://t.co/nTIaTHJaXI)
>
> — Comité de Solidaridad (@CSPP\_) [April 12, 2020](https://twitter.com/CSPP_/status/1249480380111032323?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:block {"ref":78980} /-->
