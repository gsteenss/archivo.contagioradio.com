Title: Boletín informativo Julio 7
Date: 2015-07-07 17:44
Category: datos
Tags: Cambios en la cupula militar de Santos, Constituyente cultivadores coca y amapola, Intimidaciones y amenazas a lídere comunitarios de Cajamarca, Países Garantes piden estudiar desescalamiento, Visita del Papa a Ecuador
Slug: boletin-informativo-julio-7
Status: published

[*Noticias del Día:*]

<iframe src="http://www.ivoox.com/player_ek_4733886_2_1.html?data=lZyglZ2ceo6ZmKiakpuJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpzc7cjZyRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Entre el 4 y el 6 de julio, se realizó la Constituyente Nacional de cultivadores de coca, marihuana y amapola, cuyas conclusiones estuvieron enmarcadas en la necesidad de generar una nueva política desde los cultivadores, productores y consumidores. A partir de ese escenario, se llamó la atención del presidente Juan Manuel Santos, para que escuche las propuestas de política antidrogas que se plantean desde las diferentes comunidades, Marcela Muñoz, integrante de la Mesa Regional de las organizaciones sociales de Putumayo.

-El presidente Juan Manuel Santos, anunció este lunes varios cambios respecto a su cúpula militar. Para Alirio Uribe representante a la Cámara y defensor de derechos humanos, el cambio de Jaime Lasprilla, por Alberto José Mejía, ahora comandante del Ejército Nacional, es positivo para el proceso de paz. Sin embargo, mantener a Juan Pablo Rodríguez como comandante de las Fuerzas Militares, es negativo para las víctimas en la medida en que él está vinculado a casos de ejecuciones extrajudiciales.

-Este martes los países garantes y acompañantes del proceso de paz, Cuba, Noruega, Chile y Venezuela pidieron a las partes estudiar un "desescalamiento urgente" del conflicto armado que conlleve a un cese al fuego bilateral.

-Líderes comunitarios del Comité Ambiental y Campesino de Cajamarca y Anaime, denuncian que en las últimas semanas han sido víctimas de intimidaciones y amenazas mientras se adelantan acciones por parte de la comunidad en contra del proyecto minero La Colosa, de Anglogold Ashanti. Habla Camila Méndez, integrante del Comité Ambiental y Campesino de Cajamarca y Anaime.

-Durante la estadía del Papa Francisco en Ecuador,  país dividido entre el apoyo y la oposición a las decisiones del gobierno de Rafael Correa, el sumo Pontífice ha enviado mensajes que, de acuerdo con Isabelo Cortés periodísta de Contacto Sur en el vecino país, coinciden con posturas asumidas desde el gobierno en particular las relacionadas con la inequidad y desigualdad en la distribución de la riqueza.
