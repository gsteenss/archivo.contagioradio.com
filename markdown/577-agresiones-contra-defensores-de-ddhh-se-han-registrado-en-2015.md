Title: 577 agresiones contra defensores de DDHH durante 2015
Date: 2015-12-10 13:53
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, Día internacional de los DDHH, Diálogos de paz en Colombia, Programa Somos Defensores
Slug: 577-agresiones-contra-defensores-de-ddhh-se-han-registrado-en-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Somos-defensores.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Programa Somos Defensores 

##### <iframe src="http://www.ivoox.com/player_ek_9673422_2_1.html?data=mpuklZmWdo6ZmKiakp6Jd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiReZirjMbU1MrXrdDixtiYxdTSuNPVjMnSyMrSt9DmxtiYxsqPiKW8qZDR19fFstXZjJedk5qRb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Carlos Guevara, Somos Defensores] 

###### [10 Dic 2015 ]

[Carlos Guevara integrante del ‘Programa Somos Defensores' asegura que el **balance de la defensa de los DDHH durante este año resulta agridulce en varios sentidos**, de un lado persisten las agresiones contra defensores y defensoras de DDHH, se han registrado “**más 800 amenazas y 400 homicidios en los últimos seis años**… nos sigue preocupando que la Fiscalía no da resultados, de este número de casos mencionados no tenemos resolución concreta de personas condenadas ni juzgadas ni siquiera en un solo caso, eso significa que **podemos estar hablando de una impunidad casi del 100%**”.]

[Pese a que no hay unanimidad en las cifras de agresiones contra defensores de DDHH, el ‘Programa’ registró **entre enero y septiembre de este año un total de 577 ataques**, clasificados en 465 amenazas, 51 asesinatos, 28 atentados, 19 detenciones arbitrarias, 1 caso de desaparición forzada, 6 judicializaciones y 7 robos de información.]

[De acuerdo con Guevara, esta situación se agudiza con los **índices de corrupción y perdida de dinero que aquejan a las instituciones encargadas de la protección de defensores**, como es el caso de la Unidad Nacional de Protección UNP “este año la Contraloría señaló en un informe detallado que hay un **detrimento patrimonial de \$15 mil millones, hay un déficit por encima de los \$40 mil millones** y hay un montón de problemas legales por muchos de los contratos que se firmaron en la administración pasada”.]

[En términos positivos Guevara destaca el aumento en la cantidad de defensores de DDHH que se han venido articulando en torno a la construcción de la paz “aunque sea un poco invisible su trabajo, muy de hormigas allá en los territorios, **cada vez  mas movimientos sociales desde distintas aristas se están sumando a esta lucha por apropiarse del tema de la paz e impulsarlo en sus territorios**, siendo caja de resonancia de las cosas positivas que se van logrando en La Habana y de comenzar a pensarse cómo va a ser este país en un escenario de paz"]

[“Sin lugar a dudas creemos que **el próximo año va a ser el más importante para los defensores de derechos humanos** de este país, porque durante muchísimos años estos líderes sociales se han enfrentado a la guerra y **están por primera vez frente a una posibilidad real del cese de las hostilidades**”, afirma Guevara e insiste en que son ellos quienes en la aplicación de la paz serán fundamentales pues gracias a su contacto en las comunidades verificaran el cese al fuego bilateral y las acciones que conlleven la aplicación de los acuerdos en un eventual escenario de posconflicto.]
