Title: Marzo: aislamiento, control paramilitar y cese al fuego del ELN
Date: 2020-12-28 20:48
Author: CtgAdm
Category: Actualidad, Especiales Contagio Radio, Nacional, Resumen del año
Tags: Aislamiento, Derechos Humanos, lideres sociales, pandemia
Slug: marzo-aislamiento-reforzamiento-del-control-por-parte-de-armados-y-cese-al-fuego-del-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-8.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Uno de los asuntos que más marcó la vida del mundo en este mes de Marzo fue la **rápida llegada de la pandemia del COVID 19**, con ella llegó también, en algunos casos, la solidaridad de muchas personas con sus amigos y vecinos. Sin embargo al mismo tiempo los problemas en la ruralidad se agudizaron y se evidenciaron. En el departamento de Córdoba varios líderes fueron víctimas de discriminación pues supuestamente el COVID los había alcanzado. Esta preocupación creció con los ataques directos y la estigmatización generalizada para personas en riesgo de contagio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De otro lado, muchas comunidades denunciaron que el aislamiento había sido una oportunidad para que estructuras armadas como las **AGC y otras reforzaran el control poblacional**. A este control se sumó la dependencia dado que cuando se esperaban acciones concretas por parte del gobierno para favorecer a los más necesitados Duque prefirió salvar al sistema financiero entregando dineros estatales sin voltear a mirar a campesinos y campesinas que comenzaban a sentir el rigor del aislamiento y el olvido estatal.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sin embargo, en medio de tanta adversidad y ante los llamados de comunidades en todo el país y del mismo Concejo de Seguridad de las Naciones Unidas, el Ejército de Liberación Nacional decretó un cese al fuego unilateral que alivió los dolores de cientos y cientos de personas en diversos lugares del territorio nacional que tenían que enfrentar la guerra y los problemas de salud que significó la llegada del COVID al país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último la crisis llegó hasta el sistema carcelario, **por lo menos 23 personas asesinadas y 83 heridas, la mayoría con armas de largo alcance utilizadas por la guardia del INPEC**, en una jornada de protestas en la cárcel modelo de Bogotá. Esta jornada que cubrió por lo menos 10 centros penitenciarios del país, evidenció la incapacidad del gobierno Duque para atender los llamados de las personas más necesitadas en el país.

<!-- /wp:paragraph -->

<!-- wp:heading -->

En Córdoba ya no solo los persiguen por ser líderes sociales, también por tener síntomas de Covid-19
----------------------------------------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La lideresa Yina Paola Sanchez Rodríguez del municipio de Montelibano Córdoba denuncia que desde el 24 de marzo y debido a una mala asesoría médica en el centro de atención el corregimiento de Tierradentro,** fue señalada erroneamente con Covid-19 sin que se le hicieran las pruebas adecuadas**, pese a ello, el anunció causó conmoción y ha generado una serie de estigmatizaciones, incluso hasta ser considerada blanco de los grupos armados de la zona.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde la Policía aseguraron que hasta que no se hicieran los exámenes que determinan si se es positivo, no podrían realizar una evacuación, ante dicho rechazo, relata la lideresa, este sector de la población acudió a un grupo armado, presuntamente el Clan del Golfo para amenazar a la defensora de DD.HH. y expulsarla del territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Ver más: [En Córdoba ya no solo los persiguen por ser líderes sociales, también por tener síntomas de Covid-19 - Contagio Radio](https://archivo.contagioradio.com/cordoba-persiguen-lideres-sociales-covid-19/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Control de AGC se consolida en cuarentena en Bajo Cauca Antioqueño
------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este 31 de marzo, organizaciones sociales denunciaron que en días pasados, vía mensajes de WhatsApp, las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), también llamados por el Gobierno como Clan del Golfo, decretaron una completa restricción a la movilidad en el municipio de Tarazá, subregión del Bajo Cauca Antioqueño, en lo que algunos califican como una muestra de poder territorial.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque Zapata sostuvo que hay una consolidación de la Fuerza Pública en la subregión, añadió que Tarazá está bajo el control de los ilegales al punto que las comunidades han acatado el mensaje de las AGC por temor a las amenazas. El defensor de DD.HH. dijo que por efectos del Covid-19 han visto una «reducción en los fenómenos de violencia, pero no en las formas de control», y ello se evidencia en el toque de queda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Ver más: [Control de AGC se consolida en cuarentena en Bajo Cauca Antioqueño - Contagio Radio](https://archivo.contagioradio.com/control-de-agc-se-consolida-en-cuarentena-en-bajo-cauca-antioqueno/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

Cese unilateral al fuego del ELN, una oportunidad para la paz
-------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->

*El 30 de marzo se anunció mediante un comunicado, el Ejército de Liberación Nacional (ELN) el cese al fuego unilateral durante el mes de abril.*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Considerando que el país atraviesa un momento difícil mientras enfrenta al Covid-19. Para defensores de derechos humanos, el cese es otra muestra de intenciones de paz por parte de la guerrilla, que podría conducir a un restablecimiento de la mesa de conversaciones con el Gobierno.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El movimiento [Defendamos La Paz](https://twitter.com/DefendamosPaz/status/1244605984301088768) saludó el gesto del ELN, señalando que era una respuesta a los pedidos de comunidades rurales del país así como al llamado hecho desde la [ONU](https://twitter.com/ONUHumanRights/status/1244716665553723396). En el comunicado se enfatiza que este acto significa un alivio para poblaciones que están afectadas por la violencia y, puede ser el camino para pensar nuevamente en la salida negociada al conflicto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Ver más: [Cese unilateral al fuego del ELN, una oportunidad para la paz - Contagio Radio](https://archivo.contagioradio.com/cese-unilateral-al-fuego-del-eln-una-oportunidad-para-la-paz/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

El 21 de marzo un amotinamiento dejó 23 muertos en cárcel La Modelo
-------------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

Luego de más de 10 horas la Ministra de Justicia Margarita Cabello, informó sobre los hechos sucedidos en la cárcel La Modelo que dejaron un saldo de 23 muertos y 83 heridos, cifra que según organizaciones podría ser superior. La situación fue calificada como una fuga masiva, sin embargo, la protesta se había convocado días antes por los detenidos tras las ausencia de medidas que los proteja de contraer el virus Covid-19.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las declaraciones, la Ministra aclaró que no hubo ninguna fuga y que 32 de los heridos se encuentran en hospitales y centro de atención, igualmente informó de siete funcionarios del Inpec heridos, dos de alta gravedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Ver más: [Amotinamiento dejó 23 muertos en cárcel La Modelo - Contagio Radio](https://archivo.contagioradio.com/amotinamiento-dejo-23-muertos-en-la-carcel-la-modelo/))

<!-- /wp:paragraph -->
