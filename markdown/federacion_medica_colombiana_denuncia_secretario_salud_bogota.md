Title: Federación Médica Colombiana denuncia al Secretario de Salud de Bogotá
Date: 2017-07-04 17:25
Category: DDHH, Nacional
Tags: Bogotá, Salud, secretario de salud
Slug: federacion_medica_colombiana_denuncia_secretario_salud_bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/luis_gonzalo_salud-e1499206838484.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: You Tube 

###### [4 Jul 2017] 

Luis Gonzalo Morales, Secretario de Salud de Bogotá, **fue denunciado por la Federación Médica Colombiana, por retener recursos destinados al Tribunal de Ética Médica Bogotá.** Así lo anunció mediante un comunicado de prensa la Federación, asegurando que dicha situación tiene en riesgo de cierre la corporación de salud más importante del país.

De acuerdo con el comunicado, el secretario, quien es el encargado de entregar los recursos de funcionamiento **ha incumplido en reiteradas ocasiones esa gestión**. En esa medida se señala que "Cuando un ciudadano con título de médico tome decisiones de tipo clínico o administrativo que lesionen en alguna forma la integridad de algún paciente, médico o institución de salud, es sujeto disciplinable por el sistema de control ético  de los tribunales de todo el país".

Asimismo la Federación manifiesta que **los procesos internos de la entidad se han visto demorados "de manera injustificada y varios de ellos continuarán hacia la prescripción por ineficiencia administrativa".** De hecho ne el comunicado se cita varios de los artículo del Código Penal, Contencioso Administrativo y Disciplinario Único que habría violado Luis Gonzalo.

### La respuesta del Secretario 

Ante dicha denuncia puesta el 29 de junio, el primero de julio respondió en twitter Morales, asegurando que se trataba de una "Noticia falsa. Si tiene pruebas lo invito las muestre. Lamentable acusar y condenar sin tener sustento, averigüe primero!". Además el secretario expresó, **"Imposible retener recursos cuando no se ha vencido el plazo para girarlos, a pesar de eso ya se giraron. Se les fue la mano x imprudentes!".**

Por su parte, aunque desde la Federación se ha reiterado en que no es la primera vez que existen esas demoras, la Asociación Colombiana de Sociedad Científicas dijo mediante un comunicado que "tras averiguaciones" se estableció que la suspensión de giro de recursos se trató de **"una situación transitoria que obedece a la revisión de algunos hallazgos administrativos por parte de la Contraloría Distrital como lo establece la normativa",** no obstante indican que aún siguen en investigaciones.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
