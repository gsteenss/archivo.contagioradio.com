Title: Uriel Rodríguez: Primer líder asesinado en Gobierno Duque
Date: 2018-08-08 14:43
Category: DDHH, Nacional
Tags: Cauca, Lider social, PUPSOC, Uriel Rodríguez
Slug: primer-lider-asesinado-gobierno-duque
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Uriel-RodrígueZ.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [8 Ago 2018] 

El 7 de agosto, en horas de la noche, fue asesinado Uriel Rodríguez, líder del proceso de sustitución de cultivos de uso ilícito en Cajibio, Cauca. Según miembros de la comunidad, Rodríguez se encontraba en una tienda de la vereda Altamira, al occidente del municipio, cuando hombres armados le propinaron varios disparos.

De acuerdo con Deivin Hurtado, integrante de la Comisión de Derechos Humanos de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana **(COCCAM)** y de la Federación Sindical Unitaria Agropecuaria **(FENSUAGRO)**; Rodríguez, quien era integrante de esas organizaciones, trabajaba en el tema de sustitución de cultivos de uso ilícito y era miembro del Proceso de Unidad Popular del Suroccidente Colombiano **(PUPSOC)**.

### **La comunidad ha denunciado la presencia de las AGC en Cajibio** 

Conforme a la información de la comunidad en la zona, miembros de las **autodenominadas Autodefensas Gaitanistas de Colombia (AGC)** hacen presencia en el territorio, e incluso el pasado 20 de julio, hicieron grafitis alusivos a ese grupo armado ilegal. Por su parte, el Ejército ha señalado que en Cajibio operan disidencias de las FARC, **hecho que no ha sido corroborado por la comunidad.**

Aunque la situación respecto al caso de Uriel Rodríguez es materia de investigación, Hurtado afirma que en la zona, las AGC estaban amenazando líderes, afirmando que harían control territorial, obligando a pagar vacunas y amenazando de muerte a quienes no pagaran estas extorsiones.

De otra parte, las razones de su asesinato también podría tener que ver con su liderazgo en el programa de sustitución voluntaria en el marco de los acuerdos de paz. Este asesinato se suma a los otros 3 que Contagio Radio ha reportado en julio, de integrantes del PUPSOC. (Le puede interesar: ["Bajo constante amenaza viven líderes sociales en Cauca"](https://archivo.contagioradio.com/bajo-amenaza-viven-lideres-en-cauca/))

<iframe id="audio_27703878" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27703878_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
