Title: Entrevista Yesid Conda, Líder indígena sobre procedimiento militar irregular en su contra
Date: 2020-01-22 17:02
Author: CtgAdm
Category: Entrevistas
Tags: entrevista
Slug: entrevista-yesid-conda-lider-indigena-sobre-procedimiento-militar-irregular-en-su-contra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/jugar.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:audio -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/yesid-conda-lider-indigena-sobre-procedimiento-militar-irregular_md_46802814_wp_1.mp3&quot;]" preload="auto">
</audio>
</figure>
<!-- /wp:audio -->
