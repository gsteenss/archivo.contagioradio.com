Title: México: "Detención de exalcalde de Iguala y su esposa es una cortina de humo"
Date: 2015-11-11 14:17
Category: El mundo
Tags: 43 desaparecidos, Ayotzinapa, Caso ayotzinapa, CIDH, Detención alcalde iguala, Forenses argentino, guerrero, Iguaña, Jose Luis Abarca, Omar García, Revolución 3.0
Slug: detencion-de-exalcalde-de-iguala-y-su-esposa-es-una-cortina-de-humo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/ayotzinapa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Diego Simón 

<iframe src="http://www.ivoox.com/player_ek_9355719_2_1.html?data=mpiil5yVfY6ZmKiak5eJd6KkkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkYa3lIqum93Np9CZlKaYh5eWiMboxtPQy4qnd4a2lNOYxsqPqdnVzcjOzsnJb8XZjK7U18bQpYztjNjijcrXtNDnwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alina Duarte, Revolución 3.0] 

###### [11 nov 2015] 

En el caso Ayotzinapa van **más de 100 detenciones**, la última fue la ordenada por un juez federal mexicano quien **imputó nuevos cargos al exalcalde de Iguala José Luis Abarca y su esposa María de los Ángeles Pineda** por delitos de delincuencia organizada, enriquecimiento ilícito y lavado de dinero.

Organizaciones sociales han indicado que estas detenciones se tratan “**no de autoridades importantes, sino de cargos menores**” que se han visto inmiscuidos en la desaparición de los 43 normalistas, a Abarca se le condena por cargos que tenía desde hace tiempo, pero que no se habían logrado consumar, además de estos procesos penales es acusado de ser autor intelectual del ataque contra los normalistas.

Con esta detención los padres de familia de los desaparecidos y los normalistas sobrevivientes indicaron que el día que se detuvieron a José Luis Abarca y su esposa, se trataba de hacer una **especie de cortina de humo, es por eso que los familiares de los normalistas e**xigen más líneas de investigación porque para ellos, el caso se ha “minimizado” con estas pequeñas detenciones.

La presión social que se ha hecho desde entes internacionales como a la CIDH “ha rendido bastantes frutos”, indica Alina Duarte del medio mexicano, Revolución 3.0, quien agrega que eso  ha permitido que se abran nuevas líneas de investigación, por ejemplo contra el Ejército mexicano por su participación y omisión en los hechos que rodearon la desaparición de los jóvenes.

Por otra parte, Omar García estudiante y vocero de la jornal de Ayotzinapa **denunció una campaña de desprestigio contra ellos,** por su formación política en estas escuelas que los forman como líderes sociales y como maestros que buscan cargos en el magisterio, de esta forma en los medios de comunicación incluso “se ha tratado de justificar la desaparición” de los normalistas.
