Title: Defensores del humedal La Conejera resistieron desalojo del ESMAD
Date: 2015-04-15 15:41
Author: CtgAdm
Category: Ambiente, Galerias, Otra Mirada
Tags: ambientalistas, Ambiente, ESMAD, Fontanar, Humedal La Conejera, Humedales, Suba
Slug: esmad-intenta-desalojar-forzosamente-a-defensores-del-humedal-la-conejera
Status: published

##### Foto: Twitter 

<iframe src="http://www.ivoox.com/player_ek_4359172_2_1.html?data=lZiim5abdo6ZmKiakpyJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTBoqmYy9PYqc%2FowpDRx9jFsNDewteYyNTWvtDnwtLS0NnJb8KfxcrTx9PXs9PZ1JDRx9GPrI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Andrés, defensor del humedal La Conejera] 

##### <iframe src="http://www.ivoox.com/player_ek_4365240_2_1.html?data=lZijl5eYdI6ZmKiak5mJd6KompKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbaxtPg0dfJt4zYxpDZw5DHs8%2FZy8rfw5DWqdTd1NnS0JDIqdTVzdTX0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Gina Díaz , defensora del humedal La Conejera] 

Con **gases y golpes**, más de **150 agentes del ESMAD intentaron desalojar forzosamente a los defensores del humedal la Conejera,** que desde octubre mantienen un campamento en esa zona, con el objetivo de evitar la construcción del proyecto Fontanar del Río, etapa 8, adelantado por la constructora Fontanar del Río.

De acuerdo a Andrés, una de las personas defensoras del humedal, la policía había dicho que visitaría el lugar este miércoles en horas de la mañana, con el fin de realizar una visita ocular, revisar qué actividades había en el campamento y si se realizaba alguna actividad ilegal. Sin embargo, “lo que hicieron fue llegar  a desalojar el campamento, **golpearon a varias mujeres,  detuvieron a tres jóvenes** que están en la estación de suba y los quieren judicializar por invasión al espacio público”, denuncia Andrés.

Pese a que desde octubre, los defensores y defensoras del humedal, venían adelantando este plantón de forma pacífica, **la comunidad venía denunciando que la fuerza pública está hostigando a los ambientalistas,** ya que según las autoridades, el campamento que los manifestantes tienen frente a la construcción,  genera daños a la naturaleza, “pero en cambio una construcción no…”, expresa Gina Díaz, defensora del humedal.

Así mismo, Andrés afirma que este martes 14 de abril, se desarrolló la primera audiencia en el Tribunal de Cundinamarca, que es la última instancia que decidirá el futuro del humedal. Durante el encuentro, los ambientalistas habían sido amenazados, diciéndoles que iban a recibir una sorpresa, “y nos querían era desalojar”, añade el ambientalista.

Cabe recordar, que la comunidad acompañada del personero interpusieron esa acción popular para defender el humedal y dar a conocer las irregularidades que hay en **la licencia de construcción y la licencia de urbanismo [(Ver nota relacionada)](https://archivo.contagioradio.com/fallo-de-minambiente-sobre-la-conejera-no-es-definitivo-ambientalistas/).**

Al momento, los defensores y defensoras de La Conejera, **piden apoyo de la ciudadanía en el campamento,  **para evitar que otros hechos violentos se presenten. Así mismo piden la presencia de las personas en el campamento, ya que dicen darles miedo irse y que se empiecen a construir ilegalmente y luego no se pueda hacer nada.

Andrés, asegura que si el tribunal llega a concluir que la constructora puede realizar su proyecto de vivienda, **la comunidad planea consolidar un comité de veeduría ciudadana**, para denunciar el desarrollo de la obra.

\[KGVID width="640" height="360"\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/VID-20150416-WA00101.mp4\[/KGVID\]

\[KGVID width="640" height="360"\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/VID-20150416-WA00111.mp4\[/KGVID\]

\[KGVID width="480" height="270"\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/04/VID-20150416-WA00121.mp4\[/KGVID\]
