Title: Sanciones de EEUU contra Venezuela son revancha por reducción de su cuerpo diplomático
Date: 2015-03-10 17:29
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Emergencia Nacional, Estados Unidos y Venezuela, gobierno Obama, injerencia de EE.UU
Slug: sanciones-de-eeuu-contra-venezuela-son-revancha-por-reduccion-de-su-cuerpo-diplomatico
Status: published

##### Foto:Elhorizonte.mx 

##### [**Entrevista con [Alex Díaz]:**] 

<iframe src="http://www.ivoox.com/player_ek_4194818_2_1.html?data=lZamlp2VfI6ZmKiak5iJd6KklZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiabJtpDRx8jQpdPVjMaYuMrSqdvpxtHOjcbRqc%2FV28aY0sbWpYzn1pDgx8zZtsrYwsmah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Obama declara a Venezuela como **“...amenaza inusual y extraordinaria a la seguridad nacional y política exterior...”** por motivos referidos a la **violación de los DDHH**. También impone **sanciones contra 7 miembros del gobierno** , entre ellos, cinco militares y una fiscal venezolana.

El presidente de **EEUU declara "Emergencia nacional"** la situación con el gobierno de Venezuela, dicha medida permite dictar sanciones sin previa aprobación del congreso contra países terceros.

Según **Alex Díaz**, analista político y profesor de la Universidad Bolivariana, la declaración **responde a las medidas que tomó Venezuela en contra de la diplomacia estadounidense**.

Como por ejemplo, hacer equivalente el numero de funcionarios de EEUU en Venezuela, con el que tiene Venezuela en EEUU, prohibir a los funcionarios extraditados hacer reuniones políticas sin previo aviso al gobierno, o la prohibición de la entrada de siete funcionarios de EEUU.

Para el gobierno venezolano la medida responde a una **injerencia del gobierno de EEUU**, en forma de amenaza aprovechando la crisis económica debida a los precios del petróleo, y a que la oposición no ha conseguido desestabilizar al gobierno, ni ha conseguido ganar unas elecciones .

El gobierno de Obama ha dictado las sentencias con el objetivo de bloquear los bienes económicos que tienen miembros del gobierno bolivariano en EEUU.

Alex Díaz afirma que **los sancionados no tienen bienes en el país, ni siquiera visas**, y estas sanciones son contra responsables de la seguridad nacional como por ejemplo la **fiscal que lleva todas las ordenes judiciales en el marco de la crisis de las Guarimbas.**

Solamente en 2014 el gobierno de EEUU hizo más de 100 declaraciones y en 2015 lleva ya 60 contra el gobierno de Venezuela. Acusándolo de ser un estado donde se violan los derechos humanos.

Ante el incremento de las hostilidades y las consecuencias para la política Latinoamericana, el presidente **Nicolás Maduro ha recibido solidaridad desde Cuba y Ecuador y Bolivia** así como han sido convocadas marchas en distintas regiones del mundo.

El presidente de la república venezolana ha descrito las medidas como grave injerencia e intento de **desestabilización**, ya que Venezuela respeta el orden constitucional estadounidense, y no tiene la pretensión de atacar al país vecino.
