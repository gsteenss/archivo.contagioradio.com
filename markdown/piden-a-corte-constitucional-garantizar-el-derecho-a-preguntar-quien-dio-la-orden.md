Title: Piden a Corte Constitucional garantizar el derecho a preguntar ¿Quién dio la orden?
Date: 2020-09-20 19:52
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Ejército Nacional, MOVICE
Slug: piden-a-corte-constitucional-garantizar-el-derecho-a-preguntar-quien-dio-la-orden
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Victimas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Movimiento Nacional de Víctimas de Crímenes de Estado

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El **[Movimiento Nacional de Víctimas de Crímenes de Estado](https://twitter.com/Movicecol) (Movice)** y la **Campaña por la Verdad,** han solicitado a la **Corte Constitucional revisar la tutela interpuesta por** el brigadier general Marcos Evangelista Pinto Lizarazo en contra de la imagen **del Mural ¿Quién dio la orden?** y el fallo que ordena, esta sea eliminada. Las víctimas señalan que la acción interpuesta por el brigadier, **pretende censurar la libertad de expresión de las víctimas, por lo que han solicitado** sean escuchados.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el 18 de octubre de 2019, en el marco de esta campaña, artistas se encontraban realizando el señalado mural en Bogotá, reproduciendo la imagen de cinco altos mandos del Ejército Nacional vinculados a las ejecuciones extrajudiciales. Fue durante su elaboración que la Fuerza Pública **dispuso un fuerte operativo para detener su elaboración intimidando a los artistas, y organizaciones presentes, tras ser retirados, el Ejército borró el mural en su totalidad.** [(Le puede interesar: Mural ¿quién dió la órden? le pertenece a la gente: MOVICE)](https://archivo.contagioradio.com/mural-quien-dio-la-orden-le-pertenece-a-la-gente-movice/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque rápidamente la imagen se viralizó en redes sociales y otros espacios como muestra de acción masiva e indignación colectiva, la censura continúo a través de una tutela interpuesta por el brigadier general Marcos Evangelista Pinto y el general (R) Mario Montoya contra el Movice, al sostener que se le acusaba "con una intencionalidad dañina y premeditada para afectar su honra y buen nombre de manera injustificada". [(Le recomendamos leer: Con tutelas, altos mandos militares quieren censurar mural sobre 'falsos positivos'](https://archivo.contagioradio.com/con-demandas-altos-mandos-militares-quieren-censurar-mural-sobre-falsos-positivos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 13 de noviembre dichas tutelas fueron declaradas improcedentes, sin embargo, el Juzgado 13 Civil del Circuito de Bogotá ordenó que en un término de 48 horas las imágenes debían ser eliminadas de murales, redes sociales, medios de comunicación hablados y escritos, además de abstenerse en el futuro de reproducir la imagen o similares.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde la **Campaña por la Verdad** señalan que a través de la **movilización social** han buscado generar conciencia y dar a conocer a la ciudadanía quienes fueron los máximos responsables de las 5.763 ejecuciones extrajudiciales ocurridas en Colombia entre 2.000 y 2.010 por el Ejército Nacional. Resaltan además, que dicha información ha sido recopilada en expedientes judiciales, testimonios de víctimas e informes que han sido entregadas al **Sistema Integral de Verdad, Justicia, Reparación y Garantías de no Repetición. **

<!-- /wp:paragraph -->

<!-- wp:quote -->

> Rechazamos cualquier tipo de censura y pedimos a la Corte Constitucional que se respete el derecho que tienen las víctimas de exigir verdad y preguntar: ¿Quién dio la orden?
>
> <cite>MOVICE</cite>

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Ante esta decisión las más de 200 organizaciones de víctimas de desaparición forzada, ejecuciones extrajudiciales, asesinatos selectivos y desplazamiento forzado y organizaciones sociales señalan que con esta decisión se **"desconoce flagrantemente los derechos de las víctimas y de la sociedad en general a la verdad, la libertad de expresión y la memoria".**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
