Title: Con armas robaron la sede de la Juventud Rebelde
Date: 2020-02-13 18:51
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Cristian Hurtado, juventud rebelde, Memoria Viva
Slug: con-armas-robaron-la-sede-de-la-juventud-rebelde
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Juventud-Rebelde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Juventud Rebelde Colombia {#foto-juventud-rebelde-colombia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/maria-jose-villota-secretaria-nacional-dd-hh-de_md_47745482_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Maria José Villota | secretaria nacional de DD.HH. (Juventud Rebelde)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph -->

El miércoles 12 de febrero se presentó un nuevo hecho violento contra la Juventud Rebelde (JR), hombres armados entraron a su sede en Bogotá y robaron computadores, celulares y documentos personales. Por esta acción también se vieron afectados la Federación Colombiana de Estudiantes (FEU) y el sindicato de la UNP Memoria Viva, que comparten su sede con la JR.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JuvREBELDE/status/1227923441749196800","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JuvREBELDE/status/1227923441749196800

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **Los hombres se llevaron la libreta y computador del Secretario Político Nacional**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Maria José Villota, secretaria nacional de DD.HH. de la JR, los hechos ocurrieron cerca de las 6:30 p.m. en la sede nacional del movimiento, ubicado en Bogotá. Los hombres armados entraron con casco de motos y pasamontañas, encerrando a las 7 personas que se encontraban en el lugar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los violentos robaron teléfonos celulares, y el computador portátil así como documentos personales del secretario político nacional de la JR, Cristian Delgado. (Le puede interesar: ["Hurtan información sensible a integrantes de la Juventud Rebelde en Nariño"](https://archivo.contagioradio.com/hurtan-informacion-sensible-a-integrantes-de-la-juventud-rebelde-en-narino/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La cadena de agresiones contra la Juventud Rebelde**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Villota sostuvo que este es un nuevo pico en la cadena de agresiones contra la organización a nivel nacional, e incluye hechos como la agresión a Hurtado en febrero de 2019 y el asesinato de dos integrantes más de la JR en Cauca. (Le puede interesar: ["Por el derecho a ser joven, nace Juventud Rebelde Colombia"](https://archivo.contagioradio.com/por-el-derecho-a-ser-joven-nace-juventud-rebelde-colombia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/JuvREBELDE/status/1206424442492116992","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/JuvREBELDE/status/1206424442492116992

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La Secretaria de DD.HH. del movimiento político señaló que las personas que estaban en la sede instauraron las debidas denuncias ante las autoridades, esperando que se tomen acciones de investigación por parte de las mismas. Adicionalmente, pidió que se brinden [garantías de seguridad](https://www.colectivodeabogados.org/?_Marcha-Patriotica,1180_) colectiva para quienes están en el ejercicio de liderar y hacer parte de procesos organizativos como la FEU, JR y Memoria Viva. 

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
