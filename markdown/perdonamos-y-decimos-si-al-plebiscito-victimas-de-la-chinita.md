Title: "Perdonamos y decimos sí al plebiscito": víctimas de La Chinita
Date: 2016-09-30 20:41
Category: Nicole
Tags: Acuerdos de paz en Colombia, Delegación de paz de FARC, plebiscito por la paz, Si al plebiscito por la paz
Slug: perdonamos-y-decimos-si-al-plebiscito-victimas-de-la-chinita
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Acto-en-La-Chinita.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [30 Sep 2016 ] 

"No es fácil olvidar esa noche", asegura Heiler Mosquera, familiar de una de las 35 personas que fueron asesinadas por miembros del quinto frente de las FARC-EP la noche del 24 de enero de 1994, y quien hoy 22 años después ha decidido **escuchar a los delegados de esta guerrilla para abrir su corazón y perdonarlos**, en el acto público de reconocimiento de responsabilidad que se llevó a cabo este viernes en el barrio La Chinita del municipio de Apartadó.

Para Eliécer Arteaga, alcalde del municipio, este acto prueba que el proceso de paz va enserio, que **hay compromiso por parte de las FARC-EP de pedir perdón y contar la verdad a todas las víctimas** de una "guerra maldita" que está llegando a su fin para dar inicio a "la construcción de un país distinto, con mejores oportunidades para nuestros hijos" y en el que quienes dejen las armas puedan ser acogidos en comunidades como las del Urabá que pusieron una alta cuota en el conflicto armado y ahora buscan contribuir a la construcción de paz.

Para víctimas como Stella Florez es necesario que todos los responsables, pidan perdón por las **17 masacres que fueron perpetradas en esta región**, crímenes que dejaron viudas a un centenar de mujeres y huerfanas a niñas como Diana Hurtado, quien hoy recuerda los atardeceres en los que esperaba que su padre llegara a casa o los días en los que en vez de jugar como cualquier niña de su edad, debía trabajar para ayudar a sus hermanos, pero que hoy después de tanto tiempo siente que puede vivir más tranquila, porque está escuchando a los culpables y tramitando su duelo.

Stella insiste en que todas las víctimas están dispuestas a aportar en la consolidación de la paz y que como primer paso van a ofrendar su voto por el sí en el plebiscito, para que ninguna víctima quede en el olvido, para que cese la guerra y para que sus proyectos de vida puedan fortalecerse con procesos educativos, económicos y sociales. Para ella, **quienes están promoviendo el no en el plebiscito, son aquellos que no vivieron en carne propia la crueldad del conflicto armado**, una guerra que pese a su contundencia no logró destruirles los sueños ni arrebatarles la esperanza.

"Con el más profundo sentimiento de humanidad y respeto, 22 años después de aquel triste 23 de enero, pedimos perdón, jamás debió ocurrir la masacre, pero acá estamos para responder como organización porque queremos restablecer las relaciones que la violencia nos dañó", aseguró Iván Márquez, delegado de paz de las FARC-EP, quien insistió en que estos actos deben servir para llenar de valor a **todos los actores armados, políticos y empresariales que participaron en el conflicto para que se reconozcan sus errores** y hablen con la verdad.

El delegado agregó que la guerrilla de las FARC-EP está dispuesta a reconocer sus errores y a hablar con la verdad para contribuir a la construcción de paz y a la reconciliación que promoverán por todo el territorio nacional, para hacer que renazca la esperanza basada en la justicia social y las transformaciones que requiere la nueva apuesta de país. Márquez aseguró también que **la alcaldía de Apartadó ya adjudicó el terreno en el que será construida la universidad**, que fue acordada como parte de la reparación para la población de La Chinita, además de una casa de la memoria y un centro de acopio.

Para las [[víctimas de esta masacre](https://archivo.contagioradio.com/las-victimas-de-la-masacre-de-la-chinita-si-perdonamos/)], la desatención estatal fue la que permitió que hechos como estos ocurrieran, por lo que esperan que en el posconflicto haya voluntad real del Estado para desmontar el paramilitarismo que aún pervive en la región, así como las malas condiciones laborales que deben enfrentar trabajadores de [[multinacionales como la Chiquita Brands](https://archivo.contagioradio.com/empresas-serian-investigadas-por-financiacion-de-la-guerra-en-el-tribunal-especial-de-paz/)] porque como aseguró otro de los familiares "la comunidad europea tendrá que saber que **el banano que consume está manchado de sangre** y es producto de un trabajador que pasa hambre".

https://www.youtube.com/watch?v=oY7377eoOMg

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
