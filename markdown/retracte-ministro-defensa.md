Title: ¡Que se retracte el Ministro de Defensa!
Date: 2018-09-14 15:35
Author: AdminContagio
Category: DDHH, Nacional
Tags: Defensoría del Pueblo, Derechos Humanos, Guillermo Botero, Min Defensa, protesta
Slug: retracte-ministro-defensa
Status: published

###### [Foto: @mindefensa] 

###### [14 Sep 2018] 

<iframe src="https://co.ivoox.com/es/player_ek_28587305_2_1.html?data=k52impyXdJahhpywj5WbaZS1kpWah5yncZOhhpywj5WRaZi3jpWah5yncaLgytfW0ZC5tsrWxoqfpZDXs8PmxpDZw9iPqMbXzcbfw8jNs8_Z1JDRx9GPkcrij5Cxx8vJstTVjoqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

En un acto público, **el Ministro de Defensa Guillermo Botero**, afirmó que la protesta social se financia con dineros provenientes de grupos criminales; aseveración que para defensores de derechos humanos es delicada, en un contexto en el que se presenta un incremento en el asesinato de líderes sociales, y judicialización de los mismos.

> El ejercicio de la protesta social es legítimo y necesario en una democracia. Vincularla con el accionar de los grupos armados ilegales arriesga a los protestantes y viola el principio de distinción del derecho internacional humanitario. [pic.twitter.com/Uqjjxc74YA](https://t.co/Uqjjxc74YA)
>
> — Defensoría delPueblo (@DefensoriaCol) [14 de septiembre de 2018](https://twitter.com/DefensoriaCol/status/1040428062746263552?ref_src=twsrc%5Etfw)

### **"Con los dineros ilícitos corrompen, y financian la protesta social"** 

Durante su intervención en el Congreso de Confecamarás en Cartagena, Botero afirmó que "**con  los dineros ilícitos corrompen, y financian la protesta social.  Entonces, cada vez que ustedes ven que cerraron la Panamericana o ayer que cerraron carreteras en Nariño**, detrás de eso siempre están mafias de verdad, mafias supranacionales". (Le puede interesar: ["21 líderes sociales han sido asesinados en lo corrido del gobierno Duque"](https://archivo.contagioradio.com/durante-duque-asesinados-21-lideres/))

Según el medio regional Pagina 10, **el bloqueo de la vía Junín- Barbacoas- Magüí se produjo en protesta del gremio transportador por los asesinatos a conductores en esa ruta.** Tal como lo señaló el abogado defensor de Derechos Humanos, Alirio Uribe Muñoz, "las acciones difamatorias del Ministro de Defensa contra los líderes son cuestionables", ligeras y ponen en riesgo a quienes defienden los derechos de las comunidades en el país.

Para el Abogado, **el mensaje que se envía a la ciudadanía "es que los líderes son de guerrillas o paramilitares"**, afirmación con la que esta de acuerdo la directora de Somos Defensores, Diana Sánchez, quién sostuvo que durante años, los defensores han luchado por la legitimidad de la protesta y con esta declaración, el Ministro "hecho todo por la borda".

La declaración se produce en medio del asesinato de un gran número de líderes sociales; según el último reporte de la Defensoría del Pueblo, entre 2016 y agosto de 2018 habían sido asesinados 343 defensores y defensoras de derechos humanos. Adicionalmente, tal como lo denuncian Sánchez y Uribe, **en este momento están viviendo una criminalización de líderes en el Meta.** (Le puede interesar:["Pacto por la vida se firmó excluyendo a líderes que la tienen en riesgo"](https://archivo.contagioradio.com/pacto-vida-excluyendo-lideres/))

<iframe src="https://co.ivoox.com/es/player_ek_28587342_2_1.html?data=k52impyXeJOhhpywj5WaaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncaXdwtPOjbiJh5SZopbbxc3JvoampJDg0cfWqYzZzZDdx9HNq9PjjMnSjdHFt4zYxsjZw9fFp8rjz8rgjcmRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **"El presidente se reunió con la OEA para tratar las muertes de líderes sociales y el Ministro está en otra sintonía"** 

Para Uribe Muñoz, resulta contrario que mientras Iván Duque se reúne con Luis Almagro, secretario general de la Organización de Estado Americanos (OEA), y habla de líderes sociales, el Ministro señala y criminaliza la protesta social, brindando un marco de legalidad a quienes la judicializan. Como señaló Sánchez, **en medio de una ambiente de pos acuerdo, no es posible que no se respete el derecho a la protesta.**

El Abogado defensor de Derechos Humanos, afirmó que el Congreso también debería legislar para que se respete el derecho a la movilización, mientras la Directora de Somos Defensores, sostuvo que las organizaciones sociales deben seguir dialogando con la Fiscalía, la Procuraduría y la Defensoría del pueblo, así como con las Fuerzas Militares, para que se brinden garantías para la protesta. (Le puede interesar:["El próximo 5 de octubre 'pasaran al tablero' autoridades que protegen líderes: Iván Cepeda"](https://archivo.contagioradio.com/5-octubre-lideres-cepeda/))

Por estos hechos, Alirio Uribe sostuvo que el Ministro debe retractarse de sus declaraciones, mientras que en el Senado, **Iván Cepeda** anunció que **citará a Botero a un debate de control político "por semejante afirmación genérica e irresponsable".** (Le puede interesar: ["Líderes sociales denuncian estrategia de Ecopetrol para criminalizarlos"](https://archivo.contagioradio.com/ecopetrol-pretende-judicializar-lideres/))

> En Cartagena, el Ministro Guillermo Botero afirmó que con dineros ilícitos, grupos armados organizados vinculados al narcotráfico, junto a mafias internacionales, financian protesta social. Anuncio que citaré al Ministro a debate por semejante afirmación genérica e irresponsable
>
> — Iván Cepeda Castro (@IvanCepedaCast) [14 de septiembre de 2018](https://twitter.com/IvanCepedaCast/status/1040446182303129600?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
