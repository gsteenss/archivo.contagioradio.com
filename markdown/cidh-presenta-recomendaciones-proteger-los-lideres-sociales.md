Title: CIDH presenta recomendaciones para proteger los líderes sociales
Date: 2019-01-18 19:28
Author: AdminContagio
Category: DDHH, Líderes sociales, Nacional
Tags: acuerdo de paz, asesinato de líderes sociales, Comisión Interamericana de Derechos Humanos, Estigmatización, impunidad
Slug: cidh-presenta-recomendaciones-proteger-los-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/imágenlíderes.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Coeuropa 

###### 18  ene 2019 

La Comisión Interamericana de Derechos Humanos (CIDH) presentó esta semana al Estado colombiano una lista de recomendaciones para proteger los defensores de derechos humanos tras realizar una visita de verificación y observación en Colombia el año pasado.

Entre el 27 y 30 de noviembre de 2018, una delegación de la CIDH, liderada por el Comisionado Francisco Eguiguren, se reunió con defensores de derechos humanos en Bogotá, Quibdo y Medellín tal como autoridades del Estado, a nivel nacional y regional, para documentar los riesgos de seguridad que viven los líderes al desempeñar sus cargos.

[Según un comunicado](http://www.oas.org/es/cidh/prensa/comunicados/2019/008.asp), la delegación determinó que existe "un grave problema" de asesinatos, amenazas, hostigamientos y atentados en contra de líderes sociales, que solo se ha agudizado por las limitaciones de medidas existentes de seguridad y una situación de impunidad frente estos crímenes. Según organizaciones sociales, solo el 8.5 % de 361 casos reportados por estos grupos entre enero de 2016 a octubre de 2018 han avanzado.

No es la primera vez que la Comisión se pronuncia sobre las agresiones en contras los líderes sociales. Desde el 2016, la CIDH ha otorgado 10 medidas cautelares para la protección de los líderes y en 2017, publicó dos casos en que recomendó al Estado colombiano fortalecer las instituciones de investigación de los crímenes para prevenir los patrones de impunidad.

(Le puede interesar: "[Colombia, el país con más asesinatos de defensores en le mundo](https://archivo.contagioradio.com/colombia-pais-mas-asesinatos-defensores-mundo/)")

### **Preocupaciones de las organizaciones sociales** 

En reuniones con la delegación, organizaciones sociales llamaron a la atención la grave situación de violencia que viven comunidades indígenas y Afrodescendientes en zonas rurales del país, donde el conflicto armado ha históricamente sido más fuerte. Además, destacaron que los actos de violencia contra las mujeres defensoras y líderes LGBTQ ha incrementado.

Otra preocupación de las comunidades es la estigmatización que sufren las organizaciones sociales por declaraciones de alto funcionarios del Gobierno que vinculan su trabajo a grupos al margen de la ley. La CIDH indicó que estas declaraciones "puede contribuir a exacerbar el clima de hostilidad e intolerancia" que expone a los líderes a actos de violencia.

Frente el tema de la Unidad Nacional de Protección (UNP), las organizaciones sociales expresaron dudas sobre la eficacia de las medidas que actualmente protege a 4.367 defensores de derechos humanos. Las organizaciones manifestaron sus preocupaciones sobre  los análisis de riesgo, los retrasos y la implementación de las medidas de protección, así como de los procedimientos de levantamiento de las medidas. La CIDH resaltó que algunos esquemas de protección de personas beneficiaria de medidas cautelares otorgadas de la Comisión habrían sido retirados unilateralmente.

### **Recomendaciones de la CIDH** 

Ante estas denuncias, la Comisión reiteró que es "prioritario" que el Estado colombiano adopte medidas urgentes para la protección de líderes sociales. Por esta razón, la CIDH formuló la siguiente lista de 11 recomendaciones al Estado para proteger a los defensores de derechos humanos.

1.  Redoblar esfuerzos en la implementación del Acuerdo de Paz;
2.  Trabajar con organizaciones sociales para la construcción de una política pública de protección de los defensores de derechos humanos, además de retomar las mesas de diálogo como la Mesa Nacional de Garantías y la Comisión Nacional de Garantías de Seguridad;
3.  Crear un registro integral, con la participación de las organizaciones sociales, de las agresiones contras defensores de derechos humanos;
4.  Profundizar el análisis de contexto para la evaluación del riesgo y para la adopción de las medidas de protección, bajo un enfoque diferenciado que tenga en cuenta las situaciones particulares de la población que requiere protección y el lugar en el que ejercen su labor. En especial, incluir el enfoque étnico, colectivo y de género en las medidas de prevención y protección;
5.  Implementar "debidamente" las medidas cautelares otorgadas por la CIDH y mantener los esquemas de protección respecto de las personas beneficiarias mientras estén vigentes;
6.  Implementar las recomendaciones y decisiones del Sistema Interamericano y Universal;
7.   Aumentar el nivel de coordinación entre las autoridades a nivel nacional y local para que las medidas de protección sean adecuadas para resguardar los derechos de las personas defensoras y líderes y asegurando su efectividad en zonas rurales alejadas;
8.  Adoptar planes para prevenir la estigmatización de los líderes sociales, dentro de las entidades del Estado y en la sociedad;
9.  Adoptar medidas para investigar con debida diligencia y hacer frente a la situación de impunidad respecto de los crímenes cometidos contra personas defensoras de derechos humanos y líderes sociales en el país, determinando autores materiales e intelectuales;
10. Adoptar enfoques diferenciales de género, étnico y para población LGBTI, tanto en la construcción de programas de garantías, como en la investigación de posibles delitos contra personas defensoras de derechos humanos;
11. Fortalecer la coordinación con organismos internacionales de derechos humanos.

###### Reciba toda la información de Contagio Radio en [[su correo]
