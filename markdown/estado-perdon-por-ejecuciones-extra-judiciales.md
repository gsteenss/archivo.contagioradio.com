Title: Estado pedirá perdón por ejecuciones extra judiciales en Arauca
Date: 2018-07-17 17:04
Category: DDHH, Nacional
Tags: Acto de Perdón, Ejecuciones Extra Judiciales, falsos positivos, Saravena
Slug: estado-perdon-por-ejecuciones-extra-judiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/nota_ejecuciones1.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [17 Jul 2018] 

El 18 de julio, el Estado colombiano presidirá un acto de reconocimiento de responsabilidad y perdón público, por el asesinato de los jóvenes Oscar Orlando Bueno Bonnet, Jefferson González Oquendo y Jean Carlo Cavarique, a manos de miembros del Ejército, ocurrido el 10 de enero de 1997 en el municipio de Saravena, Arauca.

Los hechos ocurrieron cuando los 3 jóvenes se movilizaban en dos motocicletas por el casco urbano del municipio de Saravena, posteriomente fueron interceptados por miembros del Grupo de Caballería Mecanizada del batallón Revéis Pizarro, quienes procedieron a dispararles. **Tras la primera agresión, los adolescentes intentaron huir de las balas, pero fueron alcanzados y asesinados por los uniformados.**

Aunque en 1998 el Juzgado Penal Militar abrió una investigación contra el Cabo Carlos Medina, el Teniente Diego Martínez, y el soldado Reimond Piñerez, miembros del Ejército involucrados en los hechos, el Tribunal Superior Militar declaró el cierre de la investigación. Sin embargo, la Fiscalía Penal Militar envío el expediente a la Unidad de Derechos Humanos y Derecho Internacional Humanitario de la Fiscalía General de la Nación, y **en la actualidad el caso se encuentra en etapa de juicio.**

### **El Estado pedirá perdón por el asesinato de Orlando, Jefferson y Jean Carlo** 

El caso fue llevado ante la Comisión Interamericana de Derechos Humanos en 1998, y en 2015 los familiares de las víctimas lograron un acuerdo con **el Estado, que finalmente reconoció su responsabilidad por la violación del derecho a la vida** y la integridad personal.

Producto del acuerdo, la Oficina del Alto Comisionado para la Paz y la Consejería Presidencial para los Derechos Humanos, realizará en nombre del Estado colombiano un acto de responsabilidad y perdón público. Adicionalmente,  se desarrollara una medida de reparación simbólica a los familiares de las víctimas con la realización de un mural.

El acto de perdón se realizará en el Parque Central de Saravena, a las 10 de la mañana de este Miércoles, y **servirá para honrar la memoria, la dignidad y el bueno nombre de los jóvenes que fueron presentados como bajas en combate.** Adicionalmente, se exigirá al Estado y el Ejercito Nacional de Colombia que se garantice la no repetición de los hechos. (Le puede interesar: ["Ningún falso positivo se dio al azar: Autor del libro ejecuciones extrajudiciales en Colombia"](https://archivo.contagioradio.com/ningun-falso-positivo-se-dio-al-azar-autor-del-libro-ejecuciones-extrajudiciales-en-colombia/))

###### [Reciba toda la información de Contagio Radio en] [[su correo] [[Contagio Radio]
