Title: Aumento en transporte público en Bogotá el más alto en 8 años
Date: 2017-03-15 16:56
Category: DDHH, Nacional
Tags: Transmilenio
Slug: aumento-en-transporte-publico-en-bogota-el-mas-alto-en-8-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/pulzo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Pulzo.com] 

###### [15 Mar 2017] 

En \$2.200 pesos quedará el pasaje de Transmilenio y \$2.000 pesos el pasaje del SITP con el nuevo ajuste emitido por la Secretaría de Movilidad de Bogotá, que empezará a regir a partir del 1 de abril,  y que en **8 años ha sido el aumento más alto que se le ha hecho al transporte público**. Modificación que ya afana a los más de 2 millones de personas que tendrán que pagar las nuevas tarifas.

La medida ya ha recibido fuertes críticas por parte de expertos y ciudadanos, debido a que el ajuste de **\$500 pesos está por encima de la inflación** y afectaría fuertemente el bolsillo de los pasajeros que tendrían que destinar, si se moviliza dos veces en Transmilenio, **\$88.000 pesos y \$80.000 pesos si usa SITP en un mes**.

Cifras que serían el 10% del salario mínimo actual vigente, que se encuentra en \$820.827 pesos y que afecta en su gran mayoría a las **personas de estrato 1,2 y 3 que son lo que más utilizan el sistema de Transporte público en la Capital.** Le puede interesar:["Transmilenio: el negocio pulpito de Peñalosa"](https://archivo.contagioradio.com/transmilenio-el-negocio-pulpito-de-penalosa/)

La Alcaldía de Peñalosa señaló que esta modificación servirá para beneficiar los tiempos de transbordos que reducirá 20 minutos de espera y ayudará a la reducción del déficit que en 2016 alcanzó los **\$661.000 millones de pesos**. Le puede interesar: ["Ya se han recolectado 200 mil firmas para la revocatoria de Peñalosa"](https://archivo.contagioradio.com/ya-se-han-recolectado-200-mil-firmas-para-la-revocatoria-de-penalosa/)

Sin embargo, este ajuste no plantea soluciones para las tres problemáticas más importantes del sistema de transporte: **La inseguridad al interior del sistema, las largas esperas** a las que se ven sometidos los usuarios y **las aglomeraciones tanto en las estaciones como al interior de los articulados**, que de acuerdo con los pasajeros, evidencia un "alto costo por un mal servicio".

###### Reciba toda la información de Contagio Radio en [[su correo]
