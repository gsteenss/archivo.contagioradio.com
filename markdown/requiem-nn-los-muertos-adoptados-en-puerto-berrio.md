Title: "Réquiem NN", los muertos adoptados en Puerto Berrio
Date: 2015-08-11 10:20
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Juan Manuel Echavarria fotógrafo, Los muertos NN de Puerto Berrio Antioquia, Requiem NN Documental
Slug: requiem-nn-los-muertos-adoptados-en-puerto-berrio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Requiem_NN_Still_Tomb.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [11 Ago 2015]

Durante más de 30 años, los pobladores de Puerto Berrio, municipio del Magdalena medio, han sacado de las aguas de su principal afluente los cadáveres  o parte de ellos, de cientos de colombianos víctimas del conflicto entre paramilitares y guerrilleros que se disputan el control de la zona, sepultándolos y adoptándolos como propios, poniéndoles flores e incluso proporcionándoles un nombre y apellido.

[![Requiem\_NN\_Mausoleum](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Requiem_NN_Mausoleum.jpg){.aligncenter .wp-image-12033 .size-full width="1920" height="1080"}](https://archivo.contagioradio.com/requiem-nn-los-muertos-adoptados-en-puerto-berrio/requiem_nn_mausoleum/)

Es lo que el fotográfo antioqueño Juan Manuel Echavarría plasma en el documental "Requiem NN", (2015) un trabajo audiovisual que presenta la manera en que a través del ritual colectivo de adopción simbólica, se reconstruye el tejido social desgarrado durante décadas en la región por los ciclos continuos de violencia.

En el ritual, celebrado desde hace varios años durante el mes de noviembre, las puertas del cementerio se abren a media noche dando paso al "Animero", un hombre vestido con capa negra, quien invita a los fieles a unirse en la procesión y a la oración del "Requiem Aeternum" una liturgia en latín por el descanso eterno de las almas, manifestación espiritual que, según la creencia, otorga a los vivos protección y favores.

"A medida que fui conociendo a la gente, me inspiré en hacer una película sobre sus vidas"cuenta Echavarría, quién entre los años 2006 y 2013 registró bajo su lente las tumbas de los "sin nombre" en "Novenario en espera", trabajo fotográfico que motivó la realización de un documental donde se recogiera con mayor detalle las historias de vida de los "adoptantes".

La posibilidad de involucrarse con la comunidad de Puerto Berrio, le abrió las puertas al director, quien durante dos años se dedicó junto su equipo de producción, a recoger las historias de quienes han hecho propios los muertos de nadie, "Nos cautivaron las historias que empezamos a escuchar y eso fue esencial para el éxito de nuestro trabajo".

"Requiem NN", coproducido por Lulo Films y la Fundación Puntos de encuentro,  se presentó el pasado 6 de agosto en Cinema Tonalá en Bogotá, y tendrá una segunda presentación el viernes 14 a partir de las 7 p.m., con presencia de Juan Manuel Echavarría junto a Fernando Grisalez, director de Fotografía del documental y con moderación de Camila Rodas, integrante de la plataforma PACIFISTA.

<iframe src="https://www.youtube.com/embed/_cNJ9t4NooA" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Sinopsis Oficial:

Es una película sobre el ritual de resistencia a la violencia que los pobladores de Puerto Berrío, Colombia han vivido durante muchas décadas. El rescate de los cuerpos del río Magdalena, su entierro, los favores que la gente pide a las almas de los NN (Ningún Nombre), el bautizo dado a estos cuerpos desconocidos, es el ritual colectivo que lleva mas de 30 años con estos cuerpos que trae el río.
