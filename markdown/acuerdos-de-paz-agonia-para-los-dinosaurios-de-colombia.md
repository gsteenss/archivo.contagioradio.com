Title: Acuerdos de paz: Agonía para los dinosaurios de Colombia
Date: 2015-09-25 16:15
Category: Opinion, Ricardo
Tags: acuerdo de paz, Alvaro Uribe, dialogos de paz, Poder Ejecutivo, Poder Legislativo, Procurador Ordóñez, Sistema Judicial
Slug: acuerdos-de-paz-agonia-para-los-dinosaurios-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/dinosaurios-de-la-u.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Ricardo Ferrer Espinosa @ferr\_es

###### [25 Sep 2015]

**El 23 de septiembre de 2015 los dinosaurios de Colombia supieron que en 180 días les caerá encima el asteroide de la paz** [\[1\]. Asustados, el líder de las bandas criminales uribistas y el procurador de iniquidades llamaron a rueda de prensa para mostrar de nuevo sus colmillos. Ya avisaron que harán daño hasta el último de sus días: ven en peligro sus zonas de influencia, los cotos vedados, y los privilegios que quieren conservar a toda costa.]

**El parque jurásico colombiano aún cuenta con selvas para sus bestias:** [ ]

-   [El poder ejecutivo. Esperamos que esta vez trabaje por el interés nacional y no se subordine a las corporaciones y multinacionales. Estamos muy lejos de construir un poder popular, pero el marco de las negociaciones debe facilitar una mayor representación del pueblo-nación en la dirección del Estado. El conflicto tiene su raíz principal en la masa de excluidos.]
-   [El poder legislativo. Debe ser renovado con nuevas especies y nuevas políticas.  ]
-   [El sistema judicial. Debe usar menos papel y producir más justicia. Todo el sistema laboral y contractual en el sistema judicial, por paradoja, es injusto y obsoleto. Otra  justicia es urgente.]
-   [Un sistema primitivo es la contratación de los abogados para la Defensoría del Pueblo: Atados con malevos “contratos de prestación de servicios”,]**¡los trabajadores de la justicia, trabajan como esclavos¡** [\[2\]. El régimen laboral y de carrera debe ser corregido.]
-   [El sistema penitenciario de Colombia es una vergüenza mundial.  ]
-   [El sistema económico. Debe ser sustituido por otro que no triture a la naturaleza ni a la humanidad.]
-   [Los nidos paramilitares en Antioquia, la costa atlántica y la frontera con Venezuela. Deben ser neutralizados \[3\].]
-   [Los gremios de la producción. Deben renunciar a sus privilegios y asumir obligaciones sociales. Sobre todo, deben renunciar a relaciones cotidianas con mercenarios, escuadrones de la muerte y sicarios. Los derechos de los trabajadores y sindicalistas deben ser protegidos.]
-   [La prensa ahistórica: que por fin cuente las verdades de Colombia.]
-   [Una parte de la izquierda caníbal: que retome la creatividad y se vincule de una vez con los cambios sociales.  ]
-   [La Seguridad Social: Es muy primitiva en educación y la salud está en manos de las EPS, ente cancerígeno. La prueba de que no se atiende la salud mental y la farmacodependencia está en los habitantes de calle. Han sido abandonados a su suerte y generan riesgo para los demás ciudadanos.]
-   [La seguridad social va muy atrás en la provisión de vivienda, la atención a personas discapacitadas, la mujer y la protección familiar.]

**Un observatorio vigilante de bestias, durante los siguientes seis meses, debe incluir los siguientes casos:**

-   [¿Seguirá en libertad el genocida que gobernó a Colombia entre 2002 y 2010?]
-   [¿Seguirá bramando el]**procurador de iniquidades**[? ¿Seguirá el fiscal corrupto?]
-   [¿Se desmontará el paramilitarismo? ¿El Ejército de Colombia seguirá agrediendo a la Comunidad de paz de San José de Apartadó?]
-   [¿Seguirá el ascenso de militares vinculados con violaciones a los Derechos Humanos?]
-   [¿Seguirá el arrasamiento de las comunidades indígenas?]
-   [¿Caracol y RCN conservarán su modelo desinformativo y racista?  ]
-   [¿Seguirá la criminalización de la protesta social?]
-   [¿Vivirán los reclamantes de tierras?]

[Estamos ante seis meses de puro vértigo. El 23 de marzo de 2016 veremos si estalla la “paz” o sigue la ley de la selva.]

###### [\[[1\] http://]][[[www.vanguardia.com/colombia/328974-gobierno-y-farc-firmaran-la-paz-a-mas-tardar-el-23-de-marzo-del-2016]](http://www.vanguardia.com/colombia/328974-gobierno-y-farc-firmaran-la-paz-a-mas-tardar-el-23-de-marzo-del-2016)] 

###### [[\[2\]][[http://www.defensoria.gov.co/es/public/atencionciudadanoa/1472/Asesor%C3%ADa-para-representaci%C3%B3n-judicial-y-extrajudicial.htm]](http://www.defensoria.gov.co/es/public/atencionciudadanoa/1472/Asesor%C3%ADa-para-representaci%C3%B3n-judicial-y-extrajudicial.htm)] 

###### [\[3\] Tara de la cultura antioqueña: la costumbre de exaltar y seguir a criminales como Pablo Escobar y Álvaro Uribe Vélez. Paramilitares y narcotraficantes tienen poca resistencia social y mediática.] 
