Title: Ilario Mecha, guardia indígena Wounaan asesinado en Bogotá
Date: 2020-03-18 10:35
Author: AdminContagio
Category: Actualidad, DDHH
Slug: ilario-mecha-guardia-indigena-wounaan-asesinado-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Ilario-Mecha-guardia-indígena-Wounaan-en-Bogotá.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @heidy\_up {#foto-heidy_up .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

La concejala de Bogotá por la Colombia Humana-UP denunció el pasado domingo 15 de marzo el asesinato de Ilario Mecha en Bogotá, él era integrante de la guardia indígena de la comunidad Wounaan Nonam que habita en Ciudad Bolívar y fue desplazada de Chocó. El cabildo indígena de este pueblo en Bogotá afirma que estas amenazas son constantes, y las 605 personas que integran la comunidad sienten temor por sus vidas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Ilario Mecha, Lorindo Membache y Yerson Membache**

<!-- /wp:heading -->

<!-- wp:paragraph -->

En un comunicado, el Cabildo Indígena Wounaan Nonam de Bogotá recuerda que en agosto de 2016 fue asesinado Lorindo Memache Chamarra de 17 años, proveniente de Pizarro en Chocó, a él se suma Yerson Membache Opua de 13 años, que perdió la vida en julio de 2017. En ambos casos, se usaron armas blancas para cometer los homicidios.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El más reciente hecho involucra al guardia indígena Ilario Mecha Pedroza, proveniente del Bajo Baudó Pizarro, Chocó, y que vivía (como los otros dos menores) en la localidad de Ciudad Bolívar. Adicionalmente, el Cabildo reporta que en diciembre de 2019 un miembro del pueblo fue apuñalado en el barrio Naciones Unidas, pero debido a que la herida no fue grave fue posible salvar su vida. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por esta razón, y ante las constantes amenazas a los líderes en el barrio Vista Hermosa de la localidad, en el comunicado se declara que los 605 habitantes del cabildo, agrupados en 145 familias se sienten atemorizados, y recuerdan los motivos que los llevaron a desplazarse desde el Chocó. (Le puede interesar: ["Antioquia y Chocó registran el mayor número de reclutamiento a menores"](https://archivo.contagioradio.com/antioquia-y-choco-registran-el-mayor-numero-de-reclutamiento-a-menores/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Atacan el nivel organizativo de la comunidad?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Francis Franco, profesora de un colegio en Ciudad Bolívar, señala que la comunidad Wounaan tiene niveles de organización importantes, llegaron a Bogotá y retomaron su proceso. Adicionalmente, mantienen su lengua materna y han tratado de incorporarse a las dinámicas de la ciudad. (Le puede interesar: ["Aumenta el control paramilitar en Chocó"](https://archivo.contagioradio.com/aumenta-control-paramilitar-en-choco/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La profesora sostiene que en la localidad hay diferentes niveles de violencia vinculados a las necesidades que vive la población, así como a los distintos tráficos que tiene cierta incidencia sobre la población juvenil. Por eso, se pregunta si los ataques a la población indígena se desprenden de la intención de los actores ilegales de afectar un proceso sólido, que le puede afectar el control territorial.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
