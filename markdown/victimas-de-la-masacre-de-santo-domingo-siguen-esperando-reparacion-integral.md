Title: Víctimas de la masacre de Santo Domingo siguen esperando reparación integral
Date: 2017-08-31 13:41
Category: DDHH, Nacional
Tags: "La injerencia de Estados Unidos, Arauca, CIDH, Fuerzas militares, masacre de santo domingo, víctimas
Slug: victimas-de-la-masacre-de-santo-domingo-siguen-esperando-reparacion-integral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/santo-domingo-e1504204862450.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Llanera.com] 

###### [31 Ago 2017] 

Tras la condena al Estado colombiano por parte de la Corte Interamericana de Derechos Humanos, **hoy se realiza en Santo Domingo un acto de reconocimiento de responsabilidad por parte del Estado** que debería abrir paso a la atención integral de las víctimas, quienes hasta el momento no han sido reparadas, por el contrario, muchos de ellos y ellas siguen en situaciones de salud, de empleo y de olvido bastante notorias y no han sido atendidas.

### **La masacre ** 

El 13 de diciembre de 1998 la Fuerza Aérea Colombiana sobrevoló la comunidad de Santo Domingo en un helicóptero. Bajo la operación “Pantera II”, **la fuerza militar arrojó sobre la población una bomba llamada cluster**, la cuál al ser disparada en el aire dispersa varias granadas. Por la explosión de estos artefactos murieron 17 civiles de los cuales 6 eran niños y otras 27 personas resultaron heridas.

Por el temor a que se repitieran estas operaciones, **300 personas se vieron obligadas a abandonar sus territorios** y ese mismo diciembre, el cacerío de Santo Domingo quedó abandonado. Por estos actos la Corte Interamericana de Derechos Humanos condenó al Estado colombiano y lo obligó a reconocer su responsabilidad en un acto público desde el año 2012. Sin embargo, tuvieron que pasar 5 años para la realización de este reconocimiento. (Le puede interesar: ["Víctimas no están recibiendo óptima atención en salud mental: MSF"](https://archivo.contagioradio.com/victimas_salud_medico_sin_fronteras/))

### **El Estado no quiso reconocer la verdad de las víctimas durante mucho tiempo** 

Según Jomary Ortegón, presidenta del Colectivo de Abogado José Alvear Restrepo, “el esfuerzo de muchos años de las víctimas y de organizaciones sociales de Arauca ha hecho que **se reconozca la existencia de la masacre**”. Dijo igualmente que hasta ahora va a ser posible que la sociedad conozca que hubo una actuación ilegal del Estado dándole la razón a las denuncias de las víctimas.

Ello después de un proceso en que **se intentó culpar a las FARC y negar toda responsabilidad por parte de los miembros de la Fuerza Pública** que, incluso fueron condenados por la justicia nacional, y que usaron bombas catalogadas como de "racimo" contra ese sector de la población civil sin mediar ningún tipo de principio de precaución. (Le puede interesar: ["20 años de justicia agridulce para las víctimas de la masacre de Mapiripán"](https://archivo.contagioradio.com/20-anos-de-justicia-agridulce-para-las-victimas-de-la-masacre-de-mapiripan/))

### [**El reconocimiento de responsabilidad debería también garantizar la reparación integral**] 

Ortegón fue enfática en manifestar que **la reparación del Estado ha sido precaria** en la medida que, por ejemplo, las víctimas de desplazamiento forzado que sobrevivieron a la masacre, no tuvieron nunca una atención adecuada, “la CIDH ordenó atención médica y sicológica para las personas que aún tienen lesiones”.

De igual forma, ella indicó que **el reconocimiento de la masacre debe significar un cambio en la postura y la actitud del Estado** para que se establezcan las indemnizaciones, los procesos de rehabilitación y el fortalecimiento a los procesos de memoria. Para Ortegón, “no puede seguir ocurriendo que digan que la masacre no ocurrió”.

En el marco de la implementación de los acuerdos de paz, **“debe haber espacios de dignificación, reconocimiento y construcción de memoria”.** Ortegón manifestó además que es necesario que se reconsidere los métodos de la doctrina militar en la medida en que “no es posible que por el hecho de librar una lucha contra insurgente se utilicen acciones con armas que destruyan a las poblaciones”.

Finalmente, los habitantes de Arauca, **prepararon para el día de hoy un acto de construcción de memoria** e instalaron dispositivos sonoros en los árboles con grabaciones de anécdotas y recuerdos de las personas que perdieron la vida en la masacre.

<iframe id="audio_20625706" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20625706_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
