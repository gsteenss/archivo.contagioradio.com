Title: Denuncian que Paramilitarismo y daños ambientales van de la mano en Buenaventura
Date: 2017-07-17 13:20
Category: DDHH, Nacional
Tags: buenaventura, la esperanza, paramilitares
Slug: 43782-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia y Paz] 

###### [17 Jul. 2017]

Según la denuncia de la Comisión Intereclesial de Justicia y Paz, durante este fin de semana los habitantes del Territorio de Paz, Humanitario y ambiental del Consejo Comunitario La Esperanza en Buenaventura, Valle del Cauca, vieron cómo **3 paramilitares irrumpieron en su comunidad y además han constatado la presencia de maquinaria pesada que ha desencadenado en** varios daños ambientales.

“Tres paramilitares de la estructura de los urabeños merodearon el salón comunal, en momentos en que miembros de la comunidad desarrollaban la reunión de análisis (...)” asevera la misiva. Le puede interesar: [Líderes de CONPAZ son víctimas de amenazas y seguimientos](https://archivo.contagioradio.com/lideres-de-conpaz-son-victimas-de-amenazas-y-seguimientos/).

En la denuncia, también aseguran que los daños siguen produciéndose, pese a la medida cautelar otorgada a estas comunidades por parte del Juzgado Segundo Civil del circuito especializado en restitución de tierra de Cali, que protege el territorio colectivo, y las denuncias de la organización Witness for Peace y la Comisión de Justicia y Paz.

“(…) visitaron el territorio colectivo y en presencia de unidades de la policía nacional que llegaron al lugar, **constataron la presencia ilegal de maquinaria pesada tipo retro-escavadora y motor de uso minero, así mismo se verificaron daños ambientales** en los predios del señor Roberto Agudelo por la apertura de una carretera en una extensión de 300 metros hacia la quebrada La Sierpe” agrega la comunicación.

Según la ONG nacional, en ese lugar algunos foráneos pretenden instalar un centro turístico y hacer extracción minera “también se verificaron nuevas construcciones de vivienda y terrenos listos para ser construidos”.

Adicionalmente dicen que el sábado 15 de julio hacia el mediodía en Buenaventura, **el líder y miembro del Consejo comunitario, Edwin Becerra fue amenazado vía celular** en la que se le dice: “sepa que, aunque ande con dos escoltas, igual le cabe plomo”. Le puede interesar: [Ante presión paramilitar, comunidad declara Territorio de Paz en Buenaventura](https://archivo.contagioradio.com/ante-presion-paramilitar-comunidad-declara-territorio-de-paz-en-buenaventura/)

Manifiesta la Comisión de Justicia y Paz en otra denuncia que **el territorio es usado por estructuras paramilitares que fomentan la deforestación,** la siembra de coca, estaderos vacacionales, desconociendo los derechos de las familias de comunidades negras, a quienes se les tituló el territorio en el año 2008. Le puede interesar: [Así va el cumplimiento de los acuerdos del Gobierno con Buenaventura](https://archivo.contagioradio.com/asi-va-el-cumplimiento-del-acuerdo-del-gobierno-con-buenaventura/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
