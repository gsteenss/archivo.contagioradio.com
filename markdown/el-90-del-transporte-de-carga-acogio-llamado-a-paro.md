Title: El 90% del transporte de carga ha acogido llamado a paro
Date: 2015-03-04 16:56
Author: CtgAdm
Category: Movilización, Nacional
Tags: Asociación Colombiana de Camioneros, Paro de camioneros en Colombia, Precio de los combustibles en Colombia
Slug: el-90-del-transporte-de-carga-acogio-llamado-a-paro
Status: published

###### Foto: publicamion.com.co 

<iframe src="http://www.ivoox.com/player_ek_4161088_2_1.html?data=lZajk5WcfI6ZmKiakpqJd6KkmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjNHWxsrWb8XZzZC9w9fTb6TVzs7c0MrWs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Pedro Aguilar ACC] 

Según informa Pedro Aguilar, representante de la Asociación Colombiana de Camioneros, el **90% de los transportadores en Colombia se acogieron al llamado a Paro Nacional** convocado por varias agremiaciones. Sin embargo, el gobierno nacional no ha acogido ninguna de las propuestas del sector en cuanto al costo de los fletes, de los combustibles y los valores de mantenimiento de los vehículos.

Aguilar afirma que en las condiciones actuales los transportadores que han acogido el paro no están perdiendo, puesto que el funcionamiento, tal como es en la actualidad, está generando pérdidas, por lo tanto, el hecho de **no mover los camiones estaría dejando de generar un gasto adicional** al que se tienen que enfrentar los transportadores en cada recorrido.

Aguilar recuerda que el primer problema que debe ser resuelto, desde el **2011 el gobierno anunció un régimen de libertad vigilada** que no ha sido aplicado y lo que solucionaría el problema de la competencia de los pequeños transportadores con las grandes compañías que pueden bajar los precios para acaparar el mercado.

En el tema de los combustibles los camioneros exigieron una **reducción del precio del galón de gasolina en \$ 1000 pesos, el gobierno solo accedió a una reducción de \$ 300** lo cual es insuficiente para solventar las pérdidas que genera el transporte de carga en Colombia. El otro aspecto es que el gobierno está imponiendo una vida útil de 15 años sin ningún tipo de estudio técnico que respalde la decisión.

Los transportadores afirman que esperan llevar a cabo una serie de reuniones con funcionarios del gobierno para intentar encontrar salidas a la crisis y **no solamente un mecanismo de mitigación de la actual protesta.**
