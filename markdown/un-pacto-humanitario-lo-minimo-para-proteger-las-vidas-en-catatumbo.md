Title: Un pacto humanitario, lo minimo para proteger las vidas en Catatumbo
Date: 2020-02-25 18:35
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Acuerdo Humanitario, ASCAMCAT, paro armado
Slug: un-pacto-humanitario-lo-minimo-para-proteger-las-vidas-en-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Marcha-en-Catatumbo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @AscamcatOficia {#foto-ascamcatoficia .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:html -->  
<iframe width="238" height="48" frameborder="0" allowfullscreen scrolling="no" src="https://co.ivoox.com/es/player_ek_48357144_2_1.html?data=lZ2gl5yVeJWhhpywj5WcaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncavpz87c1JCxpc3Y0NPOxtSJdqSfytPhx8zWpc_oxpDRx5Cll6S1rqiutpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Tras la misión humanitaria al municipio de El Carmen, en Catatumbo del pasado 22 y 23 de febrero, para verificar la situación de las poblaciones afectadas por el paro armado decretado por el ELN y el EPL en la región, organizaciones señalaron la necesidad urgente de establecer pactos humanitarios que eviten nuevas afectaciones a las comunidades. (También lo invitamos a consultar: ["En Catatumbo el paro armado se mantiene"](https://archivo.contagioradio.com/en-catatumbo-el-paro-armado-se-mantiene/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Las situaciones encontradas por la misión humanitaria** en Catatumbo

<!-- /wp:heading -->

<!-- wp:paragraph -->

La misión humanitaria estuvo a cargo de organizaciones sociales, defensoras de derechos humanos, entes internacionales y la Defensoría del Pueblo. Gracias al testimonio de los habitantes de El Carmen, la Misión señaló que se han presentado abusos por parte de la Fuerza Pública, confinamientos y desplazamientos de población, así como que se mantiene el estado de zozobra.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Junior Maldonado, integrante de la Asociación Campesina del Catatumbo (ASCAMCAT), explica que la Fuerza Pública está en los cascos urbanos, lo que puede representar un riesgo para las comunidades que en un eventual enfrentamiento quedarían en medio de balas de los armados legales e ilegales.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AscamcatOficia/status/1231352051373944832","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscamcatOficia/status/1231352051373944832

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Asimismo, las comunidades denuncian que [la Fuerza Pública las hostiga](https://twitter.com/AscamcatOficia/status/1226914759213142016) con requisas, empadronamientos y realizando registros sin una orden judicial. Por otra parte, también señalaron que en un comunicado del EPL se informó el levantamiento del paro armado, pero condicionado a negociaciones que haga con el ELN.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según esta información, la ausencia de hostilidades dependería de un pacto entre ambos grupos armados ilegales. Hecho que sin embargo, no puede evitar la confrontación con las Fuerzas Militares de Colombia y Venezuela, así como Los Rastrojos, que según declaró Maldonado, mantienen a comunidades en Hacarí y Ocaña confinadas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"Necesitamos construir unos mínimos de acuerdos humanitarios"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Maldonado aseguró que gracias al Comité Internacional de la Cruz Roja (CICR) fue posible la llegada de ayuda humanitaria a la zona, y se designó a párrocos del territorio para realizar gestiones que construyan "unos mínimos de acuerdos humanitarios: La guerra está en un máximo nivel y por eso necesitamos esos mínimos".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La misión humanitaria también solicitó a la Comisión de Paz del Congreso que visiten el territorio para verificar de primera mano lo que está ocurriendo, un evento que podría ocurrir hacia finales de febrero. Por otra parte, pidieron al Gobierno reactivar los gestores de paz del ELN, para tener interlocutores con la guerrilla que permitan desescalar el conflicto.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AscamcatOficia/status/1230573265464238080","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscamcatOficia/status/1230573265464238080

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78980} /-->
