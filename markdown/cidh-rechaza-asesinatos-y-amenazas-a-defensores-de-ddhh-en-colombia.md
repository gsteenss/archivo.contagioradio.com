Title: CIDH rechaza asesinatos y amenazas a defensores de DDHH en Colombia
Date: 2016-02-26 12:25
Category: DDHH, Nacional
Tags: Agresiones contra defensores de DDHH, Asesinato Daniel Abril, CIDH
Slug: cidh-rechaza-asesinatos-y-amenazas-a-defensores-de-ddhh-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/defensore-de-derechos-humanos-asesinados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [25 Feb 2016] 

Ante el crítico balance quen materia de [Derechos Humanos vivió Colombia durante 2015](https://archivo.contagioradio.com/54-defensores-de-derechos-humanos-fueron-asesinados-en-colombia-durante-2015/)  y comienzos de 2016, la CIDH manifestó su preocupación y rechazo, en particular por los homicidios de los defensores Jhon Jairo Ramírez Olaya, [Daniel Abril](https://archivo.contagioradio.com/asesinato-de-daniel-abril-impacto-fuerte-para-movimiento-social-casanareno/), Luis Francisco Hernández González, Johan Alexis Vargas y la defensora y lidereza Nelly Amaya.

A través de un comunicado de prensa, la organización condenó el asesinato de los 5 líderes quienes adelantaban procesos de restitución de tierras, defensa del ambiente, movimientos comunitarios y de resistencia en las regiones, así mismo se refiere al "contexto generalizado de represalias, hostigamiento y amenazas" que sufren los defensores y defensoras en el país. (Lea también [De 156 defensores de DDHH asesinados en 2015, 51 eran colombianos](https://archivo.contagioradio.com/de-156-defensores-de-ddhh-asesinados-en-2015-en-el-mundo-51-eran-colombianos-segun-informe-de-ai/))

En su escrito la Comisión Interamericana de Derechos Humanos reitera el llamado al Estado colombiano para que garantice el derecho a la vida, la integridad y la seguridad de los líderes y liderezas y que además cumpla su obligación de "investigar de oficio hechos de esta naturaleza y sancionar a los responsables materiales e intelectuales".

Adicionalmente la CIDH cita algunos casos representativos de vulneración de derechos, como los atentados contra la vida del líder indígena Feliciano Valencia y las amenazas de muerte contra miembros de MOVICE, líderes sindicales de la CUT-Valle, líderes sociales del Cauca, asi como sobre defensores de comunidades afrodescendientes y pueblos indígenas.

El organismo internacional hace enfasis en la necesidad de proteger a quienes trabajan en la defensa de los derechos humanos porque al atentar contra sus vidas e integridad afectan además "a todas aquellas personas para quienes trabajan, dejándoles en un estado de mayor vulnerabilidad y sumiéndolos en una situación de indefensión", considerando que de cara a la paz "la labor de defensores y defensoras es esencial para la construcción de una sociedad democrática y la consolidación del Estado de Derecho".

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
