Title: Continua el asesinato de líderes sociales en Nariño
Date: 2018-06-06 16:15
Category: DDHH, Nacional
Tags: acuerdo de paz, defensores de derechos humanos, lideres sociales, lideres sociales asesinados, nariño
Slug: nuevamente-un-lider-social-fue-asesinado-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/carlos-jimmy-líder.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: HSB Noticias] 

###### [06 Jun 2018] 

Un nuevo líder social fue asesinado en el pacífico colombiano, se trata de **Carlos Jimmy Prado Gallardo**, quien se desempeñaba como delegado nacional del espacio de consulta previa de las comunidades afrocolombianas, palenqueras y raizales en Nariño. Según cifras de la Defensoría del Pueblo, en el país han sido asesinados 282 líderes desde 2016.

De acuerdo con la información de diferentes medios de comunicación, el defensor de derechos humanos fue asesinado el **2 de junio** en el municipio Olaya Herrera Satinga donde vivía. Sus compañeros lo vieron por última vez con vida el viernes primero de junio mientras sostenía una reunión con la comunidad, motivada por la preocupación frente a la presencia de grupos armados.

Según los pobladores, el líder **había denunciado amenazas** contra su vida ante la Fiscalía General de la Nación y esperan que la investigación de los hechos avance con celeridad. Cabe resaltar que Nariño es el segundo departamento con el mayor número de líderes sociales asesinados desde la firma del Acuerdo de Paz con 32 crímenes. (Le puede interesar:["Solicitan medidas cautelares a CIDH para proteger a los líderes sociales"](https://archivo.contagioradio.com/solicitan-medidas-cautelares-a-cidh-para-proteger-a-los-lideres-sociales/))

### **Violencia por grupos guerrilleros ha disminuido** 

A pesar de estos hechos, la Fundación Paz y Reconciliación, por medio de un informe, indicó que la vilolencia en el país **ha disminuido producto de la firma del Acuerdo de Paz** entre el Gobierno Nacional y las FARC.  El informe indica que hubo una reducción de 10 casos en los homicidios por cada cien mil habitantes entre 2012 y 2017.

De igual forma, informó que fenómenos como el secuestro **alcanzaron su nivel más bajo** en las últimas tres décadas teniendo en cuenta que en 2017 hubo 180 casos frente a los 3 mil que se presentaron en los 90. Así mismo, en 2017 fueron afectadas 75 mil personas por desplazamiento forzado frente 272 mil víctimas que hubo en 2012.

A pesar de este panorama, Paz y Reconciliación recordó que la implementación del Acuerdo de Paz **no ha sido visible en los territorios** teniendo en cuenta que la inseguridad por la presencia de grupos armados ha aumentado en los territorios que antes eran controlados por las FARC. Además, la política de reincorporación no ha avanzado y los ex combatientes aún no tienen sus necesidades básicas satisfechas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
