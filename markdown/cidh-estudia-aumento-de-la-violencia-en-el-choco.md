Title: CIDH estudia aumento de la violencia en el Chocó
Date: 2019-05-08 18:18
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Alto Guayabal, CIDH, Jiguamiandó
Slug: cidh-estudia-aumento-de-la-violencia-en-el-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/1069355984.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/WhatsApp-Image-2019-05-08-at-7.00.20-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Como parte del periodo de audiencias 172 de la Comisión Interamericana de Derechos Humanos, CIDH, el pasado 7 de mayo fue expuesta, a través de sesiones de seguimiento a medidas cautelares, la difícil situación humanitaria que se vive en el Bajo Atrato, puntualmente en las comunidades de Cacarica, Alto Guayabal, Curbarado y Jiguamiando, donde se ha evidenciado un aumento de las actividades paramilitares y la ineficacia de las fuerzas militares frente a su accionar.

### Jiguamiandó y Curbaradó 

La primera audiencia puso en conocimiento de la CIDH, la situación que experimentan las comunidades de Pedeguita y Mancilla, Jiguamiandó, Curbaradó y los diversos factores de riesgo que viven en la actualidad, como la invasión de repobladores, o personas vestidas de civil, que con amenazas verbales o armas cortas, intimidan a los legítimos pobladores, dificultando aún más el proceso de restitución.

Durante la audiencia también se identificaron las operaciones de control por parte de las autodenominadas Autodefensas Gaitanistas de Colombia sobre las zonas humanitarias con el pretexto de perseguir a la guerrilla del ELN. Una situación que coincide con el ingreso de nuevos empresarios al lugar quienes pretenden aprovechar cerca de 6.000 hectáreas de tierra destinadas a diversos negocios, y quienes a través de estructuras armadas buscan ejercer presión sobre la población.

### Alto Guayabal 

De acuerdo con la denuncia de un líder indígena en la segunda audiencia, se denunció la retención forzada a personas por parte de integrantes de las AGC el pasado 23 de abril, que tendrían la intención de obtener información relacionada con la movilización de 12 líderes de la zona y de uno en particular que ha exigido el desmonte de dichos grupos. Asimismo se alertó que podrían declararlo a él y a los demás líderes como  objetivo militar.

Sumado a esto, las comunidades Embera han puesto en conocimiento la asociación de la violencia con intereses mineros de oro en el territorio. Frente a estas denuncias,  uno  de los líderes indígenas declaró que "espera que el Gobierno actúe para desmantelar los paramilitares para que existe tranquilidad en las comunidades indígenas y afro".

### **Cacarica** 

La tercera audiencia se centró en las medidas cautelares de Cacarica  y la gravedad de hechos evidenciados como el inicio de siembra de coca a cargo de repobladores, la restricción del uso doméstico de la madera por parte de las AGC contra las comunidades y su ingreso en la Zona Humanitaria de Nueva Vida, imposibilitando  la libre expresión, fomentando el temor en los jóvenes a ser reclutados y en las jóvenes una presión para doblegarse ante los mandos paramilitares.

Dicha situación ha generado hechos de represión y ha imposibilitado a las comunidades el uso y disfrute de su territorio, llegando incluso a prohibírseles el recibimiento de ayuda de diferentes organizaciones de DD.HH. (Le puede interesar: ["Neoparamilitares amenazan la zona humanitaria de Nueva Vida en Cacarica, Chocó"](https://archivo.contagioradio.com/neoparamilitares-amanezan-la-zona-humanitaria-de-nueva-vida-en-cacarica-choco/))

Todas estas circunstancias han sido puesta a disposición de la CIDH, esperando que se haga un reconocimiento de los mecanismos de autoprotección de las comunidades, y se exhorte a una mayor atención del  Gobierno hacia estas comunidades.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
