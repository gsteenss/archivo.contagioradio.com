Title: Por tercera vez comunidades insisten a Gobierno y ELN por un acuerdo humanitario
Date: 2020-04-30 15:36
Author: AdminContagio
Category: Actualidad, Comunidad
Tags: comunidades, ELN, Gobierno
Slug: por-tercera-vez-comunidades-insisten-a-gobierno-y-eln-por-un-acuerdo-humanitario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Cese-al-fuego-unilateral-por-parte-del-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: @AscamcatOficial {#foto-ascamcatoficial .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

A través de dos nuevas comunicaciones publicada por la[Comisión de Justicia y Paz](https://www.justiciaypazcolombia.com/carta-abierta-al-presidente-ivan-duque-acuerdo-humanitario-global-covid19/) este 29 de Noviembre, más de 113 comunidades, insistieron al ELN y al gobierno nacional que se prolongue el cese al fuego aplicado por esa guerrilla, pero también le pidieron al gobierno que se escuchen las voces que insisten por un tiempo de tranquilidad que les permita avanzar hacia acuerdos humanitarios en las regiones.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En las comunidades firmantes resaltan que el cese del ELN habría sido cumplido y por ello invitan a esa guerrilla a prolongar la decisión. Del mismo modo le insistieron, por tercera vez, y en nombre de la Virgen de Chiquinquirá, a quien Duque ha manifestado su devoción, para que los escuche y haga sus votos por un acuerdo humanitario y un cese global de hostilidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En esa comunicación al gobierno, las comunidades le recordaron al presidente que las operaciones militares que se desarrollan en sus territorios no han representado ningún tipo de tranquilidad y que por el contrario son signos de preocupación, así como los operativos de erradicación forzada y el riesgo que ellas representan por la permanencia de estructuras que se asientan en sus territorios y contra las cuales no hay operaciones ofensivas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Por la virgen de Chiquinquirá, piden al presidente un acuerdo humanitario

<!-- /wp:heading -->

<!-- wp:paragraph -->

Por el contrario le recordaron al jefe del gobierno colombiano que “Cesar en sus actuaciones ofensivas en nada significa abdicar de sus responsabilidades constitucionales, significa desarrollarlas en una condición de crisis humanitaria para proteger nuestras vidas y la de los propios integrantes de la fuerza pública.”

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

“Tampoco significa que si son atacados estén atados de pies y manos, y por tanto además de defenderse actuaran conforme al derecho internacional de los derechos humanos y el derecho internacional humanitario. Usted y todos asumimos riesgos, este riesgo es por la vida en la Vida y el derecho esperanza” (Le puede interesar: ["Comunidades piden cese al fuego y atención del Gobierno a problemas en los territorios"](https://archivo.contagioradio.com/comunidades-piden-cese-al-fuego-y-atencion-del-gobierno-a-problemas-en-los-territorios/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar:[Es la oportunidad de retomar diálogos con ELN: Defendamos La Paz.](https://archivo.contagioradio.com/es-la-oportunidad-de-retomar-dialogos-con-eln-dlp/)

<!-- /wp:paragraph -->

<!-- wp:html -->

[Carta de comunidades rurale...](https://www.scribd.com/document/459221587/Carta-de-comunidades-rurales-a-Duque#from_embed "View Carta de comunidades rurales a Duque on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" title="Carta de comunidades rurales a Duque" src="https://www.scribd.com/embeds/459221587/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-of0JcBTYyvZhn2BwgsI0" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="600" frameborder="0"></iframe>  
<!-- /wp:html -->

<!-- wp:html -->

[Carta de las comunidades al...](https://www.scribd.com/document/459218851/Carta-de-las-comunidades-al-ELN#from_embed "View Carta de las comunidades al ELN on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" title="Carta de las comunidades al ELN" src="https://www.scribd.com/embeds/459218851/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-Eu0qZRpmQHrMO3p8qaAS" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="600" frameborder="0"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
