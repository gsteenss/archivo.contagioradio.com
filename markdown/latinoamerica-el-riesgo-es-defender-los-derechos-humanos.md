Title: Latinoamérica, el riesgo es defender los derechos humanos
Date: 2015-01-27 15:25
Author: CtgAdm
Category: Carolina, Opinion
Tags: defensores de derechos humanos, Derechos Humanos, somos defesnsores
Slug: latinoamerica-el-riesgo-es-defender-los-derechos-humanos
Status: published

###### Foto: El Pueblo 

[[Carolina Garzón Díaz]](https://archivo.contagioradio.com/carolina-garzon-diaz/)([@E\_vinna](https://twitter.com/E_Vinna))

*En memoria de quienes han sido asesinados por defender los derechos humanos*

El informe más reciente de Amnistía Internacional se tituló “Defender derechos humanos en las Américas: necesario, legítimo y peligroso”, una titulación acertada. Efectivamente, las cifras y las historias coinciden en que **la defensa de los derechos humanos en los países de América Latina se ha convertido en una labor de alto riesgo para quienes la ejercen y para sus familias.** ¿Por qué son tan vulnerables los defensores y defensoras de DDHH? ¿Cuáles son los retos que enfrentan cotidianamente?

La Organización de Naciones Unidas define como defensor de derechos humanos “a la persona que, individualmente o junto con otras, se esfuerza en promover o proteger esos derechos”. Esta labor desarrolla desde la publicación de la Declaración Universal de los derechos humanos en 1948 y es la opción vital de cientos de hombres y mujeres en el mundo. Sin embargo, **su trabajo se ha convertido en blanco de sectores que, por razones económicas o políticas, han visto truncados sus planes** de poder, monopolio y/o aumento de su propio capital a costa de la vida de millones.

El riesgo para las personas defensoras de DDHH en Latinoamérica es constante y se agrava en contextos de conflicto armado, narcotráfico, guerras territoriales, abuso del poder y desarrollo de megaproyectos. Como señala Amnistía Internacional: “Ciertos grupos de defensoras y defensores son especialmente vulnerables a sufrir agresiones, incluidos quienes defienden derechos humanos en relación a: la tierra, el territorio y los recursos naturales; los derechos de las mujeres, las niñas, las personas LGBTI; y los derechos de las personas migrantes.”.

El pasado 21 y 22 de enero, durante la “Consulta Regional sobre políticas públicas para la protección y reconocimiento de personas defensoras de derechos humanos y la elaboración de una ley nacional modelo”, más de 40 defensores y defensoras de DDHH de Latinoamérica  señalaron que **la criminalización, las amenazas, la difamación, el homicidio, los secuestros y la violencia de género, son los mayores problemas que enfrentan los defensores de DDHH.** Dentro de los hostigamientos sobresalió el uso del sistema judicial para intimidar y reprimir la defensa de los derechos humanos.

En el caso de Colombia, el *Programa Somos Defensores* registró en el año 2013 más de 70 casos de defensores asesinados y más de 200 amenazados, y en el primer semestre de 2014 al menos 30 fueron víctimas de homicidios y más de 100 recibieron amenazas. Sin ir más lejos, **mientras escribo esta columna está siendo sepultado Carlos Alberto Pedraza**, un joven defensor de derechos humanos integrante del Congreso de los Pueblos y el MOVICE.

En todo el continente los defensores de DDHH tienen claridad sobre las agresiones que sufren y en la mayoría de los casos han identificado a sus responsables; sin embargo, otra trágica constante en la labor de los defensores es la desprotección estatal. Los Estados no han generado mecanismos para proteger y promover la labor de defensa de los DDHH, además, no ha sido eficiente en la investigación de los ataques en su contra. **La impunidad en los casos de agresiones contra personas defensoras de derechos humanos es prácticamente total.** A esta situación se suma la ausencia de medidas prevención de nuevos ataques.

No es fácil la situación que viven las personas defensoras de DDHH en Latinoamérica, y el panorama no es distinto en África, Asia y Europa. Por ello la invitación es a conocer más sobre esta labor, respetarla, hacerla valer, participar en ella, denunciar los ataques en su contra y especialmente entender la importancia de la defensa de los DDHH en la construcción de una sociedad justa y democrática.

#####  
