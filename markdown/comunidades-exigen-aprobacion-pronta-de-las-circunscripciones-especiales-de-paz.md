Title: Es urgente aprobar las Circunscripciones Especiales de Paz
Date: 2017-09-13 13:47
Category: Nacional, Paz
Tags: acuerdos de paz, Cámara de Representantes, Circunscripciones de paz, circunscripciones especiales de paz, comunidades, Congreso de la República
Slug: comunidades-exigen-aprobacion-pronta-de-las-circunscripciones-especiales-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/circunscripciones-de-paz-e1505328254509.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [13 Sept 2017] 

Las organizaciones  sociales, que representan a las  comunidades de diferentes territorios, le exigieron al Congreso de la República que agilicen el proceso de votación y **aprobación del Acto Legislativo que crea las 16 Circunscripciones Especiales de Paz**. Argumentan que de no ser aprobadas rápidamente, se estaría vulnerando su derecho a la participación política.

En rueda de prensa, los representantes de algunas organizaciones sociales como el Proceso de Comunidades Negras y la Ruta Pacífica de las Mujeres, entre otras, aseguraron que **el tiempo corre en contra** y aún queda por hacer procesos de pedagogía y cedulación masiva en los territorios que comprenden las circunscripciones.

Recordaron que es deber del Congreso de la República cumplir con lo estipulado en los acuerdos de paz y que **el 11 de noviembre inicia el periodo de inscripción** de las listas para las elecciones parlamentarias de marzo de 2018. Dicho periodo se cierra el 11 de Diciembre, lo que dejaría por fuera a los representantes territoriales puesto que no alcanzarían a inscribir sus candidatos y estarían en desventaja con las demás campañas que ya comenzaron (Le puede interesar: ["Circunscripciones de paz quedarían por fuera del fast track si no hay voluntad política"](https://archivo.contagioradio.com/circunscripciones-de-paz-se-quedarian-por-fuera-de-fast-track-si-no-hay-voluntda-politica/))

De no ser aprobadas estas circunscripciones, las comunidades que han sufrido en mayor medida el impacto del conflicto armado, **no tendrán representación en la Cámara** para el periodo que arranca en el próximo año. Aseguraron que "la apertura democrática requiere de garantías para la participación de las organizaciones y movimientos sociales".

### **Las comunidades exigen respeto por sus derechos civiles ** 

De acuerdo con Argenis Gamboa, líder del Proceso de Comunidades Negras, las circunscripciones especiales de paz **son la garantía para que se incluya a los territorios en las dinámicas de la democracia**. "Hemos sido excluidos, masacrados, y olvidados de manera histórica por lo que merecemos tener representación en el Congreso".

Le exigió a los representantes que reconozcan el espacio de una población que ha votado por ellos y que **tiene derecho a participar en la democracia**. Además manifestó que de ser necesario, saldrán a las calles a presionar por el reconocimiento de los derechos civiles que tienen las comunidades y que históricamente han sido acalladas. (Le puede ineteresar: ["Preocupaciones en torno a las circunscripciones de paz"](https://archivo.contagioradio.com/las-circunscripciones-de-paz-entre-el-narcotrafico-y-la-falta-de-estado/))

Dijo además que **hay congresistas que no están de acuerdo con que se establezcan están circunscripciones** porque "no es mentira que de ser aprobadas las circunscripciones de paz, es una piedra en el zapato para muchos congresistas que hoy están vendiendo los territorios".

### **En el senado y la Cámara hay departamentos que no tienen representación ** 

Por su parte, el representante por Bogota Alirio Uribe se comprometió a agilizar este proceso en la Cámara de Representantes. **Instó a las comunidades a establecer un plan para rodear el Congreso** y presionar para que se aprueben estas circunscripciones.

De igual forma reconoció que en Colombia se le ha violado el derecho a las comunidades de participar en las decisiones políticas que se toman. Dijo que **en el Senado hay 19 departamentos que no cuentan con representación** y que en la Cámara las subregiones que contemplan la circunscripciones especiales de paz nunca han estado representadas. (Le puede interesar: ["Con 60 votos, Senado le dio el sí a las circunscripciones especiales de paz"](https://archivo.contagioradio.com/44255/))

Finalmente el representante manifestó que **ha habido una mala información acerca de la naturaleza de este proyecto** en la medida que "algunos sectores han dicho que las cururles las van a ocupar los ex integrantes de las FARC, lo cual es totalmente falso".

Se espera que **en un plazo de 15 días** la Cámara de Representantes cumpla con el proceso de votación y aprobación del Proyecto de Acto Legislativo para poder establecer con claridad la forma como van a operar estas circunscripciones.

Las comunidades esperan también que **"se establezcan garantías especiales para que el ejercicio del voto sea libre** en estos territorios y de seguridad para las personas que se postulen".

<iframe id="audio_20863624" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20863624_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
