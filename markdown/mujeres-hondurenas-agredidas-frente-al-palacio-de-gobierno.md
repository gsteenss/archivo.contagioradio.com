Title: Mujeres hondureñas fueron agredidas frente al Palacio de Gobierno
Date: 2015-11-27 12:37
Category: El mundo, Otra Mirada
Tags: 25 de noviembre día de la no violencia contra la mujer, Centro de Derechos de la Mujer de Honduras CEM-H, CONADEH, Mujeres agredidas en Honduras
Slug: mujeres-hondurenas-agredidas-frente-al-palacio-de-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/dt.common.streams.StreamServer.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Archivo elheraldo.hn 

##### [27 Nov 2015]

En el marco de la conmemoración del día de la no violencia contra la mujer, 40 hondureñas fueron agredidas en el Centro de Derechos de la Mujer de Honduras CEM-H y la Red de mujeres de la Colonia Cruz Roja por un grupo contingente de policías y militares debido a la manifestación pacífica que se realizó frente a la casa Presidencial.

Suyapa Martínez, directora del Centro de Estudios de la Mujer Honduras, afirma que el objetivo fue visibilizar la problemática de violencia que enfrentan, un tema que se ha postergado y que el “Estado de Honduras no tome en serio este tema que deja consecuencias funestas contra la vida de las mujeres”.

Según Martínez durante la manifestación ubicaron una valla de seguridad para impedir el paso, luego los uniformados usaron gases lacrimógenos, en medio de la manifestación Merari Medina, joven de 15 años de la Colonia Cruz Roja, fue golpeada y otras mujeres, algunas embarazadas también fueron agredidas.

Cabe recordar que entre el mes de enero y septiembre de 2015, el Comisionado Nacional de Derechos Humanos CONADEH, atendió 2,571 quejas de mujeres, de las cuales 827 tienen que ver con amenazas a muerte, violencia doméstica, violencia intrafamiliar y violencia sexual, entre otras.
