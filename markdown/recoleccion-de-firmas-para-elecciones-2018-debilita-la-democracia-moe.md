Title: Los cuestionamientos a las candidaturas presidenciales por firmas según la MOE
Date: 2017-08-31 14:39
Category: Nacional, Política
Tags: elecciones 2018, Recolección de firmas
Slug: recoleccion-de-firmas-para-elecciones-2018-debilita-la-democracia-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/firmas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paola Holguin] 

###### [31 Ago 2017] 

Según la MOE, la recolección de firmas para las candidaturas presidenciales representa varios problemas para el ejercicio democrático en Colombia. Por una parte se pueden ver con un inicio de campaña anticipada, como en el caso de Germán Vargas Lleras, y por otros no **hay reglas de juego claras en torno a los aportes monetarios o los entes que pueden controlar las irregularidades que se puedan presentar en torno a financiación**.

Para las elecciones del 2018 diferentes personajes de la política colombiana ya han anunciado su candidatura, sin embargo, no lo han hecho a través de partidos políticos, para Camilo Mancera, coordinador del equipo jurídico de la Misión de Observación Electoral, **esta decisión no solo es una amenaza a los parámetros que deben tener las campañas, sino que también afecta al ejercicio de la política**.

### **¿Hay una crisis de los partidos políticos?** 

Mancera aseguró que para la MOE esta crisis de representatividad que afrontan los partidos políticos, si es una preocupación en la medida de la falta de regulación cuando un candidato se presenta vía recolección de firmas, “al utilizar esta estrategia básicamente lo que **se está haciendo es adelantar campaña, es una manera de afectar la norma sin que se inicien procesos de investigación**”.

Sumado a ello Mancera afirmó que, esta forma de recolección de apoyos genera inequidad en la contienda electoral, debido a que en el momento en el que **se posiciona un nombre, inicia una financiación de esta campaña anticipadamente,** sin que exista un control.

La MOE ha sido reiterativa en señalar que debe desarrollar una reglamentación en torno a los grupos significativos de ciudadanos, en puntos tan importantes como a quienes pueden apoyar, cómo deben manejar la campaña, **cómo se hace la financiación, cómo deben reportar los gastos y cómo debe realizarse la publicidad de la misma**.

“Una persona que este recolectando firmas puede llenar de vayas la ciudad, no existen topes de gastos de recolección para esta forma de campaña” afirmó Mancera. (Le puede interesar: ["Circunscripciones de paz podrían quedarse fuera del Fast Track por falta de voluntad política"](https://archivo.contagioradio.com/circunscripciones-de-paz-se-quedarian-por-fuera-de-fast-track-si-no-hay-voluntda-politica/))

### **Los riesgos de la recolección de firmas** 

Mancera expresó que después de la recolección de firmas por parte del candidato, con la idea de romper el imaginario de pertenecer a algún partido político, no existe ningún impedimento para que el mismo decida hacer coaliciones con los partidos, “se crea un organismo independiente porque **supuestamente los partidos no satisfacen al candidato, para que después se den coaliciones**”.

Situación que tendría un agravante cuando el candidato hace parte de un partido y se desliga de el en periodo de elección, como lo sería Vargas Lleras, de acuerdo con Mancera “en este momento es mucho más eficiente irse por un grupo de ciudadanos que por un partido político, al menos por ahora, porque no sabemos si en noviembre o diciembre se cambie de posición y allá un aval de partido”.

### **Los efectos de la recolección de firmas en la sociedad** 

Una de las dudas que ha surgido con las recolecciones de firmas, **es sí las personas puedan firmar diferentes listas para apoyar a más de un candidato**, sobre este punto Mancera afirmó que las personas pueden firmar las veces que quieran y por los candidatos que quieran.

Sin embargo, aclaró que, en las recolecciones de firmas, no se está aprobando ni respaldando un proyecto político, **sino un nuevo estilo de caudillismo** “este es un fenómeno cíclico en el que por la desacreditación de los partidos se buscan candidaturas independientes representadas más en personas, que debilitan aún más a los partidos” afirmó Mancera. (Le puede interesar: ["Seremos la alternativa de un nuevo poder territorial: Iván Cépeda"](https://archivo.contagioradio.com/45797/))

<iframe id="audio_20628180" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20628180_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
