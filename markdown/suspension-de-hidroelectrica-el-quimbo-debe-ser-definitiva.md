Title: Suspensión de Hidroeléctrica El Quimbo "debe ser definitiva"
Date: 2015-12-16 15:09
Category: Ambiente, Nacional
Tags: ASOQUIMBO, Corte Constitucional, El Quimbo, EMGESA, Hidroeléctrica El Quimbo, Huila
Slug: suspension-de-hidroelectrica-el-quimbo-debe-ser-definitiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/quimbo1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radiomacondo 

###### [16 Dic 2015] 

La comunidad teme que Emgesa, la empresa que maneja el Proyecto de la Hidroeléctrica El Quimbo, tome medidas jurídicas para que se reverse esta decisión, apelando a que se afecta la confianza inversionista. La construcción de la hidroeléctrica, ha dejado más de 12 mil familias desplazadas, no ha reparado a los pobladores que se quedaron sin tierras, y de las 2.900 hectáreas de terrenos que debían darse a los campesinos no se ha entregado ninguna.

Esta decisión se hace en el marco de una medida provisional, por lo que para Jenniffer Chavarro, integrante de la organización Afectados por El Proyecto Hidroeléctrico El Quimbo (Asoquimbo), “se deben adoptar medidas de fondo para que la sanción siga en pie”.

La activista agrega que es importante que se tengan en cuenta las consecuencias medioambientales del funcionamiento de la represa. Sin embargo, agrega que “el proyecto debe suspenderse y además tiene que cumplir con la reparación de los daños sociales causados”.

La comunidad actualmente exige que sean incluidas en el censo de afectados por la represa las más de 36 mil personas que se declaran víctimas de este megaproyecto y además, exigen que un organismo independiente sea el que garantice que la decisión de la Corte Constitucional se cumpla.

La multinacional Emgesa, quien es la propietaria del Proyecto Hidroeléctrico El Quimbo en el Huila, suspendió la prestación del servicio de energía en virtud a una sentencia dictada por la Corte Constitucional, que anuló el decreto 1759 del 6 de octubre de 2015, mediante el que el presidente Santos había autorizado el inicio de generación de energía eléctrica en esa represa pese a que la Corporación Autónoma del Magdalena denunció que no se había removido la biomasa en 1.000 hectáreas del vaso del embalse. Este era uno de los requisitos para iniciar trabajos, porque se podrían provocar daños ambientales irreparables.
