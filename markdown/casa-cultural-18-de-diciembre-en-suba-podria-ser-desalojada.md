Title: Casa cultural 18 de diciembre en Suba podría ser desalojada
Date: 2015-05-29 13:14
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Casa cultural 18 de dic., DDHH, desalojo en Suba, Desalojos Forzados, ESMAD, Suba
Slug: casa-cultural-18-de-diciembre-en-suba-podria-ser-desalojada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/Foto-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Casa Cultural 

<iframe src="http://www.ivoox.com/player_ek_4569822_2_1.html?data=lZqjm52Wdo6ZmKiakp2Jd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbnwtHczNSPpYzawtLWzs7Fb8bijKjc1M7SuNCZk6iYtdrGpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [                                                              [       Antonio Torres, Casa Cultural 18 de Diciembre]]

En el barrio Corinto de la localidad de Suba, Antonio Torres y su familia tienen una amenaza de desalojo en la que se señala que el próximo 5 de Junio de este año deberán entregar su inmueble. Esta casa se ha convertido en un referente para la comunidad, puesto que desde hace más de un año funciona como la **"Casa Cultural 18 de Diciembre"[.]**

El nombre y la historia de esta casa cultural se remonta a que el 18 de diciembre del 2013 se inició el proceso de desalojo por parte de la Inspección 11, sede Policía de Suba. "*Vino una tanqueta y la fuerza disponible, usando gases lacrimógenos; el chorro de agua de la tanqueta; piedras que nos lanzaron los agentes de la policía a tratar de desalojarnos de la casa, pero ese día logramos impedir que fuéramos desalojados*", asegura Torres.

\[caption id="attachment\_9391" align="alignleft" width="315"\][![Foto: Casa Cultural 18 de Diciembre](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/mm.jpg){.wp-image-9391 width="315" height="236"}](https://archivo.contagioradio.com/casa-cultural-18-de-diciembre-en-suba-podria-ser-desalojada/mm/) Fuerzas de la policía intentan desalojar familia\[/caption\]

A partir de la orden de desalojo, diversos **colectivos y organizaciones sociales han hecho del inmueble una “Casa Cultural”**, espacio en que **se realizan procesos de formación y organización en agricultura urbana, artes y procesos educativos**, "*convirtiéndose en un referente de comunidad y organización*", afirma el joven.

El desalojo se estaría efectuando, según informa la Inspección, porque el inmueble habría sido vendido a otras personas, tras el presunto no pago del préstamo con el cual se compró la casa. Sin embargo, Antonio asegura que **su madre realizó el pago a la abogada del prestamista, quien nunca lo reportó**.

Los y las habitantes del inmueble solicitan a las autoridades **prestar atención a las denuncias jurídicas a la Fiscalía y a la Personería**, en el marco de las irregularidades del pago del préstamo, y al mismo tiempo por el **desproporcionado uso de la fuerza**. "Nos preocupa que nuevamente se dé una situación de esas, y que ya no vulnera solamente el derecho a la vivienda de nuestra familia, sino que también **pone en riesgo los procesos sociales que se han tejido hoy desde la casa**", agrega Torres.
