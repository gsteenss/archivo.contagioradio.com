Title: "No solo las balas matan, también el glifosato y la indiferencia de los gobernantes"
Date: 2015-07-30 17:09
Category: DDHH, Nacional
Tags: aspersiones aéreas, Cultivos de uso ilícito, fumigaciones con glifosato, Juan Manuel Santos, Perla Amazónica, Puerto Asís, río Putumayo, Zona de reserva Campesina del Putumayo
Slug: no-solo-las-balas-matan-tambien-el-glifosato-y-la-indiferencia-de-los-gobernantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/image.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[primiciadiario.com]

<iframe src="http://www.ivoox.com/player_ek_5595063_2_1.html?data=lpqml5Wad46ZmKiakpqJd6Kpk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMbmzcaYo9LFvoa3lIqvldPNp8Kfz9rS2MbRqc%2FoxpDS1ZDaaaSnhqaxxdnNscKfxcqYyNrRrcjVxM7c0MrXcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sandra Lagos, ZRC Perla Amazónica] 

La Zona de Reserva Campesina de la Perla Amazónica sufrió una nueva aspersión aérea con glifosato, según cuenta la comunidad el martes hacia las 11:00 de la mañana, **hubo sobre vuelo por más de 30 minutos, de avionetas y helicópteros** que fumigaron algunas fincas de la comunidad.

Específicamente cuatro familias que habitan en la región fueron las que se vieron más afectadas por la aspersión que trajo graves consecuencias sobre **sus cultivos de plátano, maíz, pastos, especies forestales, fuentes de agua y además el río Putumayo.**

Precisamente, cuenta Sandra Lagos, quien hace parte de la comunidad de la ZRC Perla Amazónica, que algunos de los cultivos afectados hacen parte de iniciativas productivas como plan de sustitución alternativa a los cultivos de usos ilícito.

Las familias están desconcertadas y preocupadas por su economía y la seguridad alimentaria de la comunidad, cuenta Sandra.

Este miércoles el Presidente Juan Manuel Santos visita  Puerto Asís, por lo que las organizaciones sociales quieren aprovechar esta oportunidad para hablar con el primer mandatario sobre este problemática, sin embargo, según Lagos, **no hay ningún interés por parte del jefe de Estado de escuchar a la población.**

La denuncia de las organizaciones sociales asegura que “la operación de la policía antinarcóticos  viola el **derecho a la alimentación, al ambiente sano, a la salud y desconoce la decisión reciente del gobierno** nacional de suspender el uso de ese químico conforme a las recomendaciones de la OMS”.

La habitante de la Perla Amazónica, señala que con estos actos es muy difícil que se reactive la mesa de negociaciones con el gobierno, y afirma que estas acciones son incoherentes y generan que nuevamente la población sea vea obligada a salir a las calles a manifestarse en contra de las actuaciones del gobierno, **“no solo las balas matan, el glifosato y la indiferencia de los gobernantes también mata”,** concluye Sandra.
