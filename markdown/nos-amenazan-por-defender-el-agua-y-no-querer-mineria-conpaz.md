Title: "Nos amenazan por defender el agua y no querer minería" CONPAZ
Date: 2015-02-12 21:09
Author: CtgAdm
Category: DDHH, Nacional
Tags: Cauca, Mineria, Sucre
Slug: nos-amenazan-por-defender-el-agua-y-no-querer-mineria-conpaz
Status: published

###### Foto: [utopialapalabra.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_4076068_2_1.html?data=lZWkmJWafI6ZmKiakpuJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmr9TgjcbRqc%2FV28bbjdXTtozYxsvS0MnJtozZzZDOydrFb9qfz9SY09rJtsbmjNLW0MrWaaSnhqaxw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Chilito] 

**Líderes y lideresas socio – ambientales, que trabajan por la protección y defensa del agua, los bosques y las siembras, fueron amenazados de muerte** por medio de panfletos que desconocidos dejaron en el barrio, Siloé del departamento del Cauca.

El hecho sucedió este 10 de febrero, cuando habitantes del sector encontraron en las calles un escrito donde se amenaza de muerte a líderes campesinos y a sus familias, a quienes **involucran  con las guerrillas de las FARC, el ELN y los señalan de politiqueros**.

Una de las organizaciones campesinas amenazadas es la "Asociación Bien Andantes" que hace parte de la red **CONPAZ.** Carlos Chilito, uno de sus integrantes, afirma que ellos **trabajan en contra de la minería y no comparten aspectos de las políticas públicas de los mandatarios**, es por eso, que ven probable que las amenazas se hayan dado debido a que cada vez las comunidades se organizan más por defender sus territorios de las actividades extractivas y mineras de esa región, teniendo en cuenta que  **casi el 80% del territorio de Sucre está concesionado a mineras.**

De acuerdo a la Comisión de Jusiticia y Paz, el panfleto que no está firmado, apareció en medio de “la apuesta comunitaria por una Consulta Popular frente a la Minería que afectaría los intereses de las empresas **CONTINENTAL GOLD LTDA, NEGOCIOS MINEROS, PALMA SOM con traspaso a CGL DOMINICAL SAS”.**

Por el momento los líderes y lideresas, se encuentran en el proceso de realización de la denuncia con la personería, según indicó Carlos Chilito.
