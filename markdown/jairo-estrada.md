Title: Ahora Congreso de la República podrá re negociar los acuerdos de Paz
Date: 2017-05-18 16:41
Category: Entrevistas, Paz
Tags: Demanda ante la Corte Constitucional, voces de paz
Slug: jairo-estrada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/jairo-estrada-e1511562950155.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [18 May 2017] 

Como preocupante calificó Jairo Estrada, representante de Voces de Paz ante el senado, la decisión de la Corte Constitucional que modifica la forma de aprobar los acuerdos de paz y quem permite **re negociar los acuerdos en un momento crítico de la implementación a 13 días de que se cumpla el día D +180**.

“Se activarán allí todas las tácticas dilatorias, **todas las tácticas de bloqueo y saboteo que se intentaron activar durante todos estos meses y que no han logrado fructificar**” afirmó Estrada frente a la capacidad que el fallo de la Corte le otorga a los congresistas que votaran uno a uno los artículos del acuerdo de paz.

Voces de Paz, en este nuevo escenario tendrá que afrontar el reto de exigirse mucho más en su labor parlamentaria “**tendremos que velar con mayor fidelidad y fuerza porque el espíritu y la letra de los acuerdos se cumpla** y tratar en el debate de contrarrestar las tendencias que pretendan renegociar los acuerdos”. Le puede interesar:["Con movilizaciones y pactos políticos haremos frente a las decisiones de la Corte"](https://archivo.contagioradio.com/con-movilizaciones-y-pactos-politicos-haremos-frente-a-decision-de-la-corte-ivan-cepeda/)

De igual forma señala que es alarmante que esta decisión se dé cuando aún faltan debates trascendentales para la implementación de los acuerdos como lo es el proyecto de ley estatutaria que organiza y genera las condiciones **para la puesta en marcha de la Jurisdicción Especial para la Paz, entre otras**. Le puede interesar: ["Corte Constitucional congela acuerdo de paz con las FARC: Enrique Santiago"](https://archivo.contagioradio.com/corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago/)

Estrada señaló que el mensaje que se envía a la sociedad con esta resolución "hace referencia hacia las dificultades que hay en el país para avanzar en la consolidación de la paz y al hecho de que estructuralmente, por la forma en la que está **organizada la sociedad y el Estado, siempre se activan resistencias con pretensiones de cambio**”.

<iframe id="audio_18785064" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18785064_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
