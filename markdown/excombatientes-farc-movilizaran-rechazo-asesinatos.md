Title: Reincorporados de FARC se movilizarán para exigir el derecho a la vida y la paz
Date: 2020-10-15 12:45
Author: AdminContagio
Category: Actualidad, DDHH
Tags: asesinato de excombatientes, FARC, Movilización social
Slug: excombatientes-farc-movilizaran-rechazo-asesinatos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/bandera-farc.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Partido FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como respuesta a los hechos de violencia que han derivado en el asesinato de 49 excombatientes a lo largo del 2020, una comisión conformada por lideres y lideresas regionales del partido FARC en las diversas regiones del país adelanta los detalles de lo que **será una gran movilización para el mes de noviembre** en rechazo de lo que han denominado un extermino sistemático contra los firmantes de paz. [(Lea también: Firmante de paz, Plutarco Cardozo fue asesinado en cercanías de ETCR en Guaviare)](https://archivo.contagioradio.com/plutarco-cardozo-asesinado-guaviare/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Durante el mes de mayo, el Partico FARC ya había expresado que se apelaría a la Comisión Interamericana de Derechos Humanos, (CIDH) con el fin de interponer medidas cautelares a favor de los firmantes de paz y solicitar al organismo internacional, interceda para que el Estado colombiano adopte medidas para proteger **a los más de 12.000 firmantes del acuerdo que han manifestado no cuentan con garantías de vida.** [(Le puede interesar: Partido FARC solicitará formalmente medidas cautelares de la CIDH a favor de excombatientes)](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No existen garantías denuncia el partido FARC

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según el Partido FARC ya son 232 los excombatientes asesinados desde la firma del Acuerdo de Paz, mientras las Naciones Unidas (ONU) a través de su informe trimestral que realiza en medio de la verificación del acuerdo se acerca al registro de 224 firmantes de paz víctimas de homicidio y de 20 que han sido víctimas de desaparición. **Cauca, Meta, Caquetá, Huila, Antioquia, Chocó, Valle del Cauca, Putumayo y Nariño son algunas de las zonas donde más se evidenca este fenómeno.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Además del alto número de personas asesinadas o amenazadas, una de las problemáticas que afianzan los hechos violentos y que denuncia el [partido](https://twitter.com/PartidoFARC) es la falta de esclarecimiento de los crímenes cometidos. Pese a que no existen cifras oficiales del avance de las investigaciones, integrantes del partido FARC aseguran que la impunidad alcanza el 90%.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
