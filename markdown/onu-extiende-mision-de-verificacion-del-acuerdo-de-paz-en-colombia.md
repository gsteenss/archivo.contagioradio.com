Title: ONU extiende Misión de Verificación del Acuerdo de Paz en Colombia
Date: 2020-09-26 10:59
Author: AdminContagio
Category: Actualidad, Nacional
Tags: acuerdo de paz, Consejo de Seguridad de la ONU, Misión de Verificación, ONU
Slug: onu-extiende-mision-de-verificacion-del-acuerdo-de-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Consejo-de-Seguridad-de-la-ONU-extiende-Mision-de-Verificacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este viernes el Consejo de Seguridad de la Organización de Naciones Unidas -[ONU](https://www.un.org/es/)-, en una decisión unánime, **extendió hasta el 25 de septiembre del año 2021 el mandato de la Misión de Verificación del Acuerdo de Paz en Colombia.** (Le puede interesar: [Consejo de Seguridad de la ONU llama a un cese al fuego global](https://archivo.contagioradio.com/consejo-de-seguridad-de-la-onu-llama-a-un-cese-al-fuego-global/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MisionONUCol/status/1309600532210823168","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/MisionONUCol/status/1309600532210823168

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**La Misión de Verificación viene operando desde el año 2017,** asumiendo la labor de monitorear y hacer seguimiento a la implementación del Acuerdo de Paz firmado entre el Gobierno de Colombia y las Fuerzas Armadas Revolucionarias de Colombia -FARC-.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Particularmente, la Misión de Verificación monitorea, entre otras cosas, la reincorporación de los excombatientes de FARC, las garantías jurídicas y de seguridad para su reincorporación; y algunos asuntos transversales como el enfoque de género.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sobre estas gestiones, la Misión entrega informes trimestrales al Consejo de Seguridad de la ONU los cuales dan cuenta del estado de la implementación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El más reciente informe fue emitido el pasado mes de julio, en este **la Misión hizo un llamado de atención por la alta cifra de asesinatos de líderes sociales y firmantes del Acuerdo** **e instó al Gobierno a adoptar medidas inmediatas y concretas para hacer frente estas problemáticas.** (Le puede interesar: [«Crímenes contra excombatientes de FARC son producto de la acción y omisión del Estado»](https://archivo.contagioradio.com/crimenes-contra-excombatientes-de-farc-son-producto-de-la-accion-y-omision-del-estado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La decisión de extender el periodo de mandato se da por el compromiso de la ONU con el desarrollo de la Paz en Colombia, y por la solicitud de varias plataformas defensoras de Derechos Humanos en el país y del Gobierno Nacional que frente a este respecto elevó una comunicación el pasado 1 de septiembre de 2020. (Le puede interesar: [Plataformas de DD.HH. solicitan a la ONU permanencia de Misión de Verificación](https://archivo.contagioradio.com/plataformas-de-dd-hh-solicitan-a-la-onu-permanencia-de-mision-de-verificacion/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
