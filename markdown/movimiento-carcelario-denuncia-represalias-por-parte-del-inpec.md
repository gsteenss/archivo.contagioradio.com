Title: Movimiento Carcelario denuncia represalias por parte del INPEC
Date: 2020-03-24 17:07
Author: CtgAdm
Category: Actualidad, DDHH
Tags: carcel, INPEC
Slug: movimiento-carcelario-denuncia-represalias-por-parte-del-inpec
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/cárcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El movimiento Carcelario denuncia una serie de represalias por parte del INPEC, que vulneran los derechos humanos de la población carcelaria. Hechos como traslados arbitrarios, y suspensión del derecho al agua, son algunas de las medidas que se estarían dando en contra de la población privada de la libertad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la cárcel de Cúcuta, los prisioneros denuncian que la guardia del INPEC disparó con armas de fuego a los reclusos que protestaban, debido a que ninguna de las medidas anunciadas por Duque se han puesto en marcha. (Le puede interesar:["El Estado debe tomar medidas idóneas para proteger la población carcelaria ante la pandemia del COVID-19")](https://www.justiciaypazcolombia.com/colombia-el-estado-debe-tomar-medidas-idoneas-para-proteger-la-poblacion-carcelaria-ante-la-pandemia-del-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, en la cárcel La Modelo, los prisioneros completan tres días sin agua, producto de los amotinamientos del pasado 21 de marzo, en donde, luego del accionar desmedido del INPEC, fueron asesinadas 23 personas y más de 80 resultaron heridas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma las prisioneras de la cárcel El Buen Pastor en Bogotá, denunciaron que el director del centro penitenciario Wilson León **amenazó a luz Deyanira Herrera Triviño, prisionera política** quien protestó por la falta de medidas frente al Covid 19 en este lugar de reclusión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sumado a estos hechos, está la desaparición de 8 prisioneros políticos, **cuatro pertenecientes al partido político FARC, que se encontraban en la cárcel La Picota** identificados como **Moises Quintero**, José Ángel Parra, Oscar Rodríguez y Luis Fernando Franco.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y tres mujeres víctimas del montaje judicial conocido como caso Andino, identificadas como Lizeth Rodríguez, Lina Jiménez y Alejandra Mendez, de quienes actualmente se desconoce el paradero. (Le puede interesar: ["Se desconoce paradero de prisioneros políticos luego de traslados arbitrarios"](https://archivo.contagioradio.com/se-desconoce-paradero-de-prisioneros-politicos-luego-de-traslados-arbitrarios/))

<!-- /wp:paragraph -->
