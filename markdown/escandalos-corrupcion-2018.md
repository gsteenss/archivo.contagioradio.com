Title: 6 escándalos de corrupción en 2018
Date: 2018-12-24 15:54
Author: AdminContagio
Category: Nacional, Política
Tags: Cartel, corrupción, Grupo Aval, Nestro Humberto Martínez
Slug: escandalos-corrupcion-2018
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Nestor-Humberto-Martinez-e1468345311281.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [21 Dic 2018] 

Según cifras de la Procuraduría General de la Nación, la corrupción le cuesta a los colombianos cerca de **9 billones de pesos al año**; durante 2018 los escándalos alrededor de este flagelo también fueron noticia, algunos tienen una historia que poco a poco se ha conocido, sobre otros aún se sabe muy poco.

### **Caída del puente de Chirajara** 

A principio de año, el 15 de enero, los medios del país registraron la caída de la mitad del puente de Chirajara. Del accidente **resultaron muertos 9 obreros,** adicionalmente, fue necesario derribar parte de la estructura que tenía un costo superior a los 70 mil millones de pesos. Según Coviandes, constructora del proyecto y filial de Corficolombiana, el puente tuvo errores de diseño, que causaron el accidente.

Aunque **el nuevo puente del Chirajara tendrá un costo aproximado de 96 mil millones de pesos, la multa que pagó Coviandes fue de apenas 8.400 millones;** a ello se suma que en la investigación sobre el accidente, la firma Modjeski and Master aseguró que la construcción no tuvo la debida cimentación, ni contó con pruebas de túnel de viento, que aunque no son obligatorias, pueden ser determinantes en el diseño de la obra.

Recientemente "La W" publicó unos documentos en los que se muestra que el diseñador del puente de Chirajara es el mismo del Puente Hisgaura; no obstante, las investigaciones por parte de la Fiscalía no han avanzado tanto como las de los medios empresariales y por lo tanto, **aún no se han establecido responsabilidades por lo ocurrido con el Chirajara**. (Le puede interesar:["¿Quién responde por la tragedia del puente Chirajara?"](https://archivo.contagioradio.com/puente_chirajara_villavicencio_responsables_tragedia/))

> Interponen tutela a empresa constructora del puente Hisgaura <https://t.co/UgOhSBORKe> [pic.twitter.com/0g4zVfIN85](https://t.co/0g4zVfIN85)
>
> — América Hoy (@AmerHoy) [18 de diciembre de 2018](https://twitter.com/AmerHoy/status/1075115515427708928?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **Un fiscal anti-corrupción corrupto** 

El pasado 27 de junio de 2017 fue capturado el entonces Fiscal Anti-Corrupción Luis Gustavo Moreno gracias a una circular roja emitida por Interpol. **Moreno fue extraditado a Estados Unidos, sin que se permitiera su colaboración con la justicia**, y allí está a la espera de su condena por los delitos de lavado de activos, fraude electrónico y conspiración para lavar dinero.

Moreno está relacionado con el denominado 'Cartel de la Toga', empresa criminal que operaba junto al abogado Leonardo Luis Pinilla, en la que desviaban o dilataban investigaciones contra congresistas y funcionarios de la Corte Suprema de Justicia a cambio de dinero. El ex-fiscal también fue condenado en Colombia por los delitos de utilización indebida de información oficial privilegiada y concusión, **pero las investigaciones sobre el Cartel no han avanzado**.

### **Alejandro Lyons, ser corrupto paga** 

El gobernador de Córdoba entre 2012 y 2015 está también relacionado con el "Cartel de la Toga", así como por irregularidades en el uso de recursos para la salud en el Departamento, la destinación de recursos de regalías para Ciencia y Tecnología y el Cartel de la Hemofilia. Lyons fue encontrado culpable por los delitos de concierto para delinquir, interés indebido en la celebración de contratos e incremento injustificado de su patrimonio.

Por estos hechos la Procuraduría decidió inhabilitarlo por 15 años para ocupar cargos públicos, por su parte,  **la Corte Suprema de Justicia condenó a Lyons a pagar 5 años de cárcel y 4 mil millones de pesos de multa**. En su momento, la Procuraduría y la Contraloría criticaron la multa, puesto que **la Fiscalía había tazado el desfalco a la Gobernación en cerca de 87 mil millones de pesos**.

### **Los Carteles que tienen vigencia en 2018** 

Durante la alcaldía de Samuel Moreno se descubrió el primer Cartel, el llamado "Carrusel de la Contratación", forma de corrupción en la que se obtienen recursos de las arcas públicas gracias a licitaciones que resultan en fraudes; en 2018 la fórmula del Cartel ha evolucionado y aún  presenta nuevas formas.

En este año, la justicia ordenó cárcel para las 4 personas investigadas por **Cartel de los Pensionados**, modalidad en la que defraudaron el patrimonio de Colpensiones mediante la Jurisdicción Laboral y según la Fiscalía, el monto total del **desfalco ascendería a más de 3 mil millones de pesos**. De igual forma, a final de este año se supo la condena de 10 años contra Pedro Aguilar por el Cartel de la Chatarrización, acto con el que se desfalco al Estado en más de 600 mil millones.

A la condena de Lyons por el **Cartel de la Hemofilia, se sumó la llegada al país de Samy Spath Storino**, conocido también como "el hombre de la tula", quien se encargaba de entregar parte de los sobornos a funcionarios públicos y contratistas para el funcionamiento de este Cartel. Spath estaba escondido en Italia, pero en mayo fue encontrado por las autoridades de ese país y deportado a Colombia para responder por la malversación de **42 mil millones de pesos**, destinados para tratamientos médicos.

A mediados de 2018 la Superintendencia de Industria y Comercio (SIC) brindó más información sobre el Cartel de los Pañales, uno de los casos a los que venía haciendo seguimiento desde noviembre de 2017. Según la SIC, **Tecnoquímicas, Tecnosur, Colombiana Kimberly Colpapel, Productos Familia y Drypers Andian, se pusieron de acuerdo entre 2000 y 2013 para pactar precio y calidad de los pañales**, cometiendo el delito de colusión.

### **Corrupción en las elecciones de 2018** 

Previo a los comicios electorales, y siguiendo su carácter declaratorio, el fiscal Nestor Humberto Martínez, afirmó que tenía pruebas de graves hechos de corrupción que involucraron a congresistas, aunque aseveró que no daría más información hasta que terminarán las elecciones para evitar interferir en las mismas.

Luego de varias semanas, y tras salir electo Iván Duque como presidente de la república, el Fiscal anunció que Aída Merlano y Lilibeth Llinás de Cambio Radical, Maria Fernanda Cabal, Diego Caro y Margarita Restrepo del Centro Democrático y Faber Muñoz del Partido de la U, entre otros congresistas, **están siendo investigados por compra de votos o presión al votante**.

De estas declaraciones, el caso que más ha avanzado es el de Aída Merlano, quién está recluida en la cárcel el Buen Pastor de Bogotá y se enfrenta a la pérdida de investidura por la presunta compra de votos, fraude electoral y violación de topes de campaña. (Le puede interesar: ["Los 4 partidos políticos inmersos en el escándalo de compra de votos"](https://archivo.contagioradio.com/los-4-partidos-politicos-inmersos-en-escandalo-de-compra-de-votos/))

### **Odebrecht, Pizano y el fiscal Nestor Humberto Martínez** 

El más sonado caso de corrupción de Latinoamérica volvió a tener relevancia en Colombia durante el último trimestre del año por cuenta de la muerte de Jorge Enrique Pizano, uno de los principales testigos en el caso de corrupción de la Ruta del Sol II. El desfalco del Estado colombiano que involucra a la multinacional brasileña está avaluado en cerca de 25 mil millones de pesos. (Le puede interesar: ["Fiscalía Ad Hoc es una farsa: Jorge Robledo"](https://archivo.contagioradio.com/fiscalia-ad-hoc-farsa/))

No obstante, fue la revelación de audios de conversaciones entre Pizano, ex-controler del proyecto Ruta del Sol II y el abogado de la firma concesionaria del proyecto (Corficolombiana) y hoy fiscal general de la nación, Nestor Humberto Martínez, **en los que se probaba que él sabía del escándalo de corrupción**. A ello se sumó la aún no resuelta muerte de Pizano por un aparente envenenamiento con cianuro, y de su hijo por el mismo motivo.

Aunque la oposición citó a un debate de control político para pedir la renuncia del Fiscal, la aparición en la plenaria de un vídeo con un supuesto hecho de corrupción cometido por el senador Gustavo Petro disipó toda la atención sobre el caso; y mientras se han aclarado los hechos relacionados al vídeo, la situación del Fiscal sigue sin resolverse. Aún peor, **Martínez no ha respondido por haber nombrado a Moreno como fiscal anti-corrupción, por la denuncia sobre presuntos bienes no declarados en otros países, ni sobre  el caso Odebretch**.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
