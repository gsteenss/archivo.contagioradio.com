Title: Objetivo Libertad: una campaña por las y los prisioneros políticos
Date: 2020-11-26 09:31
Author: AdminContagio
Category: Expreso Libertad
Tags: #Fiscalía, #Montajesjudiciales, #ObjetivoLibertad, #Prisionerospolíticos
Slug: objetivo-libertad-una-campana-por-las-y-los-prisioneros-politicos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/objetivo-libertad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Con el lanzamiento de la campaña Objetivo Libertad, que busca visibilizar la situación de las y los prisioneros políticos en Colombia, una alarmante cifra salió a la luz: hay más de 45 montajes judiciales en contra de personas, que no han contado con garantías jurídicas para sus procesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta campaña, que agrupa a distintas víctimas de montajes judiciales, familias y organizaciones defensoras de derechos humanos, pretende que este tipo de hechos tengan mayor rechazo al interior de la sociedad Colombiana, y **se esclarezcan las responsabilidades detrás de los mismos**. (Le puede interesar: ["Objetivo Libertad"](https://www.facebook.com/ObjetivLibertad))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del Expreso Libertad, Érka Aguirre, víctima del montaje judicial conocido como el caso Lebrija; César Barrera, padre de César, víctima del caso Andino; y Karen Cortéz, amiga de Angie Solano, víctima del montaje judicial de la Escuela General Santander, denunciaron el conjunto de irregularidades en estos mal llamado falsos positivos judiciales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, sañalaron la urgencia por movilizarse en torno a estos casos, debido al aumento de los mismos durante el gobierno del actual presidente Iván Duque, y a **la ausencia de garantías por parte del Estado en los procesos judiciales.** (Le puede interesar: "[La lucha de Lina Jiménez, víctima de montaje judicial de caso Andino, por su libertad](https://archivo.contagioradio.com/la-lucha-de-lina-jimenez-victima-de-montaje-judicial-de-caso-andino-por-su-libertad/)")

<!-- /wp:paragraph -->

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Ffb.watch%2F1-ujgQXvT2%2F&amp;show_text=false&amp;width=734&amp;appId=1237871699583688&amp;height=413" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
