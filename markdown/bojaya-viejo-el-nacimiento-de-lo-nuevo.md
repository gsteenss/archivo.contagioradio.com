Title: Bojayá Viejo, el nacimiento de lo Nuevo
Date: 2015-12-14 09:05
Category: Camilo, Opinion
Tags: Acto de Perdón en Bojayá, Bojaya, FARC, pastor alape, proceso de paz
Slug: bojaya-viejo-el-nacimiento-de-lo-nuevo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/perdon-farc-bojaya.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONUDerechosHumanos] 

#### **[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)- [~~@~~CamilodCasas](https://twitter.com/CamilodCasas)  ** 

###### [14 Dic 2015] 

Lo sublime se experimenta, está en el nivel del sentimiento, de aquello que se hace inenarrable, aquello que se atrapa parcialmente en palabras o en imágenes, desbordando lo bello, lo verdadero. Lo ocurrido el 6 de diciembre en el llamado Bojayá viejo fue eso, algo sublime, no puede agregarse el absoluto, eso sobra.

El **[acto de reconocimiento de responsabilidad de las FARC EP](https://archivo.contagioradio.com/sentido-acto-de-perdon-se-realizo-hoy-en-bojaya/)** del pasado 6 de diciembre, por lo acontecido el 2 de mayo 2002, cuando usaron errónea y fatídicamente una pipeta, con la que murieron al menos 79 personas, la mayoría niños, y más de un centenar de lesionados, en el momento en que estaban a punto de lograr la retirada de los paramilitares apoyados por la 4ta Brigada, que se habían tomado la población, es el segundo acto solemne de este grupo alzado en armas.

El haberme mediadamente adentrarme en el alma de algunas y algunos, de los que allí estuvieron, en ese caserío a la orilla del Atrato me llevó a la conmoción interior. Lo que quería ser un simple ejercicio narrativo, se convirtió en una experiencia, como lo expresa Cortázar, “fantástica”. Una figura para decir que lo soñado y la realidad se encuentran más allá de un instante, ante la incapacidad de lo onírico para muchos, atrapados en raciocinios ideológicos contra  los rebeldes, porque esos sueños han sido incapaces de percibirse en la realidad y esos sueños se hicieron uno con la realidad el 6 de diciembre.

A través de un video e imágenes, a las que accedieron la mayoría de los periodistas, y a unas cortas entrevistas con personas que allí asistieron, viví sobrecogido,  construyendo un mundo imaginado el de la reconciliación posible en una nueva democracia.

Una abuela con su tez negra, sus cabellos encenizas, expresó: “no lo imagine, pero estoy feliz, esto ayuda a curarme, ellos reconocieron, que lo sucedido no estuvo bien hecho ni para ellos”. Otro, un adulto mayor, “les creo, su llanto contenido, no fue mentira, no era show, les dolió, por nosotros y por sus propios caídos”. Sus fuertes alabados  de dolor, y ellos, ahí, escuchando, en silencio respetuoso, y en lágrimas contenidas y cayendo lentamente.

Más allá de las palabras de contexto armado de Pastor Alape, quien se dirigió a las víctimas, en presencia de más de 10 mandos de su organización, el desborde del llanto en él y en las víctimas es lo sublime, el encuentro de lo onírico, del deseo y la realidad.

El primer reconocimiento se había realizado el pasado 18 de diciembre en La Habana, en un acto que contó con la presencia de un reducido número de las víctimas, el de hace unos días en Bojayá viejo, intentó aglutinar el máximo de sobrevivientes. Del primer acto me expresan que esa voz entrecortada, katarsis del profundo dolor por parte del Comandante del Bloque Iván Ríos de las FARC EP, también surgió.

Interpretar esta manifestación humana del llanto es un reconocimiento de una condición creada a otros, convertidos en víctimas. Es manifestación del sentimiento ético, de un proyecto de gobierno, que se pretende fundar más que de solo conocimiento (conceptual, teórico, ideológico) en el  reconocimiento conciente de un estado en desorden (volver a lo de atrás para reconducir el presente)           que se gesta hacia lo nuevo.

Para muchos externos, todo fue parafernalia, mentira, más de lo mismo del engaño, porque le creen a la verdades oficiales, que resuenan en los medios, o  por una simple experiencia han abdicado de soñar, son presos de la costumbre y nada les permite vibrar. Es el rasgo del resentimiento, del volver sobre lo mismo, porque es más fácil, perder los sueños, repetir sin transformar, reproducir sin crear ni recrear.

Muchos siguen hablando de la esperanza, sin creer en ella. Quizás, siguen siendo esquivos a enfrentar las realidades de la injusticia estructural que está detrás de la violencia, alentando esa causa de  las víctimas en la conversión de victimarios. Es más fácil negarse a lo fantástico del ser humano, para  seguir ahora, pasando de víctima a victimario, victimario de los sueños.

Lo que sucedió en Bojayá viejo, es algo más que podría romper los prejuicios y las pre comprensiones. La negación o tergiversación del hecho del reconocimiento y de la petición de perdón, es la expresión de la esclavitud ideologizada del alma, es la actitud negacionista de reconocer concientemente que las cosas han cambiado o están cambiando.

Estamos entonces ante un escenario inédito, fantástico, en la mirada cortaziana. Los seres humanos, los espacios, las personas se suman, se juntan en un sentimiento, una interacción aunada, interconectada, un enfoque sistémico, en el que se aborda, más allá de lo lógico, de lo racional, de la mimesis, la honra del pasado y sus víctimas, donde se rompe, el pasado para asumir el futuro. Reconocer el daño causado y aceptar la realidad tal como fue, iniciar un proceso de armonización o reorganización del sistema colectiva o proyecto de país, para evitar la repetición, para resignificar las conmemoraciones más que como sino de lo trágico para hacerlo prospectivo, futuro de nuevas realidades o de transformaciones profundas.

No se trata de la ingenuidad se trata de comprendernos como parte de un sistema que puede ser transformado a partir del diálogo para lograr terminar el conflicto armado, para mirar con conciencia los otros conflictos ambientales, sociales, culturales, pues a los que han hecho la guerra y se han beneficiado con ella; a ese sector, les es funcional que al lado de las víctimas, se promueva el odio, la venganza. Así los velos de la ficción de un aparato de justicia para unos y no para todos, nunca se descubrirá; la verdad seguirá siendo la farsa y el espectáculo de los poderosos, y la verdad expresada en Bojayá es la fusión de lo onírico y la realidad.

El asunto no es ya el hombre en su individualidad, es el sujeto siendo parte de una red, visto así mismo como parte de un sistema, donde todo está correlacionado, y en donde el sentimiento conciente permite transformar el todo, desde esa individualidad que es reconocida por sí misma y por otros, como afecto transformante, como amor  siendo verdad, siendo justicia. Lo sublime fue la experiencia del sentimiento conciente, victimas y responsables interconectados, al unísono en un sentimiento, expandiéndose del pasado, a un presente compartido de seres humanos, sintientes, vivientes, existentes, recorriendo hacia el futuro, a la prosperidad, el proyecto de un existir común, gobierno y nación, padre y madre, realidad y sueños.

<div style="text-align: justify;">

</div>
