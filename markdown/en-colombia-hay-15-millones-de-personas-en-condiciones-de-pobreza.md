Title: En Colombia hay 15 millones de personas en condiciones de pobreza
Date: 2017-09-19 16:01
Category: Economía, Nacional
Tags: Derechos en Colombia
Slug: en-colombia-hay-15-millones-de-personas-en-condiciones-de-pobreza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/desplazamiento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Foto: Contagio Radio] 

##### [19 Sept 2017] 

En el marco de la 62 sesión del Comité del Pacto de los Derechos Económicos, Sociales y Culturales, DESC, de las Naciones Unidas, organizaciones sociales presentaron el informe sobre el estado actual de esos derechos en Colombia, con cifras tan alarmantes como que el país tiene 15 millones de personas en condiciones de pobreza y el **0.4% de personas tiene en su poder el 40% de la tierra**.

De igual forma, el informe realizado por 75 organizaciones y redes de organizaciones civiles, corroboró que Colombia continúa siendo el segundo país más inequitativo de la región, que el 42% de los hogares del país vive en inseguridad alimentaria o **que el 13% de los menores de edad se encuentran en desnutrición crónica.**

### **Derecho a la alimentación** 

El documento presentado en la DES, determinó que son muy pocos los avances del Estado Colombiano en el cumplimiento y garantía al derecho a la alimentación. En ese sentido del 42% de hogares que viven en inseguridad alimentaria, **el 57.7% de esas familias están en la ruralidad, mientras que el 38.4% en zonas urbanas.**

En relación a la desnutrición infantil se pudo establecer que, desde enero hasta el 2 de noviembre de 2016, en la Guajira fallecieron 66 niños por hambre, pertenecientes al pueblo indígena Wayúu. **Además, en la última década, en Colombia han muerto aproximadamente 2.000 niños y niñas por este mismo motivo**.

Para las organizaciones, esta problemática se debe en gran medida a la negativa del gobierno de apoyar propuestas legislativas que aboguen por el cumplimiento de este derecho no solo en la primera infancia. De igual forma las normas expedidas que desconocen la ancetralidad campesina y sus semillas está vulnerando los derechos de agricultores. (Le puede interesar:["La Guajira alista paro cívico"](https://archivo.contagioradio.com/guajira-alista-paro-indefinido/))

### **Derechos al trabajo** 

El informó logro establecer que para el año 2015 **el 64% de los colombianos trabajaban en la informalidad, el 18% bajo relaciones laborales ilegales, mientras que el 89%** de trabajadores rurales carece de protección social.

Además, el 47.1% de los trabajadores ganan menos del salario mínimo legal, de otro lado, Colombia tiene una de las tasas más bajas de sindicalización, para el 2013, solamente el 36% de los trabajadores se encontraba afiliado a una organización de este tipo, en ese sentido el informe afirmó que el derecho a la huelga no existe y que durante el 2016 se registraron en total 23 huelgas en todo el país.

### **Derecho a la tierra y al territorio** 

El informe señaló que en Colombia persiste la desigualdad en la tenencia de la tierra en donde un 40% del área censada está ocupada por el **0.4% de unidades productivas, en contraste el 4.8% del área censada está ocupada por el 69,9% de la población.**

De igual forma el documento asevera que el despojo generado por el desplazamiento reorganizo la tenencia de la tierra, en ese sentido se calculó que entre 1980 a 2010 menos de **6.8 millones de hectáreas cambiaron de manos, profundizando el modelo de propiedad de tierras desigual**. (Le puede interesar:["El 1% de los propietarios ocupa más del 80% de la tierra en Colombia"](https://archivo.contagioradio.com/el-1-de-propietarios-ocupa-mas-del-80-de-tierras-en-colombia/))

El docuemento señala que esta desigualdad en la tenencia de la tierra se debe principalmente a la limitación de los campesinos para acceder a la tierra, las concesiones gratuitas o en arriendo, de grandes extensiones de baldíos a personas que no explotan directamente la tierra, a la inversión en tierras con fines especulativos o lavado de activos por parte de testaferros o narcotraficantes, a la compra masivas de tierras en zonas afectas por el conflicto armado y finalmente a la ineficiencia por parte del Estado para la creación de políticas públicas y leyes que transformen esta realidad.

### **Derechos al ambiente y al agua** 

Una de las denuncias más importantes que hace el informe, es que a partir del 2002 hay un incremento en la otorgación de títulos mineros e hidrocarburos llegando a **4.9 millones de hectáreas en el país. **(Le puede interesar: ["Denuncian nueva arremetida del gobierno contra las consultas populares")](https://archivo.contagioradio.com/46783/)

Situación que ha provocado la vulneración a grupos indígenas, graves afectaciones medioambientales y el denominado fenómeno de las puertas giratorias en donde la relación entre funcionarios de las instituciones estatales colombianas y empresas mineras o de explotación de hidrocarburos, **cobran más relevancia para intercambiar beneficios, como la entrega de títulos a cambio de pago de favores**.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
