Title: “No vamos a permitir que el peso de la crisis caiga sobre los hombros de los trabajadores” CUT
Date: 2015-02-11 21:18
Author: CtgAdm
Category: Economía, Política
Tags: central unitaria de trabajadores, conversaciones de paz, CUT, Plan Nacional de Desarrollo, Sindiclaismo
Slug: no-vamos-a-permitir-que-el-peso-de-la-crisis-caiga-sobre-los-hombros-de-los-trabajadores-cut
Status: published

###### Foto: cut.org 

En el **54 congreso de la Central Unitaria de Trabajadores, CUT**, los integrantes de este sindicato reafirmaron su **respaldo al proceso de paz que se desarrolla en La Habana**, ya que hay muestras reales y concretas de avances. Al mismo tiempo afirman que la paz es un derecho y que por ello es necesario que **cesen las amenazas, atentados, estigmatización y judicialización** de los y las integrantes del movimiento sindical en Colombia.

En la declaración política la CUT hace referencia a diversos conflictos a nivel mundial y señalan que la responsabilidad de esos conflictos recae sobre los intereses políticos y económicos que los generan. De igual manera afirma que rechazan “el **proceso de desestabilización contra el gobierno de Venezuela** y saludamos el inicio de los diálogos para el restablecimiento de relaciones entre **EE.UU y Cuba**”

Los y las sindicalistas denunciaron que dentro del **Plan Nacional de Desarrollo el gobierno ha incluido cerca de 160 observaciones de la OCDE**, entre las que se encuentra el aumento de la edad de jubilación hasta los 65 años en hombres y mujeres, y señalan que de ninguna manera van a “alcahuetear que descarguen sobre los hombros de los trabajadores y la Nación el peso de la crisis”

La CUT afirma también que el **PND pretende aplicar con “leyes exprés”** una serie de elementos que reestructura asuntos tan importantes como la educación, la salud, el trabajo entre otros derechos.

Este año la Central Unitaria realizará el V Congreso de la Mujer Trabajadora y el II Congreso de la Juventud, por un lado y, por el otro, el Congreso Extraordinario de la CUT. Además el **26 de Febrero se hará entrega, a nivel nacional, del pliego de exigencias** del sector de los trabajadores.
