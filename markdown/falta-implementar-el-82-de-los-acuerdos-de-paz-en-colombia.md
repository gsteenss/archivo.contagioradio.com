Title: Falta implementar el 82% de los Acuerdos de Paz en Colombia
Date: 2018-02-12 12:53
Category: Nacional, Paz
Tags: acuerdos de paz, Implementación JEP
Slug: falta-implementar-el-82-de-los-acuerdos-de-paz-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/FIRMA-DEL-ACUERDO-DE-PAZ-21.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Feb 2018] 

El balance que se presentó sobre la implementación de los Acuerdos de Paz en Colombia, dio como resultado que falta implementar el 82% de los proyectos de ley. Entre la lista de lo que falta por hacer se encuentran los temas más importantes y coyunturales del acuerdo: L**a Reforma Agraria Integral, la participación política, la sustitución de cultivos ilícitos, la estructura institucional de la JEP, y le desmonte del paramilitarismo, entre otros.**

Para Iván Cepeda, senador del Polo Democrático, la puesta en marcha de los artículos sobre la reforma rural integral significa “**el cuerpo social, la posibilidad de cambiar, en muchas zonas del país las causas estructurales de inequidad social** que han provocado el conflicto armado”. En este punto la única aprobación que se ha dado es sobre un proyecto de Ley de innovación tecnologica.

Además, el senador prendió las alarmas sobre las ZIDRES que continúan su implementación en el gobierno de Juan Manuel Santos, permitiendo nuevos mecanismos para la acumulación de tierras, sin que aún exista el proyecto de Ley marco para llevar a cabo la reforma rural.

La Reforma Política tampoco despegó en el Congreso de la República el semestre pasado. A la fecha no se ha dado la aprobación de las **16 circunscripciones de paz, a pocos meses de que se realicen las elecciones a Congreso**. (Le puede interesar: ["Territorios continúan sin saber que es la paz en Colombia: Misión Internacional en Colombia"](https://archivo.contagioradio.com/informe_mision_internacional_acuerdos_paz/))

### **Las instituciones para implementación del Acuerdos de paz tampoco están listas** 

 A ello se le suma que tampoco se ha dado avances en la implementación estructural de las instituciones que se encargaran de legislar la jurisdicción especial. “Hacen falta instituciones fundamentales para desarrollar, a través de políticas públicas, del acuerdo de paz, y me refiero concretamente, para mencionar un solo ejemplo a la Unidad de Investigación que se debía haber puesto en obra en la Fiscalía para que se persiga a los grupos paramilitares”, afirmó Cepeda.

Así mismo señaló que la Comisión de la Verdad tiene las condiciones para empezar a operar desde el punto de vista legal, pero hace falta la parte operativa y de recursos que le permita iniciar sus labores. Frente a la tarea que debe asumir la Unidad de Búsqueda, el congresista manifestó que hay una discusión, ya que, por un lado, están **quienes buscan que esta Unidad sea una dependencia del gobierno nacional**, mientras que por el otro,  se pide que se respeten los Acuerdos y se establezca la independencia de la Unidad.

“El Acuerdo es muy claro en señalar que el sistema integral de Verdad Justicia y Reparación, es un sistema independiente para evitar precisamente que ocurra lo que ha ocurrido con la** justicia en Colombia que es la intromisión de la política y de la politiquería en labores tan importantes como el esclarecimiento de la verdad**”, aseveró Iván Cepeda.

Para el senador, la continuidad y el apoyo que reciban los Acuerdos de paz dependerán de lo que pase en las próximas elecciones que puede significar un avance de lo implementado, o un retroceso de lo poco que se ha logrado. (Le puede interesar: ["Unidad de Búsqueda de personas desaparecidas a un decreto de ser realidad"](https://archivo.contagioradio.com/unidad-de-busqueda-de-personas-desaparecidas-a-un-decreto-de-convertirse-en-realidad/))

### **Las amenazas a los líderes sociales aumentan en Colombia ** 

Sobre la difícil situación que afrontan las y los líderes sociales en el país, el senador Iván Cepeda propuso internacionalizar la defensa de los líderes a partir de a**cciones de organizaciones e instancias internacionales que permitan visibilizar,** no solo las reivindicaciones, sino también los riesgos a los que se exponen los líderes en Colombia.

“Hay la necesidad de ir a la Comisión Interamericana de Derechos Humanos, a solicitar medidas de protección cautelar de carácter colectivo para los líderes sociales de Colombia y eso debe ser una labor que concertemos las fuerzas políticas que estamos en el espectro de la defensa de la paz, con los movimientos sociales y las organizaciones territoriales” expresó el congresista.

<iframe id="audio_23735054" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23735054_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
