Title: Con campamento recordaron la vida del líder social Carlos Pedraza
Date: 2018-01-22 12:00
Category: DDHH, Nacional
Tags: asesinato de líderes, Carlos Pedraza, congreso de los pueblos, crimen carlos pedraza
Slug: con-campamento-recordaron-la-vida-del-lider-social-carlos-pedraza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/carlos-pedraza-e1516640394855.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: MOVICE] 

###### [22 Ene 2018] 

El fin de semana se llevó a cabo un campamento en Bogotá por la memoria de Carlos Pedraza, educador popular, defensor de derechos humanos e integrante de Congreso de los Pueblos y **quien fue asesinado en 2015**. La investigación por su crimen aún no presenta avances por lo que su muerte se encuentra en total impunidad.

De acuerdo con Marlis Pedraza, hermana de Carlos Pedraza, el campamento se realizó con la finalidad de **conmemorar la vida de Carlos**; conmemoración que se realizó por tercera vez. Además, fue una actividad para celebrar la vida de todos los líderes sociales que han sido asesinados.

Su hermana afirma que este tipo de actos son importantes para **mantener la memoria viva de quienes han trabajado por la defensa de los derechos humanos** en el país. “Carlos Pedraza no ha muerto y sigue entre nosotros”, afirmó. Por esto recordó las palabras de su hermano: “Al final no nos preguntarán que hemos sabido sino que hemos hecho”. (Le puede interesar: ["Un año de impunidad en el asesinato del líder social Carlos Pedraza"](https://archivo.contagioradio.com/un-ano-de-impunidad-en-el-asesinato-del-lider-social-carlos-pedraza/))

### **¿Quién era Carlos Pedraza?** 

Carlos Pedraza se destacó por ser un **líder y profesor comunitario**. Además, trabajó de la mano de organizaciones de víctimas de crímenes de Estado como el MOVICE y también con organizaciones campesinas como el Movimiento de Masas Social y Político del Centro Oriente. Allí, trabajó con temas de impacto social como los procesos contra la extracción minero energética en diferentes comunidades.

De acuerdo con la información de la organización Congreso de los Pueblos, Pedraza fue visto por última vez el **19 de enero de 2015** cuando se encontraba en su casa en Bogotá en el barrio Los Molinos. Dos días después fue encontrado el cuerpo sin vida en zona rural de Gachancipá en Cundinamarca.

### **Investigación del crimen no ha avanzado** 

Indicó además que durante estos tres años la investigación del asesinato **no ha avanzado en lo absoluto** y “todo está en total impunidad”. Por esto, denunció que no hay interés por parte de las instituciones, como la Fiscalía General de la Nación, por que se conozca la verdad. (Le puede interesar: ["Carlos Pedraza fue víctima de ejecución extrajudicial"](https://archivo.contagioradio.com/carlos-pedraza-fue-victima-de-ejecucion-extrajudicial-congreso-de-los-pueblos/))

Para que se realizaran las investigaciones del caso y ante la inoperancia de las instituciones colombianas, pidieron la **colaboración del FBI de Estados Unidos**. Sin embrago, al FBI llegó un CD con 14 horas de grabaciones por lo que fue devuelto por el ente investigador estadounidense.

Ante esto, Pedraza estableció que **“ellos piden solo el pedazo donde se evidencia el caso”** por lo que la investigación no avanza por parte de ninguna autoridad. Ella manifiesta que la fiscal 74 del caso, Herlinda María Rojas Pizarro, le ha dicho a la familia en reiteradas ocasiones que “están investigando, pero nada sucede”.

<iframe id="audio_23288485" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23288485_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="osd-sms-wrapper" style="text-align: justify;">

</div>
