Title: En Quito no sobra nadie
Date: 2017-02-13 11:22
Category: Opinion
Tags: ELN, proceso de paz eln
Slug: en-quito-no-sobra-nadie
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/proceso-de-paz-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Eln Paz 

#### [**Por Mateo Córdoba Cárdenas**] 

###### 13 Feb 2017 

En pleno siglo XX, el polémico Juan Domingo Perón definió a la verdadera democracia como “aquella donde el gobierno hace lo que el pueblo quiere y defiende un solo interés: el del pueblo”. Hoy en día, sin embargo, es un desfachatez –propia de autoritarismos ramplones– hablar en nombre del interés popular sin haber escuchado a la gente de antemano. Pero no se escucha al pueblo para tomarse la foto y hablar de ‘diálogo con la comunidad’, sino para actuar en concordancia con las necesidades, expectativas y proyectos de la mayoría social.

Con el gobierno de Santos, el ELN logró algo histórico: Que el proceso de desarme de la insurgencia se diera en el marco de un diálogo sincero y de largo aliento con la sociedad civil. Lo anterior tuvo que luchárselo el Ejército de Liberación Nacional, porque Santos hablando de paz ha demostrado una estrechez de comprensión rotunda, reduciendo la terminación de la guerra al silenciamiento de los fusiles. Incluso las FARC, aunque no tan convencidas de la urgencia de integrar al pueblo en la negociación de La Habana, han insistido en que la paz se construye con democracia, con participación política y con garantías para el movimiento social. El ELN, aún asumiéndose como hijo de las luchas populares, ha dado gran muestra de humildad al abrir la mesa de diálogo para que sea la misma sociedad civil la que determine cuáles son las urgencias para que la paz se materialice en el campo y las ciudades.

El pasado 7 de febrero la voz de Juan Camilo Restrepo, jefe del equipo negociador por parte del gobierno, ha dejado ver –una vez más– que la paz para la élite es poco más que desarmar a las guerrillas para competir por premios internacionales. Ha dicho Restrepo que confía en las propuestas de la sociedad para, a renglón seguido, sentenciar que estas no tendrán un carácter vinculante en la mesa de Quito. Lo anterior, en resumidas cuentas es que al gobierno le da lo mismo lo que el pueblo proponga para construir una paz territorial y sostenible en el tiempo. Tal muestra de autoritarismo vergonzante, en donde se niega la voluntad popular con artificios retóricos, deja ver el verdadero talante del gobierno de cara al fin de la guerra. El 4 de febrero se realizaron a nivel nacional jornadas de diálogo entre la gente para ir preparando las metodologías y los contenidos de la agenda popular que será llevada a la mesa de negociaciones. Así el gobierno no quiera, la gente se está auto-organizando sin pedir permiso a nadie y hablando de paz con perspectiva social y democrática.

Cuando se trata de hablar de la Colombia del futuro, Santos prefiere dar voz y voto a otros sectores: a Odebrecht, al Grupo Aval, a Cambio Radical o a Mike Pence (vicepresidente de Trump). Esas agendas sí que son vinculantes para el gobierno. La lógica de la élite colombiana plantea que sólo con el consentimiento de las grandes firmas económicas o  el beneplácito del gobierno fascista de Estados Unidos va a ser posible la implementación de los acuerdos de paz. ¿Y la voz del pueblo?

El ELN ha sido claro al decir que con los diálogos en Quito no pretende construir el socialismo, simplemente espera generar las condiciones para que la gente sea la que, a partir de sus propias agendas, lleve la paz a los territorios. El gobierno, con el afán que tiene para decretar la pacificación del país, no se ha negado a esta exigencia de participación social por parte de los elenos, pero la voz del pueblo le resulta incómoda, por eso la quiere opacar dándole un carácter consultivo. El problema es, que aún con el apoyo de Trump, de Cambio Radical, del Grupo Aval, etc., si la paz no es agenda popular los acuerdos se quedan sin polo a tierra.

Pablo Beltrán ha sido enfático en señalar que una de las principales causas del conflicto armado en Colombia ha sido la mezquindad de la clase política nacional para abrir la democracia. Participar es paz en cuanto no hay mejor garante de una paz –como agenda territorial y subjetividad colectiva– que el propio pueblo. Pero la participación social no es una ruta apéndice de un diálogo entre actores armados, sino la posibilidad de hacer de la voluntad popular un itinerario vinculante que fije las bases de la política pública para la Colombia del futuro.

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio. 

[**[Siga toda la información de Contagio Radio a través de Facebook](https://www.facebook.com/contagioradio/)**]
