Title: Actividades mineras en el Cauca tienen amenazados a líderes sociales
Date: 2015-07-31 17:13
Category: DDHH, Nacional
Tags: amenazas contra líderes sociales, Cauca, CIMA, Defensoría del Pueblo, gran mineria, macizo colombiano, minería criminal
Slug: actividades-mineras-en-el-cauca-tienen-amenazados-a-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/antiminera2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [utopialapalabra.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_5696874_2_1.html?data=lpummJ2beI6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhcToytvWxsbIqdSfzs7bx9fFt4zZz5DSzpCnpdbXwpDhy8rSqc%2BfwtLS0MbepcXj1JDOjdjZt46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Guido Rivera, Comité de Integración del Macizo Colombiano] 

Ambientalistas, líderes y lideresas sociales del macizo colombiano  denuncian que en las últimas semanas,  **han sido víctimas de constantes seguimientos, amenazas y hostigamientos por defender su territorio** y sus ríos de la gran minería y la minería ilegal que se viene desarrollando en el departamento del Cauca.

Guido Rivera, integrante del Comité de Integración del Macizo Colombiano, CIMA, asegura que estas amenazas se vienen dando desde el 2013. De acuerdo con la denuncia pública,  **durante ese año y en el 2014 se presentaron amenazas contra líderes regionales** y locales de los municipios de Bolívar, Santa Rosa, Patía, Almaguer en el departamento del Cauca y San Lorenzo y Arboleda en Nariño.

El pasado, 28 de julio de este año, en el caso urbano de La Sierra, hombres armados ingresaron a la casa de uno de los líderes cafeteros perteneciente al CIMA, preguntaron por el líder social y luego agredieron a su hija de 17 años quien se encontraba en compañía de su hija de 10 meses. Finalmente se robaron una Tablet y un dinero de la familia y se fueron asegurando que volverían por el líder cafetero.

Según Rivera, **el macizo colombiano tiene varios títulos mineros de empresas como Anglo Gold Ashanti y Continental Gold,** aunque estos no los están explotando, el líder social, afirma que se trata de mineros criminales que llegaron de otros lugares a usar esas tierras tituladas para explotar minerales de los ríos.

Por cuenta de las actividades mineras en esta región del país, ríos como el Patía, Guachicono, Caquetá, San Jorge y la Quebrada San Miguel, entre otros, **están siendo contaminados con sedimentación y cianuro.**

Teniendo en cuenta estas denuncias, el CIMA llama a los **organismos de investigación a avanzar en la identificación de los responsables materiales e intelectuales** de esos hechos, también pide la presencia de **organismos estatales** encargados de velar por la protección y prevención de los derechos humanos y hace un llamado a la **Defensoría del Pueblo** para que realice el respectivo seguimiento para que se prevengan posibles crímenes contra los y las defensoras del ambiente.

 
