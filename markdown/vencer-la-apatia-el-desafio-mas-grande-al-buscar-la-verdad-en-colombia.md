Title: Vencer la apatía, el desafío más grande al buscar la verdad en Colombia: John Paul Lederach
Date: 2019-04-26 15:58
Author: CtgAdm
Category: DDHH, Paz
Tags: comision de la verdad, John Paul Lederach, Sistema Integral de Justicia
Slug: vencer-la-apatia-el-desafio-mas-grande-al-buscar-la-verdad-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Lederach.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

El académico y experto en construcción de paz, **John Paul Lederach,** hace una reflexión sobre una selección de 34 Acuerdos de paz que acogieron mecanismos de Comisión de Verdad. de ellos se pueden aprender de los errores y aciertos y pueden ser vistos como experiencias previas para fortalecer el trabajo de la **Comisión para el Esclarecimiento de la Verdad** en Colombia.

El profesor señala a la convivencia y la no repetición como virtudes y novedades de la **Comisión de la Verdad, en nuestro país, además de referirse a las oportunidades y retos** que se presentarán a lo largo de estos tres años en los que buscarán esclarecer lo sucedido durante más de medio siglo de conflicto armado.  [(No es cuestión de olvidar y perdonar, es cuestión de reconocer y cambiar: Lederach)](https://archivo.contagioradio.com/no-es-cuestion-de-olvidar-y-perdonar-es-cuestion-de-reconocer-y-cambiar-lederach/)

**¿Cómo fue el proceso que llevó a cabo para seleccionar las comisiones de la verdad que podrían servir de ejemplo para la Comisión de la Verdad de Colombia?**

Como punto de partida se escogieron comisiones de la verdad que surgieron de acuerdos de paz nacionales y sobre todo del trabajo comparativo del Instituto Kroc. Hay comisiones que no han tenido que ver con acuerdos de paz en contexto de conflicto armado, simplemente surgieron por razones concretas en puntos como Argentina u otros puntos del mundo.

Fueron un total de 34  acuerdos, de esos, 12 comisiones fueron aprobadas y otras 2 se cayeron antes de empezar. Estudiamos sus años de implementación y comparamos el tipo de mandato que han tenido esas comisiones. La Comisión de la Verdad de Colombia tiene un mandato más profundo, las primeras eran muy concretas en temas de esclarecimiento pero no contemplaban la convivencia o la no repetición y la mayoría se centraba más bien en hechos históricos.

En Sudáfrica agregaron la idea de reconciliación e hicieron un proceso que tenía similitudes con el de Colombia. Uno que se acerca al pueblo de forma directa y en el que pueden participar y tener acceso; esa combinación de pasado, presente y futuro es nueva, además agrega más complejidad a su trabajo y con menos tiempo, son tres años, es un tiempo bastante corto dada la complejidad del conflicto colombiano en el tiempo.

**¿Qué errores cometieron las comisiones que fallaron y no lograron cumplir con su mandato?**

Si uno mira desde lo comparativo, en primer lugar hay que tratar de comenzar en las fechas acordadas, es muy importante en la implementación porque a menudo una comisión de la verdad puede ser un motivo de diferencias políticas en un país.  Por ejemplo en Nepal tardó mucho tiempo en arrancar la comisión de verdad y todavía hoy no ha hecho todo el camino que debería haber recorrido, por su parte, Colombia arrancó en el tiempo designado por el acuerdo lo que la lleva en ese sentido por un buen camino.

Las comisiones de verdad deben ser transparentes, que la gente entienda que no se trata de malos y buenos sino que, colectivamente, el país pueda darse cuenta de lo que pasó y dar a ese sufrimiento de las víctimas un punto de legitimidad oficial de que sí pasó y de que en su mayoría fueron muchos inocentes los que estuvieron en el medio, eso recae en cómo buscar que un pueblo entienda que somos “nosotros”, no “ustedes” y “nosotros”.

El modelo de perdonar y olvidar como receta de cambio no funciona, porque es transgeneracional y repercute en las próximas generaciones; el cambio de comportamiento es el propósito final de esa idea de convivencia y no repetición, solo sabremos hasta más adelante si el país logra entenderlo y si es posible que juntemos y  miremos la forma  que no se repita.

> "Las comisiones de verdad deben ser transparentes, que la gente entienda que no se trata de malos y buenos sino que colectivamente el país pueda darse cuenta de lo que pasó"

**Usted señala en alguno de sus textos la importancia de la Curiosidad por que esta llama al diálogo y despierta el interés de la gente, ¿Cómo hacer que esta curiosidad se despierte en un país como Colombia?**

No es fácil, muy a menudo tiene que ver con que las personas logren entender que esto forma parte de su convivencia más amplia, que no es posible evitar que seamos parte de la red de relaciones afectadas por lo que ha pasado, no es posible ignorarlo como si no existiera, no es cuestión de quién tiene o no tiene la razón, en puro negro y blanco. El desafío principal es entender que eso forma parte de quiénes somos y si no somos capaces de hacer frente a quienes somos, hay muchas posibilidades de repetirlo.

<iframe src="https://www.youtube.com/embed/QCnkEGp1mY0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

El desafío más grande a vencer es la apatía, el no tener interés porque cree que no le afecta, por eso es importante el legado que viene de forma transgeneracional, haber vivido una historia de conflicto bélico tiene un impacto tremendo en las futuras generaciones- Es por que hay que llamar al futuro, al legado que uno va a dejar aquí y entender que cuando la gente escucha las historias de vidas ve en el otro a un ser humano que ha vivido cosas, ese es el punto de arranque de la Comisión.

**Dicen algunos que este Gobierno no ha dado apoyo suficiente al Sistema Integral de Verdad, Justicia, Reparación y No Repetición ¿Se presentó esta situación en otras Comisiones de la Verdad?**

Sí, hay paralelos. En muchos lugares los conflictos armados se han dado en países con mucha más pobreza que la que existe en Colombia com**o Nepal, Sierra Leona o Liberia**, en ese caso el Gobierno no tuvo  los recursos y mucha de la ayuda llegó por parte de la cooperación internacional.  Yo diría que el caso de Colombia sirve de un ejemplo tremendo para el mundo, por eso muchos países están mirando de cerca cómo vamos a lograr esa transición para que la paz sea duradera. Esto requiere una inversión de capitales, no solo de dinero, también de pensamiento, de capacidades y de esfuerzos colectivos.

[English Version](https://archivo.contagioradio.com/john-paul-lederach-overcoming-apathy-the-greatest-challenge-seeking-the-truth-in-colombia/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).una 
