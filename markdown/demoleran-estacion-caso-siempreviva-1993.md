Title: Demolerán estación de Policía donde fué violada y asesinada la Siempreviva en 1993
Date: 2018-09-19 13:48
Author: AdminContagio
Category: Nacional
Tags: feminicidios, niñez, Sandra Catalina Váquez Guzmán
Slug: demoleran-estacion-caso-siempreviva-1993
Status: published

###### Foto: Roja, negra, púrpura 

###### 19 Sep 2018 

Este miércoles 19 de septiembre a las 4 de la tarde, **la Universidad de los Andes derribará el edificio del antiguo C.A.I. de Germania**, donde la menor Sandra Catalina Vásquez fue violada y asesinada en febrero de 1993, por lo cual la Institución Universitaria ha invitado a **la familia de la víctima a dar el primer martillazo** para iniciar la demolición.

Durante el evento, además de reivindicar la memoria de "La Siempreviva" como la recuerdan año tras año sus amigos y familiares, se espera hacer una vez más **exigencia al Estado de medidas efectivas de reparación y justicia por Sandra Catalina** y los miles de niños víctimas de la violencia en el país.

**El caso de Sandra Catalina**

**Sandra Catalina Vásquez Guzmán tenía nueve años el 28 de febrero de 1993** día en que fue abusada y asesinada. Ese día junto a su madre Janeth Guzmán se dirigieron a la Estación Tercera de Policía de Germania ubicada en Bogotá, en la calle 19 con carrera 3ra, en busca de Pedro Vásquez, padre de la niña.

Sandra ingresó sola a la edificación y al notar que se demoraba mucho en regresar, Janeth fue en busca de su hija, encontrándola en un baño del tercer piso de la estación, casi sin vida, con una cordón al cuello y  sangre entre sus piernas. **La menor fue traslada al Hospital San Juan de Dios, donde murió**.

Inicialmente se responsabilizó al padre como principal sospechoso, y cerca de 120 personas hicieron parte de la investigación, hasta que **luego de 3 años se determinó que el autor del crimen era el agente de policía Diego Fernando Valencia**, quien fue condenado a 45 años de prisión, de los cuales **cumplió 10 y hoy se desconoce su paradero**.

En 2015, después de 20 años, la Corte Constitucional ordeno la reparación e indemnización a los familiares, sin embargo, **el caso aún está en la impunidad puesto que la condena no se llevó a cabo en su totalidad y no se ha establecido responsabilidad sobre otros agentes o superiores que se encontraban en el lugar**.

En su memoria se organizó [el jardín de la Siempreviva](https://archivo.contagioradio.com/la-memoria-de-sandra-catalina-vive-en-el-jardin-de-la-siempreviva/) en inmediaciones del C,A,I, lugar donde **cada 28 de febrero se conmemora su vida y se levanta un llamado para que casos como el suyo jamás se repitan.** Hoy su imagen se ha convertido en un símbolo de lucha contra la violencia sexual hacia niñas y niños, que,  según el Instituto de Medicina Legal, en 2017 se registraron 5.283 casos de violencia sexual contra niñas y niños entre los 5 y los 9 años, 9.240 casos entre los 10 y 14 años y 2.539 casos entre los 15 y 17 años.
