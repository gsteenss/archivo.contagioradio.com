Title: La guerra preventiva de Santos para las empresas
Date: 2015-11-17 11:15
Category: Camilo, Opinion
Tags: Desplazamiento interno, Explotación empresarial de recursos naturales, Ley 1448, Ley de Restitución de Tierras, Modelo minero energético en Colombia, Zidres
Slug: la-guerra-preventiva-de-santos-para-las-empresas
Status: published

#### [**Por [Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) - [~~@~~CamilodCasas](https://twitter.com/CamilodCasas) 

###### [16 Nov 2015] 

569  años es el cálculo que se hace para  devolver el reconocimiento de la propiedad y su uso, a las víctimas del despojo, propietarios legales y legítimos, que han reclamado a través de 360 mil demandas ante  la ley 1448, de 2011, conocida como la ley de víctimas y de restitución. A este tiempo habría que sumarle los obstáculos reales para su aplicación que forman parte de la guerra preventiva de Santos para asegurar la pax neoliberal en materia territorial.

Durante sus cuatro años de aplicación, la ley, que contó con el respaldo al más alto nivel, como el Secretario General de Naciones Unidas, y se reconoce como las más avanzadas del mundo, no ha logrado sus propósitos y hoy sus precarios avances, se irán al traste con apuestas de agroindustria en regiones estratégicas como la Altillanura, la Amazonia, y el Pacífico colombiano con todo el entramado de “modernización rural” para el sector empresarial.

Los  Planes estratégicos de interés nacional para minería, petróleo e infraestructura, PINES; las Zonas de Interés de Desarrollo Rural,Económico y Social, ZIDRES y la ampliación de Zonas de Reserva forestal e incentivos forestales con rentabilidad para los ganaderos dentro de la economía verde son obstáculos de fondo para la devolución de las tierras o para la titulación a campesinos mestizos, negros e indígenas.

La creación de las Zonas de Interés de Desarrollo Rural, Económico y Social (Zidres), aprobada esta semana en tercer debate en el Senado, es otros de los mecanismo que asegura a empresas privadas una legalización de acumulación de baldíos frente a las Unidades Agrícolas Familiares, UAF; acaparamiento de tierras, para la llamada energía limpia con el monocultivo de palma y la extensión de siembras industrializadas y extensivas de alimentos. Decisión política del gobierno de las víctimas, que desestimula y desvaloriza la pequeña propiedad rural y el uso diverso del suelo.

Al tiempo que la dinámica militar copa los territorios en los que hay cese unilateral del fuego por parte de las FARC EP, los PINES han definido el sentido del uso del territorio para obras de infraestructura, el modelo minero energético en 32 millones de hectáreas y agronegocios, así como, la sustracción de 5 kilómetros de tierra en línea recta que se suma a los territorios concesionados, por encima de la devolución a  los despojados.

Esas razones, más allá de la ineficacia de la 1448 que contempla dos mecanismos para la restitución de las tierras, explica porque las proyecciones oficiales, según las cuales a 2020 habrían sido resueltos 300 mil casos hasta abril de 2015 solo se brindó respuesta a cerca de 1300 demandas. Las cifras hablan por sí solas, y demuestran la lentitud de la ley y  su eficacia retórica para la comunidad internacional, hoy aprestándose para esos negocios que hay detrás de los PINES y los ZIDRA. Ni la restitución de tierras, ni las Zonas de Reserva Campesina ni  la ampliación de Resguardos o de titulación colectiva de comunidades negras avanza por esas mismas razones.

Los más de 7 millones de desplazados internos en razón del conflicto armado, y que son el mayor grueso de víctimas, han sido despojadas o han abandonado más de 6 millones de hectáreas de tierra en Colombia, en los últimos 25 años. Las disposiciones del ejecutivo están anunciando la existencia de nuevas víctimas, estas quizás no sean fruto de la violencia armada, si de la violencia institucional por un modelo de desarrollo que pone en jaque la economía rural tradicional y todas las fuentes de vida con toda su riqueza biológica.

Ya no habrá desapariciones forzadas, calculadas en datos oficiales, en más de 45 mil casos de personas, mucho más que las ocurridas en las dictaduras militares del Cono Sur; de asesinatos selectivos y masacres, pero si de un éxodo de más de 5 millones de los habitantes rurales que podemos calcular son la población en resistencia. Como lo expresó en su momento Francis Deng, encargado  especial de NU para los desplazados internos, en su informe de Colombia en los 90,  detrás de esa violación múltiple de derechos humanos se identifican intereses económicos, esta es la motivación sustancial que hay detrás de los intereses del gobierno para sentarse a conversar con las guerrillas, y son esos mismos intereses los que enfrenta el sector rural.

Santos desarrolla una guerra normativa preventiva para lograr asegurar la inversión privada por encima de los acuerdos de La Habana, implementando un modelo de desarrollo que viene de Pastrana y de Uribe. Asegurada la dejación de armas de las guerrillas, el control militar institucional se fortalecerá, como ya se hace a través de la ley 1448 y de la mano de convenios militares con las empresas privadas como ocurre en la amazonia del Putumayo. Son los militares los que subordinan al poder civil o los militares y neoparamilitares los que aseguran el modelo.

La ausencia de voluntad política para resolver asuntos de fondo del modelo económico rural, que usa el concepto de restitución, la protección ambiental,  pero que en realidad, conforme al derecho internacional, no restituye la tierra y los bienes muebles en las condiciones antes del desplazamiento y o despojo, y usa del lenguaje verde para las operaciones antiambientales, reflejan que los factores de poder que acaparan la tierra son intocables, y este asunto no se modificaran en los acuerdos con las FARC EP y ELN.

Los factores de poder, los beneficiarios del despojo y de la adquisición de los baldíos de manera irregular, sectores privados poderosos de agronegocios, industrias extractivas y minero energéticas, y de obras de infraestructura, no pretenden regresar las tierras o aceptar usos comunitarios de las tierras.

Los privados hasta  hoy tienen asegurada con esa arquitectura preventiva su inversión. Romper esos dispositivos de poder siguen siendo los retos de las víctimas del despojo, del movimiento rural y el de una sociedad que proyecte la paz con justicia socio ambiental en una apuesta de país, con alimentación sana y protección ambiental para la preservación y reproducción de las fuentes de vida.

El acaparamiento de la tierra está en el fondo de la pax neoliberal, las reformas legales son preventivas y limitantes de los acuerdos hacia la paz y con el movimiento social rural y ambiental.
