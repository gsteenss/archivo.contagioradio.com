Title: Las memorias en Inzá, testimonios de verdad y reconciliación
Date: 2020-09-06 12:50
Author: CtgAdm
Category: Actualidad, Comunidad, Sin Olvido
Slug: las-memorias-en-inza-historias-de-verdad-y-reconciliacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/WhatsApp-Image-2020-09-06-at-12.02.18-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"fontSize":"small"} -->

*Foto: Contagio radio/ Dilmer Leiva, Abelardo Muñoz y Hector Cardenas (izquierda a derecha)*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el marco del VI [Festival de las Memorias](https://archivo.contagioradio.com/asi-se-vivio-el-quinto-festival-de-las-memorias-somosgenesis/)en San Antonio de Inzá, Cauca; se llevó a cabo el conversatorio *“Experiencias de vida y hallazgos sobre las distintas formas de eliminación”* en el que participaron intervinientes del conflicto armado, pertenecientes a las extintas FARC y a las Fuerzas Militares.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Abelardo Muñoz, excombatiente de FARC, señaló sus motivaciones para sumarse a las filas de la insurgencia y explicó que una golpiza que le propinaron unos soldados**, apenas saliendo de su adolescencia, creó en él un resentimiento que justificó en ese momento su ingreso a las FARC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El coronel (r) del Ejército Nacional, [Carlos Velásquez,](https://archivo.contagioradio.com/la-jep-tiene-la-palabra-para-que-las-ffmm-recuperen-su-legitimidad/)** afirmó que durante su carrera militar **fue testigo de conductas contrarias al mandato constitucional de proteger a la población civil** al interior de las tropas, y que ello, lo apartó de muchas de las directrices impartidas en el Ejército.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde Inza, intervino Dilmer Leiva, exmilitar del Ejército, quien afirmó que la política de “seguridad democrática” implementada por parte del expresidente **[Álvaro Uribe](https://archivo.contagioradio.com/colombia-debe-respetar-a-sus-magistrados-y-sus-decisiones/), fue la que propició el actuar delictivo de muchos de los integrantes de las Fuerzas Militares.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Leiva, agregó que la directriz operacional en la que sólo servían las bajas, llevaron a que brigadas compitieran por cuál tropa conseguía mejores **cifras para conseguir condecoraciones, ascensos, descansos, recompensas económicas o por el contrario sanciones o hasta la desvinculación de la Fuerza.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Todo esto, según Leiva, llevó a que las metas de las brigadas se encaminaran erradamente y se **desvirtuara su deber, lo que desembocó en el tristemente célebre episodio de las[ejecuciones extrajudiciales](https://www.justiciaypazcolombia.com/sentencia-revela-como-pagaban-100-mil-por-cada-falso-positivo/)** o “falsos positivos”.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Desde Inzá, el exmilitar Hector Cardenas víctima de su propio Ejército

<!-- /wp:heading -->

<!-- wp:paragraph -->

En el panel también participó el **exsoldado del Ejército Nacional, Hector Cardenas;** quien narró su experiencia de vida en las Fuerzas Militares y explicó que al tiempo que tenia su papel como militar, también era víctima del **Ejército, el cual, asesinó a su suegro y cuñado en uno de los muchos casos de ejecuciones extrajudiciales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Todos y cada uno de los intervinientes revivieron sus memorias, se sentaron en una misma mesa, pese a que en su momento combatieron para bandos diferentes;** y enfatizaron en la importancia de construir la verdad con todas las voces del conflicto, incluyendo las de los responsables, quienes también tienen algunas de las piezas para construir esa verdad integralmente.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

(Aquí puede ver el video completo del IV Festiva de las Memorias: <https://www.facebook.com/watch/?v=3415749461797335&extid=peIUIsIPW7uywUYQ>)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
