Title: Acá estamos “con la dignidad intacta”: Paola Salgado
Date: 2015-09-18 18:15
Category: DDHH, Entrevistas
Tags: 13 jóvenes capturados, Atentados en Bogotá, congreso de los pueblos, derechos de las mujeres, Paola Salgado
Slug: aca-estamos-con-la-dignidad-intacta-paola-salgado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/paolasalgado1_1436376456.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.lafm.com.co]

<iframe src="http://www.ivoox.com/player_ek_8464129_2_1.html?data=mZmjlpaWfY6ZmKialpqJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdLphqigh6aob9TZyNrWz9TXb8Tjz5DRy9jLssrYwsmYy9PYpcTowpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Paola Salgado] 

Cuatro horas duró el allanamiento, en la biblioteca escarbaron todo, carpeta  por carpeta, hoja por hoja, cajón por cajón… ese día, un miércoles 8 de julio empezó la pesadilla de Paola Salgado y doce jóvenes más.

“La sensación cuando irrumpen tu intimidad es indescriptible… no me imaginaba la dimensión de lo que estaba pasando”, dice Paola de 33 años, abogada y defensora de derechos humanos, al relatar el momento en el que su hogar fue allanado por decenas de hombres armados, que irrumpieron en el lugar mientras ella se disponía a arreglarse para salir a un evento de trabajo en el marco de la defensa de los derechos de las mujeres, eje central de la vida profesional de la abogada.

“Me encontraba en la tranquilidad de mi casa con mi esposo y mis dos perros”, cuenta Salgado. Al notar toda la parafernalia de la policía, pensó que habían robado algún apartamento del conjunto residencial donde vive. Su apartamento ubicado en el quinto piso de un edificio en el barrio Nicolás de Federmán, estaba acordonado por agentes de  la fuerza pública.

De pronto golpearon su puerta, ella la abrió con tranquilidad  pues hacía parte del consejo de administración del edificio. Varios hombres armados que gritaban y buscaban a la “femenina”, ingresaron a su apartamento[.]

“No me miraban, no sé qué esperaban encontrar”, cuando los hombres armados entraron a su hogar, ella los detuvo, se paró en la puerta de su apartamento ~~casa~~ y a uno de ellos le dijo con seguridad, “a mí me explican qué pasa, usted a mi casa no entra armado, baje el arma o se sale”.

Uno de los agentes de la policía alegó que tenían una orden de allanamiento en su contra, ella, asustada y en shock, llamó a su esposo con el que lleva 13 años conviviendo, “le dije que saliera con cuidado y despacio, porque pensé que si salía rápido o afanado se atrevían a disparar… no entendía qué pasaba, no sabía qué había hecho”. Lo primero que se le vino a la cabeza fue que ese hecho estaba relacionado con su trabajo, ya que antes había sido perseguida por entidades del Estado como la Procuraduría, por su labor a favor de los derechos sexuales y reproductivos de las mujeres.

“Como abogada revisé muy bien todo”, dice Paola, asegurando que tenía que haber puesto en práctica algo de lo que había aprendido en la Universidad Nacional donde estudió derecho.

No quiso empacar nada. En una bolsa muy pequeña, llevó un cepillo de dientes, un jabón, un antibacterial, y otros elementos básicos de aseo. “Pensé… en tres días, máximo el fin de semana tardaría en solucionar esta situación, le dije a mi esposo que no le contara nada a mi familia para no preocuparlos”.

Cuando Paola empezó a darse cuenta de la magnitud de la situación, al escuchar los comentarios de sus amigos, familiares y abogados, sobre la forma como los medios masivos hegemónicos estaban tratando el hecho, por su experiencia como abogada y en el movimiento social en Colombia, inmediatamente pensó… “nos destruyeron la vida… aquí te matan, te desaparecen o te encarcelan, esas son las formas de callar a quienes nos atrevemos a cuestionar lo que pasa en este país”.

En los titulares de los medios de información tradicionales las palabras terrorismo, atentados en Bogotá, guerrilleros y ELN, eran típicas cuando de nombrar este hecho se trataba. Como si fuese una película de acción, se mostraban una y otra vez las imágenes del “gigantesco operativo” de la policía, por el cual el presidente Juan Manuel Santos felicitó a esta institución, y al tiempo, condenaba la vida de jóvenes defensores de derechos humanos, de los derechos de las mujeres, de la libertad de prensa, líderes en trabajo con comunidades y luchadores por la educación pública y de calidad.

Nadie “cayó”, la policía pudo entrar normal a las casas de los jóvenes pues muchos de ellos abrieron tranquilamente la puerta sin entender qué sucedía.

Terrorismo, rebelión, lesiones personales agravadas y daño en bien ajeno, eran los delitos de los que se les acusaba en los medios de información a estos trece líderes sociales. 30 años de cárcel era la condena que el Ministro de Defensa, el Fiscal General y Santos celebraban, tras haberlos capturado en  “uno de los operativos más grandes e importantes contra una estructura de la guerrilla”, como afirmaban ante las cámaras de televisión y en directo a todo el mundo.

Pese a que a los activistas se les tildaba de guerrilleros, [los hechos por los que se les acusaba no tenían nada que ver con las explosiones en las sedes de Porvenir el 2 de julio en Bogotá, de hecho, en ninguna audiencia se les nombró cargo alguno por esos atentados.]{lang="ES"}

Paola se dio cuenta que debía enfrentar ese acontecimiento de su vida con la cabeza fría y como lo que era, una abogada. “Siempre intentaba hacer muchas preguntas sobre el paso a paso para entender que se venía y prepararme mentalmente para eso”, ella afirma que los capturados ese 8 de julio eran el “chivo expiatorio” del momento, “estamos ante un gigante que es el Estado, en una pelea que no es limpia… estaba preparándome para una cosa de meses o años” no de días como lo había pensado en un principio.

**En la cárcel**

Durante el primer mes los trece jóvenes permanecieron en una URI de la policía, donde fueron divididos en hombres y mujeres. Desde ese momento, la vida de los trece pasó a manos de terceros, sufrieron la pérdida total de la libertad y la pérdida absoluta de la intimidad.

Paola, Lorena y Lizeth, se convirtieron en un apoyo mutuo ante “la montaña rusa de sensaciones y sentimientos, entre la tranquilidad y estar invadida de llanto y tristeza", dice con la voz quebrada la abogada.

El 28 de julio, la juez 72 de Bogotá ordenó la detención preventiva en centro carcelario, alegando que con esa decisión se evitaba la obstrucción de la justicia y se buscaba proteger a la comunidad de la Universidad Nacional. Mientras se anunciaba la medida, una parte de la comunidad estudiantil de la que hablaba la juez, se encontraba fuera de los juzgados de Paloquemao, con tambores y arengas, apoyando y fortaleciendo a los trece jóvenes, “la movilización social nos animó, nos llevó a no perder la esperanza, no nos íbamos a derrumbar tan fácilmente”, expresa Salgado.

Paola fue acusada por los delitos de porte, tráfico y fabricación de armas de uso privativo de las Fuerzas Militares y violencia contra servidor público agravado.

En la cárcel del Buen Pastor fue internada junto a Lizeth y Lorena, las tres se volvieron reclusas en el patio número siete, una sección de alta seguridad donde principalmente hay presas políticas o vinculadas con terrorismo o rebelión.

Lorena Romo, de 23 años, es politóloga de la Universidad Nacional y antes de que empezara todo este capítulo en su vida, estaba lista para iniciar su especialización en Políticas Públicas, en la Universidad Externado de Colombia. Durante 2015, había trabajado como Gestora Social del Distrito, desde la Secretaría de Educación, con un desempeño intachable.

La otra compañera de Paola era Lizeth Acosta de  21 años, estudiante de la Universidad Pedagógica de IV semestre de licenciatura en ciencias sociales. Al igual que Salgado, Lizeth se desempeñaba como defensora de los derechos de las mujeres y actualmente estaba dedicada a la construcción del consejo estudiantil en la UPN.

Durante aproximadamente dos meses, no recibieron un solo rayo de sol, no pudieron respirar el aire de la naturaleza, no podían ir solas a ningún lugar, ni a la biblioteca, ni al médico. Metidas en los muros de esas paredes frías del Buen Pastor sentían que debían pedir permiso hasta para respirar, “todo el tiempo te recuerdan que no eres una persona, que no tienes ningún derecho y que no eres nadie”, esa sensación era el pan de cada día de las tres mujeres, la misma de miles de reclusas en el país.

“Siempre he afirmado que la agenda de los derechos de las mujeres está muy al final de las reivindicaciones sociales”, asegura Salgado, “lo que es la realidad de las mujeres que están privadas de la libertad es una situación que es completamente invisible”.

Paola asegura que si el fin de la cárcel es reeducar, resocializar y brindar una nueva oportunidad a las mujeres, este espacio lleno de barrotes y en medio de un ambiente gris, sirve para todo menos para cumplir con ello.

Desde el análisis de una mujer que lucha en contra del patriarcado, la abogada pudo visibilizar fácilmente que en la cárcel hay una reproducción del rol de la mujer al cuidado de los otros, un rol papel que imposibilita el empoderamiento de las reclusas como mujeres, pues son obligadas a realizar actividades de cocina, peluquería o lavandería “lo que impide que haya una posibilidad de desarrollarse individualmente... La cárcel es una oda al ocio”, señala Paola.

Esta situación dejó inquieta a Paola quien asegura que es urgente evidenciar y visibilizar esa problemática.

**Los temores en medio de la libertad**

Dos meses y tres días, duraron privados de su libertad.

El 11 de septiembre, el juzgado 44 del circuito, ordenó la libertad de los trece jóvenes capturados por la realización de una protesta estudiantil, todos fueron dejados en libertad, luego de que se declarara ilegal la captura por sobrepasar vencimiento de términos.

Paola asegura estar feliz, ahora puede enfrentar ese proceso desde la libertad, sin embargo el daño ya está hecho, el tiempo ya está perdido, y ella teme por su vida y la de sus familiares. “Defenderse en libertad es otra cosa, pero asomarme a la ventana o poner un pie en la calle es aterrador”, se trata de una sensación de pánico constante de la que ahora vive acompañada. Cuenta que varias veces escuchó a las personas que acompañaba decir que estaban perturbadas por el miedo, pero jamás imaginó que ella algún día llegaría a sentir los mismo que le relataban sus defendidos.

“El temor por la vida y la integridad es el día a día, pienso que me va a pasar algo mientras duermo”. En medio de risas nerviosas, cuenta que en las noches mientras intenta disfrutar de su cama, que tuvo que dejar por dos meses, le pide a “Mauri”, como ella llama a su esposo, que la abrace por la espalda y la cubra, pues ahora le da miedo dormir con la espalda descubierta pensado que algo le pueda suceder.

Paola cuenta que cada vez que los desplazaban de un lado a otro cuando estaban en manos de las autoridades, era todo un “show” armado, como si llevaran al capo de los capos, algo que ella veía como un montaje de teatro para justificar que los mataran o los desaparecieran.

En la libertad las preguntas y los temores son constantes, “¿Quién nos garantiza nuestra seguridad cuando fuimos expuestos? ¿Qué tanto puede molestar nuestra libertad a los que están detrás de todo esto?”.

Aprendió a valorar las cosas más simples de la vida, las amistades, la solidaridad, las redes de afecto y de apoyo. Su madre, su esposo, tíos, abuelos, primos y sobrinos han tenido los gestos de apoyo más incondicional desde el primer día de la captura, intentando alivianar la situación, manteniéndola firme, de pie con su sonrisa y “la dignidad bien puesta”, relata Paola.

Este es un episodio que no solo se ha llevado una parte de la vida de esta defensora de los derechos de las mujeres, sino que además se ha llevado a su familia por delante, e incluso, se llevó la vida de uno de sus perros. “Su hijo” como ella le dice, tenía 10 años y no soportó la situación “para mí es muy duro que me digan que se murió de tristeza”.

Es una cadena de afectación interminable, que aún continúa vigente, pues todavía falta un largo camino para que los trece jóvenes recobren por completo su libertad.

“A veces uno no se imagina que tiene tanta fuerza alrededor”, dice extrañada, no solo por el apoyo de sus familiares sino por su carácter fuerte, que fue algo que la ayudó en medio de la adversidad.

Paola se dedica a poner en orden su espacio vital, ese que en aquel 8 de julio a las 6 de la mañana no solo fue allanado, sino violentado, destruido, y perturbado. Ahora se dedica a recuperar esos dos meses y tres días desperdiciados sin su familia, su esposo e “hija” (su perrita). Ahora se prepara junto a sus abogados para enfrentar el proceso que apenas comienza, pero con la dignidad absolutamente intacta.
