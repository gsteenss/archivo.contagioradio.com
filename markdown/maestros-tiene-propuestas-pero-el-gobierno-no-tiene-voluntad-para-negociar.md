Title: Hay propuestas pero el gobierno no tiene voluntad para negociar: FECODE
Date: 2017-05-23 12:22
Category: Educación, Entrevistas
Tags: educacion, fecode, Paro de maestros
Slug: maestros-tiene-propuestas-pero-el-gobierno-no-tiene-voluntad-para-negociar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/525468_1-e1495556477765.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Semana.com] 

###### [23 may 2017] 

En el marco de la gran movilización de maestros y maestras, Fecode ha dicho que entre sus propuestas se encuentran la **financiación progresiva del déficit de la educación y la reforma al sistema general de participaciones,** que corresponde a la transferencia de recursos a las entidades territoriales.

La Federación Colombiana de Educadores Fecode, ha dicho que en el tiempo que llevan las negociaciones con el gobierno, **este no ha demostrado la voluntad para resolver el problema de fondo de la educación.** Este se refiere al déficit presupuestal para la educación que es de 600 mil millones de pesos y que según los cálculos de Fecode aumentará para el 2018 a 1.1 billones de pesos. A esto se suma la desigualdad social, la falta cobertura de la educación pública y que la jornada única no cumple las exigencias de la sociedad. Le puede interesar: ["Déficit en educación es de 600 mil millones de pesos: Fecode"](https://archivo.contagioradio.com/deficit-en-educacion-es-de-600-mil-millones-de-pesos-fecode/)

Para Over Dorado, miembro de la junta directiva de Fecode, “el gobierno ha planteado que el magisterio y sus dirigentes somos intransigentes y esto no es cierto pues hemos entendido la situación económica y política le país y por este hemos planteado soluciones viables a largo plazo”. Es por esto que los maestros han planteado que el déficit de la educación se puede subsanar de **manera progresiva desde 2018 si el gobierno entrega los recursos que se necesitan.**

Sin embrago, las soluciones que implican mejorar la calidad educativa de los estudiantes, las condiciones laborales de los docentes y la financiación de la educación, **aún no cuentan con espacios de diálogo por parte del gobierno.** Le puede interesar: ["Explosión en Bogotá pudo estar dirigida contra Fecode"](https://archivo.contagioradio.com/explosion-en-bogota-pudo-estar-dirigido-contra-fecode/)

### **Proponer vacaciones es poner "pañitos de agua tibia" al problema** 

Igualmente, en medio del paro, los educadores han recibido la noticia de que el gobierno ha planteado adelantar las vacaciones para que los más de 8 mil niños y niñas del país no sigan perdiendo clases. Dorado afirma que **“esto son pañitos de agua tibia porque ¿qué va a suceder una vez volvamos de vacaciones si las peticiones no se cumplen?”** Le puede interesar: ["334.000 entran en paro nacional convocado por Fecode"](https://archivo.contagioradio.com/334-000-profesores-se-suman-al-paro-nacional-de-fecode/)

Ante la falta de voluntad de diálogo y negociación del gobierno, al menos 300 mil maestros y maestras realizarán una gran movilización en todo el país. Las marchas se realizarán en las **32 capitales del país para respaldar el pliego de peticiones del Magisterio.** A las movilizaciones asistirán también estudiantes, padres de familia y trabajadores estatales.

En Bogotá los puntos de encuentro fueron el Colegio Manuela Beltrán de la Avenida Caracas con calle 57, el Sena de la carrera 30 con avenida primero de Mayo y el Colegio Nicolás Esguerra de la calle 9 con calle 68. **Desde las 9:00 am iniciaron las movilizaciones para llegar a la Plaza de Bolívar.**

<iframe id="audio_18856022" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18856022_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
