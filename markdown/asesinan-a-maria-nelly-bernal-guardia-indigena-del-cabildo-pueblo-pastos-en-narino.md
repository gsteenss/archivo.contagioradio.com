Title: Asesinan a María Nelly Bernal, guardia indígena del cabildo Pueblo Pastos en Nariño
Date: 2019-02-21 15:59
Category: Comunidad, DDHH
Tags: Asesinato de indígenas, nariño, Samaniego
Slug: asesinan-a-maria-nelly-bernal-guardia-indigena-del-cabildo-pueblo-pastos-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/WhatsApp-Image-2019-02-20-at-7.11.41-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 21 Feb 2019 

En horas de la noche del pasado 14 de febrero en el municipio de Samaniego, Nariño, fue asesinada **María Nelly Bernal Andrade, comunera e integrante de la guardia indígena del cabildo del Pueblo Pastos** tras recibir cinco impactos de bala mientras se dirigía a su hogar, informaron las autoridades de la comunidad.

Luis Eusebio Cadena, gobernador del cabildo afirma que la Fiscalía es conocedora del caso pero no existe más información sobre cómo y por qué sucedieron los hechos **"lastimosamente sabemos cómo es la situación que se está viviendo en el país entero"** señala el dirigente respecto a las vidas que se están perdiendo en el territorio colombiano.

"Estamos muy tristes por la pérdida de un miembro de nuestro cabildo" expresa el gobernador de Pastos e indica que aún no existe indicio de quién pudo estar detrás del crimen, sin embargo, señala que** el ELN hace presencia en el territorio** y afirma que aunque en el cabildo nunca se han presentado sucesos de violencia, la situación más complicada se vive en el municipio de Samaniego, ubicado a tres horas del resguardo.

Según Indepaz durante el periodo registrado entre 2016 y diciembre de 2018, en Nariño se han presentado 49 casos de asesinatos contra líderes sociales y defensores de derechos humanos.[(Le puede interesar: Lideresa Maritza Martínez muere tras ser víctima de ataque en Tumaco)](https://archivo.contagioradio.com/lideresa-maritza-ramirez-pierde-vida-tras-ser-victima-ataque-tumaco/)

<iframe id="audio_32742184" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32742184_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
