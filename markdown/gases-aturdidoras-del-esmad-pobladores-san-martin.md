Title: Gases y aturdidoras del ESMAD contra pobladores de San Martín
Date: 2016-10-27 13:37
Category: Ambiente, Movilización
Tags: Abuso de fuerza ESMAD, Brutalidad policial Colombia, san martin
Slug: gases-aturdidoras-del-esmad-pobladores-san-martin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/fracking-san-martin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Gert Steenssens / EsperanzaProxima.net 

###### [27 Oct 2016] 

A través de un comunicado emitido este 27 de Octubre, las comunidades de San Martín y CORDATEC, denunciaron graves abusos de fuerza por parte del ESMAD. **Con gases, balas y aturdidoras atacaron a la población que salió a exigir respuestas del Gobierno en la Troncal del Caribe.** El saldo, varios heridos graves, 8 personas detenidas.

Karen Nieto integrante de CORDATEC, manifestó que “en la madrugada seguíamos buscando personas, habían muchachos de corregimientos vecinos , **sabemos que los desnudaron, los golpearon y que fueron llevados hacia las afueras del municipio”**.

Además, reveló que agentes del ESMAD **“lanzaron gases dentro de las casas de la comunidad, las madres salían corriendo de sus casas con sus hijos, y al que se asomaba le iban disparando gases”**. Añade, que la comunidad se encuentra bastante indignada por estos hechos y porque “hace más de 7 meses venimos luchando y nadie nos escucha ni explica ¿por qué San Martín va a ser el conejillo del fracking en Colombia?”.

Nieto comenta que muchos miembros de la comunidad y la organización creen que **“la empresa Conoco Phillips tuvo mucho que ver con esta situación**, no encontramos otra explicación para tal represión (…) el pueblo amaneció desolado y destruido por la guerra campal de anoche”. Le puede interesar: [Militarizada vereda cuatro bocas en san martín luego de entrada de multinacional.](https://archivo.contagioradio.com/militarizada-vereda-san-martin/)

Carlos Andrés Santiago, otro de los integrantes de CORDATEC, informó que **“si bien al día de hoy aún no hay licencia ambiental aprobada para No convencionales la empresa cuenta con una licencia ambiental para actividades convencionales”**, la cual fue otorgada en el 2014 y modificada en el mes de marzo de 2016.

Por otra parte, Nieto afirma que **de no obtener respuesta y soluciones en la audiencia de este jueves, ante el Congreso de la República, el paro que finalizaría hoy a la media noche, sería indefinido**. “San Martín no quiere fracking para este ni para otros territorios de Colombia, nosotros defendemos algo que es de todos, la vida y el agua”. Le puede interesar: [Arranca paro cívico en San Martín Cesar.](https://archivo.contagioradio.com/paro-civico-en-san-martin-cesar/)

<iframe id="audio_13505980" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13505980_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Comunicado de CORDATEC y comunidades de San Martín denuncian brutalidad del ESMAD](https://www.scribd.com/document/329126011/Comunicado-de-CORDATEC-y-comunidades-de-San-Martin-denuncian-brutalidad-del-ESMAD#from_embed "View Comunicado de CORDATEC y comunidades de San Martín denuncian brutalidad del ESMAD on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_66181" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/329126011/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-hiJnJLZ6VgyuWAGmtaz4&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
