Title: El oficio de la pesca artesanal debe ser reconocido por el Estado
Date: 2020-05-11 19:05
Author: CtgAdm
Category: Actualidad, Ambiente
Slug: el-oficio-de-la-pesca-artesanal-debe-ser-reconocido-por-el-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Pescadores-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Apescordel*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este lunes se llevó a cabo la [audiencia Pública](https://www.facebook.com/watch/live/?v=626942114563552&ref=watch_permalink) “**Pescadores** **Artesanales** **Reconocimientos de Derecho”**, una sesión que tenía como objetivo hablar sobre la política pública de la pesca artesanal a la luz de quienes ejercen este oficio en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la comisión cuarta del Senado los congresistas Aida Avella, Wilson Arias y Juan Luis Castro en compañía de diferentes organizaciones, desarrollaron un debate por el reconocimiento de los derechos de los pescadores artesanales en todas sus dimensiones.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AidaAvellaE/status/1259921948987056128","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AidaAvellaE/status/1259921948987056128

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

En entrevista en [Otra Mirada ,](https://web.facebook.com/contagioradio/videos/3012121345510565/)**Adriana Cadenas Directora de la Confederacion Mesa Nacional de Pesca Artesanal de Colombia** señaló que los pescadores están siendo invisibilizados en todos los proyectos que se hacen a nivel país y que ponen el riesgo las fuentes de agua que protegen y trabajan.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una dinámica apocada por los proyectos [extractivistas](https://archivo.contagioradio.com/guajira-sin-agua-por-desvio-de-arroyo-bruno-y-sin-ayudas-para-enfrentar-al-covid/), por eso para Cadenas *"es necesario que el Ministerio de Agricultura, la Autoridad Nacional de pesca, la Agencia Desarrollo Rural y el Ministerio de Seguridad y Protección Social aporten su grano de arena sobre la situación y trabajen en los temas de reconocimiento de derecho de los pescadores artesanales".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Agregando que el país desconoce el oficio **importando "el 70% del pescado** **cuando tenemos más de 2.000 ríos, 2 mares y 1.900 ciénegas**, lo cual significa un gran potencial pesquero artesanal y sostenible entregado a la industria extranjera que [explota y extingue las especias nativa](https://archivo.contagioradio.com/pescadores-exigen-medidas-ambientales-que-restauren-la-cienaga-de-bano-en-cordoba/)s".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cadenas pone como ejemplo el caso del [Río Cauca](https://archivo.contagioradio.com/mineria-ilegal-en-territorio-indigena-pone-en-riesgo-a-250-personas/), gravemente afectado por proyectos hidroeléctricos y mineros, *"acá se desconoció al pescador afectando no solo su territorio sino su empleo que no representa ningún daño en comparación con otros proyectos, porque **ellos respetan su ambiente, el agua y sus especies"***

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Los pescadores no tienen derecho a una seguridad social y en el marco de una pandemia como la actual la ayuda ha sido nula"***

<!-- /wp:quote -->

<!-- wp:paragraph -->

Cadenas afirma que *"los pescadores son campesinos sin tierra, porque su tierra es hidrica y es allí donde ellos y ellas tienen representada su economía*", agregando que el primero derecho que exigen es a la formalización del empleo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así como también a derechos básicos de cualquier trabajador como salud, seguridad social, y un ambiente laboral digno, *"**la gente se ha tomado ilegalmente los territorios para la producción de ganadería extensiva o de agricultura extensiva,** secando las aguas y amenazando, desplazando e incluso asesinando a los pescadores".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otro de los puntos del debate fue la Veda, un subsidio que no cuentan los miles de pescadores artesanales, *"pedimos un subsidio de veda, en donde durante los dos meses donde los pescadores no pueden trabajar sean protegidos y respaldados".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué y quienes hacen pesca artesanal?

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para **Adriana Cadenas** los pescadores artesanales son aquellos que usan elementos naturales para hacer pesca adecuados con el medio ambiente, ***"son personas que tienen una cultura ambiental y una cultura infundada entregada generación tras generación"***.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Afirmando que son personas cuyo oficio apoya la soberanía alimentaria y protege el ecosistema que también es su casa, *"la diferencia con los piscicultores es que estos son productores de pescado mientras que los pescadores artesanales son habitantes de su lugares de trabajo".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Ellos son conscientes de que si no cuidan su medio ambiente, destruyen su forma de sobrevivir, pero también destruyen sus hogares y el de las futuras generaciones"***

<!-- /wp:quote -->

<!-- wp:paragraph -->

Y la parte marítima se tiene confirmado que son cerca de 35000 pescadores y sobre los 100.000 superan la parte continental por mucho.  
número difícil de medir porque no hay un censo concreto para determinar cuántas personas en Colombia desarrollan este oficio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Organizaciones como la **Confederacion Mesa Nacional de Pesca Artesanal de Colombia** han respaldado desde el 2017 una serie de debate para el reconocimiento de los pescadores artesanales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello respaldan estas audiencia y las reconocen como una oportunidad para exponer el trabajo de los pescadores; y simultáneamente desarrollan proyectos como el Pacto integral por la restauración de la pesca artesanal en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una iniciativa con más de 700 organizaciones de pescadores artesanales entre marítimas y continentales, que hacen un en pro de desarrollo sostenible del país, y la protección los ríos, humedales, mares y conocimientos ancestrales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*(También le puede interesar el podcast Voces de la Tierra sobre pescadores artesanales*)

<!-- /wp:paragraph -->

<!-- wp:html -->

<div class="fb-video" data-href="https://web.facebook.com/contagioradio/videos/546327875986621/" data-show-text="false" data-width="">

> [](https://developers.facebook.com/contagioradio/videos/546327875986621/)
>
> Voces de la Tierra : Pescadores Artesanales
>
> Publicado por [Contagio Radio](https://www.facebook.com/contagioradio/) en Jueves, 5 de marzo de 2020

</div>

<!-- /wp:html -->
