Title: Estudiantes de la U del Atlántico continúan exigiendo autonomía universitaria
Date: 2020-01-22 17:49
Author: CtgAdm
Category: Entrevistas, Movilización
Tags: estudiantes, paro, Pelea, Universidad del Atlántico
Slug: estudiantes-de-la-u-del-atlantico-continuan-exigiendo-autonomia-universitaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Pelea-en-la-Universidad-del-Atlántico.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/WhatsApp-Image-2020-01-21-at-10.03.47.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Estudiantes {#foto-estudiantes .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado lunes 20 de enero las directivas de la Universidad del Atlántico convocaron a una asamblea multiestamentaria para terminar con el paro indefinido establecido el pasado 25 de octubre cuando ingresó la Fuerza Pública a la Universidad. Durante la reunión se presentaron peleas, según los estudiantes, **impulsadas por grupos externos a la Institución.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La versión de los estudiantes**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Fabián Salcedo, estudiante de la Universidad del Atlántico, recordó que durante la asamblea estuvieron los ánimos muy caldeados por la discusión sobre qué pasaría con el paro. La tensión se transformó en dos agresiones a personas que intentaban llevar la moderación del evento, y la posterior respuesta de los estudiantes para defenderlos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Salcedo, algunos estudiantes permitieron el ingreso de tres pandilleros de barrios aledaños a la Universidad, que agredieron a un estudiante de Ciencias Sociales. Como respuesta, otros integrantes de la Institución buscaron defenderlo y enfrentar a los externos. (Le puede interesar:["Universidad pública: Otro escenario donde se vive el conflicto armado"](https://archivo.contagioradio.com/universidad-publica-conflicto-armado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En un segundo momento, **de nuevo intentaron atacar a la persona que estaba moderando la Asamblea y se presentó otro enfrentamiento.** No obstante, la asamblea continuó hasta que cada facultad intentó tomar decisiones, momento en que las directivas se retiraron del recinto.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No es la primera vez que se viola la autonomía universitaria

<!-- /wp:heading -->

<!-- wp:paragraph -->

La presencia de extraños en el campus de la [Universidad del Atlántico](https://www.justiciaypazcolombia.com/unees-denuncia-ante-la-opinion-publica-la-represion-que-actualmente-esta-ocurriendo-contra-el-estudiantado-de-la-universidad-del-atlantico/) ya había ocurrido en otras ocasiones, hace casi un año, cuando pandilleros ingresaron al campus y robaron elementos de computación mientras se desarrollaba el paro nacional estudiantil. (Le puede interesar: ["Estudiantes de U. del Atlántico exigen garantías para el retorno a clases"](https://archivo.contagioradio.com/u-del-atlantico-retorno-clases/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, vale la pena recordar que el actual paro es consecuencia del ingreso de la Fuerza Pública a la institución el pasado 25 de octubre, fecha en la que también se denunció la presencia de uniformados equipados con armas largas actuando en el marco de la protesta social. Adicionalmente, el paro continua en exigencia de abrir espacios de decisión, para profundizar la participación en la Institución (Le puede interesar: ["Es la primera vez que el Ejército reprime una manifestación: Estudiante del Atlántico"](https://archivo.contagioradio.com/ejercito-reprime-una-manifestacion-estudiantes-en-la-u-del-atlantico/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->

<!-- wp:html -->  
<iframe id="audio_46925429" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46925429_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->
