Title: La canción necesaria de Alí Primera
Date: 2015-10-31 16:00
Category: Cultura
Tags: 74 años de Ali Primera, Ali Primera, Canción necesaria Venezuela, Nueva canción Latinoamericana
Slug: la-cancion-necesaria-de-ali-primera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/ALI-C.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Desde que llegó al mundo el 31 de octubre de 1941 en Coro, Estado Falcón, el camino del compositor, poeta, activista político y hombre de familia Alí Primera, estuvo siempre determinado por circunstancias que pondrían a prueba su capacidad de levantarse y surgir ante la adversidad.

La pérdida de su padre a temprana edad en un accidente ocurrido durante una fuga de detenidos y la pobreza consecuencia de su pérdida, determinarían que desde muy joven tuviese que trabajar en labores como lustrabotas, boxeador, pescador y cargador de maletas, oficios que alternaba con sus estudios.

<iframe src="https://www.youtube.com/embed/zuUvShfG05k" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Se acercaría a la música gracias a su estadía en casa de su abuela "Mamapancha" y su tio Juan Primera, en San José de Cocodite, escuchando cantos de Salve y bailes de violín y clarinete. Influencias que determinarían la combinación entre lo culto con lo popular, la poesía  estilizada y la palabra del "vulgo", lo que le permitía llegar a un público amplio de manera incluyente.

Junto a Víctor Jara, Atahualpa Yupanqui,  las cantautoras Violeta Parra y Mercedes Sosa entre muchos otros,  hace parte de la historia de la nueva canción latinoamericana, misma que destacó por su espíritu libertario, búsqueda de hermandad entre los pueblos y crítica a los sistemas económicos que perjudicaban a los más pobres en cada uno de sus países.

<iframe src="https://www.youtube.com/embed/X4MjSjWKmlc" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Entre los años 1973 y 1985 lanzó 13 discos, cargados de lo que el mismo llamaba "canción necesaria" título alternativo a la entonces popular denominación de canción protesta. En una entrevista  Alí Primera señaló: "Nuestro canto no es de protesta, porque no hacemos una canción por malcriadez, no la tomamos para encumbrarnos, ni hacernos millonarios, es una canción necesaria (…) No canto porque existe la miseria, sino porque existe la posibilidad de borrarla, de erradicarla de la faz de la tierra".

<iframe src="https://www.youtube.com/embed/w9Hc-Bi-iE4" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

“Cuando yo conocí las posibilidades de la canción fue estando preso en la Digepol; allí me di cuenta de que la canción comunicaba algo que era más que la mera diversión, o el simple pasar el rato. Me di cuenta de que la canción podía influir en otro aspecto: en el carácter del hombre. Con una canción se podía expresar una respuesta a por qué estábamos presos, por qué resistíamos”.

<iframe src="https://www.youtube.com/embed/NBcpq7Y7wuc" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
