Title: Recicladores de Bogotá piden ser tenidos en cuenta en nuevo sistema de recolección
Date: 2018-02-12 12:23
Category: Uncategorized
Tags: Bogotá, modelo de basuras, recicladores
Slug: recicladores-de-bogota-piden-ser-tenidos-en-cuenta-en-nuevo-sistema-de-recoleccion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/recicladores-e1518455222421.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: UN Radio] 

###### [12 Feb 2018] 

A partir de hoy en Bogotá empieza a funcionar el nuevo sistema de recolección de basuras impuesto por la administración de Enrique Peñalosa. Con esto, los recicladores le han pedido a las empresas que se harán cargo, que los tengan en cuenta en el nuevo sistema aludiendo al **amparo que tiene su trabajo** emitido por la Corte Constitucional.

De acuerdo con Nora Padilla, vocera de los recicladores de la capital, este sector no tenía que haber entrado en un proceso de licitación en la medida en que lo que se licitó y que se adjudicó a las empresas privadas **“es la parte de basuras no aprovechables”**. Indicó que desde hace 10 años la Corte Constitucional le dio la orden a Bogotá de “mantener el amparo del trabajo de los recicladores”.

Ante esto, los 18 mil recicladores que hay en la ciudad han argumentado que es necesario que se realicen **esquemas de coordinación con las empresas** de recolección que inician su trabajo para que ellos puedan realizar su labor. Padilla manifestó que “no puede seguir pasando que las empresas envíen comunicaciones a los usuarios para que saquen las basuras solamente cuando pasan los carros”.

Con esto en mente, los recicladores les han pedido a las empresas que emitan una comunicación clara “en donde se le diga a la comunidad que separe los desechos y que **le entregue a los recicladores** el material antes de que pase el carro”. Esto teniendo en cuenta que “la separación no tendría efecto si la comunidad saca la bolsa blanca y pasa el carro de la basura y se la lleva”. (Le puede interesar:["Alcalde Peñalosa es irresponsable y no respeta la Corte Constitucional"](https://archivo.contagioradio.com/alcalde-penalosa-es-irresponsable-y-no-respeta-a-la-corte-constitucional-recicladores/))

### **Modelo de contenedores tuvo en cuenta las inconformidades de los recicladores** 

Con relación a la propuesta que tenía el alcalde Enrique Peñalosa, de instalar contenedores de basuras en varias zonas de la ciudad, ya los recicladores habían dicho que **esto los perjudicaba** en la medida en que sería una contenerización total de las basuras en recipientes de más de 2 metros de altura.

Ante esto y en el momento de la licitación del nuevo esquema de basuras, los recicladores plantearon sus inconformidades teniendo en cuenta la decisión de la Corte Constitucional de **proteger su trabajo**. Allí, lograron que la ciudad pusiera un número de contenedores “que no supere el 28% de cubrimiento del servicio en la ciudad”.

Ellos y ellas han manifestado que **no se han opuesto a esta medida** de instalar contenedores pues saben que es un modelo que ha cogido fuerza, pero exigen que sea una medida que esté enfocada a ayudarle a los recicladores y no a las empresas. (Le puede interesar:["Distrito ampliaría hasta 2070 la existencia del relleno sanitario Doña Juana"](https://archivo.contagioradio.com/distrito-ampliaria-hasta-2070-la-existencia-del-relleno-sanitario-dona-juana/))

### **Recicladores en Bogotá trabajan desde hace 50 años** 

Padilla le recordó a la ciudadanía que el trabajo de los recicladores se ha hecho “adelantándose a los carros que recogen la basura” y **cada vez están más organizados**. Señlaó que están en las condiciones para conversar con las empresas sobre una coordinación entre los carros y el trabajo que ellos realizan. Esto, “con el ánimo de que los prestadores no pongan en riesgo el ingreso de los recicladores”.

<iframe id="audio_23735009" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23735009_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<div class="text_exposed_show">

###### Reciba toda la información de Contagio Radio en [[su correo]

</div>

 
