Title: "No les vamos a dar el gusto de desfallecer" Marcha Patriótica
Date: 2016-11-29 13:37
Category: DDHH, Entrevistas
Tags: Agresiones contra defensores de DDHH, marcha patriotica, persecución a Marcha Patriótica
Slug: cuanto-mas-podra-resistir-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/marcha-patriotica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [29 Nov 2016] 

Durante esta semana se han registrado más agresiones al movimiento social y político Marcha Patriótica. Este año han asesinado a 125 personas que hacen parte de esta plataforma,  actualmente hay un integrante desaparecido y  el día de ayer **miembros de Marcha Patriótica Valle del Cauca recibieron un mail en el que se les amenaza de muerte**.

La misiva llega con el asunto “Lamentamos la muerte Marcha Patriótica” y el cuerpo del texto anunciaba la muerte de los integrantes de la comisión de comunicaciones de esta organización “Lamentamos muerte de los guerrilleros anderson, jamesn.Rodrigo el viejo , jose el miliciano, diana y el resto de cupula del partido de la farc en cali. **No estamos jugando hijueputas colombia libre de terroristas"**(texto sin modificaciones).

De acuerdo con Anderson Ospina,  una de las personas amenazadas en el e-mail, los responsables de estos actos serían los mismos que vienen realizando una **persecución selectiva y sistemática a Marcha Patriótica en el país**, pero que ha sido mucho más fuerte en el Sur occidente de Colombia. Le puede interesar:["No cesan las agresiones contra integrantes de Marcha Patriótica"](https://archivo.contagioradio.com/no-cesan-agresiones-contra-integrantes-marcha-patriotica/)

“Nosotros podemos pensar que esto tiene que ver con **organizaciones de carácter paramilitar, las mismas que nos asesinaron en enero en Buenaventura al compañero Adrían Quintero**, las mismas que nos han venido amenazando en Tuluá, las mismas que han venido asesinado personas en el Cauca y Valle” afirmó Ospina.

Pese a los anuncios del gobierno de prestar mayor atención y seguridad a los miembros de Marcha Patriótica, las amenazas continúan, al igual que los atentados violentos.  Para Ospina **“la respuesta institucional ha sido bastante precaria”**. Le puede interesar: ["5 propuestas de Marcha Patriótica para frenar oleada de violencia"](https://archivo.contagioradio.com/5-propuestas-marcha-patriotica-frenar-oleada-violencia/)

Las personas que aparecen reseñadas en la amenaza, atienden organizativamente las responsabilidades de Marcha Patriótica en la región y generar articulación con diferentes procesos en este lugar. De igual forma han realizado pedagogía sobre los acuerdos de paz y la implementación de los mismos.

<iframe id="audio_14255039" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14255039_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
