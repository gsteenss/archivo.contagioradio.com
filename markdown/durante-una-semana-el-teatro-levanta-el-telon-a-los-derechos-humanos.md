Title: Durante una semana el teatro levanta el telón a los Derechos Humanos
Date: 2019-04-10 19:52
Author: ambiente y sociedad
Category: Cultura
Tags: Corporación Colombiana de Teatro, Derechos Humanos, memoria, teatro en bogota
Slug: durante-una-semana-el-teatro-levanta-el-telon-a-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/IMAGEN-MUESTRA-DE-TEATRO.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/49068_1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CCT 

Del 13 al 21 de abril, la Corporación Colombiana de Teatro Sala Seki Sano, la [III Festibienal de Teatro de Bogotá](https://archivo.contagioradio.com/homenaje-santiago-garcia-festibienal-2019/) y University Of Minesota, se unen  para traer  a Bogotá la **Semana del Teatro y Derechos humanos**. Una oportunidad para disfrutar algunas de las producciones más representativas del repertorio dramático colombiano, en las que se aborda desde diferentes representaciones, personajes y miradas la realidad social del país.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/IMAGEN-MUESTRA-DE-TEATRO-192x300.png){.alignnone .size-medium .wp-image-64655 .aligncenter width="192" height="300"}

Serán ocho días de programación con espectáculos artísticos y académicos, donde el público podrá asistir a nueve obras: **Antíognas Tribunal de Mujeres, Friducha en la Cantina, Rosencrantz y Guildenstern han Muerto, Soldados, Memoria, Manuela No Viene Esta Noche, Madre Coraje, Guadalupe Años Sin Cuenta, y el Angél de la culpa**.

En cuanto al **espacio académico, que se llevara a cabo los días 15 y 19 de abril**, se trataran temas como liderazgo en derechos humanos, la paz y el teatro en Colombia, conferencias y talleres que contaran con la participación de destacados panelistas.

Las funciones tendrán lugar en **Sala Seki Sano**, ubicada en la **Calle 12 \# 2 – 65**. El costo de la boletería para las funciones de teatro, serán de **\$30.000 general y \$20.000 estudiantes**. La programación académica será de entrada libre hasta completar el aforo.
