Title: Cultivadores de coca con más de 3.8 hectáreas no son narcotraficantes: Campesinos
Date: 2017-10-27 17:44
Category: Movilización, Nacional
Tags: coca, Nestor Humberto Martínez, Sustitución de cultivos
Slug: cultivadores-de-coca-con-mas-de-3-8-hectareas-no-son-narcotraficantes-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/hoja-de-coca-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [27 Oct 2017] 

El Fiscal general de la Nación, Néstor Humberto Martínez, expresó que no está de acuerdo con el proyecto de ley del gobierno en donde se establece que los pequeños cultivadores de hoja de coca son quienes tienen hasta 3.8 hectáreas de coca, debido a que estos ya podrían ser grandes productores, sin embargo, de acuerdo con los campesinos cultivadores de hoja de coca, **la afirmación del Físcal no solo es errónea, sino que desconoce la realidad del cultivo**.

### **Los Campesinos cocaleros del Putumayo** 

De acuerdo con Yaneth Silva, integrante y líder de CONPAZ en Putumayo, las familias cultivadoras de coca en esta región tienen entre **4 y 4.5 hectáreas de coca y siguen siendo cultivadores pequeños,** “por más que digan que los cultivadores de coca que tengan más de 3.8 hectáreas son narcotraficantes o cultivadores de gran envergadura, si no lo viven, si no lo ven, no se darán cuenta que es falso”.

Además, señaló que frente a los grandes cultivos de coca de 10 o 20 hectáreas que hay tampoco puede afirmarse que pertenezcan a grandes cultivadores, porque de él dependen hasta 15 familias y agregó que **las ganancias de la coca, no quedan en los cultivadores sino en quienes la sacan del país**.

De igual forma manifestó que ese proyecto de ley debería estar abierto a todo aquel que quiera ingresar al programa de sustitución, bajo en entendido que tiene voluntad de erradicar. (Le puede interesar:["En el año han asesinado a 17 líderes cocaleros en Colombia"](https://archivo.contagioradio.com/en-el-ano-han-asesinado-a-17-lideres-cocaleros-en-colombia/))

<iframe src="https://co.ivoox.com/es/player_ek_21735604_2_1.html?data=k5aklZqadJWhhpywj5aVaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncavVz86Ytc7QusKf1NTP1MqPsMKf0dTg1trWpYzYxtGYqM7Xp8LgjMvfx9PYqYzVjMfS0MrKrcShhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Los Campesinos cocaleros de Tumaco** 

Los campesinos de Tumaco, tampoco consideran que cultivos de coca de más de 3.8 hectáreas sean de narcotraficantes, José Santacruz, integrante de ASOMUNIMA aseveró que incluso 4 hectáreas son pequeñas y que la cantidad de tierras incluso depende de la variedad de lo que se esté sembrando.

“Como en toda la producción del país los cultivos de coca también tienen sus intermediarios que tiene más ganancias” **señaló Santacruz y agregó que por hectárea un campesino se puede estar ganando 1.400.000 mil pesos** para mantener a familias de hasta 5 integrantes. (Le puede interesar: ["Mujeres cocaleras entregan 14 puntos minímos para sustitución de cultivos"](https://archivo.contagioradio.com/mujeres-cocaleras-entregan-14-puntos-minimos-para-sustitucion-de-cultivo-de-coca/))

A su vez señaló que de esa misma hectárea salen entre **7 y 8 kilos de coca, y que por ende  uno de los campesinos que superen cultivos de 3.8 hectáreas** no son grandes productores y mucho menos narcotraficantes.

<iframe src="https://co.ivoox.com/es/player_ek_21735661_2_1.html?data=k5aklZqaepKhhpywj5aVaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncavj1IqwlYqlfYzHwtPhw8jWuduf2pDZw9iPtsbVzc7Rw8nJt4zYxpDZ0diPp8Lh0crgy9PTt4zX0MjOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
