Title: Las comunidades expresan dolor ante un posible nuevo ciclo de violencia
Date: 2019-09-02 14:33
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Acuerdos de paz con las FARC, comunidades, elecciones colombia, Líderes y lideresas sociales
Slug: comunidades-dolor-ante-posible-ciclo-violencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/catatumbo-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Un centenar de comunidades y organizaciones sociales emitieron un comunicado en que expresaron su tristeza y dolor por  lo que llamaron "un nuevo ciclo de violencia". Asimismo, denuncian el incumplimiento de acuerdos por parte del Gobierno y de graves infracciones al derecho humanitario que siguen sufriendo “en materia de protección, techo, salud, alimentación, tierra y ambiente” e invitan a “**cesar los anhelos de continuar la guerra y juntarnos en la paz que nace de la justicia**”. (Le puede interesar:[Incumplimientos al Acuerdo de paz ¿Están fragmentando al partido FARC?](https://archivo.contagioradio.com/incumplimientos-al-acuerdo-de-paz-estan-fragmentando-al-partido-farc/))

Janeth Silva, lideresa dirigente de la zona de reserva campesina de Perla Amazónica en Putumayo, expresó su sorpresa y dolor ante el anuncio de algunos ex jefes de las FARC de retomar las armas: “lo tomamos con mucha preocupación puesto que nosotros hemos sufrido directamente el conflicto armado y esa decisión nos genera un desconcierto total”. Además, afirma que “**cualquier acción violenta va en contra de la paz que estamos anhelando**”.

Las comunidades firmantes de la Carta insisten en el diálogo y en la paz para poner fin a los horrores que han sufrido y que siguen en la actualidad, a través de amenazas, desapariciones, torturas, asesinatos, desplazamientos, violencia de género, así como vulneraciones en los espacios naturales en los que residen. En ese sentido, las inminentes elecciones de octubre se presentan como una oportunidad y serán decisivas para el futuro de Colombia. Janeth espera “que el Gobierno dé garantías para unas elecciones buenas” ante la violencia hacia los candidatos, mayor que en 2015, según la Misión de Observación Electoral (MOE).

Las comunidades persisten en la lucha por la paz y el rechazo a la violencia y sostienen: "**Nuestra sociedad ha empezado a despertar** y tiene un reto por seguir consolidándose".

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_40884852" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_40884852_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]

 

 
