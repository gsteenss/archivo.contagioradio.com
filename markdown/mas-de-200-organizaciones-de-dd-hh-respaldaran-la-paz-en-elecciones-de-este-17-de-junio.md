Title: La apuesta de más de 200 organizaciones de DD.HH para la segunda vuelta
Date: 2018-06-15 08:11
Category: Movilización, Política
Tags: COEUROPA, Elecciones presidenciales 2018, Organizaciones Defesnoras de Derechos Humanos
Slug: mas-de-200-organizaciones-de-dd-hh-respaldaran-la-paz-en-elecciones-de-este-17-de-junio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/acuerdos-de-paz-e1506448818344.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Prensa Hn] 

###### [14 Jun 2018]

Más de 200 Organizaciones defensoras derechos humanos agrupadas en la plataforma Coordinación Colombia Europa Estados Unidos, aseguraron que en esta segunde vuelta electoral es necesario apoyar la permanencia del proceso paz con las FARC y los avances en el proceso con el ELN, **puesto que seguir en el camino de construcción de paz es garantía de protección de derechos.**

En la misiva señalan que "dependiendo del resultado de las elecciones presidenciales "podrán materializarse las esperanzas de un país incluyente y con derechos para todos y todas, o los riesgos y amenazas de un retorno a opciones de guerra". (Le puede interesar: ["Las recomendaciones de la MOE para la veeduría de la segunda vuelta electoral"](https://archivo.contagioradio.com/las-recomendaciones-de-la-moe-para-veeduria-de-la-segunda-vuelta/))

**Las preocupaciones de las organizaciones defensoras de derechos humanos**

De acuerdo con el documento, las organizaciones defensoras de derechos humanos tienen memoria de los graves impactos en materia de respeto a los derechos, la paz y el Estado de derecho de los gobierno de la "Seguridad Democrática", razón por la cual, al igual que expresiones del movimiento social y millones de víctimas afectados por el conflicto, les "preocupa la perspectiva de un retorno a escenarios de guerra, despojo, victimización e incumplimiento de los Acuerdos de Paz".** Acciones que además, podrían profundizar y favorecer la impunidad sobre quienes se han beneficiado de la violencia política en el país**.

En ese mismo sentido, en el comunicado se afirma que luego de más de medio siglo en guerra, Colombia está experimentando el fin del conflicto armado por la vía negociada  y el comienzo de la construcción de una paz estable y duradera, que se ha demostrado con la disminución de las víctimas por la confrontación armada que podrían verse fuertemente afectadas con los resultados de esa segunda vuelta.

Razón por la cual la Coordinación Colombia-Europa-Estados Unidos, la Plataforma Colombia Democracia Derechos Humanos y Desarrollo, y la Alianza de Organizaciones Sociales y Afines, plataformas que agremian a las organizaciones,  hicieron un llamado a la sociedad en general para que **"exprese su apoyo decidido a continuar por el camino de construcción de paz"**, a que se continúe la implementación de los Acuerdos de Paz y para que siga el proceso de paz en La Habana, entre la guerrilla del ELN y el gobierno nacional.

[130618 Comunicado Paz y Apuestas Electorales](https://www.scribd.com/document/381817810/130618-Comunicado-Paz-y-Apuestas-Electorales#from_embed "View 130618 Comunicado Paz y Apuestas Electorales on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_32654" class="scribd_iframe_embed" title="130618 Comunicado Paz y Apuestas Electorales" src="https://www.scribd.com/embeds/381817810/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-6E6XVXO6ICTEszyAAU6c&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.707221350078493"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
