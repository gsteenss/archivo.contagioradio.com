Title: Indulto a Alberto Fujimori es ilegal según organizaciones de DDHH y víctimas
Date: 2017-12-26 10:59
Category: DDHH, El mundo
Tags: Alberto Fujimori, indulto de Fujimori, Pedro Pablo Kuczynski, Perú
Slug: indulto_alberto_fujimori_peru_impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/peru_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Reuters] 

###### [26 Dic 2017] 

Con pancartas que llevaban mensajes como **"Fujimori asesino" y "¿Dónde están los desaparecidos?", miles de peruanos salieron a las calles,** y se plantaron  cerca de la casa del presidente Pedro Pablo Kuczynski, para protestar en contra del indulto otorgado por PPK, quien recientemente logró evitar su destitución, a Alberto Fujimori, que cumplía una pena de 25 años por delitos de lesa humanidad por las masacres ocurridas entre 1990 - 2000.

Quienes encabezaban las múltiples movilizaciones eran los familiares de las víctimas de masacres como la de Barrios Altos y La Cantuta, por las que el ex mandatario había sido condenado. No obstante, en medio de las protestas Kuczynski dijo que fue la decisión más "compleja y difícil de su vida", y agregó que "se trata de la salud y posibilidades de vida de un ex presidente del Perú".

### **¿Pago de favores?** 

En medio de la represión contra las protestas, estas se han mantenido fuertes, ya que muchos creen que se trató de un pago de favores políticos. Kenji Fujimori desobedeció la orden del partido que lidera su hermana y decidió abstenerse y no votar la destitución de PPK, lo que hizo fracasar la moción contra el actual presidente, y por lo cual se algunos concluyen que Kenji respaldó a Kuczynski buscando el indulto a su padre.

“Muchas gracias presidente Kuczynski, por este gesto magnánimo que me ha reconfortado. Desde este lugar me aúno a las esperanzas de todos los que luchan por la grandeza”, expresó Alberto Fujimori desde la clínica, agradeciéndole al presidente su decisión.

De acuerdo con Mirko Lauer, analista entrevistado por la agencia de noticias AFP, "**es evidente que ha habido un canje de vacancia presidencial (destitución) por indulto**", asegura. (Le puede interesar: [Acuerdo para no sacar a PPK tendría que ver con indulto a Fujimori)](https://archivo.contagioradio.com/ppk-indulto-fujimori/)

### **Un indulto ilegal según las víctimas** 

Durante las protestas, quienes se manifestaron aseguraron durante sus recorridos que se fue un indulto ilegal. Así lo afirmaron en una carta redactada por familiares de las víctimas de la masacre La Cantuta. El caso en el que fueron secuestrados, torturados, y asesinados nueve estudiantes universitarios y un profesor, por el Grupo Colina adscrito al Ejército peruano de ese entonces.

"El indulto humanitario se concede a personas que padecen enfermedades terminales y no terminales cuando estas son irreversibles o degenerativas, en el caso de Fujimori, **él no cumple con ninguno de estos requisitos**", dice la carta escrita por los familiares de esa masacre.

Asimismo, pidieron la impugnación del informe presentado por la Junta Médica, ya que denuncian la violación de la imparcialidad de la evaluación y actuación del Estado, lo que conlleva una violación del debido proceso, como lo explican en la misiva.

### **Las acusaciones contra Fujimori** 

Una de las acusaciones contra el expresidente, por muchos calificados como un dictador, fue el **“autogolpe” en 1992, en el que disolvió el Congreso y el Poder Judicial, l**o que le permitió consolidar un régimen autoritario, a través del cual ejerció reiteradas violaciones a los derechos humanos.

Entre esos casos de violaciones a los derechos humanos, se encuentra la masacre de Barrios Altos y la Catatuna, en ambas se encuentra involucrado el Grupo Colina del Ejército. Fujimori fue acusado por dar la libertad a ese grupo, y por irrespetar el derecho a la vida, el derecho a la integridad personal, las garantías judiciales, la protección judicial y la libertad de pensamiento y de expresión, entre otros.

El primer caso consistió en **el asesinato de 15 personas, incluido un niño de 8 años, durante una fiesta** en la que se creía que participaban miembros de Sendero Luminoso, lo que fue descartado por la justicia.y  por las cuales fue condenado a 25 años. Por otra parte, el caso de La Cantuta del que fueron víctimas nueve personas de la Universidad Nacional Enrique Guzmán y Valle, el 18 de julio de 1992.

Otro de los casos por cuales ha sido denunciado Alberto Fujimori junto a tres ex ministros de Sanidad, fue el **plan de esterilizaciones forzosas.** Según las organizaciones de mujeres del Perú, "bajo presiones, amenazas e incentivos con alimentos, sin que fueran debidamente informadas", más de 200.000 mujeres, la mayoría indígenas fueron esterilizadas entre 1996 y 2000. Una situación que además produjo **la muerte de 18 de ellas. **

Además, recientemente, Chile aprobó el pedido realizado por la justicia peruana en el 2015, para que Fujimori sea procesado por  el asesinato por parte del Grupo Colina, de seis campesinos en el distrito de Pativilca, Barranca, ocurrido en 1992. (Le puede interesar: [8 años menos para Fujimori)](https://archivo.contagioradio.com/8-anos-menos-de-condena-para-fujimori/)

Ante esa situación, organizaciones internacionales de DDHH tales como Amnistía Internacional lamentan la noticia y aseguran que "dicha medida viola además las obligaciones del Estado peruano frente al derecho internacional y es un retroceso en la justicia para las víctimas de violaciones a los derechos humanos en el Perú", y agregan que se** "debilita la lucha contra la impunidad al extinguir la acción penal en contra de Fujimori** por la matanza de seis campesinos en la localidad de Pativilca, en la región de Lima".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
