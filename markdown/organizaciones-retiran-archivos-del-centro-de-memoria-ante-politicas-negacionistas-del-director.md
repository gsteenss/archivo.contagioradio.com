Title: Organizaciones retiran archivos del Centro de Memoria ante políticas negacionistas del director
Date: 2020-03-02 18:47
Author: CtgAdm
Category: Memoria, Paz
Tags: Dario Acevedo, Memoria Histórica
Slug: organizaciones-retiran-archivos-del-centro-de-memoria-ante-politicas-negacionistas-del-director
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Sin-Olvido-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto Memoria: Contagio Radio

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Tal como anunció desde el nombramiento de Dario Acevedo como director del Centro Nacional de Memoria Histórica (CNMH), la [Asociación Minga,](https://twitter.com/asociacionminga/status/1234553213510090758)confirmó que a partir del 3 de marzo del presente año retirará los archivos físicos aportados a la institución **ante el riesgo que implica para la organización, la protección y buen uso que se le dé a la información recolectada bajo la actual dirección.** Con su retiro, afirman, se reitera que dichos archivos constituyen la afirmación de la existencia del conflicto armado en Colombia. [(Lea también: Centro de Memoria y Fedegan ¿una alianza para exonerar a responsables del conflicto?)](https://archivo.contagioradio.com/centro-de-memoria-y-fedegan-una-alianza-para-exonerar-a-responsables-del-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde que Acevedo fue nombrado en el cargo el en febrero de 2019, Minga expresó su preocupación ante las posturas del actual director, relacionadas a la negación del conflicto armado, así como de estigmatizar a víctimas y organizaciones. Ante ello, la organización es enfática en que el CNMH en el pasado ha tenido una visión incluyente por lo que debe mantenerse como garante de un derecho colectivo de la sociedad. Pese a ello, bajo la dirección de Darío Acevedo han evidenciado que se ha privilegiado unas memorias sobre otras, llegando a negar la memoria de las víctimas. [(Le puede interesar: Sin lineamientos, centros de memoria pueden convertirse en aparatos ideológicos de Gobierno)](https://archivo.contagioradio.com/sin-lineamientos-centros-de-memoria-pueden-convertirse-en-aparatos-ideologicos-de-gobierno/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Diana Sánchez su directora, afirma que la visión de Dario Acevedo, y su posición negacionista ante la existencia del conflicto, da a entender que a su juicio, el material entregado por la organización "no es válido ni legítimo". Agrega que los funcionarios de la administración Duque, han afirmado no modificar el acuerdo de paz y sin embargo "en la practica pasa lo contrario" por lo que el caso del director del CNMH también podría obedecer a una doble agenda. [(Lea también: La ruptura de confianza entre la víctimas y el Centro Nacional de Memoria Histórica)](https://archivo.contagioradio.com/victimas-memoria-historica/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "Darío Acevedo conspira contra la construcción de memoria"

<!-- /wp:heading -->

<!-- wp:quote -->

> La dirección de Darío Acevedo conspira contra la recuperación y construcción de la memoria histórica del conflicto armado y de las violaciones de derechos humanos por parte del Estado, la Fuerza pública y otros actores que han afectado a nuestra sociedad.

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque los archivos físicos compartidos con el CNMH bajo el mandato del profesor Gonzalo Sánchez serán retirados, han solicitado al Centro que el material digital del archivo que continuará existiendo en la plataforma virtual sea usado únicamente con fines de consulta, sin acudir a algún tipo de censura. **Agregan que todo tipo de relacionamiento con la institución será suspendido mientras Dario Acevedo continúe al frente.** [(Le puede interesar: Con Darío Acevedo la memoria sobre el paramilitarismo está en serio riesgo: Antonio Sanguino)](https://archivo.contagioradio.com/con-dario-acevedo-la-memoria-sobre-el-paramilitarismo-esta-en-serio-riesgo-antonio-sanguino/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Minga, organización que lleva trabajando más de 25 años junto a las poblaciones afectadas por el conflicto armado reiteró, a su vez, su compromiso en la reconstrucción de la memoria histórica de manera autónoma y con los movimientos sociales y de víctimas"; al tiempo, aclaró que continuarán en disposición de trabajar con la institucionalidad si esta demuestra voluntad de aportar a la verdad, justicia, reparación y no repetición. [(Le recomendamos leer: La memoria histórica del país no se puede crear desde la ambigüedad de Darío Acevedo)](https://archivo.contagioradio.com/la-memoria-historica-del-pais-no-se-puede-crear-desde-la-ambiguedad-de-dario-acevedo/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
