Title: El vía crucis del amor eficaz a 50 años de Camilo
Date: 2016-02-15 13:43
Category: Abilio, Opinion
Tags: camilo torres, Monseñor Romero
Slug: el-via-crucis-del-amor-eficaz-a-50-anos-de-camilo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/camilo-torres-50-anos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<div class="adn ads">

<div class="gs">

<div id=":y6" class="ii gt m152e6181360baa3e adP adO">

<div id=":y2" class="a3s">

<div class="gmail_quote">

###### Foto: Contagio Radio 

#### [**Abilio Peña**](https://archivo.contagioradio.com/abilio-pena-buendia/)- @abiliopena 

###### 15 Feb 2016

[Cerca de setecientas personas de diferentes regiones de Colombia peregrinaron hasta Barrancabeja   para conmemorar la fuerza del amor eficaz que animó al padre padre Camilo Torres Restrepo. El deseo era llegar hasta Patio Cemento, en el municipio del Cármen de Chucurí, lugar en el que perdió la vida el sacerdote,  quien al final de 13 años de  una vida intensa como seminarista, sacerdote, funcionario público, sociólogo, fundador de la facultad de sociología de la Universidad Nacional, tomó la decisión de unirse al naciente Eln, cuando vio esa vía como la única que podía general las transformaciones sociales en las que creía.]

El remoquete sensacionalista de “cae cura guerrillero” con el que los medios empresariales acompañaron la fotografía de sus despojos mortales en la que no cabía duda de que él era el muerto, ha estado en la retina de quienes superficialmente supieron de su paso por la historia de Colombia.  Esa figura fue reforzada por la novela de Joe Broderick con  un título que vendió centenares de ejemplares en  “El Circulo de Lectores” como es “Camilo Torres el Cura Guerrillero”.

A pesar de  todo el esfuerzo de quienes han abordado a fondo la vida de Camilo,  de quienes se han hecho interpelar por su testimonio, la tensión entre la imagen bien aprovechada por el establecimiento del “Camilo Guerrillero”, del Camilo del Eln, estuvo latente en la peregrinación de los cincuenta años. Al llegar a Barrancabermeja  difícilmente alguien daba razón de la plaza “Camilo Torres”.  Luego de explicar que se trataba del lugar  en el que había un busto de homenaje a él,  recordaban los lugareños: “a sí... el parque del descabezado” por que en ya varias ocasiones esta concreción de la memoria   ha sido destruida, del modo como los “mochacabezas”  lo hicieran con decenas de víctimas de la región.

Al llegar al Club Infantas  y preguntar por el acto de homenajea Camilo Torres, el guarda de seguridad  manifestó enfático “aquí no hay ningún acto con ese nombre”, aparece  es el “conversatorio de la Uso”. Ya casi a punto de dar inicio al homenaje  se quedó sin fluido eléctrico el club entero, regresando luego de finalizado el evento, cuando los organizadores se las habían arreglado para adelantar el multitudinario homenaje. Días atrás  se había presentado la negativa  a facilitar  el auditorio del Club Miramar  por parte del gerente de la refinaría de Ecopetrol, argumentando que ahí se alojarían guerrilleros (http://justiciaypazcolombia.com/Gobierno-Colbiano-debe-garantizar-peregrinarian-por-el-amor-eficaz).

[La  preparación de la peregrinación hacia Patio Cemento pudo llamarse más exactamente  ]*[Vía crucis]*[, es decir, Camino de la Cruz. Antes de la salida  se conocieron los planes del alcalde, de concejales del Cármen de Chucurí para obstruir,  y así fue. Al menos 8 estaciones acompañaron ese peregrinar: Un árbol fue cortado y atravesado en la vía, en San Vicente de Chucurí exhibieron una  pancarta en contra de la peregrinación, un segundo árbol obstruyó el paso, una grapa pinchó la llanta de un  vehículo, un tercer árbol sobre la vía obligó a una nueva parada, uno cuarto, uno quinto, uno sexto y uno séptimo obligando la caravana a detenerse mientras se habilitaba el paso. Y al final la misma policía que protegía el recorrido puso la última  estación: “La Pista, pues cerca de ahí está el puente del Opón donde hay un grupo de manifestantes. Tenemos todo un despliegue  del Esmad, acciones coordinadas con el ejército, no hay líderes unificados de los manifestantes que están en contra de ésta peregrinación, no podemos garantizar... ”. Ese sí pero no, impidió llegar al final de este camino. Primó el temor  a los roces, la intención de no dejarse provocar, que el deseo de arriesgarse hasta el lugar en que cayó Camilo aquiel 15 de febrero de 1966 . Nada se supo  de si habría o no investigaciones a quienes obstruyeron las vía pública, del que la policía fue testiga directa.]

[En la última estación, la Eucaristía. El Beato salvadoreño Mons Romero había iniciado celebraciones eucarísticas al final de la vía bloqueada por los militares que en su país obstruían el paso de los peregrinos, ese ejemplo inspiró la improvisación del altar en  la margen de la vía. Ahí  el sacerdote del amor eficaz, el que pidió la entrega hasta las últimas consecuencias por la transformación de las estructuras sociales, brilló en todo su esplendor. Fue  el Camilo  que dijo a los cristianos que el hambre si era mortal, quien  dijo a las mujeres que ellas tenían un papel trascendental por que no se habían  dejado seducir por las mieles del poder como si ha ocurrido a los hombres, quien habló a los presos políticos alentándolos a persistir en sus convicciones, quien animó a estudiantes a dejar el modo de vida de los privilegiados, quien invitó a la iglesia a no estar del lado de los poderosos. Ese fue el   sacerdote, hombre, académico, líder social del amor eficaz que se reivindicó en la peregrinación que  terminó convirtiéndose  ]*[vía crusis]*[.]

</div>

</div>

</div>

</div>

</div>
