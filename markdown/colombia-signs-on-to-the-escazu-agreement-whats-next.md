Title: Colombia signs on to the Escazú Agreement. What's next?
Date: 2019-12-13 17:25
Author: CtgAdm
Category: English
Tags: environmental defenders, Escazú Agreement, Global Witness, Iván Duque, Somos defensores
Slug: colombia-signs-on-to-the-escazu-agreement-whats-next
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Marcha-por-el-agua-e1464906866438.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Colprensa - El Nuevo Día/Jorge Cuéllar] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[As of yesterday, December 12, Colombia joined the Escazú Accord, according to an announcement made by President Iván Duque. The accord has been signed by more than 21 countries in Latin America and the Caribbean and seeks to strengthen security protocols to protect the environment and its defenders.]

[The agreement requires states to guarantee public access to environmental information, environmental justice, communities' public participation in environmental policy debates, and the protection of environmental defenders.]

[Colombia joins countries such as Bolivia, Guyana, St. Vincent and the Grenadines, Uruguay, and Saint Kitts and Nevis as signees of the agreements. At least 11 more countries need to ratify the agreement for it to have an obligatory character.]

### [The biggest challenge is the implementation of the agreement]

Environmental leaders halted negotiations with the Duque administration during talks to end the national strike, arguing that the government refused to concede on certain points such as the protection of the Páramo de Santurbán and the ban of pilot fracking projects. They acknowledged that this move to join the international deal was a positive step forward.

"This is a step toward access to information and the improvement of participation processes and environmental justice. However, in Colombia one of the greatest problems is that we sign agreements that are executed in an intermittent way," said Diana Giraldo, member of the Ríos Vivos Movement.

According to the 2019 report of the organization Somos Defensores, 805 aggressions against social leaders were reported, of which 155 were assassinations. Sixty-three of these correspond to deaths of environmental leaders that worked toward the coca crop substitution, the defense of ancestral lands, native seeds, springs of water and the conservation of forests.

Colombia is also the second country with the most environmental leaders assassinated in the world, according to a 2018 report from the Irish organization Global Witness. The organization found 83 people were killed in Colombia, at least half of the total number of those assassinated in the world.

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
