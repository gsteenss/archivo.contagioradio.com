Title: Campesinos exigen que vuelva al aire emisora comunitaria en Socorro, Santander
Date: 2019-02-20 13:20
Author: AdminContagio
Category: Comunidad, Nacional
Tags: Emisora La Cúpula, Libertad de Prensa, Santander
Slug: campesinos-exigen-que-vuelva-al-aire-emisora-comunitaria-en-socorro-santander
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Radio-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @CNAColombia/ACPO 

###### 20 Ene 2019 

Desde 1991,  la **emisora comunitaria 'La Cúpula'** con sede en la casa campesina de la Provincia Comunera informaba a la comunidad de **El Socorro, Santander,** sin embargo, desde el 3 de septiembre de 2018 el alcalde del municipio, **Alfonso Linero Rodríguez** de forma arbitraria decidió [cerrar el establecimiento e impedir el funcionamiento de la estación radial,  cinco meses después la emisora estaría a punto de perder la licencia otorgada por el Ministerio de Comunicaciones para que esta continúe operando.]{.text_exposed_show}

[**Polidoro Guaitero**, cofundador de La Cúpula e integrante de la Asociación Coordinadora de Organizaciones Campesinas expresa su preocupación ante el cierre de la estación radial pues por]más de 20 años la emisora estuvo al servicio del campesinado para "informar. recrear, compartir y socializar nuestros procesos y enseñanzas y entrenar la consciencia de El Socorro y la región" quien agrega que el ser silenciados es un golpe para la comunidad.

> Polidoro Guaitero, comunero de Santander nos explica el cierre de La Cúpula en [\#Socorro](https://twitter.com/hashtag/Socorro?src=hash&ref_src=twsrc%5Etfw) [\#Santander](https://twitter.com/hashtag/Santander?src=hash&ref_src=twsrc%5Etfw) [\#NoNosVanASilenciar](https://twitter.com/hashtag/NoNosVanASilenciar?src=hash&ref_src=twsrc%5Etfw)[\#LaEmisoraNoSeCierra](https://twitter.com/hashtag/LaEmisoraNoSeCierra?src=hash&ref_src=twsrc%5Etfw)[\#LaVozDelCampesinado](https://twitter.com/hashtag/LaVozDelCampesinado?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/HL0rcEuIWL](https://t.co/HL0rcEuIWL)
>
> — CNA Colombia (@CNA\_Colombia) [18 de febrero de 2019](https://twitter.com/CNA_Colombia/status/1097623978598903808?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **"Los campesinos estamos silenciados"  ** 

Según explica el cofundador de la emisora, desde la Alcaldía insisten en que no fueron entregados algunos papeles para renovar el contrato de concesión de la casa campesina, por tanto de la sede de la emisora, sin embargo Guaitero afirma que cuentan con la  documentación que demuestran lo contrario, a pesar de ello, en lugar de iniciarse un proceso de terminación de contrato, el alcalde procedió a realizar un desalojo y clausurar la emisora, una medida que obstruye la libertad de prensa y el acceso a la información.

Aunque la Asociación Coordinadora de Organizaciones Campesinas del Socorro ha apelado al Ministerio de Comunicaciones y al Gobierno departamental presentando diversas acciones judiciales para el reconocimiento de sus derechos, no ha sido posible llegar a un acuerdo pues sus peticiones han sido negadas y otras continúan en trámite.

**¿Qué alternativas tiene 'La Cúpula'?**

Ante la negativa de la Alcaldía para devolverles su espacio radial, Polidoro Guaitero indica que han solicitado les dejen funcionar únicamente la emisora y que la Alcaldía disponga de los demás espacios de la casa campesina,  mientras que otra probabilidad es la de trasladar La Cúpula a otro lugar, "el lío es que nosotros no tenemos la plata, además para eso se requiere tiempo, estudios y para eso no tenemos muchas posibilidades" afirma el cofundador de la estación radial.

Guaitero señala que el mandato del alcalde Linero finaliza este año y que quizá con la llegada de un nuevo mandatario las cosas sean diferentes sin embargo "el daño esta causado" pues hay pérdidas en el personal de trabajo, la parte comercial y la publicidad de la emisora cayeron en picada y todos los proyectos y programas coordinados fueron detenidos.

Aunque también han estado evaluando la posibilidad de convertirse en una emisora virtual y realizar notas periodísticas en otras emisoras, dichas opciones también representan un costo por lo que Guaitero y los demás integrantes de La Cúpula se han centrado en usar la vía del diálogo y la negociación para poder ver una vez más el regreso la emisora a los diales de la población de El Socorro.

###### Reciba toda la información de Contagio Radio en [[su correo]
