Title: ¿La economía colombiana puede sobrevivir sin Fracking?
Date: 2018-11-26 13:28
Author: AdminContagio
Category: Voces de la Tierra
Tags: acipet, Ambiente, Fracking en Colombia
Slug: economia-colombiana-puede-sobrevivir-sin-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/impactos-del-fracking-colombia1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Gesternova.com 

###### 26 Nov 2018

El debate por la implementación de la fractura hidráulica (Fracking) para la explotación de yacimientos no convencionales en Colombia sigue. Frente a aquellos que defienden que **el país debe abstenerse, siguiendo el principio de precaución**, de extraer el gas y el petróleo en regiones como el Magdalena Medio, están quienes aseguran que **es necesario explotar nuevos recursos en hidrocarburos** que alcanzarían para solventar una década más de demanda.

Para exponer argumentos a favor y en contra de ambas posturas, nos acompañaron en Voces de la tierra, **Julio César Vera**, Presidente de la Asociación Colombiana de Petróleos y **Luis Alvaro Pardo**, economista especialista en derecho constitucional y minero; abordando las oportunidades como las amenazas que esta técnica representar para la economía y el ambiente.

<iframe id="audio_30330352" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30330352_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
