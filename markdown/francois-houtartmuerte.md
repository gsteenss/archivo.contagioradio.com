Title: François Houtart. Una vida dedicada a la lucha por la liberación de los pueblos
Date: 2017-06-06 12:20
Category: Otra Mirada
Tags: colombia, comunidades, François Houtart, Iglesia, paz
Slug: francois-houtartmuerte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/5107863043_ac2f168a6c_b.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El ciudadano 

###### 06 Jun 2017 

###### Por: Napoleón Saltos Galarza, 

"Debemos encontrar un nuevo paradigma de vida frente al paradigma de muerte. El paradigma del "Bien común de la humanidad" (François Houtart)

Al amanecer del 6 de junio de 2017, falleció en Quito, **François Houtart,** teólogo y sociólogo de la liberación de los pueblos. Nació en Bruselas en 1925. Fue ordenado sacerdote en 1949. Licenciado y Doctor en Sociología por la Universidad de Lovaina.

Tempranamente surgió como una de las voces para la renovación de la Iglesia. Para la preparación del Concilio Vaticano II, el Presidente de la Conferencia Episcopal Latinoamericana (CELAM, Dom Hélder Câmera, le encomienda sistematizar la propuesta de la Iglesia de nuestro Continente para presentarla en la apertura del Concilio.

Su voz ha acompañado las luchas de los pueblos desde la década de los 50 del siglo pasado. Ninguna lucha le era extraña. En una misma semana podía estar en Vietnam, en reuniones con el Partido Gobierno, y luego en Siria, para buscar acuerdos de paz. Luego en América Latina podía pasar por las mesas de negociación de las FARC, hablar con el PT sobre la crisis en Brasil. Una conferencia en Argentina, un curso en la Escuela de Formación de los Sin Tierra, una reunión en La Habana. Trotamundos incansable, en búsqueda de la palabra, de las semillas de los de abajo, desde el Sur, portador de la palabra de esperanza desde la ciencia, la reflexión, la teología.

Nuestro país tuvo el privilegio de ser elegido como el hogar de residencia de François en estos últimos años. La Fundación Pueblo Indio, fundada por Monseñor Leonidas Proaño, fue su nueva casa. Profesor del Instituto de Altos Estudios (IAEN), docente de la Maestría de Sociología Política de la Universidad Central. Cuando estaba en el país, todos los miércoles se reunía con el Grupo de Pensamiento Alternativo, para informar sus periplos por el mundo, analizar la situación del Ecuador y de América Latina, programar las nuevas solidaridades y debatir sobre las alternativas.

Uno de los últimos actos de su vida comprometida fue la participación en el Taller de Pukahuaiko, la sede de la tumba de Proaño, para acordar el nuevo Mandato de la vida, junto a los pueblos y comunidades indígenas, las comunidades cristianas de base, las organizaciones sociales. La víspera de su muerte, organizó el Acto de solidaridad con el pueblo Tamil de Sri Lanka, para pedir que el Gobierno ecuatoriano, como Presidente de turno del Grupo de los 77, plantee una Investigación Internacional sobre el genocidio del sigo XXI.

Fundó con Samir Amín el Centro Tricontinental (CETRI), la Revista “Alternatives Sud” y el Foro Mundial de Alternativas, como tribunas de pensamiento sobre las luchas y las alternativas en el Tercer Mundo. En el 2009, como asesor del Presidente de la ONU, Miguel D’Scoto, inició el trabajo sobre un nuevo paradigma civilizatorio, el Bien Común de la Humanidad, que es su legado teórico-político más importante, en el que trabajó hasta el final de su vida.

Escritor incansable. Publicó alrededor de 70 libros, un promedio de uno anual, además de artículos, ponencias. Casi imposible saber cómo lo hacía. Los títulos muestran el recorrido de su pensamiento: El cambio social en América Latina (1964), Iglesia y Revolución. Religión e Ideología en Sri Lanka. Religión y desarrollo en Asia (1976), Sociología de la religión (1992) El otro Davos (1999), Haití y la mundialización del cultura (2000), Deslegitimar el capitalismo. Reconstruir la esperanza (2005) La ética de la incertidumbre en las ciencias sociales (2006) Africa codiciada. El desafío pendiente (2007) De los bienes comunes al bien común de la humanidad (2012) El bien común de la humanidad (2013) El camino a la utopía y el bien común de la humanidad (2014) El camino a la utopía desde un mundo de incertidumbre (2015)

Acompañó el proceso de los Gobiernos “progresistas”, entre el apoyo vigilante y la crítica serena. Una característica de su pensamiento fue la crítica con la presentación de alternativas. En los últimos dos años la pregunta era sobre el “cierre de ciclo” en América Latina, la crisis y decadencia del capitalismo, y la necesidad de abrir nuevas alternativas.

Acabó de escribir el segundo tomo de sus Memorias, que será publicado postúmamente. Allí podremos ver el camino recorrido, sus dudas y esperanzas, su mensaje sobre la comunidad universal, libre y justa, que soñó. Hoy rendimos homenaje a su memoria y recogemos su legado.
