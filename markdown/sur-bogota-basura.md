Title: Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más
Date: 2019-01-30 13:18
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: Basuras, Doña Juana, Peñalosa, Relleno Sanitario
Slug: sur-bogota-basura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/relleno-sanitario-doña-juana-e1548870723322-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

###### [30 Ene 2019] 

El pasado lunes, la alcaldía de Enrique Peñalosa anunció que mediante la construcción de un dique para contener los residuos, la adecuación de vías y la creación de una planta de tratamientos para lixiviados, **espera poder extender la vida útil del relleno sanitario Doña Juana 37 años**. Sin embargo, los habitantes de los barrios aledaños al relleno piden soluciones de fondo para resolver el tema del depósito final de las basuras que produce la capital.

> Extender la vida del relleno sanitario Doña Juana es otro de los proyectos en los que estamos trabajando: [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw) [\#PeñalosaLeCumpleABogotá](https://twitter.com/hashtag/Pe%C3%B1alosaLeCumpleABogot%C3%A1?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/Xepmhjgblu](https://t.co/Xepmhjgblu)
>
> — Alcaldía de Bogotá (@Bogota) [28 de enero de 2019](https://twitter.com/Bogota/status/1089921955522928645?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
Los habitantes de barrios como El Mochuelo, Sotavento, Lucero o La Aurora han tenido que **padecer durante 31 años los efectos de vivir cerca al depósito de basuras de Bogotá**, **que recibe cerca de 6.300 toneladas de residuos a diario** según el Ministerio de Ambiente. De acuerdo a **Javier Reyes, integrante de Asamblea Sur**, esta situación ha significado que los habitantes de esa zona de la ciudad tengan un **promedio de vida 10 años menor** al que tienen el resto de los bogotanos.

Según explica Reyes, el anuncio sobre la construcción del muro para contener y distribuir mejor las basuras en el Relleno **es una propuesta del 2005**, pero el dique sería solo una medida de optimización del espacio del basurero mientras se planteaban soluciones de largo plazo. Con la planta para el tratamiento de Lixiviados ocurre lo mismo, pues debió haber sido construida tiempo atrás, pero esa obra no se ha hecho.

Por lo tanto, el integrante de Asamblea Sur concluye que estos anuncios para extender la vida útil del relleno son la continuidad de un discursos que no soluciona los problemas de fondo, son **una respuesta más barata en términos económicos pero más cara para el ambiente**; y de la que se vera nuevamente beneficiado el administrador del relleno, que ahora recibirá recursos para hacer obras que debieron hacerse hace más de 10 años.

### **La situación de los barrios aledaños al basurero no ha mejorado** 

Tras el derrumbe de residuos que ocurrió en el Relleno en 1997, los habitantes de barrios circundantes al relleno han tenido que sufrir los olores, y la proliferación de insectos generados por Doña Juana. Tan constante como han sido esos problemas, lo han sido las respuestas de las distintas administraciones distritales, que luego de que se vuelve a hablar del tema, señalan que están brindado soluciones como estas, para calmar el estado de la opinión pública.

Sin embargo, en este caso, Reyes resalta que se debe continuar buscando una respuesta definitiva al problema porque las estructuras que se piensan construir en el Relleno yacen sobre dos fallas geológicas: Yerbabuena y Tunjuelo. De forma tal que **poner más peso sobre las fallas, "en una ciudad que está en riesgo por sismos, como lo hemos evidenciado recientemente", es poner en peligro a Bogotá**. (Le puede interesar: ["Trabajos en Doña Juana son insuficientes, y no resuelven el problema estructural"](https://archivo.contagioradio.com/acuerdo-entre-distrito-operadores-y-habitantes-de-mochuelo-alto-son-superficiales/))

### **La política nacional  ya ha dicho lo que hay que hacer con las basuras** 

Para finalizar, el activista señaló que la política nacional ya ha establecido que se deben implementar unidades para aprovechar o tratar los residuos: **La gasificación y la termólisis.** La gasificación es un proceso similar a la incineración de las basuras, pero que emite menos gases contaminantes al aire. (Le puede interesar: ["Ordenan a Peñalosa detener tala de árboles en Parque Japón"](https://archivo.contagioradio.com/penalosa-detener-arboricidio-parque-japon/))

Mientras tanto, la Termólisis es un proceso que separa los distintos materiales y permite extraer combustible solido de ellos, que posteriormente puede ser utilizado para la creación de energía o vendido para ser utilizado en la industria. Durante la alcaldía de Petro, se realizó un documento de acuerdo para realizar una planta de este tipo, **pero el actual Alcalde no ha hecho lo necesario para desarrollar esta obra.**

<iframe id="audio_32000790" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32000790_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
