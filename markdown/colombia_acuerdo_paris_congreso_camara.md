Title: Falta un debate para que Colombia empiece a hacer efectivo el Acuerdo de París
Date: 2017-06-07 18:20
Category: Ambiente, Voces de la Tierra
Tags: Acuerdo de París, cambio climatico, Congreso de la Rapública
Slug: colombia_acuerdo_paris_congreso_camara
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/CuyZJeJWcAIMIDw-e1496877484926.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Scoopnest.com 

###### [7 Jun 2017] 

Colombia avanza hacia de manera lenta en la consolidación de compromisos que permitan que se cumpla lo pactado en la COP 21 de París. Este miércoles la Comisión Segunda de Cámara de Representantes aprobó de forma unánime el proyecto de ley referente al acuerdo contra el cambio climático.

Se trata de un proyecto que ratificaría el Acuerdo de París y que obligaría al país a cumplir los compromisos adquiridos en esa cumbre mundial. La Ley implicaría que **Colombia reduzca en un 20% las emisiones de Gases de Efecto Invernadero, así mismo se busca avanzar hacia la adaptación frente a los efectos del aumento del calentamiento global, y proyectar el modelo económico** entorno a actividades que signifiquen pocas emisiones.

Aunque se esperaba que la Ley hubiese pasado el trámite en el Congreso a finales de esta legislatura, al proyecto aún le hace falta pasar el debate en plenaria de Cámara de Representantes, para que luego sea sancionado por el residente Juan Manuel Santos.

“Con este instrumento legislativo lo que hacemos es reforzar la base legal para que podamos avanzar hacia una Política Nacional de Cambio Climático, fortalecer la Comisión Intersectorial de Cambio Climático y abrir la posibilidad de que los sectores puedan hacer una inversión importante en reconversión industrial para generar menos emisiones”, explica el ministro de Ambiente Luis Gilberto Murillo.

### La responsabilidad de Colombia 

Si bien el país es responsable solo del 0,4% de las emisiones globales; **es uno de los ocho países culpable de más de la mitad del deterioro del planeta, y además es uno de los 15 países más vulnerables a los impactos del cambio climático.**

En esa medida se hace imprescindible que Colombia genere las acciones y estrategas necesarias para combatir el aumento de las temperaturas. Según ha identificado el IDEAM, **las tareas tendrían que centrarse en los departamentos de Antioquia, Meta, Caquetá, Valle del Cauca y Santander** que son los mayores emisores de gases.

A su vez, esos programas deberán transformar el campo, que pese a tener vocación para la agricultura, ha sido sobrexplotado para la ganadería, cuya actividad genera las mayores emisiones de gases de efecto invernadero. De acuerdo con el Instituto Geográfico Agustín Codazzi de los 114 millones de hectáreas que tiene el país, 26 millones tienen vocación agrícola, pero solo se usa 6,3 millones de hectáreas para ese fin. En cambio, **mientras que solo 8 millones de hectáreas tienen vocación ganadera,** [**se usan 38 millones para ese fin.**]{lang="EN"}

[**Así mismo, se deberá poner énfasis en **]{lang="EN"}las emisiones que produce la minería, el comercio, la industria manufacturera o los residuos domiciliarios.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
