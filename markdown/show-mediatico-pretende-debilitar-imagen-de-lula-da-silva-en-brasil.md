Title: Show mediático pretende debilitar imagen de Lula da Silva en Brasil
Date: 2016-03-04 17:04
Category: El mundo, Política
Tags: Lula da Silva, Partido de los Trabajadores
Slug: show-mediatico-pretende-debilitar-imagen-de-lula-da-silva-en-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Lula-da-Silva.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Televicentro ] 

<iframe src="http://co.ivoox.com/es/player_ek_10676549_2_1.html?data=kpWjmZuZeJqhhpywj5WaaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncbTc0NyYz8rIrYa3lIquk9nNp9Cf0dfS1srSqMafxcrPy9HNuMLmjM7aw8zJsozYxpC519HFb8XVjLjWj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ana Moraes, Movimiento Sin Tierra] 

<iframe src="http://co.ivoox.com/es/player_ek_10678407_2_1.html?data=kpWjmZ2YdJihhpywj5adaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncbTc0NyYz8rIrYa3lIquk9nNp9Cf0dfS1srSqMafxcrPy9HNuMLmjM7aw8zJsozYxpC519HFb8XVjLjWj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Marcelo Borman Zero, PT] 

 

###### [4 Mar 2016 ]

Tras ser obligado a rendir indagatoria por presunta vinculación con la red de corrupción y lavado de activos en la empresa Petrobras, **el expresidente brasilero Luis Inácio Lula da Silva, se retira de la sede de la Policía Federal del aeropuerto de Sao Paulo**, a la que había sido conducido por agentes policiales luego del allanamiento a su vivienda, en el marco de la operación Aletheia.

Ana Moraes, integrante del Movimiento Sin Tierra, asegura que las organizaciones sociales y el mismo Partido de los Trabajadores rechazan la conducción obligada de Lula da Silva a la sede policial, pues **al expresidente nunca lo habían citado a rendir indagatoria**, por lo que no tendría qué estar declarando de forma obligatoria, cuando además no hay evidencias concretas que justifiquen esta medida coercitiva en contra del ex mandatario.

Por su parte Marcelo Borman Zero, asesor del Partido de los Trabajadores PT, asevera que la “conducción coercitiva (…) ilegal y antidemocrática” de Lula da Silva se da en el marco de un **“proceso esencialmente político”** denominado 'Lava Jato' y **conducido por autoridades políticas y judiciales, junto a algunos medios de comunicación con “actitudes partidarias muy conocidas”** y que buscan “humillar públicamente al presidente que es considerado por la población brasileña como el mejor presidente de su historia”, sin tener pruebas ni indicios de corrupción o desvío de dineros públicos.

De acuerdo con Borman el Juez Moro, a cargo de este proceso, está ligado al mayor partido de oposición brasilero, el PSDB, que preparó el terreno para esta operación contra Lula da Silva, y que **podría ser el “anuncio de un posible golpe de estado” contra el gobierno de Dilma Rouseff**, que ha sido tildado de haber financiado su reelección con “dinero sucio”. Según afirma el asesor del PT “la oposición recibió más dinero que la presidenta (...) y de las mismas empresas”, y su vinculación con escándalos de corrupción tiene pruebas e indicios pero no son investigados.

Marcelo Borman concluye aseverando que se tendrán que evitar los atentados contra las instituciones y las provocaciones, en las manifestaciones pacíficas que se promoverán y los pronunciamientos que se harán en el Congreso, como muestra de rechazo a la **estrategia de la oposición brasilera de intentar derrotar el proyecto económico y social que retiro a decenas de millones de brasileños de la miseria**, sin una base judicial sólida,  porque “no hay pruebas ni indicios, lo que hay son calumnias”.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/)].
