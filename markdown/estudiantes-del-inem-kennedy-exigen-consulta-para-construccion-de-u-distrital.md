Title: Estudiantes del INEM exigen consulta para construcción de Universidad en Kennedy
Date: 2015-03-21 21:39
Author: CtgAdm
Category: Educación, Nacional
Tags: Inem, Predios del Colegio INEM, Universidad Distrital
Slug: estudiantes-del-inem-kennedy-exigen-consulta-para-construccion-de-u-distrital
Status: published

###### Foto: bogota.gov.co 

Estudiantes apoyan la propuesta del alcalde Petro de construir una universidad pública en la localidad de Kennedy, pero exigen ser involucrados en las decisiones que se tomen con respecto a todo lo que tenga que ver con su colegio.

El jueves pasado, el Alcalde Gustavo Petro visitó el Instituto Nacional de Educación Media, INEM, para anunciar  la construcción de una sede de la Universidad Distrital en predios del colegio.

A causa de algunas protestas realizadas por estudiantes de la institución durante la visita, medios de información anunciaron que estos rechazaban la propuesta del alcalde, situación que ha sido desmentida por parte del estudiante del grado once e integrante de la Coordinadora Distrital de Estudiantes de Secundaria, Juan Carlos, quien asegura que "la información que los medios de comunicación les han dado ha sido falsa".

Juan Carlos, declara que sus compañeros y compañeras del colegio desean establecer un espacio de diálogo entre las directivas de su institución y la alcaldía local, con el fin de aclarar las dudas que han surgido con relación a la construcción de la universidad, y que se les tome en cuenta en las decisiones que se toman relacionadas con la institución.

Los estudiantes hacen un llamado al alcalde Gustavo Petro a que lleve a cabo un foro institucional, con el fin de desmentir tal información, donde le aclare a los estudiantes las razones que beneficiarían el realizar una universidad en Kennedy y sobre todo en el INEM. Los estudiantes están dispuestos a plantear una discusión con la alcaldía local para solucionar los malentendidos presentados y esperan dialogar con las directivas del colegio para que haya espacios de discusión.
