Title: Communities support “Defendamos La Paz” proposal
Date: 2019-08-05 09:33
Author: CtgAdm
Category: English
Tags: Communities, Defendamos la Paz
Slug: communities-support-defendamos-la-paz-proposal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/1DSC_0849.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

In an open letter communities affected by the armed conflict supported 13 proposals presented by the “Defendamos La Paz” group, a movement that seeks to ensure what has been agreed in Havanais fulfilled.  One of the principal objectives is to reopen dialogue with the National Liberation Army (ELN) and putting an end to the murders of social leaders.

According to the document, the resurgence of the armed conflict in the country manifest itself in leaders’ massacre, but also in an increase in cases of forced displacement, intimidation, sexual abuse and forced recruitment. Due to this issue, the threatened communities are forced to remain silent out of fear of being killed.

### «We cannot denounce because they’ll kill us.'' 

In rural communities, people or paid informants have been installed in each community in the same way the famous CONVIVIR paramilitaries used to.

**«We lack guarantees for free association and free expression»**, reads the letter.

In their reports they also expressed regret at the Government's decision to resume the policy of aerial spraying with glyphosate. Communities reiterated that the effects on human lives and ecosystems will be "irreparable." Despite these concerns, the Government stated this controversial measure will be put into practice next month.

Finally, they expressed strong support for the work carried out by the Special Jurisdiction for Peace (JEP) in order to guarantee truth, justice, reparation and non-repetition to the victims of the country. The authors of the letter stressed that " the Agreement’s implementation is fundamental for our democracy”. (Read also: [Art installation in Bogotá commemorates lives of murdered social leaders](https://archivo.contagioradio.com/art-installation-in-bogota-remembers-lives-of-social-leaders-lost/))

Supporting this, they announced their participation collecting 1 million signatures proposed by the Defendamos La Paz platform to establish the 16 seats for the armed conflict’s victims in the Congress. In addition, they proposed to contribute to this initiative with memories, music, dances and joy "continuing defending the new democracy of peace."
