Title: Nueva tecnología permite que mujer ciega vea a su hijo
Date: 2015-01-26 23:33
Author: CtgAdm
Category: datos, Uncategorized
Tags: eSight, tecnologia
Slug: nueva-tecnologia-permite-que-mujer-ciega-vea-a-su-hijo
Status: published

**Kathy Beitz**, es una madre estadounidense invidente que tiene la enfermedad de **Stargardt**, enfermedad  hereditaria que se caracteriza por una degeneración de la parte central de la retina, problema que **le impidió ver desde sus 11 años de edad.**

Gracias a la nueva tecnología de las [gafas eSight](http://www.esighteyewear.com/what-is-esight), Kathy pudo ver a su hijo recién nacido. Este tipo de tecnología abre la posibilidad para que más personas en el mundo puedan recuperar su visión,  aunque la dificultad de acceder a ellas es alta debido a su elevado valor, más de **quince mil dólare**s.

\[embed\]https://www.youtube.com/watch?v=e9crPvfcEy4\[/embed\]

Con esta posibilidad abierta quedan las preguntas en torno al avance de la tecnología y las personas que se verían favorecidas por ese mismo avance. Las posibilidades son infinitas, pero también lo deben ser los alcances y la cobertura de los beneficios de la ciencia.
