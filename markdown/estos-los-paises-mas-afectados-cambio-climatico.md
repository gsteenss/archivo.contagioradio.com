Title: Estos son los países más afectados por el cambio climático
Date: 2016-11-24 14:06
Category: Ambiente, Voces de la Tierra
Tags: Calentamiento global, cambio climatico, COP 22, países pobres
Slug: estos-los-paises-mas-afectados-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/honduras-e1480013921849.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [Dr Roger Guillén Montalván]

###### [24 Nov 2016] 

Durante los últimos 20 años, **Honduras, Myanmar, Haití, Nicaragua, Filipinas, Bangladesh, Pakistán, Vietnam, Guatemala y Tailandia** ocupan los primeros lugares como los países más afectados por el cambio climático.

Así lo concluye un informe del Índice de Riesgo Climático Global 2017 (Global Climate Risk Index 2017) presentado en el marco de la COP22 en Marruecos. Ese reporte también evidencia que los países más afectados por el cambio climático durante el 2015 fueron: **Mozambique, Dominica, Malawi, India, Vanuatu, Myanmar, Bahamas, Ghana, Madagascar y Chile.**

De acuerdo con Sönke Kreft, autor del informe, "Las inundaciones han afectado especialmente al continente anfitrión de la cumbre climática este año”. Según su explicación, estos países ubicados principalmente en África, América Latina y Asia se encuentran mayormente amenazados por el aumento de las temperaturas debido a que son [países pobres que no cuentan con las condiciones adecuadas](https://archivo.contagioradio.com/cambios-climaticos-afectan-con-mayor-fuerza-a-la-poblacion-vulnerable-del-mundo/) para solventar los fenómenos metereológicos que se han presentado.

"La distribución de los fenómenos meteorológicos no es justa. En 20 años de análisis de los fenómenos meteorológicos extremos, se encontró que **nueve de los diez países los más afectados son las naciones en desarrollo que pertenecen a las categorías de bajo o medio-bajo ingreso**. Ellos son por lo general los países con niveles muy bajos de emisiones, es decir, los menos responsables del cambio climático", dice Kreft.

Este índice señala que de 1996 a 2015 se registraron más de **530.000 muertes causadas por más de 11.000 situaciones meteorológicas extremas**, que además causaron daños con costos mayores a 3300 millones de dólares.

La situación no parece mejorar, si no se toman medidas extremas para frenar el cambio climático. El investigador advierte que en los próximos años se espera el aumento de lluvias fuertes, inundaciones y olas de calor. Cabe recordar que las [altas temperaturas actuales serán el promedio para el año 2035](https://archivo.contagioradio.com/altas-temperaturas-actuales-seran-nuevo-promedio-2035/), como lo reveló la Universidad Nacional de Australia.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
