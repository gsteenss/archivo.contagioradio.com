Title: Cerca de 3 mil desplazados dejaría militarización de El Mango, Cauca
Date: 2015-06-25 13:56
Author: AdminContagio
Category: Nacional, Resistencias
Tags: Argelia, Cauca, Cese al fuego bilateral, cese al fuego unilateral, Cruz Roja, Ejército Nacional, el mango, procuraduria, RCN
Slug: cerca-de-3-mil-desplazados-dejaria-militarizacion-de-el-mango-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/10369715_1085503414812471_1335676682781513305_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lizeth Montero 

<iframe src="http://www.ivoox.com/player_ek_4687748_2_1.html?data=lZulmZyYfI6ZmKiak5uJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8bmxMaYxsqPd4zhytGYxsrXtM3V28bR0diPqMbewteSpZiJhaXVjMbQ1trFp8qZpJiSpJjSb8XZjNLWzs7YcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Orlando, habitante corregimiento El Mango, Cauca] 

La comunidad del corregimiento del Mango en Argelia, Cauca, asegura que la actual situación es “muy difícil, tensa y que podría empeorar”, teniendo en cuenta la llegada de **800 efectivos de la policía** que intentan retomar el control de la zona. Adicionalmente, denuncian el accionar de los medios masivos de información desde los cuales, **se está criminalizando la acción de resistencia que están llevando a cabo los pobladores,** con el único objetivo de exigir que la Policia Nacional deje de usar a la población civil como trinchera.

La indignación de la población es tal, que aseguran que “**en RCN se pasó un informe que no era”** y señalan a este tipo de medios como “**criminales y asesinos que muestran lo que no es"**, expresa el un habitante del corregimiento.

"La amenaza ha sido latente, el odio hacia la población ha sido inminente, nos han señalado desde el gobierno como guerrilleros”, expresa el habitante de el Mango, quien añade que desde la mañana del jueves, aproximadamente **500 personas del corregimiento** **hacen guardia** a las afueras del lugar para impedir el reingreso del Ejército Nacional.

“Todo el tiempo hay sobrevuelos de helicópteros y aviones de guerra, va a haber heridos, muertos y desplazamiento, la situación va a emporar”, afirma.

La situación es tan grave, que los habitantes dicen tener listas las maletas para cuando se vean en la obligación de abandonar sus viviendas, lo que provocaría el **desplazamiento masivo de cerca de 3 mil pobladores** hacia la cabecera municipal de Argelia en busca de refugio.

La comunidad, **requiere la presencia del Estado, pero sin armas,** y proponen que exista presencia de las fuerzas militares, pero de manera perimetral, para que la vida de las personas no esté en peligro durante los enfrenamientos entre militares y guerrilleros. Así mismo, **exigen el cese al fuego bilateral,** ya que aseguran que durante la tregua unilateral, la población vivió tranquila.

Desde el Mango, **se ha solicitado la presencia de la Cruz Roja Internacional y la Defensoría del Pueblo**, pero no ha llegado ningún tipo de respuesta. También, piden que exista una veeduría de parte de la Procuraduría General de la Nación, donde se verifique la realidad de la situación que viven los habitantes del corregimiento.
