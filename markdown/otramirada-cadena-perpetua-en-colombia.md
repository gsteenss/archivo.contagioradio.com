Title: OtraMirada: Cadena perpetua en Colombia.
Date: 2020-06-24 20:54
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Cadena perpetua, Congreso de la República, reforma, Save The Children
Slug: otramirada-cadena-perpetua-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Corte-constitucional-e1470436473989.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/corte-constitucional1.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Cadena Perpetua @CConstitucional

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el programa del 23 de junio, el tema se centro en la reforma al artículo 34 de la Constitución Política que avala la cadena perpetua en Colombia para violadores y asesinos de niños, niñas y adolescentes, y cuyo debate se venía realizando en el Congreso de la República y el cual fue finalmente aprobado el jueves 18 de junio. El tema se abordó con el fin de evaluar qué tan efectiva es esta medida y a qué tantos cambios conduciría.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De esta manera, se pudieron apreciar las tres miradas de lo que implica esta reforma: la mirada internacional, la mirada desde las organizaciones para los niños y niñas y por último, la mirada desde el aspecto constitucional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nuestros invistamos fueron: **Gearóid O Loingsigh**, **periodista irlandés, Luz Granada, **integrante** de Save The Children Colombia** y **Alfredo Beltrán, expresidente de la [Corte Constitucional](https://twitter.com/CConstitucional)**. Ellos nos responden a preguntas tales como a quiénes ha favorecido realmente esta medida, cómo se ha mostrado y funcionado en otros países como Irlanda, Estados Unidos o Gran Bretaña, qué opinan las organizaciones defensoras de la niñez, qué significa esto para la Constitución Política y de igual manera, si a los congresistas realmente les importan los niños, las niñas y adolescentes o, si por el contrario, están respondiendo a ciertos intereses ocultos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, es un espacio que le permite a los ciudadanos y ciudadanas reflexionar en cuáles ha sido los errores que como sociedad hemos caído al no poder defender y proteger a los niños y niñas y qué tan acertado es o no estar a favor de la cadena perpetua en Colombia y en el mundo, en general. (Si quiere conocer sobre el tema del viernes 19 de junio: [Según la FAO el COVID 19 ha afectado al 87% del campesinado en Colombia](https://archivo.contagioradio.com/segun-la-fao-el-covid-19-ha-afectado-al-87-del-campesinado-en-colombia/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/773458143394748/?__xts__[0]=68.ARCEybn_XYp_9BVHq69FTiV3Kn247oIiWaTRuUJTzqPB3Z_iWtTti44gMXlB3EVAh708_MZw580MKrrHQufI9DM3MtOrTeeDNOqVakO0toiMFLrDwF5f84snyjQOvuek4UPjfMgbdP2otmcUwC6vT7Qs_Aatf9KRFB7E20c3jw74VI61GusDXWK4Z5M8AinB6vMmBKgzuePAi64DN4Qy5PNbVFpnHZ_S9mrhYwNi2ul-G7-g-aBOqrt398MawZEmDR9_CDYHL4ShSqLXuk7DaMHdh8NpoO1iqdCiJyuAbm3wjf3fRCLT8RP-VgaPS5o9qfQTm65FWgie_N6zky79LimyGoc\u0026amp;__tn__=-R","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/773458143394748/?\_\_xts\_\_\[0\]=68.ARCEybn\_XYp\_9BVHq69FTiV3Kn247oIiWaTRuUJTzqPB3Z\_iWtTti44gMXlB3EVAh708\_MZw580MKrrHQufI9DM3MtOrTeeDNOqVakO0toiMFLrDwF5f84snyjQOvuek4UPjfMgbdP2otmcUwC6vT7Qs\_Aatf9KRFB7E20c3jw74VI61GusDXWK4Z5M8AinB6vMmBKgzuePAi64DN4Qy5PNbVFpnHZ\_S9mrhYwNi2ul-G7-g-aBOqrt398MawZEmDR9\_CDYHL4ShSqLXuk7DaMHdh8NpoO1iqdCiJyuAbm3wjf3fRCLT8RP-VgaPS5o9qfQTm65FWgie\_N6zky79LimyGoc&\_\_tn\_\_=-R

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->
