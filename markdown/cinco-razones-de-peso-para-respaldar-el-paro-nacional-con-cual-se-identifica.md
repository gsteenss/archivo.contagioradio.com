Title: Cinco razones de peso para respaldar el Paro Nacional, ¿con cuál se identifica?
Date: 2019-04-24 18:07
Author: CtgAdm
Category: Movilización, Paro Nacional
Tags: 25 de abril, marcha, Paro Nacional, Refugio Humanitario
Slug: cinco-razones-de-peso-para-respaldar-el-paro-nacional-con-cual-se-identifica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/paro-nacional-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

[  
especial paro nacional  
](https://archivo.contagioradio.com/paro-nacional-6/)

###### Foto: Contagio Radio 

Este jueves 25 de abril se desarrollará el paro nacional, cuya consigna está orientada a rechazar el Plan Nacional de Desarrollo (pnd), por considerarlo contrario a los intereses de la mayoría de colombianos; exigir el respeto a la vida de líderes y lideresas sociales; y reclamar el cumplimiento de los acuerdos pactados entre el Gobierno Nacional y las diferentes organizaciones sociales. Estas tres reivindicaciones agrupan las demandas de estudiantes, campesinos, profesores, trabajadores y pensionados; que ya se han sumado a la movilización.

### **La UNEES va al paro por una educación al alcance de los sueños de los Colombianos**

La Unión Nacional de Estudiantes de Educación Superior (UNEES) indicó que se uniría al paro nacional como una forma de respaldo a la implementación del Acuerdo de Paz y contra el PND, porque el mismo no responde a los acuerdos alcanzados el año pasado entre estudiantes y Gobierno. Los jóvenes denunciaron que el modelo de educación contenido en el Plan profundiza la crisis que el año pasado tuvo a varias universidades al borde de cerrar programas académicos. (Le puede interesar:["Estudiantes se suman al paro nacional del 25 de abril"](https://archivo.contagioradio.com/estudiantes-suman-paro-nacional-25-de-abril/))

> [\#AParaParaAvanzar](https://twitter.com/hashtag/AParaParaAvanzar?src=hash&ref_src=twsrc%5Etfw) [\#VenYTeUNEES](https://twitter.com/hashtag/VenYTeUNEES?src=hash&ref_src=twsrc%5Etfw) [\#ParoNacional](https://twitter.com/hashtag/ParoNacional?src=hash&ref_src=twsrc%5Etfw) [\#LaLigaDeLaResistencia](https://twitter.com/hashtag/LaLigaDeLaResistencia?src=hash&ref_src=twsrc%5Etfw) [\#25deAbril](https://twitter.com/hashtag/25deAbril?src=hash&ref_src=twsrc%5Etfw) Este jueves paramos en rechazo al Plan Nacional de Desarrollo y contra el asesinato sistemático de líderes sociales. [pic.twitter.com/OcWt64rK03](https://t.co/OcWt64rK03)
>
> — UNEES Colombia (@UneesCol) [24 de abril de 2019](https://twitter.com/UneesCol/status/1121014090527334401?ref_src=twsrc%5Etfw)

### **Trabajadores apoyan el paro contra las reformas laborales y por la paz de Colombia**

Según Diógenes Orjuela, presidente de la Central Unitaria de Trabajadores (CUT), son dos los asuntos en los cuales están en contradicción con el Gobierno: Las políticas económicas y sociales del Gobierno, evidenciadas en el PND; allí habría una "reforma laboral que elimina el contrato laboral como está en Colombia, elimina el salario mínimo en Colombia con la contratación por horas" y afecta la negociación colectiva. En segundo lugar, está el incumplimiento en la implementación del Acuerdo de Paz, y la búsqueda a la salida negociada al conflicto con el Ejército de Liberación Nacional (ELN). (Le puede interesar: ["El Plan Nacional de Desarrollo, un respaldo al sector minero-energético"](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/))

### **Para reducir el aporte a salud de los pensionados del 12 al 4%**

Por su parte, Orlando Restrepo presidente Confederación de Pensionados de Colombia (CPC), invitó a todos los pensionados a acompañar el paro para rechazar "esa reforma pensional parcial que ha querido meter el Gobierno en el PND y que le están cambiando hasta el nombre, porque la quieren llamar protección a la vejez para garantizar pensiones por debajo del salario mínimo en el futuro". De igual forma, la demanda de los pensionados será cumplir el pacto para reducir el aporte del gremio a la salud del 12 al 4%. (Le puede interesar: ["Presidente Santos le hizo conejo a los pensionados: Alirio Uribe Muñóz"](https://archivo.contagioradio.com/presidente-santos-les-hizo-conejo-a-los-pensionados-alirio-uribe-munoz/))

### **Profesores se movilizan por una modificación al Sistema General de Participaciones en favor de la educación**

Como lo explico Nelson Alarcón, presidente de la Federación Colombiana de Trabajadores de la Educación (FECODE), son tres asuntos los que impulsan al magisterio a participar en el paro: El PND, el respeto a la vida de los maestros en el territorio nacional y por la defensa del pliego nacional de peticiones radicado en febrero de este año. Sobre el Plan Nacional, Alarcón explicó que "atenta contra la libertad de cátedra y la libertad escolar de los profesores, atenta contra el Gobierno Escolar", y  tiende a la privatización de la educación, buscando profundizar el modelo de concesión en los colegios públicos.

En segundo lugar, la defensa de la vida tiene dos exigencias: las garantías para los profesores en todos los territorios; y el acceso a un sistema de salud digno. Por último, se incluye el apoyo al Pliego Nacional de Peticiones radicado por la federación el pasado 14 de febrero; con el que se pide una modificación al Sistema General de Participaciones (SGP), aumentando los aportes del gobierno central a las regiones para el cubrimiento de servicios básicos, entre ellos la educación. (Le puede interesar: ["Política del gobierno Duque obligó al magisterio a salir a las calles"](https://archivo.contagioradio.com/duque-magisterio-calles/))

### **Por la vida de los líderes al refugio humanitario**

Todas las organizaciones que participan del paro apoyarán el desarrollo del Refugio Humanitario por la vida de las y los líderes sociales, que traerá a más de 3 mil defensores de derechos humanos a Bogotá; como lo indicó Leandra Becerra, vocera del Refugio, el objetivo es visibilizar las violencias que sufren en los territorios y decir que "a pesar de los más de 600 asesinatos que se han reportado desde 2016, las luchas y las causas de los líderes sociales siguen con mucha vigencia".  (Le puede interesar: ["El 28 de abril abre las puertas el refugio humanitario por la vida de los líderes sociales"](https://archivo.contagioradio.com/refugio-humanitario-vida-lideres-sociales/))

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
