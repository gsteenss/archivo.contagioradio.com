Title: El Estado sabía que se iba a perpetrar un atentado en el Club el Nogal
Date: 2017-02-07 13:21
Category: Judicial, Nacional
Tags: Bogotá, Club el nogal, FARC, víctimas
Slug: estado-sabia-que-se-iba-a-perpetrar-el-atentado-del-club-el-nogal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/bomba-club-El-nogal-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: .Radiosantafe] 

###### [07 Feb 2017] 

**"El Estado colombiano sabía que se iba a cometer un atentado en el Club el Nogal"** , con estas palabras **Berta Fries**, una de las víctimas del atentado sucedido hace 14 años  en Bogotá se refirió  a las investigaciones que se adelantan sobre este tema.

**Fries** se refirió a reuniones que se adelantaban en club entre **paramilitares y funcionarios del gobierno de Álvaro Uribe.** Afirma que hay una investigación que incluye el testimonio de un informante que entregó hasta los teléfonos celulares de la persona que instaló el carro bomba y la información sobre los encuentros del Ministerio de Defensa con paramilitares, incluso se conocía que días antes la ministra Marta Lucia Ramírez estuvo pernoctando en el club.

### **CTI sabía que se iba a perpetrar el atentado al Club el Nogal** 

Una investigación del CTI revela que en 2013, reposaba en el juzgado octavo, el testimonio de  Helena Zorrilla, investigadora de la Fiscalía adscrita a la Unidad de Grupos Armados Ilegales, quien daba cuenta de que 16 días antes de la explosión en el Nogal, organismos de inteligencia tenían datos concretos acerca de la infiltración de las FARC en el club y los números de teléfono con los que se coordinaban las acciones.

Según Zorrilla, hubo un testimonio del informante Jaime Quiñónez Rodríguez un mes antes del atentado, en el que describía a Hermínsul Arellán, alias el ‘Flaco’, y daba datos precisos de su manera de operar y de los mecanismos que se usaron para infiltrarse en el club. Además se conoce que hubo otro testigo que informó la infiltración y que luego denunció al director del DAS, Jorge Noguera, porque no se hizo nada con esa información.

### **Reuniones del Ministerio de Defensa con paramilitares en el Club el Nogal** 

Las víctimas también esperan que el Estado cuente la verdad acerca de las reuniones que estaba sosteniendo la **Ministra de Defensa, Marta Lucía Ramírez**, con varios líderes paramilitares, entre los cuales se menciona a **Mancuso**, en las instalaciones del Nogal, y espera que se explique por qué esas reuniones no se realizaban en la Casa de Nariño como era debido si se trataba de acercamientos oficiales.

En su momento, voceros de las FARC señalaron que "En el lujoso club se realizaban frecuentemente reuniones de sectores políticos y empresariales con voceros del paramilitarismo” declaración que fue reseñada en la página redresistencia.org, en ese momento FARC negaron la autoría del atentado pero esta declaración se entendió como una aceptación de la responsabilidad.

### **La verdad que esperan las víctimas del atentado al Club el Nogal** 

Según **Berta Fries**, durante los últimos meses, en el marco del  proceso de paz, se han sostenido varias reuniones privadas con integrantes del secretariado de las FARC y el gobierno para actos de verdad y perdón a las víctimas, y señala que ven con esperanza la posibilidad de la participación del Papa Francisco como garante y testigo de un acto en el que tendría prioridad la verdad y así lograr un proceso de reconciliación.

Para Berta es muy importante lo que está pasando y afirma que ya no siente odio y que quiere la reconciliación, de hecho el comunicado de prensa resalta la frase “los que quedamos debemos ayudar a trascender” aunque recuerda que hay un sello imborrable en los 198 sobrevivientes y en las 37 familias que hace 14 años se enlutaron por el terrible atentado. [[**Ver: La Jurisdicción Especial no puede dejar fuera a las víctim**]as](https://archivo.contagioradio.com/jurisdiccion-especial-no-puede-dejar-fuera-a-las-victimas/)

### **14 años sin ningún tipo de atención a las víctimas  ** 

Berta Fries, también denunció que en estos 14 años, la atención ha sido inexistente. Ella tuvo graves impactos en su cuerpo por la explosión y estuvo 8 años y medio en varios tratamientos médicos para su recuperación. Según ella los avances médicos en Estados Unidos lograron salvarle la vida pero la dejaron llena de cicatrices y deudas en las que el Estado no ha aportado “ni una curita”.

Frente a esa problemática espera que se borren los estigmas que señalan a las víctimas del Nogal como las “víctimas privilegiadas” ya que no han recibido atención alguna por parte del Estado. Por esa razón también están adelantando un proceso ante la CIDH en el que esperan que se condene al Estado por no atenderlas y por no prevenir el atentado.

------------------------------------------------------------------------

<iframe id="audio_16875290" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16875290_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
