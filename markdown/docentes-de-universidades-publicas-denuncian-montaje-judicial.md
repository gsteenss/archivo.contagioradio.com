Title: Docentes de Universidades Públicas denuncian montaje judicial
Date: 2020-02-13 17:03
Author: CtgAdm
Category: Expreso Libertad
Tags: Docentes, miguel angel beltran, montaje judicial, MRP, Rosembert Ariza, Universidad pública
Slug: docentes-de-universidades-publicas-denuncian-montaje-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/3bae3ea599651aef682901d398b9edbe_XL.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Los profesores Rosembert Ariza, Miguel Ángel Beltrán, Nubia Ruíz, de la Universidad Nacional y Renán Vega, de la Universidad Pedagógica, docentes de Universidades públicas denuncian montaje judicial, como producto de una persecución a la libertad de cátedra y el acompañamiento al movimiento de prisionero políticos en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Ariza, el pasado 27 de enero, a su oficina llegó un documento sin remitente, dentro del sobre había un conjunto de páginas con el logotipo del Movimiento Revolucionario del Pueblo (MRP), y al final del mismo, aparecían los nombres de cada uno de los docentes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para Beltrán, quien ya fue víctima de un montaje, por el cual estuvo recluido en la cárcel 4 años, la inferencia del documento es "hacer parecer" que los docentes serían los artífices e ideólogos del MRP. Razón por la cuál, para Ariza "se están constituyendo pasos para lograr un falso positivo en contra de los cuatro profesores"

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_59931170" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_59931170_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Asimismo manifestó que los cuatro docentes han acompañado los procesos educativos de distintos prisioneros políticos en las cárceles y han acompañado campañas para defender la libertad de los mismos, motivo por la cual "tenemos el seguimiento de los organismos del Estado y estos hostigamientos". (Le puede interesar:["En Colombia se encarcela el pensamiento crítico"](https://archivo.contagioradio.com/pensamiento-critico/))

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F652466988834347%2F&amp;width=500&amp;show_text=false&amp;height=281&amp;appId" width="500" height="281" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:heading {"level":3} -->

### La persecución al pensamiento crítico

<!-- /wp:heading -->

<!-- wp:paragraph -->

El docente de Sociología de la Universidad Nacional, Miguel Ángel Beltrán, aseguró que hay una serie de antecedentes que han configurado un ataque sistemático en contra de las Universidades Públicas, en terminos de utilizar el **aparato judicial para inculpar tanto a estudiante como docentes en actos fuera de la ley**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma el profesor Renán Vega, afirmó que este tipo de persecuciones hacia el pensamiento crítico, se dan en contra de las universidades pública porque estas aún conservan visos democráticos, "es un lugar donde se puede seguir discutiendo, se pueden difundir ciertas ideas y proyectos que resultan incómodos para distintos sectores de la sociedad colombiana y el Estado, razón por la cuál buscan acallar a quienes son protagonistas de estas iniciativas".

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_59931770" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_59931770_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Los cuatro docentes afirmaron que previo al montaje judicial, cada uno de ellos ha sido **víctima de persecuciones, interceptaciones telefónicas y amenazas,** que no solo a violentado sus derechos humanos, sino que también transgrede y ses redes afectivas más cercanas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El pensamiento crítico es un compromiso Ético

<!-- /wp:heading -->

<!-- wp:paragraph -->

Tanto Ariza, como Beltrán y Vega aseguraron que es importante para el país que se continúe en el ejercicio de un pensamiento crítico y que hechos como estos no pueden silenciar el ejercicio de la libertad de cátedra y el derecho al libre pensamiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Finalmente el profesor Vega aseguró que un objetivo fundamental que tienen los montajes es "acallarnos, es que no nos sigamos pronunciando hacia los grandes problemas del país, pero por lo mismo la respuesta más adecuada, **es seguir expresando esa voz disonante con el establecimiento, porque una sociedad sin crítica es una sociedad que va hacia el abismo**".

<!-- /wp:paragraph -->
