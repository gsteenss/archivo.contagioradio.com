Title: Van cuatro asesinatos de líderes comunales durante enero
Date: 2018-01-22 22:07
Category: DDHH, Nacional
Tags: asesinatos de líderes sociales, Cucuta
Slug: asesinatos_lideres_cucuta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Asesinan-a-Luz-Herminia-Olarte-lideresa-social-de-Yarumal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo ] 

###### [22 Ene 2018] 

En Cúcuta también continúa la ola de violencia contra los líderes de la Acción Comunal. El pasado 18 de enero, **asesinaron a Jorge Jimy Celis, quien lideraba la creación de la Junta de Acción Comunal colinas de El Tunal comuna 6 de la ciudad**. Según se supo, Celis fue asesinado por un hombre encapuchado que le propinó varios impactos de bala.

De acuerdo con las organizaciones sociales de la zona, con Jimi Celis son cuatro los líderes comunales asesinados en los primeros días de enero. Según, Marta Maldonado, vocera de la Federación de Juntas de Acción Comunal de Cúcuta, se trataba de un líder muy apreciado por su comunidad que estaba trabajando en la organización de Colinas de El Tunal donde residía hace más de 10 años.

Aunque no se conoce de amenazas contra la vida de Celis, pero la comunidad espera que se  investiguen los hechos y se de con el paradero de los autores materiales e intelectuales del crimen. De acuerdo con Maldonado, **al momento se conoce que detuvieron a una persona relacionada con el asesinato.**

La profesora también señala que en la ciudad se encuentran trabajando de la mano con las organizaciones internacionales para garantizar la seguridad de los líderes comunales junto a la Alcaldía, la Defensoría del Pueblo, la Consejería de Derechos Humanos y el Ministerio del Interior, especialmente tras el asesinato de una de las líderes a finales de 2017.

###### Reciba toda la información de Contagio Radio en [[su correo]
