Title: 60.000 juntas de acción comunal realizarán Carnaval por la vida y la esperanza
Date: 2017-11-11 17:11
Category: eventos
Tags: juntas de acción comunal, Líderes asesinados
Slug: 48975-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/juntas-accion-comunal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [11 Nov 2017] 

Este domingo las 60.000 juntas de acción comunal en Colombia llevarán a cabo un  carnaval por la vida y la esperanza, con el objetivo de rendir un homenaje a las victimas de asesinatos en el país, y particularmente la pérdida de vidas de las y los líderes sociales, así como llamar la atención del Estado por el abandono en que se encuentran.

Con motivo de la celebración del **cumpleaños número 59 del movimiento comunal en Colombia, miles de asociados,** se darán cita en las plazas y parques de todas las ciudades y municipios del país. “La acción comunal en Colombia ha jugado un papel estratégico en el desarrollo de las comunidades. Para nadie es un secreto que alrededor del 40% de la infraestructura de barrios y veredas, ha sido construida de la mano de la organización comunal. De esta manera se han realizado acueductos y alcantarillados, carreteras, colegios y jardines infantiles, parroquias, salones comunales, parques y vías, en los últimos 50 años”, señala Alejandro Rivera, miembro de la Federación de Acción Comunal de Bogotá.

En la capital el evento se realizará a las **10 a.m. en la carrera 7 con calle 26, donde habrá un gigantesco** despliegue de comparsas, grupos juveniles, música y teatro, con la presencia de más de 500 dignatarios de las 20 localidades.

En el Carnaval se reconocerá el trabajo de los líderes de todo el país que por su trabajo decidido en defensa de sus comunidades han sido asesinados y se evidenciará la situación de riesgo en materia de seguridad por la que atraviesan los líderes comunales en el país, la cual ha arreciado en los últimos años, pues tan sólo en los inicios de octubre fueron asesinados Cuatro líderes sociales y comunitarios en menos de 48 horas.

### **Historia de las Juntas de Acción Comunal en Colombia**

El origen de las Juntas de Acción Comunal en el país se remonta a finales de la década de los cincuenta en la Universidad Nacional de Colombia y fue en la vereda de Saucito, en el municipio de Chocontá, donde tuvo su aparición la primera. Allí, en la pequeña vereda de minifundistas, los vecinos decidieron organizarse para construir una escuela comunal que hoy es un monumento nacional porque fue el laboratorio donde se determinaron algunas de las primeras reglas en lo concerniente a la conformación de juntas de acción comunal.

Este proceso se forjó en el seno de la recién creada Facultad de Sociología de la Universidad, de la cual era Decano el sociólogo Orlando Fals Borda (1925-2008). En aquel momento, el Ministro de Educación, Abel Naranjo Villegas, pidió a la institución que plasmara por escrito la experiencia vivida en esta vereda.

Tiempo después se convirtió en un capítulo de la Ley 19 de 1958, la primera norma que se expidió en Colombia sobre la acción comunal, en el gobierno de Alberto Lleras Camargo, sancionada el 25 de noviembre de ese año, la cual autoriza a los concejos municipales las asambleas departamentales y al gobierno nacional, encomendar a las juntas de acción comunal conformadas por vecinos organizados, para ejercer funciones de control y vigilancia de ciertos servicios públicos.

Actualmente la organización comuna a nivel nacional cuenta con más de 60.000 Juntas de Acción Comunal y más de 8 millones de afiliados. **Las juntas de acción comunal se rigen por la Ley 743 de 2002 y el Decreto Reglamentario 2350 del 2003.**

Ley 743, fijó el segundo domingo de noviembre de cada año para la celebración del Día Nacional de la Acción Comunal, evento que anualmente debe ser realizado, según la norma, por el Ministerio del Interior, las gobernaciones y las alcaldías, el cual será tomado el próximo 12 de noviembre como un Carnaval por la vida y la esperanza.

###### Reciba toda la información de Contagio Radio en [[su correo]
