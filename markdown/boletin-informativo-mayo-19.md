Title: Boletín informativo mayo 19
Date: 2015-05-19 17:25
Author: CtgAdm
Category: datos
Tags: Actualidad informativa, contagio radio, Noticias del día, Resumen de noticias
Slug: boletin-informativo-mayo-19
Status: published

*[Noticias del Día]*:

<iframe src="http://www.ivoox.com/player_ek_4519103_2_1.html?data=lZqem5aUd46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjbLFvdCfkp6ah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-**Partidos políticos de izquierda, nacionalistas y movimientos sociales y sindicales** se reunieron hoy en **Madrid** para anunciar que presentarán **mociones en contra del Tratado Transaltlántico de Comercio e Inversión** TTIP (por sus siglas en ingles), en los municipios donde gobiernen, de cara a las elecciones del 24 de Mayo en España.

-Aunque no fueron pocos los analistas que auguraron la ruptura del **Polo durante su 4to Congreso**, como consecuencia de discusiones que se dieron a conocer en los últimos meses a través de declaraciones públicas y cartas abiertas, **el Partido asegura haber cerrado su máximo espacio de decisión política de manera unitaria y exitosa**. Habla Clara Lopez Obregón, Presidenta de la colectividad.

-Masiva marcha en **contra de la corrupción del actual gobierno en Guatemala**. Miles de manifestantes salieron a las calles la semana pasada cuando se anunció el escándalo de **corrupción aduanera que implica a la vicepresidenta del país**. **Brenda Hernández,** líderesa de la movilización expone las reivindicaciones exigidas.

-**Continúan protestas** en Perú en contra de **proyecto minero "Tía María"**. El lunes la multinacional anunciaba un cese de actividades de dos meses con el objetivo de renegociar con el estado y de que se calme la situación. Pero **los antimineros no van a finalizar el paro y han decidido continuar secundando desde la región de Arequipa**, a la que se han sumado todas las regiones del sur del país.

-En el marco de la celebración del **día internacional contra la homofobia**, se estrena en Bogotá el **Documental "Redes sociales de afecto y apoyo para las personas de los sectores LGTBI, Mis derechos, Tus derechos**". **Fepo Acevedo**, tallerista de la **Corporación Red Somos** abre la invitación al estreno del documental.
