Title: El nido paramilitar de Urabá: los huevos de la serpiente
Date: 2015-06-26 11:04
Category: Opinion, Ricardo
Tags: Atahualpa, LUISEGO, paramilitares, uraba
Slug: el-nido-paramilitar-de-uraba-los-huevos-de-la-serpiente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/serpiente.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Luis Eduardo Gómez Cubillos y su hijo Juan Pablo Atahualpa Gómez Ruiz 

#### [**[Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/) - @ferr\_es**] 

###### **[26 Jun]** 

Se llamaba **Juan Pablo Atahualpa** Gómez; vivía en el municipio de Arboletes, Antioquia, y no quería ser parte de las estructuras paramilitares de Urabá. Lo presionaban con insistencia: “Si no quiere trabajar con nosotros nos tiene que dar la motocicleta”. Atahualpa rechazó el reclutamiento forzado y tampoco les quiso dar la moto\[1\].

El 14 de agosto de 2009, a sus 26 años, Atahualpa fue asesinado frente a su familia. Desde entonces, el periodista Luis Eduardo Gómez Cubillos se dedicó a compilar datos de la gente que festejó la muerte de su único hijo en las playas de Arboletes. Supo que bebieron toda la noche, gritaron al calor de la música e hicieron alarde público de haberlo matado.

Durante los 22 meses siguientes, el periodista LUISEGO \[2\] observó los barrios de Arboletes. Personas que habían conocido a su hijo le enviaban papeles con los nombres de los sicarios relacionados con el crimen. Furtivamente se le arrimaban personas, con mucho miedo, pero le daban datos exactos. Ningún testigo quería declarar ante la fiscalía, pues no había garantías en un pueblo dominado por los escuadrones de la muerte.

Luis Eduardo consiguió nombres y alias de algunos jefes paramilitares, datos de testaferros, pagadores, administradores, técnicos, abogados, funcionarios públicos (alcaldes y sus  secretarios, funcionarios, militares y policías) relacionados con el narcotráfico  y la presencia paramilitar en Arboletes, San Juan de Urabá, Carepa y Necoclí. Identificó “Parches” \[3\], caletas, inmuebles, empresas. Consiguió nombres de propietarios de los barcos y sumergibles.

Obtuvo valiosa información sobre las relaciones de los paramilitares en Panamá: hoteles que encubren la venta de las jovencitas secuestradas en Colombia, testaferrato, venta de armas y gestión de las rutas del narcotráfico, con los carteles de la droga en Centroamérica.  LUISEGO compiló datos desde 2009 hasta 2011. El dossier se usaría para un libro sobre la corrupción en Urabá.

Como periodista, durante los últimos 20 años, Luis Eduardo Gómez había reseñado los principales  eventos económicos en **“La revista de Urabá”:** Las ferias equinas y ganaderas, los congresos de los bananeros, las campañas políticas, los nuevos zoocriaderos y los eventos sociales. Luego de la muerte de su hijo, Luis Eduardo Gómez siguió los hilos que van desde los victimarios hasta el poder regional en Urabá.

**Más de veinte años de reportajes se cerraron  con una imagen sobrecogedora: la gente que había entrevistado, las haciendas que había visitado, los ambientes comerciales, todos se plegaron al entorno de las Autodefensas.    **

El 30 de junio de 2011, Luis Eduardo Gómez Cubillos fue silenciado, pero nos dejó un legado para la memoria histórica de Colombia. Al leer los archivos la conclusión es dolorosa: casi todos los estamentos de Urabá fueron cooptados por el narcotráfico y sus escuadrones de la muerte \[4\].

Durante los últimos años nos repiten que el fenómeno paramilitar ha concluido. Si miramos las noticias de Urabá, al día de hoy, podemos concluir que la serpiente, su nido y sus huevos gozan de buena salud. Una buena parte del empresariado de Antioquia tendrá que explicar la verdad de sus curiosas relaciones y el incremento de sus fortunas.

###### \[1\] Obsesión de los paramilitares por las motocicletas: son útiles para controlar el territorio, patrullar carreteras, avisar de los retenes, cuidar a los mandos y cargamentos, facilitar el sicariato. Mueven la red de comunicaciones. Gran parte de esas motos son robadas en otras regiones del país. 

###### \[2\] LUISEGO, así se identificaba Luis Eduardo Gómez Cubillos en sus colegas de la prensa. 

###### \[3\] Parche: sitio habitual de reunión, expresión popular en Colombia. Zonales: territorios asignados a cada estructura paramilitar. 

###### \[4\] Tristemente, cada agente de policía que llega a Urabá, debe hacer la ELECCIÓN DE METALES:  “Díganos, ¿Usted  quiere plata o quiere plomo?. 
