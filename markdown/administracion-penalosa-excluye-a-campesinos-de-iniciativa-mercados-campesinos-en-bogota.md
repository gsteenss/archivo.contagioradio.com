Title: Administración Peñalosa excluye a campesinos de iniciativa “Mercados Campesinos”
Date: 2016-10-14 16:14
Category: Comunidad, Nacional
Tags: agricultura, Alcalde Peñalosa, alimentos, Bogotá, campesinos, mercados campesinos, paz, plaza de mercado
Slug: administracion-penalosa-excluye-a-campesinos-de-iniciativa-mercados-campesinos-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Mercados-e1476478822964.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Las 2 Orillas 

###### 13 Oct 2016 

A través de un comunicado se conoció que diversas organizaciones campesinas, se encuentran en desacuerdo por el trato que han recibido de parte de la Secretaria Distrital de Desarrollo Económico, que por un lado ha desconocido el proceso que han tenido los “Mercados Campesinos” en Bogotá y por otro ha excluido a más de 1175 campesinos de una actividad planeada para el 14 de Octubre. Lea también: [Administración Distrital pondría fin a los mercados campesinos](https://archivo.contagioradio.com/administracion-distrital-pondria-fin-a-los-mercados-campesinos/)

En el comunicado, también manifiestan que **dicho evento no fue concertado con las organizaciones campesinas y comunales** que vienen liderando la iniciativa de “Mercados Campesinos” hace 12 años y quienes a través de la agricultura construyen paz.

Por su parte, también **aseguraron que la administración Distrital en cabeza del Alcalde Enrique Peñalosa, los ha excluido de esta iniciativa** “el Alcalde no tuvo en cuenta un procedimiento participativo ni incluyente, rompiendo de esa manera el fortalecimiento de las organizaciones y su derecho a asociarse. **De 1500 campesinos que participamos en años anteriores, hoy solo pretenden dar espacio a 325**” dice el comunicado.

De igual manera aseguran que **la actual actividad que se lleva a cabo** en Bogotá por la administración Peñalosa, **desconoce la misión y la visión con la que fueron creados los “Mercados Campesinos”**. Le puede interesar: [Nuevas exigencias de la Alcaldía de Bogotá acabarían con los Mercados Campesinos](https://archivo.contagioradio.com/administracion-acabara-los-mercados-campesinos-para-hacerlos-tipo-gourmet/)

Concluyen manifestando que no reconocen el evento que se denomina “Mercados Campesinos” que ha sido liderado por la Alcaldía Distrital de Enrique Peñalosa pues “es excluyente y deja de beneficiar a 3000 familias campesinas” puntualiza la comunicación.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
