Title: Consulta Popular en El Peñón le gana una batalla a Ministerio de Minas
Date: 2017-11-14 15:23
Category: Ambiente, Nacional
Tags: consulta popular, El Peñón
Slug: consulta-popular-penon-le-gana-una-batalla-ministerio-minas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/peñon.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Producciones.mia] 

###### [14 Nov 2017] 

Un fallo del Consejo de Estado negó la tutela del Ministerio de Minas y Energía que pretendía dejar sin efectos la realización de la consulta popular en El Peñón, el argumento era que con la consulta se negaba el derecho al trabajo y el debido proceso**. Sin embargo el Consejo resolvió a favor de la comunidad que convoca la consulta.**

El argumento del Ministerio de Minas y Energía era apelar a que el accionar del Tribunal Administrativo de Santander estaría vulnerando el derecho al trabajo y el derecho al debido proceso, con el mecanismo de la consulta popular. Sin embargo, la magistrada Stella Jeannette señaló que **“la concertación entre autoridades nacionales y locales no era prerrequisito para la relación de la consulta popular”**. (Le puede interesar: ["Consultas populares en Santander son demandadas por Min Minas"](https://archivo.contagioradio.com/ministerio_minas_consulta_popular_sucre_santander/))

De igual forma el Ministerio de Minas y Energía interpuso tutelas similares contra otros procesos de consulta popular como las de Jesús María, Sucre y Córdoba, Quindío. El abogado Orduz afirmó, que, de hecho, **la tutela que se ha interpuesto a estos tribunales es una copia firmada incluso por la misma person**a, en lugares en donde ni siquiera hay explotación minera.

“Nos encontramos ante una falacia y es que no se está vulnerando ningún derecho al trabajo, porque no hay nadie que se encuentre empelado en esas actividades y además porque la Corte Constitucional ha dicho **que los derechos colectivos priman sobre los individuales**” afirmó Orduz.

### **La financiación de las Consultas sigue sin tener responsables** 

La consulta popular de El Peñón ha sido otro de los procesos suspendidos por falta de financiación, al igual que la de Granada, La Macarena, El Castillo y otras, situaciones que, para el abogado, son una negativa por parte del Ministerio de Hacienda, para enviar los recursos y a lo que se ha argumentado que se están vulnerando derechos como el de la igualdad, **debido a que hay 9 consultas populares que si han sido financiadas por el Ministerio**.

“Se está **vulnerando también el derecho a la participación, que además es un principio constitucional, el derecho al voto**, porque la gente evidentemente no puede votar y se vulnera el principio de legalidad, que es pasar por alto el aparataje institucional que dice quién debe cumplir que funciones” expresó el abogado y agregó que están a la espera de lo que suceda con este debate, para continuar usando el mecanismo de la consulta popular en el país. (Le puede interesar: ["Provincia de Vélez será escenario de tres consultas populares"](https://archivo.contagioradio.com/provincia-de-velez-sera-escenario-de-3-consultas-populares-contra-la-mineria/))

###### Reciba toda la información de Contagio Radio en [[su correo]
