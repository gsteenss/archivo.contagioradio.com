Title: Convencer a la sociedad, el desafió de la mesa ELN - Gobierno
Date: 2018-01-22 13:04
Category: Nacional, Paz
Tags: ELN, Gustavo Bell, proceso de paz, Quito
Slug: eln-y-gobierno-exploran-posibilidad-de-retornar-a-la-mesa-de-conversaciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/eln-y-gobierno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular ] 

###### [22 Ene 2018] 

El equipo de conversaciones del ELN y del gobierno Nacional explorarán la posibilidad de retomar los diálogos de paz en Quito. Sin embargo, de acuerdo con el analista y profesor Víctor de Currea, lo más importante para que se blinde este proceso es re convencer a la ciudadanía del apoyo a la mesa a partir de acciones concretas, **como el cese bilateral o el desescalamiento del conflicto armado en el territorio del país**.

De acuerdo con el analista, hay puntos de común acuerdo que permiten que estas intenciones de acercamiento puedan ser fructíferas, la primera es que ambas partes quieren continuar con la mesa y diálogo, la segunda es mantener el diálogo en medio de un cese al fuego, **el tercero es que ese cese trajo cosas positivas y negativas que visibilizan la necesidad de reformularlo** y cuarto que es de vital importancia regresar a la mesa de conversaciones. (Le puede interesar: ["La agenda que debería adoptar el gobierno para facilitar conversaciones con el ELN"](https://archivo.contagioradio.com/la-agenda-que-deberia-adoptar-el-gobierno-para-facilitar-conversaciones-de-paz-con-eln/))

“Lo ideal es que lleguen a un acuerdo sin que ninguno de los dos se sienta arrinconado por el otro, eso que puede parecer infantil es fundamental en los procesos de paz y debe verse con el rigor que se requiere” afirmó Currea y agregó que, para ello, es de importancia **que se lleguen con propuestas reales y concretas a la mesa y analizar las ya hechas por la sociedad civil**.

Además señaló que para re conquistar a la sociedad civil, luego de tantos inconvenientes, **deben darse acciones concretas y un respeto a mínimos fundamental en términos de derechos humanos**, Derecho Internacional Humanitario, respeto a la palabra dada, disminución real de las hostilidades y del impacto del conflicto sobre la población.

### **Los países garantes y sus grandes esfuerzos** 

Para Cuerrea el comunicado de Naciones Unidas, invitando a ambas partes a continuar con el proceso de paz, es el motivo que llevó a Santos a enviar a Gustavo Bell, jefe del equipo negociador del gobierno, a Quito.

De igual forma el ELN sacó un comunicado de prensa en el que insta a que se lleve a cabo una reunión con los dos jefes de los equipos de conversaciones en presencia de los garantes, en ese sentido De Currea afirmó que es sumamente importante la voluntad, porque desde ambas partes hay sectores no muy convencidos por continuar la mesa.

Así mismo señalo que la presión que ha ejercido la sociedad civil, también fue efectiva para que se sienten las partes a negociar, **cumpliendo su tarea por insistir en una solución dialogada al conflicto armado entre el gobierno y el ELN**. (Le puede interesar:["4 propuestas para destrabar la mesa de conversaciones entre el ELN y el Gobierno"](https://archivo.contagioradio.com/4-propuestas-para-destrabar-la-mesa-eln-gobierno-y-reanudar-cese-bilateral/))

<iframe id="audio_23289381" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23289381_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
