Title: Agresión sexual del Ejército contra menor embera es un ataque a los 115 pueblos indígenas del país: ONIC
Date: 2020-06-24 20:15
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Agresiones del Ejército, pueblos indígenas, Violación de derechos a pueblos indígenas en Colombia, Violencia Sexua
Slug: agresion-sexual-del-ejercito-contra-menor-embera-es-un-ataque-a-los-115-pueblos-indigenas-del-pais-onic
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Pueblo-Embera-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Pueblos Indígenas Embera/ Unidad de Víctimas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La Autoridad Tradicional Indígena de Pueblo Rico, Risaralda señala que una menor de edad de 12 años perteneciente a la comunidad fue abusada sexualmente por parte de integrantes del Ejército. Según la [Organización Nacional Indígena de Colombia (ONIC)](https://twitter.com/ONIC_Colombia) y su informe sobre violaciones a los DD.HH. de las mujeres indígenas, para el 2015, **la violencia sexual era el tipo de violencia que más se ejercía contra mujeres, de las que cerca del 75% de las víctimas fueron niñas indígenas menores de 14 años.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el comunicado del Resguardo Gito Dokabú, los hechos ocurrieron el pasado 22 de junio cuando una menor de 12 años de edad perteneciente a la comunidad fue secuestrada y abusada sexualmente por "un grupo indeterminado de soldados del Ejército" que deberían cuidar a las comunidades. Según un informe de la ONIC presentado en el 2014, la mayoría de las denuncias de violencia sexual contra mujeres indígenas en el conflicto armado en las que se tienen datos del agresor señalan a miembros de la Fuerza Pública (86%) y de las AUC (14%).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La niña que estuvo desaparecida, fue hallada en una escuela cercana al resguardo en graves condiciones y llevada a un centro de salud para ser valorada y donde "se le realizó la restitución de derechos de la menor" según el comunicado del Ejército sin que se tenga claridad de a qué se refieren con restitución de derechos. Luego de haber sufrido violencias sexuales por parte de siete militares del Batallón San Mateo, según dio a conocer un reporte del Ejército Nacional que ya tiene bajo custodia a los responsables.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Comunidades indígenas son firmes en que el caso no puede ir a la justicia penal militar

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Juan de Dios Queragama Nariquiaza, gobernador mayor del resguardo exigió que los agresores sean entregados para que puedan ser juzgados según el mandato indígena** y "una vez paguen su condena bajo nuestra propia legislación, sean juzgados por las leyes de ustedes". [(Lea también: Una historia de promesas incumplidas al Pueblo Embera)](https://archivo.contagioradio.com/50-anos-de-promesas-fallidas-a-los-embera/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

"Esto no solo ha sido una agresión para nuestra niña y su dignidad como ser humana y como miembro de un pueblo ancestral, ha sido una agresión para todo nuestro pueblo Embera Katio", afirman desde el resguardo, agregando que con este acto se defrauda la confianza que se ha depositado en el Estado, siendo uno de los 30 pueblos originarios que se encuentran en vía de extinción según la Corte Constitucional. [(Lea también: Continúa el genocidio silencioso del pueblo Embera Chamí de Caldas)](https://archivo.contagioradio.com/continua-el-genocidio-silencioso-del-pueblo-embera-chami-de-caldas/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La ONU ha advertido que la violencia sexual sigue siendo uno de los motivos de desplazamiento de las mujeres colombianas, de las que el 73% son niñas menores de edad quienes a menudo quedan en estado de embarazo durante su adolescencia lo que lleva a la deserción escolar.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La ONIC, manifestó su respaldo al Resguardo Gito Dokabú y a través de Lejandrina Pastor Gil, perteneciente al Pueblo Wiwa, representante de la Consejería de Mujer, Familia y Generación, expresó que este hecho, constituye una grave vulneración a los derechos de la menor, su familia, el pueblo embera katio y se suma a un "largo prontuario de actos atroces por parte de la Fuerza Pública que constituyen un factor de riesgo en vez de garantías de seguridad para los pueblos indígenas".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Se trata de un problema estructural

<!-- /wp:heading -->

<!-- wp:paragraph -->

**"No es un tema aislado, es un tema estructural, es una doctrina de la Fuerza Pública que debe ser desmantelada en bien de la paz y de Colombia y que esa forma de mirarnos de manera xenofóbica se elimine en el país"**, expresó la consejera de DD.HH, Aida Quilcué quien agregó que les corresponde a las autoridades indígenas consensuar este proceso para que no haya impunidad, por lo que llegará una comisión presencial a la región para dialogar directamente con las comunidades.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte el Ejército expidió un comunicado en el que señala se trataría de siete integrantes del Batallón San Mateo quienes estarían vinculados al caso de abuso sexual, el comandante de la Quinta División del Ejército, el mayor general Luis Mauricio Ospina,manifestó que la denuncia hecha por las autoridades indígenas ya fue instaurada de manera penal ordinaria por lo que no será llevado a la Justicia Penal Militar sino que se le hará un seguimiento desde la Fiscalía.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
