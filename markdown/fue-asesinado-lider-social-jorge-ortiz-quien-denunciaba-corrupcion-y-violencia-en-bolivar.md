Title: Fue asesinado líder social Jorge Ortiz quien denunciaba corrupción y violencia en Bolívar
Date: 2020-06-18 13:35
Author: AdminContagio
Category: Actualidad, DDHH
Tags: Bolívar
Slug: fue-asesinado-lider-social-jorge-ortiz-quien-denunciaba-corrupcion-y-violencia-en-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/06/Jorge-Ortiz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Jorge Ortiz/ FECODE

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 16 de junio fue asesinado el docente y líder social Jorge Ortiz quien fue interceptado en la vía principal del municipio de Barranca de Lobas en Bolívar por hombres armados que le dispararon en siete ocasiones. Advierten que el profesor, quien contaba con medidas de la Unidad Nacional de Protección (UNP) le fue retirada parte de su seguridad siendo su caso de alto riesgo como habían señalado organizaciones defensoras de DD.HH.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Jorge Órtiz ya había recibido amenazas contra su vida por denunciar hechos de corrupción en el municipio lo que llevó a que fuera acogido por la UNP**, sin embargo, según Juan Meneses, líder social oriundo del departamento e integrante de la Corporación dé Líderes Sociales y Víctimas de Colombia, organización a la que también pertenecía el docente, señala que las medidas de protección que habían sido aplicadas a Jorge se habían reducido recientemente,

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Relata que el docente venía denunciando varias situaciones que ocurrían al sur de Bolívar, la primera de ellas, con relación al asesinato del líder minero Edwin Acosta quien también había advertido sobre la reactivación de grupos paramilitares en la región, razón por la que habría sido asesinado, de igual forma adelantaba denuncias contra el accionar del Ejército en el lugar tras haber atropellado a un residente del corregimiento de Pueblito Mejía sin que los uniformados se hubieran hecho responsables de lo sucedido, por lo que no se sabe cuál de estas denuncias podría haberle costado la vida. [(Le puede interesar: Asesinan a Edwin Acosta, líder social y minero de Tiquisio, Bolívar)](https://archivo.contagioradio.com/asesinan-a-edwin-acosta-lider-social-y-minero-de-tiquisio-bolivar/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Fue una persona que lucha por su región, docente, un padre que deja a una esposa con hijos muy chicos", expresa el líder social quien considera que en los próximos días la familia tendría que salir del municipio por motivos de seguridad.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Meneses resalta que la Gobernación de Bolívar, [Vicente Blel,](https://twitter.com/vablelscaff) estaba al tanto del riesgo que tenía el docente**, pues durante sus últimas conversaciones este le relató cómo Jorge Órtiz escribió el gobernador y este no respondió a sus mensajes. Al respecto el líder social denuncia que existe en municipios como Altos del Rosarios y Barranco de Lobas una connivencia entre los alcaldes, la Fuerza Pública y un grupo ilegal al que se denomina Los Ratones.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Por su parte, Julio César León director de la organización a la que estaba afiliado Jorge, resaltó que desde **hace tres años se venía insistiendo a la UNP que fortaleciera su protección, denunciando que "nunca se le prestó atención a los reclamos que se hicieron desde Corpvicol"** razón por la que la responsabilizan del asesinato.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hoy el municipio de Barranco de Lobas se queda sin doliente en términos de DD.HH", afirma Juan Meneses, quien reitera que con el asesinato de líderes y lideresas sociales en el país lo que busca es silenciar las vidas de quienes construyen tejido social y no se prestan a la corrupción que se vive en los territorios. [(Lea también: Denuncian desaparición forzada de lideresa Rocío Silva en el Sur de Bolívar)](https://archivo.contagioradio.com/denuncian-desaparicion-forzada-de-lideresa-rocio-silva-en-el-sur-de-bolivar/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
