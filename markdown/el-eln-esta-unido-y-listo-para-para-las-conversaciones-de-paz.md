Title: El ELN está unido y listo para las conversaciones de paz
Date: 2015-12-09 12:42
Category: Nacional, Paz
Tags: Antonio García, Cese Bilateral de Fuego, conversaciones de paz con el ELN, Conversaciones de paz con las FARC, ELN, Juan Manuel Santos, Nicolás Rodríguez, Víctor de Currea Lugo
Slug: el-eln-esta-unido-y-listo-para-para-las-conversaciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Antonio_Garcia_ELN_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: elespectador 

###### [9 Dic 2015]

El comandante del Ejército de Liberación, Antonio García, afirmó que esa guerrilla está unida en torno al proceso de conversaciones de paz y que desde hace tiempo está listo. Las declaraciones, otorgadas a Victor de Currea, hacen énfasis en que el inicio de la fase pública de conversaciones se ha demorado [porque el gobierno abandonó los acercamientos durante 15 meses](https://archivo.contagioradio.com/eln-asegura-que-el-gobierno-no-tiene-voluntad-real-de-paz/) sin justificación alguna, esto entre otros aspectos.

En cuanto a la posibilidad de la democratización de la sociedad García resalta que "el objetivo es poder hacer de el proceso de paz un ejercicio de sociedad dialogante" y que "busque uha mayor tolerancia para escucharse y resolver los problemas" y afirma que la sociedad tendrá que plantearse un proceso constituyente en algún momento.

<iframe src="https://www.youtube.com/embed/XuS3Wjofnw4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Frente al inicio de la fase pública, García declara que no se justifican más demoras y que [estarían listos para trabajar a partir de mañana mismo](https://archivo.contagioradio.com/crece-optimismo-en-torno-a-formalizacion-de-mesa-de-conversaciones-de-paz-con-el-eln/), sin embargo, afirma que tienen la satisfacción de haber trabajado de muy seriamente y que las condiciones del país no han cambiado y por ello no estarían quedándose del  llamado “tren de la paz”, “no lo vemos así, si creemos que no ha sido justo que la fase pública se haya demorado habiendo las condiciones para que los acuerdo ya se hubiesen establecido”.

<iframe src="https://www.youtube.com/embed/MR8vlB_lJ78" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Respecto de los avances en las conversaciones de paz con las FARC, el comandante del ELN afirma que están convencidos de que en algún momento pueden confluir las dos mesas “si a lo mejor coinciden en algunas temáticas de interés del ELN y de interés del pueblo colombiano seguramente se tocarán” afirma García respecto de los temas que han quedado pendientes en las conversaciones con las FARC y que estarían en “el congelador”.

<iframe src="https://www.youtube.com/embed/e_JTymSf1Vo" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Frente a la posibilidad de una tregua unilateral para el inicio de las conversaciones García afirma que en las oportunidades en que se han realizado gestos de este tipo “el gobierno se ha mal acostumbrado” y muchas veces lo ve como una señal de “debilidad” y hace énfasis en que las acciones deben encaminarse hacia un cese bilateral de fuegos.

<iframe src="https://www.youtube.com/embed/feh6w8RusKI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Antonio García también hizo claridad en [“las líneas rojas” de ELN de cara a las conversaciones](https://archivo.contagioradio.com/page/2/?s=eln), la primera de ellas es que “no se afecte a la sociedad colombiana” y que ellos no pueden negociar por la sociedad “no podemos ir a negociar cosas que no tienen que ver con nosotros”. Además reafirmó que una de las grandes expectativas es que “hacia el futuro Colombia pueda establecer una democracia que nos interprete a todos los sectores de la sociedad” y que “permitamos que otras expresiones sociales y políticas ejerzan la política”.
