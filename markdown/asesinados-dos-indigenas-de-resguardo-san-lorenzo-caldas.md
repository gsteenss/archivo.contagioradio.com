Title: Asesinados dos indígenas de resguardo San Lorenzo Caldas
Date: 2015-05-11 17:00
Author: CtgAdm
Category: Comunidad, Nacional
Tags: asesinato, Caldas, Comunidad Indígena, Derechos Humanos, Embera Chami, Médicos Tradicionales, San Lorenzo
Slug: asesinados-dos-indigenas-de-resguardo-san-lorenzo-caldas
Status: published

######  Foto: colombia.com 

###### [11 de May 2015]

<iframe src="http://www.ivoox.com/player_ek_4479560_2_1.html?data=lZmkm5qadI6ZmKiak5WJd6KnmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy108fS25CrpYa3lIqvk8bScYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Entrevista Arbey Gañan] 

El Cabildo Indígena del resguardo San Lorenzo denunció el asesinato de dos miembros de la comunidad, el 9 de mayo de 2015, en la vereda El Carmelo, municipio de Anserma, Caldas. Según el informe, Gustavo Bañol Rodríguez médico tradicional que hace parte de la Asociación de Médicos Tradicionales de la comunidad y su hijo Edwin Bañol Álvarez, fueron asesinados con arma de fuego cerca de las dos de la mañana, después de realizar un trabajo de sanación de la tierra.

La comunidad indígena de Caldas está en una disputa territorial desde hace aproximadamente 30 años, lo que lo convierte en un corredor estratégico para los grupos armados. Varios territorios están actualmente en concesión minera; durante este tiempo se han evidenciado homicidios, genocidios, desplazamientos y hostigamientos, lo que ha llevado a la Comisión Interamericana de Derechos Humanos a dictar medidas cautelares para el departamento de Caldas" dice el ex gobernador.

Además de la disputa territorial, el ex gobernador asegura que en Caldas hay un contexto político electoral influyente, en el que, durante los últimos 25 años se ha reclamado el derecho a la participación electoral, pero, según su hipótesis a los partidos políticos les ha disgustado la iniciativa y por la misma razón se ha asesinado a 3 candidatos, desde el año 2009 se han conocido cerca de 490 indígenas, muchos de ellos dirigentes.

El comunicado del cabildo afirma que "*Las autoridades indígenas regionales condenan este atroz hecho de violencia y lo consideran como una barbarie que enluta a la comunidad indígena caldense y piden a los organismos competentes que investiguen y esclarezcan los hechos y a su vez sancionen a los responsables del crimen"*

 
