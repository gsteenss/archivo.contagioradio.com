Title: Con proyecto de ley directores y escritores audiovisuales buscan proteger sus derechos
Date: 2016-04-04 15:20
Author: AdminContagio
Category: 24 Cuadros, DDHH
Tags: Cine Colombiano, Derechos de autor en Colombia, Ley Pépe Sánchez, Televisión colombiana
Slug: con-proyecto-de-ley-escritores-y-directores-audiovisuales-buscan-proteger-sus-derechos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Pepe-S__nchez-e1459800727132.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Squire Colombia 

##### [4 Abr 2016]

Este martes 5 de Abril, será presentado el proyecto de ley "Pépe Sánchez", con el que se pretende modificar la ley 23 de 1982, o ley de derechos de autor en Colombia, que permitiría establecer una remuneración para los escritores audiovisuales, teatrales y de nuevas tecnologías, cada vez que se exploten comercialmente sus creaciones.

A las 8:30 de la mañana, profesionales del sector audiovisual convocados desde la Red Colombiana de Escritores Audiovisuales REDES, y el Consejo directivo de DASC Directores Audiovisuales de Colombia , se presentarán en el despacho de la secretaría General de la Cámara de Representantes para acompañar la presentación de la ponencia promovida por la Representante Clara Rojas y el Senador Juan Manuel Galán.

En Colombia existe un vacío legislativo en cuanto al tema de propiedad intelectual, que no permite a los escritores y directores percibir ganancias por concepto de las regalías producidas por sus creaciones, en particular cuando estas son vendidas y/o distribuidas en otros países, situación que se presenta tanto el cine como en la televisión.

Se estima que serían cerca de 7 millones de dólares los que podrían perderse de no actualizar la ley y encontrar las herramientas que permitan recaudar esos dineros antes que las solicitudes caduquen. Por lo pronto, las organizaciones que acogen a escritores y directores, recogen firmas como respaldo a la presentación del proyecto legislativo.

<iframe src="https://www.youtube.com/embed/fVKtvR0j1OE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
