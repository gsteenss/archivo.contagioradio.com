Title: Refugiados y esclavitudes
Date: 2016-06-22 06:00
Category: Cesar, Opinion
Tags: Refugiados, Turquía, Unión europea
Slug: refugiados-y-esclavitudes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/refugiados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: libertario 

#### Por [César Torres Del Río](https://archivo.contagioradio.com/cesar-torres-del-rio/) 

###### 22 de Jul 2016 

Continúa la tragedia en el Mediterráneo, que incluye a Europa y al Próximo Oriente. Según datos de ONGs y de distintos medios de comunicación la cifra de muertos en el mar en 2015 sobrepasa los 3.600-3.700; unos 200 tratando de llegar a España. Los refugiados, en especial africanos, huyen de la guerra, del hambre y el desempleo. Los más pobres  no pueden escapar: o se quedan haciendo parte de los ejércitos de parte y parte o se acomodan en sus vidas cotidianas intentando sobrevivir con lo que ganan y tienen; quienes suben a los ataúdes marítimos integran varios niveles de las capas medias con educación profesional que logran reunir el dinero para pagar las elevadas sumas que cobran los traficantes de personas (contrabandistas); si logran coronar buen número de los refugiados se convierte en víctima de las redes de trata de personas y pasan a convertirse en esclavos - domésticos, sexuales, “laborales” (explotados durante 16-18 horas al día) -.

[La Unión Europea (UE) desprecia a los refugiados. Después de haberlos  masacrado por aire y tierra con sus bombardeos y ejércitos “humanitarios” (Kosovo, Libia, Irak, Siria, Afganistán, Eritrea …) ahora los criminaliza, los discrimina, los clasifica y, encima, los]*[concentra]*[ en]***campos***[ en donde sufren todo tipo de vejámenes y violaciones a sus derechos. Lo que irrita a nuestras conciencias es que siendo la UE  una agrupación de bandidos imperialistas se la presente en medios de comunicación y en estudios de analistas académicos y políticos como cabeza de la “civilización democrática” de Occidente, con sus “valores” envejecidos: igualitarismo, justicia, Estado del bienestar …]

[La verdad es que la legitimidad de la UE ni siquiera radica en los procesos electorales, ni en sus “mandatos”; radica en la ilegitimidad de sus acciones: atracos económicos de la Organización Internacional del Comercio y de la Asociación Transatlántica para el Comercio y la Inversión y del Fondo Monetario Internacional; operaciones político-militares abiertas y clandestinas; propaganda sucia en sus medios de comunicación; chantajes económicos y político-electorales (caso Grecia); apelación al miedo por el fantasma totalitario de los sujetos políticos. Todas esas acciones son legalizadas-legitimadas por el imperialismo del Derecho Internacional  y por el Derecho Internacional del imperialismo  - dos aspectos bien distintos -, por sus parlamentos, por el uso de la fuerza armada y por sus instituciones.  ]

[Que no haya dudas: es en el capitalismo como sistema y en su excreción imperialista en donde hay que ubicar en primera instancia el]*[crimen humanitario]*[ en marcha que apreciamos en el Mediterráneo con los refugiados y los nuevos esclavos; señalar al capitalismo como “causa última” del crimen, como desacertada y ambiguamente escribe Slavoj Žižek en su reciente libro (]**La nueva lucha de clases. Los refugiados y el terror**[, Barcelona, Editorial Anagrama, 2016, p. 51),  descentra el debate y permite que las políticas socialdemócratas, de los Verdes y de las derechas antiinmigración (caso alcaldía de Roma) sigan ocupando espacios para legislar, administrar y sostener la xenofobia y el racismo. Hay  otros factores y aspectos de coyuntura, claro, por examinar como la guerra, las alianzas regionales, las nacionalidades (kurdos), las diferencias religiosas (sunitas, chiítas, wahabistas) que examinaremos en próxima oportunidad]

El acuerdo firmado con Turquía en marzo de 2016 acerca de los refugiados es otra demostración de lo aquí afirmado. Por 3 mil millones de euros para “inversiones” inmediatas y 3 mil millones más en un futuro cercano el gobierno turco de Erdogan ha pactado con la UE convertirse en el filtro de los refugiados, disminuir los alcances del derecho de asilo en Europa y reiniciar conversaciones en torno a su ingreso a la UE.

[Como los refugiados no pueden ser expulsados masivamente y deben esperar la resolución individual de sus casos, los campos de detención y refugio se convierten en semilleros de nuevos esclavos dada la corrupción y el peso que tiene la red de trata de personas. Entre tanto crecen las corrientes antiinmigración y las izquierdas de izquierda no han logrado posicionarse para ofrecer políticas alternativas, lo cual es otra cara de la moneda. En la solidaridad en cualquiera de sus formas, en la denuncia, en la información veraz, por ahora, debe centrarse la acción política colectiva. No queremos más Aylan Kurdi.]

------------------------------------------------------------------------

###### [Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.] 
