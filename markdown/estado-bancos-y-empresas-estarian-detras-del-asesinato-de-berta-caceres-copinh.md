Title: Estado, bancos y empresas estarían detrás del asesinato de Berta Cáceres: COPINH
Date: 2016-03-03 14:54
Category: El mundo, Otra Mirada
Tags: Agua Zarca, Berta Cáceres, Copinh, honduras
Slug: estado-bancos-y-empresas-estarian-detras-del-asesinato-de-berta-caceres-copinh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Berta-Cáceres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Rel-Uita 

<iframe src="http://co.ivoox.com/es/player_ek_10662774_2_1.html?data=kpWjmJebe5Whhpywj5eaaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncabn1cbR0YqWh4zWwtPQ0diPvYzZztXfx9jFt4zZ1NnO1Iqnd4a1pcbbjcnJuNOZpJiSo5bXb8XZzZDO1crXrc%2FVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Tomas Membreño] 

###### [3 Mar 2016] 

“Estamos muy consternados por el asesinato de nuestra compañera, este hecho hace parte de la estrategia del sistema neoliberal”, dice Tomas Membreño, integrante del  Consejo Cívico de Organizaciones Populares e Indígenas de Honduras, COPINH y compañero de lucha de Berta Cáceres, líder indígena defensora de los derechos humanos y del ambiente, asesinada en la madrugada de este jueves  y quien organizó a la comunidad lenca contra la construcción de un proyecto hidroeléctrico de la empresa Desa-Agua Zarca en el noroeste de Honduras.

“Los primeros responsables son los representantes de empresa Desa-Agua Zarca, también el Banco FICOHSA, el Banco de Desarrollo Holandés FMO, el alcalde municipal de Intibucá, el alcalde de San Francisco de Ojuera” señala Membreño, asegurando que exigirán a las autoridades hondureñas que se castigue a los actores intelectuales y materiales del crimen.

El integrante del COPINH, señala que el asesinato de su compañera era un crimen que se estaba gestando tiempo atrás. Tanto Berta como la organización habían sido víctimas de amenazas por parte de la empresa que lleva a cabo el proyecto hidroeléctrico. La compañía habría iniciado una campaña de desprestigio y criminalización de las actividades que realizaba el COPINH junto a la comunidad Lenca. De hecho, el asesinato, se da en medio del ‘Foro sobre Energías Alternativas desde la visión indígena del  COPINH’, que se realiza del 2 al 4 de marzo.

Lo anterior, se suma a que el pasado 20 de febrero la organización realizó “un ejercicio de control territorial”, que consistía en una marcha con destino al lugar de construcción de la represa de Agua Zarca en el Río Gualcarque, para exigir que no continuara el proyecto y no se privatizara el agua.

Membreño, relata que tanto la empresa como la alcaldía municipal se pusieron de acuerdo para parquear una máquina que impidiera el paso de la movilización, sin embargo lograron atravesar el obstáculo, pero más adelante se encontraron con policías en la vía que les advirtieron de la presencia de personas armadas cuyo objetivo principal era acabar con la vida de Berta, y pese a que las mismas autoridades sabían no hicieron nada en ese momento. “La policía nos dijo, que nos estaban esperando hombres armados, les dijimos que por qué no los capturaban si ya lo sabían y no hicieron nada. Se vio claramente la complicidad de la misma fuerza de seguridad”, dice Tomás.

En el asesinato podrían estar involucrados exagentes de la policía, según denuncia el compañero de Berta. “Incluso el jefe de seguridad de la empresa DESA es exmiembro del Ejército hondureño y tenemos entendido que ha formado parte del Batallón 3-16 que tenía el Estado para asesinar a dirigentes que se opusieran a las políticas de los años 80”,

El Batallón 3-16, conocido como ‘el escuadrón de la muerte’ habría sido una estrategia estatal a la que se le atribuyeron todos los secuestros, desapariciones y asesinatos sucedidos en Honduras durante los años 80. Ese escuadrón habría sido responsable por secuestro, tortura, desaparición y asesinato de por lo menos 184 estudiantes, profesores, periodistas, y activistas de derechos humanos hondureños en esa época.

Membreño, incluso sostiene que mediante la ‘Operación Avalancha’, dirigida supuestamente para enfrentar el narcotráfico, se habría establecido el plan para asesinar a la líderesa indígena. Esta operación está conformada por las acciones de miembros de la Lucha Contra el Narcotráfico (DLCN), Policías Tigres, Policía Nacional (PN) Fuerza Naval y la Agencia Técnica de Investigación Criminal (ATIC). La semana pasada, el ministro de Seguridad de Honduras, Julián Pacheco Tinoco, anunció que la ‘Operación Avalancha’, continuará desarrollándose durante el 2016. “Esta es una operación que va a durar todo el año porque es a largo plazo, hay muchos casos que manejar”, dijo Tinoco.

“El Estado protege las empresas privadas y no la vida de los seres humanos”, expresa Tomás. La empresa Agua Zarca, en los últimos días ha estado enviando información asegurando que desde el COPINH se adelantan acciones criminales. Para el integrante de esa organización, se trata de una estrategia para justificar cualquier tipo de asesinato o acción contra los dirigentes indígenas.

### Sobre el proyecto Agua Zarca 

Actualmente la empresa DESA avanza en la apertura de la carretera y construcción de túneles, sin haber realizado el proceso de consulta previa libre e informada con  las comunidades indígenas. Incumpliendo con el Convenio 169 de la OIT, firmado en el año 1994 y ratificado en 1995.

Frente a esta situación, las comunidades continúan firmes para impedir el desarrollo del proyecto hidroeléctrico, por medio de la intensificación de la movilización pacífica contra la empresa en los próximos días.

Tomás Membreño, denuncia que la empresa ha realizado consejos con pobladores, para después obtener firmas que serían falsificadas y buscarían demostrar que la comunidad estaría apoyando la construcción  de la represa. Incluso, según el integrante del COPINH, algunas de las personas que estarían asistiendo a esas reuniones serían los mismos empleados de DESA- Agua- Zarca. Pero realmente, de acuerdo con Membreño, la población de San Francisco de Ojuera está en contra del proyecto hidroeléctrico, tanto así, que hace pocos días habían solicitado una reunión con el COPINH, a la que la defensora del ambiente y los derechos humanos Berta Cáceres ya no podrá llegar.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
