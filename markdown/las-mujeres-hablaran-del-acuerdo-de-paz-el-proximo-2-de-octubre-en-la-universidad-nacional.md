Title: Las mujeres hablarán del Acuerdo de Paz el próximo 2 de octubre en la Universidad Nacional
Date: 2018-10-01 08:43
Author: AdminContagio
Category: eventos, Mujer
Tags: Acuerdos de La Habana, mujeres
Slug: las-mujeres-hablaran-del-acuerdo-de-paz-el-proximo-2-de-octubre-en-la-universidad-nacional
Status: published

###### [ Foto: Cumbre de Mujeres y Paz] 

###### [28 Sept 2018]

El próximo 2 de Octubre, mujeres del movimiento social e integrantes de FARC, se darán cita en el auditorio Margarita Gómez, para hablar del papel que tuvieron las mujeres en el Acuerdo de Paz de La Habana, de sus avances  y los retos que aún faltan para garantizar la construcción de un país incluyente y en defensa de los derechos de las mujeres.

En el escenario se podrán escuchar las voces de **Milena Reyes**, excombatiente de las FARC- EP y actualmente militante de ese partido, que integró la subcomisión técnica y la subcomisión de genero en los diálogos de paz de La Habana; **Katherine Ronderos**, quien tiene una amplia experiencia en el tema sobre violencia contra las mujeres y conflicto armado, actualmente 2012 es la directora de la Liga Internacional de Mujeres por la Paz y la Libertad y **Sandra Luna** feminista y activista por los derechos humanos de las mujer, además, es integrante de la Ruta Pacífica de las Mujeres Regional Santander y asume desde la oficina nacional.

La moderación estará a cargo de la periodista Sandra Gutiérrez, integrante del equipo de Contagio Radio y Ricardo Pajarito, miembro de Contravia, y se realizará en el auditorio Margarita González, en la Universidad Nacional a las 4:00pm. (Le puede interesar: [Mujeres en defensa de la paz: la importancia de la mujer en el conflicto armado"](https://archivo.contagioradio.com/importancia-de-mujeres-en-conflicto-armado/))

Este conversatorio hace parte de los foros adelantados por la Cumbre de Mujeres y  paz, en el marco de los **Diálogos: Mujeres en defensa de la Paz**, que busca informar a los asistentes sobre la participación, los logros y los retos de las mujeres en los procesos de mediación con grupos armados.

###### Reciba toda la información de Contagio Radio en [[su correo]
