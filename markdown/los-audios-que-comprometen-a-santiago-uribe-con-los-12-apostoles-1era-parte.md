Title: Los testimonios que comprometen a Santiago Uribe con los 12 Apóstoles (1era parte)
Date: 2018-03-14 21:30
Category: Judicial, Nacional
Tags: 12 apostoles, alvaro uribe velez, audiencias, Paramilitarismo, Santiago uribe
Slug: los-audios-que-comprometen-a-santiago-uribe-con-los-12-apostoles-1era-parte
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/SANTIAGO-URIBE.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [14 Mar 2018] 

<iframe id="audio_24472331" src="https://co.ivoox.com/es/player_ej_24472331_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

“Meneses hablaba con los señores que estaban ahí, -Cuáles señores- No me acuerdo (...) Solo habían pasado 2 años al 96, uno tenía la mente fresquesita, a 20 años ya no me acuerdo que fue lo que dije. No me acuerdo”, esa fue la afirmación que parecía hacer pensar que el testigo estrella (como en varios medios decían) contra Santiago Uribe Vélez, se había caído en la audiencia del pasado 30 de enero, sin embargo, son aún más los cuestionamientos que deja abiertos ese juicio contra el hermano del hoy senador Álvaro Uribe Vélez, investigado por haber sido supuestamente uno de los financiadores y líderes del grupo paramilitar de los 12 Apóstoles, responsable de más de 570 asesinatos selectivos en el municipio de Yarumal Antioquia.

<iframe id="audio_24472392" src="https://co.ivoox.com/es/player_ej_24472392_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[En su versión del 7 de julio de 1996, Amaya había vinculado a Uribe con la conformación de ‘los 12 Apóstoles' y aseguró que lo vio con un paramilitar conocido como ‘Rodrigo’, a quien en la reciente audiencia, señala como el dirigente de las acciones operativas del grupo paramilitar. A su vez, señaló que no se acordaba cómo había funcionado el grupo, sin embargo, esta vez asegura no acordarse, o no poder si era o no así.]

<iframe id="audio_24472361" src="https://co.ivoox.com/es/player_ej_24472361_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Esta vez no conectó a Uribe con ese grupo ilegal, cuando en 1996 declaró que este había sido el jefe de ‘los 12 Apóstoles’, y que en la Hacienda La Carolina, propiedad de la familia Uribe Vélez y administrada en la época por el hermano del congresista, era uno de los lugares donde se reunían también con el ganadero y comerciante Álvaro Vásquez, exalcalde de Santa Rosa de Osos y comerciante de Yarumal, quienes habrían financiado el actuar del grupo armado.]

<iframe id="audio_24472445" src="https://co.ivoox.com/es/player_ej_24472445_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Santiago Uribe habría sido uno de los financiadores del grupo** 

[Sin embargo, Olwan de Jesús Agudelo Betancurt, quien fue integrante de la estructura de paramilitar 'Los 12 Apóstoles', y testigo de excepción en el proceso contra el empresario y  ganadero Santiago Uribe, asegura que entre los financiadores del grupo se encontraban:  Álvaro Vásquez, el Mono Rojas, Rodrígo Pérez Alzate y Santiago Uribe.]

Cabe recordar que el pasado mes de noviembre de 2017, se conoció que la Comisión de Justicia y Paz alertó de la existencia de un plan para asesinar a Agudelo  dentro del centro carcelario de Puerto Triunfo. [(Le puede interesar: Testigo clave contra Santiago Uribe denuncia plan para asesinarlo)](https://archivo.contagioradio.com/testigo-clave-en-caso-contra-santiago-uribe-denuncia-plan-para-acabar-con-su-vida/)

<iframe id="audio_24472481" src="https://co.ivoox.com/es/player_ej_24472481_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Además, es de resaltar que en la resolución de acusación contra Uribe se evidencian, según los testimonios recogidos, los hechos por los cuales se aseguraría que Santiago Uribe fue la cabeza y el principal financiador de ese grupo ilegal, y que los altos mandos policiales tenían conocimiento de esa situación teniendo en cuenta que se trata del hermano del senador Álvaro Uribe, quien en este momento era candidato a la gobernación de Antioquia. (Le puede interesar: [14 revelaciones de la resolución de acusación contra Santiago Uribe)](https://archivo.contagioradio.com/las-revelaciones-de-la-resolucion-de-acusacion-contra-santiago-uribe-velez/)]

[En un video del 27 de enero de 2014, Semana.com publicó en exclusiva la primera entrevista periodística del mayor retirado de la policía Juan Carlos Meneses. En ese entonces el oficial estaba escondido en Venezuela y preparaba su entrega a la Fiscalía colombiana para convertirse en el principal testigo contra Santiago Uribe. Allí, como lo dice Olwan de Jesús, y como lo había asegurado Amaya en 1996, Menses decía que Uribe Vélez fue parte esencial para la conformación del grupos paramilitar.]

<iframe id="audio_24478684" src="https://co.ivoox.com/es/player_ej_24478684_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Hoy Juan Carlos Meneses no quiere seguir dando información a la justicia colombiana pues asegura que no le han cumplido con nada y ante el fiscal y el juez evidenció los motivos por los cuales no continua dando información. Será ante la Justicia Especial para la Paz, donde continuará su versión de los hechos sobre los posibles vínculos entre Uribe y los 12 Apóstoles.]

<iframe id="audio_24473874" src="https://co.ivoox.com/es/player_ej_24473874_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Reuniones en la Finca la Carolina y en donde habría estado Uribe** 

[Si bien Alexander Amaya asegura no acordarse de nada, no deja de hacer ciertas afirmaciones que de alguna manera dejan ver el papel que jugó Uribe en el grupo paramilitar de los 12 Apóstoles. En la misma audiencia, Amaya si confirma que en la hacienda de los Uribe Vélez se realizaron algunas reuniones relacionadas con los paramilitares. Es de recordar que de acuerdo con sus declaraciones del 1996, allí estaban el padre ]Palacios, el comerciante investigado por nexos con el paramilitarismo, Álvaro Vásquez y Santiago Uribe.

<iframe id="audio_24472586" src="https://co.ivoox.com/es/player_ej_24472586_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe id="audio_24473234" src="https://co.ivoox.com/es/player_ej_24473234_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Pero no es el único que lo asegura. En la misma etapa del juicio, Olguan va más allá y habla sobre quienes participaban en dichas reuniones y en dónde se realizaron. En una de ella habría participado Santiago Uribe en el año 1994 en la finca Moralia.]

<iframe id="audio_24472541" src="https://co.ivoox.com/es/player_ej_24472541_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Este exintegrante de los '12 Apóstoles' también se refiere  a otras reuniones ocurridas en la Carolina, en las que asegura no haber visto a Uribe, debido a que allí se trataban temas sobre el narcotráfico, con lo cual no estaba de acuerdo el hermano del hoy senador del Centro Democrático.  ]

[De acuerdo con Agudelo, una de las reuniones en **La Finca la Carolina** se desarrolló en el año 1995. Para esa época, de acuerdo con el propio Santiago Uribe, él como administrador del predio a partir del primero de octubre de 1994 iba semanalmente a la finca.]

<iframe id="audio_24474917" src="https://co.ivoox.com/es/player_ej_24474917_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

[Uribe afirma también que para ese periodo, hasta el año 2016 cuando fue capturado por las autoridades,  se dedicaba a todo tipo de tareas en la finca de su familia.]

<iframe id="audio_24474376" src="https://co.ivoox.com/es/player_ej_24474376_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
