Title: 8 años menos de condena para Fujimori
Date: 2016-08-16 15:29
Category: El mundo, Judicial
Tags: Alberto Fujimori, Perú, Vladimiro Montesinos
Slug: 8-anos-menos-de-condena-para-fujimori
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/292715.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: America TV 

##### [16 Agos 2016] 

La Sala Permanente de la Corte Suprema peurana resolvió anular la condena contra el expresidente **Alberto Fujimori** por el caso de los **"Diarios Chicha"**, absolviendolo de la condena de 8 años que habia sido proferida en su contra en 2015 por el delito de peculado.

La dependencia magistral presidida por **Javier Villa Stein** y compuesta por cinco jueces, voto de manera unánime por la absolución del ex mandatario con lo que su sentencia pasa de 8 años de prisión efectiva a 3 años de inhabilitación.

La sentencia de la Corte decretaría la libertad inmediata de Fujimori "siempre y cuando no exista en su contra otra orden o mandato de detención emitida por una autoridad competente", situación imposible ya que el ex mandatario **cumple una condena por 25 años por los crímenes del Grupo Colina**.

El caso de los "Diarios Chicha" hace referencia a la prensa urgidos en la década de 1980, centrado en la crónica policial sensacionalista y posteriormente prensa rosa, que en 2000 fue popularizada por Fujimori **para desacreditar a opositores del su gobierno y para favorecerlo en la elecciones generales de ese año**.

Durante el tiempo de gobierno fujimorista, la prensa "Chicha" albergó editoriales controladas por el también condenado ex asesor presidencial **Vladimiro Montesinos**. La decisión de la corte le quita a Fuijmori la pena a pagar por el desvio de 122 millones de soles de las Fuerzas Armadas a la compra de los diarios en mención.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
