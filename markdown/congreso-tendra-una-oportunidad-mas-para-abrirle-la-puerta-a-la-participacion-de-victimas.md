Title: Congreso tendrá una oportunidad más para abrirle la puerta a la participación de víctimas
Date: 2018-03-22 15:02
Category: Nacional, Paz
Tags: acuerdos de paz, Circunscripciones de paz, La Habana
Slug: congreso-tendra-una-oportunidad-mas-para-abrirle-la-puerta-a-la-participacion-de-victimas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-161.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Luis Galindo Contagio Radio] 

###### [22 Mar 2018] 

La Comisión de Paz del Senado y Cámara, en cabeza de Iván Cepeda y Roy Barreras, realizaron un nuevo proyecto de ley que busca revivir las 16 circunscripciones de paz para las víctimas del conflicto armado, que intentará ser aprobado por mayorías en Senado y Cámara. Ahora será **el Congreso el que demuestre su voluntad de abrir el espacio de la política a las víctimas**.

Este proyecto de ley, fue protagonista de una gran polémica el año pasado, cuando por una pequeña mayoría fue aprobado en el Congreso y posteriormente descalificado por el presidente del senado, al asegurar que no se había alcanzado la mayoría de votos. Por esta decisión actualmente hay un recurso jurídico interpuesto, **sin embargo, debido a lo largo que podría ser la Comisión de Paz presentó otro**.

“Va a haber una gran polémica porque también se creó una comisión accidental que pretende presentar otro proyecto, **que no conocemos pero que podría desvirtuar seriamente la filosofía** y el espíritu con el que fue aprobado en los Acuerdos de paz y las 16 circunscripciones” afirmó Iván Cepeda. (Le puede interesar: ["JEP otorga medidas cautelares a los archivos del DAS"](https://archivo.contagioradio.com/jep-otorga-medidas-cauteles-a-archivos-de-inteligencia-del-das/))

Para el senador del Polo Democrático retomar esta iniciativa tiene la intensión de “permitirle a una población que ha sido históricamente excluida y víctima del conflicto, en las zonas donde se desarrolló el conflicto armado, **especialmente en las zonas rurales, tener una representación concretamente en la Cámara de Representantes**”.

Para el Senador, no se puede repetir el argumento de afirmar que las víctimas no van a estar representadas con las circunscripciones por temor a que se avance en la construcción de paz, razón por la cual este proyecto de ley también mantiene la iniciativa de que no puedan participar partidos políticos tradicionales en ellas.

<iframe id="audio_24747255" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24747255_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
