Title: El Chocó continúa sin saber qué es la paz
Date: 2017-12-11 12:31
Category: DDHH, Nacional
Tags: Chocó, Desplazamiento forzado
Slug: el-choco-continua-sin-saber-que-es-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/1489608940171-Morales6-e1511492245372.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mauricio Morales - Vice] 

###### [23 Nov 2017] 

Continúan los "Años de Soledad" en el departamento del Chocó. Este es el tema que aborda el reciente informe de Amnistía Internacional, en donde se evidencia los escasos efectos que el Acuerdo de Paz en ese territorio en donde la comunidad.

“En departamentos como el Chocó, la presencia del Estado es débil y comunidades enteras han quedado **a merced de otros grupos guerrilleros y paramilitares**. Todavía queda mucho por hacer para que el proceso de paz suponga alguna diferencia real en la vida de las personas", señala, Salil Shetty, Secretario General de Amnistía Internacional.

De acuerdo con el informe se realizaron entrevistas y reuniones con organizaciones de diferentes comunidades indígenas y afrodescendientes, se pudo conocer de cerca cuál es la situación viven los habitantes que insisten en que la paz no llega a esas poblaciones, ya que las personas se siguen viendo obligadas a **desplazarse debido a las actuaciones de diversos grupos armados como las AGC, el ELN y el Ejército** que se encuentran en constante actuación.

### **Situación de las comunidades del Chocó** 

El informe cita reportes de la Oficina en Colombia del Alto Comisionado de las Naciones Unidas para los Refugiados (ACNUR), que señala que entre **enero y septiembre de 2017, alrededor de 9.544 personas fueron víctimas de desplazamientos colectivos en el Chocó**.

A octubre de 2017 reportaron 27 eventos de desplazamiento, de los cuales, 3. 115 personas afrodescendientes y 2955 indígenas han sido víctimas. En algunos casos, las comunidades se desplazaron a zonas peligrosas situadas en las proximidades, con frecuencia a alojamientos abarrotados, sin acceso a agua corriente, sin comida suficiente y sin perspectivas de volver a sus territorios.

Por ejemplo, desde marzo del presente año, se estima que **aproximadamente 550 personas pertenecientes a los pueblos Embera Dóbida y Wounaan**, así como de comunidades afrodescendientes de la cuenca del río Truandó se encuentran desplazadas en el municipio de Riosucio, en condiciones indignas de vida, debido a los  enfrentamientos entre el ELN y las AGC.

Por otro lado, en los barrios periféricos de Quibdó se han formado 17 asentamientos de población indígena desplazada por el conflicto armado, de las etnias Embera Dovida, Embera Katío, Embera Eyávida y Wounaan. Según censos, allí viven  alrededor de 1.323 personas.

Asimismo, el informe señala que defensores y defensoras de los derechos humanos y dirigentes comunitarios que han denunciado los abusos han sido amenazados o incluso asesinados.  Además en ese contexto de reacomodo de actores armados se presenten casos de violencia de género, en particular, violencia sexual contra niñas y mujeres indígenas y afrodescendientes.

### **Las recomendaciones de Amnistía Internacional** 

Ante tal panorama, AI recomienda al Estado colombiano, **desmantelar los grupos paramilitares e investigar sus vínculos con las fuerzas de seguridad del Estado.** También, se debe cumplir plenamente la reparación integral, que debe incluir garantías de no repetición a todas las víctimas del conflicto armado, para que de esa manera los Pueblos Indígenas y afrodescendientes en situación de desplazamiento forzado en Quibdó, Bogotá y otras ciudades del país, cuenten con las medidas integrales.

Como si se estuviera haciendo un llamado a que se apruebe una Justicia Especial para la Paz para todos los actores del conflicto, AI recomienda que se garantice que **todas las personas sospechosas de responsabilidad penal por crímenes de derecho internacional o graves violaciones de derechos humanos le respondan a las víctimas.**

Y finalmente, piden que se fortalezcan los esfuerzos para prevenir y eliminar las condiciones socioeconómicas que perpetúan la vulnerabilidad de los pueblos, que además deben contar con atención en salud física y mental debido a las circunstancias de la guerra que han debido atravesar.

###### Reciba toda la información de Contagio Radio en [[su correo]
