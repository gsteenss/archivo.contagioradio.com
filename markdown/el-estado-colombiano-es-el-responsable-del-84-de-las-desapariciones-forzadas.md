Title: El Estado colombiano es el responsable del 84% de las desapariciones forzadas
Date: 2016-08-29 22:26
Category: DDHH, Nacional
Tags: crímenes de estado, Derechos Humanos, Desaparición forzada
Slug: el-estado-colombiano-es-el-responsable-del-84-de-las-desapariciones-forzadas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/La-Escombrera-e1472527270339.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pacifista 

###### [29 Ago 2016] 

Las cifras son inciertas, pero todas pasan por los miles. Las miles y miles de víctimas en Colombia de la desaparición forzada por quienes este año nuevamente se vuelve conmemorar el **30 de agosto el Día Internacional del Detenido Desaparecido** en todas las partes del país, pues este flagelo no ha sido indiferente a las regiones apartadas ni a las grandes ciudades del país.

Según los últimos datos de **Medicina Legal hay 23.441 personas desaparecidas de manera forzada en Colombia**. Por otra parte, de acuerdo con la Unidad para las Víctimas en el país hay al menos **45.944 personas desaparecidas** en el marco del conflicto armado interno y un total de 116.344 son reconocidas como víctimas indirectas.

Lo cierto, es que esa confusión o poca claridad en las cifra se da, debido a que en Colombia aún no existen los mecanismos idóneos para tener un registro veraz,  investigar eficazmente las denuncias, buscar con vida a las personas desaparecidas, ni judicializar a los victimarios.

Gloría Gómez, coordinadora de ASFADDES indica que es necesario que exista **un “mecanismo ágil, inmediato y de calidad, para la búsqueda de las personas desaparecidas”** pues este crimen de lesa humanidad “no se ha asumido con la responsabilidad y la seriedad que merece”.

El año pasado, organizaciones de familiares víctimas de este crimen de lesa humanidad presentaron un informe sobre el estado  de la desaparición forzada de mujeres en Colombia, el primero con enfoque de género en el mundo. Según ese documento, **en ningún otro país la desaparición forzada se ha presentado de manera tan recurrente,** y ha afectado a un número tan grande de personas, como en Colombia.

En el caso de las mujeres, el crimen ha estado vinculado a otros tipos de violencias de género: Aunque se encuentra un registro de 19.625 mujeres desaparecidas, la Fiscalía asegura que en la mayoría de los casos no se tiene información, motivo por el cual no los tipifica como "desapariciones forzadas".

La desaparición forzada contra las mujeres ha incluido además tratos inhumanos, crueles y degradantes, violencia exacerbada y sevicia; como agresión física y verbal, acceso carnal violento, rasurado forzado del cabello, y descuartización vivas; esta última fue la causa de muerte de todas las mujeres desaparecidas por paramilitares, que registra el informe.

**De los 19.625 casos de mujeres desaparecidas, 11.297 se presentaron entre los años 2004 y 2006, durante el gobierno de Álvaro Uribe Vélez,** y empezaron a disminuir a partir del año 2010.

El Estado colombiano no tiene un registro de cuántas personas han desaparecido forzadamente. Mientras la Fiscalía da cuenta de 60 mil personas, y la Unidad de Víctimas apunta a las 44 mil; las organizaciones de familiares víctimas de desaparición forzada, como ASFADES, Familiares Colombia, la Fundación Nidia Erika Bautista, MOVICE, y Desaparecidos del Palacio de Justicia, registran alrededor de 45 mil desaparecidas y desaparecidos forzados. Según denuncian, ha sido falta de voluntad política del Estado la que ha impedido que se establezca un registro oficial. No les falta razón si se considera, como **anuncia el Informe Basta Ya, que agentes estatales son responsables del 84% de estos crimenes.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
