Title: Decisión de Grupo Clarín "es discriminatoria": Patricia Villegas directora Telesur
Date: 2016-03-03 15:43
Category: El mundo, Política
Tags: Telesur Argentina
Slug: decision-de-grupo-clarin-es-discriminatoria-patricia-villegas-directora-telesur
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Telesur.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cupre Ecuador ] 

<iframe src="http://co.ivoox.com/es/player_ek_10663226_2_1.html?data=kpWjmJiWdpehhpywj5aWaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncaXZxM7gy4qnd4a2lNOYxsqPi9Pp0dSYpdHFtoa3lIquptOPaZOmxtiYxs7Xp9Pdzs7bw9nTtsrVhpefh5ilb7HV1dfWxZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Patricia Villegas, Telesur] 

###### [3 Mar 2016 ] 

El Grupo Clarín decidió retirar la cadena Telesur del paquete más económico de televisión por cable que presta Cablevisión en Argentina, Patricia Villegas actual directora de Telesur, asegura que esta medida es **discriminatoria e impide el acceso libre a las transmisiones** de esta cadena televisiva, pese a que su señal es libre y sin costo en todos los países. “No lo consideramos una práctica empresarial, sino una discriminación”, agrega.

De acuerdo con Villegas la razón dada por Cablevisión frente a esta decisión “se cae de su propio peso”, pues la empresa aseveró que iba a retirar a todas las cadenas internacionales del paquete básico de televisión por cable y que sólo iban a quedar las argentinas; sin embargo, hay canales de otros países que siguen siendo transmitidos, la pregunta entonces es **qué hay detrás de la salida de Telesur** y de la “mentira de patas cortas” con la que salió, según afirma la directora.

Pese a la prudencia con la que las directivas de Telesur han querido manejar el hecho, Villegas pone en consideración las **acciones que el gobierno de Macri ha puesto en marcha para intervenir la Ley de Medios**, así como su postura contradictoria de asegurar, por un lado, que priorizará los canales de países pertenecientes al Mercosur y por el otro, permitir que la mayoría de población argentina no pueda acceder a la Telesur.

La directora concluye aseverando que **Telesur es “uno de los símbolos de la integración regional”**, de importancia estratégica que no se da sólo entre iguales ni es únicamente cuestión de gobierno. La cadena interpusó un recurso de amparo que se espera sea fallado en las próximas 48 horas por un juez de Argentina pueste este hecho "enciende las alarmas" por la democratización de la palabra y la posibilidad de acceder a información diversificada.

##### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](https://archivo.contagioradio.com/)].
