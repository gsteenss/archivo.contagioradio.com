Title: En tiempos de sequía se reactiva defensa del Páramo Santurbán
Date: 2020-01-21 15:27
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente, marzo26, Movilización, páramos, santurbán
Slug: en-tiempos-de-sequia-se-reactiva-defensa-del-paramo-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/paramo-de-santurban-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Mayerly López integrante del Cómite de defensa del Páramo de Santurbán indicó que este 24 de enero junto con otros integrantes del Comité se reunirán con la alcaldesa de Bogotá Claudia López, con el fin de solicitar los permisos que les garanticen el desligue de la marcha caravana que se realizará el 26 de marzo en Bogotá.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente anuncio que convocarán ***artistas, colectivos y demás movimientos sociales en la capital***, que se quieran sumar a la defensa de agua y los ecosistemas que la producen. (Le puede interesar: <https://archivo.contagioradio.com/paramo-de-santurban/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma señaló que , ***"en el caso de Bucaramanga, Santurbán es la única fuente de agua. Por eso toda la ciudadanía debe movilizarse en torno a la defensa del Páramo"***; adicional López espera que el 26 sea declarado como día cívico en Bucaramanga , esto con la finalidad de convocar a un mayor número de personas

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El grito por el agua se sentirá en toda Colombia

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según la defensora la jornada de movilizaciones empezará el 15 de marzo cuando diferentes buses conduzcan desde Bucaramanga hacia Bogotá a más de un centenar de Personas que conformarán la movilización. (Le puede interesar: <https://archivo.contagioradio.com/permitir-mineria-en-santurban-es-arriesgar-el-85-del-agua-de-los-colombianos/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

López afirmó, *"Marcharemos en Bogotá porque allá también tienen a Sumapaz y Chingaza, importantes fuentes hídricas en riesgo "* , y agregó que este encuentro no solo tiene como finalidad pedir al presidente Duque negar la licencia ambiental de la multinacional árabe Minesa, sino alzar la voz por la defensa del agua y los ecosistemas que se han visto afectados por la intervención industrial.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ComiteSanturban/status/1219413733539270664","type":"rich","providerNameSlug":"twitter","align":"center","className":""} -->

<figure class="wp-block-embed-twitter aligncenter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ComiteSanturban/status/1219413733539270664

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por otro lado el Comite Santurbán celebró este 21 de enero el anuncio de Juan Carlos Cárdenas , Alcalde de Bucaramanga de respaldar las jornadas de movilizaciones que se darán desde la capital santandereana hasta Bogotá este 16 de marzo, por la defensa de del Páramo de Santurban.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mayerly López integrante del Comite Santurban afirmó que desean garantizar a todos los defensores del Páramo una movilización segura, *" el Alcalde de Bucaramanga dijo que apoyaría jurídicamente la movilización, ayudando a las garantías para que las personas se puedan movilizar".* (Le puede interesar: <https://www.justiciaypazcolombia.com/2017-sol-naciente/>)

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Hay que defender este recurso sin importar las ideologías policías religiosas o étnicas, porque el agua la consumimos todos por igual"*
>
> <cite>Mayerly López </cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:html -->  
<iframe id="audio_46799907" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_46799907_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
