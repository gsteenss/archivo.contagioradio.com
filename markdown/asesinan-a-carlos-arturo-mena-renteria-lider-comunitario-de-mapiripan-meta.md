Title: Asesinan a Carlos Arturo Mena Rentería, líder de Mapiripán, Meta
Date: 2017-12-07 15:26
Category: DDHH, Nacional
Tags: Alcaldía de Mapiripan, amenazas a líderes sociales, asesinato de líderes sociales, carlos arturo mena, defensores de derechos humanos, mapiripan
Slug: asesinan-a-carlos-arturo-mena-renteria-lider-comunitario-de-mapiripan-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 Dic 2017] 

El 2 de diciembre de 2017 fue asesinado en Mapiripán el líder comunitario Carlos Arturo Mena Rentería. El crimen sucedió en el caserío de **El Rincón del Indio** en horas de la tarde y solo hasta el 6 de diciembre fue posible que las comunidades confirmaran el asesinato.

De acuerdo con la Comisión Intereclesial de Justicia y Paz, **“hombres de las disidencias de las FARC** llegaron a la residencia del líder y sin mediar palabra le dispararon”. Tras el homicidio, los hombres dejaron sobre el cuerpo sin vida un papel con la frase “lo matamos por sapo y por informante”.

De acuerdo con Moisés Ortiz, también líder comunitario de Mapiripán, el Ejército ya ha hecho presencia en la zona, pero **"la gente está muy consternada** porque conocían a Carlos hace muchos años y era un líder muy reconocido”. Afirmó que hay una preocupación por este asesinato y “las personas están muy dolidas, lo querían mucho porque era un gran líder”. (Le puede interesar: ["Atentan contra Arley Velasco, líder social del Putumayo"](https://archivo.contagioradio.com/lider-social-en-putumayo/))

###  

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
