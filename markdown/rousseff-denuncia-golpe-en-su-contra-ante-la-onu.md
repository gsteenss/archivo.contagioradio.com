Title: Rousseff denuncia golpe en su contra ante la ONU
Date: 2016-04-22 17:07
Category: El mundo, Política
Tags: Dilma Rousseff, Golpe de estado Brasil, ONU
Slug: rousseff-denuncia-golpe-en-su-contra-ante-la-onu
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/dilma-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:

##### [22 Abr 2016]

Durante la sesión de firma del Acuerdo de París sobre el cambio climático realizado hoy en la sede de la Organización de las Naciones Unidas, la presidenta brasileña Dilma Rousseff se refirió al "**grave momento que vive Brasil**" en relación con el golpe de estado que se adelanta en su contra.

Al cierre de su intervención ante los líderes del mundo presente en Washington, la mandataria expresó que Brasil "**es una sociedad que supo vencer el autorismo y construir una pujante democracia**" un pueblo "trabajador con gran aprecio por las libertades que **sabrá impedir cualquier retroceso**".

Antes de finalizar, Rousseff agradecio a todos los líderes que manifestaron su solidaridad ante el proceso de impeachment aprobado en primera instancia por la Cámara y que ahora cursa en el Senado que empezará a trabajar desde la semana siguiente en la comisión especial que determinará la posibilidad de apartarla de la presidencia.

La denuncia de la mandataria, advertida desde antes de su viaje a los Estados Unidos, había sido calificada como "negativa para el país" por el vicepresidente Michel Temer, quien muchos sindican de estar vinculado con la intención de realizarle un juicio político a la presidenta, para hacerse con la presidencia del país.
