Title: Fuerza Pública entró disparando, hurtando y persiguiendo a la gente: COCOCAUCA
Date: 2020-09-23 21:24
Author: PracticasCR
Category: DDHH, Nacional
Tags: Cauca, Consejo Comunitario Parte Baja del Río Saija, violación a derechos humanos
Slug: fuerza-publica-entro-disparando-hurtando-y-persiguiendo-a-la-gente-cococauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Consejo-Comunitario-Parte-Baja-Rio-Saija.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Cons Comunitario Parte Baja Rio Saija

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El Consejo Comunitario Parte Baja del Río Saija, municipio de Timbiquí, costa pacífica del Cauca, ha realizado una [denuncia pública](https://cococauca.org/2020/09/23/comunidades-negras-en-timbiqui-cauca-denuncian-violacion-de-derechos-por-fuerza-publica/) en la que señala que la fuerza pública ingresó el 22 de septiembre en horas de la mañana a la comunidad de la Viuda disparando indiscriminadamente a las casas de los habitantes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente, restringieron la movilidad y hurtaron pertenencias y alimentos de las comunidades. Además, realizaron persecuciones  
y señalamientos contra líderes comunitarios. (Le puede interesar: [Padre e hijo, víctimas de nueva masacre en Algeciras](https://archivo.contagioradio.com/en-algeciras-se-registra-masacre-en-donde-pierde-la-vida-padre-e-hijo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El consejo denuncia que las actuaciones por parte de la fuerza pública violan el derecho a la vida, la integridad, la autonomía y el territorio de las comunidades que conforman el consejo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CococaucaOrg/status/1308807472963031044","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CococaucaOrg/status/1308807472963031044

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**El Consejo agrega que este proceder no solo es un ataque contra la integralidad de las comunidades, sino también en contra de la autonomía y los derechos territoriales de la misma**. Además, explica que “el ingreso de las autoridades de control público y militar del estado deben ser coordinados con la junta del Consejo y en coordinación con el comité de seguridad territorial , las acciones militares a desarrollar en el territorio”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la denuncia, el consejo comunitario llama al Gobierno y a autoridades locales a desmilitarizar el territorio colectivo y a exigir que se respeten y protejan los "derechos a la vida, integralidad y autonomía de las comunidades". (Le puede interesar: [Asesinatos contra líderes sociales se incrementaron un 85%: MOE](https://archivo.contagioradio.com/asesinatos-contra-lideres-sociales-se-incrementaron-un-85-moe/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, piden a organismos de derechos humanos nacionales e internacionales, y a organizaciones sociales a denunciar hechos en los que se vulneren los derechos humanos de las comunidades, líderes y lideresas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta no es la primera vez que se dan a conocer casos de abusos y agresiones por parte de la fuerza pública en el departamento del Cauca, pues medio de operativos de erradicación forzada de cultivos ilícitos campesinos y campesinas han resultado gravemente heridos.  

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
