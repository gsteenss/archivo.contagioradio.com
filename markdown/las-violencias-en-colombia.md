Title: Las Violencias en Colombia
Date: 2015-07-03 12:47
Category: Laura D, Opinion
Tags: Curvarado, GMH, Guerra de los Mil Días, Jorge Eliécer Gaitán
Slug: las-violencias-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Captura-de-pantalla-2015-07-03-a-las-12.42.24.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: seloexplicoconplastilina.org] 

[Por]**[[Laura Duque](https://archivo.contagioradio.com/laura-duque/)] - [~~@~~lavadupe](https://twitter.com/lavadupe){.twitter-atreply .pretty-link} **

###### [3 Jun 2015 ] 

A propósito de los diálogos de la paz, hablemos de las violencias. Ni siquiera tomemos en cuenta la conquista y la reconquista española y toda su injusticia; hablemos del perdido siglo XX. Inició con la terminación de la Guerra de los Mil Días, la que dejó un saldo de más de cien mil muertos. 30 años después se cocinó en la Colombia una sangrienta lucha bipartidista que detonó en la Violencia, con el asesinato del líder popular Jorge Eliécer Gaitán el 9 de abril de 1948; el pueblo se desangró hasta poco antes de los 60. Esa fue una Violencia que nombramos con mayúscula, quizá porque quienes la padecieron o fueron testigos no imaginaron que una época tan llena de terror y de sevicia pudiera repetirse.

¡Sucedió! La Violencia entró en su ciclo, surgió de nuevo, con tanto o más rigor que años atrás: a mediados de los 90 los paramilitares derramaron la sangre y los sueños de miles de colombianos. Ya no fueron los “pájaros”, se nombraron Autodefensas Unidas de Colombia; y aunque no fueron los mismos ofensores, sí aplicaron los mismos métodos sanguinarios, y aunque no fue el machete sí la motosierra.

El salvajismo de los “pájaros” o de los “mochacabezas” se puede igualar a los actos de cualquier grupo extremista del mundo, como los yihadistas o mercenarios israelíes, por ejemplo: degollamientos, empalamientos, decapitaciones, descuartizamientos… gente condenada a morir cruelmente como una forma de infringir miedo entre los pobladores y obtener el poder sobre los territorios.

¡Y la gran mayoría campesinos! Eso de que luchaban “por Colombia”, eso de que su accionar era una contribución a la “patria” para librarnos de la plaga insurgente, comunista o terrorista, considero que fue sobre todo retórica y la coartada perfecta para acaparar el botín: las tierras; supongo entonces que se referían a esa Colombia agroindustrial: de capitalistas o empresarios; políticos interesados también en el latifundio; terratenientes o hacendados, etc., ¡pero nunca a la clase popular colombiana! Porque el pueblo sólo les ha servido de mano de obra barata o de carne de cañón.

¿Y el Estado? La Fuerza Pública hizo presencia en los campos, sí; pero no en son de paz, sino con el ánimo de ofender y encubrir la barbarie. Cómplices por acción y por omisión, los gobiernos han impulsado un modelo de “desarrollo” dirigido a unos pocos poderosos a costa del dolor y la pobreza de las mayorías. En las Violencias, los gobiernos de turno sembraron el terror de la mano paramilitar. No es casualidad que, de acuerdo con los datos ofrecidos por el Grupo de Memoria Histórica, entre 1996 y 2002 se hayan presentado el mayor número de masacres: 1.089 con 6.569 víctimas; la misma época en que el exgobernador de Antioquia Álvaro Uribe Vélez defendió a capa y espada la organización y operación de las “Convivir”: el paramilitarismo se legalizó. Por lo menos hoy, ese fenómeno paraestatal es una verdad pública irrefutable.

Primero soportada en el sectarismo y luego en la guerra contrainsurgente, la violencia se ha empleado como el más eficiente de los métodos para aniquilar y someter a poblaciones enteras. ¿Por qué tanta sevicia? ¿Quién les ha indicado a esos verdugos los mismos procedimientos en todos los rincones de Colombia con hombres, mujeres, niños y ancianos?

Según el GMH, para el período comprendido entre 1981 y el 2012, se presentaron 588 eventos de violencia en los cuales hubo huellas o marcas de sevicia en los cuerpos de 1.530 personas, registro que se limita a las acciones violentas en las que el cuerpo fue exhibido públicamente como parte de su estrategia de terror. Es de anotar que de esa crueldad extrema: 63% fue atribuido a grupos paramilitares; 21,4%, a grupos no identificados; 9,7%, a miembros de la Fuerza Pública; 5,1%, a las guerrillas; y 0,7%, a grupos paramilitares y Fuerza Pública en acciones conjuntas.

Las Violencias en Colombia se han justificado en banderas o colores, nombres o ideologías; ¡pero qué va! a la larga se esconde un interés en común que los mueve a unos y a otros y los conecta a todos: el dinero.

Pero, como dicen por ahí, al pueblo le han quitado tanto, que hasta el miedo ya se nos quitó. Comunidades enteras, como las de los territorios colectivos de la cuenca del río Curvaradó,  resisten; comunidades caucanas en medio de la guerra, resisten.

Hoy, a propósito del impacto en los ríos que circulan negros a causa del derramamiento de petróleo por el accionar guerrillero; recordemos también esa época cuando los mismos ríos, y otros, se tiñeron de rojo con la sangre y la carne de civiles arrojados y olvidados por un Estado cómplice y ofensor.
