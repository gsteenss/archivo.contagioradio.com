Title: ¿Quiénes son los enemigos de la Jurisdicción Especial de Paz?
Date: 2018-05-23 12:23
Category: Paz, Política
Tags: 14 menores de edad y una mujer embarazada fueron víctimas mortales, Iván Cepeda, JEP
Slug: quienes-son-los-enemigos-de-la-jurisdiccion-especial-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/JEP.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colombia Informa] 

###### [23 May 2018] 

Una vez más se aplazó el debate sobre la Jurisdicción especial para la paz, después de que no se completara en quórum en la Comisión primera conjunta de Cámara y Senado. Para Iván Cepeda, senador del Polo Democrático este hecho evidencia que el poco interés del Congreso por aprobar la JEP con el agravante de que se está en periodo electoral, **situación que incluso podría aplazar la entrada en funcionamiento del Tribunal de paz.**

Para Cepeda, “por un lado se presiona a la Jurisdicción para que dé resultado, y por el otra se hace todo lo posible para evitar que funcione”. En ese sentido señaló que las intenciones tras las múltiples dilataciones, tienen que ver con intereses de sectores, que incluso quieren destruir la JPE, **“lo han dicho claramente candidatos presidenciales como Iván Duque por ejemplo, la iniciativa del Centro Democrático** es acabar con la Jurisdicción Especial para la paz, e incluso con las otras formas de justicia que hay en Colombia”.

Asimismo, afirmó que la JEP también tiene enemigos en las instituciones estatales y a funcionarios públicos que buscan **“inmiscuirse y limitar la autonomía de este órgano**”. Razón por la cual, hizo un llamado a la sociedad en general para q   ue defienda lo logrado hasta el momento de los Acuerdos de Paz de La Habana. (Le puede interesar: ["Víctimas piden respeto a la JEP y su independencia Judicial"](https://archivo.contagioradio.com/organizaciones-vicitimas-jep/))

### **¿Es necesaria una ley para aprobar la JEP?** 

El senador afirmó que hay Juristas, especialistas en el tema, que han manifestado que no es necesario que se apruebe una ley para que la mayoría, o la gran mayoría de los procedimientos de las salas y del Tribunal de la JEP, pudiesen actuar.

“Con normas pre existentes y con el hecho de que la Jurisdicción y los jueces se pueden dar norma de procedimiento y de reglamento para ejercer, se podrían sin ninguna clase de problema, **entrar a actuar con relación a muchos de los asuntos urgentes que tiene que abocar la JEP”** expresó Cepeda.

<iframe id="audio_26139888" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26139888_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
