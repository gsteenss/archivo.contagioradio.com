Title: "Ley que crea CTI y Fiscalía castrense es inconstitucional" Alirio Uribe
Date: 2015-07-29 15:56
Category: DDHH, Nacional
Tags: alirio uribe, CTI, das, Fiscalía, fuero penal militar, impunidad, interceptaciones ilegales, Polo Democrático Alternativo
Slug: ley-que-crea-cti-y-fiscalia-castrense-es-inconstitucional-alirio-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/FUERO-MILITAR-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [www.pulzo.com]

<iframe src="http://www.ivoox.com/player_ek_5524918_2_1.html?data=lpqflp6VfI6ZmKiakpyJd6KllJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmrcrmjdbZqYzX08rOjai4jYztjKvW1cjFsIa3lIqupsaPp8Ln1dfS0NjJt4zZ1JDW0MjTstToytnixZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alirio Uribe, Representante a la Cámara] 

###### [29 jul 2015]

El presidente Juan Manuel Santos sancionó este martes la Ley mediante la cual, el país tendrá en unos meses una **Fiscalía y un Cuerpo de Investigaciones Técnicas, CTI, dedicados específicamente al trabajo que se requiera en la Justicia Penal Militar y Policial**. Ante esta posibilidad el representante Alirio Uribe, asegura que se trata de una Ley inconstitucional que promueve la impunidad.

Uno de los argumentos que da el representante, es que el CTI creado no se estipuló bajo los parámetros constituciones, teniendo en cuenta que los militares no pueden tener funciones de policía judicial, “**por lo tanto, toca crear el CTI de naturaleza civil y no uno de militares, ya que se contribuye a la impunidad** en casos como por ejemplo, los falsos positivos”, dice Alirio Uribe.

Por otro lado, con esta nueva Ley **se viola el artículo 250 de la Constitución** que establece que la atribución de negociar penas y aplicar principio de oportunidad es de competencia exclusiva del Fiscal General o sus delgados, sin embargo, “**el Congreso dio facultades al fiscal penal militar para que negocie penas”**, explica el congresista.

De acuerdo con el representante del Polo Democrático, con la ampliación del fuero penal militar **el presidente hizo que muchos delitos comunes pasen a la jurisdicción penal militar.** De manera que delitos como, interceptaciones ilegales como el caso del DAS y otros casos de corrupción al interior del ejercito, pasarían a ser juzgados por los mismo militares, lo que no sería incongruente para que realmente haya justicia.

Los tres argumentos serán parte de una demanda que presentará el Polo ante la Corte Constitucional contra la Ley que sancionó el Presidente Santos, ya que **“se fortalece el fuero penal militar y con ello la impunidad”,** señala el congresista.
