Title: Dos masacres al sur de Córdoba en menos de 48 horas
Date: 2020-07-29 11:13
Author: CtgAdm
Category: Actualidad, Nacional
Tags: cordoba, Cordoberxia, genocidio, paramilitares
Slug: dos-masacres-al-sur-de-cordoba-en-menos-de-48-horas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/situación-sur-de-cordoba-mp3.mp3" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

*Foto: Imagen [archivo LaRazon.co](Foto:%20LaRazon.co)*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La violencia en zona rural del país no se detiene, desde las **organizaciones de Derechos Humanos del Sur de Córdoba denuncia en menos de 24 horas el asesinato de 6 personas** y el [desplazamiento](https://archivo.contagioradio.com/catatumbo-sufre-seis-tipos-de-pandemias/) de 50 familias producto de la violencia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El primer hecho armado se presento en la madrugada del lunes 27 de julio en la vereda la Cabaña, corregimiento Versalles del municipio San José de Uré en el sur de Córdoba, y en donde fueron asesinados tres miembros de la misma familia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ascsucor_org/status/1287919670109057024","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ascsucor\_org/status/1287919670109057024

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El hecho atribuido al grupo armado Los Caparrós, dejó como victimas a **dos adultos mayores identificados como Elizabeth Meléndres, y Bitaliano Feria,** asesinados junto con su hijo Édison Meléndres. Según la comunidad, la mujer hacía parte del programa Nacional de Sustitución.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además la nieta de la pareja e hija de Édison , fue amarrada y llevada en un camión hacia las afueras del territorio, además, según la[**Fundación Social CORDOBERXIA**](https://www.justiciaypazcolombia.com/una-masacre-mas-en-el-sur-de-cordoba/), los hombres armados le dan un ultimátum a la comunidad, ***"donde les dicen que se tienen que ir o les van a mochar la cabeza".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además de esto, los hombres armados ingresan a las casas de las diferentes personas en la vereda y se llevaron todo tipo de objeto de valor, incluidos los celulares, única fuente de comunicación. **El hecho causó el desplazamiento forzado de 50 familias hacia el casco urbano.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Contagioradio1/status/1288321204965842944?s=19","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Contagioradio1/status/1288321204965842944?s=19

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

El segundo suceso se registró 24 horas después, *el 28 de julio, sobre las 5:00 p.m, en la finca Villa Horizonte, corregimiento de Puerto Colombia,* también jurisdicción del municipio de San José de Úre, en el que se reportó el hallazgo de **tres cuerpos sin vida**, uno de ellos identificado como **John Jairo Velarde Andica, de 46 años**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según las Unidades del Ejército y Policía que llegaron a este territorio, el posible **responsable de este triple asesinato** es la subestructura, Rubén Darío Ávila, del grupo armado, **Clan del Golfo.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cuál es el papel del las FFMM al Sur de Córdoba?

<!-- /wp:heading -->

<!-- wp:paragraph -->

**José David Ortega, vocero de la Asociación Campesina del Sur de Córdoba,** señaló que la situación de Derechos Humanos al Sur de Córdoba se da a pesar de que **se encuentra vigente la alerta temprana 054-19, emitía el 18 de diciembre del 2019 por la Defensoría del Pueblo**; la cual señala medidas para que la institucionalidad garantice la vida de los habitantes del territorio así como su permanencia.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***" Hemos visto que la Defensoría del Pueblo se convirtió en una ONG más en las que presentan alertas y aletas pero nadie acata lo que está señalando".***
>
> <cite>**José David Ortega| vocero de la Asociación Campesina del Sur de Córdoba**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

A su vez señaló, que luego de la firma del Acuerdo de Paz se presentó el Decreto 154 ,en **el que el Gobierno debe tomar acciones contundentes ante las acciones de los grupos armados** que realicen masacres en los territorios, pero ***"este tipo de acciones, ni las muchos alertas que hemos presentado vemos que aseguren la vida en el territorio".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que pese a la presencia de la **Fuerza Tarea Conjunta Aquiles,** *"con más de 2.500 efectivos, **no alcanzamos a entender como la Fuerza Pública más grande del país y que supuestamente tiene el control, permite que ingrese un grupo armado,** torture, haga cuánta barbaridad se le ocurra en una comunidad y no haya nadie que responda"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que según Ortega, pone en evidencia que el problema no es solo la Fuerza pública, sino que, **en los territorios no existe inversión social y tampoco se acatan las alertas** y recomendaciones que se hacen para que cese la violencia en el territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y recordó otros casos similares de violencia en los últimos meses, el primero, fue el **asesinato de Manuel Osuna Tapia,** también adulto mayor, quien fue torturado, degollado e incinerado junto a su casa y frente a sus hijos, **caso que hasta el día de hoy continua en la impunidad.** A esto se suma la masacre en la que hombres armados incursionaron en tres veredas una de ellas Brazo Izquierdo, asesinado a 4 personas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Este no es un tema de rutas del narcotráfico

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para Ortega la intensión de los los grupos armados es desplazar a la gente de sus territorios, ***"ellos no quieren que haya nadie en la zona, quieren un territorio libre de la comunidad, con el objetivo de tener control sobre los predios"***, y no como una acción entorno a las rutas del narcotráfico, sino al despojo y la acumulación de tierras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez la senadora **Victoria Sandino, integrante del partido FARC,** además de condenar con mucha indignación los hechos ocurridos en la zona rural de San José de Ure, señaló, *"en el Congreso de la República, no ha habido ningún tipo de respuesta o adelanto frente a las acciones que hemos denunciado desde la Comisión de Paz".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto mientras, ***"la persecución hacia las comunidades del Sur de Córdoba, los asesinatos, amenazas e intimidaciones hacia la comunidad rural son el pan de cada día",*** en ese sentido señaló que una de las acciones que que pretenden instaurar es, **una sesión especial de la Comisión de Paz**, abordando la situación del departamento y generando respuestas reales, *"para este genocidio contra las comunidades del departamento de Córdoba se detenga".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:audio {"id":87482} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/situación-sur-de-cordoba-mp3.mp3">
</audio>
  

<figcaption>
Escuche el análisis de esta información junto a *José David* Ortega, integrante de Asociación Campesina sur de Cordoba y la senadora Victoria Sandino.

</figcaption>
</figure>
<!-- /wp:audio -->
