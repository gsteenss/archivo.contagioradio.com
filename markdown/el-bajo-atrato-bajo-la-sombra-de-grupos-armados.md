Title: El Bajo Atrato bajo la sombra de grupos armados
Date: 2018-09-19 12:36
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: Autodefensas Gaitanistas de Colombia, Bajo Atrato, ELN
Slug: el-bajo-atrato-bajo-la-sombra-de-grupos-armados
Status: published

###### [Foto: Justicia y Paz] 

###### [19 Sep 2018] 

La Comisión de Vida,  Justicia y Paz de la Diócesis de Apartadó, manifestó a través de un comunicado de prensa, un conjunto de violaciones a derechos humanos que se vienen presentando en el Bajo Atrato, **producto del control de estructuras armadas y las disputas entre las mismas por manejar las rutas del narcotráfico, que ha generado desplazamientos masivos en la región. **

De acuerdo con Pilar Plazas, integrante de la Comisión, esta situación es el resultado del tránsito que hizo la FARC con el Acuerdo de Paz y su proceso de reincorporación a la sociedad civil, dejando los territorios que antes ocupaba y que ahora están siendo disputados por organizaciones armadas como ELN o las Autodefensas Gaitanistas de Colombia, y **por la expansión y fuerza que están tomando estructuras como las AGC, que estarían siendo financiadas para garantizar su crecimiento**.

### **Las comunidad en medio del conflicto** 

Estos factores han provocado que las comunidades del Bajo Atrato  continúen en medio del conflicto, y según Plazas, ha elevado el riesgo para la vida de quienes habitan el territorio, ya que se han presentando **hechos de intimidación, siembras nuevas de minas antipersona y confinamientos de familias**.

En hechos recientes, la Comisión denunció el desplazamiento forzado en la Cuenca de Truandó de dos comunidades, la primera es la comunidad indígena Peñas Blancas que desde el pasado 11 de septiembre se encuentra en Río Sucio y  la segunda, es la comunidad afro de Taparal, que salió de su territorio después de que el ELN asesinara a un joven de 26 años y amenazara a los habitantes.

Plazas, recordó que estas comunidades ya habían sido víctimas de desplazamientos forzados en la década de los noventa  y **"desde entonces, no han dejado de ser golpeadas",** además señaló que llena de "indignación" que pese a toda su entereza y fuerza por regresar al territorio vuelvan a ser revictimizadas.

### **Las acciones de la Fuerza Pública** 

Frente a las medidas que se están tomando desde la Fuerza Pública, Plazas aseguró que si bien hay presencia de autoridad en los ríos grandes como el Atrato, en los pequeños afluentes no la hay, y es allí en donde se encuentran las comunidades que están siendo víctimas de las estructuras armadas. (Le puede interesar:["Se agudiza crisis humanitaria en el Chocó"](https://archivo.contagioradio.com/se-agudiza-crisis-humanitaria-en-choco/))

"La semana pasada hubo en Río Sucio, dos consejos de seguridad, sí se dice, pero no pasa nada" afirmó Plazas y agregó que en esos espacios hay presencia de las autoridades y conocen lo que han venido denunciando las comunidades, sin que existan garantías reales para los habitantes en el territorio. **Asimismo informó que en otro espacio con el Comisionado de paz, a quién también se le expresaron los actos de violencia**.

Para la integrante de la Comisión de Vida, Justicia y Paz de la Diócesis de Apartadó, es urgente que se den soluciones para que exista el derecho a una "vivir en paz" en las comunidades.

 

<iframe id="doc_88967" class="scribd_iframe_embed" title="13690" src="https://www.scribd.com/embeds/389000532/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-FzKlaydOwCFRhoCABj28&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

<iframe id="audio_28732942" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28732942_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
