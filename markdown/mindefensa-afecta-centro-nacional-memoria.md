Title: Inclusión del Mindefensa afectaría autonomía del Centro Nacional de Memoria
Date: 2017-04-18 15:06
Category: DDHH, Nacional
Tags: Centro Nacional de Memoria Histórica, ministerio de defensa, víctimas
Slug: mindefensa-afecta-centro-nacional-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/victimas-del-estado.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 Abr 2017]

Por decreto presidencial el Gobierno del presidente Juan Manuel Santos, decidió que el Ministerio de Defensa haga parte de la Junta directiva del Centro Nacional de Memoria Histórica. Un anuncio que preocupa a diversos sectores sociales y políticos pues **pone en juego la autonomía del Centro** **Nacional de Memoria** como lo califica Camilo Gonzáles Posso, presidente de INDEPAZ.

De acuerdo con González, si bien el Gobierno ha garantizado y respetado el trabajo del CNMH, **"esta decisión es muy problemática"** poniendose en peligro la autonomía, la independencia y la objetividad con la que ha llevado a cabo su tarea el Centro, que desde siempre ha puesto primero las voces de las víctimas.

### **Centro Nacional de Memoria respalda la presencia de las víctimas** 

Por su parte el CNMH, emitió un comunicado mediante el cual habla de su trayectoria académica e investigativa en el esclarecimiento frente a las responsabilidades de todos los actores, legales e ilegales, y las afectaciones a todas las víctimas, en el marco del conflicto armado. Es así como **hace el llamado al Estado colombiano a asumir de manera crítica su propio rol y las responsabilidades en la guerra.**

"El CNMH por mandato y convicción ratifica su compromiso con el derrotero normativo de la Ley de Víctimas que establece que no habrá memoria oficial", y agrega que seguirá **promoviendo la pluralidad de la memoria y la centralidad de las víctimas** en su construcción.

Por ello el Centro Nacional de Memoria afirma que acoge y respalda la iniciativa recientemente expresada por diversas organizaciones para que se amplíe la participación de las víctimas en el Consejo Directivo de la institución.

El CNMH "**No debe ser un centro de memoria de gobierno, no debe ser partidista, debe ser de la sociedad y para los derechos de las víctimas,** eso es lo que necesitamos para la reconciliación, no que esté bajo la vigilancia de una institución", dice Camilo González, quien agrega que la **presencia del Ministerio de Defensa es un mal paso** que puede dar pie justamente a ese control gubernamental.

### **Hay alternativas para el Centro Nacional de Memoria** 

Ante este panorama también se plantean soluciones. El presidente de INDEPAZ, explica que, como años atrás ya se había planteado, l**o que debe hacerse es crear una corporación mixta integrada por diversas instituciones, pero con estatuto y junta directica equilibrada** que a su vez nombre un presidente, y no que esté sea nombrado por el gobierno o alcalde de turno, para que de esta manera a garantice un real autonomía de este tipo de centros.

<iframe id="audio_18214170" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18214170_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
