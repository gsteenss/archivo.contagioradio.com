Title: Día internacional de la protección de la capa de Ozono
Date: 2018-09-16 11:00
Author: AdminContagio
Category: Ambiente, Otra Mirada
Tags: Ambiente
Slug: dia-internacional-capa-ozono
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/CAPA-DE-OZONO.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Expok News 

##### 16 Sep 2018 

Cada 16 de septiembre celebra el día internacional de la protección de la capa de Ozono, una frágil franja de gas que nos ha protegido de los efectos nocivos de los rayos de sol durante siglos.

Desde 1994 la Asamblea General de las Naciones Unidas proclamo este día en el marco de la firma del protocolo de Montreal, un acta que habla sobre las afectaciones que la agotan, incluyendo sustancias químicas como los clorofluorocarbono, advertencias que se venían profiriendo desde 1974 por parte de diferentes científicos del mundo.

Según la ONU, estamos lejos de la meta de recuperar el total de la capa de ozono, lo cual se podría lograr aproximadamente hasta el año 2050, solo si todos los países se comprometen en dicha tarea. América Latina y el Caribe, contribuyen únicamente con el 14% del consumo global de sustancias que destruyen la capa de ozono, siendo República Dominicana el país modelo en su protección.

La protección de la capa de ozono se ha convertido en un tema importante, particularmente en los últimos 30 años, cuando empezó a tocar intereses relacionados con el desarrollo sostenible. De ahí que se estén implementando esfuerzos como el programa Consérvate Cool que ha permitido la reducción y eliminación de sustancias que perjudican la capa de ozono, y afrontar el cambio climático.

Para proteger esta delgada capa, ubicada entre los 19 y los 23 kilómetros sobre la superficie terrestre, debe evitarse el uso de gases aerosoles que perjudiquen la capa troposférica, reducir el uso del aire acondicionado, limitar el uso del automóvil y otros aparatos como compresores y máquinas de césped.
