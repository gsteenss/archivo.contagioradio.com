Title: Ante denuncias de acoso sexual y laboral, renuncia el Defensor del Pueblo
Date: 2016-01-27 22:22
Category: Mujer, Nacional
Tags: Astrid Helena Cristancho, Defensor del pueblo, Jorge Armando Otálora
Slug: ante-denuncias-de-acoso-sexual-y-laboral-renuncia-el-defensor-del-pueblo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/defensor.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El Universal 

###### [27 Ene 2016] 

Ante las presiones de parte de diferentes sectores, entre ellos, organizaciones defensoras de los derechos de las mujeres, **el Defensor del Pueblo, Jorge Armando Otálora, renunció a su cargo por cuenta de los señalamientos por acoso sexual y laboral** en las cual ha estado envuelto en los últimos meses.

El Defensor tomó la decisión de dejar su cargo, luego de haber renunciado **el vicedefensor, Esiquio Manuel Sánchez**, haber perdido el apoyo del Partido Liberal, las presiones de varios congresistas que le solicitaron su renuncia y tras una conversación con el presidente Juan Manuel Santos en la que el mandatario le aconsejó defenderse por fuera de la institución.

El Defensor del Pueblo, Jorge Armando Otálora fue acusado de acoso sexual hacía Astrid Helena Cristancho, quien hace unos meses era su secretaria privada, según la columna de opinión publicada por Daniel Coronell **‘El acoso no era solo laboral, también sexual’**  en la revista Semana, en donde se evidencia que Otálora enviaba constantemente mensajes de whatsapp a Astrid Helena y además enviaba fotos comprometedoras.

Leer: [Caso Astrid Helena Cristancho una radiografía de la violencia sexual en Colombia ](https://archivo.contagioradio.com/caso-de-astrid-helena-cristancho-una-radiografia-de-la-violencia-sexual-en-colombia/)
