Title: Colombia Humana denuncia amenazas de Aguilas Negras
Date: 2018-06-22 09:32
Category: DDHH, Nacional
Tags: amenaza, cesar, Colombia Humana, Derechos Humanos, Panfleto
Slug: colombia-humana-denuncia-amenazas-de-aguilas-negras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/paramilitares-tolima-AUC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Censat] 

###### [22 Jun 2018] 

**El Jueves 21 de junio, el equipo de convergencia internacional de la Colombia Humana hizo públicas las amenazas recibidas en un panfleto de las Águilas Negras**,  que llegó a la sede central de la campaña en el Municipio de San Juan del Cesar, Guajira y al hospital San Rafael del Cesar, en la que advierten de muerte a 10 personas.

**En el pasquín recibido el 16 de junio, intimidan a quienes** [**hicieron**]** parte de la campaña de Gustavo Petro entre los que se encuentran**: Willer José Arciniegas Castro, Boris Armando Palacio Márquez, Aldo Raúl Amaya Daza, Betty Josefina Daza Oñate, Arnoldo Enrique Marulanda, Horacio Ramón Manjarrez Ariza,  y el periodista Hernán Cujía, Jorge Cuello, Arnaldo Gámez, Beto Mendoza,  Camilo y Silvia Daza.

### **No es la primera amenaza contra simpatizantes de Gustavo Petro** 

En el mes de mayo del presente año, diferentes organizaciones defensoras de Derechos Humanos denunciaron amenazas recibidas por medio de correo electrónico en su contra. **En dichas amenazas firmadas por las Águilas Negras, se las declaraba objetivo militar por apoyar el programa de la Colombia Humana**. (Le puede interesar: ["Defensores de DDHH del Tolima amenazados ante intención de voto por Gustavo Petro"](https://archivo.contagioradio.com/defensores-de-ddhh-del-tolima-amenazados-ante-intencion-de-voto-por-gustavo-petro/))

Por estos hechos, desde diferentes organizaciones e igualmente, desde el equipo de la Colombia Humana, se pide celeridad y contundencia en la investigación para determinar el origen de los sucesos y que se prevea la seguridad de quienes se han visto afectados. **De la misma forma, en el comunicado se responsabiliza “a las autoridades civiles y militares de la zona por la integridad de las personas señaladas en este comunicado y las demás que hayan hecho uso de su derecho constitucional de promover el voto o votar por Gustavo Petro Urrego”.**

![Foto Campaña Gustavo Petro](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Imagen-1-442x589.jpg){.alignnone .size-medium .wp-image-54132 width="442" height="589"}  
 

###### [Reciba toda la información de Contagio Radio en [[su correo] 
