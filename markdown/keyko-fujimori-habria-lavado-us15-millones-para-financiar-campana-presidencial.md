Title: Keyko Fujimori habría lavado US$15 millones para financiar campaña presidencial
Date: 2016-05-16 15:13
Category: El mundo, Política
Tags: DEA, Elecciones presidenciales Peru, keyko Fujimori, lavado de activos
Slug: keyko-fujimori-habria-lavado-us15-millones-para-financiar-campana-presidencial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Keyko-Fujimori.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Infobae ] 

###### [16 Mayo 2016] 

Keiko Fujimori, candidata a la presidencia peruana, es investigada por el Departamento Estadounidense Antidrogas, por presunto lavado de activos. Según un testimonio, Joaquín Ramírez, congresista reelegido y actual Secretario General del partido 'Fuerza Popular', **lavó US\$15 millones para financiar la campaña presidencial de Fujimori en 2011**.

La información proviene del testimonio de Jesús Vásquez, un funcionario de la DEA, que grabó una conversación que sostuvo con Ramírez, como parte de una operación encubierta en Miami, en la que el congresista aseguró que Fujimori le había dado los US\$15 millones para **usarlos en su campaña luego de haberlos lavado a través de una cadena de grifos y estaciones de gasolina**.

Contra Ramírez cursa una investigación por lavado de activos en Perú, en ella se incluyen estaciones de gasolina que figuran a su nombre. Por su parte la fiscal peruana, Rosa Villa, solicitó el levantamiento de inmunidad parlamentaria, señalando que el congresista posee "un inmenso patrimonio inmobiliario al que se suman millonarias inversiones y acciones, en personas jurídicas, que **no guardan relación con sus ingresos iniciales como cobrador de transporte público"**.

Cabe recordar que la candidata presidencial es hija del ex presidente Alberto Fujimori, quien paga una condena de 25 años por la comisión de crímenes de lesa humanidad y el **desvío de recursos públicos, a través del que durante sus 10 años de gobierno logró reunir una fortuna cercana a los US\$600 millones**, según Transparencia Internacional.

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
