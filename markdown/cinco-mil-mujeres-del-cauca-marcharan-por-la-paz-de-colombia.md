Title: Cinco mil mujeres del Cauca marcharán por la paz de Colombia
Date: 2015-11-24 16:12
Category: Movilización, Nacional
Tags: Cauca, Colegio de Las Salesianas, colombia, ELN, FARC, mujeres marchan por la paz, organizaciones de mujeres, paz, proceso de paz
Slug: cinco-mil-mujeres-del-cauca-marcharan-por-la-paz-de-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Mujeres-marchan-por-la-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [boletinesdeprensacompromiso.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_9508497_2_1.html?data=mpqdmpmde46ZmKiakpmJd6KmlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8rixNSYz87Qb87py8rfx9iPqMbgjKjO18jFb87V08jVw9eJh5SZopbbjdXTtozgwpDdw9%2BPqMafpNSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Marina Gallego] 

###### [24 Nov 2015] 

Este 25 de noviembre, más de cinco mil mujeres pertenecientes a 42 organizaciones sociales de todo el país realizarán una marcha en el departamento del Cauca para expresar su **apoyo al proceso de paz con las FARC y también demostrando su respaldo a los diálogos exploratorios con la guerrilla del ELN.**

Esta marcha, se realiza en el marco del **Día Internacional de la No Violencia contra las Mujeres,** teniendo en cuenta que el papel de las mujeres es fundamental en escenarios de construcción de paz, como lo expresa la carta escrita por las organizaciones en donde se convoca a las mujeres y a los sectores sociales a que participen en la marcha, allí también se agrega que en ese contexto, es necesaria la participación de la mujer en la implementación de los futuros acuerdos de paz.

“Sabemos que la Paz no pasa exclusivamente por el fin de la guerra, pero sabemos también que **la lucha por los derechos de las mujeres se podrá desarrollar de manera más efectiva en ausencia de los horrores generados por el conflicto armado”,** se afirma en la carta.

“Convocamos e invitamos a que a esta movilización se sumen mujeres campesinas, indígenas, afrodescendientes, urbanas, jóvenes, mayores, que han visto y han vivido los horrores de la guerra, y que hoy nos declaramos como constructoras de paz”, dice el comunicado.

La marcha se realizará en la ciudad de Popayán a las 9 de la mañana, desde el Colegio las Salesianas y llegará hasta el Parque Caldas, donde se realizará un concierto Antimilisonoro a las 3 de la tarde.
