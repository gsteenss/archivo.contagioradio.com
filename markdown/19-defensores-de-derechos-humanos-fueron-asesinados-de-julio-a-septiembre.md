Title: 19 defensores de Derechos Humanos fueron asesinados de Julio a Septiembre de 2016
Date: 2016-10-13 15:25
Category: DDHH, Otra Mirada
Tags: defensores de derechos humanos, Paramilitarismo, Somos defensores
Slug: 19-defensores-de-derechos-humanos-fueron-asesinados-de-julio-a-septiembre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_132.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: contagioradio.com] 

###### [13 Oct 2016]

Según el Sistema de Información de Agresiones a Defensores de Derechos Humanos, durante el trimestre de Julio a Septiembre han sido asesinados 19 defensores y se han registrado un total de 63 agresiones individuales. El informe publicado este jueves revela que, aunque **hay una disminución en el número de agresiones individuales, el número de asesinatos y atentados creció** en relación al mismo periodo de 2015. (Lea tambien: [Primer semestre de 2016 con record de agresiones a defensores de DDHH](https://archivo.contagioradio.com/aumentan-los-riesgos-para-defensores-de-ddhh-en-colombia/))

El informe señala que “**A septiembre de 2016 reportamos 54 homicidios de líderes sociales y defensores en el mismo periodo en 2015 se contabilizaban 51**.” Es decir 2 casos más, situación que contrasta con la disminución en otras agresiones como amenazas y hurto de información, 38 y 1 respectivamente, las cuales se redujeron en un porcentaje importante en el mismo periodo de tiempo analizado para el año anterior. (Lea también: [Balance de agresiones contra defensores en 2015](https://archivo.contagioradio.com/577-agresiones-contra-defensores-de-ddhh-se-han-registrado-en-2015/))

El documento también resalta que los principales departamentos en que se han presentado las agresiones contra defensores de derechos humanos son **Antioquia con 11 casos, Santander con 6, Cauca y Chocó con 5 cada uno**. Así mismo reiteró a los **paramilitares como los principales responsables de las agresiones** con responsabilidad en 15 casos, es decir el 24% de las agresiones. (Lea también: [Hay una oportunidad de acabar con el paramilitarismo](https://archivo.contagioradio.com/hay-una-oportunidad-de-cerrar-el-tema-del-paramilitarismo-en-colombia-coeuropa/))

En el caso de las guerrillas, por parte de las FARC no se registró ninguna agresión y solamente una de ellas sería atribuible al ELN, mientras que a la Fuerza Pública se le endilgan 4 casos, es decir un 6% de las agresiones, hecho que por una parte es esperanzador, para el caso de las guerrillas, y por otro lado **preocupante por las actuaciones de la fuerza pública en el marco del desarrollo de las conversaciones de paz.**

El Sistema señala también que se presentaron varias agresiones en el marco de la campaña por el plebiscito y resalta que hay una situación de “inestabilidad política” que generó la victoria del NO, así mismo denuncia que, aunque se han expresado las preocupaciones frente a las agresiones, la **acción del Estado para prevenir este tipo de hechos es prácticamente “nula”**.
