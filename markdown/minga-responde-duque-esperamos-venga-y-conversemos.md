Title: Minga responde a Duque: lo esperamos, venga, conversemos y busquemos soluciones
Date: 2019-03-30 19:14
Category: Comunidad, Movilización
Tags: Feliciano Valencia, Minga en el Cauca, Minga Nacional
Slug: minga-responde-duque-esperamos-venga-y-conversemos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/onic.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Feliciano Valencia] 

###### [30 Mar 2019] 

El senador **Feliciano Valencia**, en nombre de la bancada de oposición y de la Minga del suroccidente del país,  reiteró su voluntad de diálogo e invitó una vez más al presidente Duque para que visite el Cauca y de la mano puedan encontrar una solución a las exigencias de los pueblos , en particular a una inclusión satisfactoria en **el Plan Nacional de Desarrollo.**

"Es verdad que el presidente ha anunciado un piso presupuestal de 10 billones de pesos, los que tienen que satisfacer las necesidades de los **102 pueblos indígenas**, la Minga ratifica que esos recursos no pueden ser transversales ni regados en los diferentes pactos del Plan Nacional de Desarrollo" esto con el fin que no se repitan las historias de incumplimiento de los años pasados, en los que el dinero se vio dividido en diferentes artículos del PND, señaló el senador.[(Le puede interesar: No pedimos tierras para una sola persona, sino para miles; Minga Indígena)](https://archivo.contagioradio.com/no-pedimos-tierras-para-una-sola-persona-sino-para-miles-minga-indigena/)

A su vez se refirió al llamado que han hecho por la defensa de la vida de líderes indígenas y sociales e invitó una vez más al presidente Duque al diálogo para solucionar las exigencias de los sectores concentrados en la Minga, **"los pueblos, comunidades, organizaciones y la sociedad necesitan soluciones, así que reiteramos nuestra invitación para que se encuentre con la Minga"**, agregó.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FFelicianoValenciaSenador%2Fvideos%2F2164311897017721%2F&amp;show_text=0&amp;width=560" width="560" height="315" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

**"Venga señor presidente aquí no hay gente violenta, somos pueblos que hemos sufrido la riguriosidad del abandono, no obstante nos negamos a desaparecer"** añadió el senador quien destacó que esta movilización se va tornando en una Minga Nacional que poco a poco ha convocado otros pueblos y otras comunidades.  [(Lea también: ¡Con fuerza! La Minga Pijao también avanza en el Tolima)](https://archivo.contagioradio.com/con-fuerza-la-minga-pijao-nasa-tambien-avanza-en-tolima/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
