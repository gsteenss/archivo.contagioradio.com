Title: Fuerza pública impidió la participación de comunidad en asamblea del Paro Nacional
Date: 2019-12-05 12:22
Author: CtgAdm
Category: Comunidad, Paro Nacional
Tags: campesinos, congreso de los pueblos, La Lizama, Paro Nacional, policia
Slug: fuerza-publica-impidio-la-participacion-de-comunidad-en-asamblea-del-paro-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Foto-de-Cahucopona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CAHUCOPANA  
] 

Congreso de los Pueblos denunció que en la noche del pasado miércoles 4 de diciembre, **la Policía retuvo a más de 300 personas que se desplazaban desde Segovia (Antioquia) hacía La Lizama (Santander), para adelantar la asamblea local de la región del Magdalena Medio, en el marco del Paro Nacional.** La Policía retuvo a los campesinos en Vegachi aduciendo que paramilitares se tomarían la vía que conduce de Limón a Maceo para atacarlos, y posteriormente, uniformados de la SIJIN, ESMAD, Policía y Ejército llegaron para hostigarlos, y advertirles que no los dejarían llegar a su destino.

### **Un camino lleno de trampas para impedir una asamblea popular** 

Según Erika Prieto, integrante de Congreso de los Pueblos, los 300 campesinos que salían de Segovia en 7 buses, se dirigían a La Lizama, lugar histórico de movilización en la región del Magdalena Medio, **para realizar una asamblea local en el marco del Paro Nacional.** Recién abandonaron Segovia, la Policía los detuvo para revisar la documentación y los buses en los que se desplazaban, pero los dejaron continuar en vista de que todo estaba en regla.

Una vez pasaron el puente de Vegachí, Prieto afirmó que personal de Policía y Ejército los detuvo, con el argumento de que había paramilitares sobre la vía y que ellos creían que pusieron un elemento explosivo para atacar a los campesinos. No obstante, sobre las 11 de la noche, se percataron que la vía estaba abierta y había paso para todo tipo de vehículos, entonces, "se dan cuenta que **era mentira, pero también, una amenaza soterrada contra la vida de los manifestantes, porque cualquier cosa que pasara, dirían que fue culpa del dichoso grupo paramilitar"**.

Así, los campesinos deciden seguir su camino y sobre las 2 de la mañana, después de pasar por Maceo, uniformados le indican al primer bus que no puede continuar porque hay un problema con sus papeles, los mismos que habían sido revisados en Segovia. El vehículo regresa hasta Maceo para verificar junto a los demás conductores sus papeles, y deciden continuar el camino, pero cerca de 40 minutos después, "son detenidos por el ESMAD, la Sijín, la Policía y el Ejército, y el coronel Andrade de la Policía les dice que lo graben y hagan lo que quieran porque él no los va a dejar pasar".

Desde la madrugada hasta cerca de las 9 de la mañana, según la denuncia, los uniformados hostigaron a los más de 300 campesinos con amenazas de lanzarles bombas lacrimógenas, y **manteniendo el armamento empuñado y sin seguro**. Prieto afirmó que sobre las 6:30 am también adelantaron una requisa al vehículo, tras la que tampoco los dejaron arrancar, y les dijeron que "la vía está cerrada solo para ellos". (Le puede interesar:["La defensa de la vida, un punto central del Paro Nacional"](https://archivo.contagioradio.com/la-defensa-de-la-vida-un-punto-central-del-paro-nacional/))

La integrante del Congreso de los Pueblos sostuvo que el coronel del Ejército de apellido Salinas les permitió continuar su camino, pero por el temor a la situación vivida por los 300 campesinos, muchas de las personas que salían del otro lado del Río Magdalena no acudieron a la Asamblea en La Lizama. De esta forma, también se puede ver afectada la participación que estaba planteada realizar en el marco de la asamblea del paro que se desarrollará en Bogotá los días 6 y 7 de diciembre.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe><iframe id="audio_45283050" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_45283050_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
