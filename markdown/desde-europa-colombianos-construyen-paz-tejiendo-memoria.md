Title: Desde Europa colombianos construyen paz tejiendo memoria
Date: 2016-03-19 08:00
Category: Nacional, Paz
Tags: Alfredo Holguín, Diálogos de paz en Colombia, Foro Internacional de Víctimas
Slug: desde-europa-colombianos-construyen-paz-tejiendo-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Foro-Internal-V.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Foro Internacional de Víctimas] 

<iframe src="http://co.ivoox.com/es/player_ek_10881924_2_1.html?data=kpWlmpaddpWhhpywj5adaZS1k5aah5yncZOhhpywj5WRaZi3jpWah5yncaXZ1MnSjarZttDkwpDQ0dHTscPdwtPc1ZDHs8%2Fn1dfi28rSb9HV25Dhx8%2FNqc%2FY0JDax9LTtsqhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alfredo Holguín] 

###### [19 Mar 2016] 

Desde este sábado y hasta el próximo lunes en Tynset, Noruega se lleva a cabo el II Foro Internacional de Víctimas, organizado por colombianos víctimas del conflicto armado que se han exiliado en veintidos países de Europa y América Latina. Según afirma Alfredo Holguín, este evento se realiza con el fin de exhortar al Gobierno colombiano y a las FARC-EP para que continúen en conversaciones y lleguen a un acuerdo final, exigir que se formalice la mesa de diálogos con el ELN, hacer un llamado frente a los derechos de las víctimas exiliadas y generar **procesos de pedagogía de paz para que los colombianos que residen en el exterior se informen sobre los acuerdos que se están pactando en La Habana**.

Esta estrategia nació hace dos año en el marco de los foros de víctimas que se realizaron en varios lugares de Colombia, tras los que aquellos colombianos residentes en el exterior se percataron de la **escasa mención que se hacía de ellos aún cuando eran víctimas directas o indirectas del conflicto armado**, por lo que decidieron convocar reuniones que contaron con la participación de colombianos exiliados, funcionarios del Centro de Memoria y de la Unidad de Víctimas para construir insumos que aportaron a la discusión sobre el punto de Víctimas en la Mesa de Diálogos de La Habana.

Este segundo foro cuenta con una agenda en tres niveles, el primero aborda la cultura en relación con la memoria a través de talleres focalizados para **mujeres, jóvenes y niños que podrán aportar desde sus especificidades a la construcción de paz**. En un segundo nivel se aborda la fase actual de los Diálogos de paz y estudia sus proyecciones. El tercer nivel muestra los resultados sobre los estudios que se han hecho a las políticas de atención a las víctimas como la Ley 1448 y el punto quinto aprobado en La Habana, para evaluar las propuestas que las víctimas exiliadas presentan para su atención, así como la refrendación de los acuerdos y la pedagogía para la paz.

Esculturas en nieve, una carrera de skie en la que participaran migrantes de Afganistan, Eritrea y Siria y otros actos culturales harán parte de este segundo encuentro que por la paz de Colombia convocan organizaciones de víctimas del conflicto armado que decidieron exiliarse pero que no desestiman la posibilidad de retornar.

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
