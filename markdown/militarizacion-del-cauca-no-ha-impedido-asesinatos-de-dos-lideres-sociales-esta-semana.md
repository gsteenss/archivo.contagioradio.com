Title: Militarización del Cauca no ha impedido asesinatos de dos líderes sociales esta semana
Date: 2018-07-18 15:59
Category: DDHH, Nacional
Tags: Cauca, Derechos Humanos, Eduardo Dagua, ejercito, Ibes Trujillo
Slug: militarizacion-del-cauca-no-ha-impedido-asesinatos-de-dos-lideres-sociales-esta-semana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/LIDERES.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 Jul 2018] 

Organizaciones defensoras de derechos humanos rechazaron las afirmaciones hechas por el Coronel Fabio Rojas, que manifestó que Eduardo Dagua no era un líder social, sino el padre de un excombatiente de la antigua guerrilla FARC. De igual forma, cuestionaron el accionar de la Fuerza Pública en el departamento del Cauca, debido a que es uno de los territorios en donde hay más presencia de Ejército y Policía**, y al mismo tiempo, en donde más se presentan asesinatos a líderes, que esta semana ya cobró la vida de Dagua e Ibes Trujillo.**

Según Deivin Hurtado, las afirmaciones hechas por el Coronel Rojas hacen parte de una **“revictimización hacia los líderes y sus familias al intentar negar los procesos sociales que ellos adelantaban con sus comunidades”,** además señaló que no es la primera vez que el Coronel descalifica los liderazgos y afirmó que “es una situación que busca desviar la mirada sobre los sistemáticos homicidios, en vez de ayudar y aportar sobre el tema de investigación”.

Frente al papel que desarrollaba Dagua en el territorio, Hurtado señaló que fue uno de los pioneros de la Asociación Campesina en el municipio de Caloto y era el encargado de los temas sobre adulto mayor en la junta de acción comunal de su vereda. (Le puede interesar: ["Hallado sin vida el líder social Eduardo Dagua, en Caloto Cauca"](https://archivo.contagioradio.com/encontrado-sin-vida-el-lider-social-luis-eduardo-dagua-en-caloto-cauca/))

Asimismo, confirmó que Dagua si es el padre de un integrante de FARC en proceso de reincorporación, hecho que preocupa aún más frente a los asesinatos a integrantes de este partido o sus familiares. Además, expreso que c**erca de la vivienda de Dagua, en donde fue asesinado, se había acantonado el Ejército**.

<iframe id="audio_27127087" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27127087_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
